FROM node:16.16.0-alpine3.16

WORKDIR /app

RUN apt-get update && apt-get install ffmpeg -y

COPY package.json yarn.lock nx.json tsconfig.base.json jest.config.js jest.preset.js workspace.json ./
RUN yarn install --frozen-lockfile --dev

COPY apps ./apps
COPY libs/ ./libs
COPY envs ./envs
COPY envs/production.env ./env/.env
COPY  ./apps/api/ops/entrypoint.sh ./
COPY migrations/ ./migrations
COPY docs/ ./docs

CMD ["yarn", "api:e2e:ci"]
