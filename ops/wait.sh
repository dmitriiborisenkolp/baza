#!/bin/sh
set -e
  
host="$1"
attempts=30
shift
  
until curl -sf "$host" -o /dev/null; do
  echo "API is unavailable - sleeping"
  attempts=$(expr $attempts - 1)
  sleep 5
done
  
echo "API is up - executing command"
exec "$@"
