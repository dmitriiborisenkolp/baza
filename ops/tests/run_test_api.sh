#!/bin/bash
set -e
echo run api-tests

#git status to check the latest files modified in directory.
libs=`(git fetch origin develop:develop) && (git diff --name-status develop | awk '{print $2}' | grep "^libs\/" | xargs dirname | cut -d "/" -f2 | uniq | xargs -n 1 | sort -u | xargs)`

echo $libs
docker-compose -f docker-compose.e2e-ci.yml exec -T api_e2e npx jest --clearCache
for i in $libs
do
  docker-compose -f docker-compose.e2e-ci.yml exec -T api_e2e cross-env NODE_ENV=test NODE_OPTIONS='--max-old-space-size=4096' BAZA_E2E_ENDPOINT=http://api_e2e:3000 npx jest  --runInBand --forceExit --bail --passWithNoTests --logHeapUsage --config ./jest.config.js ./libs/$i
done
