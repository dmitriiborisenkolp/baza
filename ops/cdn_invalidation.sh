#!/bin/bash
CDN_DOMAIN="$1"
for i in {1..3}
do
   aws cloudfront create-invalidation --distribution-id $CDN_DOMAIN --paths "/*" 2> /dev/null
   if [ $? -eq 0 ]; then
      echo "Cloudfront invalidated!"
      break
   elif [ $i -eq 3 ]; then
      echo "ERROR: Cloudfront not invalidated!"
      exit 1
   else
      echo "ERROR: Cloudfront not invalidated! Trying again in 10s"
      sleep 10
   fi
done
