import { AuthJwtDeviceStrategies, bazaApiBundleConfigBuilder, BazaPhoneCountryRepositoryStrategy } from '@scaliolabs/baza-core-api';
import { apiRegistry } from './app/api.registry';
import { apiEnI18n } from '../i18n/en/api-en.i18n';
import { cmsEnI18n } from '../i18n/en/cms-en.i18n';
import { webEnI18n } from '../i18n/en/web-en.i18n';
import {
    Application,
    BAZA_ENVIRONMENTS_CONFIG,
    BazaEnvironments,
    ConfirmationEmailExpiryResendStrategy,
} from '@scaliolabs/baza-core-shared';
import { apiFactory } from './app/api.factory';
import { bazaContentTypesApiTags, bazaContentTypesCmsApiTags } from '@scaliolabs/baza-content-types-shared';
import { bazaNcApiConfig, bazaNcSetCharset, bazaNorthCapitalApiTags, NcCharset } from '@scaliolabs/baza-nc-shared';
import * as deepmerge from 'deepmerge';
import {
    BazaNcIntegrationCmsOpenApi,
    BazaNcIntegrationOpenApi,
    BazaNcIntegrationPublicOpenApi,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaDwollaCmsOpenApi, BazaDwollaOpenApi, BazaDwollaPublicOpenApi } from '@scaliolabs/baza-dwolla-shared';
import { mainCustomerio } from './main-customerio';

let IS_CONFIGURED = false;

if (!IS_CONFIGURED) {
    bazaNcSetCharset(NcCharset.En);

    const currentEnv = process.env.BAZA_ENV as BazaEnvironments;

    const apiConfig = [
        () =>
            bazaNcApiConfig({
                withTransactionFeeFeature: true,
                strictIntegerChecks: false,
                enableDwollaSupport: true,
                enableLegacyBankAccountsSystem: true,
                enableEmailNotificationForOfferingStatus: false,
                enableAutoCancellingFailedTrades: false,
            }),
        () => {
            if (currentEnv === 'e2e') {
                bazaNcApiConfig({
                    maskCreditCardFields: false,
                    maskSSNFields: false,
                    maskBankDetailsFields: false,
                    withTransactionFeeFeature: false,
                    forceSecureBankAccounts: false,
                    enableEmailNotificationForOfferingStatus: true,
                });
            }
        },
        () => {
            bazaNcApiConfig({
                requestsDelay: 100,
            });
        },
    ];

    apiConfig.forEach((config) => config());

    bazaApiBundleConfigBuilder()
        .withModuleConfigs({
            BazaPhoneCountryCodesApiModule: () => ({
                injects: [],
                useFactory: async () => ({
                    repository: {
                        type: BazaPhoneCountryRepositoryStrategy.Local,
                    },
                    ttl: 60 /* minutes */ * 60 /* seconds */ * 1000 /* ms */,
                }),
            }),
        })
        .withApiFactory(apiFactory)
        .withAuthDevicesStrategy(AuthJwtDeviceStrategies.Psql)
        .withRegistrySchema((defaults) => ({
            ...defaults,
            ...apiRegistry,
        }))
        .withI18n([
            {
                app: Application.API,
                factory: (language, defaults) => deepmerge.all([defaults(language), apiEnI18n]),
            },
            {
                app: Application.CMS,
                factory: (language, defaults) => deepmerge.all([defaults(language), cmsEnI18n]),
            },
            {
                app: Application.WEB,
                factory: (language, defaults) => deepmerge.all([defaults(language), webEnI18n]),
            },
        ])
        .withTagGroups((defaults) => [
            ...defaults,
            {
                name: 'Baza Content Types',
                tags: [...bazaContentTypesApiTags],
            },
            {
                name: 'Baza Content Types CMS',
                tags: [...bazaContentTypesCmsApiTags],
            },
            {
                name: 'Baza North Capital',
                tags: [...bazaNorthCapitalApiTags],
            },
            {
                name: BazaNcIntegrationOpenApi.BazaNcIntegration,
                tags: Object.values(BazaNcIntegrationPublicOpenApi),
            },
            {
                name: BazaNcIntegrationOpenApi.BazaNcIntegrationCms,
                tags: Object.values(BazaNcIntegrationCmsOpenApi),
            },
            {
                name: BazaDwollaOpenApi.BazaDwolla,
                tags: Object.values(BazaDwollaPublicOpenApi),
            },
            {
                name: BazaDwollaOpenApi.BazaDwollaCms,
                tags: Object.values(BazaDwollaCmsOpenApi),
            },
        ])
        .withConfirmationEmailExpiryResendStrategy(ConfirmationEmailExpiryResendStrategy.AUTO)
        .withNDigitsEmailVerificationTokenizer()
        .withWhitelist()
        .withMailTemplateValidation();

    if (
        ![...BAZA_ENVIRONMENTS_CONFIG.testEnvironments, ...BAZA_ENVIRONMENTS_CONFIG.localEnvironments].includes(
            process.env.BAZA_ENV as BazaEnvironments,
        )
    ) {
        bazaApiBundleConfigBuilder().withCustomerIoTemplates(mainCustomerio);
    }

    IS_CONFIGURED = true;
}
