import { Module } from '@nestjs/common';
import { BazaContentTypesApiE2eModule } from '@scaliolabs/baza-content-types-api';
import { BazaNcApiE2eModule } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationE2eModule } from '@scaliolabs/baza-nc-integration-api';
import { BazaDwollaE2eBundleModule } from '@scaliolabs/baza-dwolla-api';

const E2E_MODULES = [
    // baza-content-type E2E module
    BazaContentTypesApiE2eModule,

    // baza-nc E2E module
    BazaNcApiE2eModule,

    // baza-nc-integration E2E module
    BazaNcIntegrationE2eModule,

    // baza-dwolla e2e module
    BazaDwollaE2eBundleModule,
];

@Module({
    imports: E2E_MODULES,
    exports: E2E_MODULES,
})
export class ApiE2eModule {}
