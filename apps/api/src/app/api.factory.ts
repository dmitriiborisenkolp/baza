import '../main-config';

import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import { EventBus } from '@nestjs/cqrs';
import { ApiModule } from './api.module';
import { getBazaProjectCodeName } from '@scaliolabs/baza-core-shared';

export async function apiFactory(): Promise<NestExpressApplication> {
    const logger = new BazaLogger();
    logger.setContext(getBazaProjectCodeName());

    const app = await NestFactory.create<NestExpressApplication>(ApiModule.forRootAsync(), {
        cors: true,
        rawBody: true,
        logger,
    });

    BazaLogger.eventBus = app.get(EventBus);

    return app;
}
