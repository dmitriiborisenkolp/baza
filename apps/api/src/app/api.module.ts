import { DynamicModule, Global, Module } from '@nestjs/common';
import { bazaApiBundleConfigBuilder, BazaApiBundleModule, EnvService, pinoLoggerConfigBuilder } from '@scaliolabs/baza-core-api';
import { BazaNcApiBundleModule } from '@scaliolabs/baza-nc-api';
import { BazaDocsApiModule } from '@scaliolabs/baza-docs-api';
import { BazaContentTypesApiBundleModule } from '@scaliolabs/baza-content-types-api';
import { MigrationModule } from '@scaliolabs/migrations';
import { BazaNcIntegrationApiModule, BazaNcIntegrationSearchStrategy } from '@scaliolabs/baza-nc-integration-api';
import { ApiE2eModule } from './api-e2e.module';
import { BazaDwollaApiBundleModule } from '@scaliolabs/baza-dwolla-api';
import { BazaPlaidApiModule } from '@scaliolabs/baza-plaid-api';
import { apiSchemas } from './api.schemas';
import { LoggerModule } from 'nestjs-pino';

@Global()
@Module({})
export class ApiModule {
    static forRootAsync(): DynamicModule {
        return {
            module: ApiModule,
            imports: [
                // baza-core API module
                BazaApiBundleModule.forRootAsync(bazaApiBundleConfigBuilder().config),

                // baza-content-type API module
                BazaContentTypesApiBundleModule.forRootAsync(() => ({
                    withKafkaEvents: false,
                })),

                // baza-nc API module
                BazaNcApiBundleModule.forRootAsync({}),

                // baza-docs API module
                BazaDocsApiModule,

                // baza-nc-integration API module
                BazaNcIntegrationApiModule.forRootAsync({
                    search: {
                        strategy: {
                            type: BazaNcIntegrationSearchStrategy.Memory,
                        },
                    },
                    schema: {
                        schemas: apiSchemas,
                    },
                }),

                // baza-dwolla module
                BazaDwollaApiBundleModule.forRootAsync(),

                // baza-plaid module
                BazaPlaidApiModule,

                // baza example migrations
                MigrationModule,

                // e2e modules
                ApiE2eModule,
            ],
        };
    }
}
