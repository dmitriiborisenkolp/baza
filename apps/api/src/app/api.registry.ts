import { RegistrySchema } from '@scaliolabs/baza-core-shared';
import { bazaNcApiBundleRegistry } from '@scaliolabs/baza-nc-api';
import { bazaContentTypesApiBundleRegistry } from '@scaliolabs/baza-content-types-api';
import { bazaNcIntegrationApiRegistry } from '@scaliolabs/baza-nc-integration-api';
import { bazaDwollaApiBundleRegistry } from '@scaliolabs/baza-dwolla-api';
import { bazaMailBundleRegistry } from '@scaliolabs/baza-core-api';

export const apiRegistry: RegistrySchema = {
    ...bazaContentTypesApiBundleRegistry,
    ...bazaNcApiBundleRegistry,
    ...bazaNcIntegrationApiRegistry,
    ...bazaDwollaApiBundleRegistry,
    ...bazaMailBundleRegistry,
};
