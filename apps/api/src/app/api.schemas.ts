import { BazaNcIntegrationSchemaCmsCreateRequest } from '@scaliolabs/baza-nc-integration-shared';

/**
 * You can add Auto-Generated Listing Schema here
 * These Schemas will be automatically created, renamed, updated or deleted depends on changes
 */
export const apiSchemas: Array<BazaNcIntegrationSchemaCmsCreateRequest> = [];
