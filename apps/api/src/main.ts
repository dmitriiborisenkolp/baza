import '@angular/compiler';
import { ApiBootstrap, bazaApiBundleConfigBuilder } from '@scaliolabs/baza-core-api';
import { BazaLogger } from '@scaliolabs/baza-core-api';

import './main-acl';
import './main-config';

const appBootstrapInstance = new ApiBootstrap(bazaApiBundleConfigBuilder().config);

appBootstrapInstance
    .bootstrap()
    .then(async (app) => {
        const logger = await app.resolve(BazaLogger);

        logger.log(`[main.ts] API started`);
    })
    .catch((err) => {
        console.error(err);
        console.error(`[main.ts] API failed to start`);
    });
