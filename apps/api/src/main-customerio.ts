import { BAZA_ENVIRONMENTS_CONFIG, BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaCoreMail, SendMailCustomerIoBootstrap } from '@scaliolabs/baza-core-api';
import { BazaContentTypesMail } from '@scaliolabs/baza-content-types-api';
import { BazaNcMail } from '@scaliolabs/baza-nc-api';

export const mainCustomerio: (env: BazaEnvironments) => Array<SendMailCustomerIoBootstrap> = (env) => {
    return BAZA_ENVIRONMENTS_CONFIG.testEnvironments.includes(env)
        ? []
        : [
              {
                  name: BazaCoreMail.BazaAccountCmsChangeEmail,
                  customerIoId: 4,
              },
              {
                  name: BazaCoreMail.BazaAccountCmsResetPassword,
                  customerIoId: 5,
              },
              {
                  name: BazaCoreMail.BazaAccountCmsChangeEmailNotification,
                  customerIoId: 7,
              },
              {
                  name: BazaCoreMail.BazaAccountCmsPasswordChanged,
                  customerIoId: 6,
              },
              {
                  name: BazaCoreMail.BazaAccountWebAccountDeactivated,
                  customerIoId: 8,
              },
              {
                  name: BazaCoreMail.BazaAccountWebAlreadyRegistered,
                  customerIoId: 9,
              },
              {
                  name: BazaCoreMail.BazaAccountWebChangeEmail,
                  customerIoId: 10,
              },
              {
                  name: BazaCoreMail.BazaAccountWebChangeEmailNotification,
                  customerIoId: 11,
              },
              {
                  name: BazaCoreMail.BazaAccountWebConfirmEmail,
                  customerIoId: 12,
              },
              {
                  name: BazaCoreMail.BazaAccountWebDeactivateAccount,
                  customerIoId: 13,
              },
              {
                  name: BazaCoreMail.BazaAccountWebPasswordChanged,
                  customerIoId: 14,
              },
              {
                  name: BazaCoreMail.BazaAccountWebRequestResetPassword,
                  customerIoId: 15,
              },
              {
                  name: BazaCoreMail.BazaAccountWebResetPassword,
                  customerIoId: 16,
              },
              {
                  name: BazaCoreMail.BazaAuthTwoFactorAuth,
                  customerIoId: 17,
              },
              {
                  name: BazaCoreMail.BazaInviteCodeRequest,
                  customerIoId: 18,
              },
              {
                  name: BazaCoreMail.BazaReferralCodeSignedUpWithReferralCode,
                  customerIoId: 19,
              },
              {
                  name: BazaContentTypesMail.BazaContentTypesContactClientRequest,
                  customerIoId: 21,
              },
              {
                  name: BazaContentTypesMail.BazaContentTypesContactClientResponse,
                  customerIoId: 22,
              },
              {
                  name: BazaContentTypesMail.BazaContentTypesNewslettersInquiryEmail,
                  customerIoId: 23,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaTransferInitiated,
                  customerIoId: 24,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaTransferFailed,
                  customerIoId: 25,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaTransferCompleted,
                  customerIoId: 26,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaTransferCanceled,
                  customerIoId: 27,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaReport,
                  customerIoId: 28,
              },
              {
                  name: BazaNcMail.BazaNcDividendConfirmProcessTransaction,
                  customerIoId: 29,
              },
              {
                  name: BazaNcMail.BazaNcDividendSuccessfulPaymentWithOffering,
                  customerIoId: 30,
              },
              {
                  name: BazaNcMail.BazaNcDividendSuccessfulPaymentWithoutOffering,
                  customerIoId: 31,
              },
              {
                  name: BazaNcMail.BazaNcOfferingClientNotification,
                  customerIoId: 32,
              },
              {
                  name: BazaNcMail.BazaNcReportNcEscrowNotification,
                  customerIoId: 33,
              },
              {
                  name: BazaNcMail.BazaNcFundsTransferred,
                  customerIoId: 34,
              },
              {
                  name: BazaNcMail.BazaNcInvestmentNotification,
                  customerIoId: 35,
              },
              {
                  name: BazaNcMail.BazaNcInvestmentSubmitted,
                  customerIoId: 36,
              },
              {
                  name: BazaNcMail.BazaNcPaymentReturned,
                  customerIoId: 37,
              },
              {
                  name: BazaNcMail.BazaNcTradeCanceled,
                  customerIoId: 38,
              },
          ];
};
