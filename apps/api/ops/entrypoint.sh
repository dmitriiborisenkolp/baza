#!/bin/sh

set -e

node dist/apps/cli/typeorm/migrate/main.js
node dist/apps/cli/kafka/bootstrap-topics/main.js

node dist/apps/api/main.js
