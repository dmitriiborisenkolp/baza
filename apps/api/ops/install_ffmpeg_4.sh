#!/bin/sh

wget -q https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz
wget -q https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz.md5

if ! md5sum -c ffmpeg-release-amd64-static.tar.xz.md5 --status ; then
   echo "md5sum: WARNING: 1 computed checksum did NOT match"
   exit 1;
fi

tar xf ffmpeg-release-amd64-static.tar.xz

rm -rf ffmpeg-release-amd64-static.*
mv "$(find . -type d -name 'ffmpeg-*')" ffmpeg

mv ffmpeg/ffmpeg ffmpeg/ffprobe /usr/local/bin
rm -rf ffmpeg
