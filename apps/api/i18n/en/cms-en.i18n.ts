import * as deepmerge from 'deepmerge';
import { bazaNcCmsEnI18n } from '@scaliolabs/baza-nc-shared';
import { bazaContentTypesCmsI18n } from '@scaliolabs/baza-content-types-shared';
import { bazaNcIntegrationCmsEnI18n } from '@scaliolabs/baza-nc-integration-shared';
import { bazaDwollaCmsEnI18n } from '@scaliolabs/baza-dwolla-shared';

const i18nOverride: any = {};

export const cmsEnI18n: any = deepmerge.all([
    bazaNcCmsEnI18n,
    bazaContentTypesCmsI18n,
    bazaNcIntegrationCmsEnI18n,
    bazaDwollaCmsEnI18n,
    i18nOverride,
]);
