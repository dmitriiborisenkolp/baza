import * as deepmerge from 'deepmerge';
import { bazaNcApiEnI18n } from '@scaliolabs/baza-nc-shared';
import { bazaContentTypesApiI18n } from '@scaliolabs/baza-content-types-shared';
import { bazaNcIntegrationApiEnI18n } from '@scaliolabs/baza-nc-integration-shared';

const i18nOverride = {};

export const apiEnI18n = deepmerge.all([bazaContentTypesApiI18n, bazaNcApiEnI18n, bazaNcIntegrationApiEnI18n, i18nOverride]);
