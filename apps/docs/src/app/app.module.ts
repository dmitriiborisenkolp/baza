import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { docsAppRoutes } from './app.routes';
import { HttpClientModule } from '@angular/common/http';
import { bazaWebBundleConfigBuilder, BazaWebBundleModule } from '@scaliolabs/baza-core-web';
import { BUILT_ON_API_VERSION } from './app.version';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

if (environment.sentry) {
    bazaWebBundleConfigBuilder()
        .withSentry(environment.sentry.dsn, environment.sentry.browserOptions);
}

const config = bazaWebBundleConfigBuilder()
    .withVersion(BUILT_ON_API_VERSION)
    .withEnvironment(environment)
    .config;

const NG_MODULES = [
    HttpClientModule,
];

const BAZA_MODULES = [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    RouterModule.forRoot([], {
        initialNavigation: 'enabled',
        scrollPositionRestoration: 'top',
        anchorScrolling: 'enabled',
    }),
    BazaWebBundleModule.forRoot(config),
];

@NgModule({
    imports: [
        ...NG_MODULES,
        ...BAZA_MODULES,
        BrowserAnimationsModule,
        RouterModule.forRoot(docsAppRoutes),
    ],
    declarations: [
        AppComponent,
    ],
    bootstrap: [
        AppComponent,
    ],
})
export class AppModule {
}
