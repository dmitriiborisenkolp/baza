import { Routes } from '@angular/router';
import { BazaI18nPrefetchResolver } from '@scaliolabs/baza-core-ng';

export const docsAppRoutes: Routes = [
    {
        path: '',
        pathMatch: 'prefix',
        resolve: [
            BazaI18nPrefetchResolver,
        ],
        children: [
            {
                path: '',
                loadChildren: () => import('@scaliolabs/baza-docs-web').then((m) => m.BazaDocsWebModule),
            },
        ],
    },
    {
        path: '**',
        redirectTo: '/not-found',
    },
];
