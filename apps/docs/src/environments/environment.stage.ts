import { Environment } from '../models/environment';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export const environment: Environment = {
    ngEnv: NgEnvironment.Stage,
    enableAngularProduction: true,
    apiEndpoint: 'https://baza-api.stage.scaliolabs.com',
    sentry: {
        dsn: 'https://e123716153df456cbdce4a09550493ce@sentry.production.scaliolabs.com/24',
        browserOptions: {
            environment: NgEnvironment.Stage,
        },
    },
};
