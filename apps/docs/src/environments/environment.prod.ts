import { Environment } from '../models/environment';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export const environment: Environment = {
    ngEnv: NgEnvironment.Prod,
    enableAngularProduction: true,
    apiEndpoint: 'http://localhost:3000',
    sentry: {
        dsn: 'https://e123716153df456cbdce4a09550493ce@sentry.production.scaliolabs.com/24',
        browserOptions: {
            environment: NgEnvironment.Prod,
        },
    },
};
