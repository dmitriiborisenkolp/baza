describe('001-display-welcome-to-the-on-index.spec.ts', () => {
  beforeEach(() => cy.visit('/'));

  it('should display "welcome to the" string', () => {
    // Function helper example, see `../support/app.po.ts` file
    cy.get('.header .title span').contains('Welcome to the');
  });
});
