describe('002-should-load-docs-and-redocs-successfully.spec.ts', () => {
    it('should visit and load /docs successfully', () => {
        cy.visit('localhost:3000/docs');
        cy.get('#swagger-ui').within(() => {
            cy.get('.title').contains('Baza API').should('be.visible');
            cy.get('.renderedMarkdown').contains('API documentation for developers').should('be.visible');
            cy.get('.version').should('be.visible');
        });
    });

    it('should visit and load /redoc successfully', () => {
        cy.visit('localhost:3000/redoc');

        cy.intercept('GET', 'swagger.json').as('getSwaggerJson');
        cy.wait('@getSwaggerJson').its('response.statusCode').should('eq', 200);

        cy.get('#redoc_container').within(() => {
            cy.get('h1').contains('Baza API').should('be.visible');
            cy.get('p').contains('API documentation for developers').should('be.visible');

            // check redocs failure
            cy.get('h1').contains('Something went wrong...').should('not.exist');
            cy.contains('Invalid reference token').should('not.exist');
            cy.contains('Stack trace').should('not.exist');
        });
    });
});
