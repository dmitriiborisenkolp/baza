describe('001-will-display-page-successfully.spec.ts', () => {
    beforeEach(() => cy.visit('/'));

    it('should display welcome message', () => {
        cy.get('.baza-docs-nav--logo', { timeout: 10000 }).should('be.visible');
    });
});
