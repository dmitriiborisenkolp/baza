import { BazaWebBundleEnvironment } from '@scaliolabs/baza-core-web';

// tslint:disable-next-line:no-empty-interface
export interface Environment extends BazaWebBundleEnvironment {}
