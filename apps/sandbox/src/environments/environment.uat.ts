import { Environment } from '../models/environment';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export const environment: Environment = {
    ngEnv: NgEnvironment.UAT,
    enableAngularProduction: true,
    apiEndpoint: 'http://localhost:3000',
    sentry: {
        dsn: 'https://a042eb7c07f04ea0b1f0664a952711de@sentry.production.scaliolabs.com/25',
        browserOptions: {
            environment: NgEnvironment.UAT,
        },
    },
};
