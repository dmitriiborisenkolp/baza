import { Environment } from '../models/environment';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export const environment: Environment = {
    ngEnv: NgEnvironment.Local,
    enableAngularProduction: false,
    apiEndpoint: 'http://localhost:3000',
};
