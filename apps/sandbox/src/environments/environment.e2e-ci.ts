import { Environment } from '../models/environment';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export const environment: Environment = {
    ngEnv: NgEnvironment.E2e,
    enableAngularProduction: false,
    apiEndpoint: 'http://api_e2e:3000',
};
