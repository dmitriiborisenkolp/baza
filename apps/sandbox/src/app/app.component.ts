import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { TranslateService } from '@ngx-translate/core';
import { StartApp } from '@scaliolabs/sandbox-web/data-access';
import * as deepmerge from 'deepmerge';

// Web UI
import { BazaWebUiEnI18n } from '@scaliolabs/baza-web-ui-components';

// Dwolla Account i18n
import { BazaDwollaWebAccountEnI18n } from '@scaliolabs/baza-dwolla-web-purchase-flow';

// Dwolla VF i18n
import { bazaDwollaWebVFEnI18n } from '@scaliolabs/baza-dwolla-web-verification-flow';

// Dwolla PF i18n
import { BazaDwollaWebPFEnI18n } from '@scaliolabs/baza-dwolla-web-purchase-flow';

// NC VF i18n
import { bazaNCWebVFEnI18n } from '@scaliolabs/baza-nc-web-verification-flow';

// NC PF i18n
import { BazaNcWebPFEnI18n } from '@scaliolabs/baza-nc-web-purchase-flow';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    constructor(private readonly translate: TranslateService, private readonly store: Store) {
        this.configureI18nTranslations();

        this.store.dispatch(new StartApp());
    }

    configureI18nTranslations() {
        const EnI18n = deepmerge.all([
            // web UI
            BazaWebUiEnI18n,

            // dwolla account
            BazaDwollaWebAccountEnI18n,

            // dwolla vf
            bazaDwollaWebVFEnI18n,

            // dwolla pf
            BazaDwollaWebPFEnI18n,

            // nc vf
            bazaNCWebVFEnI18n,

            // nc pf
            BazaNcWebPFEnI18n,
        ]);

        this.translate.setTranslation('en', EnI18n);
    }
}
