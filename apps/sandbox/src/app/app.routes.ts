import { ViewportScroller } from '@angular/common';
import { NgModule } from '@angular/core';
import { Event, NavigationCancel, Router, RouterModule, Routes, Scroll } from '@angular/router';
import { MetaGuard } from '@ngx-meta/core';
import {
    BazaRegistryNgResolve,
    JwtRequireAuthGuard,
    JwtRequireNoAuthGuard,
    JwtVerifyGuard,
    MaintenanceGuard,
} from '@scaliolabs/baza-core-ng';
import { DwollaCartResolver } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { DwollaVerificationResolver } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { CartResolver } from '@scaliolabs/baza-nc-web-purchase-flow';
import { VerificationResolver } from '@scaliolabs/baza-nc-web-verification-flow';
import { BootstrapResolver, CART_IS_EMPTY, CartGuard, DwollaCartGuard } from '@scaliolabs/sandbox-web/data-access';
import { LayoutComponent, LayoutModule } from '@scaliolabs/sandbox-web/feature-layout';
import { filter, tap } from 'rxjs/operators';

export const routes: Routes = [
    {
        path: 'service-pages',
        pathMatch: 'prefix',
        children: [
            {
                path: 'maintenance',
                pathMatch: 'full',
                loadChildren: () => import('@scaliolabs/sandbox-web/feature-maintenance').then((m) => m.MaintenanceModule),
                canActivate: [MaintenanceGuard],
            },
        ],
    },
    {
        path: '',
        resolve: {
            __baza_core_registry: BazaRegistryNgResolve,
        },
        children: [
            {
                path: 'auth',
                loadChildren: () => import('@scaliolabs/sandbox-web/feature-auth').then((m) => m.AuthModule),
                canActivate: [JwtRequireNoAuthGuard],
            },
            {
                path: '',
                component: LayoutComponent,
                canActivateChild: [MetaGuard],
                children: [
                    {
                        path: '',
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-home').then((m) => m.HomeModule),
                    },
                    {
                        path: 'mail',
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-mail').then((m) => m.MailModule),
                    },
                    {
                        path: 'items',
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-item').then((m) => m.ItemModule),
                    },
                    {
                        path: 'portfolio',
                        canActivate: [JwtRequireAuthGuard, JwtVerifyGuard],
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-portfolio').then((m) => m.PortfolioModule),
                    },
                    {
                        path: 'verification',
                        canActivate: [JwtRequireAuthGuard, JwtVerifyGuard],
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-verification').then((m) => m.VerificationModule),
                        resolve: {
                            bootstrap: BootstrapResolver,
                            purchase: CartResolver,
                        },
                    },
                    {
                        path: 'verification-dwolla',
                        canActivate: [JwtRequireAuthGuard, JwtVerifyGuard],
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-verification-dwolla').then((m) => m.VerificationModule),
                        resolve: {
                            bootstrap: BootstrapResolver,
                            purchase: DwollaCartResolver,
                        },
                    },
                    {
                        path: 'buy-shares',
                        canActivate: [JwtVerifyGuard, JwtRequireAuthGuard, CartGuard],
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-purchase').then((m) => m.PurchaseModule),
                        resolve: {
                            bootstrap: BootstrapResolver,
                            purchase: CartResolver,
                            verification: VerificationResolver,
                        },
                    },
                    {
                        path: 'buy-shares-dwolla',
                        canActivate: [JwtVerifyGuard, JwtRequireAuthGuard, DwollaCartGuard],
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-purchase-dwolla').then((m) => m.DwollaPurchaseModule),
                        resolve: {
                            bootstrap: BootstrapResolver,
                            purchase: DwollaCartResolver,
                            verification: DwollaVerificationResolver,
                        },
                    },
                    {
                        path: 'account',
                        canActivate: [JwtVerifyGuard, JwtRequireAuthGuard],
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-account').then((m) => m.AccountModule),
                    },
                    {
                        path: 'favorites',
                        canActivate: [JwtVerifyGuard, JwtRequireAuthGuard],
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-favorite').then((m) => m.FavoriteModule),
                    },
                    {
                        path: '**',
                        loadChildren: () => import('@scaliolabs/sandbox-web/feature-not-found').then((m) => m.NotFoundModule),
                    },
                ],
            },
            {
                outlet: 'modal',
                path: 'auth',
                loadChildren: () => import('@scaliolabs/sandbox-web/feature-auth').then((m) => m.AuthModule),
                canActivate: [JwtRequireNoAuthGuard],
            },
            {
                outlet: 'modal',
                path: 'contact-us',
                loadChildren: () => import('@scaliolabs/sandbox-web/feature-contact-us').then((m) => m.ContactUsModule),
            },
        ],
    },
];

@NgModule({
    imports: [
        LayoutModule,
        RouterModule.forRoot(routes, {
            // Notice custom routing logic down below
            scrollPositionRestoration: 'disabled',
            anchorScrolling: 'disabled',
            onSameUrlNavigation: 'reload',
            initialNavigation: 'enabled',
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {
    constructor(private router: Router, private viewportScroller: ViewportScroller) {
        this.router.events
            .pipe(
                filter((e: Event): e is Scroll => e instanceof Scroll),
                tap((current) => {
                    const state = this.router.getCurrentNavigation()?.extras?.state;
                    if (state?.skipScroll) {
                        return;
                    }
                    if (current.position) {
                        // Backward navigation
                        this.viewportScroller.scrollToPosition(current.position);
                    } else if (current.anchor) {
                        // Anchor navigation
                        this.viewportScroller.scrollToAnchor(current.anchor);
                    } else {
                        // Page navigation
                        this.viewportScroller.scrollToPosition([0, 0]);
                    }
                }),
            )
            .subscribe();

        this.router.events.pipe(filter((event) => event instanceof NavigationCancel)).subscribe((event: any) => {
            switch (event.reason) {
                case CART_IS_EMPTY:
                    this.router.navigate(['/items']);
                    break;
            }
        });
    }
}
