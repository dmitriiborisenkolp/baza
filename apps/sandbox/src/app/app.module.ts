import { NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
import { NZ_CONFIG, NzConfig } from 'ng-zorro-antd/core/config';
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { IconSpriteModule } from 'ng-svg-icon-sprite';
import { MetaLoader, MetaModule, MetaStaticLoader, PageTitlePositioning } from '@ngx-meta/core';
import { NgxPlaidLinkModule } from 'ngx-plaid-link';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule, LoaderModule, StorageModule, UtilModule } from '@scaliolabs/baza-web-utils';
import { AuthStateModule, BootstrapStateModule, CartGuard, DwollaCartGuard } from '@scaliolabs/sandbox-web/data-access';
import { BAZA_WEB_BUNDLE_GUARD_CONFIGS, bazaWebBundleConfigBuilder, BazaWebBundleModule } from '@scaliolabs/baza-core-web';
import { DwollaPurchaseStateModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { getBazaProjectCodeName } from '@scaliolabs/baza-core-shared';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { PurchaseStateModule } from '@scaliolabs/baza-nc-web-purchase-flow';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { BUILT_ON_API_VERSION } from './app.version';
import { environment } from '../environments/environment';
import { TransferHttpResponseInterceptor } from './interceptor';

const config = bazaWebBundleConfigBuilder()
    .withVersion(BUILT_ON_API_VERSION)
    .withNgEnvironment(environment.ngEnv)
    .withApiEndpoint(environment.apiEndpoint)
    .withoutI18nSupport()
    .withJwtStorages({
        jwtStorages: {
            localStorage: {
                enabled: true,
                jwtKey: `${getBazaProjectCodeName()}WebJwt`,
                jwtPayloadKey: `${getBazaProjectCodeName()}JwtWebPayload`,
            },
            sessionStorage: {
                enabled: false,
            },
            documentCookieStorage: {
                enabled: false,
            },
        },
    }).config;

BAZA_WEB_BUNDLE_GUARD_CONFIGS.requireNoAuthGuardConfig.redirect = () => ({
    commands: ['/items'],
});

const ngZorroConfig: NzConfig = {
    notification: {
        nzDuration: 5000,
        nzMaxStack: 1,
    },
};

// set default meta tags
export function metaFactory(): MetaLoader {
    return new MetaStaticLoader({
        applicationName: 'Scalio - Investment Platform',
        pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
        pageTitleSeparator: ' | ',
        defaults: {
            title: 'Scalio - Investment Platform',
            description: '',
            'og:type': 'website',
            'og:locale': 'en_US',
        },
    });
}

@NgModule({
    declarations: [AppComponent],
    imports: [
        AlertModule.forRoot({ hostSelector: '#alertPlaceholder' }),
        AppRoutingModule,
        AuthStateModule,
        BazaWebBundleModule.forRoot(config),
        BootstrapStateModule,
        BrowserAnimationsModule,
        BrowserModule.withServerTransition({ appId: 'sandbox' }),
        BrowserTransferStateModule,
        DwollaPurchaseStateModule,
        IconSpriteModule.forRoot({ path: 'assets/sprites/icons.svg' }),
        LoaderModule,
        HttpClientModule,
        MetaModule.forRoot({
            provide: MetaLoader,
            useFactory: metaFactory,
        }),
        NgxPlaidLinkModule,
        NgxsModule.forRoot([], {
            developmentMode: environment.ngEnv === NgEnvironment.Local,
        }),
        PurchaseStateModule,
        TranslateModule.forRoot({
            defaultLanguage: 'en',
        }),
        RouterModule,
        StorageModule,
        UtilModule,
    ],
    providers: [
        {
            provide: NZ_I18N,
            useValue: en_US,
        },
        {
            provide: NZ_CONFIG,
            useValue: ngZorroConfig,
        },
        CartGuard,
        DwollaCartGuard,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TransferHttpResponseInterceptor,
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
