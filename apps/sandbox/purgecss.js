const exec = require('child_process').exec;
const fs = require('fs');
const path = require('path');
const pathPrefix = 'dist/sandbox/browser';
const files = getAllFiles(`./${pathPrefix}/`, '.css');
let cmd = [];

if (files?.length > 0) {
    for (let file of files) {
        cmd.push(`purgecss -css ${file} --content ${pathPrefix}/index.html ${pathPrefix}/*.js -o ${file}`);
    }
}

cmd = cmd.join(' & ');
exec(cmd, function (error, stdout, stderr) {});

function getAllFiles(dir, extension, arrayOfFiles = []) {
    const files = fs.readdirSync(dir);
    files.forEach(function (file) {
        if (fs.statSync(dir + '/' + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dir + '/' + file, extension, arrayOfFiles);
        } else if (file.endsWith(extension)) {
            arrayOfFiles.push(path.join(dir, '/', file));
        }
    });

    return arrayOfFiles;
}
