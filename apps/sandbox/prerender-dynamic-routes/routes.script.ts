// eslint-disable-next-line @typescript-eslint/no-var-requires
const dotenv = require('dotenv');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const axios = require('axios');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const endOfLine = require('os').EOL;
const routesFile = `${process.cwd()}/apps/sandbox/prerender-dynamic-routes/routes.txt`;

const envFileConfigPath = path.join(process.cwd(), 'envs', `${process.env.NODE_ENV || 'production'}.env`);

dotenv.config({ path: envFileConfigPath });

// TODO: use variable to specify an API
const itemsApi = `${process.env.BAZA_APP_API_URL}/baza-nc-integration/listings/list`;

axios
    .get(itemsApi, { params: { size: 100 } })
    .then(({ data }) => {
        const routes = [];

        data.items.forEach(({ id }) => {
            routes.push('items/' + id);
        });

        fs.writeFileSync(routesFile, routes.join(endOfLine));
    })
    .catch((e) => {
        // eslint-disable-next-line no-console
        console.log(e);
    });
