import { BazaCmsBundleEnvironment } from '@scaliolabs/baza-core-cms';

export interface Environment extends BazaCmsBundleEnvironment {
}
