import { NgModule } from '@angular/core';
import { environment } from '../environments/environment';
import { RouterModule } from '@angular/router';
import {
    BazaCmsAppComponent,
    bazaCmsBundleConfigBuilder,
    BazaCmsBundleModule,
    bazaCmsBundleRoutesFactory,
} from '@scaliolabs/baza-core-cms';
import { bazaContentTypesCmsMenu, BazaContentTypesCmsSharedModule } from '@scaliolabs/baza-content-types-cms';
import { bazaNcBundleCmsConfig, bazaNcBundlesCmsMenu, BazaNcBundlesCmsModule, bazaNcBundlesCmsRoutes } from '@scaliolabs/baza-nc-cms';
import {
    bazaNcIntegrationBundleCmsConfig,
    bazaNcIntegrationCmsMenu,
    BazaNcIntegrationCmsModule,
    bazaNcIntegrationCmsRoutes,
} from '@scaliolabs/baza-nc-integration-cms';
import { bazaContentTypesConfigure } from '@scaliolabs/baza-content-types-shared';
import { bazaDwollaCmsMenu, bazaDwollaCmsRoutes, BazaDwollaPaymentCmsModule } from '@scaliolabs/baza-dwolla-cms';

if (environment.sentry) {
    bazaCmsBundleConfigBuilder().withSentry(environment.sentry.dsn, environment.sentry.browserOptions);
}

const cmsConfig = [
    // Set up NC features
    () =>
        bazaNcBundleCmsConfig({
            withDwollaFeatures: true,
            withImportOfferingFeature: true,
            withForgetOfferingFeature: true,
            withTaxDocumentsFeature: true,
            withTransactionFeeFeature: true,
            withEliteInvestorsFeature: true,
            withNcDividendsFeature: true,
            withDwollaDividendsFeature: true,
            withReportFeature: true,
        }),

    // Set up NC Integration features
    () =>
        bazaNcIntegrationBundleCmsConfig({
            withSchemaCMS: true,
            withEditableSchemaCMS: true,
            withStatementsFeature: true,
            withListingMetadataFieldFeature: false,
            withTimelineFeature: true,
            withTestimonialsFeature: true,
        }),

    // Common Config
    () =>
        bazaCmsBundleConfigBuilder()
            .withEnvironment(environment)
            .withoutI18nSupport()
            .withSidePanelGroups(() => [
                ...bazaContentTypesCmsMenu(),
                ...bazaNcBundlesCmsMenu(),
                ...bazaNcIntegrationCmsMenu(),
                ...bazaDwollaCmsMenu,
            ])
            .withDeviceTokenFeature()
            .withCustomerIoLinkCms({
                withCustomerIoLinkCmsUnlinkFeature: true,
            }),
    // Set up Timeline feature
    () =>
        bazaContentTypesConfigure((defaults) => ({
            configs: {
                ...defaults.configs,
                timeline: {
                    ...defaults.configs.timeline,
                    withTagsSupport: false,
                    withCategoriesSupport: false,
                    withTextDescription: false,
                    withImageSupport: false,
                    withSEO: false,
                },
            },
        })),
];

cmsConfig.forEach((config) => config());

@NgModule({
    imports: [
        RouterModule.forRoot(
            bazaCmsBundleRoutesFactory([
                {
                    path: 'content-types',
                    loadChildren: () => import('@scaliolabs/baza-content-types-cms').then((m) => m.BazaContentTypesCmsBundleModule),
                },
                ...bazaNcBundlesCmsRoutes,
                ...bazaNcIntegrationCmsRoutes,
                ...bazaDwollaCmsRoutes,
            ]),
            {
                initialNavigation: 'enabled',
                enableTracing: false,
                scrollPositionRestoration: 'enabled',
            },
        ),
        BazaCmsBundleModule.forRoot(bazaCmsBundleConfigBuilder().config),
        BazaContentTypesCmsSharedModule.forRoot({}),
        BazaNcBundlesCmsModule,
        BazaNcIntegrationCmsModule,
        BazaDwollaPaymentCmsModule,
    ],
    bootstrap: [BazaCmsAppComponent],
})
export class AppModule {}
