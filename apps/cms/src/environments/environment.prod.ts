import { Environment } from '../models/environment';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export const environment: Environment = {
    ngEnv: NgEnvironment.Prod,
    enableAngularProduction: true,
    apiEndpoint: 'https://localhost:3000',
    sentry: {
        dsn: 'https://172f7d6a40ff4858b0301f590e681fa5@sentry.production.scaliolabs.com/26',
        browserOptions: {
            environment: NgEnvironment.Prod,
        },
    },
};
