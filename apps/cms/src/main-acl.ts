import { configureBazaBundleAcl } from '@scaliolabs/baza-core-shared';
import { BazaContentTypeAcl, BazaContentTypeAclGroup } from '@scaliolabs/baza-content-types-shared';
import { BazaNorthCapitalAcl, BazaNorthCapitalAclGroup } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationAcl, BazaNcIntegrationAclGroups } from '@scaliolabs/baza-nc-integration-shared';
import { BazaDwollaAcl, BazaDwollaAclGroups } from '@scaliolabs/baza-dwolla-shared';

configureBazaBundleAcl({
    groups: [
        {
            group: BazaContentTypeAclGroup.BazaContentTypes,
            nodes: Object.values(BazaContentTypeAcl),
        },
        {
            group: BazaNorthCapitalAclGroup.BazaNorthCapital,
            nodes: Object.values(BazaNorthCapitalAcl),
        },
        {
            group: BazaNcIntegrationAclGroups.BazaNcIntegration,
            nodes: Object.values(BazaNcIntegrationAcl),
        },
        {
            group: BazaDwollaAclGroups.BazaDwolla,
            nodes: Object.values(BazaDwollaAcl),
        },
    ],
});
