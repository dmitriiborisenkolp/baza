import { cliFactory } from '@scaliolabs/baza-cli';
import { RedisService } from '@liaoliaots/nestjs-redis';

cliFactory({
    name: 'typeorm:flush',
    startMessage: 'Flush redis cache...',
    finishedMessage: 'Redis is clean now',
}, async (app) => {
    const redisService = app.get(RedisService);

    await redisService.getClient().flushall();
});
