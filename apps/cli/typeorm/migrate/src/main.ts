import { Connection } from 'typeorm';
import { cliFactory } from '@scaliolabs/baza-cli';
import { BazaMigrationsService } from '@scaliolabs/baza-core-api';

cliFactory({
    name: 'typeorm:migrate',
    startMessage: 'Run TypeORM migrations...',
    finishedMessage: 'Migrations completed!',
}, async (app) => {
    const connection = app.get(Connection);
    const migrations = app.get(BazaMigrationsService);

    await connection.synchronize();
    await connection.runMigrations();

    await migrations.up();
});
