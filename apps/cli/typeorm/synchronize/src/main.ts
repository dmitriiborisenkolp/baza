import { Connection } from 'typeorm';
import { cliFactory } from '@scaliolabs/baza-cli';

cliFactory({
    name: 'typeorm:flush',
    startMessage: 'Run TypeORM schema synchronization...',
    finishedMessage: 'Schema synchronization completed!',
}, async (app) => {
    const connection = app.get(Connection);

    await connection.synchronize();
});
