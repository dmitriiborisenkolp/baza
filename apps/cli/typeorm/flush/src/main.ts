import '@angular/compiler';

import { cliFactory } from '@scaliolabs/baza-cli';
import { EnvService } from '@scaliolabs/baza-core-api';
import { Connection } from 'typeorm';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';

const ALLOWED_ENVIRONMENTS: Array<BazaEnvironments> = [BazaEnvironments.Local, BazaEnvironments.Test];

cliFactory(
    {
        name: 'typeorm:flush',
        startMessage: 'Destroy database',
        finishedMessage: 'Database is completely clean now',
    },
    async (app, logger, exitWithError) => {
        const env = app.get(EnvService);
        const connection = app.get(Connection);

        if (!ALLOWED_ENVIRONMENTS.includes(env.current)) {
            exitWithError(`The command cannot be executed for "${env.current}" environment`);
        }

        await connection.synchronize(true);
    },
);
