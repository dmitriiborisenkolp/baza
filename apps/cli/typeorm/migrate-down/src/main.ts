import { Connection } from 'typeorm';
import { cliFactory } from '@scaliolabs/baza-cli';
import { BazaMigrationsService } from '@scaliolabs/baza-core-api';

cliFactory({
    name: 'typeorm:down',
    startMessage: 'Rollback TypeORM migrations...',
    finishedMessage: 'Rollback complete',
}, async (app) => {
    const connection = app.get(Connection);
    const migrations = app.get(BazaMigrationsService);

    await connection.synchronize();
    await migrations.down();
});
