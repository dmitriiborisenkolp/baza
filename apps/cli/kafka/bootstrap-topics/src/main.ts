import '@angular/compiler';

import { cliFactory } from '@scaliolabs/baza-cli';
import { KafkaAdminService } from '@scaliolabs/baza-core-api';

cliFactory(
    {
        name: 'cli:kafka:bootstrap-topics',
        startMessage: 'Bootstrap Kafka topics...', // Optional
        finishedMessage: 'Kafka topics bootstraped!', // Optional
    },
    async (app) => {
        const kafkaAdmin = app.get(KafkaAdminService);

        await kafkaAdmin.bootstrapTopics();
    },
);
