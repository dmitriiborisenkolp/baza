import 'cypress-network-idle';

describe('001-display-SANDBOX-in-title.spec.ts', () => {
    beforeEach(() => cy.visit('/'));

    it('should display "SANDBOX" in title', () => {
        cy.waitForNetworkIdle(2000);
        cy.get('nz-header').contains('SANDBOX');
    });
});
