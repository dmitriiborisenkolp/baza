import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MetaLoader, MetaModule, MetaStaticLoader, PageTitlePositioning } from '@ngx-meta/core';
import { NgxsModule } from '@ngxs/store';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { bazaWebBundleConfigBuilder, BazaWebBundleModule, BAZA_WEB_BUNDLE_GUARD_CONFIGS } from '@scaliolabs/baza-core-web';
import { AlertModule, LoaderModule, StorageModule, UtilModule } from '@scaliolabs/baza-web-utils';
import { NzConfig, NZ_CONFIG } from 'ng-zorro-antd/core/config';
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { BUILT_ON_API_VERSION } from './app.version';
import { getBazaProjectCodeName } from '@scaliolabs/baza-core-shared';

const config = bazaWebBundleConfigBuilder()
    .withVersion(BUILT_ON_API_VERSION)
    .withNgEnvironment(environment.ngEnv)
    .withApiEndpoint(environment.apiEndpoint)
    .withoutI18nSupport()
    .withJwtStorages({
        jwtStorages: {
            localStorage: {
                enabled: true,
                jwtKey: `${getBazaProjectCodeName()}WebJwt`,
                jwtPayloadKey: `${getBazaProjectCodeName()}JwtWebPayload`,
            },
            sessionStorage: {
                enabled: false,
            },
            documentCookieStorage: {
                enabled: false,
            },
        },
    }).config;

BAZA_WEB_BUNDLE_GUARD_CONFIGS.requireNoAuthGuardConfig.redirect = () => ({
    commands: ['/items'],
});

const ngZorroConfig: NzConfig = {
    notification: {
        nzDuration: 5000,
        nzMaxStack: 1,
    },
};

// set default meta tags
export function metaFactory(): MetaLoader {
    return new MetaStaticLoader({
        applicationName: 'Scalio - Investment Platform',
        pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
        pageTitleSeparator: ' | ',
        defaults: {
            title: 'Scalio - Investment Platform',
            description: '',
            'og:type': 'website',
            'og:locale': 'en_US',
        },
    });
}

@NgModule({
    declarations: [AppComponent],
    imports: [
        AlertModule.forRoot({ hostSelector: '#alertPlaceholder' }),
        AppRoutingModule,
        BazaWebBundleModule.forRoot(config),
        BrowserModule,
        BrowserAnimationsModule,
        LoaderModule,
        HttpClientModule,
        MetaModule.forRoot({
            provide: MetaLoader,
            useFactory: metaFactory,
        }),
        NgxsModule.forRoot([], {
            developmentMode: environment.ngEnv === NgEnvironment.Local,
        }),
        UtilModule,
        StorageModule,
    ],
    providers: [
        {
            provide: NZ_I18N,
            useValue: en_US,
        },
        {
            provide: NZ_CONFIG,
            useValue: ngZorroConfig,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
