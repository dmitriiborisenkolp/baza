import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MetaGuard } from '@ngx-meta/core';

export const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                canActivateChild: [MetaGuard],
                loadChildren: () => import('@scaliolabs/sandbox-web-marketing/feature-landing').then((m) => m.LandingModule),
            },
            {
                path: '**',
                redirectTo: '',
            },
        ],
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            enableTracing: false,
            onSameUrlNavigation: 'reload',
            initialNavigation: 'enabled',
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
