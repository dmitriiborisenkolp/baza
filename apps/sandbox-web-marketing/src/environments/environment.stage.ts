import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { Environment } from '@scaliolabs/sandbox-web/data-access';

export const environment: Environment = {
    ngEnv: NgEnvironment.Stage,
    enableAngularProduction: true,
    apiEndpoint: 'https://baza-api.stage.scaliolabs.com',
};
