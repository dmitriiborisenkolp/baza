import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { Environment } from '@scaliolabs/sandbox-web/data-access';

export const environment: Environment = {
    ngEnv: NgEnvironment.Test,
    enableAngularProduction: true,
    apiEndpoint: 'https://baza-api.test.scaliolabs.com',
};
