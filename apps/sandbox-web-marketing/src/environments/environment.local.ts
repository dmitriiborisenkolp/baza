import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { Environment } from '@scaliolabs/sandbox-web/data-access';

export const environment: Environment = {
    ngEnv: NgEnvironment.Local,
    enableAngularProduction: false,
    apiEndpoint: 'http://localhost:3000',
};
