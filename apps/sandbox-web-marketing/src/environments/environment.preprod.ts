import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { Environment } from '@scaliolabs/sandbox-web/data-access';

export const environment: Environment = {
    ngEnv: NgEnvironment.Preprod,
    enableAngularProduction: true,
    apiEndpoint: 'http://localhost:3000',
};
