import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { Environment } from '@scaliolabs/sandbox-web/data-access';

export const environment: Environment = {
    ngEnv: NgEnvironment.E2e,
    enableAngularProduction: false,
    apiEndpoint: 'http://api_e2e:3000',
};
