const nxPreset = require('@nrwl/jest/preset');

module.exports = {
    ...nxPreset,
    testMatch: ['**/+(*.)+(spec).+(ts|js)?(x)'],
    testTimeout: 30000, // Specifically for North Capital tests
    globals: {
        'ts-jest': {
            isolatedModules: true,
        }
    }
};
