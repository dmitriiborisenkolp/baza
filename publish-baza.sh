#!/usr/bin/env bash

# baza-core
cd dist/libs/baza-core-api && npm publish && cd -
cd dist/libs/baza-core-cms && npm publish && cd -
cd dist/libs/baza-core-cms-data-access && npm publish && cd -
cd dist/libs/baza-core-data-access && npm publish && cd -
cd dist/libs/baza-core-node-access && npm publish && cd -
cd dist/libs/baza-core-ng && npm publish && cd -
cd dist/libs/baza-core-shared && npm publish && cd -
cd dist/libs/baza-core-web && npm publish && cd -
cd dist/libs/baza-core-devops && npm publish && cd -

# baza-content-types
cd dist/libs/baza-content-types-api && npm publish && cd -
cd dist/libs/baza-content-types-cms && npm publish && cd -
cd dist/libs/baza-content-types-data-access && npm publish && cd -
cd dist/libs/baza-content-types-node-access && npm publish && cd -
cd dist/libs/baza-content-types-shared && npm publish && cd -

# baza-nc
cd dist/libs/baza-nc-api && npm publish && cd -
cd dist/libs/baza-nc-cms && npm publish && cd -
cd dist/libs/baza-nc-cms-data-access && npm publish && cd -
cd dist/libs/baza-nc-data-access && npm publish && cd -
cd dist/libs/baza-nc-node-access && npm publish && cd -
cd dist/libs/baza-nc-shared && npm publish && cd -

# baza-nc-integration
cd dist/libs/baza-nc-integration-shared && npm publish && cd -
cd dist/libs/baza-nc-integration-cms-data-access && npm publish && cd -
cd dist/libs/baza-nc-integration-data-access && npm publish && cd -
cd dist/libs/baza-nc-integration-node-access && npm publish && cd -
cd dist/libs/baza-nc-integration-api && npm publish && cd -
cd dist/libs/baza-nc-integration-cms && npm publish && cd -

# baza-nc-web
cd dist/libs/baza-nc-web-purchase-flow && npm publish && cd -
cd dist/libs/baza-nc-web-verification-flow && npm publish && cd -

# baza-dwolla
cd dist/libs/baza-dwolla-api && npm publish && cd -
cd dist/libs/baza-dwolla-cms && npm publish && cd -
cd dist/libs/baza-dwolla-shared && npm publish && cd -
cd dist/libs/baza-dwolla-cms-data-access && npm publish && cd -
cd dist/libs/baza-dwolla-data-access && npm publish && cd -
cd dist/libs/baza-dwolla-node-access && npm publish && cd -

# baza-dwolla
cd dist/libs/baza-plaid-api && npm publish && cd -
cd dist/libs/baza-plaid-shared && npm publish && cd -

# baza-dwolla-web
cd dist/libs/baza-dwolla-web-purchase-flow && npm publish && cd -
cd dist/libs/baza-dwolla-web-verification-flow && npm publish && cd -

# baza-web
cd dist/libs/baza-web-utils && npm publish && cd -
cd dist/libs/baza-web-ui-components && npm publish && cd -