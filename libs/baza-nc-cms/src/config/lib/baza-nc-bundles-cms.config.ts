import { BazaNcOfferingListItemDto } from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

export class BazaNcBundlesCmsConfig {
    withDwollaFeatures: boolean;
    withImportOfferingFeature: boolean;
    withForgetOfferingFeature: boolean;
    withTaxDocumentsFeature: boolean;
    withTransactionFeeFeature: boolean;
    withEliteInvestorsFeature: boolean;
    withNcDividendsFeature: boolean;
    withDwollaDividendsFeature: boolean;
    withReportFeature: boolean;
    filterOfferingsForDividends?: (offerings: Array<BazaNcOfferingListItemDto>) => Observable<Array<BazaNcOfferingListItemDto>>;
    withNotifySubscribedUsersFeature: boolean;
}

const BAZA_NC_BUNDLE_CMS_CONFIG: BazaNcBundlesCmsConfig = {
    withDwollaFeatures: false,
    withForgetOfferingFeature: false,
    withImportOfferingFeature: false,
    withTaxDocumentsFeature: false,
    withTransactionFeeFeature: false,
    withEliteInvestorsFeature: false,
    withNcDividendsFeature: false,
    withDwollaDividendsFeature: false,
    withReportFeature: false,
    withNotifySubscribedUsersFeature: true,
};

export function bazaNcBundleCmsConfig(partialConfig?: Partial<BazaNcBundlesCmsConfig>): BazaNcBundlesCmsConfig {
    if (partialConfig) {
        Object.assign(BAZA_NC_BUNDLE_CMS_CONFIG, partialConfig);
    }

    return {
        ...BAZA_NC_BUNDLE_CMS_CONFIG,
        withDwollaDividendsFeature: BAZA_NC_BUNDLE_CMS_CONFIG.withDwollaFeatures && BAZA_NC_BUNDLE_CMS_CONFIG.withDwollaDividendsFeature,
    };
}
