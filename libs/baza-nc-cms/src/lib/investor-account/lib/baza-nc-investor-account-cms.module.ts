import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BazaCmsLayoutModule, BazaCrudCmsModule, BazaFormBuilderModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsDataAccessModule } from '@scaliolabs/baza-nc-cms-data-access';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NcInvestorAccountRetryCmsComponent } from './nc-investor-account-retry-cms/nc-investor-account-retry-cms.component';
import { NcInvestorAccountRetryCmsService } from './nc-investor-account-retry-cms/nc-investor-account-retry-cms.service';
import { NcInvestorAccountRetryDocumentCmsComponent } from './nc-investor-account-retry-document-cms/nc-investor-account-retry-document-cms.component';
import { NcInvestorAccountRetryDocumentCmsService } from './nc-investor-account-retry-document-cms/nc-investor-account-retry-document-cms.service';
import { NcInvestorAccountsCmsComponent } from './nc-investor-accounts-cms/nc-investor-accounts-cms.component';

const NZ_MODULES = [NzModalModule, NzIconModule, NzAlertModule, NzUploadModule, NzButtonModule];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        TranslateModule,
        BazaFormBuilderModule,
        BazaCrudCmsModule,
        BazaCmsLayoutModule,
        BazaNcCmsDataAccessModule,
        ...NZ_MODULES,
    ],
    declarations: [NcInvestorAccountsCmsComponent, NcInvestorAccountRetryCmsComponent, NcInvestorAccountRetryDocumentCmsComponent],
    providers: [NcInvestorAccountRetryCmsService, NcInvestorAccountRetryDocumentCmsService],
})
export class BazaNcInvestorAccountCmsModule {}
