import { ChangeDetectionStrategy, Component, TemplateRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { DwollaDocumentType } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcDwollaCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { NzModalComponent } from 'ng-zorro-antd/modal';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { catchError, finalize, Observable, Observer, throwError } from 'rxjs';

@Component({
    templateUrl: './nc-investor-account-retry-document-cms.component.html',
    styleUrls: ['./nc-investor-account-retry-document-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcInvestorAccountRetryDocumentCmsComponent {
    private readonly ALLOWED_MAX_FILE_SIZE_IN_MB = 10;

    private readonly ALLOWED_MAX_FILE_SIZE_IN_BYTES = this.ALLOWED_MAX_FILE_SIZE_IN_MB * 1024 * 1024; // 10MB

    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<NzModalComponent>;

    public nzModalRef: NzModalRef<NzModalComponent>;

    public userId!: number;

    public selectedFile: File = null;

    public imgPreviewUrl = '';

    public errorMessage = null;

    constructor(
        private readonly dwollaDataAccess: BazaNcDwollaCmsDataAccess,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly translateService: TranslateService,
        private readonly loading: BazaLoadingService,
    ) {}

    get rules$(): Observable<string[]> {
        return this.translateService.get(this.i18n('rules'));
    }

    i18n(key: string): string {
        return `baza-nc.investorAccount.components.ncInvestorAccountRetryDocumentCms.${key}`;
    }

    beforeUpload = (file: NzUploadFile): Observable<boolean> => {
        return new Observable((observer: Observer<boolean>) => {
            // File type validation
            const isFileTypeValid = ['image/jpeg', 'image/png'].includes(file.type);

            if (!isFileTypeValid) {
                this.errorMessage = this.translateService.instant(this.i18n('validationErrors.fileType'), {
                    allowedFileTypes: 'either a .jpg, .jpeg, or .png ',
                });

                observer.complete();
                return;
            }

            // File size validation
            const isFileMaxSizeValid = file.size < this.ALLOWED_MAX_FILE_SIZE_IN_BYTES;

            if (!isFileMaxSizeValid) {
                this.errorMessage = this.translateService.instant(this.i18n('validationErrors.fileMaxSize'), {
                    allowedFileMaxSize: this.ALLOWED_MAX_FILE_SIZE_IN_MB,
                    allowedFileMaxSizeUnit: 'MB',
                });

                observer.complete();
                return;
            }

            this.errorMessage = null;

            this.selectedFile = file as unknown as File;

            this.readFileUrl(this.selectedFile).then((url) => (this.imgPreviewUrl = url));

            observer.next(isFileTypeValid && isFileMaxSizeValid);
            observer.complete();
        });
    };

    clearFile(): void {
        this.errorMessage = null;
        this.selectedFile = null;
        this.imgPreviewUrl = '';
    }

    close(): void {
        this.nzModalRef.close();
    }

    upload(): void {
        const loading = this.loading.addLoading();

        this.dwollaDataAccess
            .createDocumentForCustomer(this.selectedFile, {
                userId: this.userId,
                documentType: DwollaDocumentType.IdCard,
            })
            .pipe(
                catchError((error) => {
                    this.errorMessage = error.message || JSON.stringify(error);

                    return throwError(() => error);
                }),
                finalize(() => loading.complete()),
            )
            .subscribe(() => {
                this.ngMessages.success({
                    message: this.i18n('notifications.success'),
                    translate: true,
                });

                this.close();
            });
    }

    private readFileUrl(file: File): Promise<string> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.readAsDataURL(file);

            reader.onload = (event) => resolve(event.target.result as string);

            reader.onerror = (error) => reject(error);
        });
    }
}
