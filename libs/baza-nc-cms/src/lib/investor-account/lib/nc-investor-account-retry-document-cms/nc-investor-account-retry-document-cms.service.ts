import { ComponentFactoryResolver, Injectable, Injector, ViewContainerRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { take } from 'rxjs/operators';
import { NcInvestorAccountRetryDocumentCmsComponent } from './nc-investor-account-retry-document-cms.component';

@Injectable()
export class NcInvestorAccountRetryDocumentCmsService {
    constructor(private readonly nzModal: NzModalService, private readonly componentFactoryResolver: ComponentFactoryResolver) {}

    open(props: { userId: number; parentVcr: ViewContainerRef }): Promise<string | undefined> {
        return new Promise((resolve) => {
            const factory = this.componentFactoryResolver.resolveComponentFactory(NcInvestorAccountRetryDocumentCmsComponent);

            const injector = Injector.create({
                providers: [],
                parent: props.parentVcr.injector,
            });

            const component = factory.create(injector);

            props.parentVcr.insert(component.hostView);

            setTimeout(() => {
                component.instance.userId = props.userId;

                component.instance.nzModalRef = this.nzModal.create({
                    nzContent: component.instance.nzModal,
                    nzFooter: null,
                    nzWidth: 600,
                    nzAutofocus: null,
                    nzClosable: false,
                    nzKeyboard: false,
                });

                component.instance.nzModalRef.afterClose.pipe(take(1)).subscribe((result) => resolve(result));
            });
        });
    }
}
