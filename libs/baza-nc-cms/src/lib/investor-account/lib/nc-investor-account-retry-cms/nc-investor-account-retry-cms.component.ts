import { ChangeDetectionStrategy, Component, TemplateRef, ViewChild } from '@angular/core';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { BazaNcDwollaCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { NzModalComponent } from 'ng-zorro-antd/modal';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { catchError, finalize, throwError } from 'rxjs';

@Component({
    templateUrl: './nc-investor-account-retry-cms.component.html',
    styleUrls: ['./nc-investor-account-retry-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcInvestorAccountRetryCmsComponent {
    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<NzModalComponent>;

    public nzModalRef: NzModalRef<NzModalComponent>;

    public userId!: number;

    constructor(
        private readonly dwollaDataAccess: BazaNcDwollaCmsDataAccess,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly loading: BazaLoadingService,
    ) {}

    i18n(key: string): string {
        return `baza-nc.investorAccount.components.ncInvestorAccountRetryCms.${key}`;
    }

    close(): void {
        this.nzModalRef.close();
    }

    retry(): void {
        const loading = this.loading.addLoading();

        this.dwollaDataAccess
            .retryVerificationForPersonalCustomer({
                userId: this.userId,
            })
            .pipe(
                catchError((error) => {
                    this.ngMessages.error({
                        message: error.message || JSON.stringify(error),
                        translate: false,
                    });

                    this.close();

                    return throwError(() => error);
                }),
                finalize(() => loading.complete()),
            )
            .subscribe(() => {
                this.ngMessages.success({
                    message: this.i18n('notifications.success'),
                    translate: true,
                });

                this.close();
            });
    }
}
