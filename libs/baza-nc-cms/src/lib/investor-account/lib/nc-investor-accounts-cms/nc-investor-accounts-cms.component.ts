import { ChangeDetectionStrategy, Component, ComponentRef, OnDestroy, ViewContainerRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
    BazaAccountSearchModalService,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFieldAccountComponentOptions,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormBuilderTextComponent,
    BazaFormControlFieldSelect,
    BazaFormLayoutModalService,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { AccountDto } from '@scaliolabs/baza-core-shared';
import { DwollaCustomerStatus } from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcAccountVerificationCmsDataAccess,
    BazaNcDwollaCmsDataAccess,
    BazaNcInvestorAccountCmsDataAccess,
    BazaNcSyncCmsDataAccess,
} from '@scaliolabs/baza-nc-cms-data-access';
import {
    BazaNcDwollaTouchResult,
    BazaNcInvestorAccountDto,
    BazaNcInvestorAccountUpdatePersonalInformationRequest,
    BazaNorthCapitalAcl,
    ListStatesResponseCmsDto,
    NcGetInvestorAccountPersonalInfoResponse,
} from '@scaliolabs/baza-nc-shared';
import IMask from 'imask';
import * as moment from 'moment';
import { NzSelectComponent } from 'ng-zorro-antd/select';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, finalize, first, skipWhile, switchMap, takeUntil, tap } from 'rxjs/operators';
import { bazaNcBundleCmsConfig } from '../../../../config';
import { NcInvestorAccountRetryCmsService } from '../nc-investor-account-retry-cms/nc-investor-account-retry-cms.service';
import { NcInvestorAccountRetryDocumentCmsService } from '../nc-investor-account-retry-document-cms/nc-investor-account-retry-document-cms.service';
import { NcInvestorAccountsCmsResolverData } from './nc-investor-accounts-cms.resolver';

interface State {
    config: BazaCrudConfig<BazaNcInvestorAccountDto>;
}

interface SyncFormValue {
    ncAccountId: string;
    ncPartyId: string;
}

interface AddInvestorAccountFormValue {
    account: AccountDto;
    ncAccountId: string;
    ncPartyId: string;
    sync: boolean;
}

export const BAZA_NC_INVESTOR_ACCOUNT_CMS_ID = 'baza-nc-investor-account-cms-id';
export const BAZA_NC_INVESTOR_ACCOUNT_FORM_ID = 'baza-nc-investor-account-form-id';
export const BAZA_NC_INVESTOR_ACCOUNT_EDIT_FORM_ID = 'baza-nc-investor-account-edit-form-id';
export const BAZA_NC_INVESTOR_ACCOUNT_SYNC_FORM_ID = 'baza-nc-investor-account-sync-form-id';

@Component({
    templateUrl: './nc-investor-accounts-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcInvestorAccountsCmsComponent implements OnDestroy {
    public crud: BazaCrudComponent<BazaNcInvestorAccountDto>;

    public state: State = {
        config: this.crudConfig(),
    };

    public residentialStates$!: Observable<ListStatesResponseCmsDto>;

    private readonly RETRYABLE_CUSTOMER_STATUSES = [DwollaCustomerStatus.Retry, DwollaCustomerStatus.Document];

    private readonly ngOnDestroy$ = new Subject<void>();

    constructor(
        private readonly fb: FormBuilder,
        private readonly route: ActivatedRoute,
        private readonly vcr: ViewContainerRef,
        private readonly translate: TranslateService,
        private readonly endpoint: BazaNcInvestorAccountCmsDataAccess,
        private readonly accountVerificationDataAccess: BazaNcAccountVerificationCmsDataAccess,
        private readonly syncEndpoint: BazaNcSyncCmsDataAccess,
        private readonly dwollaEndpoint: BazaNcDwollaCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly modalLayout: BazaFormLayoutModalService,
        private readonly confirm: BazaNgConfirmService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly searchModal: BazaAccountSearchModalService,
        private readonly investorAccountRetryCmsService: NcInvestorAccountRetryCmsService,
        private readonly investorAccountRetryDocumentCmsService: NcInvestorAccountRetryDocumentCmsService,
        private readonly loading: BazaLoadingService,
    ) {}

    get resolvedData(): NcInvestorAccountsCmsResolverData {
        return this.route.snapshot.data['resources'];
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
        this.ngOnDestroy$.complete();
    }

    i18n(key: string): string {
        return `baza-nc.investorAccount.components.ncInvestorAccountCms.${key}`;
    }

    private crudConfig(): BazaCrudConfig<BazaNcInvestorAccountDto> {
        return {
            id: BAZA_NC_INVESTOR_ACCOUNT_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('actions.exportToCsv'),
                            icon: 'download',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (request) =>
                                this.endpoint.exportInvestorAccountsToCsv({
                                    ...request,
                                    listRequest: this.crud.data.state.lastRequest,
                                    fields: [
                                        {
                                            field: 'id',
                                            title: this.translate.instant(this.i18n('columns.id')),
                                        },
                                        {
                                            field: 'userFullName',
                                            title: this.translate.instant(this.i18n('columns.userId')),
                                        },
                                        {
                                            field: 'northCapitalAccountId',
                                            title: this.translate.instant(this.i18n('columns.northCapitalAccountId')),
                                        },
                                        {
                                            field: 'northCapitalPartyId',
                                            title: this.translate.instant(this.i18n('columns.northCapitalPartyId')),
                                        },
                                        {
                                            field: 'dwollaCustomerId',
                                            title: this.translate.instant(this.i18n('columns.dwollaCustomerId')),
                                            disabled: !bazaNcBundleCmsConfig().withDwollaFeatures,
                                        },
                                        {
                                            field: 'isAccountVerificationCompleted',
                                            title: this.translate.instant(this.i18n('columns.isAccountVerificationCompleted')),
                                        },
                                        {
                                            field: 'isBankAccountLinked',
                                            title: this.translate.instant(this.i18n('columns.isBankAccountLinked')),
                                        },
                                        {
                                            field: 'isCreditCardLinked',
                                            title: this.translate.instant(this.i18n('columns.isCreditCardLinked')),
                                        },
                                        {
                                            field: 'isInvestorVerified',
                                            title: this.translate.instant(this.i18n('columns.isInvestorVerified')),
                                        },
                                    ],
                                }),
                            fileNameGenerator: () => 'investor-accounts-' + moment(new Date()).format('mm-dd-yyyy'),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add.title'),
                            type: BazaNzButtonStyle.Default,
                            icon: 'plus',
                            callback: () => {
                                const formGroup = this.fb.group({
                                    account: [undefined, [Validators.required]],
                                    ncAccountId: [undefined, [Validators.required]],
                                    ncPartyId: [undefined, [Validators.required]],
                                    sync: [true],
                                });

                                const formBuilder: BazaFormBuilder = {
                                    id: BAZA_NC_INVESTOR_ACCOUNT_FORM_ID,
                                    layout: BazaFormBuilderLayout.FormOnly,
                                    contents: {
                                        fields: [
                                            {
                                                type: BazaFormBuilderControlType.BazaAccount,
                                                formControlName: 'account',
                                                label: this.i18n('actions.add.form.fields.accountId'),
                                                props: {
                                                    options: {
                                                        searchModal: (selected) =>
                                                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                                                            this.searchModal.open({
                                                                vcr: this.vcr,
                                                                selected,
                                                            }),
                                                    } as BazaFieldAccountComponentOptions,
                                                },
                                            },
                                            {
                                                type: BazaFormBuilderControlType.Text,
                                                formControlName: 'ncAccountId',
                                                label: this.i18n('actions.add.form.fields.ncAccountId'),
                                            },
                                            {
                                                type: BazaFormBuilderControlType.Text,
                                                formControlName: 'ncPartyId',
                                                label: this.i18n('actions.add.form.fields.ncPartyId'),
                                            },
                                            {
                                                type: BazaFormBuilderControlType.Checkbox,
                                                formControlName: 'sync',
                                                label: this.i18n('actions.add.form.fields.sync'),
                                            },
                                        ],
                                    },
                                };

                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.modalLayout.open({
                                    formGroup,
                                    formBuilder,
                                    title: this.i18n('actions.add.title'),
                                    onSubmit: () => {
                                        const formValue: AddInvestorAccountFormValue = formGroup.value;

                                        return this.endpoint
                                            .addInvestorAccount({
                                                ...formValue,
                                                accountId: formValue.account.id,
                                            })
                                            .pipe(
                                                tap((response) => {
                                                    this.ngMessages.success({
                                                        message: formValue.sync
                                                            ? this.i18n('actions.add.form.success.withSync')
                                                            : this.i18n('actions.add.form.success.withoutSync'),
                                                        translate: true,
                                                        translateParams: response.syncStatus,
                                                    });
                                                }),
                                                tap(() => this.crud.refresh()),
                                            );
                                    },
                                });
                            },
                        },
                    },
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                ],
                bottomLeft: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.syncPaymentMethods.bulk.title'),
                            isDanger: true,
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.confirm.open({
                                    message: this.i18n('actions.syncPaymentMethods.bulk.confirm'),
                                    confirm: () =>
                                        this.syncEndpoint
                                            .syncAllInvestorAccountsPaymentMethods()
                                            .pipe(
                                                tap(() =>
                                                    this.ngMessages.success({
                                                        message: this.i18n('actions.syncPaymentMethods.bulk.success'),
                                                        translate: true,
                                                    }),
                                                ),
                                            )
                                            .toPromise(),
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.syncAll.title'),
                            isDanger: true,
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.confirm.open({
                                    message: this.i18n('actions.syncAll.confirm'),
                                    confirm: () =>
                                        this.syncEndpoint
                                            .syncAllInvestorAccounts()
                                            .pipe(
                                                tap(() =>
                                                    this.ngMessages.success({
                                                        message: this.i18n('actions.syncAll.success'),
                                                        translate: true,
                                                    }),
                                                ),
                                            )
                                            .toPromise(),
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        visible: of(bazaNcBundleCmsConfig().withDwollaFeatures),
                        options: {
                            title: this.i18n('actions.syncAllDwollaCustomers.title'),
                            isDanger: true,
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.confirm.open({
                                    message: this.i18n('actions.syncAllDwollaCustomers.confirm'),
                                    confirm: () =>
                                        this.dwollaEndpoint
                                            .touchAllInvestors({ async: true })
                                            .pipe(
                                                tap(() =>
                                                    this.ngMessages.success({
                                                        message: this.i18n('actions.syncAllDwollaCustomers.success'),
                                                        translate: true,
                                                    }),
                                                ),
                                            )
                                            .toPromise(),
                                }),
                        },
                    },
                ],
                bottomRight: [],
            },
            data: {
                dataSource: {
                    list: (request) => this.endpoint.listInvestorAccounts(request),
                },
                actions: [
                    {
                        title: this.i18n('actions.edit.title'),
                        icon: 'edit',
                        action: (entity) => this.updateInvestorAccount(entity),
                        acl: [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement],
                    },
                    {
                        title: this.i18n('actions.syncPaymentMethods.single.title'),
                        icon: 'bank',
                        visible: (entity) => !!entity.userId,
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.translate.instant(this.i18n('actions.syncPaymentMethods.single.confirm'), {
                                    northCapitalAccountId: entity.northCapitalAccountId,
                                    northCapitalPartyId: entity.northCapitalPartyId,
                                }),
                                confirm: () =>
                                    this.syncEndpoint
                                        .syncInvestorAccountPaymentMethods({
                                            userId: entity.userId,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    message: this.i18n('actions.syncPaymentMethods.single.success'),
                                                    translate: true,
                                                    translateParams: entity,
                                                }),
                                            ),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                        acl: [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement],
                    },
                    {
                        title: this.i18n('actions.syncTransactions.title'),
                        icon: 'dollar-circle',
                        visible: (entity) => !!entity.userId,
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.syncTransactions.confirm'),
                                confirm: () =>
                                    this.syncEndpoint
                                        .syncTransactions({
                                            userId: entity.userId,
                                            async: true,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    message: this.i18n('actions.syncTransactions.success'),
                                                    translate: true,
                                                    translateParams: entity,
                                                }),
                                            ),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                        acl: [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement],
                    },
                    {
                        title: this.i18n('actions.sync.title'),
                        icon: 'sync',
                        visible: (entity) => !!entity.userId,
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.modalLayout.open<BazaNcInvestorAccountDto, SyncFormValue, any>(
                                {
                                    title: this.i18n('actions.sync.form.title'),
                                    formGroup: this.fb.group({
                                        ncAccountId: [entity.northCapitalAccountId, [Validators.required]],
                                        ncPartyId: [entity.northCapitalPartyId, [Validators.required]],
                                    }),

                                    formBuilder: this.formBuilderSync(),
                                    fetch: () => entity,
                                    onSubmit: (entity, formValue) =>
                                        this.syncEndpoint
                                            .syncInvestorAccount({
                                                userId: entity.userId,
                                                ncAccountId: formValue.ncAccountId,
                                                ncPartyId: formValue.ncPartyId,
                                            })
                                            .pipe(
                                                tap(() =>
                                                    this.ngMessages.success({
                                                        message: this.i18n('actions.sync.success'),
                                                        translate: true,
                                                        translateParams: entity,
                                                    }),
                                                ),
                                                tap(() => this.crud.refresh()),
                                            ),
                                },
                                {
                                    nzClosable: true,
                                },
                            ),
                        acl: [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement],
                    },
                    {
                        title: this.i18n('actions.dwolla.create.title'),
                        icon: 'user-add',
                        visible: (entity) => bazaNcBundleCmsConfig().withDwollaFeatures && !entity.dwollaCustomerId,
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.dwollaEndpoint
                                .touch({ userId: entity.userId })
                                .pipe(
                                    tap(({ result, failedReason }) => {
                                        if (result === BazaNcDwollaTouchResult.Created) {
                                            this.ngMessages.success({
                                                message: this.i18n('actions.dwolla.create.success'),
                                                translate: true,
                                                translateParams: entity,
                                            });
                                        } else if (result === BazaNcDwollaTouchResult.Failed) {
                                            this.ngMessages.warning({
                                                message: failedReason,
                                                translate: false,
                                            });
                                        } else {
                                            this.ngMessages.warning({
                                                message: result,
                                                translate: false,
                                            });
                                        }
                                    }),
                                    tap(() => this.crud.refresh()),
                                    finalize(() => loading.complete()),
                                )
                                .toPromise();
                        },
                        acl: [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement],
                    },
                    {
                        title: this.i18n('actions.dwolla.update.title'),
                        icon: 'user-switch',
                        visible: (entity) => bazaNcBundleCmsConfig().withDwollaFeatures && !!entity.dwollaCustomerId,
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.dwollaEndpoint
                                .touch({ userId: entity.userId })
                                .pipe(
                                    tap(({ result, failedReason }) => {
                                        if (result === BazaNcDwollaTouchResult.Created) {
                                            this.ngMessages.success({
                                                message: this.i18n('actions.dwolla.update.success'),
                                                translate: true,
                                                translateParams: entity,
                                            });
                                        } else if (result === BazaNcDwollaTouchResult.Failed) {
                                            this.ngMessages.warning({
                                                message: failedReason,
                                                translate: false,
                                            });
                                        } else {
                                            this.ngMessages.warning({
                                                message: result,
                                                translate: false,
                                            });
                                        }
                                    }),
                                    tap(() => this.crud.refresh()),
                                    finalize(() => loading.complete()),
                                )
                                .toPromise();
                        },
                        acl: [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement],
                    },
                    {
                        title: this.i18n('actions.dwolla.retryVerification.title'),
                        icon: 'redo',
                        visible: (entity) =>
                            bazaNcBundleCmsConfig().withDwollaFeatures &&
                            this.RETRYABLE_CUSTOMER_STATUSES.includes(entity.dwollaCustomerVerificationStatus as DwollaCustomerStatus),
                        action: (entity) => {
                            switch (entity.dwollaCustomerVerificationStatus) {
                                case DwollaCustomerStatus.Retry:
                                    // eslint-disable-next-line security/detect-non-literal-fs-filename
                                    this.investorAccountRetryCmsService
                                        .open({
                                            userId: entity.userId,
                                            parentVcr: this.vcr,
                                        })
                                        .then(() => this.crud.refresh());
                                    break;
                                case DwollaCustomerStatus.Document:
                                    // eslint-disable-next-line security/detect-non-literal-fs-filename
                                    this.investorAccountRetryDocumentCmsService
                                        .open({
                                            userId: entity.userId,
                                            parentVcr: this.vcr,
                                        })
                                        .then(() => this.crud.refresh());
                                    break;
                            }
                        },
                        acl: [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement],
                    },
                    {
                        title: this.i18n('actions.remove.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.remove.confirm'),
                                confirm: () =>
                                    this.endpoint
                                        .removeInvestorAccount({
                                            investorAccountId: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    message: this.i18n('actions.remove.success'),
                                                    translate: true,
                                                    translateParams: entity,
                                                }),
                                            ),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                        acl: [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement],
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: true,
                    },
                    {
                        field: 'userId',
                        title: this.i18n('columns.userId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => (entity.userId ? `${entity.userFullName} / id (${entity.userId})` : `– UNKNOWN –`),
                        },
                        sortable: true,
                        sortField: 'fullName',
                    },
                    {
                        field: 'northCapitalAccountId',
                        title: this.i18n('columns.northCapitalAccountId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                        sortable: true,
                    },
                    {
                        field: 'northCapitalPartyId',
                        title: this.i18n('columns.northCapitalPartyId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                        sortable: true,
                    },
                    {
                        field: 'dwollaCustomerId',
                        title: this.i18n('columns.dwollaCustomerId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => {
                                return entity.dwollaCustomerId || 'N/A';
                            },
                            hint: (entity) => {
                                const status = entity.dwollaCustomerVerificationStatus;

                                if (status && status !== DwollaCustomerStatus.Verified) {
                                    if (
                                        [DwollaCustomerStatus.DocumentProcessing, DwollaCustomerStatus.Suspended].includes(
                                            status as DwollaCustomerStatus,
                                        ) ||
                                        this.RETRYABLE_CUSTOMER_STATUSES.includes(status as DwollaCustomerStatus)
                                    ) {
                                        return {
                                            text: this.i18n(`hints.${status}`),
                                            translate: true,
                                            icon: 'warning',
                                        };
                                    }

                                    return {
                                        text: status,
                                        translate: false,
                                        icon: 'warning',
                                    };
                                }

                                if (entity.dwollaCustomerError) {
                                    return {
                                        text: entity.dwollaCustomerError,
                                        translate: false,
                                        icon: 'warning',
                                    };
                                }

                                return null;
                            },
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                        sortable: true,
                        disabled: !bazaNcBundleCmsConfig().withDwollaFeatures,
                        ngStyle: (entity) => {
                            const status = entity.dwollaCustomerVerificationStatus;
                            const isInvalid =
                                !entity.dwollaCustomerId ||
                                entity.dwollaCustomerError ||
                                (status && status !== DwollaCustomerStatus.Verified);

                            return isInvalid ? { color: 'var(--error-color)' } : {};
                        },
                    },
                    {
                        field: 'isAccountVerificationCompleted',
                        title: this.i18n('columns.isAccountVerificationCompleted'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                        sortable: true,
                    },
                    {
                        field: 'isBankAccountLinked',
                        title: this.i18n('columns.isBankAccountLinked'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                        sortable: true,
                    },
                    {
                        field: 'isCreditCardLinked',
                        title: this.i18n('columns.isCreditCardLinked'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                        sortable: true,
                    },
                    {
                        field: 'isInvestorVerified',
                        title: this.i18n('columns.isInvestorVerified'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                        sortable: true,
                    },
                    {
                        title: this.i18n('columns.taxDocuments'),
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (entity) => ({
                                routerLink: ['/baza-nc/tax-documents', entity.id],
                            }),
                            icon: 'file-pdf',
                            format: () => this.i18n('columns.taxDocuments'),
                            translate: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 120,
                        disabled: !bazaNcBundleCmsConfig().withTaxDocumentsFeature,
                    },
                    {
                        title: this.i18n('columns.transactions'),
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (entity) => ({
                                routerLink: ['/baza-nc/transactions'],
                                queryParams: {
                                    accountId: entity.userId,
                                },
                            }),
                            icon: 'inbox',
                            format: () => this.i18n('columns.transactions'),
                            translate: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                    },
                    {
                        title: this.i18n('columns.dividends'),
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (entity) => ({
                                routerLink: ['/baza-nc/dividends/investor-account', entity.id],
                            }),
                            icon: 'dollar-circle',
                            format: () => this.i18n('columns.dividends'),
                            translate: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                        disabled: !(bazaNcBundleCmsConfig().withNcDividendsFeature || bazaNcBundleCmsConfig().withDwollaDividendsFeature),
                    },
                ],
            },
        };
    }

    private updateInvestorAccount(entity: BazaNcInvestorAccountDto): void {
        const formGroup = this.fb.group({
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            ssn: ['', Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(11)])],
            dateOfBirth: ['', [Validators.required]],
            residentialCountry: ['', [Validators.required]],
            residentialStreetAddress1: ['', [Validators.required]],
            residentialStreetAddress2: [''],
            residentialCity: ['', [Validators.required]],
            residentialState: ['', [Validators.required]],
            residentialZipCode: ['', [Validators.required]],
        });

        const formControlCountry = formGroup.controls['residentialCountry'];
        const formControlState = formGroup.controls['residentialState'];

        this.residentialStates$ = formControlCountry.valueChanges.pipe(
            skipWhile((country) => !country),
            switchMap((country, streamIndex) => {
                if (streamIndex > 0) {
                    formControlState.reset();
                }

                return this.accountVerificationDataAccess.listStates({ country });
            }),
            tap(({ states }) => (states.length ? formControlState.enable() : formControlState.disable())),
        );

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const form = this.formLayout.open<NcGetInvestorAccountPersonalInfoResponse, BazaNcInvestorAccountUpdatePersonalInformationRequest>({
            formGroup,
            title: this.i18n('forms.updateInvestorAccount.title'),
            formBuilder: this.updateInvestorAccountFormBuilder(entity),
            fetch: () => {
                const loading = this.loading.addLoading();

                return this.endpoint.getInvestorAccountPersonalInfo({ id: entity.id }).pipe(
                    catchError((error) => {
                        form.destroy();

                        this.ngMessages.error({ message: error.message || JSON.stringify(error) });

                        return throwError(() => error);
                    }),
                    finalize(() => loading.complete()),
                );
            },
            populate: (input) => formGroup.patchValue(input),
            onSubmit: (_, formValue) => {
                const loading = this.loading.addLoading();

                this.endpoint
                    .updateInvestorAccountPersonalInfo({
                        ...formValue,
                        id: entity.id,
                        dateOfBirth: moment(new Date(formValue.dateOfBirth)).format('MM-DD-YYYY'),
                    })
                    .pipe(
                        catchError((error) => {
                            this.ngMessages.error({ message: error.message || JSON.stringify(error) });

                            return throwError(() => error);
                        }),
                        finalize(() => loading.complete()),
                    )
                    .subscribe(() => {
                        this.ngMessages.success({
                            message: this.i18n('forms.updateInvestorAccount.success'),
                            translate: true,
                        });
                    });
            },
            afterSubmit: () => {
                this.crud.refresh();
            },
        });
    }

    /**
     *
     * Possible scenarios for edition of available investor account
     *
     * 1st: Dwolla account is NOT available. NC account KYC is NOT passed.
     *   -> All the fields validations work as per description. It is possible to modify all the fields.
     *
     * 2nd: Dwolla account is not available. NC account KYC is passed.
     *   -> All the fields validations work as per description. It is possible to modify all the fields except SSN.
     *
     * 3rd: Dwolla account is available and is NOT verified (one of retry/document/suspended statuses). NC account KYC is NOT passed.
     *   -> All the fields validations work as per description. It is possible to modify all the fields.
     *
     * 4th: Dwolla account is available and is NOT verified (one of retry/document/suspended statuses). NC account KYC is passed.
     *   -> All the fields validations work as per description. It is possible to modify all the fields except SSN.
     *
     * 5th: Dwolla account is available and is verified. NC account KYC is NOT passed.
     *   -> All the fields validations work as per description. It is possible to modify all the fields except SSN.
     *
     * 6th: Dwolla account is available and is verified. NC account KYC is passed.
     *   -> All the fields validations work as per description. It is possible to modify all the fields except SSN.
     *
     * @see https://scalio.atlassian.net/browse/BAZA-1361
     */
    private updateInvestorAccountFormBuilder(entity: BazaNcInvestorAccountDto): BazaFormBuilder {
        const isDwollaAccountAvailable = entity.isDwollaAvailable;
        const isDwollaAccountVerified = entity.dwollaCustomerVerificationStatus === DwollaCustomerStatus.Verified;
        const isKycPassed = entity.isInvestorVerified;

        const scenarios: Record<'first' | 'second' | 'third' | 'fourth' | 'fifth' | 'sixth', boolean> = {
            first: !isDwollaAccountAvailable && !isKycPassed,
            second: !isDwollaAccountAvailable && isKycPassed,
            third: isDwollaAccountAvailable && !isDwollaAccountVerified && !isKycPassed,
            fourth: isDwollaAccountAvailable && !isDwollaAccountVerified && isKycPassed,
            fifth: isDwollaAccountAvailable && isDwollaAccountVerified && !isKycPassed,
            sixth: isDwollaAccountAvailable && isDwollaAccountVerified && isKycPassed,
        };

        const isSsnDisabled = scenarios.second || scenarios.fourth || scenarios.fifth || scenarios.sixth;

        const isDateOfBirthDisabled = isDwollaAccountVerified || isKycPassed;

        return {
            id: BAZA_NC_INVESTOR_ACCOUNT_EDIT_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderStaticComponentType.Divider,
                        props: {
                            nzText: this.i18n('forms.updateInvestorAccount.sections.personalInformation'),
                            nzOrientation: 'center',
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'firstName',
                        label: this.i18n('forms.updateInvestorAccount.fields.firstName'),
                        required: true,
                        autoFocus: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'lastName',
                        label: this.i18n('forms.updateInvestorAccount.fields.lastName'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'ssn',
                        label: this.i18n('forms.updateInvestorAccount.fields.ssn'),
                        required: true,
                        disabled: isSsnDisabled,
                        afterViewInit: (componentRef: ComponentRef<BazaFormBuilderTextComponent>, portal) => {
                            const formControlSsn = portal.formGroup.controls[portal.formControlName];

                            const input = componentRef.location.nativeElement.querySelector('input') as HTMLInputElement;

                            const maskConfig: IMask.AnyMaskedOptions = {
                                mask: 'nnn-vv-cccc',
                                lazy: true,
                                overwrite: true,
                                autofix: true,
                                blocks: {
                                    nnn: { mask: IMask.MaskedRange, from: 0, to: 999, maxLength: 3 },
                                    vv: { mask: IMask.MaskedRange, from: 0, to: 99, maxLength: 2 },
                                    cccc: { mask: IMask.MaskedRange, from: 0, to: 9999, maxLength: 4 },
                                },
                            };

                            const mask = IMask(input, maskConfig);

                            formControlSsn.valueChanges
                                .pipe(
                                    first((ssn) => ssn),
                                    tap(() => mask.updateValue()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe((ssn: string) => {
                                    input.onfocus = () => {
                                        if (ssn.includes('*')) {
                                            formControlSsn.reset();
                                        }
                                    };

                                    input.onblur = ({ target }: Event) => {
                                        const currentValue = (target as HTMLInputElement).value;

                                        if (ssn.includes('*') && !currentValue) {
                                            formControlSsn.patchValue(ssn);
                                        }
                                    };
                                });
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Date,
                        formControlName: 'dateOfBirth',
                        label: this.i18n('forms.updateInvestorAccount.fields.dateOfBirth'),
                        required: true,
                        disabled: isDateOfBirthDisabled,
                    },
                    {
                        type: BazaFormBuilderStaticComponentType.Divider,
                        props: {
                            nzText: this.i18n('forms.updateInvestorAccount.sections.residentialAddress'),
                            nzOrientation: 'center',
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Select,
                        formControlName: 'residentialCountry',
                        label: this.i18n('forms.updateInvestorAccount.fields.country'),
                        values: Object.values(this.resolvedData.formResources.listCountries).map((country) => ({
                            label: country,
                            value: country,
                        })),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'residentialStreetAddress1',
                        label: this.i18n('forms.updateInvestorAccount.fields.streetAddress'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'residentialStreetAddress2',
                        label: this.i18n('forms.updateInvestorAccount.fields.streetAddress2'),
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'residentialCity',
                        label: this.i18n('forms.updateInvestorAccount.fields.city'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Select,
                        formControlName: 'residentialState',
                        label: this.i18n('forms.updateInvestorAccount.fields.state'),
                        required: true,
                        afterViewInit: (componentRef: ComponentRef<NzSelectComponent>, portal) => {
                            this.residentialStates$.pipe(takeUntil(this.ngOnDestroy$)).subscribe(({ states }) => {
                                const definition = portal.definition as BazaFormControlFieldSelect;

                                definition.values = states.map((state) => ({
                                    label: state.title,
                                    value: state.value,
                                }));

                                portal.updateProps();

                                componentRef.changeDetectorRef.detectChanges();
                            });
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'residentialZipCode',
                        label: this.i18n('forms.updateInvestorAccount.fields.zipCode'),
                        required: true,
                    },
                ],
            },
        };
    }

    private formBuilderSync(): BazaFormBuilder {
        return {
            id: BAZA_NC_INVESTOR_ACCOUNT_SYNC_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'ncAccountId',
                        label: this.i18n('actions.sync.form.fields.ncAccountId'),
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'ncPartyId',
                        label: this.i18n('actions.sync.form.fields.ncPartyId'),
                    },
                ],
            },
        };
    }
}
