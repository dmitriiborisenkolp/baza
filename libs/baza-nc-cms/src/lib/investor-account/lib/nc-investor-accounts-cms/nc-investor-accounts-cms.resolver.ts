import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaNcAccountVerificationCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNcCmsAccountVerificationFormResourcesDto } from '@scaliolabs/baza-nc-shared';
import { combineLatest, Observable, retry } from 'rxjs';

export interface ResolverData {
    formResources: BazaNcCmsAccountVerificationFormResourcesDto;
}

export { ResolverData as NcInvestorAccountsCmsResolverData };

@Injectable({ providedIn: 'root' })
export class NcInvestorAccountsCmsResolver implements Resolve<ResolverData> {
    constructor(private readonly accountVerificationDataAccess: BazaNcAccountVerificationCmsDataAccess) {}

    resolve(): Observable<ResolverData> {
        return combineLatest({
            formResources: this.accountVerificationDataAccess.formResources(),
        }).pipe(
            retry({
                delay: genericRetryStrategy(),
            }),
        );
    }
}
