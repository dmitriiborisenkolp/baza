import { Routes } from '@angular/router';
import { NcInvestorAccountsCmsComponent } from './nc-investor-accounts-cms/nc-investor-accounts-cms.component';
import { NcInvestorAccountsCmsResolver } from './nc-investor-accounts-cms/nc-investor-accounts-cms.resolver';

export const bazaNcInvestorAccountCmsRoutes: Routes = [
    {
        path: 'baza-nc/investor-accounts',
        component: NcInvestorAccountsCmsComponent,
        resolve: {
            resources: NcInvestorAccountsCmsResolver,
        },
    },
];
