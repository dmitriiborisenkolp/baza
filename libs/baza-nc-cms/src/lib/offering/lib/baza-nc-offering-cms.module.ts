import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCrudCmsModule } from '@scaliolabs/baza-core-cms';
import { NcOfferingCmsComponent } from './components/nc-offering-cms/nc-offering-cms.component';
import { BazaCmsLayoutModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsDataAccessModule } from '@scaliolabs/baza-nc-cms-data-access';
import { NcOfferingHealthCheckCmsComponent } from './components/nc-offering-health-check-cms/nc-offering-health-check-cms.component';
import { NcOfferingHealthCheckCmsResolve } from './components/nc-offering-health-check-cms/nc-offering-health-check-cms.resolve';

@NgModule({
    imports: [CommonModule, BazaCrudCmsModule, BazaNcCmsDataAccessModule, BazaCmsLayoutModule],
    declarations: [NcOfferingCmsComponent, NcOfferingHealthCheckCmsComponent],
    providers: [NcOfferingHealthCheckCmsResolve],
})
export class BazaNcOfferingCmsModule {}
