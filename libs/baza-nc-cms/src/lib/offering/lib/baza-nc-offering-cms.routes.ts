import { Routes } from '@angular/router';
import { NcOfferingCmsComponent } from './components/nc-offering-cms/nc-offering-cms.component';
import { NcOfferingHealthCheckCmsComponent } from './components/nc-offering-health-check-cms/nc-offering-health-check-cms.component';
import { NcOfferingHealthCheckCmsResolve } from './components/nc-offering-health-check-cms/nc-offering-health-check-cms.resolve';

export const bazaNcOfferingCmsRoutes: Routes = [
    {
        path: 'baza-nc/offerings',
        component: NcOfferingCmsComponent,
    },
    {
        path: 'baza-nc/offerings/:offeringId/health-check',
        component: NcOfferingHealthCheckCmsComponent,
        resolve: {
            data: NcOfferingHealthCheckCmsResolve,
        },
    },
];
