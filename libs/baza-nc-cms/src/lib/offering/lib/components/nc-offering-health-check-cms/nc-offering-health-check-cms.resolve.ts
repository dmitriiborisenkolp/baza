import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { BazaNcOfferingDto } from '@scaliolabs/baza-nc-shared';
import { combineLatest, Observable, throwError } from 'rxjs';
import { BazaNcOfferingCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNotFoundService } from '@scaliolabs/baza-core-cms';
import { BazaNgMessagesService, genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { catchError, map, retryWhen } from 'rxjs/operators';

interface ResolveData {
    offering: BazaNcOfferingDto;
}

export { ResolveData as NcOfferingHealthCheckCmsResolveData };

@Injectable()
export class NcOfferingHealthCheckCmsResolve implements Resolve<ResolveData> {
    constructor(
        private readonly notFound: BazaNotFoundService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly offeringDataAccess: BazaNcOfferingCmsDataAccess,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> {
        const ncOfferingId = route.params['offeringId'];

        const observables: [Observable<BazaNcOfferingDto>] = [
            this.offeringDataAccess.getByNcOfferingId({
                ncOfferingId,
            }),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([offering]) => ({
                offering,
            })),
            catchError((err) => {
                this.notFound.navigateToNotFound();

                this.ngMessages.bazaError(err);

                return throwError(err);
            }),
        );
    }
}
