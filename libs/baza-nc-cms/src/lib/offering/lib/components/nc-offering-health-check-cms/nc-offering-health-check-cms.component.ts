import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaCrudMessage,
    BazaCrudMessageLevel,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    BazaNcOfferingHealthCheckIssue,
    BazaNcOfferingHealthCheckIssueDto,
    NcOfferingHealthCheckResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { ActivatedRoute } from '@angular/router';
import { NcOfferingHealthCheckCmsResolveData } from './nc-offering-health-check-cms.resolve';
import { map, tap } from 'rxjs/operators';
import { BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';

export const BAZA_NC_HEALTH_CHECK_CMS_ID = 'baza-nc-health-check-cms-id';

interface State {
    config: BazaCrudConfig<BazaNcOfferingHealthCheckIssueDto>;
}

@Component({
    templateUrl: './nc-offering-health-check-cms.component.html',
    styleUrls: ['./nc-offering-health-check-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcOfferingHealthCheckCmsComponent {
    public crud: BazaCrudComponent<BazaNcOfferingHealthCheckIssueDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly activatedRoute: ActivatedRoute,
        private readonly offeringDataAccess: BazaNcOfferingCmsDataAccess,
    ) {}

    i18n(key: string): string {
        return `baza-nc.offering.components.offeringHealthCheckCms.${key}`;
    }

    i18nIssueType(issueType: BazaNcOfferingHealthCheckIssue): string {
        return `baza-nc._.types.offeringHealthCheckIssue.enum.${issueType}`;
    }

    i18nIssueAction(issueType: BazaNcOfferingHealthCheckIssue): string {
        return `baza-nc._.types.offeringHealthCheckIssue.action.${issueType}`;
    }

    get resolvedData(): NcOfferingHealthCheckCmsResolveData {
        return this.activatedRoute.snapshot.data['data'];
    }

    private get crudConfig(): BazaCrudConfig<BazaNcOfferingHealthCheckIssueDto> {
        return {
            id: BAZA_NC_HEALTH_CHECK_CMS_ID,
            title: this.i18n('title'),
            titleArguments: this.resolvedData.offering,
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.refresh'),
                            type: BazaNzButtonStyle.Link,
                            callback: () => this.crud.refresh(),
                            icon: 'sync',
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    array: () =>
                        this.offeringDataAccess
                            .healthCheck({
                                ncOfferingId: this.resolvedData.offering.ncOfferingId,
                            })
                            .pipe(
                                tap((response) => {
                                    this.updateMessages(response);
                                }),
                                map((response) => ({
                                    items: response.issues,
                                })),
                            ),
                },
                columns: [
                    {
                        field: 'source',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.source'),
                        width: 250,
                        ngStyle: () => ({
                            'font-weight': 500,
                        }),
                    },
                    {
                        field: 'issue',
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nIssueType(entity.issue),
                            translate: true,
                        },
                        title: this.i18n('columns.issue'),
                    },
                    {
                        field: 'issue-action',
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nIssueAction(entity.issue),
                            translate: true,
                        },
                        title: this.i18n('columns.action'),
                    },
                ],
            },
            backRouterLink: {
                routerLink: ['/baza-nc/offerings'],
            },
        };
    }

    private updateMessages(response: NcOfferingHealthCheckResponse): void {
        const messages: Array<BazaCrudMessage> = [];

        if (response.healthy) {
            messages.push({
                message: {
                    text: this.i18n('messages.healthy'),
                    translate: true,
                },
                level: BazaCrudMessageLevel.Success,
                withIcon: true,
            });
        } else {
            messages.push({
                message: {
                    text: this.i18n('messages.notHealthy'),
                    translate: true,
                },
                level: BazaCrudMessageLevel.Warning,
                withIcon: true,
            });
        }

        messages.push({
            message: {
                text: this.i18n('messages.ncBugMessage'),
                translate: true,
            },
            level: BazaCrudMessageLevel.Info,
            withIcon: true,
        });

        this.state = {
            ...this.state,
            config: {
                ...this.state.config,
                messages,
            },
        };

        this.cdr.markForCheck();
    }
}
