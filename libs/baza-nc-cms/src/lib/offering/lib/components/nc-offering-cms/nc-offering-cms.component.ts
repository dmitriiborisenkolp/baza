import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudDataColumn,
    BazaCrudItem,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutModalService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { distinctUntilChanged, finalize, takeUntil, tap } from 'rxjs/operators';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { BazaNcOfferingDto, BazaNcOfferingStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { BazaNcOfferingCmsDataAccess, BazaNcSyncCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { bazaNcBundleCmsConfig } from '../../../../../config';
import { Router } from '@angular/router';

interface State {
    config: BazaCrudConfig<BazaNcOfferingDto>;
}

interface FormValue {
    ncOfferingId: string;
}

const columnsWithoutDetails: Array<keyof BazaNcOfferingDto | string> = [
    'id',
    'ncOfferingId',
    'status',
    'percentsFunded',
    'ncUnitPrice',
    'ncMinAmount',
    'ncTargetAmount',
    'link',
];

export const BAZA_NC_OFFERING_CMS_ID = 'baza-nc-offering-cms-id';
export const BAZA_NC_OFFERING_CMS_IMPORT_OFFERING_FORM_ID = 'baza-nc-offering-cms-import-offering-form-id';

@Component({
    templateUrl: './nc-offering-cms.component.html',
    styleUrls: ['./nc-offering-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcOfferingCmsComponent implements OnInit, OnDestroy {
    public crud: BazaCrudComponent<BazaNcOfferingDto>;

    private withDetails: FormControl = new FormControl(false);

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly router: Router,
        private readonly loading: BazaLoadingService,
        private readonly endpoint: BazaNcOfferingCmsDataAccess,
        private readonly syncEndpoint: BazaNcSyncCmsDataAccess,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly formLayout: BazaFormLayoutModalService,
    ) {}

    public state: State = {
        config: this.crudConfig(),
    };

    ngOnInit(): void {
        this.withDetails.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            this.state = {
                ...this.state,
                config: {
                    ...this.state.config,
                    data: {
                        ...this.state.config.data,
                        columns: next ? this.crudColumns() : this.crudColumns(columnsWithoutDetails),
                    },
                },
            };

            this.cdr.markForCheck();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza-nc.offering.components.ncOfferingCms.${key}`;
    }

    i18nOfferingStatus(status: BazaNcOfferingStatus): string {
        return `baza-nc._.types.offeringStatus.${status}`;
    }

    private crudConfig(): BazaCrudConfig<BazaNcOfferingDto> {
        const crudConfig: BazaCrudConfig<BazaNcOfferingDto> = {
            id: BAZA_NC_OFFERING_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Checkbox,
                        options: {
                            label: this.i18n('withDetails'),
                            control: this.withDetails,
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.syncAllPercentsFunded.label'),
                            icon: 'sync',
                            isDanger: true,
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.ngConfirm.open({
                                    message: this.i18n('actions.syncAllPercentsFunded.confirm'),
                                    confirm: () =>
                                        this.syncEndpoint
                                            .syncPercentsFunded()
                                            .pipe(
                                                tap(() =>
                                                    this.ngMessages.success({
                                                        message: this.i18n('actions.syncAllPercentsFunded.success'),
                                                        translate: true,
                                                    }),
                                                ),
                                            )
                                            .toPromise(),
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.syncAllOfferings.label'),
                            icon: 'sync',
                            isDanger: true,
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.ngConfirm.open({
                                    message: this.i18n('actions.syncAllOfferings.confirm'),
                                    confirm: () =>
                                        this.endpoint
                                            .syncAllOfferings()
                                            .pipe(
                                                tap(() =>
                                                    this.ngMessages.success({
                                                        message: this.i18n('actions.syncAllOfferings.success'),
                                                        translate: true,
                                                    }),
                                                ),
                                            )
                                            .toPromise(),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.endpoint.list(request),
                },
                columns: [...this.crudColumns(columnsWithoutDetails)],
                actions: [
                    {
                        title: this.i18n('actions.syncPercentsFunded.title'),
                        icon: 'percentage',
                        action: (entity) =>
                            this.syncEndpoint
                                .syncPercentsFundedOfOffering({
                                    offeringId: entity.ncOfferingId,
                                })
                                .subscribe(
                                    () => {
                                        this.ngMessages.success({
                                            message: this.i18n('actions.syncPercentsFunded.success'),
                                            translate: true,
                                        });

                                        this.crud.refresh();
                                    },
                                    (err) => this.ngMessages.bazaError(err),
                                ),
                    },
                    {
                        title: this.i18n('actions.healthCheck'),
                        icon: 'medicine-box',
                        action: (entity) => this.router.navigate(['/baza-nc/offerings', entity.ncOfferingId, 'health-check']),
                    },
                ],
            },
        };

        if (bazaNcBundleCmsConfig().withImportOfferingFeature) {
            crudConfig.items.topRight.push({
                type: BazaCrudItem.Button,
                options: {
                    title: this.i18n('actions.importOffering'),
                    icon: 'import',
                    callback: () => this.importOffering(),
                },
            });
        }

        if (bazaNcBundleCmsConfig().withForgetOfferingFeature) {
            if (!Array.isArray(crudConfig.data.actions)) {
                crudConfig.data.actions = [];
            }

            crudConfig.data.actions.push({
                icon: 'delete',
                title: this.i18n('actions.forgetOffering.title'),
                action: (entity) => this.forgetOffering(entity.ncOfferingId),
            });
        }

        return crudConfig;
    }

    crudColumns(
        withColumns: Array<keyof BazaNcOfferingDto | string> = [
            'id',
            'ncOfferingId',
            'ncOfferingStatus',
            'status',
            'percentsFunded',
            'ncUnitPrice',
            'ncMinAmount',
            'ncMaxAmount',
            'ncTargetAmount',
            'link',
        ],
    ): Array<BazaCrudDataColumn<BazaNcOfferingDto>> {
        const columns: Array<BazaCrudDataColumn<BazaNcOfferingDto>> = [
            {
                field: 'id',
                title: this.i18n('columns.id'),
                type: {
                    kind: BazaTableFieldType.Id,
                },
                width: 80,
                sortable: true,
            },
            {
                field: 'ncOfferingId',
                title: this.i18n('columns.ncOfferingId'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                ngStyle: () => ({
                    'font-weight': 500,
                }),
                width: 170,
                sortable: true,
            },
            {
                field: 'ncOfferingStatus',
                title: this.i18n('columns.ncOfferingStatus'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                textAlign: BazaTableFieldAlign.Center,
                sortable: true,
                width: 140,
            },
            {
                field: 'status',
                title: this.i18n('columns.status'),
                type: {
                    kind: BazaTableFieldType.Text,
                    format: (entity) => this.i18nOfferingStatus(entity.status),
                    translate: true,
                },
                textAlign: BazaTableFieldAlign.Center,
                sortable: true,
                width: 160,
                ngStyle: (entity) => ({
                    color: (() => {
                        switch (entity.status) {
                            default: {
                                return 'var(--black)';
                            }

                            case BazaNcOfferingStatus.Open: {
                                return 'var(--success-color)';
                            }

                            case BazaNcOfferingStatus.Closed: {
                                return 'var(--error-color)';
                            }

                            case BazaNcOfferingStatus.ComingSoon: {
                                return 'var(--processing-color)';
                            }
                        }
                    })(),
                }),
            },
            {
                field: 'percentsFunded',
                title: this.i18n('columns.percentsFunded'),
                type: {
                    kind: BazaTableFieldType.Progress,
                    nzStatus: 'normal',
                },
                sortable: true,
            },
            {
                field: 'ncUnitPrice',
                title: this.i18n('columns.ncUnitPrice'),
                source: (entity) => `$${entity.ncUnitPrice.toFixed(2)}`,
                type: {
                    kind: BazaTableFieldType.Text,
                },
                sortable: true,
                width: 165,
            },
            {
                field: 'ncMinAmount',
                title: this.i18n('columns.ncMinAmount'),
                source: (entity) => `$${entity.ncMinAmount.toFixed(2)}`,
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 170,
            },
            {
                field: 'ncMaxAmount',
                title: this.i18n('columns.ncMaxAmount'),
                source: (entity) => `$${entity.ncMaxAmount.toFixed(2)}`,
                type: {
                    kind: BazaTableFieldType.Text,
                },
                sortable: true,
                width: 170,
            },
            {
                field: 'ncTargetAmount',
                title: this.i18n('columns.ncTargetAmount'),
                source: (entity) => `$${entity.ncTargetAmount.toFixed(2)}`,
                type: {
                    kind: BazaTableFieldType.Text,
                },
                sortable: true,
                width: 170,
            },
            {
                field: 'link',
                title: '',
                type: {
                    kind: BazaTableFieldType.Link,
                    link: (entity) => ({
                        routerLink: ['/baza-nc/transactions'],
                        queryParams: {
                            ncOfferingId: entity.ncOfferingId,
                        },
                    }),
                    icon: 'inbox',
                    format: () => this.i18n('columns.transactions'),
                    translate: true,
                },
                textAlign: BazaTableFieldAlign.Center,
                width: 150,
            },
        ];

        return columns.filter((column) => withColumns.includes(column.field));
    }

    importOffering(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaNcOfferingDto, FormValue>({
            title: this.i18n('forms.importOffering.title'),
            formGroup: this.fb.group({
                ncOfferingId: [undefined, [Validators.required]],
            }),
            formBuilder: {
                id: BAZA_NC_OFFERING_CMS_IMPORT_OFFERING_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            formControlName: 'ncOfferingId',
                            type: BazaFormBuilderControlType.Text,
                            label: this.i18n('forms.importOffering.fields.ncOfferingId'),
                        },
                    ],
                },
            },
            onSubmit: (entity, formValue) =>
                this.endpoint
                    .importNcOffering({
                        ncOfferingId: formValue.ncOfferingId,
                    })
                    .pipe(
                        finalize(() => this.crud.refresh()),
                        takeUntil(this.ngOnDestroy$),
                    ),
        });
    }

    forgetOffering(ncOfferingId: OfferingId): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            title: this.i18n('actions.forgetOffering.title'),
            message: this.i18n('actions.forgetOffering.message'),
            messageArgs: {
                ncOfferingId,
            },
            confirm: () => {
                const loading = this.loading.addLoading();

                this.endpoint
                    .forgetNcOffering({
                        ncOfferingId,
                    })
                    .pipe(
                        finalize(() => loading.complete()),
                        finalize(() => this.crud.refresh()),
                        takeUntil(this.ngOnDestroy$),
                    )
                    .subscribe(
                        () =>
                            this.ngMessages.success({
                                message: this.i18n('actions.forgetOffering.success'),
                                translate: true,
                                translateParams: {
                                    ncOfferingId,
                                },
                            }),
                        () =>
                            this.ngMessages.error({
                                message: this.i18n('actions.forgetOffering.failed'),
                                translate: true,
                                translateParams: {
                                    ncOfferingId,
                                },
                            }),
                    );
            },
        });
    }
}
