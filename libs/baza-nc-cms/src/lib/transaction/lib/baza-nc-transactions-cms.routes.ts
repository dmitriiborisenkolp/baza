import { Routes } from '@angular/router';
import { NcTransactionsCmsComponent } from './components/nc-transactions-cms/nc-transactions-cms.component';

export const bazaNcTransactionsCmsRoutes: Routes = [
    {
        path: 'baza-nc/transactions',
        component: NcTransactionsCmsComponent,
    },
];
