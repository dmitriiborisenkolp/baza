import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NcTransactionsCmsComponent } from './components/nc-transactions-cms/nc-transactions-cms.component';
import { BazaCrudCmsModule } from '@scaliolabs/baza-core-cms';
import { BazaCmsLayoutModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsDataAccessModule } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNcKycLogsCmsModule } from '../../kyc-logs';

@NgModule({
    imports: [CommonModule, BazaCrudCmsModule, BazaCmsLayoutModule, BazaNcCmsDataAccessModule, BazaNcKycLogsCmsModule],
    declarations: [NcTransactionsCmsComponent],
})
export class BazaNcTransactionsCmsModule {}
