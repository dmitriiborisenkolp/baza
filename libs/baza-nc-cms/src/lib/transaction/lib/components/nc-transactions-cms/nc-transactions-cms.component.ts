import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudDataColumn, BazaCrudItem, BazaTableFieldType } from '@scaliolabs/baza-core-cms';
import { distinctUntilChanged, switchMap, takeUntil } from 'rxjs/operators';
import { convertFromCents, NcTransactionDto, TransactionState } from '@scaliolabs/baza-nc-shared';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { OrderStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { BazaNcTransactionsCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';

interface FiltersContext {
    accountId?: number;
    ncOfferingId?: string;
}

interface State {
    config: BazaCrudConfig<NcTransactionDto>;
}

export const BAZA_NC_TRANSACTION_CMS_ID = 'baza-nc-transaction-cms-id';

const columnsWithoutDetails: Array<keyof NcTransactionDto> = [
    'id',
    'ncOfferingName',
    'accountFullName',
    'createdAt',
    'amountCents',
    'feeCents',
    'totalCents',
    'shares',
    'pricePerShareCents',
    'state',
];

@Component({
    templateUrl: './nc-transactions-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcTransactionsCmsComponent implements OnInit, OnDestroy {
    public crud: BazaCrudComponent<NcTransactionDto>;

    private filterContext$: BehaviorSubject<FiltersContext> = new BehaviorSubject<FiltersContext>({});
    private withDetails: FormControl = new FormControl(false);

    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly translate: TranslateService,
        private readonly endpoint: BazaNcTransactionsCmsDataAccess,
        private readonly activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams.pipe(takeUntil(this.ngOnDestroy$)).subscribe((qp) => {
            this.filterContext$.next({
                accountId: qp['accountId'] ? parseInt(qp['accountId'], 10) : undefined,
                ncOfferingId: qp['ncOfferingId'],
            });
        });

        this.withDetails.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            this.state = {
                ...this.state,
                config: {
                    ...this.state.config,
                    data: {
                        ...this.state.config.data,
                        columns: next ? this.crudColumns() : this.crudColumns(columnsWithoutDetails),
                    },
                },
            };

            this.cdr.markForCheck();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza-nc.transaction.components.ncTransactionsCms.${key}`;
    }

    private crudConfig(): BazaCrudConfig<NcTransactionDto> {
        return {
            id: BAZA_NC_TRANSACTION_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('actions.exportToCsv'),
                            icon: 'download',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (request) =>
                                this.filterContext$.asObservable().pipe(
                                    takeUntil(this.ngOnDestroy$),
                                    switchMap((filterContext) =>
                                        this.endpoint.exportToCsv({
                                            ...request,
                                            listRequest: {
                                                ...this.crud.data.state.lastRequest,
                                                ...filterContext,
                                            },
                                            fields: [
                                                {
                                                    field: 'id',
                                                    title: this.translate.instant(this.i18n('columns.id')),
                                                },
                                                {
                                                    field: 'ncOfferingName',
                                                    title: this.translate.instant(this.i18n('columns.ncOfferingName')),
                                                },
                                                {
                                                    field: 'accountFullName',
                                                    title: this.translate.instant(this.i18n('columns.accountFullName')),
                                                },
                                                {
                                                    field: 'accountId',
                                                    title: this.translate.instant(this.i18n('columns.accountId')),
                                                },
                                                {
                                                    field: 'createdAt',
                                                    title: this.translate.instant(this.i18n('columns.createdAt')),
                                                },
                                                {
                                                    field: 'ncOfferingId',
                                                    title: this.translate.instant(this.i18n('columns.ncOfferingId')),
                                                },
                                                {
                                                    field: 'ncTradeId',
                                                    title: this.translate.instant(this.i18n('columns.ncTradeId')),
                                                },
                                                {
                                                    field: 'ncOrderStatus',
                                                    title: this.translate.instant(this.i18n('columns.ncOrderStatus')),
                                                },
                                                {
                                                    field: 'amount',
                                                    title: this.translate.instant(this.i18n('columns.amountCents')),
                                                },
                                                {
                                                    field: 'fee',
                                                    title: this.translate.instant(this.i18n('columns.feeCents')),
                                                },
                                                {
                                                    field: 'total',
                                                    title: this.translate.instant(this.i18n('columns.totalCents')),
                                                },
                                                {
                                                    field: 'shares',
                                                    title: this.translate.instant(this.i18n('columns.shares')),
                                                },
                                                {
                                                    field: 'pricePerShare',
                                                    title: this.translate.instant(this.i18n('columns.pricePerShareCents')),
                                                },
                                                {
                                                    field: 'state',
                                                    title: this.translate.instant(this.i18n('columns.state')),
                                                },
                                            ],
                                        }),
                                    ),
                                ),
                            fileNameGenerator: () => 'investor-accounts-' + moment(new Date()).format('mm-dd-yyyy'),
                        },
                    },
                    {
                        type: BazaCrudItem.Checkbox,
                        options: {
                            label: this.i18n('withDetails'),
                            control: this.withDetails,
                        },
                    },
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.filterContext$.asObservable().pipe(
                            takeUntil(this.ngOnDestroy$),
                            switchMap((filterContext) => this.endpoint.list({ ...request, ...filterContext })),
                        ),
                },
                columns: this.crudColumns(columnsWithoutDetails),
            },
        };
    }

    crudColumns(
        withColumns: Array<keyof NcTransactionDto> = [
            'id',
            'ncOfferingName',
            'accountFullName',
            'createdAt',
            'ncOfferingId',
            'ncTradeId',
            'ncOrderStatus',
            'amountCents',
            'feeCents',
            'totalCents',
            'shares',
            'pricePerShareCents',
            'state',
        ],
    ): Array<BazaCrudDataColumn<NcTransactionDto>> {
        const columns: Array<BazaCrudDataColumn<NcTransactionDto>> = [
            {
                field: 'id',
                title: this.i18n('columns.id'),
                type: {
                    kind: BazaTableFieldType.Id,
                },
                sortable: true,
                width: 80,
            },
            {
                field: 'ncOfferingName',
                title: this.i18n('columns.ncOfferingName'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                ngStyle: () => ({
                    'font-weight': '500',
                }),
                sortable: true,
            },
            {
                field: 'accountFullName',
                title: this.i18n('columns.accountFullName'),
                source: (entity) => `${entity.accountFullName} (${entity.accountId})`,
                width: 240,
                type: {
                    kind: BazaTableFieldType.Text,
                },
            },
            {
                field: 'createdAt',
                title: this.i18n('columns.createdAt'),
                type: {
                    kind: BazaTableFieldType.DateTime,
                },
                width: 230,
                sortable: true,
            },
            {
                field: 'ncOfferingId',
                title: this.i18n('columns.ncOfferingId'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 150,
                sortable: true,
            },
            {
                field: 'ncTradeId',
                title: this.i18n('columns.ncTradeId'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 150,
                sortable: true,
            },
            {
                field: 'ncOrderStatus',
                title: this.i18n('columns.ncOrderStatus'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 150,
                sortable: true,
                ngStyle: (entity) => {
                    return {
                        color: (() => {
                            switch (entity.ncOrderStatus) {
                                default: {
                                    return 'var(--normal-color)';
                                }

                                case OrderStatus.Created:
                                case OrderStatus.Pending:
                                case OrderStatus.UnwindPending: {
                                    return 'var(--primary-color)';
                                }

                                case OrderStatus.Rejected: {
                                    return 'var(--white)';
                                }

                                case OrderStatus.Canceled: {
                                    return 'var(--warning-color)';
                                }

                                case OrderStatus.Funded:
                                case OrderStatus.Settled: {
                                    return 'var(--success-color)';
                                }
                            }
                        })(),
                        'background-color': (() => {
                            switch (entity.ncOrderStatus) {
                                default: {
                                    return 'none';
                                }

                                case OrderStatus.Rejected: {
                                    return 'var(--error-color)';
                                }
                            }
                        })(),
                    };
                },
            },
            {
                field: 'amountCents',
                title: this.i18n('columns.amountCents'),
                source: (entity) => `$${convertFromCents(entity.amountCents).toFixed(2)}`,
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 120,
                sortable: true,
            },
            {
                field: 'feeCents',
                title: this.i18n('columns.feeCents'),
                source: (entity) => `$${convertFromCents(entity.feeCents).toFixed(2)}`,
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 120,
                sortable: true,
            },
            {
                field: 'totalCents',
                title: this.i18n('columns.totalCents'),
                source: (entity) => `$${convertFromCents(entity.totalCents).toFixed(2)}`,
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 120,
                sortable: true,
            },
            {
                field: 'shares',
                title: this.i18n('columns.shares'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 100,
                sortable: true,
            },
            {
                field: 'pricePerShareCents',
                title: this.i18n('columns.pricePerShareCents'),
                source: (entity) => `$${convertFromCents(entity.pricePerShareCents).toFixed(2)}`,
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 120,
                sortable: true,
            },
            {
                field: 'state',
                title: this.i18n('columns.state'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 240,
                sortable: true,
                ngStyle: (entity) => {
                    return {
                        color: (() => {
                            switch (entity.state) {
                                default: {
                                    return 'var(--normal-color)';
                                }

                                case TransactionState.PendingPayment:
                                case TransactionState.UnwindPending: {
                                    return 'var(--primary-color)';
                                }

                                case TransactionState.PaymentRejected: {
                                    return 'var(--white)';
                                }

                                case TransactionState.PaymentCancelled: {
                                    return 'var(--error-color)';
                                }

                                case TransactionState.PaymentFunded:
                                case TransactionState.PaymentConfirmed: {
                                    return 'var(--success-color)';
                                }
                            }
                        })(),
                        'background-color': (() => {
                            switch (entity.state) {
                                default: {
                                    return 'none';
                                }

                                case TransactionState.PaymentRejected: {
                                    return 'var(--error-color)';
                                }
                            }
                        })(),
                    };
                },
            },
        ];

        return columns.filter((column) => withColumns.includes(column.field as keyof NcTransactionDto));
    }
}
