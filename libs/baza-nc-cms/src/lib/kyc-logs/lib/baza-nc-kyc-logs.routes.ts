import { Routes } from '@angular/router';
import { BazaNcKycLogsComponent } from './components/baza-nc-kyc-logs/baza-nc-kyc-logs.component';

export const bazaNcKycLogsRoutes: Routes = [
    {
        path: 'baza-nc/kyc-logs',
        component: BazaNcKycLogsComponent,
    },
];
