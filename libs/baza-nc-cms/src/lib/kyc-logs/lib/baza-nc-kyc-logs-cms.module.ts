import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaCmsLayoutModule, BazaCrudCmsModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsDataAccessModule } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNcKycLogsDetailsComponent } from './components/baza-nc-kyc-logs-details/baza-nc-kyc-logs-details.component';
import { BazaNcKycLogsDetailsService } from './components/baza-nc-kyc-logs-details/baza-nc-kyc-logs-details.service';
import { BazaNcKycLogsComponent } from './components/baza-nc-kyc-logs/baza-nc-kyc-logs.component';

@NgModule({
    imports: [CommonModule, BazaCrudCmsModule, BazaNcCmsDataAccessModule, BazaCmsLayoutModule],
    declarations: [BazaNcKycLogsComponent, BazaNcKycLogsDetailsComponent],
    providers: [BazaNcKycLogsDetailsService],
})
export class BazaNcKycLogsCmsModule {}
