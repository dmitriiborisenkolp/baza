import { ChangeDetectionStrategy, Component, ViewContainerRef } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaTableFieldAlign, BazaTableFieldType } from '@scaliolabs/baza-core-cms';
import { KycLogDto } from '@scaliolabs/baza-nc-shared';
import { BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { BazaNcKycLogsDetailsService } from '../baza-nc-kyc-logs-details/baza-nc-kyc-logs-details.service';
import { KYCStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';

interface State {
    config: BazaCrudConfig<KycLogDto>;
}

function kycAmlNgStyles(kyc: KYCStatus): Record<string, string> {
    switch (kyc) {
        default: {
            return {
                color: 'var(--primary-color)',
            };
        }

        case KYCStatus.Approved:
        case KYCStatus.AutoApproved: {
            return {
                color: 'var(--success-color)',
            };
        }

        case KYCStatus.NeedMoreInfo: {
            return {
                color: 'var(--warning-color)',
            };
        }

        case KYCStatus.Disapproved: {
            return {
                color: 'var(--error-color)',
            };
        }
    }
}

export const BAZA_NC_KYC_LOGS_CMS_ID = 'baza-nc-kyc-logs-cms-id';

@Component({
    templateUrl: './baza-nc-kyc-logs.component.html',
    styleUrls: ['./baza-nc-kyc-logs.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcKycLogsComponent {
    public crud: BazaCrudComponent<KycLogDto>;

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly vcr: ViewContainerRef,
        private readonly kycDetails: BazaNcKycLogsDetailsService,
        private readonly endpoint: BazaNcAccountVerificationCmsDataAccess,
    ) {}

    i18n(key: string): string {
        return `baza-nc.accountVerification.components.ncAccountVerificationKycLogsCms.${key}`;
    }

    private crudConfig(): BazaCrudConfig<KycLogDto> {
        return {
            id: BAZA_NC_KYC_LOGS_CMS_ID,
            title: this.i18n('title'),
            data: {
                dataSource: {
                    list: (request) => this.endpoint.kycLogs(request),
                },
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: true,
                    },
                    {
                        field: 'createdAt',
                        title: this.i18n('columns.createdAt'),
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        sortable: true,
                        width: 180,
                    },
                    {
                        field: 'userEmail',
                        title: this.i18n('columns.userEmail'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => `${entity.userEmail}`,
                        },
                        sortable: true,
                    },
                    {
                        field: 'ncAccountId',
                        title: this.i18n('columns.ncAccountId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                    },
                    {
                        field: 'ncPartyId',
                        title: this.i18n('columns.ncPartyId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 150,
                    },
                    {
                        field: 'previousKycStatus',
                        title: this.i18n('columns.previousKycStatus'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => (entity.previousKycStatus ? entity.previousKycStatus : '–'),
                        },
                        sortable: true,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 190,
                        ngStyle: (entity) => kycAmlNgStyles(entity.previousKycStatus),
                    },
                    {
                        field: 'newKycStatus',
                        title: this.i18n('columns.newKycStatus'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => (entity.newKycStatus ? entity.newKycStatus : '–'),
                        },
                        sortable: true,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 190,
                        ngStyle: (entity) => kycAmlNgStyles(entity.newKycStatus),
                    },
                    {
                        field: 'kycDetails',
                        title: '',
                        type: {
                            kind: BazaTableFieldType.Button,
                            translate: true,
                            buttonType: BazaNzButtonStyle.Dashed,
                            format: () => this.i18n('columns.kycDetails'),
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            onClick: (entity) => this.kycDetails.open(this.vcr, entity.kycDetails),
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 135,
                    },
                ],
            },
        };
    }
}
