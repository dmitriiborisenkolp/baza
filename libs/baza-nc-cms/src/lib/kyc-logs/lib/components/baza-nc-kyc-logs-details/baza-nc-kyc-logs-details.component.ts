import { ChangeDetectionStrategy, Component, Input, TemplateRef, ViewChild } from '@angular/core';

@Component({
    templateUrl: './baza-nc-kyc-logs-details.component.html',
    styleUrls: ['./baza-nc-kyc-logs-details.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcKycLogsDetailsComponent {
    @ViewChild('nzModal') nzModal: TemplateRef<any>;

    @Input() kycDetails: any;

    get json(): string {
        return JSON.stringify(this.kycDetails, null, 4);
    }
}
