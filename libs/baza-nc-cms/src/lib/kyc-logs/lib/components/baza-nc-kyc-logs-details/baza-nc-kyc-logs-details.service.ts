import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BazaNcKycLogsDetailsComponent } from './baza-nc-kyc-logs-details.component';

@Injectable()
export class BazaNcKycLogsDetailsService {
    constructor(
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    open(vcr: ViewContainerRef, kycDetails: any): void {
        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaNcKycLogsDetailsComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], vcr.injector);

        const component = factory.create(injector);

        component.instance.kycDetails = kycDetails;
        component.changeDetectorRef.markForCheck();

        vcr.insert(component.hostView);

        setTimeout(() => {
            this.nzModal.create({
                nzContent: component.instance.nzModal,
                nzClosable: true,
                nzFooter: null,
                nzWidth: 950,
            });
        });
    }
}
