import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaCmsLayoutModule, BazaCrudCmsModule } from '@scaliolabs/baza-core-cms';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BazaNcCmsDataAccessModule } from '@scaliolabs/baza-nc-cms-data-access';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { BazaNcDividendConfirmModalComponent } from './components/baza-nc-dividend-confirm-modal/baza-nc-dividend-confirm-modal.component';
import { BazaNcDividendConfirmModalService } from './components/baza-nc-dividend-confirm-modal/baza-nc-dividend-confirm-modal.service';
import { BazaNcDividendImportCsvModalComponent } from './components/baza-nc-dividend-import-csv-modal/baza-nc-dividend-import-csv-modal.component';
import { BazaNcDividendTransactionEntriesComponent } from './components/baza-nc-dividend-transaction-entries/baza-nc-dividend-transaction-entries.component';
import { BazaNcDividendTransactionEntriesResolve } from './components/baza-nc-dividend-transaction-entries/baza-nc-dividend-transaction-entries.resolve';
import { BazaNcDividendTransactionComponent } from './components/baza-nc-dividend-transaction/baza-nc-dividend-transaction.component';
import { BazaNcDividendComponent } from './components/baza-nc-dividend/baza-nc-dividend.component';
import { BazaNcDividendResolve } from './components/baza-nc-dividend/baza-nc-dividend.resolve';

@NgModule({
    imports: [
        CommonModule,
        BazaCrudCmsModule,
        BazaNcCmsDataAccessModule,
        BazaCmsLayoutModule,
        TranslateModule,
        NzTypographyModule,
        ReactiveFormsModule,
        NzInputModule,
        NzButtonModule,
        NzSpaceModule,
        NzTableModule,
        NzRadioModule,
    ],
    declarations: [
        BazaNcDividendComponent,
        BazaNcDividendTransactionComponent,
        BazaNcDividendTransactionEntriesComponent,
        BazaNcDividendConfirmModalComponent,
        BazaNcDividendImportCsvModalComponent,
    ],
    providers: [BazaNcDividendResolve, BazaNcDividendTransactionEntriesResolve, BazaNcDividendConfirmModalService],
})
export class BazaNcDividendCmsModule {}
