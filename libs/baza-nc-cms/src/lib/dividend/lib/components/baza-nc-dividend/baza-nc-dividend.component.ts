import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutModalService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    BazaNcDividendCmsCsvDto,
    BazaNcDividendCmsDto,
    BazaNcDividendPaymentSource,
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionDto,
    BazaNorthCapitalAcl,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendCmsDataAccess, BazaNcDividendTransactionCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNcDividendResolveData } from './baza-nc-dividend.resolve';
import { ActivatedRoute, Router } from '@angular/router';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { tap } from 'rxjs/operators';

export const BAZA_NC_DIVIDEND_CMS_ID = 'baza-nc-dividends-cms-id';
export const BAZA_NC_DIVIDEND_FORM_ID = 'baza-nc-dividends-form-id';

interface State {
    config: BazaCrudConfig<BazaNcDividendCmsDto>;
}

interface CreateDividendTransactionFormValue {
    title?: string;
}

@Component({
    templateUrl: './baza-nc-dividend.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcDividendComponent {
    public crud: BazaCrudComponent<BazaNcDividendCmsDto>;

    public state: State = {
        config: this.config,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly dataAccess: BazaNcDividendCmsDataAccess,
        private readonly dividendTransactionDataAccess: BazaNcDividendTransactionCmsDataAccess,
        private readonly activatedRoute: ActivatedRoute,
        private readonly modalLayout: BazaFormLayoutModalService,
        private readonly translate: TranslateService,
        private readonly ngConfirm: BazaNgConfirmService,
    ) {}

    i18n(key: string): string {
        return `baza-nc.dividend.components.bazaNcDividend.${key}`;
    }

    i18nSource(source: BazaNcDividendPaymentSource): string {
        return `baza-nc.dividend.types.BazaNcDividendPaymentSource.${source}`;
    }

    i18nStatus(status: BazaNcDividendPaymentStatus): string {
        return `baza-nc.dividend.types.BazaNcDividendPaymentStatus.${status}`;
    }

    cssColorStatus(status: BazaNcDividendPaymentStatus): string {
        switch (status) {
            default: {
                return `var(--black)`;
            }

            case BazaNcDividendPaymentStatus.Pending: {
                return `var(--processing-color)`;
            }

            case BazaNcDividendPaymentStatus.Failed: {
                return `var(--error-color)`;
            }

            case BazaNcDividendPaymentStatus.Processed: {
                return `var(--success-color)`;
            }

            case BazaNcDividendPaymentStatus.Cancelled: {
                return `var(--warning-color)`;
            }
        }
    }

    get resolvedData(): BazaNcDividendResolveData {
        return this.activatedRoute.snapshot.data['data'];
    }

    private get config(): BazaCrudConfig<BazaNcDividendCmsDto> {
        const baseConfig: BazaCrudConfig<BazaNcDividendCmsDto> = {
            id: BAZA_NC_DIVIDEND_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                    {
                        type: BazaCrudItem.DateRange,
                        options: {
                            nzMode: 'date',
                        },
                    },
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('items.exportToCsv.title'),
                            icon: 'download',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (request) =>
                                this.dataAccess.exportToCsv({
                                    ...request,
                                    fields: (
                                        [
                                            'ulid',
                                            'source',
                                            'date',
                                            'investorAccount',
                                            'ncAccountId',
                                            'ncPartyId',
                                            'amount',
                                            'status',
                                            'offering',
                                            'ncOfferingId',
                                        ] as Array<keyof BazaNcDividendCmsCsvDto>
                                    ).map((field) => ({
                                        field,
                                        title: this.translate.instant(this.i18n(`items.exportToCsv.fields.${field}`)),
                                    })),
                                }),
                            fileNameGenerator: () =>
                                this.translate.instant(this.i18n('items.exportToCsv.fileName'), {
                                    date: moment(new Date()).format('MM-DD-YYYY'),
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransaction],
                        options: {
                            type: BazaNzButtonStyle.Link,
                            title: this.i18n('items.navigateToDividendTransactions'),
                            icon: 'unordered-list',
                            callback: () => this.navigateToDividendTransactions(),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransaction],
                        options: {
                            type: BazaNzButtonStyle.Primary,
                            title: this.i18n('items.createDividendTransaction'),
                            icon: 'plus',
                            callback: () => this.createDividendTransaction(),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => {
                        if (this.resolvedData.type == 'all') {
                            return this.dataAccess.list(request);
                        } else if (this.resolvedData.type === 'investor-account') {
                            return this.dataAccess.list({
                                ...request,
                                investorAccountId: this.resolvedData.investorAccount?.id,
                            });
                        } else {
                            throw new Error('Unknown source');
                        }
                    },
                },
                columns: [
                    {
                        field: 'date',
                        title: this.i18n('columns.date'),
                        type: {
                            kind: BazaTableFieldType.DateUTC,
                        },
                        width: 180,
                    },
                    {
                        field: 'source',
                        title: this.i18n('columns.source'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nSource(entity.source),
                            translate: true,
                        },
                        width: 120,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'investorAccount',
                        title: this.i18n('columns.investorAccount'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) =>
                                `${entity.investorAccount.userFullName} (${entity.investorAccount.northCapitalAccountId || 'N/A'})`,
                            translate: false,
                        },
                        width: 280,
                    },
                    {
                        field: 'offeringId',
                        title: this.i18n('columns.offeringId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => entity.offeringId || 'N/A',
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'offeringTitle',
                        title: this.i18n('columns.offeringTitle'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => entity.offeringTitle || 'N/A',
                        },
                    },
                    {
                        field: 'amount',
                        title: this.i18n('columns.amount'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => `$${entity.amount}`,
                        },
                        width: 180,
                    },
                    {
                        field: 'status',
                        title: this.i18n('columns.status'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nStatus(entity.status),
                            translate: true,
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                        ngStyle: (entity) => ({
                            color: this.cssColorStatus(entity.status),
                        }),
                    },
                ],
                actions: [
                    {
                        icon: 'delete',
                        title: this.i18n('actions.delete.title'),
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        action: (entity) => this.delete(entity),
                    },
                ],
            },
        };

        if (this.resolvedData.type === 'investor-account') {
            baseConfig.backRouterLink = {
                routerLink: ['/baza-nc/investor-accounts'],
            };

            baseConfig.navigation = {
                nodes: [
                    {
                        title: this.i18n('navigation.allDividends'),
                        routerLink: {
                            routerLink: ['/baza-nc/dividends'],
                        },
                    },
                    {
                        title: this.i18n('navigation.investorAccount'),
                    },
                    {
                        title: this.i18n('navigation.accountDividends'),
                        translateArgs: this.resolvedData,
                    },
                ],
            };
        } else {
            baseConfig.backRouterLink = undefined;
            baseConfig.navigation = undefined;
        }

        return baseConfig;
    }

    navigateToDividendTransactions(): void {
        this.router.navigate(['/baza-nc/dividends/transactions']);
    }

    createDividendTransaction(): void {
        const date = moment().format('YYYY-MM-DD');

        const formGroup = this.fb.group({
            title: [],
        });

        const formBuilder: BazaFormBuilder = {
            id: BAZA_NC_DIVIDEND_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'title',
                        label: this.i18n('createDividendTransaction.fields.title'),
                    },
                ],
            },
        };

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.modalLayout.open<BazaNcDividendTransactionDto, CreateDividendTransactionFormValue>({
            formGroup,
            formBuilder,
            title: this.i18n('createDividendTransaction.title'),
            onSubmit: (entity, formValue) =>
                this.dividendTransactionDataAccess
                    .create({
                        title: formValue.title || this.translate.instant('baza-nc.dividend.defaultDividendTitle', { date }),
                    })
                    .pipe(
                        tap((response) => {
                            this.router.navigate(['/baza-nc/dividends/transactions', response.ulid]);
                        }),
                    ),
        });
    }

    refresh(): void {
        this.crud.refresh();
    }

    delete(entity: BazaNcDividendCmsDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.delete.confirm'),
            messageArgs: {
                amount: entity.amount,
                investor: entity.investorAccount.userFullName,
            },
            confirm: () =>
                this.dataAccess
                    .delete({
                        ulid: entity.ulid,
                    })
                    .pipe(tap(() => this.refresh()))
                    .toPromise(),
        });
    }
}
