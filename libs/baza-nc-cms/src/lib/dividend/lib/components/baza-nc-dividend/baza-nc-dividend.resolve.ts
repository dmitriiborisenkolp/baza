import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { BazaNcInvestorAccountDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { Observable, throwError } from 'rxjs';
import { catchError, map, retryWhen } from 'rxjs/operators';
import { BazaNgMessagesService, genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    type: 'all' | 'investor-account';
    investorAccount?: BazaNcInvestorAccountDto;
}

export { ResolveData as BazaNcDividendResolveData };

@Injectable()
export class BazaNcDividendResolve implements Resolve<ResolveData> {
    constructor(
        private readonly investorAccountDataAccess: BazaNcInvestorAccountCmsDataAccess,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly router: Router,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> | ResolveData {
        const investorAccountId = parseInt(route.params['investorAccountId'], 10);

        if (investorAccountId) {
            return this.investorAccountDataAccess
                .getInvestorAccount({
                    id: investorAccountId,
                })
                .pipe(
                    retryWhen(genericRetryStrategy()),
                    catchError((err) => {
                        this.ngMessages.error({
                            translate: true,
                            message: 'baza-nc.dividend.component.bazaNcDividend.resolve.errorInvestorAccount',
                        });

                        this.router.navigate(['/baza-nc/dividends']);

                        return throwError(() => err);
                    }),
                    map((investorAccount) => ({
                        type: 'investor-account',
                        investorAccount,
                    })),
                );
        } else {
            return {
                type: 'all',
            };
        }
    }
}
