import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { takeUntil } from 'rxjs/operators';
import { BazaNcDividendConfirmModalComponent } from './baza-nc-dividend-confirm-modal.component';
import { Subject } from 'rxjs';
import { BazaNcDividendTransactionStatus } from '@scaliolabs/baza-nc-shared';

interface PromiseResponse {
    success: boolean;
    newStatus?: BazaNcDividendTransactionStatus;
}

@Injectable()
export class BazaNcDividendConfirmModalService {
    constructor(private readonly nzModal: NzModalService, private readonly componentFactoryResolver: ComponentFactoryResolver) {}

    open(request: { token: string; vcr: ViewContainerRef }): Promise<PromiseResponse> {
        return new Promise((resolve) => {
            const factory = this.componentFactoryResolver.resolveComponentFactory(BazaNcDividendConfirmModalComponent);
            const injector = ReflectiveInjector.fromResolvedProviders([], request.vcr.injector);

            const component = factory.create(injector);

            request.vcr.insert(component.hostView);

            setTimeout(() => {
                const afterClosed$ = new Subject<void>();

                component.instance.token = request.token;

                component.instance.state.nzModalRef = this.nzModal.create({
                    nzTitle: null,
                    nzOkText: null,
                    nzContent: component.instance.nzModal,
                    nzClosable: false,
                    nzOnOk: () => component.instance.submit(),
                    nzWidth: 520,
                    nzFooter: null,
                    nzCloseOnNavigation: true,
                    nzMaskClosable: false,
                });

                component.instance.onSuccess.pipe(takeUntil(afterClosed$)).subscribe((newStatus) => {
                    resolve({
                        success: true,
                        newStatus,
                    });

                    afterClosed$.next();
                });

                component.instance.onClose.pipe(takeUntil(afterClosed$)).subscribe(() => {
                    resolve({
                        success: false,
                    });

                    afterClosed$.next();
                });
            });
        });
    }
}
