import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, Output, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaNcDividendTransactionCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { AuthValidatorsService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendErrorCodes, BazaNcDividendTransactionStatus } from '@scaliolabs/baza-nc-shared';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';

interface FormValue {
    password: string;
}

interface State {
    form: FormGroup;
    nzModalRef?: NzModalRef<BazaNcDividendConfirmModalComponent>;
}

@Component({
    templateUrl: './baza-nc-dividend-confirm-modal.component.html',
    styleUrls: ['./baza-nc-dividend-confirm-modal.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcDividendConfirmModalComponent implements OnDestroy {
    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<any>;

    @Input() token: string;

    @Output() onSuccess = new EventEmitter<BazaNcDividendTransactionStatus>();
    @Output() onClose = new EventEmitter<void>();

    private readonly ngOnDestroy$ = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            password: ['', [...this.authValidators.signUpValidators(), Validators.required]],
        }),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly dataAccess: BazaNcDividendTransactionCmsDataAccess,
        private readonly authValidators: AuthValidatorsService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza-nc.dividend.components.bazaNcDividendConfirmModal.${key}`;
    }

    submit(): void {
        if (!this.state.form.valid) {
            return;
        }

        const formValue: FormValue = this.state.form.value;

        this.state.form.disable();

        this.dataAccess
            .process({
                token: this.token,
                password: formValue.password,
                async: true,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                (response) => {
                    this.ngMessages.success({
                        message: this.i18n('success'),
                        translate: true,
                    });

                    this.onSuccess.next(response.newStatus);

                    this.close();
                },
                (err: BazaError) => {
                    if (isBazaErrorResponse(err)) {
                        if (err.code === BazaNcDividendErrorCodes.BazaNcDividendConfirmationNotFound) {
                            this.ngMessages.error({
                                message: this.i18n('errors.BazaNcDividendConfirmationNotFound'),
                                translate: true,
                            });

                            this.close();
                        } else if (err.code === BazaNcDividendErrorCodes.BazaNcDividendConfirmationInvalidPassword) {
                            this.ngMessages.error({
                                message: this.i18n('errors.BazaNcDividendConfirmationInvalidPassword'),
                                translate: true,
                            });
                        } else if (err.code === BazaNcDividendErrorCodes.BazaNcDividendConfirmationUnauthorized) {
                            this.ngMessages.error({
                                message: this.i18n('errors.BazaNcDividendConfirmationUnauthorized'),
                                translate: true,
                            });

                            this.close();
                        } else {
                            this.ngMessages.bazaError(err);
                        }
                    }

                    this.state.form.enable();
                },
            );
    }

    close(): void {
        this.onClose.next();

        this.state.nzModalRef?.close();
    }
}
