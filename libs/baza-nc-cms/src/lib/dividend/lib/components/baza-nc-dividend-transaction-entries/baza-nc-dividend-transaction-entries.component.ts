import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ComponentRef, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BazaNcDividendTransactionEntriesResolveData } from './baza-nc-dividend-transaction-entries.resolve';
import { BazaNcDividendTransactionCmsDataAccess, BazaNcDividendTransactionEntryCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormControlFieldSelect,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    BazaNcDividendErrorCodes,
    bazaNcDividendNonDraftTransactionStatuses,
    BazaNcDividendPaymentSource,
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionDto,
    BazaNcDividendTransactionEntryCsvDto,
    BazaNcDividendTransactionEntryDto,
    BazaNcDividendTransactionStatus,
    bazaNcDividendTransactionStatusesToProcess,
    BazaNcInvestorAccountDto,
    BazaNorthCapitalAcl,
    convertToCents,
    OfferingId,
} from '@scaliolabs/baza-nc-shared';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
    BazaNgConfirmService,
    BazaNgMessagesService,
    BazaNzButtonStyle,
    floatValidator,
    moreThanZeroValidator,
} from '@scaliolabs/baza-core-ng';
import { BazaNcCmsUi, BazaNcCmsUiFields } from '../../../../ui';
import { catchError, distinctUntilChanged, filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BehaviorSubject, interval, Observable, of, Subject } from 'rxjs';
import { BazaNcDividendConfirmModalService } from '../baza-nc-dividend-confirm-modal/baza-nc-dividend-confirm-modal.service';
import { bazaNcBundleCmsConfig } from '../../../../../config';
import { NzSelectComponent } from 'ng-zorro-antd/select';
import { BazaNcDividendImportCsvModalService } from '../baza-nc-dividend-import-csv-modal/baza-nc-dividend-import-csv-modal.service';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { bazaEmptyCrudListResponse, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { DwollaFailureCodes } from '@scaliolabs/baza-dwolla-shared';

export const BAZA_NC_DIVIDEND_TRANSACTION_ENTRIES_CMS_ID = 'baza-nc-dividend-transaction-entries-cms-id';
export const BAZA_NC_DIVIDEND_TRANSACTION_ENTRIES_CREATE_FORM_ID = 'baza-nc-dividend-transaction-entries-create-form-id';
export const BAZA_NC_DIVIDEND_TRANSACTION_ENTRIES_UPDATED_FORM_ID = 'baza-nc-dividend-transaction-entries-update-form-id';

interface State {
    config: BazaCrudConfig<BazaNcDividendTransactionEntryDto>;
    hasEntries: boolean;
}

interface FormValue {
    date: Date;
    source: BazaNcDividendPaymentSource;
    offeringId?: OfferingId;
    investorAccount: BazaNcInvestorAccountDto;
    amount: number;
}

type FormBuilderControls = BazaCoreFormBuilderComponents | BazaNcCmsUiFields;

const dividendSources = () => [
    {
        source: BazaNcDividendPaymentSource.NC,
        enabled: bazaNcBundleCmsConfig().withNcDividendsFeature,
    },
    {
        source: BazaNcDividendPaymentSource.Dwolla,
        enabled: bazaNcBundleCmsConfig().withDwollaDividendsFeature,
    },
];

const UPDATE_INTERVAL_MS = 1000;

@Component({
    templateUrl: './baza-nc-dividend-transaction-entries.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [BazaNcDividendImportCsvModalService],
})
export class BazaNcDividendTransactionEntriesComponent implements OnInit, OnDestroy {
    public crud: BazaCrudComponent<BazaNcDividendTransactionEntryDto>;

    private dividendTransaction$ = new BehaviorSubject<BazaNcDividendTransactionDto>(this.resolvedData.dividendTransaction);

    private readonly ngOnDestroy$ = new Subject<void>();
    private readonly locked$ = new Subject<void>();

    public state: State = {
        config: this.config,
        hasEntries: false,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly vcr: ViewContainerRef,
        private readonly router: Router,
        private readonly translate: TranslateService,
        private readonly dataAccess: BazaNcDividendTransactionEntryCmsDataAccess,
        private readonly dataAccessDividendTransaction: BazaNcDividendTransactionCmsDataAccess,
        private readonly activatedRoute: ActivatedRoute,
        private readonly formLayout: BazaFormLayoutService,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly confirmTokenService: BazaNcDividendConfirmModalService,
        private readonly importCsvModalService: BazaNcDividendImportCsvModalService,
    ) {}

    ngOnInit(): void {
        const checkForConfirmToken = () => {
            const token = this.activatedRoute.snapshot.queryParams['confirm'];

            if (token) {
                if (this.canProcess) {
                    // eslint-disable-next-line security/detect-non-literal-fs-filename
                    this.confirmTokenService
                        .open({
                            vcr: this.vcr,
                            token,
                        })
                        .then((response) => {
                            if (response.success && response.newStatus) {
                                this.resolvedData.dividendTransaction.status = response.newStatus;

                                this.dividendTransaction$.next(this.resolvedData.dividendTransaction);
                                this.cdr.markForCheck();
                                this.refresh();
                            }

                            this.router.navigate(['/baza-nc/dividends/transactions', this.resolvedData.dividendTransaction.ulid]);
                        });
                } else {
                    this.ngMessages.warning({
                        message: this.i18n('cannotBeProcessed'),
                        translate: true,
                    });
                }
            }
        };

        const checkIsTransactionLocked = () => {
            interval(UPDATE_INTERVAL_MS)
                .pipe(
                    switchMap(() =>
                        this.dataAccessDividendTransaction.getByUlid({
                            ulid: this.resolvedData.dividendTransaction.ulid,
                        }),
                    ),
                    filter((response) => {
                        return (
                            this.resolvedData.dividendTransaction.ulid !== response.ulid ||
                            this.resolvedData.dividendTransaction.status !== response.status ||
                            this.resolvedData.dividendTransaction.isLocked !== response.isLocked ||
                            this.resolvedData.dividendTransaction.isProcessing !== response.isProcessing
                        );
                    }),
                    tap((response) => {
                        this.resolvedData.dividendTransaction = response;

                        this.dividendTransaction$.next(response);

                        if (response.status === BazaNcDividendTransactionStatus.Successful) {
                            this.locked$.next();
                        }

                        this.refresh();
                        this.cdr.markForCheck();
                    }),
                    catchError((err: BazaError) => {
                        if (isBazaErrorResponse(err) && err.code === BazaNcDividendErrorCodes.BazaNcDividendTransactionNotFound) {
                            this.locked$.next();

                            return of(undefined);
                        } else {
                            throw err;
                        }
                    }),
                    takeUntil(this.locked$),
                )
                .subscribe();
        };

        checkForConfirmToken();
        checkIsTransactionLocked();
    }

    ngOnDestroy(): void {
        this.locked$.next();
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza-nc.dividend.components.bazaNcDividendTransactionEntries.${key}`;
    }

    i18nStatus(status: BazaNcDividendPaymentStatus): string {
        return `baza-nc.dividend.types.BazaNcDividendPaymentStatus.${status}`;
    }

    i18nSource(source: BazaNcDividendPaymentSource): string {
        return `baza-nc.dividend.types.BazaNcDividendPaymentSource.${source}`;
    }

    cssColorStatus(status: BazaNcDividendPaymentStatus): string {
        switch (status) {
            default: {
                return `var(--black)`;
            }

            case BazaNcDividendPaymentStatus.Pending: {
                return `var(--processing-color)`;
            }

            case BazaNcDividendPaymentStatus.Failed: {
                return `var(--error-color)`;
            }

            case BazaNcDividendPaymentStatus.Processed: {
                return `var(--success-color)`;
            }

            case BazaNcDividendPaymentStatus.Cancelled: {
                return `var(--warning-color)`;
            }
        }
    }

    get resolvedData(): BazaNcDividendTransactionEntriesResolveData {
        return this.activatedRoute.snapshot.data['data'];
    }

    get config(): BazaCrudConfig<BazaNcDividendTransactionEntryDto> {
        return {
            id: BAZA_NC_DIVIDEND_TRANSACTION_ENTRIES_CMS_ID,
            title: this.i18n('title'),
            titleArguments: {
                title: this.resolvedData.dividendTransaction.title,
            },
            backRouterLink: {
                routerLink: ['/baza-nc/dividends/transactions'],
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('navigation.allDividends'),
                        routerLink: {
                            routerLink: ['/baza-nc/dividends'],
                        },
                    },
                    {
                        title: this.i18n('navigation.dividendTransactions'),
                        routerLink: {
                            routerLink: ['/baza-nc/dividends/transactions'],
                        },
                    },
                    {
                        title: this.i18n('navigation.dividendTransactionEntries'),
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        options: {
                            icon: 'plus',
                            title: this.i18n('actions.create'),
                            type: BazaNzButtonStyle.Default,
                            callback: () => this.create(),
                        },
                        visible: this.dividendTransaction$.asObservable().pipe(
                            map((next) => next.status),
                            distinctUntilChanged(),
                            map((next) => !bazaNcDividendNonDraftTransactionStatuses.includes(next)),
                        ),
                    },
                    {
                        type: BazaCrudItem.Button,
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        options: {
                            icon: 'caret-right',
                            type: BazaNzButtonStyle.Primary,
                            title: this.i18n('actions.process.title'),
                            callback: () => this.sendProcessVerificationEmail(),
                            disabled: this.dividendTransaction$
                                .asObservable()
                                .pipe(map((next) => next.isProcessing || !this.state.hasEntries)),
                        },
                        visible: this.dividendTransaction$.asObservable().pipe(map((next) => !next.isLocked)),
                    },
                ],
                toolbarLeft: [
                    {
                        type: BazaCrudItem.DateRange,
                        options: {
                            nzMode: 'date',
                        },
                    },
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                ],
                toolbarRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.refresh'),
                            icon: 'sync',
                            callback: () => this.crud.refresh(),
                            type: BazaNzButtonStyle.Link,
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        options: {
                            title: this.i18n('actions.sync.title'),
                            icon: 'import',
                            callback: () => this.sync(),
                            type: BazaNzButtonStyle.Link,
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        options: {
                            icon: 'upload',
                            title: this.i18n('actions.importCsv'),
                            type: BazaNzButtonStyle.Link,
                            callback: () => this.importCsv(),
                        },
                        visible: this.dividendTransaction$.asObservable().pipe(
                            map((next) => next.status),
                            distinctUntilChanged(),
                            map((next) => !bazaNcDividendNonDraftTransactionStatuses.includes(next)),
                        ),
                    },
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('actions.exportToCsv.title'),
                            icon: 'download',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (request) =>
                                this.dataAccess.exportToCsv({
                                    ...request,
                                    listRequest: {
                                        ...request.listRequest,
                                        dividendTransactionUlid: this.dividendTransaction$.getValue().ulid,
                                    },
                                    fields: (
                                        [
                                            'ulid',
                                            'date',
                                            'dateCreatedAt',
                                            'dateUpdatedAt',
                                            'dateProcessedAt',
                                            'offering',
                                            'ncOfferingId',
                                            'source',
                                            'investorAccount',
                                            'ncAccountId',
                                            'ncPartyId',
                                            'amount',
                                            'status',
                                        ] as Array<keyof BazaNcDividendTransactionEntryCsvDto>
                                    ).map((field) => ({
                                        field,
                                        title: this.translate.instant(this.i18n(`actions.exportToCsv.fields.${field}`)),
                                    })),
                                }),
                            fileNameGenerator: () =>
                                this.translate.instant(this.i18n('actions.exportToCsv.fileName'), {
                                    date: moment(new Date()).format('MM-DD-YYYY'),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccess
                            .list({
                                ...request,
                                dividendTransactionUlid: this.resolvedData.dividendTransaction.ulid,
                            })
                            .pipe(
                                catchError((err: BazaError) => {
                                    if (
                                        isBazaErrorResponse(err) &&
                                        err.code === BazaNcDividendErrorCodes.BazaNcDividendTransactionNotFound
                                    ) {
                                        this.router.navigate(['/baza-nc/dividends/transactions']);

                                        return of(bazaEmptyCrudListResponse());
                                    } else {
                                        throw err;
                                    }
                                }),
                                tap((response) => {
                                    this.state.hasEntries = response.items.length > 0;
                                }),
                                tap(() => this.dividendTransaction$.next(this.dividendTransaction$.getValue())),
                                tap(() => this.cdr.markForCheck()),
                            ),
                },
                columns: [
                    {
                        field: 'date',
                        title: this.i18n('columns.date'),
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        width: 180,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'investorAccount',
                        title: this.i18n('columns.investorAccount'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => `${entity.investorAccount.userFullName} (${entity.investorAccount.northCapitalAccountId})`,
                            translate: false,
                        },
                        width: 280,
                    },
                    {
                        field: 'offeringId',
                        title: this.i18n('columns.offeringId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => entity.offeringId || 'N/A',
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'offeringTitle',
                        title: this.i18n('columns.offeringTitle'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => entity.offeringTitle || 'N/A',
                        },
                    },
                    {
                        field: 'amount',
                        title: this.i18n('columns.amount'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => `$${entity.amount}`,
                        },
                        width: 180,
                    },
                    {
                        field: 'dateCreatedAt',
                        title: this.i18n('columns.dateCreatedAt'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        width: 220,
                    },
                    {
                        field: 'dateProcessedAt',
                        title: this.i18n('columns.dateProcessedAt'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        width: 220,
                    },
                    {
                        field: 'source',
                        title: this.i18n('columns.source'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nSource(entity.source),
                            translate: true,
                        },
                        width: 150,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'status',
                        title: this.i18n('columns.status'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nStatus(entity.status),
                            translate: true,
                            hint: (entity) => {
                                if (entity.status === BazaNcDividendPaymentStatus.Failed && entity.failureReason) {
                                    const isDwollaErrorCode = Object.values(DwollaFailureCodes).includes(
                                        entity.failureReason.code as DwollaFailureCodes,
                                    );

                                    const text = isDwollaErrorCode
                                        ? `${entity.failureReason.code} ${entity.failureReason.reason}`
                                        : entity.failureReason.reason;

                                    return {
                                        text,
                                        translate: false,
                                        icon: 'warning',
                                    };
                                }

                                return undefined;
                            },
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                        ngStyle: (entity) => ({
                            color: this.cssColorStatus(entity.status),
                        }),
                    },
                ],
                actions: [
                    {
                        icon: 'edit',
                        title: this.i18n('actions.update'),
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        action: (entity) => this.edit(entity),
                        visible: (entity) => entity.canBeUpdated,
                    },
                    {
                        icon: 'sync',
                        title: this.i18n('actions.reprocess.title'),
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        action: (entity) => this.reprocess(entity),
                        visible: (entity) => entity.canBeReprocessed,
                    },
                    {
                        icon: 'delete',
                        title: this.i18n('actions.delete.title'),
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        action: (entity) => this.delete(entity),
                        visible: (entity) => entity.canBeDeleted,
                    },
                ],
            },
        };
    }

    get canProcess(): boolean {
        return bazaNcDividendTransactionStatusesToProcess.includes(this.resolvedData.dividendTransaction.status);
    }

    formGroup(): FormGroup {
        return this.fb.group({
            date: [new Date(), [Validators.required]],
            source: [BazaNcDividendPaymentSource.NC, [Validators.required]],
            amount: ['0.00', [Validators.required, moreThanZeroValidator, floatValidator]],
            investorAccount: [undefined, [Validators.required]],
            offeringId: [undefined],
        });
    }

    refresh(): void {
        this.crud.refresh();
    }

    create(): void {
        const formGroup = this.formGroup();

        const formBuilder: BazaFormBuilder<FormBuilderControls> = {
            id: BAZA_NC_DIVIDEND_TRANSACTION_ENTRIES_CREATE_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        formControlName: 'date',
                        type: BazaFormBuilderControlType.Date,
                        label: this.i18n('form.fields.date'),
                    },
                    {
                        formControlName: 'source',
                        type: BazaFormBuilderControlType.Select,
                        label: this.i18n('form.fields.source'),
                        values: dividendSources()
                            .filter((next) => next.enabled)
                            .map((next) => next.source)
                            .map((value) => ({
                                value,
                                label: this.i18nSource(value),
                                translate: true,
                            })),
                    },
                    {
                        formControlName: 'investorAccount',
                        type: BazaNcCmsUi.BazaNcInvestorAccountField,
                        label: this.i18n('form.fields.investorAccount'),
                    },
                    {
                        type: BazaFormBuilderControlType.Select,
                        formControlName: 'offeringId',
                        label: this.i18n('form.fields.offering'),
                        values: this.resolvedData.offerings.map((next) => ({
                            value: next.ncOfferingId,
                            label: next.ncOfferingName,
                            translate: false,
                        })),
                        props: {
                            nzFilterOption: () => true,
                            nzShowSearch: true,
                            nzServerSearch: true,
                            nzAllowClear: true,
                        },
                        afterViewInit: (componentRef: ComponentRef<NzSelectComponent>, portal) => {
                            componentRef.instance.nzOnSearch.pipe().subscribe((next) => {
                                const definition = portal.definition as BazaFormControlFieldSelect;

                                definition.values = this.resolvedData.offerings
                                    .filter((offering) => offering.ncOfferingName.toLowerCase().includes(next.toLowerCase()))
                                    .map((offering) => ({
                                        value: offering.ncOfferingId,
                                        label: offering.ncOfferingName,
                                        translate: false,
                                    }));

                                portal.updateProps();
                            });
                        },
                    },
                    {
                        formControlName: 'amount',
                        type: BazaFormBuilderControlType.Number,
                        label: this.i18n('form.fields.amount'),
                        props: {
                            nzMin: 0,
                            nzStep: 0.01,
                            nzFormatter: (input: number) => (input || input === 0 ? `$${input}` : ''),
                            nzParser: (input: string) => (input ? input.replace('$', '') : ''),
                            nzPrecision: 2,
                        },
                    },
                ],
            },
        };

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaNcDividendTransactionEntryDto, FormValue>({
            formGroup,
            formBuilder,
            title: this.i18n('form.titleNew'),
            onSubmit: (entity, formValue) =>
                this.dataAccess
                    .create({
                        dividendTransactionUlid: this.resolvedData.dividendTransaction.ulid,
                        source: formValue.source,
                        date: formValue.date.toISOString(),
                        amountCents: convertToCents(formValue.amount),
                        investorAccountId: formValue.investorAccount.id,
                        offeringId: formValue.offeringId,
                    })
                    .pipe(tap(() => this.refresh())),
        });
    }

    edit(entity: BazaNcDividendTransactionEntryDto): void {
        const formGroup = this.formGroup();

        const formBuilder: BazaFormBuilder<FormBuilderControls> = {
            id: BAZA_NC_DIVIDEND_TRANSACTION_ENTRIES_UPDATED_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        formControlName: 'date',
                        type: BazaFormBuilderControlType.Date,
                        label: this.i18n('form.fields.date'),
                    },
                    {
                        formControlName: 'source',
                        type: BazaFormBuilderControlType.Select,
                        label: this.i18n('form.fields.source'),
                        values: Object.values(BazaNcDividendPaymentSource).map((value) => ({
                            value,
                            label: this.i18nSource(value),
                            translate: true,
                        })),
                        disabled: true,
                    },
                    {
                        formControlName: 'investorAccount',
                        type: BazaNcCmsUi.BazaNcInvestorAccountField,
                        label: this.i18n('form.fields.investorAccount'),
                        disabled: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Select,
                        formControlName: 'offeringId',
                        label: this.i18n('form.fields.offering'),
                        values: this.resolvedData.offerings.map((next) => ({
                            value: next.ncOfferingId,
                            label: next.ncOfferingName,
                            translate: false,
                        })),
                        props: {
                            nzFilterOption: () => true,
                            nzShowSearch: true,
                            nzServerSearch: true,
                            nzAllowClear: true,
                        },
                        afterViewInit: (componentRef: ComponentRef<NzSelectComponent>, portal) => {
                            componentRef.instance.nzOnSearch.pipe().subscribe((next) => {
                                const definition = portal.definition as BazaFormControlFieldSelect;

                                definition.values = this.resolvedData.offerings
                                    .filter((offering) => offering.ncOfferingName.toLowerCase().includes(next.toLowerCase()))
                                    .map((offering) => ({
                                        value: offering.ncOfferingId,
                                        label: offering.ncOfferingName,
                                        translate: false,
                                    }));

                                portal.updateProps();
                            });
                        },
                    },
                    {
                        formControlName: 'amount',
                        type: BazaFormBuilderControlType.Number,
                        label: this.i18n('form.fields.amount'),
                        props: {
                            nzMin: 0,
                            nzStep: 0.01,
                            nzFormatter: (input: number) => (input || input === 0 ? `$${input}` : ''),
                            nzParser: (input: string) => (input ? input.replace('$', '') : ''),
                            nzPrecision: 2,
                        },
                    },
                ],
            },
        };

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaNcDividendTransactionEntryDto, FormValue>({
            formGroup,
            formBuilder,
            title: this.i18n('form.titleExisting'),
            fetch: () =>
                this.dataAccess.getByUlid({
                    ulid: entity.ulid,
                }),
            populate: (response, formGroup) => {
                formGroup.patchValue({
                    ...response,
                    date: new Date(response.date),
                });
            },
            onSubmit: (entity, formValue) =>
                this.dataAccess
                    .update({
                        date: formValue.date.toISOString(),
                        amountCents: convertToCents(formValue.amount),
                        ulid: entity.ulid,
                        offeringId: formValue.offeringId,
                    })
                    .pipe(tap(() => this.refresh())),
        });
    }

    delete(entity: BazaNcDividendTransactionEntryDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.delete.confirm'),
            messageArgs: {
                userFullName: entity.investorAccount.userFullName,
                amount: entity.amount,
            },
            confirm: () =>
                this.pipeAction(
                    this.dataAccess.delete({
                        ulid: entity.ulid,
                    }),
                ).toPromise(),
        });
    }

    reprocess(entity: BazaNcDividendTransactionEntryDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.reprocess.confirm'),
            messageArgs: {
                userFullName: entity.investorAccount.userFullName,
                amount: entity.amount,
            },
            confirm: () =>
                this.pipeAction(
                    this.dataAccessDividendTransaction
                        .reprocess({
                            ulid: this.resolvedData.dividendTransaction.ulid,
                            entryULIDs: [entity.ulid],
                        })
                        .pipe(
                            tap((response) => {
                                const result = response.entries[0];

                                if (result.status === BazaNcDividendPaymentStatus.Failed) {
                                    this.ngMessages.error({
                                        message: result.failureReason.reason,
                                    });
                                } else {
                                    this.ngMessages.success({
                                        message: this.i18n('actions.reprocess.success'),
                                        translateParams: {
                                            userFullName: entity.investorAccount.userFullName,
                                            amount: entity.amount,
                                        },
                                        translate: true,
                                    });
                                }
                            }),
                        ),
                ).toPromise(),
        });
    }

    sync(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.sync.confirm'),
            confirm: () =>
                this.pipeAction(
                    this.dataAccessDividendTransaction.sync({
                        ulid: this.resolvedData.dividendTransaction.ulid,
                    }),
                )
                    .pipe(
                        tap(() => {
                            this.ngMessages.success({
                                message: this.i18n('actions.sync.success'),
                                translate: true,
                            });
                        }),
                    )
                    .toPromise(),
        });
    }

    sendProcessVerificationEmail(): void {
        this.pipeAction(
            this.dataAccessDividendTransaction.sendProcessConfirmation({
                ulid: this.resolvedData.dividendTransaction.ulid,
            }),
        ).subscribe(() => {
            this.ngMessages.success({
                message: this.i18n('actions.process.success'),
                translate: true,
            });
        });
    }

    private pipeAction(input: Observable<unknown>): Observable<unknown> {
        return input.pipe(
            switchMap(() =>
                this.dataAccessDividendTransaction.getByUlid({
                    ulid: this.resolvedData.dividendTransaction.ulid,
                }),
            ),
            catchError((err: BazaError) => {
                if (isBazaErrorResponse(err) && err.code === BazaNcDividendErrorCodes.BazaNcDividendTransactionNotFound) {
                    this.router.navigate(['/baza-nc/dividends/transactions']);

                    return of(undefined);
                } else {
                    this.ngMessages.bazaError(err);

                    throw err;
                }
            }),
            tap(() => this.refresh()),
            tap((response) => this.dividendTransaction$.next(response)),
            tap((response) => (this.resolvedData.dividendTransaction = response)),
            tap(() => this.cdr.markForCheck()),
            takeUntil(this.ngOnDestroy$),
        );
    }

    importCsv(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.importCsvModalService
            .open({
                vcr: this.vcr,
                dividendTransactionUlid: this.dividendTransaction$.getValue().ulid,
            })
            .then(() => this.refresh());
    }
}
