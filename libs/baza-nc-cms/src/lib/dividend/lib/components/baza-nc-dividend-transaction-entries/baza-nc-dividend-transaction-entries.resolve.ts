import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { BazaNcDividendTransactionDto, BazaNcOfferingListItemDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendTransactionCmsDataAccess, BazaNcOfferingCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNgMessagesService, genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { catchError, map, retryWhen, switchMap } from 'rxjs/operators';
import { bazaNcBundleCmsConfig } from '../../../../../config';

interface ResolveData {
    offerings: Array<BazaNcOfferingListItemDto>;
    dividendTransaction: BazaNcDividendTransactionDto;
}

export { ResolveData as BazaNcDividendTransactionEntriesResolveData };

@Injectable()
export class BazaNcDividendTransactionEntriesResolve implements Resolve<ResolveData> {
    constructor(
        private readonly dataAccess: BazaNcDividendTransactionCmsDataAccess,
        private readonly dataAccessOfferings: BazaNcOfferingCmsDataAccess,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly router: Router,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> | ResolveData {
        const ulid = route.params['ulid'];

        if (!ulid) {
            this.ngMessages.error({
                message: 'baza-nc.dividend.components.bazaNcDividendTransactionEntries.resolve.errDividendTransactionNotFound',
                translate: true,
            });

            this.router.navigate(['/baza-nc/dividend/transactions']);

            throw new Error('Dividend Transaction not found');
        }

        const observables: [Observable<BazaNcDividendTransactionDto>, Observable<Array<BazaNcOfferingListItemDto>>] = [
            this.dataAccess
                .getByUlid({
                    ulid,
                })
                .pipe(
                    retryWhen(genericRetryStrategy()),
                    catchError((err) => {
                        this.ngMessages.error({
                            message: 'baza-nc.dividend.components.bazaNcDividendTransactionEntries.resolve.errDividendTransactionNotFound',
                            translate: true,
                        });

                        this.router.navigate(['/baza-nc/dividend/transactions']);

                        return throwError(err);
                    }),
                ),
            this.dataAccessOfferings.listAll().pipe(
                switchMap((offerings) => {
                    const filterCallback = bazaNcBundleCmsConfig().filterOfferingsForDividends;

                    return filterCallback ? filterCallback(offerings) : of(offerings);
                }),
            ),
        ];

        return forkJoin(observables).pipe(
            map(([dividendTransaction, offerings]) => ({
                dividendTransaction,
                offerings,
            })),
        );
    }
}
