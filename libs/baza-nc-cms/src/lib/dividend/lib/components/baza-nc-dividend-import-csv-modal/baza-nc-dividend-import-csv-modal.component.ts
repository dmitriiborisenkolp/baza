import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    TemplateRef,
    ViewChild,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { BazaNcDividendConfirmModalComponent } from '../baza-nc-dividend-confirm-modal/baza-nc-dividend-confirm-modal.component';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { BazaNcDividendTransactionEntryCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import {
    BazaNcDividendImportCsvEntryError,
    BazaNcDividendImportCsvEntryErrorDto,
    BazaNcDividendPaymentSource,
} from '@scaliolabs/baza-nc-shared';
import { filter, finalize, take, takeUntil } from 'rxjs/operators';
import { NavigationStart, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BAZA_CRUD_CONSTANTS } from '@scaliolabs/baza-core-shared';
import { bazaNcBundleCmsConfig } from '../../../../../config';

enum ModalScreen {
    SelectFile = 'select-file',
    Errors = 'errors',
}

interface State {
    screen: ModalScreen;
    isDragOver: boolean;
    errors?: Array<BazaNcDividendImportCsvEntryErrorDto>;
    nzModalRef?: NzModalRef<BazaNcDividendConfirmModalComponent>;
    form: FormGroup;
}

interface FormValue {
    source: BazaNcDividendPaymentSource;
    delimiter: string;
}

type DividendTransactionUlid = string;

@Component({
    templateUrl: './baza-nc-dividend-import-csv-modal.component.html',
    styleUrls: ['./baza-nc-dividend-import-csv-modal.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [BazaLoadingService],
})
export class BazaNcDividendImportCsvModalComponent implements OnInit, OnDestroy {
    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<any>;
    @ViewChild('fileInput', { static: true }) fileInputRef: ElementRef<HTMLInputElement>;

    @Input() dividendTransactionUlid: string;

    @Output() onSuccess = new EventEmitter<DividendTransactionUlid>();
    @Output() onClose = new EventEmitter<void>();

    private readonly ngOnDestroy$ = new Subject<void>();

    public state: State = {
        screen: ModalScreen.SelectFile,
        isDragOver: false,
        form: this.fb.group({
            source: [
                (() => {
                    if (bazaNcBundleCmsConfig().withNcDividendsFeature && !bazaNcBundleCmsConfig().withDwollaDividendsFeature) {
                        return BazaNcDividendPaymentSource.NC;
                    } else {
                        return BazaNcDividendPaymentSource.Dwolla;
                    }
                })(),
                [Validators.required],
            ],
            delimiter: [BAZA_CRUD_CONSTANTS.csvDefaultDelimiter, [Validators.required]],
        }),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly router: Router,
        private readonly loading: BazaLoadingService,
        private readonly dataAccess: BazaNcDividendTransactionEntryCmsDataAccess,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnInit(): void {
        this.router.events
            .pipe(
                filter((event) => event instanceof NavigationStart),
                take(1),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe(() => {
                if (this.state.nzModalRef) {
                    this.state.nzModalRef.close();
                }
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza-nc.dividend.components.bazaNcDividendImportCsvModal.${key}`;
    }

    i18nErrorType(type: BazaNcDividendImportCsvEntryError): string {
        return `baza-nc.dividend.types.BazaNcDividendImportCsvEntryError.enum.${type}`;
    }

    i18nErrorDetails(type: BazaNcDividendImportCsvEntryError): string {
        return `baza-nc.dividend.types.BazaNcDividendImportCsvEntryError.details.${type}`;
    }

    get isLoading$(): Observable<boolean> {
        return this.loading.isLoading$;
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }
    get hasDwollaDividendsFeature(): boolean {
        return bazaNcBundleCmsConfig().withDwollaDividendsFeature;
    }

    get hasNcDividendsFeature(): boolean {
        return bazaNcBundleCmsConfig().withNcDividendsFeature;
    }

    close(): void {
        this.state.nzModalRef?.close();
        this.onClose.next();
    }

    onFileInputChange($event: Event): void {
        const target = $event.target as HTMLInputElement;

        if (!target.files.length) {
            return;
        }

        this.upload(target.files.item(0));
    }

    onDragEnter($event: DragEvent): void {
        $event.preventDefault();

        this.state = {
            ...this.state,
            isDragOver: true,
        };

        this.cdr.markForCheck();
    }

    onDragOver($event: DragEvent): void {
        $event.preventDefault();

        this.state = {
            ...this.state,
            isDragOver: true,
        };

        this.cdr.markForCheck();
    }

    onDragLeave($event: any): void {
        $event.preventDefault();

        this.state = {
            ...this.state,
            isDragOver: false,
        };

        this.cdr.markForCheck();
    }

    onDrag($event: DragEvent): void {
        $event.preventDefault();

        this.state = {
            ...this.state,
            isDragOver: false,
        };

        const files = $event.dataTransfer.files;

        if (!files.length) {
            return;
        }

        this.upload(files.item(0));

        this.cdr.markForCheck();
    }

    upload(file: File): void {
        const loading = this.loading.addLoading();
        const formValue = this.formValue;

        this.state.form.disable();

        this.dataAccess
            .importCsv(file, {
                source: formValue.source,
                csvDelimiter: formValue.delimiter,
                dividendTransactionUlid: this.dividendTransactionUlid,
            })
            .pipe(
                finalize(() => {
                    if (this.fileInputRef) {
                        this.fileInputRef.nativeElement.value = null;
                    }
                }),
                finalize(() => loading.complete()),
                finalize(() => this.cdr.markForCheck()),
            )
            .subscribe(
                (response) => {
                    if (response.success) {
                        this.onSuccess.next(this.dividendTransactionUlid);
                        this.state.nzModalRef?.close();

                        this.ngMessages.success({
                            message: this.i18n('success'),
                            translate: true,
                        });
                    } else {
                        this.state = {
                            ...this.state,
                            errors: response.errors,
                            screen: ModalScreen.Errors,
                        };
                    }
                },
                (err) => {
                    this.ngMessages.bazaError(err);

                    this.state.form.enable();
                },
            );
    }
}
