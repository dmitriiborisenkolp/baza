import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BazaNcDividendImportCsvModalComponent } from './baza-nc-dividend-import-csv-modal.component';

interface PromiseResponse {
    success: boolean;
    dividendTransactionUlid?: string;
}

@Injectable()
export class BazaNcDividendImportCsvModalService {
    constructor(private readonly nzModal: NzModalService, private readonly componentFactoryResolver: ComponentFactoryResolver) {}

    open(request: { dividendTransactionUlid: string; vcr: ViewContainerRef }): Promise<PromiseResponse> {
        return new Promise((resolve) => {
            const factory = this.componentFactoryResolver.resolveComponentFactory(BazaNcDividendImportCsvModalComponent);
            const injector = ReflectiveInjector.fromResolvedProviders([], request.vcr.injector);

            const component = factory.create(injector);

            request.vcr.insert(component.hostView);

            setTimeout(() => {
                const afterClosed$ = new Subject<void>();

                component.instance.dividendTransactionUlid = request.dividendTransactionUlid;

                component.instance.state.nzModalRef = this.nzModal.create({
                    nzTitle: null,
                    nzOkText: null,
                    nzContent: component.instance.nzModal,
                    nzClosable: false,
                    nzWidth: 720,
                    nzFooter: null,
                    nzCloseOnNavigation: true,
                    nzMaskClosable: false,
                });

                component.instance.onSuccess.pipe(takeUntil(afterClosed$)).subscribe((dividendTransactionUlid) => {
                    resolve({
                        success: true,
                        dividendTransactionUlid,
                    });

                    afterClosed$.next();
                });

                component.instance.onClose.pipe(takeUntil(afterClosed$)).subscribe(() => {
                    resolve({
                        success: false,
                    });

                    afterClosed$.next();
                });
            });
        });
    }
}
