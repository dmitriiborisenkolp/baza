import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutModalService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    BazaNcDividendTransactionCsvDto,
    BazaNcDividendTransactionDto,
    BazaNcDividendTransactionStatus,
    BazaNorthCapitalAcl,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendTransactionCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNgConfirmService, BazaNgMessagesService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { tap } from 'rxjs/operators';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

export const BAZA_NC_DIVIDEND_TRANSACTION_CMS_ID = 'baza-nc-dividend-transactions-cms-id';
export const BAZA_NC_DIVIDEND_TRANSACTION_FORM_ID = 'baza-nc-dividend-transactions-form-id';

interface State {
    config: BazaCrudConfig<BazaNcDividendTransactionDto>;
}

interface CreateDividendTransactionFormValue {
    title?: string;
}

@Component({
    templateUrl: './baza-nc-dividend-transaction.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcDividendTransactionComponent {
    public crud: BazaCrudComponent<BazaNcDividendTransactionDto>;

    public state: State = {
        config: this.config,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly dataAccess: BazaNcDividendTransactionCmsDataAccess,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly translate: TranslateService,
        private readonly modalLayout: BazaFormLayoutModalService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    i18n(key: string): string {
        return `baza-nc.dividend.components.bazaNcDividendTransaction.${key}`;
    }

    i18nStatus(status: BazaNcDividendTransactionStatus): string {
        return `baza-nc.dividend.types.BazaNcDividendTransactionStatus.${status}`;
    }

    cssColorStatus(status: BazaNcDividendTransactionStatus): string {
        switch (status) {
            default: {
                return `var(--black)`;
            }

            case BazaNcDividendTransactionStatus.Draft: {
                return `var(--normal-color)`;
            }

            case BazaNcDividendTransactionStatus.Started: {
                return `var(--
                ing-color)`;
            }

            case BazaNcDividendTransactionStatus.InProgress: {
                return `var(--processing-color)`;
            }

            case BazaNcDividendTransactionStatus.Failed: {
                return `var(--error-color)`;
            }

            case BazaNcDividendTransactionStatus.Cancelled: {
                return `var(--warning-color)`;
            }

            case BazaNcDividendTransactionStatus.Successful: {
                return `var(--success-color)`;
            }
        }
    }

    get config(): BazaCrudConfig<BazaNcDividendTransactionDto> {
        return {
            id: BAZA_NC_DIVIDEND_TRANSACTION_CMS_ID,
            title: this.i18n('title'),
            navigation: {
                nodes: [
                    {
                        title: this.i18n('navigation.allDividends'),
                        routerLink: {
                            routerLink: ['/baza-nc/dividends'],
                        },
                    },
                    {
                        title: this.i18n('navigation.dividendTransactions'),
                    },
                ],
            },
            backRouterLink: {
                routerLink: ['/baza-nc/dividends'],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                    {
                        type: BazaCrudItem.DateRange,
                        options: {
                            nzMode: 'date',
                        },
                    },
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('items.exportToCsv.title'),
                            icon: 'download',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (request) =>
                                this.dataAccess.exportToCsv({
                                    ...request,
                                    fields: (
                                        ['ulid', 'dateCreatedAt', 'dateUpdatedAt', 'dateProcessedAt', 'status', 'title'] as Array<
                                            keyof BazaNcDividendTransactionCsvDto
                                        >
                                    ).map((field) => ({
                                        field,
                                        title: this.translate.instant(this.i18n(`items.exportToCsv.fields.${field}`)),
                                    })),
                                }),
                            fileNameGenerator: () =>
                                this.translate.instant(this.i18n('items.exportToCsv.fileName'), {
                                    date: moment(new Date()).format('MM-DD-YYYY'),
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransaction],
                        options: {
                            type: BazaNzButtonStyle.Primary,
                            title: this.i18n('items.createDividendTransaction'),
                            icon: 'plus',
                            callback: () => this.createDividendTransaction(),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request),
                },
                columns: [
                    {
                        field: 'title',
                        title: this.i18n('columns.title'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                    {
                        field: 'dateCreatedAt',
                        title: this.i18n('columns.dateCreatedAt'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        width: 220,
                    },
                    {
                        field: 'dateUpdatedAt',
                        title: this.i18n('columns.dateUpdatedAt'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        width: 220,
                    },
                    {
                        field: 'dateProcessedAt',
                        title: this.i18n('columns.dateProcessedAt'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        width: 220,
                    },
                    {
                        field: 'status',
                        title: this.i18n('columns.status'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nStatus(entity.status),
                            translate: true,
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                        ngStyle: (entity) => ({
                            color: this.cssColorStatus(entity.status),
                        }),
                    },
                    {
                        field: 'navigate',
                        title: '',
                        type: {
                            kind: BazaTableFieldType.Link,
                            format: () => this.i18n('actions.navigate'),
                            translate: true,
                            icon: 'caret-right',
                            link: (entity) => ({
                                routerLink: ['/baza-nc/dividends/transactions', entity.ulid],
                            }),
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                ],
                actions: [
                    {
                        icon: 'import',
                        title: this.i18n('actions.sync.title'),
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        action: (entity) => this.sync(entity),
                    },
                    {
                        icon: 'delete',
                        title: this.i18n('actions.delete.title'),
                        visible: (entity) => entity.canBeDeleted,
                        acl: [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement],
                        action: (entity) => this.delete(entity),
                    },
                ],
            },
        };
    }

    refresh(): void {
        this.crud.refresh();
    }

    createDividendTransaction(): void {
        const date = moment().format('YYYY-MM-DD');

        const formGroup = this.fb.group({
            title: [],
        });

        const formBuilder: BazaFormBuilder = {
            id: BAZA_NC_DIVIDEND_TRANSACTION_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'title',
                        label: this.i18n('createDividendTransaction.fields.title'),
                    },
                ],
            },
        };

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.modalLayout.open<BazaNcDividendTransactionDto, CreateDividendTransactionFormValue>({
            formGroup,
            formBuilder,
            title: this.i18n('createDividendTransaction.title'),
            onSubmit: (entity, formValue) =>
                this.dataAccess
                    .create({
                        title: formValue.title || this.translate.instant('baza-nc.dividend.defaultDividendTitle', { date }),
                    })
                    .pipe(
                        tap((response) => {
                            this.router.navigate(['/baza-nc/dividends/transactions', response.ulid]);
                        }),
                    ),
        });
    }

    delete(entity: BazaNcDividendTransactionDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.delete.confirm'),
            messageArgs: {
                title: entity.title,
            },
            confirm: () =>
                this.dataAccess
                    .delete({
                        ulid: entity.ulid,
                    })
                    .pipe(tap(() => this.refresh()))
                    .toPromise(),
        });
    }

    sync(entity: BazaNcDividendTransactionDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.sync.confirm'),
            confirm: () =>
                this.dataAccess
                    .sync({
                        ulid: entity.ulid,
                    })
                    .pipe(
                        tap(() => {
                            this.ngMessages.success({
                                message: this.i18n('actions.sync.success'),
                                translate: true,
                            });
                        }),
                    )
                    .toPromise(),
        });
    }
}
