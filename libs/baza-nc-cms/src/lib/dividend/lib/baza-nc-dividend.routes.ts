import { Routes } from '@angular/router';
import { BazaNcDividendComponent } from './components/baza-nc-dividend/baza-nc-dividend.component';
import { BazaNcDividendTransactionComponent } from './components/baza-nc-dividend-transaction/baza-nc-dividend-transaction.component';
import { BazaNcDividendTransactionEntriesComponent } from './components/baza-nc-dividend-transaction-entries/baza-nc-dividend-transaction-entries.component';
import { BazaNcDividendResolve } from './components/baza-nc-dividend/baza-nc-dividend.resolve';
import { BazaNcDividendTransactionEntriesResolve } from './components/baza-nc-dividend-transaction-entries/baza-nc-dividend-transaction-entries.resolve';

export const bazaNcDividendRoutes: Routes = [
    {
        path: 'baza-nc/dividends',
        component: BazaNcDividendComponent,
        resolve: {
            data: BazaNcDividendResolve,
        },
    },
    {
        path: 'baza-nc/dividends/investor-account/:investorAccountId',
        component: BazaNcDividendComponent,
        resolve: {
            data: BazaNcDividendResolve,
        },
    },
    {
        path: 'baza-nc/dividends/transactions',
        component: BazaNcDividendTransactionComponent,
    },
    {
        path: 'baza-nc/dividends/transactions/:ulid',
        component: BazaNcDividendTransactionEntriesComponent,
        resolve: {
            data: BazaNcDividendTransactionEntriesResolve,
        },
    },
];
