import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCmsLayoutModule, BazaCrudCmsModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsDataAccessModule } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNcReportCmsComponent } from './components/baza-nc-report-cms/baza-nc-report-cms.component';

@NgModule({
    imports: [CommonModule, BazaCrudCmsModule, BazaNcCmsDataAccessModule, BazaCmsLayoutModule],
    declarations: [BazaNcReportCmsComponent],
})
export class BazaNcReportCmsModule {}
