import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudItem, BazaTableFieldAlign, BazaTableFieldType } from '@scaliolabs/baza-core-cms';
import { BazaNcReportDto, BazaNcReportStatus } from '@scaliolabs/baza-nc-shared';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { ActivatedRoute } from '@angular/router';
import { BazaNcReportCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BehaviorSubject, Subject } from 'rxjs';
import { finalize, takeUntil, tap, map } from 'rxjs/operators';
import { getBazaProjectCodeName } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';

interface State {
    config: BazaCrudConfig<BazaNcReportDto>;
}

@Component({
    templateUrl: './baza-nc-report-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcReportCmsComponent implements OnDestroy {
    private readonly ngOnDestroy$ = new Subject();
    private readonly hasReportsToSend$ = new BehaviorSubject(false);

    public crud: BazaCrudComponent<BazaNcReportDto>;

    public state: State = {
        config: this.config,
    };

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly confirm: BazaNgConfirmService,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly activatedRoute: ActivatedRoute,
        private readonly dataAccess: BazaNcReportCmsDataAccess,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    i18n(key: string): string {
        return `baza-nc.report.components.bazaNcReportCms.${key}`;
    }

    get config(): BazaCrudConfig<BazaNcReportDto> {
        return {
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.sendMany.title'),
                            callback: () => this.sendMany(),
                            type: BazaNzButtonStyle.Primary,
                            disabled: this.hasReportsToSend$.pipe(
                                map((next) => {
                                    return !next;
                                }),
                            ),
                        },
                    },
                ],
                toolbarLeft: [
                    {
                        type: BazaCrudItem.DateRange,
                        options: {
                            nzMode: 'date',
                        },
                    },
                ],
                toolbarRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            icon: 'sync',
                            title: this.i18n('actions.syncAll.title'),
                            callback: () => this.syncAll(),
                            type: BazaNzButtonStyle.Link,
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            icon: 'table',
                            title: this.i18n('actions.exportToCSV'),
                            callback: () => this.exportToCSV(),
                            type: BazaNzButtonStyle.Link,
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccess.list(request).pipe(
                            tap((response) => {
                                if ((request.dateFrom || request.dateTo) && response.pager.total === 0) {
                                    this.ngMessages.info({
                                        message: this.i18n('noRecordsInDateRange'),
                                        translate: true,
                                    });
                                }

                                this.hasReportsToSend$.next(response.totalRecordsToSend > 0);

                                this.cdr.markForCheck();
                            }),
                        ),
                },
                columns: [
                    {
                        field: 'tradeId',
                        title: this.i18n('columns.tradeId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 200,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'achId',
                        title: this.i18n('columns.achId'),
                        textAlign: BazaTableFieldAlign.Center,
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => entity.achId || 'N/A',
                        },
                    },
                    {
                        field: 'dateCreatedAt',
                        title: this.i18n('columns.dateCreatedAt'),
                        width: 240,
                        type: {
                            kind: BazaTableFieldType.DateUTC,
                        },
                    },
                    {
                        field: 'dateFundedAt',
                        title: this.i18n('columns.dateFundedAt'),
                        width: 240,
                        type: {
                            kind: BazaTableFieldType.DateUTC,
                        },
                    },
                    {
                        field: 'dateReportedAt',
                        title: this.i18n('columns.dateReportedAt'),
                        width: 240,
                        type: {
                            kind: BazaTableFieldType.DateUTC,
                        },
                    },
                    {
                        field: 'status',
                        title: this.i18n('columns.status'),
                        width: 200,
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => `baza-nc.__types.BazaNcReportStatus.${entity.status}`,
                            translate: true,
                        },
                        ngStyle: (entity) => ({
                            color: (() => {
                                switch (entity.status) {
                                    default: {
                                        return `var(--black)`;
                                    }

                                    case BazaNcReportStatus.PendingPayment: {
                                        return `var(--processing-color)`;
                                    }

                                    case BazaNcReportStatus.ReadyToReport: {
                                        return `var(--success-color)`;
                                    }

                                    case BazaNcReportStatus.Reported: {
                                        return `var(--normal-color)`;
                                    }
                                }
                            })(),
                        }),
                    },
                ],
                actions: [
                    {
                        icon: 'send',
                        title: this.i18n('actions.sendOne.title'),
                        action: (entity) => this.sendOne(entity),
                        visible: (entity) => entity.canBeSent,
                    },
                    {
                        icon: 'send',
                        title: this.i18n('actions.resendOne.title'),
                        action: (entity) => this.resendOne(entity),
                        visible: (entity) => entity.canBeResent,
                    },
                    {
                        icon: 'sync',
                        title: this.i18n('actions.sync.title'),
                        action: (entity) => this.sync(entity),
                        visible: (entity) => entity.canBySynced,
                    },
                ],
            },
        };
    }

    sendOne(entity: BazaNcReportDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.confirm.open({
            message: this.i18n('actions.sendOne.confirm'),
            messageArgs: entity,
            confirm: () => {
                const loading = this.loading.addLoading();

                this.dataAccess
                    .sendOne({
                        ulid: entity.ulid,
                    })
                    .pipe(
                        finalize(() => loading.complete()),
                        takeUntil(this.ngOnDestroy$),
                    )
                    .subscribe(
                        () => {
                            this.ngMessages.success({
                                message: this.i18n('actions.sendOne.success'),
                                translate: true,
                                translateParams: entity,
                            });

                            this.crud.refresh();

                            loading.complete();
                        },
                        (err) => this.ngMessages.bazaError(err),
                    );
            },
        });
    }

    resendOne(entity: BazaNcReportDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.confirm.open({
            message: this.i18n('actions.resendOne.confirm'),
            messageArgs: entity,
            confirm: () => {
                const loading = this.loading.addLoading();

                this.dataAccess
                    .resendOne({
                        ulid: entity.ulid,
                    })
                    .pipe(
                        finalize(() => loading.complete()),
                        takeUntil(this.ngOnDestroy$),
                    )
                    .subscribe(
                        () => {
                            this.ngMessages.success({
                                message: this.i18n('actions.resendOne.success'),
                                translate: true,
                                translateParams: entity,
                            });

                            this.crud.refresh();

                            loading.complete();
                        },
                        (err) => this.ngMessages.bazaError(err),
                    );
            },
        });
    }

    sendMany(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.confirm.open({
            message: this.i18n('actions.sendMany.confirm'),
            confirm: () => {
                const loading = this.loading.addLoading();

                this.dataAccess
                    .sendMany(this.crud.data.lastRequest)
                    .pipe(
                        finalize(() => loading.complete()),
                        takeUntil(this.ngOnDestroy$),
                    )
                    .subscribe(
                        () => {
                            this.ngMessages.success({
                                message: this.i18n('actions.sendMany.success'),
                                translate: true,
                            });

                            this.crud.refresh();

                            loading.complete();
                        },
                        (err) => this.ngMessages.bazaError(err),
                    );
            },
        });
    }

    exportToCSV(): void {
        const loading = this.loading.addLoading();

        this.dataAccess
            .exportToCsv(this.crud.data.lastRequest)
            .pipe(
                finalize(() => loading.complete()),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe(
                (response) => {
                    const now = moment(new Date()).format('YYYY-MM-DD');
                    const pom = document.createElement('a');
                    const blob = new Blob([response], { type: 'text/csv;charset=utf-8;' });

                    pom.href = URL.createObjectURL(blob);
                    pom.setAttribute('download', `escrow-${getBazaProjectCodeName()}-${now}.csv`);
                    pom.click();
                },
                (err) => this.ngMessages.bazaError(err),
            );
    }

    sync(entity: BazaNcReportDto): void {
        const loading = this.loading.addLoading();

        this.dataAccess
            .sync({
                ulid: entity.ulid,
            })
            .pipe(
                finalize(() => loading.complete()),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe(
                () => {
                    this.ngMessages.success({
                        message: this.i18n('actions.sync.success'),
                        translate: true,
                        translateParams: entity,
                    });

                    this.crud.refresh();

                    loading.complete();
                },
                (err) => this.ngMessages.bazaError(err),
            );
    }

    syncAll(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.confirm.open({
            message: this.i18n('actions.syncAll.confirm'),
            confirm: () => {
                const loading = this.loading.addLoading();

                this.dataAccess
                    .syncAll({
                        async: true,
                    })
                    .pipe(
                        finalize(() => loading.complete()),
                        takeUntil(this.ngOnDestroy$),
                    )
                    .subscribe(
                        () => {
                            this.ngMessages.success({
                                message: this.i18n('actions.syncAll.success'),
                                translate: true,
                            });

                            loading.complete();
                        },
                        (err) => this.ngMessages.bazaError(err),
                    );
            },
        });
    }
}
