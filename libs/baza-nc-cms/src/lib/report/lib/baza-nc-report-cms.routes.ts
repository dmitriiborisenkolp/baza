import { Routes } from '@angular/router';
import { BazaNcReportCmsComponent } from './components/baza-nc-report-cms/baza-nc-report-cms.component';

export const bazaNcReportCmsRoutes: Routes = [
    {
        path: 'baza-nc/report',
        component: BazaNcReportCmsComponent,
    },
];
