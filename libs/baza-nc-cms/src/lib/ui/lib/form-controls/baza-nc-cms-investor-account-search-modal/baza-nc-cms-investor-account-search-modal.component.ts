import { ChangeDetectionStrategy, Component, TemplateRef, ViewChild } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { BazaNcInvestorAccountDto } from '@scaliolabs/baza-nc-shared';
import { BazaCrudConfig, BazaCrudItem, BazaTableFieldType } from '@scaliolabs/baza-core-cms';
import { BazaNcInvestorAccountCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';

export const BAZA_NC_INVESTOR_ACCOUNT_SEARCH_MODAL_CMS_ID = 'baza-nc-investor-account-search-modal-cms-id';

export const MAX_ACCOUNT_IN_OPTIONS = 10;

interface State {
    config: BazaCrudConfig<BazaNcInvestorAccountDto>;
    nzModalRef?: NzModalRef;
}

@Component({
    templateUrl: './baza-nc-cms-investor-account-search-modal.component.html',
    styleUrls: ['./baza-nc-cms-investor-account-search-modal.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcCmsInvestorAccountSearchModalComponent {
    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<any>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(private readonly dataAccess: BazaNcInvestorAccountCmsDataAccess) {}

    submit(selected?: BazaNcInvestorAccountDto): void {
        this.state.nzModalRef.close({
            selected,
        });
    }

    i18n(input: string): string {
        return `baza-nc.ui.formControls.bazaNcCmsInvestorAccountSearchModal.${input}`;
    }

    get crudConfig(): BazaCrudConfig<BazaNcInvestorAccountDto> {
        return {
            id: BAZA_NC_INVESTOR_ACCOUNT_SEARCH_MODAL_CMS_ID,
            title: this.i18n('title'),
            titleLevel: 4,
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Search,
                        options: {},
                    },
                ],
            },
            data: {
                columns: [
                    {
                        field: 'userId',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('columns.userId'),
                    },
                    {
                        field: 'userFullName',
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => `${entity.userFullName} (${entity.userEmail})`,
                        },
                        title: this.i18n('columns.userFullName'),
                        width: 'auto',
                    },
                    {
                        field: 'northCapitalAccountId',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.northCapitalAccountId'),
                        width: 180,
                    },
                    {
                        field: 'northCapitalPartyId',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.northCapitalPartyId'),
                        width: 180,
                    },
                ],
                dataSource: {
                    list: (request) => this.dataAccess.listInvestorAccounts(request),
                    size: MAX_ACCOUNT_IN_OPTIONS,
                    sizes: [MAX_ACCOUNT_IN_OPTIONS],
                    nzShowSizeChanger: false,
                    nzShowTotal: false,
                    isRowClickable: true,
                    onRowClick: (account) => {
                        this.submit(account);
                    },
                },
            },
        };
    }
}
