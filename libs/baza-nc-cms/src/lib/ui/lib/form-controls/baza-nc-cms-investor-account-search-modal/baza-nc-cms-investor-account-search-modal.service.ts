import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BazaNcCmsInvestorAccountSearchModalComponent } from './baza-nc-cms-investor-account-search-modal.component';
import { TranslateService } from '@ngx-translate/core';
import { take } from 'rxjs/operators';
import { BazaNcInvestorAccountDto } from '@scaliolabs/baza-nc-shared';

interface SearchModalRequest {
    vcr: ViewContainerRef;
    selected?: BazaNcInvestorAccountDto;
}

@Injectable()
export class BazaNcCmsInvestorAccountSearchModalService {
    constructor(
        private readonly translate: TranslateService,
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    private i18n(input: string): string {
        return this.translate.instant(`baza.formBuilder.components.bazaFieldAccount.searchModal.${input}`);
    }

    open(request: SearchModalRequest): Promise<BazaNcInvestorAccountDto> {
        return new Promise<BazaNcInvestorAccountDto>((resolve) => {
            const factory = this.componentFactoryResolver.resolveComponentFactory(BazaNcCmsInvestorAccountSearchModalComponent);
            const injector = ReflectiveInjector.fromResolvedProviders([], request.vcr.injector);

            const component = factory.create(injector);

            request.vcr.insert(component.hostView);

            setTimeout(() => {
                component.instance.state.nzModalRef = this.nzModal.create({
                    nzTitle: null,
                    nzOkText: null,
                    nzCancelText: this.i18n('cancel'),
                    nzContent: component.instance.nzModal,
                    nzClosable: false,
                    nzOnOk: () => component.instance.submit(),
                    nzWidth: 960,
                });

                component.instance.state.nzModalRef.afterClose.pipe(take(1)).subscribe((response) => {
                    resolve(response?.selected);
                });
            });
        });
    }
}
