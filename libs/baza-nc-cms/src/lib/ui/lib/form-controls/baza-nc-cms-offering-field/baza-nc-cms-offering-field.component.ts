import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    forwardRef,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    SimpleChanges,
} from '@angular/core';
import {
    BazaNcUiOfferingDto,
    BazaNcOfferingsErrorCodes,
    convertFromCents,
    convertToCents,
    NcOfferingUiDetailsRequest,
    NcOfferingUiDetailsResponse,
    NcOfferingUiStatus,
    OfferingId,
    bazaNcApiConfig,
} from '@scaliolabs/baza-nc-shared';
import { AbstractControl, ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { BazaFormBuilderContents, BazaFormBuilderControlType, BazaFormBuilderStaticComponentType } from '@scaliolabs/baza-core-cms';
import { Observable, of, Subject, throwError } from 'rxjs';
import { catchError, distinctUntilChanged, filter, finalize, map, switchMap, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { floatValidator, intValidator, moreThanZeroValidator } from '@scaliolabs/baza-core-ng';
import { BazaError, isBazaErrorResponse, isEmpty } from '@scaliolabs/baza-core-shared';
import { BazaNcCmsOfferingFieldResolveData } from './baza-nc-cms-offering-field.resolve';
import { BazaNcOfferingCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import {
    BAZA_NC_CHARSET_VALIDATOR_MESSAGE_DOCUSIGN,
    bazaNcCharsetValidate,
    bazaNcCharsetValidator,
} from '../../validators/baza-nc-charset.validator';
import { bazaNcBundleCmsConfig } from '../../../../../config';

class FormValue {
    ncOfferingId?: OfferingId;
    ncTargetAmount: number;
    ncMinAmount: number;
    ncMaxAmount: number;
    ncCurrentValue: number;
    ncMaxSharesPerAccount: number;
    ncUnitPrice: number;
    ncOfferingFees: number;
    ncStartDate: Date;
    ncEndDate: Date;
    ncSubscriptionOfferingId: string;
    daysToReleaseFailedTrades: number;
}

interface State {
    form: FormGroup;
    contents: BazaFormBuilderContents;
    ncOfferingResponse?: NcOfferingUiDetailsResponse;
}

@Component({
    selector: 'baza-nc-cms-offering-field',
    templateUrl: './baza-nc-cms-offering-field.component.html',
    styleUrls: ['./baza-nc-cms-offering-field.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaNcCmsOfferingFieldComponent),
            multi: true,
        },
    ],
})
export class BazaNcCmsOfferingFieldComponent implements ControlValueAccessor, OnInit, OnChanges, OnDestroy {
    @Input() resolvedData: BazaNcCmsOfferingFieldResolveData;
    @Input() resolveOffering: (request: NcOfferingUiDetailsRequest) => Observable<NcOfferingUiDetailsResponse>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    private disableUpdateFromNcOffering = false;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    private nextFormGroup$: Subject<void> = new Subject<void>();

    private entity: BazaNcUiOfferingDto;

    public state: State = {
        form: this.formGroup(),
        contents: undefined,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly ncOfferingDataAccess: BazaNcOfferingCmsDataAccess,
    ) {}

    ngOnInit(): void {
        this.state.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next: FormValue) => {
            if (this.onChange) {
                if (next && this.state.form.valid) {
                    this.onChange({
                        ...next,
                        ncCurrentValueCents: convertToCents(next.ncCurrentValue),
                        ncStartDate: next.ncStartDate ? next.ncStartDate.toISOString() : undefined,
                        ncEndDate: next.ncEndDate ? next.ncEndDate.toISOString() : undefined,
                    } as BazaNcUiOfferingDto);
                } else {
                    this.onChange(null);
                }
            }
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['resolvedData'] && !!this.resolvedData) {
            this.state = {
                ...this.state,
                contents: this.formBuilder(),
            };
        }
    }

    ngOnDestroy(): void {
        this.nextFormGroup$.next();
        this.ngOnDestroy$.next();
    }

    i18n(field: string): string {
        return `baza-nc.ui.formControls.bazaNcCmsOfferingField.${field}`;
    }

    formGroup(entity?: BazaNcUiOfferingDto): FormGroup {
        // eslint-disable-next-line prefer-const
        let exportedForm: FormGroup;

        this.entity = entity;

        const form = this.fb.group({
            ncOfferingId: [
                undefined,
                [
                    bazaNcCharsetValidator(),
                    () => {
                        if (!exportedForm) {
                            return null;
                        }

                        if (!this.state.ncOfferingResponse) {
                            return null;
                        }

                        switch (this.state.ncOfferingResponse.status) {
                            default: {
                                return {
                                    [this.i18n('form.errors.ncOfferingStatus.noData')]: true,
                                };
                            }

                            case NcOfferingUiStatus.NotFound: {
                                return {
                                    [this.i18n('form.errors.ncOfferingStatus.notFound')]: true,
                                };
                            }

                            case NcOfferingUiStatus.Used: {
                                return {
                                    [this.i18n('form.errors.ncOfferingStatus.used')]: true,
                                };
                            }

                            case NcOfferingUiStatus.Available: {
                                return null;
                            }
                        }
                    },
                ],
            ],
            ncTargetAmount: [0, [Validators.required, intValidator, moreThanZeroValidator, bazaNcCharsetValidator()]],
            ncMinAmount: [
                0,
                [
                    Validators.required,
                    intValidator,
                    moreThanZeroValidator,
                    bazaNcCharsetValidator(),
                    (control: AbstractControl) => {
                        if (!exportedForm) {
                            return null;
                        }

                        const fv: FormValue = exportedForm.value;

                        const hasMinAmount = !isEmpty(control.value) && !isNaN(+control.value) && control.value > 0;
                        const hasUnitPrice = !isEmpty(fv.ncUnitPrice) && !isNaN(+fv.ncUnitPrice) && fv.ncUnitPrice > 0;

                        if (hasMinAmount && hasUnitPrice) {
                            if (control.value < fv.ncUnitPrice) {
                                return {
                                    [this.i18n('form.errors.ncMinAmount.lessThanUnitPrice')]: true,
                                };
                            } else if (Math.floor(control.value / fv.ncUnitPrice) !== control.value / fv.ncUnitPrice) {
                                return {
                                    [this.i18n('form.errors.ncMinAmount.notIntegerShares')]: true,
                                };
                            } else {
                                return null;
                            }
                        } else {
                            return null;
                        }
                    },
                ],
            ],
            ncMaxAmount: [
                0,
                [
                    Validators.required,
                    intValidator,
                    moreThanZeroValidator,
                    bazaNcCharsetValidator(),
                    (control: AbstractControl) => {
                        if (!exportedForm) {
                            return null;
                        }

                        const fv: FormValue = exportedForm.value;

                        const hasMaxAmount = !isEmpty(control.value) && !isNaN(+control.value) && control.value > 0;
                        const hasTargetAmount = !isEmpty(fv.ncTargetAmount) && !isNaN(+fv.ncTargetAmount) && fv.ncTargetAmount > 0;

                        if (hasMaxAmount && hasTargetAmount) {
                            if (control.value < fv.ncTargetAmount) {
                                return {
                                    [this.i18n('form.errors.ncMaxAmount')]: true,
                                };
                            } else {
                                return null;
                            }
                        } else {
                            return null;
                        }
                    },
                ],
            ],
            ncCurrentValue: [0, [Validators.required, floatValidator, moreThanZeroValidator]],
            ncMaxSharesPerAccount: [
                0,
                [
                    Validators.required,
                    intValidator,
                    moreThanZeroValidator,
                    bazaNcCharsetValidator(),
                    (control: AbstractControl) => {
                        if (!exportedForm) {
                            return null;
                        }

                        const fv: FormValue = exportedForm.value;

                        const hasMaxSharesPerAccount = !isEmpty(control.value) && !isNaN(+control.value) && control.value > 0;
                        const hasUnitPrice = !isEmpty(fv.ncUnitPrice) && !isNaN(+fv.ncUnitPrice) && fv.ncUnitPrice > 0;

                        if (hasMaxSharesPerAccount && hasUnitPrice) {
                            if (control.value * fv.ncUnitPrice > this.resolvedData.maxACHTransferAmount) {
                                return {
                                    [this.i18n('form.errors.ncMaxSharesPerAccount')]: {
                                        limit: this.resolvedData.maxACHTransferAmount,
                                    },
                                };
                            } else {
                                return null;
                            }
                        } else {
                            return null;
                        }
                    },
                ],
            ],
            ncUnitPrice: [
                0,
                [
                    Validators.required,
                    floatValidator,
                    moreThanZeroValidator,
                    bazaNcCharsetValidator(),
                    (control: AbstractControl) => {
                        if (!exportedForm) {
                            return null;
                        }

                        const fv: FormValue = exportedForm.value;

                        const hasUnitPrice = !isEmpty(control.value) && !isNaN(+control.value) && control.value > 0;
                        const hasTargetAmount = !isEmpty(fv.ncTargetAmount) && !isNaN(+fv.ncTargetAmount) && fv.ncTargetAmount > 0;

                        if (hasUnitPrice && hasTargetAmount) {
                            if (control.value > fv.ncTargetAmount) {
                                return {
                                    [this.i18n('form.errors.ncUnitPrice')]: true,
                                };
                            } else {
                                return null;
                            }
                        } else {
                            return null;
                        }
                    },
                ],
            ],
            ncStartDate: [new Date(), [Validators.required, bazaNcCharsetValidator()]],
            ncEndDate: [
                moment(new Date()).add(1, 'years').toDate(),
                [
                    Validators.required,
                    bazaNcCharsetValidator(),
                    () => {
                        if (exportedForm) {
                            const fv: FormValue = exportedForm.value;

                            if (fv.ncStartDate && fv.ncEndDate && fv.ncStartDate.getTime() > fv.ncEndDate.getTime()) {
                                return {
                                    [this.i18n('form.errors.ncDateRangeError')]: true,
                                };
                            } else {
                                return null;
                            }
                        } else {
                            return null;
                        }
                    },
                ],
            ],
            ncSubscriptionOfferingId: [
                undefined,
                [
                    Validators.required,
                    (control: FormControl) => {
                        if (control.disabled || isEmpty(control.value)) {
                            return null;
                        }

                        const docuSignTemplate = this.resolvedData.docusignTemplates.find((template) => template.id === control.value);

                        return bazaNcCharsetValidate(docuSignTemplate.name, BAZA_NC_CHARSET_VALIDATOR_MESSAGE_DOCUSIGN);
                    },
                ],
            ],
            daysToReleaseFailedTrades: [undefined],
        });

        exportedForm = form;

        const ncOfferingFormControls: Array<keyof FormValue> = [
            'ncTargetAmount',
            'ncMaxSharesPerAccount',
            'ncUnitPrice',
            'ncStartDate',
            'ncEndDate',
            'ncMinAmount',
            'ncCurrentValue',
            'ncMaxAmount',
            'ncSubscriptionOfferingId',
            'daysToReleaseFailedTrades',
        ];

        const enableNcOfferingFormControl = () => {
            for (const field of ncOfferingFormControls) {
                form.get(field).enable();
            }
        };

        const disableNcOfferingFormControl = () => {
            for (const field of ncOfferingFormControls) {
                form.get(field).disable();
            }
        };

        form.get('ncStartDate')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextFormGroup$))
            .subscribe(() => {
                setTimeout(() => {
                    form.get('ncEndDate').markAllAsTouched();
                    form.get('ncEndDate').updateValueAndValidity();

                    this.cdr.markForCheck();
                });
            });

        form.get('ncEndDate')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextFormGroup$))
            .subscribe(() => {
                setTimeout(() => {
                    form.get('ncEndDate').markAllAsTouched();
                    form.get('ncEndDate').updateValueAndValidity();

                    this.cdr.markForCheck();
                });
            });

        form.get('ncMaxAmount')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextFormGroup$))
            .subscribe(() => {
                setTimeout(() => {
                    form.get('ncMaxAmount').markAllAsTouched();
                    form.get('ncMaxAmount').updateValueAndValidity();

                    this.cdr.markForCheck();
                });
            });

        form.get('ncMinAmount')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextFormGroup$))
            .subscribe(() => {
                setTimeout(() => {
                    form.get('ncUnitPrice').markAllAsTouched();
                    form.get('ncUnitPrice').updateValueAndValidity();

                    this.cdr.markForCheck();
                });
            });

        form.get('ncTargetAmount')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextFormGroup$))
            .subscribe(() => {
                setTimeout(() => {
                    form.get('ncMaxAmount').markAllAsTouched();
                    form.get('ncMaxAmount').updateValueAndValidity();

                    form.get('ncUnitPrice').markAllAsTouched();
                    form.get('ncUnitPrice').updateValueAndValidity();

                    this.cdr.markForCheck();
                });
            });

        form.get('ncUnitPrice')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextFormGroup$))
            .subscribe(() => {
                setTimeout(() => {
                    form.get('ncMinAmount').markAllAsTouched();
                    form.get('ncMinAmount').updateValueAndValidity();

                    form.get('ncMaxAmount').markAllAsTouched();
                    form.get('ncMaxAmount').updateValueAndValidity();

                    form.get('ncMaxSharesPerAccount').markAllAsTouched();
                    form.get('ncMaxSharesPerAccount').updateValueAndValidity();

                    this.cdr.markForCheck();
                });
            });

        form.get('ncOfferingId')
            .valueChanges.pipe(
                distinctUntilChanged(),
                filter(() => form.get('ncOfferingId').enabled),
                filter(() => !this.disableUpdateFromNcOffering),
                switchMap((ncOfferingId) => {
                    if (isEmpty(ncOfferingId)) {
                        enableNcOfferingFormControl();

                        return of(undefined);
                    } else {
                        disableNcOfferingFormControl();

                        const observable = this.resolveOffering
                            ? this.resolveOffering({
                                  ncOfferingId,
                              })
                            : this.ncOfferingDataAccess
                                  .ncOfferingDetails({
                                      ncOfferingId: ncOfferingId,
                                  })
                                  .pipe(
                                      map(
                                          (details) =>
                                              ({
                                                  details,
                                                  status: NcOfferingUiStatus.Available,
                                              } as NcOfferingUiDetailsResponse),
                                      ),
                                  );

                        return observable.pipe(
                            catchError((err: BazaError) => {
                                if (isBazaErrorResponse(err) && err.code === BazaNcOfferingsErrorCodes.NcOfferingNotFound) {
                                    const response: NcOfferingUiDetailsResponse = {
                                        status: NcOfferingUiStatus.NotFound,
                                    };

                                    return of(response);
                                } else {
                                    return throwError(err);
                                }
                            }),
                            finalize(() => enableNcOfferingFormControl()),
                        );
                    }
                }),
                takeUntil(this.nextFormGroup$),
            )
            .subscribe((ncOfferingResponse: NcOfferingUiDetailsResponse) => {
                this.state = {
                    ...this.state,
                    ncOfferingResponse,
                };

                form.get('ncOfferingId').markAllAsTouched();
                form.get('ncOfferingId').updateValueAndValidity();

                if (ncOfferingResponse && ncOfferingResponse.details) {
                    form.patchValue({
                        ncTargetAmount: ncOfferingResponse.details.targetAmount,
                        ncMaxAmount: ncOfferingResponse.details.maxAmount,
                        ncCurrentValue: ncOfferingResponse.details.maxAmount,
                        ncMinAmount: ncOfferingResponse.details.minAmount,
                        ncUnitPrice: ncOfferingResponse.details.unitPrice,
                        ncStartDate: new Date(ncOfferingResponse.details.startDate),
                        ncEndDate: new Date(ncOfferingResponse.details.endDate),
                    } as Partial<FormValue>);
                } else {
                    form.patchValue({
                        ncMinAmount: undefined,
                        ncMaxAmount: undefined,
                        ncCurrentValue: undefined,
                        ncTargetAmount: undefined,
                        ncMaxSharesPerAccount: undefined,
                        ncUnitPrice: undefined,
                        ncStartDate: undefined,
                        ncEndDate: undefined,
                    } as Partial<FormValue>);
                }

                this.cdr.markForCheck();
            });

        if (bazaNcBundleCmsConfig().withTransactionFeeFeature) {
            form.addControl('ncOfferingFees', new FormControl(0, [Validators.required]));
        }

        return form;
    }

    formBuilder(): BazaFormBuilderContents {
        const formBuilderContents: BazaFormBuilderContents = {
            fields: [
                {
                    formControlName: 'ncOfferingId',
                    type: BazaFormBuilderControlType.Text,
                    label: this.i18n('form.fields.ncOfferingId'),
                    placeholder: this.i18n('form.placeholders.ncOfferingId'),
                    hint: {
                        text: this.i18n('form.hints.ncOfferingId'),
                        translate: true,
                    },
                },
                {
                    type: BazaFormBuilderStaticComponentType.Divider,
                },
                {
                    formControlName: 'ncSubscriptionOfferingId',
                    type: BazaFormBuilderControlType.Select,
                    label: this.i18n('form.fields.ncSubscriptionOfferingId'),
                    props: {
                        nzShowSearch: true,
                        nzAllowClear: true,
                        nzOptions: this.resolvedData.docusignTemplates.map((document) => ({
                            label: document.name,
                            value: document.id,
                        })),
                    },
                },
                {
                    formControlName: 'daysToReleaseFailedTrades',
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('form.fields.daysToReleaseFailedTrades'),
                    visible: bazaNcApiConfig().enableAutoCancellingFailedTrades,
                },
                {
                    formControlName: 'ncTargetAmount',
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('form.fields.ncTargetAmount'),
                    hint: {
                        text: this.i18n('form.hints.ncTargetAmount'),
                        translate: true,
                    },
                },
                {
                    formControlName: 'ncMaxAmount',
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('form.fields.ncMaxAmount'),
                    hint: {
                        text: this.i18n('form.hints.ncMaxAmount'),
                        translate: true,
                    },
                },
                {
                    formControlName: 'ncMaxSharesPerAccount',
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('form.fields.ncMaxSharesPerAccount'),
                    hint: {
                        text: this.i18n('form.hints.ncMaxSharesPerAccount'),
                        translate: true,
                    },
                },
                {
                    formControlName: 'ncOfferingFees',
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('form.fields.ncOfferingFees'),
                    help: {
                        text: this.i18n('form.help.ncOfferingFees'),
                        translate: true,
                    },
                    hint: {
                        text: this.i18n('form.hints.ncOfferingFees'),
                        translate: true,
                    },
                    props: {
                        nzMin: 0,
                        nzMax: 100,
                        nzStep: 0.01,
                        nzFormatter: (input: number) => (input || input === 0 ? `${input}%` : ''),
                        nzParser: (input: string) => (input ? input.replace('%', '') : ''),
                        nzPrecision: 2,
                    },
                    visible: bazaNcBundleCmsConfig().withTransactionFeeFeature,
                },
                {
                    formControlName: 'ncUnitPrice',
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('form.fields.ncUnitPrice'),
                    props: {
                        nzStep: 0.01,
                    },
                    hint: {
                        text: this.i18n('form.hints.ncUnitPrice'),
                        translate: true,
                    },
                },
                {
                    formControlName: 'ncMinAmount',
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('form.fields.ncMinAmount'),
                    hint: {
                        text: this.i18n('form.hints.ncMinAmount'),
                        translate: true,
                    },
                },
                {
                    formControlName: 'ncStartDate',
                    type: BazaFormBuilderControlType.Date,
                    label: this.i18n('form.fields.ncStartDate'),
                    hint: {
                        text: this.i18n('form.hints.ncStartDate'),
                        translate: true,
                    },
                },
                {
                    formControlName: 'ncEndDate',
                    type: BazaFormBuilderControlType.Date,
                    label: this.i18n('form.fields.ncEndDate'),
                    hint: {
                        text: this.i18n('form.hints.ncEndDate'),
                        translate: true,
                    },
                },
                {
                    type: BazaFormBuilderStaticComponentType.Divider,
                },
                {
                    formControlName: 'ncCurrentValue',
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('form.fields.ncCurrentValue'),
                    hint: {
                        text: this.i18n('form.hints.ncCurrentValue'),
                        translate: true,
                    },
                },
            ],
        };

        return formBuilderContents;
    }

    writeValue(value: BazaNcUiOfferingDto): void {
        this.disableUpdateFromNcOffering = true;

        this.entity = value;

        if (value) {
            this.state.form.patchValue({
                ...value,
                ncCurrentValue: Number(convertFromCents(value.ncCurrentValueCents).toFixed(2)),
                ncStartDate: value.ncStartDate ? new Date(value.ncStartDate) : undefined,
                ncEndDate: value.ncEndDate ? new Date(value.ncEndDate) : undefined,
            } as FormValue);
        } else {
            this.state.form.reset();
        }

        this.disableUpdateFromNcOffering = false;

        this.cdr.markForCheck();
    }

    registerOnChange(fn: () => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
