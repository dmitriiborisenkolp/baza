import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { BazaRegistryDataAccess } from '@scaliolabs/baza-core-data-access';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaNcDocusignDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcDocusignCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaRegistryGetSchemaRecordResponse } from '@scaliolabs/baza-core-shared';

interface ResolveData {
    maxACHTransferAmount: number;
    docusignTemplates: Array<BazaNcDocusignDto>;
}

export const BAZA_NC_CMS_OFFERING_FIELD_RESOLVE_DATA_KEY = 'bazaNcCmsOfferingFieldResolve';

export { ResolveData as BazaNcCmsOfferingFieldResolveData };

@Injectable()
export class BazaNcCmsOfferingFieldResolve implements Resolve<ResolveData> {
    constructor(private readonly registry: BazaRegistryDataAccess, private readonly docusign: BazaNcDocusignCmsDataAccess) {}

    resolve(): Observable<ResolveData> | Promise<ResolveData> | ResolveData {
        const observables: [Observable<BazaRegistryGetSchemaRecordResponse>, Observable<Array<BazaNcDocusignDto>>] = [
            this.registry.getSchemaRecord({
                path: 'bazaNc.maxACHTransferAmount',
            }),
            this.docusign.getAll(),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([maxACHTransferAmount, docusignTemplates]) => ({
                maxACHTransferAmount: maxACHTransferAmount.value,
                docusignTemplates,
            })),
        );
    }
}
