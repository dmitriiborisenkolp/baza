import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    forwardRef,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewContainerRef,
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, finalize, map, switchMap, takeUntil } from 'rxjs/operators';
import { isEmpty } from '@scaliolabs/baza-core-shared';
import { BazaLoadingService } from '@scaliolabs/baza-core-ng';
import { BazaNcInvestorAccountCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNcInvestorAccountDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcCmsInvestorAccountSearchModalService } from '../baza-nc-cms-investor-account-search-modal/baza-nc-cms-investor-account-search-modal.service';

interface FormValue {
    search: string;
    selected: BazaNcInvestorAccountDto;
}

interface State {
    form: FormGroup;
    options: Array<BazaNcInvestorAccountDto>;
}

const DEBOUNCE_TIME_MS = 100;

@Component({
    selector: 'baza-field-account',
    templateUrl: './baza-nc-cms-investor-account-field.component.html',
    styleUrls: ['./baza-nc-cms-investor-account-field.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaNcCmsInvestorAccountFieldComponent),
            multi: true,
        },
    ],
})
export class BazaNcCmsInvestorAccountFieldComponent implements OnInit, OnDestroy, ControlValueAccessor {
    @Input() title: string;
    @Input() nzRequired: boolean;
    @Input() formControlName: string;
    @Input() placeholder: string;

    @Output() searchModal: EventEmitter<BazaNcInvestorAccountDto> = new EventEmitter<BazaNcInvestorAccountDto>();

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    private disableTriggerSearch = false;

    public state: State = {
        form: this.fb.group({
            search: [],
            selected: [undefined, [Validators.required]],
        }),
        options: [],
    };

    public compareFun = (o1: BazaNcInvestorAccountDto | string, o2: BazaNcInvestorAccountDto) => {
        if (o1) {
            return typeof o1 === 'string' ? o1 === o2.northCapitalPartyId : o1.northCapitalPartyId === o2.northCapitalPartyId;
        } else {
            return false;
        }
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly vcr: ViewContainerRef,
        private readonly loading: BazaLoadingService,
        private readonly dataAccess: BazaNcInvestorAccountCmsDataAccess,
        private readonly searchModalService: BazaNcCmsInvestorAccountSearchModalService,
    ) {}

    ngOnInit(): void {
        this.state.form
            .get('search')
            .valueChanges.pipe(
                distinctUntilChanged(),
                filter(() => !this.disableTriggerSearch),
                switchMap((queryString) => {
                    if (isEmpty(queryString)) {
                        return of([]);
                    } else {
                        const loading = this.loading.addLoading();

                        return this.dataAccess
                            .listInvestorAccounts({
                                queryString: typeof queryString === 'string' && !isEmpty(queryString) ? queryString : undefined,
                            })
                            .pipe(
                                map((response) => response.items),
                                finalize(() => loading.complete()),
                            );
                    }
                }),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((options: Array<BazaNcInvestorAccountDto>) => {
                this.state = {
                    ...this.state,
                    options,
                };

                this.cdr.markForCheck();
            });

        this.state.form
            .get('search')
            .valueChanges.pipe(
                distinctUntilChanged(),
                filter(() => !this.disableTriggerSearch),
                debounceTime(DEBOUNCE_TIME_MS),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((next) => {
                if (!!next && typeof next == 'object' && !!(next as BazaNcInvestorAccountDto).id) {
                    this.patchFormValue = {
                        selected: next,
                    };

                    this.onChange(next);
                } else {
                    const isSameAccount =
                        typeof next === 'string' &&
                        !isEmpty(next) &&
                        !!this.formValue.selected &&
                        this.formValue.search === this.formValue.selected.userFullName;

                    if (!isSameAccount) {
                        this.patchFormValue = {
                            selected: undefined,
                        };

                        this.onChange(undefined);
                    }
                }
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    set patchFormValue(patch: Partial<FormValue>) {
        this.disableTriggerSearch = true;

        this.state.form.patchValue(patch);

        this.disableTriggerSearch = false;
    }

    registerOnChange(fn: () => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    writeValue(obj: () => void): void {
        if (!!obj && typeof obj === 'object' && !!(obj as BazaNcInvestorAccountDto).id) {
            this.patchFormValue = {
                selected: obj,
                search: (obj as BazaNcInvestorAccountDto).userFullName,
            };
        } else {
            this.patchFormValue = {
                selected: undefined,
                search: undefined,
            };
        }
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }

    search(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.searchModalService
            .open({
                vcr: this.vcr,
                selected: this.formValue.selected,
            })
            .then((selected) => {
                this.patchFormValue = {
                    selected,
                    search: selected ? `${selected.userFullName} (ID: ${selected.userId})` : undefined,
                };

                if (this.onChange) {
                    this.onChange(this.formValue.selected);
                }
            });

        this.searchModal.emit();
    }
}
