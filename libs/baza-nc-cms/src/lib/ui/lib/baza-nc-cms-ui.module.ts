import { NgModule } from '@angular/core';
import { BazaNcCmsOfferingFieldComponent } from './form-controls/baza-nc-cms-offering-field/baza-nc-cms-offering-field.component';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaNcCmsOfferingFieldResolve } from './form-controls/baza-nc-cms-offering-field/baza-nc-cms-offering-field.resolve';
import { CommonModule } from '@angular/common';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzFormModule } from 'ng-zorro-antd/form';
import { BazaNgCoreModule } from '@scaliolabs/baza-core-ng';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { BazaNcCmsInvestorAccountFieldComponent } from './form-controls/baza-nc-cms-investor-account-field';
import { BazaNcCmsInvestorAccountSearchModalComponent } from './form-controls/baza-nc-cms-investor-account-search-modal/baza-nc-cms-investor-account-search-modal.component';
import { BazaNcCmsInvestorAccountSearchModalService } from './form-controls/baza-nc-cms-investor-account-search-modal/baza-nc-cms-investor-account-search-modal.service';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        ReactiveFormsModule,
        NzInputModule,
        NzAutocompleteModule,
        NzFormModule,
        NzIconModule,
        BazaNgCoreModule,
    ],
    declarations: [BazaNcCmsOfferingFieldComponent, BazaNcCmsInvestorAccountFieldComponent, BazaNcCmsInvestorAccountSearchModalComponent],
    exports: [BazaNcCmsOfferingFieldComponent, BazaNcCmsInvestorAccountFieldComponent, BazaNcCmsInvestorAccountSearchModalComponent],
    providers: [BazaNcCmsOfferingFieldResolve, BazaNcCmsInvestorAccountSearchModalService],
})
export class BazaNcCmsUiModule {}
