import { BazaFormControl, registerFormControl } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsOfferingFieldComponent } from './form-controls/baza-nc-cms-offering-field/baza-nc-cms-offering-field.component';
import { BazaNcCmsInvestorAccountFieldComponent } from './form-controls/baza-nc-cms-investor-account-field';

export enum BazaNcCmsUi {
    BazaNcOfferingField = 'BazaNcOfferingField',
    BazaNcInvestorAccountField = 'BazaNcInvestorAccountField',
}

export interface BazaNcCmsUiOfferingField extends BazaFormControl<BazaNcCmsOfferingFieldComponent, BazaNcCmsUi> {
    type: BazaNcCmsUi.BazaNcOfferingField;
    props?: Partial<BazaNcCmsOfferingFieldComponent>;
}

export interface BazaNcCmsUiInvestorAccountField extends BazaFormControl<BazaNcCmsInvestorAccountFieldComponent, BazaNcCmsUi> {
    type: BazaNcCmsUi.BazaNcInvestorAccountField;
    props?: Partial<BazaNcCmsInvestorAccountFieldComponent>;
}

export type BazaNcCmsUiFields = BazaNcCmsUiOfferingField | BazaNcCmsUiInvestorAccountField;

registerFormControl<BazaNcCmsUi>(BazaNcCmsUi.BazaNcOfferingField, {
    component: BazaNcCmsOfferingFieldComponent,
    enableNzFormContainer: false,
});

registerFormControl<BazaNcCmsUi>(BazaNcCmsUi.BazaNcInvestorAccountField, {
    component: BazaNcCmsInvestorAccountFieldComponent,
    enableNzFormContainer: true,
});
