import { AbstractControl, ValidatorFn } from '@angular/forms';
import { BazaNcIntegrationSchemaDefinition, BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';

/**
 * Custom validator function for BazaNcIntegrationSchemaDefinition
 *
 * @param schemaDefinitions - An array of BazaNcIntegrationSchemaDefinition objects to validate
 *
 * @returns A validator function that checks for the following validation rules:
 * - if the formGroup value is null and at least one of the schema definitions is required
 * - if the value of a form control is null and its corresponding schema definition is required
 * - if the type of the schema definition is 'Number' or 'Slider', it also checks for minimum and maximum values for the schema definition
 * - returns a validation error object if the validation fails
 * - returns null if the validation passes
 *
 * @example
 * const schemaDefinition: BazaNcIntegrationSchemaDefinition[] = [{
 *   type: BazaNcIntegrationSchemaType.Number,
 *   payload: {
 *     field: 'field1',
 *     required: true,
 *     min: 0,
 *     max: 10
 *   },
 *   {
 *     type: BazaNcIntegrationSchemaType.Slider,
 *     payload: {
 *       field: 'field2',
 *       required: false,
 *       min: 0,
 *       max: 100
 *     }
 * }];
 *
 * const formGroup = new FormGroup({ field1: null, field2: 50 }, bazaNcSchemaValidator(schemaDefinitions));
 */
export function bazaNcSchemaValidator(schemaDefinitions: BazaNcIntegrationSchemaDefinition[]): ValidatorFn {
    return (formGroup: AbstractControl) => {
        if (!formGroup.value && schemaDefinitions.some((def) => def.payload.required)) {
            return { required: true };
        }

        for (const field of Object.keys(formGroup.value || {})) {
            const definition = schemaDefinitions.find((def) => def.payload.field === field);

            if (!definition) {
                continue;
            }

            const value = formGroup.value[`${field}`];

            if (!value && definition?.payload?.required) {
                return { required: true };
            }

            if ([BazaNcIntegrationSchemaType.Number, BazaNcIntegrationSchemaType.Slider].includes(definition?.type)) {
                const { min, max } = definition.payload as { min: number; max: number };

                if (value && ((typeof min === 'number' && value < min) || (typeof max === 'number' && value > max))) {
                    return { range: true };
                }
            }
        }

        return null;
    };
}
