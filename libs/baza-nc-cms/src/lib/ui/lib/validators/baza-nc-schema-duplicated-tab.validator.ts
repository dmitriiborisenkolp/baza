import { AbstractControl, ValidatorFn } from '@angular/forms';
import { BazaNcIntegrationSchemaTab } from '@scaliolabs/baza-nc-integration-shared';

export const BAZA_NC_SCHEMA_DUPLICATED_TAB_VALIDATOR_MESSAGE = 'baza-nc._.validators.bazaNcSchemaDuplicatedTab';

/**
 * Custom validator function that checks for duplicated tabs in a BazaNcIntegrationSchemaTab array.
 *
 * @param options - An object containing the array of BazaNcIntegrationSchemaTab objects to validate
 *
 * @returns
 * - returns a validation error object if a duplicated tab is found
 * - returns null if the validation passes
 */
export function bazaNcSchemaDuplicatedTabValidator(options: { tabs: BazaNcIntegrationSchemaTab[] }): ValidatorFn {
    return ({ value }: AbstractControl) => {
        const hasDuplicatedTab = options.tabs.some((tab) => tab.title?.toLowerCase() === value?.toLowerCase());

        if (hasDuplicatedTab) {
            return { [BAZA_NC_SCHEMA_DUPLICATED_TAB_VALIDATOR_MESSAGE]: true };
        }

        return null;
    };
}
