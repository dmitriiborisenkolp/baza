import { AbstractControl, ValidatorFn } from '@angular/forms';
import { isEmpty } from '@scaliolabs/baza-core-shared';
import { bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';

export const BAZA_NC_CHARSET_VALIDATOR_MESSAGE_DEFAULT = 'baza-nc._.validators.bazaNcCharsetValidator';
export const BAZA_NC_CHARSET_VALIDATOR_MESSAGE_DOCUSIGN = 'baza-nc._.validators.bazaNcCharsetValidatorDocuSign';

export function bazaNcCharsetValidator(errorMessage = BAZA_NC_CHARSET_VALIDATOR_MESSAGE_DEFAULT): ValidatorFn {
    return (control: AbstractControl) => {
        if (control.disabled || isEmpty(control.value)) {
            return null;
        }

        return bazaNcCharsetValidate(control.value, errorMessage);
    };
}

export function bazaNcCharsetValidate(value: string, errorMessage = BAZA_NC_CHARSET_VALIDATOR_MESSAGE_DEFAULT) {
    const config = bazaNcApiConfig();

    for (const symbol of (value || '').toString().split('')) {
        if (config.enableCharsetFiltering) {
            if (!config.charset.includes(symbol)) {
                return {
                    [errorMessage]: true,
                };
            }
        }
    }

    return null;
}
