import { AbstractControl, ValidatorFn } from '@angular/forms';
import { BazaNcIntegrationSchemaDefinition } from '@scaliolabs/baza-nc-integration-shared';

export const BAZA_NC_SCHEMA_DUPLICATED_FIELD_VALIDATOR_MESSAGE = 'baza-nc._.validators.bazaNcSchemaDuplicatedField';

/**
 * Custom validator function that checks for duplicated fields in a BazaNcIntegrationSchemaDefinition array.
 *
 * @param options - An object containing the tabId and an array of BazaNcIntegrationSchemaDefinition objects to validate
 *
 * @returns
 * - returns a validation error object if a duplicated field is found
 * - returns null if the validation passes
 */
export function bazaNcSchemaDuplicatedFieldValidator(options: {
    tabId: string;
    definitions: BazaNcIntegrationSchemaDefinition[];
}): ValidatorFn {
    return ({ value }: AbstractControl) => {
        const hasDuplicatedField = options.definitions
            .filter((def) => def.payload?.tabId === options.tabId)
            .some((def) => def.payload?.field?.toLowerCase() === value?.toLowerCase());

        if (hasDuplicatedField) {
            return { [BAZA_NC_SCHEMA_DUPLICATED_FIELD_VALIDATOR_MESSAGE]: true };
        }

        return null;
    };
}
