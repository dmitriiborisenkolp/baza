import { Routes } from '@angular/router';
import { BazaNcTaxDocumentComponent } from './components/baza-nc-tax-document/baza-nc-tax-document.component';
import { BazaNcTaxDocumentResolve } from './components/baza-nc-tax-document/baza-nc-tax-document.resolve';
import { BazaNcTaxDocumentGuard } from './components/baza-nc-tax-document/baza-nc-tax-document.guard';

export const bazaNcTaxDocumentCmsRoutes: Routes = [
    {
        path: 'baza-nc/tax-documents/:investorAccountId',
        component: BazaNcTaxDocumentComponent,
        resolve: {
            bazaNcTaxDocumentResolve: BazaNcTaxDocumentResolve,
        },
        canActivate: [
            BazaNcTaxDocumentGuard,
        ],
    },
];
