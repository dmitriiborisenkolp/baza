import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCmsLayoutModule, BazaCrudCmsModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsDataAccessModule } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNcTaxDocumentComponent } from './components/baza-nc-tax-document/baza-nc-tax-document.component';
import { BazaNcTaxDocumentResolve } from './components/baza-nc-tax-document/baza-nc-tax-document.resolve';
import { BazaNcTaxDocumentGuard } from './components/baza-nc-tax-document/baza-nc-tax-document.guard';

@NgModule({
    imports: [CommonModule, BazaCrudCmsModule, BazaCmsLayoutModule, BazaNcCmsDataAccessModule],
    declarations: [BazaNcTaxDocumentComponent],
    providers: [BazaNcTaxDocumentResolve, BazaNcTaxDocumentGuard],
})
export class BazaNcTaxDocumentCmsModule {}
