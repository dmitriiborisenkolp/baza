import { Injectable } from '@angular/core';
import { BazaNcInvestorAccountDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { BazaNotFoundService } from '@scaliolabs/baza-core-cms';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { map, retryWhen } from 'rxjs/operators';

interface ResolveData {
    investorAccount: BazaNcInvestorAccountDto;
}

export { ResolveData as BazaNcTaxDocumentResolveData };

@Injectable()
export class BazaNcTaxDocumentResolve implements Resolve<ResolveData> {
    constructor(
        private readonly notFound: BazaNotFoundService,
        private readonly investorAccountDataAccess: BazaNcInvestorAccountCmsDataAccess,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> | Promise<ResolveData> | ResolveData {
        const id = parseInt(route.params['investorAccountId'], 10);

        if (!id || isNaN(id)) {
            this.notFound.navigateToNotFound();
        }

        const observables: [Observable<BazaNcInvestorAccountDto>] = [
            this.investorAccountDataAccess.getInvestorAccount({
                id,
            }),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([investorAccount]) => ({
                investorAccount,
            })),
        );
    }
}
