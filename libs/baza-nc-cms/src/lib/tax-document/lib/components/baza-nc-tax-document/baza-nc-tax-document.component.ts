import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFieldUploadPreset,
    BazaFileUploadTransport,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaNcTaxDocumentCmsDto } from '@scaliolabs/baza-nc-shared';
import { ActivatedRoute } from '@angular/router';
import { BazaNcTaxDocumentResolveData } from './baza-nc-tax-document.resolve';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaNcTaxDocumentCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AttachmentType } from '@scaliolabs/baza-core-shared';

export const BAZA_NC_TAX_DOCUMENTS_CMS_ID = 'baza-nc-tax-documents-cms-id';
export const BAZA_NC_TAX_DOCUMENTS_FORM_ID = 'baza-nc-tax-documents-form-id';

interface FormValue {
    isPublished: boolean;
    title: string;
    description?: string;
    fileS3Key: string;
}

interface State {
    config: BazaCrudConfig<BazaNcTaxDocumentCmsDto>;
}

@Component({
    templateUrl: './baza-nc-tax-document.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcTaxDocumentComponent {
    public crud: BazaCrudComponent<BazaNcTaxDocumentCmsDto>;

    public MAX_PDF_FILE_SIZE: number = 20 * 1024 * 1024; // 20MB

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly activatedRoute: ActivatedRoute,
        private readonly dataAccess: BazaNcTaxDocumentCmsDataAccess,
    ) {}

    i18n(key: string): string {
        return `baza-nc.taxDocument.components.bazaNcTaxDocument.${key}`;
    }

    get resolvedData(): BazaNcTaxDocumentResolveData {
        return this.activatedRoute.snapshot.data['bazaNcTaxDocumentResolve'];
    }

    private crudConfig(): BazaCrudConfig<BazaNcTaxDocumentCmsDto> {
        return {
            id: BAZA_NC_TAX_DOCUMENTS_CMS_ID,
            title: this.i18n('title'),
            titleArguments: {
                fullName: this.resolvedData.investorAccount.userFullName,
            },
            backRouterLink: {
                routerLink: ['/baza-nc/investor-accounts'],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.create'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<BazaNcTaxDocumentCmsDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.create'),
                                    onSubmit: (entity, formValue) =>
                                        this.dataAccess
                                            .create({
                                                ...formValue,
                                                investorAccountId: this.resolvedData.investorAccount.id,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccess.list({
                            ...request,
                            investorAccountId: this.resolvedData.investorAccount.id,
                        }),
                    inactive: (row) => !row.isPublished,
                },
                actions: [
                    {
                        title: this.i18n('actions.update'),
                        icon: 'edit',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<BazaNcTaxDocumentCmsDto, FormValue>({
                                formGroup: this.formGroup(),
                                formBuilder: this.formBuilder(),
                                title: this.i18n('actions.update'),
                                fetch: () =>
                                    this.dataAccess.getById({
                                        id: entity.id,
                                    }),
                                onSubmit: (fetched, formValue) =>
                                    this.dataAccess
                                        .update({
                                            id: fetched.id,
                                            ...formValue,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            }),
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.success'),
                                                    translateParams: entity,
                                                }),
                                            ),
                                            catchError((err) => {
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.failed'),
                                                    translateParams: entity,
                                                });

                                                return throwError(err);
                                            }),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('fields.id'),
                    },
                    {
                        field: 'title',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.title'),
                    },
                    {
                        title: '',
                        type: {
                            kind: BazaTableFieldType.DownloadLink,
                            link: (entity) => entity.fileUrl,
                            icon: 'download',
                            format: () => this.i18n('actions.download'),
                            translate: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 90,
                    },
                    {
                        field: 'isPublished',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        title: this.i18n('fields.isPublished'),
                        width: 180,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isPublished: [true, [Validators.required]],
            title: [undefined, [Validators.required]],
            description: [undefined],
            fileS3Key: [undefined, [Validators.required]],
        });
    }

    private formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_NC_TAX_DOCUMENTS_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Checkbox,
                        formControlName: 'isPublished',
                        label: this.i18n('fields.isPublished'),
                    },
                    {
                        type: BazaFormBuilderStaticComponentType.Divider,
                    },
                    {
                        formControlName: 'title',
                        type: BazaFormBuilderControlType.Text,
                        label: this.i18n('fields.title'),
                    },
                    {
                        formControlName: 'description',
                        type: BazaFormBuilderControlType.TextArea,
                        label: this.i18n('fields.description'),
                    },
                    {
                        formControlName: 'fileS3Key',
                        type: BazaFormBuilderControlType.BazaUpload,
                        label: this.i18n('fields.fileS3Key'),
                        props: {
                            withOptions: {
                                preset: BazaFieldUploadPreset.Document,
                                transport: {
                                    type: BazaFileUploadTransport.BazaAttachment,
                                    options: () => ({
                                        type: AttachmentType.None,
                                        payload: {
                                            maxFileSize: this.MAX_PDF_FILE_SIZE,
                                        },
                                    }),
                                },
                                maxFileSizeBytes: this.MAX_PDF_FILE_SIZE,
                                message: {
                                    text: this.i18n('labels.fileS3Key'),
                                    translate: true,
                                    translateArgs: {
                                        maxSizeMb: Math.ceil(this.MAX_PDF_FILE_SIZE / 1024 / 1024),
                                    },
                                },
                            },
                        },
                    },
                ],
            },
        };
    }
}
