import { Injectable } from '@angular/core';
import { CanActivate } from '@nestjs/common';
import { BazaNotFoundService } from '@scaliolabs/baza-core-cms';
import { bazaNcBundleCmsConfig } from '../../../../../config';

@Injectable()
export class BazaNcTaxDocumentGuard implements CanActivate {
    constructor(
        private readonly notFound: BazaNotFoundService,
    ) {}

    canActivate(): boolean {
        if (! bazaNcBundleCmsConfig().withTaxDocumentsFeature) {
            this.notFound.navigateToNotFound();

            return false;
        } else {
            return true;
        }
    }
}
