import { Routes } from '@angular/router';
import { bazaNcOfferingCmsRoutes } from '../../lib/offering';
import { bazaNcTransactionsCmsRoutes } from '../../lib/transaction';
import { bazaNcInvestorAccountCmsRoutes } from '../../lib/investor-account';
import { bazaNcKycLogsRoutes } from '../../lib/kyc-logs';
import { bazaNcTaxDocumentCmsRoutes } from '../../lib/tax-document';
import { bazaNcDividendRoutes } from '../../lib/dividend';
import { bazaNcReportCmsRoutes } from '../../lib/report';

export const bazaNcBundlesCmsRoutes: Routes = [
    ...bazaNcInvestorAccountCmsRoutes,
    ...bazaNcKycLogsRoutes,
    ...bazaNcOfferingCmsRoutes,
    ...bazaNcTransactionsCmsRoutes,
    ...bazaNcTaxDocumentCmsRoutes,
    ...bazaNcDividendRoutes,
    ...bazaNcReportCmsRoutes,
];
