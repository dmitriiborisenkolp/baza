import { NgModule } from '@angular/core';
import { BazaNcOfferingCmsModule } from '../../lib/offering';
import { BazaNcInvestorAccountCmsModule } from '../../lib/investor-account';
import { BazaNcTransactionsCmsModule } from '../../lib/transaction';
import { BazaNcKycLogsCmsModule } from '../../lib/kyc-logs';
import { BazaNcTaxDocumentCmsModule } from '../../lib/tax-document';
import { BazaNcDividendCmsModule } from '../../lib/dividend';

@NgModule({
    imports: [
        BazaNcInvestorAccountCmsModule,
        BazaNcKycLogsCmsModule,
        BazaNcTransactionsCmsModule,
        BazaNcOfferingCmsModule,
        BazaNcTaxDocumentCmsModule,
        BazaNcDividendCmsModule,
    ],
})
export class BazaNcBundlesCmsModule {}
