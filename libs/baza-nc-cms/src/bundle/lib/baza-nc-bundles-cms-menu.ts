import { CmsSidePanelMenuGroupConfig } from '@scaliolabs/baza-core-cms';
import { BazaNorthCapitalAcl } from '@scaliolabs/baza-nc-shared';
import { bazaNcBundleCmsConfig } from '../../config';

export const bazaNcBundlesCmsMenu: () => Array<CmsSidePanelMenuGroupConfig> = () => [
    {
        title: 'baza-nc.title',
        items: [
            {
                title: 'baza-nc.investorAccount.title',
                translate: true,
                routerLink: ['/baza-nc/investor-accounts'],
                requiresACL: [BazaNorthCapitalAcl.BazaNcInvestorAccount],
                icon: 'user-add',
            },
            {
                title: 'baza-nc.transaction.title',
                translate: true,
                routerLink: ['/baza-nc/transactions'],
                requiresACL: [BazaNorthCapitalAcl.BazaNcTransactions],
                icon: 'inbox',
            },
            {
                title: 'baza-nc.dividend.title',
                translate: true,
                routerLink: ['/baza-nc/dividends'],
                requiresACL: [BazaNorthCapitalAcl.BazaNcDividend],
                icon: 'dollar-circle',
                disabled: !(bazaNcBundleCmsConfig().withNcDividendsFeature || bazaNcBundleCmsConfig().withDwollaDividendsFeature),
            },
            {
                title: 'baza-nc.offering.title',
                translate: true,
                routerLink: ['/baza-nc/offerings'],
                requiresACL: [BazaNorthCapitalAcl.BazaNcOfferings],
                icon: 'container',
            },
            {
                title: 'baza-nc.purchaseFlow.kycLogs',
                translate: true,
                routerLink: ['/baza-nc/kyc-logs'],
                requiresACL: [BazaNorthCapitalAcl.BazaNcKycLogs],
                icon: 'monitor',
            },
            {
                title: 'baza-nc.report.title',
                translate: true,
                routerLink: ['/baza-nc/report'],
                requiresACL: [BazaNorthCapitalAcl.BazaNcReport],
                icon: 'snippets',
                disabled: !bazaNcBundleCmsConfig().withReportFeature,
            },
        ],
    },
];
