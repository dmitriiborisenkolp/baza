# baza-core-data-access

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-core-ng-endpoint` to execute the unit tests.

## Build

Run `nx build baza-core-data-access`

## Publish

Run `yarn publish:baza-core-data-access`
