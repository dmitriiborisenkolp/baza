import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaStatusDataAccess } from './ng/baza-status.data-access';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';

@NgModule({
    imports: [CommonModule, BazaDataAccessModule],
    providers: [BazaStatusDataAccess],
})
export class BazaStatusDataAccessModule {}
