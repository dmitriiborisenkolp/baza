import { Injectable } from '@angular/core';
import { BazaStatusEndpointPaths, BazaStatusIndexResponse, StatusEndpoint } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessService } from '../../../../../ng/src/lib/baza-data-access.service';
import { Observable } from 'rxjs';

@Injectable()
export class BazaStatusDataAccess implements StatusEndpoint {
    constructor(
        private readonly dataAccess: BazaDataAccessService,
    ) {}

    index(): Observable<BazaStatusIndexResponse> {
        return this.dataAccess.get(BazaStatusEndpointPaths.index);
    }
}
