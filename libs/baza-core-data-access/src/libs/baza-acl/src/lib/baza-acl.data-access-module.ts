import { NgModule } from '@angular/core';
import { BazaAclDataAccess } from './ng/baza-acl.data-access';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaAclDataAccess],
})
export class BazaAclDataAccessModule {}
