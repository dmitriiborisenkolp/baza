import { Injectable } from '@angular/core';
import { AclEndpoint, BazaAclCurrentResponse, BazaAclEndpointPaths, BazaAclGetAclRequest, BazaAclGetAclResponse, BazaAclSetAclRequest, BazaAclSetAclResponse } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessService } from '../../../../../ng/src/lib/baza-data-access.service';
import { Observable } from 'rxjs';

@Injectable()
export class BazaAclDataAccess implements AclEndpoint {
    constructor(
        private readonly ngEndpoint: BazaDataAccessService,
    ) {}

    current(): Observable<BazaAclCurrentResponse> {
        return this.ngEndpoint.post<BazaAclCurrentResponse>(BazaAclEndpointPaths.current);
    }

    getAcl(request: BazaAclGetAclRequest): Observable<BazaAclGetAclResponse> {
        return this.ngEndpoint.post<BazaAclGetAclResponse>(BazaAclEndpointPaths.getAcl, request);
    }

    setAcl(request: BazaAclSetAclRequest): Observable<BazaAclSetAclResponse> {
        return this.ngEndpoint.post<BazaAclSetAclResponse>(BazaAclEndpointPaths.setAcl, request);
    }
}
