import { NgModule } from '@angular/core';
import { BazaCreditCardDataAccess } from './ng/baza-credit-card.data-access';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaCreditCardDataAccess],
})
export class BazaCreditCardDataAccessModule {}
