import { Injectable } from '@angular/core';
import { BazaCreditCardDetailsRequest, BazaCreditCardDetailsResponse, BazaCreditCardEndpoint, BazaCreditCardEndpointPaths, BazaCreditCardIsExpiredRequest, BazaCreditCardIsExpiredResponse, BazaCreditCardValidateRequest, BazaCreditCardValidateResponse } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaDataAccessService } from '../../../../../ng/src/lib/baza-data-access.service';

@Injectable()
export class BazaCreditCardDataAccess implements BazaCreditCardEndpoint {
    constructor(
        private readonly ngEndpoint: BazaDataAccessService,
    ) {}

    validate(request: BazaCreditCardValidateRequest): Observable<BazaCreditCardValidateResponse> {
        return this.ngEndpoint.post<BazaCreditCardValidateResponse>(BazaCreditCardEndpointPaths.validate, request);
    }

    details(request: BazaCreditCardDetailsRequest): Observable<BazaCreditCardDetailsResponse> {
        return this.ngEndpoint.post<BazaCreditCardDetailsResponse>(BazaCreditCardEndpointPaths.details, request);
    }

    isExpired(request: BazaCreditCardIsExpiredRequest): Observable<BazaCreditCardIsExpiredResponse> {
        return this.ngEndpoint.post<BazaCreditCardIsExpiredResponse>(BazaCreditCardEndpointPaths.isExpired, request);
    }
}
