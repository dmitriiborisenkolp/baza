import { NgModule } from '@angular/core';
import { BazaRegistryDataAccess } from './ng/baza-registry.data-access';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaRegistryDataAccess],
})
export class BazaRegistryDataAccessModule {}
