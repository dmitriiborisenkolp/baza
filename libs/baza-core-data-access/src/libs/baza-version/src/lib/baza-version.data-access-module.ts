import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src';
import { BazaVersionDataAccess } from './ng/baza-version.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaVersionDataAccess],
})
export class BazaVersionDataAccessModule {}
