import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import type { VersionEndpoint, VersionDto } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessService } from '../../../../../ng/src/lib/baza-data-access.service';
import { VersionEndpointPaths } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaVersionDataAccess implements VersionEndpoint {
    constructor(
        private readonly ngEndpoint: BazaDataAccessService,
    ) {}

    version(): Observable<VersionDto> {
        return this.ngEndpoint.get<VersionDto>(VersionEndpointPaths.version);
    }
}
