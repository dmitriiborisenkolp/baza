import { Injectable } from '@angular/core';
import {
    BazaAwsDeleteRequest,
    BazaAwsEndpoint,
    BazaAwsEndpointPaths,
    BazaAwsPresignedUrlRequest,
    BazaAwsUploadResponse,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaDataAccessService } from '../../../../../ng/src/lib/baza-data-access.service';

@Injectable()
export class BazaAwsDataAccess implements BazaAwsEndpoint {
    constructor(private readonly ngEndpoint: BazaDataAccessService) {}

    upload(file: File): Observable<BazaAwsUploadResponse> {
        const formData = new FormData();

        formData.append('file', file);

        return this.ngEndpoint.post<BazaAwsUploadResponse>(BazaAwsEndpointPaths.upload, formData);
    }

    delete(request: BazaAwsDeleteRequest): Observable<void> {
        return this.ngEndpoint.post<void>(BazaAwsEndpointPaths.delete, request);
    }

    presignedUrl(request: BazaAwsPresignedUrlRequest): Observable<string> {
        return this.ngEndpoint.post<string>(BazaAwsEndpointPaths.presignedUrl, request, {
            responseType: 'text',
        });
    }
}
