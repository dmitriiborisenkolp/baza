import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';
import { BazaAwsDataAccess } from './ng/baza-aws.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaAwsDataAccess],
})
export class BazaAwsDataAccessModule {}
