import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';
import { BazaAccountDataAccess } from './ng/baza-account.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaAccountDataAccess],
})
export class BazaAccountDataAccessModule {}
