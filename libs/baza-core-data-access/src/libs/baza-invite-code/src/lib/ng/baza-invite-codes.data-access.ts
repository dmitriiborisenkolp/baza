import { Injectable } from '@angular/core';
import { BazaInviteCodeEndpoint, BazaInviteCodeEndpointPaths, BazaRequestForInviteCodesRequest, BazaRequestForInviteCodesResponse, BazaValidateInviteCodeRequest, BazaValidateInviteCodeResponse } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaInviteCodesDataAccess implements BazaInviteCodeEndpoint {
    constructor(
        private readonly http: BazaDataAccessService,
    ) {}

    validateInviteCode(request: BazaValidateInviteCodeRequest): Observable<BazaValidateInviteCodeResponse> {
        return this.http.post<BazaValidateInviteCodeResponse>(request, BazaInviteCodeEndpointPaths.validateInviteCode);
    }

    requestForInviteCodes(request: BazaRequestForInviteCodesRequest): Observable<BazaRequestForInviteCodesResponse> {
        return this.http.post<BazaRequestForInviteCodesResponse>(request, BazaInviteCodeEndpointPaths.requestForInviteCodes);
    }
}
