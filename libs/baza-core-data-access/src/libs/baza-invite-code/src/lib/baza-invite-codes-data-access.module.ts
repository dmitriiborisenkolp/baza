import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src';
import { BazaInviteCodesDataAccess } from './ng/baza-invite-codes.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaInviteCodesDataAccess],
})
export class BazaInviteCodesDataAccessModule {}
