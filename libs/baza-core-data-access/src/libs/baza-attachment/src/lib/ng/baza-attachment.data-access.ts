import { Injectable } from '@angular/core';
import type { AttachmentEndpoint, AttachmentUploadRequest, AttachmentUploadResponse } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaDataAccessService } from '../../../../../ng/src/lib/baza-data-access.service';
import { AttachmentEndpointPaths } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaAttachmentDataAccess implements AttachmentEndpoint {
    constructor(
        private readonly ngEndpoint: BazaDataAccessService,
    ) {}

    upload(file: File, request: AttachmentUploadRequest): Observable<AttachmentUploadResponse> {
        const formData = new FormData();

        formData.append('file', file);
        formData.append('request', JSON.stringify(request));

        return this.ngEndpoint.post<AttachmentUploadResponse>(AttachmentEndpointPaths.upload, formData);
    }
}
