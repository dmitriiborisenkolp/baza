import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';
import { BazaAttachmentDataAccess } from './ng/baza-attachment.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaAttachmentDataAccess],
})
export class BazaAttachmentDataAccessModule {}
