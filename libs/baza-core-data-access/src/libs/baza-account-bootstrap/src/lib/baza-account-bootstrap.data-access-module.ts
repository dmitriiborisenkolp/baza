import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src';
import { BazaAccountBootstrapDataAccess } from './ng/baza-account-bootstrap.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaAccountBootstrapDataAccess],
})
export class BazaAccountBootstrapDataAccessModule {}
