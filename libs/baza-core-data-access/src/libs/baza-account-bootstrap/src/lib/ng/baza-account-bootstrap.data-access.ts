import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '../../../../../ng/src';
import { AccountCmsBootstrapEndpoint, AccountCmsBootstrapEndpointPaths } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaAccountBootstrapDataAccess implements AccountCmsBootstrapEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    bootstrap(): Observable<void> {
        return this.http.get(AccountCmsBootstrapEndpointPaths.bootstrap);
    }
}
