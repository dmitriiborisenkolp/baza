import { NgModule } from '@angular/core';
import { BazaPasswordDataAccess } from './ng/baza-password.data-access';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaPasswordDataAccess],
})
export class BazaPasswordDataAccessModule {}
