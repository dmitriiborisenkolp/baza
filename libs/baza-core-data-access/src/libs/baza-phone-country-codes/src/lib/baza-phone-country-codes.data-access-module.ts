import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';
import { BazaPhoneCountryCodesDataAccess } from './ng/baza-phone-country-codes.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaPhoneCountryCodesDataAccess],
})
export class BazaPhoneCountryCodesDataAccessModule {}
