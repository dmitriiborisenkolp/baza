import { Injectable } from '@angular/core';
import {
    BazaE2eDisableFeatureRequest,
    BazaE2eEnable2FAForAccountRolesRequest,
    BazaE2eEnableFeatureRequest,
    BazaE2eEndpoint,
    BazaE2eEndpointPaths,
    BazaE2eMailDto,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaDataAccessService } from '../../../../../ng/src/lib/baza-data-access.service';

@Injectable()
export class BazaE2eDataAccess implements BazaE2eEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    flush(): Observable<void> {
        return this.http.post<void>(BazaE2eEndpointPaths.flush);
    }

    flushDatabase(): Observable<void> {
        return this.http.post<void>(BazaE2eEndpointPaths.flushDatabase);
    }

    flushRedis(): Observable<void> {
        return this.http.post<void>(BazaE2eEndpointPaths.flushRedis);
    }

    flushMailbox(): Observable<void> {
        return this.http.post<void>(BazaE2eEndpointPaths.flushMailbox);
    }

    mailbox(): Observable<Array<BazaE2eMailDto>> {
        return this.http.get<Array<BazaE2eMailDto>>(BazaE2eEndpointPaths.mailbox);
    }

    enable2FAForAccountRoles(request: BazaE2eEnable2FAForAccountRolesRequest): Observable<void> {
        return this.http.post(BazaE2eEndpointPaths.enable2FAForAccountRoles, request);
    }

    enableFeature(request: BazaE2eEnableFeatureRequest): Observable<void> {
        return this.http.post(BazaE2eEndpointPaths.enableFeature, request);
    }

    disableFeature(request: BazaE2eDisableFeatureRequest): Observable<void> {
        return this.http.post(BazaE2eEndpointPaths.disableFeature, request);
    }

    setEnv(request: Record<string, string>): Observable<void> {
        return this.http.post(BazaE2eEndpointPaths.setEnv, request);
    }

    resetEnv(): Observable<void> {
        return this.http.post(BazaE2eEndpointPaths.resetEnv);
    }
}
