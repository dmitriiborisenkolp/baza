import { NgModule } from '@angular/core';
import { BazaE2eDataAccess } from './ng/baza-e2e.data-access';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaE2eDataAccess],
})
export class BazaE2eDataAccessModule {}
