import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';
import { BazaE2eFixturesDataAccess } from './ng/baza-e2e-fixtures.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaE2eFixturesDataAccess],
})
export class BazaE2eFixturesDataAccessModule {}
