import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BazaE2eFixturesEndpoint, BazaE2eFixturesEndpointPaths, BazaE2eFixturesUpRequest } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessService } from '../../../../../ng/src/lib/baza-data-access.service';

@Injectable()
export class BazaE2eFixturesDataAccess implements BazaE2eFixturesEndpoint {
    constructor(
        private readonly ngEndpoint: BazaDataAccessService,
    ) {}

    up(request: BazaE2eFixturesUpRequest): Observable<void> {
        return this.ngEndpoint.post(BazaE2eFixturesEndpointPaths.up, request);
    }
}
