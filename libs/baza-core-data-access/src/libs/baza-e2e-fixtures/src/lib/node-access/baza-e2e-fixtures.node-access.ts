import { BazaDataAccessNode } from '../../../../../ng/src/lib/baza-data-access.node';
import { BazaE2eFixturesEndpoint, BazaE2eFixturesEndpointPaths, BazaE2eFixturesUpRequest } from '@scaliolabs/baza-core-shared';

export class BazaE2eFixturesNodeAccess implements BazaE2eFixturesEndpoint {
    constructor(
        private readonly http: BazaDataAccessNode,
    ) {}

    async up(request: BazaE2eFixturesUpRequest): Promise<void> {
        return this.http.post(BazaE2eFixturesEndpointPaths.up, request, {
            asJsonResponse: false,
        });
    }
}
