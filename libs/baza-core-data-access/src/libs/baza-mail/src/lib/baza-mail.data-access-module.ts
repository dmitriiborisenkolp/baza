import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src';
import { BazaMailDataAccess } from './ng/baza-mail.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaMailDataAccess],
})
export class BazaMailDataAccessModule {}
