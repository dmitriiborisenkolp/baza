import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '../../../../../ng/src';
import {
    BazaMailEndpoint,
    BazaMailEndpointPaths,
    BazaMailIsReadyToUseResponse,
    BazaMailTemplate,
    replacePathArgs,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';

/**
 * Data-Access Service for Baza-Core - Mail module
 */
@Injectable()
export class BazaMailDataAccess implements BazaMailEndpoint {
    constructor(private readonly dataAccess: BazaDataAccessService) {}

    /**
     * Returns Ready status for current selected Mail Transport
     */
    isReadyToUse(): Observable<BazaMailIsReadyToUseResponse> {
        return this.dataAccess.get(BazaMailEndpointPaths.isReadyToUse);
    }

    /**
     * Returns list of MailTemplate definitions
     * Used for Documentation Purposed
     */
    mailTemplates(): Observable<Array<BazaMailTemplate>> {
        return this.dataAccess.get(BazaMailEndpointPaths.mailTemplates);
    }

    /**
     * Returns Mail Templates which are associated with given Tag
     * @param tag
     */
    mailTemplatesOfTag(tag: string): Observable<Array<BazaMailTemplate>> {
        return this.dataAccess.get(replacePathArgs(BazaMailEndpointPaths.mailTemplatesOfTag, { tag }));
    }

    /**
     * Returns all Mail Template Tags
     */
    mailTemplatesTags(): Observable<Array<string>> {
        return this.dataAccess.get(BazaMailEndpointPaths.mailTemplatesTags);
    }

    /**
     * Returns Mail Template by Name
     * @param name
     */
    mailTemplate(name: string): Observable<BazaMailTemplate> {
        return this.dataAccess.get(replacePathArgs(BazaMailEndpointPaths.mailTemplate, { name }));
    }
}
