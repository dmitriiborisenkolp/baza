import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '../../../../ng/src/lib/baza-data-access.module';
import { BazaI18nDataAccess } from './ng/baza-i18n.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaI18nDataAccess],
})
export class BazaI18nDataAccessModule {}
