import {
    Application,
    AuthEndpointPaths,
    AuthRequest,
    AuthResponse,
    BazaHttpHeaders,
    isBazaErrorResponse,
    replacePathArgs,
    withoutEndingSlash,
    withoutTrailingSlash,
} from '@scaliolabs/baza-core-shared';
import * as process from 'process';
import * as qp from 'querystring';
import { RequestInit, Response } from 'node-fetch';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const fetch = require('node-fetch');

export interface BazaNgEndpointNodeOptions {
    asStatusCode: boolean;
    asJsonResponse: boolean;
    withJwt: boolean;
    asFormData: boolean;
    withHeaders?: any;
    withApplication?: Application;
}

const defaultOptions: () => BazaNgEndpointNodeOptions = () => ({
    asStatusCode: false,
    asJsonResponse: true,
    withJwt: true,
    asFormData: false,
});

export class BazaDataAccessNode {
    private apiEndpointUrl: string = process.env.BAZA_E2E_ENDPOINT;
    private _authResponse: AuthResponse;
    private _optionsForNextRequest: Partial<BazaNgEndpointNodeOptions>;

    get baseUrl(): string {
        return withoutEndingSlash(this.apiEndpointUrl);
    }

    set baseUrl(baseUrl: string) {
        this.apiEndpointUrl = baseUrl;
    }

    set optionsForNextRequest(options: Partial<BazaNgEndpointNodeOptions>) {
        this._optionsForNextRequest = options;
    }

    url(endpoint: any): string {
        if (!this.baseUrl) {
            throw new Error('No baseUrl set for BazaDataAccessNode!');
        }

        return `${this.baseUrl}/${withoutTrailingSlash(endpoint as any)}`;
    }

    async auth(request: AuthRequest): Promise<void> {
        await this.post<AuthResponse>(AuthEndpointPaths.auth, request, {
            asJsonResponse: true,
            withJwt: false,
            withApplication: Application.API,
        }).then((response) => {
            if (isBazaErrorResponse(response)) {
                console.error(response);

                throw new Error('[BazaDataAccessNode] Failed to auth');
            }

            this._authResponse = response;
        });
    }

    async authE2eUser(): Promise<void> {
        await this.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });
    }

    async authE2eUser2(): Promise<void> {
        await this.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        });
    }

    async authE2eAdmin(): Promise<void> {
        await this.auth({
            email: 'e2e-admin@scal.io',
            password: 'e2e-admin-password',
        });
    }

    async authE2eAdmin2(): Promise<void> {
        await this.auth({
            email: 'e2e-admin-2@scal.io',
            password: 'e2e-admin-password',
        });
    }

    noAuth(): void {
        this._authResponse = undefined;
    }

    get authResponse(): AuthResponse {
        return this._authResponse;
    }

    path(path: string, args: Record<string, unknown>): string {
        return replacePathArgs(path, args);
    }

    get<R, T = any>(endpoint: T, request?: any, withOptions: Partial<BazaNgEndpointNodeOptions> = {}): Promise<R> {
        const options = this.options(withOptions);

        let url = this.url(endpoint);

        if (request) {
            url += '?' + qp.encode(request);
        }

        return this.response<R>(fetch(url, this.request('get', options, request)), options);
    }

    post<R, T = any>(endpoint: T, request: any = {}, withOptions: Partial<BazaNgEndpointNodeOptions> = {}): Promise<R> {
        const options = this.options(withOptions);

        return this.response<R>(fetch(this.url(endpoint), this.request('post', options, request)), options);
    }

    patch<R, T = any>(endpoint: T, request: any = {}, withOptions: Partial<BazaNgEndpointNodeOptions> = {}): Promise<R> {
        const options = this.options(withOptions);

        return this.response<R>(fetch(this.url(endpoint), this.request('patch', options, request)), options);
    }

    put<R, T = any>(endpoint: T, request: any, withOptions: Partial<BazaNgEndpointNodeOptions> = {}): Promise<R> {
        const options = this.options(withOptions);

        return this.response<R>(fetch(this.url(endpoint), this.request('put', options, request)), options);
    }

    head<R, T = any>(endpoint: T, withOptions: Partial<BazaNgEndpointNodeOptions> = {}): Promise<R> {
        const options = this.options(withOptions);

        return fetch(this.url(endpoint), this.request('head', options)).then((response) => response.text());
    }

    delete<R, T = any>(endpoint: T, withOptions: Partial<BazaNgEndpointNodeOptions> = {}): Promise<R> {
        const options = this.options(withOptions);

        return this.response<R>(fetch(this.url(endpoint), this.request('delete', options)), options);
    }

    private options(withOptions: Partial<BazaNgEndpointNodeOptions>): BazaNgEndpointNodeOptions {
        const options: BazaNgEndpointNodeOptions = {
            ...defaultOptions(),
            ...withOptions,
        };

        if (this._optionsForNextRequest) {
            Object.assign(options, this._optionsForNextRequest);

            this._optionsForNextRequest = undefined;
        }

        return options;
    }

    private request(method: string, options: BazaNgEndpointNodeOptions, request?: any): RequestInit {
        if (options.asFormData) {
            return {
                method,
                body: request,
                headers: {
                    ...request.getHeaders(),
                    'Content-Type': 'application/json',
                    Authorization: this._authResponse && options.withJwt ? `Bearer ${this._authResponse.accessToken}` : undefined,
                    [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '1',
                    [BazaHttpHeaders.HTTP_HEADER_VERSION]: BazaHttpHeaders.HTTP_HEADER_VERSION_SKIP,
                    [BazaHttpHeaders.HTTP_HEADER_APP]: options.withApplication ? options.withApplication : Application.WEB,
                    ...options.withHeaders,
                },
            };
        } else {
            return {
                method,
                body: request && (method || '').toLowerCase() !== 'get' ? JSON.stringify(request) : undefined,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: this._authResponse && options.withJwt ? `Bearer ${this._authResponse.accessToken}` : undefined,
                    [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '1',
                    [BazaHttpHeaders.HTTP_HEADER_VERSION]: BazaHttpHeaders.HTTP_HEADER_VERSION_SKIP,
                    [BazaHttpHeaders.HTTP_HEADER_APP]: options.withApplication ? options.withApplication : Application.WEB,
                    ...options.withHeaders,
                },
            };
        }
    }

    private response<T>(nodeFetchResponse: Promise<Response>, options: BazaNgEndpointNodeOptions): Promise<T> {
        return nodeFetchResponse.then((response) => {
            if (options.asStatusCode) {
                return response.status;
            }

            if (response.status === 204) {
                return undefined;
            } else if (response.status >= 200 && response.status <= 299) {
                return options.asJsonResponse === false ? response.text() : response.json();
            } else {
                return response.json();
            }
        });
    }
}
