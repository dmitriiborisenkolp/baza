import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypesNewsErrorCodes, bazaContentTypesNewsErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaContentTypeNewsNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesNewsErrorCodes.BazaContentTypesNewsNotFound,
            bazaContentTypesNewsErrorCodesI18n[BazaContentTypesNewsErrorCodes.BazaContentTypesNewsNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
