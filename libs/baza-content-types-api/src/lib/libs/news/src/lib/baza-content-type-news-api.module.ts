import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { NewsController } from './controllers/news.controller';
import { NewsCmsController } from './controllers/news-cms.controller';
import { NewsRepository } from './repositories/news.repository';
import { NewsMapper } from './mappers/news.mapper';
import { NewsCmsMapper } from './mappers/news-cms.mapper';
import { NewsService } from './services/news.service';
import { NewsCmsService } from './services/news-cms.service';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        NewsController,
        NewsCmsController,
    ],
    providers: [
        NewsRepository,
        NewsMapper,
        NewsCmsMapper,
        NewsService,
        NewsCmsService,
    ],
    exports: [
        NewsRepository,
        NewsMapper,
        NewsCmsMapper,
        NewsService,
        NewsCmsService,
    ],
})
export class BazaContentTypeNewsApiModule {}
