import { Injectable } from '@nestjs/common';
import { NEWS_RELATIONS, NewsRepository } from '../repositories/news.repository';
import { NewsMapper } from '../mappers/news.mapper';
import { CrudService } from '@scaliolabs/baza-core-api';
import { NewsDto, NewsListRequest, NewsListResponse } from '@scaliolabs/baza-content-types-shared';
import { NewsEntity } from '../entities/news.entity';
import { BazaContentTypeNewsNotFoundException } from '../exceptions/baza-content-type-news-not-found.exception';

@Injectable()
export class NewsService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: NewsMapper,
        private readonly repository: NewsRepository,
    ) {}

    async list(request: NewsListRequest): Promise<NewsListResponse> {
        return this.crud.find<NewsEntity, NewsDto>({
            request,
            entity: NewsEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
                relations: NEWS_RELATIONS,
                where: [{
                    isPublished: true,
                }],
            },
            withMaxSortOrder: true,
        });
    }

    async getById(id: number): Promise<NewsEntity> {
        const entity = await this.repository.getById(id);

        if (! entity.isPublished) {
            throw new BazaContentTypeNewsNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<NewsEntity> {
        const entity = await this.repository.getByUrl(url);

        if (! entity.isPublished) {
            throw new BazaContentTypeNewsNotFoundException();
        }

        return entity;
    }
}
