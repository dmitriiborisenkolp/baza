import { Injectable } from "@nestjs/common";
import { Connection, Repository } from 'typeorm';
import { NewsEntity } from '../entities/news.entity';
import { BazaContentTypeNewsNotFoundException } from '../exceptions/baza-content-type-news-not-found.exception';

export const NEWS_RELATIONS = [
    'primaryCategory',
    'additionalCategories',
];

@Injectable()
export class NewsRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<NewsEntity> {
        return this.connection.getRepository(NewsEntity);
    }

    async save(entities: Array<NewsEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<NewsEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<NewsEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: NEWS_RELATIONS,
        });
    }

    async getById(id: number): Promise<NewsEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BazaContentTypeNewsNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<NewsEntity> {
        const entity = await this.repository.findOne({
            where: [{
                url,
            }],
            relations: NEWS_RELATIONS,
        });

        if (! entity) {
            throw new BazaContentTypeNewsNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<NewsEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'ASC',
            },
            relations: NEWS_RELATIONS,
        });
    }

    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return result.max || 0;
    }
}
