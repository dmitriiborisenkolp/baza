import { Body, Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypesOpenApi,
    NewsDto,
    NewsEndpoint,
    NewsEndpointPaths,
    NewsListRequest,
    NewsListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { NewsMapper } from '../mappers/news.mapper';
import { NewsService } from '../services/news.service';

@Controller()
@ApiTags(BazaContentTypesOpenApi.News)
export class NewsController implements NewsEndpoint {
    constructor(private readonly mapper: NewsMapper, private readonly service: NewsService) {}

    @Get(NewsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List news',
    })
    @ApiOkResponse({
        type: NewsListResponse,
    })
    async list(@Body() request: NewsListRequest): Promise<NewsListResponse> {
        return this.service.list(request);
    }

    @Get(NewsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns news record by Id',
    })
    @ApiOkResponse({
        type: NewsDto,
    })
    async getById(@Param('id') id: number): Promise<NewsDto> {
        const entity = await this.service.getById(id);

        return this.mapper.entityToDTO(entity);
    }

    @Get(NewsEndpointPaths.getByUrl)
    @ApiOperation({
        summary: 'getByUrl',
        description: 'Returns news record by URL',
    })
    @ApiOkResponse({
        type: NewsDto,
    })
    async getByUrl(@Param('url') url: string): Promise<NewsDto> {
        const entity = await this.service.getByUrl(url);

        return this.mapper.entityToDTO(entity);
    }
}
