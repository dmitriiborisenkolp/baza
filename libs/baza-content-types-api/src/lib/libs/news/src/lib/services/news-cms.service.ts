import { Injectable } from '@nestjs/common';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { NEWS_RELATIONS, NewsRepository } from '../repositories/news.repository';
import { NewsCmsMapper } from '../mappers/news-cms.mapper';
import { NewsCmsCreateRequest, NewsCmsDeleteRequest, NewsCmsDto, NewsCmsEntityBody, NewsCmsListRequest, NewsCmsListResponse, NewsCmsSetSortOrderRequest, NewsCmsSetSortOrderResponse, NewsCmsUpdateRequest } from '@scaliolabs/baza-content-types-shared';
import { NewsEntity } from '../entities/news.entity';
import { CategoryRepository } from '../../../../categories/src';

@Injectable()
export class NewsCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: NewsRepository,
        private readonly categoryRepository: CategoryRepository,
        private readonly mapper: NewsCmsMapper,
    ) {}

    async create(request: NewsCmsCreateRequest): Promise<NewsEntity> {
        const entity = new NewsEntity();

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: NewsCmsUpdateRequest): Promise<NewsEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: NewsCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);

        const entities = await this.repository.findAll();

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async list(request: NewsCmsListRequest): Promise<NewsCmsListResponse> {
        return this.crud.find<NewsEntity, NewsCmsDto>({
            entity: NewsEntity,
            request,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
                relations: NEWS_RELATIONS,
            },
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: NewsCmsSetSortOrderRequest): Promise<NewsCmsSetSortOrderResponse> {
        const entities = await this.repository.findAll();

        const response = await this.crudSort.setSortOrder<NewsEntity, NewsCmsDto>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: entities,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });

        await this.repository.save(entities);

        return response;
    }

    async populate(target: NewsEntity, entityBody: NewsCmsEntityBody): Promise<void> {
        if (! target.id) {
            target.sortOrder = await this.repository.maxSortOrder() + 1;
        }

        if (! target.dateCreatedAt) {
            target.dateCreatedAt = new Date();
        }

        target.title = entityBody.title;
        target.isPublished = entityBody.isPublished;
        target.url = entityBody.seo && entityBody.seo.urn
            ? entityBody.seo.urn
            : undefined;
        target.datePublishedAt = entityBody.datePublishedAt
            ? new Date(entityBody.datePublishedAt)
            : null;
        target.primaryCategory = entityBody.primaryCategoryId
            ? await this.categoryRepository.getById(entityBody.primaryCategoryId)
            : null;
        target.additionalCategories = entityBody.additionalCategoryIds
            ? await this.categoryRepository.findByIds(entityBody.additionalCategoryIds)
            : [];
        target.title = entityBody.title;
        target.link = entityBody.link;
        target.imageAwsS3Key = entityBody.imageAwsS3Key;
        target.previewAwsS3Key = entityBody.previewAwsS3Key;
        target.contents = entityBody.contents;
        target.seo = entityBody.seo;
    }
}
