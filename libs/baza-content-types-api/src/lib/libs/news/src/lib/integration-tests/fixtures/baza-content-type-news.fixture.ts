import { Injectable } from '@nestjs/common';
import { emptyBazaContentDto, NewsCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { NewsCmsService } from '../../services/news-cms.service';
import { BazaContentTypesNewsFixtures } from '../baza-content-types-news.fixtures';

export const E2E_BAZA_CONTENT_TYPES_NEWS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    request: NewsCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'News 1',
            link: 'https://scal.io/news/news-1',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'news-1',
            },
        },
    },
    {
        $refId: 2,
        request: {
            isPublished: false,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'News 2',
            link: 'https://scal.io/news/news-2',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'news-2',
            },
        },
    },
    {
        $refId: 3,
        request: {
            isPublished: true,
            datePublishedAt: 'Tue Aug 20 2021 15:11:57 GMT+0300',
            title: 'News 3',
            link: 'https://scal.io/news/news-3',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'news-3',
            },
        },
    },
];

@Injectable()
export class BazaContentTypeNewsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: NewsCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_NEWS_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.request);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesNewsFixtures>([{
    fixture: BazaContentTypesNewsFixtures.BazaContentTypesNews,
    provider: BazaContentTypeNewsFixture,
}]);
