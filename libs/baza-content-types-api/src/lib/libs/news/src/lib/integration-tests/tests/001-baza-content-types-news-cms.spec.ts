import 'reflect-metadata';
import { NewsCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-content-types/news/001-baza-content-types-news-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new NewsCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new news record', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'News 1',
            link: 'https://scal.io/news/news-1',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'news-1',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new news record in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].title).toBe('News 1');
        expect(listResponse.items[0].sortOrder).toBe(1);
    });

    it('will successfully create new news record without link', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'News 2',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'news-2',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ID = response.id;
    });

    it('will successfully returns news record  by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('News 2');
    });

    it('will successfully update news record', async () => {
        const response = await dataAccessCms.update({
            id: ID,
            isPublished: true,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'News 2 *',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'news-2',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updates in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).toBe(ID);
        expect(listResponse.items[0].title).toBe('News 2 *');
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[1].id).not.toBe(ID);
        expect(listResponse.items[1].sortOrder).toBe(1);
        expect(listResponse.items[1].title).toBe('News 1');

        ID = listResponse.items[0].id;
    });

    it('will successfully update sort order of news record', async () => {
        const response = await dataAccessCms.setSortOrder({
            id: ID,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updated sort order in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[0].title).toBe('News 1');
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].title).toBe('News 2 *');
        expect(listResponse.items[1].sortOrder).toBe(1);

        ID = listResponse.items[1].id;
    });

    it('will successfully delete news record', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted team member in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].title).toBe('News 1');

        ID = listResponse.items[0].id;
    });
});
