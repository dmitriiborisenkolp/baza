import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    NewsCmsCreateRequest,
    NewsCmsDeleteRequest,
    NewsCmsDto,
    NewsCmsEndpoint,
    NewsCmsEndpointPaths,
    NewsCmsGetByIdRequest,
    NewsCmsListRequest,
    NewsCmsListResponse,
    NewsCmsSetSortOrderRequest,
    NewsCmsSetSortOrderResponse,
    NewsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { NewsCmsMapper } from '../mappers/news-cms.mapper';
import { NewsRepository } from '../repositories/news.repository';
import { NewsCmsService } from '../services/news-cms.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.News)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesNews])
export class NewsCmsController implements NewsCmsEndpoint {
    constructor(
        private readonly mapper: NewsCmsMapper,
        private readonly repository: NewsRepository,
        private readonly service: NewsCmsService,
    ) {}

    @Post(NewsCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: NewsCmsDto,
    })
    async create(@Body() request: NewsCmsCreateRequest): Promise<NewsCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(NewsCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: NewsCmsDto,
    })
    async update(@Body() request: NewsCmsUpdateRequest): Promise<NewsCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(NewsCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: NewsCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(NewsCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: NewsCmsListResponse,
    })
    async list(@Body() request: NewsCmsListRequest): Promise<NewsCmsListResponse> {
        return this.service.list(request);
    }

    @Post(NewsCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: NewsCmsDto,
    })
    async getById(@Body() request: NewsCmsGetByIdRequest): Promise<NewsCmsDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(NewsCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: NewsCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: NewsCmsSetSortOrderRequest): Promise<NewsCmsSetSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
