import 'reflect-metadata';
import { NewsCmsNodeAccess, NewsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaContentTypesNewsErrorCodes, NewsCmsListResponse } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesNewsFixtures } from '../baza-content-types-news.fixtures';

describe('@scaliolabs/baza-content-types/news/002-baza-content-types-news.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new NewsNodeAccess(http);
    const dataAccessCms = new NewsCmsNodeAccess(http);

    let ID: number;
    let ID_UNPUBLISHED: number;

    let CMS_LIST_RESPONSE: NewsCmsListResponse;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesNewsFixtures.BazaContentTypesNews],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        CMS_LIST_RESPONSE = await dataAccessCms.list({});
        ID_UNPUBLISHED = CMS_LIST_RESPONSE.items.find((e) => !e.isPublished).id;
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will successfully returns list of published news', async () => {
        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('News 3');
        expect(response.items[1].title).toBe('News 1');

        ID = response.items[0].id;
    });

    it('will successfully returns published news record by id', async () => {
        const response = await dataAccess.getById(ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('News 3');
    });

    it('will successfully returns published news record by url', async () => {
        const response = await dataAccess.getByUrl('news-3');

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('News 3');
    });

    it('will not returns unpublished news record by id', async () => {
        const response: BazaError = (await dataAccess.getById(ID_UNPUBLISHED)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesNewsErrorCodes.BazaContentTypesNewsNotFound);
    });

    it('will not returns unpublished news record by url', async () => {
        const response: BazaError = (await dataAccess.getByUrl('news-2')) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesNewsErrorCodes.BazaContentTypesNewsNotFound);
    });
});
