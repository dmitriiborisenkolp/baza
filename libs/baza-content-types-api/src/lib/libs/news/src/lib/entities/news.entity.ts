import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { bazaSeoDefault, BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { BazaContentTypeCategoryEntity } from '../../../../categories/src';
import { BazaContentDto, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';

@Entity({
    name: 'baza_content_types_news_entity',
})
export class NewsEntity  implements CrudSortableEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    sortOrder: number;

    @Column()
    isPublished: boolean;

    @Column({
        nullable: true,
    })
    url?: string;

    @Column()
    dateCreatedAt: Date = new Date();

    @Column()
    datePublishedAt: Date;

    @ManyToOne(() => BazaContentTypeCategoryEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
    })
    primaryCategory: BazaContentTypeCategoryEntity;

    @ManyToMany(() => BazaContentTypeCategoryEntity)
    @JoinTable()
    additionalCategories: Array<BazaContentTypeCategoryEntity>;

    @Column()
    title: string;

    @Column({
        nullable: true,
    })
    link?: string;

    @Column({
        nullable: true,
    })
    imageAwsS3Key?: string;

    @Column({
        nullable: true,
    })
    previewAwsS3Key?: string;

    @Column({
        type: 'jsonb',
        default: emptyBazaContentDto(),
    })
    contents: BazaContentDto;

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto;
}
