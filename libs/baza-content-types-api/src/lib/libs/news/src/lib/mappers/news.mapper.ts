import { Injectable } from '@nestjs/common';
import { AwsService } from '@scaliolabs/baza-core-api';
import { NewsEntity } from '../entities/news.entity';
import { NewsDto } from '@scaliolabs/baza-content-types-shared';
import { CategoryListMapper } from '../../../../categories/src';

@Injectable()
export class NewsMapper {
    constructor(
        private readonly aws: AwsService,
        private readonly categoryMapper: CategoryListMapper,
    ) {}

    async entityToDTO(input: NewsEntity): Promise<NewsDto> {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            url: input.url,
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            datePublishedAt: input.datePublishedAt
                ? input.datePublishedAt.toISOString()
                : undefined,
            primaryCategory: input.primaryCategory
                ? await this.categoryMapper.entityToDTO(input.primaryCategory)
                : undefined,
            additionalCategories: await  this.categoryMapper.entitiesToDTOs(input.additionalCategories || []),
            title: input.title,
            link: input.link,
            imageUrl: input.imageAwsS3Key
                ? await this.aws.presignedUrl({ s3ObjectId: input.imageAwsS3Key })
                : undefined,
            previewImageUrl: input.imageAwsS3Key
                ? await this.aws.presignedUrl({ s3ObjectId: input.previewAwsS3Key })
                : undefined,
            contents: input.contents,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<NewsEntity>): Promise<Array<NewsDto>> {
        const result: Array<NewsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
     }
}
