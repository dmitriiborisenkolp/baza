import { Injectable } from "@nestjs/common";
import { NewsMapper } from './news.mapper';
import { NewsEntity } from '../entities/news.entity';
import { NewsCmsDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class NewsCmsMapper {
    constructor(
        private readonly baseMapper: NewsMapper,
    ) {}

    async entityToDTO(input: NewsEntity): Promise<NewsCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            isPublished: input.isPublished,
            imageAwsS3Key: input.imageAwsS3Key,
            previewAwsS3Key: input.previewAwsS3Key,
        };
    }

    async entitiesToDTOs(input: Array<NewsEntity>): Promise<Array<NewsCmsDto>> {
        const result: Array<NewsCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
