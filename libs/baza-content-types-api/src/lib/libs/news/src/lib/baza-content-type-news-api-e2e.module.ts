import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypeNewsApiModule } from './baza-content-type-news-api.module';
import { BazaContentTypeNewsFixture } from './integration-tests/fixtures/baza-content-type-news.fixture';

const E2E_FIXTURES = [
    BazaContentTypeNewsFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypeNewsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypeNewsApiE2eModule {}
