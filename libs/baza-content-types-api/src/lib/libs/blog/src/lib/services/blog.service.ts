import { Injectable } from '@nestjs/common';
import { BlogRepository } from '../repositories/blog-repository.service';
import { BlogEntity } from '../entities/blog.entity';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { BlogDto, BlogListRequest, BlogListResponse, BlogSetSortOrderRequest, BlogSetSortOrderResponse } from '@scaliolabs/baza-content-types-shared';
import { BlogMapper } from '../mappers/blog-mapper.service';

@Injectable()
export class BlogService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: BlogRepository,
        private readonly mapper: BlogMapper,
    ) {}

    async list(request: BlogListRequest): Promise<BlogListResponse> {
        return this.crud.find<BlogEntity, BlogDto>({
            entity: BlogEntity,
            request,
            mapper: async(items) => this.mapper.entitiesToDTOs(items.filter((e) => e.isPublished)),
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
            },
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: BlogSetSortOrderRequest): Promise<BlogSetSortOrderResponse> {
        const entities = await this.repository.findAll();

        const response = await this.crudSort.setSortOrder<BlogEntity, BlogDto>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: entities,
            mapper: async(items) => this.mapper.entitiesToDTOs(items),
        });

        await this.repository.save(entities);

        return response;
    }
}
