import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { bazaSeoDefault, BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { BazaContentDto, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypeCategoryEntity } from '../../../../categories/src';

@Entity({
    name: 'baza_content_types_blog_entity',
})
export class BlogEntity implements CrudSortableEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        nullable: true,
    })
    url?: string;

    @Column()
    dateCreatedAt: Date;

    @Column({
        nullable: true,
    })
    datePublishedAt?: Date;

    @Column()
    sortOrder: number;

    @Column()
    isPublished: boolean;

    @Column()
    title: string;

    @Column({
        type: 'jsonb',
        default: emptyBazaContentDto(),
    })
    contents: BazaContentDto;

    @ManyToOne(() => BazaContentTypeCategoryEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
    })
    primaryCategory: BazaContentTypeCategoryEntity;

    @ManyToMany(() => BazaContentTypeCategoryEntity)
    @JoinTable()
    additionalCategories: Array<BazaContentTypeCategoryEntity>;

    @Column({
        nullable: true,
    })
    headerImgS3Key?: string;

    @Column({
        nullable: true,
    })
    thumbnailImgS3Key?: string;

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto;
}
