import { Injectable } from '@nestjs/common';
import { BlogCmsCreateRequest, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BlogCmsService } from '../../services/blog-cms.service';
import { BazaContentTypesBlogFixtures } from '../baza-content-types-blog.fixtures';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';

export const E2E_BAZA_CONTENT_TYPES_BLOG_FIXTURE: Array<{
    request: BlogCmsCreateRequest;
}> = [
    {
        request: {
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'Blog Record 1',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'blog-record-1',
            },
        },
    },
    {
        request: {
            isPublished: false,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'Blog Record 2',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'blog-record-2',
            },
        },
    },
    {
        request: {
            isPublished: true,
            datePublishedAt: 'Tue Aug 22 2021 15:11:57 GMT+0300',
            title: 'Blog Record 3',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'blog-record-3',
            },
        },
    },
];

@Injectable()
export class BazaContentTypeBlogFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BlogCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_BLOG_FIXTURE) {
            await this.service.create(fixtureRequest.request);
        }
    }
}

defineE2eFixtures<BazaContentTypesBlogFixtures>([{
    fixture: BazaContentTypesBlogFixtures.BazaContentTypesBlog,
    provider: BazaContentTypeBlogFixture,
}]);
