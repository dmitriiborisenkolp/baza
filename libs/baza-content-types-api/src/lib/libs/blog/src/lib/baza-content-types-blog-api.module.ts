import { Module } from "@nestjs/common";
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BlogCmsMapper } from './mappers/blog-cms-mapper.service';
import { BlogMapper } from './mappers/blog-mapper.service';
import { BlogController } from './controllers/blog.controller';
import { BlogCmsController } from './controllers/blog-cms.controller';
import { BlogRepository } from './repositories/blog-repository.service';
import { BlogCmsService } from './services/blog-cms.service';
import { BlogService } from "./services/blog.service";

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        BlogController,
        BlogCmsController,
    ],
    providers: [
        BlogMapper,
        BlogCmsMapper,
        BlogRepository,
        BlogService,
        BlogCmsService,
    ],
    exports: [
        BlogMapper,
        BlogCmsMapper,
        BlogRepository,
        BlogService,
        BlogCmsService,
    ],
})
export class BazaContentTypesBlogApiModule {
}
