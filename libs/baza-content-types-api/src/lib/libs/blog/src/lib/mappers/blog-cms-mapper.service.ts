import { Injectable } from "@nestjs/common";
import { BlogEntity } from '../entities/blog.entity';
import { BlogCmsDto } from '@scaliolabs/baza-content-types-shared';
import { BlogMapper } from './blog-mapper.service';

@Injectable()
export class BlogCmsMapper {
    constructor(
        private readonly baseMapper: BlogMapper,
    ) {}

    async entityToDTO(input: BlogEntity): Promise<BlogCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            headerImgS3Key: input.headerImgS3Key,
            thumbnailImgS3Key: input.thumbnailImgS3Key,
        };
    }

    async entitiesToDTOs(input: Array<BlogEntity>): Promise<Array<BlogCmsDto>> {
        const result: Array<BlogCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
