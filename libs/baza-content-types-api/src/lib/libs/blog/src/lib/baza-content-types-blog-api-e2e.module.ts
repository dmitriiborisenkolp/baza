import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypesBlogApiModule } from './baza-content-types-blog-api.module';
import { BazaContentTypeBlogFixture } from './integration-tests/fixtures/baza-content-type-blog.fixture';

const E2E_FIXTURES = [
    BazaContentTypeBlogFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypesBlogApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypesBlogApiE2eModule {}
