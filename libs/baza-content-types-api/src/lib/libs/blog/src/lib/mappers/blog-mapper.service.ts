import { Injectable } from "@nestjs/common";
import { BlogEntity } from '../entities/blog.entity';
import { BlogDto } from '@scaliolabs/baza-content-types-shared';
import { AwsService } from '@scaliolabs/baza-core-api';
import { CategoryListMapper } from '../../../../categories/src';

@Injectable()
export class BlogMapper {
    constructor(
        private readonly aws: AwsService,
        private readonly categoryMapper: CategoryListMapper,
    ) {}

    async entityToDTO(input: BlogEntity): Promise<BlogDto> {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            isPublished: input.isPublished,
            title: input.title,
            contents: input.contents,
            primaryCategory: input.primaryCategory
                ? await this.categoryMapper.entityToDTO(input.primaryCategory)
                : undefined,
            additionalCategories: await this.categoryMapper.entitiesToDTOs(input.additionalCategories || []),
            headerImgUrl: input.headerImgS3Key
                ? await this.aws.presignedUrl({ s3ObjectId: input.headerImgS3Key })
                : undefined,
            thumbnailImgUrl: input.thumbnailImgS3Key
                ? await this.aws.presignedUrl({ s3ObjectId: input.thumbnailImgS3Key })
                : undefined,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<BlogEntity>): Promise<Array<BlogDto>> {
        const result: Array<BlogDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
