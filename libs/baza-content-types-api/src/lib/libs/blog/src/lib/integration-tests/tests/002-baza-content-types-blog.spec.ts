import 'reflect-metadata';
import { BlogCmsNodeAccess, BlogNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaContentTypesBlogErrorCodes, BlogListResponse } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesBlogFixtures } from '../baza-content-types-blog.fixtures';

describe('@scaliolabs/baza-content-types/blog/002-baza-content-types-blog-members.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BlogNodeAccess(http);
    const dataAccessCms = new BlogCmsNodeAccess(http);

    let ID: number;
    let ID_UNPUBLISHED: number;

    let CMS_LIST_RESPONSE: BlogListResponse;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesBlogFixtures.BazaContentTypesBlog],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        CMS_LIST_RESPONSE = await dataAccessCms.list({});
        ID_UNPUBLISHED = CMS_LIST_RESPONSE.items.find((e) => !e.isPublished).id;
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will successfully returns list of published blogs', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Blog Record 3');
        expect(response.items[1].title).toBe('Blog Record 1');

        ID = response.items[0].id;
    });

    it('will successfully returns published blog by id', async () => {
        const response = await dataAccess.getById(ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Blog Record 3');
    });

    it('will not returns unpublished blog by id', async () => {
        const response: BazaError = (await dataAccess.getById(ID_UNPUBLISHED)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesBlogErrorCodes.BazaContentTypesBlogNotFound);
    });
});
