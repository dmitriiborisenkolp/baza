import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypesBlogErrorCodes, bazaContentTypesBlogErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class BlogNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesBlogErrorCodes.BazaContentTypesBlogNotFound,
            bazaContentTypesBlogErrorCodesI18n[BazaContentTypesBlogErrorCodes.BazaContentTypesBlogNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
