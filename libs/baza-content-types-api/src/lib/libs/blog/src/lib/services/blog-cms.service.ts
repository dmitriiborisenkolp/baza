import { Injectable } from '@nestjs/common';
import { BlogRepository } from '../repositories/blog-repository.service';
import { BlogEntity } from '../entities/blog.entity';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { BlogCmsMapper } from '../mappers/blog-cms-mapper.service';
import { BlogCmsCreateRequest, BlogCmsDeleteRequest, BlogCmsDto, BlogCmsEntityBody, BlogCmsListRequest, BlogCmsListResponse, BlogCmsSetSortOrderRequest, BlogCmsSetSortOrderResponse, BlogCmsUpdateRequest } from '@scaliolabs/baza-content-types-shared';
import { CategoryRepository } from '../../../../categories/src';

@Injectable()
export class BlogCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: BlogRepository,
        private readonly mapper: BlogCmsMapper,
        private readonly categoryRepository: CategoryRepository,
    ) {}

    async create(request: BlogCmsCreateRequest): Promise<BlogEntity> {
        const entity = new BlogEntity();

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: BlogCmsUpdateRequest): Promise<BlogEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: BlogCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);

        const entities = await this.repository.findAll();

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async list(request: BlogCmsListRequest): Promise<BlogCmsListResponse> {
        return this.crud.find<BlogEntity, BlogCmsDto>({
            entity: BlogEntity,
            request,
            mapper: async(items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
            },
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: BlogCmsSetSortOrderRequest): Promise<BlogCmsSetSortOrderResponse> {
        const entities = await this.repository.findAll();

        const response = await this.crudSort.setSortOrder<BlogEntity, BlogCmsDto>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: entities,
            mapper: async(items) => this.mapper.entitiesToDTOs(items),
        });

        await this.repository.save(entities);

        return response;
    }

    async populate(target: BlogEntity, entityBody: BlogCmsEntityBody): Promise<void> {
        if (! target.id) {
            target.sortOrder = await this.repository.maxSortOrder() + 1;
        }

        if (! target.dateCreatedAt) {
            target.dateCreatedAt = new Date();
        }

        target.isPublished = entityBody.isPublished;
        target.url = entityBody.seo && entityBody.seo.urn
            ? entityBody.seo.urn
            : undefined;
        target.datePublishedAt = entityBody.datePublishedAt
            ? new Date(entityBody.datePublishedAt)
            : null;
        target.primaryCategory = entityBody.primaryCategoryId
            ? await this.categoryRepository.getById(entityBody.primaryCategoryId)
            : null;
        target.additionalCategories = entityBody.additionalCategoryIds
            ? await this.categoryRepository.findByIds(entityBody.additionalCategoryIds)
            : [];
        target.title = entityBody.title;
        target.contents = entityBody.contents;
        target.headerImgS3Key = entityBody.headerImgS3Key;
        target.thumbnailImgS3Key = entityBody.thumbnailImgS3Key;
        target.seo = entityBody.seo;
    }
}
