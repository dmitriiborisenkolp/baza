import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BlogEntity } from '../entities/blog.entity';
import { BlogNotFoundException } from '../exceptions/blog-not-found.exception';

export const BLOG_RELATIONS = [
    'primaryCategory',
    'additionalCategories',
];

@Injectable()
export class BlogRepository {
    constructor(
        private readonly connection: Connection,
    ) {
    }

    get repository(): Repository<BlogEntity> {
        return this.connection.getRepository(BlogEntity);
    }

    async save(entities: Array<BlogEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BlogEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<BlogEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: BLOG_RELATIONS,
        });
    }

    async getById(id: number): Promise<BlogEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BlogNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<BlogEntity> {
        const entity = await this.repository.findOne({
            where: [{
                url,
            }],
            relations: BLOG_RELATIONS,
        });

        if (! entity) {
            throw new BlogNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<BlogEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'ASC',
            },
            relations: BLOG_RELATIONS,
        });
    }

    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return result.max || 0;
    }
}
