import 'reflect-metadata';
import { BlogCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-content-types/blog/001-baza-content-types-blog-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new BlogCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new blog record', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'Blog Record 1',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'blog-record-1',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ID = response.id;
    });

    it('will display new blog in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].title).toBe('Blog Record 1');
        expect(listResponse.items[0].sortOrder).toBe(1);
    });

    it('will successfully one more new blog record', async () => {
        const response = await dataAccessCms.create({
            isPublished: false,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'Blog Record 2',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'blog-record-2',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully returns blog by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Blog Record 1');
    });

    it('will successfully update blog', async () => {
        const response = await dataAccessCms.update({
            id: ID,
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'Blog Record 1 *',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'blog-record-1',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updates in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].title).toBe('Blog Record 2');
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].sortOrder).toBe(1);
        expect(listResponse.items[1].title).toBe('Blog Record 1 *');
    });

    it('will successfully update sort order of blog', async () => {
        const response = await dataAccessCms.setSortOrder({
            id: ID,
            setSortOrder: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updated sort order in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[0].title).toBe('Blog Record 1 *');
        expect(listResponse.items[1].title).toBe('Blog Record 2');
        expect(listResponse.items[1].sortOrder).toBe(1);

        ID = listResponse.items[0].id;
    });

    it('will successfully delete blog', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted blog in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].title).toBe('Blog Record 2');

        ID = listResponse.items[0].id;
    });
});
