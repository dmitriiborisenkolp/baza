import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    BlogCmsCreateRequest,
    BlogCmsDeleteRequest,
    BlogCmsDto,
    BlogCmsEndpoint,
    BlogCmsEndpointPaths,
    BlogCmsGetByIdRequest,
    BlogCmsListRequest,
    BlogCmsListResponse,
    BlogCmsSetSortOrderRequest,
    BlogCmsSetSortOrderResponse,
    BlogCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BlogCmsService } from '../services/blog-cms.service';
import { BlogRepository } from '../repositories/blog-repository.service';
import { BlogCmsMapper } from '../mappers/blog-cms-mapper.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Blog)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesBlog])
export class BlogCmsController implements BlogCmsEndpoint {
    constructor(
        private readonly service: BlogCmsService,
        private readonly repository: BlogRepository,
        private readonly mapper: BlogCmsMapper,
    ) {}

    @Post(BlogCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: BlogCmsDto,
    })
    async create(@Body() request: BlogCmsCreateRequest): Promise<BlogCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BlogCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: BlogCmsDto,
    })
    async update(@Body() request: BlogCmsUpdateRequest): Promise<BlogCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BlogCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: BlogCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(BlogCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: BlogCmsDto,
    })
    async getById(@Body() request: BlogCmsGetByIdRequest): Promise<BlogCmsDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BlogCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: BlogCmsListResponse,
    })
    async list(@Body() request: BlogCmsListRequest): Promise<BlogCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BlogCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: BlogCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: BlogCmsSetSortOrderRequest): Promise<BlogCmsSetSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
