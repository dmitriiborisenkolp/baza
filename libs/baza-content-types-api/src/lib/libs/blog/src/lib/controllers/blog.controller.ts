import { Body, Controller, Get, Param } from '@nestjs/common';
import {
    BazaContentTypesOpenApi,
    BlogDto,
    BlogEndpoint,
    BlogEndpointPaths,
    BlogListRequest,
    BlogListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BlogRepository } from '../repositories/blog-repository.service';
import { BlogMapper } from '../mappers/blog-mapper.service';
import { BlogNotFoundException } from '../exceptions/blog-not-found.exception';
import { BlogService } from '../services/blog.service';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Blog)
export class BlogController implements BlogEndpoint {
    constructor(private readonly service: BlogService, private readonly repository: BlogRepository, private readonly mapper: BlogMapper) {}

    @Get(BlogEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: BlogListResponse,
    })
    async list(@Body() request: BlogListRequest): Promise<BlogListResponse> {
        return this.service.list(request);
    }

    @Get(BlogEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns blog by ID',
    })
    @ApiOkResponse({
        type: BlogDto,
    })
    async getById(@Param('id') id: number): Promise<BlogDto> {
        const entity = await this.repository.getById(id);

        if (!entity.isPublished) {
            throw new BlogNotFoundException();
        }

        return this.mapper.entityToDTO(entity);
    }
}
