import { Injectable } from '@nestjs/common';
import { TagCmsDto, TagsCmsCreateRequest, TagsCmsEntityBody, TagsCmsListRequest, TagsCmsListResponse, TagsCmsSetSortOrderRequest, TagsCmsSetSortOrderResponse, TagsCmsUpdateRequest, TagsCmsDeleteRequest, TeamMemberCmsDto } from '@scaliolabs/baza-content-types-shared';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { TagsRepository } from '../repositories/tags-repository.service';
import { TagEntity } from '../entities/tag.entity';
import { TagsCmsMapper } from '../mappers/tags-cms-mapper.service';

@Injectable()
export class TagsCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: TagsRepository,
        private readonly mapper: TagsCmsMapper,
    ) {
    }

    async create(request: TagsCmsCreateRequest): Promise<TagEntity> {
        const entity = new TagEntity();

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: TagsCmsUpdateRequest): Promise<TagEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: TagsCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);

        const entities = await this.repository.findAll();

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async list(request: TagsCmsListRequest): Promise<TagsCmsListResponse> {
        return this.crud.find<TagEntity, TagCmsDto>({
            entity: TagEntity,
            request,
            mapper: async(items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'ASC',
                },
            },
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: TagsCmsSetSortOrderRequest): Promise<TagsCmsSetSortOrderResponse> {
        const entities = await this.repository.findAll();

        const response = await this.crudSort.setSortOrder<TagEntity, TeamMemberCmsDto>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: entities,
            mapper: async(items) => this.mapper.entitiesToDTOs(items),
        });

        await this.repository.save(entities);

        return response;
    }

    async populate(target: TagEntity, entityBody: TagsCmsEntityBody): Promise<void> {
        if (! target.id) {
            target.sortOrder = await this.repository.maxSortOrder() + 1;
        }

        target.url = entityBody.seo
            ? entityBody.seo.urn
            : undefined;

        target.title = entityBody.title;
        target.isPublished = entityBody.isPublished;
        target.seo = entityBody.seo;
    }
}
