import 'reflect-metadata';
import { TagsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesTagsFixtures } from '../baza-content-types-tags.fixtures';

describe('@scaliolabs/baza-content-types/tags/002-baza-content-types-tags.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new TagsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesTagsFixtures.BazaContentTypesTags],
            specFile: __filename,
        });

        await http.authE2eAdmin();
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will successfully returns all published tags', async () => {
        const response = await dataAccess.getAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);
        expect(response[0].title).toBe('Tag A');
        expect(response[1].title).toBe('Tag C');

        ID = response[0].id;
    });

    it('will successfully returns tag by ID', async () => {
        const response = await dataAccess.getById(ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(ID);
        expect(response.title).toBe('Tag A');
    });

    it('will successfully returns tag by Url', async () => {
        const response = await dataAccess.getByUrl('tag-a');

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(ID);
        expect(response.title).toBe('Tag A');
    });
});
