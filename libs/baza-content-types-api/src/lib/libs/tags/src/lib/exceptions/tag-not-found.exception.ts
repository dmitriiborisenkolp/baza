import { BazaAppException } from '@scaliolabs/baza-core-api';
import { bazaContentTypesTagsCmsI18n, BazaContentTypesTagsErrorCodes } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class TagNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesTagsErrorCodes.BazaContentTypesTagNotFound,
            bazaContentTypesTagsCmsI18n[BazaContentTypesTagsErrorCodes.BazaContentTypesTagNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
