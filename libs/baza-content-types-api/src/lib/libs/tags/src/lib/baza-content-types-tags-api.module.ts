import { Module } from "@nestjs/common";
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { TagsController } from './controllers/tags.controller';
import { TagsCmsController } from './controllers/tags-cms.controller';
import { TagsMapper } from './mappers/tags-mapper.service';
import { TagsRepository } from './repositories/tags-repository.service';
import { TagsCmsMapper } from './mappers/tags-cms-mapper.service';
import { TagsCmsService } from './services/tags-cms.service';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        TagsController,
        TagsCmsController,
    ],
    providers: [
        TagsMapper,
        TagsCmsMapper,
        TagsRepository,
        TagsCmsService,
    ],
    exports: [
        TagsMapper,
        TagsCmsMapper,
        TagsRepository,
        TagsCmsService,
    ],
})
export class BazaContentTypesTagsApiModule {}
