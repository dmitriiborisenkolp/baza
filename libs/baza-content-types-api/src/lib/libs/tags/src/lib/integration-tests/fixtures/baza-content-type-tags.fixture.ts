import { Injectable } from '@nestjs/common';
import { TagsCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { TagsCmsService } from '../../services/tags-cms.service';
import { BazaContentTypesTagsFixtures } from '../baza-content-types-tags.fixtures';

export const E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    request: TagsCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            title: 'Tag A',
            seo: {
                ...bazaSeoDefault(),
                urn: 'tag-a',
            },
            isPublished: true,
        },
    },
    {
        $refId: 2,
        request: {
            title: 'Tag B',
            seo: {
                ...bazaSeoDefault(),
                urn: 'tag-b',
            },
            isPublished: false,
        },
    },
    {
        $refId: 3,
        request: {
            title: 'Tag C',
            seo: bazaSeoDefault(),
            isPublished: true,
        },
    },
];

@Injectable()
export class BazaContentTypeTagsFixture implements BazaE2eFixture {
    constructor(private readonly service: TagsCmsService) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.request);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesTagsFixtures>([
    {
        fixture: BazaContentTypesTagsFixtures.BazaContentTypesTags,
        provider: BazaContentTypeTagsFixture,
    },
]);
