import { Controller, Get, Param } from '@nestjs/common';
import { BazaContentTypesOpenApi, TagDto, TagEndpoint, TagEndpointPaths } from '@scaliolabs/baza-content-types-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { TagsRepository } from '../repositories/tags-repository.service';
import { TagsMapper } from '../mappers/tags-mapper.service';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Tags)
export class TagsController implements TagEndpoint {
    constructor(private readonly repository: TagsRepository, private readonly mapper: TagsMapper) {}

    @Get(TagEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
        description: 'Returns all tags',
    })
    @ApiOkResponse({
        type: TagDto,
        isArray: true,
    })
    async getAll(): Promise<Array<TagDto>> {
        const entities = await this.repository.findAll();

        return this.mapper.entitiesToDTOs(entities.filter((e) => e.isPublished));
    }

    @Get(TagEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns tag by Id',
    })
    @ApiOkResponse({
        type: TagDto,
    })
    async getById(@Param('id') id: number): Promise<TagDto> {
        const entity = await this.repository.getById(id);

        return this.mapper.entityToDTO(entity);
    }

    @Get(TagEndpointPaths.getByUrl)
    @ApiOperation({
        summary: 'getByUrl',
        description: 'Returns tag by Url',
    })
    @ApiOkResponse({
        type: TagDto,
    })
    async getByUrl(@Param('url') url: string): Promise<TagDto> {
        const entity = await this.repository.getByUrl(url);

        return this.mapper.entityToDTO(entity);
    }
}
