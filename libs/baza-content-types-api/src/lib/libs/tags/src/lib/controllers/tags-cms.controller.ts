import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    TagCmsDto,
    TagsCmsCreateRequest,
    TagsCmsDeleteRequest,
    TagsCmsEndpoint,
    TagsCmsEndpointPaths,
    TagsCmsGetByIdRequest,
    TagsCmsListRequest,
    TagsCmsListResponse,
    TagsCmsSetSortOrderRequest,
    TagsCmsSetSortOrderResponse,
    TagsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { TagsCmsService } from '../services/tags-cms.service';
import { TagsRepository } from '../repositories/tags-repository.service';
import { TagsCmsMapper } from '../mappers/tags-cms-mapper.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Tags)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesTags])
export class TagsCmsController implements TagsCmsEndpoint {
    constructor(
        private readonly service: TagsCmsService,
        private readonly repository: TagsRepository,
        private readonly mapper: TagsCmsMapper,
    ) {}

    @Post(TagsCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: TagCmsDto,
    })
    async create(@Body() request: TagsCmsCreateRequest): Promise<TagCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TagsCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: TagCmsDto,
    })
    async update(@Body() request: TagsCmsUpdateRequest): Promise<TagCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TagsCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: TagsCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(TagsCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: TagCmsDto,
    })
    async getById(@Body() request: TagsCmsGetByIdRequest): Promise<TagCmsDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TagsCmsEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
    })
    @ApiOkResponse({
        type: TagCmsDto,
        isArray: true,
    })
    async getAll(): Promise<Array<TagCmsDto>> {
        const entities = await this.repository.findAll();

        return this.mapper.entitiesToDTOs(entities);
    }

    @Post(TagsCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: TagsCmsListResponse,
    })
    async list(@Body() request: TagsCmsListRequest): Promise<TagsCmsListResponse> {
        return this.service.list(request);
    }

    @Post(TagsCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: TagsCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: TagsCmsSetSortOrderRequest): Promise<TagsCmsSetSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
