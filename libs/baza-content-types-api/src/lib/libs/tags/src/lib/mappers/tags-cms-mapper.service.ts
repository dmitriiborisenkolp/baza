import { Injectable } from "@nestjs/common";
import { TagCmsDto } from '@scaliolabs/baza-content-types-shared';
import { TagEntity } from '../entities/tag.entity';
import { TagsMapper } from './tags-mapper.service';

@Injectable()
export class TagsCmsMapper {
    constructor(
        private readonly baseMapper: TagsMapper,
    ) {
    }

    async entityToDTO(input: TagEntity): Promise<TagCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            isPublished: input.isPublished
        };
    }

    async entitiesToDTOs(input: Array<TagEntity>): Promise<Array<TagCmsDto>> {
        const result: Array<TagCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
