import { Injectable } from "@nestjs/common";
import { TagDto } from '@scaliolabs/baza-content-types-shared';
import { TagEntity } from '../entities/tag.entity';

@Injectable()
export class TagsMapper {
    async entityToDTO(input: TagEntity): Promise<TagDto> {
        return {
            id: input.id,
            title: input.title,
            url: input.url,
            seo: input.seo,
            sortOrder: input.sortOrder,
        };
    }

    async entitiesToDTOs(input: Array<TagEntity>): Promise<Array<TagDto>> {
        const result: Array<TagDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
