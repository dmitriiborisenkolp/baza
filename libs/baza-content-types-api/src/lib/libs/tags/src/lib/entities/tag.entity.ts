import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';

@Entity({
    name: 'baza_content_types_tag_entity',
})
export class TagEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    sortOrder: number;

    @Column()
    isPublished: boolean;

    @Column()
    title: string;

    @Column({
        nullable: true,
    })
    url?: string;

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto;
}
