import { Injectable } from '@nestjs/common';
import { Connection, In, Repository } from 'typeorm';
import { TagEntity } from '../entities/tag.entity';
import { TagNotFoundException } from '../exceptions/tag-not-found.exception';

@Injectable()
export class TagsRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<TagEntity> {
        return this.connection.getRepository(TagEntity);
    }

    async save(entities: Array<TagEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<TagEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<TagEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
        });
    }

    async findByIds(ids: Array<number>): Promise<Array<TagEntity>> {
        return this.repository.find({
            where: [{
                id: In(ids),
            }],
        });
    }

    async findByUrl(url: string): Promise<TagEntity | undefined> {
        return this.repository.findOne({
            where: [{
                url,
            }],
        });
    }

    async getById(id: number): Promise<TagEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new TagNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<TagEntity> {
        const entity = await this.findByUrl(url);

        if (! entity) {
            throw new TagNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<TagEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'ASC',
            },
        });
    }

    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return result.max || 0;
    }
}
