import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypesTagsApiModule } from './baza-content-types-tags-api.module';
import { BazaContentTypeTagsFixture } from './integration-tests/fixtures/baza-content-type-tags.fixture';

const E2E_FIXTURES = [
    BazaContentTypeTagsFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypesTagsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypesTagsApiE2eModule {}
