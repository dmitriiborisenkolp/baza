import 'reflect-metadata';
import { TagsCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-content-types/tags/001-baza-nc-tax-document-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new TagsCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new tag', async () => {
        const response = await dataAccessCms.create({
            title: 'Tag A',
            seo: bazaSeoDefault(),
            isPublished: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new tag in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].title).toBe('Tag A');
        expect(listResponse.items[0].sortOrder).toBe(1);

        ID = listResponse.items[0].id;
    });

    it('will successfully create another new tag', async () => {
        const response = await dataAccessCms.create({
            title: 'Tag B',
            seo: bazaSeoDefault(),
            isPublished: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully returns tag by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Tag A');
    });

    it('will successfully update tag', async () => {
        const response = await dataAccessCms.update({
            id: ID,
            title: 'Tag A *',
            seo: bazaSeoDefault(),
            isPublished: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updates in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).toBe(ID);
        expect(listResponse.items[0].title).toBe('Tag A *');
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[1].id).not.toBe(ID);
        expect(listResponse.items[1].sortOrder).toBe(2);
        expect(listResponse.items[1].title).toBe('Tag B');

        ID = listResponse.items[0].id;
    });

    it('will display all tags in getAll response', async () => {
        const response = await dataAccessCms.getAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);
        expect(response[0].title).toBe('Tag A *');
        expect(response[1].title).toBe('Tag B');
    });

    it('will successfully update sort order of team member', async () => {
        const response = await dataAccessCms.setSortOrder({
            id: ID,
            setSortOrder: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updated sort order in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].title).toBe('Tag B');
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].title).toBe('Tag A *');
        expect(listResponse.items[1].sortOrder).toBe(2);
    });

    it('will successfully delete tag', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted tag in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].title).toBe('Tag B');

        ID = listResponse.items[0].id;
    });
});
