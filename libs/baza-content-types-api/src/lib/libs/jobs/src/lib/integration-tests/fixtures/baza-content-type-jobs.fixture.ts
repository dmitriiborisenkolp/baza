import { Injectable } from '@nestjs/common';
import { emptyBazaContentDto, JobsCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesJobsFixtures } from '../baza-content-types-jobs.fixtures';
import { JobCmsService } from '../../services/job-cms.service';

export const E2E_BAZA_CONTENT_TYPES_JOBS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    request: JobsCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'Jobs 1',
            link: 'https://scal.io/jobs/jobs-1',
            shortContents: 'Job 1 Short Description',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'jobs-1',
            },
        },
    },
    {
        $refId: 2,
        request: {
            isPublished: false,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'Jobs 2',
            link: 'https://scal.io/jobs/jobs-2',
            contents: emptyBazaContentDto(),
            shortContents: 'Job 2 Short Description',
            seo: {
                ...bazaSeoDefault(),
                urn: 'jobs-2',
            },
        },
    },
    {
        $refId: 3,
        request: {
            isPublished: true,
            datePublishedAt: 'Tue Aug 20 2021 15:11:57 GMT+0300',
            title: 'Jobs 3',
            link: 'https://scal.io/jobs/jobs-3',
            contents: emptyBazaContentDto(),
            shortContents: 'Job 3 Short Description',
            seo: {
                ...bazaSeoDefault(),
                urn: 'jobs-3',
            },
        },
    },
];

@Injectable()
export class BazaContentTypeJobsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: JobCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_JOBS_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.request);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesJobsFixtures>([{
    fixture: BazaContentTypesJobsFixtures.BazaContentTypesJobs,
    provider: BazaContentTypeJobsFixture,
}]);
