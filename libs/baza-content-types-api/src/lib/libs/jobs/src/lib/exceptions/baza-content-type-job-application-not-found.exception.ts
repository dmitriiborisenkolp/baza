import { BazaAppException } from '@scaliolabs/baza-core-api';
import { bazaContentTypesJobsErrorCodes, BazaContentTypesJobsErrorCodes } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaContentTypeJobApplicationNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesJobsErrorCodes.BazaContentTypesJobApplicationNotFound,
            bazaContentTypesJobsErrorCodes[BazaContentTypesJobsErrorCodes.BazaContentTypesJobApplicationNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
