import { Injectable } from '@nestjs/common';
import { JobApplicationSendRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaContentTypesJobsFixtures } from '../baza-content-types-jobs.fixtures';
import { JobApplicationService } from '../../services/job-application.service';
import { E2E_BAZA_CONTENT_TYPES_JOBS_FIXTURE } from './baza-content-type-jobs.fixture';

export const E2E_BAZA_CONTENT_TYPES_JOB_APPLICATIONS_FIXTURE: Array<{
    $jobRefId: number;
    request: JobApplicationSendRequest;
}> = [
    {
        $jobRefId: 1,
        request: {
            jobId: undefined,
            firstName: 'Foo',
            lastName: 'Bar',
            email: 'scalio-user@scal.io',
            location: 'Some Location',
            phone: '+8xxxxxxxxxx',
        },
    },
    {
        $jobRefId: 2,
        request: {
            jobId: undefined,
            firstName: 'Foo',
            lastName: 'Bar',
            email: 'scalio-user@scal.io',
            location: 'Some Location',
            phone: '+8xxxxxxxxxx',
        },
    },
    {
        $jobRefId: 3,
        request: {
            jobId: undefined,
            firstName: 'Foo',
            lastName: 'Bar',
            email: 'scalio-user@scal.io',
            location: 'Some Location',
            phone: '+8xxxxxxxxxx',
        },
    },
];

@Injectable()
export class BazaContentTypeJobApplicationsFixture implements BazaE2eFixture {
    constructor(private readonly service: JobApplicationService) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_JOB_APPLICATIONS_FIXTURE) {
            await this.service.send({
                ...fixtureRequest.request,
                jobId: E2E_BAZA_CONTENT_TYPES_JOBS_FIXTURE.find((d) => d.$refId === fixtureRequest.$jobRefId).$id,
            });
        }
    }
}

defineE2eFixtures<BazaContentTypesJobsFixtures>([
    {
        fixture: BazaContentTypesJobsFixtures.BazaContentTypesJobApplications,
        provider: BazaContentTypeJobApplicationsFixture,
    },
]);
