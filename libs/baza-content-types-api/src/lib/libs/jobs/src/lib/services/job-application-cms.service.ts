import { Injectable } from "@nestjs/common";
import { CrudService } from '@scaliolabs/baza-core-api';
import { JobApplicationCmsDeleteRequest, JobApplicationCmsListRequest, JobApplicationCmsListResponse, JobApplicationDto } from '@scaliolabs/baza-content-types-shared';
import { JobApplicationEntity } from '../entities/job-application.entity';
import { JobApplicationMapper } from '../mappers/job-application.mapper';
import { JOB_APPLICATION_RELATIONS, JobApplicationRepository } from '../repositories/job-application.repository';

@Injectable()
export class JobApplicationCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: JobApplicationMapper,
        private readonly repository: JobApplicationRepository,
    ) {}

    async delete(request: JobApplicationCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);
    }

    async list(request: JobApplicationCmsListRequest): Promise<JobApplicationCmsListResponse> {
        return this.crud.find<JobApplicationEntity, JobApplicationDto>({
            request,
            entity: JobApplicationEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                where: [{
                    job: request.jobId,
                }],
                order: {
                    id: 'DESC',
                },
                relations: JOB_APPLICATION_RELATIONS,
            },
        });
    }
}
