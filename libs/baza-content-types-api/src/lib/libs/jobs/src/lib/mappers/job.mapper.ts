import { Injectable } from "@nestjs/common";
import { CategoryListMapper } from '../../../../categories/src';
import { JobEntity } from '../entities/job.entity';
import { JobDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class JobMapper {
    constructor(
        private readonly categoryMapper: CategoryListMapper,
    ) {}

    async entityToDTO(input: JobEntity): Promise<JobDto> {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            url: input.url,
            primaryCategory: input.primaryCategory
                ? await this.categoryMapper.entityToDTO(input.primaryCategory)
                : undefined,
            additionalCategories: await  this.categoryMapper.entitiesToDTOs(input.additionalCategories || []),
            datePublishedAt: input.datePublishedAt
                ? input.datePublishedAt.toISOString()
                : undefined,
            title: input.title,
            team: input.team,
            location: input.location,
            link: input.link,
            jobSpecificEmail: input.jobSpecificEmail,
            shortContents: input.shortContents,
            contents: input.contents,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<JobEntity>): Promise<Array<JobDto>> {
        const result: Array<JobDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
