import { BazaAppException } from '@scaliolabs/baza-core-api';
import { bazaContentTypesJobsErrorCodes, BazaContentTypesJobsErrorCodes } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaContentTypeJobNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesJobsErrorCodes.BazaContentTypesJobNotFound,
            bazaContentTypesJobsErrorCodes[BazaContentTypesJobsErrorCodes.BazaContentTypesJobNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
