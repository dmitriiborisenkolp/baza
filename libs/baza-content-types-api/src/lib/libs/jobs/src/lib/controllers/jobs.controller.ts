import { Body, Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypesOpenApi,
    JobDto,
    JobEndpoint,
    JobEndpointPaths,
    JobsListRequest,
    JobsListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { JobMapper } from '../mappers/job.mapper';
import { JobService } from '../services/job.service';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Jobs)
export class JobController implements JobEndpoint {
    constructor(private readonly mapper: JobMapper, private readonly service: JobService) {}

    @Get(JobEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List jobs',
    })
    @ApiOkResponse({
        type: JobsListResponse,
    })
    async list(@Body() request: JobsListRequest): Promise<JobsListResponse> {
        return this.service.list(request);
    }

    @Get(JobEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
        description: 'Returns all jobs',
    })
    @ApiOkResponse({
        type: JobDto,
        isArray: true,
    })
    async getAll(): Promise<Array<JobDto>> {
        const entities = await this.service.getAll();

        return this.mapper.entitiesToDTOs(entities);
    }

    @Get(JobEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns job record by Id',
    })
    @ApiOkResponse({
        type: JobDto,
    })
    async getById(@Param('id') id: number): Promise<JobDto> {
        const entity = await this.service.getById(id);

        return this.mapper.entityToDTO(entity);
    }

    @Get(JobEndpointPaths.getByUrl)
    @ApiOperation({
        summary: 'getByUrl',
        description: 'Returns job record by URL',
    })
    @ApiOkResponse({
        type: JobDto,
    })
    async getByUrl(@Param('url') url: string): Promise<JobDto> {
        const entity = await this.service.getByUrl(url);

        return this.mapper.entityToDTO(entity);
    }
}
