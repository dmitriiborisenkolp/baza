import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { JobEntity } from './job.entity';

@Entity({
    name: 'baza_content_types_job_application_entity',
})
export class JobApplicationEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    dateCreatedAt: Date;

    @ManyToOne(() => JobEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    job: JobEntity;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    email: string;

    @Column({
        nullable: true,
    })
    location?: string;

    @Column({
        nullable: true,
    })
    phone?: string;
}
