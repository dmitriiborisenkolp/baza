import 'reflect-metadata';
import { JobsCmsNodeAccess, JobsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaContentTypesJobsErrorCodes, JobsCmsListResponse } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesJobsFixtures } from '../baza-content-types-jobs.fixtures';

describe('@scaliolabs/baza-content-types/jobs/002-baza-content-types-jobs.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new JobsNodeAccess(http);
    const dataAccessCms = new JobsCmsNodeAccess(http);

    let ID: number;
    let ID_UNPUBLISHED: number;

    let CMS_LIST_RESPONSE: JobsCmsListResponse;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesJobsFixtures.BazaContentTypesJobs],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        CMS_LIST_RESPONSE = await dataAccessCms.list({});
        ID_UNPUBLISHED = CMS_LIST_RESPONSE.items.find((e) => !e.isPublished).id;
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will successfully returns list of published jobs', async () => {
        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Jobs 3');
        expect(response.items[1].title).toBe('Jobs 1');

        ID = response.items[0].id;
    });

    it('will successfully returns all published jobs', async () => {
        const response = await dataAccess.getAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);
        expect(response[0].title).toBe('Jobs 3');
        expect(response[1].title).toBe('Jobs 1');
    });

    it('will successfully returns published jobs record by id', async () => {
        const response = await dataAccess.getById(ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Jobs 3');
    });

    it('will successfully returns published jobs record by url', async () => {
        const response = await dataAccess.getByUrl('jobs-3');

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Jobs 3');
    });

    it('will not returns unpublished jobs record by id', async () => {
        const response: BazaError = (await dataAccess.getById(ID_UNPUBLISHED)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesJobsErrorCodes.BazaContentTypesJobNotFound);
    });

    it('will not returns unpublished jobs record by url', async () => {
        const response: BazaError = (await dataAccess.getByUrl('jobs-2')) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesJobsErrorCodes.BazaContentTypesJobNotFound);
    });
});
