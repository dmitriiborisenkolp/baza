import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { JobMapper } from './mappers/job.mapper';
import { JobCmsMapper } from './mappers/job-cms.mapper';
import { JobService } from './services/job.service';
import { JobRepository } from './repositories/job.repository';
import { JobController } from './controllers/jobs.controller';
import { JobCmsController } from './controllers/jobs-cms.controller';
import { JobCmsService } from './services/job-cms.service';
import { JobApplicationMapper } from './mappers/job-application.mapper';
import { JobApplicationService } from './services/job-application.service';
import { JobApplicationCmsService } from './services/job-application-cms.service';
import { JobApplicationRepository } from './repositories/job-application.repository';
import { JobApplicationController } from './controllers/job-application.controller';
import { JobApplicationCmsController } from './controllers/job-application-cms.controller';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        JobController,
        JobCmsController,
        JobApplicationController,
        JobApplicationCmsController,
    ],
    providers: [
        JobRepository,
        JobMapper,
        JobCmsMapper,
        JobService,
        JobCmsService,
        JobApplicationMapper,
        JobApplicationService,
        JobApplicationCmsService,
        JobApplicationRepository,
    ],
    exports: [
        JobRepository,
        JobMapper,
        JobCmsMapper,
        JobService,
        JobCmsService,
        JobApplicationMapper,
        JobApplicationService,
        JobApplicationCmsService,
        JobApplicationRepository,
    ],
})
export class BazaContentTypeJobsApiModule {}
