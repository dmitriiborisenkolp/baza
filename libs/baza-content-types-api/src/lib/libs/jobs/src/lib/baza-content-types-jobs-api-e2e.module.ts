import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypeJobsApiModule } from './baza-content-types-jobs-api.module';
import { BazaContentTypeJobApplicationsFixture } from './integration-tests/fixtures/baza-content-type-job-applications.fixture';
import { BazaContentTypeJobsFixture } from './integration-tests/fixtures/baza-content-type-jobs.fixture';

const E2E_FIXTURES = [
    BazaContentTypeJobApplicationsFixture,
    BazaContentTypeJobsFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypeJobsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypesJobsApiE2eModule {}
