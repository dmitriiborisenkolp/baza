import { Injectable } from '@nestjs/common';
import { JobApplicationRepository } from '../repositories/job-application.repository';
import { JobRepository } from '../repositories/job.repository';
import { JobApplicationSendRequest } from '@scaliolabs/baza-content-types-shared';
import { JobApplicationEntity } from '../entities/job-application.entity';

@Injectable()
export class JobApplicationService {
    constructor(private readonly repository: JobApplicationRepository, private readonly jobRepository: JobRepository) {}

    async send(request: JobApplicationSendRequest): Promise<JobApplicationEntity> {
        const entity = new JobApplicationEntity();

        entity.job = await this.jobRepository.getById(request.jobId);
        entity.dateCreatedAt = new Date();
        entity.firstName = request.firstName;
        entity.lastName = request.lastName;
        entity.email = request.email;
        entity.location = request.location;
        entity.phone = request.phone;

        await this.repository.save([entity]);

        return entity;
    }
}
