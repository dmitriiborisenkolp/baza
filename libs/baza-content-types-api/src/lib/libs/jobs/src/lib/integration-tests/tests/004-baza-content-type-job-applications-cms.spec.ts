import 'reflect-metadata';
import { JobsApplicationsCmsNodeAccess, JobsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { JobDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesJobsFixtures } from '../baza-content-types-jobs.fixtures';

describe('@scaliolabs/baza-content-types/jobs/004-baza-content-types-job-applications-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new JobsApplicationsCmsNodeAccess(http);
    const dataAccessJobs = new JobsNodeAccess(http);

    let jobs: Array<JobDto> = [];
    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaContentTypesJobsFixtures.BazaContentTypesJobs,
                BazaContentTypesJobsFixtures.BazaContentTypesJobApplications,
            ],
            specFile: __filename,
        });

        jobs = await dataAccessJobs.getAll();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will display list of job applications', async () => {
        const response = await dataAccessCms.list({
            jobId: jobs[0].id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);

        ID = response.items[0].id;
    });

    it('will returns job application by id', async () => {
        const response = await dataAccessCms.getById({ id: ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to delete job application', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will remove job application from list response', async () => {
        const response = await dataAccessCms.list({
            jobId: jobs[0].id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });
});
