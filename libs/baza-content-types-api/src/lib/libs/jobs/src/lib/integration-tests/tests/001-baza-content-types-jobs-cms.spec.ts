import 'reflect-metadata';
import { JobsCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-content-types/jobs/001-baza-content-types-jobs-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new JobsCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new jobs record', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'Jobs 1',
            link: 'https://scal.io/jobs/jobs-1',
            shortContents: 'Job 1 Short Description',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'jobs-1',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new jobs record in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].title).toBe('Jobs 1');
        expect(listResponse.items[0].sortOrder).toBe(1);
    });

    it('will successfully create new jobs record without link', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'Jobs 2',
            shortContents: 'Job 2 Short Description',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'jobs-2',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ID = response.id;
    });

    it('will successfully returns jobs record  by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Jobs 2');
    });

    it('will successfully update jobs record', async () => {
        const response = await dataAccessCms.update({
            id: ID,
            isPublished: true,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'Jobs 2 *',
            shortContents: 'Job 2 Short Description',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'jobs-2',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updates in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).toBe(ID);
        expect(listResponse.items[0].title).toBe('Jobs 2 *');
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[1].id).not.toBe(ID);
        expect(listResponse.items[1].sortOrder).toBe(1);
        expect(listResponse.items[1].title).toBe('Jobs 1');

        ID = listResponse.items[0].id;
    });

    it('will successfully update sort order of jobs record', async () => {
        const response = await dataAccessCms.setSortOrder({
            id: ID,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updated sort order in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[0].title).toBe('Jobs 1');
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].title).toBe('Jobs 2 *');
        expect(listResponse.items[1].sortOrder).toBe(1);

        ID = listResponse.items[1].id;
    });

    it('will successfully delete jobs record', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted team member in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].title).toBe('Jobs 1');

        ID = listResponse.items[0].id;
    });
});
