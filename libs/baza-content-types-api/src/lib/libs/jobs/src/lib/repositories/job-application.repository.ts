import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { JobApplicationEntity } from '../entities/job-application.entity';
import { JobEntity } from '../entities/job.entity';
import { BazaContentTypeJobApplicationNotFoundException } from '../exceptions/baza-content-type-job-application-not-found.exception';

export const JOB_APPLICATION_RELATIONS = [
    'job',
];

@Injectable()
export class JobApplicationRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<JobApplicationEntity> {
        return this.connection.getRepository(JobApplicationEntity);
    }

    async save(entities: Array<JobApplicationEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<JobApplicationEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<JobApplicationEntity> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: JOB_APPLICATION_RELATIONS,
        });
    }

    async getById(id: number): Promise<JobApplicationEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BazaContentTypeJobApplicationNotFoundException();
        }

        return entity;
    }

    async findAllOfJob(job: number | JobEntity): Promise<Array<JobApplicationEntity>> {
        return this.repository.find({
            where: [{
                job,
            }],
            order: {
                id: 'DESC',
            },
            relations: JOB_APPLICATION_RELATIONS,
        });
    }

    async countOfJob(job: number | JobEntity): Promise<number> {
        return this.repository.count({
            where: [{
                job,
            }],
            relations: JOB_APPLICATION_RELATIONS,
        });
    }
}
