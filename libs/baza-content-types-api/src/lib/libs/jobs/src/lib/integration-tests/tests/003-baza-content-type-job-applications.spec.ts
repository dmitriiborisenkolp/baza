import 'reflect-metadata';
import { JobsApplicationsCmsNodeAccess, JobsApplicationsNodeAccess, JobsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { JobDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesJobsFixtures } from '../baza-content-types-jobs.fixtures';

describe('@scaliolabs/baza-content-types/jobs/003-baza-content-types-job-applications.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new JobsApplicationsNodeAccess(http);
    const dataAccessCms = new JobsApplicationsCmsNodeAccess(http);
    const dataAccessJobs = new JobsNodeAccess(http);

    let jobs: Array<JobDto> = [];

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesJobsFixtures.BazaContentTypesJobs],
            specFile: __filename,
        });

        jobs = await dataAccessJobs.getAll();
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will successfully send job application', async () => {
        const response = await dataAccess.send({
            jobId: jobs[0].id,
            firstName: 'Foo',
            lastName: 'Bar',
            email: 'scalio-user@scal.io',
            location: 'Some Location',
            phone: '+8xxxxxxxxxx',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new job application in CMS', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessCms.list({
            jobId: jobs[0].id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].email).toBe('scalio-user@scal.io');
    });
});
