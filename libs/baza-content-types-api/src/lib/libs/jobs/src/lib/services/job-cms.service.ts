import { Injectable } from '@nestjs/common';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { JOB_RELATIONS, JobRepository } from '../repositories/job.repository';
import { JobCmsMapper } from '../mappers/job-cms.mapper';
import { JobEntity } from '../entities/job.entity';
import { CategoryRepository } from '../../../../categories/src';
import { JobCmsDto, JobCmsEntityBody, JobsCmsCreateRequest, JobsCmsDeleteRequest, JobsCmsListRequest, JobsCmsListResponse, JobsCmsSetSortOrderRequest, JobsCmsSetSortOrderResponse, JobsCmsUpdateRequest } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class JobCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: JobRepository,
        private readonly categoryRepository: CategoryRepository,
        private readonly mapper: JobCmsMapper,
    ) {}

    async create(request: JobsCmsCreateRequest): Promise<JobEntity> {
        const entity = new JobEntity();

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: JobsCmsUpdateRequest): Promise<JobEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: JobsCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);

        const entities = await this.repository.findAll();

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async list(request: JobsCmsListRequest): Promise<JobsCmsListResponse> {
        return this.crud.find<JobEntity, JobCmsDto>({
            entity: JobEntity,
            request,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
                relations: JOB_RELATIONS,
            },
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: JobsCmsSetSortOrderRequest): Promise<JobsCmsSetSortOrderResponse> {
        const entities = await this.repository.findAll();

        const response = await this.crudSort.setSortOrder<JobEntity, JobCmsDto>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: entities,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });

        await this.repository.save(entities);

        return response;
    }

    async populate(target: JobEntity, entityBody: JobCmsEntityBody): Promise<void> {
        if (! target.id) {
            target.sortOrder = await this.repository.maxSortOrder() + 1;
        }

        if (! target.dateCreatedAt) {
            target.dateCreatedAt = new Date();
        }

        target.title = entityBody.title;
        target.isPublished = entityBody.isPublished;
        target.url = entityBody.seo && entityBody.seo.urn
            ? entityBody.seo.urn
            : undefined;
        target.datePublishedAt = entityBody.datePublishedAt
            ? new Date(entityBody.datePublishedAt)
            : null;
        target.primaryCategory = entityBody.primaryCategoryId
            ? await this.categoryRepository.getById(entityBody.primaryCategoryId)
            : null;
        target.additionalCategories = entityBody.additionalCategoryIds
            ? await this.categoryRepository.findByIds(entityBody.additionalCategoryIds)
            : [];
        target.title = entityBody.title;
        target.link = entityBody.link;
        target.team = entityBody.team;
        target.jobSpecificEmail = entityBody.jobSpecificEmail;
        target.shortContents = entityBody.shortContents;
        target.contents = entityBody.contents;
        target.seo = entityBody.seo;
    }
}
