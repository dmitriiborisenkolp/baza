import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    JobCmsDto,
    JobCmsEndpoint,
    JobCmsEndpointPaths,
    JobsCmsCreateRequest,
    JobsCmsDeleteRequest,
    JobsCmsGetByIdRequest,
    JobsCmsListRequest,
    JobsCmsListResponse,
    JobsCmsSetSortOrderRequest,
    JobsCmsSetSortOrderResponse,
    JobsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { JobCmsMapper } from '../mappers/job-cms.mapper';
import { JobRepository } from '../repositories/job.repository';
import { JobCmsService } from '../services/job-cms.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Jobs)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesJobs])
export class JobCmsController implements JobCmsEndpoint {
    constructor(
        private readonly mapper: JobCmsMapper,
        private readonly repository: JobRepository,
        private readonly service: JobCmsService,
    ) {}

    @Post(JobCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: JobCmsDto,
    })
    async create(@Body() request: JobsCmsCreateRequest): Promise<JobCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(JobCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: JobCmsDto,
    })
    async update(@Body() request: JobsCmsUpdateRequest): Promise<JobCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(JobCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: JobsCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(JobCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: JobsCmsListResponse,
    })
    async list(@Body() request: JobsCmsListRequest): Promise<JobsCmsListResponse> {
        return this.service.list(request);
    }

    @Post(JobCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: JobCmsDto,
    })
    async getById(@Body() request: JobsCmsGetByIdRequest): Promise<JobCmsDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(JobCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: JobsCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: JobsCmsSetSortOrderRequest): Promise<JobsCmsSetSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
