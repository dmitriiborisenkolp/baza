import { Injectable } from "@nestjs/common";
import { JobMapper } from './job.mapper';
import { JobApplicationEntity } from '../entities/job-application.entity';
import { JobApplicationDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class JobApplicationMapper {
    constructor(
        private readonly jobMapper: JobMapper,
    ) {}

    async entityToDTO(input: JobApplicationEntity): Promise<JobApplicationDto> {
        return {
            id: input.id,
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            job: await this.jobMapper.entityToDTO(input.job),
            firstName: input.firstName,
            lastName: input.lastName,
            email: input.email,
            location: input.location,
            phone: input.phone,
        };
    }

    async entitiesToDTOs(input: Array<JobApplicationEntity>): Promise<Array<JobApplicationDto>> {
        const result: Array<JobApplicationDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
