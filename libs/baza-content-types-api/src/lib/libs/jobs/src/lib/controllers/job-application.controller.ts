import { Body, Controller, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypesOpenApi,
    JobApplicationEndpoint,
    JobApplicationEndpointPaths,
    JobApplicationSendRequest,
} from '@scaliolabs/baza-content-types-shared';
import { JobApplicationService } from '../services/job-application.service';

@Controller()
@ApiTags(BazaContentTypesOpenApi.JobApplications)
export class JobApplicationController implements JobApplicationEndpoint {
    constructor(private readonly service: JobApplicationService) {}

    @Post(JobApplicationEndpointPaths.send)
    @ApiOperation({
        summary: 'send',
        description: 'Send request to contact for job',
    })
    @ApiOkResponse()
    async send(@Body() request: JobApplicationSendRequest): Promise<void> {
        await this.service.send(request);
    }
}
