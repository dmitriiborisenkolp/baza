import { Injectable } from '@nestjs/common';
import { JOB_RELATIONS, JobRepository } from '../repositories/job.repository';
import { JobMapper } from '../mappers/job.mapper';
import { CrudService } from '@scaliolabs/baza-core-api';
import { JobEntity } from '../entities/job.entity';
import { BazaContentTypeJobNotFoundException } from '../exceptions/baza-content-type-job-not-found.exception';
import { JobDto, JobsListRequest, JobsListResponse } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class JobService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: JobMapper,
        private readonly repository: JobRepository,
    ) {}

    async list(request: JobsListRequest): Promise<JobsListResponse> {
        return this.crud.find<JobEntity, JobDto>({
            request,
            entity: JobEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
                relations: JOB_RELATIONS,
                where: [{
                    isPublished: true,
                }],
            },
            withMaxSortOrder: true,
        });
    }

    async getAll(): Promise<Array<JobEntity>> {
        return this.repository.findAllPublished();
    }

    async getById(id: number): Promise<JobEntity> {
        const entity = await this.repository.getById(id);

        if (! entity.isPublished) {
            throw new BazaContentTypeJobNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<JobEntity> {
        const entity = await this.repository.getByUrl(url);

        if (! entity.isPublished) {
            throw new BazaContentTypeJobNotFoundException();
        }

        return entity;
    }
}
