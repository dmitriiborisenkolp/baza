import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    JobApplicationCmsDeleteRequest,
    JobApplicationCmsEndpoint,
    JobApplicationCmsEndpointPaths,
    JobApplicationCmsGetByIdRequest,
    JobApplicationCmsListRequest,
    JobApplicationCmsListResponse,
    JobApplicationDto,
} from '@scaliolabs/baza-content-types-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { JobApplicationRepository } from '../repositories/job-application.repository';
import { JobApplicationMapper } from '../mappers/job-application.mapper';
import { JobApplicationCmsService } from '../services/job-application-cms.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.JobApplications)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesJobApplications])
export class JobApplicationCmsController implements JobApplicationCmsEndpoint {
    constructor(
        private readonly service: JobApplicationCmsService,
        private readonly mapper: JobApplicationMapper,
        private readonly repository: JobApplicationRepository,
    ) {}

    @Post(JobApplicationCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: JobApplicationCmsListResponse,
    })
    async list(@Body() request: JobApplicationCmsListRequest): Promise<JobApplicationCmsListResponse> {
        return this.service.list(request);
    }

    @Post(JobApplicationCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: JobApplicationDto,
    })
    async getById(@Body() request: JobApplicationCmsGetByIdRequest): Promise<JobApplicationDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(JobApplicationCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: JobApplicationCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }
}
