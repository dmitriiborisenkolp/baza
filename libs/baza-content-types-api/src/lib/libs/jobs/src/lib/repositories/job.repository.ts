import { Injectable } from "@nestjs/common";
import { Connection, Repository } from 'typeorm';
import { JobEntity } from '../entities/job.entity';
import { BazaContentTypeJobNotFoundException } from '../exceptions/baza-content-type-job-not-found.exception';

export const JOB_RELATIONS = [
    'primaryCategory',
    'additionalCategories',
];

@Injectable()
export class JobRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<JobEntity> {
        return this.connection.getRepository(JobEntity);
    }

    async save(entities: Array<JobEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<JobEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<JobEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: JOB_RELATIONS,
        });
    }

    async getById(id: number): Promise<JobEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BazaContentTypeJobNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<JobEntity> {
        const entity = await this.repository.findOne({
            where: [{
                url,
            }],
            relations: JOB_RELATIONS,
        });

        if (! entity) {
            throw new BazaContentTypeJobNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<JobEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'DESC',
            },
            relations: JOB_RELATIONS,
        });
    }

    async findAllPublished(): Promise<Array<JobEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'DESC',
            },
            where: [{
                isPublished: true,
            }],
            relations: JOB_RELATIONS,
        });
    }

    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return result.max || 0;
    }
}
