import { Injectable } from "@nestjs/common";
import { JobMapper } from './job.mapper';
import { JobEntity } from '../entities/job.entity';
import { JobCmsDto } from '@scaliolabs/baza-content-types-shared';
import { JobApplicationRepository } from '../repositories/job-application.repository';

@Injectable()
export class JobCmsMapper {
    constructor(
        private readonly baseMapper: JobMapper,
        private readonly jobApplicationsRepository: JobApplicationRepository,
    ) {}

    async entityToDTO(input: JobEntity): Promise<JobCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            isPublished: input.isPublished,
            countApplications: await this.jobApplicationsRepository.countOfJob(input),
        };
    }

    async entitiesToDTOs(input: Array<JobEntity>): Promise<Array<JobCmsDto>> {
        const result: Array<JobCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
