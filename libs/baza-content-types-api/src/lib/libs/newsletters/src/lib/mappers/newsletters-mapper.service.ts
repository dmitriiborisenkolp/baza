import { Injectable } from '@nestjs/common';
import { BazaContentTypesNewsletterEntity } from '../entities/baza-content-types-newsletter.entity';
import { NewslettersDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class NewslettersMapper {
    entityToDTO(entity: BazaContentTypesNewsletterEntity): NewslettersDto {
        return {
            id: entity.id,
            email: entity.email,
            name: entity.name,
            dateCreated: entity.dateCreated.toISOString(),
            dateUpdated: entity.dateUpdated.toISOString(),
        };
    }

    entitiesToDTOs(entities: Array<BazaContentTypesNewsletterEntity>): Array<NewslettersDto> {
        return entities.map((e) => this.entityToDTO(e));
    }
}
