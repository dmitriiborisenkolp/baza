import { RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';

export const bazaContentTypesNewslettersApiRegistry: RegistrySchema = {
    newsletters: {
        subject: {
            name: 'Contacts - Subject',
            type: RegistryType.String,
            hiddenFromList: false,
            public: false,
            defaults: 'Newsletter',
        },
        email: {
            name: 'Coming Soon Subscription - Email',
            type: RegistryType.Email,
            hiddenFromList: false,
            public: false,
            defaults: 'scalio-admin@scal.io',
        },
        fullName: {
            name: 'Company - Name',
            type: RegistryType.String,
            hiddenFromList: false,
            public: false,
            defaults: 'Scalio LLC',
        },
        website: {
            name: 'Company - Website',
            type: RegistryType.String,
            hiddenFromList: false,
            public: false,
            defaults: 'http://scal.io',
        },
        comingSoon: {
            name: 'Coming Soon Subscription - Content',
            type: RegistryType.Email,
            hiddenFromList: false,
            public: false,
            defaults: 'scalio-admin@scal.io',
        },
    },
};
