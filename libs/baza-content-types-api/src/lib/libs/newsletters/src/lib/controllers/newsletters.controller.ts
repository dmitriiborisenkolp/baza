import { Body, Controller, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { NewslettersService } from '../services/newsletters.service';
import { RequestContextService } from '@scaliolabs/baza-core-api';
import {
    NewsletterEndpoint,
    NewsletterEndpointPaths,
    NewsletterSubscribeRequest,
    NewsletterUnsubscribeRequest,
    BazaContentTypesOpenApi,
} from '@scaliolabs/baza-content-types-shared';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Newsletters)
export class NewslettersController implements NewsletterEndpoint {
    constructor(private readonly requestContext: RequestContextService, private readonly service: NewslettersService) {}

    @Post(NewsletterEndpointPaths.subscribe)
    @ApiOperation({
        summary: 'subscribe',
        description: 'Subscribe with given email. Will not throw any errors for duplicate attempts',
    })
    @ApiOkResponse()
    async subscribe(@Body() request: NewsletterSubscribeRequest): Promise<void> {
        await this.service.subscribe(request, await this.requestContext.current());
    }

    @Post(NewsletterEndpointPaths.unsubscribe)
    @ApiOperation({
        summary: 'unsubscribe',
        description: 'Unsubscribe with given email. Will not throw any errors for duplicate attempts',
    })
    @ApiOkResponse()
    async unsubscribe(@Body() request: NewsletterUnsubscribeRequest): Promise<void> {
        await this.service.unsubscribe(request);
    }
}
