import 'reflect-metadata';
import { NewslettersCmsNodeAccess, NewslettersNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesNewslettersFixtures } from '../baza-content-types-newsletters-fixtures';

describe('@scaliolabs/baza-content-types/newsletters/002-baza-content-types-newsletters.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessNewsletters = new NewslettersNodeAccess(http);
    const dataAccessNewslettersCms = new NewslettersCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesNewslettersFixtures.BazaContentTypesNewsletters],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will successfully subscribe email', async () => {
        const response = await dataAccessNewsletters.subscribe({
            email: 'e2e-example-4@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);
    });

    it('will display new subscription in CMS', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNewslettersCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].email).toBe('e2e-example-4@scal.io');
        expect(response.items[1].email).toBe('e2e-example-3@scal.io');
        expect(response.items[2].email).toBe('e2e-example-2@scal.io');
        expect(response.items[3].email).toBe('e2e-example-1@scal.io');
    });

    it('will not fail to duplicate subscribe', async () => {
        const response = await dataAccessNewsletters.subscribe({
            email: 'e2e-example-4@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);
    });

    it('will still display new subscription in CMS without duplicates', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNewslettersCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].email).toBe('e2e-example-4@scal.io');
        expect(response.items[1].email).toBe('e2e-example-3@scal.io');
        expect(response.items[2].email).toBe('e2e-example-2@scal.io');
        expect(response.items[3].email).toBe('e2e-example-1@scal.io');
    });

    it('will successfully unsubscribe', async () => {
        const response = await dataAccessNewsletters.unsubscribe({
            email: 'e2e-example-4@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display 3 subscription in CMS', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNewslettersCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].email).toBe('e2e-example-3@scal.io');
        expect(response.items[1].email).toBe('e2e-example-2@scal.io');
        expect(response.items[2].email).toBe('e2e-example-1@scal.io');
    });

    it('will not fail to unsubscribe with duplicate request', async () => {
        const response = await dataAccessNewsletters.unsubscribe({
            email: 'e2e-example-4@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
