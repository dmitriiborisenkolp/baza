import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity({
    name: 'baza_content_types_newsletter_entity',
})
export class BazaContentTypesNewsletterEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @CreateDateColumn()
    dateCreated: Date;

    @UpdateDateColumn()
    dateUpdated: Date;

    @Column({
        nullable: true,
    })
    name?: string;

    @Column({
        unique: true,
    })
    email: string;
}
