import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, RequestContextService, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { NewslettersCmsService } from '../services/newsletters-cms.service';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    NewsletterCmsAddRequest,
    NewsletterCmsEndpoint,
    NewsletterCmsEndpointPaths,
    NewsletterCmsExportToCsvExportRequest,
    NewsletterCmsListRequest,
    NewsletterCmsListResponse,
    NewsletterCmsRemoveRequest,
    NewslettersDto,
} from '@scaliolabs/baza-content-types-shared';
import { NewslettersService } from '../services/newsletters.service';
import { NewslettersMapper } from '../mappers/newsletters-mapper.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Newsletters)
@UseGuards(AuthGuard, WithAccessGuard, AuthRequireAdminRoleGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesNewsletters])
export class NewslettersCmsController implements NewsletterCmsEndpoint {
    constructor(
        private readonly requestContext: RequestContextService,
        private readonly service: NewslettersService,
        private readonly cmsService: NewslettersCmsService,
        private readonly mapper: NewslettersMapper,
    ) {}

    @Post(NewsletterCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List of subscriptions',
    })
    @ApiOkResponse({
        type: NewsletterCmsListRequest,
    })
    async list(@Body() request: NewsletterCmsListRequest): Promise<NewsletterCmsListResponse> {
        return this.cmsService.list(request);
    }

    @Post(NewsletterCmsEndpointPaths.exportToCSV)
    @ApiOperation({
        summary: 'exportToCSV',
        description: 'Export list of subscriptions to CSV',
    })
    @ApiOkResponse()
    async exportToCSV(@Body() request: NewsletterCmsExportToCsvExportRequest): Promise<string> {
        return this.cmsService.exportToCSV(request);
    }

    @Post(NewsletterCmsEndpointPaths.add)
    @ApiOperation({
        summary: 'add',
        description: 'Add subscription',
    })
    @ApiOkResponse({
        type: NewslettersDto,
    })
    async add(@Body() request: NewsletterCmsAddRequest): Promise<NewslettersDto> {
        const entity = await this.service.subscribe(
            {
                name: request.name,
                email: request.email,
            },
            await this.requestContext.current(),
        );

        return this.mapper.entityToDTO(entity);
    }

    @Post(NewsletterCmsEndpointPaths.remove)
    @ApiOperation({
        summary: 'remove',
        description: 'Remove (unsubscribe) by email',
    })
    @ApiOkResponse()
    async remove(@Body() request: NewsletterCmsRemoveRequest): Promise<void> {
        return this.cmsService.remove(request);
    }
}
