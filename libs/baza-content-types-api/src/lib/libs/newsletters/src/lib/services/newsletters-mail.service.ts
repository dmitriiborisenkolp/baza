import { Injectable } from '@nestjs/common';
import { BazaRegistryService, MailService, ProjectService } from '@scaliolabs/baza-core-api';
import { Application, ProjectLanguage, replaceTags } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesMail } from '../../../../../constants/baza-content-types.mail-templates';

@Injectable()
export class NewslettersMailService {
    constructor(
        private readonly mailService: MailService,
        private readonly project: ProjectService,
        private readonly registry: BazaRegistryService,
    ) {}

    async sendWelcome(request: { app: Application; language: ProjectLanguage; email: string; name: string }) {
        const { name } = request;

        await this.mailService.send({
            name: BazaContentTypesMail.BazaContentTypesNewslettersInquiryEmail,
            to: [
                {
                    email: request.email,
                },
            ],
            subject: `${replaceTags(this.registry.getValue('bazaCommon.clientName'), {
                subject: `${name}`,
            })}: Welcome`,
            variables: () => ({
                site: this.project.createLinkToApplication({ application: Application.WEB }),
                fullName: this.registry.getValue('bazaContentTypes.newsletters.fullName'),
                inquiryEmail: this.registry.getValue('bazaContentTypes.newsletters.email'),
                website: this.registry.getValue('bazaContentTypes.newsletters.website'),
                comingSoon: this.registry.getValue('bazaContentTypes.newsletters.comingSoon'),
                name: request.name,
            }),
        });
    }
}
