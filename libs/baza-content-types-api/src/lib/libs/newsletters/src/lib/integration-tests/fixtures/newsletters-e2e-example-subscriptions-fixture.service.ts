import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaContentTypesNewslettersFixtures } from '../baza-content-types-newsletters-fixtures';
import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaContentTypesNewsletterEntity } from '../../entities/baza-content-types-newsletter.entity';

@Injectable()
export class NewslettersE2eExampleSubscriptionsFixture implements BazaE2eFixture {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaContentTypesNewsletterEntity> {
        return this.connection.getRepository(BazaContentTypesNewsletterEntity);
    }

    async up(): Promise<void> {
        const requests: Array<{
            email: string;
        }> = [
            { email: 'e2e-example-1@scal.io' },
            { email: 'e2e-example-2@scal.io' },
            { email: 'e2e-example-3@scal.io' },
        ];

        for (const request of requests) {
            const entity = new BazaContentTypesNewsletterEntity();

            entity.email = request.email;

            await this.repository.save(entity);
        }
    }
}

defineE2eFixtures<BazaContentTypesNewslettersFixtures>([{
    fixture: BazaContentTypesNewslettersFixtures.BazaContentTypesNewsletters,
    provider: NewslettersE2eExampleSubscriptionsFixture,
}]);
