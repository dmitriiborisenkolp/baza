import { Injectable } from '@nestjs/common';
import { BazaContentTypesNewsletterEntity } from '../entities/baza-content-types-newsletter.entity';
import * as moment from 'moment';
import { NewslettersCsvDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class NewslettersCsvMapper {
    entityToDTO(entity: BazaContentTypesNewsletterEntity): NewslettersCsvDto {
        return {
            id: entity.id,
            email: entity.email,
            name: entity.name,
            dateCreated: moment(entity.dateCreated).format('MM-DD-YYYY HH:mm:ss'),
            dateUpdated: moment(entity.dateCreated).format('MM-DD-YYYY HH:mm:ss'),
        };
    }

    entitiesToDTOs(entities: Array<BazaContentTypesNewsletterEntity>): Array<NewslettersCsvDto> {
        return entities.map((e) => this.entityToDTO(e));
    }
}
