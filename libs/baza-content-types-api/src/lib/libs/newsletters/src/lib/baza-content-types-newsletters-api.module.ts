import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { NewslettersCmsController } from './controllers/newsletters-cms.controller';
import { NewslettersController } from './controllers/newsletters.controller';
import { NewsletterRepository } from './repositories/newsletter-repository.service';
import { NewslettersService } from './services/newsletters.service';
import { NewslettersMailService } from './services/newsletters-mail.service';
import { NewslettersCmsService } from './services/newsletters-cms.service';
import { NewslettersMapper } from './mappers/newsletters-mapper.service';
import { NewslettersCsvMapper } from './mappers/newsletters-csv-mapper.service';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        NewslettersController,
        NewslettersCmsController,
    ],
    providers: [
        NewslettersMapper,
        NewslettersCsvMapper,
        NewsletterRepository,
        NewslettersService,
        NewslettersCmsService,
        NewslettersMailService,
    ],
    exports: [
        NewslettersMapper,
        NewslettersCsvMapper,
        NewsletterRepository,
        NewslettersService,
    ],
})
export class BazaContentTypesNewslettersApiModule {}
