import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaContentTypesNewsletterEntity } from '../entities/baza-content-types-newsletter.entity';
import { NewslettersNotFoundException } from '../exceptions/newsletters-not-found.exception';

@Injectable()
export class NewsletterRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaContentTypesNewsletterEntity> {
        return this.connection.getRepository(BazaContentTypesNewsletterEntity);
    }

    async save(entity: BazaContentTypesNewsletterEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async remove(entity: BazaContentTypesNewsletterEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async getById(id: number): Promise<BazaContentTypesNewsletterEntity> {
        const entity = await this.repository.findOne({
            where: [{
                id,
            }],
        });

        if (! entity) {
            throw new NewslettersNotFoundException();
        }

        return entity;
    }

    async findByEmail(email: string): Promise<BazaContentTypesNewsletterEntity | undefined> {
        return this.repository.findOne({
            where: [{
                email,
            }],
        });
    }

    async getByEmail(email: string): Promise<BazaContentTypesNewsletterEntity> {
        const entity = await this.findByEmail(email);

        if (! entity) {
            throw new NewslettersNotFoundException();
        }

        return entity;
    }
}
