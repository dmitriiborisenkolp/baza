import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypesNewslettersApiModule } from './baza-content-types-newsletters-api.module';
import { NewslettersE2eExampleSubscriptionsFixture } from './integration-tests/fixtures/newsletters-e2e-example-subscriptions-fixture.service';

const E2E_FIXTURES = [
    NewslettersE2eExampleSubscriptionsFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypesNewslettersApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypesNewslettersApiE2eModule {}
