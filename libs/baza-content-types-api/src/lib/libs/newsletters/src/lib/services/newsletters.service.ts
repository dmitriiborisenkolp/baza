import { Injectable } from '@nestjs/common';
import { NewsletterRepository } from '../repositories/newsletter-repository.service';
import { BazaContentTypesNewsletterEntity } from '../entities/baza-content-types-newsletter.entity';
import { ApiEventBus, RequestContext } from '@scaliolabs/baza-core-api';
import { NewslettersMailService } from './newsletters-mail.service';
import {
    BAZA_CONTENT_TYPES_CONFIGURATION,
    NewslettersApiEvent,
    NewslettersApiEvents,
    NewsletterSubscribeRequest,
    NewsletterUnsubscribeRequest,
} from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class NewslettersService {
    constructor(
        private readonly mail: NewslettersMailService,
        private readonly repository: NewsletterRepository,
        private readonly bazaEventBus: ApiEventBus<NewslettersApiEvent, NewslettersApiEvents>,
    ) {}

    async subscribe(request: NewsletterSubscribeRequest, requestContext: RequestContext): Promise<BazaContentTypesNewsletterEntity> {
        const existing = await this.repository.findByEmail(request.email);

        if (existing) {
            return undefined;
        }

        if (BAZA_CONTENT_TYPES_CONFIGURATION.configs.newsletters.sendWelcomeEmail) {
            await this.mail.sendWelcome({
                name: request.name,
                email: request.email,
                app: requestContext.application,
                language: requestContext.language,
            });
        }

        const entity = new BazaContentTypesNewsletterEntity();

        entity.name = request.name;
        entity.email = request.email;

        await this.repository.save(entity);

        await this.bazaEventBus.publish({
            topic: NewslettersApiEvent.BazaNewsletterSubscribed,
            payload: {
                name: entity.name,
                email: entity.email,
            },
        });

        return entity;
    }

    async unsubscribe(request: NewsletterUnsubscribeRequest): Promise<void> {
        const entity = await this.repository.findByEmail(request.email);

        if (entity) {
            await this.repository.remove(entity);

            await this.bazaEventBus.publish({
                topic: NewslettersApiEvent.BazaNewsletterUnSubscribed,
                payload: {
                    name: entity.name,
                    email: entity.email,
                },
            });
        }
    }
}
