import 'reflect-metadata';
import { NewslettersCmsNodeAccess, NewslettersNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesNewslettersFixtures } from '../baza-content-types-newsletters-fixtures';

describe('@scaliolabs/baza-content-types/newsletters/001-baza-content-types-newsletters-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessNewslettersCms = new NewslettersCmsNodeAccess(http);
    const dataAccessNewsletters = new NewslettersNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesNewslettersFixtures.BazaContentTypesNewsletters],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will properly displays existing subscriptions in CMS', async () => {
        const response = await dataAccessNewslettersCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].email).toBe('e2e-example-3@scal.io');
        expect(response.items[1].email).toBe('e2e-example-2@scal.io');
        expect(response.items[2].email).toBe('e2e-example-1@scal.io');
    });

    it('will properly export existing subscriptions to CSV', async () => {
        const response = await dataAccessNewslettersCms.exportToCSV({
            listRequest: {},
            fields: [
                {
                    title: 'ID',
                    field: 'id',
                },
                {
                    title: 'Email',
                    field: 'email',
                },
                {
                    title: 'Created At',
                    field: 'dateCreated',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.split(`\n`).length).toBe(4);
    });

    it('will remove existing email from subscriptions', async () => {
        const response = await dataAccessNewslettersCms.remove({
            email: 'e2e-example-1@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that subscription is actually removed', async () => {
        const response = await dataAccessNewslettersCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].email).toBe('e2e-example-3@scal.io');
        expect(response.items[1].email).toBe('e2e-example-2@scal.io');
    });

    it('will successfully add subscription via CMS endpoint', async () => {
        const response = await dataAccessNewslettersCms.add({
            name: 'John Doe',
            email: 'john-doe@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessNewslettersCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(listResponse.items.length).toBe(3);
        expect(listResponse.items[0].name).toBe('John Doe');
        expect(listResponse.items[0].email).toBe('john-doe@scal.io');
        expect(listResponse.items[1].email).toBe('e2e-example-3@scal.io');
        expect(listResponse.items[2].email).toBe('e2e-example-2@scal.io');
    });

    it('will successfully add subscription via Public endpoint + name', async () => {
        const response = await dataAccessNewsletters.subscribe({
            name: 'Foo Bar',
            email: 'foo-bar@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessNewslettersCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(listResponse.items.length).toBe(4);
        expect(listResponse.items[0].name).toBe('Foo Bar');
        expect(listResponse.items[0].email).toBe('foo-bar@scal.io');
        expect(listResponse.items[1].name).toBe('John Doe');
        expect(listResponse.items[1].email).toBe('john-doe@scal.io');
        expect(listResponse.items[2].email).toBe('e2e-example-3@scal.io');
        expect(listResponse.items[3].email).toBe('e2e-example-2@scal.io');
    });
});
