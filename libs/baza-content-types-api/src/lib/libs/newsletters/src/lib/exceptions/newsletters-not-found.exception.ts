import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaContentTypesNewslettersErrorCodes, bazaContentTypesNewslettersErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';

export class NewslettersNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesNewslettersErrorCodes.BazaContentTypesNewslettersNotFound,
            bazaContentTypesNewslettersErrorCodesI18n[BazaContentTypesNewslettersErrorCodes.BazaContentTypesNewslettersNotFound],
            HttpStatus.NOT_FOUND
        );
    }
}
