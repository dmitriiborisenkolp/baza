import { Injectable } from '@nestjs/common';
import { NewsletterRepository } from '../repositories/newsletter-repository.service';
import { CrudCsvService, CrudService } from '@scaliolabs/baza-core-api';
import { NewslettersMapper } from '../mappers/newsletters-mapper.service';
import { BazaContentTypesNewsletterEntity } from '../entities/baza-content-types-newsletter.entity';
import { NewslettersCsvMapper } from '../mappers/newsletters-csv-mapper.service';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { ILike } from 'typeorm';
import { postgresLikeEscape } from '@scaliolabs/baza-core-shared';
import {
    NewsletterCmsExportToCsvExportRequest,
    NewsletterCmsListRequest,
    NewsletterCmsListResponse,
    NewsletterCmsRemoveRequest,
    NewslettersCsvDto,
} from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class NewslettersCmsService {
    constructor(
        private readonly mapper: NewslettersMapper,
        private readonly csvMapper: NewslettersCsvMapper,
        private readonly repository: NewsletterRepository,
        private readonly crud: CrudService,
        private readonly crudCsv: CrudCsvService,
    ) {}

    async list(request: NewsletterCmsListRequest): Promise<NewsletterCmsListResponse> {
        const findOptions: FindManyOptions<BazaContentTypesNewsletterEntity> = {
            order: {
                id: 'DESC',
            },
        };

        if (request.queryString) {
            findOptions.where = [
                {
                    email: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                },
            ];
        }

        return this.crud.find({
            request,
            entity: BazaContentTypesNewsletterEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions,
        });
    }

    async exportToCSV(request: NewsletterCmsExportToCsvExportRequest): Promise<string> {
        return this.crudCsv.exportToCsv<BazaContentTypesNewsletterEntity, NewslettersCsvDto>({
            fields: request.fields,
            request: request.listRequest,
            delimiter: request.delimiter,
            entity: BazaContentTypesNewsletterEntity,
            mapper: async (items) => this.csvMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
            },
        });
    }

    async remove(request: NewsletterCmsRemoveRequest): Promise<void> {
        const entity = await this.repository.getByEmail(request.email);

        await this.repository.remove(entity);
    }
}
