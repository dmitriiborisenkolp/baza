import { Controller, Get } from '@nestjs/common';
import { BazaContentTypesFaqService } from '../service/baza-content-types-faq.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { FaqEndpoint, FaqEndpointPaths, FaqListResponse, BazaContentTypesOpenApi } from '@scaliolabs/baza-content-types-shared';

@Controller()
@ApiTags(BazaContentTypesOpenApi.FAQ)
export class BazaContentTypesFaqController implements FaqEndpoint {
    constructor(private readonly service: BazaContentTypesFaqService) {}

    @Get(FaqEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Full list of FAQ Q/A',
    })
    @ApiOkResponse({
        type: FaqListResponse,
    })
    async list(): Promise<FaqListResponse> {
        return this.service.list();
    }
}
