import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaContentTypesFaqFixtures } from '../baza-content-types-faq.fixtures';
import { FaqCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesFaqCmsService } from '../../service/baza-content-types-faq-cms.service';
import { BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE } from '../../../../../categories/src/lib/integration-tests/fixtures/baza-content-types-categories.fixture';

export const E2E_BAZA_CONTENT_TYPES_FAQ_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    $categoryRefId?: number;
    request: FaqCmsCreateRequest;
}> = [
    {
        $refId: 1,
        $categoryRefId: 1,
        request: {
            question: 'Question 1',
            answer: 'Answer Q1',
            isPublished: true,
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 2,
        request: {
            question: 'Question 2',
            answer: 'Answer Q2',
            isPublished: true,
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 3,
        $categoryRefId: 1,
        request: {
            question: 'Question 3',
            answer: 'Answer Q1',
            isPublished: false,
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 4,
        $categoryRefId: 1,
        request: {
            question: 'Question 4',
            answer: 'Answer Q4',
            isPublished: true,
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 5,
        $categoryRefId: 2,
        request: {
            question: 'Question 5',
            answer: 'Answer Q5',
            isPublished: true,
            seo: bazaSeoDefault(),
        },
    },
];

@Injectable()
export class BazaContentTypeFaqFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaContentTypesFaqCmsService,
    ) {
    }

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_FAQ_FIXTURE) {
            const categoryId = fixtureRequest.$categoryRefId
                ? BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE.find((c) => c.$refId === fixtureRequest.$categoryRefId).$id
                : undefined;

            const entity = await this.service.create({
                ...fixtureRequest.request,
                categoryId,
            });

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesFaqFixtures>([{
    fixture: BazaContentTypesFaqFixtures.BazaContentTypesFaq,
    provider: BazaContentTypeFaqFixture,
}]);
