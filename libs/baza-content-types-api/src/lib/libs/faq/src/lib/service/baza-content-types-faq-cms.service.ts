import { Injectable } from '@nestjs/common';
import { FAQ_RELATIONS, BazaContentTypesFaqRepository } from '../repositories/baza-content-types-faq.repository';
import { BazaContentTypesFaqEntity } from '../entities/baza-content-types-faq.entity';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { CrudSetSortOrderResponseDto } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesFaqMapper } from '../mappers/baza-content-types-faq.mapper';
import { FaqCmsCreateRequest, FaqCmsGetByIdRequest, FaqCmsListRequest, FaqCmsListResponse, FaqCmsRemoveRequest, FaqCmsSetSortOrderRequest, FaqCmsUpdateRequest, FaqDto, BazaContentTypesFaqEntityBody } from '@scaliolabs/baza-content-types-shared';
import { CategoryRepository } from '../../../../categories/src';

@Injectable()
export class BazaContentTypesFaqCmsService {
    constructor(
        private readonly repository: BazaContentTypesFaqRepository,
        private readonly mapper: BazaContentTypesFaqMapper,
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly categoryRepository: CategoryRepository,
    ) {}

    async list(request: FaqCmsListRequest): Promise<FaqCmsListResponse> {
        return this.crud.find<BazaContentTypesFaqEntity, FaqDto>({
            request,
            entity: BazaContentTypesFaqEntity,
            mapper: async (items) => await this.mapper.entitiesToDTOs(items),
            findOptions: {
                relations: FAQ_RELATIONS,
                order: {
                    sortOrder: 'ASC',
                },
            },
            withMaxSortOrder: true,
        });
    }

    async create(request: FaqCmsCreateRequest): Promise<BazaContentTypesFaqEntity> {
        const entity = new BazaContentTypesFaqEntity();

        entity.dateCreated = new Date();
        entity.dateUpdated = entity.dateCreated;
        entity.sortOrder = await this.repository.maxSortOrder() + 1;

        await this.populate(entity, request);
        await this.repository.save(entity);

        return entity;
    }

    async update(request: FaqCmsUpdateRequest): Promise<BazaContentTypesFaqEntity> {
        const entity = await this.repository.getById(request.id);

        entity.dateUpdated = new Date();

        await this.populate(entity, request);
        await this.repository.save(entity);

        return entity;
    }

    async remove(request: FaqCmsRemoveRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove(entity);

        const entities = await this.repository.findAll();

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async getById(request: FaqCmsGetByIdRequest): Promise<BazaContentTypesFaqEntity> {
        return this.repository.getById(request.id);
    }

    async setSortOrder(request: FaqCmsSetSortOrderRequest): Promise<CrudSetSortOrderResponseDto<BazaContentTypesFaqEntity>> {
        const entity = await this.repository.getById(request.id);

        const response = await this.crudSort.setSortOrder<BazaContentTypesFaqEntity>({
            id: entity.id,
            setSortOrder: request.setSortOrder,
            entities: await this.repository.findAll(),
        });

        await this.repository.save(response.affected);

        return response;
    }

    async populate(entity: BazaContentTypesFaqEntity, entityBody: BazaContentTypesFaqEntityBody): Promise<void> {
        entity.isPublished = entityBody.isPublished;
        entity.category = entityBody.categoryId
            ? await this.categoryRepository.getById(entityBody.categoryId)
            : null;
        entity.question = entityBody.question;
        entity.answer = entityBody.answer;
        entity.seo = entityBody.seo;
    }
}
