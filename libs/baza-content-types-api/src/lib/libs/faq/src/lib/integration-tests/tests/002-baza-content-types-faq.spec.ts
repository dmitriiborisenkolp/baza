import 'reflect-metadata';
import { FaqNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesCategoriesFixtures } from '../../../../../categories/src';
import { BazaContentTypesFaqFixtures } from '../baza-content-types-faq.fixtures';

describe('@scaliolabs/baza-content-types-api/faq/001-baza-content-types-faq-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new FaqNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaContentTypesCategoriesFixtures.BazaContentTypeCategories,
                BazaContentTypesFaqFixtures.BazaContentTypesFaq,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it("will returns only published Q/A's", async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.entries.length).toBe(4);

        expect(response.entries.every((e) => e.isPublished)).toBeTruthy();
    });
});
