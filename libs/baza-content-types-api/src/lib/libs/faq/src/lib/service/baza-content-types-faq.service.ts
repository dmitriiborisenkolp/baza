import { Injectable } from '@nestjs/common';
import { BazaContentTypesFaqRepository } from '../repositories/baza-content-types-faq.repository';
import { BazaContentTypesFaqMapper } from '../mappers/baza-content-types-faq.mapper';
import { FaqListResponse } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class BazaContentTypesFaqService {
    constructor(
        private readonly faqRepository: BazaContentTypesFaqRepository,
        private readonly faqMapper: BazaContentTypesFaqMapper,
    ) {}

    async list(): Promise<FaqListResponse> {
        const entries = await this.faqRepository.findAllPublishedEntries();

        return {
            entries: await this.faqMapper.entitiesToDTOs(entries),
        };
    }
}
