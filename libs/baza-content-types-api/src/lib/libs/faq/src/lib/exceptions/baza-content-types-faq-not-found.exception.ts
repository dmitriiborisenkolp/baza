import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { bazaContentTypesErrorCodesI18n, BazaContentTypesFaqErrorCodes } from '@scaliolabs/baza-content-types-shared';

export class BazaContentTypesFaqNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesFaqErrorCodes.BazaContentTypesFaqNotFound,
            bazaContentTypesErrorCodesI18n[BazaContentTypesFaqErrorCodes.BazaContentTypesFaqNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
