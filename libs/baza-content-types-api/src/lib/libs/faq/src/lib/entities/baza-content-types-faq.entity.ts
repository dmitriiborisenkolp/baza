import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { bazaSeoDefault, BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { BazaContentTypeCategoryEntity } from '../../../../categories/src';

@Entity()
export class BazaContentTypesFaqEntity implements CrudSortableEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    sortOrder: number;

    @CreateDateColumn()
    dateCreated: Date;

    @UpdateDateColumn()
    dateUpdated: Date;

    @Column({
        default: false,
    })
    isPublished: boolean;

    @ManyToOne(() => BazaContentTypeCategoryEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
    })
    category?: BazaContentTypeCategoryEntity;

    @Column()
    question: string;

    @Column()
    answer: string;

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto;
}
