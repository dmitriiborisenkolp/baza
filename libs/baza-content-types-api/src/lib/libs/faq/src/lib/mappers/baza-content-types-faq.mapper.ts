import { Injectable } from '@nestjs/common';
import { BazaContentTypesFaqEntity } from '../entities/baza-content-types-faq.entity';
import { FaqDto } from '@scaliolabs/baza-content-types-shared';
import { CategoryListMapper } from '../../../../categories/src';

@Injectable()
export class BazaContentTypesFaqMapper {
    constructor(
        private readonly categoryMapper: CategoryListMapper,
    ) {}

    async entityToDTO(input: BazaContentTypesFaqEntity): Promise<FaqDto> {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            url: input.seo && input.seo.urn
                ? input.seo.urn
                : input.id.toString(),
            dateCreated: input.dateCreated.toISOString(),
            dateUpdated: input.dateUpdated.toISOString(),
            isPublished: input.isPublished,
            categoryId: input.category
                ? input.category.id
                : undefined,
            category: input.category
                ? this.categoryMapper.entityToDTO(input.category)
                : undefined,
            question: input.question,
            answer: input.answer,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<BazaContentTypesFaqEntity>): Promise<Array<FaqDto>> {
        const result: Array<FaqDto> = [];

        for (const entity of input) {
            result.push(
                await this.entityToDTO(entity),
            );
        }

        return result;
    }
}
