import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypesFaqApiModule } from './baza-content-types-faq-api.module';
import { BazaContentTypeFaqFixture } from './integration-tests/fixtures/baza-content-type-faq.fixture';

const E2E_FIXTURES = [
    BazaContentTypeFaqFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypesFaqApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypesFaqApiE2eModule {}
