import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { WithAccessGuard } from '@scaliolabs/baza-core-api';
import { AclNodes } from '@scaliolabs/baza-core-api';
import { BazaContentTypesFaqCmsService } from '../service/baza-content-types-faq-cms.service';
import { BazaContentTypesFaqMapper } from '../mappers/baza-content-types-faq.mapper';
import {
    BazaContentTypeAcl,
    FaqDto,
    BazaContentTypesCmsOpenApi,
    FaqCmsEndpoint,
    FaqCmsEndpointPaths,
    FaqCmsListRequest,
    FaqCmsListResponse,
    FaqCmsCreateRequest,
    FaqCmsCreateResponse,
    FaqCmsUpdateRequest,
    FaqCmsUpdateResponse,
    FaqCmsRemoveRequest,
    FaqCmsRemoveResponse,
    FaqCmsGetByIdRequest,
    FaqCmsGetByIdResponse,
    FaqCmsSetSortOrderResponse,
    FaqCmsSetSortOrderRequest,
} from '@scaliolabs/baza-content-types-shared';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.FAQ)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesFAQ])
export class BazaContentTypesFaqCmsController implements FaqCmsEndpoint {
    constructor(private readonly service: BazaContentTypesFaqCmsService, private readonly mapper: BazaContentTypesFaqMapper) {}

    @Post(FaqCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List of FAQ entities',
    })
    @ApiOkResponse({
        type: FaqCmsListResponse,
    })
    async list(@Body() request: FaqCmsListRequest): Promise<FaqCmsListResponse> {
        return this.service.list(request);
    }

    @Post(FaqCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
        description: 'Create new FAQ entity',
    })
    @ApiOkResponse({
        type: FaqDto,
    })
    async create(@Body() request: FaqCmsCreateRequest): Promise<FaqCmsCreateResponse> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(FaqCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
        description: 'Update existing FAQ entity',
    })
    @ApiOkResponse({
        type: FaqDto,
    })
    async update(@Body() request: FaqCmsUpdateRequest): Promise<FaqCmsUpdateResponse> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(FaqCmsEndpointPaths.remove)
    @ApiOperation({
        summary: 'remove',
        description: 'Remove existing FAQ entity',
    })
    @ApiOkResponse()
    async remove(@Body() request: FaqCmsRemoveRequest): Promise<FaqCmsRemoveResponse> {
        await this.service.remove(request);
    }

    @Post(FaqCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns FAQ entity by ID',
    })
    @ApiOkResponse({
        type: FaqDto,
    })
    async getById(@Body() request: FaqCmsGetByIdRequest): Promise<FaqCmsGetByIdResponse> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(FaqCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
        description: 'Set sort order of FAQ entities',
    })
    @ApiOkResponse({
        type: FaqCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: FaqCmsSetSortOrderRequest): Promise<FaqCmsSetSortOrderResponse> {
        const response = await this.service.setSortOrder(request);

        return {
            ...response,
            affected: await this.mapper.entitiesToDTOs(response.affected),
            entity: await this.mapper.entityToDTO(response.entity),
        };
    }
}
