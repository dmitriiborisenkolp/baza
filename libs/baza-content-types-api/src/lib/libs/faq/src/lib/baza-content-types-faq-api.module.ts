import { Module } from '@nestjs/common';
import { BazaContentTypesFaqCmsController } from './controllers/baza-content-types-faq-cms.controller';
import { BazaContentTypesFaqMapper } from './mappers/baza-content-types-faq.mapper';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BazaContentTypesFaqRepository } from './repositories/baza-content-types-faq.repository';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaContentTypesFaqService } from './service/baza-content-types-faq.service';
import { BazaContentTypesFaqController } from './controllers/baza-content-types-faq.controller';
import { BazaContentTypesFaqCmsService } from './service/baza-content-types-faq-cms.service';

const SERVICES = [
    BazaContentTypesFaqService,
    BazaContentTypesFaqCmsService,
    BazaContentTypesFaqMapper,
    BazaContentTypesFaqRepository,
];

@Module({
    imports: [
        TypeOrmModule,
        BazaCrudApiModule,
    ],
    controllers: [
        BazaContentTypesFaqController,
        BazaContentTypesFaqCmsController,
    ],
    providers: [
        ...SERVICES,
    ],
    exports: [
        ...SERVICES,
    ]
})
export class BazaContentTypesFaqApiModule {
}
