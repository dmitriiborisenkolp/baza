import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaContentTypesFaqEntity } from '../entities/baza-content-types-faq.entity';
import { BazaContentTypesFaqNotFoundException } from '../exceptions/baza-content-types-faq-not-found.exception';

export const FAQ_RELATIONS = [
    'category',
];

@Injectable()
export class BazaContentTypesFaqRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaContentTypesFaqEntity> {
        return this.connection.getRepository(BazaContentTypesFaqEntity);
    }

    async save(entities: BazaContentTypesFaqEntity | Array<BazaContentTypesFaqEntity>): Promise<void> {
        if (Array.isArray(entities)) {
            for (const entity of entities) {
                await this.repository.save(entity);
            }
        } else {
            await this.repository.save(entities);
        }
    }

    async remove(entity: BazaContentTypesFaqEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async findById(id: number): Promise<BazaContentTypesFaqEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: FAQ_RELATIONS,
        });
    }

    async getById(id: number): Promise<BazaContentTypesFaqEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BazaContentTypesFaqNotFoundException();
        }

        return entity;
    }

    async maxSortOrder(): Promise<number> {
        const result: { max: number } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return result.max || 0;
    }

    async findAll(): Promise<Array<BazaContentTypesFaqEntity>> {
        return this.repository.find({
            relations: FAQ_RELATIONS,
            order: {
                sortOrder: 'ASC',
            },
        });
    }

    async findAllPublishedEntries(): Promise<Array<BazaContentTypesFaqEntity>> {
        return this.repository.find({
            relations: FAQ_RELATIONS,
            order: {
                sortOrder: 'ASC',
            },
            where: [{
                isPublished: true,
            }],
        });
    }
}
