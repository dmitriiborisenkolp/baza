import 'reflect-metadata';
import { CategoriesCmsNodeAccess, FaqCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { CategoriesDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesCategoriesFixtures } from '../../../../../categories/src';

describe('@scaliolabs/baza-content-types-api/faq/001-baza-content-types-faq-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new FaqCmsNodeAccess(http);
    const dataAccessCategories = new CategoriesCmsNodeAccess(http);

    let ID: number;
    let CATEGORIES: CategoriesDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesCategoriesFixtures.BazaContentTypeCategories],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        CATEGORIES = await dataAccessCategories.getAll();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create faq entry with category', async () => {
        const categoryId = CATEGORIES.children[0].category.id;

        const response = await dataAccess.create({
            categoryId,
            isPublished: true,
            question: 'Q1',
            answer: 'A1',
            seo: bazaSeoDefault(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.question).toBe('Q1');
        expect(response.answer).toBe('A1');
        expect(response.isPublished).toBeTruthy();
        expect(response.categoryId).toBe(categoryId);
        expect(response.category.id).toBe(categoryId);
    });

    it('will successfully create faq entry without category', async () => {
        const response = await dataAccess.create({
            isPublished: true,
            question: 'Q2',
            answer: 'A2',
            seo: bazaSeoDefault(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBeDefined();
        expect(response.question).toBe('Q2');
        expect(response.answer).toBe('A2');
        expect(response.isPublished).toBeTruthy();
        expect(response.categoryId).not.toBeDefined();
        expect(response.category).not.toBeDefined();

        ID = response.id;
    });

    it('will successfully update category', async () => {
        const response = await dataAccess.update({
            id: ID,
            isPublished: false,
            question: 'Q2 *',
            answer: 'A2 *',
            seo: bazaSeoDefault(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(ID);
        expect(response.question).toBe('Q2 *');
        expect(response.answer).toBe('A2 *');
    });

    it('will display updated category correctly', async () => {
        const listResponse = await dataAccess.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].question).toBe('Q1');
        expect(listResponse.items[1].question).toBe('Q2 *');
    });

    it('will successfully move faq entry', async () => {
        const response = await dataAccess.setSortOrder({
            id: ID,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.entity.sortOrder).toBe(1);
    });

    it('will display new sort order correctly', async () => {
        const listResponse = await dataAccess.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].question).toBe('Q2 *');
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[1].question).toBe('Q1');
        expect(listResponse.items[1].sortOrder).toBe(2);
    });

    it('will successfully delete faq entry', async () => {
        const response = await dataAccess.remove({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will removes entity from next list response and update sort order correctly', async () => {
        const listResponse = await dataAccess.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].question).toBe('Q1');
        expect(listResponse.items[0].sortOrder).toBe(1);
    });
});
