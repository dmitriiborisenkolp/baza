import { Injectable } from '@angular/core';
import { Connection, Repository } from 'typeorm';
import { BazaContentTypesContactsSubjectEntity } from '../entities/baza-content-type-contacts-subject.entity';
import { BazaContentTypesContactsSubjectNotFoundException } from '../exceptions/baza-content-type-contacts-subject-not-found.exception';

export const BAZA_CONTENT_TYPES_CONTACTS_SUBJECTS_RELATIONS = [];

@Injectable()
export class BazaContentTypesContactsSubjectRepository {
    constructor(private readonly connection: Connection) {}

    get repository(): Repository<BazaContentTypesContactsSubjectEntity> {
        return this.connection.getRepository(BazaContentTypesContactsSubjectEntity);
    }

    async save(entities: BazaContentTypesContactsSubjectEntity | Array<BazaContentTypesContactsSubjectEntity>): Promise<void> {
        if (Array.isArray(entities)) {
            for (const entity of entities) {
                await this.save(entity);
            }
        } else {
            await this.repository.save(entities);
        }
    }

    async remove(): Promise<void> {
        throw new Error('Do not delete subjects at all!');
    }

    async getById(id: number): Promise<BazaContentTypesContactsSubjectEntity> {
        const entity = await this.repository.findOne({
            where: [
                {
                    id,
                },
            ],
            relations: BAZA_CONTENT_TYPES_CONTACTS_SUBJECTS_RELATIONS,
        });

        if (!entity) {
            throw new BazaContentTypesContactsSubjectNotFoundException();
        }

        return entity;
    }

    async maxSortOrder(): Promise<number> {
        const result: { max: number } = await this.repository.createQueryBuilder('e').select('MAX(e.sortOrder)', 'max').getRawOne();

        return result.max || 0;
    }

    async findAll(): Promise<Array<BazaContentTypesContactsSubjectEntity>> {
        return this.repository.find({
            where: [
                {
                    isDeleted: false,
                },
            ],
            order: {
                sortOrder: 'ASC',
            },
        });
    }

    async findAllEnabled(): Promise<Array<BazaContentTypesContactsSubjectEntity>> {
        return this.repository.find({
            where: [
                {
                    isDeleted: false,
                    isEnabled: true,
                },
            ],
            order: {
                sortOrder: 'ASC',
            },
        });
    }
}
