import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypesContactsErrorCodes, bazaContentTypesContactsErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaContentTypesContactsSubjectNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesContactsErrorCodes.BazaContentTypesContactsSubjectNotFound,
            bazaContentTypesContactsErrorCodesI18n[BazaContentTypesContactsErrorCodes.BazaContentTypesContactsSubjectNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
