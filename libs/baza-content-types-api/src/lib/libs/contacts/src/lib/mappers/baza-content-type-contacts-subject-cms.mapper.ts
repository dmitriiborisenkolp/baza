import { Injectable } from '@nestjs/common';
import { BazaContentTypesContactsSubjectEntity } from '../entities/baza-content-type-contacts-subject.entity';
import { ContactsSubjectCmsDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class BazaContentTypesContactsSubjectCmsMapper {
    entityToDTO(entity: BazaContentTypesContactsSubjectEntity): ContactsSubjectCmsDto {
        return {
            id: entity.id,
            sortOrder: entity.sortOrder,
            isEnabled: entity.isEnabled,
            subject: entity.subject,
        };
    }

    entitiesToDTOs(entities: Array<BazaContentTypesContactsSubjectEntity>): Array<ContactsSubjectCmsDto> {
        return entities.map((e) => this.entityToDTO(e));
    }
}
