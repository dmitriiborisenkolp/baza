import 'reflect-metadata';
import { ContactsNodeAccess, ContactsSubjectCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaContentTypesContactsErrorCodes, ContactsSubjectCmsDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesContactsFixture } from '../baza-content-type-contacts.fixture';
import { asyncExpect } from '@scaliolabs/baza-core-api';

describe('@scaliolabs/baza-content-types-api/contacts//003-baza-content-type-contacts.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new ContactsNodeAccess(http);
    const dataAccessSubjectCms = new ContactsSubjectCmsNodeAccess(http);

    let subjects: Array<ContactsSubjectCmsDto> = [];

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesContactsFixture.BazaContentTypesContactsSubjects],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        subjects = (await dataAccessSubjectCms.list({})).items;
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will display only enabled subjects', async () => {
        const response = await dataAccess.subjects();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);
        expect(response[0].subject).toBe('Example Subject 1');
        expect(response[1].subject).toBe('Example Subject 2');
    });

    it('will allow to send new message', async () => {
        const mailboxBefore = await dataAccessE2e.mailbox();

        expect(mailboxBefore.length).toBe(0);

        const response = await dataAccess.send({
            subjectId: subjects.find((s) => s.subject === 'Example Subject 1').id,
            email: 'foo-bar@scal.io',
            name: 'Foo Bar',
            message: 'Some example message',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('verify admin email message', async () => {
        await asyncExpect(
            async () => {
                const mailbox = await dataAccessE2e.mailbox();
                const contactUsEmail = 'baza-test-contact-us@scal.io';
                const mail = mailbox.find((i) => i.to.includes(contactUsEmail));

                expect(mailbox.length).toBe(2);
                expect(mail).toBeDefined();
                expect(mail.to[0]).not.toBe('foo-bar@scal.io');
                expect(mail.subject).toContain('Baza Client e2e: Welcome');
                expect(mail.messageBodyHtml).toContain('Foo Bar');
                expect(mail.messageBodyHtml).toContain('foo-bar@scal.io');
                expect(mail.messageBodyHtml).toContain('Some example message');
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('verify client email message', async () => {
        await asyncExpect(
            async () => {
                const mailbox = await dataAccessE2e.mailbox();
                const clientEmail = 'foo-bar@scal.io';
                const contactUsEmail = 'baza-test-contact-us@scal.io';
                const mail = mailbox.find((i) => i.to.includes(clientEmail));

                expect(mailbox.length).toBe(2);
                expect(mail).toBeDefined();
                expect(mail.to[0]).toBe(clientEmail);
                expect(mail.subject).toContain('Thanks for Reaching Out!');
                expect(mail.messageBodyHtml).toContain('Foo Bar');
                expect(mail.messageBodyHtml).toContain(contactUsEmail);
                expect(mail.messageBodyHtml).toContain('Some example message');
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will not allow to send contact us to disabled subject', async () => {
        const response: BazaError = (await dataAccess.send({
            subjectId: subjects.find((s) => !s.isEnabled).id,
            email: 'foo-bar@scal.io',
            name: 'Foo Bar',
            message: 'Some example message',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaContentTypesContactsErrorCodes.BazaContentTypesContactsSubjectIsNotEnabled);
    });

    it('will not allow to send contact us to unknown subject', async () => {
        const response: BazaError = (await dataAccess.send({
            subjectId: 999,
            email: 'foo-bar@scal.io',
            name: 'Foo Bar',
            message: 'Some example message',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaContentTypesContactsErrorCodes.BazaContentTypesContactsSubjectNotFound);
    });
});
