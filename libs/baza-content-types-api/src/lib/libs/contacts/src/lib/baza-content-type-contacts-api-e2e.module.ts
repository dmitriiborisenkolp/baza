import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypesContactsApiModule } from './baza-content-type-contacts-api.module';
import { BazaContentTypesContactsExamplesFixture } from './integration-tests/fixtures/baza-content-type-contacts-examples.fixture';
import { BazaContentTypesContactsSubjectsFixture } from './integration-tests/fixtures/baza-content-type-contacts-subjects.fixture';

const E2E_FIXTURES = [
    BazaContentTypesContactsExamplesFixture,
    BazaContentTypesContactsSubjectsFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypesContactsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypeContactsApiE2eModule {}
