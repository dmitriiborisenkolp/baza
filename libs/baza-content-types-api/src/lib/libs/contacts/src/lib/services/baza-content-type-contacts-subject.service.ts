import { Injectable } from '@nestjs/common';
import { BazaContentTypesContactsSubjectRepository } from '../repositories/baza-content-type-contacts-subject.repository';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { ContactsSubjectCmsCreateRequest, ContactsSubjectCmsDeleteRequest, ContactsSubjectCmsDto, BazaContentTypesContactsSubjectCmsEntityBody, ContactsSubjectCmsListRequest, ContactsSubjectCmsListResponse, ContactsSubjectCmsSetSortOrderRequest, ContactsSubjectCmsSetSortOrderResponse, ContactsSubjectCmsUpdateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypesContactsSubjectCmsMapper } from '../mappers/baza-content-type-contacts-subject-cms.mapper';
import { BazaContentTypesContactsSubjectEntity } from '../entities/baza-content-type-contacts-subject.entity';

@Injectable()
export class BazaContentTypesContactsSubjectService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: BazaContentTypesContactsSubjectRepository,
        private readonly cmsMapper: BazaContentTypesContactsSubjectCmsMapper,
    ) {}

    async list(request: ContactsSubjectCmsListRequest): Promise<ContactsSubjectCmsListResponse> {
        return this.crud.find<BazaContentTypesContactsSubjectEntity, ContactsSubjectCmsDto>({
            request,
            entity: BazaContentTypesContactsSubjectEntity,
            mapper: async (items) => this.cmsMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'ASC',
                },
                where: [{
                    isDeleted: false,
                }],
            },
        })
    }

    async subjects(): Promise<Array<BazaContentTypesContactsSubjectEntity>> {
        return this.repository.findAllEnabled();
    }

    async create(request: ContactsSubjectCmsCreateRequest): Promise<BazaContentTypesContactsSubjectEntity> {
        const entity = new BazaContentTypesContactsSubjectEntity();

        await this.populate(entity, request);

        entity.sortOrder = (await this.repository.maxSortOrder()) + 1;

        await this.repository.save(entity);

        return entity;
    }

    async update(request: ContactsSubjectCmsUpdateRequest): Promise<BazaContentTypesContactsSubjectEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);

        await this.repository.save(entity);

        return entity;
    }

    async delete(request: ContactsSubjectCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        entity.isDeleted = true;

        await this.repository.save(entity);
    }

    async setSortOrder(request: ContactsSubjectCmsSetSortOrderRequest): Promise<ContactsSubjectCmsSetSortOrderResponse> {
        const response = await this.crudSort.setSortOrder<BazaContentTypesContactsSubjectEntity>({
            id: request.id,
            entities: await this.repository.findAll(),
            setSortOrder: request.setSortOrder,
        });

        await this.repository.save(response.entity);
        await this.repository.save(response.affected);

        return {
            ...response,
            affected: this.cmsMapper.entitiesToDTOs(response.affected),
            entity: this.cmsMapper.entityToDTO(response.entity),
        };
    }

    private async populate(entity: BazaContentTypesContactsSubjectEntity, requestBody: BazaContentTypesContactsSubjectCmsEntityBody): Promise<void> {
        entity.subject = requestBody.subject;
        entity.isEnabled = requestBody.isEnabled;
    }
}
