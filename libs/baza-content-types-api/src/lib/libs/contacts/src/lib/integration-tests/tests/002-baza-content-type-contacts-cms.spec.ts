import 'reflect-metadata';
import { ContactsCmsNodeAccess, ContactsSubjectCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesContactsFixture } from '../baza-content-type-contacts.fixture';

describe('@scaliolabs/baza-content-types-api/contacts//002-baza-content-type-contacts-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessContactUsCms = new ContactsCmsNodeAccess(http);
    const dataAccessSubjectCms = new ContactsSubjectCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaContentTypesContactsFixture.BazaContentTypesContactsSubjects,
                BazaContentTypesContactsFixture.BazaContentTypesContactsExamples,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will have entities in list response', async () => {
        const response = await dataAccessContactUsCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].subject.subject).toBe('Example Subject 2');
        expect(response.items[1].subject.subject).toBe('Example Subject 2');
        expect(response.items[2].subject.subject).toBe('Example Subject 2');
        expect(response.items[3].subject.subject).toBe('Example Subject 1');
    });

    it('will allow to return entity by id', async () => {
        const listResponse = await dataAccessContactUsCms.list({});

        const response = await dataAccessContactUsCms.getById({
            id: listResponse.items[0].id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(listResponse.items[0].id);
    });

    it('will mark entity as processed and set dataProcessedAt', async () => {
        const response = await dataAccessContactUsCms.list({});

        expect(response.items[0].dateProcessedAt).toBeUndefined();

        const markResponse = await dataAccessContactUsCms.markAsProcessed({
            id: response.items[0].id,
        });

        expect(isBazaErrorResponse(markResponse)).toBeFalsy();

        const newResponse = await dataAccessContactUsCms.list({});

        expect(newResponse.items[0].dateProcessedAt).not.toBeUndefined();
    });

    it('will unmark entity as processed and unset dateProcessedAt', async () => {
        const response = await dataAccessContactUsCms.list({});

        const markResponse = await dataAccessContactUsCms.markAsUnprocessed({
            id: response.items[0].id,
        });

        expect(isBazaErrorResponse(markResponse)).toBeFalsy();

        const newResponse = await dataAccessContactUsCms.list({});

        expect(newResponse.items[0].dateProcessedAt).toBeUndefined();
    });

    it('will still displays entities after removing subject', async () => {
        const subjects = await dataAccessSubjectCms.list({});

        for (const subject of subjects.items) {
            const deleteResponse = await dataAccessSubjectCms.delete({
                id: subject.id,
            });

            expect(isBazaErrorResponse(deleteResponse)).toBeFalsy();
        }

        const response = await dataAccessContactUsCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);

        expect(response.items[0].subject.subject).toBe('Example Subject 2');
        expect(response.items[1].subject.subject).toBe('Example Subject 2');
        expect(response.items[2].subject.subject).toBe('Example Subject 2');
        expect(response.items[3].subject.subject).toBe('Example Subject 1');
    });
});
