import { RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';

export const bazaContentTypeContactsApiRegistry: RegistrySchema = {
    contacts: {
        email: {
            name: 'Contacts Email',
            type: RegistryType.Email,
            hiddenFromList: false,
            public: true,
            defaults: 'baza-test-contact-us@scal.io',
        },
        subject: {
            name: 'Contacts Subject Template',
            type: RegistryType.String,
            hiddenFromList: false,
            public: false,
            defaults: '{{ clientName }}: Welcome',
        },
        clientSubject: {
            name: 'Contact Us Client Response Subject Template',
            type: RegistryType.String,
            hiddenFromList: false,
            public: false,
            defaults: 'Thanks for Reaching Out!',
        },
    },
};
