import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BazaContentTypesContactsSubjectEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    isEnabled: boolean;

    @Column({
        type: 'boolean',
    })
    isDeleted = false;

    @Column()
    sortOrder: number;

    @Column()
    subject: string;
}
