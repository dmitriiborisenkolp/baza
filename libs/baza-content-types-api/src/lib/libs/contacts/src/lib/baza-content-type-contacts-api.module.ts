import { Global, Module } from '@nestjs/common';
import { BazaContentTypesContactsSubjectCmsController } from './controllers/baza-content-type-contacts-subject-cms.controller';
import { BazaContentTypesContactsController } from './controllers/baza-content-type-contacts.controller';
import { BazaContentTypesContactsCmsController } from './controllers/baza-content-type-contacts-cms.controller';
import { BazaContentTypesContactsSubjectMapper } from './mappers/baza-content-type-contacts-subject.mapper';
import { BazaContentTypesContactsSubjectCmsMapper } from './mappers/baza-content-type-contacts-subject-cms.mapper';
import { BazaContentTypesContactsCmsMapper } from './mappers/baza-content-type-contacts-cms.mapper';
import { BazaContentTypesContactsSubjectService } from './services/baza-content-type-contacts-subject.service';
import { BazaContentTypesContactsSubjectRepository } from './repositories/baza-content-type-contacts-subject.repository';
import { BazaContentTypesContactsService } from './services/baza-content-type-contacts.service';
import { BazaContentTypesContactsRepository } from './repositories/baza-content-type-contacts.repository';
import { BazaCrudApiModule, BazaRegistryApiModule } from '@scaliolabs/baza-core-api';

@Global()
@Module({
    imports: [
        BazaCrudApiModule,
        BazaRegistryApiModule,
    ],
    controllers: [
        BazaContentTypesContactsController,
        BazaContentTypesContactsCmsController,
        BazaContentTypesContactsSubjectCmsController,
    ],
    providers: [
        BazaContentTypesContactsCmsMapper,
        BazaContentTypesContactsSubjectMapper,
        BazaContentTypesContactsSubjectCmsMapper,
        BazaContentTypesContactsRepository,
        BazaContentTypesContactsSubjectRepository,
        BazaContentTypesContactsService,
        BazaContentTypesContactsSubjectService,
    ],
    exports: [
        BazaContentTypesContactsCmsMapper,
        BazaContentTypesContactsSubjectMapper,
        BazaContentTypesContactsSubjectCmsMapper,
        BazaContentTypesContactsRepository,
        BazaContentTypesContactsSubjectRepository,
        BazaContentTypesContactsService,
        BazaContentTypesContactsSubjectService,
    ],
})
export class BazaContentTypesContactsApiModule {
}
