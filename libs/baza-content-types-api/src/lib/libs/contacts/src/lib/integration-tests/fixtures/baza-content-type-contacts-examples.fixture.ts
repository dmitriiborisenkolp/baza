import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaContentTypesContactsFixture } from '../baza-content-type-contacts.fixture';
import { BazaContentTypesContactsApiModule } from '../../baza-content-type-contacts-api.module';
import { E2E_BAZA_CONTENT_TYPES_CONTACTS_SUBJECTS } from './baza-content-type-contacts-subjects.fixture';
import { BazaContentTypesContactsService } from '../../services/baza-content-type-contacts.service';
import { ContactsSendRequest } from '@scaliolabs/baza-content-types-shared';

const subjectId = (id: number) => {
    return E2E_BAZA_CONTENT_TYPES_CONTACTS_SUBJECTS.find((e) => e.$refId === id).$id;
}

export const E2E_BAZA_CONTENT_TYPES_CONTACTS_EXAMPLES: () => Array<{
    $id?: number;
    request: ContactsSendRequest;
}> = () => ([
    {
        request: {
            name: 'Foo Bar',
            message: 'Example message body 1',
            email: 'foo-bar@scal.io',
            subjectId: subjectId(1),
        },
    },
    {
        request: {
            name: 'Foo Baz',
            message: 'Example message body 2',
            email: 'foo-bar@scal.io',
            subjectId: subjectId(2),
        },
    },
    {
        request: {
            name: 'John Doe',
            message: 'Example message body 3',
            email: 'foo-bar@scal.io',
            subjectId: subjectId(2),
        },
    },
    {
        request: {
            name: 'Donald Joe',
            message: 'Example message body 4',
            email: 'foo-bar@scal.io',
            subjectId: subjectId(2),
        },
    },
]);

@Injectable()
export class BazaContentTypesContactsExamplesFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaContentTypesContactsService,
    ) {}

    async up(): Promise<void> {
        for (const request of E2E_BAZA_CONTENT_TYPES_CONTACTS_EXAMPLES()) {
            const entity = await this.service.send(request.request);

            request.$id = entity.id;
        }
    }
}

defineE2eFixtures([{
    fixture: BazaContentTypesContactsFixture.BazaContentTypesContactsExamples,
    provider: BazaContentTypesContactsExamplesFixture,
    module: BazaContentTypesContactsApiModule,
}]);
