import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BazaContentTypesContactsSubjectEntity } from './baza-content-type-contacts-subject.entity';

@Entity()
export class BazaContentTypesContactsEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    dateCreatedAt: Date = new Date();

    @Column({
        nullable: true,
    })
    dateProcessedAt: Date;

    @ManyToOne(() => BazaContentTypesContactsSubjectEntity, {
        onDelete: 'SET NULL',
        onUpdate: 'CASCADE',
    })
    subject?: BazaContentTypesContactsSubjectEntity;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    message: string;

    @Column({
        type: 'boolean',
    })
    sent = false;
}
