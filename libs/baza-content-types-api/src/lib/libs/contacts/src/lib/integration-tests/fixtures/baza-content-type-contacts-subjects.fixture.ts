import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaContentTypesContactsSubjectService } from '../../services/baza-content-type-contacts-subject.service';
import { BazaContentTypesContactsFixture } from '../baza-content-type-contacts.fixture';
import { BazaContentTypesContactsApiModule } from '../../baza-content-type-contacts-api.module';

export const E2E_BAZA_CONTENT_TYPES_CONTACTS_SUBJECTS: Array<{
    $id?: number;
    $refId: number;
    subject: string;
    isEnabled: boolean;
}> = [
    {
        $refId: 1,
        subject: 'Example Subject 1',
        isEnabled: true,
    },
    {
        $refId: 2,
        subject: 'Example Subject 2',
        isEnabled: true,
    },
    {
        $refId: 3,
        subject: 'Example Subject 3',
        isEnabled: false,
    },
];

@Injectable()
export class BazaContentTypesContactsSubjectsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaContentTypesContactsSubjectService,
    ) {}

    async up(): Promise<void> {
        for (const request of E2E_BAZA_CONTENT_TYPES_CONTACTS_SUBJECTS) {
            const entity = await this.service.create({
                isEnabled: request.isEnabled,
                subject: request.subject,
            });

            request.$id = entity.id;
        }
    }
}

defineE2eFixtures([{
    fixture: BazaContentTypesContactsFixture.BazaContentTypesContactsSubjects,
    provider: BazaContentTypesContactsSubjectsFixture,
    module: BazaContentTypesContactsApiModule,
}]);
