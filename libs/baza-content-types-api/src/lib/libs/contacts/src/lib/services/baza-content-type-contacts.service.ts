import { Injectable } from '@nestjs/common';
import {
    BAZA_CONTENT_TYPES_CONTACTS_RELATIONS,
    BazaContentTypesContactsRepository,
} from '../repositories/baza-content-type-contacts.repository';
import { BazaLogger, BazaRegistryService, CrudService, MailService } from '@scaliolabs/baza-core-api';
import {
    ContactsCmsListRequest,
    ContactsCmsListResponse,
    ContactsCmsMarkAsProcessedRequest,
    ContactsCmsMarkAsUnprocessedRequest,
    ContactsSendRequest,
} from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypesContactsCmsMapper } from '../mappers/baza-content-type-contacts-cms.mapper';
import { BazaContentTypesContactsEntity } from '../entities/baza-content-type-contacts.entity';
import { BazaContentTypesContactsSubjectRepository } from '../repositories/baza-content-type-contacts-subject.repository';
import { postgresLikeEscape, replaceTags } from '@scaliolabs/baza-core-shared';
import * as _ from 'underscore';
import { BazaContentTypesContactsSubjectIsNotEnabledException } from '../exceptions/baza-content-type-contacts-subject-is-not-enabled.exception';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { ILike } from 'typeorm';
import { BazaContentTypesMail } from '../../../../../constants/baza-content-types.mail-templates';

@Injectable()
export class BazaContentTypesContactsService {
    constructor(
        private readonly crud: CrudService,
        private readonly mail: MailService,
        private readonly logger: BazaLogger,
        private readonly mapper: BazaContentTypesContactsCmsMapper,
        private readonly repository: BazaContentTypesContactsRepository,
        private readonly subjectRepository: BazaContentTypesContactsSubjectRepository,
        private readonly registry: BazaRegistryService,
    ) {}

    async send(request: ContactsSendRequest): Promise<BazaContentTypesContactsEntity> {
        const entity = new BazaContentTypesContactsEntity();

        entity.name = request.name;
        entity.email = request.email;
        entity.subject = await this.subjectRepository.getById(request.subjectId);
        entity.message = request.message;

        if (!entity.subject.isEnabled) {
            throw new BazaContentTypesContactsSubjectIsNotEnabledException();
        }

        await this.repository.save(entity);

        this.publishClientResponseEmail(entity);

        const sendMailResponse = this.mail.send({
            name: BazaContentTypesMail.BazaContentTypesContactClientRequest,
            subject: replaceTags(this.registry.getValue('bazaContentTypes.contacts.subject'), {
                subject: entity.subject.subject,
            }),
            to: [
                {
                    email: this.registry.getValue('bazaContentTypes.contacts.email'),
                },
            ],
            uniqueId: `baza_content_types_contacts_${entity.id}`,
            variables: () => ({
                name: entity.name,
                email: entity.email,
                subject: entity.subject.subject,
                rawMessage: entity.message,
                htmlMessage: _.escape(entity.message).split('\n').join('<br/>'),
                contactUs: this.registry.getValue('bazaContentTypes.contacts.email'),
            }),
        });

        sendMailResponse
            .then(() => {
                entity.sent = true;

                return this.repository.save(entity);
            })
            .catch((err) => {
                this.logger.error('Failed to send mail:');
                this.logger.error(err);
            });

        return entity;
    }

    async list(request: ContactsCmsListRequest): Promise<ContactsCmsListResponse> {
        const findOptions: FindManyOptions<BazaContentTypesContactsEntity> = {
            order: {
                id: 'DESC',
            },
            relations: BAZA_CONTENT_TYPES_CONTACTS_RELATIONS,
        };

        if (request.queryString) {
            findOptions.where = [
                {
                    email: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                },
                {
                    name: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                },
            ];
        }

        return this.crud.find({
            request,
            findOptions,
            entity: BazaContentTypesContactsEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });
    }

    async markAsProcessed(request: ContactsCmsMarkAsProcessedRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        if (!entity.dateProcessedAt) {
            entity.dateProcessedAt = new Date();

            await this.repository.save(entity);
        }
    }

    async markAsUnprocessed(request: ContactsCmsMarkAsUnprocessedRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        if (entity.dateProcessedAt) {
            entity.dateProcessedAt = null;

            await this.repository.save(entity);
        }
    }

    publishClientResponseEmail(entity: BazaContentTypesContactsEntity): void {
        this.mail
            .send({
                name: BazaContentTypesMail.BazaContentTypesContactClientResponse,
                subject: replaceTags(this.registry.getValue('bazaContentTypes.contacts.clientSubject'), {
                    subject: entity.subject.subject,
                }),
                to: [
                    {
                        email: entity.email,
                        name: entity.name,
                    },
                ],
                variables: () => ({
                    name: entity.name,
                    email: entity.email,
                    subject: entity.subject.subject,
                    rawMessage: entity.message,
                    htmlMessage: _.escape(entity.message).split('\n').join('<br/>'),
                    contactUs: this.registry.getValue('bazaContentTypes.contacts.email'),
                }),
            })
            .then(() => {
                this.logger.log(`[BazaContactUsService] [sendClientEmail] [${entity.email}] Response for contact-us sent.`);
            })
            .catch((error) => {
                this.logger.error('Failed to send client mail:');
                this.logger.error(error);
            });
    }
}
