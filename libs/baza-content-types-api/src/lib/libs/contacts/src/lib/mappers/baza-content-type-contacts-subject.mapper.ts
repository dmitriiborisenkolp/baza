import { Injectable } from '@nestjs/common';
import { BazaContentTypesContactsSubjectEntity } from '../entities/baza-content-type-contacts-subject.entity';
import { ContactsSubjectDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class BazaContentTypesContactsSubjectMapper {
    entityToDTO(entity: BazaContentTypesContactsSubjectEntity): ContactsSubjectDto {
        return {
            id: entity.id,
            subject: entity.subject,
        };
    }

    entitiesToDTOs(entities: Array<BazaContentTypesContactsSubjectEntity>): Array<ContactsSubjectDto> {
        return entities.map((e) => this.entityToDTO(e));
    }
}
