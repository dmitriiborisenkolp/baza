import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypesContactsErrorCodes, bazaContentTypesContactsErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaContentTypesContactsSubjectIsNotEnabledException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesContactsErrorCodes.BazaContentTypesContactsSubjectIsNotEnabled,
            bazaContentTypesContactsErrorCodesI18n[BazaContentTypesContactsErrorCodes.BazaContentTypesContactsSubjectIsNotEnabled],
            HttpStatus.BAD_REQUEST,
        );
    }
}
