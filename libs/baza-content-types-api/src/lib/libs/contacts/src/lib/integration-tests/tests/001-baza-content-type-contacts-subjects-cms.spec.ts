import 'reflect-metadata';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { ContactsNodeAccess, ContactsSubjectCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';

describe('@scaliolabs/baza-content-types-api/contacts/001-baza-content-type-contacts-subjects-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new ContactsNodeAccess(http);
    const dataAccessSubjectCms = new ContactsSubjectCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will contains no subjects in list', async () => {
        const response = await dataAccessSubjectCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will add new subject', async () => {
        const response = await dataAccessSubjectCms.create({
            subject: 'My Subject 1',
            isEnabled: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('wil display added subject in list', async () => {
        const response = await dataAccessSubjectCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].subject).toBe('My Subject 1');
        expect(response.items[0].isEnabled).toBeTruthy();

        ID = response.items[0].id;
    });

    it('will display added subject in public endpoint', async () => {
        http.noAuth();

        const response = await dataAccess.subjects();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);
        expect(response[0].id).toBe(ID);
        expect(response[0].subject).toBe('My Subject 1');
    });

    it('will update subject to inactive', async () => {
        const response = await dataAccessSubjectCms.update({
            id: ID,
            subject: 'My Subject 1*',
            isEnabled: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updated subject in list', async () => {
        const response = await dataAccessSubjectCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].subject).toBe('My Subject 1*');
        expect(response.items[0].isEnabled).toBeFalsy();

        ID = response.items[0].id;
    });

    it('will not display disabled subject anymore for public', async () => {
        http.noAuth();

        const response = await dataAccess.subjects();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(0);
    });

    it('will allow to remove subject', async () => {
        const response = await dataAccessSubjectCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display subject anymore in list', async () => {
        const response = await dataAccessSubjectCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will not display disabled subject anymore for public (#2)', async () => {
        http.noAuth();

        const response = await dataAccess.subjects();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(0);
    });

    it('will correctly set sort order', async () => {
        await dataAccessSubjectCms.create({
            subject: 'My Subject 1',
            isEnabled: true,
        });

        await dataAccessSubjectCms.create({
            subject: 'My Subject 2',
            isEnabled: true,
        });

        const createResponse = await dataAccessSubjectCms.create({
            subject: 'My Subject 3',
            isEnabled: true,
        });

        const moveResponse = await dataAccessSubjectCms.setSortOrder({
            id: createResponse.id,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(moveResponse)).toBeFalsy();

        const listResponse = await dataAccessSubjectCms.list({});

        expect(listResponse.items[0].subject).toBe('My Subject 3');
        expect(listResponse.items[1].subject).toBe('My Subject 1');
        expect(listResponse.items[2].subject).toBe('My Subject 2');
    });

    it('will also display correctly new order for public endpoint', async () => {
        const response = await dataAccess.subjects();

        expect(response.length).toBe(3);
        expect(response[0].subject).toBe('My Subject 3');
        expect(response[1].subject).toBe('My Subject 1');
        expect(response[2].subject).toBe('My Subject 2');
    });
});
