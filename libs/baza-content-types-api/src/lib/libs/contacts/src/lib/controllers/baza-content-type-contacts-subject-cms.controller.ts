import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
    BazaContentTypeAcl,
    ContactsSubjectCmsCreateRequest,
    ContactsSubjectCmsCreateResponse,
    ContactsSubjectCmsDeleteRequest,
    ContactsSubjectCmsDto,
    ContactsSubjectCmsEndpoint,
    ContactsSubjectCmsEndpointPaths,
    ContactsSubjectCmsGetByIdRequest,
    ContactsSubjectCmsListRequest,
    ContactsSubjectCmsListResponse,
    ContactsSubjectCmsResponse,
    ContactsSubjectCmsSetSortOrderRequest,
    ContactsSubjectCmsSetSortOrderResponse,
    ContactsSubjectCmsUpdateRequest,
    ContactsSubjectCmsUpdateResponse,
} from '@scaliolabs/baza-content-types-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaContentTypesContactsSubjectCmsMapper } from '../mappers/baza-content-type-contacts-subject-cms.mapper';
import { BazaContentTypesContactsSubjectService } from '../services/baza-content-type-contacts-subject.service';
import { BazaContentTypesContactsSubjectRepository } from '../repositories/baza-content-type-contacts-subject.repository';
import { BazaContentTypesCmsOpenApi } from '@scaliolabs/baza-content-types-shared';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.ContactsSubjects)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesContacts])
export class BazaContentTypesContactsSubjectCmsController implements ContactsSubjectCmsEndpoint {
    constructor(
        private readonly mapper: BazaContentTypesContactsSubjectCmsMapper,
        private readonly service: BazaContentTypesContactsSubjectService,
        private readonly repository: BazaContentTypesContactsSubjectRepository,
    ) {}

    @Post(ContactsSubjectCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of subjects for Contact Us modal',
    })
    @ApiOkResponse({
        type: ContactsSubjectCmsListResponse,
    })
    async list(@Body() request: ContactsSubjectCmsListRequest): Promise<ContactsSubjectCmsListResponse> {
        return this.service.list(request);
    }

    @Post(ContactsSubjectCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Return subject by ID',
    })
    @ApiOkResponse({
        type: ContactsSubjectCmsDto,
    })
    async getById(@Body() request: ContactsSubjectCmsGetByIdRequest): Promise<ContactsSubjectCmsResponse> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(ContactsSubjectCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
        description: 'Create new subject for Contact Us modal',
    })
    @ApiOkResponse({
        type: ContactsSubjectCmsDto,
    })
    async create(@Body() request: ContactsSubjectCmsCreateRequest): Promise<ContactsSubjectCmsCreateResponse> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(ContactsSubjectCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
        description: 'Update subject for Contact Us modal',
    })
    @ApiOkResponse({
        type: ContactsSubjectCmsDto,
    })
    async update(@Body() request: ContactsSubjectCmsUpdateRequest): Promise<ContactsSubjectCmsUpdateResponse> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(ContactsSubjectCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
        description: 'Delete subject for Contact Us modal',
    })
    @ApiOkResponse()
    async delete(@Body() request: ContactsSubjectCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(ContactsSubjectCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
        description: 'Set sort order of subject in select box',
    })
    @ApiOkResponse({
        type: ContactsSubjectCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: ContactsSubjectCmsSetSortOrderRequest): Promise<ContactsSubjectCmsSetSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
