import { Injectable } from '@angular/core';
import { Connection, Repository } from 'typeorm';
import { BazaContentTypesContactsEntity } from '../entities/baza-content-type-contacts.entity';
import { BazaContentTypesContactsNotFoundException } from '../exceptions/baza-content-type-contacts-not-found.exception';

export const BAZA_CONTENT_TYPES_CONTACTS_RELATIONS = ['subject'];

@Injectable()
export class BazaContentTypesContactsRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaContentTypesContactsEntity> {
        return this.connection.getRepository(BazaContentTypesContactsEntity);
    }

    async save(entity: BazaContentTypesContactsEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async remove(entity: BazaContentTypesContactsEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async getById(id: number): Promise<BazaContentTypesContactsEntity> {
        const entity = await this.repository.findOne({
            where: [{
                id,
            }],
            relations: BAZA_CONTENT_TYPES_CONTACTS_RELATIONS,
        });

        if (! entity) {
            throw new BazaContentTypesContactsNotFoundException();
        }

        return entity;
    }
}
