import { Body, Controller, Get, Post } from '@nestjs/common';
import {
    ContactsEndpoint,
    ContactsEndpointPaths,
    ContactsSendRequest,
    ContactsSubjectDto,
    ContactsSubjectsResponse,
    BazaContentTypesOpenApi,
} from '@scaliolabs/baza-content-types-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaContentTypesContactsSubjectService } from '../services/baza-content-type-contacts-subject.service';
import { BazaContentTypesContactsSubjectMapper } from '../mappers/baza-content-type-contacts-subject.mapper';
import { BazaContentTypesContactsService } from '../services/baza-content-type-contacts.service';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Contacts)
export class BazaContentTypesContactsController implements ContactsEndpoint {
    constructor(
        private readonly service: BazaContentTypesContactsService,
        private readonly subjectService: BazaContentTypesContactsSubjectService,
        private readonly subjectMapper: BazaContentTypesContactsSubjectMapper,
    ) {}

    @Get(ContactsEndpointPaths.subjects)
    @ApiOperation({
        summary: 'subjects',
        description: 'Returns list of available subject for select list',
    })
    @ApiOkResponse({
        type: ContactsSubjectDto,
        isArray: true,
    })
    async subjects(): Promise<ContactsSubjectsResponse> {
        return this.subjectMapper.entitiesToDTOs(await this.subjectService.subjects());
    }

    @Post(ContactsEndpointPaths.send)
    @ApiOperation({
        summary: 'send',
        description: 'Send a message to Contact Us CMS',
    })
    @ApiOkResponse()
    async send(@Body() request: ContactsSendRequest): Promise<void> {
        await this.service.send(request);
    }
}
