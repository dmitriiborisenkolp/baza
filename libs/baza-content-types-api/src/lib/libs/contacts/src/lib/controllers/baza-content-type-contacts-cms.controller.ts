import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
    ContactsCmsDto,
    ContactsCmsEndpoint,
    ContactsCmsEndpointPaths,
    ContactsCmsListRequest,
    ContactsCmsListResponse,
    ContactsCmsMarkAsProcessedRequest,
    ContactsCmsMarkAsUnprocessedRequest,
    ContactsGetByIdRequest,
    ContactsGetByIdResponse,
} from '@scaliolabs/baza-content-types-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaContentTypesContactsCmsMapper } from '../mappers/baza-content-type-contacts-cms.mapper';
import { BazaContentTypesContactsRepository } from '../repositories/baza-content-type-contacts.repository';
import { BazaContentTypesContactsService } from '../services/baza-content-type-contacts.service';
import { BazaContentTypeAcl, BazaContentTypesCmsOpenApi } from '@scaliolabs/baza-content-types-shared';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Contacts)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesContacts])
export class BazaContentTypesContactsCmsController implements ContactsCmsEndpoint {
    constructor(
        private readonly service: BazaContentTypesContactsService,
        private readonly mapper: BazaContentTypesContactsCmsMapper,
        private readonly repository: BazaContentTypesContactsRepository,
    ) {}

    @Post(ContactsCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Contact Us emails',
    })
    @ApiOkResponse({
        type: ContactsCmsListResponse,
    })
    async list(@Body() request: ContactsCmsListRequest): Promise<ContactsCmsListResponse> {
        return this.service.list(request);
    }

    @Post(ContactsCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns Contact Us email by ID',
    })
    @ApiOkResponse({
        type: ContactsCmsDto,
    })
    async getById(@Body() request: ContactsGetByIdRequest): Promise<ContactsGetByIdResponse> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(ContactsCmsEndpointPaths.markAsProcessed)
    @ApiOperation({
        summary: 'markAsProcessed',
        description: 'Mark Contact Us email as processed',
    })
    @ApiOkResponse()
    async markAsProcessed(@Body() request: ContactsCmsMarkAsProcessedRequest): Promise<void> {
        await this.service.markAsProcessed(request);
    }

    @Post(ContactsCmsEndpointPaths.markAsUnprocessed)
    @ApiOperation({
        summary: 'markAsUnprocessed',
        description: 'Mark Contact Us email as unprocessed',
    })
    @ApiOkResponse()
    async markAsUnprocessed(@Body() request: ContactsCmsMarkAsUnprocessedRequest): Promise<void> {
        await this.service.markAsUnprocessed(request);
    }
}
