import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypesContactsErrorCodes, bazaContentTypesContactsErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaContentTypesContactsNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesContactsErrorCodes.BazaContentTypesContactsNotFound,
            bazaContentTypesContactsErrorCodesI18n[BazaContentTypesContactsErrorCodes.BazaContentTypesContactsNotFound],
            HttpStatus.NOT_FOUND
        );
    }
}
