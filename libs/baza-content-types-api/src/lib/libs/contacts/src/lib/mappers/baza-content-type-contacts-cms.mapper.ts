import { Injectable } from '@nestjs/common';
import { BazaContentTypesContactsEntity } from '../entities/baza-content-type-contacts.entity';
import { ContactsCmsDto } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypesContactsSubjectCmsMapper } from './baza-content-type-contacts-subject-cms.mapper';

@Injectable()
export class BazaContentTypesContactsCmsMapper {
    constructor(
        private readonly mapperSubject: BazaContentTypesContactsSubjectCmsMapper,
    ) {}

    entityToDTO(entity: BazaContentTypesContactsEntity): ContactsCmsDto {
        return {
            id: entity.id,
            dateCreatedAt: entity.dateCreatedAt.toISOString(),
            dateProcessedAt: entity.dateProcessedAt
                ? entity.dateProcessedAt.toISOString()
                : undefined,
            subject: entity.subject
                ? this.mapperSubject.entityToDTO(entity.subject)
                : undefined,
            email: entity.email,
            name: entity.name,
            message: entity.message,
            sent: entity.sent,
        };
    }

    entitiesToDTOs(entities: Array<BazaContentTypesContactsEntity>): Array<ContactsCmsDto> {
        return entities.map((e) => this.entityToDTO(e));
    }
}
