import { Injectable } from '@nestjs/common';
import { AwsService } from '@scaliolabs/baza-core-api';
import { EventEntity } from '../entities/event.entity';
import { EventDto } from '@scaliolabs/baza-content-types-shared';
import { CategoryListMapper } from '../../../../categories/src';

@Injectable()
export class EventMapper {
    constructor(
        private readonly aws: AwsService,
        private readonly categoryMapper: CategoryListMapper,
    ) {}

    async entityToDTO(input: EventEntity): Promise<EventDto> {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            url: input.url,
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            datePublishedAt: input.datePublishedAt
                ? input.datePublishedAt.toISOString()
                : undefined,
            primaryCategory: input.primaryCategory
                ? await this.categoryMapper.entityToDTO(input.primaryCategory)
                : undefined,
            additionalCategories: await  this.categoryMapper.entitiesToDTOs(input.additionalCategories || []),
            title: input.title,
            link: input.link,
            location: input.location,
            date: input.date.toISOString(),
            imageUrl: input.imageAwsS3Key
                ? await this.aws.presignedUrl({ s3ObjectId: input.imageAwsS3Key })
                : undefined,
            thumbnailUrl: input.thumbnailAwsS3Key
                ? await this.aws.presignedUrl({ s3ObjectId: input.thumbnailAwsS3Key })
                : undefined,
            contents: input.contents,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<EventEntity>): Promise<Array<EventDto>> {
        const result: Array<EventDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
     }
}
