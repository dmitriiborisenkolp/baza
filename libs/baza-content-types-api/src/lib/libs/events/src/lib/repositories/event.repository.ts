import { Injectable } from "@nestjs/common";
import { Connection, Repository } from 'typeorm';
import { EventEntity } from '../entities/event.entity';
import { BazaContentTypeEventNotFoundException } from '../exceptions/baza-content-type-event-not-found.exception';

export const EVENT_RELATIONS = [
    'primaryCategory',
    'additionalCategories',
];

@Injectable()
export class EventRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<EventEntity> {
        return this.connection.getRepository(EventEntity);
    }

    async save(entities: Array<EventEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<EventEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<EventEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: EVENT_RELATIONS,
        });
    }

    async getById(id: number): Promise<EventEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BazaContentTypeEventNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<EventEntity> {
        const entity = await this.repository.findOne({
            where: [{
                url,
            }],
            relations: EVENT_RELATIONS,
        });

        if (! entity) {
            throw new BazaContentTypeEventNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<EventEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'DESC',
            },
            relations: EVENT_RELATIONS,
        });
    }

    async findAllPublished(): Promise<Array<EventEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'DESC',
            },
            where: [{
                isPublished: true,
            }],
            relations: EVENT_RELATIONS,
        });
    }

    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return result.max || 0;
    }
}
