import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypesEventsErrorCodes, bazaContentTypesEventsErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaContentTypeEventNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesEventsErrorCodes.BazaContentTypesEventNotFound,
            bazaContentTypesEventsErrorCodesI18n[BazaContentTypesEventsErrorCodes.BazaContentTypesEventNotFound],
            HttpStatus.NOT_FOUND
        );
    }
}
