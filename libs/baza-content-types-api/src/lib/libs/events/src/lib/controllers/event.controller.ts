import { Body, Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypesOpenApi,
    EventDto,
    EventsEndpoint,
    EventsEndpointPaths,
    EventsListRequest,
    EventsListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { EventMapper } from '../mappers/event.mapper';
import { EventService } from '../services/event.service';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Events)
export class EventController implements EventsEndpoint {
    constructor(private readonly mapper: EventMapper, private readonly service: EventService) {}

    @Get(EventsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List event',
    })
    @ApiOkResponse({
        type: EventsListResponse,
    })
    async list(@Body() request: EventsListRequest): Promise<EventsListResponse> {
        return this.service.list(request);
    }

    @Get(EventsEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
        description: 'Returns all events',
    })
    @ApiOkResponse({
        type: EventDto,
        isArray: true,
    })
    async getAll(): Promise<Array<EventDto>> {
        const entities = await this.service.getAll();

        return this.mapper.entitiesToDTOs(entities);
    }

    @Get(EventsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns event record by Id',
    })
    @ApiOkResponse({
        type: EventDto,
    })
    async getById(@Param('id') id: number): Promise<EventDto> {
        const entity = await this.service.getById(id);

        return this.mapper.entityToDTO(entity);
    }

    @Get(EventsEndpointPaths.getByUrl)
    @ApiOperation({
        summary: 'getByUrl',
        description: 'Returns event record by URL',
    })
    @ApiOkResponse({
        type: EventDto,
    })
    async getByUrl(@Param('url') url: string): Promise<EventDto> {
        const entity = await this.service.getByUrl(url);

        return this.mapper.entityToDTO(entity);
    }
}
