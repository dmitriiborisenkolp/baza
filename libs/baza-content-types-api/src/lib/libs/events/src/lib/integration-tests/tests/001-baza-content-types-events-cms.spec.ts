import 'reflect-metadata';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { EventsCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';

describe('@scaliolabs/baza-content-types/events/001-baza-content-types-events-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new EventsCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new events record', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            date: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'Events 1',
            link: 'https://scal.io/events/events-1',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'events-1',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new events record in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].title).toBe('Events 1');
        expect(listResponse.items[0].sortOrder).toBe(1);
    });

    it('will successfully create new events record without link', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            date: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'Events 2',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'events-2',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ID = response.id;
    });

    it('will successfully returns events record  by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Events 2');
    });

    it('will successfully update events record', async () => {
        const response = await dataAccessCms.update({
            id: ID,
            isPublished: true,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            date: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'Events 2 *',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'events-2',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updates in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).toBe(ID);
        expect(listResponse.items[0].title).toBe('Events 2 *');
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[1].id).not.toBe(ID);
        expect(listResponse.items[1].sortOrder).toBe(1);
        expect(listResponse.items[1].title).toBe('Events 1');

        ID = listResponse.items[0].id;
    });

    it('will successfully update sort order of events record', async () => {
        const response = await dataAccessCms.setSortOrder({
            id: ID,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updated sort order in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[0].title).toBe('Events 1');
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].title).toBe('Events 2 *');
        expect(listResponse.items[1].sortOrder).toBe(1);

        ID = listResponse.items[1].id;
    });

    it('will successfully delete events record', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted team member in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].title).toBe('Events 1');

        ID = listResponse.items[0].id;
    });
});
