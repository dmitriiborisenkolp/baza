import { Injectable } from '@nestjs/common';
import { emptyBazaContentDto, EventsCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { EventCmsService } from '../../services/event-cms.service';
import { BazaContentTypesEventsFixtures } from '../baza-content-types-events.fixtures';

export const E2E_BAZA_CONTENT_TYPES_EVENTS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    request: EventsCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            isPublished: true,
            datePublishedAt: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            date: 'Tue Aug 24 2021 15:11:57 GMT+0300',
            title: 'Events 1',
            link: 'https://scal.io/events/events-1',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'events-1',
            },
        },
    },
    {
        $refId: 2,
        request: {
            isPublished: false,
            datePublishedAt: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            date: 'Tue Aug 23 2021 15:11:57 GMT+0300',
            title: 'Events 2',
            link: 'https://scal.io/events/events-2',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'events-2',
            },
        },
    },
    {
        $refId: 3,
        request: {
            isPublished: true,
            datePublishedAt: 'Tue Aug 20 2021 15:11:57 GMT+0300',
            date: 'Tue Aug 20 2021 15:11:57 GMT+0300',
            title: 'Events 3',
            link: 'https://scal.io/events/events-3',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'events-3',
            },
        },
    },
];

@Injectable()
export class BazaContentTypeEventsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: EventCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_EVENTS_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.request);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesEventsFixtures>([{
    fixture: BazaContentTypesEventsFixtures.BazaContentTypesEvents,
    provider: BazaContentTypeEventsFixture,
}]);
