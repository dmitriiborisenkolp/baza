import { Injectable } from '@nestjs/common';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { EVENT_RELATIONS, EventRepository } from '../repositories/event.repository';
import { EventCmsMapper } from '../mappers/event-cms.mapper';
import { EventEntity } from '../entities/event.entity';
import { CategoryRepository } from '../../../../categories/src';
import { EventCmsDto, EventsCmsCreateRequest, EventsCmsDeleteRequest, EventsCmsEntityBody, EventsCmsListRequest, EventsCmsListResponse, EventsCmsSetSortOrderRequest, EventsCmsSetSortOrderResponse, EventsCmsUpdateRequest } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class EventCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: EventRepository,
        private readonly categoryRepository: CategoryRepository,
        private readonly mapper: EventCmsMapper,
    ) {}

    async create(request: EventsCmsCreateRequest): Promise<EventEntity> {
        const entity = new EventEntity();

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: EventsCmsUpdateRequest): Promise<EventEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: EventsCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);

        const entities = await this.repository.findAll();

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async list(request: EventsCmsListRequest): Promise<EventsCmsListResponse> {
        return this.crud.find<EventEntity, EventCmsDto>({
            entity: EventEntity,
            request,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
                relations: EVENT_RELATIONS,
            },
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: EventsCmsSetSortOrderRequest): Promise<EventsCmsSetSortOrderResponse> {
        const entities = await this.repository.findAll();

        const response = await this.crudSort.setSortOrder<EventEntity, EventCmsDto>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: entities,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });

        await this.repository.save(entities);

        return response;
    }

    async populate(target: EventEntity, entityBody: EventsCmsEntityBody): Promise<void> {
        if (! target.id) {
            target.sortOrder = await this.repository.maxSortOrder() + 1;
        }

        if (! target.dateCreatedAt) {
            target.dateCreatedAt = new Date();
        }

        target.title = entityBody.title;
        target.isPublished = entityBody.isPublished;
        target.url = entityBody.seo && entityBody.seo.urn
            ? entityBody.seo.urn
            : undefined;
        target.datePublishedAt = entityBody.datePublishedAt
            ? new Date(entityBody.datePublishedAt)
            : null;
        target.primaryCategory = entityBody.primaryCategoryId
            ? await this.categoryRepository.getById(entityBody.primaryCategoryId)
            : null;
        target.additionalCategories = entityBody.additionalCategoryIds
            ? await this.categoryRepository.findByIds(entityBody.additionalCategoryIds)
            : [];
        target.title = entityBody.title;
        target.link = entityBody.link;
        target.location = entityBody.location;
        target.date = new Date(entityBody.date);
        target.imageAwsS3Key = entityBody.imageAwsS3Key;
        target.thumbnailAwsS3Key = entityBody.thumbnailAwsS3Key;
        target.contents = entityBody.contents;
        target.seo = entityBody.seo;
    }
}
