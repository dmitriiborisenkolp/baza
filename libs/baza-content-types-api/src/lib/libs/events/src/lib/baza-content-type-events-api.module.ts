import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { EventController } from './controllers/event.controller';
import { EventCmsController } from './controllers/event-cms.controller';
import { EventRepository } from './repositories/event.repository';
import { EventMapper } from './mappers/event.mapper';
import { EventCmsMapper } from './mappers/event-cms.mapper';
import { EventService } from './services/event.service';
import { EventCmsService } from './services/event-cms.service';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        EventController,
        EventCmsController,
    ],
    providers: [
        EventRepository,
        EventMapper,
        EventCmsMapper,
        EventService,
        EventCmsService,
    ],
    exports: [
        EventRepository,
        EventMapper,
        EventCmsMapper,
        EventService,
        EventCmsService,
    ],
})
export class BazaContentTypeEventsApiModule {}
