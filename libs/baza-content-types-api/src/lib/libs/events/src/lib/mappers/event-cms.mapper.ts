import { Injectable } from "@nestjs/common";
import { EventMapper } from './event.mapper';
import { EventEntity } from '../entities/event.entity';
import { EventCmsDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class EventCmsMapper {
    constructor(
        private readonly baseMapper: EventMapper,
    ) {}

    async entityToDTO(input: EventEntity): Promise<EventCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            isPublished: input.isPublished,
            imageAwsS3Key: input.imageAwsS3Key,
            thumbnailAwsS3Key: input.thumbnailAwsS3Key,
        };
    }

    async entitiesToDTOs(input: Array<EventEntity>): Promise<Array<EventCmsDto>> {
        const result: Array<EventCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
