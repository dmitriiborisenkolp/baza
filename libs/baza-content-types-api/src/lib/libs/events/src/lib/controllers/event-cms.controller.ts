import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    EventCmsDto,
    EventsCmsCreateRequest,
    EventsCmsDeleteRequest,
    EventsCmsEndpoint,
    EventsCmsEndpointPaths,
    EventsCmsGetByIdRequest,
    EventsCmsListRequest,
    EventsCmsListResponse,
    EventsCmsSetSortOrderRequest,
    EventsCmsSetSortOrderResponse,
    EventsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { EventCmsMapper } from '../mappers/event-cms.mapper';
import { EventRepository } from '../repositories/event.repository';
import { EventCmsService } from '../services/event-cms.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Events)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesEvents])
export class EventCmsController implements EventsCmsEndpoint {
    constructor(
        private readonly mapper: EventCmsMapper,
        private readonly repository: EventRepository,
        private readonly service: EventCmsService,
    ) {}

    @Post(EventsCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: EventCmsDto,
    })
    async create(@Body() request: EventsCmsCreateRequest): Promise<EventCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(EventsCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: EventCmsDto,
    })
    async update(@Body() request: EventsCmsUpdateRequest): Promise<EventCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(EventsCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: EventsCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(EventsCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: EventsCmsListResponse,
    })
    async list(@Body() request: EventsCmsListRequest): Promise<EventsCmsListResponse> {
        return this.service.list(request);
    }

    @Post(EventsCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: EventCmsDto,
    })
    async getById(@Body() request: EventsCmsGetByIdRequest): Promise<EventCmsDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(EventsCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: EventsCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: EventsCmsSetSortOrderRequest): Promise<EventsCmsSetSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
