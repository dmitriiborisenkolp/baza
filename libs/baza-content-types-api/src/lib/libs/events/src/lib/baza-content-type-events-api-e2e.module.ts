import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypeEventsApiModule } from './baza-content-type-events-api.module';
import { BazaContentTypeEventsFixture } from './integration-tests/fixtures/baza-content-type-events.fixture';

const E2E_FIXTURES = [
    BazaContentTypeEventsFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypeEventsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypeEventsApiE2eModule {}
