import { Injectable } from '@nestjs/common';
import { EVENT_RELATIONS, EventRepository } from '../repositories/event.repository';
import { EventMapper } from '../mappers/event.mapper';
import { CrudService } from '@scaliolabs/baza-core-api';
import { EventDto, EventsListRequest, EventsListResponse } from '@scaliolabs/baza-content-types-shared';
import { EventEntity } from '../entities/event.entity';
import { BazaContentTypeEventNotFoundException } from '../exceptions/baza-content-type-event-not-found.exception';

@Injectable()
export class EventService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: EventMapper,
        private readonly repository: EventRepository,
    ) {}

    async list(request: EventsListRequest): Promise<EventsListResponse> {
        return this.crud.find<EventEntity, EventDto>({
            request,
            entity: EventEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
                relations: EVENT_RELATIONS,
                where: [{
                    isPublished: true,
                }],
            },
            withMaxSortOrder: true,
        });
    }

    async getAll(): Promise<Array<EventEntity>> {
        return this.repository.findAllPublished();
    }

    async getById(id: number): Promise<EventEntity> {
        const entity = await this.repository.getById(id);

        if (! entity.isPublished) {
            throw new BazaContentTypeEventNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<EventEntity> {
        const entity = await this.repository.getByUrl(url);

        if (! entity.isPublished) {
            throw new BazaContentTypeEventNotFoundException();
        }

        return entity;
    }
}
