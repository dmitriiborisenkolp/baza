import { Module } from "@nestjs/common";
import { CategoryCmsController } from './controllers/category-cms.controller';
import { CategoryMapper } from './mappers/category.mapper';
import { CategoryRepository } from './repositories/category.repository';
import { CategoryService } from './services/category.service';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { CategoryListMapper } from './mappers/category-list.mapper';
import { CategoryController } from './controllers/category.controller';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        CategoryController,
        CategoryCmsController,
    ],
    providers: [
        CategoryMapper,
        CategoryListMapper,
        CategoryRepository,
        CategoryService,
    ],
    exports: [
        CategoryMapper,
        CategoryListMapper,
        CategoryRepository,
        CategoryService,
    ],
})
export class BazaContentTypeCategoriesApiModule {
}
