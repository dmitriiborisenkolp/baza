import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypeCategoriesApiModule } from './baza-content-type-categories-api.module';
import { BazaContentTypesCategoriesFixture } from './integration-tests/fixtures/baza-content-types-categories.fixture';

const E2E_FIXTURES = [
    BazaContentTypesCategoriesFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypeCategoriesApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypeCategoriesApiE2eModule {}
