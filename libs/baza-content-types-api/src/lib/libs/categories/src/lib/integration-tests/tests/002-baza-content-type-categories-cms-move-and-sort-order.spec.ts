import 'reflect-metadata';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { CategoriesCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { CategoriesDto, CategoryListDto, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaContentTypesCategoriesFixtures } from '../baza-content-types-categories.fixtures';

describe('@scaliolabs/baza-content-types-api/categories/002-baza-content-type-categories-cms-move-and-sort-order.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new CategoriesCmsNodeAccess(http);

    let CATEGORIES_MAP: { [title: string]: CategoryListDto } = {};

    const updateCategoriesMap = async () => {
        CATEGORIES_MAP = {};

        await http.authE2eAdmin();

        const getAllResponse = await dataAccessCms.getAll();

        const deep = (input: CategoriesDto) => {
            if (input.category) {
                CATEGORIES_MAP[input.category.title] = input.category;
            }

            if (Array.isArray(input.children) && input.children.length > 0) {
                for (const dto of input.children) {
                    deep(dto);
                }
            }
        };

        deep(getAllResponse);
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesCategoriesFixtures.BazaContentTypeCategories],
            specFile: __filename,
        });

        await updateCategoriesMap();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
        await updateCategoriesMap();
    });

    it('will have proper sort orders of Root Category 2', async () => {
        expect(CATEGORIES_MAP['Child Category 2.1'].sortOrder).toBe(1);
        expect(CATEGORIES_MAP['Child Category 2.2'].sortOrder).toBe(2);
        expect(CATEGORIES_MAP['Child Category 2.3'].sortOrder).toBe(3);
    });

    it('will successfully move Child Category 2.2 to Root Category 3', async () => {
        const updateResponse = await dataAccessCms.update({
            ...CATEGORIES_MAP['Child Category 2.2'],
            parentId: CATEGORIES_MAP['Root Category 3'].id,
            seo: bazaSeoDefault(),
            contents: emptyBazaContentDto(),
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();

        expect(updateResponse.parentId).toBe(CATEGORIES_MAP['Root Category 3'].id);
    });

    it('will properly update sort orders of Root Category 2', async () => {
        expect(CATEGORIES_MAP['Child Category 2.1'].sortOrder).toBe(1);
        expect(CATEGORIES_MAP['Child Category 2.3'].sortOrder).toBe(2);
    });

    it('will properly update sort orders of Root Category 3', async () => {
        expect(CATEGORIES_MAP['Child Category 2.2'].sortOrder).toBe(1);
    });

    it('will successfully move Child Category 2.3 to Root', async () => {
        const response = await dataAccessCms.update({
            ...CATEGORIES_MAP['Child Category 2.3'],
            parentId: undefined,
            seo: bazaSeoDefault(),
            contents: emptyBazaContentDto(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.parentId).not.toBeDefined();
    });

    it('will properly update sort orders of root categories', async () => {
        expect(CATEGORIES_MAP['Root Category 1'].sortOrder).toBe(1);
        expect(CATEGORIES_MAP['Root Category 2'].sortOrder).toBe(2);
        expect(CATEGORIES_MAP['Root Category 3'].sortOrder).toBe(3);
        expect(CATEGORIES_MAP['Child Category 2.3'].sortOrder).toBe(4);
    });

    it('will successfully move Root Category 1 to Child Category 2.1', async () => {
        const response = await dataAccessCms.update({
            ...CATEGORIES_MAP['Root Category 1'],
            parentId: CATEGORIES_MAP['Child Category 2.1'].id,
            seo: bazaSeoDefault(),
            contents: emptyBazaContentDto(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.parentId).toBe(CATEGORIES_MAP['Child Category 2.1'].id);
    });

    it('will properly update sort orders of root categories', async () => {
        expect(CATEGORIES_MAP['Root Category 2'].sortOrder).toBe(1);
        expect(CATEGORIES_MAP['Root Category 3'].sortOrder).toBe(2);
        expect(CATEGORIES_MAP['Child Category 2.3'].sortOrder).toBe(3);
    });

    it('will properly put Root Category 1 to the end of the Child Category 2.1 list', async () => {
        expect(CATEGORIES_MAP['Child Category 2.1.1'].sortOrder).toBe(1);
        expect(CATEGORIES_MAP['Child Category 2.1.2'].sortOrder).toBe(2);
        expect(CATEGORIES_MAP['Root Category 1'].sortOrder).toBe(3);
    });
});
