import 'reflect-metadata';
import { CategoriesNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { CategoriesDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesCategoriesFixtures } from '../baza-content-types-categories.fixtures';

describe('@scaliolabs/baza-content-types-api/categories/002-baza-content-type-categories-cms-move-and-sort-order.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new CategoriesNodeAccess(http);

    let id: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesCategoriesFixtures.BazaContentTypeCategories],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns all categories', async () => {
        const response = await dataAccess.getAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.category).not.toBeDefined();
        expect(response.children.length).toBe(3);
    });

    it('will not display inactive categories', async () => {
        const response = await dataAccess.getAll();

        const deep = (input: CategoriesDto) => {
            if (input.category) {
                expect(input.category.isActive).toBeTruthy();
            }

            if (Array.isArray(input.children) && input.children.length > 0) {
                for (const child of input.children) {
                    deep(child);
                }
            }
        };

        deep(response);
    });

    it('will return category by url', async () => {
        const response = await dataAccess.get('root-category-2');

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.title).toBe('Root Category 2');

        id = response.id;
    });

    it('will return Root Category 2 by id', async () => {
        const response = await dataAccess.get(id);

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.title).toBe('Root Category 2');
    });
});
