import 'reflect-metadata';
import { CategoriesCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaContentBlock, BazaContentBlockPairHtml, CategoriesCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-content-types-api/categories/001-baza-content-type-categories-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new CategoriesCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    const ROOT_CATEGORIES: Array<{
        $id?: number;
        $refId: number;
        request: CategoriesCmsCreateRequest;
    }> = [
        {
            $refId: 1,
            request: {
                title: 'Root Category 1',
                isActive: true,
                contents: {
                    blocks: [
                        {
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Root Category 1</p>',
                            },
                        },
                    ],
                },
                seo: {
                    title: 'Root Category 1 SEO Title',
                    meta: [],
                },
            },
        },
        {
            $refId: 2,
            request: {
                title: 'Root Category 2',
                isActive: true,
                contents: {
                    blocks: [
                        {
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Root Category 2</p>',
                            },
                        },
                    ],
                },
                seo: {
                    title: 'Root Category 2 SEO Title',
                    meta: [],
                },
            },
        },
        {
            $refId: 3,
            request: {
                title: 'Root Category 3',
                isActive: true,
                contents: {
                    blocks: [
                        {
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Root Category 3</p>',
                            },
                        },
                    ],
                },
                seo: {
                    title: 'Root Category 3 SEO Title',
                    meta: [],
                },
            },
        },
    ];

    const CHILD_R2_CATEGORIES: Array<{
        $id?: number;
        $refId: number;
        request: CategoriesCmsCreateRequest;
    }> = [
        {
            $refId: 1,
            request: {
                title: 'Child Category 2.1',
                isActive: true,
                contents: {
                    blocks: [
                        {
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Child Category 2.1</p>',
                            },
                        },
                    ],
                },
                seo: {
                    title: 'Child Category 2.1 SEO Title',
                    meta: [],
                },
            },
        },
        {
            $refId: 2,
            request: {
                title: 'Child Category 2.2',
                isActive: true,
                contents: {
                    blocks: [
                        {
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Child Category 2.2</p>',
                            },
                        },
                    ],
                },
                seo: {
                    title: 'Child Category 2.2 SEO Title',
                    meta: [],
                },
            },
        },
        {
            $refId: 3,
            request: {
                title: 'Child Category 2.3',
                isActive: true,
                contents: {
                    blocks: [
                        {
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Child Category 2.3</p>',
                            },
                        },
                    ],
                },
                seo: {
                    title: 'Child Category 2.3 SEO Title',
                    meta: [],
                },
            },
        },
    ];

    it('will successfully create new root categories', async () => {
        for (const fixtureRequest of ROOT_CATEGORIES) {
            const response = await dataAccessCms.create(fixtureRequest.request);

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect(response.title).toBe(`Root Category ${fixtureRequest.$refId}`);
            expect(response.parentId).not.toBeDefined();
            expect((response.contents.blocks[0] as BazaContentBlockPairHtml).payload.contents).toBe(
                `<p>Root Category ${fixtureRequest.$refId}</p>`,
            );
            expect(response.seo.title).toBe(`Root Category ${fixtureRequest.$refId} SEO Title`);

            fixtureRequest.$id = response.id;
        }
    });

    it('will successfully return root category by id', async () => {
        const response = await dataAccessCms.getById(ROOT_CATEGORIES[0].$id);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ROOT_CATEGORIES[0].$id);
        expect(response.parentId).not.toBeDefined();
        expect(response.title).toBe('Root Category 1');
    });

    it('will display 3 categories only on root level', async () => {
        const getAllResponse = await dataAccessCms.getAll();

        expect(isBazaErrorResponse(getAllResponse)).toBeFalsy();
        expect(getAllResponse.level).toBe(0);
        expect(getAllResponse.category).not.toBeDefined();
        expect(getAllResponse.children.length).toBe(3);
        expect(getAllResponse.children[0].category).toBeDefined();
        expect(getAllResponse.children[0].category.title).toBe('Root Category 1');
        expect(getAllResponse.children[0].category.sortOrder).toBe(1);
        expect(getAllResponse.children[1].category).toBeDefined();
        expect(getAllResponse.children[1].category.title).toBe('Root Category 2');
        expect(getAllResponse.children[1].category.sortOrder).toBe(2);
        expect(getAllResponse.children[2].category).toBeDefined();
        expect(getAllResponse.children[2].category.title).toBe('Root Category 3');
        expect(getAllResponse.children[2].category.sortOrder).toBe(3);
    });

    it('will successfully update root category', async () => {
        const response = await dataAccessCms.update({
            id: ROOT_CATEGORIES[0].$id,
            ...ROOT_CATEGORIES[0].request,
            title: 'Root Category 1 *',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Root Category 1 *');
    });

    it('will still display 3 categories only on root level', async () => {
        const getAllResponse = await dataAccessCms.getAll();

        expect(isBazaErrorResponse(getAllResponse)).toBeFalsy();
        expect(getAllResponse.level).toBe(0);
        expect(getAllResponse.category).not.toBeDefined();
        expect(getAllResponse.children.length).toBe(3);
        expect(getAllResponse.children[0].category).toBeDefined();
        expect(getAllResponse.children[0].category.title).toBe('Root Category 1 *');
        expect(getAllResponse.children[0].category.sortOrder).toBe(1);
        expect(getAllResponse.children[1].category).toBeDefined();
        expect(getAllResponse.children[1].category.title).toBe('Root Category 2');
        expect(getAllResponse.children[1].category.sortOrder).toBe(2);
        expect(getAllResponse.children[2].category).toBeDefined();
        expect(getAllResponse.children[2].category.title).toBe('Root Category 3');
        expect(getAllResponse.children[2].category.sortOrder).toBe(3);
    });

    it('will successfully add child category', async () => {
        const parentId = ROOT_CATEGORIES[1].$id;

        for (const fixtureRequest of CHILD_R2_CATEGORIES) {
            const response = await dataAccessCms.create({
                ...fixtureRequest.request,
                parentId,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect(response.title).toBe(`Child Category 2.${fixtureRequest.$refId}`);
            expect(response.parentId).toBe(parentId);
            expect((response.contents.blocks[0] as BazaContentBlockPairHtml).payload.contents).toBe(
                `<p>Child Category 2.${fixtureRequest.$refId}</p>`,
            );
            expect(response.seo.title).toBe(`Child Category 2.${fixtureRequest.$refId} SEO Title`);

            const getAllResponse = await dataAccessCms.getAll();

            expect(isBazaErrorResponse(getAllResponse)).toBeFalsy();
            expect(getAllResponse.level).toBe(0);
            expect(getAllResponse.category).not.toBeDefined();
            expect(getAllResponse.children.length).toBe(3);
            expect(getAllResponse.children[0].category).toBeDefined();
            expect(getAllResponse.children[0].category.title).toBe('Root Category 1 *');
            expect(getAllResponse.children[0].category.sortOrder).toBe(1);
            expect(getAllResponse.children[0].children.length).toBe(0);
            expect(getAllResponse.children[1].category).toBeDefined();
            expect(getAllResponse.children[1].category.title).toBe('Root Category 2');
            expect(getAllResponse.children[1].category.sortOrder).toBe(2);
            expect(getAllResponse.children[1].children.length).toBe(fixtureRequest.$refId);
            expect(getAllResponse.children[1].children[fixtureRequest.$refId - 1].category).toBeDefined();
            expect(getAllResponse.children[1].children[fixtureRequest.$refId - 1].category.sortOrder).toBe(fixtureRequest.$refId);
            expect(getAllResponse.children[2].category).toBeDefined();
            expect(getAllResponse.children[2].category.title).toBe('Root Category 3');
            expect(getAllResponse.children[2].category.sortOrder).toBe(3);
            expect(getAllResponse.children[2].children.length).toBe(0);

            fixtureRequest.$id = response.id;
        }
    });

    it('will successfully return child category by id', async () => {
        const response = await dataAccessCms.getById(CHILD_R2_CATEGORIES[0].$id);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(CHILD_R2_CATEGORIES[0].$id);
        expect(response.parentId).toBe(ROOT_CATEGORIES[1].$refId);
        expect(response.title).toBe('Child Category 2.1');
    });

    it('will successfully update sort order of children category', async () => {
        const response = await dataAccessCms.setSortOrder({
            id: CHILD_R2_CATEGORIES[2].$id,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const getAllResponse = await dataAccessCms.getAll();

        expect(isBazaErrorResponse(getAllResponse)).toBeFalsy();
        expect(getAllResponse.level).toBe(0);
        expect(getAllResponse.category).not.toBeDefined();
        expect(getAllResponse.children.length).toBe(3);
        expect(getAllResponse.children[0].category).toBeDefined();
        expect(getAllResponse.children[0].category.title).toBe('Root Category 1 *');
        expect(getAllResponse.children[0].category.sortOrder).toBe(1);
        expect(getAllResponse.children[0].children.length).toBe(0);
        expect(getAllResponse.children[1].category).toBeDefined();
        expect(getAllResponse.children[1].category.title).toBe('Root Category 2');
        expect(getAllResponse.children[1].category.sortOrder).toBe(2);
        expect(getAllResponse.children[2].category).toBeDefined();
        expect(getAllResponse.children[2].category.title).toBe('Root Category 3');
        expect(getAllResponse.children[2].category.sortOrder).toBe(3);
        expect(getAllResponse.children[2].children.length).toBe(0);

        expect(getAllResponse.children[1].children.length).toBe(3);
        expect(getAllResponse.children[1].children[0].category).toBeDefined();
        expect(getAllResponse.children[1].children[0].category.title).toBe('Child Category 2.3');
        expect(getAllResponse.children[1].children[1].category).toBeDefined();
        expect(getAllResponse.children[1].children[1].category.title).toBe('Child Category 2.1');
        expect(getAllResponse.children[1].children[2].category).toBeDefined();
        expect(getAllResponse.children[1].children[2].category.title).toBe('Child Category 2.2');
    });

    it('will successfully remove child category and properly updates sort orders', async () => {
        const response = await dataAccessCms.delete({
            id: CHILD_R2_CATEGORIES[0].$id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const getAllResponse = await dataAccessCms.getAll();

        expect(isBazaErrorResponse(getAllResponse)).toBeFalsy();
        expect(getAllResponse.level).toBe(0);
        expect(getAllResponse.category).not.toBeDefined();
        expect(getAllResponse.children.length).toBe(3);
        expect(getAllResponse.children[0].category).toBeDefined();
        expect(getAllResponse.children[0].category.title).toBe('Root Category 1 *');
        expect(getAllResponse.children[0].category.sortOrder).toBe(1);
        expect(getAllResponse.children[0].children.length).toBe(0);
        expect(getAllResponse.children[1].category).toBeDefined();
        expect(getAllResponse.children[1].category.title).toBe('Root Category 2');
        expect(getAllResponse.children[1].category.sortOrder).toBe(2);
        expect(getAllResponse.children[2].category).toBeDefined();
        expect(getAllResponse.children[2].category.title).toBe('Root Category 3');
        expect(getAllResponse.children[2].category.sortOrder).toBe(3);
        expect(getAllResponse.children[2].children.length).toBe(0);

        expect(getAllResponse.children[1].children.length).toBe(2);
        expect(getAllResponse.children[1].children[0].category).toBeDefined();
        expect(getAllResponse.children[1].children[0].category.title).toBe('Child Category 2.3');
        expect(getAllResponse.children[1].children[0].category.sortOrder).toBe(1);
        expect(getAllResponse.children[1].children[1].category).toBeDefined();
        expect(getAllResponse.children[1].children[1].category.title).toBe('Child Category 2.2');
        expect(getAllResponse.children[1].children[1].category.sortOrder).toBe(2);
    });

    it('will successfully delete root category with children', async () => {
        const response = await dataAccessCms.delete({
            id: ROOT_CATEGORIES[1].$id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const getAllResponse = await dataAccessCms.getAll();

        expect(isBazaErrorResponse(getAllResponse)).toBeFalsy();
        expect(getAllResponse.level).toBe(0);
        expect(getAllResponse.category).not.toBeDefined();
        expect(getAllResponse.children.length).toBe(2);
        expect(getAllResponse.children[0].category).toBeDefined();
        expect(getAllResponse.children[0].category.title).toBe('Root Category 1 *');
        expect(getAllResponse.children[0].category.sortOrder).toBe(1);
        expect(getAllResponse.children[0].children.length).toBe(0);
        expect(getAllResponse.children[1].category).toBeDefined();
        expect(getAllResponse.children[1].category.title).toBe('Root Category 3');
        expect(getAllResponse.children[1].category.sortOrder).toBe(2);
        expect(getAllResponse.children[1].children.length).toBe(0);
    });
});
