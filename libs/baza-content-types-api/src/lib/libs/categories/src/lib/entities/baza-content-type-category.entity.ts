import { BazaContentDto, CategoryScope, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId } from 'typeorm';
import { bazaSeoDefault, BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';

@Entity()
export class BazaContentTypeCategoryEntity implements CrudSortableEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        default: 0,
    })
    sortOrder: number;

    @Column({
        type: 'int',
        default: CategoryScope.Public,
    })
    scope: CategoryScope;

    @Column({
        default: false,
    })
    isActive: boolean;

    @ManyToOne(() => BazaContentTypeCategoryEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    parent?: BazaContentTypeCategoryEntity;

    @RelationId((e: BazaContentTypeCategoryEntity) => e.parent)
    parentId?: number;

    @Column({
        nullable: true,
    })
    url?: string;

    @Column()
    title: string;

    @Column({
        type: 'jsonb',
        default: emptyBazaContentDto(),
    })
    contents: BazaContentDto = emptyBazaContentDto();

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto = bazaSeoDefault();
}
