import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypeCategoriesErrorCodes, bazaContentTypeCategoriesErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class CategoryIsInternalException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypeCategoriesErrorCodes.BazaContentTypeCategoryIsInternal,
            bazaContentTypeCategoriesErrorCodesI18n[BazaContentTypeCategoriesErrorCodes.BazaContentTypeCategoryIsInternal],
            HttpStatus.BAD_REQUEST,
        );
    }
}
