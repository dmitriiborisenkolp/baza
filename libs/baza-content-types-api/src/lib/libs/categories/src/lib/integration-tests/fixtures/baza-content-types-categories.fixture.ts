import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaContentBlock, CategoriesCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { Injectable } from '@nestjs/common';
import { CategoryService } from '../../services/category.service';
import { BazaContentTypesCategoriesFixtures } from '../baza-content-types-categories.fixtures';

interface FixtureRequest {
    $id?: number;
    $refId: number;
    category: CategoriesCmsCreateRequest;
    children?: Array<FixtureRequest>;
}

export const BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE: Array<FixtureRequest> = [
    {
        $refId: 1,
        category: {
            title: 'Root Category 1',
            isActive: true,
            contents: {
                blocks: [{
                    type: BazaContentBlock.HTML,
                    payload: {
                        contents: '<p>Root Category 1</p>',
                    },
                }],
            },
            seo: {
                title: 'Root Category 1 SEO Title',
                urn: 'root-category-1',
                meta: [],
            },
        },
    },
    {
        $refId: 2,
        category: {
            title: 'Root Category 2',
            isActive: true,
            contents: {
                blocks: [{
                    type: BazaContentBlock.HTML,
                    payload: {
                        contents: '<p>Root Category 2</p>',
                    },
                }],
            },
            seo: {
                title: 'Root Category 2 SEO Title',
                urn: 'root-category-2',
                meta: [],
            },
        },
        children: [
            {
                $refId: 1,
                category: {
                    title: 'Child Category 2.1',
                    isActive: true,
                    contents: {
                        blocks: [{
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Child Category 2.1</p>',
                            },
                        }],
                    },
                    seo: {
                        title: 'Child Category 2.1 SEO Title',
                        meta: [],
                    },
                },
                children: [
                    {
                        $refId: 1,
                        category: {
                            title: 'Child Category 2.1.1',
                            isActive: true,
                            contents: {
                                blocks: [{
                                    type: BazaContentBlock.HTML,
                                    payload: {
                                        contents: '<p>Child Category 2.1.1</p>',
                                    },
                                }],
                            },
                            seo: {
                                title: 'Child Category 2.1.1 SEO Title',
                                meta: [],
                                urn: 'child-category-2-1-1',
                            },
                        },
                    },
                    {
                        $refId: 2,
                        category: {
                            title: 'Child Category 2.1.2',
                            isActive: false,
                            contents: {
                                blocks: [{
                                    type: BazaContentBlock.HTML,
                                    payload: {
                                        contents: '<p>Child Category 2.1.2</p>',
                                    },
                                }],
                            },
                            seo: {
                                title: 'Child Category 2.1.2 SEO Title',
                                meta: [],
                                urn: 'child-category-2-1-2',
                            },
                        },
                        children: [
                            {
                                $refId: 1,
                                category: {
                                    title: 'Child Category 2.1.2.1 (INACTIVE)',
                                    isActive: false,
                                    contents: {
                                        blocks: [{
                                            type: BazaContentBlock.HTML,
                                            payload: {
                                                contents: '<p>Child Category 2.1.2.1</p>',
                                            },
                                        }],
                                    },
                                    seo: {
                                        title: 'Child Category 2.1.2.1 SEO Title',
                                        meta: [],
                                        urn: 'child-category-2-1-2.1',
                                    },
                                },
                            },
                            {
                                $refId: 1,
                                category: {
                                    title: 'Child Category 2.1.2.2 (INACTIVE)',
                                    isActive: true,
                                    contents: {
                                        blocks: [{
                                            type: BazaContentBlock.HTML,
                                            payload: {
                                                contents: '<p>Child Category 2.1.2.1</p>',
                                            },
                                        }],
                                    },
                                    seo: {
                                        title: 'Child Category 2.1.2.2 SEO Title',
                                        meta: [],
                                        urn: 'child-category-2-1-2.2',
                                    },
                                },
                            },
                        ],
                    },
                ],
            },
            {
                $refId: 2,
                category: {
                    title: 'Child Category 2.2',
                    isActive: true,
                    contents: {
                        blocks: [{
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Child Category 2.2</p>',
                            },
                        }],
                    },
                    seo: {
                        title: 'Child Category 2.2 SEO Title',
                        meta: [],
                        urn: 'child-category-2-2',
                    },
                },
            },
            {
                $refId: 3,
                category: {
                    title: 'Child Category 2.3',
                    isActive: true,
                    contents: {
                        blocks: [{
                            type: BazaContentBlock.HTML,
                            payload: {
                                contents: '<p>Child Category 2.3</p>',
                            },
                        }],
                    },
                    seo: {
                        title: 'Child Category 2.3 SEO Title',
                        meta: [],
                        urn: 'child-category-2-3',
                    },
                },
            },
        ],
    },
    {
        $refId: 3,
        category: {
            title: 'Root Category 3',
            isActive: true,
            contents: {
                blocks: [{
                    type: BazaContentBlock.HTML,
                    payload: {
                        contents: '<p>Root Category 3</p>',
                    },
                }],
            },
            seo: {
                title: 'Root Category 3 SEO Title',
                meta: [],
                urn: 'root-category-3',
            },
        },
    },
];

@Injectable()
export class BazaContentTypesCategoriesFixture implements BazaE2eFixture {
    constructor(
        private readonly service: CategoryService,
    ) {}

    async up(): Promise<void> {
        await this.upCategories(BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE);
    }

    private async upCategories(requests: Array<FixtureRequest>, parentId?: number): Promise<void> {
        for (const fixtureRequest of requests) {
            const entity = await this.service.create({
                ...fixtureRequest.category,
                parentId,
            });

            fixtureRequest.$id = entity.id;

            if (Array.isArray(fixtureRequest.children) && fixtureRequest.children.length > 0) {
                await this.upCategories(fixtureRequest.children, entity.id);
            }
        }
    }
}

defineE2eFixtures([{
    fixture: BazaContentTypesCategoriesFixtures.BazaContentTypeCategories,
    provider: BazaContentTypesCategoriesFixture,
}]);
