import { Body, Controller, Get, Param, ParseIntPipe, Post, UseGuards } from '@nestjs/common';
import {
    allBazaContentBlockPairs,
    CategoriesCmsEndpoint,
    CategoriesCmsEndpointPaths,
    CategoriesDto,
    CategoryDto,
    CategoriesCmsCreateRequest,
    CategoriesDeleteRequest,
    CategoriesUpdateRequest,
    BazaContentTypesCmsOpenApi,
    CategoriesCmsSetSortOrderRequest,
    BazaContentTypesCmsSetSortOrderResponse,
    BazaContentTypeAcl,
} from '@scaliolabs/baza-content-types-shared';
import { ApiBearerAuth, ApiExtraModels, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { CategoryService } from '../services/category.service';
import { CategoryMapper } from '../mappers/category.mapper';
import { CategoryRepository } from '../repositories/category.repository';
import { CategoryListMapper } from '../mappers/category-list.mapper';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Categories)
@ApiExtraModels(...allBazaContentBlockPairs)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesCategories])
export class CategoryCmsController implements CategoriesCmsEndpoint {
    constructor(
        private readonly service: CategoryService,
        private readonly repository: CategoryRepository,
        private readonly mapper: CategoryMapper,
        private readonly listMapper: CategoryListMapper,
    ) {}

    @Post(CategoriesCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: CategoryDto,
    })
    async create(@Body() request: CategoriesCmsCreateRequest): Promise<CategoryDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(CategoriesCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: CategoryDto,
    })
    async update(@Body() request: CategoriesUpdateRequest): Promise<CategoryDto> {
        await this.service.shouldNotBeInternalCategory(request.id);

        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(CategoriesCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: CategoriesDeleteRequest): Promise<void> {
        await this.service.shouldNotBeInternalCategory(request.id);

        await this.service.delete(request);
    }

    @Get(CategoriesCmsEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
    })
    @ApiOkResponse({
        type: CategoriesDto,
    })
    async getAll(): Promise<CategoriesDto> {
        const entities = await this.repository.getAll();

        return this.listMapper.entitiesToTree(entities);
    }

    @Get(CategoriesCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: CategoryDto,
    })
    async getById(@Param('id', ParseIntPipe) id: number): Promise<CategoryDto> {
        await this.service.shouldNotBeInternalCategory(id);

        const entity = await this.repository.getById(id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(CategoriesCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: BazaContentTypesCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: CategoriesCmsSetSortOrderRequest): Promise<BazaContentTypesCmsSetSortOrderResponse> {
        await this.service.shouldNotBeInternalCategory(request.id);

        return this.service.setSortOrder(request);
    }
}
