import { Injectable } from '@nestjs/common';
import { Connection, In, IsNull, Repository } from 'typeorm';
import { BazaContentTypeCategoryEntity } from '../entities/baza-content-type-category.entity';
import { CategoryNotFoundException } from '../exceptions/category-not-found.exception';
import { CategoryScope } from '@scaliolabs/baza-content-types-shared';

export const BAZA_CONTENT_TYPE_CATEGORY_RELATIONS: Array<keyof BazaContentTypeCategoryEntity> = [
    'parent',
];

@Injectable()
export class CategoryRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaContentTypeCategoryEntity> {
        return this.connection.getRepository(BazaContentTypeCategoryEntity);
    }

    async save(entities: Array<BazaContentTypeCategoryEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaContentTypeCategoryEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<BazaContentTypeCategoryEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: BAZA_CONTENT_TYPE_CATEGORY_RELATIONS,
        });
    }

    async findByIds(ids: Array<number>): Promise<Array<BazaContentTypeCategoryEntity>> {
        if (! ids.length) {
            return [];
        }

        return this.repository.find({
            where: [{
                id: In(ids),
            }],
            relations: BAZA_CONTENT_TYPE_CATEGORY_RELATIONS,
        });
    }

    async findByUrl(url: string): Promise<BazaContentTypeCategoryEntity | undefined> {
        return this.repository.findOne({
            where: [{
                url,
            }],
            relations: BAZA_CONTENT_TYPE_CATEGORY_RELATIONS,
        });
    }

    async getById(id: number): Promise<BazaContentTypeCategoryEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new CategoryNotFoundException();
        }

        return entity;
    }

    async getAll(): Promise<Array<BazaContentTypeCategoryEntity>> {
        return this.repository.find({
            relations: BAZA_CONTENT_TYPE_CATEGORY_RELATIONS,
            where: [{
                scope: CategoryScope.Public,
            }],
        });
    }

    async getAllActiveCategories(): Promise<Array<BazaContentTypeCategoryEntity>> {
        const entities = await this.getAll();
        const filtered: Array<BazaContentTypeCategoryEntity> = [];

        const deep = (input: Array<BazaContentTypeCategoryEntity>) => {
            for (const entity of input) {
                if (entity.isActive) {
                    filtered.push(entity);

                    deep(entities.filter((e) => e.parentId === entity.id));
                }
            }
        };

        deep(entities.filter((e) => ! e.parentId));

        return filtered;
    }

    async getRootCategories(): Promise<Array<BazaContentTypeCategoryEntity>> {
        return this.repository.find({
            relations: BAZA_CONTENT_TYPE_CATEGORY_RELATIONS,
            where: [{
                parent: IsNull(),
            }],
        });
    }

    async getAllOfParent(parent: BazaContentTypeCategoryEntity | number): Promise<Array<BazaContentTypeCategoryEntity>> {
        return this.repository.find({
            relations: BAZA_CONTENT_TYPE_CATEGORY_RELATIONS,
            where: [{
                parent,
            }],
        });
    }
}
