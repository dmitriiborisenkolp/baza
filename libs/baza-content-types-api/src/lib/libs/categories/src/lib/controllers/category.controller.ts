import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypesOpenApi,
    CategoriesDto,
    CategoriesEndpoint,
    CategoriesEndpointPaths,
    CategoryDto,
    CategoryScope,
} from '@scaliolabs/baza-content-types-shared';
import { CategoryMapper } from '../mappers/category.mapper';
import { CategoryService } from '../services/category.service';
import { CategoryListMapper } from '../mappers/category-list.mapper';
import { CategoryRepository } from '../repositories/category.repository';
import { CategoryNotFoundException } from '../exceptions/category-not-found.exception';
import { CategoryIsInternalException } from '../exceptions/category-is-internal.exception';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Categories)
export class CategoryController implements CategoriesEndpoint {
    constructor(
        private readonly service: CategoryService,
        private readonly repository: CategoryRepository,
        private readonly mapper: CategoryMapper,
        private readonly listMapper: CategoryListMapper,
    ) {}

    @Get(CategoriesEndpointPaths.get)
    @ApiOperation({
        summary: 'get',
        description: 'Returns category by URL or ID (if possible)',
    })
    @ApiOkResponse({
        type: CategoryDto,
    })
    async get(@Param('urlOrId') urlOrId: string | number): Promise<CategoryDto> {
        const parsedInt = parseInt(urlOrId as string, 10);

        const entity = parsedInt
            ? (await this.repository.findByUrl(urlOrId.toString())) || (await this.repository.findById(parsedInt))
            : await this.repository.findByUrl(urlOrId.toString());

        if (!entity) {
            throw new CategoryNotFoundException();
        }

        if (entity.scope === CategoryScope.Internal) {
            throw new CategoryIsInternalException();
        }

        return this.mapper.entityToDTO(entity);
    }

    @Get(CategoriesEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
        description: 'Returns categories tree',
    })
    @ApiOkResponse({
        type: CategoriesDto,
    })
    async getAll(): Promise<CategoriesDto> {
        const entities = await this.repository.getAllActiveCategories();

        return this.listMapper.entitiesToTree(entities);
    }
}
