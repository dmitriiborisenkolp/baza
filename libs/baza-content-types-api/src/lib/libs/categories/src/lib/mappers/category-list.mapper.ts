import { Injectable } from '@nestjs/common';
import { BazaContentTypeCategoryEntity } from '../entities/baza-content-type-category.entity';
import { CategoriesDto, CategoryListDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class CategoryListMapper {
    entityToDTO(input: BazaContentTypeCategoryEntity): CategoryListDto {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            isActive: input.isActive,
            parentId: input.parentId || undefined,
            title: input.title,
            url: input.seo && input.seo.urn
                ? input.seo.urn
                : input.id.toString(),
        };
    }

    entitiesToDTOs(input: Array<BazaContentTypeCategoryEntity>): Array<CategoryListDto> {
        return input.map((e) => this.entityToDTO(e));
    }

    entitiesToTree(input: Array<BazaContentTypeCategoryEntity>): CategoriesDto {
        const result: CategoriesDto = {
            level: 0,
            children: [],
        };

        const deep = (node: CategoriesDto, level: number, parentId?: number) => {
            const filtered = input
                .filter((e) => parentId ? e.parentId === parentId : ! e.parentId)
                .sort((a, b) => a.sortOrder - b.sortOrder);

            for (const category of filtered) {
                const nextNode: CategoriesDto = {
                    level,
                    category,
                    children: [],
                };

                deep(nextNode, level + 1, category.id);

                if (nextNode.category || nextNode.children.length > 0) {
                    nextNode.children.sort((a, b) => a.category.sortOrder - b.category.sortOrder);

                    node.children.push(nextNode);
                }
            }
        };

        deep(result, 1);

        return result;
    }
}
