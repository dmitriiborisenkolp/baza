import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypeCategoriesErrorCodes, bazaContentTypeCategoriesErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class CategoryNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypeCategoriesErrorCodes.BazaContentTypeCategoryNotFound,
            bazaContentTypeCategoriesErrorCodesI18n[BazaContentTypeCategoriesErrorCodes.BazaContentTypeCategoryNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
