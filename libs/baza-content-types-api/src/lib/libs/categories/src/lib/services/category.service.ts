import { Injectable } from '@nestjs/common';
import { CategoryRepository } from '../repositories/category.repository';
import { BazaContentTypeCategoriesCmsEntityBody, BazaContentTypesCmsSetSortOrderResponse, CategoriesCmsCreateRequest, CategoriesCmsSetSortOrderRequest, CategoriesDeleteRequest, CategoriesUpdateRequest, CategoryDto, CategoryScope, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypeCategoryEntity } from '../entities/baza-content-type-category.entity';
import * as _ from 'lodash';
import { CrudSortService } from '@scaliolabs/baza-core-api';
import { CategoryListMapper } from '../mappers/category-list.mapper';
import { CategoryIsInternalException } from '../exceptions/category-is-internal.exception';
import { bazaSeoDefault, generateRandomHexString } from '@scaliolabs/baza-core-shared';

@Injectable()
export class CategoryService {
    constructor(
        private readonly repository: CategoryRepository,
        private readonly listMapper: CategoryListMapper,
        private readonly crudSort: CrudSortService,
    ) {}

    async create(request: CategoriesCmsCreateRequest, scope: CategoryScope = CategoryScope.Public): Promise<BazaContentTypeCategoryEntity> {
        const entity = new BazaContentTypeCategoryEntity();

        await this.populate(entity, request);

        entity.scope = scope;

        const nearby = entity.parent
            ? await this.repository.getAllOfParent(entity.parent)
            : await this.repository.getRootCategories();

        entity.sortOrder = (_.max(nearby.map((e) => e.sortOrder)) || 0) + 1;

        await this.crudSort.normalize([...nearby, entity]);

        await this.repository.save([...nearby, entity]);

        return entity;
    }

    async createInternalCategory(request: {
        title?: string,
    } = {}): Promise<BazaContentTypeCategoryEntity> {
        const entity = new BazaContentTypeCategoryEntity();

        entity.title = request.title || generateRandomHexString();
        entity.scope = CategoryScope.Internal;
        entity.isActive = true;
        entity.contents = emptyBazaContentDto();
        entity.seo = bazaSeoDefault();

        await this.repository.save([entity]);

        return entity;
    }

    async update(request: CategoriesUpdateRequest): Promise<BazaContentTypeCategoryEntity> {
        const entity = await this.repository.getById(request.id);

        const previousParentId = entity.parentId || null;
        const newParentId = request.parentId || null;

        await this.populate(entity, request);

        if (previousParentId !== newParentId) {
            for (const parentId of [previousParentId, newParentId]) {
                const nearby = (parentId
                    ? await this.repository.getAllOfParent(parentId)
                    : await this.repository.getRootCategories()).filter((e) => e.id !== entity.id);

                await this.crudSort.normalize(nearby);
                await this.repository.save(nearby);

                if (parentId === newParentId) {
                    entity.sortOrder = nearby.length + 1;
                }
            }
        }

        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: CategoriesDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        const nearby = (entity.parent
            ? await this.repository.getAllOfParent(entity.parent)
            : await this.repository.getRootCategories()).filter((n) => n.id !== request.id);

        await this.crudSort.normalize(nearby);

        await this.repository.remove([entity]);
        await this.repository.save(nearby);
    }

    async setSortOrder(request: CategoriesCmsSetSortOrderRequest): Promise<BazaContentTypesCmsSetSortOrderResponse> {
        const entity = await this.repository.getById(request.id);

        const nearby = entity.parent
            ? await this.repository.getAllOfParent(entity.parent)
            : await this.repository.getRootCategories();

        const response = this.crudSort.setSortOrder<BazaContentTypeCategoryEntity, CategoryDto>({
            id: entity.id,
            setSortOrder: request.setSortOrder,
            entities: nearby,
            mapper: async (items) => this.listMapper.entitiesToDTOs(items),
        });

        await this.repository.save([...nearby, entity]);

        return response;
    }

    async shouldNotBeInternalCategory(categoryId: number): Promise<void> {
        const entity = await this.repository.getById(categoryId);

        if (entity.scope === CategoryScope.Internal) {
            throw new CategoryIsInternalException();
        }
    }

    async populate(target: BazaContentTypeCategoryEntity, entityBody: BazaContentTypeCategoriesCmsEntityBody): Promise<void> {
        target.url = entityBody.seo && entityBody.seo.urn
            ? entityBody.seo.urn
            : undefined;
        target.isActive = entityBody.isActive;
        target.title = entityBody.title;
        target.contents = entityBody.contents;
        target.seo = entityBody.seo;
        target.parent = entityBody.parentId
            ? await this.repository.getById(entityBody.parentId)
            : null;
    }
}
