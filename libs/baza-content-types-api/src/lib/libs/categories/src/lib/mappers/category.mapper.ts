import { Injectable } from '@nestjs/common';
import { BazaContentTypeCategoryEntity } from '../entities/baza-content-type-category.entity';
import { CategoriesDto, CategoryDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class CategoryMapper {
    entityToDTO(input: BazaContentTypeCategoryEntity): CategoryDto {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            isActive: input.isActive,
            parentId: input.parentId || undefined,
            title: input.title,
            contents: input.contents,
            seo: input.seo,
        };
    }

    entitiesToDTOs(input: Array<BazaContentTypeCategoryEntity>): Array<CategoryDto> {
        return input.map((e) => this.entityToDTO(e));
    }

    entitiesToTree(input: Array<BazaContentTypeCategoryEntity>): CategoriesDto {
        const result: CategoriesDto = {
            level: 0,
            children: [],
        };

        const deep = (node: CategoriesDto, level: number, parentId?: number) => {
            const filtered = input
                .filter((e) => parentId ? e.parentId === parentId : ! e.parentId)
                .sort((a, b) => a.sortOrder - b.sortOrder);

            for (const category of filtered) {
                const nextNode: CategoriesDto = {
                    level,
                    category,
                    children: [],
                };

                deep(nextNode, level + 1, category.id);

                if (nextNode.category || nextNode.children.length > 0) {
                    nextNode.children.sort((a, b) => a.category.sortOrder - b.category.sortOrder);

                    node.children.push(nextNode);
                }
            }
        };

        deep(result, 1);

        return result;
    }
}
