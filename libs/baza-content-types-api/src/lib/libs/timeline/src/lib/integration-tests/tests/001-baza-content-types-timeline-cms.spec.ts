import 'reflect-metadata';
import { CategoriesCmsNodeAccess, TagsCmsNodeAccess, TimelineCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { CategoriesDto, CategoryDto, emptyBazaContentDto, TagCmsDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesCategoriesFixtures } from '../../../../../categories/src';
import { BazaContentTypesTagsFixtures } from '../../../../../tags/src';

describe('@scaliolabs/baza-content-types/timeline/001-baza-content-types-timeline-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new TimelineCmsNodeAccess(http);
    const dataAccessTags = new TagsCmsNodeAccess(http);
    const dataAccessCategories = new CategoriesCmsNodeAccess(http);

    let ID: number;
    let ID_WITH_GROUP: number;

    let CATEGORIES: CategoriesDto;
    let TAGS: Array<TagCmsDto>;

    let CATEGORY_IDS: Array<number>;
    let TAG_IDS: Array<number>;

    let CATEGORY_GROUP: CategoryDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaContentTypesCategoriesFixtures.BazaContentTypeCategories,
                BazaContentTypesTagsFixtures.BazaContentTypesTags,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        CATEGORIES = await dataAccessCategories.getAll();
        CATEGORY_IDS = [CATEGORIES.children[0].category.id, CATEGORIES.children[1].category.id];

        CATEGORY_GROUP = await dataAccessCategories.create({
            title: 'EliteInvestors',
            isActive: true,
            contents: emptyBazaContentDto(),
            seo: bazaSeoDefault(),
        });

        TAGS = (
            await dataAccessTags.list({
                index: 1,
                size: 10,
            })
        ).items;

        TAG_IDS = [TAGS[0].id, TAGS[1].id];
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new timeline entry', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-20T00:00:00.000Z',
            title: 'Timeline 1',
            textDescription: 'Timeline 1 Text Description',
            htmlDescription: 'Timeline 1 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isPublished).toBeTruthy();
        expect(response.title).toBe('Timeline 1');
        expect(response.date).toBe('2022-01-20T00:00:00.000Z');
        expect(response.tags.map((t) => t.id)).toEqual([...TAG_IDS]);
        expect(response.categories.map((c) => c.id)).toEqual([...CATEGORY_IDS]);

        ID = response.id;
    });

    it('will returns created timeline entry by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isPublished).toBeTruthy();
        expect(response.title).toBe('Timeline 1');
        expect(response.date).toBe('2022-01-20T00:00:00.000Z');
        expect(response.tags.map((t) => t.id)).toEqual([...TAG_IDS]);
        expect(response.categories.map((c) => c.id)).toEqual([...CATEGORY_IDS]);
    });

    it('will display created timeline entry in list', async () => {
        const response = await dataAccessCms.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);
        expect(response.items.length).toBe(1);

        const item = response.items[0];

        expect(item.isPublished).toBeTruthy();
        expect(item.title).toBe('Timeline 1');
        expect(item.date).toBe('2022-01-20T00:00:00.000Z');
        expect(item.tags.map((t) => t.id)).toEqual([...TAG_IDS]);
        expect(item.categories.map((c) => c.id)).toEqual([...CATEGORY_IDS]);
    });

    it('will successfully create timeline entry with group', async () => {
        const response = await dataAccessCms.create({
            isPublished: true,
            categoryId: CATEGORY_GROUP.id,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-20T00:00:00.000Z',
            title: 'Timeline EliteInvestors-1',
            textDescription: 'Timeline EliteInvestors-1 Text Description',
            htmlDescription: 'Timeline EliteInvestors-1 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Timeline EliteInvestors-1');

        ID_WITH_GROUP = response.id;
    });

    it('will returns timeline entry with group by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID_WITH_GROUP,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isPublished).toBeTruthy();
        expect(response.title).toBe('Timeline EliteInvestors-1');
        expect(response.date).toBe('2022-01-20T00:00:00.000Z');
        expect(response.tags.map((t) => t.id)).toEqual([...TAG_IDS]);
        expect(response.categories.map((c) => c.id)).toEqual([...CATEGORY_IDS]);
    });

    it('will not display created timeline entry with group in common list response', async () => {
        const response = await dataAccessCms.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);
        expect(response.items.length).toBe(1);

        const item = response.items[0];

        expect(item.isPublished).toBeTruthy();
        expect(item.title).toBe('Timeline 1');
        expect(item.date).toBe('2022-01-20T00:00:00.000Z');
        expect(item.tags.map((t) => t.id)).toEqual([...TAG_IDS]);
        expect(item.categories.map((c) => c.id)).toEqual([...CATEGORY_IDS]);
    });

    it('display created timeline entry with group in group list response', async () => {
        const response = await dataAccessCms.list({
            index: 1,
            size: 10,
            categoryId: CATEGORY_GROUP.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);
        expect(response.items.length).toBe(1);

        const item = response.items[0];

        expect(item.isPublished).toBeTruthy();
        expect(item.title).toBe('Timeline EliteInvestors-1');
        expect(item.date).toBe('2022-01-20T00:00:00.000Z');
        expect(item.tags.map((t) => t.id)).toEqual([...TAG_IDS]);
        expect(item.categories.map((c) => c.id)).toEqual([...CATEGORY_IDS]);
    });

    it('will successfully update timeline entry', async () => {
        const response = await dataAccessCms.update({
            id: ID,
            isPublished: false,
            tagIds: [...TAG_IDS.slice(0, 1)],
            categoryIds: [...CATEGORY_IDS.slice(0, 1)],
            date: '2022-01-20T00:00:00.000Z',
            title: 'Timeline 1*',
            textDescription: 'Timeline 1 Text Description',
            htmlDescription: 'Timeline 1 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isPublished).toBeFalsy();
        expect(response.title).toBe('Timeline 1*');
        expect(response.date).toBe('2022-01-20T00:00:00.000Z');
        expect(response.tags.map((t) => t.id)).toEqual([...TAG_IDS.slice(0, 1)]);
        expect(response.categories.map((c) => c.id)).toEqual([...CATEGORY_IDS.slice(0, 1)]);

        ID = response.id;
    });

    it('will display updates in list', async () => {
        const response = await dataAccessCms.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);
        expect(response.items.length).toBe(1);

        const item = response.items[0];

        expect(item.isPublished).toBeFalsy();
        expect(item.title).toBe('Timeline 1*');
        expect(item.date).toBe('2022-01-20T00:00:00.000Z');
        expect(item.tags.map((t) => t.id)).toEqual([...TAG_IDS.slice(0, 1)]);
        expect(item.categories.map((c) => c.id)).toEqual([...CATEGORY_IDS.slice(0, 1)]);
    });

    it('will successfully delete timeline entry', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted timeline entry in list', async () => {
        const response = await dataAccessCms.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(0);
        expect(response.items.length).toBe(0);
    });
});
