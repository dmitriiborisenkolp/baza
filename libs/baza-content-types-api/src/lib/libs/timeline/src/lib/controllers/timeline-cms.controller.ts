import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    TimelineCmsCreateRequest,
    TimelineCmsDeleteRequest,
    TimelineCmsDto,
    TimelineCmsEndpoint,
    TimelineCmsEndpointPaths,
    TimelineCmsGetByIdRequest,
    TimelineCmsListRequest,
    TimelineCmsListResponse,
    TimelineCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { TimelineCmsMapper } from '../mappers/timeline-cms.mapper';
import { TimelineCmsService } from '../services/timeline-cms.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Timeline)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesTimeline])
export class TimelineCmsController implements TimelineCmsEndpoint {
    constructor(private readonly mapper: TimelineCmsMapper, private readonly service: TimelineCmsService) {}

    @Post(TimelineCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: TimelineCmsDto,
    })
    async create(@Body() request: TimelineCmsCreateRequest): Promise<TimelineCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TimelineCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: TimelineCmsDto,
    })
    async update(@Body() request: TimelineCmsUpdateRequest): Promise<TimelineCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TimelineCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: TimelineCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(TimelineCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: TimelineCmsListResponse,
    })
    async list(@Body() request: TimelineCmsListRequest): Promise<TimelineCmsListResponse> {
        return this.service.list(request);
    }

    @Post(TimelineCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: TimelineCmsDto,
    })
    async getById(@Body() request: TimelineCmsGetByIdRequest): Promise<TimelineCmsDto> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity);
    }
}
