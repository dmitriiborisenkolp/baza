import { Injectable } from '@nestjs/common';
import { Connection, IsNull, Repository } from 'typeorm';
import { TIMELINE_ENTITY_RELATIONS, TimelineEntity } from '../entities/timeline.entity';
import { TimelineNotFoundException } from '../exceptions/timeline-not-found.exception';
import { BAZA_CONTENT_TYPES_CONFIGURATION } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypeCategoryEntity } from '../../../../categories/src';

@Injectable()
export class TimelineRepository {
    constructor(
        private readonly connection: Connection,
    ) {
    }

    get repository(): Repository<TimelineEntity> {
        return this.connection.getRepository(TimelineEntity);
    }

    async save(entities: Array<TimelineEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<TimelineEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<TimelineEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: TIMELINE_ENTITY_RELATIONS,
        });
    }

    async getById(id: number): Promise<TimelineEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new TimelineNotFoundException({ id });
        }

        return entity;
    }

    async findAll(): Promise<Array<TimelineEntity>> {
        return this.repository.find({
            relations: TIMELINE_ENTITY_RELATIONS,
            order: {
                date: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.sortOrder,
            },
        });
    }

    async findPublished(): Promise<Array<TimelineEntity>> {
        return this.repository.find({
            where: [{
                isPublished: true,
                category: IsNull(),
            }],
            relations: TIMELINE_ENTITY_RELATIONS,
            order: {
                date: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.sortOrder,
            },
        });
    }

    async findPublishedById(id: number): Promise<TimelineEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
                isPublished: true,
                category: IsNull(),
            }],
            relations: TIMELINE_ENTITY_RELATIONS,
        });
    }

    async getPublishedById(id: number): Promise<TimelineEntity> {
        const entity = await this.findPublishedById(id);

        if (! entity) {
            throw new TimelineNotFoundException();
        }

        return entity;
    }

    async findTimelineOfCategory(category: number | BazaContentTypeCategoryEntity): Promise<Array<TimelineEntity>> {
        return this.repository.find({
            where: [{
                isPublished: true,
                category,
            }],
            relations: TIMELINE_ENTITY_RELATIONS,
            order: {
                date: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.sortOrder,
            },
        });
    }
}
