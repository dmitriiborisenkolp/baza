import { BazaAppException } from '@scaliolabs/baza-core-api';
import { timelineErrorCodeI18n, TimelineErrorCodes } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class TimelineNotFoundException extends BazaAppException {
    constructor(details?: any) {
        super(
            TimelineErrorCodes.TimeLineNotFound,
            timelineErrorCodeI18n[TimelineErrorCodes.TimeLineNotFound],
            HttpStatus.NOT_FOUND,
            undefined,
            details,
        );
    }
}
