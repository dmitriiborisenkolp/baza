import { Module } from '@nestjs/common';
import { BazaAttachmentModule, BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { TimelineController } from './controllers/timeline.controller';
import { TimelineCmsController } from './controllers/timeline-cms.controller';
import { TimelineMapper } from './mappers/timeline.mapper';
import { TimelineCmsMapper } from './mappers/timeline-cms.mapper';
import { TimelineRepository } from './repositories/timeline.repository';
import { TimelineService } from './services/timeline.service';
import { TimelineCmsService } from './services/timeline-cms.service';
import { BazaContentTypeCategoriesApiModule } from '../../../categories/src';

@Module({
    imports: [
        BazaCrudApiModule,
        BazaAttachmentModule,
        BazaContentTypeCategoriesApiModule,
    ],
    controllers: [
        TimelineController,
        TimelineCmsController,
    ],
    providers: [
        TimelineMapper,
        TimelineCmsMapper,
        TimelineRepository,
        TimelineService,
        TimelineCmsService,
    ],
    exports: [
        TimelineMapper,
        TimelineCmsMapper,
        TimelineRepository,
        TimelineService,
        TimelineCmsService,
    ],
})
export class BazaContentTypesTimelineApiModule {}
