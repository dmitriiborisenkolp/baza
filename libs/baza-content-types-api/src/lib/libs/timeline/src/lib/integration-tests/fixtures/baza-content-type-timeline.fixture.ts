import { TimelineCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { TimelineCmsService } from '../../services/timeline-cms.service';
import { BazaContentTypesTimelineFixtures } from '../baza-content-types-timeline.fixtures';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE } from '../../../../../tags/src';
import { BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE } from '../../../../../categories/src';

const TAG_IDS = [E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE[0].$refId, E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE[1].$refId];
const CATEGORY_IDS = [BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE[0].$refId, BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE[1].$refId];

export const E2E_BAZA_CONTENT_TYPES_TIMELINE_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    request: TimelineCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            isPublished: true,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-20T00:00:00.000Z',
            title: 'Timeline 1',
            textDescription: 'Timeline 1 Text Description',
            htmlDescription: 'Timeline 1 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 2,
        request: {
            isPublished: false,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-21T00:00:00.000Z',
            title: 'Timeline 2',
            textDescription: 'Timeline 2 Text Description',
            htmlDescription: 'Timeline 2 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 3,
        request: {
            isPublished: true,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-22T00:00:00.000Z',
            title: 'Timeline 3',
            textDescription: 'Timeline 3 Text Description',
            htmlDescription: 'Timeline 3 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        },
    },
];

@Injectable()
export class BazaContentTypeTimelineFixture implements BazaE2eFixture {
    constructor(
        private readonly service: TimelineCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_TIMELINE_FIXTURE) {
            const entity = await this.service.create({
                ...fixtureRequest.request,
                tagIds: fixtureRequest.request.tagIds.map(
                    ($refId) => E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE.find((d) => d.$refId === $refId).$id,
                ),
                categoryIds: fixtureRequest.request.categoryIds.map(
                    ($refId) => BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE.find((d) => d.$refId === $refId).$id,
                ),
            });

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesTimelineFixtures>([{
    fixture: BazaContentTypesTimelineFixtures.BazaContentTypesTimeline,
    provider: BazaContentTypeTimelineFixture,
}]);
