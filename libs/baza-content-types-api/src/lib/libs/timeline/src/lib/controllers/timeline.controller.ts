import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaContentTypesOpenApi, TimelineDto, TimelineEndpoint, TimelineEndpointPaths } from '@scaliolabs/baza-content-types-shared';
import { TimelineMapper } from '../mappers/timeline.mapper';
import { TimelineService } from '../services/timeline.service';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Timeline)
export class TimelineController implements TimelineEndpoint {
    constructor(private readonly mapper: TimelineMapper, private readonly service: TimelineService) {}

    @Get(TimelineEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
        description: 'Returns all published timeline entries',
    })
    @ApiOkResponse({
        type: TimelineDto,
        isArray: true,
    })
    async getAll(): Promise<Array<TimelineDto>> {
        const entities = await this.service.getAll();

        return this.mapper.entitiesToDTOs(entities);
    }

    @Get(TimelineEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns published timeline entry',
    })
    @ApiOkResponse({
        type: TimelineDto,
    })
    async getById(@Param('id', ParseIntPipe) id: number): Promise<TimelineDto> {
        const entity = await this.service.getById(id);

        return this.mapper.entityToDTO(entity);
    }
}
