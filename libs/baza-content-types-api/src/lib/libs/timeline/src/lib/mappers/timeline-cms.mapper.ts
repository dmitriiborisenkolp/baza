import { Injectable } from '@nestjs/common';
import { TimelineMapper } from './timeline.mapper';
import { TimelineEntity } from '../entities/timeline.entity';
import { TimelineCmsDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class TimelineCmsMapper {
    constructor(
        private readonly baseMapper: TimelineMapper,
    ) {
    }

    async entityToDTO(input: TimelineEntity): Promise<TimelineCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            imageAttachment: input.image,
        };
    }

    async entitiesToDTOs(input: Array<TimelineEntity>): Promise<Array<TimelineCmsDto>> {
        return Promise.all(
            input.map((e) => this.entityToDTO(e)),
        );
    }
}
