import { emptyBazaContentDto, TimelineCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { TimelineCmsService } from '../../services/timeline-cms.service';
import { BazaContentTypesTimelineFixtures } from '../baza-content-types-timeline.fixtures';
import { E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE } from '../../../../../tags/src';
import { BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE, CategoryService } from '../../../../../categories/src';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';

const TAG_IDS = [E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE[0].$refId, E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE[1].$refId];
const CATEGORY_IDS = [BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE[0].$refId, BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE[1].$refId];

export const E2E_BAZA_CONTENT_TYPES_TIMELINE_GROUP_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    $group: string;
    request: TimelineCmsCreateRequest;
}> = [
    {
        $refId: 1,
        $group: 'EliteInvestors',
        request: {
            isPublished: true,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-20T00:00:00.000Z',
            title: 'Timeline EliteInvestors-1',
            textDescription: 'Timeline EliteInvestors-1 Text Description',
            htmlDescription: 'Timeline EliteInvestors-1 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 2,
        $group: 'EliteInvestors',
        request: {
            isPublished: true,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-21T00:00:00.000Z',
            title: 'Timeline EliteInvestors-1',
            textDescription: 'Timeline EliteInvestors-2 Text Description',
            htmlDescription: 'Timeline EliteInvestors-2 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 3,
        $group: 'EliteInvestors',
        request: {
            isPublished: false,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-21T00:00:00.000Z',
            title: 'Timeline EliteInvestors-3',
            textDescription: 'Timeline EliteInvestors-3 Text Description',
            htmlDescription: 'Timeline EliteInvestors-3 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        },
    },
    {
        $refId: 4,
        $group: 'G2',
        request: {
            isPublished: true,
            tagIds: [...TAG_IDS],
            categoryIds: [...CATEGORY_IDS],
            date: '2022-01-22T00:00:00.000Z',
            title: 'Timeline G2-1',
            textDescription: 'Timeline G2-1 Text Description',
            htmlDescription: 'Timeline G2-2 HTML Description',
            metadata: {
                foo: 'bar',
            },
            seo: bazaSeoDefault(),
        },
    },
];

@Injectable()
export class BazaContentTypeTimelineGroupFixture implements BazaE2eFixture {
    constructor(
        private readonly service: TimelineCmsService,
        private readonly categoryService: CategoryService,
    ) {}

    async up(): Promise<void> {
        const categories: { [title: string]: number } = {};

        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_TIMELINE_GROUP_FIXTURE) {
            if (! categories[fixtureRequest.$group]) {
                categories[fixtureRequest.$group] = (await this.categoryService.create({
                    title: fixtureRequest.$group,
                    isActive: true,
                    contents: emptyBazaContentDto(),
                    seo: bazaSeoDefault(),
                })).id;
            }

            const entity = await this.service.create({
                ...fixtureRequest.request,
                categoryId: categories[fixtureRequest.$group],
                tagIds: fixtureRequest.request.tagIds.map(
                    ($refId) => E2E_BAZA_CONTENT_TYPES_TAGS_FIXTURE.find((d) => d.$refId === $refId).$id,
                ),
                categoryIds: fixtureRequest.request.categoryIds.map(
                    ($refId) => BAZA_CONTENT_TYPES_CATEGORIES_FIXTURE.find((d) => d.$refId === $refId).$id,
                ),
            });

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesTimelineFixtures>([{
    fixture: BazaContentTypesTimelineFixtures.BazaContentTypesTimelineGroup,
    provider: BazaContentTypeTimelineGroupFixture,
}]);
