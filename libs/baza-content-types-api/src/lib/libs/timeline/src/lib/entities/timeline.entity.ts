import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { TagEntity } from '../../../../tags/src';
import { BazaContentTypeCategoryEntity } from '../../../../categories/src';
import { AttachmentDto, bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';

export const TIMELINE_ENTITY_RELATIONS = ['tags', 'categories', 'category'];

@Entity({
    name: 'baza_content_types_timeline_entity',
})
export class TimelineEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    isPublished: boolean;

    @ManyToOne(() => BazaContentTypeCategoryEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    category?: BazaContentTypeCategoryEntity;

    @ManyToMany(() => TagEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinTable()
    tags: Array<TagEntity>;

    get tagIds(): Array<number> {
        return (this.tags || []).map((t) => t.id);
    }

    @ManyToMany(() => BazaContentTypeCategoryEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinTable()
    categories: Array<BazaContentTypeCategoryEntity>;

    get categoryIds(): Array<number> {
        return (this.categories || []).map((c) => c.id);
    }

    @Column()
    date: Date;

    @Column()
    title: string;

    @Column({
        nullable: true,
    })
    textDescription?: string;

    @Column({
        nullable: true,
    })
    htmlDescription?: string;

    @Column({
        type: 'jsonb',
        nullable: true,
    })
    image?: AttachmentDto;

    @Column({
        type: 'json',
    })
    metadata: Record<string, string>;

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto;
}
