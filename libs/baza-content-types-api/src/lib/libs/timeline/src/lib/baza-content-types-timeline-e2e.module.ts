import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypesTimelineApiModule } from './baza-content-types-timeline-api.module';
import { BazaContentTypeTimelineGroupFixture } from './integration-tests/fixtures/baza-content-type-timeline-group.fixture';
import { BazaContentTypeTimelineFixture } from './integration-tests/fixtures/baza-content-type-timeline.fixture';

const E2E_FIXTURES = [
    BazaContentTypeTimelineFixture,
    BazaContentTypeTimelineGroupFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypesTimelineApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypesTimelineE2eModule {}
