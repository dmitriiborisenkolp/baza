import { Injectable } from '@nestjs/common';
import { TimelineRepository } from '../repositories/timeline.repository';
import { CrudService } from '@scaliolabs/baza-core-api';
import { TIMELINE_ENTITY_RELATIONS, TimelineEntity } from '../entities/timeline.entity';
import { BAZA_CONTENT_TYPES_CONFIGURATION, TimelineCmsCreateRequest, TimelineCmsDeleteRequest, TimelineCmsDto, TimelineCmsEntityBody, TimelineCmsGetByIdRequest, TimelineCmsListRequest, TimelineCmsListResponse, TimelineCmsUpdateRequest } from '@scaliolabs/baza-content-types-shared';
import { TagsRepository } from '../../../../tags/src';
import { CategoryRepository } from '../../../../categories/src';
import { TimelineCmsMapper } from '../mappers/timeline-cms.mapper';
import { IsNull } from 'typeorm';

@Injectable()
export class TimelineCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: TimelineCmsMapper,
        private readonly repository: TimelineRepository,
        private readonly tagRepository: TagsRepository,
        private readonly categoryRepository: CategoryRepository,
    ) {}

    async create(request: TimelineCmsCreateRequest): Promise<TimelineEntity> {
        const entity = new TimelineEntity();

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: TimelineCmsUpdateRequest): Promise<TimelineEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: TimelineCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);
    }

    async getById(request: TimelineCmsGetByIdRequest): Promise<TimelineEntity> {
        return this.repository.getById(request.id);
    }

    async list(request: TimelineCmsListRequest): Promise<TimelineCmsListResponse> {
        return this.crud.find<TimelineEntity ,TimelineCmsDto>({
            request,
            entity: TimelineEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                relations: TIMELINE_ENTITY_RELATIONS,
                order: {
                    date: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.sortOrder,
                },
                where: [{
                    category: request.categoryId || IsNull(),
                }],
            },
        });
    }

    private async populate(target: TimelineEntity, entityBody: TimelineCmsEntityBody): Promise<void> {
        target.category = entityBody.categoryId
            ? await this.categoryRepository.getById(entityBody.categoryId)
            : undefined;

        target.isPublished = entityBody.isPublished;
        target.tags = await this.tagRepository.findByIds(entityBody.tagIds);
        target.categories = await this.categoryRepository.findByIds(entityBody.categoryIds);
        target.date = new Date(entityBody.date);
        target.title = entityBody.title;
        target.textDescription = entityBody.textDescription;
        target.htmlDescription = entityBody.htmlDescription;
        target.image = entityBody.imageAttachment;
        target.metadata = entityBody.metadata;
        target.seo = entityBody.seo;
    }
}
