import { Injectable } from '@nestjs/common';
import { TimelineEntity } from '../entities/timeline.entity';
import { TimelineDto } from '@scaliolabs/baza-content-types-shared';
import { TagsMapper } from '../../../../tags/src';
import { CategoryListMapper } from '../../../../categories/src';
import { AttachmentImageService } from '@scaliolabs/baza-core-api';

@Injectable()
export class TimelineMapper {
    constructor(
        private readonly tagsMapper: TagsMapper,
        private readonly categoriesMapper: CategoryListMapper,
        private readonly attachmentImageService: AttachmentImageService,
    ) {
    }

    async entityToDTO(input: TimelineEntity): Promise<TimelineDto> {
        return {
            id: input.id,
            isPublished: input.isPublished,
            tags: await this.tagsMapper.entitiesToDTOs(input.tags || []),
            categories: await this.categoriesMapper.entitiesToDTOs(input.categories || []),
            date: input.date.toISOString(),
            title: input.title,
            textDescription: input.textDescription,
            htmlDescription: input.htmlDescription,
            image: input.image
                ? await this.attachmentImageService.toImageSet(input.image)
                : undefined,
            metadata: input.metadata,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<TimelineEntity>): Promise<Array<TimelineDto>> {
        return Promise.all(
            input.map((e) => this.entityToDTO(e)),
        );
    }
}
