import 'reflect-metadata';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { CategoriesCmsNodeAccess, TagsCmsNodeAccess, TimelineNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { CategoriesDto, TagCmsDto, TimelineErrorCodes } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaContentTypesCategoriesFixtures } from '../../../../../categories/src';
import { BazaContentTypesTagsFixtures } from '../../../../../tags/src';
import { BazaContentTypesTimelineFixtures } from '../baza-content-types-timeline.fixtures';

describe('@scaliolabs/baza-content-types/timeline/002-baza-content-type-timeline.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new TimelineNodeAccess(http);
    const dataAccessTags = new TagsCmsNodeAccess(http);
    const dataAccessCategories = new CategoriesCmsNodeAccess(http);

    let ID: number;

    let CATEGORIES: CategoriesDto;
    let TAGS: Array<TagCmsDto>;

    let CATEGORY_IDS: Array<number>;
    let TAG_IDS: Array<number>;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaContentTypesCategoriesFixtures.BazaContentTypeCategories,
                BazaContentTypesTagsFixtures.BazaContentTypesTags,
                BazaContentTypesTimelineFixtures.BazaContentTypesTimeline,
                BazaContentTypesTimelineFixtures.BazaContentTypesTimelineGroup,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        CATEGORIES = await dataAccessCategories.getAll();
        CATEGORY_IDS = [CATEGORIES.children[0].category.id, CATEGORIES.children[1].category.id];

        TAGS = (
            await dataAccessTags.list({
                index: 1,
                size: 10,
            })
        ).items;

        TAG_IDS = [TAGS[0].id, TAGS[1].id];
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('display list of common timeline entries', async () => {
        const response = await dataAccess.getAll();

        expect(response.length).toBe(2);
        expect(response[0].title).toBe('Timeline 1');
        expect(response[1].title).toBe('Timeline 3');

        ID = response[0].id;
    });

    it('will allow to get published entry by id', async () => {
        const response = await dataAccess.getById(ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Timeline 1');
    });

    it('will not allow to get unpublished entry by id', async () => {
        const response: BazaError = (await dataAccess.getById(ID + 1)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(TimelineErrorCodes.TimeLineNotFound);
    });

    it('will not allow to get unknown entry by id', async () => {
        const response: BazaError = (await dataAccess.getById(ID + 1000)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(TimelineErrorCodes.TimeLineNotFound);
    });
});
