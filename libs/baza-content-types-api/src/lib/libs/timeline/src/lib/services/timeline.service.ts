import { Injectable } from '@nestjs/common';
import { TimelineEntity } from '../entities/timeline.entity';
import { TimelineRepository } from '../repositories/timeline.repository';

@Injectable()
export class TimelineService {
    constructor(private readonly repository: TimelineRepository) {}

    async getById(id: number): Promise<TimelineEntity> {
        return this.repository.getPublishedById(id);
    }

    async getAll(): Promise<Array<TimelineEntity>> {
        return this.repository.findPublished();
    }
}
