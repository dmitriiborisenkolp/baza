import { Injectable } from "@nestjs/common";
import { TeamsMemberEntity } from '../entities/teams-member.entity';
import { TeamMemberCmsDto } from '@scaliolabs/baza-content-types-shared';
import { TeamsMemberMapper } from './teams-member-mapper.service';

@Injectable()
export class TeamsMemberCmsMapper {
    constructor(
        private readonly baseMapper: TeamsMemberMapper,
    ) {}

    async entityToDTO(input: TeamsMemberEntity): Promise<TeamMemberCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            fullImageS3Key: input.fullImageS3Key,
            previewImageS3Key: input.previewImageS3Key,
            isPublished: input.isPublished,
        };
    }

    async entitiesToDTOs(input: Array<TeamsMemberEntity>): Promise<Array<TeamMemberCmsDto>> {
        const result: Array<TeamMemberCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
