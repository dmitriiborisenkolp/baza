import 'reflect-metadata';
import { TeamsMembersCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-content-types/teams/001-baza-content-types-teams-members-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new TeamsMembersCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new team member', async () => {
        const response = await dataAccessCms.create({
            name: 'John Doe',
            title: 'Senior Developer',
            bio: emptyBazaContentDto(),
            seo: bazaSeoDefault(),
            networks: [],
            isPublished: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new team member in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].name).toBe('John Doe');
        expect(listResponse.items[0].sortOrder).toBe(1);
    });

    it('will successfully create new team member without title', async () => {
        const response = await dataAccessCms.create({
            name: 'John Smith',
            bio: emptyBazaContentDto(),
            seo: bazaSeoDefault(),
            networks: [],
            isPublished: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ID = response.id;
    });

    it('will successfully returns team member by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.name).toBe('John Smith');
    });

    it('will successfully update team member', async () => {
        const response = await dataAccessCms.update({
            id: ID,
            name: 'John Smith *',
            bio: emptyBazaContentDto(),
            seo: bazaSeoDefault(),
            networks: [],
            isPublished: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updates in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].name).toBe('John Doe');
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].sortOrder).toBe(2);
        expect(listResponse.items[1].name).toBe('John Smith *');

        ID = listResponse.items[0].id;
    });

    it('will successfully update sort order of team member', async () => {
        const response = await dataAccessCms.setSortOrder({
            id: ID,
            setSortOrder: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updated sort order in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].name).toBe('John Smith *');
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].name).toBe('John Doe');
        expect(listResponse.items[1].sortOrder).toBe(2);

        ID = listResponse.items[0].id;
    });

    it('will successfully delete team member', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted team member in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].name).toBe('John Doe');

        ID = listResponse.items[0].id;
    });
});
