import { Injectable } from '@nestjs/common';
import { TeamsMemberRepository } from '../repositories/teams-member-repository.service';
import { TeamMemberCmsDto, TeamsMemberCmsCreateRequest, TeamsMemberCmsUpdateRequest, TeamsMemberDeleteRequest, TeamsMemberEntityBody, TeamsMemberListRequest, TeamsMemberListResponse, TeamsMemberSetSortOrderRequest, TeamsMemberSetSortOrderResponse } from '@scaliolabs/baza-content-types-shared';
import { TeamsMemberEntity } from '../entities/teams-member.entity';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { TeamsMemberCmsMapper } from '../mappers/teams-member-cms-mapper.service';

@Injectable()
export class TeamsMembersCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: TeamsMemberRepository,
        private readonly mapper: TeamsMemberCmsMapper,
    ) {}

    async create(request: TeamsMemberCmsCreateRequest): Promise<TeamsMemberEntity> {
        const entity = new TeamsMemberEntity();

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: TeamsMemberCmsUpdateRequest): Promise<TeamsMemberEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: TeamsMemberDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);

        const entities = await this.repository.findAll();

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async list(request: TeamsMemberListRequest): Promise<TeamsMemberListResponse> {
        return this.crud.find<TeamsMemberEntity, TeamMemberCmsDto>({
            entity: TeamsMemberEntity,
            request,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'ASC',
                },
            },
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: TeamsMemberSetSortOrderRequest): Promise<TeamsMemberSetSortOrderResponse> {
        const entities = await this.repository.findAll();

        const response = await this.crudSort.setSortOrder<TeamsMemberEntity, TeamMemberCmsDto>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: entities,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });

        await this.repository.save(entities);

        return response;
    }

    async populate(target: TeamsMemberEntity, entityBody: TeamsMemberEntityBody): Promise<void> {
        if (! target.id) {
            target.sortOrder = await this.repository.maxSortOrder() + 1;
        }

        target.name = entityBody.name;
        target.title = entityBody.title;
        target.isPublished = entityBody.isPublished;
        target.url = entityBody.seo && entityBody.seo.urn
            ? entityBody.seo.urn
            : undefined;
        target.bio = entityBody.bio;
        target.fullImageS3Key = entityBody.fullImageS3Key;
        target.previewImageS3Key = entityBody.previewImageS3Key;
        target.networks = entityBody.networks;
        target.seo = entityBody.seo;
    }
}
