import { Module } from "@nestjs/common";
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { TeamsMemberRepository } from './repositories/teams-member-repository.service';
import { TeamsMembersCmsService } from './services/teams-members-cms.service';
import { TeamsMemberCmsMapper } from './mappers/teams-member-cms-mapper.service';
import { TeamsMemberMapper } from './mappers/teams-member-mapper.service';
import { TeamsMemberController } from './controllers/teams-member.controller';
import { TeamsMemberCmsController } from './controllers/teams-member-cms.controller';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        TeamsMemberController,
        TeamsMemberCmsController,
    ],
    providers: [
        TeamsMemberMapper,
        TeamsMemberCmsMapper,
        TeamsMemberRepository,
        TeamsMembersCmsService,
    ],
    exports: [
        TeamsMemberMapper,
        TeamsMemberCmsMapper,
        TeamsMemberRepository,
        TeamsMembersCmsService,
    ],
})
export class BazaContentTypesTeamsApiModule {}
