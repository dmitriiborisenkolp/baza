import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    TeamMemberCmsDto,
    TeamsMemberCmsCreateRequest,
    TeamsMemberCmsEndpoint,
    TeamsMemberCmsEndpointPaths,
    TeamsMemberCmsUpdateRequest,
    TeamsMemberDeleteRequest,
    TeamsMemberGetByIdRequest,
    TeamsMemberListRequest,
    TeamsMemberListResponse,
    TeamsMemberSetSortOrderRequest,
    TeamsMemberSetSortOrderResponse,
} from '@scaliolabs/baza-content-types-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { TeamsMembersCmsService } from '../services/teams-members-cms.service';
import { TeamsMemberRepository } from '../repositories/teams-member-repository.service';
import { TeamsMemberCmsMapper } from '../mappers/teams-member-cms-mapper.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Team)
@AclNodes([BazaContentTypeAcl.BazaContentTypesTeams])
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
export class TeamsMemberCmsController implements TeamsMemberCmsEndpoint {
    constructor(
        private readonly service: TeamsMembersCmsService,
        private readonly repository: TeamsMemberRepository,
        private readonly mapper: TeamsMemberCmsMapper,
    ) {}

    @Post(TeamsMemberCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: TeamMemberCmsDto,
    })
    async create(@Body() request: TeamsMemberCmsCreateRequest): Promise<TeamMemberCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TeamsMemberCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: TeamMemberCmsDto,
    })
    async update(@Body() request: TeamsMemberCmsUpdateRequest): Promise<TeamMemberCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TeamsMemberCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: TeamsMemberDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(TeamsMemberCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: TeamMemberCmsDto,
    })
    async getById(@Body() request: TeamsMemberGetByIdRequest): Promise<TeamMemberCmsDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TeamsMemberCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: TeamsMemberListResponse,
    })
    async list(@Body() request: TeamsMemberListRequest): Promise<TeamsMemberListResponse> {
        return this.service.list(request);
    }

    @Post(TeamsMemberCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: TeamsMemberSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: TeamsMemberSetSortOrderRequest): Promise<TeamsMemberSetSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
