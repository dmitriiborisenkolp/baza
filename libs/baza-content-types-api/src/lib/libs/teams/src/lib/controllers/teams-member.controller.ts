import { Controller, Get, Param } from '@nestjs/common';
import {
    BazaContentTypesOpenApi,
    TeamMemberDto,
    TeamsMemberEndpoint,
    TeamsMemberEndpointPaths,
    TeamsMemberGetAllResponse,
} from '@scaliolabs/baza-content-types-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { TeamsMemberRepository } from '../repositories/teams-member-repository.service';
import { TeamsMemberMapper } from '../mappers/teams-member-mapper.service';
import { TeamsMemberNotFoundException } from '../exceptions/teams-member-not-found.exception';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Team)
export class TeamsMemberController implements TeamsMemberEndpoint {
    constructor(private readonly repository: TeamsMemberRepository, private readonly mapper: TeamsMemberMapper) {}

    @Get(TeamsMemberEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
        description: 'Returns all team members',
    })
    @ApiOkResponse({
        type: TeamMemberDto,
        isArray: true,
    })
    async getAll(): Promise<TeamsMemberGetAllResponse> {
        const entities = await this.repository.findAll();

        return this.mapper.entitiesToDTOs(entities.filter((e) => e.isPublished));
    }

    @Get(TeamsMemberEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns team member by ID',
    })
    @ApiOkResponse({
        type: TeamMemberDto,
    })
    async getById(@Param('id') id: number): Promise<TeamMemberDto> {
        const entity = await this.repository.getById(id);

        if (!entity.isPublished) {
            throw new TeamsMemberNotFoundException();
        }

        return this.mapper.entityToDTO(entity);
    }

    @Get(TeamsMemberEndpointPaths.getByUrl)
    @ApiOperation({
        summary: 'getByUrl',
        description: 'Returns team member by URL',
    })
    @ApiOkResponse({
        type: TeamMemberDto,
    })
    async getByUrl(@Param('url') url: string): Promise<TeamMemberDto> {
        const entity = await this.repository.getByUrl(url);

        if (!entity.isPublished) {
            throw new TeamsMemberNotFoundException();
        }

        return this.mapper.entityToDTO(entity);
    }
}
