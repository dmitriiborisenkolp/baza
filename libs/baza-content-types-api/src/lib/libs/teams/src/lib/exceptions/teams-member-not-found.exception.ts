import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypesTeamsErrorCodes, bazaContentTypesTeamsErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class TeamsMemberNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesTeamsErrorCodes.BazaContentTypesTeamNotFound,
            bazaContentTypesTeamsErrorCodesI18n[BazaContentTypesTeamsErrorCodes.BazaContentTypesTeamNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
