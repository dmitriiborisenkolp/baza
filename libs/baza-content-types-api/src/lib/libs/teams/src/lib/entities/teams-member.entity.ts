import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { bazaSeoDefault, BazaSeoDto, BazaSocialNetworksDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { BazaContentDto, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';

@Entity({
    name: 'baza_content_types_team_member_entity',
})
export class TeamsMemberEntity implements CrudSortableEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    sortOrder: number;

    @Column()
    isPublished: boolean;

    @Column()
    name: string;

    @Column({
        nullable: true,
    })
    url: string;

    @Column({
        nullable: true,
    })
    title?: string;

    @Column({
        type: 'jsonb',
        default: emptyBazaContentDto(),
    })
    bio: BazaContentDto;

    @Column({
        nullable: true,
    })
    fullImageS3Key?: string;

    @Column({
        nullable: true,
    })
    previewImageS3Key?: string;

    @Column({
        type: 'jsonb',
        default: [],
    })
    networks: BazaSocialNetworksDto;

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto;
}
