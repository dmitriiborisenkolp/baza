import { Injectable } from "@nestjs/common";
import { TeamsMemberEntity } from '../entities/teams-member.entity';
import { TeamMemberDto } from '@scaliolabs/baza-content-types-shared';
import { AwsService } from '@scaliolabs/baza-core-api';

@Injectable()
export class TeamsMemberMapper {
    constructor(
        private readonly aws: AwsService,
    ) {}

    async entityToDTO(input: TeamsMemberEntity): Promise<TeamMemberDto> {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            isPublished: input.isPublished,
            fullImageUrl: input.fullImageS3Key
                ? await this.aws.presignedUrl({
                    s3ObjectId: input.fullImageS3Key,
                })
                : undefined,
            previewImageUrl: input.previewImageS3Key
                ? await this.aws.presignedUrl({
                    s3ObjectId: input.previewImageS3Key,
                })
                : undefined,
            url: input.url,
            name: input.name,
            title: input.title,
            bio: input.bio,
            networks: input.networks,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<TeamsMemberEntity>): Promise<Array<TeamMemberDto>> {
        const result: Array<TeamMemberDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
