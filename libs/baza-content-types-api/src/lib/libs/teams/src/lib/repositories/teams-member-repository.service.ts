import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { TeamsMemberEntity } from '../entities/teams-member.entity';
import { TeamsMemberNotFoundException } from '../exceptions/teams-member-not-found.exception';

@Injectable()
export class TeamsMemberRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<TeamsMemberEntity> {
        return this.connection.getRepository(TeamsMemberEntity);
    }

    async save(entities: Array<TeamsMemberEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<TeamsMemberEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<TeamsMemberEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
        });
    }

    async getById(id: number): Promise<TeamsMemberEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new TeamsMemberNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<TeamsMemberEntity> {
        const entity = await this.repository.findOne({
            where: [{
                url,
            }],
        });

        if (! entity) {
            throw new TeamsMemberNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<TeamsMemberEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'ASC',
            },
        });
    }

    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return result.max || 0;
    }
}
