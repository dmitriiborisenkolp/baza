import { Injectable } from '@nestjs/common';
import { emptyBazaContentDto, TeamsMemberCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { TeamsMembersCmsService } from '../../services/teams-members-cms.service';
import { BazaContentTypesTeamsFixtures } from '../baza-content-types-teams.fixtures';

export const E2E_BAZA_CONTENT_TYPES_TEAMS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    request: TeamsMemberCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            name: 'John Doe',
            title: 'Senior Developer',
            bio: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'john-doe',
            },
            networks: [],
            isPublished: true,
        },
    },
    {
        $refId: 2,
        request: {
            name: 'John Smith',
            title: 'Junior Web Developer',
            bio: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'john-smith',
            },
            networks: [],
            isPublished: false,
        },
    },
    {
        $refId: 3,
        request: {
            name: 'Robert Smith',
            title: 'HR',
            bio: emptyBazaContentDto(),
            seo: bazaSeoDefault(),
            networks: [],
            isPublished: true,
        },
    },
];

@Injectable()
export class BazaContentTypeTeamsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: TeamsMembersCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_TEAMS_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.request);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesTeamsFixtures>([{
    fixture: BazaContentTypesTeamsFixtures.BazaContentTypesTeams,
    provider: BazaContentTypeTeamsFixture,
}]);
