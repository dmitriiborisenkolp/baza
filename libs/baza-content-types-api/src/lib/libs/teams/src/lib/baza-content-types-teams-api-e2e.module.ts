import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypesTeamsApiModule } from './baza-content-types-teams-api.module';
import { BazaContentTypeTeamsFixture } from './integration-tests/fixtures/baza-content-type-teams.fixture';

const E2E_FIXTURES = [
    BazaContentTypeTeamsFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypesTeamsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypesTeamsApiE2eModule {}
