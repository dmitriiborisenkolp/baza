import 'reflect-metadata';
import { TeamsMembersCmsNodeAccess, TeamsMembersNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaContentTypesTeamsErrorCodes, TeamsMemberListResponse } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesTeamsFixtures } from '../baza-content-types-teams.fixtures';

describe('@scaliolabs/baza-content-types/teams/002-baza-content-types-teams-members.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new TeamsMembersNodeAccess(http);
    const dataAccessCms = new TeamsMembersCmsNodeAccess(http);

    let ID: number;
    let ID_UNPUBLISHED: number;

    let CMS_LIST_RESPONSE: TeamsMemberListResponse;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesTeamsFixtures.BazaContentTypesTeams],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        CMS_LIST_RESPONSE = await dataAccessCms.list({});
        ID_UNPUBLISHED = CMS_LIST_RESPONSE.items.find((e) => !e.isPublished).id;
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will successfully returns list of published team members', async () => {
        const response = await dataAccess.getAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);
        expect(response[0].name).toBe('John Doe');
        expect(response[1].name).toBe('Robert Smith');

        ID = response[0].id;
    });

    it('will successfully returns published team member by id', async () => {
        const response = await dataAccess.getById(ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.name).toBe('John Doe');
    });

    it('will successfully returns published team member by url', async () => {
        const response = await dataAccess.getByUrl('john-doe');

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.name).toBe('John Doe');
    });

    it('will not returns unpublished team member by id', async () => {
        const response: BazaError = (await dataAccess.getById(ID_UNPUBLISHED)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesTeamsErrorCodes.BazaContentTypesTeamNotFound);
    });

    it('will not returns unpublished team member by url', async () => {
        const response: BazaError = (await dataAccess.getByUrl('john-smith')) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesTeamsErrorCodes.BazaContentTypesTeamNotFound);
    });
});
