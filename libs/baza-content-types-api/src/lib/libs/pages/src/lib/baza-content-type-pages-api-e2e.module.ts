import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypePagesApiModule } from './baza-content-type-pages-api.module';
import { BazaContentTypePageFixture } from './integration-tests/fixtures/baza-content-type-pages.fixture';

const E2E_FIXTURES = [
    BazaContentTypePageFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypePagesApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypePagesApiE2eModule {}
