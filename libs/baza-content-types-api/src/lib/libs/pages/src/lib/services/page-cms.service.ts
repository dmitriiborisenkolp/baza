import { Injectable } from '@nestjs/common';
import { PageCmsMapper } from '../mappers/page-cms.mapper';
import { PAGE_RELATIONS, PageRepository } from '../repositories/page.repository';
import { BAZA_CONTENT_TYPES_CONFIGURATION, PageCmsDto, PagesCmsCreateRequest, PagesCmsDeleteRequest, PagesCmsEntityBody, PagesCmsListRequest, PagesCmsListResponse, PagesCmsUpdateRequest } from '@scaliolabs/baza-content-types-shared';
import { PageEntity } from '../entities/page.entity';
import { CategoryRepository } from '../../../../categories/src';
import { CrudService } from '@scaliolabs/baza-core-api';
import { BazaContentTypePagesListIsImmutableException } from '../exceptions/baza-content-type-pages-list-is-immutable.exception';

@Injectable()
export class PageCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: PageCmsMapper,
        private readonly repository: PageRepository,
        private readonly categoryRepository: CategoryRepository,
    ) {}

    async create(request: PagesCmsCreateRequest, updateId?: number): Promise<PageEntity> {
        if (! BAZA_CONTENT_TYPES_CONFIGURATION.configs.pages.allowModifyPagesList) {
            throw new BazaContentTypePagesListIsImmutableException();
        }

        const entity = new PageEntity();

        await this.populate(entity, request);
        entity.pageUniqueUpdateId = updateId;
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: PagesCmsUpdateRequest, updateId?: number): Promise<PageEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        entity.pageUniqueUpdateId = updateId;
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: PagesCmsDeleteRequest): Promise<void> {
        if (! BAZA_CONTENT_TYPES_CONFIGURATION.configs.pages.allowModifyPagesList) {
            throw new BazaContentTypePagesListIsImmutableException();
        }

        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);
    }

    async list(request: PagesCmsListRequest): Promise<PagesCmsListResponse> {
        return this.crud.find<PageEntity, PageCmsDto>({
            request,
            entity: PageEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                relations: PAGE_RELATIONS,
                order: {
                    id: 'ASC',
                }
            },
        });
    }

    async populate(target: PageEntity, entityBody: PagesCmsEntityBody): Promise<void> {
        target.pageUniqueSid = entityBody.pageUniqueSid;
        target.title = entityBody.title;
        target.isPublished = entityBody.isPublished;
        target.url = entityBody.seo
            ? entityBody.seo.urn
            : undefined;
        target.primaryCategory = entityBody.primaryCategoryId
            ? await this.categoryRepository.getById(entityBody.primaryCategoryId)
            : null;
        target.additionalCategories = (Array.isArray(entityBody.additionalCategoryIds) && entityBody.additionalCategoryIds.length > 0)
            ? await this.categoryRepository.findByIds(entityBody.additionalCategoryIds)
            : [];
        target.headerImageAwsS3Key = entityBody.headerImageAwsS3Key;
        target.contents = entityBody.contents;
        target.seo = entityBody.seo;
    }
}

