import { Injectable } from "@nestjs/common";
import { Connection, Repository } from 'typeorm';
import { PageEntity } from '../entities/page.entity';
import { BazaContentTypePageNotFoundException } from '../exceptions/baza-content-type-page-not-found.exception';

export const PAGE_RELATIONS = [
    'primaryCategory',
    'additionalCategories',
];

@Injectable()
export class PageRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<PageEntity> {
        return this.connection.getRepository(PageEntity);
    }

    async save(entities: Array<PageEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<PageEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<PageEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: PAGE_RELATIONS,
        });
    }

    async findByPageUniqueSid(pageUniqueSid: string): Promise<PageEntity | undefined> {
        return this.repository.findOne({
            where: [{
                pageUniqueSid,
            }],
            relations: PAGE_RELATIONS,
        });
    }

    async getById(id: number): Promise<PageEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BazaContentTypePageNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<PageEntity> {
        const entity = await this.repository.findOne({
            where: [{
                url,
            }],
            relations: PAGE_RELATIONS,
        });

        if (! entity) {
            throw new BazaContentTypePageNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<PageEntity>> {
        return this.repository.find({
            order: {
                id: 'ASC',
            },
            relations: PAGE_RELATIONS,
        });
    }
}
