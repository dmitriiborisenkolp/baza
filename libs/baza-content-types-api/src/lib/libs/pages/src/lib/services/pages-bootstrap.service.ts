import { Injectable } from "@nestjs/common";
import { PageCmsService } from './page-cms.service';
import { BAZA_CONTENT_TYPES_CONFIGURATION } from '@scaliolabs/baza-content-types-shared';
import { PageRepository } from '../repositories/page.repository';
import { BazaContentTypePageNoPageUniqueSidAvailableException } from '../exceptions/baza-content-type-page-no-page-unique-sid-available.exception';
import { BazaLogger } from '@scaliolabs/baza-core-api';

@Injectable()
export class PagesBootstrapService {
    constructor(
        private readonly logger: BazaLogger,
        private readonly repository: PageRepository,
        private readonly cmsService: PageCmsService,
    ) {}

    async bootstrap(): Promise<void> {
        if (! BAZA_CONTENT_TYPES_CONFIGURATION.configs.pages.bootstrapPages.length) {
            return;
        }

        for (const bootstrapRequest of BAZA_CONTENT_TYPES_CONFIGURATION.configs.pages.bootstrapPages) {
            if (! bootstrapRequest.$sid) {
                throw new BazaContentTypePageNoPageUniqueSidAvailableException();
            }

            const found = await this.repository.findByPageUniqueSid(bootstrapRequest.$sid);

            if (! found) {
                this.logger.log(`[PagesBootstrapService] Create new page: ${bootstrapRequest.$sid} – ${bootstrapRequest.request.title}`)

                await this.cmsService.create({
                    ...bootstrapRequest.request,
                    pageUniqueSid: bootstrapRequest.$sid,
                }, bootstrapRequest.$updateId);
            } else {
                if (found.pageUniqueUpdateId !== bootstrapRequest.$updateId) {
                    this.logger.log(`[PagesBootstrapService] Update page: ${bootstrapRequest.$sid} #${bootstrapRequest.$updateId} – ${bootstrapRequest.request.title}`)

                    await this.cmsService.update({
                        id: found.id,
                        ...bootstrapRequest.request,
                        pageUniqueSid: bootstrapRequest.$sid,
                    }, bootstrapRequest.$updateId);
                }
            }
        }
    }
}
