import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaContentTypesPagesErrorCodes, bazaContentTypesPagesErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';

export class BazaContentTypePagesListIsImmutableException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesPagesErrorCodes.BazaContentTypesPagesListIsImmutable,
            bazaContentTypesPagesErrorCodesI18n[BazaContentTypesPagesErrorCodes.BazaContentTypesPagesListIsImmutable],
            HttpStatus.BAD_REQUEST,
        );
    }
}
