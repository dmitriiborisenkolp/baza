import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaContentTypesPagesErrorCodes, bazaContentTypesPagesErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';

export class BazaContentTypePageNoPageUniqueSidAvailableException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesPagesErrorCodes.BazaContentTypesPagesNoUniqueSidAvailable,
            bazaContentTypesPagesErrorCodesI18n[BazaContentTypesPagesErrorCodes.BazaContentTypesPagesNoUniqueSidAvailable],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
