import { Injectable } from '@nestjs/common';
import { emptyBazaContentDto, PagesCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { PageCmsService } from '../../services/page-cms.service';
import { BazaContentTypePageFixtures } from '../baza-content-type-page-fixtures';

export const E2E_BAZA_CONTENT_TYPES_PAGE_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    request: PagesCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            isPublished: true,
            title: 'Page 1',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'page-1',
            },
        },
    },
    {
        $refId: 2,
        request: {
            isPublished: false,
            title: 'Page 2',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'page-2',
            },
        },
    },
    {
        $refId: 3,
        request: {
            isPublished: true,
            title: 'Page 3',
            contents: emptyBazaContentDto(),
            seo: {
                ...bazaSeoDefault(),
                urn: 'page-3',
            },
        },
    },
];

@Injectable()
export class BazaContentTypePageFixture implements BazaE2eFixture {
    constructor(
        private readonly service: PageCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_PAGE_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.request);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypePageFixtures>([{
    fixture: BazaContentTypePageFixtures.BazaContentTypesPage,
    provider: BazaContentTypePageFixture,
}]);
