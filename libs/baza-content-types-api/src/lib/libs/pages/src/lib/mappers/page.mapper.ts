import { Injectable } from "@nestjs/common";
import { AwsService } from '@scaliolabs/baza-core-api';
import { PageEntity } from '../entities/page.entity';
import { PageDto } from '@scaliolabs/baza-content-types-shared';
import { CategoryListMapper } from '../../../../categories/src';

@Injectable()
export class PageMapper {
    constructor(
        private readonly aws: AwsService,
        private readonly categoryMapper: CategoryListMapper,
    ) {}

    async entityToDTO(input: PageEntity): Promise<PageDto> {
        return {
            id: input.id,
            primaryCategory: input.primaryCategory
                ? this.categoryMapper.entityToDTO(input.primaryCategory)
                : undefined,
            additionalCategories: this.categoryMapper.entitiesToDTOs(input.additionalCategories || []),
            title: input.title,
            url: input.url,
            headerImageUrl: input.headerImageAwsS3Key
                ? await this.aws.presignedUrl({
                    s3ObjectId: input.headerImageAwsS3Key,
                })
                : undefined,
            contents: input.contents,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<PageEntity>): Promise<Array<PageDto>> {
        const result: Array<PageDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
