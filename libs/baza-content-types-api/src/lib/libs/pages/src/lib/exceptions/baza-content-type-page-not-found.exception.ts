import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaContentTypesPagesErrorCodes, bazaContentTypesPagesErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';

export class BazaContentTypePageNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesPagesErrorCodes.BazaContentTypesPagesNotFound,
            bazaContentTypesPagesErrorCodesI18n[BazaContentTypesPagesErrorCodes.BazaContentTypesPagesNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
