import { Module, OnApplicationBootstrap } from '@nestjs/common';
import { PagesController } from './controllers/pages.controller';
import { PagesCmsController } from './controllers/pages-cms.controller';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { PageRepository } from './repositories/page.repository';
import { PageMapper } from './mappers/page.mapper';
import { PageCmsMapper } from './mappers/page-cms.mapper';
import { PageService } from './services/page.service';
import { PageCmsService } from './services/page-cms.service';
import { PagesBootstrapService } from './services/pages-bootstrap.service';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        PagesController,
        PagesCmsController,
    ],
    providers: [
        PageRepository,
        PageMapper,
        PageCmsMapper,
        PageService,
        PageCmsService,
        PagesBootstrapService,
    ],
    exports: [
        PageRepository,
        PageMapper,
        PageCmsMapper,
        PageService,
        PageCmsService,
    ],
})
export class BazaContentTypePagesApiModule implements OnApplicationBootstrap {
    constructor(
        private readonly bootstrap: PagesBootstrapService,
    ) {}

    async onApplicationBootstrap(): Promise<void> {
        await this.bootstrap.bootstrap();
    }
}
