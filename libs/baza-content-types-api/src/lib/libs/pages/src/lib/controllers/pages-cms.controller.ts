import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    PageCmsDto,
    PagesCmsCreateRequest,
    PagesCmsDeleteRequest,
    PagesCmsEndpoint,
    PagesCmsEndpointPaths,
    PagesCmsGetByIdRequest,
    PagesCmsListRequest,
    PagesCmsListResponse,
    PagesCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { PageCmsMapper } from '../mappers/page-cms.mapper';
import { PageRepository } from '../repositories/page.repository';
import { PageCmsService } from '../services/page-cms.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Pages)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesPages])
export class PagesCmsController implements PagesCmsEndpoint {
    constructor(
        private readonly mapper: PageCmsMapper,
        private readonly service: PageCmsService,
        private readonly repository: PageRepository,
    ) {}

    @Post(PagesCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: PageCmsDto,
    })
    async create(@Body() request: PagesCmsCreateRequest): Promise<PageCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(PagesCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: PageCmsDto,
    })
    async update(@Body() request: PagesCmsUpdateRequest): Promise<PageCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(PagesCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: PagesCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(PagesCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: PagesCmsListResponse,
    })
    async list(@Body() request: PagesCmsListRequest): Promise<PagesCmsListResponse> {
        return this.service.list(request);
    }

    @Post(PagesCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: PageCmsDto,
    })
    async getById(@Body() request: PagesCmsGetByIdRequest): Promise<PageCmsDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }
}
