import 'reflect-metadata';
import { PagesNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaContentTypesPagesErrorCodes } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypePageFixtures } from '../baza-content-type-page-fixtures';

describe('@scaliolabs/baza-content-types/pages/002-baza-content-types-pages.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new PagesNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypePageFixtures.BazaContentTypesPage],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('successfully returns page by url', async () => {
        const response = await dataAccess.get('page-1');

        expect(isBazaErrorResponse('page-1')).toBeFalsy();

        expect(response.title).toBe('Page 1');
        expect(response.url).toBe('page-1');
    });

    it('will not returns unpublished page by url', async () => {
        const response: BazaError = (await dataAccess.get('page-2')) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesPagesErrorCodes.BazaContentTypesPagesNotFound);
    });
});
