import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';
import { BazaContentDto, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypeCategoryEntity } from '../../../../categories/src';

@Entity({
    name: 'baza_content_types_page_entity',
})
export class PageEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    title: string;

    @Column()
    isPublished: boolean;

    @Column({
        nullable: true,
    })
    url?: string;

    @Column({
        nullable: true,
    })
    pageUniqueSid?: string;

    @Column({
        nullable: true,
    })
    pageUniqueUpdateId?: number;

    @ManyToOne(() => BazaContentTypeCategoryEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
    })
    primaryCategory: BazaContentTypeCategoryEntity;

    @ManyToMany(() => BazaContentTypeCategoryEntity)
    @JoinTable()
    additionalCategories: Array<BazaContentTypeCategoryEntity>;

    @Column({
        nullable: true,
    })
    headerImageAwsS3Key?: string;

    @Column({
        type: 'jsonb',
        default: emptyBazaContentDto(),
    })
    contents: BazaContentDto;

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto;
}
