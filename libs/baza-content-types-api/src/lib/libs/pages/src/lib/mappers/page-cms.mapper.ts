import { Injectable } from '@nestjs/common';
import { PageMapper } from './page.mapper';
import { PageEntity } from '../entities/page.entity';
import { PageCmsDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class PageCmsMapper {
    constructor(
        private readonly baseMapper: PageMapper,
    ) {}

    async entityToDTO(input: PageEntity): Promise<PageCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            isPublished: input.isPublished,
            headerImageAwsS3Key: input.headerImageAwsS3Key,
        };
    }

    async entitiesToDTOs(input: Array<PageEntity>): Promise<Array<PageCmsDto>> {
        const result: Array<PageCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
