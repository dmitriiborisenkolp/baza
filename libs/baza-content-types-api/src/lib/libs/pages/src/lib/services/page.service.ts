import { Injectable } from '@nestjs/common';
import { PageRepository } from '../repositories/page.repository';
import { PageMapper } from '../mappers/page.mapper';
import { PageEntity } from '../entities/page.entity';
import { BazaContentTypePageNotFoundException } from '../exceptions/baza-content-type-page-not-found.exception';

@Injectable()
export class PageService {
    constructor(
        private readonly mapper: PageMapper,
        private readonly repository: PageRepository,
    ) {}

    async getById(id: number): Promise<PageEntity> {
        const entity = await this.repository.getById(id);

        if (! entity.isPublished) {
            throw new BazaContentTypePageNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<PageEntity> {
        const entity = await this.repository.getByUrl(url);

        if (! entity.isPublished) {
            throw new BazaContentTypePageNotFoundException();
        }

        return entity;
    }
}
