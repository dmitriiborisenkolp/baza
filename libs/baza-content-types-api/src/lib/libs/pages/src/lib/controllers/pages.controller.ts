import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaContentTypesOpenApi, PageDto, PagesEndpoint, PagesEndpointPaths } from '@scaliolabs/baza-content-types-shared';
import { PageService } from '../services/page.service';
import { PageMapper } from '../mappers/page.mapper';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Pages)
export class PagesController implements PagesEndpoint {
    constructor(private readonly service: PageService, private readonly mapper: PageMapper) {}

    @Get(PagesEndpointPaths.get)
    @ApiOperation({
        summary: 'get',
        description: 'Returns page by url',
    })
    @ApiOkResponse({
        type: PageDto,
    })
    async get(@Param('url') url: string): Promise<PageDto> {
        const entity = await this.service.getByUrl(url);

        return this.mapper.entityToDTO(entity);
    }
}
