import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { bazaSeoDefault, BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';

@Entity({
    name: 'baza_content_types_testimonial_entity',
})
export class TestimonialEntity implements CrudSortableEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    sortOrder: number;

    @Column()
    isPublished: boolean;

    @Column({
        nullable: true,
    })
    url?: string;

    @Column()
    name: string;

    @Column({
        nullable: true,
    })
    imageS3AwsKey?: string;

    @Column({
        nullable: true,
    })
    company?: string;

    @Column({
        nullable: true,
    })
    title?: string;

    @Column({
        nullable: true,
    })
    link?: string;

    @Column({
        nullable: true,
    })
    location?: string;

    @Column({
        nullable: true,
    })
    email?: string;

    @Column()
    quote: string;

    @Column()
    preview: string;

    @Column({
        type: 'jsonb',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto;
}
