import { Injectable } from '@nestjs/common';
import { TestimonialsCmsCreateRequest } from '@scaliolabs/baza-content-types-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { TestimonialsCmsService } from '../../services/testimonials-cms.service';
import { BazaContentTypesTestimonialsFixtures } from '../baza-content-types-testimonials-fixtures';

export const E2E_BAZA_CONTENT_TYPES_TESTIMONIALS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    request: TestimonialsCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            name: 'John Doe',
            title: 'Senior Developer',
            seo: {
                ...bazaSeoDefault(),
                urn: 'john-doe',
            },
            isPublished: true,
            quote: 'Example Quote',
            preview: 'Example Preview',
        },
    },
    {
        $refId: 2,
        request: {
            name: 'John Smith',
            title: 'Junior Web Developer',
            seo: {
                ...bazaSeoDefault(),
                urn: 'john-smith',
            },
            isPublished: false,
            quote: 'Example Quote',
            preview: 'Example Preview',
        },
    },
    {
        $refId: 3,
        request: {
            name: 'Robert Smith',
            title: 'HR',
            seo: bazaSeoDefault(),
            isPublished: true,
            quote: 'Example Quote',
            preview: 'Example Preview',
        },
    },
];

@Injectable()
export class BazaContentTypeTestimonialsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: TestimonialsCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_BAZA_CONTENT_TYPES_TESTIMONIALS_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.request);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaContentTypesTestimonialsFixtures>([{
    fixture: BazaContentTypesTestimonialsFixtures.BazaContentTypesTestimonials,
    provider: BazaContentTypeTestimonialsFixture,
}]);
