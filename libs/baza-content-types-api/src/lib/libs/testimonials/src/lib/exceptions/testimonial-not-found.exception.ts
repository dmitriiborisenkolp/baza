import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaContentTypesTestimonialsErrorCodes, bazaContentTypesTestimonialsErrorCodesI18n } from '@scaliolabs/baza-content-types-shared';
import { HttpStatus } from '@nestjs/common';

export class TestimonialNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaContentTypesTestimonialsErrorCodes.BazaContentTypesTestimonialsNotFound,
            bazaContentTypesTestimonialsErrorCodesI18n[BazaContentTypesTestimonialsErrorCodes.BazaContentTypesTestimonialsNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
