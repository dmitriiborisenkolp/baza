import { Injectable } from '@nestjs/common';
import { TestimonialsRepository } from '../repositories/testimonials.repository';
import { TestimonialsCmsUpdateRequest, TestimonialsCmsCreateRequest, TestimonialsCmsEntityBody, TestimonialsCmsDeleteRequest, TestimonialsCmsListRequest, TestimonialsCmsListResponse, TestimonialCmsDto, TestimonialsCmsSetSortOrderRequest, TestimonialsCmsSetSortOrderResponse } from '@scaliolabs/baza-content-types-shared';
import { TestimonialEntity } from '../entities/testimonial.entity';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { TestimonialCmsMapper } from '../mappers/testimonial-cms.mapper';

@Injectable()
export class TestimonialsCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: TestimonialsRepository,
        private readonly mapper: TestimonialCmsMapper,
    ) {}

    async create(request: TestimonialsCmsCreateRequest): Promise<TestimonialEntity> {
        const entity = new TestimonialEntity();

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: TestimonialsCmsUpdateRequest): Promise<TestimonialEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: TestimonialsCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);

        const entities = await this.repository.findAll();

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async list(request: TestimonialsCmsListRequest): Promise<TestimonialsCmsListResponse> {
        return this.crud.find<TestimonialEntity, TestimonialCmsDto>({
            entity: TestimonialEntity,
            request,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'ASC',
                },
            },
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: TestimonialsCmsSetSortOrderRequest): Promise<TestimonialsCmsSetSortOrderResponse> {
        const entities = await this.repository.findAll();

        const response = await this.crudSort.setSortOrder<TestimonialEntity, TestimonialCmsDto>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: entities,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });

        await this.repository.save(entities);

        return response;
    }

    private async populate(target: TestimonialEntity, entityBody: TestimonialsCmsEntityBody): Promise<void> {
        if (! target.id) {
            target.sortOrder = await this.repository.maxSortOrder() + 1;
        }

        target.isPublished = entityBody.isPublished;
        target.url = entityBody.seo
            ? entityBody.seo.urn
            : undefined;
        target.name = entityBody.name;
        target.imageS3AwsKey = entityBody.imageS3AwsKey;
        target.company = entityBody.company;
        target.title = entityBody.title;
        target.link = entityBody.link;
        target.location = entityBody.location;
        target.email = entityBody.email;
        target.quote = entityBody.quote;
        target.preview = entityBody.preview;
        target.seo = entityBody.seo;
    }
}

