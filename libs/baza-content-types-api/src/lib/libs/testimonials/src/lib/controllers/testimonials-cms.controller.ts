import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypeAcl,
    BazaContentTypesCmsOpenApi,
    TestimonialCmsDto,
    TestimonialsCmsCreateRequest,
    TestimonialsCmsDeleteRequest,
    TestimonialsCmsEndpoint,
    TestimonialsCmsEndpointPaths,
    TestimonialsCmsGetByIdRequest,
    TestimonialsCmsListRequest,
    TestimonialsCmsListResponse,
    TestimonialsCmsSetSortOrderRequest,
    TestimonialsCmsSetSortOrderResponse,
    TestimonialsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { TestimonialsRepository } from '../repositories/testimonials.repository';
import { TestimonialsCmsService } from '../services/testimonials-cms.service';
import { TestimonialCmsMapper } from '../mappers/testimonial-cms.mapper';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaContentTypesCmsOpenApi.Testimonials)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaContentTypeAcl.BazaContentTypesTestimonials])
export class TestimonialsCmsController implements TestimonialsCmsEndpoint {
    constructor(
        private readonly service: TestimonialsCmsService,
        private readonly mapper: TestimonialCmsMapper,
        private readonly repository: TestimonialsRepository,
    ) {}

    @Post(TestimonialsCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: TestimonialCmsDto,
    })
    async create(@Body() request: TestimonialsCmsCreateRequest): Promise<TestimonialCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TestimonialsCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: TestimonialCmsDto,
    })
    async update(@Body() request: TestimonialsCmsUpdateRequest): Promise<TestimonialCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TestimonialsCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: TestimonialsCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(TestimonialsCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: TestimonialsCmsListResponse,
    })
    async list(@Body() request: TestimonialsCmsListRequest): Promise<TestimonialsCmsListResponse> {
        return this.service.list(request);
    }

    @Post(TestimonialsCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: TestimonialCmsDto,
    })
    async getById(@Body() request: TestimonialsCmsGetByIdRequest): Promise<TestimonialCmsDto> {
        const entity = await this.repository.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(TestimonialsCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: TestimonialsCmsSetSortOrderResponse,
    })
    async setSortOrder(@Body() request: TestimonialsCmsSetSortOrderRequest): Promise<TestimonialsCmsSetSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
