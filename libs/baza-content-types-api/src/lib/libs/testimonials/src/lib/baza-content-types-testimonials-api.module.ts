import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { TestimonialsRepository } from './repositories/testimonials.repository';
import { TestimonialMapper } from './mappers/testimonial.mapper';
import { TestimonialCmsMapper } from './mappers/testimonial-cms.mapper';
import { TestimonialsService } from './services/testimonials.service';
import { TestimonialsCmsService } from './services/testimonials-cms.service';
import { TestimonialsController } from './controllers/testimonials.controller';
import { TestimonialsCmsController } from './controllers/testimonials-cms.controller';

@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        TestimonialsController,
        TestimonialsCmsController,
    ],
    providers: [
        TestimonialsRepository,
        TestimonialMapper,
        TestimonialCmsMapper,
        TestimonialsService,
        TestimonialsCmsService,
    ],
    exports: [
        TestimonialsRepository,
        TestimonialMapper,
        TestimonialCmsMapper,
        TestimonialsService,
        TestimonialsCmsService,
    ],
})
export class BazaContentTypesTestimonialsApiModule {}
