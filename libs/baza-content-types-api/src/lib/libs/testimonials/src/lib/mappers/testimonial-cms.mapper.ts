import { Injectable } from '@nestjs/common';
import { TestimonialMapper } from './testimonial.mapper';
import { TestimonialEntity } from '../entities/testimonial.entity';
import { TestimonialCmsDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class TestimonialCmsMapper {
    constructor(
        private readonly baseMapper: TestimonialMapper,
    ) {}

    async entityToDTO(input: TestimonialEntity): Promise<TestimonialCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            isPublished: input.isPublished,
            imageS3AwsKey: input.imageS3AwsKey,
        };
    }

    async entitiesToDTOs(input: Array<TestimonialEntity>): Promise<Array<TestimonialCmsDto>> {
        const result: Array<TestimonialCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
