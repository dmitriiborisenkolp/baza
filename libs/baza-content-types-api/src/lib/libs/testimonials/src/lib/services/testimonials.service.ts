import { Injectable } from "@nestjs/common";
import { TestimonialsRepository } from '../repositories/testimonials.repository';
import { CrudService } from '@scaliolabs/baza-core-api';
import { TestimonialDto, TestimonialsListRequest, TestimonialsListResponse } from '@scaliolabs/baza-content-types-shared';
import { TestimonialMapper } from '../mappers/testimonial.mapper';
import { TestimonialEntity } from '../entities/testimonial.entity';
import { TestimonialNotFoundException } from '../exceptions/testimonial-not-found.exception';

@Injectable()
export class TestimonialsService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: TestimonialMapper,
        private readonly repository: TestimonialsRepository,
    ) {}

    async list(request: TestimonialsListRequest): Promise<TestimonialsListResponse> {
        return this.crud.find<TestimonialEntity, TestimonialDto>({
            entity: TestimonialEntity,
            request,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'ASC',
                },
                where: [{
                    isPublished: true,
                }],
            },
            withMaxSortOrder: true,
        });
    }

    async getAll(): Promise<Array<TestimonialEntity>> {
        return this.repository.findAllPublished();
    }

    async getById(id: number): Promise<TestimonialEntity> {
        const entity = await this.repository.getById(id);

        if (! entity.isPublished) {
            throw new TestimonialNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<TestimonialEntity> {
        const entity = await this.repository.getByUrl(url);

        if (! entity.isPublished) {
            throw new TestimonialNotFoundException();
        }

        return entity;
    }
}
