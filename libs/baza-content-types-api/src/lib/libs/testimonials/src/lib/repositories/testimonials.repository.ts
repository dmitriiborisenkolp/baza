import { Injectable } from "@nestjs/common";
import { Connection, Repository } from 'typeorm';
import { TestimonialEntity } from "../entities/testimonial.entity";
import { TestimonialNotFoundException } from '../exceptions/testimonial-not-found.exception';

@Injectable()
export class TestimonialsRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<TestimonialEntity> {
        return this.connection.getRepository(TestimonialEntity);
    }

    async save(entities: Array<TestimonialEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<TestimonialEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<TestimonialEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
        });
    }

    async getById(id: number): Promise<TestimonialEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new TestimonialNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<TestimonialEntity> {
        const entity = await this.repository.findOne({
            where: [{
                url,
            }],
        });

        if (! entity) {
            throw new TestimonialNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<TestimonialEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'ASC',
            },
        });
    }

    async findAllPublished(): Promise<Array<TestimonialEntity>> {
        return this.repository.find({
            order: {
                sortOrder: 'ASC',
            },
            where: [{
                isPublished: true,
            }],
        });
    }

    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return result.max || 0;
    }
}
