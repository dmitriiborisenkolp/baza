import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaContentTypesOpenApi,
    TestimonialDto,
    TestimonialsEndpoint,
    TestimonialsEndpointPaths,
    TestimonialsListRequest,
    TestimonialsListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { TestimonialsService } from '../services/testimonials.service';
import { TestimonialMapper } from '../mappers/testimonial.mapper';

@Controller()
@ApiTags(BazaContentTypesOpenApi.Testimonials)
export class TestimonialsController implements TestimonialsEndpoint {
    constructor(private readonly service: TestimonialsService, private readonly mapper: TestimonialMapper) {}

    @Get(TestimonialsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: TestimonialsListResponse,
    })
    async list(@Query() request: TestimonialsListRequest): Promise<TestimonialsListResponse> {
        return this.service.list(request);
    }

    @Get(TestimonialsEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
    })
    @ApiOkResponse({
        type: TestimonialDto,
        isArray: true,
    })
    async getAll(): Promise<Array<TestimonialDto>> {
        const entities = await this.service.getAll();

        return this.mapper.entitiesToDTOs(entities);
    }

    @Get(TestimonialsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: TestimonialDto,
    })
    async getById(@Param('id', ParseIntPipe) id: number): Promise<TestimonialDto> {
        const entity = await this.service.getById(id);

        return this.mapper.entityToDTO(entity);
    }

    @Get(TestimonialsEndpointPaths.getByUrl)
    @ApiOperation({
        summary: 'getByUrl',
    })
    @ApiOkResponse({
        type: TestimonialDto,
    })
    async getByUrl(@Param('url') url: string): Promise<TestimonialDto> {
        const entity = await this.service.getByUrl(url);

        return this.mapper.entityToDTO(entity);
    }
}
