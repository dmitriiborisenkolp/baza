import 'reflect-metadata';
import { TestimonialsCmsNodeAccess, TestimonialsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaContentTypesTestimonialsErrorCodes, TestimonialsCmsListResponse } from '@scaliolabs/baza-content-types-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesTestimonialsFixtures } from '../baza-content-types-testimonials-fixtures';

describe('@scaliolabs/baza-content-types/testimonials/002-baza-content-types-testimonials.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new TestimonialsNodeAccess(http);
    const dataAccessCms = new TestimonialsCmsNodeAccess(http);

    let ID: number;
    let ID_UNPUBLISHED: number;

    let CMS_LIST_RESPONSE: TestimonialsCmsListResponse;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaContentTypesTestimonialsFixtures.BazaContentTypesTestimonials],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        CMS_LIST_RESPONSE = await dataAccessCms.list({});
        ID_UNPUBLISHED = CMS_LIST_RESPONSE.items.find((e) => !e.isPublished).id;
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will successfully returns list of all published testimonials', async () => {
        const response = await dataAccess.getAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);
        expect(response[0].name).toBe('John Doe');
        expect(response[1].name).toBe('Robert Smith');

        ID = response[0].id;
    });

    it('will successfully returns list of published testimonials', async () => {
        const response = await dataAccess.list({
            index: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].name).toBe('John Doe');
        expect(response.items[1].name).toBe('Robert Smith');
    });

    it('will successfully returns published testimonial by id', async () => {
        const response = await dataAccess.getById(ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.name).toBe('John Doe');
    });

    it('will successfully returns published testimonial by url', async () => {
        const response = await dataAccess.getByUrl('john-doe');

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.name).toBe('John Doe');
    });

    it('will not returns unpublished testimonial by id', async () => {
        const response: BazaError = (await dataAccess.getById(ID_UNPUBLISHED)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesTestimonialsErrorCodes.BazaContentTypesTestimonialsNotFound);
    });

    it('will not returns unpublished testimonials by url', async () => {
        const response: BazaError = (await dataAccess.getByUrl('john-smith')) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaContentTypesTestimonialsErrorCodes.BazaContentTypesTestimonialsNotFound);
    });
});
