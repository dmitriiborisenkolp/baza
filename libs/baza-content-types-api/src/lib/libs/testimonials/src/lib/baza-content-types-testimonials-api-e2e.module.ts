import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaContentTypesTestimonialsApiModule } from './baza-content-types-testimonials-api.module';
import { BazaContentTypeTestimonialsFixture } from './integration-tests/fixtures/baza-content-type-testimonials.fixture';

const E2E_FIXTURES = [
    BazaContentTypeTestimonialsFixture,
];

@Global()
@Module({
    imports: [
        forwardRef(() => BazaContentTypesTestimonialsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaContentTypesTestimonialsApiE2eModule {}
