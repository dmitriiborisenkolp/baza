import { AwsService } from '@scaliolabs/baza-core-api';
import { TestimonialEntity } from '../entities/testimonial.entity';
import { TestimonialDto } from '@scaliolabs/baza-content-types-shared';
import { Injectable } from '@nestjs/common';

@Injectable()
export class TestimonialMapper {
    constructor(
        private readonly aws: AwsService,
    ) {}

    async entityToDTO(input: TestimonialEntity): Promise<TestimonialDto> {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            url: input.seo.urn,
            name: input.name,
            title: input.title,
            company: input.company,
            link: input.link,
            location: input.location,
            email: input.email,
            imageUrl: input.imageS3AwsKey
                ? await this.aws.presignedUrl({
                    s3ObjectId: input.imageS3AwsKey,
                })
                : undefined,
            quote: input.quote,
            preview: input.preview,
            seo: input.seo,
        };
    }

    async entitiesToDTOs(input: Array<TestimonialEntity>): Promise<Array<TestimonialDto>> {
        const result: Array<TestimonialDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
