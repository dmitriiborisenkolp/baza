import 'reflect-metadata';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { TestimonialsCmsNodeAccess } from '@scaliolabs/baza-content-types-node-access';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';

describe('@scaliolabs/baza-content-types/testimonials/001-baza-content-types-testimonials-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new TestimonialsCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new testimonial', async () => {
        const response = await dataAccessCms.create({
            name: 'John Doe',
            isPublished: true,
            seo: bazaSeoDefault(),
            preview: 'Example Preview',
            quote: 'Example Quote',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new testimonial in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].name).toBe('John Doe');
        expect(listResponse.items[0].sortOrder).toBe(1);
    });

    it('will successfully create new testimonial without title', async () => {
        const response = await dataAccessCms.create({
            name: 'John Smith',
            isPublished: true,
            seo: bazaSeoDefault(),
            preview: 'Example Preview',
            quote: 'Example Quote',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ID = response.id;
    });

    it('will successfully returns testimonial by id', async () => {
        const response = await dataAccessCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.name).toBe('John Smith');
    });

    it('will successfully update testimonial', async () => {
        const response = await dataAccessCms.update({
            id: ID,
            name: 'John Smith *',
            isPublished: true,
            seo: bazaSeoDefault(),
            preview: 'Example Preview',
            quote: 'Example Quote',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updates in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].name).toBe('John Doe');
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].sortOrder).toBe(2);
        expect(listResponse.items[1].name).toBe('John Smith *');

        ID = listResponse.items[0].id;
    });

    it('will successfully update sort order of testimonial', async () => {
        const response = await dataAccessCms.setSortOrder({
            id: ID,
            setSortOrder: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updated sort order in list', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].name).toBe('John Smith *');
        expect(listResponse.items[1].id).toBe(ID);
        expect(listResponse.items[1].name).toBe('John Doe');
        expect(listResponse.items[1].sortOrder).toBe(2);

        ID = listResponse.items[0].id;
    });

    it('will successfully delete testimonial', async () => {
        const response = await dataAccessCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted testimonial in list response', async () => {
        const listResponse = await dataAccessCms.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].id).not.toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(1);
        expect(listResponse.items[0].name).toBe('John Doe');

        ID = listResponse.items[0].id;
    });
});
