import { registerMailTemplates } from '@scaliolabs/baza-core-api';

/**
 * Mail Templates of baza-content-types package
 */
export enum BazaContentTypesMail {
    BazaContentTypesContactClientRequest = 'BazaContentTypesContactClientRequest',
    BazaContentTypesContactClientResponse = 'BazaContentTypesContactClientResponse',
    BazaContentTypesNewslettersInquiryEmail = 'BazaContentTypesNewslettersInquiryEmail',
}

/**
 * Tags uses for Mail Templates of baza-content-types package
 */
export enum BazaContentTypesMailTags {
    BazaContentTypesContacts = 'Content Types – Contacts',
    BazaContentTypesNewsletters = 'Content Types – Newsletters',
}

/**
 * Registers Mail Templates of baza-content-types package
 * @see BazaContentTypesMail
 * @see BazaContentTypesMailTags
 */
registerMailTemplates([
    {
        name: BazaContentTypesMail.BazaContentTypesContactClientRequest,
        title: 'Contact Us Client Request',
        description: 'Email notification sent to CMS Admin from Contact Us Form',
        tag: BazaContentTypesMailTags.BazaContentTypesContacts,
        filePath: 'baza-content-types/contacts/contact-client-request/contact-client-request.{type}.hbs',
        variables: [
            {
                field: 'name',
                description: 'Name (from Contact Us Form)',
                optional: true,
            },
            {
                field: 'email',
                description: 'Email Address (from Contact Us Form)',
                optional: false,
            },
            {
                field: 'subject',
                description: 'Subject (from Contact Us Form)',
                optional: false,
            },
            {
                field: 'rawMessage',
                description: 'Message, unsecure raw output (from Contact Us Form)',
                optional: false,
            },
            {
                field: 'htmlMessage',
                description: 'Message, HTML-safe output (from Contact Us Form)',
                optional: false,
            },
            {
                field: 'contactUs',
                description: 'Email Address used for Contact Us communications',
                optional: false,
            },
        ],
    },
    {
        name: BazaContentTypesMail.BazaContentTypesContactClientResponse,
        title: 'Contact Us Client Response',
        description: 'Email notification sent to original sender about receiving Contact Us request',
        tag: BazaContentTypesMailTags.BazaContentTypesContacts,
        filePath: 'baza-content-types/contacts/contact-client-response/contact-client-response.{type}.hbs',
        variables: [
            {
                field: 'name',
                description: 'Name (from Contact Us Form)',
                optional: true,
            },
            {
                field: 'email',
                description: 'Email Address (from Contact Us Form)',
                optional: false,
            },
            {
                field: 'subject',
                description: 'Subject (from Contact Us Form)',
                optional: false,
            },
            {
                field: 'rawMessage',
                description: 'Message, unsecure raw output (from Contact Us Form)',
                optional: false,
            },
            {
                field: 'htmlMessage',
                description: 'Message, HTML-safe output (from Contact Us Form)',
                optional: false,
            },
            {
                field: 'contactUs',
                description: 'Email Address used for Contact Us communications',
                optional: false,
            },
        ],
    },
    {
        name: BazaContentTypesMail.BazaContentTypesNewslettersInquiryEmail,
        title: 'Inquiry Email',
        description: 'Email notification which is sent to user after subscribing to newsletters',
        tag: BazaContentTypesMailTags.BazaContentTypesNewsletters,
        filePath: 'baza-content-types/newsletters/inquiry-email/inquiry-email.{type}.hbs',
        variables: [
            {
                field: 'site',
                description: 'URL to Web Application',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (from Registry)',
                optional: false,
            },
            {
                field: 'inquiryEmail',
                description: 'Email Address (from Registry)',
                optional: false,
            },
            {
                field: 'website',
                description: 'Web Site (from Registry)',
                optional: false,
            },
            {
                field: 'comingSoon',
                description: 'Coming Soon (from Registry)',
                optional: false,
            },
            {
                field: 'name',
                description: 'Name (from Request)',
                optional: true,
            },
        ],
    },
]);
