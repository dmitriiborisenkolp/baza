import { Module } from '@nestjs/common';
import { BazaContentTypesTimelineE2eModule } from '../../../libs/timeline/src';
import { BazaContentTypesBlogApiE2eModule } from '../../../libs/blog/src';
import { BazaContentTypeCategoriesApiE2eModule } from '../../../libs/categories/src';
import { BazaContentTypeContactsApiE2eModule } from '../../../libs/contacts/src';
import { BazaContentTypeEventsApiE2eModule } from '../../../libs/events/src';
import { BazaContentTypesFaqApiE2eModule } from '../../../libs/faq/src';
import { BazaContentTypesJobsApiE2eModule } from '../../../libs/jobs/src';
import { BazaContentTypeNewsApiE2eModule } from '../../../libs/news/src';
import { BazaContentTypesNewslettersApiE2eModule } from '../../../libs/newsletters/src';
import { BazaContentTypePagesApiE2eModule } from '../../../libs/pages/src';
import { BazaContentTypesTagsApiE2eModule } from '../../../libs/tags/src';
import { BazaContentTypesTeamsApiE2eModule } from '../../../libs/teams/src';
import { BazaContentTypesTestimonialsApiE2eModule } from '../../../libs/testimonials/src';

const E2E_MODULES = [
    BazaContentTypesBlogApiE2eModule,
    BazaContentTypeCategoriesApiE2eModule,
    BazaContentTypeContactsApiE2eModule,
    BazaContentTypeEventsApiE2eModule,
    BazaContentTypesFaqApiE2eModule,
    BazaContentTypesJobsApiE2eModule,
    BazaContentTypeNewsApiE2eModule,
    BazaContentTypesNewslettersApiE2eModule,
    BazaContentTypePagesApiE2eModule,
    BazaContentTypesTagsApiE2eModule,
    BazaContentTypesTeamsApiE2eModule,
    BazaContentTypesTestimonialsApiE2eModule,
    BazaContentTypesTimelineE2eModule,
];

@Module({
    imports: E2E_MODULES,
    exports: E2E_MODULES,
})
export class BazaContentTypesApiE2eModule {}
