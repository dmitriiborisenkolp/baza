import { RegistrySchema } from '@scaliolabs/baza-core-shared';
import { bazaContentTypeContactsApiRegistry } from '../../../libs/contacts/src';
import { bazaContentTypesNewslettersApiRegistry } from '../../../libs/newsletters/src';
import { bazaContentTypeJobsApiRegistry } from '../../../libs/jobs/src';

export const bazaContentTypesApiBundleRegistry: RegistrySchema = {
    bazaContentTypes: {
        ...bazaContentTypeContactsApiRegistry,
        ...bazaContentTypesNewslettersApiRegistry,
        ...bazaContentTypeJobsApiRegistry,
    },
};
