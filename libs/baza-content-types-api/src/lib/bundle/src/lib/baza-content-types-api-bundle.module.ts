import { DynamicModule } from '@nestjs/common';
import { ApiExtraModels } from '@nestjs/swagger';
import { allBazaContentBlockPairs, BazaContentTypes, isBazaContentTypeEnabled } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypesApiConfig, BazaContentTypesApiConfigModule } from '../../../config/src';
import { BazaContentTypeCategoriesApiModule } from '../../../libs/categories/src';
import { BazaContentTypesContactsApiModule } from '../../../libs/contacts/src';
import { BazaContentTypesFaqApiModule } from '../../../libs/faq/src';
import { BazaContentTypesNewslettersApiModule } from '../../../libs/newsletters/src';
import { BazaContentTypesTeamsApiModule } from '../../../libs/teams/src';
import { BazaContentTypesTestimonialsApiModule } from '../../../libs/testimonials/src';
import { BazaContentTypesTagsApiModule } from '../../../libs/tags/src';
import { BazaContentTypesBlogApiModule } from '../../../libs/blog/src';
import { BazaContentTypeNewsApiModule } from '../../../libs/news/src';
import { BazaContentTypePagesApiModule } from '../../../libs/pages/src';
import { BazaContentTypeJobsApiModule } from '../../../libs/jobs/src';
import { BazaContentTypeEventsApiModule } from '../../../libs/events/src';
import { BazaContentTypesTimelineApiModule } from '../../../libs/timeline/src';

const BAZA_CT_MODULES = {
    [BazaContentTypes.Categories]: BazaContentTypeCategoriesApiModule,
    [BazaContentTypes.Contacts]: BazaContentTypesContactsApiModule,
    [BazaContentTypes.Faq]: BazaContentTypesFaqApiModule,
    [BazaContentTypes.Newsletters]: BazaContentTypesNewslettersApiModule,
    [BazaContentTypes.Teams]: BazaContentTypesTeamsApiModule,
    [BazaContentTypes.Testimonials]: BazaContentTypesTestimonialsApiModule,
    [BazaContentTypes.Blogs]: BazaContentTypesBlogApiModule,
    [BazaContentTypes.Tags]: BazaContentTypesTagsApiModule,
    [BazaContentTypes.News]: BazaContentTypeNewsApiModule,
    [BazaContentTypes.Pages]: BazaContentTypePagesApiModule,
    [BazaContentTypes.Jobs]: BazaContentTypeJobsApiModule,
    [BazaContentTypes.Events]: BazaContentTypeEventsApiModule,
    [BazaContentTypes.Timeline]: BazaContentTypesTimelineApiModule,
};

@ApiExtraModels(...allBazaContentBlockPairs)
export class BazaContentTypesApiBundleModule {
    static forRootAsync(
        configFactory: (defaults: BazaContentTypesApiConfig) => Partial<BazaContentTypesApiConfig | undefined>,
    ): DynamicModule {
        const imports = Object.keys(BAZA_CT_MODULES)
            .filter((contentType) => isBazaContentTypeEnabled(contentType as BazaContentTypes))
            .map((contentType) => BAZA_CT_MODULES[`${contentType}`]);

        return {
            global: true,
            module: BazaContentTypesApiBundleModule,
            imports: [BazaContentTypesApiConfigModule.forRootAsync(configFactory), ...imports],
            exports: [BazaContentTypesApiConfigModule, ...imports],
        };
    }
}
