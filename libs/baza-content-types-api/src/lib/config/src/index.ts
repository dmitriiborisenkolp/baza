import { DynamicModule, Global, Injectable, Module } from '@nestjs/common';

@Injectable()
export class BazaContentTypesApiConfig {
    withKafkaEvents: boolean;
}

export function defaultBazaContentTypesBundleConfig(): BazaContentTypesApiConfig {
    return {
        withKafkaEvents: true,
    };
}

@Global()
@Module({})
export class BazaContentTypesApiConfigModule {
    static forRootAsync(configFactory: (defaults: BazaContentTypesApiConfig) => Partial<BazaContentTypesApiConfig | undefined>): DynamicModule {
        return {
            module: BazaContentTypesApiConfigModule,
            providers: [{
                provide: BazaContentTypesApiConfig,
                useFactory: () => {
                    const defaults = defaultBazaContentTypesBundleConfig();
                    const partial = configFactory(defaultBazaContentTypesBundleConfig());

                    if (partial) {
                        Object.assign(defaults, partial);
                    }

                    return defaults;
                },
            }],
            exports: [
                BazaContentTypesApiConfig,
            ],
        };
    }
}
