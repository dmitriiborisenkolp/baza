import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaDwollaEscrowCmsDataAccess } from './ng/baza-dwolla-escrow-cms.data-access';
import { BazaDwollaPaymentCmsDataAccess } from './ng/baza-dwolla-payment-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaDwollaPaymentCmsDataAccess, BazaDwollaEscrowCmsDataAccess],
})
export class BazaDwollaCmsDataAccessModule {}
