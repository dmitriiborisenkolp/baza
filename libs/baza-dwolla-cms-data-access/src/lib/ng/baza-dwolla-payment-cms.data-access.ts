import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import {
    BazaDwollaPaymentCmsEndpoint,
    BazaDwollaPaymentCmsEndpointPaths,
    BazaDwollaPaymentCmsListRequest,
    BazaDwollaPaymentCmsListResponse,
    BazaDwollaPaymentCmsGetByUlidRequest,
    BazaDwollaPaymentCmsDto,
    BazaDwollaPaymentCmsSyncRequest,
    BazaDwollaPaymentCmsFetchAchDetailsRequest,
    BazaDwollaPaymentCmsFetchAchDetailsResponse,
} from '@scaliolabs/baza-dwolla-shared';
import { Observable } from 'rxjs';

/**
 * Dwolla Payments CMS Data Access service
 */
@Injectable()
export class BazaDwollaPaymentCmsDataAccess implements BazaDwollaPaymentCmsEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    /**
     * Returns list of Dwolla Payments (CMS)
     * @param request
     */
    list(request: BazaDwollaPaymentCmsListRequest): Observable<BazaDwollaPaymentCmsListResponse> {
        return this.http.post(BazaDwollaPaymentCmsEndpointPaths.list, request);
    }

    /**
     * Returns Dwolla Payment by ULID (CMS)
     * @param request
     */
    getByUlid(request: BazaDwollaPaymentCmsGetByUlidRequest): Observable<BazaDwollaPaymentCmsDto> {
        return this.http.post(BazaDwollaPaymentCmsEndpointPaths.getByUlid, request);
    }

    /**
     * Sync Dwolla Payment (Updates actual status of Dwolla Payment)
     * @param request
     */
    sync(request: BazaDwollaPaymentCmsSyncRequest): Observable<BazaDwollaPaymentCmsDto> {
        return this.http.post(BazaDwollaPaymentCmsEndpointPaths.sync, request);
    }

    /**
     * Fetches and saves details of related Dwolla ACH Transfer
     * @param request
     */
    fetchAchDetails(request: BazaDwollaPaymentCmsFetchAchDetailsRequest): Observable<BazaDwollaPaymentCmsFetchAchDetailsResponse> {
        return this.http.post(BazaDwollaPaymentCmsEndpointPaths.fetchAchDetails, request);
    }
}
