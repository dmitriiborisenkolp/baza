import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import {
    BazaDwollaEscrowCmsEndpoint,
    BazaDwollaEscrowCmsEndpointPaths,
    BazaDwollaEscrowCmsSetRequest,
    BazaDwollaEscrowFundingSourceDto,
} from '@scaliolabs/baza-dwolla-shared';
import { Observable } from 'rxjs';

/**
 * Data-Access Service for Dwolla Escrow CMS
 */
@Injectable()
export class BazaDwollaEscrowCmsDataAccess implements BazaDwollaEscrowCmsEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    /**
     * Returns list of available Dwolla Funding Sources
     */
    list(): Observable<Array<BazaDwollaEscrowFundingSourceDto>> {
        return this.http.post(BazaDwollaEscrowCmsEndpointPaths.list);
    }

    /**
     * Sets Dwolla Funding Source as Default Escrow Funding Source
     * Production environments will not allow Account Balance as Escrow FS
     * UAT environments will not allow Bank Account as Escrow FS
     * @param request
     */
    set(request: BazaDwollaEscrowCmsSetRequest): Observable<BazaDwollaEscrowFundingSourceDto> {
        return this.http.post(BazaDwollaEscrowCmsEndpointPaths.set, request);
    }
}
