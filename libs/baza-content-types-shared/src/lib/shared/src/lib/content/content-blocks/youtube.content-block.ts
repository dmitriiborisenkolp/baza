import { ApiModelProperty } from "@nestjs/swagger/dist/decorators/api-model-property.decorator";

export class ContentBlockYouTube {
    @ApiModelProperty()
    url: string;

    @ApiModelProperty()
    youtubeId: string;
}
