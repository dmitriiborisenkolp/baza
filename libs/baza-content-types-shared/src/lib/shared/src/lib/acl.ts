export enum BazaContentTypeAclGroup {
    BazaContentTypes = 'BazaContentTypes',
}

export enum BazaContentTypeAcl {
    BazaContentTypesContacts = 'BazaContentTypesContacts',
    BazaContentTypesFAQ = 'BazaContentTypesFAQ',
    BazaContentTypesNewsletters = 'BazaContentTypesNewsletters',
    BazaContentTypesTeams = 'BazaContentTypesTeams',
    BazaContentTypesTestimonials = 'BazaContentTypesTestimonials',
    BazaContentTypesBlog = 'BazaContentTypesBlog',
    BazaContentTypesTags = 'BazaContentTypesTags',
    BazaContentTypesNews = 'BazaContentTypesNews',
    BazaContentTypesPages = 'BazaContentTypesPages',
    BazaContentTypesJobs = 'BazaContentTypesJobs',
    BazaContentTypesJobApplications = 'BazaContentTypesJobApplications',
    BazaContentTypesEvents = 'BazaContentTypesEvents',
    BazaContentTypesCategories = 'BazaContentTypesCategories',
    BazaContentTypesTimeline = 'BazaContentTypesTimeline',
}
