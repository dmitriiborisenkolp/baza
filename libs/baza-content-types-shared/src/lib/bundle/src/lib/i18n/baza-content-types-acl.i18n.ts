import { BazaContentTypeAclGroup, BazaContentTypeAcl } from '../../../../shared/src/lib/acl';

export const bazaContentTypesAclI18n = {
    [BazaContentTypeAclGroup.BazaContentTypes]: {
        group: 'Baza Content Types',
        nodes: {
            [BazaContentTypeAcl.BazaContentTypesCategories]: 'Categories',
            [BazaContentTypeAcl.BazaContentTypesContacts]: 'Contacts',
            [BazaContentTypeAcl.BazaContentTypesFAQ]: 'FAQ',
            [BazaContentTypeAcl.BazaContentTypesNewsletters]: 'Newsletters',
            [BazaContentTypeAcl.BazaContentTypesTeams]: 'Teams',
            [BazaContentTypeAcl.BazaContentTypesTestimonials]: 'Testimonials',
            [BazaContentTypeAcl.BazaContentTypesBlog]: 'Blog',
            [BazaContentTypeAcl.BazaContentTypesTags]: 'Tags',
            [BazaContentTypeAcl.BazaContentTypesNews]: 'News',
            [BazaContentTypeAcl.BazaContentTypesPages]: 'Pages',
            [BazaContentTypeAcl.BazaContentTypesJobs]: 'Jobs',
            [BazaContentTypeAcl.BazaContentTypesJobApplications]: 'Job Applications',
            [BazaContentTypeAcl.BazaContentTypesEvents]: 'Events',
            [BazaContentTypeAcl.BazaContentTypesTimeline]: 'Timeline',
        },
    },
};
