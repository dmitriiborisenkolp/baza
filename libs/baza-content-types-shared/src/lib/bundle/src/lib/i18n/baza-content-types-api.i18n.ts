export const bazaContentTypesApiI18n = {
    bazaContentTypes: {
        newsletters: {
            subject: '{{ clientName }}: Newsletter'
        },
    },
};
