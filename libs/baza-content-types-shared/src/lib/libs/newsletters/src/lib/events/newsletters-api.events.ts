export enum NewslettersApiEvent {
    BazaNewsletterSubscribed = 'BazaNewsletterSubscribed',
    BazaNewsletterUnSubscribed = 'BazaNewsletterUnSubscribed',
}

export type NewslettersApiEvents = BazaNewsletterSubscribed | BazaNewsletterUnSubscribed;

export interface BazaNewsletterSubscribed {
    topic: NewslettersApiEvent.BazaNewsletterSubscribed;
    payload: { name: string; email: string };
}
export interface BazaNewsletterUnSubscribed {
    topic: NewslettersApiEvent.BazaNewsletterUnSubscribed;
    payload: { name: string; email: string };
}
