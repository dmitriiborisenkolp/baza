export const bazaContentTypesNewslettersCmsEnI18n = {
    title: 'Newsletters',
    components: {
        bazaContentTypesNewslettersCms: {
            title: 'Newsletters',
            search: 'Search',
            fields: {
                name: 'Name',
                email: 'Email',
                dateCreated: 'Created At',
            },
            placeholders: {
                name: 'Name (optional)',
            },
            actions: {
                add: 'Add subscription',
                exportToCSV: 'Export to CSV',
                remove: {
                    title: 'Remove subscription',
                    confirm: 'Do you really want to remove {{ email }} from newsletters list?'
                }
            },
        },
    },
};

