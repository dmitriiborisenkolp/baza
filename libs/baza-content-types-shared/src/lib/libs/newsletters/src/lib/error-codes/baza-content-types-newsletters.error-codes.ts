export enum BazaContentTypesNewslettersErrorCodes {
    BazaContentTypesNewslettersNotFound = 'BazaContentTypesNewslettersNotFound',
}

export const bazaContentTypesNewslettersErrorCodesI18n = {
    [BazaContentTypesNewslettersErrorCodes.BazaContentTypesNewslettersNotFound]: 'Newsletter with given ID was not found',
};
