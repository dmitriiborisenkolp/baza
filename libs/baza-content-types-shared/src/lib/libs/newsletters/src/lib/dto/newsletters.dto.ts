import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class NewslettersDto {
    @ApiModelProperty()
    id: number;

    @ApiModelProperty({
        required: false,
    })
    name?: string;

    @ApiModelProperty()
    email: string;

    @ApiModelProperty({
        required: false,
    })
    dateCreated: string;

    @ApiModelProperty({
        required: false,
    })
    dateUpdated: string;
}
