export enum BazaContentTypesBlogErrorCodes {
    BazaContentTypesBlogNotFound = 'BazaContentTypesBlogNotFound',
}

export const bazaContentTypesBlogErrorCodesI18n = {
    [BazaContentTypesBlogErrorCodes.BazaContentTypesBlogNotFound]: 'Blog not found',
};
