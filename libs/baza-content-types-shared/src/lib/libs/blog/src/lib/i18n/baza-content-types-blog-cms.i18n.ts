export const bazaContentTypesBlogCmsI18n = {
    title: 'Blog',
    components: {
        bazaContentTypesBlogCms: {
            title: 'Blog',
            fields: {
                id: 'ID',
                title: 'Title',
                url: 'URL',
                isPublished: 'Is Published?',
                datePublishedAt: 'Date Published At',
                primaryCategory: 'Primary Category',
                additionalCategories: 'Additional Categories',
                thumbnail: 'Thumbnail',
                headerImage: 'Header Image',
                contents: 'Contents',
                seo: 'SEO',
            },
            actions: {
                create: 'Add Blog Record',
                update: 'Update Blog Record',
                moveUp: 'Move Up',
                moveDown: 'Move Down',
                delete: {
                    title: 'Delete Blog Record',
                    confirm: 'Do you really want to remove Blog Record "{{ title }}"?',
                    success: 'Blog record "{{ title }}" removed',
                    failed: 'Failed to remove Blog record "{{ title }}"',
                },
            },
            tabs: {
                common: 'General',
                contents: 'Contents',
                seo: 'SEO',
            },
        },
    },
};

