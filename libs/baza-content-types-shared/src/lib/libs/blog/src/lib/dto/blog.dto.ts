import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { CategoryListDto } from '../../../../categories/src';
import { BazaContentDto } from '../../../../../shared/src';

export class BlogDto implements CrudSortableEntity {
    @ApiModelProperty()
    id: number;

    @ApiModelProperty()
    sortOrder: number;

    @ApiModelProperty()
    isPublished: boolean;

    @ApiModelProperty()
    title: string;

    @ApiModelProperty()
    contents: BazaContentDto;

    @ApiModelProperty()
    primaryCategory: CategoryListDto;

    @ApiModelProperty({
        type: CategoryListDto,
        isArray: true,
    })
    additionalCategories: Array<CategoryListDto>;

    @ApiModelProperty()
    headerImgUrl: string;

    @ApiModelProperty()
    thumbnailImgUrl: string;

    @ApiModelProperty()
    seo: BazaSeoDto;
}
