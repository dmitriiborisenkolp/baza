import { Observable } from 'rxjs';
import { TagDto } from '../dto/tag.dto';

export enum TagEndpointPaths {
    getAll = '/baza-content-types/tags/all',
    getById = '/baza-content-types/tags/id/:id',
    getByUrl = '/baza-content-types/tags/url/:url',
}

export interface TagEndpoint {
    getAll(): Promise<Array<TagDto>> | Observable<Array<TagDto>>;
    getById(id: number): Promise<TagDto> | Observable<TagDto>;
    getByUrl(url: string): Promise<TagDto> | Observable<TagDto>;
}
