import { TagDto } from './tag.dto';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class TagCmsDto extends TagDto {
    @ApiModelProperty()
    isPublished: boolean;
}
