export const bazaContentTypesTagsCmsI18n = {
    title: 'Tags',
    components: {
        bazaContentTypesTagsCms: {
            title: 'Tags',
            fields: {
                id: 'ID',
                isPublished: 'Is Published?',
                title: 'Title',
                url: 'Url',
            },
            actions: {
                create: 'Add tag',
                update: 'Edit tag',
                delete: {
                    title: 'Delete tag',
                    confirm: 'Do you really want to delete "{{ title }}"?',
                    success: '"{{ title }}" deleted',
                    failed: 'Failed to delete "{{ title }}"',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
            },
        },
    },
};
