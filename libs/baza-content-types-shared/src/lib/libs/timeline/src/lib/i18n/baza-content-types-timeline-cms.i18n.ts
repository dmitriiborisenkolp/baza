export const bazaContentTypesTimelineCmsI18n = {
    title: 'Timeline',
    components: {
        bazaContentTypesTimelineCms: {
            title: 'Timeline',
            fields: {
                id: 'ID',
                isPublished: 'Is Published?',
                tags: 'Tags',
                categories: 'Categories',
                date: 'Date',
                title: 'Title',
                textDescription: 'Description (Plain Text)',
                htmlDescription: 'Description (HTML)',
                image: 'Image',
                metadata: 'Metadata',
            },
            actions: {
                create: 'Add Entry',
                update: 'Edit Entry',
                delete: {
                    title: 'Delete Entry',
                    confirm: 'Do you really want to delete timeline entry "{{ title }}"?',
                    success: 'Timeline entry "{{ title }}" deleted',
                },
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
            },
        },
    },
};
