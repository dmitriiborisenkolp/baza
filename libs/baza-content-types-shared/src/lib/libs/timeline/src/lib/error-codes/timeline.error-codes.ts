export enum TimelineErrorCodes {
    TimeLineNotFound = 'TimeLineNotFound',
}

export const timelineErrorCodeI18n = {
    [TimelineErrorCodes.TimeLineNotFound]: 'TimeLine entry was not found',
};
