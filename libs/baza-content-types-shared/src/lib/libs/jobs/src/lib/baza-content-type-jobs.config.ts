export class BazaContentTypeJobsConfig {
    enableJobApplicationNotifications: boolean;
}

export function bazaContentTypeJobsDefaultConfig(): BazaContentTypeJobsConfig {
    return {
        enableJobApplicationNotifications: true,
    };
}
