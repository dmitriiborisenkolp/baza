import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { BazaContentDto } from '../../../../../shared/src';
import { CategoryListDto } from '../../../../categories/src';

export class JobDto implements CrudSortableEntity {
    @ApiModelProperty()
    id: number;

    @ApiModelProperty()
    sortOrder: number;

    @ApiModelProperty({
        required: false,
    })
    url?: string;

    @ApiModelProperty()
    datePublishedAt: string;

    @ApiModelProperty()
    primaryCategory: CategoryListDto;

    @ApiModelProperty({
        type: CategoryListDto,
        isArray: true,
    })
    additionalCategories: Array<CategoryListDto>;

    @ApiModelProperty()
    title: string;

    @ApiModelProperty({
        required: false,
    })
    team?: string;

    @ApiModelProperty({
        required: false,
    })
    location?: string;

    @ApiModelProperty({
        required: false,
    })
    link?: string;

    @ApiModelProperty({
        required: false,
    })
    jobSpecificEmail?: string;

    @ApiModelProperty()
    shortContents: string;

    @ApiModelProperty()
    contents: BazaContentDto;

    @ApiModelProperty()
    seo: BazaSeoDto;
}
