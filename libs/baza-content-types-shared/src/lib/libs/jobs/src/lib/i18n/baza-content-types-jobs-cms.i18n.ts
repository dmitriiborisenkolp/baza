export const bazaContentTypesJobsCmsI18n = {
    title: 'Jobs',
    components: {
        bazaContentTypesJobsCms: {
            title: 'Jobs',
            fields: {
                id: 'ID',
                isPublished: 'Is Published?',
                datePublishedAt: 'Date Published At',
                primaryCategory: 'Primary Category',
                additionalCategories: 'Additional Categories',
                url: 'URL',
                title: 'Title',
                link: 'Link',
                team: 'Team',
                jobSpecificEmail: 'Job Specific Email',
                shortContents: 'Short Description',
                contents: 'Contents',
                applications: `Applications`,
            },
            applications: {
                no_applications: 'No applications',
                one_application: '1 application',
                many_applications: '{{ count }} applications',
            },
            actions: {
                create: 'Add new job',
                update: 'Edit job',
                delete: {
                    title: 'Delete job',
                    confirm: 'Do you really want to remove "{{ title }}" from jobs?',
                    success: '"{{ title }}" removed from jobs',
                    failed: 'Failed to remove "{{ title }}" from jobs',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
                contents: 'Contents',
            },
        },
        bazaContentTypesJobsApplicationsCms: {
            title: 'Jobs – {{ title }} – Applications',
            fields: {
                id: 'ID',
                firstName: 'First Name',
                lastName: 'Last Name',
                phone: 'Phone',
                email: 'Email',
                location: 'Location',
                dateCreatedAt: 'Date Created At',
                name: 'Name',
            },
            actions: {
                delete: {
                    title: 'Delete application',
                    confirm: 'Do you really want to remove "{{ firstName }} {{ lastName }}" application?',
                    success: '"{{ firstName }} {{ lastName }}" application removed',
                    failed: 'Failed to remove "{{ firstName }} {{ lastName }}" application',
                },
            },
        },
    },
};
