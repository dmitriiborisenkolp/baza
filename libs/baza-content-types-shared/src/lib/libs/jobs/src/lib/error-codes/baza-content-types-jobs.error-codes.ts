export enum BazaContentTypesJobsErrorCodes {
    BazaContentTypesJobNotFound = 'BazaContentTypesJobNotFound',
    BazaContentTypesJobApplicationNotFound = 'BazaContentTypesJobApplicationNotFound',
}

export const bazaContentTypesJobsErrorCodes = {
    [BazaContentTypesJobsErrorCodes.BazaContentTypesJobNotFound]: 'Job not found',
    [BazaContentTypesJobsErrorCodes.BazaContentTypesJobApplicationNotFound]: 'Job application not found',
};
