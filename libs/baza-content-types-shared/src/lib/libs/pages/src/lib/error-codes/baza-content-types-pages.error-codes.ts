export enum BazaContentTypesPagesErrorCodes {
    BazaContentTypesPagesNotFound = 'BazaContentTypesPagesNotFound',
    BazaContentTypesPagesListIsImmutable = 'BazaContentTypesPagesListIsImmutable',
    BazaContentTypesPagesNoUniqueSidAvailable = 'BazaContentTypesPagesNoUniqueSidAvailable',
}

export const bazaContentTypesPagesErrorCodesI18n = {
    [BazaContentTypesPagesErrorCodes.BazaContentTypesPagesNotFound]: 'Page not found',
    [BazaContentTypesPagesErrorCodes.BazaContentTypesPagesListIsImmutable]: 'You are not able to add, remove or do any changes in pages list',
    [BazaContentTypesPagesErrorCodes.BazaContentTypesPagesNoUniqueSidAvailable]: 'You must provide pageUniqueSid for page',
};
