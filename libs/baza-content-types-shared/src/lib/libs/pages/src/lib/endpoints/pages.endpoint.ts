import { Observable } from 'rxjs';
import { PageDto } from '../dto/page.dto';

export enum PagesEndpointPaths {
    get = '/baza-content-types/pages/get/:url',
}

export interface PagesEndpoint {
    get(url: string): Promise<PageDto> | Observable<PageDto>;
}
