export const bazaContentTypesPagesCmsI18n = {
    title: 'Pages',
    components: {
        bazaContentTypesPagesCms: {
            title: 'Pages',
            fields: {
                id: 'ID',
                isPublished: 'Is Published?',
                url: 'URL',
                title: 'Title',
                primaryCategory: 'Primary Category',
                additionalCategories: 'Additional Categories',
                headerImage: 'Header Image',
                contents: 'Contents',
            },
            actions: {
                create: 'Add page',
                update: 'Edit',
                delete: {
                    title: 'Delete',
                    confirm: 'Do you really want to delete "{{ title }}"?',
                    success: '"{{ title }}" deleted',
                    failed: 'Failed to delete "{{ title }}"',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
                contents: 'Contents',
            },
        },
    },
};
