export const bazaContentTypesTeamsCmsI18n = {
    title: 'Team',
    components: {
        bazaContentTypesTeamsCms: {
            title: 'Team',
            fields: {
                id: 'ID',
                name: 'Name',
                title: 'Title',
                fullImage: 'Image',
                previewImage: 'Preview',
                isPublished: 'Is Published?',
                bio: 'Bio',
                url: 'URL',
            },
            actions: {
                create: 'Add new team member',
                update: 'Edit team member',
                delete: {
                    title: 'Delete team member',
                    confirm: 'Do you really want to remove {{ name }} from team?',
                    success: '{{ name }} removed from team',
                    failed: 'Failed to remove {{ name }} from team',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
            },
        },
    },
};

