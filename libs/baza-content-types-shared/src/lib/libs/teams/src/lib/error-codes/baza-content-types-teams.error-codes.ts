export enum BazaContentTypesTeamsErrorCodes {
    BazaContentTypesTeamNotFound = 'BazaContentTypesTeamNotFound',
}

export const bazaContentTypesTeamsErrorCodesI18n = {
    [BazaContentTypesTeamsErrorCodes.BazaContentTypesTeamNotFound]: 'Team not found',
};
