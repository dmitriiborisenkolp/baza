import { BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BazaContentDto } from '../../../../../shared/src';

export class CategoryDto implements CrudSortableEntity {
    @ApiModelProperty()
    id: number;

    @ApiModelProperty()
    sortOrder: number;

    @ApiModelProperty()
    isActive: boolean;

    @ApiModelProperty({
        required: false,
    })
    parentId?: number;

    @ApiModelProperty()
    title: string;

    @ApiModelProperty()
    contents: BazaContentDto;

    @ApiModelProperty()
    seo: BazaSeoDto;
}
