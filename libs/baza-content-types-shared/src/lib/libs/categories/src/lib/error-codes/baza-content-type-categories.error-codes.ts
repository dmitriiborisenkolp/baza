export enum BazaContentTypeCategoriesErrorCodes {
    BazaContentTypeCategoryNotFound = 'BazaContentTypeCategoryNotFound',
    BazaContentTypeCategoryIsInternal = 'BazaContentTypeCategoryIsInternal',
}

export const bazaContentTypeCategoriesErrorCodesI18n = {
    [BazaContentTypeCategoriesErrorCodes.BazaContentTypeCategoryNotFound]: 'Category not found',
    [BazaContentTypeCategoriesErrorCodes.BazaContentTypeCategoryIsInternal]: 'Category is internal',
};
