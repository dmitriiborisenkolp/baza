export const bazaContentTypeCategoriesCmsI18n = {
    title: 'Categories',
    components: {
        bazaContentTypesCategoriesCms: {
            title: 'Categories',
            columns: {
                id: 'ID',
                title: 'Title',
                url: 'URL',
            },
            actions: {
                create: 'Add new category',
                update: 'Edit category',
                delete: {
                    title: 'Delete category',
                    confirm: 'Do you really want to delete category "{{ title }}"?',
                    success: 'Category "{{ title }}" and child categories deleted',
                    failed: 'Failed to delete category "{{ title }}"',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            form: {
                tabs: {
                    common: 'General',
                    contents: 'Contents',
                    seo: 'SEO',
                },
                fields: {
                    isActive: 'Is Active?',
                    parentId: 'Parent Category',
                    title: 'Title',
                    contents: 'Contents',
                },
            },
        },
    },
};
