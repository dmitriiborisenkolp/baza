export enum CategoryScope {
    // Internal scope used for categories which should not be displayed in Category CMS
    // Interal-scoped categories used for external integrations with entities outside content-type package
    Internal = 0,

    // Public scope used for categories displayed & managed with Category CMS
    Public = 1,
}
