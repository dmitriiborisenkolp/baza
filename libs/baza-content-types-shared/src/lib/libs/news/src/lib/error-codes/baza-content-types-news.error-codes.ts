export enum BazaContentTypesNewsErrorCodes {
    BazaContentTypesNewsNotFound = 'BazaContentTypesNewsNotFound',
}

export const bazaContentTypesNewsErrorCodesI18n = {
    [BazaContentTypesNewsErrorCodes.BazaContentTypesNewsNotFound]: 'News record not found',
};
