export const bazaContentTypesNewsCmsI18n = {
    title: 'News',
    components: {
        bazaContentTypesNewsCms: {
            title: 'News',
            fields: {
                id: 'ID',
                isPublished: 'Is Published?',
                url: 'URL',
                title: 'Title',
                link: 'Link',
                image: 'Image',
                previewImage: 'Preview Image',
                datePublishedAt: 'Published At',
                contents: 'Contents',
                primaryCategory: 'Primary Category',
                additionalCategories: 'Additional Categories',
            },
            actions: {
                create: 'Add news',
                update: 'Edit',
                delete: {
                    title: 'Delete',
                    confirm: 'Do you really want to delete "{{ title }}"?',
                    success: '"{{ name }}" deleted',
                    failed: 'Failed to delete "{{ name }}"',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
                contents: 'Contents',
            },
        },
    },
};
