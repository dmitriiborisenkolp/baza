export enum BazaContentTypesTestimonialsErrorCodes {
    BazaContentTypesTestimonialsNotFound = 'BazaContentTypesTestimonialsNotFound',
}

export const bazaContentTypesTestimonialsErrorCodesI18n = {
    [BazaContentTypesTestimonialsErrorCodes.BazaContentTypesTestimonialsNotFound]: 'Testimonial not found',
};
