export const bazaContentTypesTestimonialsCmsI18n = {
    title: 'Testimonial',
    components: {
        bazaContentTypesTestimonialsCms: {
            title: 'Testimonials',
            fields: {
                id: 'ID',
                isPublished: 'Is Published?',
                url: 'URL',
                name: 'Name',
                title: 'Title',
                company: 'Company',
                link: 'Link',
                location: 'Location',
                email: 'Email',
                image: 'Image',
                quote: 'Quote',
                preview: 'Contents',
            },
            actions: {
                create: 'Add new testimonial',
                update: 'Edit testimonial',
                delete: {
                    title: 'Delete testimonial',
                    confirm: 'Do you really want to remove {{ name }} from testimonials?',
                    success: '{{ name }} removed from testimonials',
                    failed: 'Failed to remove {{ name }} from testimonials',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
                description: 'Description',
            },
        },
    },
};

