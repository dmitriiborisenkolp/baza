export const bazaContentTypesContactsCmsEn18n: Record<string, unknown> = {
    title: 'Contacts',
    components: {
        bazaContentTypesContactsCms: {
            title: 'Contacts – Inbox',
            search: 'Search',
            columns: {
                id: 'ID',
                subject: 'Subject',
                name: 'Name',
                email: 'Email',
                sent: 'Email Sent?',
                dateCreatedAt: 'Created At',
                dateProcessedAt: 'Processed At',
            },
            actions: {
                view: 'View',
                refresh: 'Refresh',
                markAsProcessed: 'Mark as processed',
                markAsUnprocessed: 'Mark as unprocessed',
                subjects: 'Subjects',
            },
            fields: {
                subject: 'Subject',
                name: 'Name',
                email: 'Email',
                message: 'Message',
                isProcessed: 'Is processed?',
            },
        },
        bazaContentTypesContactsSubjectCms: {
            title: 'Contacts – Subjects',
            columns: {
                id: 'ID',
                subject: 'Subject',
                isEnabled: 'Is Enabled?',
            },
            actions: {
                inbox: 'Inbox',
                create: 'Add new subject',
                update: 'Edit subject',
                delete: {
                    title: 'Delete subject',
                    confirm: 'Do you really want to remove subject "{{ subject }}"?',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            fields: {
                subject: 'Subject',
                isEnabled: 'Is Enabled?',
            },
        },
    },
};
