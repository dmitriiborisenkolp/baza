import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class ContactsSubjectDto {
    @ApiModelProperty()
    id: number;

    @ApiModelProperty()
    subject: string;
}
