export enum BazaContentTypesContactsErrorCodes {
    BazaContentTypesContactsNotFound = 'BazaContentTypesContactsNotFound',
    BazaContentTypesContactsSubjectNotFound = 'BazaContentTypesContactsSubjectNotFound',
    BazaContentTypesContactsSubjectIsNotEnabled = 'BazaContentTypesContactsSubjectIsNotEnabled',
}

export const bazaContentTypesContactsErrorCodesI18n = {
    [BazaContentTypesContactsErrorCodes.BazaContentTypesContactsNotFound]: 'Contacts message not found',
    [BazaContentTypesContactsErrorCodes.BazaContentTypesContactsSubjectNotFound]: 'Contacts subject is not available',
    [BazaContentTypesContactsErrorCodes.BazaContentTypesContactsSubjectIsNotEnabled]: 'Contacts subject not found',
};
