// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace BazaContentTypesContactsConstants {
    export const VALIDATION_EMAIL_MAX_LENGTH = 127;
    export const VALIDATION_NAME_MAX_LENGTH = 120;
    export const VALIDATION_MESSAGE_MAX_LENGTH = 3000;
}
