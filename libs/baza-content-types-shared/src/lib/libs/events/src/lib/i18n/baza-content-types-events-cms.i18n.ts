export const bazaContentTypesEventsCmsI18n = {
    title: 'Events',
    components: {
        bazaContentTypesEventsCms: {
            title: 'Events',
            fields: {
                id: 'ID',
                isPublished: 'Is Published?',
                datePublishedAt: 'Date Published At',
                url: 'URL',
                title: 'Title',
                link: 'Link',
                location: 'Location',
                date: 'Date',
                image: 'Image',
                thumbnailImage: 'Thumbnail',
                primaryCategory: 'Primary Category',
                additionalCategories: 'Additional Categories',
                contents: 'Contents',
            },
            actions: {
                create: 'Add new event',
                update: 'Edit event',
                delete: {
                    title: 'Delete event',
                    confirm: 'Do you really want to remove "{{ title }}" from events?',
                    success: '"{{ title }}" removed from events',
                    failed: 'Failed to remove "{{ title }}" from events',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
                contents: 'Contents',
            },
        },
    },
};
