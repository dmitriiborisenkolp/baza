export enum BazaContentTypesEventsErrorCodes {
    BazaContentTypesEventNotFound = 'BazaContentTypesEventNotFound',
}

export const bazaContentTypesEventsErrorCodesI18n = {
    [BazaContentTypesEventsErrorCodes.BazaContentTypesEventNotFound]: 'Event not found',
};
