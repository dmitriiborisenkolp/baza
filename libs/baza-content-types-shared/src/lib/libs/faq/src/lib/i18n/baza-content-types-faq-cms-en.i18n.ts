export const bazaContentTypesFaqCmsEn18n: Record<string, unknown> = {
    title: 'FAQ',
    components: {
        bazaContentTypesFaqCms: {
            title: 'FAQ',
            columns: {
                id: 'ID',
                dateCreated: 'Date',
                question: 'Question',
                isPublished: 'Is Published?',
                category: 'Category',
            },
            fields: {
                question: 'Question',
                answer: 'Answer',
                isPublished: 'Is Published?',
                categoryId: 'Category',
            },
            placeholders: {
                question: 'Question',
                answer: 'Answer',
            },
            actions: {
                add: 'Add new Q/A',
                edit: 'Edit Q/A',
                delete: {
                    title: 'Remove',
                    confirm: 'Do you really want to remove Q/A?',
                    success: 'Q/A removed',
                    failed: 'Failed to remove QA',
                },
                moveUp: 'Move Up',
                moveDown: 'Move Down',
            },
            tabs: {
                common: 'General',
                seo: 'SEO',
            },
        },
    },
};
