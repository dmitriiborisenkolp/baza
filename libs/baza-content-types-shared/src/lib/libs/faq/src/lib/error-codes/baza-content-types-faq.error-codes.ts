export enum BazaContentTypesFaqErrorCodes {
    BazaContentTypesFaqNotFound = 'BazaContentTypesFaqNotFound',
}

export const bazaContentTypesErrorCodesI18n = {
    [BazaContentTypesFaqErrorCodes.BazaContentTypesFaqNotFound]: 'FAQ entity was not found',
};
