# baza-core-web

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-core-web` to execute the unit tests.

## Build

Run `nx build baza-core-web`

## Publish

Run `yarn publish:baza-core-web`
