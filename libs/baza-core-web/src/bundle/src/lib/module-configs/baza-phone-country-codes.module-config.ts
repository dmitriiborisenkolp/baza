import { BazaPhoneCountryCodesNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BazaWebBundleConfig } from '../baza-web-bundle.config';

export const BAZA_PHONE_COUNTRY_CODES_NG_MODULE_CONFIG: (config: BazaWebBundleConfig) => BazaPhoneCountryCodesNgModuleForRootOptions = () => ({
    useFactory: () => ({
        ttlMilliseconds: 60 /* minutes */ * 60 /* seconds */ * 1000 /* ms */,
    }),
});
