import { BazaNgEndpointModuleForRootOptions } from '@scaliolabs/baza-core-data-access';
import { BazaWebBundleConfig } from '../baza-web-bundle.config';

export const BAZA_NG_ENDPOINT_MODULE_CONFIG: (config: BazaWebBundleConfig) => BazaNgEndpointModuleForRootOptions = (config) => ({
    injects: [],
    useFactory: () => ({
        apiEndpointUrl: config.apiEndpoint(),
    }),
});
