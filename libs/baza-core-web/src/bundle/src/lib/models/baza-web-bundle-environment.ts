import { BrowserOptions } from '@sentry/browser/dist/backend';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export interface BazaWebBundleEnvironment {
    ngEnv: NgEnvironment;
    enableAngularProduction: boolean;
    apiEndpoint: string;
    sentry?: {
        dsn: string;
        browserOptions?: BrowserOptions;
    };
}
