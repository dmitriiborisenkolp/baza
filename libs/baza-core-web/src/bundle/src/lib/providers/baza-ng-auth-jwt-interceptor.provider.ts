import { Provider } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtHttpInterceptor } from '@scaliolabs/baza-core-ng';

export const BAZA_NG_AUTH_JWT_INTERCEPTOR_PROVIDER: Provider = {
    provide: HTTP_INTERCEPTORS,
    useClass: JwtHttpInterceptor,
    multi: true
};
