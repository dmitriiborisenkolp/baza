import {
    BazaCreditCardDetailsRequest,
    BazaCreditCardDetailsResponse,
    BazaCreditCardEndpoint,
    BazaCreditCardEndpointPaths,
    BazaCreditCardIsExpiredRequest,
    BazaCreditCardIsExpiredResponse,
    BazaCreditCardValidateRequest,
    BazaCreditCardValidateResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaCreditCardNodeAccess implements BazaCreditCardEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async validate(request: BazaCreditCardValidateRequest): Promise<BazaCreditCardValidateResponse> {
        return this.http.post(BazaCreditCardEndpointPaths.validate, request);
    }

    async details(request: BazaCreditCardDetailsRequest): Promise<BazaCreditCardDetailsResponse> {
        return this.http.post(BazaCreditCardEndpointPaths.details, request);
    }

    async isExpired(request: BazaCreditCardIsExpiredRequest): Promise<BazaCreditCardIsExpiredResponse> {
        return this.http.post(BazaCreditCardEndpointPaths.isExpired, request);
    }
}
