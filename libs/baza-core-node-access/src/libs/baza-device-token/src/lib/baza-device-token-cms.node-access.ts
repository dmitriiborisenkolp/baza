import { Injectable } from '@angular/core';
import {
    BazaDeviceTokenCmsEndpoint,
    BazaDeviceTokenCmsEndpointPaths,
    BazaDeviceTokenGetAllRequest,
    BazaDeviceTokenGetAllResponse,
    BazaDeviceTokenLinkRequest,
    BazaDeviceTokenLinkResponse,
    BazaDeviceTokenUnlinkRequest,
    BazaDeviceTokenUnlinkResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

@Injectable()
export class BazaDeviceTokenCmsNodeAccess implements BazaDeviceTokenCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    getAll(request: BazaDeviceTokenGetAllRequest): Promise<BazaDeviceTokenGetAllResponse> {
        return this.http.post(BazaDeviceTokenCmsEndpointPaths.GetAll, request);
    }

    link(request: BazaDeviceTokenLinkRequest): Promise<BazaDeviceTokenLinkResponse> {
        return this.http.post(BazaDeviceTokenCmsEndpointPaths.Link, request);
    }

    unlink(request: BazaDeviceTokenUnlinkRequest): Promise<BazaDeviceTokenUnlinkResponse> {
        return this.http.post(BazaDeviceTokenCmsEndpointPaths.Unlink, request);
    }
}
