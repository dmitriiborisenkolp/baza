import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    WhitelistAccountEndpoint,
    WhitelistAccountEndpointPaths,
    WhitelistAccountDto,
    WhitelistAccountListRequest,
    WhitelistAccountListResponse,
    WhitelistAccountRemoveRequest,
    WhitelistAccountAddRequest,
} from '@scaliolabs/baza-core-shared';

export class BazaWhitelistAccountCmsNodeAccess implements WhitelistAccountEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: WhitelistAccountListRequest): Promise<WhitelistAccountListResponse> {
        return this.http.post(WhitelistAccountEndpointPaths.list, request);
    }

    async add(request: WhitelistAccountAddRequest): Promise<WhitelistAccountDto> {
        return this.http.post(WhitelistAccountEndpointPaths.add, request);
    }

    async remove(request: WhitelistAccountRemoveRequest): Promise<void> {
        return this.http.post(WhitelistAccountEndpointPaths.remove, request, {
            asJsonResponse: false,
        });
    }

    importCsv(file: File): Promise<void> {
        const formData = new FormData();

        formData.append('file', file);

        return this.http.post(WhitelistAccountEndpointPaths.importCsv, formData);
    }
}
