import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaPhoneCountryCodesEndpoint,
    BazaPhoneCountryCodesEndpointPaths,
    PhoneCountryCodesDto,
    PhoneCountryCodesRepositoryDto,
} from '@scaliolabs/baza-core-shared';

export class BazaPhoneCountryCodesNodeAccess implements BazaPhoneCountryCodesEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async repository(): Promise<PhoneCountryCodesRepositoryDto> {
        return this.http.get(BazaPhoneCountryCodesEndpointPaths.repository);
    }

    async getByNumericCode(numericCode: string): Promise<PhoneCountryCodesDto> {
        return this.http.get(
            this.http.path(BazaPhoneCountryCodesEndpointPaths.getByNumericCode, {
                numericCode,
            }),
        );
    }
}
