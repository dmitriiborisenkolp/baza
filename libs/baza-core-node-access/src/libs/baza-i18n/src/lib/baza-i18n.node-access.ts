import { BazaDataAccessNode } from '../../../baza-data-access.node';
import { BazaI18nEndpointPaths, BazaI18nGetRequest, BazaI18nGetResponse, I18nEndpoint } from '@scaliolabs/baza-core-shared';

export class BazaI18nNodeAccess implements I18nEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async get(request: BazaI18nGetRequest): Promise<BazaI18nGetResponse> {
        return this.http.post(BazaI18nEndpointPaths.get, request);
    }
}
