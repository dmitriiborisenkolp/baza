import { AuthMasterPasswordDto, AuthMasterPasswordEndpoint, AuthMasterPasswordEndpointPaths } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaAuthMasterPasswordNodeAccess implements AuthMasterPasswordEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    enable(): Promise<void> {
        return this.http.post(AuthMasterPasswordEndpointPaths.enable, undefined, {
            asJsonResponse: false,
            withJwt: true,
        });
    }

    disable(): Promise<void> {
        return this.http.post(AuthMasterPasswordEndpointPaths.disable, undefined, {
            asJsonResponse: false,
            withJwt: true,
        });
    }

    current(): Promise<AuthMasterPasswordDto> {
        return this.http.post(AuthMasterPasswordEndpointPaths.current, undefined, {
            withJwt: true,
        });
    }

    refresh(): Promise<AuthMasterPasswordDto> {
        return this.http.post(AuthMasterPasswordEndpointPaths.refresh, undefined, {
            asJsonResponse: false,
            withJwt: true,
        });
    }
}
