import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    Auth2FAConfigResponse,
    Auth2FASettingsEndpoint,
    Auth2FASettingsEndpointPaths,
    Auth2FAUpdateSettingsRequest,
} from '@scaliolabs/baza-core-shared';

export class BazaAuth2FASettingsNodeAccess implements Auth2FASettingsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async config(): Promise<Auth2FAConfigResponse> {
        return this.http.post(Auth2FASettingsEndpointPaths.config);
    }

    async enable2FAEmail(request: Auth2FAUpdateSettingsRequest): Promise<void> {
        return this.http.post(Auth2FASettingsEndpointPaths.enable2FAEmail, request, {
            asJsonResponse: false,
        });
    }

    async disable2FAEmail(request: Auth2FAUpdateSettingsRequest): Promise<void> {
        return this.http.post(Auth2FASettingsEndpointPaths.disable2FAEmail, request, {
            asJsonResponse: false,
        });
    }

    async enable2FAGoogle(request: Auth2FAUpdateSettingsRequest): Promise<void> {
        return this.http.post(Auth2FASettingsEndpointPaths.enable2FAGoogle, request, {
            asJsonResponse: false,
        });
    }

    async disable2FAGoogle(request: Auth2FAUpdateSettingsRequest): Promise<void> {
        return this.http.post(Auth2FASettingsEndpointPaths.disable2FAGoogle, request, {
            asJsonResponse: false,
        });
    }
}
