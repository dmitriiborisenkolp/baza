import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    AuthEndpoint,
    AuthEndpointPaths,
    AuthRequest,
    AuthResponse,
    AuthVerifyCredentials,
    InvalidateAllTokensResponse,
    InvalidateByRefreshTokenRequest,
    InvalidateByRefreshTokenResponse,
    InvalidateTokenRequest,
    InvalidateTokenResponse,
    RefreshTokenRequest,
    RefreshTokenResponse,
    VerifyRequest,
    VerifyResponse,
} from '@scaliolabs/baza-core-shared';

export class BazaAuthNodeAccess implements AuthEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async auth(request: AuthRequest): Promise<AuthResponse> {
        return this.http.post(AuthEndpointPaths.auth, request);
    }

    async verifyCredentials(request: AuthVerifyCredentials): Promise<boolean> {
        return this.http.post(AuthEndpointPaths.verifyCredentials, request);
    }

    async verify(request: VerifyRequest): Promise<VerifyResponse> {
        return this.http.post(AuthEndpointPaths.verify, request, {
            asJsonResponse: false,
            withJwt: false,
        });
    }

    async refreshToken(request: RefreshTokenRequest): Promise<RefreshTokenResponse> {
        return this.http.post(AuthEndpointPaths.refreshToken, request, {
            asJsonResponse: true,
            withJwt: true,
        });
    }

    async invalidateToken(request: InvalidateTokenRequest): Promise<InvalidateTokenResponse> {
        return this.http.post(AuthEndpointPaths.invalidateToken, request, {
            asJsonResponse: false,
            withJwt: false,
        });
    }

    async invalidateAllTokens(): Promise<InvalidateAllTokensResponse> {
        return this.http.post(
            AuthEndpointPaths.invalidateAllTokens,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    async invalidateByRefreshToken(request: InvalidateByRefreshTokenRequest): Promise<InvalidateByRefreshTokenResponse> {
        return this.http.post(AuthEndpointPaths.invalidateByRefreshToken, request, {
            asJsonResponse: false,
            withJwt: false,
        });
    }
}
