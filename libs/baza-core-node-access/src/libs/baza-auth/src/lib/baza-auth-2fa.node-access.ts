import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    Auth2FAEndpoint,
    Auth2FAEndpointPaths,
    Auth2FAMethodsRequest,
    Auth2FAMethodsResponse,
    Auth2FARequest,
    Auth2FAVerifyRequest,
    AuthGoogle2FAQrCodeRequest,
    AuthGoogle2FAQrCodeResponse,
    AuthResponse,
} from '@scaliolabs/baza-core-shared';

export class BazaAuth2FANodeAccess implements Auth2FAEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async auth2FA(request: Auth2FARequest): Promise<void> {
        return this.http.post(Auth2FAEndpointPaths.auth2FA, request, {
            asJsonResponse: false,
        });
    }

    async auth2FAVerify(request: Auth2FAVerifyRequest): Promise<AuthResponse> {
        return this.http.post(Auth2FAEndpointPaths.auth2FAVerify, request);
    }

    async auth2FAMethods(request: Auth2FAMethodsRequest): Promise<Auth2FAMethodsResponse> {
        return this.http.post(Auth2FAEndpointPaths.auth2FAMethods, request);
    }

    async generate2FAGoogleQrCode(request: AuthGoogle2FAQrCodeRequest): Promise<AuthGoogle2FAQrCodeResponse> {
        return this.http.get(Auth2FAEndpointPaths.generate2FAGoogleQrCode, request);
    }
}
