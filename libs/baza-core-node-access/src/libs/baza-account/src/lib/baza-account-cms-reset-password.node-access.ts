import {
    AccountCmsResetPasswordEndpoint,
    AccountCmsResetPasswordEndpointPaths,
    RequestAllAccountsResetPasswordCmsAccountRequest,
    RequestResetPasswordCmsAccountRequest,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';
export class BazaAccountCmsResetPasswordNodeAccess implements AccountCmsResetPasswordEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    requestResetPassword(request: RequestResetPasswordCmsAccountRequest): Promise<void> {
        return this.http.post(AccountCmsResetPasswordEndpointPaths.requestResetPassword, request, {
            asJsonResponse: false,
        });
    }

    requestResetPasswordAllAccounts(request: RequestAllAccountsResetPasswordCmsAccountRequest): Promise<void> {
        return this.http.post(AccountCmsResetPasswordEndpointPaths.requestResetPasswordAllAccounts, request, {
            asJsonResponse: false,
        });
    }
}
