import {
    AccountEndpoint,
    AccountEndpointPaths,
    AccountExistsRequest,
    AccountExistsResponse,
    BazaAccountRegisterOptions,
    ChangeEmailRequest,
    ChangeEmailResponse,
    ChangePasswordRequest,
    ChangePasswordResponse,
    ConfirmEmailRequest,
    ConfirmEmailResponse,
    CurrentAccountResponse,
    DeactivateAccountRequest,
    RegisterAccountRequest,
    RegisterAccountResponse,
    ResetPasswordRequest,
    ResetPasswordResponse,
    SendChangeEmailLinkRequest,
    SendConfirmEmailLinkRequest,
    SendConfirmEmailLinkResponse,
    SendDeactivateAccountLinkRequest,
    SendResetPasswordLinkRequest,
    SendResetPasswordLinkResponse,
    SetAccountSettingsRequest,
    SetAccountSettingsResponse,
    UpdateProfileRequest,
    UpdateProfileResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode, BazaNgEndpointNodeOptions } from '../../../baza-data-access.node';
export class BazaAccountNodeAccess implements AccountEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async currentAccount(): Promise<CurrentAccountResponse> {
        return this.http.get(AccountEndpointPaths.currentAccount);
    }

    async registerAccount(request: RegisterAccountRequest): Promise<RegisterAccountResponse> {
        return this.http.post(AccountEndpointPaths.registerAccount, request, {
            asJsonResponse: false,
        });
    }

    async registerAccountOptions(): Promise<BazaAccountRegisterOptions> {
        return this.http.get(AccountEndpointPaths.registerAccountOptions);
    }

    async confirmEmail(request: ConfirmEmailRequest): Promise<ConfirmEmailResponse> {
        return this.http.post(AccountEndpointPaths.confirmEmail, request);
    }

    async sendConfirmEmailLink(request: SendConfirmEmailLinkRequest): Promise<SendConfirmEmailLinkResponse> {
        return this.http.post(AccountEndpointPaths.sendConfirmEmailLink, request, {
            asJsonResponse: false,
        });
    }

    async changePassword(
        request: ChangePasswordRequest,
        withOptions: Partial<BazaNgEndpointNodeOptions> = {},
    ): Promise<ChangePasswordResponse> {
        return this.http.post(AccountEndpointPaths.changePassword, request, {
            asJsonResponse: false,
            withJwt: true,
            ...withOptions,
        });
    }

    async sendResetPasswordLink(request: SendResetPasswordLinkRequest): Promise<SendResetPasswordLinkResponse> {
        return this.http.post(AccountEndpointPaths.sendResetPasswordLink, request, {
            asJsonResponse: false,
        });
    }

    sendChangeEmailLink(request: SendChangeEmailLinkRequest): Promise<void> {
        return this.http.post(AccountEndpointPaths.sendChangeEmailLink, request, {
            asJsonResponse: false,
        });
    }

    changeEmail(request: ChangeEmailRequest): Promise<ChangeEmailResponse> {
        return this.http.post(AccountEndpointPaths.changeEmail, request);
    }

    async resetPassword(request: ResetPasswordRequest): Promise<ResetPasswordResponse> {
        return this.http.post(AccountEndpointPaths.resetPassword, request, {
            withJwt: false,
        });
    }

    async updateSettings(request: SetAccountSettingsRequest): Promise<SetAccountSettingsResponse> {
        return this.http.post(AccountEndpointPaths.updateAccountSettings, request);
    }

    async updateProfile(request: UpdateProfileRequest): Promise<UpdateProfileResponse> {
        return this.http.post(AccountEndpointPaths.updateProfile, request);
    }

    async sendDeactivateAccountLink(request: SendDeactivateAccountLinkRequest): Promise<void> {
        return this.http.post(AccountEndpointPaths.sendDeactivateAccountLink, request, {
            asJsonResponse: false,
        });
    }

    async deactivateAccount(request: DeactivateAccountRequest): Promise<void> {
        return this.http.post(AccountEndpointPaths.deactivateAccount, request, {
            asJsonResponse: false,
            withJwt: false,
        });
    }

    async accountExists(request: AccountExistsRequest): Promise<AccountExistsResponse> {
        return this.http.post(AccountEndpointPaths.accountExists, request);
    }
}
