import {
    AccountCmsEndpoint,
    AccountCmsEndpointPaths,
    AssignAdminAccessCmsRequest,
    AssignAdminAccessCmsResponse,
    ChangeEmailAccountCmsRequest,
    ChangeEmailAccountCmsResponse,
    ConfirmEmailAccountByIdCmsRequest,
    ConfirmEmailAccountByIdCmsResponse,
    DeactivateAccountCmsRequest,
    DeactivateAccountCmsResponse,
    DeleteAccountByIdCmsRequest,
    DeleteAccountByIdCmsResponse,
    ExportToCsvAccountRequest,
    GetAccountByEmailCmsRequest,
    GetAccountByEmailCmsResponse,
    GetAccountByIdCmsRequest,
    GetAccountByIdCmsResponse,
    ListAccountsCmsRequest,
    ListAccountsCmsResponse,
    RegisterAdminAccountCmsRequest,
    RegisterAdminAccountCmsResponse,
    RegisterUserAccountCmsRequest,
    RegisterUserAccountCmsResponse,
    RevokeAdminAccessCmsRequest,
    RevokeAdminAccessCmsResponse,
    UpdateAccountCmsRequest,
    UpdateAccountCmsResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../../../src/libs/baza-data-access.node';

export class BazaAccountCmsNodeAccess implements AccountCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async registerAdminAccount(request: RegisterAdminAccountCmsRequest): Promise<RegisterAdminAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.registerAdminAccount, request);
    }

    async registerUserAccount(request: RegisterUserAccountCmsRequest): Promise<RegisterUserAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.registerUserAccount, request);
    }

    async listAccounts(request: ListAccountsCmsRequest): Promise<ListAccountsCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.listAccounts, request);
    }

    async assignAdminAccess(request: AssignAdminAccessCmsRequest): Promise<AssignAdminAccessCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.assignAdminAccess, request, {
            asJsonResponse: false,
        });
    }

    async revokeAdminAccess(request: RevokeAdminAccessCmsRequest): Promise<RevokeAdminAccessCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.revokeAdminAccess, request, {
            asJsonResponse: false,
        });
    }

    async getAccountById(request: GetAccountByIdCmsRequest): Promise<GetAccountByIdCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.getAccountById, request);
    }

    async getAccountByEmail(request: GetAccountByEmailCmsRequest): Promise<GetAccountByEmailCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.getAccountByEmail, request);
    }

    async deleteAccountById(request: DeleteAccountByIdCmsRequest): Promise<DeleteAccountByIdCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.deleteAccountById, request, {
            asJsonResponse: false,
        });
    }

    async confirmEmailAccountById(request: ConfirmEmailAccountByIdCmsRequest): Promise<ConfirmEmailAccountByIdCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.confirmEmailAccountById, request);
    }

    async updateAccount(request: UpdateAccountCmsRequest): Promise<UpdateAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.updateAccount, request);
    }

    async exportToCsv(request: ExportToCsvAccountRequest): Promise<string> {
        return this.http.post(AccountCmsEndpointPaths.exportToCsv, request);
    }

    async deactivateAccount(request: DeactivateAccountCmsRequest): Promise<DeactivateAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.deactivateAccount, request);
    }

    async changeEmail(request: ChangeEmailAccountCmsRequest): Promise<ChangeEmailAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.changeEmail, request);
    }
}
