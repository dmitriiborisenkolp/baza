import { BazaStatusEndpointPaths, BazaStatusIndexResponse, StatusEndpoint } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaStatusNodeAccess implements StatusEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    index(): Promise<BazaStatusIndexResponse> {
        return this.http.get(BazaStatusEndpointPaths.index);
    }
}
