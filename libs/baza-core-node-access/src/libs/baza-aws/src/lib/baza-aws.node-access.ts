import { BazaDataAccessNode } from '../../../baza-data-access.node';
import * as FormData from 'form-data';
import {
    BazaAwsDeleteRequest,
    BazaAwsEndpoint,
    BazaAwsEndpointPaths,
    BazaAwsPresignedUrlRequest,
    BazaAwsUploadResponse,
} from '@scaliolabs/baza-core-shared';

export class BazaAwsNodeAccess implements BazaAwsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async upload(file: File): Promise<BazaAwsUploadResponse> {
        const formData = new FormData();

        formData.append('file', file);

        return this.http.post(BazaAwsEndpointPaths.upload, formData, {
            asFormData: true,
        });
    }

    async delete(request: BazaAwsDeleteRequest): Promise<void> {
        return this.http.post(BazaAwsEndpointPaths.delete, request);
    }

    async presignedUrl(request: BazaAwsPresignedUrlRequest): Promise<string> {
        return this.http.post(BazaAwsEndpointPaths.presignedUrl, request);
    }
}
