import {
    BazaMaintenanceCmsDisableRequest,
    BazaMaintenanceCmsEnableRequest,
    BazaMaintenanceCmsEndpoint,
    BazaMaintenanceCmsEndpointPaths,
    BazaMaintenanceDto,
    BazaMaintenanceMessageDto,
    BazaMaintenanceSetMessageRequest,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaMaintenanceCmsNodeAccess implements BazaMaintenanceCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    status(): Promise<BazaMaintenanceDto> {
        return this.http.get(BazaMaintenanceCmsEndpointPaths.status);
    }

    enable(request: BazaMaintenanceCmsEnableRequest): Promise<BazaMaintenanceDto> {
        return this.http.post(BazaMaintenanceCmsEndpointPaths.enable, request);
    }

    disable(request: BazaMaintenanceCmsDisableRequest): Promise<BazaMaintenanceDto> {
        return this.http.post(BazaMaintenanceCmsEndpointPaths.disable, request);
    }

    getMessage(): Promise<BazaMaintenanceMessageDto> {
        return this.http.get(BazaMaintenanceCmsEndpointPaths.getMessage);
    }

    setMessage(request: BazaMaintenanceSetMessageRequest): Promise<BazaMaintenanceMessageDto> {
        return this.http.post(BazaMaintenanceCmsEndpointPaths.setMessage, request);
    }
}
