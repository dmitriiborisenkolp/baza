import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaMailEndpoint,
    BazaMailEndpointPaths,
    BazaMailIsReadyToUseResponse,
    BazaMailTemplate,
    replacePathArgs,
} from '@scaliolabs/baza-core-shared';

/**
 * Node-Access Service for Baza-Core - Mail module
 */
export class BazaMailNodeAccess implements BazaMailEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns Ready status for current selected Mail Transport
     */
    async isReadyToUse(): Promise<BazaMailIsReadyToUseResponse> {
        return this.http.get(BazaMailEndpointPaths.isReadyToUse);
    }

    /**
     * Returns list of MailTemplate definitions
     * Used for Documentation Purposed
     */
    async mailTemplates(): Promise<Array<BazaMailTemplate>> {
        return this.http.get(BazaMailEndpointPaths.mailTemplates);
    }

    /**
     * Returns Mail Templates which are associated with given Tag
     * @param tag
     */
    async mailTemplatesOfTag(tag: string): Promise<Array<BazaMailTemplate>> {
        return this.http.get(replacePathArgs(BazaMailEndpointPaths.mailTemplatesOfTag, { tag }));
    }

    /**
     * Returns all Mail Template Tags
     */
    async mailTemplatesTags(): Promise<Array<string>> {
        return this.http.get(BazaMailEndpointPaths.mailTemplatesTags);
    }

    /**
     * Returns Mail Template by Name
     * @param name
     */
    async mailTemplate(name: string): Promise<BazaMailTemplate> {
        return this.http.get(replacePathArgs(BazaMailEndpointPaths.mailTemplate, { name }));
    }
}
