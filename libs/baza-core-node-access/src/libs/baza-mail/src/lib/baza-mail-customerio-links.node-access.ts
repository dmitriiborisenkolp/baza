import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaMailCustomerioLinkDto,
    BazaMailCustomerioLinkEndpoint,
    BazaMailCustomerioLinEndpointPaths,
    BazaMailCustomerioLinkSetRequest,
    replacePathArgs,
    BazaMailCustomerioLinksUnsetRequest,
} from '@scaliolabs/baza-core-shared';

/**
 * Node-Access Service for Baza Mail Customer.IO Links CMS API
 */
export class BazaMailCustomerioLinksNodeAccess implements BazaMailCustomerioLinkEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of Mail Templates with Customer.IO Link associations
     */
    async list(): Promise<Array<BazaMailCustomerioLinkDto>> {
        return this.http.get(BazaMailCustomerioLinEndpointPaths.list);
    }

    /**
     * Returns list of Mail Templates with Customer.IO Link associations
     * Filtered by Tag
     */
    async listByTag(tag: string): Promise<Array<BazaMailCustomerioLinkDto>> {
        return this.http.get(replacePathArgs(BazaMailCustomerioLinEndpointPaths.listByTag, { tag }));
    }

    /**
     * Set Customer.IO Template Id for Mail Template
     * @param request
     */
    async set(request: BazaMailCustomerioLinkSetRequest): Promise<BazaMailCustomerioLinkDto> {
        return this.http.post(BazaMailCustomerioLinEndpointPaths.set, request);
    }

    /**
     * Removes Customer.IO Link for given Mail Template
     * @param request
     */
    async unset(request: BazaMailCustomerioLinksUnsetRequest): Promise<void> {
        return this.http.post(BazaMailCustomerioLinEndpointPaths.unset, request, {
            asJsonResponse: false,
        });
    }
}
