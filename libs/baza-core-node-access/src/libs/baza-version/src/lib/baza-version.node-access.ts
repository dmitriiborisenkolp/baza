import { VersionDto, VersionEndpoint, VersionEndpointPaths } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaVersionNodeAccess implements VersionEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    version(): Promise<VersionDto> {
        return this.http.get(VersionEndpointPaths.version);
    }
}
