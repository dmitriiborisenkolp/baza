import {
    BazaAccountE2eChangeEmailConfirmationExpiryStrategyRequest,
    BazaAccountE2eExpireConfirmationTokenRequest,
    BazaAccountE2eGetConfirmationTokenRequest,
    BazaAccountE2eEndpointPaths,
    BazaAccountE2eEndpoint,
    BazaAccountE2eConfirmationTokenDto,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaAccountE2eNodeAccess implements BazaAccountE2eEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async changeEmailConfirmationExpiryStrategy(request: BazaAccountE2eChangeEmailConfirmationExpiryStrategyRequest): Promise<void> {
        return this.http.post(BazaAccountE2eEndpointPaths.changeEmailConfirmationExpiryStrategy, request, {
            asJsonResponse: false,
        });
    }

    async expireConfirmationToken(request: BazaAccountE2eExpireConfirmationTokenRequest): Promise<void> {
        return this.http.post(BazaAccountE2eEndpointPaths.expireConfirmationToken, request, {
            asJsonResponse: false,
        });
    }

    async getAccountConfirmationToken(request: BazaAccountE2eGetConfirmationTokenRequest): Promise<BazaAccountE2eConfirmationTokenDto> {
        return this.http.post(BazaAccountE2eEndpointPaths.getAccountConfirmationToken, request, {
            asJsonResponse: true,
        });
    }
}
