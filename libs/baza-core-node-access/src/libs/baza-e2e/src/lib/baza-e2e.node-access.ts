import {
    BazaE2eDisableFeatureRequest,
    BazaE2eEnable2FAForAccountRolesRequest,
    BazaE2eEnableFeatureRequest,
    BazaE2eEndpoint,
    BazaE2eEndpointPaths,
    BazaE2eMailDto,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaE2eNodeAccess implements BazaE2eEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async flush(): Promise<void> {
        return this.http.post(
            BazaE2eEndpointPaths.flush,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    async flushDatabase(): Promise<void> {
        return this.http.post(
            BazaE2eEndpointPaths.flushDatabase,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    async flushRedis(): Promise<void> {
        return this.http.post(
            BazaE2eEndpointPaths.flushRedis,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    async flushMailbox(): Promise<void> {
        return this.http.post(
            BazaE2eEndpointPaths.flushMailbox,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    async mailbox(): Promise<Array<BazaE2eMailDto>> {
        return this.http.get(BazaE2eEndpointPaths.mailbox);
    }

    async enable2FAForAccountRoles(request: BazaE2eEnable2FAForAccountRolesRequest): Promise<void> {
        return this.http.post(BazaE2eEndpointPaths.enable2FAForAccountRoles, request, {
            asJsonResponse: false,
        });
    }

    async enableFeature(request: BazaE2eEnableFeatureRequest): Promise<void> {
        return this.http.post(BazaE2eEndpointPaths.enableFeature, request, {
            asJsonResponse: false,
        });
    }

    async disableFeature(request: BazaE2eDisableFeatureRequest): Promise<void> {
        return this.http.post(BazaE2eEndpointPaths.disableFeature, request, {
            asJsonResponse: false,
        });
    }

    async setEnv(request: Record<string, string>): Promise<void> {
        return this.http.post(BazaE2eEndpointPaths.setEnv, request, {
            asJsonResponse: false,
        });
    }

    async resetEnv(): Promise<void> {
        return this.http.post(
            BazaE2eEndpointPaths.resetEnv,
            {},
            {
                asJsonResponse: false,
            },
        );
    }
}
