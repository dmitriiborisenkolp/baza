import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaRegistryCurrentSchemaResponse,
    BazaRegistryEndpoint,
    BazaRegistryEndpointPaths,
    BazaRegistryGetSchemaRecordRequest,
    BazaRegistryGetSchemaRecordResponse,
    BazaRegistryPublicSchema,
    BazaRegistryUpdateSchemaRecordRequest,
    BazaRegistryUpdateSchemaRecordResponse,
} from '@scaliolabs/baza-core-shared';

export class BazaRegistryNodeAccess implements BazaRegistryEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    publicSchema(): Promise<BazaRegistryPublicSchema> {
        return this.http.get(BazaRegistryEndpointPaths.publicSchema);
    }

    currentSchema(): Promise<BazaRegistryCurrentSchemaResponse> {
        return this.http.get(BazaRegistryEndpointPaths.currentSchema);
    }

    getSchemaRecord(request: BazaRegistryGetSchemaRecordRequest): Promise<BazaRegistryGetSchemaRecordResponse> {
        return this.http.post(BazaRegistryEndpointPaths.getSchemaRecord, request);
    }

    updateSchemaRecord(request: BazaRegistryUpdateSchemaRecordRequest): Promise<BazaRegistryUpdateSchemaRecordResponse> {
        return this.http.post(BazaRegistryEndpointPaths.updateSchemaRecord, request, {
            asJsonResponse: false,
        });
    }
}
