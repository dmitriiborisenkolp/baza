import {
    AclEndpoint,
    BazaAclCurrentResponse,
    BazaAclEndpointPaths,
    BazaAclGetAclRequest,
    BazaAclGetAclResponse,
    BazaAclSetAclRequest,
    BazaAclSetAclResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaAclNgNodeAccess implements AclEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    current(): Promise<BazaAclCurrentResponse> {
        return this.http.post(BazaAclEndpointPaths.current);
    }

    getAcl(request: BazaAclGetAclRequest): Promise<BazaAclGetAclResponse> {
        return this.http.post(BazaAclEndpointPaths.getAcl, request);
    }

    setAcl(request: BazaAclSetAclRequest): Promise<BazaAclSetAclResponse> {
        return this.http.post(BazaAclEndpointPaths.setAcl, request);
    }
}
