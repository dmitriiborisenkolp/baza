import { BazaDataAccessNode } from '../../../baza-data-access.node';
import { AccountCmsBootstrapEndpoint, AccountCmsBootstrapEndpointPaths } from '@scaliolabs/baza-core-shared';

export class BazaAccountBootstrapNodeAccess implements AccountCmsBootstrapEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async bootstrap(): Promise<void> {
        return this.http.get(
            AccountCmsBootstrapEndpointPaths.bootstrap,
            {},
            {
                asJsonResponse: false,
            },
        );
    }
}
