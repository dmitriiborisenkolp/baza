import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    AttachmentEndpoint,
    AttachmentEndpointPaths,
    AttachmentUploadRequest,
    AttachmentUploadResponse,
} from '@scaliolabs/baza-core-shared';
import * as FormData from 'form-data';

export class BazaAttachmentNodeAccess implements AttachmentEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async upload(file: File | unknown, request: AttachmentUploadRequest): Promise<AttachmentUploadResponse> {
        const formData = new FormData();

        formData.append('file', file);
        formData.append('request', JSON.stringify(request));

        return this.http.post(AttachmentEndpointPaths.upload, formData, {
            asFormData: true,
        });
    }
}
