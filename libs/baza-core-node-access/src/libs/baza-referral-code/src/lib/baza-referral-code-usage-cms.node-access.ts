import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaReferralCodeCmsUsageByAccountExportListToCSVRequest,
    BazaReferralCodeCmsUsageByAccountListRequest,
    BazaReferralCodeCmsUsageByAccountListResponse,
    BazaReferralCodeCmsUsageByCampaignExportListToCSVRequest,
    BazaReferralCodeCmsUsageByCampaignListRequest,
    BazaReferralCodeCmsUsageByCampaignListResponse,
    BazaReferralCodeCmsUsageByCodeExportListToCSVRequest,
    BazaReferralCodeCmsUsageByCodeListRequest,
    BazaReferralCodeCmsUsageByCodeListResponse,
    BazaReferralCodeUsageCmsEndpoint,
    ReferralCodeUsageCmsEndpointPaths,
} from '@scaliolabs/baza-core-shared';

export class BazaReferralCodeUsageCmsNodeAccess implements BazaReferralCodeUsageCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    listByReferralCode(request: BazaReferralCodeCmsUsageByCodeListRequest): Promise<BazaReferralCodeCmsUsageByCodeListResponse> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.listByReferralCode, request);
    }

    exportListByReferralCodeToCSV(request: BazaReferralCodeCmsUsageByCodeExportListToCSVRequest): Promise<string> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralCodeToCSV, request, {
            asJsonResponse: false,
        });
    }

    listByReferralCampaign(
        request: BazaReferralCodeCmsUsageByCampaignListRequest,
    ): Promise<BazaReferralCodeCmsUsageByCampaignListResponse> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.listByReferralCampaign, request);
    }

    exportListByReferralCampaignToCSV(request: BazaReferralCodeCmsUsageByCampaignExportListToCSVRequest): Promise<string> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralCampaignToCSV, request, {
            asJsonResponse: false,
        });
    }

    listByReferralAccount(request: BazaReferralCodeCmsUsageByAccountListRequest): Promise<BazaReferralCodeCmsUsageByAccountListResponse> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.listByReferralAccount, request);
    }

    exportListByReferralAccountToCSV(request: BazaReferralCodeCmsUsageByAccountExportListToCSVRequest): Promise<string> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralAccountToCSV, request, {
            asJsonResponse: false,
        });
    }
}
