import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    ReferralCampaignCmsCreateRequest,
    ReferralCampaignCmsDeleteRequest,
    ReferralCampaignCmsEditRequest,
    BazaReferralCampaignCmsEndpoint,
    BazaReferralCampaignCmsEndpointPaths,
    ReferralCampaignCmsExportListToCsvRequest,
    ReferralCampaignCmsGetByIdRequest,
    ReferralCampaignCmsGetByIdResponse,
    ReferralCampaignCmsListRequest,
    ReferralCampaignCmsListResponse,
    BazaReferralCampaignDto,
} from '@scaliolabs/baza-core-shared';

export class BazaReferralCampaignCmsNodeAccess implements BazaReferralCampaignCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    list(request: ReferralCampaignCmsListRequest): Promise<ReferralCampaignCmsListResponse> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.list, request);
    }

    getById(request: ReferralCampaignCmsGetByIdRequest): Promise<ReferralCampaignCmsGetByIdResponse> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.getById, request);
    }

    create(request: ReferralCampaignCmsCreateRequest): Promise<BazaReferralCampaignDto> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.create, request);
    }

    edit(request: ReferralCampaignCmsEditRequest): Promise<BazaReferralCampaignDto> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.edit, request);
    }

    delete(request: ReferralCampaignCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    exportListToCSV(request: ReferralCampaignCmsExportListToCsvRequest): Promise<string> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.exportListToCSV, request, {
            asJsonResponse: false,
        });
    }
}
