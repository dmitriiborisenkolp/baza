import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaReferralCodeCmsCreateRequest,
    BazaReferralCodeCmsDeleteRequest,
    BazaReferralCodeCmsEditRequest,
    BazaReferralCodeCmsEndpoint,
    BazaReferralCodeCmsEndpointPaths,
    BazaReferralCodeCmsExportListToCSVRequest,
    BazaReferralCodeCmsGetByIdRequest,
    BazaReferralCodeCmsGetByIdResponse,
    BazaReferralCodeCmsListResponse,
    BazaReferralCodeDto,
    BazaReferralCodeCmsListRequest,
} from '@scaliolabs/baza-core-shared';

export class BazaReferralCodeCmsNodeAccess implements BazaReferralCodeCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    list(request: BazaReferralCodeCmsListRequest): Promise<BazaReferralCodeCmsListResponse> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.list, request);
    }

    getById(request: BazaReferralCodeCmsGetByIdRequest): Promise<BazaReferralCodeCmsGetByIdResponse> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.getById, request);
    }

    create(request: BazaReferralCodeCmsCreateRequest): Promise<BazaReferralCodeDto> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.create, request);
    }

    edit(request: BazaReferralCodeCmsEditRequest): Promise<BazaReferralCodeDto> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.edit, request);
    }

    delete(request: BazaReferralCodeCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    exportListToCSV(request: BazaReferralCodeCmsExportListToCSVRequest): Promise<string> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.exportListToCSV, request, {
            asJsonResponse: false,
        });
    }
}
