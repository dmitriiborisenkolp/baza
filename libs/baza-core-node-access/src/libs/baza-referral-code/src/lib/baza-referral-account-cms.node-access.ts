import {
    BazaReferralAccountCmsEndpoint,
    BazaReferralAccountCmsEndpointPaths,
    BazaReferralAccountCmsExportToCsvListRequest,
    BazaReferralAccountCmsListRequest,
    BazaReferralAccountCmsListResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaReferralAccountCmsNodeAccess implements BazaReferralAccountCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    list(request: BazaReferralAccountCmsListRequest): Promise<BazaReferralAccountCmsListResponse> {
        return this.http.post(BazaReferralAccountCmsEndpointPaths.list, request);
    }

    exportListToCSV(request: BazaReferralAccountCmsExportToCsvListRequest): Promise<string> {
        return this.http.post(BazaReferralAccountCmsEndpointPaths.exportListToCSV, request);
    }
}
