import {
    BazaPasswordEndpoint,
    BazaPasswordEndpointPaths,
    BazaPasswordResourcesDto,
    BazaPasswordValidateDto,
    BazaPasswordValidateRequest,
} from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaPasswordNodeAccess implements BazaPasswordEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    resources(): Promise<BazaPasswordResourcesDto> {
        return this.http.get(BazaPasswordEndpointPaths.resources);
    }

    validate(request: BazaPasswordValidateRequest): Promise<BazaPasswordValidateDto> {
        return this.http.post(BazaPasswordEndpointPaths.validate, request);
    }
}
