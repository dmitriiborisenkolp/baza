import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaInviteCodeCmsEndpoint,
    BazaCreateInviteCodeRequest,
    BazaDeleteInviteCodeRequest,
    BazaInviteCodeCmsEndpointPaths,
    BazaInviteCodeGetByIdRequest,
    BazaListOfSharedInviteCodesRequest,
    BazaListOfSharedInviteCodesResponse,
    BazaUpdateInviteCodeRequest,
    BazaInviteCodeCmsDto,
} from '@scaliolabs/baza-core-shared';

export class BazaInviteCodesCmsNodeAccess implements BazaInviteCodeCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async getById(request: BazaInviteCodeGetByIdRequest): Promise<BazaInviteCodeCmsDto> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.getById, request);
    }

    async listOfSharedInviteCodes(request: BazaListOfSharedInviteCodesRequest): Promise<BazaListOfSharedInviteCodesResponse> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.listOfSharedInviteCodes, request);
    }

    async createInviteCode(request: BazaCreateInviteCodeRequest): Promise<BazaInviteCodeCmsDto> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.createInviteCode, request);
    }

    async updateInviteCode(request: BazaUpdateInviteCodeRequest): Promise<BazaInviteCodeCmsDto> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.updateInviteCode, request);
    }

    async deleteInviteCode(request: BazaDeleteInviteCodeRequest): Promise<void> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.deleteInviteCode, request, {
            asJsonResponse: false,
        });
    }
}
