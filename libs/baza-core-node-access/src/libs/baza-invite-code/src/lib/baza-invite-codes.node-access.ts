import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaInviteCodeEndpoint,
    BazaInviteCodeEndpointPaths,
    BazaRequestForInviteCodesRequest,
    BazaRequestForInviteCodesResponse,
    BazaValidateInviteCodeRequest,
    BazaValidateInviteCodeResponse,
} from '@scaliolabs/baza-core-shared';

export class BazaInviteCodesNodeAccess implements BazaInviteCodeEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async validateInviteCode(request: BazaValidateInviteCodeRequest): Promise<BazaValidateInviteCodeResponse> {
        return this.http.post(BazaInviteCodeEndpointPaths.validateInviteCode, request);
    }

    async requestForInviteCodes(request: BazaRequestForInviteCodesRequest): Promise<BazaRequestForInviteCodesResponse> {
        return this.http.post(BazaInviteCodeEndpointPaths.requestForInviteCodes, request);
    }
}
