import { BazaDataAccessNode } from '../../../baza-data-access.node';
import {
    BazaAuthSessionCmsAccountsRequest,
    BazaAuthSessionCmsAccountsResponse,
    BazaAuthSessionCmsEndpoint,
    BazaAuthSessionCmsEndpointPaths,
    BazaAuthSessionCmsSessionsRequest,
    BazaAuthSessionCmsSessionsResponse,
} from '@scaliolabs/baza-core-shared';

export class BazaAuthSessionCmsNodeAccess implements BazaAuthSessionCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async accounts(request: BazaAuthSessionCmsAccountsRequest): Promise<BazaAuthSessionCmsAccountsResponse> {
        return this.http.post(BazaAuthSessionCmsEndpointPaths.accounts, request);
    }

    async sessions(request: BazaAuthSessionCmsSessionsRequest): Promise<BazaAuthSessionCmsSessionsResponse> {
        return this.http.post(BazaAuthSessionCmsEndpointPaths.sessions, request);
    }
}
