import { BazaHealthCheckEndpointPaths, BazaHealthCheckResponse, HealthCheckEndpoint } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '../../../baza-data-access.node';

export class BazaHealthNodeAccess implements HealthCheckEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    check(): Promise<BazaHealthCheckResponse> {
        return this.http.get(BazaHealthCheckEndpointPaths.check);
    }
}
