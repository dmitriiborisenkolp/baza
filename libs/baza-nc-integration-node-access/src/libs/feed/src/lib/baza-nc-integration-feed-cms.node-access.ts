import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationFeedCmsEndpoint,
    FeedCmsPaths,
    FeedCmsPostsCreateRequest,
    FeedCmsPostsCreateResponse,
    FeedCmsPostsDeleteRequest,
    FeedCmsPostsDeleteResponse,
    FeedCmsPostsGetByIdRequest,
    FeedCmsPostsGetByIdResponse,
    FeedCmsPostsListRequest,
    FeedCmsPostsListResponse,
    FeedCmsPostsUpdateRequest,
    FeedCmsPostsUpdateResponse,
    FeedCmsSetSortOrderRequest,
} from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedCmsNodeAccess implements BazaNcIntegrationFeedCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: FeedCmsPostsCreateRequest): Promise<FeedCmsPostsCreateResponse> {
        return this.http.post(FeedCmsPaths.create, request);
    }

    async update(request: FeedCmsPostsUpdateRequest): Promise<FeedCmsPostsUpdateResponse> {
        return this.http.post(FeedCmsPaths.update, request);
    }

    async delete(request: FeedCmsPostsDeleteRequest): Promise<FeedCmsPostsDeleteResponse> {
        return this.http.post(FeedCmsPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: FeedCmsPostsListRequest): Promise<FeedCmsPostsListResponse> {
        return this.http.post(FeedCmsPaths.list, request);
    }

    async getById(request: FeedCmsPostsGetByIdRequest): Promise<FeedCmsPostsGetByIdResponse> {
        return this.http.post(FeedCmsPaths.getById, request);
    }

    async setSortOrder(request: FeedCmsSetSortOrderRequest): Promise<void> {
        return this.http.post(FeedCmsPaths.setSortOrder, request, {
            asJsonResponse: false,
        });
    }
}
