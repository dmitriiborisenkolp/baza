import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationFeedEndpoint,
    FeedApplauseRequest,
    FeedApplauseResponse,
    FeedEndpointPaths,
    FeedGetByIdRequest,
    FeedGetByIdResponse,
    FeedHasAnyUpdatesResponse,
    FeedListRequest,
    FeedListResponse,
    FeedMarkAllAsReadRequest,
    FeedMarkAllAsReadResponse,
    FeedUnapplauseRequest,
    FeedUnapplauseResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BazaNcIntegrationFeedNodeAccess implements BazaNcIntegrationFeedEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: FeedListRequest): Promise<FeedListResponse> {
        return this.http.post(FeedEndpointPaths.list, request);
    }

    async getById(request: FeedGetByIdRequest): Promise<FeedGetByIdResponse> {
        return this.http.post(FeedEndpointPaths.getById, request);
    }

    async applause(request: FeedApplauseRequest): Promise<FeedApplauseResponse> {
        return this.http.post(FeedEndpointPaths.applause, request);
    }

    async unapplause(request: FeedUnapplauseRequest): Promise<FeedUnapplauseResponse> {
        return this.http.post(FeedEndpointPaths.unapplause, request);
    }

    async markAllAsRead(request: FeedMarkAllAsReadRequest): Promise<FeedMarkAllAsReadResponse> {
        return this.http.post(FeedEndpointPaths.markAllAsRead, request);
    }

    async hasAnyUpdates(): Promise<FeedHasAnyUpdatesResponse> {
        return this.http.post(FeedEndpointPaths.hasAnyUpdates);
    }

    async s3ImageUrl(sid: string): Promise<void> {
        return this.http.get(
            replacePathArgs(FeedEndpointPaths.s3ImageUrl, {
                sid,
            }),
        );
    }

    async s3VideoUrl(sid: string): Promise<void> {
        return this.http.get(
            replacePathArgs(FeedEndpointPaths.s3VideoUrl, {
                sid,
            }),
        );
    }
}
