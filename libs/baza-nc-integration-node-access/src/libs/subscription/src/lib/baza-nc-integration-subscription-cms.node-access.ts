import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationSubscriptionCmsEndpoint,
    BazaNcIntegrationSubscriptionCmsEndpointPaths,
    BazaNcIntegrationSubscriptionCmsExcludeRequest,
    BazaNcIntegrationSubscriptionCmsExcludeResponse,
    BazaNcIntegrationSubscriptionCmsExportToCsvRequest,
    BazaNcIntegrationSubscriptionCmsIncludeRequest,
    BazaNcIntegrationSubscriptionCmsIncludeResponse,
    BazaNcIntegrationSubscriptionCmsListRequest,
    BazaNcIntegrationSubscriptionCmsListResponse,
    BazaNcIntegrationSubscriptionCmsNotifyUsersRequest,
} from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationSubscriptionCmsNodeAccess implements BazaNcIntegrationSubscriptionCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: BazaNcIntegrationSubscriptionCmsListRequest): Promise<BazaNcIntegrationSubscriptionCmsListResponse> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.list, request);
    }

    async include(request: BazaNcIntegrationSubscriptionCmsIncludeRequest): Promise<BazaNcIntegrationSubscriptionCmsIncludeResponse> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.include, request);
    }

    async exclude(request: BazaNcIntegrationSubscriptionCmsExcludeRequest): Promise<BazaNcIntegrationSubscriptionCmsExcludeResponse> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.exclude, request, {
            asJsonResponse: false,
        });
    }

    async exportToCsv(request: BazaNcIntegrationSubscriptionCmsExportToCsvRequest): Promise<string> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.exportToCsv, request, {
            asJsonResponse: false,
        });
    }

    async notifyUsers(request: BazaNcIntegrationSubscriptionCmsNotifyUsersRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.notifyUsers, request, {
            asJsonResponse: false,
        });
    }
}
