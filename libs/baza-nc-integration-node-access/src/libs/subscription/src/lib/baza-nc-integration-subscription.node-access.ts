import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationSubscriptionEndpoint,
    BazaNcIntegrationSubscriptionEndpointPaths,
    BazaNcIntegrationSubscribeToRequest,
    BazaNcIntegrationSubscribedResponse,
    BazaNcIntegrationUnsubscribeRequest,
    BazaNcIntegrationUnsubscribedResponse,
} from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationSubscriptionNodeAccess implements BazaNcIntegrationSubscriptionEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async subscribe(request: BazaNcIntegrationSubscribeToRequest): Promise<BazaNcIntegrationSubscribedResponse> {
        return this.http.post(BazaNcIntegrationSubscriptionEndpointPaths.subscribe, request);
    }

    async unsubscribe(request: BazaNcIntegrationUnsubscribeRequest): Promise<BazaNcIntegrationUnsubscribedResponse> {
        return this.http.post(BazaNcIntegrationSubscriptionEndpointPaths.unsubscribe, request);
    }
}
