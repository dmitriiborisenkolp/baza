import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationSchemaCmsCreateRequest,
    BazaNcIntegrationSchemaCmsDeleteRequest,
    BazaNcIntegrationSchemaCmsEndpoint,
    BazaNcIntegrationSchemaCmsEndpointPaths,
    BazaNcIntegrationSchemaCmsGetByIdRequest,
    BazaNcIntegrationSchemaCmsListRequest,
    BazaNcIntegrationSchemaCmsListResponse,
    BazaNcIntegrationSchemaCmsRenameRequest,
    BazaNcIntegrationSchemaCmsSetSortOrderRequest,
    BazaNcIntegrationSchemaCmsUpdateRequest,
    BazaNcIntegrationSchemaDto,
} from '@scaliolabs/baza-nc-integration-shared';

/**
 * Node Access Service for Schema
 */
export class BazaNcIntegrationSchemaCmsNodeAccess implements BazaNcIntegrationSchemaCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Creates a new Schema
     * @param request
     */
    async create(request: BazaNcIntegrationSchemaCmsCreateRequest): Promise<BazaNcIntegrationSchemaDto> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.create, request);
    }

    /**
     * Updates existing Schema
     * @param request
     */
    async update(request: BazaNcIntegrationSchemaCmsUpdateRequest): Promise<BazaNcIntegrationSchemaDto> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.update, request);
    }

    /**
     * Renames existing Schema
     * @param request
     */
    async rename(request: BazaNcIntegrationSchemaCmsRenameRequest): Promise<BazaNcIntegrationSchemaDto> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.rename, request);
    }

    /**
     * Deletes existing Schema
     * @param request
     */
    async delete(request: BazaNcIntegrationSchemaCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    /**
     * Returns list of Schemas
     * @param request
     */
    async list(request: BazaNcIntegrationSchemaCmsListRequest): Promise<BazaNcIntegrationSchemaCmsListResponse> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.list, request);
    }

    /**
     * Returns Schema by ID
     * @param request
     */
    async getById(request: BazaNcIntegrationSchemaCmsGetByIdRequest): Promise<BazaNcIntegrationSchemaDto> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.getById, request);
    }

    async setSortOrder(request: BazaNcIntegrationSchemaCmsSetSortOrderRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.setSortOrder, request, {
            asJsonResponse: false,
        });
    }
}
