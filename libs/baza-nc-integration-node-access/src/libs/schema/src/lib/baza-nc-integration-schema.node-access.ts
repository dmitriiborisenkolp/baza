import { BazaNcIntegrationSchemaEndpoint, BazaNcIntegrationSchemaEndpointPaths } from '@scaliolabs/baza-nc-integration-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';

/**
 * Baza NC Integration Schema Node-Access Service
 */
export class BazaNcIntegrationSchemaNodeAccess implements BazaNcIntegrationSchemaEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of Schema (Public-Schema only)
     * Use received strings as value(s) for filtering data
     */
    async list(): Promise<Array<string>> {
        return this.http.get(BazaNcIntegrationSchemaEndpointPaths.list);
    }
}
