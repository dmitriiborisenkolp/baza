import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationOperationEndpoint,
    BazaNcIntegrationOperationEndpointPaths,
    BazaNcIntegrationOperationFiltersDto,
    BazaNcIntegrationOperationListRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcOperationListResponse, BazaNcOperationStatsResponse } from '@scaliolabs/baza-nc-shared';

export class BazaNcIntegrationOperationNodeAccess implements BazaNcIntegrationOperationEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns filters & other settings which could be used to fully automate FE implementation
     */
    async filters(): Promise<BazaNcIntegrationOperationFiltersDto> {
        return this.http.get(BazaNcIntegrationOperationEndpointPaths.filters);
    }

    /**
     * Operation stats for Investor Account
     */
    async stats(): Promise<BazaNcOperationStatsResponse> {
        return this.http.get(BazaNcIntegrationOperationEndpointPaths.stats);
    }

    /**
     * Returns operations search results
     */
    async list(request: BazaNcIntegrationOperationListRequest): Promise<BazaNcOperationListResponse> {
        return this.http.get(BazaNcIntegrationOperationEndpointPaths.list, request);
    }
}
