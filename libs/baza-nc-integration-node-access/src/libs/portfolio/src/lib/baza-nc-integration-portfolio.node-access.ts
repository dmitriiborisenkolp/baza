import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationPortfolioAssetsRequest,
    BazaNcIntegrationPortfolioAssetsResponse,
    BazaNcIntegrationPortfolioEndpoint,
    PortfolioEndpointPaths,
    BazaNcIntegrationPortfolioStatementsRequest,
    BazaNcIntegrationPortfolioStatementsResponse,
    BazaNcIntegrationPortfolioTotalStatsDto,
    BazaNcIntegrationPortfolioTransactionsRequest,
    BazaNcIntegrationPortfolioTransactionsResponse,
} from '@scaliolabs/baza-nc-integration-shared';

/**
 * Node Access Service for Portfolio
 */
export class BazaNcIntegrationPortfolioNodeAccess implements BazaNcIntegrationPortfolioEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns aggregated numbers for Total block in Portfolio
     */
    async total(): Promise<BazaNcIntegrationPortfolioTotalStatsDto> {
        return this.http.get(PortfolioEndpointPaths.total);
    }

    /**
     * Returns Assets List (v1)
     * @param request
     */
    async assets(request: BazaNcIntegrationPortfolioAssetsRequest): Promise<BazaNcIntegrationPortfolioAssetsResponse> {
        return this.http.post(PortfolioEndpointPaths.assets, request);
    }

    /**
     * Returns Assets List (v2)
     * @param request
     */
    async assetsTotal(request: BazaNcIntegrationPortfolioAssetsRequest): Promise<BazaNcIntegrationPortfolioAssetsResponse> {
        return this.http.post(PortfolioEndpointPaths.assetsTotal, request);
    }

    /**
     * DEPRECATED - Returns list of transactions
     * @deprecated
     * @param request
     */
    async transactions(request: BazaNcIntegrationPortfolioTransactionsRequest): Promise<BazaNcIntegrationPortfolioTransactionsResponse> {
        return this.http.post(PortfolioEndpointPaths.transactions, request);
    }

    /**
     * Returns list of Statement Documents of Purchased Offerings
     * @param request
     */
    async statements(request: BazaNcIntegrationPortfolioStatementsRequest): Promise<BazaNcIntegrationPortfolioStatementsResponse> {
        return this.http.post(PortfolioEndpointPaths.statements, request);
    }
}
