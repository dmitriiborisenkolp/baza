import {
    BazaNcIntegrationEliteInvestorsCmsEndpoint,
    BazaNcIntegrationEliteInvestorsCmsEndpointPaths,
    BazaNcIntegrationEliteInvestorsCmsExcludeRequest,
    BazaNcIntegrationEliteInvestorsCmsExcludeResponse,
    BazaNcIntegrationEliteInvestorsCmsGetByIdRequest,
    BazaNcIntegrationEliteInvestorsCmsGetByIdResponse,
    BazaNcIntegrationEliteInvestorsCmsIncludeRequest,
    BazaNcIntegrationEliteInvestorsCmsIncludeResponse,
    BazaNcIntegrationEliteInvestorsCmsListRequest,
    BazaNcIntegrationEliteInvestorsCmsListResponse,
    BazaNcIntegrationEliteInvestorsCmsSyncAccountRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';

export class BazaNcIntegrationEliteInvestorsCmsNodeAccess implements BazaNcIntegrationEliteInvestorsCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async include(request: BazaNcIntegrationEliteInvestorsCmsIncludeRequest): Promise<BazaNcIntegrationEliteInvestorsCmsIncludeResponse> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.include, request);
    }

    async exclude(request: BazaNcIntegrationEliteInvestorsCmsExcludeRequest): Promise<BazaNcIntegrationEliteInvestorsCmsExcludeResponse> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.exclude, request, {
            asJsonResponse: false,
        });
    }

    async list(request: BazaNcIntegrationEliteInvestorsCmsListRequest): Promise<BazaNcIntegrationEliteInvestorsCmsListResponse> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.list, request);
    }

    async getById(request: BazaNcIntegrationEliteInvestorsCmsGetByIdRequest): Promise<BazaNcIntegrationEliteInvestorsCmsGetByIdResponse> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.getById, request);
    }

    async syncAccount(request: BazaNcIntegrationEliteInvestorsCmsSyncAccountRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.syncAccount, request, {
            asJsonResponse: false,
        });
    }

    async syncAllAccounts(): Promise<void> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.syncAllAccounts, undefined, {
            asJsonResponse: false,
        });
    }
}
