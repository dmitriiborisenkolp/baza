import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationFavoriteEndpoint,
    BazaNcIntegrationFavoriteEndpointPaths,
    BazaNcIntegrationFavoriteExcludeRequest,
    BazaNcIntegrationFavoriteIncludeRequest,
} from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFavoriteNodeAccess implements BazaNcIntegrationFavoriteEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async include(request: BazaNcIntegrationFavoriteIncludeRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationFavoriteEndpointPaths.include, request, {
            asJsonResponse: false,
        });
    }

    async exclude(request: BazaNcIntegrationFavoriteExcludeRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationFavoriteEndpointPaths.exclude, request, {
            asJsonResponse: false,
        });
    }
}
