import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingTestimonialCmsDto,
    BazaNcIntegrationListingTestimonialCmsEndpoint,
    BazaNcIntegrationListingTestimonialCmsEndpointPaths,
    BazaNcIntegrationListingTestimonialCmsCreateRequest,
    BazaNcIntegrationListingTestimonialCmsDeleteRequest,
    BazaNcIntegrationListingTestimonialCmsGetByUlidRequest,
    BazaNcIntegrationListingTestimonialCmsSetSortOrderRequest,
    BazaNcIntegrationListingTestimonialCmsUpdateRequest,
    BazaNcIntegrationListingTestimonialCmsListRequest,
    BazaNcIntegrationListingTestimonialCmsListResponse,
} from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationListingTestimonialCmsNodeAccess implements BazaNcIntegrationListingTestimonialCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: BazaNcIntegrationListingTestimonialCmsCreateRequest): Promise<BazaNcIntegrationListingTestimonialCmsDto> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.create, request);
    }

    async update(request: BazaNcIntegrationListingTestimonialCmsUpdateRequest): Promise<BazaNcIntegrationListingTestimonialCmsDto> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.update, request);
    }

    async delete(request: BazaNcIntegrationListingTestimonialCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: BazaNcIntegrationListingTestimonialCmsListRequest): Promise<BazaNcIntegrationListingTestimonialCmsListResponse> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.list, request);
    }

    async getByUlid(request: BazaNcIntegrationListingTestimonialCmsGetByUlidRequest): Promise<BazaNcIntegrationListingTestimonialCmsDto> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.getByUlid, request);
    }

    async setSortOrder(request: BazaNcIntegrationListingTestimonialCmsSetSortOrderRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.setSortOrder, request, {
            asJsonResponse: false,
        });
    }
}
