import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingTestimonialDto,
    BazaNcIntegrationListingTestimonialEndpoint,
    BazaNcIntegrationListingTestimonialEndpointPaths,
    BazaNcIntegrationListingTestimonialListRequest,
    BazaNcIntegrationListingTestimonialListResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BazaNcIntegrationListingTestimonialNodeAccess implements BazaNcIntegrationListingTestimonialEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(
        ulid: string,
        request: BazaNcIntegrationListingTestimonialListRequest,
    ): Promise<BazaNcIntegrationListingTestimonialListResponse> {
        return this.http.get(
            replacePathArgs(BazaNcIntegrationListingTestimonialEndpointPaths.list, {
                ulid,
            }),
            request,
        );
    }

    async getByUlid(ulid: string): Promise<BazaNcIntegrationListingTestimonialDto> {
        return this.http.get(
            replacePathArgs(BazaNcIntegrationListingTestimonialEndpointPaths.getByUlid, {
                ulid,
            }),
        );
    }
}
