import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaNcSearchCmsEndpoint, BazaNcSearchCmsEndpointPaths } from '@scaliolabs/baza-nc-integration-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

/**
 * Node Access Service for Search CMS API
 */
export class BazaNcIntegrationSearchCmsNodeAccess implements BazaNcSearchCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Reindex specific Listing for Search API
     * @param ulid
     */
    async reindex(ulid: string): Promise<void> {
        return this.http.post(replacePathArgs(BazaNcSearchCmsEndpointPaths.reindex, { ulid }), undefined, {
            asJsonResponse: false,
        });
    }

    /**
     * Reindex all Listings for Search API
     */
    async reindexAll(): Promise<void> {
        return this.http.post(BazaNcSearchCmsEndpointPaths.reindex, undefined, {
            asJsonResponse: false,
        });
    }
}
