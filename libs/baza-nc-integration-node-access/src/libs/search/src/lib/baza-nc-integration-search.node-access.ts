import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcSearchEndpoint,
    BazaNcSearchEndpointPaths,
    BazaNcSearchRangesResponseDto,
    BazaNcSearchRequestBaseDto,
    BazaNcSearchRequestDto,
    BazaNcSearchResponseDto,
} from '@scaliolabs/baza-nc-integration-shared';

/**
 * Baza NC Integration Search Node-Access Service
 */
export class BazaNcIntegrationSearchNodeAccess implements BazaNcSearchEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Search & Filter Listings
     */
    async search(request: BazaNcSearchRequestDto): Promise<BazaNcSearchResponseDto> {
        return this.http.post(BazaNcSearchEndpointPaths.search, request);
    }

    /**
     * Search & Filter Listings (Returns Array of Listing ULIDs only)
     */
    searchULIDs(request: BazaNcSearchRequestBaseDto): Promise<Array<string>> {
        return this.http.post(BazaNcSearchEndpointPaths.searchULIDs, request);
    }

    /**
     * Search & Filter Listings (Returns Array of Offering IDs only)
     */
    searchOfferingIds(request: BazaNcSearchRequestBaseDto): Promise<Array<string>> {
        return this.http.post(BazaNcSearchEndpointPaths.searchOfferingIds, request);
    }

    /**
     * Returns possible values for filters
     */
    async ranges(): Promise<BazaNcSearchRangesResponseDto> {
        return this.http.get(BazaNcSearchEndpointPaths.ranges);
    }
}
