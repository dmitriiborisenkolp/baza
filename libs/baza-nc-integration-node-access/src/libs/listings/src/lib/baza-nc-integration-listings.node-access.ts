import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsDto,
    BazaNcIntegrationListingsEndpoint,
    BazaNcIntegrationListingsEndpointPaths,
    BazaNcIntegrationListingsGetByIdRequest,
    BazaNcIntegrationListingsGetByOfferingIdRequest,
    BazaNcIntegrationListingsListRequest,
    BazaNcIntegrationListingsListResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BazaNcIntegrationListingsNodeAccess implements BazaNcIntegrationListingsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: BazaNcIntegrationListingsListRequest): Promise<BazaNcIntegrationListingsListResponse> {
        const qp: Record<string, unknown> = {
            ...request,
        };

        if (Array.isArray(request.schemas) && request.schemas.length) {
            qp.schemas = request.schemas.join(',');
        }

        return this.http.get(BazaNcIntegrationListingsEndpointPaths.list, qp);
    }

    async getById(request: BazaNcIntegrationListingsGetByIdRequest): Promise<BazaNcIntegrationListingsDto> {
        return this.http.get(
            replacePathArgs(BazaNcIntegrationListingsEndpointPaths.getById, {
                id: request.id,
            }),
        );
    }

    async getByOfferingId(request: BazaNcIntegrationListingsGetByOfferingIdRequest): Promise<BazaNcIntegrationListingsDto> {
        return this.http.get(
            replacePathArgs(BazaNcIntegrationListingsEndpointPaths.getByOfferingId, {
                offeringId: request.offeringId,
            }),
        );
    }
}
