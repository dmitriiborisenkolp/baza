import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationChangeStatusRequest,
    BazaNcIntegrationListingsCmsCreateRequest,
    BazaNcIntegrationListingsCmsDeleteRequest,
    BazaNcIntegrationListingsCmsDto,
    BazaNcIntegrationListingsCmsEndpoint,
    BazaNcIntegrationListingsCmsEndpointPaths,
    BazaNcIntegrationListingsCmsGetByIdRequest,
    BazaNcIntegrationListingsCmsGetByUlidRequest,
    BazaNcIntegrationListingsCmsListRequest,
    BazaNcIntegrationListingsCmsListResponse,
    BazaNcIntegrationListingsCmsUpdateRequest,
    BazaNcIntegrationListingsListItemDto,
    BazaNcIntegrationListingsCmsSetSortOrderRequest,
    BazaNcIntegrationListingsCmsSortOrderResponse,
} from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationListingsCmsNodeAccess implements BazaNcIntegrationListingsCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: BazaNcIntegrationListingsCmsCreateRequest): Promise<BazaNcIntegrationListingsCmsDto> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.create, request);
    }

    async update(request: BazaNcIntegrationListingsCmsUpdateRequest): Promise<BazaNcIntegrationListingsCmsDto> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.update, request);
    }

    async delete(request: BazaNcIntegrationListingsCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: BazaNcIntegrationListingsCmsListRequest): Promise<BazaNcIntegrationListingsCmsListResponse> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.list, request);
    }

    async listAll(): Promise<Array<BazaNcIntegrationListingsListItemDto>> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.listAll);
    }

    async getById(request: BazaNcIntegrationListingsCmsGetByIdRequest): Promise<BazaNcIntegrationListingsCmsDto> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.getById, request);
    }

    async getByUlid(request: BazaNcIntegrationListingsCmsGetByUlidRequest): Promise<BazaNcIntegrationListingsCmsDto> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.getByUlid, request);
    }

    async changeStatus(request: BazaNcIntegrationChangeStatusRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.changeStatus, request, {
            asJsonResponse: false,
        });
    }

    async setSortOrder(request: BazaNcIntegrationListingsCmsSetSortOrderRequest): Promise<BazaNcIntegrationListingsCmsSortOrderResponse> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.setSortOrder, request);
    }
}
