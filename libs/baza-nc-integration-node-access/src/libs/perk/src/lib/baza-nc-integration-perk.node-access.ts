import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationPerkDto,
    BazaNcIntegrationPerkEndpoint,
    BazaNcIntegrationPerkEndpointPaths,
} from '@scaliolabs/baza-nc-integration-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BazaNcIntegrationPerkNodeAccess implements BazaNcIntegrationPerkEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async getById(id: number): Promise<BazaNcIntegrationPerkDto> {
        return this.http.get(replacePathArgs(BazaNcIntegrationPerkEndpointPaths.getById, { id }));
    }

    async list(listingId: number): Promise<Array<BazaNcIntegrationPerkDto>> {
        return this.http.get(replacePathArgs(BazaNcIntegrationPerkEndpointPaths.list, { listingId }));
    }
}
