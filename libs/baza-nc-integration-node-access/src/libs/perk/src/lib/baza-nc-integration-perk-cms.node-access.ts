import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationPerkCmsCreateRequest,
    BazaNcIntegrationPerkCmsDeleteRequest,
    BazaNcIntegrationPerkCmsEndpoint,
    BazaNcIntegrationPerkCmsEndpointPaths,
    BazaNcIntegrationPerkCmsGetByIdRequest,
    BazaNcIntegrationPerkCmsListRequest,
    BazaNcIntegrationPerkCmsListResponse,
    BazaNcIntegrationPerkCmsSetSortOrderRequest,
    BazaNcIntegrationPerkCmsUpdateRequest,
    BazaNcIntegrationPerkDto,
} from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationPerkCmsNodeAccess implements BazaNcIntegrationPerkCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: BazaNcIntegrationPerkCmsCreateRequest): Promise<BazaNcIntegrationPerkDto> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.create, request);
    }

    async update(request: BazaNcIntegrationPerkCmsUpdateRequest): Promise<BazaNcIntegrationPerkDto> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.update, request);
    }

    async delete(request: BazaNcIntegrationPerkCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async getById(request: BazaNcIntegrationPerkCmsGetByIdRequest): Promise<BazaNcIntegrationPerkDto> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.getById, request);
    }

    async list(request: BazaNcIntegrationPerkCmsListRequest): Promise<BazaNcIntegrationPerkCmsListResponse> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.list, request);
    }

    async setSortOrder(request: BazaNcIntegrationPerkCmsSetSortOrderRequest): Promise<void> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.setSortOrder, request, {
            asJsonResponse: false,
        });
    }
}
