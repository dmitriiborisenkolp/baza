import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationInvestmentsEndpointPaths,
    BazaNcIntegrationsInvestmentsListRequest,
    BazaNcIntegrationsInvestmentsListResponse,
} from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationInvestmentsNodeAccess {
    constructor(private readonly http: BazaDataAccessNode) {}

    async investments(request: BazaNcIntegrationsInvestmentsListRequest): Promise<BazaNcIntegrationsInvestmentsListResponse> {
        return this.http.post(BazaNcIntegrationInvestmentsEndpointPaths.investments, request);
    }
}
