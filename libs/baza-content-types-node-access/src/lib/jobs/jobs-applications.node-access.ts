import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { JobApplicationEndpoint, JobApplicationEndpointPaths, JobApplicationSendRequest } from '@scaliolabs/baza-content-types-shared';

export class JobsApplicationsNodeAccess implements JobApplicationEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async send(request: JobApplicationSendRequest): Promise<void> {
        return this.http.post(JobApplicationEndpointPaths.send, request, {
            asJsonResponse: false,
        });
    }
}
