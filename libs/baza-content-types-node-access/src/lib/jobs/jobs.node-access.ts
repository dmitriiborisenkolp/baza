import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { JobDto, JobEndpoint, JobEndpointPaths, JobsListRequest, JobsListResponse } from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class JobsNodeAccess implements JobEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: JobsListRequest): Promise<JobsListResponse> {
        return this.http.get(JobEndpointPaths.list, request);
    }

    async getAll(): Promise<Array<JobDto>> {
        return this.http.get(JobEndpointPaths.getAll);
    }

    async getById(id: number): Promise<JobDto> {
        return this.http.get(replacePathArgs(JobEndpointPaths.getById, { id }));
    }

    async getByUrl(url: string): Promise<JobDto> {
        return this.http.get(replacePathArgs(JobEndpointPaths.getByUrl, { url }));
    }
}
