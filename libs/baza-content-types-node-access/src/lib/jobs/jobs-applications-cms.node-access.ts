import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    JobApplicationCmsDeleteRequest,
    JobApplicationCmsEndpoint,
    JobApplicationCmsEndpointPaths,
    JobApplicationCmsGetByIdRequest,
    JobApplicationCmsListRequest,
    JobApplicationCmsListResponse,
    JobApplicationDto,
} from '@scaliolabs/baza-content-types-shared';

export class JobsApplicationsCmsNodeAccess implements JobApplicationCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: JobApplicationCmsListRequest): Promise<JobApplicationCmsListResponse> {
        return this.http.post(JobApplicationCmsEndpointPaths.list, request);
    }

    async getById(request: JobApplicationCmsGetByIdRequest): Promise<JobApplicationDto> {
        return this.http.post(JobApplicationCmsEndpointPaths.getById, request);
    }

    async delete(request: JobApplicationCmsDeleteRequest): Promise<void> {
        return this.http.post(JobApplicationCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }
}
