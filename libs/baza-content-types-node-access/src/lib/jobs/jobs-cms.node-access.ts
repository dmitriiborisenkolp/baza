import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    JobCmsDto,
    JobCmsEndpoint,
    JobCmsEndpointPaths,
    JobsCmsCreateRequest,
    JobsCmsDeleteRequest,
    JobsCmsGetByIdRequest,
    JobsCmsListRequest,
    JobsCmsListResponse,
    JobsCmsSetSortOrderRequest,
    JobsCmsSetSortOrderResponse,
    JobsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';

export class JobsCmsNodeAccess implements JobCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: JobsCmsCreateRequest): Promise<JobCmsDto> {
        return this.http.post(JobCmsEndpointPaths.create, request);
    }

    async update(request: JobsCmsUpdateRequest): Promise<JobCmsDto> {
        return this.http.post(JobCmsEndpointPaths.update, request);
    }

    async delete(request: JobsCmsDeleteRequest): Promise<void> {
        return this.http.post(JobCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: JobsCmsListRequest): Promise<JobsCmsListResponse> {
        return this.http.post(JobCmsEndpointPaths.list, request);
    }

    async getById(request: JobsCmsGetByIdRequest): Promise<JobCmsDto> {
        return this.http.post(JobCmsEndpointPaths.getById, request);
    }

    async setSortOrder(request: JobsCmsSetSortOrderRequest): Promise<JobsCmsSetSortOrderResponse> {
        return this.http.post(JobCmsEndpointPaths.setSortOrder, request);
    }
}
