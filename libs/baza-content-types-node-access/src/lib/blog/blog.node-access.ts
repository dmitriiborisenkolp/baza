import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BlogDto, BlogEndpoint, BlogEndpointPaths, BlogListResponse } from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BlogNodeAccess implements BlogEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(): Promise<BlogListResponse> {
        return this.http.get(BlogEndpointPaths.list);
    }

    async getById(id: number): Promise<BlogDto> {
        return this.http.get(
            replacePathArgs(BlogEndpointPaths.getById, {
                id,
            }),
        );
    }
}
