import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
  BlogCmsCreateRequest,
  BlogCmsDeleteRequest,
  BlogCmsDto,
  BlogCmsEndpoint,
  BlogCmsEndpointPaths,
  BlogCmsGetByIdRequest,
  BlogCmsListRequest,
  BlogCmsListResponse,
  BlogCmsSetSortOrderRequest,
  BlogCmsSetSortOrderResponse,
  BlogCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';

export class BlogCmsNodeAccess implements BlogCmsEndpoint {
  constructor(private readonly http: BazaDataAccessNode) {}

  async create(request: BlogCmsCreateRequest): Promise<BlogCmsDto> {
    return this.http.post(BlogCmsEndpointPaths.create, request);
  }

  async update(request: BlogCmsUpdateRequest): Promise<BlogCmsDto> {
    return this.http.post(BlogCmsEndpointPaths.update, request);
  }

  async delete(request: BlogCmsDeleteRequest): Promise<void> {
    return this.http.post(BlogCmsEndpointPaths.delete, request, {
      asJsonResponse: false,
    });
  }

  async getById(request: BlogCmsGetByIdRequest): Promise<BlogCmsDto> {
    return this.http.post(BlogCmsEndpointPaths.getById, request);
  }

  async list(request: BlogCmsListRequest): Promise<BlogCmsListResponse> {
    return this.http.post(BlogCmsEndpointPaths.list, request);
  }

  async setSortOrder(request: BlogCmsSetSortOrderRequest): Promise<BlogCmsSetSortOrderResponse> {
    return this.http.post(BlogCmsEndpointPaths.setSortOrder, request);
  }
}
