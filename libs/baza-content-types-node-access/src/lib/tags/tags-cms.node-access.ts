import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    TagCmsDto,
    TagsCmsCreateRequest,
    TagsCmsDeleteRequest,
    TagsCmsEndpoint,
    TagsCmsEndpointPaths,
    TagsCmsGetByIdRequest,
    TagsCmsListRequest,
    TagsCmsListResponse,
    TagsCmsSetSortOrderRequest,
    TagsCmsSetSortOrderResponse,
    TagsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';

export class TagsCmsNodeAccess implements TagsCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: TagsCmsCreateRequest): Promise<TagCmsDto> {
        return this.http.post(TagsCmsEndpointPaths.create, request);
    }

    async update(request: TagsCmsUpdateRequest): Promise<TagCmsDto> {
        return this.http.post(TagsCmsEndpointPaths.update, request);
    }

    async delete(request: TagsCmsDeleteRequest): Promise<void> {
        return this.http.post(TagsCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: TagsCmsListRequest): Promise<TagsCmsListResponse> {
        return this.http.post(TagsCmsEndpointPaths.list, request);
    }

    async getById(request: TagsCmsGetByIdRequest): Promise<TagCmsDto> {
        return this.http.post(TagsCmsEndpointPaths.getById, request);
    }

    async getAll(): Promise<Array<TagCmsDto>> {
        return this.http.post(TagsCmsEndpointPaths.getAll);
    }

    async setSortOrder(request: TagsCmsSetSortOrderRequest): Promise<TagsCmsSetSortOrderResponse> {
        return this.http.post(TagsCmsEndpointPaths.setSortOrder, request);
    }
}
