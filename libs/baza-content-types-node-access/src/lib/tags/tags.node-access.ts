import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { TagDto, TagEndpoint, TagEndpointPaths } from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class TagsNodeAccess implements TagEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async getAll(): Promise<Array<TagDto>> {
        return this.http.get(TagEndpointPaths.getAll);
    }

    getById(id: number): Promise<TagDto> {
        return this.http.get(replacePathArgs(TagEndpointPaths.getById, { id }));
    }

    getByUrl(url: string): Promise<TagDto> {
        return this.http.get(replacePathArgs(TagEndpointPaths.getByUrl, { url }));
    }
}
