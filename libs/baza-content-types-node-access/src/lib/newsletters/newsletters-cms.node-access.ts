import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    NewsletterCmsAddRequest,
    NewsletterCmsEndpoint,
    NewsletterCmsEndpointPaths,
    NewsletterCmsExportToCsvExportRequest,
    NewsletterCmsListRequest,
    NewsletterCmsListResponse,
    NewsletterCmsRemoveRequest,
    NewslettersDto,
} from '@scaliolabs/baza-content-types-shared';

export class NewslettersCmsNodeAccess implements NewsletterCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: NewsletterCmsListRequest): Promise<NewsletterCmsListResponse> {
        return this.http.post(NewsletterCmsEndpointPaths.list, request);
    }

    async exportToCSV(request: NewsletterCmsExportToCsvExportRequest): Promise<string> {
        return this.http.post(NewsletterCmsEndpointPaths.exportToCSV, request, {
            asJsonResponse: false,
        });
    }

    async add(request: NewsletterCmsAddRequest): Promise<NewslettersDto> {
        return this.http.post(NewsletterCmsEndpointPaths.add, request);
    }

    async remove(request: NewsletterCmsRemoveRequest): Promise<void> {
        return this.http.post(NewsletterCmsEndpointPaths.remove, request, {
            asJsonResponse: false,
        });
    }
}
