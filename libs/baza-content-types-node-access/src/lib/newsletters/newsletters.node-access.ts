import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    NewsletterEndpoint,
    NewsletterEndpointPaths,
    NewsletterSubscribeRequest,
    NewsletterUnsubscribeRequest,
} from '@scaliolabs/baza-content-types-shared';

export class NewslettersNodeAccess implements NewsletterEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async subscribe(request: NewsletterSubscribeRequest): Promise<void> {
        return this.http.post(NewsletterEndpointPaths.subscribe, request, {
            asJsonResponse: false,
        });
    }

    async unsubscribe(request: NewsletterUnsubscribeRequest): Promise<void> {
        return this.http.post(NewsletterEndpointPaths.unsubscribe, request, {
            asJsonResponse: false,
        });
    }
}
