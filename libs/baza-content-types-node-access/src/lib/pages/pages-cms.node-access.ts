import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    PageCmsDto,
    PagesCmsCreateRequest,
    PagesCmsDeleteRequest,
    PagesCmsEndpoint,
    PagesCmsEndpointPaths,
    PagesCmsGetByIdRequest,
    PagesCmsListRequest,
    PagesCmsListResponse,
    PagesCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';

export class PagesCmsNodeAccess implements PagesCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: PagesCmsCreateRequest): Promise<PageCmsDto> {
        return this.http.post(PagesCmsEndpointPaths.create, request);
    }

    async update(request: PagesCmsUpdateRequest): Promise<PageCmsDto> {
        return this.http.post(PagesCmsEndpointPaths.update, request);
    }

    async delete(request: PagesCmsDeleteRequest): Promise<void> {
        return this.http.post(PagesCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: PagesCmsListRequest): Promise<PagesCmsListResponse> {
        return this.http.post(PagesCmsEndpointPaths.list, request);
    }

    async getById(request: PagesCmsGetByIdRequest): Promise<PageCmsDto> {
        return this.http.post(PagesCmsEndpointPaths.getById, request);
    }
}
