import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { PageDto, PagesEndpoint, PagesEndpointPaths } from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class PagesNodeAccess implements PagesEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async get(url: string): Promise<PageDto> {
        return this.http.get(replacePathArgs(PagesEndpointPaths.get, { url }));
    }
}
