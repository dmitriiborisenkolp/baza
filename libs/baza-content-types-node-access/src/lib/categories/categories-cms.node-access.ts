import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    CategoriesCmsEndpoint,
    CategoriesCmsEndpointPaths,
    CategoriesDto,
    CategoryDto,
    CategoriesCmsCreateRequest,
    CategoriesDeleteRequest,
    CategoriesUpdateRequest,
    CategoriesCmsSetSortOrderRequest,
    BazaContentTypesCmsSetSortOrderResponse,
} from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class CategoriesCmsNodeAccess implements CategoriesCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: CategoriesCmsCreateRequest): Promise<CategoryDto> {
        return this.http.post(CategoriesCmsEndpointPaths.create, request);
    }

    async update(request: CategoriesUpdateRequest): Promise<CategoryDto> {
        return this.http.post(CategoriesCmsEndpointPaths.update, request);
    }

    async delete(request: CategoriesDeleteRequest): Promise<void> {
        return this.http.post(CategoriesCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async getAll(): Promise<CategoriesDto> {
        return this.http.get(CategoriesCmsEndpointPaths.getAll);
    }

    async getById(id: number): Promise<CategoryDto> {
        return this.http.get(
            replacePathArgs(CategoriesCmsEndpointPaths.getById, {
                id,
            }),
        );
    }

    async setSortOrder(request: CategoriesCmsSetSortOrderRequest): Promise<BazaContentTypesCmsSetSortOrderResponse> {
        return this.http.post(CategoriesCmsEndpointPaths.setSortOrder, request);
    }
}
