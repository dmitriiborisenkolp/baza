import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { CategoriesDto, CategoriesEndpoint, CategoriesEndpointPaths, CategoryDto } from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class CategoriesNodeAccess implements CategoriesEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    get(urlOrId: string | number): Promise<CategoryDto> {
        return this.http.get(
            replacePathArgs(CategoriesEndpointPaths.get, {
                urlOrId,
            }),
        );
    }

    async getAll(): Promise<CategoriesDto> {
        return this.http.get(CategoriesEndpointPaths.getAll);
    }
}
