import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { FaqEndpoint, FaqEndpointPaths, FaqListResponse } from '@scaliolabs/baza-content-types-shared';

export class FaqNodeAccess implements FaqEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(): Promise<FaqListResponse> {
        return this.http.get(FaqEndpointPaths.list);
    }
}
