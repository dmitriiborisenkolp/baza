import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    FaqCmsCreateResponse,
    FaqCmsCreateRequest,
    FaqCmsEndpoint,
    FaqCmsListRequest,
    FaqCmsListResponse,
    FaqCmsSetSortOrderRequest,
    FaqCmsSetSortOrderResponse,
    FaqCmsUpdateRequest,
    FaqCmsRemoveRequest,
    FaqCmsGetByIdRequest,
    FaqCmsUpdateResponse,
    FaqCmsRemoveResponse,
    FaqCmsGetByIdResponse,
} from '@scaliolabs/baza-content-types-shared';
import { FaqCmsEndpointPaths } from '@scaliolabs/baza-content-types-shared';

export class FaqCmsNodeAccess implements FaqCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: FaqCmsListRequest): Promise<FaqCmsListResponse> {
        return this.http.post(FaqCmsEndpointPaths.list, request);
    }

    async create(request: FaqCmsCreateRequest): Promise<FaqCmsCreateResponse> {
        return this.http.post(FaqCmsEndpointPaths.create, request);
    }

    async update(request: FaqCmsUpdateRequest): Promise<FaqCmsUpdateResponse> {
        return this.http.post(FaqCmsEndpointPaths.update, request);
    }

    async remove(request: FaqCmsRemoveRequest): Promise<FaqCmsRemoveResponse> {
        return this.http.post(FaqCmsEndpointPaths.remove, request, {
            asJsonResponse: false,
        });
    }

    async getById(request: FaqCmsGetByIdRequest): Promise<FaqCmsGetByIdResponse> {
        return this.http.post(FaqCmsEndpointPaths.getById, request);
    }

    async setSortOrder(request: FaqCmsSetSortOrderRequest): Promise<FaqCmsSetSortOrderResponse> {
        return this.http.post(FaqCmsEndpointPaths.setSortOrder, request);
    }
}
