import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    EventDto,
    EventsEndpoint,
    EventsEndpointPaths,
    EventsListRequest,
    EventsListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class EventsNodeAccess implements EventsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: EventsListRequest): Promise<EventsListResponse> {
        return this.http.get(EventsEndpointPaths.list, request);
    }

    async getAll(): Promise<Array<EventDto>> {
        return this.http.get(EventsEndpointPaths.getAll);
    }

    async getById(id: number): Promise<EventDto> {
        return this.http.get(replacePathArgs(EventsEndpointPaths.getById, { id }));
    }

    async getByUrl(url: string): Promise<EventDto> {
        return this.http.get(replacePathArgs(EventsEndpointPaths.getByUrl, { url }));
    }
}
