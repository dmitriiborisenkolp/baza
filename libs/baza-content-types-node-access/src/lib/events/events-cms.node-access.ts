import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    EventCmsDto,
    EventsCmsCreateRequest,
    EventsCmsDeleteRequest,
    EventsCmsEndpoint,
    EventsCmsEndpointPaths,
    EventsCmsGetByIdRequest,
    EventsCmsListRequest,
    EventsCmsListResponse,
    EventsCmsSetSortOrderRequest,
    EventsCmsSetSortOrderResponse,
    EventsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';

export class EventsCmsNodeAccess implements EventsCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: EventsCmsCreateRequest): Promise<EventCmsDto> {
        return this.http.post(EventsCmsEndpointPaths.create, request);
    }

    async update(request: EventsCmsUpdateRequest): Promise<EventCmsDto> {
        return this.http.post(EventsCmsEndpointPaths.update, request);
    }

    async delete(request: EventsCmsDeleteRequest): Promise<void> {
        return this.http.post(EventsCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: EventsCmsListRequest): Promise<EventsCmsListResponse> {
        return this.http.post(EventsCmsEndpointPaths.list, request);
    }

    async getById(request: EventsCmsGetByIdRequest): Promise<EventCmsDto> {
        return this.http.post(EventsCmsEndpointPaths.getById, request);
    }

    async setSortOrder(request: EventsCmsSetSortOrderRequest): Promise<EventsCmsSetSortOrderResponse> {
        return this.http.post(EventsCmsEndpointPaths.setSortOrder, request);
    }
}
