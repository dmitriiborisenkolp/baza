import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    TestimonialCmsDto,
    TestimonialsCmsCreateRequest,
    TestimonialsCmsDeleteRequest,
    TestimonialsCmsEndpoint,
    TestimonialsCmsEndpointPaths,
    TestimonialsCmsGetByIdRequest,
    TestimonialsCmsListRequest,
    TestimonialsCmsListResponse,
    TestimonialsCmsSetSortOrderRequest,
    TestimonialsCmsSetSortOrderResponse,
    TestimonialsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';

export class TestimonialsCmsNodeAccess implements TestimonialsCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: TestimonialsCmsCreateRequest): Promise<TestimonialCmsDto> {
        return this.http.post(TestimonialsCmsEndpointPaths.create, request);
    }

    async update(request: TestimonialsCmsUpdateRequest): Promise<TestimonialCmsDto> {
        return this.http.post(TestimonialsCmsEndpointPaths.update, request);
    }

    async delete(request: TestimonialsCmsDeleteRequest): Promise<void> {
        return this.http.post(TestimonialsCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: TestimonialsCmsListRequest): Promise<TestimonialsCmsListResponse> {
        return this.http.post(TestimonialsCmsEndpointPaths.list, request);
    }

    async getById(request: TestimonialsCmsGetByIdRequest): Promise<TestimonialCmsDto> {
        return this.http.post(TestimonialsCmsEndpointPaths.getById, request);
    }

    async setSortOrder(request: TestimonialsCmsSetSortOrderRequest): Promise<TestimonialsCmsSetSortOrderResponse> {
        return this.http.post(TestimonialsCmsEndpointPaths.setSortOrder, request);
    }
}
