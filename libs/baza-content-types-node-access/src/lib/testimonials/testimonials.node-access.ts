import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    TestimonialDto,
    TestimonialsEndpoint,
    TestimonialsEndpointPaths,
    TestimonialsListRequest,
    TestimonialsListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class TestimonialsNodeAccess implements TestimonialsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: TestimonialsListRequest): Promise<TestimonialsListResponse> {
        return this.http.get(TestimonialsEndpointPaths.list, request);
    }

    async getAll(): Promise<Array<TestimonialDto>> {
        return this.http.get(TestimonialsEndpointPaths.getAll);
    }

    async getById(id: number): Promise<TestimonialDto> {
        return this.http.get(replacePathArgs(TestimonialsEndpointPaths.getById, { id }));
    }

    async getByUrl(url: string): Promise<TestimonialDto> {
        return this.http.get(replacePathArgs(TestimonialsEndpointPaths.getByUrl, { url }));
    }
}
