import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { TimelineDto, TimelineEndpoint, TimelineEndpointPaths } from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class TimelineNodeAccess implements TimelineEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async getAll(): Promise<Array<TimelineDto>> {
        return this.http.get(TimelineEndpointPaths.getAll);
    }

    async getById(id: number): Promise<TimelineDto> {
        return this.http.get(replacePathArgs(TimelineEndpointPaths.getById, { id }));
    }
}
