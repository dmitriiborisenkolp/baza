import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    TimelineCmsCreateRequest,
    TimelineCmsDeleteRequest,
    TimelineCmsDto,
    TimelineCmsEndpoint,
    TimelineCmsEndpointPaths,
    TimelineCmsGetByIdRequest,
    TimelineCmsListRequest,
    TimelineCmsListResponse,
    TimelineCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';

export class TimelineCmsNodeAccess implements TimelineCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: TimelineCmsCreateRequest): Promise<TimelineCmsDto> {
        return this.http.post(TimelineCmsEndpointPaths.create, request);
    }

    async update(request: TimelineCmsUpdateRequest): Promise<TimelineCmsDto> {
        return this.http.post(TimelineCmsEndpointPaths.update, request);
    }

    async delete(request: TimelineCmsDeleteRequest): Promise<void> {
        return this.http.post(TimelineCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: TimelineCmsListRequest): Promise<TimelineCmsListResponse> {
        return this.http.post(TimelineCmsEndpointPaths.list, request);
    }

    async getById(request: TimelineCmsGetByIdRequest): Promise<TimelineCmsDto> {
        return this.http.post(TimelineCmsEndpointPaths.getById, request);
    }
}
