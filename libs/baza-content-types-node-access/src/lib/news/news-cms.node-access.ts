import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    NewsCmsCreateRequest,
    NewsCmsDeleteRequest,
    NewsCmsDto,
    NewsCmsEndpoint,
    NewsCmsEndpointPaths,
    NewsCmsGetByIdRequest,
    NewsCmsListRequest,
    NewsCmsListResponse,
    NewsCmsSetSortOrderRequest,
    NewsCmsSetSortOrderResponse,
    NewsCmsUpdateRequest,
} from '@scaliolabs/baza-content-types-shared';

export class NewsCmsNodeAccess implements NewsCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: NewsCmsCreateRequest): Promise<NewsCmsDto> {
        return this.http.post(NewsCmsEndpointPaths.create, request);
    }

    async update(request: NewsCmsUpdateRequest): Promise<NewsCmsDto> {
        return this.http.post(NewsCmsEndpointPaths.update, request);
    }

    async delete(request: NewsCmsDeleteRequest): Promise<void> {
        return this.http.post(NewsCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: NewsCmsListRequest): Promise<NewsCmsListResponse> {
        return this.http.post(NewsCmsEndpointPaths.list, request);
    }

    async getById(request: NewsCmsGetByIdRequest): Promise<NewsCmsDto> {
        return this.http.post(NewsCmsEndpointPaths.getById, request);
    }

    async setSortOrder(request: NewsCmsSetSortOrderRequest): Promise<NewsCmsSetSortOrderResponse> {
        return this.http.post(NewsCmsEndpointPaths.setSortOrder, request);
    }
}
