import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { NewsDto, NewsEndpoint, NewsEndpointPaths, NewsListRequest, NewsListResponse } from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class NewsNodeAccess implements NewsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: NewsListRequest): Promise<NewsListResponse> {
        return this.http.get(NewsEndpointPaths.list, request);
    }

    async getById(id: number): Promise<NewsDto> {
        return this.http.get(replacePathArgs(NewsEndpointPaths.getById, { id }));
    }

    async getByUrl(url: string): Promise<NewsDto> {
        return this.http.get(replacePathArgs(NewsEndpointPaths.getByUrl, { url }));
    }
}
