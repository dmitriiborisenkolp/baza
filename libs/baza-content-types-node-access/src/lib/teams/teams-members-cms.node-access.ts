import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    TeamMemberCmsDto,
    TeamsMemberCmsCreateRequest,
    TeamsMemberCmsEndpoint,
    TeamsMemberCmsEndpointPaths,
    TeamsMemberCmsUpdateRequest,
    TeamsMemberDeleteRequest,
    TeamsMemberGetByIdRequest,
    TeamsMemberListRequest,
    TeamsMemberListResponse,
    TeamsMemberSetSortOrderRequest,
    TeamsMemberSetSortOrderResponse,
} from '@scaliolabs/baza-content-types-shared';

export class TeamsMembersCmsNodeAccess implements TeamsMemberCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    create(request: TeamsMemberCmsCreateRequest): Promise<TeamMemberCmsDto> {
        return this.http.post(TeamsMemberCmsEndpointPaths.create, request);
    }

    update(request: TeamsMemberCmsUpdateRequest): Promise<TeamMemberCmsDto> {
        return this.http.post(TeamsMemberCmsEndpointPaths.update, request);
    }

    delete(request: TeamsMemberDeleteRequest): Promise<void> {
        return this.http.post(TeamsMemberCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    getById(request: TeamsMemberGetByIdRequest): Promise<TeamMemberCmsDto> {
        return this.http.post(TeamsMemberCmsEndpointPaths.getById, request);
    }

    list(request: TeamsMemberListRequest): Promise<TeamsMemberListResponse> {
        return this.http.post(TeamsMemberCmsEndpointPaths.list, request);
    }

    setSortOrder(request: TeamsMemberSetSortOrderRequest): Promise<TeamsMemberSetSortOrderResponse> {
        return this.http.post(TeamsMemberCmsEndpointPaths.setSortOrder, request);
    }
}
