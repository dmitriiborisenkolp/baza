import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    TeamMemberDto,
    TeamsMemberEndpoint,
    TeamsMemberEndpointPaths,
    TeamsMemberGetAllResponse,
} from '@scaliolabs/baza-content-types-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class TeamsMembersNodeAccess implements TeamsMemberEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    getAll(): Promise<TeamsMemberGetAllResponse> {
        return this.http.get(TeamsMemberEndpointPaths.getAll);
    }

    getById(id: number): Promise<TeamMemberDto> {
        return this.http.get(
            replacePathArgs(TeamsMemberEndpointPaths.getById, {
                id,
            }),
        );
    }

    getByUrl(url: string): Promise<TeamMemberDto> {
        return this.http.get(
            replacePathArgs(TeamsMemberEndpointPaths.getByUrl, {
                url,
            }),
        );
    }
}
