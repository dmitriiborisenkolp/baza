import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    ContactsSubjectCmsCreateRequest,
    ContactsSubjectCmsCreateResponse,
    ContactsSubjectCmsDeleteRequest,
    ContactsSubjectCmsEndpoint,
    ContactsSubjectCmsEndpointPaths,
    ContactsSubjectCmsGetByIdRequest,
    ContactsSubjectCmsListRequest,
    ContactsSubjectCmsListResponse,
    ContactsSubjectCmsResponse,
    ContactsSubjectCmsUpdateResponse,
    ContactsSubjectCmsUpdateRequest,
    ContactsSubjectCmsSetSortOrderResponse,
    ContactsSubjectCmsSetSortOrderRequest,
} from '@scaliolabs/baza-content-types-shared';

export class ContactsSubjectCmsNodeAccess implements ContactsSubjectCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    list(request: ContactsSubjectCmsListRequest): Promise<ContactsSubjectCmsListResponse> {
        return this.http.post(ContactsSubjectCmsEndpointPaths.list, request);
    }

    getById(request: ContactsSubjectCmsGetByIdRequest): Promise<ContactsSubjectCmsResponse> {
        return this.http.post(ContactsSubjectCmsEndpointPaths.getById, request);
    }

    create(request: ContactsSubjectCmsCreateRequest): Promise<ContactsSubjectCmsCreateResponse> {
        return this.http.post(ContactsSubjectCmsEndpointPaths.create, request);
    }

    update(request: ContactsSubjectCmsUpdateRequest): Promise<ContactsSubjectCmsUpdateResponse> {
        return this.http.post(ContactsSubjectCmsEndpointPaths.update, request);
    }

    delete(request: ContactsSubjectCmsDeleteRequest): Promise<void> {
        return this.http.post(ContactsSubjectCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    setSortOrder(request: ContactsSubjectCmsSetSortOrderRequest): Promise<ContactsSubjectCmsSetSortOrderResponse> {
        return this.http.post(ContactsSubjectCmsEndpointPaths.setSortOrder, request);
    }
}
