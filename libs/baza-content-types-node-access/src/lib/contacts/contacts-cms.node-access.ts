import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    ContactsCmsEndpoint,
    ContactsCmsEndpointPaths,
    ContactsCmsListRequest,
    ContactsCmsListResponse,
    ContactsCmsMarkAsProcessedRequest,
    ContactsCmsMarkAsUnprocessedRequest,
    ContactsGetByIdRequest,
    ContactsGetByIdResponse,
} from '@scaliolabs/baza-content-types-shared';

export class ContactsCmsNodeAccess implements ContactsCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    list(request: ContactsCmsListRequest): Promise<ContactsCmsListResponse> {
        return this.http.post(ContactsCmsEndpointPaths.list, request);
    }

    getById(request: ContactsGetByIdRequest): Promise<ContactsGetByIdResponse> {
        return this.http.post(ContactsCmsEndpointPaths.getById, request);
    }

    markAsProcessed(request: ContactsCmsMarkAsProcessedRequest): Promise<void> {
        return this.http.post(ContactsCmsEndpointPaths.markAsProcessed, request, {
            asJsonResponse: false,
        });
    }

    markAsUnprocessed(request: ContactsCmsMarkAsUnprocessedRequest): Promise<void> {
        return this.http.post(ContactsCmsEndpointPaths.markAsUnprocessed, request, {
            asJsonResponse: false,
        });
    }
}
