import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    ContactsEndpoint,
    ContactsEndpointPaths,
    ContactsSendRequest,
    ContactsSubjectsResponse,
} from '@scaliolabs/baza-content-types-shared';

export class ContactsNodeAccess implements ContactsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    subjects(): Promise<ContactsSubjectsResponse> {
        return this.http.get(ContactsEndpointPaths.subjects);
    }

    send(request: ContactsSendRequest): Promise<void> {
        return this.http.post(ContactsEndpointPaths.send, request, {
            asJsonResponse: false,
            withJwt: false,
        });
    }
}
