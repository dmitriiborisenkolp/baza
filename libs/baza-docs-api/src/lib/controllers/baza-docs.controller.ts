import { Controller, Get, Query } from '@nestjs/common';
import {
    BazaDocsDto,
    BazaDocsEndpoint,
    BazaDocsEndpointPaths,
    BazaDocsFileDto,
    BazaDocsFileRequest,
    bazaDocsStructure,
} from '@scaliolabs/baza-docs-shared';
import { BazaDocsService } from '../services/baza-docs.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaDocsOpenApi } from '@scaliolabs/baza-docs-shared';
import { withoutTrailingSlash } from '@scaliolabs/baza-core-shared';

@Controller()
@ApiTags(BazaDocsOpenApi.BazaDocs)
export class BazaDocsController implements BazaDocsEndpoint {
    constructor(private readonly service: BazaDocsService) {}

    @Get(BazaDocsEndpointPaths.File)
    @ApiOperation({
        summary: 'file',
        description: 'Returns MD file by relative path',
    })
    @ApiOkResponse({
        type: BazaDocsFileDto,
    })
    async file(@Query() query: BazaDocsFileRequest): Promise<BazaDocsFileDto> {
        return this.service.getMdFile(withoutTrailingSlash(query['relativePath']));
    }

    @Get(BazaDocsEndpointPaths.Structure)
    @ApiOperation({
        summary: 'structure',
        description: 'Returns documentation structure',
    })
    @ApiOkResponse({
        type: BazaDocsDto,
    })
    async structure(): Promise<BazaDocsDto> {
        return bazaDocsStructure;
    }
}
