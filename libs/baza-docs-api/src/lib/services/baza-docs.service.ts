import { Injectable } from '@nestjs/common';
import { BazaDocsFileDto, BazaDocsNode, BazaDocsNodeType, bazaDocsStructure } from '@scaliolabs/baza-docs-shared';
import * as process from 'process';
import * as path from 'path';
import * as fs from 'fs';
import { BazaDocsNotFoundException } from '../exceptions/baza-docs-not-found.exception';
import { isEmpty } from '@scaliolabs/baza-core-shared';

const MD_DOCS_SOURCE_DIR = path.join(process.cwd(), 'docs');

@Injectable()
export class BazaDocsService {
    async getMdFile(relativePath: string): Promise<BazaDocsFileDto> {
        this.validateRelativePath(relativePath);

        const filePath = path.join(MD_DOCS_SOURCE_DIR, relativePath || 'index.md');

        return {
            mdSource: fs.readFileSync(filePath, 'utf8'),
        };
    }

    private validateRelativePath(relativePath: string): void {
        if (isEmpty(relativePath)) {
            return;
        }

        if (relativePath.includes('..')) {
            throw new BazaDocsNotFoundException();
        }

        if (!relativePath.match(/^([a-zA-Z/0-9.-]+)$/)) {
            throw new BazaDocsNotFoundException();
        }

        const traverse = (input: Array<BazaDocsNode>) => {
            for (const node of input) {
                if (node.type === BazaDocsNodeType.Directory) {
                    if (traverse(node.nodes)) {
                        return true;
                    }
                } else if (node.type === BazaDocsNodeType.File) {
                    if (node.path === relativePath || (Array.isArray(node.deps) && node.deps.includes(relativePath))) {
                        return true;
                    }
                }
            }

            return false;
        };

        if (!traverse([bazaDocsStructure.index, ...bazaDocsStructure.nodes])) {
            throw new BazaDocsNotFoundException();
        }

        const filePath = path.join(MD_DOCS_SOURCE_DIR, relativePath || 'index.md');

        if (!fs.existsSync(filePath)) {
            throw new BazaDocsNotFoundException();
        }
    }
}
