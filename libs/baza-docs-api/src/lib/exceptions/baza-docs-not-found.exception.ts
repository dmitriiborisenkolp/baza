import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDocsErrorCodes } from '@scaliolabs/baza-docs-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDocsNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaDocsErrorCodes.BazaDocsMdFileNotFound,
            'Documentation file not found',
            HttpStatus.NOT_FOUND,
        );
    }
}
