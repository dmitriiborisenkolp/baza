import { Module } from '@nestjs/common';
import { BazaDocsService } from './services/baza-docs.service';
import { BazaDocsController } from './controllers/baza-docs.controller';

@Module({
    controllers: [
        BazaDocsController,
    ],
    providers: [
        BazaDocsService,
    ],
})
export class BazaDocsApiModule {
}
