import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcDividendTransactionEntryCmsCreateRequest,
    BazaNcDividendTransactionEntryCmsDeleteRequest,
    BazaNcDividendTransactionEntryCmsEndpoint,
    BazaNcDividendTransactionEntryCmsEndpointPaths,
    BazaNcDividendTransactionEntryCmsGetByUlidRequest,
    BazaNcDividendTransactionEntryCmsListRequest,
    BazaNcDividendTransactionEntryCmsListResponse,
    BazaNcDividendTransactionEntryCmsUpdateRequest,
    BazaNcDividendTransactionEntryDto,
    BazaNcDividendTransactionEntryExportCsvRequest,
    BazaNcDividendTransactionEntryImportCsvRequest,
    BazaNcDividendTransactionEntryImportCsvResponse,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class BazaNcDividendTransactionEntryCmsDataAccess implements BazaNcDividendTransactionEntryCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    create(request: BazaNcDividendTransactionEntryCmsCreateRequest): Observable<BazaNcDividendTransactionEntryDto> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.create, request);
    }

    update(request: BazaNcDividendTransactionEntryCmsUpdateRequest): Observable<BazaNcDividendTransactionEntryDto> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.update, request);
    }

    delete(request: BazaNcDividendTransactionEntryCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.delete, request);
    }

    getByUlid(request: BazaNcDividendTransactionEntryCmsGetByUlidRequest): Observable<BazaNcDividendTransactionEntryDto> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.getByUlid, request);
    }

    list(request: BazaNcDividendTransactionEntryCmsListRequest): Observable<BazaNcDividendTransactionEntryCmsListResponse> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.list, request);
    }

    importCsv(
        uploadedCsvFile: File,
        request: BazaNcDividendTransactionEntryImportCsvRequest,
    ): Observable<BazaNcDividendTransactionEntryImportCsvResponse> {
        const formData = new FormData();

        formData.append('file', uploadedCsvFile);
        formData.append('source', request.source);
        formData.append('dividendTransactionUlid', request.dividendTransactionUlid);

        if (request.csvDelimiter) {
            formData.append('csvDelimiter', request.csvDelimiter);
        }

        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.importCsv, formData);
    }

    exportToCsv(request: BazaNcDividendTransactionEntryExportCsvRequest): Observable<string> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.exportToCsv, request, {
            responseType: 'text',
        });
    }
}
