import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import { BazaDwollaCreatePersonalCustomerResponse, CreateDocumentRequest } from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcDwollaCmsEndpoint,
    BazaNcDwollaCmsEndpointPaths,
    BazaNcDwollaCmsTouchAllRequest,
    BazaNcTouchDwollaCustomerRequest,
    BazaNcTouchDwollaCustomerResponse,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

/**
 * Data-Access Service for Baza NC Dwolla CMS API
 */
@Injectable()
export class BazaNcDwollaCmsDataAccess implements BazaNcDwollaCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    /**
     * Attempts to create a Dwolla Customer for a specific Investor Account
     * @param request
     */
    touch(request: BazaNcTouchDwollaCustomerRequest): Observable<BazaNcTouchDwollaCustomerResponse> {
        return this.http.post(BazaNcDwollaCmsEndpointPaths.touch, request);
    }

    /**
     * Executes "touch" operation for all existing Investor Accounts
     * @param request
     */
    touchAllInvestors(request: BazaNcDwollaCmsTouchAllRequest): Observable<void> {
        return this.http.post(BazaNcDwollaCmsEndpointPaths.touchAllInvestors, request);
    }

    /**
     * Attempts to retry customer verification for Dwolla Customer
     * @param request
     */
    retryVerificationForPersonalCustomer(request: BazaNcTouchDwollaCustomerRequest): Observable<BazaDwollaCreatePersonalCustomerResponse> {
        return this.http.post(BazaNcDwollaCmsEndpointPaths.retryVerification, request);
    }

    /**
     * Attempts to upload document for Dwolla Customer
     * @param file
     * @param request
     */
    createDocumentForCustomer(file: File | Blob | unknown, request: CreateDocumentRequest): Observable<void> {
        const formData = new FormData();

        formData.append('file', file as File);

        for (const key of Object.keys(request)) {
            formData.append(key, request[`${key}`]);
        }

        return this.http.post(BazaNcDwollaCmsEndpointPaths.createDocument, formData, {
            asFormData: true,
            asJsonResponse: false,
        });
    }
}
