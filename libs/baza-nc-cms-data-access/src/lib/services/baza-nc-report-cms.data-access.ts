import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcReportCmsEndpoint,
    BazaNcReportCmsEndpointPaths,
    BazaNcReportCmsExportToCsvRequest,
    BazaNcReportCmsGetByIdRequest,
    BazaNcReportCmsListRequest,
    BazaNcReportCmsListResponse,
    BazaNcReportCmsResendOneRequest,
    BazaNcReportCmsSendManyRequest,
    BazaNcReportCmsSendManyResponse,
    BazaNcReportCmsSendOneRequest,
    BazaNcReportCmsSyncAllRequest,
    BazaNcReportCmsSyncRequest,
    BazaNcReportDto,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcReportCmsDataAccess implements BazaNcReportCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    /**
     * Returns List of Reports
     * @param request
     */
    list(request: BazaNcReportCmsListRequest): Observable<BazaNcReportCmsListResponse> {
        return this.http.post(BazaNcReportCmsEndpointPaths.list, request);
    }

    /**
     * Returns Report by ULID
     * @param request
     */
    getById(request: BazaNcReportCmsGetByIdRequest): Observable<BazaNcReportDto> {
        return this.http.post(BazaNcReportCmsEndpointPaths.getById, request);
    }

    /**
     * Sends Report
     * @param request
     */
    sendOne(request: BazaNcReportCmsSendOneRequest): Observable<BazaNcReportDto> {
        return this.http.post(BazaNcReportCmsEndpointPaths.sendOne, request);
    }

    /**
     * Sends all Reports. Could be filtered by Date From and Date To
     * @param request
     */
    sendMany(request: BazaNcReportCmsSendManyRequest): Observable<BazaNcReportCmsSendManyResponse> {
        return this.http.post(BazaNcReportCmsEndpointPaths.sendMany, request);
    }

    /**
     * Resends Report which was sent recently
     * @param request
     */
    resendOne(request: BazaNcReportCmsResendOneRequest): Observable<BazaNcReportDto> {
        return this.http.post(BazaNcReportCmsEndpointPaths.resendOne, request);
    }

    /**
     * Exports all Reports to CSV. Could be filtered by Date From and Date To
     * @param request
     */
    exportToCsv(request: BazaNcReportCmsExportToCsvRequest): Observable<string> {
        return this.http.post(BazaNcReportCmsEndpointPaths.exportToCsv, request, {
            responseType: 'text',
        });
    }

    /**
     * Sync Status for Report
     * @param request
     */
    sync(request: BazaNcReportCmsSyncRequest): Observable<BazaNcReportDto> {
        return this.http.post(BazaNcReportCmsEndpointPaths.sync, request);
    }

    /**
     * Sync Statuses for all unreported Reports. Works in asyc way.
     */
    syncAll(request: BazaNcReportCmsSyncAllRequest): Observable<void> {
        return this.http.post(BazaNcReportCmsEndpointPaths.syncAll, request);
    }
}
