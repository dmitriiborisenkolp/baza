import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcInvestorAccountCmsEndpoint,
    BazaNcInvestorAccountCmsEndpointPaths,
    BazaNcInvestorAccountUpdatePersonalInformationRequest,
    NcAddInvestorAccountRequest,
    NcAddInvestorAccountResponse,
    NcExportToCsvInvestorAccountRequest,
    NcGetInvestorAccountByUserIdRequest,
    NcGetInvestorAccountByUserIdResponse,
    NcGetInvestorAccountPersonalInfoRequest,
    NcGetInvestorAccountPersonalInfoResponse,
    NcGetInvestorAccountRequest,
    NcGetInvestorAccountResponse,
    NcListInvestorAccountRequest,
    NcListInvestorAccountResponse,
    NcRemoveInvestorAccountRequest,
    NcRemoveInvestorAccountResponse,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcInvestorAccountCmsDataAccess implements BazaNcInvestorAccountCmsEndpoint {
    constructor(private readonly ngEndpoint: BazaCmsDataAccessService) {}

    getInvestorAccount(request: NcGetInvestorAccountRequest): Observable<NcGetInvestorAccountResponse> {
        return this.ngEndpoint.post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccount, request);
    }

    getInvestorAccountByUserId(request: NcGetInvestorAccountByUserIdRequest): Observable<NcGetInvestorAccountByUserIdResponse> {
        return this.ngEndpoint.post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccountByUserId, request);
    }

    listInvestorAccounts(request: NcListInvestorAccountRequest): Observable<NcListInvestorAccountResponse> {
        return this.ngEndpoint.post(BazaNcInvestorAccountCmsEndpointPaths.listInvestorAccounts, request);
    }

    addInvestorAccount(request: NcAddInvestorAccountRequest): Observable<NcAddInvestorAccountResponse> {
        return this.ngEndpoint.post(BazaNcInvestorAccountCmsEndpointPaths.addInvestorAccount, request);
    }

    removeInvestorAccount(request: NcRemoveInvestorAccountRequest): Observable<NcRemoveInvestorAccountResponse> {
        return this.ngEndpoint.post(BazaNcInvestorAccountCmsEndpointPaths.removeInvestorAccount, request);
    }

    exportInvestorAccountsToCsv(request: NcExportToCsvInvestorAccountRequest): Observable<string> {
        return this.ngEndpoint.post(BazaNcInvestorAccountCmsEndpointPaths.exportInvestorAccountsToCsv, request, {
            responseType: 'text',
        });
    }

    getInvestorAccountPersonalInfo(request: NcGetInvestorAccountPersonalInfoRequest): Observable<NcGetInvestorAccountPersonalInfoResponse> {
        return this.ngEndpoint.post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccountPersonalInfo, request);
    }

    updateInvestorAccountPersonalInfo(request: BazaNcInvestorAccountUpdatePersonalInformationRequest): Observable<void> {
        return this.ngEndpoint.post(BazaNcInvestorAccountCmsEndpointPaths.updateInvestorAccountPersonalInfo, request);
    }
}
