import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcOfferingCmsEndpoint,
    BazaNcOfferingCmsEndpointPaths,
    BazaNcOfferingDto,
    BazaNcOfferingListItemDto,
    NcOfferingDetailsRequest,
    NcOfferingDetailsResponse,
    NcOfferingForgetRequest,
    NcOfferingGetByIdRequest,
    NcOfferingGetByIdResponse,
    NcOfferingHealthCheckRequest,
    NcOfferingHealthCheckResponse,
    NcOfferingImportRequest,
    NcOfferingListRequest,
    NcOfferingListResponse,
    NcOfferingSyncAllOfferingsResponse,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcOfferingCmsDataAccess implements BazaNcOfferingCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    list(request: NcOfferingListRequest): Observable<NcOfferingListResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.list, request);
    }

    listAll(): Observable<Array<BazaNcOfferingListItemDto>> {
        return this.http.get(BazaNcOfferingCmsEndpointPaths.listAll);
    }

    getByNcOfferingId(request: NcOfferingGetByIdRequest): Observable<NcOfferingGetByIdResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.getByNcOfferingId, request);
    }

    ncOfferingDetails(request: NcOfferingDetailsRequest): Observable<NcOfferingDetailsResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.ncOfferingDetails, request);
    }

    syncAllOfferings(): Observable<NcOfferingSyncAllOfferingsResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.syncAllOfferings);
    }

    importNcOffering(request: NcOfferingImportRequest): Observable<BazaNcOfferingDto> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.importNcOffering, request);
    }

    forgetNcOffering(request: NcOfferingForgetRequest): Observable<void> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.forgetNcOffering, request);
    }

    healthCheck(request: NcOfferingHealthCheckRequest): Observable<NcOfferingHealthCheckResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.healthCheck, request);
    }
}
