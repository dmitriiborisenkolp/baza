import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcDividendTransactionCmsCreateRequest,
    BazaNcDividendTransactionCmsDeleteRequest,
    BazaNcDividendTransactionCmsEndpoint,
    BazaNcDividendTransactionCmsEndpointPaths,
    BazaNcDividendTransactionCmsExportToCsvRequest,
    BazaNcDividendTransactionCmsGetByUlidRequest,
    BazaNcDividendTransactionCmsListRequest,
    BazaNcDividendTransactionCmsListResponse,
    BazaNcDividendTransactionCmsProcessRequest,
    BazaNcDividendTransactionCmsProcessResponse,
    BazaNcDividendTransactionCmsReprocessRequest,
    BazaNcDividendTransactionCmsReprocessResponse,
    BazaNcDividendTransactionCmsSendProcessConfirmationRequest,
    BazaNcDividendTransactionCmsSyncRequest,
    BazaNcDividendTransactionDto,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcDividendTransactionCmsDataAccess implements BazaNcDividendTransactionCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    create(request: BazaNcDividendTransactionCmsCreateRequest): Observable<BazaNcDividendTransactionDto> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.create, request);
    }

    delete(request: BazaNcDividendTransactionCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.delete, request);
    }

    list(request: BazaNcDividendTransactionCmsListRequest): Observable<BazaNcDividendTransactionCmsListResponse> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.list, request);
    }

    getByUlid(request: BazaNcDividendTransactionCmsGetByUlidRequest): Observable<BazaNcDividendTransactionDto> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.getByUlid, request);
    }

    process(request: BazaNcDividendTransactionCmsProcessRequest): Observable<BazaNcDividendTransactionCmsProcessResponse> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.process, request);
    }

    reprocess(request: BazaNcDividendTransactionCmsReprocessRequest): Observable<BazaNcDividendTransactionCmsReprocessResponse> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.reprocess, request);
    }

    sync(request: BazaNcDividendTransactionCmsSyncRequest): Observable<void> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.sync, request);
    }

    sendProcessConfirmation(request: BazaNcDividendTransactionCmsSendProcessConfirmationRequest): Observable<void> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.sendProcessConfirmation, request);
    }

    exportToCsv(request: BazaNcDividendTransactionCmsExportToCsvRequest): Observable<string> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.exportToCsv, request, {
            responseType: 'text',
        });
    }
}
