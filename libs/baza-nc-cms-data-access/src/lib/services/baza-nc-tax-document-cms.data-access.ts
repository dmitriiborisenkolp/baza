import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcTaxDocumentCmsCreateRequest,
    BazaNcTaxDocumentCmsDeleteRequest,
    BazaNcTaxDocumentCmsDto,
    BazaNcTaxDocumentCmsEndpoint,
    BazaNcTaxDocumentCmsEndpointPaths,
    BazaNcTaxDocumentCmsGetByIdRequest,
    BazaNcTaxDocumentCmsListRequest,
    BazaNcTaxDocumentCmsListResponse,
    BazaNcTaxDocumentCmsUpdateRequest,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcTaxDocumentCmsDataAccess implements BazaNcTaxDocumentCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    create(request: BazaNcTaxDocumentCmsCreateRequest): Observable<BazaNcTaxDocumentCmsDto> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.create, request);
    }

    update(request: BazaNcTaxDocumentCmsUpdateRequest): Observable<BazaNcTaxDocumentCmsDto> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.update, request);
    }

    delete(request: BazaNcTaxDocumentCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.delete, request);
    }

    list(request: BazaNcTaxDocumentCmsListRequest): Observable<BazaNcTaxDocumentCmsListResponse> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.list, request);
    }

    getById(request: BazaNcTaxDocumentCmsGetByIdRequest): Observable<BazaNcTaxDocumentCmsDto> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.getById, request);
    }
}
