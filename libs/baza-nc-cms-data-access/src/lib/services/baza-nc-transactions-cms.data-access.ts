import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcTransactionCmsEndpoint,
    BazaNcTransactionCmsEndpointPaths,
    ExportToCsvTransactionCmsRequest,
    GetByIdTransactionCmsRequest,
    GetByIdTransactionCmsResponse,
    ListTransactionsCmsRequest,
    ListTransactionsCmsResponse,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcTransactionsCmsDataAccess implements BazaNcTransactionCmsEndpoint {
    constructor(private readonly ngEndpoint: BazaCmsDataAccessService) {}

    list(request: ListTransactionsCmsRequest): Observable<ListTransactionsCmsResponse> {
        return this.ngEndpoint.post<ListTransactionsCmsResponse>(BazaNcTransactionCmsEndpointPaths.list, request);
    }

    getById(request: GetByIdTransactionCmsRequest): Observable<GetByIdTransactionCmsResponse> {
        return this.ngEndpoint.post<GetByIdTransactionCmsResponse>(BazaNcTransactionCmsEndpointPaths.getById, request);
    }

    exportToCsv(request: ExportToCsvTransactionCmsRequest): Observable<string> {
        return this.ngEndpoint.post<string>(BazaNcTransactionCmsEndpointPaths.exportToCsv, request, {
            responseType: 'text',
        });
    }
}
