import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcDividendCmsDeleteRequest,
    BazaNcDividendCmsDto,
    BazaNcDividendCmsEndpoint,
    BazaNcDividendCmsEndpointPaths,
    BazaNcDividendCmsExportToCsvRequest,
    BazaNcDividendCmsGetByUlidRequest,
    BazaNcDividendCmsListRequest,
    BazaNcDividendCmsListResponse,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcDividendCmsDataAccess implements BazaNcDividendCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    list(request: BazaNcDividendCmsListRequest): Observable<BazaNcDividendCmsListResponse> {
        return this.http.post(BazaNcDividendCmsEndpointPaths.list, request);
    }

    getByUlid(request: BazaNcDividendCmsGetByUlidRequest): Observable<BazaNcDividendCmsDto> {
        return this.http.post(BazaNcDividendCmsEndpointPaths.getByUlid, request);
    }

    delete(request: BazaNcDividendCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaNcDividendCmsEndpointPaths.delete, request);
    }

    exportToCsv(request: BazaNcDividendCmsExportToCsvRequest): Observable<string> {
        return this.http.post(BazaNcDividendCmsEndpointPaths.exportToCsv, request, {
            responseType: 'text',
        });
    }
}
