import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcAccountVerificationCmsEndpoint,
    BazaNcAccountVerificationCmsEndpointPaths,
    BazaNcCmsAccountVerificationFormResourcesDto,
    ListStatesRequestCmsDto,
    ListStatesResponseCmsDto,
    NcAccountVerificationKycLogsRequest,
    NcAccountVerificationKycLogsResponse,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcAccountVerificationCmsDataAccess implements BazaNcAccountVerificationCmsEndpoint {
    constructor(private readonly ngEndpoint: BazaCmsDataAccessService) {}

    kycLogs(request: NcAccountVerificationKycLogsRequest): Observable<NcAccountVerificationKycLogsResponse> {
        return this.ngEndpoint.post<NcAccountVerificationKycLogsResponse>(BazaNcAccountVerificationCmsEndpointPaths.kycLogs, request);
    }

    formResources(): Observable<BazaNcCmsAccountVerificationFormResourcesDto> {
        return this.ngEndpoint.get<BazaNcCmsAccountVerificationFormResourcesDto>(BazaNcAccountVerificationCmsEndpointPaths.formResources);
    }

    listStates(request: ListStatesRequestCmsDto): Observable<ListStatesResponseCmsDto> {
        return this.ngEndpoint.post<ListStatesResponseCmsDto>(BazaNcAccountVerificationCmsEndpointPaths.listStates, request);
    }
}
