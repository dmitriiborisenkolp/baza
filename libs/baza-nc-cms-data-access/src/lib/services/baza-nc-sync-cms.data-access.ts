import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcSyncEndpoint,
    NcSyncEndpointPaths,
    NcSyncInvestorAccountPaymentMethodsRequest,
    NcSyncInvestorAccountRequest,
    NcSyncInvestorAccountResponse,
    NcSyncPercentsFundedForOfferingRequest,
    NcSyncRemoveInvestorAccountRequest,
    NcSyncRemoveInvestorAccountResponse,
    NcSyncTransactionsRequest,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

/**
 * Baza NC Sync Data Access Service
 */
@Injectable()
export class BazaNcSyncCmsDataAccess implements BazaNcSyncEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    /**
     * Sync all investor accounts and investor account transactions
     */
    syncAllInvestorAccounts(): Observable<void> {
        return this.http.post(NcSyncEndpointPaths.syncAllInvestorAccounts);
    }

    /**
     * Sync investor account and transactions
     * @param request
     */
    syncInvestorAccount(request: NcSyncInvestorAccountRequest): Observable<NcSyncInvestorAccountResponse> {
        return this.http.post(NcSyncEndpointPaths.syncInvestorAccount, request);
    }

    /**
     * Sync investor account payment methods
     * @param request
     */
    syncInvestorAccountPaymentMethods(request: NcSyncInvestorAccountPaymentMethodsRequest): Observable<void> {
        return this.http.post(NcSyncEndpointPaths.syncInvestorAccountPaymentMethods, request);
    }

    /**
     * Sync payment methods for every investor account
     */
    syncAllInvestorAccountsPaymentMethods(): Observable<void> {
        return this.http.post(NcSyncEndpointPaths.syncAllInvestorAccountsPaymentMethods);
    }

    /**
     * Sync transactions only for investor account
     * @param request
     */
    syncTransactions(request: NcSyncTransactionsRequest): Observable<void> {
        return this.http.post(NcSyncEndpointPaths.syncTransactions, request);
    }

    /**
     * Sync percents funded for all offerings
     */
    syncPercentsFunded(): Observable<void> {
        return this.http.post(NcSyncEndpointPaths.syncPercentsFunded);
    }

    /**
     * Sync percents funded for offering
     * @param request
     */
    syncPercentsFundedOfOffering(request: NcSyncPercentsFundedForOfferingRequest): Observable<void> {
        return this.http.post(NcSyncEndpointPaths.syncPercentsFundedOfOffering, request);
    }

    /**
     * Remove (exclude) previously synced data
     * @param request
     */
    syncRemove(request: NcSyncRemoveInvestorAccountRequest): Observable<NcSyncRemoveInvestorAccountResponse> {
        return this.http.post(NcSyncEndpointPaths.syncRemove, request);
    }
}
