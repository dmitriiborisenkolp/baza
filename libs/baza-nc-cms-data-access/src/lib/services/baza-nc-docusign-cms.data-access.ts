import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcDocusignCmsEndpoint,
    BazaNcDocusignDto,
    NcDocusignCmsEndpointPaths,
    NcDocusignCmsGetByIdRequest,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcDocusignCmsDataAccess implements BazaNcDocusignCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    getAll(): Observable<Array<BazaNcDocusignDto>> {
        return this.http.post(NcDocusignCmsEndpointPaths.getAll);
    }

    getById(request: NcDocusignCmsGetByIdRequest): Observable<BazaNcDocusignDto> {
        return this.http.post(NcDocusignCmsEndpointPaths.getById, request);
    }
}
