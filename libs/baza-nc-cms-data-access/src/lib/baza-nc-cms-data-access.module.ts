import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcAccountVerificationCmsDataAccess } from './services/baza-nc-account-verification-cms.data-access';
import { BazaNcDividendCmsDataAccess } from './services/baza-nc-dividend-cms.data-access';
import { BazaNcDividendTransactionCmsDataAccess } from './services/baza-nc-dividend-transaction-cms.data-access';
import { BazaNcDividendTransactionEntryCmsDataAccess } from './services/baza-nc-dividend-transaction-entry-cms.data-access';
import { BazaNcDocusignCmsDataAccess } from './services/baza-nc-docusign-cms.data-access';
import { BazaNcDwollaCmsDataAccess } from './services/baza-nc-dwolla-cms.data-access';
import { BazaNcInvestorAccountCmsDataAccess } from './services/baza-nc-investor-account-cms.data-access';
import { BazaNcOfferingCmsDataAccess } from './services/baza-nc-offering-cms.data-access';
import { BazaNcReportCmsDataAccess } from './services/baza-nc-report-cms.data-access';
import { BazaNcSyncCmsDataAccess } from './services/baza-nc-sync-cms.data-access';
import { BazaNcTaxDocumentCmsDataAccess } from './services/baza-nc-tax-document-cms.data-access';
import { BazaNcTransactionsCmsDataAccess } from './services/baza-nc-transactions-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [
        BazaNcAccountVerificationCmsDataAccess,
        BazaNcInvestorAccountCmsDataAccess,
        BazaNcOfferingCmsDataAccess,
        BazaNcSyncCmsDataAccess,
        BazaNcTransactionsCmsDataAccess,
        BazaNcTaxDocumentCmsDataAccess,
        BazaNcDocusignCmsDataAccess,
        BazaNcDividendCmsDataAccess,
        BazaNcDividendTransactionCmsDataAccess,
        BazaNcDividendTransactionEntryCmsDataAccess,
        BazaNcDwollaCmsDataAccess,
        BazaNcReportCmsDataAccess,
    ],
})
export class BazaNcCmsDataAccessModule {}
