# Changelog

All notable changes of @scaliolabs/baza-core-api are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2021-06-15

### Added

Add latest changes from Baza.

## [1.1.0] - 2021-05-17

### Added

Add `baza-bundles` package 

## [1.0.0] - 2021-05-13

### Added

Add initial version of package
