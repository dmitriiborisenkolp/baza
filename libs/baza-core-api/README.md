# baza-core-api

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-core-api` to execute the unit tests via [Jest](https://jestjs.io).

## Build

Run `nx build baza-core-api`

## Publish

Run `yarn publish:baza-core-api`
