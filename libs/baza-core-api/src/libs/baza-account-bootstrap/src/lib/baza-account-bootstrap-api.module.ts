import { Module } from '@nestjs/common';
import { BazaAccountApiModule } from '../../../baza-account/src';
import { BazaEnvModule } from '../../../baza-env/src';
import { AccountCmsBootstrapController } from './controllers/account-cms-bootstrap.controller';
import { BazaAccountBootstrapService } from './services/baza-account-bootstrap.service';

@Module({
    imports: [BazaAccountApiModule, BazaEnvModule],
    controllers: [AccountCmsBootstrapController],
    providers: [BazaAccountBootstrapService],
    exports: [BazaAccountBootstrapService],
})
export class BazaAccountBootstrapApiModule {}
