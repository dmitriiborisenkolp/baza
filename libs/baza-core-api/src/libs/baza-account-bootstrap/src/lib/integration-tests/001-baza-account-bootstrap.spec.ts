import 'reflect-metadata';
import {
    BazaAccountBootstrapNodeAccess,
    BazaAccountNodeAccess,
    BazaAuthNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { AccountRole, AuthErrorCodes, BazaCoreE2eFixtures, BazaError, CoreAcl, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-account-bootstrap/integration-tests/001-baza-account-bootstrap.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessBootstrap = new BazaAccountBootstrapNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    let idUser: number;
    let idRoot: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
    });

    afterAll(async () => {
        await dataAccessE2e.resetEnv();
    });

    it('will set BAZA_MANAGED_USERS environment', async () => {
        const response = await dataAccessE2e.setEnv({
            BAZA_MANAGED_USERS:
                'admin scalio-admin-test@scal.io scalio-admin-test-password, user scalio-user-test@scal.io scalio-user-test-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will execute bootstrap', async () => {
        const response = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to auth with user', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-user-test@scal.io',
            password: 'scalio-user-test-password',
            requireRole: AccountRole.User,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.jwtPayload.accountRole).toBe(AccountRole.User);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
        expect(response.jwtPayload.accountAcl).toEqual([]);

        idUser = response.jwtPayload.accountId;
    });

    it('will allow to auth with admin', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-admin-test@scal.io',
            password: 'scalio-admin-test-password',
            requireRole: AccountRole.Admin,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.jwtPayload.accountRole).toBe(AccountRole.Admin);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
        expect(response.jwtPayload.accountAcl).toEqual([CoreAcl.SystemRoot]);

        idRoot = response.jwtPayload.accountId;
    });

    it('will execute bootstrap again', async () => {
        const response = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will still allow to auth with user', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-user-test@scal.io',
            password: 'scalio-user-test-password',
            requireRole: AccountRole.User,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.jwtPayload.accountRole).toBe(AccountRole.User);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
        expect(response.jwtPayload.accountId).toBe(idUser);
        expect(response.jwtPayload.accountAcl).toEqual([]);
    });

    it('will still allow to auth with admin', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-admin-test@scal.io',
            password: 'scalio-admin-test-password',
            requireRole: AccountRole.Admin,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.jwtPayload.accountRole).toBe(AccountRole.Admin);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
        expect(response.jwtPayload.accountId).toBe(idRoot);
        expect(response.jwtPayload.accountAcl).toEqual([CoreAcl.SystemRoot]);
    });

    it('will auth & call bootstrap with same data again', async () => {
        await http.auth({
            email: 'scalio-user-test@scal.io',
            password: 'scalio-user-test-password',
        });

        const response = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to call /current endpoint with same JWT', async () => {
        const response = await dataAccessAccount.currentAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(idUser);
        expect(response.email).toBe('scalio-user-test@scal.io');
    });

    it('will bootstrap default e2e users', async () => {
        const response = await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will change BAZA_MANAGED_USERS to different one', async () => {
        const response = await dataAccessE2e.setEnv({
            BAZA_MANAGED_USERS:
                'admin scalio-root-admin@scal.io scalio-root-password, user scalio-user-test@scal.io scalio-user-test-password, user scalio-user-2@scal.io scalio-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will execute bootstrap with new data', async () => {
        const response = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will still allow to auth with default e2e users', async () => {
        const responseUser = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
            requireRole: AccountRole.User,
        });

        expect(isBazaErrorResponse(responseUser)).toBeFalsy();

        const responseAdmin = await dataAccessAuth.auth({
            email: 'e2e-admin@scal.io',
            password: 'e2e-admin-password',
            requireRole: AccountRole.Admin,
        });

        expect(isBazaErrorResponse(responseAdmin)).toBeFalsy();
    });

    it('will not allow to auth with old managed account', async () => {
        const response: BazaError = (await dataAccessAuth.auth({
            email: 'scalio-admin-test@scal.io',
            password: 'scalio-admin-test-password',
            requireRole: AccountRole.Admin,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will not allow to auth with old managed user account', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-user-test@scal.io',
            password: 'scalio-user-test-password',
            requireRole: AccountRole.User,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountId).toBe(idUser);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
    });

    it('will allow to sign in with new managed user account', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-user-2@scal.io',
            password: 'scalio-user-password',
            requireRole: AccountRole.User,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountId).not.toBe(idUser);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
    });

    it('will allow to sign in with new managed admin account', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-root-admin@scal.io',
            password: 'scalio-root-password',
            requireRole: AccountRole.Admin,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountId).not.toBe(idRoot);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();

        idRoot = response.jwtPayload.accountId;
    });

    it('will update password with BAZA_MANAGED_USERS for user', async () => {
        const response = await dataAccessE2e.setEnv({
            BAZA_MANAGED_USERS:
                'admin scalio-root-admin@scal.io scalio-root-password, user scalio-user-test@scal.io scalio-user-test-password-new, user scalio-user-2@scal.io scalio-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const bootstrap = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(bootstrap)).toBeFalsy();
    });

    it('will allow to sign in with new password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-user-test@scal.io',
            password: 'scalio-user-test-password-new',
            requireRole: AccountRole.User,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountId).toBe(idUser);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
    });

    it('will now allow to sign in with old password', async () => {
        const response: BazaError = (await dataAccessAuth.auth({
            email: 'scalio-user-test@scal.io',
            password: 'scalio-user-test-password',
            requireRole: AccountRole.User,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will update password with BAZA_MANAGED_USERS for admin', async () => {
        const response = await dataAccessE2e.setEnv({
            BAZA_MANAGED_USERS:
                'admin scalio-root-admin@scal.io scalio-root-password-new, user scalio-user-test@scal.io scalio-user-test-password-new, user scalio-user-2@scal.io scalio-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const bootstrap = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(bootstrap)).toBeFalsy();
    });

    it('will allow to sign in with admin account with new password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'scalio-root-admin@scal.io',
            password: 'scalio-root-password-new',
            requireRole: AccountRole.Admin,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountId).toBe(idRoot);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
    });
});
