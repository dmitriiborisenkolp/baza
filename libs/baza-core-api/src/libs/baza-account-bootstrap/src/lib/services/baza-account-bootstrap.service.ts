import { Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { RedisService } from '@liaoliaots/nestjs-redis';
import {
    BazaAccountCmsService,
    BazaAccountManagedUser,
    BazaAccountManagedUsersService,
    BazaAccountRepository,
} from '../../../../baza-account/src';
import { BazaLogger } from '../../../../baza-logger/src';
import { BcryptService } from '../../../../baza-common/src';
import { AclService } from '../../../../baza-acl/src';
import { EnvService } from '../../../../baza-env/src';
import { AccountUnknownRoleException } from '../exceptions/account-unknown-role.exception';

const REDIS_KEY = 'BAZA_ACCOUNT_BOOTSTRAP_LOCK';
const REDIS_LOCK_TTL = 5;

/**
 * Default accounts for Baza
 * These account will be automatically created if BAZA_MANAGED_USERS is not set
 * By default Baza will create `scalio-admin@scal.io` admin account with Scalio#1337! password
 * By default Baza will create `scalio-user@scal.io` user account with Scalio#1337! password
 * @see BAZA_DEFAULT_ACCOUNTS_PASSWORD
 */
export const BAZA_DEFAULT_ACCOUNTS: Array<{
    email: string;
    firstName: string;
    lastName: string;
    role: AccountRole;
}> = [
    {
        email: 'scalio-admin@scal.io',
        firstName: 'CMS',
        lastName: 'Administrator',
        role: AccountRole.Admin,
    },
    {
        email: 'scalio-user@scal.io',
        firstName: 'John',
        lastName: 'Doe',
        role: AccountRole.User,
    },
];

/**
 * Default password for bootstrap accounts
 */
export const BAZA_DEFAULT_ACCOUNTS_PASSWORD = 'Scalio#1337!';

/**
 * Default name for managed administrators
 */
export const BAZA_DEFAULT_ADMIN_FULL_NAME = 'Managed Admin';

/**
 * Default name for managed users
 */
export const BAZA_DEFAULT_USER_FULL_NAME = 'Managed User';

/**
 * Bootstrap script for Managed or Default Accounts
 * The script will automatically creates default or managed accounts when application starts
 * The script should works correctly in most cluster cases due of Redis lock. If it's not, call GET /baza-account/cms/bootstrap
 * endpoint
 */
@Injectable()
export class BazaAccountBootstrapService implements OnApplicationBootstrap {
    constructor(
        private readonly env: EnvService,
        private readonly bcrypt: BcryptService,
        private readonly logger: BazaLogger,
        private readonly redis: RedisService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly accountCmsService: BazaAccountCmsService,
        private readonly aclService: AclService,
        private readonly managedUsers: BazaAccountManagedUsersService,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * onApplicationBootstrap hook to bootstrap default accounts or refresh managed users on application start
     */
    async onApplicationBootstrap(): Promise<void> {
        if (this.env.isTestEnvironment) {
            return;
        }

        try {
            const isLocked = await this.redis.getClient().get(REDIS_KEY);

            if (isLocked) {
                return;
            }

            await this.redis.getClient().set(REDIS_KEY, '1', 'EX', REDIS_LOCK_TTL);
            await this.bootstrap();
            await this.redis.getClient().del(REDIS_KEY);
        } catch (err) {
            this.logger.error('Failed to bootstrap accounts:');
            this.logger.error((err as Error)?.message);
            this.logger.error(err);
        }
    }

    /**
     * Bootstrap default accounts or refresh managed users
     */
    async bootstrap(): Promise<void> {
        if (this.env.getAsBoolean('BAZA_MANAGED_USERS_LOCK', { defaultValue: false })) {
            return;
        }

        const managedUsers = await this.managedUsers.config;

        if (managedUsers.accounts.length > 0) {
            await this.bootstrapManagedUsers();
        } else {
            await this.bootstrapDefaultAccounts();
        }
    }

    /**
     * Bootstrap default accounts
     * @see BAZA_DEFAULT_ACCOUNTS
     * @see BAZA_DEFAULT_ACCOUNTS_PASSWORD
     * @private
     */
    private async bootstrapDefaultAccounts(): Promise<void> {
        await this.cleanUpManagedOutdatedAccounts();

        for (const account of BAZA_DEFAULT_ACCOUNTS) {
            const existing = await this.accountRepository.findActiveOrDeactivatedAccountsWithEmail(account.email);

            if (!existing.length) {
                if (account.role === AccountRole.Admin) {
                    const created = await this.accountCmsService.registerAdminAccount(
                        {
                            email: account.email,
                            firstName: account.firstName,
                            lastName: account.lastName,
                            password: BAZA_DEFAULT_ACCOUNTS_PASSWORD,
                            metadata: {},
                        },
                        {
                            ignoreManagedUserAccountRestrictions: true,
                            destroyJwtSessions: false,
                            withPasswordValidation: false,
                        },
                    );

                    if (!this.managedUsers.isManagedUser(created.email)) {
                        await this.accountCmsService.confirmEmailAccountById({
                            id: created.id,
                            confirmed: true,
                        });
                    }

                    await this.aclService.setSystemRootAccessToAdmin(created, {
                        destroyJwtSessions: false,
                    });

                    this.logger.log(`Created new admin account ${account.email} (${account.firstName} ${account.lastName})`);
                } else if (account.role === AccountRole.User) {
                    const created = await this.accountCmsService.registerUserAccount({
                        email: account.email,
                        firstName: account.firstName,
                        lastName: account.lastName,
                        password: BAZA_DEFAULT_ACCOUNTS_PASSWORD,
                        metadata: {},
                    });

                    if (!this.managedUsers.isManagedUser(created.email)) {
                        await this.accountCmsService.confirmEmailAccountById({
                            id: created.id,
                            confirmed: true,
                        });
                    }

                    this.logger.log(`Created new user account ${account.email} (${account.firstName} ${account.lastName})`);
                }
            }
        }
    }

    /**
     * Bootstrap Managed Users
     * @see BazaAccountManagedUsersService
     * @private
     */
    private async bootstrapManagedUsers(): Promise<void> {
        await this.cleanUpManagedOutdatedAccounts();
        await this.cleanUpDefaultOutdatedAccounts();

        const managedConfig = this.managedUsers.config;
        const managedAccounts = await this.accountRepository.findManagedAccounts();

        for (const accountConfig of managedConfig.accounts) {
            const existing = managedAccounts.find((next) => next.email === accountConfig.email);

            if (existing) {
                switch (accountConfig.role) {
                    default: {
                        throw new AccountUnknownRoleException({
                            role: accountConfig.role,
                        });
                    }

                    case AccountRole.Admin: {
                        await this.updateManagedAdminAccount(existing);

                        break;
                    }

                    case AccountRole.User: {
                        await this.updateManagedUserAccount(existing);

                        break;
                    }
                }
            } else {
                switch (accountConfig.role) {
                    default: {
                        throw new AccountUnknownRoleException({
                            role: accountConfig.role,
                        });
                    }

                    case AccountRole.Admin: {
                        await this.createManagedAdminAccount(accountConfig);

                        break;
                    }

                    case AccountRole.User: {
                        await this.createManagedUserAccount(accountConfig);

                        break;
                    }
                }
            }
        }
    }

    /**
     * Updates configuration for Managed User (Admin account)
     * First / Last / Full names will not be updated
     * @param account
     * @private
     */
    private async updateManagedAdminAccount(account: BazaAccountManagedUser): Promise<void> {
        const found = await this.accountRepository.findActiveAccountWithEmail(account.email);

        found.role = AccountRole.Admin;
        found.password = await this.bcrypt.hash(account.password);
        found.isEmailConfirmed = true;
        found.isManagedUser = true;

        await this.accountRepository.save(found);

        await this.aclService.setSystemRootAccessToAdmin(found, {
            destroyJwtSessions: false,
        });

        this.logger.log(`Updated managed admin account ${found.email} (${found.firstName} ${found.lastName})`);
    }

    /**
     * Create Managed User (Admin account)
     * @param account
     * @private
     */
    private async createManagedAdminAccount(account: BazaAccountManagedUser): Promise<void> {
        const created = await this.accountCmsService.registerAdminAccount(
            {
                email: account.email,
                firstName: BAZA_DEFAULT_ADMIN_FULL_NAME.split(' ')[0],
                lastName: BAZA_DEFAULT_ADMIN_FULL_NAME.split(' ')[1],
                password: BAZA_DEFAULT_ACCOUNTS_PASSWORD,
                metadata: {},
            },
            {
                ignoreManagedUserAccountRestrictions: true,
                destroyJwtSessions: false,
                withPasswordValidation: false,
            },
        );

        created.isEmailConfirmed = true;
        created.isManagedUser = true;

        await this.accountRepository.save(created);

        await this.aclService.setSystemRootAccessToAdmin(created, {
            destroyJwtSessions: false,
        });

        this.logger.log(`Created managed admin account ${created.email} (${created.firstName} ${created.lastName})`);
    }

    /**
     * Updates configuration for Managed User (User account)
     * First / Last / Full names will not be updated
     * @param account
     * @private
     */
    private async updateManagedUserAccount(account: BazaAccountManagedUser): Promise<void> {
        const found = await this.accountRepository.findActiveAccountWithEmail(account.email);

        found.role = AccountRole.User;
        found.password = await this.bcrypt.hash(account.password);
        found.isEmailConfirmed = true;
        found.isManagedUser = true;

        await this.accountRepository.save(found);

        await this.aclService.destroyAclOfAccount(found);

        this.logger.log(`Updated managed user account ${found.email} (${found.firstName} ${found.lastName})`);
    }

    /**
     * Create Managed User (User account)
     * @param account
     * @private
     */
    private async createManagedUserAccount(account: BazaAccountManagedUser): Promise<void> {
        const created = await this.accountCmsService.registerUserAccount(
            {
                email: account.email,
                firstName: BAZA_DEFAULT_ADMIN_FULL_NAME.split(' ')[0],
                lastName: BAZA_DEFAULT_ADMIN_FULL_NAME.split(' ')[1],
                password: BAZA_DEFAULT_ACCOUNTS_PASSWORD,
                metadata: {},
            },
            {
                ignoreManagedAccountRestrictions: true,
                withPasswordValidation: false,
            },
        );

        created.isEmailConfirmed = true;
        created.isManagedUser = true;

        await this.accountRepository.save(created);

        await this.aclService.destroyAclOfAccount(created);

        this.logger.log(`Created managed user account ${created.email} (${created.firstName} ${created.lastName})`);
    }

    /**
     * Clean Up all managed users which are not available in default list or BAZA_MANAGED_USERS
     * @see BazaAccountManagedUsersService
     * @private
     */
    private async cleanUpManagedOutdatedAccounts(): Promise<void> {
        const managedConfig = this.managedUsers.config;
        const managedAccounts = await this.accountRepository.findManagedAccounts();

        for (const account of managedAccounts) {
            const exists = managedConfig.accounts.find((next) => next.email === account.email);

            if (!exists) {
                await this.accountRepository.deleteAccount(account);
            }
        }
    }

    /**
     * Clean Up all default outdated accounts
     * @private
     */
    private async cleanUpDefaultOutdatedAccounts(): Promise<void> {
        for (const accountConfig of BAZA_DEFAULT_ACCOUNTS) {
            const found = await this.accountRepository.findActiveOrDeactivatedAccountsWithEmail(accountConfig.email);

            if (found.length) {
                await Promise.all(found.map((next) => this.accountRepository.deleteAccount(next)));
            }
        }
    }
}
