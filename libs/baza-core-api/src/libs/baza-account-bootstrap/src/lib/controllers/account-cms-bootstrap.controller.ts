import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AccountCmsBootstrapEndpoint, AccountCmsBootstrapEndpointPaths, CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { BazaAccountBootstrapService } from '../services/baza-account-bootstrap.service';

/**
 * Do not add Auth guards here - this endpoint is supposed to work for non-authorized users
 * You can disable endpoint by setting BAZA_MANAGED_USERS_LOCK=1 in environment variables
 */
@Controller()
@ApiTags(CoreOpenApiTags.BazaAccountCMS)
export class AccountCmsBootstrapController implements AccountCmsBootstrapEndpoint {
    constructor(private readonly service: BazaAccountBootstrapService) {}

    @Get(AccountCmsBootstrapEndpointPaths.bootstrap)
    @ApiOperation({
        summary: 'bootstrap',
        description: 'Bootstrap managed or default users',
    })
    @ApiOkResponse()
    async bootstrap(): Promise<void> {
        await this.service.bootstrap();
    }
}
