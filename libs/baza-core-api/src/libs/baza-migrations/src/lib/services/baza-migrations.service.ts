import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaMigration, BazaMigrationRunMode } from '../baza-migrations.models';
import { BazaMigrationEntity } from '../entities/baza-migration.entity';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { EnvService } from '../../../../baza-env/src';
import { BazaLogger } from '../../../../baza-logger/src';

@Injectable()
export class BazaMigrationsService {
    private _migrations: Array<BazaMigration> = [];

    constructor(private readonly logger: BazaLogger, private readonly connection: Connection, private readonly env: EnvService) {}

    set migrations(migrations: Array<BazaMigration>) {
        this._migrations = migrations;
    }

    private get repository(): Repository<BazaMigrationEntity> {
        return this.connection.getRepository(BazaMigrationEntity);
    }

    async up(): Promise<void> {
        const queryRunner = this.connection.createQueryRunner();

        if (Array.isArray(this._migrations) && this._migrations.length > 0) {
            for (const migration of this._migrations) {
                if (
                    migration.forEnvs().includes(
                        this.env.getAsEnum('BAZA_ENV', {
                            enum: BazaEnvironments,
                            defaultValue: BazaEnvironments.Local,
                        }) as any,
                    )
                ) {
                    switch (migration.runMode()) {
                        case BazaMigrationRunMode.Once: {
                            if (await this.isMigrationApplied(migration)) {
                                continue;
                            }

                            this.logger.log(`[UP] [${new Date().toISOString()}] [${this.getIdOfMigration(migration)}]`);

                            await migration.up(queryRunner);
                            await this.markMigrationAsUp(migration);

                            break;
                        }

                        case BazaMigrationRunMode.EveryTime: {
                            this.logger.log(`[UP] [${new Date().toISOString()}] [${this.getIdOfMigration(migration)}]`);

                            await migration.up(queryRunner);
                            await this.markMigrationAsUp(migration);

                            break;
                        }
                    }
                }
            }
        }

        this.logger.log(`[UP] [${new Date().toISOString()}] Completed!`);
    }

    async down(): Promise<void> {
        const queryRunner = this.connection.createQueryRunner();

        if (Array.isArray(this._migrations) && this._migrations.length > 0) {
            for (const migration of [...this._migrations].reverse()) {
                if (
                    migration.down &&
                    migration.forEnvs().includes(
                        this.env.getAsEnum<BazaEnvironments>('BAZA_ENV', {
                            enum: BazaEnvironments,
                            defaultValue: BazaEnvironments.Local,
                        }) as any,
                    )
                ) {
                    this.logger.log(`[DOWN] [${new Date().toISOString()}] [${this.getIdOfMigration(migration)}]`);

                    await migration.down(queryRunner);
                    await this.markMigrationAsDown(migration);
                }
            }
        }

        this.logger.log(`[DOWN] [${new Date().toISOString()}] Completed!`);
    }

    private getIdOfMigration(migration: BazaMigration): string {
        return (migration as any).constructor.name;
    }

    private async isMigrationApplied(migration: BazaMigration): Promise<boolean> {
        return !!(await this.repository.findOne({
            where: [
                {
                    name: this.getIdOfMigration(migration),
                },
            ],
        }));
    }

    private async markMigrationAsUp(migration: BazaMigration): Promise<void> {
        const entity = new BazaMigrationEntity();

        entity.name = this.getIdOfMigration(migration);
        entity.dateCreated = new Date();

        await this.repository.save(entity);
    }

    private async markMigrationAsDown(migration: BazaMigration): Promise<void> {
        const entity = await this.repository.findOne({
            where: [
                {
                    name: this.getIdOfMigration(migration),
                },
            ],
        });

        if (entity) {
            await this.repository.remove(entity);
        }
    }
}
