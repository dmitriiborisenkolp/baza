import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BazaMigrationEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    name: string;

    @Column()
    dateCreated: Date = new Date();
}
