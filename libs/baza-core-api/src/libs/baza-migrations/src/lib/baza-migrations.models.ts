import { QueryRunner } from 'typeorm';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';

export enum BazaMigrationRunMode {
    Once = 'Once',
    EveryTime = 'EveryTime',
}

export interface BazaMigration<E = BazaEnvironments> {
    forEnvs(): Array<E>;
    runMode(): BazaMigrationRunMode;
    up(queryRunner: QueryRunner): Promise<void>;
    down?(queryRunner: QueryRunner): Promise<void>;
}
