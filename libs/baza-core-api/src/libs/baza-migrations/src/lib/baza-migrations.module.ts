import { DynamicModule, Global, Module } from '@nestjs/common';
import { BazaMigrationsService } from './services/baza-migrations.service';
import { BAZA_MIGRATIONS_CONFIG, BazaMigrationsConfig } from './baza-migrations.config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BazaEnvModule } from '../../../baza-env/src';
import { BazaLoggerModule } from '../../../baza-logger/src';

interface ForRootAsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaMigrationsConfig>;
}

@Module({})
export class BazaMigrationsModule {
    static forRootASync(options: ForRootAsyncOptions): DynamicModule {
        return {
            module: BazaMigrationsGlobalModule,
            providers: [{
                provide: BAZA_MIGRATIONS_CONFIG,
                inject: options.injects,
                useFactory: options.useFactory,
            }],
            exports: [
                BAZA_MIGRATIONS_CONFIG,
            ],
        };
    }
}

@Global()
@Module({
    imports: [
        BazaLoggerModule,
        BazaEnvModule,
        TypeOrmModule,
    ],
    providers: [
        BazaMigrationsService,
    ],
    exports: [
        BazaMigrationsService,
    ],
})
class BazaMigrationsGlobalModule {
}
