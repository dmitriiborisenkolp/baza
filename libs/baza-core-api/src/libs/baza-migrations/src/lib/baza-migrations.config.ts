export interface BazaMigrationsConfig {
    migrations: Array<any>;
}

export const BAZA_MIGRATIONS_CONFIG = Symbol();
