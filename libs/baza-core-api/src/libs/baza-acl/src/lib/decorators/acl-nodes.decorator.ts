import { SetMetadata } from '@nestjs/common';

export const AclNodes = (...accessNodes: Array<Array<string>>) => SetMetadata('accessNodes', accessNodes);
