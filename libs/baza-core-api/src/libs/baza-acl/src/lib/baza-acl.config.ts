import { AccountRole, CoreAcl } from '@scaliolabs/baza-core-shared';

export interface BazaAclConfig<T = CoreAcl> {
    defaultACLForRoles: Array<{
        role: AccountRole;
        acl: Array<CoreAcl>;
    }>
}

export const BAZA_ACL_CONFIG = Symbol();

export function defaultBazaAclConfig<T = CoreAcl>(): BazaAclConfig<T> {
    return {
        defaultACLForRoles: [
            {
                role: AccountRole.User,
                acl: [],
            },
            {
                role: AccountRole.Admin,
                acl: [],
            }
        ],
    };
}
