import { Connection, Repository } from 'typeorm';
import { AccountAclEntity } from '../entity/account-acl.entity';
import { BazaAccountRepository } from '../../../../baza-account/src/lib/repositories/baza-account.repository';
import { AccountEntity } from '../../../../baza-account/src/lib/entities/account.entity';
import { Injectable } from '@nestjs/common';

export const ACCOUNT_ACL_RELATIONS = ['account'];

@Injectable()
export class AccountAclRepository {
    constructor(
        private readonly connection: Connection,
        private readonly accountRepository: BazaAccountRepository,
    ) {}

    get repository(): Repository<AccountAclEntity> {
        return this.connection.getRepository(AccountAclEntity) as Repository<AccountAclEntity>;
    }

    async saveEntity(entity: AccountAclEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async deleteEntity(entity: AccountAclEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async findAclForAccount(account: AccountEntity | number): Promise<AccountAclEntity | undefined> {
        return this.repository.findOne({
            where: [{
                account,
            }],
        });
    }

    async getAclForAccount(account: AccountEntity | number): Promise<AccountAclEntity> {
        const entity = await this.repository.findOne({
            where: [{
                account,
            }],
            relations: ACCOUNT_ACL_RELATIONS,
        });

        if (! entity) {
            const newEntity = new AccountAclEntity();

            const accountEntity = typeof account === 'number'
                ? await this.accountRepository.getActiveAccountWithId(account)
                : account;

            newEntity.account = accountEntity;
            newEntity.acl = [];

            await this.repository.save(newEntity);

            return newEntity;
        }

        return entity;
    }

    async deleteAclOfAccount(account: AccountEntity | number): Promise<void> {
        const entity = await this.findAclForAccount(account);

        if (entity) {
            await this.deleteEntity(entity);
        }
    }
}
