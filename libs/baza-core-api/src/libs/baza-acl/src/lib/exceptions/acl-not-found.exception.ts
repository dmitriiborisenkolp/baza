import { AclErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'No ACL specified for given account';

export class AclNotFoundException extends BazaAppException {
    constructor() {
        super(AclErrorCodes.AclNotFound, ERR_MESSAGE, HttpStatus.NOT_FOUND);
    }
}
