import { AclErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'No default ACL available for given account';

export class AclNoDefaultsAvailableException extends BazaAppException {
    constructor() {
        super(AclErrorCodes.AclNoDefaultsAvailable, ERR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
