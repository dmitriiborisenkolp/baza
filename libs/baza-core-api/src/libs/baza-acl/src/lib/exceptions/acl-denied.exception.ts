import { AclErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Access denied by ACL';

export class AclDeniedException extends BazaAppException {
  constructor(errMessage = ERR_MESSAGE) {
    super(AclErrorCodes.AclDenied, errMessage, HttpStatus.FORBIDDEN);
  }
}
