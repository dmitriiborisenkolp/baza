import { AclErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'You are not able to assign ACL out of your scope';

export class AclOutOfScopeException extends BazaAppException {
    constructor() {
        super(AclErrorCodes.AclOutOfScope, ERR_MESSAGE, HttpStatus.NOT_FOUND);
    }
}
