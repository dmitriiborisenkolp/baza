import { Inject, Injectable } from '@nestjs/common';
import { AclDeniedException } from '../exceptions/acl-denied.exception';
import { AccountRole, BazaAclSetAclRequest, BazaAuthDestroyJwtsOfAccountCommand, CoreAcl } from '@scaliolabs/baza-core-shared';
import { AccountAclRepository } from '../repositories/account-acl.repository';
import { AccountAclEntity } from '../entity/account-acl.entity';
import type { BazaAclConfig } from '../baza-acl.config';
import { BAZA_ACL_CONFIG } from '../baza-acl.config';
import { AclOutOfScopeException } from '../exceptions/acl-out-of-scope.exception';
import { AccountEntity } from '../../../../baza-account/src/lib/entities/account.entity';
import { CommandBus } from '@nestjs/cqrs';
import { BazaAccountRepository, BazaAccountManagedUsersService } from '../../../../baza-account/src';

@Injectable()
export class AclService<ACL = string> {
    constructor(
        @Inject(BAZA_ACL_CONFIG) private readonly moduleConfig: BazaAclConfig,
        private readonly repository: AccountAclRepository,
        private readonly commandBus: CommandBus,
        private readonly accountRepository: BazaAccountRepository,
        private readonly managedAccounts: BazaAccountManagedUsersService,
    ) {}

    async hasDefaultsAclFor(account: AccountEntity): Promise<boolean> {
        const found = this.moduleConfig.defaultACLForRoles.find((d) => d.role === account.role);

        return !!found && found.acl.length > 0;
    }

    async hasAclOfAccount(account: AccountEntity | number): Promise<boolean> {
        const accountEmail = await this.accountRepository.getAccountEmail(account);

        if (this.managedAccounts.isManagedUser(accountEmail)) {
            const managedAccount = this.managedAccounts.getManagedUserConfig(accountEmail);

            return managedAccount.role === AccountRole.Admin;
        } else {
            return !!(await this.repository.findAclForAccount(account));
        }
    }

    async getAclOfAccount(account: AccountEntity | number): Promise<AccountAclEntity> {
        const accountEmail = await this.accountRepository.getAccountEmail(account);

        if (this.managedAccounts.isManagedUser(accountEmail)) {
            const managedAccount = this.managedAccounts.getManagedUserConfig(accountEmail);

            if (managedAccount.role === AccountRole.Admin) {
                const tmpAclEntity = new AccountAclEntity();

                tmpAclEntity.account = typeof account === 'number' ? await this.accountRepository.getActiveAccountWithId(account) : account;
                tmpAclEntity.acl = [CoreAcl.SystemRoot];

                return tmpAclEntity;
            } else {
                return this.repository.getAclForAccount(account);
            }
        } else {
            return this.repository.getAclForAccount(account);
        }
    }

    async setACLOfAccount(assigner: AccountEntity, request: BazaAclSetAclRequest): Promise<AccountAclEntity> {
        if (assigner.role !== AccountRole.Admin) {
            throw new AclOutOfScopeException();
        }

        if (request.accountId === assigner.id) {
            throw new AclOutOfScopeException();
        }

        const assignerAcl = await this.getAclOfAccount(assigner);

        const isAllowedToApplyACLs =
            assignerAcl.acl.includes(CoreAcl.SystemRoot) || request.acl.every((node) => assignerAcl.acl.includes(node));

        if (!isAllowedToApplyACLs) {
            throw new AclOutOfScopeException();
        }

        const aclEntity = await this.repository.getAclForAccount(request.accountId);

        if (aclEntity.acl.includes(CoreAcl.SystemRoot) && !assignerAcl.acl.includes(CoreAcl.SystemRoot)) {
            throw new AclOutOfScopeException();
        }

        aclEntity.acl = request.acl;

        await this.commandBus.execute(
            new BazaAuthDestroyJwtsOfAccountCommand({
                accountId: aclEntity.account.id,
            }),
        );

        await this.repository.saveEntity(aclEntity);

        return aclEntity;
    }

    async setSystemRootAccessToAdmin(
        account: AccountEntity<any>,
        options = {
            destroyJwtSessions: true,
        },
    ): Promise<AccountAclEntity> {
        if (account.role !== AccountRole.Admin) {
            throw new AclOutOfScopeException();
        }

        const aclEntity = await this.repository.getAclForAccount(account.id);

        aclEntity.acl = [CoreAcl.SystemRoot];

        if (options.destroyJwtSessions) {
            await this.commandBus.execute(
                new BazaAuthDestroyJwtsOfAccountCommand({
                    accountId: aclEntity.account.id,
                }),
            );
        }

        await this.repository.saveEntity(aclEntity);

        return aclEntity;
    }

    async hasAccess(account: AccountEntity, accessNode: Array<ACL>): Promise<boolean> {
        if (await this.hasAclOfAccount(account)) {
            const aclEntity = await this.getAclOfAccount(account);

            return aclEntity.acl.includes(CoreAcl.SystemRoot) || accessNode.some((accessNode) => aclEntity.acl.includes(accessNode as any));
        } else {
            return false;
        }
    }

    async withAccess(account: AccountEntity, accessNodes: ACL | Array<ACL>): Promise<boolean> {
        const rAccessNodes = Array.isArray(accessNodes) ? accessNodes : [accessNodes];

        for (const accessNode of rAccessNodes) {
            if (!(await this.hasAccess(account, [accessNode]))) {
                throw new AclDeniedException();
            }
        }

        return true;
    }

    async destroyAclOfAccount(account: AccountEntity | number): Promise<void> {
        await this.repository.deleteAclOfAccount(account);
    }
}
