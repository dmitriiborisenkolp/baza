import { CommandBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AclService } from '../services/acl.service';
import { AccountApiEvent, AccountApiEvents, BazaAuthDestroyJwtsOfAccountCommand } from '@scaliolabs/baza-core-shared';
import { ApiCqrsEvent } from '@scaliolabs/baza-core-shared';
import { BazaAccountRepository } from '../../../../baza-account/src';
import { cqrs } from '../../../../baza-common/src';

@EventsHandler(ApiCqrsEvent)
export class AccountAdminAccessRevokedEventHandler implements IEventHandler<ApiCqrsEvent<AccountApiEvent, AccountApiEvents>> {
    constructor(
        private readonly commandBus: CommandBus,
        private readonly aclService: AclService,
        private readonly accountRepository: BazaAccountRepository,
    ) {}

    handle(e: ApiCqrsEvent<AccountApiEvent, AccountApiEvents>): void {
        cqrs(AccountAdminAccessRevokedEventHandler.name, async () => {
            switch (e.apiEvent.event.topic) {
                case AccountApiEvent.BazaAccountAdminAccessRevoked: {
                    const account = await this.accountRepository.getActiveAccountWithId(e.apiEvent.event.payload.accountId);

                    await this.aclService.destroyAclOfAccount(account);

                    await this.commandBus.execute(
                        new BazaAuthDestroyJwtsOfAccountCommand({
                            accountId: account.id,
                        }),
                    );
                }
            }
        });
    }
}
