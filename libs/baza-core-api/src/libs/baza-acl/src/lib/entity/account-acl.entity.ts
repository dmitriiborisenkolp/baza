import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '../../../../baza-account/src';

@Entity()
export class AccountAclEntity<T = string> {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @OneToOne(() => AccountEntity, {
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity;

    @Column({
        type: 'json',
        nullable: false,
        default: [],
    })
    acl: Array<T>;
}
