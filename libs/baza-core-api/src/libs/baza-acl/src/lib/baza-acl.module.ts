import { DynamicModule, Global, Module } from '@nestjs/common';
import { AclService } from './services/acl.service';
import { BAZA_ACL_CONFIG, BazaAclConfig } from './baza-acl.config';
import { AccountAclRepository } from './repositories/account-acl.repository';
import { AccountAdminAccessRevokedEventHandler } from './event-handlers/account-admin-access-revoked.event-handler';
import { BazaEventBusApiModule } from '../../../baza-event-bus/src';
import { CqrsModule } from '@nestjs/cqrs';

interface AsyncOptions {
    inject: Array<any>;
    useFactory(...inject): Promise<BazaAclConfig>;
}

export { AsyncOptions as BazaAclModuleAsyncOptions };

@Module({})
export class BazaAclModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaAclGlobalModule,
            providers: [{
                provide: BAZA_ACL_CONFIG,
                inject: options.inject,
                useFactory: (...injects) => options.useFactory(...injects),
            }],
        };
    }
}

@Global()
@Module({
    imports: [
        CqrsModule,
        BazaEventBusApiModule,
    ],
    providers: [
        AclService,
        AccountAclRepository,
        AccountAdminAccessRevokedEventHandler,
    ],
    exports: [
        AclService,
        AccountAclRepository,
    ],
})
class BazaAclGlobalModule {
}
