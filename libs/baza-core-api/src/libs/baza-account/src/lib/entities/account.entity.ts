import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Application } from '@scaliolabs/baza-core-shared';
import {
    AccountManualResetPasswordRequestDto,
    AccountRole,
    AccountSettingsDto,
    defaultAccountSettings,
} from '@scaliolabs/baza-core-shared';
import { BazaAccount2FAConfiguration, defaultBazaAccount2FAConfiguration } from '../models/baza-account-2fa-configuration';
import { ulid } from 'ulid';

/**
 * Account Entity
 * Accepts <T> as AccountSettingsDto. You can define your additional settings with extending AccountSettingsDto and
 * bypassing it to generic
 */
@Entity({
    name: 'baza_account_rev1',
})
export class AccountEntity<T = AccountSettingsDto> {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        unique: true,
        nullable: true,
    })
    // TODO: Add `readonly` here after 1st Jan 2023
    ulid: string = ulid();

    @Column({
        type: 'varchar',
        nullable: false,
        default: AccountRole.User,
    })
    role: AccountRole;

    @Column({
        type: 'jsonb',
        default: defaultBazaAccount2FAConfiguration(),
    })
    twoFactorMethods: BazaAccount2FAConfiguration;

    @Column({
        default: new Date(),
    })
    dateCreated: Date = new Date();

    @Column({
        default: new Date(),
    })
    dateUpdated: Date = new Date();

    @Column({
        nullable: true,
    })
    lastSignIn: Date;

    @Column({
        type: 'varchar',
        default: Application.WEB,
    })
    signedUpWithClient: Application = Application.WEB;

    @Column({
        type: 'varchar',
        nullable: false,
        unique: false,
    })
    email: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    password: string;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    fullName: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    firstName: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    lastName: string;

    @Column({
        type: 'json',
        nullable: false,
        default: [],
    })
    profileImages: Array<string>;

    @Column({
        type: 'boolean',
        nullable: false,
    })
    isEmailConfirmed = false;

    @Column({
        type: 'boolean',
        nullable: false,
        default: false,
    })
    isDeactivated = false;

    @Column({
        nullable: true,
    })
    phone?: string;

    @Column({
        type: 'json',
        nullable: false,
        default: defaultAccountSettings(),
    })
    settings: T;

    @Column({
        type: 'json',
        default: {},
    })
    metadata: Record<string, unknown> = {};

    @Column({
        type: 'json',
        nullable: true,
    })
    manualResetPasswordRequest?: AccountManualResetPasswordRequestDto;

    @Column({
        default: false,
    })
    isManagedUser: boolean;
}
