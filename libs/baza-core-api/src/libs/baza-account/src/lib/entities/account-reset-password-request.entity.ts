import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from './account.entity';
import { AccountSettingsDto } from '@scaliolabs/baza-core-shared';

/**
 * Relations for AccountResetPasswordRequestEntity
 * @see AccountResetPasswordRequestEntity
 */
export const ACCOUNT_RESET_PASSWORD_REQUEST_ENTITY_RELATIONS = ['account'];

/**
 * Account Reset Password Request Entity
 * A token entity
 */
@Entity()
export class AccountResetPasswordRequestEntity<T = AccountSettingsDto> {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        default: new Date(),
    })
    dateCreatedAt: Date = new Date();

    @Column({
        default: new Date(),
    })
    dateExpiresAt: Date;

    @OneToOne(() => AccountEntity, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity<T>;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    token: string;
}
