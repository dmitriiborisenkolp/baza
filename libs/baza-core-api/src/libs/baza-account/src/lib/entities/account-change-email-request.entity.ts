import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from './account.entity';
import { AccountSettingsDto } from '@scaliolabs/baza-core-shared';
import { Application } from '@scaliolabs/baza-core-shared';

/**
 * Relations for AccountChangeEmailRequestEntity
 * @see AccountChangeEmailRequestEntity
 */
export const ACCOUNT_CHANGE_EMAIL_REQUEST_ENTITY_RELATIONS = ['account'];

/**
 * Account Change Email Request
 * A token entity.
 */
@Entity()
export class AccountChangeEmailRequestEntity<T = AccountSettingsDto> {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        default: new Date(),
    })
    dateCreatedAt: Date = new Date();

    @Column({
        default: new Date(),
    })
    dateExpiresAt: Date;

    @OneToOne(() => AccountEntity, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity<T>;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    application: Application = Application.WEB;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    token: string;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    newEmail: string;
}
