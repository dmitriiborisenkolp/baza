import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from './account.entity';
import { AccountSettingsDto } from '@scaliolabs/baza-core-shared';

/**
 * Relations for AccountConfirmEmailRequestEntity
 * @see AccountConfirmEmailRequestEntity
 */
export const ACCOUNT_CONFIRM_EMAIL_REQUEST_ENTITY_RELATIONS = ['account'];

/**
 * Account Confirm Email Entity
 * A token entity
 */
@Entity()
export class AccountConfirmEmailRequestEntity<T = AccountSettingsDto> {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        default: new Date(),
    })
    dateCreatedAt: Date = new Date();

    @Column({
        default: new Date(),
    })
    dateExpiresAt: Date;

    @OneToOne(() => AccountEntity, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity<T>;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    token: string;
}
