import { RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';

export const bazaAccountRegistry: RegistrySchema = {
    bazaAccounts: {
        referralCodes: {
            copyText: {
                name: 'Referral Code – Copy&Paste message',
                type: RegistryType.Text,
                hiddenFromList: false,
                public: false,
                defaults: 'Join MyProject with my unique code and help me earn shares: {{ code }}',
            },
        },
        registration: {
            allowNonCodeRegistration: {
                name: 'Accounts - Allow Registration w/o Referral or Invite Codes',
                type: RegistryType.Boolean,
                hiddenFromList: false,
                public: true,
                defaults: true,
            },
            allowInviteCodeRegistration: {
                name: 'Accounts - Allow Registration with Invite Codes',
                type: RegistryType.Boolean,
                hiddenFromList: false,
                public: true,
                defaults: true,
            },
            allowReferralCodeRegistration: {
                name: 'Accounts - Allow Registration with Referral Codes',
                type: RegistryType.Boolean,
                hiddenFromList: false,
                public: true,
                defaults: true,
            },
        },
    },
};
