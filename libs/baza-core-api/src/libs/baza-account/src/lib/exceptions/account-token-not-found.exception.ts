import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountTokenNotFoundException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountTokenNotFound,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountTokenNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
