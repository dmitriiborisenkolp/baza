import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountInvalidCredentialsException extends BazaAppException {
    // Different status code may be required for cases when API should not
    // sends 403 HTTP Status. When API sends 403 HTTP Status, Frontend/Mobile
    // clients will automatically sign out user which is not a valid case
    // when user is changing password.
    constructor(statusCode = HttpStatus.FORBIDDEN) {
        super(
            AccountErrorCodes.BazaAccountInvalidCredentials,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountInvalidCredentials],
            statusCode,
        );
    }
}
