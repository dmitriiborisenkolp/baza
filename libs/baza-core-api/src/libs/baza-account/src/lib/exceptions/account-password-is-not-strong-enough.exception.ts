import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountPasswordIsNotStrongEnoughException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough],
            HttpStatus.BAD_REQUEST,
        );
    }
}
