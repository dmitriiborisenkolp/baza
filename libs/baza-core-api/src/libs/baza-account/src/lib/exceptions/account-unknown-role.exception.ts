import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountUnknownRoleException extends BazaAppException {
    constructor(args: { role: string }) {
        super(
            AccountErrorCodes.BazaAccountUnknownRole,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountUnknownRole],
            HttpStatus.BAD_REQUEST,
            args,
        );
    }
}
