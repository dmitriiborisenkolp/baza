import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountInvalidPasswordException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountInvalidPassword,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountInvalidPassword],
            HttpStatus.BAD_REQUEST,
        );
    }
}
