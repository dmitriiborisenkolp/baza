import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountTokenExpiredExceptionWithNewEmailSent extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountTokenExpiredNewConfirmationEmailSent,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountTokenExpiredNewConfirmationEmailSent],
            HttpStatus.NOT_FOUND,
        );
    }
}
