import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountPasswordsDoNotMatchException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountPasswordsDoNotMatch,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountPasswordsDoNotMatch],
            HttpStatus.BAD_REQUEST,
        );
    }
}
