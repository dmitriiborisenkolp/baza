import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Your account is already using {{ email }} email';

export class AccountChangeEmailSameException extends BazaAppException {
    constructor(args: {
        email: string,
    }) {
        super(
            AccountErrorCodes.BazaAccountChangeEmailSame,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountChangeEmailSame],
            HttpStatus.CONFLICT,
            args,
        );
    }
}
