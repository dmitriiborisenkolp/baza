import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountModuleFeatureIsNotEnabledException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountModuleFeatureIsNotEnabled,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountModuleFeatureIsNotEnabled],
            HttpStatus.BAD_REQUEST,
        );
    }
}
