import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountSelfAttemptUnverifyException extends BazaAppException {
    constructor(args: {
        email: string;
    }) {
        super(
            AccountErrorCodes.BazaAccountSelfAttemptUnverify,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountSelfAttemptUnverify],
            HttpStatus.NOT_FOUND,
            args,
        );
    }
}
