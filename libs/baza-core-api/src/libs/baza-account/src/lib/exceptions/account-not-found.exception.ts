import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountNotFoundException extends BazaAppException {
    constructor(args: {
        email: string;
    }) {
        super(
            AccountErrorCodes.BazaAccountNotFound,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountNotFound],
            HttpStatus.NOT_FOUND,
            args,
        );
    }
}
