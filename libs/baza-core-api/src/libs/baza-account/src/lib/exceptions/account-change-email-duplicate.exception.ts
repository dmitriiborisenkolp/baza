import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountChangeEmailDuplicateException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountChangeEmailDuplicate,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountChangeEmailDuplicate],
            HttpStatus.CONFLICT,
        );
    }
}
