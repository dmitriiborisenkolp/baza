import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountNoAccessToRevokeAccountException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountNoAccessToRevokeAccount,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountNoAccessToRevokeAccount],
            HttpStatus.BAD_REQUEST,
        );
    }
}
