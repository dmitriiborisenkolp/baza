import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountIsDeactivatedException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountIsDeactivated,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountIsDeactivated],
            HttpStatus.BAD_REQUEST,
        );
    }
}
