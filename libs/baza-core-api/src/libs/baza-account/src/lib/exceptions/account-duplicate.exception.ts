import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountDuplicateException extends BazaAppException {
    constructor(args: {
        email: string;
        notActivated?: boolean;
    }) {
        super(
            AccountErrorCodes.BazaAccountDuplicate,
            args.notActivated
                ? accountErrorCodesI18n[AccountErrorCodes.BazaAccountDuplicate].NOT_ACTIVATED
                : accountErrorCodesI18n[AccountErrorCodes.BazaAccountDuplicate].ACTIVATED,
            HttpStatus.CONFLICT,
            args,
        );
    }
}
