import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountManagedUserNotFoundException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountManagedUserNotFound,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountManagedUserNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
