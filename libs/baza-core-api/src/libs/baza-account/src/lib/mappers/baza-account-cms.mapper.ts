import { Injectable } from '@nestjs/common';
import { BazaAccountMapper } from './baza-account.mapper';
import { AccountEntity } from '../entities/account.entity';
import { AccountCmsDto } from '@scaliolabs/baza-core-shared';

/**
 * AccountEntity to AccountCmsDto mapper
 * @see AccountEntity
 * @see AccountCmsDto
 */
@Injectable()
export class BazaAccountCmsMapper {
    constructor(private readonly baseMapper: BazaAccountMapper) {}

    entityToDTO(input: AccountEntity): AccountCmsDto {
        return {
            ...this.baseMapper.entityToDto(input),
            metadata: input.metadata,
        };
    }

    entitiesToDTOs(input: Array<AccountEntity>): Array<AccountCmsDto> {
        return input.map((entity) => this.entityToDTO(entity));
    }
}
