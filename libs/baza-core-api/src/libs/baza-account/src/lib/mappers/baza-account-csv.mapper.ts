import { AccountEntity } from '../entities/account.entity';
import { AccountCsvDto } from '@scaliolabs/baza-core-shared';
import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

/**
 * AccountEntity to AccountCsvDto mapper
 * @see AccountEntity
 * @see AccountCsvDto
 * @see BazaAccountCmsService.exportToCsv
 */
@Injectable()
export class BazaAccountCsvMapper {
    entityToCSVRow(input: AccountEntity): AccountCsvDto {
        return {
            id: input.id,
            dateCreated: moment(input.dateCreated).format('LLL'),
            dateUpdated: input.dateUpdated ? moment(input.dateUpdated).format('LLL') : '',
            lastSignIn: input.lastSignIn ? moment(input.lastSignIn).format('LLL') : '',
            email: input.email,
            fullName: input.fullName,
            isEmailConfirmed: input.isEmailConfirmed,
            role: input.role,
            signedUpWithClient: input.signedUpWithClient,
        };
    }

    entitiesToCSVRows(input: Array<AccountEntity>): Array<AccountCsvDto> {
        return input.map((e) => this.entityToCSVRow(e));
    }
}
