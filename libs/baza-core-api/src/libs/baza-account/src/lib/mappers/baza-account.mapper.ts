import { Injectable } from '@nestjs/common';
import { AccountEntity } from '../entities/account.entity';
import { AccountDto } from '@scaliolabs/baza-core-shared';
import { BazaAccountManagedUsersService } from '../services/baza-account-managed-users.service';

/**
 * AccountEntity to AccountDto mapper
 * @see AccountEntity
 * @see AccountDto
 */
@Injectable()
export class BazaAccountMapper {
    constructor(private readonly managedUserAccounts: BazaAccountManagedUsersService) {}

    entityToDto(entity: AccountEntity): AccountDto {
        return {
            id: entity.id,
            dateCreated: entity.dateCreated.toISOString(),
            dateUpdated: entity.dateUpdated.toISOString(),
            lastSignIn: entity.lastSignIn ? entity.lastSignIn.toISOString() : undefined,
            signedUpWithClient: entity.signedUpWithClient,
            email: entity.email,
            isEmailConfirmed: entity.isEmailConfirmed,
            isDeactivated: entity.isDeactivated,
            fullName: entity.fullName,
            firstName: entity.firstName,
            lastName: entity.lastName,
            phone: entity.phone,
            role: entity.role,
            settings: entity.settings,
            profileImages: entity.profileImages,
            isManagedUserAccount: this.managedUserAccounts.isManagedUser(entity.email),
        };
    }

    entitiesToDTOs(entities: Array<AccountEntity>): Array<AccountDto> {
        return entities.map((e) => this.entityToDto(e));
    }
}
