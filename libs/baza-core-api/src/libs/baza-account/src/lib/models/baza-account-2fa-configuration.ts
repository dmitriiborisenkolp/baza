/**
 * JSON DTO Version for 2FA configuration
 * We're using version for Configuration DTO because configuration model could be changed later. API
 * should be able to deal with outdated 2FA configs.
 */
export const BAZA_ACCOUNT_2FA_LAST_VERSION = 1;

/**
 * 2FA Configuration
 * The configuration is stored in AccountEntity.twoFactorMethods field
 * @see AccountEntity.twoFactorMethods
 */
interface BazaAccount2FAFullConfiguration {
    email: {
        enabled: boolean;
    };
    phoneSMS:
        | { enabled: false }
        | {
              enabled: true;
              phoneNumber: string;
          };
    phoneCall:
        | { enabled: false }
        | {
              enabled: true;
              phoneNumber: string;
          };
    googleAuthenticator:
        | { enabled: false }
        | {
              enabled: true;
              secretCode: string;
          };
    microsoftAuthenticator:
        | { enabled: false }
        | {
              enabled: true;
              secretCode: string;
          };
}

/**
 * JSON 2FA Configuration DTO
 * Same as BazaAccount2FAFullConfiguration + additional `version` with `BAZA_ACCOUNT_2FA_LAST_VERSION` value
 * @see BAZA_ACCOUNT_2FA_LAST_VERSION
 * @see BazaAccount2FAFullConfiguration
 * @see AccountEntity.twoFactorMethods
 */
export type BazaAccount2FAConfiguration = {
    version: number;
} & Partial<BazaAccount2FAFullConfiguration>;

/**
 * Returns default 2FA configuration JSON DTO
 * @see AccountEntity.twoFactorMethods
 */
export const defaultBazaAccount2FAConfiguration: () => BazaAccount2FAConfiguration = () => ({
    version: BAZA_ACCOUNT_2FA_LAST_VERSION,
    email: {
        enabled: false,
    },
    phoneSMS: {
        enabled: false,
    },
    phoneCall: {
        enabled: false,
    },
    googleAuthenticator: {
        enabled: false,
    },
    microsoftAuthenticator: {
        enabled: false,
    },
});
