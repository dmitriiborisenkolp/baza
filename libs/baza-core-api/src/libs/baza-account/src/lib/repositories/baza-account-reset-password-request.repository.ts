import { Inject, Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { AccountEntity } from '../entities/account.entity';
import {
    ACCOUNT_RESET_PASSWORD_REQUEST_ENTITY_RELATIONS,
    AccountResetPasswordRequestEntity,
} from '../entities/account-reset-password-request.entity';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { AccountSettingsDto, BazaAccountConstants } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';
import { BAZA_ACCOUNT_CONFIG, BazaAccountConfig } from '../baza-account.config';

@Injectable()
export class BazaAccountResetPasswordRequestRepository<T extends AccountSettingsDto = any> {
    constructor(@Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig, private readonly connection: Connection) {}

    /**
     * Returns TypeOrm repository for AccountResetPasswordRequestEntity
     * @see Repository
     * @see AccountResetPasswordRequestEntity
     */
    get repository(): Repository<AccountResetPasswordRequestEntity<T>> {
        return this.connection.getRepository(AccountResetPasswordRequestEntity) as Repository<AccountResetPasswordRequestEntity<T>>;
    }

    /**
     * Returns Reset Password Token entity (by Account)
     * The method can returns no result
     * @param account
     */
    async findConfirmationByAccount(account: AccountEntity<T>): Promise<AccountResetPasswordRequestEntity<T> | undefined> {
        return this.repository.findOne({
            account: account,
        });
    }

    /**
     * Returns Reset Password Token entity (by Token)
     * The method can returns no result
     * @param token
     */
    async findConfirmationByToken(token: string): Promise<AccountResetPasswordRequestEntity<T> | undefined> {
        const entity = await this.repository.findOne(
            {
                token: token,
            },
            {
                relations: ACCOUNT_RESET_PASSWORD_REQUEST_ENTITY_RELATIONS,
            },
        );

        if (!entity) {
            return undefined;
        }

        if (entity.dateExpiresAt.getTime() < new Date().getTime()) {
            await this.cleanUpRequestsOf(entity.account);

            return undefined;
        }

        return entity;
    }

    /**
     * Creates a new Reset Password Token
     * Old tokens will be automatically destroyed
     * @param account
     */
    async createRequestFor(account: AccountEntity<T>): Promise<AccountResetPasswordRequestEntity<T>> {
        await this.cleanUpRequestsOf(account);

        const entity = new AccountResetPasswordRequestEntity<T>();

        entity.account = account;
        entity.token = generateRandomHexString(BazaAccountConstants.TOKEN_LENGTH);
        entity.dateExpiresAt = moment(new Date()).add(this.moduleConfig.ttlTokensSeconds.changeEmail, 'seconds').toDate();

        await this.repository.save(entity);

        return entity;
    }

    /**
     * Destroys all Reset Password tokens
     * @param account
     */
    async cleanUpRequestsOf(account: AccountEntity<T>): Promise<void> {
        await this.repository.delete({
            account: account,
        });
    }
}
