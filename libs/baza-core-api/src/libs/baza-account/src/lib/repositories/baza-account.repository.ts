import { Injectable } from '@nestjs/common';
import { Connection, ILike, Repository } from 'typeorm';
import { AccountNotFoundException } from '../exceptions/account-not-found.exception';
import { AccountEntity } from '../entities/account.entity';
import { AccountRole, AccountSettingsDto, postgresLikeEscape } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaAccountRepository<T = AccountSettingsDto> {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeOrm repository for AccountEntity
     * @see AccountEntity
     * @see Repository
     */
    get repository(): Repository<AccountEntity<T>> {
        return this.connection.getRepository(AccountEntity) as Repository<AccountEntity<T>>;
    }

    /**
     * Saves AccountEntity(-ies) to DB
     * @param accountEntity
     */
    async save(accountEntity: AccountEntity<T>): Promise<AccountEntity<T>> {
        accountEntity.email = accountEntity.email.toLowerCase();

        await this.repository.save(accountEntity as any);

        return accountEntity;
    }

    /**
     * Returns IDs of all Accounts
     */
    async findAllIds(): Promise<Array<number>> {
        const qb = await this.repository.createQueryBuilder('account').select(['id']).getRawMany();

        return (qb || []).map((next) => next.id);
    }

    /**
     * Deletes accounts from DB
     * Do not use it unless for very specific cases. By default you should deactivate accounts instead of deleting
     * it from DB
     * @see BazaAccountDeactivateService
     * @param accountEntity
     */
    async deleteAccount(accountEntity: AccountEntity<T>): Promise<void> {
        await this.repository.remove(accountEntity);
    }

    /**
     * Returns active accounts by ID.
     * The method could return no result
     * @param accountId
     */
    async findActiveAccountWithId(accountId: number): Promise<AccountEntity<T> | undefined> {
        return this.repository.findOne({
            where: [{ id: accountId, isDeactivated: false }],
        });
    }

    /**
     * Returns active or deactivated account by ID.
     * The method could return no result
     * @param accountId
     */
    async findActiveOrDeactivatedAccountWithId(accountId: number): Promise<AccountEntity<T>> {
        return this.repository.findOne({
            where: [{ id: accountId }],
        });
    }

    /**
     * Returns true if there is an active accounts exists by ID
     * @param accountId
     */
    async hasActiveAccountWithId(accountId: number): Promise<boolean> {
        return (await this.findActiveAccountWithId(accountId)) !== undefined;
    }

    /**
     * Returns active account by ID
     * The method will throw an error in case if account was not found
     * @see AccountNotFoundException
     * @param accountId
     */
    async getActiveAccountWithId(accountId: number): Promise<AccountEntity<T>> {
        const result = await this.findActiveAccountWithId(accountId);

        if (!result) {
            throw new AccountNotFoundException({
                email: `(id: ${accountId})`,
            });
        }

        return result;
    }

    /**
     * Returns active or deactivated account by ID
     * The method will throw an error in case if account was not found
     * @see AccountNotFoundException
     * @param accountId
     */
    async getActiveOrDeactivatedAccountWithId(accountId: number): Promise<AccountEntity<T>> {
        const result = await this.findActiveOrDeactivatedAccountWithId(accountId);

        if (!result) {
            throw new AccountNotFoundException({
                email: `(id: ${accountId})`,
            });
        }

        return result;
    }

    /**
     * Returns active account by Email
     * The method could return no result
     * @param email
     */
    async findActiveAccountWithEmail(email: string): Promise<AccountEntity<T> | undefined> {
        return this.repository.findOne({
            where: [{ email: email.toLowerCase(), isDeactivated: false }],
        });
    }

    /**
     * Returns deactivated accounts by Email
     * @param email
     */
    async findDeactivatedAccountsWithEmail(email: string): Promise<Array<AccountEntity<T>>> {
        return this.repository.find({
            where: [{ email: email.toLowerCase(), isDeactivated: true }],
        });
    }

    /**
     * Returns active and deactivated accounts by Email
     * @param email
     */
    async findActiveOrDeactivatedAccountsWithEmail(email: string): Promise<Array<AccountEntity<T>>> {
        return this.repository.find({
            where: [{ email: email.toLowerCase() }],
            order: {
                isDeactivated: 'ASC',
            },
        });
    }

    /**
     * Returns active account by Email
     * The method will throw an error in case if account was not found
     * @see AccountNotFoundException
     * @param email
     */
    async getActiveAccountWithEmail(email: string): Promise<AccountEntity<T>> {
        const result = await this.findActiveAccountWithEmail(email.toLowerCase());

        if (!result) {
            throw new AccountNotFoundException({
                email,
            });
        }

        return result;
    }

    /**
     * Returns true if there is active account with specific email available
     * @param email
     */
    async hasActiveAccountWithEmail(email: string): Promise<boolean> {
        return (await this.findActiveAccountWithEmail(email.toLowerCase())) !== undefined;
    }

    /**
     * Returns true if there are deactivated accounts with specific email available
     * @param email
     */
    async hasDeactivatedAccountsWithEmail(email: string): Promise<boolean> {
        return (await this.findDeactivatedAccountsWithEmail(email.toLowerCase())).length > 0;
    }

    /**
     * Returns all active administrator accounts
     * @see AccountRole.Admin
     */
    async findAdminAccounts(): Promise<Array<AccountEntity<T>>> {
        return this.repository.find({
            where: [{ role: AccountRole.Admin, isDeactivated: false }],
        });
    }

    /**
     * Returns all active user accounts
     * The list will contains only id, email, firstName, lastName and fullName
     * @see AccountRole.User
     */
    async findUserAccounts(): Promise<Array<AccountEntity<T>>> {
        return this.repository.find({
            select: ['id', 'email', 'firstName', 'lastName', 'fullName'],
            where: [{ role: AccountRole.User, isDeactivated: false }],
        });
    }

    /**
     * Returns email by Account Id
     * The method will search only by email
     * The method will throw an error in case if account was not found
     * @see AccountNotFoundException
     * @param account
     */
    async getAccountEmail(account: AccountEntity | number) {
        if (typeof account === 'number') {
            return (await this.getActiveAccountWithId(account)).email;
        } else {
            return account.email;
        }
    }

    /**
     * Returns number of admin active accounts
     * @see AccountRole.Admin
     */
    async countAdminAccounts(): Promise<number> {
        return this.repository.count({
            where: [{ role: AccountRole.Admin, isDeactivated: false }],
        });
    }

    /**
     * Returns number of user active accounts
     * @see AccountRole.User
     */
    async countUserAccounts(): Promise<number> {
        return this.repository.count({
            where: [{ role: AccountRole.User, isDeactivated: false }],
        });
    }

    /**
     * Search for active accounts by partial fullName or email
     * The method will not filter result by AccountRole
     * @param partial
     */
    async searchActiveAccountsByFullNameOrEmail(partial: string): Promise<Array<AccountEntity<T>>> {
        return this.repository.find({
            select: ['id', 'email', 'firstName', 'lastName', 'fullName'],
            where: [
                { fullName: ILike(`%${postgresLikeEscape(partial)}%`), isDeactivated: false },
                { email: ILike(`%${postgresLikeEscape(partial)}%`), isDeactivated: false },
            ],
        });
    }

    /**
     * Returns all managed accounts
     * @see BazaAccountManagedUsersService
     */
    async findManagedAccounts(): Promise<Array<AccountEntity<T>>> {
        return this.repository.find({
            where: [
                {
                    isManagedUser: true,
                },
            ],
        });
    }
}
