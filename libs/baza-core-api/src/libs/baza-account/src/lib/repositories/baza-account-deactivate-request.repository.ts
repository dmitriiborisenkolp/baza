import { Inject, Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { AccountEntity } from '../entities/account.entity';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { AccountSettingsDto, BazaAccountConstants } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';
import { BAZA_ACCOUNT_CONFIG, BazaAccountConfig } from '../baza-account.config';
import { ACCOUNT_DEACTIVATE_REQUEST_ENTITY_RELATIONS, AccountDeactivateRequestEntity } from '../entities/account-deactivate-request.entity';

@Injectable()
export class BazaAccountDeactivateRequestRepository<T = AccountSettingsDto> {
    constructor(@Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig, private readonly connection: Connection) {}

    /**
     * Returns TypeOrm repository for AccountDeactivateRequestEntity
     * @see Repository
     * @see AccountDeactivateRequestEntity
     */
    get repository(): Repository<AccountDeactivateRequestEntity<T>> {
        return this.connection.getRepository(AccountDeactivateRequestEntity) as Repository<AccountDeactivateRequestEntity<T>>;
    }

    /**
     * Returns Deactivate Account Token entity (by Account)
     * The method could returns no result
     * @param account
     */
    async findConfirmationByAccount(account: AccountEntity<any>): Promise<AccountDeactivateRequestEntity<T> | undefined> {
        return this.repository.findOne({
            account: account,
        });
    }

    /**
     * Returns Deactivate Account Token entity (by Account)
     * The method could returns no result
     * @param token
     */
    async findConfirmationByToken(token: string): Promise<AccountDeactivateRequestEntity<T> | undefined> {
        const entity = await this.repository.findOne(
            {
                token: token,
            },
            {
                relations: ACCOUNT_DEACTIVATE_REQUEST_ENTITY_RELATIONS,
            },
        );

        if (!entity) {
            return undefined;
        }

        if (entity.dateExpiresAt.getTime() < new Date().getTime()) {
            await this.cleanUpRequestsOf(entity.account);

            return undefined;
        }

        return entity;
    }

    /**
     * Creates a new Deactivate Account Token
     * Old tokens will be automatically destroyed
     * @param account
     */
    async createRequestFor(account: AccountEntity<T>): Promise<AccountDeactivateRequestEntity<T>> {
        await this.cleanUpRequestsOf(account);

        const entity = new AccountDeactivateRequestEntity<T>();

        entity.account = account;
        entity.token = generateRandomHexString(BazaAccountConstants.TOKEN_LENGTH);
        entity.dateExpiresAt = moment(new Date()).add(this.moduleConfig.ttlTokensSeconds.changeEmail, 'seconds').toDate();

        await this.repository.save(entity);

        return entity;
    }

    /**
     * Destroys all Deactive Account tokens
     * @param account
     */
    async cleanUpRequestsOf(account: AccountEntity<any>): Promise<void> {
        await this.repository.delete({
            account: account,
        });
    }
}
