import { Inject, Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import {
    ACCOUNT_CONFIRM_EMAIL_REQUEST_ENTITY_RELATIONS,
    AccountConfirmEmailRequestEntity,
} from '../entities/account-confirm-email-request.entity';
import { AccountEntity } from '../entities/account.entity';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { AccountSettingsDto, BazaAccountConstants } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';
import { BAZA_ACCOUNT_CONFIG, BazaAccountConfig } from '../baza-account.config';

@Injectable()
export class BazaAccountConfirmEmailRequestRepository<T extends AccountSettingsDto = any> {
    constructor(@Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig, private readonly connection: Connection) {}

    /**
     * Returns TypeOrm repository for AccountConfirmEmailRequestEntity
     * @see Repository
     * @see AccountConfirmEmailRequestEntity
     */
    get repository(): Repository<AccountConfirmEmailRequestEntity<T>> {
        return this.connection.getRepository(AccountConfirmEmailRequestEntity) as Repository<AccountConfirmEmailRequestEntity<T>>;
    }

    /**
     * Returns Confirm Email Token entity (by Account)
     * The method can returns no value
     * @param account
     */
    async findConfirmationByAccount(account: AccountEntity<T>): Promise<AccountConfirmEmailRequestEntity<T> | undefined> {
        return this.repository.findOne({
            account: account as AccountEntity<T>,
        });
    }

    /**
     * Returns Confirm Email Token entity (by Token)
     * The method can returns no value
     * @param token
     * @param checkTokenExpiry
     */
    async findConfirmationByToken(token: string, checkTokenExpiry = true): Promise<AccountConfirmEmailRequestEntity<T> | undefined> {
        const entity = await this.repository.findOne(
            {
                token: token,
            },
            {
                relations: ACCOUNT_CONFIRM_EMAIL_REQUEST_ENTITY_RELATIONS,
            },
        );

        if (!entity) {
            return undefined;
        }

        if (checkTokenExpiry && entity.dateExpiresAt.getTime() < new Date().getTime()) {
            await this.cleanUpRequestsOf(entity.account);

            return undefined;
        }

        return entity;
    }

    /**
     * Creates a new Confirm Email Token
     * Old tokens will be automatically destroyed
     * @param account
     */
    async createRequestFor(account: AccountEntity<T>): Promise<AccountConfirmEmailRequestEntity<T>> {
        await this.cleanUpRequestsOf(account);

        const entity = new AccountConfirmEmailRequestEntity<T>();

        entity.account = account;
        entity.token = this.moduleConfig.customEmailVerificationTokenizer
            ? this.moduleConfig.customEmailVerificationTokenizer(account)
            : generateRandomHexString(BazaAccountConstants.TOKEN_LENGTH);
        entity.dateExpiresAt = moment(new Date()).add(this.moduleConfig.ttlTokensSeconds.changeEmail, 'seconds').toDate();

        await this.repository.save(entity);

        return entity;
    }

    /**
     * Destroys all Confirm Email tokens
     * @param account
     */
    async cleanUpRequestsOf(account: AccountEntity<T>): Promise<void> {
        await this.repository.delete({
            account: account as AccountEntity<T>,
        });
    }
}
