import { Inject, Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { AccountEntity } from '../entities/account.entity';
import {
    ACCOUNT_CHANGE_EMAIL_REQUEST_ENTITY_RELATIONS,
    AccountChangeEmailRequestEntity,
} from '../entities/account-change-email-request.entity';
import { Application, generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { AccountSettingsDto, BazaAccountConstants } from '@scaliolabs/baza-core-shared';
import { BAZA_ACCOUNT_CONFIG, BazaAccountConfig } from '../baza-account.config';
import * as moment from 'moment';

@Injectable()
export class BazaAccountChangeEmailRequestRepository<T extends AccountSettingsDto = any> {
    constructor(@Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig, private readonly connection: Connection) {}

    /**
     * Returns TypeOrm repository for AccountChangeEmailRequestEntity
     * @see AccountChangeEmailRequestEntity
     * @see Repository
     */
    get repository(): Repository<AccountChangeEmailRequestEntity<T>> {
        return this.connection.getRepository(AccountChangeEmailRequestEntity) as Repository<AccountChangeEmailRequestEntity<T>>;
    }

    /**
     * Returns Confirmation Token for Account (by AccountEntity)
     * The method can returns no value
     * @param account
     */
    async findConfirmationByAccount(account: AccountEntity<T>): Promise<AccountChangeEmailRequestEntity<T> | undefined> {
        return this.repository.findOne({
            account: account,
        });
    }

    /**
     * Returns Confirmation Token for Account (by Token)
     * The method can returns no value
     * @param token
     */
    async findConfirmationByToken(token: string): Promise<AccountChangeEmailRequestEntity<T> | undefined> {
        const entity = await this.repository.findOne(
            {
                token: token,
            },
            {
                relations: ACCOUNT_CHANGE_EMAIL_REQUEST_ENTITY_RELATIONS,
            },
        );

        if (!entity) {
            return undefined;
        }

        if (entity.dateExpiresAt.getTime() < new Date().getTime()) {
            await this.cleanUpRequestsOf(entity.account);

            return undefined;
        }

        return entity;
    }

    /**
     * Create a new change email request token entity. Old requests will be automatically destroyed.
     * @param account
     * @param newEmail
     * @param application
     */
    async createRequestFor(
        account: AccountEntity<T>,
        newEmail: string,
        application: Application,
    ): Promise<AccountChangeEmailRequestEntity<T>> {
        await this.cleanUpRequestsOf(account);

        const entity = new AccountChangeEmailRequestEntity<T>();

        entity.account = account;
        entity.application = application;
        entity.token = generateRandomHexString(BazaAccountConstants.TOKEN_LENGTH);
        entity.newEmail = newEmail;
        entity.dateExpiresAt = moment(new Date()).add(this.moduleConfig.ttlTokensSeconds.changeEmail, 'seconds').toDate();

        await this.repository.save(entity);

        return entity;
    }

    /**
     * Clean up (destroys) all change email request tokens
     * @param account
     */
    async cleanUpRequestsOf(account: AccountEntity<T>): Promise<void> {
        await this.repository.delete({
            account: account,
        });
    }
}
