import { RegisterAccountRequest } from '@scaliolabs/baza-core-shared';
import { RequestContext } from '../../../../baza-common/src';
import { AccountEntity } from '../entities/account.entity';

/**
 * CQRS Register Account Command
 * Use this command to register account as alternative to BazaAccountService.registerAccount
 * @see BazaAccountService.registerAccount
 */
export class BazaRegisterAccountCommand {
    constructor(
        public readonly request: RegisterAccountRequest,
        public readonly context: RequestContext,
        public readonly additionalMetadata?: any,
    ) {}
}

export type BazaRegisterAccountCommandResult = AccountEntity;
