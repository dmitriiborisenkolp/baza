import { AccountEntity } from '../entities/account.entity';
import { BazaAccountRegisterOptions } from '@scaliolabs/baza-core-shared';

/**
 * CQRS Account Options
 * Returns information / options for account registration.
 * If it's executed from authorized user, it also returns information about referral codes
 * Use this CQRS command as alternative to BazaAccountService.registerAccountOptions
 * @see BazaAccountService.registerAccountOptions
 */
export class BazaRegisterAccountOptionsCommand {
    constructor(public readonly account?: AccountEntity) {}
}

export type BazaRegisterAccountOptionsCommandResult = BazaAccountRegisterOptions;
