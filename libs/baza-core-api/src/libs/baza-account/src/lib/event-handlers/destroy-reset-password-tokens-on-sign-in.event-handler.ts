import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ApiCqrsEvent, AuthApiEvent, AuthApiEvents } from '@scaliolabs/baza-core-shared';
import { BazaAccountResetPasswordRequestRepository } from '../repositories/baza-account-reset-password-request.repository';
import { BazaAccountRepository } from '../repositories/baza-account.repository';
import { cqrs } from '../../../../baza-common/src';

/**
 * Automatically destroys all JWT tokens in database in case of resetting password
 * Baza is using whitelist strategy for JWT tokens
 */
@EventsHandler(ApiCqrsEvent)
export class DestroyResetPasswordTokensOnSignInEventHandler implements IEventHandler<ApiCqrsEvent<AuthApiEvent, AuthApiEvents>> {
    constructor(
        private readonly authRepository: BazaAccountRepository,
        private readonly tokensRepository: BazaAccountResetPasswordRequestRepository,
    ) {}

    handle(event: ApiCqrsEvent<AuthApiEvent, AuthApiEvents>): void {
        cqrs(DestroyResetPasswordTokensOnSignInEventHandler.name, async () => {
            switch (event.apiEvent.event.topic) {
                case AuthApiEvent.BazaAuth: {
                    const account = await this.authRepository.findActiveAccountWithId(
                        (event.apiEvent.event.payload as any).jwtPayload.accountId,
                    );

                    if (!account) {
                        return;
                    }

                    await this.tokensRepository.cleanUpRequestsOf(account);

                    break;
                }
            }
        });
    }
}
