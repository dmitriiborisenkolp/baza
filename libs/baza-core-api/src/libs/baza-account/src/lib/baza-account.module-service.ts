import { Inject, Injectable } from '@nestjs/common';
import { BazaAccountConfig, BazaAccountFeatures } from './baza-account.config';
import { BAZA_ACCOUNT_CONFIG } from './baza-account.config';
import { AccountEntity } from './entities/account.entity';
import { AccountModuleFeatureIsNotEnabledException } from './exceptions/account-module-feature-is-not-enabled.exception';

@Injectable()
export class BazaAccountModuleService {
    constructor(
        @Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig,
    ) {}

    validateFeature(account: AccountEntity<any>, feature: BazaAccountFeatures): void {
        if (! this.moduleConfig.features.find((f) => f.role === account.role && f.enabled.includes(feature))) {
            throw new AccountModuleFeatureIsNotEnabledException();
        }
    }
}
