import { Inject, Injectable } from '@nestjs/common';
import type { BazaAccountConfig } from '../baza-account.config';
import { BAZA_ACCOUNT_CONFIG } from '../baza-account.config';
import { AccountEntity } from '../entities/account.entity';
import { AccountResetPasswordRequestEntity } from '../entities/account-reset-password-request.entity';
import { AccountConfirmEmailRequestEntity } from '../entities/account-confirm-email-request.entity';
import { AccountSettingsDto, Application } from '@scaliolabs/baza-core-shared';
import { AccountChangeEmailRequestEntity } from '../entities/account-change-email-request.entity';
import { ProjectService } from '../../../../baza-common/src';
import { MailService } from '../../../../baza-mail/src';
import { AccountDeactivateRequestEntity } from '../entities/account-deactivate-request.entity';
import { BazaCoreMail } from '../../../../../constants/baza-core.mail';

/**
 * Baza Account Mail Service
 * Collection of Email Notifications for users
 * @private
 */
@Injectable()
export class BazaAccountMailService<T extends AccountSettingsDto = any> {
    constructor(
        @Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig<T>,
        private readonly mail: MailService,
        private readonly project: ProjectService,
    ) {}

    /**
     * Sends Confirm Email Token to user
     * @param request
     */
    async sendConfirmEmailLink(request: {
        app: Application;
        account: AccountEntity<T>;
        confirmRequest: AccountConfirmEmailRequestEntity<T>;
    }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        await this.mail.send({
            name: BazaCoreMail.BazaAccountWebConfirmEmail,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-account.confirm-email.subject',
            variables: () => ({
                token: request.confirmRequest.token,
                link: this.moduleConfig.sendConfirmEmailLink.urlGenerator(request.app, request.confirmRequest),
                site: this.project.createLinkToApplication({
                    application: request.app,
                }),
                firstName: request.confirmRequest.account.firstName,
                lastName: request.confirmRequest.account.lastName,
                fullName: request.confirmRequest.account.fullName,
                email: request.confirmRequest.account.email,
            }),
        });
    }

    /**
     * Sends Reset Password Token to user
     * @param request
     */
    async sendResetPasswordRequest(request: {
        app: Application;
        account: AccountEntity<T>;
        resetPasswordRequest: AccountResetPasswordRequestEntity<T>;
    }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        await this.mail.send({
            name: request.app === Application.CMS ? BazaCoreMail.BazaAccountCmsResetPassword : BazaCoreMail.BazaAccountWebResetPassword,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-account.reset-password.subject',
            variables: () => ({
                token: request.resetPasswordRequest.token,
                link: this.moduleConfig.resetPasswordRequest.urlGenerator(request.app, request.resetPasswordRequest),
                site: this.project.createLinkToApplication({
                    application: request.app,
                }),
                firstName: request.resetPasswordRequest.account.firstName,
                lastName: request.resetPasswordRequest.account.lastName,
                fullName: request.resetPasswordRequest.account.fullName,
                email: request.resetPasswordRequest.account.email,
            }),
        });
    }

    /**
     * Sends Manual Reset Password Token to user
     * @param request
     */
    async sendManualResetPasswordRequest(request: {
        account: AccountEntity<T>;
        resetPasswordRequest: AccountResetPasswordRequestEntity<T>;
    }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        if (!request.account.manualResetPasswordRequest) {
            return;
        }

        await this.mail.send({
            name: BazaCoreMail.BazaAccountWebRequestResetPassword,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: request.account.manualResetPasswordRequest.subject,
            variables: () => ({
                token: request.resetPasswordRequest.token,
                link: this.moduleConfig.cmsRequestResetPassword.urlGenerator(Application.WEB, request.resetPasswordRequest),
                site: this.project.createLinkToApplication({
                    application: Application.WEB,
                }),
                firstName: request.account.firstName,
                lastName: request.account.lastName,
                fullName: request.account.fullName,
                email: request.account.email,
                subject: request.account.manualResetPasswordRequest.subject,
                message: request.account.manualResetPasswordRequest.message,
            }),
        });
    }

    /**
     * Sends notification about changed password to user
     * @param request
     */
    async sendPasswordChangedNotification(request: { app: Application; account: AccountEntity<T> }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        await this.mail.send({
            name: request.app === Application.CMS ? BazaCoreMail.BazaAccountCmsPasswordChanged : BazaCoreMail.BazaAccountWebPasswordChanged,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-account.password-changed.subject',
            variables: () => ({
                site: this.project.createLinkToApplication({
                    application: request.app,
                }),
                firstName: request.account.firstName,
                lastName: request.account.lastName,
                fullName: request.account.fullName,
                email: request.account.email,
                emailUrlEncoded: encodeURI(request.account.email),
            }),
        });
    }

    /**
     * Sends Change Email Token to user
     * @param request
     */
    async sendChangeEmailLink(request: {
        account: AccountEntity<T>;
        changeEmailRequest: AccountChangeEmailRequestEntity<T>;
        app: Application;
    }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        await this.mail.send({
            name: request.app === Application.CMS ? BazaCoreMail.BazaAccountCmsChangeEmail : BazaCoreMail.BazaAccountWebChangeEmail,
            to: [
                {
                    name: request.account.fullName,
                    email: request.changeEmailRequest.newEmail,
                },
            ],
            subject: 'baza-account.change-email.subject',
            variables: () => ({
                token: request.changeEmailRequest.token,
                oldEmail: request.account.email,
                newEmail: request.changeEmailRequest.newEmail,
                link: this.moduleConfig.changeEmailRequest.urlGenerator(Application.WEB, request.changeEmailRequest),
                site: this.project.createLinkToApplication({
                    application: request.app,
                }),
                firstName: request.account.firstName,
                lastName: request.account.lastName,
                fullName: request.account.fullName,
                email: request.account.email,
            }),
        });
    }

    /**
     * Sends notification about changed email to user
     * @param request
     */
    async sendChangeEmailNotification(request: {
        account: AccountEntity<T>;
        changeEmailRequest: AccountChangeEmailRequestEntity<T>;
        app: Application;
    }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        await this.mail.send({
            name:
                request.app === Application.CMS
                    ? BazaCoreMail.BazaAccountCmsChangeEmailNotification
                    : BazaCoreMail.BazaAccountWebChangeEmailNotification,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-account.change-email-notification.subject',
            variables: () => ({
                token: request.changeEmailRequest.token,
                link: this.moduleConfig.sendConfirmEmailLink.urlGenerator(request.app, request.changeEmailRequest),
                oldEmail: request.account.email,
                newEmail: request.changeEmailRequest.newEmail,
                site: this.project.createLinkToApplication({
                    application: request.app,
                }),
                firstName: request.account.firstName,
                lastName: request.account.lastName,
                fullName: request.account.fullName,
                email: request.account.email,
            }),
        });
    }

    /**
     * Sends Reset Password token for Duplicate Account Registration case
     * TODO: Investigate why it's not used anymore
     * @param request
     */
    async sendAccountAlreadyRegisteredNotification(request: {
        account: AccountEntity<T>;
        resetPasswordRequest: AccountResetPasswordRequestEntity<T>;
    }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        await this.mail.send({
            name: BazaCoreMail.BazaAccountWebAlreadyRegistered,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-account.already-registered.subject',
            variables: () => ({
                firstName: request.resetPasswordRequest.account.firstName,
                lastName: request.resetPasswordRequest.account.lastName,
                fullName: request.resetPasswordRequest.account.fullName,
                email: request.resetPasswordRequest.account.email,
            }),
        });
    }

    /**
     * Sends notification about deactivating account to user
     * @param request
     */
    async sendAccountDeactivatedNotification(request: { account: AccountEntity<T> }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        await this.mail.send({
            name: BazaCoreMail.BazaAccountWebAccountDeactivated,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-account.account-deactivated.subject',
            variables: () => ({
                site: this.project.createLinkToApplication({
                    application: Application.WEB,
                }),
                firstName: request.account.firstName,
                lastName: request.account.lastName,
                fullName: request.account.fullName,
                email: request.account.email,
                emailUrlEncoded: encodeURI(request.account.email),
            }),
        });
    }

    /**
     * Sends Deactivate Account Token to User
     * @param request
     */
    async sendDeactivateAccountRequest(request: {
        app: Application;
        account: AccountEntity<T>;
        deactivateAccountRequest: AccountDeactivateRequestEntity<T>;
    }): Promise<void> {
        if (!this.mail.isServiceEnabled) {
            return;
        }

        await this.mail.send({
            name: BazaCoreMail.BazaAccountWebDeactivateAccount,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-account.deactivate-account.subject',
            variables: () => ({
                token: request.deactivateAccountRequest.token,
                link: this.moduleConfig.deactivateAccountLink.urlGenerator(request.app, request.deactivateAccountRequest),
                site: this.project.createLinkToApplication({
                    application: request.app,
                }),
                firstName: request.deactivateAccountRequest.account.firstName,
                lastName: request.deactivateAccountRequest.account.lastName,
                fullName: request.deactivateAccountRequest.account.fullName,
                email: request.deactivateAccountRequest.account.email,
            }),
        });
    }
}
