import { Injectable } from '@nestjs/common';
import { AccountManagedUserOperationNotPermittedException } from '../exceptions/account-managed-user-operation-not-permitted.exception';
import {
    AccountApiEvent,
    AccountApiEvents,
    AccountSettingsDto,
    Application,
    BazaAuthDestroyJwtsOfAccountCommand,
    BazaDeviceTokenDestroyCommand,
    DeactivateAccountRequest,
    defaultAccountSettings,
    SendDeactivateAccountLinkRequest,
} from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../entities/account.entity';
import { AccountIsDeactivatedException } from '../exceptions/account-is-deactivated.exception';
import { BazaAccountManagedUsersService } from './baza-account-managed-users.service';
import { BazaAccountModuleService } from '../baza-account.module-service';
import { BazaAccountConfirmEmailRequestRepository } from '../repositories/baza-account-confirm-email-request.repository';
import { BazaAccountResetPasswordRequestRepository } from '../repositories/baza-account-reset-password-request.repository';
import { BazaAccountChangeEmailRequestRepository } from '../repositories/baza-account-change-email-request.repository';
import { BazaAccountDeactivateRequestRepository } from '../repositories/baza-account-deactivate-request.repository';
import { BazaAccountFeatures } from '../baza-account.config';
import { BazaAccountRepository } from '../repositories/baza-account.repository';
import { ApiEventBus } from '../../../../baza-event-bus/src';
import { BazaAccountMailService } from './baza-account-mail.service';
import { AccountTokenNotFoundException } from '../exceptions/account-token-not-found.exception';
import { BcryptService } from '../../../../baza-common/src';
import { AccountInvalidPasswordException } from '../exceptions/account-invalid-password.exception';
import { CommandBus } from '@nestjs/cqrs';

/**
 * Baza Account Deactivate Service
 * Instead of deleting Accounts you should Deactivate it. Additionally, you should clean up all sensitive data if possible
 */
@Injectable()
export class BazaAccountDeactivateService<T extends AccountSettingsDto = any> {
    constructor(
        private readonly commandBus: CommandBus,
        private readonly bcrypt: BcryptService,
        private readonly bazaEventBus: ApiEventBus<AccountApiEvent, AccountApiEvents>,
        private readonly managedUserAccounts: BazaAccountManagedUsersService,
        private readonly moduleConfigService: BazaAccountModuleService,
        private readonly accountRepository: BazaAccountRepository<T>,
        private readonly accountMailService: BazaAccountMailService<T>,
        private readonly accountConfirmEmailRequestRepository: BazaAccountConfirmEmailRequestRepository<T>,
        private readonly accountResetPasswordRequestRepository: BazaAccountResetPasswordRequestRepository<T>,
        private readonly accountChangeEmailRequestRepository: BazaAccountChangeEmailRequestRepository<T>,
        private readonly accountDeactivateRequestRepository: BazaAccountDeactivateRequestRepository<T>,
    ) {}

    /**
     * Deactivates Account
     * @param account
     */
    async deactivateAccount(account: AccountEntity<T>): Promise<AccountEntity<T>> {
        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        if (this.managedUserAccounts.isManagedUser(account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        this.moduleConfigService.validateFeature(account, BazaAccountFeatures.Deactivate);

        await this.accountConfirmEmailRequestRepository.cleanUpRequestsOf(account);
        await this.accountChangeEmailRequestRepository.cleanUpRequestsOf(account);
        await this.accountResetPasswordRequestRepository.cleanUpRequestsOf(account);
        await this.accountDeactivateRequestRepository.cleanUpRequestsOf(account);

        account.fullName = 'Deleted';
        account.firstName = 'Deleted';
        account.lastName = '';
        account.isDeactivated = true;
        account.password = null;
        account.settings = defaultAccountSettings() as any;
        account.metadata = {};
        account.manualResetPasswordRequest = null;

        await this.commandBus.execute(
            new BazaAuthDestroyJwtsOfAccountCommand({
                accountId: account.id,
            }),
        );

        await this.commandBus.execute(
            new BazaDeviceTokenDestroyCommand({
                accountId: account.id,
                scope: 'All',
            }),
        );

        await this.accountRepository.save(account);

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountDeactivated,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
            },
        });

        return account;
    }

    /**
     * Sends Deactivate Account Token email to user
     * @see BazaAccountDeactivateService.deactivateAccountWithToken
     * @param account
     * @param request
     * @param options
     */
    async sendDeactivateAccountLink(
        account: AccountEntity<T>,
        request: SendDeactivateAccountLinkRequest,
        options: {
            application: Application;
        },
    ): Promise<void> {
        if (this.managedUserAccounts.isManagedUser(account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        this.moduleConfigService.validateFeature(account, BazaAccountFeatures.Deactivate);

        if (!(await this.bcrypt.compare(request.password, account.password))) {
            throw new AccountInvalidPasswordException();
        }

        const deactivateAccountRequest = await this.accountDeactivateRequestRepository.createRequestFor(account);

        await this.accountMailService.sendDeactivateAccountRequest({
            account,
            deactivateAccountRequest,
            app: options.application,
        });

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountDeactivateRequestSent,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                accountDeactivateRequestId: deactivateAccountRequest.id,
                token: deactivateAccountRequest.token,
            },
        });
    }

    /**
     * Deactivates Account with Deactivate Account Token
     * @see BazaAccountDeactivateService.sendDeactivateAccountLink
     * @param request
     * @param options
     */
    async deactivateAccountWithToken(request: DeactivateAccountRequest): Promise<void> {
        const deactivateAccountRequest = await this.accountDeactivateRequestRepository.findConfirmationByToken(request.token);

        if (!deactivateAccountRequest) {
            throw new AccountTokenNotFoundException();
        }

        if (this.managedUserAccounts.isManagedUser(deactivateAccountRequest.account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        this.moduleConfigService.validateFeature(deactivateAccountRequest.account, BazaAccountFeatures.Deactivate);

        const accountUlid = deactivateAccountRequest.account.ulid;
        const accountFirstName = deactivateAccountRequest.account.firstName;
        const accountLastName = deactivateAccountRequest.account.lastName;
        const accountFullName = deactivateAccountRequest.account.fullName;

        await this.deactivateAccount(deactivateAccountRequest.account);

        await this.accountMailService.sendAccountDeactivatedNotification({
            account: {
                ...deactivateAccountRequest.account,
                ulid: accountUlid,
                firstName: accountFirstName,
                lastName: accountLastName,
                fullName: accountFullName,
            },
        });
    }
}
