import { Injectable } from '@nestjs/common';
import { AccountNotFoundException } from '../../exceptions/account-not-found.exception';
import { BazaAccountService } from '../baza-account.service';
import { BazaAccountRepository } from '../../repositories/baza-account.repository';
import {
    AccountApiEvent,
    AccountApiEvents,
    AccountCmsDto,
    AccountCsvDto,
    accountFullName,
    AccountRole,
    AssignAdminAccessCmsRequest,
    BazaAuthDestroyJwtsOfAccountCommand,
    ChangeEmailAccountCmsRequest,
    ConfirmEmailAccountByIdCmsRequest,
    DeleteAccountByIdCmsRequest,
    ExportToCsvAccountRequest,
    GetAccountByEmailCmsRequest,
    GetAccountByIdCmsRequest,
    isId,
    ListAccountsCmsRequest,
    ListAccountsCmsResponse,
    RegisterAdminAccountCmsRequest,
    RegisterUserAccountCmsRequest,
    RevokeAdminAccessCmsRequest,
    UpdateAccountCmsRequest,
} from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../../entities/account.entity';
import { Application, generateRandomHexString, postgresLikeEscape } from '@scaliolabs/baza-core-shared';
import { AccountDuplicateException } from '../../exceptions/account-duplicate.exception';
import { ILike, In } from 'typeorm';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { BazaAccountCsvMapper } from '../../mappers/baza-account-csv.mapper';
import { BazaAccountManagedUsersService } from '../baza-account-managed-users.service';
import { AccountManagedUserOperationNotPermittedException } from '../../exceptions/account-managed-user-operation-not-permitted.exception';
import { ApiEventBus } from '../../../../../baza-event-bus/src';
import { BcryptService } from '../../../../../baza-common/src';
import { CrudCsvService, CrudService } from '../../../../../baza-crud/src';
import { AccountIsDeactivatedException } from '../../exceptions/account-is-deactivated.exception';
import { BazaAccountDeactivateService } from '../baza-account-deactivate.service';
import { CommandBus } from '@nestjs/cqrs';
import { AccountChangeEmailSameException } from '../../exceptions/account-change-email-same.exception';
import { AccountChangeEmailDuplicateException } from '../../exceptions/account-change-email-duplicate.exception';
import { BazaAccountCmsMapper } from '../../mappers/baza-account-cms.mapper';
import { AccountSelfAttemptUnverifyException } from '../../exceptions/account-self-attempt-unverify.exception';
import { BazaPasswordService } from '../../../../../baza-password/src';
import { AccountPasswordIsNotStrongEnoughException } from '../../exceptions/account-password-is-not-strong-enough.exception';
import { AccountPasswordsDoNotMatchException } from '../../exceptions/account-passwords-do-not-match.exception';

const GENERATED_PASSWORD_HEX_LENGTH = 16;

/**
 * Baza Account CMS Service
 * The service is mostly used for CMS Controllers
 */
@Injectable()
export class BazaAccountCmsService {
    constructor(
        private readonly commandBus: CommandBus,
        private readonly crud: CrudService,
        private readonly crudCsvService: CrudCsvService,
        private readonly bcrypt: BcryptService,
        private readonly bazaEventBus: ApiEventBus<AccountApiEvent, AccountApiEvents>,
        private readonly accountRepository: BazaAccountRepository,
        private readonly accountService: BazaAccountService,
        private readonly accountMapper: BazaAccountCmsMapper,
        private readonly accountCsvMapper: BazaAccountCsvMapper,
        private readonly managedUserAccounts: BazaAccountManagedUsersService,
        private readonly deactivateAccountService: BazaAccountDeactivateService,
        private readonly passwordService: BazaPasswordService,
    ) {}

    /**
     * Register Admin Account.
     * The method is used for Account CMS -> Register Admin and for Managed Users feature
     * @param request
     * @param options
     */
    async registerAdminAccount(
        request: RegisterAdminAccountCmsRequest,
        options = {
            withPasswordValidation: false,
            ignoreManagedUserAccountRestrictions: false,
            destroyJwtSessions: true,
        },
    ): Promise<AccountEntity> {
        if (await this.accountRepository.hasActiveAccountWithEmail(request.email)) {
            throw new AccountDuplicateException({
                email: request.email,
            });
        }

        if (!options.ignoreManagedUserAccountRestrictions && this.managedUserAccounts.isManagedUser(request.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        if (request.password && options.withPasswordValidation) {
            if (request.passwordConfirm && request.passwordConfirm !== request.password) {
                throw new AccountPasswordsDoNotMatchException();
            }

            const passwordResponse = await this.passwordService.validate({
                password: request.password,
            });

            if (!passwordResponse.isValid) {
                throw new AccountPasswordIsNotStrongEnoughException();
            }
        }

        const accountEntity: AccountEntity = new AccountEntity();

        accountEntity.role = AccountRole.Admin;
        accountEntity.email = request.email;
        accountEntity.isEmailConfirmed = true;
        accountEntity.firstName = request.firstName;
        accountEntity.lastName = request.lastName;
        accountEntity.metadata = request.metadata;

        accountEntity.fullName = accountFullName(accountEntity);

        if (request.profileImages) {
            accountEntity.profileImages = request.profileImages;
        }

        if (request.password) {
            accountEntity.password = await this.bcrypt.hash(request.password);
        } else {
            accountEntity.password = await this.bcrypt.hash(generateRandomHexString(GENERATED_PASSWORD_HEX_LENGTH));
        }

        await this.accountRepository.save(accountEntity);

        await this.assignAdminRole(
            {
                email: request.email,
            },
            {
                ignoreManagedUserAccountRestrictions: options.ignoreManagedUserAccountRestrictions,
                destroyJwtSessions: options.destroyJwtSessions,
            },
        );

        if (!request.password) {
            await this.accountService.sendResetPasswordLinkRequest(
                {
                    email: accountEntity.email,
                },
                {
                    application: Application.CMS,
                },
            );
        }

        return accountEntity;
    }

    /**
     * Register User Account.
     * The method is used for Account CMS -> Register User and Managed User features
     * @param request
     * @param options
     */
    async registerUserAccount(
        request: RegisterUserAccountCmsRequest,
        options = {
            withPasswordValidation: false,
            ignoreManagedAccountRestrictions: false,
        },
    ): Promise<AccountEntity> {
        if (await this.accountRepository.hasActiveAccountWithEmail(request.email)) {
            throw new AccountDuplicateException({
                email: request.email,
            });
        }

        if (!options.ignoreManagedAccountRestrictions && this.managedUserAccounts.isManagedUser(request.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        if (request.password && options.withPasswordValidation) {
            if (request.passwordConfirm && request.passwordConfirm !== request.password) {
                throw new AccountPasswordsDoNotMatchException();
            }

            const passwordResponse = await this.passwordService.validate({
                password: request.password,
            });

            if (!passwordResponse.isValid) {
                throw new AccountPasswordIsNotStrongEnoughException();
            }
        }

        const accountEntity: AccountEntity = new AccountEntity();

        accountEntity.role = AccountRole.User;
        accountEntity.email = request.email;
        accountEntity.isEmailConfirmed = true;
        accountEntity.firstName = request.firstName;
        accountEntity.lastName = request.lastName;
        accountEntity.metadata = request.metadata;

        accountEntity.fullName = accountFullName(accountEntity);

        if (request.profileImages) {
            accountEntity.profileImages = request.profileImages;
        }

        if (request.password) {
            accountEntity.password = await this.bcrypt.hash(request.password);
        } else {
            accountEntity.password = await this.bcrypt.hash(generateRandomHexString(GENERATED_PASSWORD_HEX_LENGTH));
        }

        await this.accountRepository.save(accountEntity);

        if (!request.password) {
            await this.accountService.sendResetPasswordLinkRequest(
                {
                    email: accountEntity.email,
                },
                {
                    application: Application.WEB,
                },
            );
        }

        return accountEntity;
    }

    /**
     * Returns list of all available active admin accounts (full entities)
     */
    async listAdminAccounts(): Promise<Array<AccountEntity>> {
        return this.accountRepository.findAdminAccounts();
    }

    /**
     * Returns number of all available admin accounts
     */
    async countAdminAccounts(): Promise<number> {
        return this.accountRepository.countAdminAccounts();
    }

    /**
     * Returns List of Accounts
     * @param request
     */
    async listAccounts(request: ListAccountsCmsRequest): Promise<ListAccountsCmsResponse> {
        const where: Array<FindConditions<AccountEntity>> = [];

        const baseFindCondition: () => FindConditions<AccountEntity> = () => {
            return Array.isArray(request.roles) && request.roles.length > 0
                ? {
                      role: In(request.roles),
                      isDeactivated: false,
                  }
                : { isDeactivated: false };
        };

        if (request.queryString) {
            const id = parseInt(request.queryString, 10);

            if (isId(id)) {
                where.push({ ...baseFindCondition(), id });
            } else {
                where.push(
                    { ...baseFindCondition(), fullName: ILike(`%${postgresLikeEscape(request.queryString)}%`) },
                    { ...baseFindCondition(), email: ILike(`%${postgresLikeEscape(request.queryString)}%`) },
                );
            }
        } else {
            where.push(baseFindCondition());
        }

        return this.crud.find<AccountEntity, AccountCmsDto>({
            request,
            entity: AccountEntity,
            findOptions: {
                where,
                order: {
                    id: 'DESC',
                },
            },
            mapper: (accounts) => Promise.resolve(this.accountMapper.entitiesToDTOs(accounts)),
        });
    }

    /**
     * Assign Admin Role to account
     * The method is used for Account CMS -> Assign Admin Role feature
     * The method will not set ACL for new admin
     * @param request
     * @param options
     */
    async assignAdminRole(
        request: AssignAdminAccessCmsRequest,
        options = {
            ignoreManagedUserAccountRestrictions: false,
            destroyJwtSessions: true,
        },
    ): Promise<AccountEntity> {
        const account = await this.accountRepository.findActiveAccountWithEmail(request.email);

        if (!options.ignoreManagedUserAccountRestrictions && this.managedUserAccounts.isManagedUser(request.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        if (!account) {
            throw new AccountNotFoundException({
                email: request.email,
            });
        }

        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        account.role = AccountRole.Admin;

        if (options.destroyJwtSessions) {
            await this.commandBus.execute(
                new BazaAuthDestroyJwtsOfAccountCommand({
                    accountId: account.id,
                }),
            );
        }

        await this.accountRepository.save(account);

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountAdminAccessAssigned,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
            },
        });

        return account;
    }

    /**
     * Revoke Admin Role from account
     * The method is used for Account CMS -> Revoke Admin feature
     * @param request
     */
    async revokeAdminRole(request: RevokeAdminAccessCmsRequest): Promise<AccountEntity> {
        const account = await this.accountRepository.findActiveAccountWithEmail(request.email);

        if (this.managedUserAccounts.isManagedUser(request.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        if (!account) {
            throw new AccountNotFoundException({
                email: request.email,
            });
        }

        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        account.role = AccountRole.User;

        await this.commandBus.execute(
            new BazaAuthDestroyJwtsOfAccountCommand({
                accountId: account.id,
            }),
        );

        await this.accountRepository.save(account);

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountAdminAccessRevoked,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
            },
        });

        return account;
    }

    /**
     * Deletes account by ID
     * Do not use it unless very specific cases. You should deactivate accounts instead of deleting them from database
     * @see BazaAccountCmsService.deactivateAccount
     * @param request
     * @deprecated
     */
    async deleteAccountById(request: DeleteAccountByIdCmsRequest): Promise<void> {
        const account = await this.accountRepository.getActiveOrDeactivatedAccountWithId(request.id);

        if (this.managedUserAccounts.isManagedUser(account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        const accountId = account.id;
        const accountEmail = account.email.toLowerCase();

        await this.accountRepository.deleteAccount(account);

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountDeleted,
            payload: {
                accountId,
                accountEmail,
            },
        });
    }

    /**
     * Confirm account (confirm email)
     * The method is used for Account CMS -> Confirm Email feature
     * @param request
     * @param sessionAccount
     */
    async confirmEmailAccountById(request: ConfirmEmailAccountByIdCmsRequest, sessionAccount?: AccountEntity): Promise<AccountEntity> {
        const account = await this.accountRepository.getActiveAccountWithId(request.id);

        if (sessionAccount && account.id === sessionAccount.id) {
            throw new AccountSelfAttemptUnverifyException({
                email: account.email,
            });
        }

        if (this.managedUserAccounts.isManagedUser(account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        if (request.confirmed === false) {
            account.isEmailConfirmed = false;

            await this.commandBus.execute(
                new BazaAuthDestroyJwtsOfAccountCommand({
                    accountId: account.id,
                }),
            );

            await this.accountRepository.repository.save(account);

            await this.bazaEventBus.publish({
                topic: AccountApiEvent.BazaAccountUnConfirmedEmail,
                payload: {
                    accountId: account.id,
                    accountEmail: account.email,
                },
            });
        } else {
            account.isEmailConfirmed = true;

            await this.accountRepository.repository.save(account);

            await this.bazaEventBus.publish({
                topic: AccountApiEvent.BazaAccountConfirmedEmail,
                payload: {
                    accountId: account.id,
                    accountEmail: account.email,
                },
            });
        }

        return account;
    }

    /**
     * Updates Account
     * @param request
     */
    async updateAccount(request: UpdateAccountCmsRequest): Promise<AccountEntity> {
        const account = await this.accountRepository.getActiveAccountWithId(request.id);

        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        account.firstName = request.firstName || account.firstName;
        account.lastName = request.lastName || account.lastName;
        account.phone = request.phone || account.phone;
        account.metadata = request.metadata || account.metadata;
        account.profileImages = request.profileImages || account.profileImages;

        account.fullName = accountFullName(account);

        if (request.password) {
            if (request.passwordConfirm && request.passwordConfirm !== request.password) {
                throw new AccountPasswordsDoNotMatchException();
            }

            if (this.managedUserAccounts.isManagedUser(account.email)) {
                throw new AccountManagedUserOperationNotPermittedException();
            }

            account.password = await this.bcrypt.hash(request.password);
        }

        await this.accountRepository.save(account);

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountUpdated,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
            },
        });

        return account;
    }

    /**
     * Exports Accounts List to CSV
     * @param request
     */
    async exportToCsv(request: ExportToCsvAccountRequest): Promise<string> {
        const baseFindCondition: () => FindConditions<AccountEntity> = () => {
            return Array.isArray(request.listRequest.roles) && request.listRequest.roles.length > 0
                ? {
                      role: In(request.listRequest.roles),
                      isDeactivated: false,
                  }
                : { isDeactivated: false };
        };

        return this.crudCsvService.exportToCsv<AccountEntity, AccountCsvDto>({
            entity: AccountEntity,
            mapper: async (items) => this.accountCsvMapper.entitiesToCSVRows(items),
            request: request.listRequest as any,
            delimiter: request.delimiter,
            fields: request.fields,
            findOptions: {
                where: baseFindCondition(),
                order: {
                    id: 'DESC',
                },
            },
        });
    }

    /**
     * Returns Account by ID
     * @param request
     */
    async getAccountById(request: GetAccountByIdCmsRequest): Promise<AccountEntity> {
        return this.accountRepository.getActiveOrDeactivatedAccountWithId(request.id);
    }

    /**
     * Returns Account by Email
     * @param request
     */
    async getAccountByEmail(request: GetAccountByEmailCmsRequest): Promise<AccountEntity> {
        return this.accountRepository.getActiveAccountWithEmail(request.email);
    }

    /**
     * Deactivates Account
     * The method is used for Account CMS -> Deactivate
     * @param account
     */
    async deactivateAccount(account: AccountEntity): Promise<AccountEntity> {
        return this.deactivateAccountService.deactivateAccount(account);
    }

    /**
     * Changes Email
     * The method is used for Account CMS -> Change Email feature
     * @param request
     */
    async changeEmail(request: ChangeEmailAccountCmsRequest): Promise<AccountEntity> {
        const account = await this.accountRepository.getActiveAccountWithId(request.id);

        const previousEmail = account.email.trim();
        const newEmail = request.email.trim();

        if (previousEmail === newEmail) {
            throw new AccountChangeEmailSameException({
                email: newEmail,
            });
        }

        if (await this.accountRepository.hasActiveAccountWithEmail(newEmail)) {
            throw new AccountChangeEmailDuplicateException();
        }

        account.email = newEmail;

        await this.accountRepository.save(account);

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountEmailChanged,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                previousEmail,
            },
        });

        await this.commandBus.execute(
            new BazaAuthDestroyJwtsOfAccountCommand({
                accountId: account.id,
            }),
        );

        return account;
    }
}
