import { CommandBus } from '@nestjs/cqrs';
import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { BazaAccountRepository } from '../repositories/baza-account.repository';
import {
    AccountApiEvent,
    AccountApiEvents,
    AccountExistsRequest,
    AccountExistsResponse,
    accountFullName,
    BazaAccountRegisterOptions,
    AccountRole,
    AccountSettingsDto,
    Application,
    BazaAuthDestroyJwtsOfAccountCommand,
    ChangeEmailRequest,
    ChangePasswordRequest,
    ConfirmEmailRequest,
    ProjectLanguage,
    RegisterAccountRequest,
    ResetPasswordRequest,
    SendChangeEmailLinkRequest,
    SendConfirmEmailLinkRequest,
    SendResetPasswordLinkRequest,
    SetAccountSettingsRequest,
    UpdateProfileRequest,
    ConfirmationEmailExpiryResendStrategy,
} from '@scaliolabs/baza-core-shared';
import { AccountTokenNotFoundException } from '../exceptions/account-token-not-found.exception';
import { AccountInvalidCredentialsException } from '../exceptions/account-invalid-credentials.exception';
import { BazaAccountConfirmEmailRequestRepository } from '../repositories/baza-account-confirm-email-request.repository';
import { BazaAccountResetPasswordRequestRepository } from '../repositories/baza-account-reset-password-request.repository';
import { AccountEntity } from '../entities/account.entity';
import type { BazaAccountConfig } from '../baza-account.config';
import { BAZA_ACCOUNT_CONFIG, BazaAccountFeatures } from '../baza-account.config';
import { AccountResetPasswordRequestEntity } from '../entities/account-reset-password-request.entity';
import { BazaAccountMailService } from './baza-account-mail.service';
import { AccountPasswordIsNotStrongEnoughException } from '../exceptions/account-password-is-not-strong-enough.exception';
import { BazaAccountChangeEmailRequestRepository } from '../repositories/baza-account-change-email-request.repository';
import { AccountChangeEmailRequestEntity } from '../entities/account-change-email-request.entity';
import { BazaAccountModuleService } from '../baza-account.module-service';
import { AccountChangeEmailDuplicateException } from '../exceptions/account-change-email-duplicate.exception';
import { AccountInvalidPasswordException } from '../exceptions/account-invalid-password.exception';
import { BazaAccountManagedUsersService } from './baza-account-managed-users.service';
import { AccountManagedUserOperationNotPermittedException } from '../exceptions/account-managed-user-operation-not-permitted.exception';
import { ApiEventBus } from '../../../../baza-event-bus/src';
import { BcryptService, RequestContext } from '../../../../baza-common/src';
import { BazaPasswordService } from '../../../../baza-password/src';
import { BazaAccountDeactivateRequestRepository } from '../repositories/baza-account-deactivate-request.repository';
import { AccountIsDeactivatedException } from '../exceptions/account-is-deactivated.exception';
import { AccountChangeEmailSameException } from '../exceptions/account-change-email-same.exception';
import { BazaAccountDeactivateService } from './baza-account-deactivate.service';
import { AccountPasswordsDoNotMatchException } from '../exceptions/account-passwords-do-not-match.exception';
import { AccountTokenExpiredException } from '../exceptions/account-token-expired.exception';
import { AccountTokenExpiredExceptionWithNewEmailSent } from '../exceptions/account-token-expired-with-new-email-sent.exception';
import { BazaRegistryService } from '../../../../baza-registry/src';
import { BazaRegisterAccountCommand } from '../commands/baza-register-account.command';
import { BazaRegisterAccountOptionsCommand } from '../commands/baza-register-account-options.command';

/**
 * Baza Account Service
 * The service is mostly used for Account Controller
 */
@Injectable()
export class BazaAccountService<T extends AccountSettingsDto = any> {
    constructor(
        @Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig,
        private readonly commandBus: CommandBus,
        private readonly moduleConfigService: BazaAccountModuleService,
        private readonly bazaEventBus: ApiEventBus<AccountApiEvent, AccountApiEvents>,
        private readonly mail: BazaAccountMailService<T>,
        private readonly bcrypt: BcryptService,
        private readonly accountRepository: BazaAccountRepository<T>,
        private readonly registry: BazaRegistryService,
        private readonly accountConfirmEmailRequestRepository: BazaAccountConfirmEmailRequestRepository<T>,
        private readonly accountResetPasswordRequestRepository: BazaAccountResetPasswordRequestRepository<T>,
        private readonly accountChangeEmailRequestRepository: BazaAccountChangeEmailRequestRepository<T>,
        private readonly accountDeactivateRequestRepository: BazaAccountDeactivateRequestRepository<T>,
        private readonly passwordService: BazaPasswordService,
        private readonly managedUserAccounts: BazaAccountManagedUsersService,
        private readonly deactivateAccountService: BazaAccountDeactivateService<T>,
    ) {}

    /**
     * Returns true if active Account with specific email address is exists
     * @param request
     * @param options
     */
    async accountExists(
        request: AccountExistsRequest,
        options: {
            hideAdminAccounts: boolean;
        } = { hideAdminAccounts: true },
    ): Promise<AccountExistsResponse> {
        const activeAccount = await this.accountRepository.findActiveAccountWithEmail(request.email);
        const deactivatedAccounts = await this.accountRepository.findDeactivatedAccountsWithEmail(request.email);

        if (activeAccount && activeAccount.role === AccountRole.Admin && options.hideAdminAccounts) {
            return {
                hasActiveAccount: false,
                hasDeactivatedAccounts: false,
            };
        } else {
            return {
                hasActiveAccount: !!activeAccount,
                hasDeactivatedAccounts: deactivatedAccounts.length > 0,
            };
        }
    }

    /**
     * Register User Account
     * @param request
     * @param context
     * @param additionalMetadata
     */
    async registerAccount(request: RegisterAccountRequest, context: RequestContext, additionalMetadata?: any): Promise<AccountEntity<T>> {
        return this.commandBus.execute(new BazaRegisterAccountCommand(request, context, additionalMetadata));
    }

    /**
     * Returns Register Options which contains information about Registration requirements
     * The method could be called with AccountEntity to return information about Referral Code and Copy&Paste (Share) text
     * @param account
     */
    async registerAccountOptions(account?: AccountEntity): Promise<BazaAccountRegisterOptions> {
        return this.commandBus.execute(new BazaRegisterAccountOptionsCommand(account));
    }

    /**
     * Sends email confirmation to user
     * @see BazaAccountService.confirmEmail
     * @param request
     * @param context
     */
    async sendConfirmEmailLink(request: SendConfirmEmailLinkRequest, context: RequestContext): Promise<void> {
        if (this.managedUserAccounts.isManagedUser(request.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        const account = await this.accountRepository.findActiveAccountWithEmail(request.email);

        if (!account) {
            return;
        }

        if (account.email === request.email && account.isEmailConfirmed) {
            return;
        }

        const confirmRequest = await this.accountConfirmEmailRequestRepository.createRequestFor(account);

        await this.mail.sendConfirmEmailLink({
            account,
            confirmRequest,
            app: context.application,
        });

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountConfirmEmailSent,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                accountConfirmEmailRequestId: confirmRequest.id,
                token: confirmRequest.token,
            },
        });
    }

    /**
     * Confirms email with Confirm Email Token
     * @see BazaAccountService.sendConfirmEmailLink
     * @param request
     * @param context
     */
    async confirmEmail(request: ConfirmEmailRequest, context: RequestContext): Promise<AccountEntity<T>> {
        const confirmRequest = await this.accountConfirmEmailRequestRepository.findConfirmationByToken(request.token, false);

        if (!confirmRequest) {
            throw new AccountTokenNotFoundException();
        }

        if (confirmRequest.dateExpiresAt.getTime() < new Date().getTime()) {
            await this.accountConfirmEmailRequestRepository.cleanUpRequestsOf(confirmRequest.account);

            if (this.moduleConfig.confirmationEmailExpiryResendStrategy === ConfirmationEmailExpiryResendStrategy.AUTO) {
                const email = confirmRequest.account.email;

                await this.sendConfirmEmailLink(
                    {
                        email,
                    },
                    context,
                );
                throw new AccountTokenExpiredExceptionWithNewEmailSent();
            } else {
                throw new AccountTokenExpiredException();
            }
        }

        const account = confirmRequest.account;

        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        if (this.managedUserAccounts.isManagedUser(account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        account.isEmailConfirmed = true;

        await this.accountRepository.save(account);
        await this.accountConfirmEmailRequestRepository.cleanUpRequestsOf(account);

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountConfirmedEmail,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                accountConfirmEmailRequestId: confirmRequest.id,
                token: confirmRequest.token,
            },
        });

        return account;
    }

    /**
     * Changes password of User. Requires old password to continue.
     * @param account
     * @param request
     * @param context
     */
    async changePassword(account: AccountEntity<T>, request: ChangePasswordRequest, context: RequestContext): Promise<void> {
        if (request.confirmPassword && request.newPassword !== request.confirmPassword) {
            throw new AccountPasswordsDoNotMatchException();
        }

        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        if (this.managedUserAccounts.isManagedUser(account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        this.moduleConfigService.validateFeature(account, BazaAccountFeatures.ChangePassword);

        const passwordResponse = await this.passwordService.validate({
            password: request.newPassword,
        });

        if (!passwordResponse.isValid) {
            throw new AccountPasswordIsNotStrongEnoughException();
        }

        if (!(await this.bcrypt.compare(request.oldPassword, account.password))) {
            throw new AccountInvalidCredentialsException(HttpStatus.BAD_REQUEST);
        }

        account.password = await this.bcrypt.hash(request.newPassword);

        await this.accountRepository.save(account);

        await this.mail.sendPasswordChangedNotification({
            account,
            app: context.application,
        });

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountPasswordChanged,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
            },
        });
    }

    /**
     * Sends email with token to Reset Password
     * @see BazaAccountService.resetPassword
     * @param request
     * @param options
     */
    async sendResetPasswordLinkRequest(
        request: SendResetPasswordLinkRequest,
        options: {
            language?: ProjectLanguage;
            application: Application;
        },
    ): Promise<AccountResetPasswordRequestEntity<T> | undefined> {
        const account = await this.accountRepository.findActiveAccountWithEmail(request.email);

        if (!account) {
            return undefined;
        }

        if (this.managedUserAccounts.isManagedUser(account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        this.moduleConfigService.validateFeature(account, BazaAccountFeatures.ResetPassword);

        const resetPasswordRequest = await this.accountResetPasswordRequestRepository.createRequestFor(account);

        await this.mail.sendResetPasswordRequest({
            app: options.application,
            account,
            resetPasswordRequest,
        });

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountResetPasswordSent,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                accountResetPasswordRequestId: resetPasswordRequest.id,
                token: resetPasswordRequest.token,
            },
        });

        return resetPasswordRequest;
    }

    /**
     * Reset Password with Reset Password Token
     * @see BazaAccountService.sendResetPasswordLinkRequest
     * @param request
     * @param options
     */
    async resetPassword(
        request: ResetPasswordRequest,
        options: {
            language?: ProjectLanguage;
            application: Application;
        },
    ): Promise<AccountEntity<T>> {
        if (request.confirmPassword && request.newPassword !== request.confirmPassword) {
            throw new AccountPasswordsDoNotMatchException();
        }

        const resetPasswordRequest = await this.accountResetPasswordRequestRepository.findConfirmationByToken(request.token);

        if (!resetPasswordRequest) {
            throw new AccountTokenNotFoundException();
        }

        if (this.managedUserAccounts.isManagedUser(resetPasswordRequest.account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        const passwordResponse = await this.passwordService.validate({
            password: request.newPassword,
        });

        if (!passwordResponse.isValid) {
            throw new AccountPasswordIsNotStrongEnoughException();
        }

        const account = resetPasswordRequest.account;

        this.moduleConfigService.validateFeature(account, BazaAccountFeatures.ResetPassword);

        const isNotConfirmedYet = !account.isEmailConfirmed;

        account.password = await this.bcrypt.hash(request.newPassword);
        account.manualResetPasswordRequest = null;
        account.isEmailConfirmed = true;

        await this.accountRepository.save(account);
        await this.accountResetPasswordRequestRepository.cleanUpRequestsOf(account);

        if (!isNotConfirmedYet) {
            await this.mail.sendPasswordChangedNotification({
                app: options.application,
                account,
            });
        }

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountResetPasswordComplete,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                accountResetPasswordRequestId: resetPasswordRequest.id,
                token: resetPasswordRequest.token,
            },
        });

        await this.commandBus.execute(
            new BazaAuthDestroyJwtsOfAccountCommand({
                accountId: account.id,
            }),
        );

        return account;
    }

    /**
     * Sends email with token to Change Email
     * @see BazaAccountService.changeEmail
     * @param account
     * @param request
     * @param options
     */
    async sendChangeEmailLink(
        account: AccountEntity<T>,
        request: SendChangeEmailLinkRequest,
        options: {
            language?: ProjectLanguage;
            application: Application;
        },
    ): Promise<AccountChangeEmailRequestEntity<T>> {
        if (this.managedUserAccounts.isManagedUser(account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        this.moduleConfigService.validateFeature(account, BazaAccountFeatures.ChangeEmail);

        if (account.email.trim().toLowerCase() === request.newEmail.toLowerCase().trim()) {
            throw new AccountChangeEmailSameException({
                email: request.newEmail,
            });
        }

        const existingAccount = await this.accountRepository.findActiveAccountWithEmail(request.newEmail);

        if (existingAccount && existingAccount.isEmailConfirmed) {
            throw new AccountChangeEmailDuplicateException();
        }

        const changeEmailRequest = await this.accountChangeEmailRequestRepository.createRequestFor(
            account,
            request.newEmail,
            options.application,
        );

        if (!(await this.bcrypt.compare(request.password, account.password))) {
            throw new AccountInvalidPasswordException();
        }

        await this.mail.sendChangeEmailLink({
            account,
            changeEmailRequest,
            app: options.application,
        });

        await this.mail.sendChangeEmailNotification({
            account,
            changeEmailRequest,
            app: options.application,
        });

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountChangeEmailSent,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                accountChangeEmailRequestId: changeEmailRequest.id,
                token: changeEmailRequest.token,
                newEmail: changeEmailRequest.newEmail,
            },
        });

        return changeEmailRequest;
    }

    /**
     * Changes Email with Change Email Token
     * @see BazaAccountService.sendChangeEmailLink
     * @param request
     */
    async changeEmail(request: ChangeEmailRequest): Promise<AccountEntity<T>> {
        const token = await this.accountChangeEmailRequestRepository.findConfirmationByToken(request.token);

        if (!token) {
            throw new AccountTokenNotFoundException();
        }

        if (this.managedUserAccounts.isManagedUser(token.account.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        this.moduleConfigService.validateFeature(token.account, BazaAccountFeatures.ChangeEmail);

        const account = await this.accountRepository.getActiveAccountWithId(token.account.id);

        const previousEmail = account.email.trim();
        const newEmail = token.newEmail.trim();

        const existing = await this.accountRepository.findActiveAccountWithEmail(newEmail);

        if (existing) {
            if (existing.id === account.id) {
                throw new AccountChangeEmailSameException({
                    email: existing.email,
                });
            }

            if (existing.isEmailConfirmed) {
                throw new AccountChangeEmailDuplicateException();
            }

            await this.deactivateAccountService.deactivateAccount(existing);
        }

        account.email = newEmail;

        if (!account.isEmailConfirmed) {
            account.isEmailConfirmed = true;

            await this.bazaEventBus.publish({
                topic: AccountApiEvent.BazaAccountConfirmedEmail,
                payload: {
                    accountId: account.id,
                    accountEmail: account.email,
                },
            });
        }

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountEmailChanged,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                previousEmail,
            },
        });

        await this.accountRepository.save(account);
        await this.accountChangeEmailRequestRepository.cleanUpRequestsOf(account);

        return account;
    }

    /**
     * Updates Account Settings JSON for Account
     * @param account
     * @param request
     */
    async setAccountSettings(account: AccountEntity<T>, request: SetAccountSettingsRequest): Promise<AccountEntity<T>> {
        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        const previousSettings: T = JSON.parse(JSON.stringify(account.settings));
        const newSettings: T = {
            ...account.settings,
            ...request,
        };

        account.settings = newSettings;

        await this.accountRepository.save(account);

        await this.bazaEventBus.publish({
            topic: AccountApiEvent.BazaAccountSettingsUpdated,
            payload: {
                accountId: account.id,
                accountEmail: account.email,
                newSettings: newSettings as any,
                previousSettings: previousSettings as any,
            },
        });

        return account;
    }

    /**
     * Updates First Name, Last Name, Phone and Full Name of Account
     * @param account
     * @param request
     */
    async updateProfile(account: AccountEntity<T>, request: UpdateProfileRequest): Promise<AccountEntity<T>> {
        if (account.isDeactivated) {
            throw new AccountIsDeactivatedException();
        }

        account.firstName = request.firstName || account.firstName;
        account.lastName = request.lastName || account.lastName;
        account.phone = request.phone || account.phone;

        account.fullName = accountFullName(account);

        await this.accountRepository.save(account);

        return account;
    }
}
