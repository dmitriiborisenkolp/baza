import { Inject, Injectable } from '@nestjs/common';
import {
    BazaAuthDestroyJwtsOfAccountCommand,
    RequestAllAccountsResetPasswordCmsAccountRequest,
    RequestResetPasswordCmsAccountRequest,
} from '@scaliolabs/baza-core-shared';
import { BazaAccountService } from '../baza-account.service';
import { BazaAccountRepository } from '../../repositories/baza-account.repository';
import { BazaAccountResetPasswordRequestRepository } from '../../repositories/baza-account-reset-password-request.repository';
import { BAZA_ACCOUNT_CONFIG, BazaAccountConfig } from '../../baza-account.config';
import { BazaAccountMailService } from '../baza-account-mail.service';
import { AccountEntity } from '../../entities/account.entity';
import { BazaAccountManagedUsersService } from '../baza-account-managed-users.service';
import { AccountManagedUserOperationNotPermittedException } from '../../exceptions/account-managed-user-operation-not-permitted.exception';
import { ProjectService } from '../../../../../baza-common/src';
import { BazaLogger } from '../../../../../baza-logger/src';
import { CommandBus } from '@nestjs/cqrs';

/**
 * Baza Account CMS Service - Reset Password
 * The service is mostly used for CMS Controllers
 */
@Injectable()
export class BazaAccountCmsResetPasswordService {
    constructor(
        @Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig<any>,
        private readonly commandBus: CommandBus,
        private readonly service: BazaAccountService,
        private readonly repository: BazaAccountRepository,
        private readonly accountResetPasswordRequestRepository: BazaAccountResetPasswordRequestRepository<any>,
        private readonly logger: BazaLogger,
        private readonly accountMailService: BazaAccountMailService,
        private readonly project: ProjectService,
        private readonly managedUserAccounts: BazaAccountManagedUsersService,
    ) {}

    /**
     * Locks accounts and sends an email which asks user to reset his password
     * Sign In attempts will fails with specific message if user will attempt to sign in before resetting password
     * @param request
     */
    async requestResetPassword(request: RequestResetPasswordCmsAccountRequest): Promise<void> {
        if (this.managedUserAccounts.isManagedUser(request.email)) {
            throw new AccountManagedUserOperationNotPermittedException();
        }

        const account = await this.repository.getActiveAccountWithEmail(request.email);

        if (account.isDeactivated) {
            return;
        }

        account.password = '';
        account.manualResetPasswordRequest = {
            attemptToSignInMessage: request.attemptToSignInErrorMessage,
            subject: request.subject,
            message: request.message,
        };

        await this.commandBus.execute(
            new BazaAuthDestroyJwtsOfAccountCommand({
                accountId: account.id,
            }),
        );

        await this.repository.save(account);

        await this.sendManualPasswordRequestEmail(account);
    }

    /**
     * Locks ALL user accounts and sends emails which asks user to reset his password
     * Sign In attempts will fails with specific message if user will attempt to sign in before resetting password
     * @param request
     */
    async requestResetPasswordAllAccounts(request: RequestAllAccountsResetPasswordCmsAccountRequest): Promise<void> {
        const accounts = await this.repository.findUserAccounts();

        const perform = async () => {
            for (const account of accounts) {
                if (this.managedUserAccounts.isManagedUser(account.email)) {
                    continue;
                }

                await this.requestResetPassword({
                    ...request,
                    email: account.email,
                });
            }
        };

        if (request.synchronous) {
            await perform();
        } else {
            perform().catch((err) => console.error(err));
        }
    }

    /**
     * Sends email notification to reset password for user
     * @param account
     */
    async sendManualPasswordRequestEmail(account: AccountEntity): Promise<void> {
        const resetPasswordRequest = await this.accountResetPasswordRequestRepository.createRequestFor(account);

        await this.accountMailService.sendManualResetPasswordRequest({
            account,
            resetPasswordRequest,
        });
    }
}
