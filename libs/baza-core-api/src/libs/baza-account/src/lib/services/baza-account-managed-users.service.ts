import { Injectable } from '@nestjs/common';
import { AccountManagedUserNotFoundException } from '../exceptions/account-managed-user-not-found.exception';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { EnvService } from '../../../../baza-env/src';
import { BazaCommonEnvironments } from '../../../../baza-common/src';

/**
 * Managed Users configuration
 * Configuration is parsed from BAZA_MANAGED_USERS environment variable
 * @see BazaAccountManagedUsersService
 */
export interface BazaAccountManagedUserConfig {
    accounts: Array<BazaAccountManagedUser>;
}

/**
 * Managed Users configuration - Account (User entry)
 * Configuration is parsed from BAZA_MANAGED_USERS environment variable
 * @see BazaAccountManagedUsersService
 */
export interface BazaAccountManagedUser {
    email: string;
    password: string;
    role: AccountRole;
}

/**
 * Managed Users
 * Manages Users are users managed with BAZA_MANAGED_USERS environment variable. The feature automatically bootstraps
 * accounts configured with BAZA_MANAGED_USERS environment variable
 *
 * BAZA_MANAGED_USERS should have following format: `[user-config-1],[user-config-2],...`, where `[user-config-N]` should
 * follows `${email} ${role} ${password}` format
 * If BAZA_MANAGED_USERS is not set, Baza will created scalio-admin@scal.io and scalio-user@scal.io accounts with `Scalio#1337!` passwords
 * You should not store BAZA_MANAGED_USERS in repository for Test/Stage/Production environments; keep it in secure place
 *
 * @example BAZA_MANAGED_USERS=admin scalio-admin@scal.io Scalio#1337!, user scalio-user@scal.io Scalio#1337!
 *
 * @see BAZA_DEFAULT_ACCOUNTS
 * @see BAZA_DEFAULT_ACCOUNTS_PASSWORD
 */
@Injectable()
export class BazaAccountManagedUsersService {
    constructor(private readonly env: EnvService<BazaCommonEnvironments.AuthManagedUserEnvironment>) {}

    /**
     * Returns configuration parsed from BAZA_MANAGED_USERS environment
     * If BAZA_MANAGED_USERS is not set, the method will returns empty accounts list
     */
    get config(): BazaAccountManagedUserConfig {
        const accounts = (
            this.env.getAsString('BAZA_MANAGED_USERS', {
                defaultValue: '',
            }) || ''
        ).trim();

        if (accounts) {
            return {
                accounts: accounts.split(',').map((pair) => ({
                    role: pair.trim().split(' ')[0].trim().toLowerCase() as any,
                    email: pair.trim().split(' ')[1].trim(),
                    password: pair.trim().split(' ')[2].trim(),
                })),
            };
        } else {
            return {
                accounts: [],
            };
        }
    }

    /**
     * Returns true if there should be managed user with specific email
     * @param email
     */
    isManagedUser(email: string): boolean {
        return !!this.config.accounts.find((a) => a.email === email);
    }

    /**
     * Returns configuration for Managed User by email
     * @param email
     */
    getManagedUserConfig(email: string): BazaAccountManagedUser {
        const found = this.config.accounts.find((a) => a.email === email);

        if (!found) {
            throw new AccountManagedUserNotFoundException();
        }

        return found;
    }
}
