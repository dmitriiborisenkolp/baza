import { DynamicModule, Global, Module } from '@nestjs/common';
import { BazaAccountService } from './services/baza-account.service';
import { BazaAccountMapper } from './mappers/baza-account.mapper';
import { BazaAccountCmsService } from './services/cms/baza-account-cms.service';
import { BAZA_ACCOUNT_CONFIG, defaultBazaAccountConfig } from './baza-account.config';
import type { BazaAccountConfig } from './baza-account.config';
import { BazaAccountRepository } from './repositories/baza-account.repository';
import { BazaAccountConfirmEmailRequestRepository } from './repositories/baza-account-confirm-email-request.repository';
import { BazaAccountResetPasswordRequestRepository } from './repositories/baza-account-reset-password-request.repository';
import { BazaAccountMailService } from './services/baza-account-mail.service';
import { BazaAccountCsvMapper } from './mappers/baza-account-csv.mapper';
import { BazaAccountCmsResetPasswordService } from './services/cms/baza-account-cms-reset-password.service';
import { BazaAccountChangeEmailRequestRepository } from './repositories/baza-account-change-email-request.repository';
import { BazaAccountModuleService } from './baza-account.module-service';
import { BazaAccountManagedUsersService } from './services/baza-account-managed-users.service';
import { BazaPasswordApiModule } from '../../../baza-password/src';
import { BazaCrudApiModule } from '../../../baza-crud/src';
import { BazaAccountDeactivateRequestRepository } from './repositories/baza-account-deactivate-request.repository';
import { BazaAccountDeactivateService } from './services/baza-account-deactivate.service';
import { CqrsModule } from '@nestjs/cqrs';
import { DestroyResetPasswordTokensOnSignInEventHandler } from './event-handlers/destroy-reset-password-tokens-on-sign-in.event-handler';
import { BazaAccountCmsMapper } from './mappers/baza-account-cms.mapper';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaAccountConfig>;
}

const services = [
    BazaAccountRepository,
    BazaAccountConfirmEmailRequestRepository,
    BazaAccountResetPasswordRequestRepository,
    BazaAccountChangeEmailRequestRepository,
    BazaAccountDeactivateRequestRepository,
    BazaAccountService,
    BazaAccountCmsService,
    BazaAccountMapper,
    BazaAccountCmsMapper,
    BazaAccountMailService,
    BazaAccountCsvMapper,
    BazaAccountCmsResetPasswordService,
    BazaAccountModuleService,
    BazaAccountManagedUsersService,
    BazaAccountDeactivateService,
];

const eventHandlers = [DestroyResetPasswordTokensOnSignInEventHandler];

export { AsyncOptions as BazaAccountModuleAsyncOptions };

@Module({})
export class BazaAccountApiModule {
    static forRootAsync(asyncOptions: AsyncOptions): DynamicModule {
        return {
            module: BazaAccountGlobalModule,
            providers: [
                {
                    provide: BAZA_ACCOUNT_CONFIG,
                    inject: asyncOptions.injects,
                    useFactory: async (...injects) => ({
                        ...defaultBazaAccountConfig(),
                        ...(await asyncOptions.useFactory(...injects)),
                    }),
                },
            ],
            exports: [BAZA_ACCOUNT_CONFIG],
        };
    }
}

@Global()
@Module({
    imports: [CqrsModule, BazaCrudApiModule, BazaPasswordApiModule],
    providers: [...services, ...eventHandlers],
    exports: [...services],
})
class BazaAccountGlobalModule {}
