import { AccountConfirmEmailRequestEntity } from './entities/account-confirm-email-request.entity';
import { AccountResetPasswordRequestEntity } from './entities/account-reset-password-request.entity';
import { AccountEntity } from './entities/account.entity';
import { AccountRole, AccountSettingsDto, ConfirmationEmailExpiryResendStrategy } from '@scaliolabs/baza-core-shared';
import { Application } from '@scaliolabs/baza-core-shared';
import { AccountChangeEmailRequestEntity } from './entities/account-change-email-request.entity';
import { AccountDeactivateRequestEntity } from './entities/account-deactivate-request.entity';

/**
 * Injection Token for Baza Account API Module configuration
 */
export const BAZA_ACCOUNT_CONFIG = Symbol();

/**
 * List of features for Baza Account
 */
export enum BazaAccountFeatures {
    /**
     * If enabled, API will allow to change email with sendChangeEmailRequest/changeEmail methods
     */
    ChangeEmail = 'BazaAccountFeaturesChangeEmail',

    /**
     * If enabled, API will allow to change password with changePassword method
     */
    ChangePassword = 'BazaAccountFeaturesChangePassword',

    /**
     * If enabled, API will allow to reset password with sendResetPasswordLink/resetPassword methods
     */
    ResetPassword = 'BazaAccountFeaturesResetPassword',

    /**
     * If enabled, API will allow to reset password with sendResetPasswordLink/resetPassword methods
     */
    Deactivate = 'BazaAccountFeaturesDeactivate',

    /**
     * If enabled, API will allow to check account is account exists or not
     */
    Exists = 'BazaAccountFeaturesExists',
}

/**
 * Account Mail Configuration
 */
interface AccountMailConfig<T, ACCOUNT_T> {
    mailVariablesGenerator: (application: Application, request: T) => any;
    mailTemplatePaths: {
        asText: (application: Application, account: AccountEntity<ACCOUNT_T>) => string;
        asHtml: (application: Application, account: AccountEntity<ACCOUNT_T>) => string;
    };
}

/**
 * Account Mail Configuration (with Link Generator)
 */
interface AccountMailWithLinkGeneratorConfig<T, ACCOUNT_T> {
    urlGenerator: (application: Application, request: T) => string;
    mailVariablesGenerator: (application: Application, request: T) => any;
    mailTemplatePaths: {
        asText: (application: Application, account: AccountEntity<ACCOUNT_T>) => string;
        asHtml: (application: Application, account: AccountEntity<ACCOUNT_T>) => string;
    };
}

export type BazaAccountConfigFeatures = Array<{
    role: AccountRole;
    enabled: Array<BazaAccountFeatures>;
}>;

export interface BazaAccountConfig<T extends AccountSettingsDto = any> {
    /**
     * Features configuration depends on input account role
     */
    features: BazaAccountConfigFeatures;

    /**
     * Expiration time for tokens (in seconds)
     */
    ttlTokensSeconds: {
        changeEmail: number;
        confirmEmail: number;
        resetPassword: number;
        deactivateAccount: number;
    };

    /**
     * Mail configuration
     * Mail sent to confirm account email after sign-up
     */
    sendConfirmEmailLink: AccountMailWithLinkGeneratorConfig<AccountConfirmEmailRequestEntity<T>, T>;

    /**
     * Mail configuration
     * Mail sent to reset account password
     */
    resetPasswordRequest: AccountMailWithLinkGeneratorConfig<AccountResetPasswordRequestEntity<T>, T>;

    /**
     * Mail configuration
     * Manual request to change password. Will force account (user) to change password.
     */
    cmsRequestResetPassword: AccountMailWithLinkGeneratorConfig<AccountResetPasswordRequestEntity<T>, T>;

    /**
     * Mail configuration
     * Notification that password has been changed
     */
    passwordChangedNotification: AccountMailConfig<AccountEntity<T>, T>;

    /**
     * Mail configuration
     * Notification that account is already registered if user is trying to sign up with existing email
     */
    alreadyRegistered: AccountMailConfig<AccountEntity<T>, T>;

    /**
     * Mail configuration
     * Mail sent to change account email
     */
    changeEmailRequest: AccountMailWithLinkGeneratorConfig<AccountChangeEmailRequestEntity<T>, T>;

    /**
     * Mail configuration
     * Mail notification sent to OLD (previous) email that email change was requested by user or someone else
     */
    changeEmailRequestNotification: AccountMailConfig<AccountEntity<T>, T>;

    /**
     * Mail configuration
     * Mail sent to deactivate account
     */
    deactivateAccountLink: AccountMailWithLinkGeneratorConfig<AccountDeactivateRequestEntity<T>, T>;

    /**
     * Mail configuration
     * Notification that account has been deactivated
     */
    accountDeactivated: AccountMailConfig<AccountEntity<T>, T>;

    /**
     * Custom tokenizer for verification emails
     */
    customEmailVerificationTokenizer?: (account: AccountEntity<any>) => string;

    /*
     * Email Resend strategy for expired confirmation emails
     */
    confirmationEmailExpiryResendStrategy: ConfirmationEmailExpiryResendStrategy;
}

export const defaultBazaAccountConfig: () => Partial<BazaAccountConfig> = () => ({});
