import 'reflect-metadata';
import { BazaAccountCmsNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AccountErrorCodes, AuthErrorCodes, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { asyncExpect } from '../../../../../baza-test-utils/src';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/13-baza-account-cms-change-email.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let USER_ACCOUNT_ID: number;
    let USER_ACCOUNT_EMAIL: string;
    let USER_ACCOUNT_JWT: string;
    let ADMIN_ACCOUNT_EMAIL: string;

    const NEW_USER_ACCOUNT_EMAIL = 'user-changed-email@scal.io';

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eUser();

        USER_ACCOUNT_ID = http.authResponse.jwtPayload.accountId;
        USER_ACCOUNT_EMAIL = http.authResponse.jwtPayload.accountEmail;
        USER_ACCOUNT_JWT = http.authResponse.accessToken;

        await http.authE2eAdmin();

        ADMIN_ACCOUNT_EMAIL = http.authResponse.jwtPayload.accountEmail;
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will not allow to change email if there is already an account with same new email', async () => {
        const response: BazaError = (await dataAccessAccountCms.changeEmail({
            id: USER_ACCOUNT_ID,
            email: ADMIN_ACCOUNT_EMAIL,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountChangeEmailDuplicate);
    });

    it('will allow to verify current JWT', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessAuth.auth({
                    email: USER_ACCOUNT_EMAIL,
                    password: 'e2e-user-password',
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will not allow to change email with same new email', async () => {
        const response: BazaError = (await dataAccessAccountCms.changeEmail({
            id: USER_ACCOUNT_ID,
            email: USER_ACCOUNT_EMAIL,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountChangeEmailSame);
    });

    it('will allow to verify current JWT', async () => {
        await asyncExpect(async () => {
            const response = await dataAccessAuth.auth({
                email: USER_ACCOUNT_EMAIL,
                password: 'e2e-user-password',
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
        });
    });

    it('will successfully change email', async () => {
        const response = await dataAccessAccountCms.changeEmail({
            id: USER_ACCOUNT_ID,
            email: NEW_USER_ACCOUNT_EMAIL,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(USER_ACCOUNT_ID);
        expect(response.email).toBe(NEW_USER_ACCOUNT_EMAIL);
    });

    it('will sign out target account after changing email', async () => {
        await asyncExpect(async () => {
            const response: BazaError = (await dataAccessAuth.verify({
                jwt: USER_ACCOUNT_JWT,
            })) as any;

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect(response.code).toBe(AuthErrorCodes.AuthInvalidJwt);
        });
    });

    it('will not allow to verify current JWT token', async () => {
        const response: BazaError = (await dataAccessAuth.auth({
            email: USER_ACCOUNT_EMAIL,
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will allow to sign in with new email', async () => {
        const response = await dataAccessAuth.auth({
            email: NEW_USER_ACCOUNT_EMAIL,
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.jwtPayload.accountId).toBe(USER_ACCOUNT_ID);
        expect(response.jwtPayload.accountEmail).toBe(NEW_USER_ACCOUNT_EMAIL);
    });
});
