import 'reflect-metadata';
import { BazaAccountBootstrapNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsNodeAccess, BazaAccountCmsResetPasswordNodeAccess, BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes } from '@scaliolabs/baza-core-shared';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/08-baza-account-managed-user-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAccountCMS = new BazaAccountCmsNodeAccess(http);
    const dataAccessAccountCMSResetPassword = new BazaAccountCmsResetPasswordNodeAccess(http);
    const dataAccessAccountBootstrap = new BazaAccountBootstrapNodeAccess(http);

    let E2E_ADMIN_ACCOUNT_ID: number;
    let E2E_USER_ACCOUNT_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        const bootstrap = await dataAccessAccountBootstrap.bootstrap();

        expect(isBazaErrorResponse(bootstrap)).toBeFalsy();

        await http.authE2eAdmin();

        const response = await dataAccessAccountCMS.listAccounts({});

        E2E_ADMIN_ACCOUNT_ID = response.items.find((a) => a.email === 'e2e-managed-admin@scal.io').id;
        E2E_USER_ACCOUNT_ID = response.items.find((a) => a.email === 'e2e-managed-user@scal.io').id;
    });

    it('will not allow to revoke admin access from managedUser admin account', async () => {
        const response: BazaError = (await dataAccessAccountCMS.revokeAdminAccess({
            email: 'e2e-managed-admin@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will not allow to assign admin access from managedUser user account', async () => {
        const response: BazaError = (await dataAccessAccountCMS.assignAdminAccess({
            email: 'e2e-managed-user@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will allow to update first and last name of managedUser account', async () => {
        const response = await dataAccessAccountCMS.updateAccount({
            id: E2E_ADMIN_ACCOUNT_ID,
            firstName: 'Foo',
            lastName: 'Bar',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will now allow to update password of managedUser account', async () => {
        const response: BazaError = (await dataAccessAccountCMS.updateAccount({
            id: E2E_ADMIN_ACCOUNT_ID,
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'ExampleScalioPassword#1337!',
            passwordConfirm: 'ExampleScalioPassword#1337!',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will not allow to update password with non matching passwordConfirm', async () => {
        const response: BazaError = (await dataAccessAccountCMS.updateAccount({
            id: E2E_ADMIN_ACCOUNT_ID,
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'ExampleScalioPassword#1337!',
            passwordConfirm: 'non-matching',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordsDoNotMatch);
    });

    it('will not allow to mark managedUser account as confirmed', async () => {
        const response: BazaError = (await dataAccessAccountCMS.confirmEmailAccountById({
            id: E2E_ADMIN_ACCOUNT_ID,
            confirmed: true,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will not allow to mark managedUser account as not confirmed', async () => {
        const response: BazaError = (await dataAccessAccountCMS.confirmEmailAccountById({
            id: E2E_ADMIN_ACCOUNT_ID,
            confirmed: false,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will not allow to send manual reset password request to managedUser account', async () => {
        const response: BazaError = (await dataAccessAccountCMSResetPassword.requestResetPassword({
            email: 'e2e-managed-admin@scal.io',
            message: 'E2E Reset Password Message',
            subject: 'E2E Reset Password Subject',
            attemptToSignInErrorMessage: 'E2E Please Reset Password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will not break request reset password for all accounts because of managedUser accounts', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(0);

        const response = await dataAccessAccountCMSResetPassword.requestResetPasswordAllAccounts({
            message: 'E2E Reset Password Message',
            subject: 'E2E Reset Password Subject',
            attemptToSignInErrorMessage: 'E2E Please Reset Password',
            synchronous: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const mailbox2 = await dataAccessE2e.mailbox();

        expect(mailbox2.length).toBe(2);
    });
});
