import 'reflect-metadata';
import { BazaAccountNodeAccess, BazaAuthNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AccountErrorCodes, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { asyncExpect } from '../../../../../baza-test-utils/src';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/api-controllers/integration-tests/12-baza-account-clean-up-reset-password-token-on-sign-in.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will send reset password link', async () => {
        const resetPasswordResponse = await dataAccessAccount.sendResetPasswordLink({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(resetPasswordResponse)).toBeFalsy();
    });

    it('will successfully sign in with current password', async () => {
        const authResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();
    });

    it('will not allow to reset password anymore', async () => {
        const token = await asyncExpect(
            async () => {
                const mailbox = await dataAccessE2e.mailbox();
                const token = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

                expect(mailbox.length).toBe(1);
                expect(token).not.toBeUndefined();

                return token;
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const response: BazaError = (await dataAccessAccount.resetPassword({
                    token,
                    newPassword: 'Scalio#1337!!!',
                })) as any;

                expect(isBazaErrorResponse(response)).toBeTruthy();
                expect(response.code).toBe(AccountErrorCodes.BazaAccountTokenNotFound);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully sign in with current password', async () => {
        const authResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();
    });

    it('will not sign in with new password', async () => {
        const authResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'Scalio#1337!!!',
        });

        expect(isBazaErrorResponse(authResponse)).toBeTruthy();
    });
});
