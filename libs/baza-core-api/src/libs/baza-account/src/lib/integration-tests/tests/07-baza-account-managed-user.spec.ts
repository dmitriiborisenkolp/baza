import 'reflect-metadata';
import { BazaAccountBootstrapNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes, AccountRole } from '@scaliolabs/baza-core-shared';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/07-baza-account-managedUser.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAccountBootstrap = new BazaAccountBootstrapNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will not allow to register managed admin account', async () => {
        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'e2e-managed-admin@scal.io',
            password: 'SomeRandomPassword#1337!',
            firstName: 'Managed',
            lastName: 'Admin',
        })) as BazaError;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will not allow to register managed user account', async () => {
        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'e2e-managed-user@scal.io',
            password: 'SomeRandomPassword#1337!',
            firstName: 'ManagedUser',
            lastName: 'User',
        })) as BazaError;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will bootstrap accounts', async () => {
        const bootstrap = await dataAccessAccountBootstrap.bootstrap();

        expect(isBazaErrorResponse(bootstrap)).toBeFalsy();
    });

    it('will not send any emails to managed user account', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(0);
    });

    it('will allow to sign in without email confirmation', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e-managed-admin@scal.io',
            password: 'ScalioManagedUser1337!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountRole).toBe(AccountRole.Admin);
        expect(response.jwtPayload.isManagedUserAccount).toBeTruthy();
    });

    it('will not allow to sign in with custom password', async () => {
        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-managed-admin@scal.io',
            password: 'SomeRandomPassword#1337!',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will not allow to reset password', async () => {
        const response: BazaError = (await dataAccessAccount.sendResetPasswordLink({
            email: 'e2e-managed-admin@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will not allow to change password', async () => {
        await http.auth({
            email: 'e2e-managed-admin@scal.io',
            password: 'ScalioManagedUser1337!',
        });

        const response: BazaError = (await dataAccessAccount.changePassword({
            newPassword: 'ScalioManagedUserUpdated1337!',
            oldPassword: 'ScalioManagedUser1337!',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });

    it('will not allow to send change email request', async () => {
        const response: BazaError = (await dataAccessAccount.sendChangeEmailLink({
            password: 'ScalioManagedUser#1337!',
            newEmail: 'e2e-change-managedUser-email@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted);
    });
});
