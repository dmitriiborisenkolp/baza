import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaError, generateRandomHexString, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/06-baza-account-change-email.spec', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will not allow to send change email request for admin account', async () => {
        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessAccount.sendChangeEmailLink({
            newEmail: 'e2e-changed@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountModuleFeatureIsNotEnabled);
    });

    it('will fail with invalid password', async () => {
        await http.authE2eUser();

        const response: BazaError = (await dataAccessAccount.sendChangeEmailLink({
            newEmail: 'e2e-changed@scal.io',
            password: 'e2e-invalid-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountInvalidPassword);
    });

    it('will send change email request', async () => {
        const response = await dataAccessAccount.sendChangeEmailLink({
            newEmail: 'e2e-changed@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will have 2 emails in inbox', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(2);

        expect(mailbox[1].to).toEqual(['e2e-changed@scal.io']);
        expect(mailbox[0].to).toEqual(['e2e@scal.io']);

        const match = mailbox[1].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/);

        expect(match).not.toBeNull();

        const token = match[1];

        expect(token).not.toBeUndefined();
    });

    it('will fail to change email with invalid email', async () => {
        http.noAuth();

        const response: BazaError = (await dataAccessAccount.changeEmail({
            token: generateRandomHexString(),
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountTokenNotFound);
    });

    it('will successfully update email', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        const match = mailbox[1].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/);

        expect(match).not.toBeNull();

        const token = match[1];

        expect(token).not.toBeUndefined();

        const response = await dataAccessAccount.changeEmail({
            token,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountEmail).toBe('e2e-changed@scal.io');
    });

    it('will not allow to reuse same token', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        const match = mailbox[1].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/);

        expect(match).not.toBeNull();

        const token = match[1];

        expect(token).not.toBeUndefined();

        const response: BazaError = (await dataAccessAccount.changeEmail({
            token,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountTokenNotFound);
    });

    it('will not allow to sign in with old email', async () => {
        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will allow to sign in with new email', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e-changed@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountEmail).toBe('e2e-changed@scal.io');
    });
});
