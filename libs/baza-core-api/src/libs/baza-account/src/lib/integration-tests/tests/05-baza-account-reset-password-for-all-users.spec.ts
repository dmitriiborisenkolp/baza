// Send email to users for updating passwords
// https://scalio.atlassian.net/browse/CMNW-1485

import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsResetPasswordNodeAccess, BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

// Register Account Flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/04-baza-account-reset-passwords.spec.ts', () => {
    let ACCESS_TOKEN_E2E_1: string;
    let ACCESS_TOKEN_E2E_2: string;

    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessResetPassword = new BazaAccountCmsResetPasswordNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eUser();

        ACCESS_TOKEN_E2E_1 = http.authResponse.accessToken;

        await http.authE2eUser2();

        ACCESS_TOKEN_E2E_2 = http.authResponse.accessToken;
    });

    it('will successfully request for password reset for all users', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessResetPassword.requestResetPasswordAllAccounts({
            subject: 'E2E Subject',
            message: 'E2E Message',
            synchronous: true,
            attemptToSignInErrorMessage: 'E2E Request to reset password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will invalidate existing token for E2E user 1 and will invalidate existing token for E2E user 2', async () => {
        const e2eVerify1 = await dataAccessAuth.verify({
            jwt: ACCESS_TOKEN_E2E_1,
        });

        expect(isBazaErrorResponse(e2eVerify1)).toBeTruthy();

        const e2eVerify2 = await dataAccessAuth.verify({
            jwt: ACCESS_TOKEN_E2E_2,
        });

        expect(isBazaErrorResponse(e2eVerify2)).toBeTruthy();
    });

    it('will have 2 messages in inbox', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(2);
        expect(mailbox[0].subject).toBe('E2E Subject');
        expect(mailbox[0].messageBodyText.includes('E2E Message'));

        expect(mailbox.length).toBe(2);
        expect(mailbox[1].subject).toBe('E2E Subject');
        expect(mailbox[1].messageBodyText.includes('E2E Message'));
    });

    it('will successfully updates password for E2E user 1', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        const token = mailbox[1].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        expect(token).not.toBeUndefined();

        const resetPassword = await dataAccessAccount.resetPassword({
            newPassword: 'NewScalio#1337!',
            token,
        });

        expect(isBazaErrorResponse(resetPassword)).toBeFalsy();
    });

    it('will successfully sign in E2E user 1 with new password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'NewScalio#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not sign in E2E user 2 with old password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will successfully updates password for E2E user 2', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        const token = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        expect(token).not.toBeUndefined();

        const resetPassword = await dataAccessAccount.resetPassword({
            newPassword: 'NewScalio#1337!-2',
            token,
        });

        expect(isBazaErrorResponse(resetPassword)).toBeFalsy();
    });

    it('will successfully sign in E2E user 2 with new password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'NewScalio#1337!-2',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
