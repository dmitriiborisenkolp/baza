import 'reflect-metadata';
import { BazaAccountCmsNodeAccess, BazaAccountNodeAccess, BazaAuthNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse, RegisterAccountRequest } from '@scaliolabs/baza-core-shared';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/api-controllers/integration-tests/14-baza-account-allow-use-custom-metadata.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    const PASSWORD = 'Scalio#1337!';

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will allow to register user account with custom metadata', async () => {
        const response = await dataAccessAccountCms.registerUserAccount({
            email: 'user-metadata@scal.io',
            password: PASSWORD,
            passwordConfirm: PASSWORD,
            metadata: {
                foo: 'bar',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.metadata.foo).toBe('bar');
    });

    it('will allow to register admin account with custom metadata', async () => {
        const response = await dataAccessAccountCms.registerAdminAccount({
            email: 'admin-metadata@scal.io',
            password: PASSWORD,
            passwordConfirm: PASSWORD,
            metadata: {
                foo: 'bar',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.metadata.foo).toBe('bar');
    });

    it('will display metadata fields in CMS List response', async () => {
        const response = await dataAccessAccountCms.listAccounts({});

        const userAccount = response.items.find((account) => account.email === 'user-metadata@scal.io');
        const adminAccount = response.items.find((account) => account.email === 'admin-metadata@scal.io');

        expect(userAccount).toBeDefined();
        expect(adminAccount).toBeDefined();

        expect(userAccount.metadata).toBeDefined();
        expect(adminAccount.metadata).toBeDefined();

        expect(userAccount.metadata.foo).toBe('bar');
        expect(adminAccount.metadata.foo).toBe('bar');
    });

    it('will allow to update metadata', async () => {
        const listResponse = await dataAccessAccountCms.listAccounts({});

        const userAccount = listResponse.items.find((account) => account.email === 'user-metadata@scal.io');

        expect(userAccount.metadata).toEqual({
            foo: 'bar',
        });

        const updateResponse = await dataAccessAccountCms.updateAccount({
            id: userAccount.id,
            metadata: {
                foo: 'bar',
                bar: 'baz',
            },
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();

        expect(updateResponse.metadata).toEqual({
            foo: 'bar',
            bar: 'baz',
        });
    });

    it('will display updated metadata in CMS List response', async () => {
        const listResponse = await dataAccessAccountCms.listAccounts({});

        const userAccount = listResponse.items.find((account) => account.email === 'user-metadata@scal.io');

        expect(userAccount.metadata).toEqual({
            foo: 'bar',
            bar: 'baz',
        });
    });

    /*
     * Account Metadata may contains information which should not be published
     * for security or any other reasons. If you need data from Account's Metadata,
     * you should implement additional endpoints which will response required
     * metadata fields to account.
     */

    it('will not display metadata in auth response', async () => {
        await http.noAuth();

        const authResponse = await dataAccessAuth.auth({
            email: 'user-metadata@scal.io',
            password: PASSWORD,
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();

        expect(authResponse['metadata']).not.toBeDefined();
    });

    it('will not allow to setup metadata with sign up request', async () => {
        await http.noAuth();

        const signUpRequest: RegisterAccountRequest = {
            email: 'user-metadata-2@scal.io',
            password: PASSWORD,
            firstName: 'User',
            lastName: 'Metadata',
        };

        Object.assign(signUpRequest, {
            metadata: {
                foo: 'bar',
            },
        });

        const signUpResponse = await dataAccessAccount.registerAccount(signUpRequest);

        expect(isBazaErrorResponse(signUpResponse)).toBeFalsy();

        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.listAccounts({});

        const userAccount = response.items.find((account) => account.email === 'user-metadata-2@scal.io');

        expect(userAccount).toBeDefined();
        expect(userAccount.metadata).toBeDefined();
        expect(userAccount.metadata.foo).not.toBeDefined();
    });

    it('will not displays metadata object in /current response', async () => {
        await http.auth({
            email: 'user-metadata@scal.io',
            password: PASSWORD,
        });

        const currentResponse = await dataAccessAccount.currentAccount();

        expect(isBazaErrorResponse(currentResponse)).toBeFalsy();

        expect((currentResponse as any).metadata).not.toBeDefined();
    });
});
