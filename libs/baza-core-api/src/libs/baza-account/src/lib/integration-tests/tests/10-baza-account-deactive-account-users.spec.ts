import 'reflect-metadata';
import { BazaAccountNodeAccess, BazaAuthNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    AccountErrorCodes,
    AuthErrorCodes,
    BazaCoreE2eFixtures,
    BazaError,
    generateRandomHexString,
    isBazaErrorResponse,
} from '@scaliolabs/baza-core-shared';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/api-controllers/integration-tests/10-baza-account-deactive-account-users.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    let USER_ACCOUNT_ID: number;
    let USER_ACCOUNT_JWT: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eUser();

        USER_ACCOUNT_ID = http.authResponse.jwtPayload.accountId;
        USER_ACCOUNT_JWT = http.authResponse.accessToken;
    });

    it('will allow to send deactivate account request', async () => {
        await http.authE2eUser();

        const response = await dataAccessAccount.sendDeactivateAccountLink({
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);
    });

    it('will fail to send deactivate account request with invalid password', async () => {
        await http.authE2eUser();

        const response: BazaError = (await dataAccessAccount.sendDeactivateAccountLink({
            password: 'e2e-user-password-invalid',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountInvalidPassword);

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);
    });

    it('will not allow to deactivate account with random token', async () => {
        const response: BazaError = (await dataAccessAccount.deactivateAccount({
            token: generateRandomHexString(),
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountTokenNotFound);

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);
    });

    it('will allow to deactivate account with token from mail', async () => {
        http.noAuth();

        const mailbox = await dataAccessE2e.mailbox();

        const token = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        expect(token).not.toBeUndefined();

        const response = await dataAccessAccount.deactivateAccount({
            token,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const mailbox2 = await dataAccessE2e.mailbox();

        expect(mailbox2.length).toBe(2);
    });

    it('will not allow to use previous JWT of deactivated account', async () => {
        const response: BazaError = (await dataAccessAuth.verify({
            jwt: USER_ACCOUNT_JWT,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will not allow to sign in anymore', async () => {
        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will not allow to send reset password or confirm email', async () => {
        await dataAccessAccount.sendConfirmEmailLink({ email: 'e2e@scal.io' });
        await dataAccessAccount.sendResetPasswordLink({ email: 'e2e@scal.io' });

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(2);
    });

    it('will allow to register new account with same email', async () => {
        const response = await dataAccessAccount.registerAccount({
            email: 'e2e@scal.io',
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to confirm new account', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(3);

        const token = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        const response = await dataAccessAccount.confirmEmail({
            token,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountId).not.toBe(USER_ACCOUNT_ID);
    });

    it('will allow to sign in with new account', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.jwtPayload.accountId).not.toBe(USER_ACCOUNT_ID);
    });
});
