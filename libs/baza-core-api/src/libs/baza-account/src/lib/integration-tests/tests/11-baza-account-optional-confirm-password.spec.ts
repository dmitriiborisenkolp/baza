import 'reflect-metadata';
import { BazaAccountNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AccountErrorCodes, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/api-controllers/integration-tests/11-baza-account-optional-confirm-password.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eUser();
    });

    it('will works for registerAccount endpoint', async () => {
        const responseFail: BazaError = (await dataAccessAccount.registerAccount({
            email: 'foo@scal.io',
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'Scalio#1337!',
            confirmPassword: 'Scalio#1337!!!',
        })) as any;

        expect(isBazaErrorResponse(responseFail)).toBeTruthy();
        expect(responseFail.code).toBe(AccountErrorCodes.BazaAccountPasswordsDoNotMatch);

        const responseSuccess: BazaError = (await dataAccessAccount.registerAccount({
            email: 'foo@scal.io',
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'Scalio#1337!',
            confirmPassword: 'Scalio#1337!',
        })) as any;

        expect(isBazaErrorResponse(responseSuccess)).toBeFalsy();
    });

    it('will works for changePassword endpoint', async () => {
        await http.authE2eUser();

        const responseFail: BazaError = (await dataAccessAccount.changePassword({
            oldPassword: 'e2e-user-password',
            newPassword: 'NScalio#1337!',
            confirmPassword: 'NScalio#1337!!!',
        })) as any;

        expect(isBazaErrorResponse(responseFail)).toBeTruthy();
        expect(responseFail.code).toBe(AccountErrorCodes.BazaAccountPasswordsDoNotMatch);

        const responseSuccess: BazaError = (await dataAccessAccount.changePassword({
            oldPassword: 'e2e-user-password',
            newPassword: 'NScalio#1337!',
            confirmPassword: 'NScalio#1337!',
        })) as any;

        expect(isBazaErrorResponse(responseSuccess)).toBeFalsy();
    });

    it('will works for resetPassword endpoint', async () => {
        await dataAccessE2e.flushMailbox();

        await dataAccessAccount.sendResetPasswordLink({
            email: 'e2e@scal.io',
        });

        const mailbox = await dataAccessE2e.mailbox();

        const token = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        expect(token).not.toBeUndefined();

        const responseFail: BazaError = (await dataAccessAccount.resetPassword({
            token,
            newPassword: 'Scalio#1337!',
            confirmPassword: 'Scalio#1337!!!',
        })) as any;

        expect(isBazaErrorResponse(responseFail)).toBeTruthy();
        expect(responseFail.code).toBe(AccountErrorCodes.BazaAccountPasswordsDoNotMatch);

        const responseSuccess: BazaError = (await dataAccessAccount.resetPassword({
            token,
            newPassword: 'Scalio#1337!',
            confirmPassword: 'Scalio#1337!',
        })) as any;

        expect(isBazaErrorResponse(responseSuccess)).toBeFalsy();
    });
});
