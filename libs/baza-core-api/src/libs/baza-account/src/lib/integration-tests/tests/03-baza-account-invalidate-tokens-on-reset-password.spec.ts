// https://scalio.atlassian.net/browse/CMNW-1556

import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { asyncExpect } from '../../../../../baza-test-utils/src';

// Register Account Flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/03-baza-account-invalidate-tokens-on-reset-password.spec.ts', () => {
    let ACCESS_TOKEN: string;

    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eUser();

        ACCESS_TOKEN = http.authResponse.accessToken;
    });

    it('will successfully verify token', async () => {
        const response = await dataAccessAuth.verify({
            jwt: ACCESS_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully reset password', async () => {
        const resetPasswordLink = await dataAccessAccount.sendResetPasswordLink({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(resetPasswordLink)).toBeFalsy();

        const mail = (await dataAccessE2e.mailbox())[0];

        expect(mail).not.toBeUndefined();

        const token = mail.messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        expect(token).not.toBeUndefined();

        const resetPassword = await dataAccessAccount.resetPassword({
            newPassword: 'NewScalio#1337!',
            token,
        });

        expect(isBazaErrorResponse(resetPassword)).toBeFalsy();
    });

    it('will not allow to sign in with old access token', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessAuth.verify({
                    jwt: ACCESS_TOKEN,
                });

                expect(isBazaErrorResponse(response)).toBeTruthy();
            },
            null,
            {
                intervalMillis: 2000,
            },
        );
    });

    it('will not allow to sign in with old password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will allow to sign in with new password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'NewScalio#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
