// https://scalio.atlassian.net/browse/BAZA-873

import 'reflect-metadata';
import { BazaAccountE2eNodeAccess, BazaAccountNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaCoreE2eFixtures,
    isBazaErrorResponse,
    ConfirmationEmailExpiryResendStrategy,
    AccountErrorCodes,
} from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-account/integration-tests/21-baza-account-email-conifirmation.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccountAccessE2e = new BazaAccountE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    const email = 'john.doe@gmail.com';

    let TOKEN: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
        await dataAccountAccessE2e.changeEmailConfirmationExpiryStrategy({
            // Set the confirmation expiray strategy to AUTO (in case its already changed in the running app)
            strategy: ConfirmationEmailExpiryResendStrategy.AUTO,
        });
    });

    it('will register user', async () => {
        const response = await dataAccessAccount.registerAccount({
            email,
            firstName: 'John',
            lastName: 'Doe',
            password: 'Scalio#1337',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow send reset password link for existing account', async () => {
        const response = await dataAccessAccount.sendConfirmEmailLink({
            email,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('aquire token using the email', async () => {
        const response = await dataAccountAccessE2e.getAccountConfirmationToken({
            email,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();

        TOKEN = response.token;
    });

    it('will expire the confirmation token', async () => {
        const response = await dataAccountAccessE2e.expireConfirmationToken({
            token: TOKEN,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to confirm email with default strategy being AUTO , so app will resend new email', async () => {
        const response = (await dataAccessAccount.confirmEmail({
            token: TOKEN,
        })) as any;
        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountTokenExpiredNewConfirmationEmailSent);
    });

    it('get current confirmation token again and it should be different with previous one', async () => {
        const response = await dataAccountAccessE2e.getAccountConfirmationToken({
            email,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.token).not.toEqual(TOKEN);
        TOKEN = response.token;
    });

    it('expire the newly generated token', async () => {
        const response = await dataAccountAccessE2e.expireConfirmationToken({
            token: TOKEN,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('change confirmation email strategy to manual', async () => {
        const response = await dataAccountAccessE2e.changeEmailConfirmationExpiryStrategy({
            strategy: ConfirmationEmailExpiryResendStrategy.MANUAL,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('ask for token confirmation again', async () => {
        const response = (await dataAccessAccount.confirmEmail({
            token: TOKEN,
        })) as any;
        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountTokenExpired);
    });

    it('try get current confirmation token again and this time there should be no tokens', async () => {
        const response = (await dataAccountAccessE2e.getAccountConfirmationToken({
            email,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.token).toBeUndefined();
    });

    it('ask for confirmation email since it should be manually requested now', async () => {
        const response = await dataAccessAccount.sendConfirmEmailLink({
            email,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('get the new confirmation token', async () => {
        const response = await dataAccountAccessE2e.getAccountConfirmationToken({
            email,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.token).not.toEqual(TOKEN);
        TOKEN = response.token;
    });

    it('use the new token to confirm email', async () => {
        const response = await dataAccessAccount.confirmEmail({
            token: TOKEN,
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
