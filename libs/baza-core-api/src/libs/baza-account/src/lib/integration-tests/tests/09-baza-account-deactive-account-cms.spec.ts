import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AccountErrorCodes, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsNodeAccess, BazaAccountCmsResetPasswordNodeAccess } from '@scaliolabs/baza-core-node-access';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/api-controllers/integration-tests/09-baza-account-deactivate-account-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountCMS = new BazaAccountCmsNodeAccess(http);
    const dataAccessAccountCMSResetPassword = new BazaAccountCmsResetPasswordNodeAccess(http);

    let USER_ACCOUNT_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const response = await dataAccessAccountCMS.listAccounts({});

        USER_ACCOUNT_ID = response.items.find((a) => a.email === 'e2e@scal.io').id;
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully deactivate account', async () => {
        const response = await dataAccessAccountCMS.deactivateAccount({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isDeactivated).toBeTruthy();
        expect(response.firstName).toBe('Deleted');
        expect(response.lastName).toBe('');
        expect(response.fullName).toBe('Deleted');
    });

    it('will fail to deactivate account once more', async () => {
        const response: BazaError = (await dataAccessAccountCMS.deactivateAccount({
            email: 'e2e@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountNotFound);
    });

    it('will not display deactivated account', async () => {
        const response = await dataAccessAccountCMS.listAccounts({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.find((a) => a.email === 'e2e@scal.io')).toBeUndefined();
    });

    it('will fail to assign admin access for deactivated account', async () => {
        const response: BazaError = (await dataAccessAccountCMS.assignAdminAccess({
            email: 'e2e@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountNotFound);
    });

    it('will fail to revoke admin access for deactivated account', async () => {
        const response: BazaError = (await dataAccessAccountCMS.revokeAdminAccess({
            email: 'e2e@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountNotFound);
    });

    it('will fail to return account by email with CMS endpoint', async () => {
        const response: BazaError = (await dataAccessAccountCMS.getAccountByEmail({
            email: 'e2e@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountNotFound);
    });

    it('will successfully return account by id with CMS endpoint', async () => {
        const response: BazaError = (await dataAccessAccountCMS.getAccountById({
            id: USER_ACCOUNT_ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fail to confirm update account with CMS endpoint', async () => {
        const response: BazaError = (await dataAccessAccountCMS.updateAccount({
            id: USER_ACCOUNT_ID,
            firstName: 'Foo',
            lastName: 'Bar',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountNotFound);
    });

    it('will sucessfully return account by id with CMS endpoint', async () => {
        const response = await dataAccessAccountCMS.getAccountById({
            id: USER_ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fail to manually reset password of deactivated account', async () => {
        const response: BazaError = (await dataAccessAccountCMSResetPassword.requestResetPassword({
            email: 'e2e@scal.io',
            subject: 'E2E 1 Subject',
            message: 'E2E 1 Message',
            attemptToSignInErrorMessage: 'E2E Request to reset password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountNotFound);

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(0);
    });

    it('will still allow to reset password for all users', async () => {
        const response = await dataAccessAccountCMSResetPassword.requestResetPasswordAllAccounts({
            synchronous: true,
            subject: 'E2E 1 Subject',
            message: 'E2E 1 Message',
            attemptToSignInErrorMessage: 'E2E Request to reset password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);
    });

    it('will allow to delete account', async () => {
        const response = await dataAccessAccountCMS.deleteAccountById({
            id: USER_ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
