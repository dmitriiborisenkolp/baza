import 'reflect-metadata';
import { BazaAccountCmsNodeAccess, BazaAuthNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AccountErrorCodes, BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-account/integration-tests/15-baza-account-change-password.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAccountCMS = new BazaAccountCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will not allow to remove confirmation from self', async () => {
        const response: BazaError = (await dataAccessAccountCMS.confirmEmailAccountById({
            id: http.authResponse.jwtPayload.accountId,
            confirmed: false,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountSelfAttemptUnverify);
    });

    it('will allow to sign in after failed attempt to remove confirmation from self', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e-admin@scal.io',
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
