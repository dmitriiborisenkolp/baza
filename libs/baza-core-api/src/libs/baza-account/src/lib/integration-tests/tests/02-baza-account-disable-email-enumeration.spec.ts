// https://scalio.atlassian.net/browse/CMNW-1555

import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

// Send Reset Password Link Flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/02-baza-account-disable-email-enumeration.spec.ts (send reset password link)', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will allow send reset password link for existing account', async () => {
        const response = await dataAccessAccount.sendResetPasswordLink({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow send reset password link for unknown account w/o errors', async () => {
        const response = await dataAccessAccount.sendResetPasswordLink({
            email: 'e2e-unknown-user@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});

// Send Email Confirmation Link Flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/02-baza-account-disable-email-enumeration.spec.ts (send email confirmation link)', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will allow send reset password link for existing account', async () => {
        const response = await dataAccessAccount.sendConfirmEmailLink({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow send reset password link for unknown account w/o errors', async () => {
        const response = await dataAccessAccount.sendConfirmEmailLink({
            email: 'e2e-unknown-user@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
