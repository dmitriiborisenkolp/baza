import 'reflect-metadata';
import { BazaAccountNodeAccess, BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaAccountFeatures } from '../../baza-account.config';

describe('@scaliolabs/baza-core-api/baza-account/api-controllers/integration-tests/16-baza-account-exists-feature.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will be disabled by default', async () => {
        const response: BazaError = (await dataAccessAccount.accountExists({
            email: 'e2e@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will allow to use when feature is enabled', async () => {
        await dataAccessE2e.enableFeature({
            feature: BazaAccountFeatures.Exists,
        });

        const response = await dataAccessAccount.accountExists({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will correctly display that account e2e@scal.io exists', async () => {
        const response = await dataAccessAccount.accountExists({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.hasActiveAccount).toBeTruthy();
        expect(response.hasDeactivatedAccounts).toBeFalsy();
    });

    it('will correctly display that account e2e-1234@scal.io does not exists', async () => {
        const response = await dataAccessAccount.accountExists({
            email: 'e2e-1234@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.hasActiveAccount).toBeFalsy();
        expect(response.hasDeactivatedAccounts).toBeFalsy();
    });

    it('will not display that admin accounts exists', async () => {
        const response = await dataAccessAccount.accountExists({
            email: 'e2e-admin@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.hasActiveAccount).toBeFalsy();
        expect(response.hasDeactivatedAccounts).toBeFalsy();
    });

    it('will correctly display deactivated accounts', async () => {
        await http.authE2eUser();

        const deactivateRequestResponse = await dataAccessAccount.sendDeactivateAccountLink({
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(deactivateRequestResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);

        const token = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        expect(token).not.toBeUndefined();

        const deactivateResponse = await dataAccessAccount.deactivateAccount({
            token,
        });

        expect(isBazaErrorResponse(deactivateResponse)).toBeFalsy();

        const response = await dataAccessAccount.accountExists({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.hasActiveAccount).toBeFalsy();
        expect(response.hasDeactivatedAccounts).toBeTruthy();
    });

    it('will correctly displays both active and deactivated accounts', async () => {
        http.noAuth();

        const registerResponse = await dataAccessAccount.registerAccount({
            email: 'e2e@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
        });

        expect(isBazaErrorResponse(registerResponse)).toBeFalsy();

        const response = await dataAccessAccount.accountExists({
            email: 'e2e@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.hasActiveAccount).toBeTruthy();
        expect(response.hasDeactivatedAccounts).toBeTruthy();
    });
});
