import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-account/integration-tests/17-baza-account-update-profile.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will successfully update first name, last name and phone number', async () => {
        const response = await dataAccessAccount.updateProfile({
            firstName: 'Foo',
            lastName: 'Bar',
            phone: '+7 123 456 789',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.firstName).toBe('Foo');
        expect(response.lastName).toBe('Bar');
        expect(response.fullName).toBe('Foo Bar');
        expect(response.phone).toBe('+7 123 456 789');

        const currentProfile = await dataAccessAccount.currentAccount();

        expect(currentProfile.firstName).toBe('Foo');
        expect(currentProfile.lastName).toBe('Bar');
        expect(currentProfile.fullName).toBe('Foo Bar');
        expect(currentProfile.phone).toBe('+7 123 456 789');
    });

    it('will correctly update first name only', async () => {
        const response = await dataAccessAccount.updateProfile({
            firstName: 'Boo',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.firstName).toBe('Boo');
        expect(response.lastName).toBe('Bar');
        expect(response.fullName).toBe('Boo Bar');
        expect(response.phone).toBe('+7 123 456 789');

        const currentProfile = await dataAccessAccount.currentAccount();

        expect(currentProfile.firstName).toBe('Boo');
        expect(currentProfile.lastName).toBe('Bar');
        expect(currentProfile.fullName).toBe('Boo Bar');
        expect(currentProfile.phone).toBe('+7 123 456 789');
    });

    it('will correctly update last name only', async () => {
        const response = await dataAccessAccount.updateProfile({
            lastName: 'Baz',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.firstName).toBe('Boo');
        expect(response.lastName).toBe('Baz');
        expect(response.fullName).toBe('Boo Baz');
        expect(response.phone).toBe('+7 123 456 789');

        const currentProfile = await dataAccessAccount.currentAccount();

        expect(currentProfile.firstName).toBe('Boo');
        expect(currentProfile.lastName).toBe('Baz');
        expect(currentProfile.fullName).toBe('Boo Baz');
        expect(currentProfile.phone).toBe('+7 123 456 789');
    });

    it('will correctly update phone number only', async () => {
        const response = await dataAccessAccount.updateProfile({
            phone: '+1 123 456 789',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.firstName).toBe('Boo');
        expect(response.lastName).toBe('Baz');
        expect(response.fullName).toBe('Boo Baz');
        expect(response.phone).toBe('+1 123 456 789');

        const currentProfile = await dataAccessAccount.currentAccount();

        expect(currentProfile.firstName).toBe('Boo');
        expect(currentProfile.lastName).toBe('Baz');
        expect(currentProfile.fullName).toBe('Boo Baz');
        expect(currentProfile.phone).toBe('+1 123 456 789');
    });
});
