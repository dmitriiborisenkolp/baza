import 'reflect-metadata';
import { BazaAccountCmsNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/a.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will search account by first name (full)', async () => {
        const response = await dataAccessAccountCms.listAccounts({
            queryString: 'E2E',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.find((a) => a.email === 'e2e@scal.io')).not.toBeUndefined();
    });

    it('will search account by first name (partial)', async () => {
        const response = await dataAccessAccountCms.listAccounts({
            queryString: 'E2',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.find((a) => a.email === 'e2e@scal.io')).not.toBeUndefined();
    });

    it('will search account by last name (full)', async () => {
        const response = await dataAccessAccountCms.listAccounts({
            queryString: 'User',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.find((a) => a.email === 'e2e@scal.io')).not.toBeUndefined();
    });

    it('will search account by last name (partial)', async () => {
        const response = await dataAccessAccountCms.listAccounts({
            queryString: 'Use',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.find((a) => a.email === 'e2e@scal.io')).not.toBeUndefined();
    });

    it('will search account by ID', async () => {
        const response = await dataAccessAccountCms.listAccounts({
            queryString: '1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.find((a) => a.email === 'e2e@scal.io')).not.toBeUndefined();
    });
});
