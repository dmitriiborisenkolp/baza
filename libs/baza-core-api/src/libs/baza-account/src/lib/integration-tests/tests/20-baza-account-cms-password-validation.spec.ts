import 'reflect-metadata';
import { BazaAccountCmsNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaError, generateRandomHexString, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes } from '@scaliolabs/baza-core-shared';

const randomEmail = () => `${generateRandomHexString()}@example.com`;

describe('@scaliolabs/baza-core-api/baza-account/integration-tests/20-baza-account-cms-password-validation.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will not allow to register user with simple password', async () => {
        const response: BazaError = (await dataAccessAccountCms.registerUserAccount({
            email: randomEmail(),
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'a123456',
            passwordConfirm: 'a123456',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough);
    });

    it('will not allow to register admin with simple password', async () => {
        const response: BazaError = (await dataAccessAccountCms.registerAdminAccount({
            email: randomEmail(),
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'a123456',
            passwordConfirm: 'a123456',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough);
    });

    it('will not allow to register admin with non matching passwordConfirm', async () => {
        const response: BazaError = (await dataAccessAccountCms.registerAdminAccount({
            email: randomEmail(),
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'a123456',
            passwordConfirm: 'a1234256',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordsDoNotMatch);
    });

    it('will not allow to register user with simple password', async () => {
        const response: BazaError = (await dataAccessAccountCms.registerUserAccount({
            email: randomEmail(),
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'a123456',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough);
    });

    it('will not allow to register user with non matching password confirm', async () => {
        const response: BazaError = (await dataAccessAccountCms.registerUserAccount({
            email: randomEmail(),
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'a123456',
            passwordConfirm: 'non-matching',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordsDoNotMatch);
    });

    it('will not allow to register admin without passwordConfirm', async () => {
        const response: BazaError = (await dataAccessAccountCms.registerAdminAccount({
            email: randomEmail(),
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'a123456',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough);
    });

    it('will not allow to register admin with non matching password confima', async () => {
        const response: BazaError = (await dataAccessAccountCms.registerAdminAccount({
            email: randomEmail(),
            firstName: 'Foo',
            lastName: 'Bar',
            password: 'a123456',
            passwordConfirm: 'non-matching',
            metadata: {},
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordsDoNotMatch);
    });
});
