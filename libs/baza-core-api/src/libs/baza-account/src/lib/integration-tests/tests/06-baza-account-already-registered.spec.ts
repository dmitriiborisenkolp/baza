import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsNodeAccess, BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/06-baza-account-already-registered.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAccountCMS = new BazaAccountCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will register account and there will be an email about it', async () => {
        const registerAccountResponse = await dataAccessAccount.registerAccount({
            email: 'foo-example@scal.io',
            firstName: 'Foo',
            lastName: 'Example',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(registerAccountResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);
    });

    it('will update first/last names for non-confirmed and dont send one more notification with reset password token', async () => {
        const registerAccountResponse: BazaError = (await dataAccessAccount.registerAccount({
            email: 'foo-example@scal.io',
            firstName: 'Bar',
            lastName: 'Baz',
            password: 'Scalio#1337!',
        })) as any;

        expect(isBazaErrorResponse(registerAccountResponse)).toBeTruthy();
        expect(registerAccountResponse.code).toBe(AccountErrorCodes.BazaAccountDuplicate);

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCMS.listAccounts({
            index: 1,
        });

        const account = accounts.items.find((a) => a.email === 'foo-example@scal.io');

        expect(account).toBeDefined();
        expect(account.firstName).toBe('Bar');
        expect(account.lastName).toBe('Baz');
        expect(account.fullName).toBe('Bar Baz');

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(2);
    });

    it('will allow to send reset password link for not email confirmed account', async () => {
        const response = await dataAccessAccount.sendResetPasswordLink({
            email: 'foo-example@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(3);
    });

    it('will reset password and will allow to sign in with new password', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        const match = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/);

        expect(match).not.toBeNull();

        const token = match[1];

        expect(token).not.toBeUndefined();

        const resetPasswordResponse = await dataAccessAccount.resetPassword({
            token: token,
            newPassword: 'DemoFooBar#1337!',
        });

        expect(isBazaErrorResponse(resetPasswordResponse)).toBeFalsy();
    });

    it('will not allow to sign in with previous password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'foo-example@scal.io',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will allow to sign in with new password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'foo-example@scal.io',
            password: 'DemoFooBar#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not have additional emails in mailbox after except reset password notification', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(3);

        const authResponse = dataAccessAuth.auth({
            email: 'scalio-admin@scal.io',
            password: 'DemoFooBar#1337!',
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();
    });

    it('will not allow to update first/last names of already registered account', async () => {
        const registerAccountResponse = await dataAccessAccount.registerAccount({
            email: 'foo-example@scal.io',
            firstName: 'Reset',
            lastName: 'Name',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(registerAccountResponse)).toBeTruthy();

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCMS.listAccounts({
            index: 1,
        });

        const account = accounts.items.find((a) => a.email === 'foo-example@scal.io');

        expect(account).toBeDefined();
        expect(account.firstName).toBe('Bar');
        expect(account.lastName).toBe('Baz');
        expect(account.fullName).toBe('Bar Baz');

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(3);
    });
});
