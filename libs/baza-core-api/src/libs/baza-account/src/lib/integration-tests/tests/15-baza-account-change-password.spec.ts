import 'reflect-metadata';
import { BazaAuthNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AuthErrorCodes, BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

describe('@scaliolabs/baza-core-api/baza-account/integration-tests/15-baza-account-change-password.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will allow to change password for user account (without confirm)', async () => {
        await http.authE2eUser();

        const response = await dataAccessAccount.changePassword({
            oldPassword: 'e2e-user-password',
            newPassword: 'SomePass#1234',
            confirmPassword: 'SomePass#1234',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        http.noAuth();
    });

    it('will not sign in user with previous password', async () => {
        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will sign in user with new password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'SomePass#1234',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to change password for admin account (without confirm)', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccount.changePassword({
            oldPassword: 'e2e-admin-password',
            newPassword: 'SomePass#1234',
            confirmPassword: 'SomePass#1234',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        http.noAuth();
    });

    it('will not sign in user with admin password', async () => {
        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will sign in admin with admin password', async () => {
        const response = await dataAccessAuth.auth({
            email: 'e2e-admin@scal.io',
            password: 'SomePass#1234',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not send 403 HTTP status when invalid password provided', async () => {
        const response = await dataAccessAccount.changePassword(
            {
                oldPassword: 'e2e-admin-invalid-password',
                newPassword: 'SomePass#1234',
                confirmPassword: 'SomePass#1234',
            },
            {
                asStatusCode: true,
            },
        );

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).not.toBe(HttpStatus.FORBIDDEN);
    });
});
