import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaError, generateRandomHexString, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes } from '@scaliolabs/baza-core-shared';

const randomEmail = () => `${generateRandomHexString()}@example.com`;

// Register account flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/01-baza-account-password.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will not allow to register user with simple "123456" password', async () => {
        const response = (await dataAccessAccount.registerAccount({
            email: randomEmail(),
            firstName: 'John',
            lastName: 'Doe',
            password: '1234567',
        })) as BazaError;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough);
    });

    it('will not allow to register user with simple "12345678" password', async () => {
        const response = (await dataAccessAccount.registerAccount({
            email: randomEmail(),
            firstName: 'John',
            lastName: 'Doe',
            password: '12345678',
        })) as BazaError;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough);
    });

    it('will not allow to register user with simple "aA123456789" password', async () => {
        const response = (await dataAccessAccount.registerAccount({
            email: randomEmail(),
            firstName: 'John',
            lastName: 'Doe',
            password: 'aA123456789',
        })) as BazaError;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough);
    });

    it('will allow to register user with "Scalio#1337!" password', async () => {
        const response = (await dataAccessAccount.registerAccount({
            email: randomEmail(),
            firstName: 'John',
            lastName: 'Doe',
            password: 'Scalio#1337!',
        })) as BazaError;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});

// Change password flow
describe('@scaliolabs/baza-core-api/baza-account/integration-tests/01-baza-account-password.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eUser();
    });

    it('will not allow to change password with simple "123456" password', async () => {
        const response = (await dataAccessAccount.changePassword({
            newPassword: '123456',
            oldPassword: 'e2e-user-password',
        })) as BazaError;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountPasswordIsNotStrongEnough);
    });

    it('will allow to change with "Scalio#1337!" password', async () => {
        const response = (await dataAccessAccount.changePassword({
            newPassword: 'Scalio#1337!',
            oldPassword: 'e2e-user-password',
        })) as BazaError;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});

// TODO: Same for forgot/reset password flow
