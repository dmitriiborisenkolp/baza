import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaAccountNodeAccess,
    BazaDataAccessNode,
    BazaRegistryNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaAccountRegisterOptions,
    AccountRole,
    BazaCoreE2eFixtures,
    BazaError,
    BazaReferralCodeAccountMetadata,
    isBazaErrorResponse,
    AccountErrorCodes,
} from '@scaliolabs/baza-core-shared';
import { BazaInviteCodeFixtures } from '../../../../baza-invite-code/src';
import { BazaReferralCodesE2eFixtures } from '../../../../baza-referral-code/src';

describe('@baza-code-api/baza-account-register/01-baza-account-register.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessRegistry = new BazaRegistryNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    const reset = async (withOptions: Partial<BazaAccountRegisterOptions> = {}) => {
        await http.authE2eAdmin();

        const options: BazaAccountRegisterOptions = {
            areNonCodeRegistrationsAllowed: false,
            areInviteCodeRegistrationsAllowed: false,
            areReferralCodeRegistrationsAllowed: false,
            ...withOptions,
        };

        await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaAccounts.registration.allowNonCodeRegistration',
            value: options.areNonCodeRegistrationsAllowed,
        });

        await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaAccounts.registration.allowInviteCodeRegistration',
            value: options.areInviteCodeRegistrationsAllowed,
        });

        await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaAccounts.registration.allowReferralCodeRegistration',
            value: options.areReferralCodeRegistrationsAllowed,
        });
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaInviteCodeFixtures.BazaInviteCodeFixture,
                BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCodes,
            ],
            specFile: __filename,
        });

        await reset();
    });

    beforeEach(async () => {
        await http.noAuth();
    });

    it('will not allow register account without invite codes if option is disabled', async () => {
        await reset();

        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'example-1@scal.io',
            firstName: 'Example',
            lastName: 'User',
            password: 'Scalio#1337!',
            referralCode: 'ExampleCode1',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountRegisterWithCodeForbidden);
    });

    it('will allow register account without invite codes if option is enabled', async () => {
        await reset({
            areInviteCodeRegistrationsAllowed: true,
        });

        const response = await dataAccessAccount.registerAccount({
            email: 'example-1@scal.io',
            firstName: 'Example',
            lastName: 'User',
            password: 'Scalio#1337!',
            referralCode: 'ExampleCode1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display invite code in account metadata', async () => {
        await http.authE2eAdmin();

        const listResponse = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
            size: -1,
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        const registeredAccount = listResponse.items.find((a) => a.email === 'example-1@scal.io');

        expect(registeredAccount).toBeDefined();

        const accountMetadata = registeredAccount.metadata as BazaReferralCodeAccountMetadata;

        expect(accountMetadata.signedUpWithReferralCode).toBeDefined();
        expect(accountMetadata.signedUpWithReferralCode).toBe('ExampleCode1');
    });

    it('will not allow to register account with referral code if option is not enabled', async () => {
        await reset();

        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'example-2@scal.io',
            firstName: 'Example',
            lastName: 'User',
            password: 'Scalio#1337!',
            referralCode: 'ExampleCode1_2',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountRegisterWithCodeForbidden);
    });

    it('will allow to register account with referral code if option is enabled', async () => {
        await reset({
            areReferralCodeRegistrationsAllowed: true,
        });

        const response = await dataAccessAccount.registerAccount({
            email: 'example-2@scal.io',
            firstName: 'Example',
            lastName: 'User',
            password: 'Scalio#1337!',
            referralCode: 'ExampleCode1_2',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display referral code in account metadata', async () => {
        await http.authE2eAdmin();

        const listResponse = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
            size: -1,
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        const registeredAccount = listResponse.items.find((a) => a.email === 'example-2@scal.io');

        expect(registeredAccount).toBeDefined();

        const accountMetadata = registeredAccount.metadata as BazaReferralCodeAccountMetadata;

        expect(accountMetadata.signedUpWithReferralCode).toBeDefined();
        expect(accountMetadata.signedUpWithReferralCode).toBe('ExampleCode1_2');
    });

    it('will not allow to register account w/o codes if option is disabled', async () => {
        await reset();

        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'example-4@scal.io',
            firstName: 'Example',
            lastName: 'User',
            password: 'Scalio#1337!',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(AccountErrorCodes.BazaAccountRegisterWithoutCodeForbidden);
    });

    it('will allow to register account w/o codes if option is enabled', async () => {
        await reset({
            areNonCodeRegistrationsAllowed: true,
        });

        const response = await dataAccessAccount.registerAccount({
            email: 'example-4@scal.io',
            firstName: 'Example',
            lastName: 'User',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
