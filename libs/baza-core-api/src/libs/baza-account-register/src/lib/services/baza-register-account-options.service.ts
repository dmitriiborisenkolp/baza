import { Injectable } from '@nestjs/common';
import { BazaRegistryService } from '../../../../baza-registry/src';
import { replaceTags } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeEntity, BazaReferralCodeService } from '../../../../baza-referral-code/src';
import { AccountEntity } from '../../../../baza-account/src';

@Injectable()
export class BazaRegisterAccountOptionsService {
    constructor(private readonly registry: BazaRegistryService, private readonly referralCodeService: BazaReferralCodeService) {}

    async defaultReferralCode(account: AccountEntity): Promise<BazaReferralCodeEntity | undefined> {
        return this.referralCodeService.findDefaultOrActiveReferralCodeOfAccount(account);
    }

    async referralCodeCopyText(account: AccountEntity): Promise<string | undefined> {
        const defaultReferralCode = await this.defaultReferralCode(account);

        if (defaultReferralCode) {
            const copyPasteReferralCode = this.registry.getValue('bazaAccounts.referralCodes.copyText');

            return replaceTags(copyPasteReferralCode, {
                code: defaultReferralCode.codeDisplay,
            });
        } else {
            return undefined;
        }
    }

    get areNonCodeRegistrationsAllowed(): boolean {
        return this.registry.getValue('bazaAccounts.registration.allowNonCodeRegistration');
    }

    get areInviteCodeRegistrationsAllowed(): boolean {
        return this.registry.getValue('bazaAccounts.registration.allowInviteCodeRegistration');
    }

    get areReferralCodeRegistrationsAllowed(): boolean {
        return this.registry.getValue('bazaAccounts.registration.allowReferralCodeRegistration');
    }
}
