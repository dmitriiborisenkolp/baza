import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaAccountApiModule } from '../../../baza-account/src';
import { BazaInviteCodeApiModule } from '../../../baza-invite-code/src';
import { BazaReferralCodesApiModule } from '../../../baza-referral-code/src';
import { BazaPasswordApiModule } from '../../../baza-password/src';
import { BazaApiCommonModule } from '../../../baza-common/src';
import { BazaRegisterAccountCommandHandler } from './command-handlers/baza-register-account.command-handler';
import { BazaRegisterAccountOptionsCommandHandler } from './command-handlers/baza-register-account-options.command-handler';
import { BazaRegisterAccountOptionsService } from './services/baza-register-account-options.service';
import { AccountConfirmedEventHandler } from './event-handlers/account-confirmed.event-handler';

const COMMAND_HANDLERS = [
    BazaRegisterAccountCommandHandler,
    BazaRegisterAccountOptionsCommandHandler,
];

const EVENT_HANDLERS = [
    AccountConfirmedEventHandler,
];

@Module({
    imports: [
        CqrsModule,
        BazaAccountApiModule,
        BazaInviteCodeApiModule,
        BazaReferralCodesApiModule,
        BazaPasswordApiModule,
        BazaApiCommonModule,
    ],
    providers: [
        ...COMMAND_HANDLERS,
        ...EVENT_HANDLERS,
        BazaRegisterAccountOptionsService,
    ],
    exports: [
        BazaRegisterAccountOptionsService,
    ],
})
export class BazaAccountRegisterApiModule {}
