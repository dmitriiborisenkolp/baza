import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountApiEvent, AccountApiEvents, ApiCqrsEvent, BazaReferralCodeAccountMetadata } from '@scaliolabs/baza-core-shared';
import { bazaNormalizeReferralCode } from '@scaliolabs/baza-core-shared';
import { BazaAccountRepository } from '../../../../baza-account/src';
import { BazaInviteCodeRepository, BazaInviteCodeService } from '../../../../baza-invite-code/src';
import {
    BazaReferralCodeMailService,
    BazaReferralCodeRepository,
    BazaReferralCodeService,
    BazaReferralCodeUsageService,
} from '../../../../baza-referral-code/src';
import { cqrs } from '../../../../baza-common/src';

/**
 * Event Handler which fills `signedUpWithReferralCode` and Invite Code / Referral Code data after confirming Email Address
 */
@EventsHandler(ApiCqrsEvent)
export class AccountConfirmedEventHandler implements IEventHandler<ApiCqrsEvent<AccountApiEvent, AccountApiEvents>> {
    constructor(
        private readonly service: BazaInviteCodeService,
        private readonly bazaNcIntegrationReferralCodesService: BazaReferralCodeService,
        private readonly referralCodeUsageService: BazaReferralCodeUsageService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly inviteCodeRepository: BazaInviteCodeRepository,
        private readonly bazaNcIntegrationReferralCodesMail: BazaReferralCodeMailService,
        private readonly bazaNcIntegrationReferralCodesRepository: BazaReferralCodeRepository,
    ) {}

    handle(event: ApiCqrsEvent<AccountApiEvent, AccountApiEvents>): void {
        cqrs<void>(AccountConfirmedEventHandler.name, async () => {
            switch (event.apiEvent.event.topic) {
                case AccountApiEvent.BazaAccountConfirmedEmail: {
                    const account = await this.accountRepository.findActiveAccountWithId(event.apiEvent.event.payload.accountId);

                    if (!account) {
                        return;
                    }

                    const inviteCode = (account.metadata as BazaReferralCodeAccountMetadata).signedUpWithReferralCode;

                    if (!inviteCode) {
                        return;
                    }

                    const inviteCodeEntity = await this.inviteCodeRepository.findInviteCodeByCode({
                        code: bazaNormalizeReferralCode(inviteCode),
                    });

                    if (inviteCodeEntity) {
                        await this.service.incrementInviteCodeUsage(inviteCode);
                    } else {
                        const hasReferralCode = await this.bazaNcIntegrationReferralCodesService.hasReferralCode(
                            bazaNormalizeReferralCode(inviteCode),
                        );

                        if (!hasReferralCode) {
                            return;
                        }

                        const referralCode = await this.bazaNcIntegrationReferralCodesRepository.findByCode(inviteCode);

                        if (referralCode) {
                            const hasReferralCodeUsageRecords = await this.referralCodeUsageService.hasReferralCodeUsageFor(account);

                            if (!hasReferralCodeUsageRecords) {
                                await this.referralCodeUsageService.writeReferralCodeUsage(account, inviteCode);

                                if (referralCode.associatedWithAccount) {
                                    this.bazaNcIntegrationReferralCodesMail
                                        .sendSignedUpWithReferralCodeNotification({
                                            referralCodeOwnerFullName: referralCode.associatedWithAccount.fullName,
                                            referralCodeOwnerFirstName: referralCode.associatedWithAccount.firstName,
                                            referralCodeOwnerEmail: referralCode.associatedWithAccount.email,
                                        })
                                        .catch((err) => {
                                            console.error(err);
                                        });
                                }
                            }
                        }
                    }
                }
            }
        });
    }
}
