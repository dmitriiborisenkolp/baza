import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaRegisterAccountOptionsCommand, BazaRegisterAccountOptionsCommandResult } from '../../../../baza-account/src';
import { BazaRegisterAccountOptionsService } from '../services/baza-register-account-options.service';
import { cqrsCommand } from '../../../../baza-common/src';

@CommandHandler(BazaRegisterAccountOptionsCommand)
export class BazaRegisterAccountOptionsCommandHandler
    implements ICommandHandler<BazaRegisterAccountOptionsCommand, BazaRegisterAccountOptionsCommandResult>
{
    constructor(private readonly service: BazaRegisterAccountOptionsService) {}

    async execute(command: BazaRegisterAccountOptionsCommand): Promise<BazaRegisterAccountOptionsCommandResult> {
        return cqrsCommand<BazaRegisterAccountOptionsCommandResult>(BazaRegisterAccountOptionsCommandHandler.name, async () => {
            return {
                referralCode: command.account ? (await this.service.defaultReferralCode(command.account))?.codeDisplay : undefined,
                referralCodeCopyText: command.account ? await this.service.referralCodeCopyText(command.account) : undefined,
                areInviteCodeRegistrationsAllowed: this.service.areInviteCodeRegistrationsAllowed,
                areReferralCodeRegistrationsAllowed: this.service.areReferralCodeRegistrationsAllowed,
                areNonCodeRegistrationsAllowed: this.service.areNonCodeRegistrationsAllowed,
            };
        });
    }
}
