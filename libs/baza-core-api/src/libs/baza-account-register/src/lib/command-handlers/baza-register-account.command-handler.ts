import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import {
    AccountEntity,
    BazaAccountRepository,
    BazaAccountService,
    BazaAccountManagedUsersService,
    BazaRegisterAccountCommand,
    BazaRegisterAccountCommandResult,
} from '../../../../baza-account/src';
import { AccountApiEvent, AccountApiEvents, accountFullName, applicationToClientApplication } from '@scaliolabs/baza-core-shared';
import { AccountPasswordsDoNotMatchException } from '../exceptions/account-passwords-do-not-match.exception';
import { BazaRegistryService } from '../../../../baza-registry/src';
import { WhitelistAccountService } from '../../../../baza-whitelist-account/src';
import { ApiEventBus } from '../../../../baza-event-bus/src';
import { AccountPasswordIsNotStrongEnoughException } from '../exceptions/account-password-is-not-strong-enough.exception';
import { AccountDuplicateException } from '../exceptions/account-duplicate.exception';
import { BazaPasswordService } from '../../../../baza-password/src';
import { BazaRegisterAccountOptionsService } from '../services/baza-register-account-options.service';
import { BcryptService, cqrsCommand } from '../../../../baza-common/src';
import { BazaInviteCodeService } from '../../../../baza-invite-code/src';
import { BazaReferralCodeService } from '../../../../baza-referral-code/src';
import { AccountRegisterWithCodeForbiddenException } from '../exceptions/account-register-with-code-forbidden.exception';
import { AccountRegisterWithoutCodeForbiddenException } from '../exceptions/account-register-without-code-forbidden.exception';
import { AccountRegisterCodeNotFoundException } from '../exceptions/account-register-code-not-found.exception';
import { AccountManagedUserOperationNotPermittedException } from '../exceptions/account-managed-user-operation-not-permitted.exception';

@CommandHandler(BazaRegisterAccountCommand)
export class BazaRegisterAccountCommandHandler implements ICommandHandler<BazaRegisterAccountCommand, BazaRegisterAccountCommandResult> {
    constructor(
        private readonly accountService: BazaAccountService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly registry: BazaRegistryService,
        private readonly whitelistAccount: WhitelistAccountService,
        private readonly managedUserAccounts: BazaAccountManagedUsersService,
        private readonly bcrypt: BcryptService,
        private readonly passwordService: BazaPasswordService,
        private readonly bazaEventBus: ApiEventBus<AccountApiEvent, AccountApiEvents>,
        private readonly registerAccountOptions: BazaRegisterAccountOptionsService,
        private readonly inviteCodeService: BazaInviteCodeService,
        private readonly referralCodesService: BazaReferralCodeService,
    ) {}

    async execute(command: BazaRegisterAccountCommand): Promise<BazaRegisterAccountCommandResult> {
        return cqrsCommand<BazaRegisterAccountCommandResult>(BazaRegisterAccountCommandHandler.name, async () => {
            if (this.managedUserAccounts.isManagedUser(command.request.email)) {
                throw new AccountManagedUserOperationNotPermittedException();
            }

            const hasReferralCode = await this.validateInviteOrReferralCode(command);

            return this.registerBazaAccount({
                ...command,
                request: {
                    ...command.request,
                    referralCode: hasReferralCode ? command.request.referralCode : undefined,
                },
            });
        });
    }

    private async validateInviteOrReferralCode(command: BazaRegisterAccountCommand): Promise<boolean> {
        const { request } = command;

        let hasReferralCode = false;
        let hasInviteCode = false;

        if (request.referralCode) {
            hasInviteCode = await this.inviteCodeService.hasInviteCode({
                code: request.referralCode,
            });

            hasReferralCode = await this.referralCodesService.hasReferralCode(request.referralCode);

            if (hasInviteCode && !this.registerAccountOptions.areInviteCodeRegistrationsAllowed) {
                throw new AccountRegisterWithCodeForbiddenException();
            }

            if (hasReferralCode && !this.registerAccountOptions.areReferralCodeRegistrationsAllowed) {
                throw new AccountRegisterWithCodeForbiddenException();
            }

            if (hasInviteCode) {
                await this.inviteCodeService.validateInviteCode({
                    code: request.referralCode,
                });
            } else if (hasReferralCode) {
                await this.referralCodesService.validateReferralCode({
                    code: request.referralCode,
                });
            } else {
                throw new AccountRegisterCodeNotFoundException();
            }
        } else {
            if (!this.registerAccountOptions.areNonCodeRegistrationsAllowed) {
                throw new AccountRegisterWithoutCodeForbiddenException();
            }
        }

        return hasReferralCode || hasInviteCode;
    }

    private async registerBazaAccount(command: BazaRegisterAccountCommand): Promise<BazaRegisterAccountCommandResult> {
        let { additionalMetadata } = command;
        const { request, context } = command;

        if (request.referralCode) {
            additionalMetadata = {
                ...(additionalMetadata || {}),
                signedUpWithReferralCode: request.referralCode,
            };
        }

        if (request.confirmPassword && request.password !== request.confirmPassword) {
            throw new AccountPasswordsDoNotMatchException();
        }

        const whitelistAccountAccessEnabled = this.registry.getValue('bazaCoreAuth.whitelistAccountAccess');

        if (whitelistAccountAccessEnabled) {
            await this.whitelistAccount.validateAccountAccess(request.email);
        }

        const passwordResponse = await this.passwordService.validate({
            password: request.password,
        });

        if (!passwordResponse.isValid) {
            throw new AccountPasswordIsNotStrongEnoughException();
        }

        if (await this.accountRepository.hasActiveAccountWithEmail(request.email)) {
            const existingAccount = await this.accountRepository.getActiveAccountWithEmail(request.email);

            if (existingAccount.isEmailConfirmed) {
                throw new AccountDuplicateException({
                    email: request.email,
                });
            } else {
                existingAccount.firstName = request.firstName;
                existingAccount.lastName = request.lastName;
                existingAccount.email = request.email;
                existingAccount.password = await this.bcrypt.hash(request.password);
                existingAccount.signedUpWithClient = applicationToClientApplication(context.application);

                existingAccount.fullName = accountFullName(existingAccount);

                if (additionalMetadata) {
                    existingAccount.metadata = {
                        ...existingAccount.metadata,
                        ...additionalMetadata,
                    };
                }

                await this.accountRepository.save(existingAccount);

                await this.accountService.sendConfirmEmailLink(
                    {
                        email: request.email,
                    },
                    context,
                );

                throw new AccountDuplicateException({
                    email: request.email,
                    notActivated: true,
                });
            }
        } else {
            const newAccount = new AccountEntity();

            newAccount.firstName = request.firstName;
            newAccount.lastName = request.lastName;
            newAccount.email = request.email;
            newAccount.password = await this.bcrypt.hash(request.password);
            newAccount.signedUpWithClient = applicationToClientApplication(context.application);

            newAccount.fullName = accountFullName(newAccount);

            if (additionalMetadata) {
                newAccount.metadata = {
                    ...newAccount.metadata,
                    ...additionalMetadata,
                };
            }

            const account = await this.accountRepository.save(newAccount);

            if (!this.managedUserAccounts.isManagedUser(request.email)) {
                await this.accountService.sendConfirmEmailLink(
                    {
                        email: request.email,
                    },
                    context,
                );
            }

            await this.bazaEventBus.publish({
                topic: AccountApiEvent.BazaAccountRegistered,
                payload: {
                    accountId: account.id,
                    accountEmail: account.email,
                    accountRole: account.role,
                    firstName: account.firstName,
                    lastName: account.lastName,
                    isRetryAttempt: false,
                },
            });

            return newAccount;
        }
    }
}
