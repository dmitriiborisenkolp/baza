import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountManagedUserOperationNotPermittedException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountManagedUserOperationNotPermitted],
            HttpStatus.BAD_REQUEST,
        );
    }
}
