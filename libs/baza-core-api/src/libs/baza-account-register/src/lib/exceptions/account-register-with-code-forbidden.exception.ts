import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountRegisterWithCodeForbiddenException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountRegisterWithCodeForbidden,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountRegisterWithCodeForbidden],
            HttpStatus.BAD_REQUEST,
        );
    }
}
