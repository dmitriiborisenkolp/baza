import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountRegisterWithoutCodeForbiddenException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountRegisterWithoutCodeForbidden,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountRegisterWithoutCodeForbidden],
            HttpStatus.BAD_REQUEST,
        );
    }
}
