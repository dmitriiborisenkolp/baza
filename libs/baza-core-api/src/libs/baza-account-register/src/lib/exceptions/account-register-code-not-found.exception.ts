import { AccountErrorCodes, accountErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AccountRegisterCodeNotFoundException extends BazaAppException {
    constructor() {
        super(
            AccountErrorCodes.BazaAccountRegisterOrInviteCodeNotFound,
            accountErrorCodesI18n[AccountErrorCodes.BazaAccountRegisterOrInviteCodeNotFound],
            HttpStatus.BAD_REQUEST,
        );
    }
}
