export * from './lib/exceptions/auth-account-email-is-not-verified.exception';
export * from './lib/exceptions/auth-admin-role-is-required.exception';
export * from './lib/exceptions/auth-session-is-not-available.exception';
export * from './lib/exceptions/auth-invalid-credentials.exception';
export * from './lib/exceptions/auth-invalid-jwt.exception';
export * from './lib/exceptions/auth-jwt-expired.exception';
export * from './lib/exceptions/auth-required-role-mismatch.exception';
export * from './lib/exceptions/auth-unknown-device-strategy.exception';
export * from './lib/exceptions/auth-account-reset-password-requested.exception';

export * from './lib/models/auth-api-session.model';
export * from './lib/models/request-with-user.model';

export * from './lib/service/auth-session.service';
export * from './lib/service/auth.service';

export * from './lib/service/auth.service';
export * from './lib/service/auth-master-password.service';
export * from './lib/service/auth-device-manager.service';
export * from './lib/service/auth-2fa.service';
export * from './lib/service/auth-2fa-settings.service';

export * from './lib/strategy/auth-jwt-device-strategy';
export * from './lib/strategy/psql/entities/baza-auth-access-token.entity';
export * from './lib/strategy/psql/entities/baza-auth-device.entity';
export * from './lib/strategy/psql/repositories/baza-auth-access-token.repository';
export * from './lib/strategy/psql/repositories/baza-auth-device.repository';
export * from './lib/strategy/redis/auth-jwt-redis-key.factory';
export * from './lib/strategy/redis/auth-jwt-redis.model';
export * from './lib/strategy/redis/auth-jwt-redis.strategy';

export * from './lib/guards/auth.guard';
export * from './lib/guards/auth-optional.guard';
export * from './lib/guards/auth-optional-no-fail-on-invalid-token.guard';
export * from './lib/guards/auth-require-admin-role.guard';

export * from './lib/resources/baza-auth.registry';

export * from './lib/baza-auth.config';
export * from './lib/baza-auth-jwt.module';
export * from './lib/baza-auth.module';
export * from './lib/baza-auth-master-password.module';

export * from './lib/decorators/req-account.decorator';
export * from './lib/decorators/req-account-id.decorator';
