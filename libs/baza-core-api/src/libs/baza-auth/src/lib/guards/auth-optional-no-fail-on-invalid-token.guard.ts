import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthSessionService } from '../service/auth-session.service';
import { AuthService } from '../service/auth.service';
import { BazaAuthApiModuleConfig } from '../baza-auth.config';
import { RequestWithUser } from '../models/request-with-user.model';

/**
 * The guards work in same way as AuthOptionalGuard, but additionally
 * guard will not throw any exceptions if something is wrong with JWT tokens
 * in `Authorization: Bearer ...` header
 */
@Injectable()
export class AuthOptionalNoFailOnInvalidTokenGuard implements CanActivate {
    constructor(
        private readonly moduleConfig: BazaAuthApiModuleConfig,
        private readonly authSession: AuthSessionService,
        private readonly authService: AuthService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest<RequestWithUser>();
        const header = (request.headers.authorization || '').split(' ');

        const path = request.path;

        if (this.moduleConfig.skipAuthGuardForEndpoints.includes(path)) {
            return true;
        } else if (header.length === 2 && header[0].toLowerCase() === 'bearer') {
            try {
                const jwt: string = header[1];

                const verifyResponse = await this.authService.verify({
                    jwt: jwt,
                });

                this.authSession.setupSession(jwt, verifyResponse.jwtPayload);

                request.user = await this.authSession.getAccountOptional();

                // Note: Do not add manual reset password request check here.
                // eslint-disable-next-line no-empty
            } catch (err) {}

            return true;
        } else {
            return true;
        }
    }
}
