import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthSessionService } from '../service/auth-session.service';
import { AuthAdminRoleIsRequiredException } from '../exceptions/auth-admin-role-is-required.exception';
import { AccountRole } from '@scaliolabs/baza-core-shared';

/**
 * The guard will require that current user has active auth session
 * and user has Admin role. The guard is commonly used for CMS controllers.
 *
 * @example
 * It's good practice to not mix Public and CMS-methods in same controller.
 * Avoid mixing Public/CMS methods because you may accidentally publish methods which
 * are not supposed to be used for non-admin users.
 *
 * ```typescript
 * // CMS API - we protect it with AuthGuard, Admin role and ACL
 *
 * import { Controller, UseGuards } from '@nestjs/common';
 * import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
 * import { MyProjectAcl, MyCreateRequest, MyCreateResponse } from '@scaliolabs/my-project-shared;
 *
 * @Controller()
 * @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
 * @AclNodes([MyProjectAcl.MyFeature])
 * export class MyCmsController {
 *     // All methods here will requires active auth session with Admin account only
 *     // ...
 *
 *     // Some methods will requires additional ACL:
 *     @UseGuards(WithAccessGuard)
 *     @AclNodes([MyProjectAcl.MyFeatureCreate])
 *     async create(@Body() request: MyCreateRequest): Promise<MyCreateResponse> {
 *         // ...
 *     }
 * }
 * ```
 *
 * ```typescript
 * // Public API - we protect it only with AuthGuard
 *
 * import { Controller, UseGuards } from '@nestjs/common';
 * import { AuthGuard } from '@scaliolabs/baza-core-api';
 *
 * @Controller()
 * @UseGuards(AuthSession)
 * export class MyController {
 *     // All methods here will requires active auth session
 * }
 * ```
 */
@Injectable()
export class AuthRequireAdminRoleGuard implements CanActivate {
    constructor(private readonly authSession: AuthSessionService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        if (this.authSession.session.jwtPayload.accountRole === AccountRole.Admin) {
            return true;
        } else {
            throw new AuthAdminRoleIsRequiredException();
        }
    }
}
