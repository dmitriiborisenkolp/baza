import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthSessionService } from '../service/auth-session.service';
import { AuthService } from '../service/auth.service';
import { BazaAuthApiModuleConfig } from '../baza-auth.config';
import { AuthAccountResetPasswordRequestedException } from '../exceptions/auth-account-reset-password-requested.exception';
import { RequestWithUser } from '../models/request-with-user.model';

/**
 * AuthOptionalGuard
 *
 * AuthOptionalGuards works in same way as AuthGuard with only exception
 * that if there is no `Authorization: Bearer ...` header available, the guard
 * will not throw any error and will allow to use target method. In this case
 * `AuthSessionService` will not be configured.
 */
@Injectable()
export class AuthOptionalGuard implements CanActivate {
    constructor(
        private readonly moduleConfig: BazaAuthApiModuleConfig,
        private readonly authSession: AuthSessionService,
        private readonly authService: AuthService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest<RequestWithUser>();
        const header = (request.headers.authorization || '').split(' ');

        const path = request.path;

        if (this.moduleConfig.skipAuthGuardForEndpoints.includes(path)) {
            return true;
        } else if (header.length === 2 && header[0].toLowerCase() === 'bearer') {
            const jwt: string = header[1];

            const verifyResponse = await this.authService.verify({
                jwt: jwt,
            });

            this.authSession.setupSession(jwt, verifyResponse.jwtPayload);

            const account = await this.authSession.getAccountOptional();

            if (account && account.manualResetPasswordRequest) {
                throw new AuthAccountResetPasswordRequestedException(account.manualResetPasswordRequest.attemptToSignInMessage);
            }

            request.user = account;

            return true;
        } else {
            return true;
        }
    }
}
