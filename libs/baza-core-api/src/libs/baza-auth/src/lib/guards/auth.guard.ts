import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request as ExpressRequest } from 'express';
import { BazaAuthApiModuleConfig } from '../baza-auth.config';
import { AuthInvalidJwtException } from '../exceptions/auth-invalid-jwt.exception';
import { AuthSessionService } from '../service/auth-session.service';
import { AuthService } from '../service/auth.service';
import { AuthAccountResetPasswordRequestedException } from '../exceptions/auth-account-reset-password-requested.exception';
import { RequestWithUser } from '../models/request-with-user.model';

/**
 * AuthGuard is used commonly in controller's UseGuards to check if user is authenticated
 * or not. If not, user will not be allowed to use method
 *
 * Additional information:
 *
 * - AuthGuard uses `Authorization: Bearer ...` header to read access token. If Authorization header is presented,
 * but contains something wrong, AuthGuard will throw an exception with `AuthErrorCodes.AuthInvalidJwt` error code.
 * - If endpoint (method) is listed in `BazaAuthApiModuleConfig.moduleConfig.skipAuthGuardForEndpoints`, AuthGuard
 * will not perform any authentication attempts and will completely ignore the route
 * - If access token is outdated, AuthGuard will throw verify/access/auth-related error codes
 * - If user has manual request to reset password (i.e. admins asks user to reset password), AuthGuard will throw error
 * with `AuthErrorCodes.AuthAccountResetPasswordRequested` error code
 *
 * @example
 * ```typescript
 * import { Controller, UseGuards } from '@nestjs/common';
 * import { AuthGuard } from '@scaliolabs/baza-core-api';
 *
 * @Controller()
 * @UseGuards(AuthSession)
 * export class MyController {
 *     // All methods here will require active auth session
 * }
 * ```
 */
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private readonly moduleConfig: BazaAuthApiModuleConfig,
        private readonly authSession: AuthSessionService,
        private readonly authService: AuthService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const path = context.switchToHttp().getRequest<ExpressRequest>().path;

        if (this.moduleConfig.skipAuthGuardForEndpoints.includes(path)) {
            return true;
        } else {
            const request = context.switchToHttp().getRequest<RequestWithUser>();
            const header = (request.headers.authorization || '').split(' ');

            if (header.length === 2 && header[0].toLowerCase() === 'bearer') {
                const jwt: string = header[1];

                const verifyResponse = await this.authService.verify({
                    jwt: jwt,
                });

                this.authSession.setupSession(jwt, verifyResponse.jwtPayload);

                const account = await this.authSession.getAccount();

                if (account.manualResetPasswordRequest) {
                    throw new AuthAccountResetPasswordRequestedException(account.manualResetPasswordRequest.attemptToSignInMessage);
                }

                request.user = account;

                return true;
            } else {
                throw new AuthInvalidJwtException();
            }
        }
    }
}
