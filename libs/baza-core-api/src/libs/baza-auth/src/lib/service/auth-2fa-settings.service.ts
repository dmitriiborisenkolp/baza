import { Injectable } from '@nestjs/common';
import { AuthOptions, AuthService } from './auth.service';
import { AccountEntity, BazaAccountRepository } from '../../../../baza-account/src';
import { Auth2FAConfigResponse, Auth2FAUpdateSettingsRequest, Auth2FAVerificationMethod } from '@scaliolabs/baza-core-shared';
import { BazaAuthApiModuleConfig } from '../baza-auth.config';
import * as _ from 'lodash';
import { authenticator } from 'otplib';
import { MailService } from '../../../../baza-mail/src';

/**
 * 2FA Settings for Account Service
 */
@Injectable()
export class Auth2FASettingsService {
    constructor(
        private readonly authService: AuthService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly moduleConfig: BazaAuthApiModuleConfig,
        private readonly mailService: MailService,
    ) {}

    /**
     * Returns information about available / force 2FA methods
     * @param account
     */
    async config(account: AccountEntity): Promise<Auth2FAConfigResponse> {
        if (await this.mailService.transport.shouldDisable2Fa) {
            return this.no2FAConfig(account);
        }

        const allowed2FAMethods: Array<Auth2FAVerificationMethod> = this.moduleConfig.allowed2FAMethods(account.role);
        const enabled2FAMethods: Array<Auth2FAVerificationMethod> = [];
        const forced2FAMethods: Array<Auth2FAVerificationMethod> = [];

        if (account.twoFactorMethods.email?.enabled) {
            enabled2FAMethods.push(Auth2FAVerificationMethod.Email);
        }
        if (account.twoFactorMethods.phoneSMS?.enabled) {
            enabled2FAMethods.push(Auth2FAVerificationMethod.PhoneSMS);
        }
        if (account.twoFactorMethods.phoneCall?.enabled) {
            enabled2FAMethods.push(Auth2FAVerificationMethod.PhoneCall);
        }
        if (account.twoFactorMethods.googleAuthenticator?.enabled) {
            enabled2FAMethods.push(Auth2FAVerificationMethod.GoogleAuthenticator);
        }
        if (account.twoFactorMethods.microsoftAuthenticator?.enabled) {
            enabled2FAMethods.push(Auth2FAVerificationMethod.MicrosoftAuthenticator);
        }

        if (this.moduleConfig.require2FAEmailVerificationForAccountRoles.includes(account.role)) {
            forced2FAMethods.push(Auth2FAVerificationMethod.Email);
        }

        return {
            allowed2FAMethods,
            forced2FAMethods,
            enabled2FAMethods: _.uniq([...enabled2FAMethods, ...forced2FAMethods]),
        };
    }

    /**
     * Returns 2FA configuration with disable 2FA for current usage
     * Specifically used for case when Mail Transport is not configured yet
     * to allow sending emails using 2FA
     * @param account
     * @private
     */
    private async no2FAConfig(account: AccountEntity): Promise<Auth2FAConfigResponse> {
        const allowed2FAMethods: Array<Auth2FAVerificationMethod> = this.moduleConfig.allowed2FAMethods(account.role);

        return {
            allowed2FAMethods,
            enabled2FAMethods: [],
            forced2FAMethods: [],
        };
    }

    /**
     * Enables 2FA via Email for Account
     * @param account
     * @param request
     * @param options
     */
    async enable2FAEmail(account: AccountEntity, request: Auth2FAUpdateSettingsRequest, options: AuthOptions): Promise<void> {
        await this.authService.authVerifyCredentials(
            {
                email: account.email,
                password: request.password,
            },
            options,
        );

        account.twoFactorMethods.email.enabled = true;

        await this.accountRepository.save(account);
    }

    /**
     * Disables 2FA via Email for Account
     * @param account
     * @param request
     * @param options
     */
    async disable2FAEmail(account: AccountEntity, request: Auth2FAUpdateSettingsRequest, options: AuthOptions): Promise<void> {
        await this.authService.authVerifyCredentials(
            {
                email: account.email,
                password: request.password,
            },
            options,
        );

        account.twoFactorMethods.email.enabled = false;

        await this.accountRepository.save(account);
    }

    /**
     * Enables 2FA via Google Authenticator for Account
     * @param account
     * @param request
     * @param options
     */
    async enable2FAGoogle(account: AccountEntity, request: Auth2FAUpdateSettingsRequest, options: AuthOptions): Promise<string> {
        await this.authService.authVerifyCredentials(
            {
                email: account.email,
                password: request.password,
            },
            options,
        );

        const secretCode = authenticator.generateSecret();

        account.twoFactorMethods.googleAuthenticator = {
            enabled: true,
            secretCode,
        };

        await this.accountRepository.save(account);

        return secretCode;
    }

    /**
     * Disables 2FA via Google Authenticator for Account
     * @param account
     * @param request
     * @param options
     */
    async disable2FAGoogle(account: AccountEntity, request: Auth2FAUpdateSettingsRequest, options: AuthOptions): Promise<void> {
        await this.authService.authVerifyCredentials(
            {
                email: account.email,
                password: request.password,
            },
            options,
        );

        account.twoFactorMethods.googleAuthenticator.enabled = false;

        await this.accountRepository.save(account);
    }
}
