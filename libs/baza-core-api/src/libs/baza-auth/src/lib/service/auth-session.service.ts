import { Injectable, Scope } from '@nestjs/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthSessionIsNotAvailableException } from '../exceptions/auth-session-is-not-available.exception';
import { JwtPayload } from '@scaliolabs/baza-core-shared';
import { AuthApiSession } from '../models/auth-api-session.model';
import { BazaAccountRepository } from '../../../../baza-account/src/lib/repositories/baza-account.repository';
import { AccountEntity } from '../../../../baza-account/src/lib/entities/account.entity';

/**
 * Auth Session service is responsible for store current Auth Session for current
 * Request scope. AuthSessionService can be injected only on controller level,
 * and you should not use it directly in services. If you need to pass current
 * Auth Session to target services, use additional arguments in your target services
 * and bypass Auth Session into your services.
 *
 * Auth Session stores information about current account, JWT or JWT payload. Please
 * note that JWT Payload may have outdated information as far as it's being generated
 * and stored on clients side for some amount of time. If you need to have some
 * well-updated additional configurations, store is as AccountSettings in AccountEntity.
 *
 * AuthSessionService requires AuthGuard guard. Additionally, you can use AuthOptionalGuard
 * or AuthOptionalNoFailOnInvalidTokenGuard guards; before getting account session
 * using these guards, you need to check does AuthSession exist or not.
 *
 * @decorator `@Injectable({ scope: Scope.REQUEST })`
 *
 * @example
 * Common scenario - Auth session is required to use these methods:
 *
 * @example
 * ```typescript
 * import { Controller, UseGuards } from '@nestjs/common';
 * import { AuthGuard, AuthSessionService } from '@scaliolabs/baza-core-api';
 *
 * @Controller()
 * @UseGuards(AuthSession)
 * export class MyController {
 *      constructor(
 *          private readonly authSession: AuthSessionService,
 *          private readonly myService: MyService,
 *      ) {}
 *
 *      async foo(request: FooRequest): Promise<FooResponse> {
 *          const account = (await this.authSession.getAccount());
 *
 *          return this.myService.foo(request, account);
 *      }
 * }
 * ```
 *
 * Optional scenario - AuthSession is optional
 *
 * ```typescript
 * import { Controller, UseGuards } from '@nestjs/common';
 * import { AuthOptionalGuard, AuthSessionService } from '@scaliolabs/baza-core-api';
 *
 * @Controller()
 * @UseGuards(AuthOptionalGuard)
 * export class MyController {
 *      constructor(
 *          private readonly authSession: AuthSessionService,
 *          private readonly myService: MyService,
 *      ) {}
 *
 *      async foo(request: FooRequest): Promise<FooResponse> {
 *          const account = this.authSession.hasSession
 *              ? await this.authSession.getAccount()
 *              : undefined;
 *
 *          return this.myService.foo(request, account);
 *      }
 * }
 * ```
 */
@Injectable({
    scope: Scope.REQUEST,
})
export class AuthSessionService {
    private readonly _session$: BehaviorSubject<AuthApiSession | undefined> = new BehaviorSubject<AuthApiSession | undefined>(undefined);

    constructor(private readonly accountRepository: BazaAccountRepository) {}

    /**
     * Returns current AuthSession as Observable.
     * It's rarely used.
     */
    get session$(): Observable<AuthApiSession | undefined> {
        return this._session$.asObservable();
    }

    /**
     * Returns current AuthSession.
     *
     * If session is not available, method will throws an exception with
     * `AuthErrorCodes.AuthSessionIsNotAvailable` error code.
     *
     * @see AuthErrorCodes
     * @see AuthErrorCodes.AuthSessionIsNotAvailable
     */
    get session(): AuthApiSession {
        if (this.hasSession) {
            return this._session$.getValue();
        } else {
            throw new AuthSessionIsNotAvailableException();
        }
    }

    /**
     * Returns true if AuthSession is available for current request
     */
    get hasSession(): boolean {
        return this._session$.getValue() !== undefined;
    }

    /**
     * Set up Auth Session. Used mostly by AuthService.
     *
     * @see AuthService
     *
     * @param jwt
     * @param jwtPayload
     */
    setupSession(jwt: string, jwtPayload: JwtPayload): void {
        this._session$.next({
            ...this._session$.getValue(),
            jwt,
            jwtPayload,
        });
    }

    /**
     * Returns AccountEntity of current Auth Session.
     * Account entity is not cached somewhere.
     *
     * If you don't need full Account Entity, it's preferred to use accountId
     * getter instead if getAccount method.
     *
     * If session is not available, method will throw an exception with
     * `AuthErrorCodes.AuthSessionIsNotAvailable` error code.
     *
     * @see AuthErrorCodes
     * @see AuthErrorCodes.AuthSessionIsNotAvailable
     * @see AuthSessionService.accountId
     */
    async getAccount(): Promise<AccountEntity> {
        return this.accountRepository.getActiveAccountWithId(this.session.jwtPayload.accountId);
    }

    /**
     * Returns AccountEntity if there is active Auth Session
     *
     * @see hasSession
     * @see getAccount
     */
    async getAccountOptional(): Promise<AccountEntity | undefined> {
        return this.hasSession ? this.getAccount() : undefined;
    }

    /**
     * Returns Account ID of current Auth Session.
     *
     * If you don't need full Account Entity, it's preferred to use accountId
     * getter instead if getAccount method.
     *
     * If session is not available, method will throw an exception with
     * `AuthErrorCodes.AuthSessionIsNotAvailable` error code.
     *
     * @see AuthErrorCodes
     * @see AuthErrorCodes.AuthSessionIsNotAvailable
     * @see AuthSessionService.getAccount
     */
    get accountId(): number {
        return this.session.jwtPayload.accountId;
    }
}
