import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthInvalidCredentialsException } from '../exceptions/auth-invalid-credentials.exception';
import { AuthAccountEmailIsNotVerifiedException } from '../exceptions/auth-account-email-is-not-verified.exception';
import { AuthInvalidJwtException } from '../exceptions/auth-invalid-jwt.exception';
import { AuthSessionService } from './auth-session.service';
import {
    AccountRole,
    allowedApplicationsForAdministrators,
    ApiEventSource,
    Application,
    Auth2FAVerificationMethod,
    AuthApiEvent,
    AuthApiEvents,
    AuthErrorCodes,
    AuthRequest,
    AuthResponse,
    BazaAccountConstants,
    BazaDeviceTokenActionSets,
    BazaDeviceTokenBulkCommand,
    generateRandomHexString,
    InvalidateByRefreshTokenRequest,
    InvalidateTokenRequest,
    isEmpty,
    JwtPayload,
    RefreshTokenRequest,
    RefreshTokenResponse,
    VerifyRequest,
} from '@scaliolabs/baza-core-shared';
import { BAZA_AUTH_E2E_HELPERS_INJECTS, BazaAuthApiModuleConfig } from '../baza-auth.config';
import { AuthApiSession } from '../models/auth-api-session.model';
import { AuthJwtExpiredException } from '../exceptions/auth-jwt-expired.exception';
import { AuthRequiredRoleMismatchException } from '../exceptions/auth-required-role-mismatch.exception';
import { AuthMasterPasswordService } from './auth-master-password.service';
import { AuthAccountResetPasswordRequestedException } from '../exceptions/auth-account-reset-password-requested.exception';
import { ApiEventBus } from '../../../../baza-event-bus/src';
import { BcryptService } from '../../../../baza-common/src';
import { AclService } from '../../../../baza-acl/src';
import {
    AccountEntity,
    BazaAccountCmsResetPasswordService,
    BazaAccountRepository,
    BazaAccountManagedUsersService,
} from '../../../../baza-account/src';
import { AuthDeviceManagerService } from './auth-device-manager.service';
import { EnvService } from '../../../../baza-env/src';
import { AuthAdminIsNotAllowedToSignInWebAppException } from '../exceptions/auth-admin-is-not-allowed-to-sign-in-web-app.exception';
import { Auth2FARequiredException } from '../exceptions/auth-2fa-required.exception';
import { CommandBus } from '@nestjs/cqrs';
import { MailService } from '../../../../baza-mail/src';

/**
 * Additional options passed to auth methods
 */
export interface AuthOptions {
    application: Application;
    applicationId?: string;
    ip: string;
    userAgent: string;
    deviceTokens: BazaDeviceTokenActionSets;
}

/**
 * AuthService allows to authenticated current used withing current session,
 * refresh token (receives new JWT by refreshToken), invalidate some or all
 * access tokens and anything else related to Auth sessions.
 *
 * Please note that AuthService has common injection scope; It will not automatically
 * set up current Auth session (@see `AuthSessionService`) and it works as standalone
 * service on request / responses basis.
 *
 * @decorator `@Injectable()`
 *
 * @see AuthDeviceManagerService
 * @see AuthSessionService
 */
@Injectable()
export class AuthService {
    constructor(
        private readonly moduleConfig: BazaAuthApiModuleConfig,
        private readonly env: EnvService,
        private readonly jwtService: JwtService,
        private readonly commandBus: CommandBus,
        private readonly bazaEventBus: ApiEventBus<AuthApiEvent, AuthApiEvents>,
        private readonly accountRepository: BazaAccountRepository,
        private readonly bcryptService: BcryptService,
        private readonly authDeviceManager: AuthDeviceManagerService,
        private readonly authSession: AuthSessionService,
        private readonly aclService: AclService,
        private readonly authMasterPassword: AuthMasterPasswordService,
        private readonly accountCmsResetPassword: BazaAccountCmsResetPasswordService,
        private readonly managedUserAccounts: BazaAccountManagedUsersService,
        private readonly mailService: MailService,
    ) {}

    /**
     * Method attempts to validate email-password combination. Method will returns
     * AuthResponse object if email-password combination is valid. Method uses
     * authAccount method to generate AuthResponse object.
     *
     * If requireRole specified, method will throw an error if email-password
     * is valid, but active account with given email does not has same role as
     * required.
     *
     * Method will returns same `AuthErrorCodes.AuthInvalidCredentials` error
     * code both for Invalid Email or Invalid Password scenarios. It's implemented
     * this way due of security reasons.
     *
     * Additionally, if account has manual reset password request (i.e. admins requested
     * account to reset password), method will throw an exception with
     * `AuthErrorCodes.AuthAccountResetPasswordRequested` error code.
     *
     * Method requires `application` option. Method will not allow to sign in
     * admin accounts to non-administrator-allowed applications such as WEB, IOS
     * or Android. You can sign in as admin user only for API and CMS applications.
     *
     * Generated JWT tokens will be attached to account using AuthDeviceManagerService
     * service. Baza uses whitelist strategy for authorization.
     *
     * @see Application
     * @see AuthDeviceManagerService
     *
     * @param request AuthRequest object (email, password, requireRole optionally)
     * @param options Additional options (application, applicationId)
     */
    async auth(request: AuthRequest, options: AuthOptions): Promise<AuthResponse> {
        const account = await this.authVerifyCredentials(request, options);
        const account2FAMethods = await this.auth2FAMethods(account);

        if (account2FAMethods.length > 0) {
            throw new Auth2FARequiredException();
        }

        const authDTO = await this.authAccount(account, {
            withRefreshToken: true,
            application: options.application,
            applicationId: options.applicationId,
            ip: options.ip,
            userAgent: options.userAgent,
            deviceTokens: options.deviceTokens,
        });

        await this.bazaEventBus.publish(
            {
                topic: AuthApiEvent.BazaAuth,
                payload: {
                    ip: options.ip,
                    userAgent: options.userAgent,
                    accountId: account.id,
                    jwt: authDTO.accessToken,
                    jwtPayload: authDTO.jwtPayload,
                    accountSettings: authDTO.accountSettings,
                },
            },
            {
                target: ApiEventSource.CurrentNode,
            },
        );

        return authDTO;
    }

    /**
     * Returns list of possible 2FA options
     *
     * @param account
     */
    async auth2FAMethods(account: AccountEntity): Promise<Array<Auth2FAVerificationMethod>> {
        const availableAuth2FAMethods: Array<Auth2FAVerificationMethod> = [];

        if (await this.mailService.transport.shouldDisable2Fa) {
            return [];
        }

        const isLocalEnvironment = this.env.isLocalEnvironment;
        const isEnabledWithModuleConfig = this.moduleConfig.require2FAEmailVerificationForAccountRoles.includes(account.role);
        const isAuth2FADisabled = this.env.getAsBoolean('AUTH_2FA_DISABLED', { defaultValue: false });

        if (account.twoFactorMethods.email?.enabled) {
            availableAuth2FAMethods.push(Auth2FAVerificationMethod.Email);
        } else if (this.env.isTestEnvironment && BAZA_AUTH_E2E_HELPERS_INJECTS.enable2FAForAccountRoles.includes(account.role)) {
            availableAuth2FAMethods.push(Auth2FAVerificationMethod.Email);
        } else if (!this.env.isTestEnvironment) {
            if (!isLocalEnvironment && isEnabledWithModuleConfig) {
                availableAuth2FAMethods.push(Auth2FAVerificationMethod.Email);
            } else if (isLocalEnvironment && !isAuth2FADisabled && isEnabledWithModuleConfig) {
                availableAuth2FAMethods.push(Auth2FAVerificationMethod.Email);
            }
        }

        if (account.twoFactorMethods.phoneSMS?.enabled && !!account.twoFactorMethods.phoneSMS?.phoneNumber) {
            availableAuth2FAMethods.push(Auth2FAVerificationMethod.PhoneSMS);
        }

        if (account.twoFactorMethods.phoneCall?.enabled && !!account.twoFactorMethods.phoneCall?.phoneNumber) {
            availableAuth2FAMethods.push(Auth2FAVerificationMethod.PhoneCall);
        }

        if (account.twoFactorMethods.googleAuthenticator?.enabled && !!account.twoFactorMethods.googleAuthenticator?.secretCode) {
            availableAuth2FAMethods.push(Auth2FAVerificationMethod.GoogleAuthenticator);
        }

        if (account.twoFactorMethods.microsoftAuthenticator?.enabled && !!account.twoFactorMethods.microsoftAuthenticator?.secretCode) {
            availableAuth2FAMethods.push(Auth2FAVerificationMethod.MicrosoftAuthenticator);
        }

        return availableAuth2FAMethods;
    }

    /**
     * Method attempts to validate email-password combination. Method will returns
     * AccountEntity object and will not generate JWT tokens and will not add device
     * to JWT device manager.
     *
     * @param request AuthRequest object (email, password, requireRole optionally)
     * @param options Additional options (application, applicationId)
     */
    async authVerifyCredentials(request: AuthRequest, options: AuthOptions): Promise<AccountEntity> {
        const account = await this.accountRepository.findActiveAccountWithEmail(request.email);

        if (
            request.password.length < BazaAccountConstants.V_PASSWORD_MIN ||
            request.password.length > BazaAccountConstants.V_PASSWORD_MAX
        ) {
            throw new AuthInvalidCredentialsException();
        }

        if (!account) {
            await this.bazaEventBus.publish({
                topic: AuthApiEvent.BazaAuthFailed,
                payload: {
                    errorCode: AuthErrorCodes.AuthInvalidCredentials,
                },
            });

            throw new AuthInvalidCredentialsException();
        }

        if (account.manualResetPasswordRequest) {
            await this.accountCmsResetPassword.sendManualPasswordRequestEmail(account);

            throw new AuthAccountResetPasswordRequestedException(account.manualResetPasswordRequest.attemptToSignInMessage);
        }

        if (request.requireRole && account.role !== request.requireRole) {
            throw new AuthRequiredRoleMismatchException({
                role: request.requireRole,
            });
        }

        if (
            !this.env.isTestEnvironment &&
            account.role === AccountRole.Admin &&
            !allowedApplicationsForAdministrators.includes(options.application)
        ) {
            throw new AuthAdminIsNotAllowedToSignInWebAppException();
        }

        const isAuthWithMasterPassword =
            (await this.authMasterPassword.isEnabled) &&
            request.password === (await this.authMasterPassword.current()) &&
            account.role !== AccountRole.Admin;

        const isPasswordMatches = this.managedUserAccounts.isManagedUser(request.email)
            ? request.password === this.managedUserAccounts.getManagedUserConfig(request.email).password
            : !isEmpty(account.password) && (await this.bcryptService.compare(request.password, account.password));

        if (!isAuthWithMasterPassword && !isPasswordMatches) {
            await this.bazaEventBus.publish({
                topic: AuthApiEvent.BazaAuthFailed,
                payload: {
                    errorCode: AuthErrorCodes.AuthInvalidCredentials,
                    accountId: account.id,
                    ip: options.ip,
                    userAgent: options.userAgent,
                },
            });

            throw new AuthInvalidCredentialsException();
        }

        return account;
    }

    /**
     * Method will validate refreshToken and it's valid and not expired, method will
     * generate new auth payload object with Access Token, Refresh Token, JWY Payload and Account Settings.
     *
     * @param request
     * @param options
     */
    async refreshToken(request: RefreshTokenRequest, options: AuthOptions): Promise<RefreshTokenResponse> {
        try {
            this.jwtService.verify(request.refreshToken, {
                secret: this.moduleConfig.jwtSecret,
            });
        } catch (error) {
            /* if(error.name === 'TokenExpiredError'){
                throw new AuthJwtExpiredException();
            } */

            throw new AuthInvalidJwtException();
        }

        const jwtPayload: JwtPayload = this.jwtService.decode(request.refreshToken, {
            json: true,
        }) as JwtPayload;

        if (!jwtPayload) {
            throw new AuthInvalidJwtException();
        }

        const account = await this.accountRepository.findActiveAccountWithId(jwtPayload.accountId);

        if (!account) {
            throw new AuthInvalidCredentialsException();
        }

        if (account.manualResetPasswordRequest) {
            throw new AuthAccountResetPasswordRequestedException(account.manualResetPasswordRequest.attemptToSignInMessage);
        }

        if (
            !(await this.authDeviceManager.hasDeviceWithRefreshToken(account, {
                refreshToken: request.refreshToken,
            }))
        ) {
            throw new AuthInvalidJwtException();
        }

        if (jwtPayload.exp < new Date().getTime() / 1000) {
            throw new AuthJwtExpiredException();
        }

        return await this.authAccount(account, {
            withRefreshToken: true,
            application: options.application,
            applicationId: options.applicationId,
            ip: options.ip,
            userAgent: options.userAgent,
            deviceTokens: options.deviceTokens,
        });
    }

    /**
     * Method will generate JWT tokens and AuthResponse object for given account.
     * Method requires withRefreshToken. For long updatable sessions you will need
     * refreshToken; for single-time use you need only single access token which
     * will expires very shortly (~5 minutes).
     *
     * Method requires `application` option. Method will not allow to sign in
     * admin accounts to non-administrator-allowed applications such as WEB, IOS
     * or Android. You can sign in as admin user only for API and CMS applications.
     *
     * Generated JWT tokens will be stored in Account using AuthDeviceManagerService
     * service.
     *
     * @see Application
     * @see AuthDeviceManagerService
     *
     * @param account - AccountEntity
     * @param options – additional options (withRefreshAccount, application applicationId optionally)
     */
    async authAccount(
        account: AccountEntity,
        options: AuthOptions & {
            withRefreshToken: boolean;
        },
    ): Promise<AuthResponse> {
        if (!account.isEmailConfirmed) {
            if (this.managedUserAccounts.isManagedUser(account.email)) {
                account.isEmailConfirmed = true;

                await this.accountRepository.save(account);
            } else {
                throw new AuthAccountEmailIsNotVerifiedException();
            }
        }

        const accountAcl = (await this.aclService.hasAclOfAccount(account)) ? (await this.aclService.getAclOfAccount(account)).acl : [];

        const jwtPayload: JwtPayload = {
            accountId: account.id,
            accountEmail: account.email,
            accountRole: account.role,
            isManagedUserAccount: this.managedUserAccounts.isManagedUser(account.email),
            accountAcl,
        };

        const accessToken = this.jwtService.sign(jwtPayload, {
            expiresIn: this.moduleConfig.accessTokenExpiresSettings.find((j) => j.role === account.role).expiresIn,
            secret: this.moduleConfig.jwtSecret,
            jwtid: generateRandomHexString(32),
        });

        const refreshToken = options.withRefreshToken
            ? this.jwtService.sign(jwtPayload, {
                  expiresIn: this.moduleConfig.refreshTokenExpiresSettings.find((j) => j.role === account.role).expiresIn,
                  secret: this.moduleConfig.jwtSecret,
              })
            : undefined;

        await this.authDeviceManager.attachDevice(account, {
            application: options.application,
            refreshToken,
        });

        await this.authDeviceManager.attachAccessToken(account, {
            refreshToken,
            accessToken,
        });

        account.lastSignIn = new Date();

        await this.accountRepository.save(account);

        await this.commandBus.execute(
            new BazaDeviceTokenBulkCommand({
                accountId: account.id,
                actionSets: options.deviceTokens,
            }),
        );

        await this.bazaEventBus.publish(
            {
                topic: AuthApiEvent.BazaAuthAccessTokenAttached,
                payload: {
                    jwt: accessToken,
                    application: options.application,
                    accountEmail: account.email,
                    accountId: account.id,
                },
            },
            {
                target: ApiEventSource.CurrentNode,
            },
        );

        return {
            accessToken,
            refreshToken,
            jwtPayload,
            accountSettings: account.settings,
        };
    }

    /**
     * Verify JWT (access or refresh token). If JWT token is not valid,
     * the method will throws exception with AuthErrorCodes.AuthInvalidJwt error
     * code.
     *
     * If account has active request to reset password, method
     * will throw an exception with AuthErrorCodes.AuthAccountResetPasswordRequested
     * error code.
     *
     * Optional `requireRole` will additionally check does account has given role
     * or not.
     *
     * Method produces BazaAuthVerifySuccess or BazaAuthVerifyFailed events.
     *
     * @see AuthApiEvent
     * @see AuthApiEvent.BazaAuthVerifySuccess
     * @see AuthApiEvent.BazaAuthVerifyFailed
     * @param request
     */
    async verify(request: VerifyRequest): Promise<AuthApiSession> {
        try {
            this.jwtService.verify(request.jwt, {
                secret: this.moduleConfig.jwtSecret,
            });
        } catch (err) {
            throw new AuthInvalidJwtException();
        }

        const jwtPayload: JwtPayload = this.jwtService.decode(request.jwt, {
            json: true,
        }) as any;

        if (jwtPayload === null) {
            throw new AuthInvalidJwtException();
        }

        const account = await this.accountRepository.findActiveAccountWithId(jwtPayload.accountId);

        if (!account) {
            await this.bazaEventBus.publish(
                {
                    topic: AuthApiEvent.BazaAuthVerifyFailed,
                    payload: {
                        jwt: request.jwt,
                        errorCode: AuthErrorCodes.AuthInvalidCredentials,
                    },
                },
                {
                    target: ApiEventSource.CurrentNode,
                },
            );

            throw new AuthInvalidCredentialsException();
        }

        if (account.manualResetPasswordRequest) {
            throw new AuthAccountResetPasswordRequestedException(account.manualResetPasswordRequest.attemptToSignInMessage);
        }

        if (request.requireRole && account.role !== request.requireRole) {
            throw new AuthRequiredRoleMismatchException({
                role: request.requireRole,
            });
        }

        if (
            !(await this.authDeviceManager.hasAccessToken(account, {
                accessToken: request.jwt,
            }))
        ) {
            await this.bazaEventBus.publish(
                {
                    topic: AuthApiEvent.BazaAuthVerifyFailed,
                    payload: {
                        jwt: request.jwt,
                        errorCode: AuthErrorCodes.AuthInvalidJwt,
                    },
                },
                {
                    target: ApiEventSource.CurrentNode,
                },
            );

            throw new AuthInvalidJwtException();
        }

        if (jwtPayload.exp < new Date().getTime() / 1000) {
            await this.bazaEventBus.publish(
                {
                    topic: AuthApiEvent.BazaAuthVerifyFailed,
                    payload: {
                        jwt: request.jwt,
                        errorCode: AuthErrorCodes.AuthJwtExpired,
                    },
                },
                {
                    target: ApiEventSource.CurrentNode,
                },
            );

            throw new AuthJwtExpiredException();
        }

        await this.bazaEventBus.publish({
            topic: AuthApiEvent.BazaAuthVerifySuccess,
            payload: {
                jwt: request.jwt,
                accountId: account.id,
                accountEmail: account.email,
            },
        });

        return {
            account,
            jwt: request.jwt,
            jwtPayload,
        };
    }

    /**
     * Invalidate all tokens which are bind to the Account. It works like
     * "Sign out from all devices" feature. Account JWT tokens are managed
     * with AuthDeviceManagerService service. As far as Baza uses whitelist
     * strategy for JWT tokens, invalidated tokens will not be allowed to use
     * anymore even if it's not expired yet.
     *
     * Method produces AuthApiEvent.BazaAuthInvalidatedAllTokens event
     * on success
     *
     * @see AuthApiEvent
     * @see AuthApiEvent.BazaAuthInvalidatedAllTokens
     * @see AuthDeviceManagerService
     */
    async invalidateAllTokens(deviceTokens: BazaDeviceTokenActionSets): Promise<void> {
        const account = await this.authSession.getAccount();

        await this.commandBus.execute(
            new BazaDeviceTokenBulkCommand({
                accountId: account.id,
                actionSets: deviceTokens,
            }),
        );

        await this.authDeviceManager.detachAllDevices(account);

        await this.bazaEventBus.publish(
            {
                topic: AuthApiEvent.BazaAuthInvalidatedAllTokens,
                payload: {
                    accountId: account.id,
                    accountEmail: account.email,
                },
            },
            {
                target: ApiEventSource.CurrentNode,
            },
        );
    }

    /**
     * Invalidate (detach) given token from account. Account JWT tokens are managed
     * with AuthDeviceManagerService service. As far as Baza uses whitelist
     * strategy for JWT tokens, invalidated token will not be allowed to use
     * anymore even if it's not expired yet.
     *
     * Method produces AuthApiEvent.BazaAuthInvalidatedToken event on success.
     *
     * It's kinda technical method. For "Sign Out from Current Device" it's better
     * to invalidate tokens by refresh token
     *
     * @see AuthApiEvent
     * @see AuthApiEvent.BazaAuthInvalidatedToken
     * @see AuthService.invalidateByRefreshToken
     *
     * @param request
     */
    async invalidateToken(request: InvalidateTokenRequest, deviceTokens: BazaDeviceTokenActionSets): Promise<void> {
        const jwtPayload = this.jwtService.decode(request.jwt, {
            json: true,
        }) as JwtPayload;

        if (jwtPayload) {
            await this.commandBus.execute(
                new BazaDeviceTokenBulkCommand({
                    accountId: jwtPayload.accountId,
                    actionSets: deviceTokens,
                }),
            );
        }

        await this.authDeviceManager.detachAccessToken({
            accessToken: request.jwt,
        });

        await this.bazaEventBus.publish(
            {
                topic: AuthApiEvent.BazaAuthInvalidatedToken,
                payload: {
                    jwt: request.jwt,
                },
            },
            {
                target: ApiEventSource.CurrentNode,
            },
        );
    }

    /**
     * Invalidates refresh token and all access tokens generated for refresh token
     * before. It works like "Sign Out from Current Device" feature. Account JWT tokens are managed
     * with AuthDeviceManagerService service. As far as Baza uses whitelist
     * strategy for JWT tokens, invalidated tokens will not be allowed to use
     * anymore even if it's not expired yet.
     *
     * Method produces AuthApiEvent.BazaAuthInvalidatedRefreshToken event on success.
     *
     * @param request
     */
    async invalidateByRefreshToken(request: InvalidateByRefreshTokenRequest, deviceTokens: BazaDeviceTokenActionSets): Promise<void> {
        const jwtPayload = this.jwtService.decode(request.refreshToken, {
            json: true,
        }) as JwtPayload;

        if (jwtPayload) {
            await this.commandBus.execute(
                new BazaDeviceTokenBulkCommand({
                    accountId: jwtPayload.accountId,
                    actionSets: deviceTokens,
                }),
            );
        }

        await this.authDeviceManager.detachDevice({
            refreshToken: request.refreshToken,
        });

        await this.bazaEventBus.publish(
            {
                topic: AuthApiEvent.BazaAuthInvalidatedRefreshToken,
                payload: {
                    jwt: request.refreshToken,
                },
            },
            {
                target: ApiEventSource.CurrentNode,
            },
        );
    }
}
