import { Injectable } from '@nestjs/common';
import { AuthOptions, AuthService } from './auth.service';
import {
    AccountSettingsDto,
    ApiEventSource,
    Auth2FAErrorCodes,
    Auth2FARequest,
    Auth2FAVerificationMethod,
    Auth2FAVerifyRequest,
    AuthApiEvent,
    AuthApiEvents,
    AuthGoogle2FAQrCodeRequest,
    AuthResponse,
    getBazaClientName,
} from '@scaliolabs/baza-core-shared';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { authenticator } from 'otplib';
import { MailService } from '../../../../baza-mail/src';
import { BazaAuthApiModuleConfig } from '../baza-auth.config';
import { AccountEntity, BazaAccountRepository } from '../../../../baza-account/src';
import { Auth2FAInvalidToken } from '../exceptions/auth-2fa-invalid.token';
import { Auth2faUnknownVerificationMethodException } from '../exceptions/auth-2fa-unknown-verification-method.exception';
import { ApiEventBus } from '../../../../baza-event-bus/src';
import { Auth2faGoogleNotEnabledException } from '../exceptions/auth-2fa-google-verification-method.exception';
import { BazaCoreMail } from '../../../../../constants/baza-core.mail';

const MIN = 100000;
const MAX = 999999;

const cryptoStrongRandomNumber = (x, y) => {
    let crypto;

    try {
        crypto = require('crypto');

        return (x + ((y - x + 1) * crypto.getRandomValues(new Uint32Array(1))[0]) / 2 ** 32) | 0;
    } catch (err) {
        return Math.floor(Math.random() * (MAX - MIN + 1)) + MIN;
    }
};

const factoryRedisKey = (accountId: number) => `AUTH_CODE_${accountId}`;
const factoryVerificationCode = () => cryptoStrongRandomNumber(MIN, MAX).toString();

@Injectable()
export class Auth2FAService {
    constructor(
        private readonly moduleConfig: BazaAuthApiModuleConfig,
        private readonly authService: AuthService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly redis: RedisService,
        private readonly mail: MailService,
        private readonly bazaEventBus: ApiEventBus<AuthApiEvent, AuthApiEvents>,
    ) {}

    /**
     * Performs Auth Request with 2FA
     * @param request
     * @param options
     */
    async auth2FA(request: Auth2FARequest, options: AuthOptions): Promise<void> {
        const account = await this.authService.authVerifyCredentials(request, options);
        const code = factoryVerificationCode();

        switch (request.method) {
            default: {
                throw new Auth2faUnknownVerificationMethodException();
            }

            case Auth2FAVerificationMethod.Email: {
                await this.perform2FAWithEmail({
                    code,
                    account,
                });

                break;
            }

            case Auth2FAVerificationMethod.PhoneSMS: {
                await this.perform2FAPhoneSMS({
                    code,
                    account,
                });

                break;
            }

            case Auth2FAVerificationMethod.PhoneCall: {
                await this.perform2FAPhoneCall({
                    code,
                    account,
                });

                break;
            }

            case Auth2FAVerificationMethod.GoogleAuthenticator: {
                this.perform2FAGoogleAuthenticator({
                    account,
                });

                break;
            }

            case Auth2FAVerificationMethod.MicrosoftAuthenticator: {
                await this.perform2FAMicrosoftAuthenticator({
                    code,
                    account,
                });

                break;
            }
        }
    }

    /**
     * Verify code to authenticate user (2nd step of 2FA authentication)
     * @param request
     * @param options
     */
    async auth2FAVerify(
        request: Auth2FAVerifyRequest,
        options: AuthOptions & {
            withRefreshToken: boolean;
        },
    ): Promise<AuthResponse> {
        const account = await this.accountRepository.getActiveAccountWithEmail(request.email);

        switch (request.method) {
            default: {
                throw new Auth2faUnknownVerificationMethodException();
            }

            case Auth2FAVerificationMethod.Email:
            case Auth2FAVerificationMethod.PhoneCall:
            case Auth2FAVerificationMethod.PhoneSMS: {
                await this.verifyEmail2FA(request, account);

                break;
            }

            case Auth2FAVerificationMethod.GoogleAuthenticator: {
                this.verifyGoogleAuthenticator2FA(request, account);

                break;
            }
        }

        const authResponse = await this.authService.authAccount(account, options);

        await this.bazaEventBus.publish(
            {
                topic: AuthApiEvent.BazaAuth,
                payload: {
                    ip: options.ip,
                    userAgent: options.userAgent,
                    accountId: account.id,
                    jwt: authResponse.accessToken,
                    jwtPayload: authResponse.jwtPayload,
                    accountSettings: authResponse.accountSettings,
                },
            },
            {
                target: ApiEventSource.CurrentNode,
            },
        );

        return authResponse;
    }

    /**
     * Returns available 2FA methods for Account
     * @param account
     */
    async auth2FAMethods(account: AccountEntity): Promise<Array<Auth2FAVerificationMethod>> {
        return this.authService.auth2FAMethods(account);
    }

    /**
     * Generates code for Google Authenticator 2FA
     * @param request
     */
    async generateGoogle2FAQrCode(request: AuthGoogle2FAQrCodeRequest): Promise<string> {
        const account = await this.accountRepository.getActiveAccountWithEmail(request.email);

        if (account.twoFactorMethods.googleAuthenticator.enabled) {
            return authenticator.keyuri(account.email, getBazaClientName(), account.twoFactorMethods.googleAuthenticator.secretCode);
        }

        return undefined;
    }

    /**
     * Set Verification Code for Google Authenticator 2FA
     * @param request
     * @private
     */
    private async setVerificationCode(request: { account: AccountEntity; code: string }): Promise<void> {
        await this.redis
            .getClient()
            .set(factoryRedisKey(request.account.id), request.code, 'EX', this.moduleConfig.auth2FAVerificationTokenTTLSeconds);
    }

    /**
     * Validates 2FA code received from email
     * @param request
     * @private
     */
    private async perform2FAWithEmail(request: { account: AccountEntity; code: string }): Promise<void> {
        await this.setVerificationCode({
            account: request.account,
            code: request.code,
        });

        await this.mail.send({
            name: BazaCoreMail.BazaAuthTwoFactorAuth,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-auth.two-factor-auth.subject',
            variables: () => ({
                code: request.code,
                firstName: request.account.firstName,
                lastName: request.account.lastName,
                fullName: request.account.fullName,
                email: request.account.email,
                validity: this.moduleConfig.auth2FAVerificationTokenTTLSeconds / 60,
            }),
        });
    }

    /**
     * Performs 2FA using SMS method
     * Not Implemented
     * @param request
     * @private
     */
    private async perform2FAPhoneSMS(request: { account: AccountEntity; code: string }): Promise<void> {
        throw new Auth2faUnknownVerificationMethodException();
    }

    /**
     * Performs 2FA using Phone
     * Not Implemented
     * @param request
     * @private
     */
    private async perform2FAPhoneCall(request: { account: AccountEntity; code: string }): Promise<void> {
        throw new Auth2faUnknownVerificationMethodException();
    }

    /**
     * Performs 2FA using Google Authenticator
     * @param request
     * @private
     */
    private perform2FAGoogleAuthenticator(request: { account: AccountEntity }): void {
        if (!request.account.twoFactorMethods.googleAuthenticator?.enabled) {
            throw new Auth2faGoogleNotEnabledException();
        }
    }

    /**
     * Performs 2FA using Microsoft Authenticator
     * Not Implemented
     * @param request
     * @private
     */
    private async perform2FAMicrosoftAuthenticator(request: { account: AccountEntity; code: string }): Promise<void> {
        throw new Auth2faUnknownVerificationMethodException();
    }

    /**
     * Verifies code received with Email 2FA Verification
     * @param request Auth2FAVerifyRequest
     * @param account AccountEntity<AccountSettingsDto>
     */
    private async verifyEmail2FA(request: Auth2FAVerifyRequest, account: AccountEntity<AccountSettingsDto>) {
        const expect = await this.redis.getClient().get(factoryRedisKey(account.id));

        if (!expect || expect !== request.code.trim()) {
            throw new Auth2FAInvalidToken();
        }

        await this.redis.getClient().del(factoryRedisKey(account.id));
    }

    /**
     * Verifies code received with Google 2FA Verification
     * @param request Auth2FAVerifyRequest
     * @param account AccountEntity<AccountSettingsDto>
     */
    private verifyGoogleAuthenticator2FA(request: Auth2FAVerifyRequest, account: AccountEntity<AccountSettingsDto>): void {
        if (account.twoFactorMethods.googleAuthenticator.enabled) {
            const isValid = authenticator.verify({
                token: request.code,
                secret: account.twoFactorMethods.googleAuthenticator.secretCode,
            });

            if (!isValid) {
                throw new Auth2FAInvalidToken(Auth2FAErrorCodes.AuthGoogle2FAInvalidToken);
            }
        } else {
            throw new Auth2FAInvalidToken(Auth2FAErrorCodes.AuthGoogle2FANotEnabled);
        }
    }
}
