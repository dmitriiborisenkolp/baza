import { Injectable } from '@nestjs/common';
import { AuthJwtAttachAccessTokenRequest, AuthJwtAttachDeviceRequest, AuthJwtDetachAccessTokenRequest, AuthJwtDetachDeviceRequest, AuthJwtDeviceStrategies, AuthJwtDeviceStrategy, AuthJwtHasAccessTokenRequest, AuthJwtHasDeviceWithRefreshTokenRequest } from '../strategy/auth-jwt-device-strategy';
import { BazaAuthApiModuleConfig } from '../baza-auth.config';
import { AuthJwtPsqlStrategy } from '../strategy/psql/auth-jwt-psql.strategy';
import { AuthUnknownDeviceStrategyException } from '../exceptions/auth-unknown-device-strategy.exception';
import { AuthJwtRedisStrategy } from '../strategy/redis/auth-jwt-redis.strategy';
import { AccountSettingsDto } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../../../../baza-account/src';

/**
 * Device Manager service for Accounts
 *
 * Baza uses whitelist strategy for JWT Tokens. All access/refresh tokens
 * are being stored in PSQL or Redis (depends on used strategy) and Baza
 * sync access/refresh tokens with storage. If someone attempts to send valid
 * but custom generated JWT Token, Baza will not accept it and will not allow
 * user to access protected methods.
 *
 * Baza uses *Device* term as combination of refreshToken + accessToken[] generated
 * using given refresh token.
 *
 * @decorator `@Injectable()`
 *
 * @see AuthService
 */
@Injectable()
export class AuthDeviceManagerService<T = AccountSettingsDto> implements AuthJwtDeviceStrategy<T> {
    constructor(
        private readonly moduleConfig: BazaAuthApiModuleConfig,
        private readonly strategyPsql: AuthJwtPsqlStrategy,
        private readonly strategyRedis: AuthJwtRedisStrategy,
    ) {}

    /**
     * Returns current storage strategy
     *
     * Baza can use PSQL or Redis as storage for JWT tokens
     *
     * - Redis storage (`AuthJwtDeviceStrategies.Redis`) is more faster and secured storage
     * and uses EX key to automatically clean up tokens on expiration. Downside of
     * Redis storage is limited amount of active auth session can be maintained at same
     * time
     *
     * - Postgres strategy (`AuthJwtDeviceStrategies.Psql`) uses postgres as storage
     * and `BazaAuthDeviceEntity`/`BazaAuthAccessTokenEntity` TypeORM entities. It's
     * slower storage but it allows to have as much as possible auth session at the same
     * time.
     *
     * Most of projects should go with Redis strategy. If you have 1000+ active
     * auth sessions in the same time, you should go with Psql strategy.
     *
     * Both strategies automatically clean up expired tokens.
     *
     * @see AuthJwtDeviceStrategies
     */
    get strategy(): AuthJwtDeviceStrategy<T> {
        switch (this.moduleConfig.authDeviceStrategy) {
            default: {
                throw new AuthUnknownDeviceStrategyException();
            }

            case AuthJwtDeviceStrategies.Psql: {
                return this.strategyPsql as any;
            }

            case AuthJwtDeviceStrategies.Redis: {
                return this.strategyRedis as any;
            }
        }
    }

    /**
     * Attach device (JWT Tokens) to the account

     * @param account
     * @param request
     */
    async attachDevice(account: AccountEntity<T>, request: AuthJwtAttachDeviceRequest): Promise<void> {
        return this.strategy.attachDevice(account, request);
    }

    /**
     * Detach device (JWT Tokens) from the account.
     *
     * @param request
     */
    async detachDevice(request: AuthJwtDetachDeviceRequest): Promise<void> {
        return this.strategy.detachDevice(request);
    }

    /**
     * Attach Access Token to account
     *
     * @param account
     * @param request
     */
    async attachAccessToken(account: AccountEntity<T>, request: AuthJwtAttachAccessTokenRequest): Promise<void> {
        return this.strategy.attachAccessToken(account, request);
    }

    /**
     * Detach Access Token from account
     * @param request
     */
    async detachAccessToken(request: AuthJwtDetachAccessTokenRequest): Promise<void> {
        return this.strategy.detachAccessToken(request);
    }

    /**
     * Detach all devices from Account
     *
     * @param account
     */
    async detachAllDevices(account: AccountEntity<T>): Promise<void> {
        return this.strategy.detachAllDevices(account);
    }

    /**
     * Returns true if there is device with given refresh token for account
     *
     * @param account
     * @param request
     */
    async hasDeviceWithRefreshToken(account: AccountEntity<T>, request: AuthJwtHasDeviceWithRefreshTokenRequest): Promise<boolean> {
        return this.strategy.hasDeviceWithRefreshToken(account, request);
    }

    /**
     * Returns true if given accessToken exists for account
     *
     * @param account
     * @param request
     */
    async hasAccessToken(account: AccountEntity<T>, request: AuthJwtHasAccessTokenRequest): Promise<boolean> {
        return this.strategy.hasAccessToken(account, request);
    }

    /**
     * Clean up all expired tokens.
     *
     * Redis strategy automatically clean up expired tokens with Redis EX feature
     * Psql strategy does not requires manual calls on the method, but Baza
     * manually and automatically clean up expired tokens from DB
     */
    async cleanUpExpiredTokens(): Promise<void> {
        return this.strategy.cleanUpExpiredTokens();
    }
}
