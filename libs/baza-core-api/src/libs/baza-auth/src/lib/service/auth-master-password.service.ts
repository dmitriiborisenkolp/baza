import { Injectable } from '@nestjs/common';
import { generateRandomHexString, isEmpty } from '@scaliolabs/baza-core-shared';
import { AuthMasterPasswordIsNotEnabledException } from '../exceptions/auth-master-password-is-not-enabled.exception';
import { BazaAuthApiModuleConfig } from '../baza-auth.config';
import { RedisService } from '@liaoliaots/nestjs-redis';

const MASTER_PASSWORD_TTL = 60 * 60; // 1 HOUR
const MASTER_PASSWORD_REDIS_KEY = 'BAZA_AUTH_MASTER_PASSWORD_X4AQD'; // we're adding a random string here just to be sure that someone with access to Redis will not try to guess key
const MASTER_PASSWORD_LENGTH = 32;

/**
 * Auth Master Password service
 *
 * Auth Master Password features allow to sign in to any existing account
 * using temporary generates & shortly expires (~5 minutes) unique password.
 *
 * Auth Master Password is toggleable feature .It can be used for any deployed
 * instance to debug some user-specific issues.
 *
 * Master Password is published to all nodes using Kafka. Make sure that for
 * Production instances Kafka is secured or placed under private network.
 *
 * @decorator `@Injectable()`
 */
@Injectable()
export class AuthMasterPasswordService {
    constructor(private readonly moduleConfig: BazaAuthApiModuleConfig, private readonly redis: RedisService) {}

    /**
     * Returns true if Auth Master Password feature is enabled
     */
    get isEnabled(): Promise<boolean> {
        return this.redis
            .getClient()
            .get(MASTER_PASSWORD_REDIS_KEY)
            .then((result) => !isEmpty(result));
    }

    /**
     * Returns current Master Password. Master Password has short
     * expiration time (~5 minutes).
     */
    get masterPassword(): Promise<string> {
        return this.redis.getClient().get(MASTER_PASSWORD_REDIS_KEY);
    }

    /**
     * Returns current Master Password. Method will throws exceptions if
     * Master Password feature is disabled completely on Module level or
     * Master Password feature is not enabled in CMS
     */
    async current(): Promise<string> {
        if (!this.isModuleFeatureEnabled || !(await this.isEnabled)) {
            throw new AuthMasterPasswordIsNotEnabledException();
        }

        const masterPassword = await this.masterPassword;

        if (isEmpty(masterPassword)) {
            throw new AuthMasterPasswordIsNotEnabledException();
        }

        return masterPassword;
    }

    /**
     * Enables Master Password feature.
     *
     * Once enabled, master password will be disabled automatically in one hour.
     *
     * Method will throw and error if Master Password feature is disabled on
     * Module level.
     */
    async enable(): Promise<void> {
        if (!this.isModuleFeatureEnabled) {
            throw new AuthMasterPasswordIsNotEnabledException();
        }

        await this.refresh();
    }

    /**
     * Disabled Master Password feature.
     *
     * Method will throw and error if Master Password feature is disabled on
     * Module level or was not previously enabled in CMS.
     */
    async disable(): Promise<void> {
        if (!this.isModuleFeatureEnabled || !this.isEnabled) {
            throw new AuthMasterPasswordIsNotEnabledException();
        }

        await this.redis.getClient().del(MASTER_PASSWORD_REDIS_KEY);
    }

    /**
     * Generates new Master Password
     */
    async refresh(): Promise<void> {
        if (!this.isModuleFeatureEnabled || !this.isEnabled) {
            throw new AuthMasterPasswordIsNotEnabledException();
        }

        await this.redis
            .getClient()
            .set(MASTER_PASSWORD_REDIS_KEY, generateRandomHexString(MASTER_PASSWORD_LENGTH), 'EX', MASTER_PASSWORD_TTL);
    }

    /**
     * Returns true if Master Password feature is enabled on Module level
     * @private
     */
    private get isModuleFeatureEnabled(): boolean {
        return this.moduleConfig.withMasterPasswordFeature;
    }
}
