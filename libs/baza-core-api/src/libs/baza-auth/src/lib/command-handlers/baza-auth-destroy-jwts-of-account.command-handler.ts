import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AuthDeviceManagerService } from '../service/auth-device-manager.service';
import { BazaAuthDestroyJwtsOfAccountCommand } from '@scaliolabs/baza-core-shared';
import { BazaAccountRepository } from '../../../../baza-account/src';
import { cqrsCommand } from '../../../../baza-common/src';

/**
 * CommandHandler which will destroys all active auth session of given account.
 * Auth sessions will be destroyed on ACL changes, deactivating account,
 * resetting password, assigning / revoking admin role or setting SystemRoot
 * access role to account
 *
 * @decorator `@CommandHandler(BazaAuthDestroyJwtsOfAccountCommand)`
 */
@CommandHandler(BazaAuthDestroyJwtsOfAccountCommand)
export class BazaAuthDestroyJwtsOfAccountCommandHandler implements ICommandHandler<BazaAuthDestroyJwtsOfAccountCommand> {
    constructor(private readonly accountRepository: BazaAccountRepository, private readonly deviceManager: AuthDeviceManagerService) {}

    async execute(command: BazaAuthDestroyJwtsOfAccountCommand): Promise<void> {
        return cqrsCommand<void>(BazaAuthDestroyJwtsOfAccountCommandHandler.name, async () => {
            const account = await this.accountRepository.findActiveAccountWithId(command.request.accountId);

            if (account) {
                await this.deviceManager.detachAllDevices(account);
            }
        });
    }
}
