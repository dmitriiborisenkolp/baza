import 'reflect-metadata';
import { bazaAuthRegistry } from './baza-auth.registry';
import { RegistryNodeBoolean, RegistryNodeString, RegistrySchema } from '@scaliolabs/baza-core-shared';

/**
 * We should be 100% sure that Master Password will not be provided to non-administrator users
 */
describe('@scaliolabs/resources/baza-auth.registry.spec.ts', () => {
    const resource = {
        ...bazaAuthRegistry,
    };

    const masterPasswordKey = (resource.bazaCoreAuth as RegistrySchema).masterPassword as RegistryNodeString;
    const masterPasswordEnabledKey = (resource.bazaCoreAuth as RegistrySchema).masterPasswordEnabled as RegistryNodeBoolean;

    it('must be not available at public scope', () => {
        expect(masterPasswordKey.public).toBeFalsy();
        expect(masterPasswordEnabledKey.public).toBeFalsy();
    });

    it('must be not visible at registry CMS', () => {
        expect(masterPasswordKey.hiddenFromList).toBeTruthy();
        expect(masterPasswordEnabledKey.hiddenFromList).toBeTruthy();
    });

    it('should be disabled by default', () => {
        expect(masterPasswordEnabledKey.defaults).toBeFalsy();
    });
});
