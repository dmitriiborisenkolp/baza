import { RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';

/**
 * Registry config which is required for baza-auth subpackage.
 */
export const bazaAuthRegistry: RegistrySchema = {
    bazaCoreAuth: {
        masterPassword: {
            name: 'Master Password',
            type: RegistryType.String,
            hiddenFromList: true,
            public: false,
            defaults: undefined,
        },
        masterPasswordEnabled: {
            name: 'Master Password',
            type: RegistryType.Boolean,
            hiddenFromList: true,
            public: false,
            defaults: false,
        },
        whitelistAccountAccess: {
            name: 'Enable Only Whitelisted Account Access for Sign-up',
            type: RegistryType.Boolean,
            hiddenFromList: false,
            public: false,
            defaults: false,
        },
        whitelistAccountAccessMessage: {
            name: 'Non-Whitelisted User Message',
            type: RegistryType.Text,
            hiddenFromList: false,
            public: false,
            defaults: 'We are currently only accepting users who signed up for early access. Please stay tuned for news on when we will be open to the general public.'
        }
    }
}
