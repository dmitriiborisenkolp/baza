import { BazaAppException } from '../../../../baza-exceptions/src';
import { Auth2FAErrorCodes, auth2FAErrorCodesI18nEn } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class Auth2faGoogleNotEnabledException extends BazaAppException {
    constructor() {
        super(
            Auth2FAErrorCodes.AuthGoogle2FANotEnabled,
            auth2FAErrorCodesI18nEn[Auth2FAErrorCodes.AuthGoogle2FANotEnabled],
            HttpStatus.BAD_REQUEST,
        );
    }
}
