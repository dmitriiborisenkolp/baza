import { AuthMasterPasswordErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

/**
 * Exception with `AuthMasterPasswordErrorCodes.BazaAuthMasterPasswordIsNotEnabled` error code
 */
export class AuthMasterPasswordIsNotEnabledException extends BazaAppException {
    constructor() {
        super(AuthMasterPasswordErrorCodes.BazaAuthMasterPasswordIsNotEnabled, 'Master Password feature is not enabled', HttpStatus.BAD_REQUEST);
    }
}
