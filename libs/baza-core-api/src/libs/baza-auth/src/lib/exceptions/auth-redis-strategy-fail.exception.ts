import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

/**
 * Exception with `AuthErrorCodes.AuthRedisStrategyFail` error code
 * Requires custom error message
 *
 * @param errorMessage Message
 */
export class AuthRedisStrategyFailException extends BazaAppException {
    constructor(
        errorMessage: string,
    ) {
        super(AuthErrorCodes.AuthRedisStrategyFail, errorMessage, HttpStatus.UNAUTHORIZED);
    }
}
