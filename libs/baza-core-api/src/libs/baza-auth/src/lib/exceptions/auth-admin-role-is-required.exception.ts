import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Admin role is required';

/**
 * Exception with `AuthErrorCodes.AuthAdminRoleIsRequired` error code
 */
export class AuthAdminRoleIsRequiredException extends BazaAppException {
  constructor() {
    super(AuthErrorCodes.AuthAdminRoleIsRequired, ERR_MESSAGE, HttpStatus.FORBIDDEN);
  }
}
