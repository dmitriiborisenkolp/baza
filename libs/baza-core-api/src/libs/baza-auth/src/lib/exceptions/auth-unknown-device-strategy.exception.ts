import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Unknown device strategy used for JWT tokens';

/**
 * Exception with `AuthErrorCodes.AuthUnknownDeviceStrategy` error code
 */
export class AuthUnknownDeviceStrategyException extends BazaAppException {
    constructor() {
        super(AuthErrorCodes.AuthUnknownDeviceStrategy, ERR_MESSAGE, HttpStatus.FORBIDDEN);
    }
}
