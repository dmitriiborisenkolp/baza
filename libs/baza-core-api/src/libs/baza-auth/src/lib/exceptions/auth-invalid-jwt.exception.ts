import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Invalid JWT';

/**
 * Exception with `AuthErrorCodes.AuthInvalidJwt` error code
 */
export class AuthInvalidJwtException extends BazaAppException {
  constructor() {
    super(AuthErrorCodes.AuthInvalidJwt, ERR_MESSAGE, HttpStatus.UNAUTHORIZED);
  }
}
