import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Auth session is not available';

/**
 * Exception for `AuthErrorCodes.AuthSessionIsNotAvailable` error code.
 */
export class AuthSessionIsNotAvailableException extends BazaAppException {
  constructor() {
    super(AuthErrorCodes.AuthSessionIsNotAvailable, ERR_MESSAGE, HttpStatus.UNAUTHORIZED);
  }
}
