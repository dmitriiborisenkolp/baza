import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Administrators are not allowed to sign in to web applications';

/**
 * Exception with `AuthErrorCodes.AuthAdminIsNotAllowedToSignInWebApp` error code
 */
export class AuthAdminIsNotAllowedToSignInWebAppException extends BazaAppException {
  constructor() {
    super(AuthErrorCodes.AuthAdminIsNotAllowedToSignInWebApp, ERR_MESSAGE, HttpStatus.FORBIDDEN);
  }
}
