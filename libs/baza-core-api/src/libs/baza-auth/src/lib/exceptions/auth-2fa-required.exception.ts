import { BazaAppException } from '../../../../baza-exceptions/src';
import { Auth2FAErrorCodes, auth2FAErrorCodesI18nEn } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class Auth2FARequiredException extends BazaAppException {
    constructor() {
        super(
            Auth2FAErrorCodes.Auth2FARequired,
            auth2FAErrorCodesI18nEn[Auth2FAErrorCodes.Auth2FARequired],
            HttpStatus.UNAUTHORIZED,
        );
    }
}
