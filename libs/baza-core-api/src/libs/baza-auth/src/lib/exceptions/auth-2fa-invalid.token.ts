import { BazaAppException } from '../../../../baza-exceptions/src';
import { Auth2FAErrorCodes, auth2FAErrorCodesI18nEn } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class Auth2FAInvalidToken extends BazaAppException {
    constructor(errorCode?: Auth2FAErrorCodes) {
        const code = errorCode ? Auth2FAErrorCodes[errorCode] : Auth2FAErrorCodes.Auth2FAInvalidToken;
        super(
            code,
            auth2FAErrorCodesI18nEn[code],
            HttpStatus.BAD_REQUEST,
        );
    }
}
