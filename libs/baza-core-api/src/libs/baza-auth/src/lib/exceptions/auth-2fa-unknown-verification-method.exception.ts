import { BazaAppException } from '../../../../baza-exceptions/src';
import { Auth2FAErrorCodes, auth2FAErrorCodesI18nEn } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class Auth2faUnknownVerificationMethodException extends BazaAppException {
    constructor() {
        super(
            Auth2FAErrorCodes.Auth2FAUnknownVerificationMethod,
            auth2FAErrorCodesI18nEn[Auth2FAErrorCodes.Auth2FAUnknownVerificationMethod],
            HttpStatus.BAD_REQUEST,
        );
    }
}
