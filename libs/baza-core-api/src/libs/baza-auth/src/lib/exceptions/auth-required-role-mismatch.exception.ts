import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Auth requires role "{{ role }}" but account has no given role';

/**
 * Exception with `AuthErrorCodes.AuthRequiredRoleMismatchException` error code
 * Requires args object with `role: AccountRole` property
 *
 * @param args Arguments for error message
 */
export class AuthRequiredRoleMismatchException extends BazaAppException {
  constructor(args: {
      role: AccountRole;
  }) {
    super(AuthErrorCodes.AuthRequiredRoleMismatch, ERR_MESSAGE, HttpStatus.FORBIDDEN, args);
  }
}
