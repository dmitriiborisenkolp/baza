import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Account email is not verified';

/**
 * Exception with `AuthErrorCodes.AuthEmailIsNotVerified` error code
 */
export class AuthAccountEmailIsNotVerifiedException extends BazaAppException {
  constructor() {
    super(AuthErrorCodes.AuthEmailIsNotVerified, ERR_MESSAGE, HttpStatus.FORBIDDEN);
  }
}
