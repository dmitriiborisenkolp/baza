import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Invalid credentials';

/**
 * Exception with `AuthErrorCodes.AuthInvalidCredentials` error code
 */
export class AuthInvalidCredentialsException extends BazaAppException {
  constructor() {
    super(AuthErrorCodes.AuthInvalidCredentials, ERR_MESSAGE, HttpStatus.UNAUTHORIZED);
  }
}
