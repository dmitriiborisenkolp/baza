import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Due of security reasons we requests to reset password for your account. Please check your mailbox!';

/**
 * Exception with `AuthErrorCodes.AuthAccountResetPasswordRequested` error code
 * Default error message: Due of security reasons we requests to reset password for your account. Please check your mailbox!
 *
 * @param message Custom message.
 */
export class AuthAccountResetPasswordRequestedException extends BazaAppException {
    constructor(message: string = ERR_MESSAGE) {
        super(AuthErrorCodes.AuthAccountResetPasswordRequested, message, HttpStatus.UNAUTHORIZED);
    }
}
