import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'JWT token is expired';

/**
 * Exception with `AuthErrorCodes.AuthJwtExpired` error code
 */
export class AuthJwtExpiredException extends BazaAppException {
  constructor() {
    super(AuthErrorCodes.AuthJwtExpired, ERR_MESSAGE, HttpStatus.UNAUTHORIZED);
  }
}
