import { Request } from 'express';
import { AccountEntity } from '../../../../baza-account/src';

export interface RequestWithUser extends Request {
    user: AccountEntity;
}
