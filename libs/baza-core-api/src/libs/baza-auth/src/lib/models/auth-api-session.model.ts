import { JwtPayload } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../../../../baza-account/src/lib/entities/account.entity';

/**
 * Auth Session object which is used in AuthSessionService.
 * Should never be used in Angular applications, because it depends on AccountEntity
 * TypeOrm object
 */
export class AuthApiSession {
    /**
     * JWT Token
     * @see http://jwt.io
     */
    jwt: string;

    /**
     * JWT Token Payload
     */
    jwtPayload: JwtPayload;

    /**
     * Account Entity
     */
    account: AccountEntity;
}
