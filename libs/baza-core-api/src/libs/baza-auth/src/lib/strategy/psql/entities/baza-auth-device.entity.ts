import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Application } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../../../../../../baza-account/src';

@Entity()
export class BazaAuthDeviceEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        default: new Date(),
    })
    createdAt: Date;

    @Column({
        default: new Date(),
    })
    expiresAt: Date;

    @ManyToOne(() => AccountEntity, {
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity;

    @Column({
        type: 'varchar',
        length: 12,
    })
    application: Application;

    @Column({
        nullable: true,
    })
    applicationId: string;

    @Column()
    refreshToken: string;
}
