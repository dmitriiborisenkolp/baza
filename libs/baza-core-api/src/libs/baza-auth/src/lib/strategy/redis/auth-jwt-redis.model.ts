import { Application } from '@scaliolabs/baza-core-shared';

export interface AuthJwtRedisDevice {
    accountId: number;
    refreshToken: string;
    accessTokens: Array<string>;
    application: Application;
    applicationId?: string;
}
