import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BazaAuthAccessTokenEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        default: new Date(),
    })
    createdAt: Date;

    @Column({
        default: new Date(),
    })
    expiresAt: Date;

    @Column()
    accessToken: string;

    @Column()
    refreshToken: string;
}
