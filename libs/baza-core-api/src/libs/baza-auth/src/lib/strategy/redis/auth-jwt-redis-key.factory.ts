import { Injectable } from '@nestjs/common';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const sha1 = require('sha1');

@Injectable()
export class AuthJwtRedisKeyFactory {
    factoryKeyDevice(refreshToken: string): string {
        return `BAZA_AUTH_DV_${sha1(refreshToken)}`;
    }

    factoryKeyAccessToken(accessToken: string): string {
        return `BAZA_AUTH_AT_${sha1(accessToken)}`;
    }

    factoryKeyAccountDevices(accountId: number): string {
        return `BAZA_AUTH_AD_${accountId}`;
    }
}
