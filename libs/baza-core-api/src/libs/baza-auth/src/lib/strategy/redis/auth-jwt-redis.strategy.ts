import { Injectable } from '@nestjs/common';
import {
    AuthJwtAttachAccessTokenRequest,
    AuthJwtAttachDeviceRequest,
    AuthJwtDetachAccessTokenRequest,
    AuthJwtDetachDeviceRequest,
    AuthJwtDeviceStrategy,
    AuthJwtHasAccessTokenRequest,
    AuthJwtHasDeviceWithRefreshTokenRequest,
} from '../auth-jwt-device-strategy';
import { AccountSettingsDto } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../../../../../baza-account/src';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { AuthJwtRedisDevice } from './auth-jwt-redis.model';
import { AuthJwtRedisKeyFactory } from './auth-jwt-redis-key.factory';
import * as jwt from 'jsonwebtoken';
import { AuthRedisStrategyFailException } from '../../exceptions/auth-redis-strategy-fail.exception';
import { JwtPayload } from 'jsonwebtoken';

const filterForExpiredTokens = (tokens: Array<string>) => {
    return tokens.filter((token) => {
        const decoded = jwt.decode(token, {
            complete: true,
            json: true,
        });

        return !!decoded && new Date().getTime() < new Date((decoded.payload as JwtPayload).exp * 1000).getTime();
    });
};

const calculateExpireSeconds = (token: string) => {
    const decoded = jwt.decode(token, {
        json: true,
        complete: true,
    });

    return Math.floor((new Date((decoded.payload as JwtPayload).exp * 1000).getTime() - new Date().getTime()) / 1000);
};

@Injectable()
export class AuthJwtRedisStrategy<T = AccountSettingsDto> implements AuthJwtDeviceStrategy<T> {
    constructor(private readonly redis: RedisService, private readonly keyFactory: AuthJwtRedisKeyFactory) {}

    async attachDevice(account: AccountEntity<T>, request: AuthJwtAttachDeviceRequest): Promise<void> {
        const device: AuthJwtRedisDevice = {
            accountId: account.id,
            application: request.application,
            applicationId: request.applicationId,
            refreshToken: request.refreshToken,
            accessTokens: [],
        };

        const newRefreshTokens = filterForExpiredTokens([...(await this.getAccountDevices(account.id)), request.refreshToken]);

        await this.redis
            .getClient()
            .set(
                this.keyFactory.factoryKeyDevice(request.refreshToken),
                JSON.stringify(device),
                'EX',
                calculateExpireSeconds(request.refreshToken),
            );
        await this.redis.getClient().set(this.keyFactory.factoryKeyAccountDevices(account.id), JSON.stringify(newRefreshTokens));
    }

    async attachAccessToken(account: AccountEntity<T>, request: AuthJwtAttachAccessTokenRequest): Promise<void> {
        const deviceRaw = await this.redis.getClient().get(this.keyFactory.factoryKeyDevice(request.refreshToken));

        if (!deviceRaw) {
            throw new AuthRedisStrategyFailException('Failed to attach new access token to device; No device registered in Redis');
        }

        const device: AuthJwtRedisDevice = JSON.parse(deviceRaw);

        device.accessTokens = filterForExpiredTokens([...device.accessTokens, request.accessToken]);

        await this.redis
            .getClient()
            .set(
                this.keyFactory.factoryKeyDevice(request.refreshToken),
                JSON.stringify(device),
                'EX',
                calculateExpireSeconds(request.refreshToken),
            );
        await this.redis
            .getClient()
            .set(
                this.keyFactory.factoryKeyAccessToken(request.accessToken),
                request.accessToken,
                'EX',
                calculateExpireSeconds(request.accessToken),
            );
    }

    async detachDevice(request: AuthJwtDetachDeviceRequest): Promise<void> {
        const deviceRaw = await this.redis.getClient().get(this.keyFactory.factoryKeyDevice(request.refreshToken));

        if (deviceRaw) {
            const device: AuthJwtRedisDevice = JSON.parse(deviceRaw);
            const accountDevices = filterForExpiredTokens(
                (await this.getAccountDevices(device.accountId)).filter((rt) => rt !== request.refreshToken),
            );

            await this.redis.getClient().set(this.keyFactory.factoryKeyAccountDevices(device.accountId), JSON.stringify(accountDevices));
        }

        await this.redis.getClient().del(this.keyFactory.factoryKeyDevice(request.refreshToken));
    }

    async detachAccessToken(request: AuthJwtDetachAccessTokenRequest): Promise<void> {
        await this.redis.getClient().del(this.keyFactory.factoryKeyAccessToken(request.accessToken));
    }

    async detachAllDevices(account: AccountEntity<T>): Promise<void> {
        const refreshTokens = await this.getAccountDevices(account.id);

        for (const refreshToken of refreshTokens) {
            const deviceRaw = await this.redis.getClient().get(this.keyFactory.factoryKeyDevice(refreshToken));

            if (deviceRaw) {
                const device: AuthJwtRedisDevice = JSON.parse(deviceRaw);

                for (const accessToken of device.accessTokens) {
                    await this.redis.getClient().del(this.keyFactory.factoryKeyAccessToken(accessToken));
                }

                await this.redis.getClient().del(this.keyFactory.factoryKeyDevice(refreshToken));
            }
        }

        await this.redis.getClient().del(this.keyFactory.factoryKeyAccountDevices(account.id));
    }

    async hasDeviceWithRefreshToken(account: AccountEntity<T>, request: AuthJwtHasDeviceWithRefreshTokenRequest): Promise<boolean> {
        return !!(await this.redis.getClient().get(this.keyFactory.factoryKeyDevice(request.refreshToken)));
    }

    async hasAccessToken(account: AccountEntity<T>, request: AuthJwtHasAccessTokenRequest): Promise<boolean> {
        return !!(await this.redis.getClient().get(this.keyFactory.factoryKeyAccessToken(request.accessToken)));
    }

    async cleanUpExpiredTokens(): Promise<void> {
        // Nothing to do here; Redis will clean up automatically.
    }

    private async getAccountDevices(accountId: number): Promise<Array<string>> {
        const value = await this.redis.getClient().get(this.keyFactory.factoryKeyAccountDevices(accountId));

        if (!value) {
            return [];
        } else {
            return JSON.parse(value);
        }
    }
}
