import { Injectable } from '@nestjs/common';
import { Connection, LessThanOrEqual, Repository } from 'typeorm';
import { BazaAuthDeviceEntity } from '../entities/baza-auth-device.entity';
import { AccountEntity } from '../../../../../../baza-account/src';

export const BAZA_AUTH_DEVICE_RELATIONS = [
    'account',
];

@Injectable()
export class BazaAuthDeviceRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaAuthDeviceEntity> {
        return this.connection.getRepository(BazaAuthDeviceEntity);
    }

    async save(entity: BazaAuthDeviceEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async remove(entity: BazaAuthDeviceEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async findDeviceByRefreshToken(refreshToken: string): Promise<BazaAuthDeviceEntity | undefined> {
        return this.repository.findOne({
            where: [{
                refreshToken,
            }],
            relations: BAZA_AUTH_DEVICE_RELATIONS,
        });
    }

    async removeDevicesByRefreshToken(refreshToken: string): Promise<void> {
        await this.repository.delete({
            refreshToken,
        });
    }

    async removeDevicesByAccount(account: AccountEntity): Promise<void> {
        await this.repository.delete({
            account,
        });
    }

    async findDevicesByAccount(account: AccountEntity): Promise<Array<BazaAuthDeviceEntity>> {
        return this.repository.find({
            relations: BAZA_AUTH_DEVICE_RELATIONS,
            where: [{
                account,
            }],
        });
    }

    async hasDeviceWithRefreshToken(refreshToken: string): Promise<boolean> {
        return !! (await this.findDeviceByRefreshToken(refreshToken));
    }

    async cleanUpExpiredDevices(): Promise<void> {
        await this.repository.delete({
            expiresAt: LessThanOrEqual(new Date()),
        });
    }
}
