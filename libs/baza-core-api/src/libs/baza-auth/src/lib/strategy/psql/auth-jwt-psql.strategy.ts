import { Injectable } from '@nestjs/common';
import {
    AuthJwtAttachAccessTokenRequest,
    AuthJwtAttachDeviceRequest,
    AuthJwtDetachAccessTokenRequest,
    AuthJwtDetachDeviceRequest,
    AuthJwtDeviceStrategy,
    AuthJwtHasAccessTokenRequest,
    AuthJwtHasDeviceWithRefreshTokenRequest,
} from '../auth-jwt-device-strategy';
import { AccountEntity } from '../../../../../baza-account/src';
import { BazaAuthDeviceRepository } from './repositories/baza-auth-device.repository';
import { BazaAuthDeviceEntity } from './entities/baza-auth-device.entity';
import { BazaAuthAccessTokenRepository } from './repositories/baza-auth-access-token.repository';
import { BazaAuthAccessTokenEntity } from './entities/baza-auth-access-token.entity';
import * as jwt from 'jsonwebtoken';
import { JwtPayload } from 'jsonwebtoken';

@Injectable()
export class AuthJwtPsqlStrategy implements AuthJwtDeviceStrategy {
    constructor(
        private readonly devicesRepository: BazaAuthDeviceRepository,
        private readonly accessTokensRepository: BazaAuthAccessTokenRepository,
    ) {}

    async attachDevice(account: AccountEntity, request: AuthJwtAttachDeviceRequest): Promise<void> {
        const newDevice = new BazaAuthDeviceEntity();

        const decoded = jwt.decode(request.refreshToken, {
            json: true,
            complete: true,
        });

        newDevice.account = account;
        newDevice.createdAt = new Date();
        newDevice.expiresAt = new Date((decoded.payload as JwtPayload).exp * 1000);
        newDevice.application = request.application;
        newDevice.applicationId = request.applicationId;
        newDevice.refreshToken = request.refreshToken;

        await this.devicesRepository.save(newDevice);
    }

    async attachAccessToken(account: AccountEntity, request: AuthJwtAttachAccessTokenRequest): Promise<void> {
        const newToken = new BazaAuthAccessTokenEntity();

        const decoded = jwt.decode(request.refreshToken, {
            json: true,
            complete: true,
        });

        newToken.createdAt = new Date();
        newToken.expiresAt = new Date((decoded.payload as JwtPayload).exp * 1000);
        newToken.accessToken = request.accessToken;
        newToken.refreshToken = request.refreshToken;

        await this.accessTokensRepository.save(newToken);
    }

    async detachDevice(request: AuthJwtDetachDeviceRequest): Promise<void> {
        await this.devicesRepository.removeDevicesByRefreshToken(request.refreshToken);
    }

    async detachAccessToken(request: AuthJwtDetachAccessTokenRequest): Promise<void> {
        await this.accessTokensRepository.removeByAccessToken(request.accessToken);
    }

    async detachAllDevices(account: AccountEntity): Promise<void> {
        const devices = await this.devicesRepository.findDevicesByAccount(account);

        for (const device of devices) {
            await this.accessTokensRepository.removeAccessTokensByRefreshToken(device.refreshToken);
        }

        await this.devicesRepository.removeDevicesByAccount(account);
    }

    async hasDeviceWithRefreshToken(account: AccountEntity, request: AuthJwtHasDeviceWithRefreshTokenRequest): Promise<boolean> {
        return this.devicesRepository.hasDeviceWithRefreshToken(request.refreshToken);
    }

    async hasAccessToken(account: AccountEntity, request: AuthJwtHasAccessTokenRequest): Promise<boolean> {
        return this.accessTokensRepository.hasAccessToken(request.accessToken);
    }

    async cleanUpExpiredTokens(): Promise<void> {
        await this.devicesRepository.cleanUpExpiredDevices();
        await this.accessTokensRepository.cleanUpExpiredDevices();
    }
}
