import { AccountSettingsDto, Application } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../../../../baza-account/src';

export enum AuthJwtDeviceStrategies {
    Psql = 'psql',
    Redis = 'redis',
}

export interface AuthJwtAttachDeviceRequest {
    application: Application;
    applicationId?: string;
    refreshToken: string;
}

export interface AuthJwtDetachDeviceRequest {
    refreshToken: string;
}

export interface AuthJwtAttachAccessTokenRequest {
    accessToken: string;
    refreshToken: string;
}

export interface AuthJwtDetachAccessTokenRequest {
    accessToken: string;
}

export interface AuthJwtHasDeviceWithRefreshTokenRequest {
    refreshToken: string;
}

export interface AuthJwtHasAccessTokenRequest{
    accessToken: string;
}

export interface AuthJwtDeviceStrategy<T = AccountSettingsDto> {
    attachDevice(account: AccountEntity<T>, request: AuthJwtAttachDeviceRequest): Promise<void>;
    attachAccessToken(account: AccountEntity<T>, request: AuthJwtAttachAccessTokenRequest): Promise<void>;
    detachDevice(request: AuthJwtDetachDeviceRequest): Promise<void>;
    detachAccessToken(request: AuthJwtDetachAccessTokenRequest): Promise<void>;
    detachAllDevices(account: AccountEntity<T>): Promise<void>;
    hasDeviceWithRefreshToken(account: AccountEntity<T>, request: AuthJwtHasDeviceWithRefreshTokenRequest): Promise<boolean>;
    hasAccessToken(account: AccountEntity<T>, request: AuthJwtHasAccessTokenRequest): Promise<boolean>;
    cleanUpExpiredTokens(): Promise<void>;
}
