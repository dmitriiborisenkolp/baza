import { Injectable } from '@nestjs/common';
import { Connection, LessThanOrEqual, Repository } from 'typeorm';
import { BazaAuthAccessTokenEntity } from '../entities/baza-auth-access-token.entity';

@Injectable()
export class BazaAuthAccessTokenRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaAuthAccessTokenEntity> {
        return this.connection.getRepository(BazaAuthAccessTokenEntity);
    }

    async save(entity: BazaAuthAccessTokenEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async remove(entity: BazaAuthAccessTokenEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async removeAccessTokensByRefreshToken(refreshToken: string): Promise<void> {
        await this.repository.delete({
            refreshToken,
        });
    }

    async removeByAccessToken(accessToken: string): Promise<void> {
        await this.repository.delete({
            accessToken,
        });
    }

    async findByAccessToken(accessToken: string): Promise<BazaAuthAccessTokenEntity | undefined> {
        return this.repository.findOne({
            where: [{
                accessToken,
            }],
        });
    }

    async hasAccessToken(accessToken: string): Promise<boolean> {
        return !! (await this.findByAccessToken(accessToken));
    }

    async cleanUpExpiredDevices(): Promise<void> {
        await this.repository.delete({
            expiresAt: LessThanOrEqual(new Date()),
        });
    }
}
