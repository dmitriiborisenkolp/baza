import { DynamicModule, Global, Module, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { AuthService } from './service/auth.service';
import { AuthSessionService } from './service/auth-session.service';
import { BazaAuthApiModuleConfig } from './baza-auth.config';
import { BazaAuthJwtModule } from './baza-auth-jwt.module';
import { AuthGuard } from './guards/auth.guard';
import { AuthOptionalGuard } from './guards/auth-optional.guard';
import { AuthRequireAdminRoleGuard } from './guards/auth-require-admin-role.guard';
import { AuthEndpointPaths } from '@scaliolabs/baza-core-shared';
import { BazaI18nEndpointPaths } from '@scaliolabs/baza-core-shared';
import { BazaAuthMasterPasswordModule } from './baza-auth-master-password.module';
import { BazaAccountApiModule } from '../../../baza-account/src/lib/baza-account-api.module';
import { BazaRegistryApiModule } from '../../../baza-registry/src';
import { AuthOptionalNoFailOnInvalidTokenGuard } from './guards/auth-optional-no-fail-on-invalid-token.guard';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaAuthDestroyJwtsOfAccountCommandHandler } from './command-handlers/baza-auth-destroy-jwts-of-account.command-handler';
import { BazaAuthAccessTokenRepository } from './strategy/psql/repositories/baza-auth-access-token.repository';
import { BazaAuthDeviceRepository } from './strategy/psql/repositories/baza-auth-device.repository';
import { AuthDeviceManagerService } from './service/auth-device-manager.service';
import { AuthJwtPsqlStrategy } from './strategy/psql/auth-jwt-psql.strategy';
import { AuthJwtRedisStrategy } from './strategy/redis/auth-jwt-redis.strategy';
import { interval, Subject } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';
import { BazaLogger } from '../../../baza-logger/src';
import { AuthJwtRedisKeyFactory } from './strategy/redis/auth-jwt-redis-key.factory';
import { Auth2FAService } from './service/auth-2fa.service';
import { Auth2FASettingsService } from './service/auth-2fa-settings.service';
import { AuthMasterPasswordService } from './service/auth-master-password.service';
import { EnvService } from '../../../baza-env/src';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaAuthApiModuleConfig>;
}

export { AsyncOptions as BazaAuthModuleAsyncOptions };

const commandHandlers = [BazaAuthDestroyJwtsOfAccountCommandHandler];

const deviceStrategies = [AuthJwtPsqlStrategy, AuthJwtRedisStrategy];

/**
 * Baza Core Auth module
 *
 * Contains core services for Auth & Sign-On / Sign-Out
 * JWT configuration and Master Password features are located in different modules.
 *
 * @see BazaAuthApiModuleConfig
 * @see BazaAuthJwtModule
 * @see BazaAuthMasterPasswordModule
 */
@Module({})
export class BazaAuthModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaAuthGlobalModule,
            imports: [
                BazaAuthJwtModule.forRootAsync({
                    injects: options.injects,
                    useFactory: options.useFactory,
                }),
                BazaAccountApiModule,
                BazaRegistryApiModule,
            ],
            providers: [
                {
                    provide: BazaAuthApiModuleConfig,
                    inject: options.injects,
                    useFactory: async (...injects) => {
                        const userConfig = await options.useFactory(...injects);

                        return {
                            ...userConfig,
                            skipAuthGuardForEndpoints: [
                                ...userConfig.skipAuthGuardForEndpoints,
                                ...[AuthEndpointPaths.verify, AuthEndpointPaths.refreshToken, BazaI18nEndpointPaths.get],
                            ],
                        };
                    },
                },
            ],
            exports: [BazaAuthApiModuleConfig],
        };
    }
}

@Global()
@Module({
    imports: [CqrsModule, BazaAccountApiModule, BazaAuthJwtModule, BazaAuthMasterPasswordModule],
    providers: [
        AuthService,
        AuthService,
        AuthSessionService,
        AuthGuard,
        AuthOptionalGuard,
        AuthRequireAdminRoleGuard,
        AuthOptionalNoFailOnInvalidTokenGuard,
        BazaAuthAccessTokenRepository,
        BazaAuthDeviceRepository,
        AuthDeviceManagerService,
        AuthJwtRedisKeyFactory,
        Auth2FAService,
        Auth2FASettingsService,
        ...deviceStrategies,
        ...commandHandlers,
    ],
    exports: [
        BazaAuthJwtModule,
        AuthService,
        AuthSessionService,
        BazaAuthAccessTokenRepository,
        BazaAuthDeviceRepository,
        AuthDeviceManagerService,
        AuthJwtRedisKeyFactory,
        Auth2FAService,
        Auth2FASettingsService,
    ],
})
class BazaAuthGlobalModule implements OnApplicationBootstrap, OnApplicationShutdown {
    private shutdown$: Subject<void> = new Subject<void>();

    constructor(
        private readonly env: EnvService,
        private readonly logger: BazaLogger,
        private readonly devicesManager: AuthDeviceManagerService,
        private readonly authMasterPasswordService: AuthMasterPasswordService,
    ) {}

    async onApplicationBootstrap(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            interval(10 /* minutes */ * 60 /* seconds */)
                .pipe(startWith(0), takeUntil(this.shutdown$))
                .subscribe(() => {
                    this.devicesManager.cleanUpExpiredTokens().catch((err) => {
                        this.logger.error('[baza-auth] Failed to clean up expired tokens:');
                        this.logger.error(err);
                    });
                });
        }
    }

    async onApplicationShutdown(signal?: string): Promise<void> {
        if (this.env.areWatchersAllowed) {
            await this.shutdown$.next();
            await this.authMasterPasswordService.disable();
        }
    }
}
