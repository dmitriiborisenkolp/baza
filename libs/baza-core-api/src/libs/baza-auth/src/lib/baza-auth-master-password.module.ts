import { Module } from '@nestjs/common';
import { AuthMasterPasswordService } from './service/auth-master-password.service';

/**
 * Baza Core Auth Master Password module
 *
 * Allows to use Master Password feature. You can enable or disable Master Password
 * feature on configuration level.
 *
 * @see BazaAuthApiModuleConfig
 */
@Module({
    providers: [AuthMasterPasswordService],
    exports: [AuthMasterPasswordService],
})
export class BazaAuthMasterPasswordModule {}
