import { DynamicModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { BazaAuthApiModuleConfig } from './baza-auth.config';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaAuthApiModuleConfig>;
}

/**
 * Baza Core JWT Module
 *
 * Configures @nestjs/jwt package to store / generate JWT tokens.
 */
@Module({})
export class BazaAuthJwtModule
{
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaAuthJwtModule,
            imports: [
                JwtModule.registerAsync({
                    inject: options.injects,
                    useFactory: async (...injects) => {
                        const authModuleConfig = await options.useFactory(...injects);

                        return {
                            secret: authModuleConfig.jwtSecret,
                        };
                    },
                }),
            ],
            exports: [
                JwtModule,
            ],
        };
    }
}
