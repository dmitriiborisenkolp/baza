import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { AccountEntity } from '../../../../baza-account/src';

/**
 * ReqAccount Decorator Can use with both `@UseGuards(AuthGuard)` and `@UseGuards(AuthOptionalGuard)`.
 * `user` object will inject to `request` in both of these guards and `ReqAccount` will return the `request.user`.
 * Because `AuthGuard` and `AuthOptionalGuard` are using `authSession.getAccount()` and `authSession.getAccountOptional()`.
 * So it is possible to return AccountEntity (user) or undefined.
 */
export const ReqAccount = createParamDecorator((data: unknown, ctx: ExecutionContext): AccountEntity | undefined => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
});
