import { Injectable } from '@nestjs/common';
import {
    AccessTokenExpiresSettingsMany,
    AccountRole,
    Auth2FAVerificationMethod,
    RefreshTokenExpiresSettingsMany,
} from '@scaliolabs/baza-core-shared';
import { AuthJwtDeviceStrategies } from './strategy/auth-jwt-device-strategy';

export const BAZA_AUTH_E2E_HELPERS_INJECTS: {
    enable2FAForAccountRoles: Array<AccountRole>;
} = {
    enable2FAForAccountRoles: [],
};

/**
 * Async configuration for BazaAuthApiModule
 *
 * You can override default BazaAuthApiModule configuration with
 * `bazaApiBundleConfigBuilder` helper:
 *
 * @decorator `@Injectable()`
 *
 * ```typescript
 *    bazaApiBundleConfigBuilder().withModuleConfigs({
 *          BazaAuthModule: (bundleConfig) => ({
 *              // (your configuration)
 *          }),
 *      })
 * ```
 */
@Injectable()
export class BazaAuthApiModuleConfig {
    /**
     * JWT secret used to generate access/refresh tokens. You should use unique JWT secrets for each environment.
     * JWT secret could be set with AUTH_JWT_SECRET .env variable.
     */
    jwtSecret: string;

    /**
     * Access Token Expiration settings
     */
    accessTokenExpiresSettings: AccessTokenExpiresSettingsMany;

    /**
     * Refresh Token Expiration settings
     */
    refreshTokenExpiresSettings: RefreshTokenExpiresSettingsMany;

    /**
     * Defines list of endpoints which should not be covered with AuthGuard's. AuthGuard's will ignore these
     * endpoints and will not throw errors if Access or Refresh tokens are outdated
     */
    skipAuthGuardForEndpoints: Array<string>;

    /**
     * Enables Master Password feature. You should not enable it on Production environment.
     */
    withMasterPasswordFeature: boolean;

    /**
     * TTL for Master Password.
     */
    masterPasswordTTLSeconds: number;

    /**
     * Strategy used to store access/refresh tokens. There are 2 strategies available:
     *
     *  - AuthJwtDeviceStrategies.psql will store access/refresh tokens in PgSQL DB. It's less secured, but can store more
     *  active users. Baza automatically clean up outdated access/refresh tokens on time interval basis.
     *  - AuthJwtDeviceStrategies.redis will store access/refresh tokens in Memory (Redis). It's more secured, automatically
     *  clean up refresh / access tokens with default Redis EX rules, but you should use it carefully on resources with huge
     *  amount of active users.
     */
    authDeviceStrategy: AuthJwtDeviceStrategies;

    /**
     * Relative Mail Template (HTML) Path to 2FA (Email method)
     */
    auth2FAMailTemplateHtmlPath: string;

    /**
     * Relative Mail Template (TEXT) Path to 2FA (Email method)
     */
    auth2FAMailTemplateTextPath: string;

    /**
     * Allowed 2FA Methods for User / Admin accounts
     */
    allowed2FAMethods: (accountRole: AccountRole) => Array<Auth2FAVerificationMethod>;

    /**
     * TTL for 2FA verification code.
     */
    auth2FAVerificationTokenTTLSeconds: number;

    /**
     * Force require 2FA via Email for specific roles (even if it's disabled in AccountEntity.twoFactorConfig object)
     */
    require2FAEmailVerificationForAccountRoles: Array<AccountRole>;
}
