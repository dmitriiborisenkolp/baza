import { Body, Controller, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';
import { AclNodes } from '../../../../baza-acl/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src';
import { WhitelistCmsAccountService } from '../services/whitelist-account-cms.service';
import {
    WhitelistAccountEndpoint,
    CoreOpenApiTags,
    CoreAcl,
    WhitelistAccountEndpointPaths,
    WhitelistAccountListRequest,
    WhitelistAccountAddRequest,
    WhitelistAccountListResponse,
    WhitelistAccountRemoveRequest,
    WhitelistAccountDto,
} from '@scaliolabs/baza-core-shared';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller()
@ApiBearerAuth()
@ApiTags(CoreOpenApiTags.BazaWhitelistAccount)
@UseGuards(AuthGuard, WithAccessGuard, AuthRequireAdminRoleGuard)
@AclNodes([CoreAcl.SystemRoot])
export class WhitelistAccountCmsController implements WhitelistAccountEndpoint {
    constructor(private readonly cmsService: WhitelistCmsAccountService) {}

    @Post(WhitelistAccountEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List of subscriptions',
    })
    @ApiOkResponse({
        type: WhitelistAccountListRequest,
    })
    async list(@Body() request: WhitelistAccountListRequest): Promise<WhitelistAccountListResponse> {
        return this.cmsService.list(request);
    }

    @Post(WhitelistAccountEndpointPaths.add)
    @ApiOperation({
        summary: 'add',
        description: 'Add subscription',
    })
    @ApiOkResponse({
        type: WhitelistAccountDto,
    })
    async add(@Body() request: WhitelistAccountAddRequest): Promise<WhitelistAccountDto> {
        return await this.cmsService.add(request);
    }

    @Post(WhitelistAccountEndpointPaths.remove)
    @ApiOperation({
        summary: 'remove',
        description: 'Remove (unsubscribe) by email',
    })
    @ApiOkResponse()
    async remove(@Body() request: WhitelistAccountRemoveRequest): Promise<void> {
        return this.cmsService.remove(request);
    }

    @Post(WhitelistAccountEndpointPaths.importCsv)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'Import CSV',
        description: 'Import accounts from CSV',
    })
    @ApiOkResponse()
    @UseInterceptors(FileInterceptor('file'))
    async importCsv(@UploadedFile() file: Express.Multer.File): Promise<void> {
        return this.cmsService.importCsv(file);
    }
}
