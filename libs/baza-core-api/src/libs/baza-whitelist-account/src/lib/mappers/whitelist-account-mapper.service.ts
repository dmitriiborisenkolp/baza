import { Injectable } from '@nestjs/common';
import { BazaWhitelistAccountEntity } from '../entities/baza-whitelist-account.entity';
import { WhitelistAccountDto } from '@scaliolabs/baza-core-shared';

@Injectable()
export class WhitelistAccountMapper {
    entityToDTO(entity: BazaWhitelistAccountEntity): WhitelistAccountDto {
        return {
            id: entity.id,
            email: entity.email,
            dateCreated: entity.dateCreated.toISOString(),
            dateUpdated: entity.dateUpdated.toISOString(),
        };
    }

    entitiesToDTOs(entities: Array<BazaWhitelistAccountEntity>): Array<WhitelistAccountDto> {
        return entities.map((e) => this.entityToDTO(e));
    }
}
