import { DynamicModule, Global, Module } from '@nestjs/common';
import { BazaCrudApiModule } from '../../../baza-crud/src';
import { WhitelistAccountRepository } from './repositories/whitelist-account-repository.service';
import { WhitelistCmsAccountService } from './services/whitelist-account-cms.service';
import { WhitelistAccountMapper } from './mappers/whitelist-account-mapper.service';
import { WhitelistAccountCmsController } from './controllers/whitelist-account-cms.controller';
import { WhitelistAccountCSVService } from './services/whitelist-account-csv.service';
import { WhitelistAccountService } from './services/whitelist-account.service';

const MAPPERS = [
    WhitelistAccountMapper,
]

const SERVICES = [
    WhitelistCmsAccountService,
    WhitelistAccountService,
    WhitelistAccountCSVService
]

@Module({})
export class BazaWhitelistAccountApiModule {
    static forRootAsync(): DynamicModule {
        return {
            module: BazaWhitelistAccountApiGlobalModule,
        };
    }
}

@Global()
@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        WhitelistAccountCmsController,
    ],
    providers: [
        WhitelistAccountRepository,
        ...MAPPERS,
        ...SERVICES
    ],
    exports: [
        WhitelistAccountRepository,
        ...MAPPERS,
        ...SERVICES
    ],
})
class BazaWhitelistAccountApiGlobalModule { }
