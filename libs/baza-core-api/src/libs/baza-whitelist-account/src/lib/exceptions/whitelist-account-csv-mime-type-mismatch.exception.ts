import { HttpStatus } from '@nestjs/common';
import { BazaWhitelistAccountErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class WhitelistAccountCsvMimeTypeMismatchException extends BazaAppException {
    constructor(message: string) {
        super(BazaWhitelistAccountErrorCodes.WhitelistAccountCsvMimeTypeMismatch, message, HttpStatus.BAD_REQUEST);
    }
}
