import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaWhitelistAccountErrorCodes } from '@scaliolabs/baza-core-shared';

export class WhitelistAccountInvalidCsvException extends BazaAppException {
    constructor(message: string) {
        super(BazaWhitelistAccountErrorCodes.BazaWhitelistAccountInvalidCsv, message, HttpStatus.BAD_REQUEST);
    }
}
