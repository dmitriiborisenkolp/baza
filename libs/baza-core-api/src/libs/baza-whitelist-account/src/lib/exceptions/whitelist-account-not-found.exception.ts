import { BazaAppException } from '../../../../baza-exceptions/src';
import { HttpStatus } from '@nestjs/common';
import { BazaWhitelistAccountErrorCodes, bazaWhitelistAccountErrorCodesI18n } from '@scaliolabs/baza-core-shared';

export class WhitelistAccountNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaWhitelistAccountErrorCodes.BazaWhitelistAccountNotFound,
            bazaWhitelistAccountErrorCodesI18n[BazaWhitelistAccountErrorCodes.BazaWhitelistAccountNotFound],
            HttpStatus.NOT_FOUND
        );
    }
}
