import { BazaAppException } from '../../../../baza-exceptions/src';
import { HttpStatus } from '@nestjs/common';
import { BazaWhitelistAccountErrorCodes, bazaWhitelistAccountErrorCodesI18n } from '@scaliolabs/baza-core-shared';

export class WhitelistAccountAlreadyExistsException extends BazaAppException {
    constructor() {
        super(
            BazaWhitelistAccountErrorCodes.BazaWhitelistAccountAlreadyExists,
            bazaWhitelistAccountErrorCodesI18n[BazaWhitelistAccountErrorCodes.BazaWhitelistAccountAlreadyExists],
            HttpStatus.BAD_REQUEST
        );
    }
}
