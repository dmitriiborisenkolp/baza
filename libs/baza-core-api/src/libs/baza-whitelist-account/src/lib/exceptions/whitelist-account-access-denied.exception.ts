import { BazaAppException } from '../../../../baza-exceptions/src';
import { HttpStatus } from '@nestjs/common';
import { BazaWhitelistAccountErrorCodes } from '@scaliolabs/baza-core-shared';

export class WhitelistAccountAccessDeniedException extends BazaAppException {
    constructor(message) {
        super(
            BazaWhitelistAccountErrorCodes.WhitelistAccountAccessDeniedException,
            message,
            HttpStatus.UNAUTHORIZED
        );
    }
}
