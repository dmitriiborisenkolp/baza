import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaWhitelistAccountEntity } from '../entities/baza-whitelist-account.entity';
import { WhitelistAccountNotFoundException } from '../exceptions/whitelist-account-not-found.exception';

@Injectable()
export class WhitelistAccountRepository {
    constructor(
        private readonly connection: Connection,
    ) { }

    get repository(): Repository<BazaWhitelistAccountEntity> {
        return this.connection.getRepository(BazaWhitelistAccountEntity);
    }

    async save(entity: BazaWhitelistAccountEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async remove(entity: BazaWhitelistAccountEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async getById(id: number): Promise<BazaWhitelistAccountEntity> {
        const entity = await this.repository.findOne({
            where: [{
                id,
            }],
        });

        if (!entity) {
            throw new WhitelistAccountNotFoundException();
        }

        return entity;
    }

    async findByEmail(email: string): Promise<BazaWhitelistAccountEntity | undefined> {
        return this.repository.findOne({
            where: [{
                email,
            }],
        });
    }

    async getByEmail(email: string): Promise<BazaWhitelistAccountEntity> {
        const entity = await this.findByEmail(email);

        if (!entity) {
            throw new WhitelistAccountNotFoundException();
        }

        return entity;
    }
}
