import { Injectable } from '@nestjs/common';
import { WhitelistAccountRepository } from '../repositories/whitelist-account-repository.service';
import { CrudService } from '../../../../baza-crud/src';
import { WhitelistAccountMapper } from '../mappers/whitelist-account-mapper.service';
import { BazaWhitelistAccountEntity } from '../entities/baza-whitelist-account.entity';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { ILike } from 'typeorm';
import {
    postgresLikeEscape,
    WhitelistAccountAddRequest,
    WhitelistAccountDto,
    WhitelistAccountRemoveRequest,
    WhitelistAccountListRequest,
    WhitelistAccountListResponse,
} from '@scaliolabs/baza-core-shared';
import { WhitelistAccountAlreadyExistsException } from '../exceptions/whitelist-account-already-exists.exception';
import { WhitelistAccountCSVService } from './whitelist-account-csv.service';

/* eslint no-empty: "error"*/

@Injectable()
export class WhitelistCmsAccountService {
    constructor(
        private readonly mapper: WhitelistAccountMapper,
        private readonly repository: WhitelistAccountRepository,
        private readonly crud: CrudService,
        private readonly csv: WhitelistAccountCSVService,
    ) {}

    async list(request: WhitelistAccountListRequest): Promise<WhitelistAccountListResponse> {
        const findOptions: FindManyOptions<BazaWhitelistAccountEntity> = {
            order: {
                id: 'DESC',
            },
        };

        if (request.queryString) {
            findOptions.where = [
                {
                    email: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                },
            ];
        }

        return this.crud.find({
            request,
            entity: BazaWhitelistAccountEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions,
        });
    }

    async remove(request: WhitelistAccountRemoveRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove(entity);
    }

    async add(request: WhitelistAccountAddRequest): Promise<WhitelistAccountDto> {
        const entity = await this.repository.findByEmail(request.email);

        if (entity) {
            throw new WhitelistAccountAlreadyExistsException();
        }

        const whitelistAccount = new BazaWhitelistAccountEntity();
        whitelistAccount.email = request.email;
        await this.repository.save(whitelistAccount);

        return this.mapper.entityToDTO(whitelistAccount);
    }

    async importCsv(file: Express.Multer.File): Promise<void> {
        await this.csv.validateImportFile(file);
        await this.csv.validateCsvData(file);
        await this.csv.validateCsvEmails(file);

        const accounts = await this.csv.getAccountEmails(file);

        for (let i = 0; i < accounts.length; i++) {
            const email = accounts[i];

            try {
                await this.add({ email });
            } catch {
                // continue regardless of error
            }
        }
    }
}
