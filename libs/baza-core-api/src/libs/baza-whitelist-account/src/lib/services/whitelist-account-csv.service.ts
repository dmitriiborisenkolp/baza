import { Injectable } from '@nestjs/common';
import { WhitelistAccountCsvMimeTypeMismatchException } from '../exceptions/whitelist-account-csv-mime-type-mismatch.exception';
import { WhitelistAccountInvalidCsvException } from '../exceptions/whitelist-account-invalid-csv.exception';

enum FILE_MIME_TYPES {
    csv = 'text/csv',
}

export const DEFAULT_WHITELIST_ACCOUNT_CSV_HEADER = 'Email';
/* eslint-disable */
export const VALID_EMAIL_REGEX =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

interface WhitelistAccountCsvData {
    headers: string[];
    columns: Array<Array<string>>;
}

@Injectable()
export class WhitelistAccountCSVService {
    async validateImportFile(file: Express.Multer.File): Promise<void> {
        if (!file) {
            throw new WhitelistAccountInvalidCsvException(`No file attached, please attach a CSV.`);
        }

        if (file.mimetype !== FILE_MIME_TYPES.csv) {
            throw new WhitelistAccountCsvMimeTypeMismatchException(
                `MIME mismatch, expected [${FILE_MIME_TYPES.csv}], got [${file.mimetype}]`,
            );
        }
    }

    async validateCsvData(file): Promise<void> {
        const data = this.getCsvData(file);
        const invalidRow = data.columns.find((i) => i.length !== 1);

        if (data.headers.length !== 1) {
            throw new WhitelistAccountInvalidCsvException(
                `Invalid csv headers, expected single header [${DEFAULT_WHITELIST_ACCOUNT_CSV_HEADER}], got [${data.headers[0]}]`,
            );
        }

        if (!data.headers.includes(DEFAULT_WHITELIST_ACCOUNT_CSV_HEADER)) {
            throw new WhitelistAccountInvalidCsvException(
                `Invalid csv headers, expected [${DEFAULT_WHITELIST_ACCOUNT_CSV_HEADER}], got [${data.headers[0]}]`,
            );
        }

        if (invalidRow) {
            throw new WhitelistAccountInvalidCsvException(
                `Invalid csv row [${invalidRow}], please make sure there is only single email on each row.`,
            );
        }
    }

    getAccountEmails(file: Express.Multer.File): string[] {
        const data = this.getCsvData(file);

        return data.columns.map((element) => element[0]);
    }

    getCsvData(file: Express.Multer.File): WhitelistAccountCsvData {
        let csv: Array<Array<string>>;

        // NOTE: Following can be used for text CSV parsing, for xlsx its better to go with a library as it may cause issues with special/hidden characters.
        try {
            csv = file.buffer
                .toString() // convert Buffer to string
                .split('\n') // split string to lines
                .map((e) => e.trim()) // remove white spaces for each line
                .map((e) => e.split(',').map((e) => e.trim()));
        } catch (error) {
            throw new WhitelistAccountInvalidCsvException(`Error in parsing provided csv : ${error}.`);
        }

        return {
            headers: csv.shift(),
            columns: csv,
        };
    }

    async validateCsvEmails(file: Express.Multer.File): Promise<void> {
        const emails = this.getAccountEmails(file);

        for (let index = 0; index < emails.length; index++) {
            const validEmail = !!emails[index].match(VALID_EMAIL_REGEX);

            if (!validEmail) {
                throw new WhitelistAccountInvalidCsvException(
                    `Invalid account email at index [${index}][${emails[index]}], please add valid emails.`,
                );
            }
        }
    }
}
