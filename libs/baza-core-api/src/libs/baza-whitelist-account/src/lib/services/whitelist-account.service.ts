import { Injectable } from '@nestjs/common';
import { WhitelistAccountRepository } from '../repositories/whitelist-account-repository.service';
import { WhitelistAccountAccessDeniedException } from '../exceptions/whitelist-account-access-denied.exception';
import { BazaRegistryService } from '../../../../baza-registry/src';

@Injectable()
export class WhitelistAccountService {
    constructor(
        private readonly repository: WhitelistAccountRepository,
        private readonly registry: BazaRegistryService
    ) { }

    async validateAccountAccess(email: string): Promise<void> {
        const account = await this.repository.findByEmail(email);

        const message = this.registry.getValue('bazaCoreAuth.whitelistAccountAccessMessage');

        if (!account) {
            throw new WhitelistAccountAccessDeniedException(message);
        }
    }
}
