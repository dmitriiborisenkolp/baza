import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity({
    name: 'baza_whitelist_account_entity',
})
export class BazaWhitelistAccountEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @CreateDateColumn()
    dateCreated: Date;

    @UpdateDateColumn()
    dateUpdated: Date;

    @Column({
        unique: true,
    })
    email: string;
}
