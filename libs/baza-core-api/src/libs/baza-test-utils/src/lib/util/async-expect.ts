import { BAZA_ASYNC_EXPECT_MAX_ATTEMPTS, BAZA_ASYNC_EXPECT_MAX_INTERVAL_MILLIS } from '../constants/async-expects.constants';

const wait = (interval) => new Promise((resolve) => setTimeout(resolve, interval));

interface IAsyncExpectOptions {
    /**
     * Retry Interval in Milli-Second
     */
    intervalMillis?: number;

    /**
     * Total attempts (Retry Number)
     */
    attempts?: number;
}

/**
 * Calls a data access function and validates its return value with a validation function.
 * Retries the data access function with a specified interval if the validation function returns false.
 * @param dataAccessFn A function that returns a promise resolving to the data to be validated.
 * @param validateDataFn A function that takes the data returned by the data access function
 *   and returns true if the data is valid, or false if it is not.
 * @param options An object containing the following optional properties:
 *   - intervalMillis: The number of milliseconds to wait between retries.
 *   - attempts: The maximum number of times to retry the data access function.
 * @returns A promise that resolves to the data returned by the data access function if it is valid,
 *   or rejects with an error if the maximum number of attempts is reached without valid data.
 */
export async function asyncExpect<T>(
    dataAccessFn: () => Promise<T>,
    validateDataFn?: (data?: T) => boolean,
    options?: IAsyncExpectOptions,
): Promise<T> {
    let intervalMillis = options?.intervalMillis;
    let attempts = options?.attempts;

    if (
        intervalMillis === undefined ||
        intervalMillis === null ||
        intervalMillis < 0 ||
        intervalMillis > BAZA_ASYNC_EXPECT_MAX_INTERVAL_MILLIS
    ) {
        intervalMillis = BAZA_ASYNC_EXPECT_MAX_INTERVAL_MILLIS;
    }

    if (attempts === undefined || attempts === null || attempts < 0 || attempts > BAZA_ASYNC_EXPECT_MAX_ATTEMPTS) {
        attempts = BAZA_ASYNC_EXPECT_MAX_ATTEMPTS;
    }

    try {
        const data = await dataAccessFn();

        if (validateDataFn) {
            const isValidData = validateDataFn(data);
            if (!isValidData) {
                throw new Error("Async expectations wasn't achieved");
            }
        }

        return data;
    } catch (err) {
        if (attempts === 0) {
            throw err;
        }

        await wait(intervalMillis);

        return asyncExpect(dataAccessFn, validateDataFn, { intervalMillis, attempts: attempts - 1 });
    }
}
