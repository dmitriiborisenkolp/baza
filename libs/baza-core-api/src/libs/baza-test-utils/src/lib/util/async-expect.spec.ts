import { asyncExpect } from './async-expect';

describe('@scaliolabs/baza-core-api/baza-test-utils/util/async-expect.spec.ts', () => {
    let REJECTS = 0;

    const successOnNAttempts = (maxAttempts: number) => {
        let attempts = 1;

        return () =>
            new Promise((resolve, reject) => {
                if (attempts < maxAttempts) {
                    attempts++;
                    REJECTS++;

                    reject();
                } else {
                    resolve(attempts);
                }
            });
    };

    it('will works correctly on expectation which will be successful immediately', async () => {
        const callback = successOnNAttempts(1);

        await asyncExpect(callback, null, {
            intervalMillis: 100,
        });

        expect(REJECTS).toBe(0);
    });

    it('will works correctly on expectation which will be successful on 2nd attempt', async () => {
        const callback = successOnNAttempts(2);

        const result = await asyncExpect(callback, null, {
            intervalMillis: 100,
        });

        expect(result).toBe(2);
        expect(REJECTS).toBe(1);
    });

    it('will works correctly on expectation which will be successful on 5nd attempt', async () => {
        const callback = successOnNAttempts(5);

        const result = await asyncExpect(callback, null, {
            intervalMillis: 300,
        });

        expect(result).toBe(5);
        expect(REJECTS).toBe(5);
    });
});
