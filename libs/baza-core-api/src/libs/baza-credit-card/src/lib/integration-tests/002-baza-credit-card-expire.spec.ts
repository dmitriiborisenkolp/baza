import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaCreditCardNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse, shiftMonth } from '@scaliolabs/baza-core-shared';
import { BazaCreditCardExpiredResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/integration-tests/002-baza-credit-card-expire.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessCreditCard = new BazaCreditCardNodeAccess(http);

    const CURRENT_YEAR = new Date().getFullYear() - 2000;
    const CURRENT_MONTH = shiftMonth(new Date().getMonth(), 1);

    it('will correctly validate credit card number (NOW + 1 month)', async () => {
        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: CURRENT_YEAR,
            creditCardExpireMonth: shiftMonth(CURRENT_MONTH, 1),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Valid);
    });

    it('will correctly validate credit card number (NOW + 2 month)', async () => {
        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: CURRENT_YEAR,
            creditCardExpireMonth: shiftMonth(CURRENT_MONTH, 2),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Valid);
    });

    it('will correctly validate credit card number (NOW + 1 year)', async () => {
        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: CURRENT_YEAR + 1,
            creditCardExpireMonth: CURRENT_MONTH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Valid);
    });

    it('will correctly validate credit card number (NOW + 1 year + 1 month)', async () => {
        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: CURRENT_YEAR + 1,
            creditCardExpireMonth: shiftMonth(CURRENT_MONTH, 1),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Valid);
    });

    it('will correctly validate credit card number (NOW + 0)', async () => {
        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: CURRENT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Valid);
    });

    it('will correctly validate credit card number (NOW - 1 month)', async () => {
        let currentMonth = CURRENT_MONTH - 1;
        let currentYear = CURRENT_YEAR;

        if (currentMonth === 0) {
            currentMonth = 12;
            currentYear = currentYear - 1;
        }

        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: currentYear,
            creditCardExpireMonth: currentMonth,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Expired);
    });

    it('will correctly validate credit card number (NOW - 2 month)', async () => {
        let currentMonth = CURRENT_MONTH - 2;
        let currentYear = CURRENT_YEAR;

        if (currentMonth === 0) {
            currentMonth = 12;
            currentYear = currentYear - 1;
        } else if (currentMonth < 0) {
            currentMonth = 11; // to be sure we are testing => (NOW - 2 month)
            currentYear = currentYear - 1;
        }

        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: currentYear,
            creditCardExpireMonth: currentMonth,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Expired);
    });

    it('will correctly validate credit card number (NOW - 1 year)', async () => {
        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: CURRENT_YEAR - 1,
            creditCardExpireMonth: CURRENT_MONTH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Expired);
    });

    it('will correctly validate credit card number (NOW - 1 year - 1 month)', async () => {
        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: CURRENT_YEAR - 1,
            creditCardExpireMonth: shiftMonth(CURRENT_MONTH, -1),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Expired);
    });

    it('will correctly validate credit card number (NOW - 1 year + 1 month)', async () => {
        const response = await dataAccessCreditCard.isExpired({
            creditCardExpireYear: CURRENT_YEAR - 1,
            creditCardExpireMonth: shiftMonth(CURRENT_MONTH, 1),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaCreditCardExpiredResponse.Expired);
    });

    it('will allow to use months from 1 up to 12', async () => {
        for (let i = 1; i <= 12; i++) {
            const response = await dataAccessCreditCard.isExpired({
                creditCardExpireYear: CURRENT_YEAR + 1,
                creditCardExpireMonth: i,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect(response.result).toBe(BazaCreditCardExpiredResponse.Valid);
        }
    });

    it('will fail for invalid months', async () => {
        const months = [13, 14, 15, 99];

        for (const month of months) {
            const response = await dataAccessCreditCard.isExpired({
                creditCardExpireYear: CURRENT_YEAR + 1,
                creditCardExpireMonth: month,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect(response.result).toBe(BazaCreditCardExpiredResponse.OutOfRange);
        }
    });

    it('will fail for invalid years', async () => {
        const years = [100, 101, 199, CURRENT_YEAR + 2000, CURRENT_YEAR + 2001];

        for (const year of years) {
            const response = await dataAccessCreditCard.isExpired({
                creditCardExpireYear: year,
                creditCardExpireMonth: shiftMonth(CURRENT_MONTH, 1),
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect(response.result).toBe(BazaCreditCardExpiredResponse.OutOfRange);
        }
    });
});
