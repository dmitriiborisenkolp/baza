import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaCreditCardNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaCreditCardType } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/integration-tests/001-baza-credit-card.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessCreditCard = new BazaCreditCardNodeAccess(http);

    // Validate endpoint

    it('works correctly with validate endpoint (Visa)', async () => {
        const response = await dataAccessCreditCard.validate({
            creditCardNumber: '4916338506082832',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeTruthy();
        expect(response.creditCardType).toBe(BazaCreditCardType.Visa);
    });

    it('works correctly with validate endpoint (MasterCard)', async () => {
        const response = await dataAccessCreditCard.validate({
            creditCardNumber: '5280934283171080',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeTruthy();
        expect(response.creditCardType).toBe(BazaCreditCardType.MasterCard);
    });

    it('works correctly with validate endpoint (Visa but exclude Visa as accepted Card Type)', async () => {
        const response = await dataAccessCreditCard.validate({
            creditCardNumber: '4916338506082832',
            accept: [BazaCreditCardType.MasterCard],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeFalsy();
        expect(response.creditCardType).toBe(BazaCreditCardType.Visa);
    });

    it('works correctly with validate endpoint (Invalid Type)', async () => {
        const response = await dataAccessCreditCard.validate({
            creditCardNumber: '99998888777766665555',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeFalsy();
        expect(response.creditCardType).toBeUndefined();
    });

    // Details endpoint

    it('works correctly with details endpoint (Visa)', async () => {
        const response = await dataAccessCreditCard.details({
            creditCardNumber: '4916338506082832',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeTruthy();
        expect(response.creditCard.type).toBe(BazaCreditCardType.Visa);
        expect(response.creditCard.lengths).toEqual([16, 18, 19]);
        expect(response.creditCard.gaps).toEqual([4, 8, 12]);
        expect(response.creditCard.codeName).toEqual('CVV');
        expect(response.creditCard.codeLength).toEqual(3);
    });

    it('works correctly with details endpoint (MasterCard)', async () => {
        const response = await dataAccessCreditCard.details({
            creditCardNumber: '5280934283171080',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeTruthy();
        expect(response.creditCard.type).toBe(BazaCreditCardType.MasterCard);
    });

    it('works correctly with details endpoint (Visa but exclude Visa as accepted Card Type)', async () => {
        const response = await dataAccessCreditCard.details({
            creditCardNumber: '4916338506082832',
            accept: [BazaCreditCardType.MasterCard],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeFalsy();
        expect(response.creditCard.type).toBe(BazaCreditCardType.Visa);
    });

    it('works correctly with details endpoint (Invalid Type)', async () => {
        const response = await dataAccessCreditCard.details({
            creditCardNumber: '99998888777766665555',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeFalsy();
        expect(response.creditCard).toBeUndefined();
    });
});
