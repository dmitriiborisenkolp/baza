import {
    BazaCreditCardDetailsRequest,
    BazaCreditCardDetailsResponse,
    BazaCreditCardEndpoint,
    BazaCreditCardEndpointPaths,
    BazaCreditCardIsExpiredRequest,
    BazaCreditCardIsExpiredResponse,
    BazaCreditCardValidateRequest,
    BazaCreditCardValidateResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaCreditCardService } from '../services/baza-credit-card.service';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';

@Controller()
@ApiTags(CoreOpenApiTags.BazaCreditCard)
export class BazaCreditCardController implements BazaCreditCardEndpoint {
    constructor(private readonly service: BazaCreditCardService) {}

    @Post(BazaCreditCardEndpointPaths.validate)
    @ApiOperation({
        summary: 'validate',
        description: 'Validate credit card number and returns detected credit card type',
    })
    @ApiOkResponse({
        type: BazaCreditCardValidateResponse,
    })
    async validate(@Body() request: BazaCreditCardValidateRequest): Promise<BazaCreditCardValidateResponse> {
        return this.service.validate(request);
    }

    @Post(BazaCreditCardEndpointPaths.details)
    @ApiOperation({
        summary: 'details',
        description: 'Returns details of Credit Card',
    })
    @ApiOkResponse({
        type: BazaCreditCardDetailsResponse,
    })
    async details(@Body() request: BazaCreditCardDetailsRequest): Promise<BazaCreditCardDetailsResponse> {
        return this.service.details(request);
    }

    @Post(BazaCreditCardEndpointPaths.isExpired)
    @ApiOperation({
        summary: 'isExpired',
        description: 'Validate month/year of card against current date',
    })
    @ApiOkResponse({
        type: BazaCreditCardIsExpiredResponse,
    })
    async isExpired(@Body() request: BazaCreditCardIsExpiredRequest): Promise<BazaCreditCardIsExpiredResponse> {
        return this.service.isExpired(request);
    }
}
