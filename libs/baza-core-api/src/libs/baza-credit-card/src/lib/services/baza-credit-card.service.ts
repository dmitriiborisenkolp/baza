import { Injectable } from '@nestjs/common';
import { BazaCreditCardDetailsRequest, BazaCreditCardDetailsResponse, bazaCreditCardDetect, bazaCreditCardIsExpired, BazaCreditCardIsExpiredRequest, BazaCreditCardIsExpiredResponse, BazaCreditCardValidateRequest, BazaCreditCardValidateResponse } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaCreditCardService {
    validate(request: BazaCreditCardValidateRequest): BazaCreditCardValidateResponse {
        const creditCard = bazaCreditCardDetect(request.creditCardNumber);

        if (creditCard) {
            if (Array.isArray(request.accept) && ! request.accept.includes(creditCard.type)) {
                return {
                    isValid: false,
                    creditCardType: creditCard.type,
                };
            } else {
                return {
                    isValid: true,
                    creditCardType: creditCard.type,
                };
            }
        } else {
            return {
                isValid: false,
            };
        }
    }

    details(request: BazaCreditCardDetailsRequest): BazaCreditCardDetailsResponse {
        const creditCard = bazaCreditCardDetect(request.creditCardNumber);

        if (creditCard) {
            if (Array.isArray(request.accept) && ! request.accept.includes(creditCard.type)) {
                return {
                    isValid: false,
                    creditCard,
                };
            } else {
                return {
                    isValid: true,
                    creditCard,
                };
            }
        } else {
            return {
                isValid: false,
            };
        }
    }

    isExpired(request: BazaCreditCardIsExpiredRequest): BazaCreditCardIsExpiredResponse {
        return {
            result: bazaCreditCardIsExpired(
                request.creditCardExpireMonth,
                request.creditCardExpireYear,
            ),
        }
    }
}
