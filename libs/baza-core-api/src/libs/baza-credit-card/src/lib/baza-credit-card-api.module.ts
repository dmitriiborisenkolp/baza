import { Module } from '@nestjs/common';
import { BazaCreditCardController } from './controllers/baza-credit-card.controller';
import { BazaCreditCardService } from './services/baza-credit-card.service';

@Module({
    controllers: [
        BazaCreditCardController,
    ],
    providers: [
        BazaCreditCardService,
    ],
    exports: [
        BazaCreditCardService,
    ],
})
export class BazaCreditCardApiModule {
}
