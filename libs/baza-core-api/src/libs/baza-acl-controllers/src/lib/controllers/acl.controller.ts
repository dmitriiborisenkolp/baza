import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
    BazaAclCurrentResponse,
    AclEndpoint,
    BazaAclEndpointPaths,
    BazaAclGetAclRequest,
    BazaAclGetAclResponse,
    BazaAclSetAclRequest,
    BazaAclSetAclResponse,
    CoreAcl,
} from '@scaliolabs/baza-core-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthRequireAdminRoleGuard, ReqAccount, ReqAccountId } from '../../../../baza-auth/src';
import { AclNodes, AclService } from '../../../../baza-acl/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src/lib/guards/with-access.guard';
import { AccountEntity } from '../../../../../../../baza-core-api/src';

@ApiTags(CoreOpenApiTags.BazaACL)
@ApiBearerAuth()
@Controller()
export class AclController implements AclEndpoint {
    constructor(private readonly service: AclService) {}

    @Post(BazaAclEndpointPaths.current)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'current',
        description: 'Returns current ACL settings',
    })
    @ApiOkResponse({
        type: BazaAclCurrentResponse,
    })
    async current(@ReqAccountId() accountId: number): Promise<BazaAclCurrentResponse> {
        if (await this.service.hasAclOfAccount(accountId)) {
            const aclEntity = await this.service.getAclOfAccount(accountId);

            return {
                acl: aclEntity.acl,
            };
        } else {
            return {
                acl: [],
            };
        }
    }

    @Post(BazaAclEndpointPaths.getAcl)
    @AclNodes([CoreAcl.BazaAcl])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getAcl',
        description: 'Returns ACL settings of requested account',
    })
    @ApiOkResponse({
        type: BazaAclGetAclResponse,
    })
    async getAcl(@Body() request: BazaAclGetAclRequest, @ReqAccountId() accountId: number): Promise<BazaAclGetAclResponse> {
        if (await this.service.hasAclOfAccount(accountId)) {
            const aclEntity = await this.service.getAclOfAccount(request.accountId);

            return {
                acl: aclEntity.acl,
            };
        } else {
            return {
                acl: [],
            };
        }
    }

    @Post(BazaAclEndpointPaths.setAcl)
    @AclNodes([CoreAcl.BazaAcl, CoreAcl.BazaSetAcl])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'setAcl',
        description: 'Update ACL for requested account',
    })
    @ApiOkResponse({
        type: BazaAclSetAclResponse,
    })
    async setAcl(@Body() request: BazaAclSetAclRequest, @ReqAccount() account: AccountEntity): Promise<BazaAclSetAclResponse> {
        const aclEntity = await this.service.setACLOfAccount(account, request);

        return {
            acl: aclEntity.acl,
        };
    }
}
