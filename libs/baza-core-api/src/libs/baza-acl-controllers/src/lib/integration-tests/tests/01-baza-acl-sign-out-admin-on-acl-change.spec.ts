// https://scalio.atlassian.net/browse/CMNW-1552

import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, CoreAcl } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaAclNgNodeAccess } from '@scaliolabs/baza-core-node-access';

describe('@scaliolabs/baza-core-api-controllers/integration-tests/tests/01-baza-acl-sign-out-admin-on-acl-change.spec.ts', () => {
    let ADMIN_ACCOUNT_ID: number;
    let ACCESS_TOKEN: string;

    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAcl = new BazaAclNgNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        ACCESS_TOKEN = http.authResponse.accessToken;
        ADMIN_ACCOUNT_ID = http.authResponse.jwtPayload.accountId;
    });

    it('will successfully verify token', async () => {
        const response = await dataAccessAuth.verify({
            jwt: ACCESS_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully update ACL for Admin 1', async () => {
        await http.authE2eAdmin2();

        const aclResponse = await dataAccessAcl.setAcl({
            accountId: ADMIN_ACCOUNT_ID,
            acl: [CoreAcl.BazaAcl],
        });

        expect(isBazaErrorResponse(aclResponse)).toBeFalsy();
        expect(aclResponse.acl).toEqual([CoreAcl.BazaAcl]);
    });

    it('will fail for verify token request for updated admin', async () => {
        const response = await dataAccessAuth.verify({
            jwt: ACCESS_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will have new ACL rules for Admin 1', async () => {
        await http.authE2eAdmin();

        const aclResponse = await dataAccessAcl.current();

        expect(isBazaErrorResponse(aclResponse)).toBeFalsy();
        expect(aclResponse.acl).toEqual([CoreAcl.BazaAcl]);
    });
});
