import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, CoreAcl } from '@scaliolabs/baza-core-shared';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaAclNgNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaAccountCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AccountAcl, AccountErrorCodes } from '@scaliolabs/baza-core-shared';
import { AclErrorCodes } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api-controllers/integration-tests/tests/02-baza-acl-limit-assign-to-new-accounts.spec.ts', () => {
    let ACCOUNT_ROOT_ID: number;
    let ACCOUNT_ID: number;
    let ACCOUNT_2_ID: number;

    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAcl = new BazaAclNgNodeAccess(http);
    const dataAccessAccountCMS = new BazaAccountCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        ACCOUNT_ROOT_ID = http.authResponse.jwtPayload.accountId;
    });

    it('will allow to create administrator with limited access', async () => {
        const response = await dataAccessAccountCMS.registerAdminAccount({
            email: 'e2e-acl-example@scal.io',
            firstName: 'E2E',
            lastName: 'Example',
            password: 'Scalio#1337!',
            passwordConfirm: 'Scalio#1337!',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ACCOUNT_ID = response.id;

        const aclSetResponse = await dataAccessAcl.setAcl({
            acl: [AccountAcl.Accounts, AccountAcl.AccountsResetPassword],
            accountId: ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(aclSetResponse)).toBeFalsy();

        const authResponse = await dataAccessAuth.auth({
            email: 'e2e-acl-example@scal.io',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();
        expect(authResponse.jwtPayload.accountAcl).toEqual([AccountAcl.Accounts, AccountAcl.AccountsResetPassword]);
    });

    it('will allow to change ACL of new admin account', async () => {
        const aclSetResponse = await dataAccessAcl.setAcl({
            acl: [
                AccountAcl.Accounts,
                AccountAcl.AccountsResetPassword,
                AccountAcl.AccountsAssign,
                AccountAcl.AccountsAdminCreate,
                CoreAcl.BazaAcl,
                CoreAcl.BazaSetAcl,
            ],
            accountId: ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(aclSetResponse)).toBeFalsy();

        const authResponse = await dataAccessAuth.auth({
            email: 'e2e-acl-example@scal.io',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();
        expect(authResponse.jwtPayload.accountAcl).toEqual([
            AccountAcl.Accounts,
            AccountAcl.AccountsResetPassword,
            AccountAcl.AccountsAssign,
            AccountAcl.AccountsAdminCreate,
            CoreAcl.BazaAcl,
            CoreAcl.BazaSetAcl,
        ]);
    });

    it('will allow to create one more admin account with limited admin account', async () => {
        await http.auth({
            email: 'e2e-acl-example@scal.io',
            password: 'Scalio#1337!',
        });

        const response = await dataAccessAccountCMS.registerAdminAccount({
            email: 'e2e-acl-example-2@scal.io',
            firstName: 'E2E',
            lastName: 'Example 2',
            password: 'Scalio#1337!',
            passwordConfirm: 'Scalio#1337!',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const authResponse = await dataAccessAuth.auth({
            email: 'e2e-acl-example-2@scal.io',
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();
        expect(authResponse.jwtPayload.accountAcl).toEqual([]);

        ACCOUNT_2_ID = response.id;
    });

    it('will not allow to set system root access with limited admin account', async () => {
        const aclSetResponse: BazaError = (await dataAccessAcl.setAcl({
            acl: [CoreAcl.SystemRoot],
            accountId: ACCOUNT_2_ID,
        })) as any;

        expect(isBazaErrorResponse(aclSetResponse)).toBeTruthy();
        expect(aclSetResponse.code).toBe(AclErrorCodes.AclOutOfScope);
    });

    it('will not allow to update acl with nodes which are not assigned to new admin account', async () => {
        const aclSetResponse: BazaError = (await dataAccessAcl.setAcl({
            acl: [AccountAcl.AccountsActivate],
            accountId: ACCOUNT_2_ID,
        })) as any;

        expect(isBazaErrorResponse(aclSetResponse)).toBeTruthy();
        expect(aclSetResponse.code).toBe(AclErrorCodes.AclOutOfScope);
    });

    it('will allow to set acl with nodes available for new admin account', async () => {
        const aclSetResponse: BazaError = (await dataAccessAcl.setAcl({
            acl: [AccountAcl.Accounts, AccountAcl.AccountsResetPassword, AccountAcl.AccountsAssign],
            accountId: ACCOUNT_2_ID,
        })) as any;

        expect(isBazaErrorResponse(aclSetResponse)).toBeFalsy();
    });

    it('will not allow to change ACL of admin account with SystemRoot access', async () => {
        const aclSetResponse: BazaError = (await dataAccessAcl.setAcl({
            acl: [AccountAcl.Accounts, AccountAcl.AccountsResetPassword, AccountAcl.AccountsAssign],
            accountId: ACCOUNT_ROOT_ID,
        })) as any;

        expect(isBazaErrorResponse(aclSetResponse)).toBeTruthy();
        expect(aclSetResponse.code).toBe(AclErrorCodes.AclOutOfScope);
    });

    it('will allow system root admin assign new roles', async () => {
        await http.authE2eAdmin();

        const aclSetResponse: BazaError = (await dataAccessAcl.setAcl({
            acl: [AccountAcl.Accounts, AccountAcl.AccountsResetPassword, AccountAcl.AccountsAssign, AccountAcl.AccountsRevoke],
            accountId: ACCOUNT_ID,
        })) as any;

        expect(isBazaErrorResponse(aclSetResponse)).toBeFalsy();
    });

    it('will allow to revoke admin access from limited admin account to another limited admin account', async () => {
        await http.auth({
            email: 'e2e-acl-example@scal.io',
            password: 'Scalio#1337!',
        });

        const response = await dataAccessAccountCMS.revokeAdminAccess({
            email: 'e2e-acl-example-2@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to revoke admin access of super admin from limited admin account', async () => {
        const response: BazaError = (await dataAccessAccountCMS.revokeAdminAccess({
            email: 'e2e-admin@scal.io',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountNoAccessToRevokeAccount);
    });
});
