import { Module } from '@nestjs/common';
import { BazaAclModule } from '../../../baza-acl/src/lib/baza-acl.module';
import { AclController } from './controllers/acl.controller';

@Module({
    imports: [
        BazaAclModule,
    ],
    controllers: [
        AclController,
    ]
})
export class BazaAclApiControllersModule {
}
