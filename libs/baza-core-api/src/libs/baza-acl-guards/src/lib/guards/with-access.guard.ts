import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthSessionService } from '../../../../baza-auth/src';
import { AclService } from '../../../../baza-acl/src';

@Injectable()
export class WithAccessGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        private readonly authSession: AuthSessionService,
        private readonly aclService: AclService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const account = await this.authSession.getAccount();
        const allAccessNodesVariants: Array<Array<string>> = [
            ...(this.reflector.get<Array<Array<string>>>('accessNodes', context.getHandler()) || []),
            ...(this.reflector.get<Array<Array<string>>>('accessNodes', context.getClass()) || []),
        ];

        if (! allAccessNodesVariants || (Array.isArray(allAccessNodesVariants) && allAccessNodesVariants.length === 0)) {
            return true;
        }

        for (const accessNodes of allAccessNodesVariants) {
            if (! await this.aclService.hasAccess(account, accessNodes)) {
                return false;
            }
        }

        return true;
    }
}
