import { Module } from '@nestjs/common';
import { WithAccessGuard } from './guards/with-access.guard';

@Module({
    providers: [
        WithAccessGuard,
    ],
    exports: [
        WithAccessGuard,
    ]
})
export class BazaAclApiGuardsModule {
}
