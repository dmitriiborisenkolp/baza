import { Injectable } from '@nestjs/common';
import { interval, Subject } from 'rxjs';
import { skipWhile, startWith, takeUntil } from 'rxjs/operators';
import { BazaLogger } from '../../../../../libs/baza-logger/src';
import { BazaDeviceTokenService } from '../services/baza-device-token.service';
import { EnvService } from '../../../../baza-env/src';

const INTERVAL = 12 /* hours */ * 60 /* minutes */ * 60 /* seconds */ * 1000; /* ms */

@Injectable()
export class BazaDeviceTokenCleanUpAppleWatcher {
    logsEnabled: boolean;

    private readonly logs$ = new Subject<{ level: 'info' | 'error'; message: string }>();

    private readonly stop$ = new Subject<void>();

    constructor(
        private readonly env: EnvService,
        private readonly logger: BazaLogger,
        private readonly deviceTokenService: BazaDeviceTokenService,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    async start(ttl: number): Promise<void> {
        if (this.env.areWatchersAllowed) {
            this.startLogger();

            interval(INTERVAL)
                .pipe(startWith(0), takeUntil(this.stop$))
                .subscribe(async (value) => {
                    try {
                        const now = new Date();
                        const timeFrame = new Date(now.setMinutes(now.getMinutes() - ttl));

                        const { affected } = await this.deviceTokenService.destroyDeviceTokens({
                            scope: 'Apple',
                            beforeDate: timeFrame,
                        });

                        this.logInfo(`Iteration ${value} - Affected ${affected} - Destroy from: ${timeFrame.toLocaleString()}`);
                    } catch (error) {
                        this.logError(`Iteration ${value} - ${error}`);
                    }
                });

            this.logInfo(`Started watch for outdated Apple device tokens clean up`);
        }
    }

    async stop(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            this.logInfo(`Stopped watch for outdated Apple device tokens clean up`);

            this.stop$.next();
            this.stop$.complete();
        }
    }

    private startLogger() {
        this.logs$
            .pipe(
                skipWhile(() => !this.logsEnabled),
                takeUntil(this.stop$),
            )
            .subscribe(({ level, message }) => {
                const mappingsFn = {
                    info: () => this.logger.log(message),
                    error: () => this.logger.error(message),
                };

                mappingsFn[level as string]();
            });
    }

    private logInfo = (message: string) => this.logs$.next({ level: 'info', message });

    private logError = (message: string) => this.logs$.next({ level: 'error', message });
}
