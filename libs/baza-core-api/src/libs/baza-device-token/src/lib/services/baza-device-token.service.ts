import { Injectable } from '@nestjs/common';
import {
    BazaDeviceTokenDestroyRequest,
    BazaDeviceTokenDestroyResponse,
    BazaDeviceTokenDto,
    BazaDeviceTokenGetAllRequest,
    BazaDeviceTokenGetAllResponse,
    BazaDeviceTokenLinkRequest,
    BazaDeviceTokenLinkResponse,
    BazaDeviceTokenType,
    BazaDeviceTokenUnlinkRequest,
    BazaDeviceTokenUnlinkResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaAccountMapper, BazaAccountRepository } from '../../../../baza-account/src';
import { CrudService } from '../../../../baza-crud/src';
import { AccountDeviceTokenEntity, BAZA_DEVICE_TOKEN_RELATIONS } from '../entities/baza-device-token.entity';
import { BazaDeviceTokenNotFoundException } from '../exceptions/baza-device-token-not-found.exception';
import { BazaDeviceTokenBadAccountTokenException } from '../exceptions/baza-device-token-bad-account-token.exception';
import { BazaDeviceTokenMapper } from '../mappers/baza-device-token.mapper';
import { BazaDeviceTokenRepository } from '../repositories/baza-device-token.repository';
import { BazaDeviceTokenAppleStrategy } from '../strategy/apple/baza-device-token-apple.strategy';
import { BazaDeviceTokenStrategy } from '../strategy/baza-device-token.strategy';
import { BazaDeviceTokenFcmStrategy } from '../strategy/fcm/baza-device-token-fcm.strategy';

@Injectable()
export class BazaDeviceTokenService {
    constructor(
        private readonly crud: CrudService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly accountMapper: BazaAccountMapper,
        private readonly deviceTokenRepository: BazaDeviceTokenRepository,
        private readonly deviceTokenMapper: BazaDeviceTokenMapper,
        private readonly deviceTokenAppleStrategy: BazaDeviceTokenAppleStrategy,
        private readonly deviceTokenFcmStrategy: BazaDeviceTokenFcmStrategy,
    ) {}

    /**
     * Gets list of all device tokens that belong to a specific user sorted by updatedAt timestamp
     * @param request CrudListRequestDto<BazaDeviceTokenDto>
     * @returns CrudListResponseDto<BazaDeviceTokenDto>
     */
    async getDeviceTokens(request: BazaDeviceTokenGetAllRequest): Promise<BazaDeviceTokenGetAllResponse> {
        const account = await this.accountRepository.getActiveAccountWithId(request.accountId);

        const listResponse = await this.crud.find<AccountDeviceTokenEntity, BazaDeviceTokenDto>({
            request,
            entity: AccountDeviceTokenEntity,
            mapper: async (items) => this.deviceTokenMapper.entitiesToDTOs(items),
            findOptions: {
                relations: BAZA_DEVICE_TOKEN_RELATIONS,
                where: [
                    {
                        account,
                    },
                ],
                order: {
                    updatedAt: 'DESC',
                },
            },
        });

        return {
            ...listResponse,
            account: this.accountMapper.entityToDto(account),
        };
    }

    /**
     * Tries to link device token using the BazaDeviceTokenStrategy.
     * If the token exists and doesn't belong to the passed account, it throws a bad request exception
     * If the token exists and belongs to the passed account, it refreshes its updatedAt timestamp
     * @param BazaDeviceTokenLinkRequest.accountId User account ID
     * @param BazaDeviceTokenLinkRequest.type FCM / Apple
     * @param BazaDeviceTokenLinkRequest.token FCM device token / Apple device token
     * @returns Linked / refreshed token
     */
    async linkDeviceToken({ accountId, type, token }: BazaDeviceTokenLinkRequest): Promise<BazaDeviceTokenLinkResponse> {
        const account = await this.accountRepository.getActiveAccountWithId(accountId);

        const deviceToken = await this.deviceTokenRepository.getDeviceToken(type, token);

        if (deviceToken && deviceToken.account.id !== account.id) {
            throw new BazaDeviceTokenBadAccountTokenException(deviceToken.account.id);
        }

        const linkedDeviceToken = await this.getStrategy(type).linkDeviceToken({ account, token });

        return this.deviceTokenMapper.entityToDto(linkedDeviceToken);
    }

    /**
     * Tries to unlink device token
     * @param BazaDeviceTokenLinkRequest.accountId User account ID
     * @param BazaDeviceTokenLinkRequest.type FCM / Apple
     * @param BazaDeviceTokenLinkRequest.token FCM device token / Apple device token
     * @returns Unlinked token
     */
    async unlinkDeviceToken({ accountId, type, token }: BazaDeviceTokenUnlinkRequest): Promise<BazaDeviceTokenUnlinkResponse> {
        const deviceToken = await this.deviceTokenRepository.getDeviceTokenByAccount(accountId, type, token);

        if (!deviceToken) {
            throw new BazaDeviceTokenNotFoundException();
        }

        await this.deviceTokenRepository.deleteDeviceToken(deviceToken);

        return this.deviceTokenMapper.entityToDto(deviceToken);
    }

    /**
     * Tries to destroy account device tokens by scope
     * @param BazaDeviceTokenDestroyRequest.accountId User account ID
     * @param BazaDeviceTokenDestroyRequest.scope FCM | Apple | All
     * @param BazaDeviceTokenDestroyRequest.beforeDate Date where the latest update occurred
     * @returns number of destroyed device tokens
     */
    async destroyDeviceTokens({ accountId, scope, beforeDate }: BazaDeviceTokenDestroyRequest): Promise<BazaDeviceTokenDestroyResponse> {
        const affected = await this.deviceTokenRepository.destroyDeviceTokens(accountId, scope, beforeDate);

        return { affected };
    }

    private getStrategy(deviceTokenType: BazaDeviceTokenType): BazaDeviceTokenStrategy {
        const strategies: Record<BazaDeviceTokenType, BazaDeviceTokenStrategy> = {
            Apple: this.deviceTokenAppleStrategy,
            FCM: this.deviceTokenFcmStrategy,
        };

        return strategies[deviceTokenType as BazaDeviceTokenType];
    }
}
