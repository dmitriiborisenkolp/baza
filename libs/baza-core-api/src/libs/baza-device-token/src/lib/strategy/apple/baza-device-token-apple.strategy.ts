import { Injectable } from '@nestjs/common';
import { BazaDeviceTokenType } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../../../../../baza-account/src';
import { AccountDeviceTokenEntity } from '../../entities/baza-device-token.entity';
import { BazaDeviceTokenRepository } from '../../repositories/baza-device-token.repository';
import { BazaDeviceTokenStrategy } from '../baza-device-token.strategy';

@Injectable()
export class BazaDeviceTokenAppleStrategy implements BazaDeviceTokenStrategy {
    constructor(private readonly deviceTokenRepository: BazaDeviceTokenRepository) {}

    async linkDeviceToken(payload: { account: AccountEntity; token: string }): Promise<AccountDeviceTokenEntity> {
        const entity = new AccountDeviceTokenEntity();

        entity.type = BazaDeviceTokenType.Apple;
        entity.account = payload.account;
        entity.token = payload.token;

        return this.deviceTokenRepository.saveDeviceToken(entity);
    }
}
