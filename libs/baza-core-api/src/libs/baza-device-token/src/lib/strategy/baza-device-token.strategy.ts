import { AccountEntity } from '../../../../baza-account/src';
import { AccountDeviceTokenEntity } from '../entities/baza-device-token.entity';

export interface BazaDeviceTokenStrategy {
    linkDeviceToken(payload: { account: AccountEntity; token: string }): Promise<AccountDeviceTokenEntity>;
}
