import { HttpStatus } from '@nestjs/common';
import { BazaDeviceTokenErrorCodes, bazaDeviceTokenErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class BazaDeviceTokenNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaDeviceTokenErrorCodes.BazaDeviceTokenNotFound,
            bazaDeviceTokenErrorCodesI18n[BazaDeviceTokenErrorCodes.BazaDeviceTokenNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
