import { HttpStatus } from '@nestjs/common';
import { BazaDeviceTokenErrorCodes, bazaDeviceTokenErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class BazaDeviceTokenBadAccountTokenException extends BazaAppException {
    constructor(accountId: number) {
        super(
            BazaDeviceTokenErrorCodes.BazaDeviceTokenBadAccountToken,
            bazaDeviceTokenErrorCodesI18n[BazaDeviceTokenErrorCodes.BazaDeviceTokenBadAccountToken].replace(
                '{{accountId}}',
                accountId.toString(),
            ),
            HttpStatus.BAD_REQUEST,
        );
    }
}
