import { CallHandler, ExecutionContext, Injectable, NestInterceptor, Scope } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { BazaDeviceTokenBulkCommand, extractDeviceTokensFromRequest } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { AuthSessionService } from '../../../../../libs/baza-auth/src';

@Injectable({
    scope: Scope.REQUEST,
})
export class BazaDeviceTokenInterceptor implements NestInterceptor {
    constructor(private readonly commandBus: CommandBus, private readonly authSession: AuthSessionService) {}

    /**
     * Intercepts all HTTP requests if there's an active user session
     * and checks HTTP headers to see if a device token should be
     * linked or unliked from the current user account.
     *
     * @param context Interface describing details about the current request pipeline.
     * @param next Interface providing access to the response stream.
     * @returns Observable representing the response stream from the route handler.
     */
    async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<unknown>> {
        if (!this.authSession.hasSession) {
            return next.handle();
        }

        const request = context.switchToHttp().getRequest();

        const actionSets = extractDeviceTokensFromRequest(request);

        await this.commandBus.execute(
            new BazaDeviceTokenBulkCommand({
                accountId: this.authSession.accountId,
                actionSets,
            }),
        );

        return next.handle();
    }
}
