import { Injectable } from '@nestjs/common';
import { BazaDeviceTokenDto } from '@scaliolabs/baza-core-shared';
import { AccountDeviceTokenEntity } from '../entities/baza-device-token.entity';

/**
 * AccountDeviceTokenEntity to BazaDeviceTokenDto mapper
 * @see AccountDeviceTokenEntity
 * @see BazaDeviceTokenDto
 */
@Injectable()
export class BazaDeviceTokenMapper {
    entityToDto(entity: AccountDeviceTokenEntity): BazaDeviceTokenDto {
        return {
            id: entity?.id,
            createdAt: entity?.createdAt,
            updatedAt: entity?.updatedAt,
            accountId: entity?.account?.id,
            token: entity?.token,
            type: entity?.type,
        };
    }

    entitiesToDTOs(entities: Array<AccountDeviceTokenEntity>): Array<BazaDeviceTokenDto> {
        return entities.map((e) => this.entityToDto(e));
    }
}
