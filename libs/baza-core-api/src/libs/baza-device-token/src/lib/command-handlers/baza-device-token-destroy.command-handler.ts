import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaDeviceTokenDestroyCommand } from '@scaliolabs/baza-core-shared';
import { BazaDeviceTokenService } from '../services/baza-device-token.service';
import { cqrsCommand } from '../../../../baza-common/src';

/**
 * CommandHandler that can destroy device tokens dynamically.
 *
 * @decorator `@CommandHandler(BazaDeviceTokenDestroyCommand)`
 */
@CommandHandler(BazaDeviceTokenDestroyCommand)
export class BazaDeviceTokenDestroyCommandHandler implements ICommandHandler<BazaDeviceTokenDestroyCommand> {
    constructor(private readonly deviceTokenService: BazaDeviceTokenService) {}

    async execute(command: BazaDeviceTokenDestroyCommand): Promise<void> {
        return cqrsCommand<void>(BazaDeviceTokenDestroyCommandHandler.name, async () => {
            await this.deviceTokenService.destroyDeviceTokens({
                accountId: command.request?.accountId,
                scope: command.request?.scope,
                beforeDate: command.request?.beforeDate,
            });
        });
    }
}
