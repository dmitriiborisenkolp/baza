import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaDeviceTokenBulkCommand } from '@scaliolabs/baza-core-shared';
import { BazaAccountRepository } from '../../../../../libs/baza-account/src';
import { BazaDeviceTokenService } from '../services/baza-device-token.service';
import { cqrsCommand } from '../../../../baza-common/src';

/**
 * CommandHandler which will validate HTTP request headers to see
 * if a device token should be link or unlinked from the user account.
 *
 * Unhandled exceptions are ignored to prevent clients from receiving unwanted
 * error responses (e.g. non-existent device token trying to be unlinked wouldn't return 404 to the client)
 *
 * @decorator `@CommandHandler(BazaDeviceTokenBulkCommand)`
 */
@CommandHandler(BazaDeviceTokenBulkCommand)
export class BazaDeviceTokenBulkCommandHandler implements ICommandHandler<BazaDeviceTokenBulkCommand> {
    constructor(private readonly accountRepository: BazaAccountRepository, private readonly deviceTokenService: BazaDeviceTokenService) {}

    async execute({ request: { accountId, actionSets } }: BazaDeviceTokenBulkCommand): Promise<void> {
        return cqrsCommand<void>(BazaDeviceTokenBulkCommandHandler.name, async () => {
            const account = await this.accountRepository.findActiveAccountWithId(accountId);

            if (!account) {
                return;
            }

            try {
                for (const [type, token] of actionSets.link) {
                    await this.deviceTokenService.linkDeviceToken({ accountId, type, token });
                }

                for (const [type, token] of actionSets.unlink) {
                    await this.deviceTokenService.unlinkDeviceToken({ accountId, type, token });
                }
            } catch {
                return;
            }
        });
    }
}
