import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaDeviceTokenCmsEndpoint,
    BazaDeviceTokenCmsEndpointPaths,
    CoreOpenApiTags,
    BazaDeviceTokenGetAllRequest,
    BazaDeviceTokenGetAllResponse,
    BazaDeviceTokenLinkResponse,
    BazaDeviceTokenUnlinkResponse,
    BazaDeviceTokenLinkRequest,
    BazaDeviceTokenUnlinkRequest,
} from '@scaliolabs/baza-core-shared';
import { WithAccessGuard } from '../../../../../libs/baza-acl-guards/src';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../../libs/baza-auth/src';
import { BazaDeviceTokenService } from '../services/baza-device-token.service';

@Controller()
@ApiBearerAuth()
@ApiTags(CoreOpenApiTags.BazaAccountCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
export class BazaDeviceTokenCmsController implements BazaDeviceTokenCmsEndpoint {
    constructor(private readonly deviceTokenService: BazaDeviceTokenService) {}

    @Post(BazaDeviceTokenCmsEndpointPaths.GetAll)
    @ApiOperation({
        summary: 'Get user device tokens',
        description: 'Attempts to get list of device tokens that belongs to a specific account',
    })
    @ApiOkResponse()
    getAll(@Body() request: BazaDeviceTokenGetAllRequest): Promise<BazaDeviceTokenGetAllResponse> {
        return this.deviceTokenService.getDeviceTokens(request);
    }

    @Post(BazaDeviceTokenCmsEndpointPaths.Link)
    @ApiOperation({
        summary: 'Link device token',
        description: 'Attempts to link device token to a specific user account',
    })
    @ApiOkResponse()
    link(@Body() request: BazaDeviceTokenLinkRequest): Promise<BazaDeviceTokenLinkResponse> {
        return this.deviceTokenService.linkDeviceToken(request);
    }

    @Post(BazaDeviceTokenCmsEndpointPaths.Unlink)
    @ApiOperation({
        summary: 'Unlink device token',
        description: 'Attempts to unlink device token from a specific user account',
    })
    @ApiOkResponse()
    unlink(@Body() request: BazaDeviceTokenUnlinkRequest): Promise<BazaDeviceTokenUnlinkResponse> {
        return this.deviceTokenService.unlinkDeviceToken(request);
    }
}
