import { Module, Global, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaCrudApiModule } from '../../../baza-crud/src';
import { EnvService } from '../../../baza-env/src';
import { BazaDeviceTokenDestroyCommandHandler } from './command-handlers/baza-device-token-destroy.command-handler';
import { BazaDeviceTokenBulkCommandHandler } from './command-handlers/baza-device-token-bulk.command-handler';
import { BazaDeviceTokenCmsController } from './controllers/baza-device-token-cms.controller';
import { BazaDeviceTokenInterceptor } from './interceptors/baza-device-token.interceptor';
import { BazaDeviceTokenMapper } from './mappers/baza-device-token.mapper';
import { BazaDeviceTokenRepository } from './repositories/baza-device-token.repository';
import { BazaDeviceTokenService } from './services/baza-device-token.service';
import { BazaDeviceTokenAppleStrategy } from './strategy/apple/baza-device-token-apple.strategy';
import { BazaDeviceTokenFcmStrategy } from './strategy/fcm/baza-device-token-fcm.strategy';
import { BazaDeviceTokenCleanUpAppleWatcher } from './watchers/baza-device-token-clean-up-apple.watcher';
import { BazaDeviceTokenCleanUpFcmWatcher } from './watchers/baza-device-token-clean-up-fcm.watcher';

const REPOSITORIES = [BazaDeviceTokenRepository];

const MAPPERS = [BazaDeviceTokenMapper];

const STRATEGIES = [BazaDeviceTokenAppleStrategy, BazaDeviceTokenFcmStrategy];

const SERVICES = [BazaDeviceTokenService];

const COMMAND_HANDLERS = [BazaDeviceTokenDestroyCommandHandler, BazaDeviceTokenBulkCommandHandler];

const WATCHERS = [BazaDeviceTokenCleanUpFcmWatcher, BazaDeviceTokenCleanUpAppleWatcher];

@Global()
@Module({
    imports: [CqrsModule, BazaCrudApiModule],
    controllers: [BazaDeviceTokenCmsController],
    providers: [
        ...REPOSITORIES,
        ...MAPPERS,
        ...STRATEGIES,
        ...SERVICES,
        ...WATCHERS,
        ...COMMAND_HANDLERS,
        {
            provide: APP_INTERCEPTOR,
            useClass: BazaDeviceTokenInterceptor,
        },
    ],
    exports: [...REPOSITORIES, ...MAPPERS, ...STRATEGIES, ...SERVICES, ...WATCHERS, ...COMMAND_HANDLERS],
})
export class BazaDeviceTokenApiModule implements OnApplicationBootstrap, OnApplicationShutdown {
    constructor(
        private readonly envService: EnvService,
        private readonly deviceTokenCleanUpFcmWatcher: BazaDeviceTokenCleanUpFcmWatcher,
        private readonly deviceTokenCleanUpAppleWatcher: BazaDeviceTokenCleanUpAppleWatcher,
    ) {}

    async onApplicationBootstrap(): Promise<void> {
        const ttlFcm = this.envService.getAsNumeric('BAZA_DEVICE_TOKEN_FCM_TTL_MINUTES');
        const ttlApple = this.envService.getAsNumeric('BAZA_DEVICE_TOKEN_APPLE_TTL_MINUTES');
        const logsEnabled = this.envService.getAsBoolean('BAZA_DEVICE_TOKEN_LOGS', { defaultValue: false });

        if (ttlFcm) {
            this.deviceTokenCleanUpFcmWatcher.logsEnabled = logsEnabled;
            await this.deviceTokenCleanUpFcmWatcher.start(ttlFcm);
        }

        if (ttlApple) {
            this.deviceTokenCleanUpAppleWatcher.logsEnabled = logsEnabled;
            await this.deviceTokenCleanUpAppleWatcher.start(ttlApple);
        }
    }

    async onApplicationShutdown(): Promise<void> {
        await this.deviceTokenCleanUpFcmWatcher.stop();
        await this.deviceTokenCleanUpAppleWatcher.stop();
    }
}
