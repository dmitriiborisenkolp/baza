import { AccountSettingsDto, BazaDeviceTokenType } from '@scaliolabs/baza-core-shared';
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '../../../../baza-account/src';

/**
 * Relations for AccountDeviceTokenEntity
 * @see AccountDeviceTokenEntity
 */
export const BAZA_DEVICE_TOKEN_RELATIONS = ['account'];

/**
 * Account Device Token Entity
 */
@Entity()
export class AccountDeviceTokenEntity<T = AccountSettingsDto> {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @ManyToOne(() => AccountEntity, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity<T>;

    @Column({
        default: new Date(),
    })
    createdAt: Date = new Date();

    @Column({
        default: new Date(),
    })
    updatedAt: Date = new Date();

    @Column({
        type: 'varchar',
        length: 5,
    })
    type: BazaDeviceTokenType;

    @Column({
        type: 'varchar',
        unique: true,
    })
    token: string;
}
