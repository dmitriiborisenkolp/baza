import 'reflect-metadata';
import { BazaAccountCmsNodeAccess, BazaDataAccessNode, BazaDeviceTokenCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaDeviceTokenType, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { ulid } from 'ulid';

describe('@scaliolabs/baza-core-api/baza-device-token/integration-tests/01-baza-device-token-fcm.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessDeviceTokenCms = new BazaDeviceTokenCmsNodeAccess(http);

    const firstEmail = `e2e-${ulid()}@scal.io`;
    const firstPassword = 'Scalio#14544!';

    const secondEmail = `e2e-${ulid()}@scal.io`;
    const secondPassword = 'Scalio#14544!';

    const MOCK_FIRST_FCM_TOKEN = '1-mocked-fcm-device-token' as const;
    const MOCK_SECOND_FCM_TOKEN = '2-mocked-fcm-device-token' as const;
    const MOCK_NON_EXISTENT_FCM_TOKEN = 'non-existent-fcm-device-token' as const;

    let FIRST_ACCOUNT_ID: number;
    let SECOND_ACCOUNT_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will create the first e2e user account', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email: firstEmail,
            password: firstPassword,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        FIRST_ACCOUNT_ID = response.id;
    });

    it('will create the second e2e user account', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email: secondEmail,
            password: secondPassword,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        SECOND_ACCOUNT_ID = response.id;
    });

    it('will link FCM device token to the first user account', async () => {
        const response = await dataAccessDeviceTokenCms.link({
            accountId: FIRST_ACCOUNT_ID,
            type: BazaDeviceTokenType.FCM,
            token: MOCK_FIRST_FCM_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBeDefined();
        expect(response.createdAt).toBeDefined();
        expect(response.updatedAt).toBeDefined();
        expect(response.type).toBe(BazaDeviceTokenType.FCM);
        expect(response.token).toBe(MOCK_FIRST_FCM_TOKEN);
    });

    it('will unlink FCM device token from the first user account', async () => {
        const response = await dataAccessDeviceTokenCms.unlink({
            accountId: FIRST_ACCOUNT_ID,
            type: BazaDeviceTokenType.FCM,
            token: MOCK_FIRST_FCM_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toBeDefined();
        expect(typeof response).toBe('object');
    });

    it('will link first FCM device token once again to the first user account', async () => {
        const response = await dataAccessDeviceTokenCms.link({
            accountId: FIRST_ACCOUNT_ID,
            type: BazaDeviceTokenType.FCM,
            token: MOCK_FIRST_FCM_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBeDefined();
        expect(response.createdAt).toBeDefined();
        expect(response.updatedAt).toBeDefined();
        expect(response.type).toBe(BazaDeviceTokenType.FCM);
        expect(response.token).toBe(MOCK_FIRST_FCM_TOKEN);
    });

    it('will refresh linked FCM device token attached to the first user account and update its timestamp', async () => {
        // Sleeps for 2s seconds to make sure the updatedAt is greater than createdAt
        await new Promise((resolve) => setTimeout(resolve, 2000));

        const response = await dataAccessDeviceTokenCms.link({
            accountId: FIRST_ACCOUNT_ID,
            type: BazaDeviceTokenType.FCM,
            token: MOCK_FIRST_FCM_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBeDefined();
        expect(response.createdAt).toBeDefined();
        expect(response.updatedAt).toBeDefined();
        expect(new Date(response.updatedAt) > new Date(response.createdAt)).toBeTruthy();
        expect(response.type).toBe(BazaDeviceTokenType.FCM);
        expect(response.token).toBe(MOCK_FIRST_FCM_TOKEN);
    });

    it('will link second FCM device token to the first user account', async () => {
        const response = await dataAccessDeviceTokenCms.link({
            accountId: FIRST_ACCOUNT_ID,
            type: BazaDeviceTokenType.FCM,
            token: MOCK_SECOND_FCM_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBeDefined();
        expect(response.createdAt).toBeDefined();
        expect(response.updatedAt).toBeDefined();
        expect(response.type).toBe(BazaDeviceTokenType.FCM);
        expect(response.token).toBe(MOCK_SECOND_FCM_TOKEN);
    });

    it('will throw if it tries to unlink non existent FCM token', async () => {
        const response = await dataAccessDeviceTokenCms.unlink({
            accountId: FIRST_ACCOUNT_ID,
            type: BazaDeviceTokenType.FCM,
            token: MOCK_NON_EXISTENT_FCM_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it(`will return all device tokens that belong to the first user account along with the account object`, async () => {
        const response = await dataAccessDeviceTokenCms.getAll({
            accountId: FIRST_ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(typeof response).toBe('object');

        expect(response.account).toBeDefined();
        expect(typeof response.account).toBe('object');

        expect(response.items).toBeDefined();
        expect(Array.isArray(response.items)).toBeTruthy();

        for (const item of response.items) {
            expect(item.id).toBeDefined();
            expect(item.createdAt).toBeDefined();
            expect(item.updatedAt).toBeDefined();
            expect(item.type).toBeDefined();
            expect(item.token).toBeDefined();
        }
    });

    it('will try to link an FCM token to the sencond account which belongs to the first account and it will throw', async () => {
        const response = await dataAccessDeviceTokenCms.link({
            accountId: SECOND_ACCOUNT_ID,
            type: BazaDeviceTokenType.FCM,
            token: MOCK_FIRST_FCM_TOKEN,
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will make sure the second account does not have device tokens linked', async () => {
        const response = await dataAccessDeviceTokenCms.getAll({
            accountId: SECOND_ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(typeof response).toBe('object');

        expect(response.account).toBeDefined();
        expect(typeof response.account).toBe('object');

        expect(response.items).toBeDefined();
        expect(Array.isArray(response.items)).toBeTruthy();
        expect(response.items).toHaveLength(0);
    });
});
