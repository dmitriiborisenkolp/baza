import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaDeviceTokenCmsNodeAccess,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaDeviceTokenType, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { ulid } from 'ulid';

describe('@scaliolabs/baza-core-api/baza-device-token/integration-tests/03-baza-device-token-destroy.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessDeviceTokenCms = new BazaDeviceTokenCmsNodeAccess(http);

    const email = `e2e-${ulid()}@scal.io`;
    const password = 'Scalio#14544!';

    const MOCK_DEVICE_TOKENS: Array<{ readonly type: BazaDeviceTokenType; readonly token: string }> = [
        { type: BazaDeviceTokenType.FCM, token: '1-mocked-fcm-device-token' },
        { type: BazaDeviceTokenType.FCM, token: '2-mocked-fcm-device-token' },
        { type: BazaDeviceTokenType.Apple, token: '1-mocked-apple-device-token' },
        { type: BazaDeviceTokenType.Apple, token: '2-mocked-apple-device-token' },
        { type: BazaDeviceTokenType.Apple, token: '3-mocked-apple-device-token' },
    ];

    let ACCOUNT_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });

        await http.authE2eAdmin();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will create a new e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email,
            password,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ACCOUNT_ID = response.id;
    });

    it('will link device token to the user account', async () => {
        for (const { type, token } of MOCK_DEVICE_TOKENS) {
            const response = await dataAccessDeviceTokenCms.link({ accountId: ACCOUNT_ID, type, token });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect(response.id).toBeDefined();
            expect(response.createdAt).toBeDefined();
            expect(response.updatedAt).toBeDefined();
            expect(response.type).toBe(type);
            expect(response.token).toBe(token);
        }
    });

    it('will successfully deactivate account', async () => {
        const response = await dataAccessAccountCms.deactivateAccount({ email });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isDeactivated).toBeTruthy();
        expect(response.firstName).toBe('Deleted');
        expect(response.lastName).toBe('');
        expect(response.fullName).toBe('Deleted');
    });

    it('will fail to find user account', async () => {
        const response = await dataAccessDeviceTokenCms.getAll({
            accountId: ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });
});
