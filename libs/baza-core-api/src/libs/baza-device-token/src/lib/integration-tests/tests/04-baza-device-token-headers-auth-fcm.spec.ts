import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaAuthNodeAccess,
    BazaDataAccessNode,
    BazaDeviceTokenCmsNodeAccess,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import {
    BazaCoreE2eFixtures,
    BazaDeviceTokenGetAllResponse,
    BazaDeviceTokenType,
    BazaHttpHeaders,
    isBazaErrorResponse,
} from '@scaliolabs/baza-core-shared';
import { ulid } from 'ulid';

describe('@scaliolabs/baza-core-api/baza-device-token/integration-tests/04-baza-device-token-headers-auth-fcm.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessDeviceTokenCms = new BazaDeviceTokenCmsNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    const email = `e2e-${ulid()}@scal.io`;
    const password = 'Scalio#14544!';

    const MOCK_FCM_TOKEN = '1-mocked-fcm-device-token' as const;

    let ACCOUNT_ID: number;
    let ACCESS_TOKEN: string;

    const signInWithHeaders = async (headers?: {
        [BazaHttpHeaders.HTTP_HEADER_LINK_FCM_TOKEN]?: string;
        [BazaHttpHeaders.HTTP_HEADER_LINK_APPLE_TOKEN]?: string;
    }) => {
        http.optionsForNextRequest = {
            withHeaders: headers,
        };

        await http.auth({ email, password });

        ACCESS_TOKEN = http.authResponse.accessToken;
    };

    const signOutWithHeaders = async (headers?: {
        [BazaHttpHeaders.HTTP_HEADER_UNLINK_FCM_TOKEN]?: string;
        [BazaHttpHeaders.HTTP_HEADER_UNLINK_APPLE_TOKEN]?: string;
    }) => {
        http.optionsForNextRequest = {
            withHeaders: headers,
        };

        await dataAccessAuth.invalidateToken({ jwt: ACCESS_TOKEN });
    };

    const getAccountDeviceTokens = async (accountId: number): Promise<BazaDeviceTokenGetAllResponse> => {
        await http.authE2eAdmin();

        const response = await dataAccessDeviceTokenCms.getAll({ accountId });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(typeof response).toBe('object');

        expect(response.account).toBeDefined();
        expect(typeof response.account).toBe('object');

        expect(response.items).toBeDefined();
        expect(Array.isArray(response.items)).toBeTruthy();

        return response;
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will create a new e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email,
            password,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ACCOUNT_ID = response.id;
    });

    it('will sign in with the newly created user and will link new FCM device token for that account', async () => {
        await signInWithHeaders({ 'X-Baza-Fcm-Token': MOCK_FCM_TOKEN });

        const { items: deviceTokens } = await getAccountDeviceTokens(ACCOUNT_ID);

        expect(deviceTokens).toHaveLength(1);
        expect(deviceTokens[0].token).toBe(MOCK_FCM_TOKEN);
        expect(deviceTokens[0].type).toBe(BazaDeviceTokenType.FCM);
    });

    it('will sign out from the current session and will unlink FCM device token', async () => {
        await signOutWithHeaders({ 'X-Baza-Fcm-Token-Unlink': MOCK_FCM_TOKEN });

        const { items: deviceTokens } = await getAccountDeviceTokens(ACCOUNT_ID);

        expect(deviceTokens).toHaveLength(0);
    });

    it('will sign in once again and will link FCM device token', async () => {
        await signInWithHeaders({ 'X-Baza-Fcm-Token': MOCK_FCM_TOKEN });

        const { items: deviceTokens } = await getAccountDeviceTokens(ACCOUNT_ID);

        expect(deviceTokens).toHaveLength(1);
        expect(deviceTokens[0].token).toBe(MOCK_FCM_TOKEN);
        expect(deviceTokens[0].type).toBe(BazaDeviceTokenType.FCM);
    });

    it('will not unlink device token if passed header is incorrect', async () => {
        await signOutWithHeaders({ 'X-Baza-Fcm-Token-Unlink': 'non-existent-device-token' });

        const { items: deviceTokens } = await getAccountDeviceTokens(ACCOUNT_ID);

        expect(deviceTokens).toHaveLength(1);
    });
});
