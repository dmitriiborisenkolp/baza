import { Injectable } from '@nestjs/common';
import { AccountSettingsDto, BazaDeviceTokenDestroyScope, BazaDeviceTokenType } from '@scaliolabs/baza-core-shared';
import { Connection, FindConditions, LessThan, Repository } from 'typeorm';
import { AccountDeviceTokenEntity, BAZA_DEVICE_TOKEN_RELATIONS } from '../entities/baza-device-token.entity';

@Injectable()
export class BazaDeviceTokenRepository<T = AccountSettingsDto> {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeOrm repository for AccountDeviceTokenEntity
     * @see AccountDeviceTokenEntity
     * @see Repository
     */
    get repository(): Repository<AccountDeviceTokenEntity<T>> {
        return this.connection.getRepository(AccountDeviceTokenEntity) as Repository<AccountDeviceTokenEntity<T>>;
    }

    /**
     * Gets AccountDeviceTokenEntity from the database
     * @param type FCM / Apple
     * @param token FCM device token / Apple device token
     * @returns AccountDeviceTokenEntity or undefined
     */
    async getDeviceToken(type: BazaDeviceTokenType, token: string): Promise<AccountDeviceTokenEntity<T> | undefined> {
        return this.repository.findOne({
            relations: BAZA_DEVICE_TOKEN_RELATIONS,
            where: {
                type,
                token,
            },
        });
    }

    /**
     * Gets AccountDeviceTokenEntity from the database by account
     * @param accountId User account ID
     * @param type FCM / Apple
     * @param token FCM device token / Apple device token
     * @returns AccountDeviceTokenEntity or undefined
     */
    async getDeviceTokenByAccount(
        accountId: number,
        type: BazaDeviceTokenType,
        token: string,
    ): Promise<AccountDeviceTokenEntity<T> | undefined> {
        return this.repository.findOne({
            where: {
                account: { id: accountId },
                type,
                token,
            },
        });
    }

    /**
     * Stores AccountDeviceTokenEntity in the database
     * @param entity AccountDeviceTokenEntity
     * @returns stored entity
     */
    async saveDeviceToken(entity: AccountDeviceTokenEntity<T>): Promise<AccountDeviceTokenEntity<T>> {
        entity.token = entity.token.trim();

        const accountDeviceToken = await this.getDeviceTokenByAccount(entity.account.id, entity.type, entity.token);

        if (accountDeviceToken) {
            return this.repository.save(
                {
                    ...accountDeviceToken,
                    token: entity.token,
                    updatedAt: new Date(),
                },
                {
                    reload: true,
                },
            );
        }

        return this.repository.save(entity, { reload: true });
    }

    /**
     * Deletes AccountDeviceTokenEntity from the database
     * @param entity AccountDeviceTokenEntity
     * @returns deleted entity
     */
    async deleteDeviceToken(entity: AccountDeviceTokenEntity<T>): Promise<AccountDeviceTokenEntity<T>> {
        await this.repository.remove(entity);

        return entity;
    }

    /**
     * Bulk deletes AccountDeviceTokenEntity from the database
     * @param accountId User account ID
     * @param scope FCM | Apple | All
     * @param beforeDate Date where the latest update occurred
     * @returns number of deleted device tokens
     */
    async destroyDeviceTokens(accountId?: number, scope?: BazaDeviceTokenDestroyScope, beforeDate?: Date): Promise<number> {
        const condition: FindConditions<AccountDeviceTokenEntity> = {};

        if (accountId) {
            condition.account = { id: accountId };
        }

        if (scope && scope in BazaDeviceTokenType) {
            condition.type = scope as BazaDeviceTokenType;
        }

        if (beforeDate) {
            condition.updatedAt = LessThan(beforeDate);
        }

        const result = await this.repository.delete(condition);

        return result.affected;
    }
}
