import { Global, Module } from '@nestjs/common';
import { BazaAwsModule } from '../../../baza-aws/src';
import { BazaHtmlService } from './services/baza-html.service';

@Global()
@Module({
    imports: [
         BazaAwsModule,
    ],
    providers: [
        BazaHtmlService,
    ],
    exports: [
        BazaHtmlService,
    ],
})
export class BazaHtmlApiModule {}
