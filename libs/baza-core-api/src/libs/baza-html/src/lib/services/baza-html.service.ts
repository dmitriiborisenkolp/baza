import { Injectable } from '@nestjs/common';
import { AwsHtmlHelper } from '../../../../baza-aws/src';

@Injectable()
export class BazaHtmlService {
    constructor(
        private readonly awsHtmlHelper: AwsHtmlHelper,
    ) {}

    async process(html: string): Promise<string> {
        return this.awsHtmlHelper.process(html);
    }
}
