import { BazaFeatures } from './models/baza-features';

export const BAZA_API_FEATURE_CONFIG = Symbol();

export class BazaApiFeatureConfig {
    /**
     * List of enabled features
     */
    enabled: BazaFeatures;
}
