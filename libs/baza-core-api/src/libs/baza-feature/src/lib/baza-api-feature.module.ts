import { DynamicModule, Global, Module } from '@nestjs/common';
import { BazaFeatureService } from './services/baza-feature.service';
import { BAZA_API_FEATURE_CONFIG, BazaApiFeatureConfig } from './baza-api-feature.config';
import { FeaturesRequiredGuard } from './guards/features-required.guard';

interface AsyncOptions {
    inject: Array<any>;
    imports: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaApiFeatureConfig>;
}

export { AsyncOptions as BazaApiFeatureModuleAsyncOptions };

@Module({})
export class BazaApiFeatureModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaApiFeatureGlobalModule,
            imports: options.imports,
            providers: [
                {
                    provide: BAZA_API_FEATURE_CONFIG,
                    inject: options.inject,
                    useFactory: options.useFactory,
                },
            ],
        };
    }
}

@Global()
@Module({
    providers: [
        FeaturesRequiredGuard,
        BazaFeatureService,
    ],
    exports: [
        FeaturesRequiredGuard,
        BazaFeatureService,
    ],
})
class BazaApiFeatureGlobalModule {}
