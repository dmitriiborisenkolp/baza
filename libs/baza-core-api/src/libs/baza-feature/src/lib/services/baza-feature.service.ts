import { Inject, Injectable } from '@nestjs/common';
import { BAZA_API_FEATURE_CONFIG, BazaApiFeatureConfig } from '../baza-api-feature.config';
import { BazaFeatures } from '../models/baza-features';
import { BazaLogger } from '../../../../baza-logger/src';
import { CommonFeatureDisabledException } from '../../../../baza-common/src';

@Injectable()
export class BazaFeatureService {
    private runtimeFeatures: BazaFeatures = {
        ...this.moduleConfig.enabled,
    };

    constructor(@Inject(BAZA_API_FEATURE_CONFIG) private readonly moduleConfig: BazaApiFeatureConfig, private readonly logger: BazaLogger) {
        this.logger.setContext(this.constructor.name);
    }

    enable(feature: string): void {
        this.runtimeFeatures[feature] = true;

        this.logger.log(`Feature "${feature}" enabled`);
    }

    disable(feature: string): void {
        this.runtimeFeatures[feature] = false;

        this.logger.log(`Feature "${feature}" disabled`);
    }

    isEnabled(feature: string): boolean {
        return !!this.runtimeFeatures[feature];
    }

    require(feature: string): void {
        if (!this.isEnabled(feature)) {
            throw new CommonFeatureDisabledException(feature);
        }
    }

    reset(): void {
        this.runtimeFeatures = {
            ...this.moduleConfig.enabled,
        };
    }
}
