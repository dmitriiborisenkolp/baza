import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { BazaFeatureService } from '../services/baza-feature.service';
import { FEATURES_REQUIRED_METADATA_KEY } from '../decorators/features-required.decorator';

@Injectable()
export class FeaturesRequiredGuard implements CanActivate {
    constructor(
        private readonly reflector: Reflector,
        private readonly service: BazaFeatureService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const requiredFeatures: Array<string> = [
            ...(this.reflector.get<Array<string>>(FEATURES_REQUIRED_METADATA_KEY, context.getHandler()) || []),
            ...(this.reflector.get<Array<string>>(FEATURES_REQUIRED_METADATA_KEY, context.getClass()) || []),
        ];

        if (! requiredFeatures || (Array.isArray(requiredFeatures) && requiredFeatures.length === 0)) {
            return true;
        }

        for (const feature of requiredFeatures) {
            if (! this.service.isEnabled(feature)) {
                return false;
            }
        }

        return true;
    }
}
