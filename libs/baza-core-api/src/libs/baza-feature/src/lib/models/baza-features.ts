export interface BazaFeatures {
    [feature: string]: boolean;
}
