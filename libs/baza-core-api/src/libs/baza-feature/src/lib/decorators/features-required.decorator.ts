import { SetMetadata } from '@nestjs/common';

export const FEATURES_REQUIRED_METADATA_KEY = 'bazaFeaturesRequired';

export const FeaturesRequired = (...accessNodes: Array<Array<string>>) => SetMetadata(FEATURES_REQUIRED_METADATA_KEY, accessNodes);
