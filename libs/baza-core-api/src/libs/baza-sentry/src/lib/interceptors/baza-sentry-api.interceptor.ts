import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { BazaSentryApiService } from '../services/baza-sentry-api.service';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class BazaSentryApiInterceptor implements NestInterceptor {
    constructor(
        private readonly service: BazaSentryApiService,
    ) {}

    intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        // https://mantosz.medium.com/use-sentry-with-nestjs-6d255027d3e1
        return next
            .handle()
            .pipe(
                catchError((exception) => {
                    this.service.captureException(exception);

                    return throwError(exception);
                }),
            );
    }
}
