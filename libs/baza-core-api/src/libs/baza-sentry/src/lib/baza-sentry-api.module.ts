import * as Sentry from '@sentry/node';
import { DynamicModule, Global, Inject, Module, OnModuleInit } from '@nestjs/common';
import { BAZA_SENTRY_API_CONFIG, BazaSentryApiConfig } from './baza-sentry-api.config';
import { BazaSentryApiService } from './services/baza-sentry-api.service';
import { BazaSentryApiInterceptor } from './interceptors/baza-sentry-api.interceptor';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { BazaEnvModule, EnvService } from '../../../baza-env/src';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';

interface AsyncOptions {
    injects?: Array<any>;
    imports?: Array<any>;
    useFactory: (...injects) => Promise<BazaSentryApiConfig>;
}

export { AsyncOptions as BazaSentryApiModuleAsyncOptions };

@Module({})
export class BazaSentryApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaSentryApiGlobalModule,
            imports: options.imports,
            providers: [
                {
                    provide: BAZA_SENTRY_API_CONFIG,
                    inject: options.injects,
                    useFactory: options.useFactory,
                },
            ],
        };
    }
}

@Global()
@Module({
    imports: [
        BazaEnvModule,
    ],
    providers: [
        BazaSentryApiService,
        BazaSentryApiInterceptor,
        {
            provide: APP_INTERCEPTOR,
            useClass: BazaSentryApiInterceptor,
        },
    ],
    exports: [
        BazaSentryApiService,
        BazaSentryApiInterceptor,
    ],
})
class BazaSentryApiGlobalModule implements OnModuleInit {
    constructor(
        private readonly env: EnvService,
        @Inject(BAZA_SENTRY_API_CONFIG) private readonly moduleConfig: BazaSentryApiConfig,
    ) {}

    onModuleInit(): void {
        const dsn = this.moduleConfig.dsn;

        if (dsn && this.env.isSentryAllowedEnvironment) {
            Sentry.init({
                dsn: this.moduleConfig.dsn,
                environment: this.env.getAsEnum<BazaEnvironments>('BAZA_ENV', {
                    enum: BazaEnvironments,
                }),
                ...this.moduleConfig.nodeOptions,
            });
        }
    }
}
