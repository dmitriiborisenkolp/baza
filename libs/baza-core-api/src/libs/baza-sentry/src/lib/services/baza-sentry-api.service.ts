import { Inject, Injectable } from '@nestjs/common';
import { BAZA_SENTRY_API_CONFIG, BazaSentryApiConfig } from '../baza-sentry-api.config';
import * as Sentry from '@sentry/node';
import { EnvService } from '../../../../baza-env/src';
import { BazaAppException } from '../../../../baza-exceptions/src';

@Injectable()
export class BazaSentryApiService {
    constructor(
        @Inject(BAZA_SENTRY_API_CONFIG) private readonly moduleConfig: BazaSentryApiConfig,
        private readonly env: EnvService,
    ) {}

    get sentry() {
        return Sentry;
    }

    captureException(exception: BazaAppException | any): void {
        if (this.env.isTestEnvironment || this.env.isLocalEnvironment) {
            return;
        }

        if (exception instanceof BazaAppException) {
            if ((this.moduleConfig.ignoreErrors || []).includes((exception as BazaAppException).errorCode)) {
                return;
            }
        }

        Sentry.captureException(exception);
    }

    captureEvent(event: any): void {
        Sentry.captureException(event);
    }

    captureMessage(message: any): void {
        Sentry.captureMessage(message);
    }
}
