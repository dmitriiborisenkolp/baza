import { NodeOptions } from '@sentry/node';

export interface BazaSentryApiConfig {
    dsn: string;
    nodeOptions?: Partial<NodeOptions>;
    ignoreErrors?: Array<any>;
}

export const BAZA_SENTRY_API_CONFIG = Symbol();
