import { Kafka, Producer, Consumer } from 'kafkajs';

export type BazaKafkaApiConfig = BazaKafkaApiConfigDisabled | BazaKafkaApiConfigEnabled;

export interface BazaKafkaApiConfigDisabled {
    /**
     * Mark Kafka Integration as Disabled
     */
    enabled: false;
}

export interface BazaKafkaApiConfigEnabled {
    /**
     * Mark Kafka Integrations as Enabled
     */
    enabled: true;

    /**
     * Log events from Kafka
     */
    logEvents: boolean;

    /**
     * Automatically bootstrap listed Kafka topics
     */
    kafkaTopics: Array<string>;

    /**
     * Kafka Namespace
     * Namespace kafka events between multiple environments
     */
    kafkaNamespace?: string;

    /**
     * Kafka Connection Options
     */
    kafkaConnectionOptions: {
        clientId: string;
        clientGroupId: string;
        brokers: string;
    };
}

export interface BazaKafka {
    client: Kafka;
    producer: Producer;
    consumer: Consumer;
}

export const BAZA_API_KAFKA = Symbol();
export const BAZA_KAFKA_API_CONFIG = Symbol();
