import { Inject, Injectable } from '@nestjs/common';
import type { BazaKafka, BazaKafkaApiConfig } from '../baza-kafka-api.config';
import { BAZA_API_KAFKA, BAZA_KAFKA_API_CONFIG, BazaKafkaApiConfigEnabled } from '../baza-kafka-api.config';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApiEventSource, KafkaEvent } from '@scaliolabs/baza-core-shared';
import { BazaLogger, bazaLoggerConfig } from '../../../../baza-logger/src';
import { deepMask } from '../../../../baza-logger/src';

interface PublishOptions {
    /**
     * By defaults all events will be filtered by namespace.
     * Set true if every projects / every deployment target should receive target within same Kafka instance.
     */
    shared?: boolean;
}

const defaultPublishOptions: PublishOptions = {
    shared: false,
};

@Injectable()
export class KafkaService<TOPIC, PAYLOAD> {
    private _kafkaEvents: Subject<KafkaEvent<TOPIC, PAYLOAD> & { namespace: string }> = new Subject<
        KafkaEvent<TOPIC, PAYLOAD> & { namespace: string }
    >();

    constructor(
        @Inject(BAZA_KAFKA_API_CONFIG) private readonly moduleConfig: BazaKafkaApiConfig,
        @Inject(BAZA_API_KAFKA) private readonly bazaKafka: BazaKafka,
        private readonly logger: BazaLogger,
    ) {}

    async initPipeKafkaEvents(): Promise<void> {
        if (this.moduleConfig.enabled) {
            this.bazaKafka.consumer
                .run({
                    eachMessage: async (payload) => {
                        const message: KafkaEvent<TOPIC, PAYLOAD> & { namespace: string; source: ApiEventSource } = JSON.parse(
                            payload.message.value.toString(),
                        );
                        const source = (message.payload as any).source as ApiEventSource;

                        if (source === ApiEventSource.EveryNode) {
                            if (
                                !message.namespace ||
                                message.namespace === (this.moduleConfig as BazaKafkaApiConfigEnabled).kafkaNamespace
                            ) {
                                this._kafkaEvents.next({
                                    topic: payload.topic as any,
                                    payload: JSON.parse(payload.message.value.toString()),
                                    namespace: message.namespace,
                                });
                            }
                        }
                    },
                })
                .catch((err) => this.logger.error(`[BazaKafkaService] [CONSUMER] [EVERY_NODE] ${err}`));
        }
    }

    async initKafkaLogEvents(takeUntil$: Observable<void>): Promise<void> {
        if (this.moduleConfig.enabled && this.moduleConfig.logEvents) {
            this._kafkaEvents.pipe(takeUntil(takeUntil$)).subscribe((event) => {
                this.logger.log(`[BazaKafkaService] [KAFKA EVENT] [EVERY_NODE] ${event.topic}`);
                this.logger.log(deepMask(event.payload, bazaLoggerConfig().masked));
            });
        }
    }

    get kafkaEvents$(): Observable<KafkaEvent<TOPIC, PAYLOAD> & { namespace: string }> {
        return this._kafkaEvents.asObservable();
    }

    async publish(message: KafkaEvent<TOPIC, PAYLOAD>, options: PublishOptions = defaultPublishOptions): Promise<void> {
        if (this.moduleConfig.enabled) {
            await this.bazaKafka.producer.send({
                topic: message.topic as any,
                messages: [
                    {
                        value: JSON.stringify({
                            ...message.payload,
                            namespace: !options.shared ? this.moduleConfig.kafkaNamespace : undefined,
                        }),
                    },
                ],
            });
        }
    }

    async disconnect(): Promise<void> {
        if (this.moduleConfig.enabled) {
            this.logger.log('[KafkaService] [BazaKafkaService] disconnect from kafka');

            if (this.bazaKafka.producer) {
                await this.bazaKafka.producer.disconnect();
            }

            if (this.bazaKafka.consumer) {
                await this.bazaKafka.consumer.disconnect();
            }
        }
    }
}
