import { Inject, Injectable } from '@nestjs/common';
import type { BazaKafka, BazaKafkaApiConfig } from '../baza-kafka-api.config';
import { BAZA_API_KAFKA, BAZA_KAFKA_API_CONFIG } from '../baza-kafka-api.config';
import { BazaLogger } from '../../../../baza-logger/src';

@Injectable()
export class KafkaAdminService {
    constructor(
        @Inject(BAZA_KAFKA_API_CONFIG) private readonly moduleConfig: BazaKafkaApiConfig,
        @Inject(BAZA_API_KAFKA) private readonly bazaKafka: BazaKafka,
        private readonly logger: BazaLogger,
    ) {}

    get kafka(): BazaKafka {
        return this.bazaKafka;
    }

    async bootstrapTopics(): Promise<void> {
        if (this.moduleConfig.enabled) {
            this.logger.log(`[BazaKafkaAdminService] [bootstrapTopics] Bootstrap topics`);

            try {
                await this.bazaKafka.client.admin().createTopics({
                    topics: this.moduleConfig.kafkaTopics.map((topic) => {
                        return {
                            topic,
                        };
                    }),
                });
            } catch (err) {
                this.logger.warn(`[BazaKafkaAdminService] [bootstrapTopics] Bootstrap failed, maybe because they are already exists`);
                this.logger.warn(err);
            }
        } else {
            this.logger.log(`[BazaKafkaAdminService] [bootstrapTopics] Kafka is disabled; skip`);
        }
    }
}
