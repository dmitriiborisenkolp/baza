import { DynamicModule, Global, Module, OnApplicationShutdown, OnModuleInit } from '@nestjs/common';
import { BAZA_API_KAFKA, BAZA_KAFKA_API_CONFIG, BazaKafka } from './baza-kafka-api.config';
import type { BazaKafkaApiConfig } from './baza-kafka-api.config';
import { Kafka } from 'kafkajs';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { KafkaService } from './services/kafka.service';
import { KafkaAdminService } from './services/kafka-admin.service';
import { Subject } from 'rxjs';

const UNIQUE_HEX_ID_LENGTH = 8;

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaKafkaApiConfig>;
}

export { AsyncOptions as BazaKafkaApiModuleAsyncOptions };

@Module({})
export class BazaKafkaApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaKafkaApiGlobalModule,
            providers: [
                {
                    provide: BAZA_KAFKA_API_CONFIG,
                    inject: options.injects,
                    useFactory: async (...injects) => options.useFactory(...injects),
                },
                {
                    provide: BAZA_API_KAFKA,
                    inject: options.injects,
                    useFactory: async (...injects) => {
                        const moduleConfig = await options.useFactory(...injects);

                        if (!moduleConfig.enabled) {
                            return null;
                        }

                        const uniqueClientId = generateRandomHexString(UNIQUE_HEX_ID_LENGTH);
                        const uniqueGroupId = generateRandomHexString(UNIQUE_HEX_ID_LENGTH);

                        const client = new Kafka({
                            clientId: `${moduleConfig.kafkaConnectionOptions.clientId}-${uniqueClientId}`,
                            brokers: moduleConfig.kafkaConnectionOptions.brokers.split(','),
                        });

                        await client.admin().createTopics({
                            topics: moduleConfig.kafkaTopics.map((topic) => {
                                return {
                                    topic,
                                };
                            }),
                        });

                        const producer = client.producer();
                        const consumer = client.consumer({
                            groupId: `${moduleConfig.kafkaConnectionOptions.clientGroupId}-${uniqueGroupId}`,
                            allowAutoTopicCreation: true,
                        });

                        const bazaKafka: BazaKafka = {
                            client,
                            producer,
                            consumer,
                        };

                        await bazaKafka.producer.connect();
                        await bazaKafka.consumer.connect();

                        for (const topic of moduleConfig.kafkaTopics) {
                            await consumer.subscribe({
                                topic,
                                fromBeginning: false,
                            });
                        }

                        return bazaKafka;
                    },
                },
            ],
        };
    }
}

@Global()
@Module({
    providers: [KafkaService, KafkaAdminService],
    exports: [KafkaService, KafkaAdminService],
})
export class BazaKafkaApiGlobalModule implements OnModuleInit, OnApplicationShutdown {
    private readonly onApplicationShutdown$: Subject<void> = new Subject<void>();

    constructor(private readonly kafkaService: KafkaService<any, any>) {}

    async onModuleInit(): Promise<void> {
        await this.kafkaService.initPipeKafkaEvents();
        await this.kafkaService.initKafkaLogEvents(this.onApplicationShutdown$);
    }

    async onApplicationShutdown(): Promise<void> {
        this.onApplicationShutdown$.next();

        await this.kafkaService.disconnect();
    }
}
