export * from './lib/detector/default-project-language.detector';
export * from './lib/detector/i18n-header.detector';

export * from './lib/baza-i18n-api-detectors.service';
export * from './lib/baza-i18n-api-detectors.config';
export * from './lib/baza-i18n-api-detectors.module';
