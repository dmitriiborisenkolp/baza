import { Inject, Injectable } from '@nestjs/common';
import { Request as ExpressRequest } from 'express';
import { BAZA_I18N_API_DETECTORS_CONFIG } from '../baza-i18n-api-detectors.config';
import type { BazaI18nApiDetectorsConfig } from '../baza-i18n-api-detectors.config';
import { Application, ProjectLanguage } from '@scaliolabs/baza-core-shared';
import { BazaI18nDetector } from '../../../../baza-i18n/src';

@Injectable()
export class I18nHeaderDetector implements BazaI18nDetector {
    constructor(
        @Inject(BAZA_I18N_API_DETECTORS_CONFIG) private readonly moduleConfig: BazaI18nApiDetectorsConfig,
    ) {}

    async detectLanguage(app: Application, expressRequest: ExpressRequest): Promise<ProjectLanguage | undefined> {
        return expressRequest.header(this.moduleConfig.headerDetectorConfig.xI18nLanguageHeader) as any;
    }
}
