import { Injectable } from '@nestjs/common';
import { BazaI18nDetector } from '../../../baza-i18n/src';
import { DefaultProjectLanguageDetector } from './detector/default-project-language.detector';
import { I18nHeaderDetector } from './detector/i18n-header.detector';

@Injectable()
export class BazaI18nApiDetectorsService {
    constructor(
        private readonly i18nHeaderDetector: I18nHeaderDetector,
        private readonly defaultProjectLanguageDetector: DefaultProjectLanguageDetector,
    ) {
    }

    get defaultDetectors(): Array<BazaI18nDetector> {
        return [
            this.i18nHeaderDetector,
            this.defaultProjectLanguageDetector,
        ];
    }
}
