import { DynamicModule, Global, Module, OnModuleInit } from '@nestjs/common';
import { DefaultProjectLanguageDetector } from './detector/default-project-language.detector';
import { I18nHeaderDetector } from './detector/i18n-header.detector';
import { BAZA_I18N_API_DETECTORS_CONFIG, BazaI18nApiDetectorsConfig } from './baza-i18n-api-detectors.config';
import { BazaI18nApiDetectorsService } from './baza-i18n-api-detectors.service';
import { BazaAccountApiModule } from '../../../baza-account/src/lib/baza-account-api.module';
import { I18nLanguageService } from '../../../baza-i18n/src';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaI18nApiDetectorsConfig>;
}

export const BAZA_I18N_DEFAULT_DETECTORS = [
    DefaultProjectLanguageDetector,
    I18nHeaderDetector,
];

export { AsyncOptions as BazaI18nApiDetectorsModuleAsyncOptions };

@Module({})
export class BazaI18nApiDetectorsModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaI18nApiDetectorsGlobalModule,
            providers: [{
                provide: BAZA_I18N_API_DETECTORS_CONFIG,
                inject: options.injects,
                useFactory: options.useFactory,
            }],
            exports: [
                BAZA_I18N_API_DETECTORS_CONFIG,
            ],
        };
    }
}

@Global()
@Module({
    imports: [
        BazaAccountApiModule,
    ],
    providers: [
        ...BAZA_I18N_DEFAULT_DETECTORS,
        BazaI18nApiDetectorsService,
    ],
    exports: [
        ...BAZA_I18N_DEFAULT_DETECTORS,
        BazaI18nApiDetectorsService,
    ],
})
class BazaI18nApiDetectorsGlobalModule implements OnModuleInit {
    constructor(
        private readonly i18nLanguage: I18nLanguageService,
        private readonly i18nDefaultDetectors: BazaI18nApiDetectorsService,
    ) {}

    async onModuleInit(): Promise<void> {
        this.i18nLanguage.detectors = this.i18nDefaultDetectors.defaultDetectors;
    }
}
