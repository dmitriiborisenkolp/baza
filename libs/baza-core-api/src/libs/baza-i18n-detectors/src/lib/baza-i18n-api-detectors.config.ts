import { Application, ProjectLanguage } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '../../../baza-account/src/lib/entities/account.entity';

export interface BazaI18nApiDetectorsConfig {
    headerDetectorConfig: {
        xI18nLanguageHeader: string;
    },
    accountDetectorConfig: {
        getLanguage: (app: Application, account: AccountEntity) => ProjectLanguage | undefined;
    },
    defaultProjectLanguage: {
        defaults: Array<{
            app: Application;
            defaultLanguage: ProjectLanguage;
        }>;
    }
}

export const BAZA_I18N_API_DETECTORS_CONFIG = Symbol();
