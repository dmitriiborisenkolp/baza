export enum EnvErrorCodes {
    EnvInstanceIsNotAvailable = 'EnvInstanceIsNotAvailable',
    EnvKeyIsNotAvailable = 'EnvKeyIsNotAvailable',
    EnvValueIsNotNumeric = 'EnvValueIsNotNumeric',
    EnvValueIsNotEnum = 'EnvValueIsNotEnum',
    EnvValueIsNotBoolean = 'EnvValueIsNotBoolean',
}
