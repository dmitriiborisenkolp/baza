import { HttpStatus } from '@nestjs/common';
import { EnvErrorCodes } from '../error-codes/env.error-codes';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Environment key "%s" is not available';

export class EnvKeyIsNotAvailableException extends BazaAppException {
    constructor(envKey: string) {
        super(
            EnvErrorCodes.EnvKeyIsNotAvailable,
            ERR_MESSAGE.split('%s').join(envKey),
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
