import { HttpStatus } from '@nestjs/common';
import { EnvErrorCodes } from '../error-codes/env.error-codes';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Environment value "%s" is not numeric';

export class EnvValueIsNotNumericException extends BazaAppException {
    constructor(envKey: string) {
        super(
            EnvErrorCodes.EnvValueIsNotNumeric,
            ERR_MESSAGE.split('%s').join(envKey),
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
