import { HttpStatus } from '@nestjs/common';
import { EnvErrorCodes } from '../error-codes/env.error-codes';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'BazaEnvService instance is not available yet';

export class EnvInstanceIsNotAvailableException extends BazaAppException {
    constructor() {
        super(
            EnvErrorCodes.EnvInstanceIsNotAvailable,
            ERR_MESSAGE,
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
