import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import { EnvValueIsNotNumericException } from '../exceptions/env-value-is-not-numeric.exception';
import { EnvKeyIsNotAvailableException } from '../exceptions/env-key-is-not-available.exception';
import { EnvValueIsNotBooleanException } from '../exceptions/env-value-is-not-boolean.exception';
import { EnvInstanceIsNotAvailableException } from '../exceptions/env-instance-is-not-available.exception';
import { BAZA_ENVIRONMENTS_CONFIG, BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaCommonEnvironments } from '../../../../baza-common/src';
import { EnvValueIsNotEnumException } from '../exceptions/env-value-is-not-enum.exception';

export interface GetOptions<T = any> {
    defaultValue?: T;
}
export interface GetAsNumericOptions {
    defaultValue?: number;
}
export interface GetAsBooleanOptions {
    defaultValue?: boolean;
}
export interface GetAsEnumOptions<ENUM = any> {
    enum: any /* TODO: Should be something like ComponentType<ENUM> */;
    defaultValue?: ENUM;
}

export const BOOLEAN_TRUE_ENV_VALUES = ['1', 'true', 'yes', 'y', true];
export const BOOLEAN_FALSE_ENV_VALUES = ['0', 'false', 'no', 'n', false];

export function isBooleanEnvTrue(input: string): boolean {
    return BOOLEAN_TRUE_ENV_VALUES.includes((input || '').toString().toLowerCase());
}

@Injectable()
export class EnvService<T = BazaCommonEnvironments.CombinedBazaEnvironments> {
    private _env: BazaEnvironments = process.env as any;

    private static __instance: EnvService;

    /**
     * Imports .env file from specific file path
     */
    static importEnvFile(request: { filePath: string }): void {
        dotenv.config({ path: request.filePath });
    }

    /**
     * Returns true of singleton service is available to use
     */
    static hasInstance(): boolean {
        return !!EnvService.__instance;
    }

    /**
     * Returns singleton instance of EnvService
     * @see hasInstance
     */
    static getInstance(): EnvService {
        if (!EnvService.hasInstance()) {
            throw new EnvInstanceIsNotAvailableException();
        }

        return this.__instance;
    }

    constructor() {
        EnvService.__instance = this as any;
    }

    get isMasterNode(): boolean {
        return this.getAsBoolean('BAZA_MASTER', {
            defaultValue: false,
        });
    }

    /**
     * Returns current environment
     */
    get current(): BazaEnvironments {
        return this.getAsEnum<BazaEnvironments, BazaCommonEnvironments.NodeEnvironment>('BAZA_ENV', {
            enum: BazaEnvironments,
            defaultValue: BazaEnvironments.Local,
        });
    }

    /**
     * Returns all environment variables
     */
    get all(): Record<string, string> {
        return { ...process.env };
    }

    /**
     * Returns true for E2E or Unit Test environments
     */
    get isTestEnvironment(): boolean {
        return BAZA_ENVIRONMENTS_CONFIG.testEnvironments.includes(this.current);
    }

    /**
     * Returns true for local environments
     */
    get isLocalEnvironment(): boolean {
        return BAZA_ENVIRONMENTS_CONFIG.localEnvironments.includes(this.current);
    }

    /**
     * Returns true for production environments
     * Production environments includes Production, UAT and Preprod environments
     * @see EnvService.isPublicProductionEnvironment
     * @see EnvService.isUATEnvironment
     */
    get isProductionEnvironment(): boolean {
        return BAZA_ENVIRONMENTS_CONFIG.productionEnvironments.includes(this.current);
    }

    /**
     * Returns true for UAT or Preprod environments
     */
    get isUATEnvironment(): boolean {
        return BAZA_ENVIRONMENTS_CONFIG.uatEnvironments.includes(this.current);
    }

    /**
     * Returns true for Real Production (i.e. Public Production) environments
     * This includes only Production environments, not UAT/Preprod
     * @see EnvService.isProductionEnvironment
     * @see EnvService.isUATEnvironment
     */
    get isPublicProductionEnvironment(): boolean {
        return BAZA_ENVIRONMENTS_CONFIG.publicProductionEnvironments.includes(this.current);
    }

    /**
     * Returns true for environment which should report errors to Sentry
     */
    get isSentryAllowedEnvironment(): boolean {
        return BAZA_ENVIRONMENTS_CONFIG.productionEnvironments.includes(this.current);
    }

    /**
     * Returns true if Watchers-like features (based on rxjs `interval`) are
     * allowed to work
     * We added it because TypeOrm can "resurrect" dead entities which are
     * flushed by /baza-e2e/flush endpoint
     */
    get areWatchersAllowed(): boolean {
        return !this.isTestEnvironment;
    }

    /**
     * Returns true if there is an environment defined with given name
     * @param envKey
     * @private
     */
    private has<T>(envKey: keyof T) {
        // eslint-disable-next-line no-prototype-builtins
        return this._env.hasOwnProperty(envKey);
    }

    /**
     * The method will be private in future.
     * Use getAsString, getAsNumber, getAsEnum or getAsBoolean methods to fetch environment values.
     * TODO:: Make as private
     *
     * @see EnvService.getAsString
     * @see EnvService.getAsNumber
     * @see EnvService.getAsEnum
     * @see EnvService.getAsBoolean
     * @deprecated
     */
    public get<E = BazaCommonEnvironments.CombinedBazaEnvironments>(
        envKey: keyof T | keyof E,
        options: GetOptions = {},
    ): string | boolean | undefined | null {
        const value = this._env[envKey as string];

        if (value === undefined) {
            // eslint-disable-next-line no-prototype-builtins
            if (!options.hasOwnProperty('defaultValue')) {
                throw new EnvKeyIsNotAvailableException(envKey as string);
            }

            return options.defaultValue;
        } else {
            return value;
        }
    }

    /**
     * Returns environment variable as string value
     */
    public getAsString<E = BazaCommonEnvironments.CombinedBazaEnvironments>(envKey: keyof T | keyof E, options: GetOptions = {}): string {
        return (this.get(envKey, options) || '').toString();
    }

    /**
     * Returns environment variable as ENUM value
     */
    public getAsEnum<ENUM, E = BazaCommonEnvironments.CombinedBazaEnvironments>(
        envKey: keyof T | keyof E,
        options: GetAsEnumOptions<ENUM>,
    ): ENUM {
        const result = this.get(envKey, options);

        if ((result as any) !== options.defaultValue && !Object.values(options.enum).includes(result)) {
            throw new EnvValueIsNotEnumException(envKey as any);
        }

        return result as any;
    }

    /**
     * Returns environment variable as numeric (integer or float) value
     */
    public getAsNumeric<E = BazaCommonEnvironments.CombinedBazaEnvironments>(
        envKey: keyof T | keyof E,
        options: GetAsNumericOptions = { defaultValue: undefined },
    ): number {
        const value = this.get(envKey, options);

        if (value === undefined) {
            // eslint-disable-next-line no-prototype-builtins
            if (!options.hasOwnProperty('defaultValue')) {
                throw new EnvKeyIsNotAvailableException(envKey as string);
            }

            return options.defaultValue;
        } else {
            if (typeof value === 'number') {
                if (isNaN(value)) {
                    throw new EnvValueIsNotNumericException(envKey as string);
                }

                return value;
            } else if (typeof value === 'string') {
                if (isNaN(parseInt(value, 10))) {
                    throw new EnvValueIsNotNumericException(envKey as string);
                }

                return parseInt(value, 10);
            } else {
                throw new EnvValueIsNotNumericException(envKey as string);
            }
        }
    }

    /**
     * Returns environment variable as Boolean value
     * @see BOOLEAN_TRUE_ENV_VALUES
     * @see BOOLEAN_FALSE_ENV_VALUES
     */
    public getAsBoolean<E = BazaCommonEnvironments.CombinedBazaEnvironments>(
        envKey: keyof T | keyof E,
        options: GetAsBooleanOptions = { defaultValue: undefined },
    ): boolean {
        const value = this.get(envKey, options);

        if (value === undefined) {
            // eslint-disable-next-line no-prototype-builtins
            if (!options.hasOwnProperty('defaultValue')) {
                throw new EnvKeyIsNotAvailableException(envKey as string);
            }

            return options.defaultValue;
        } else {
            if (typeof value === 'boolean') {
                return value;
            } else if ([...BOOLEAN_TRUE_ENV_VALUES, ...BOOLEAN_FALSE_ENV_VALUES].includes(value.toLowerCase())) {
                return isBooleanEnvTrue(value);
            } else {
                throw new EnvValueIsNotBooleanException(envKey as string);
            }
        }
    }
}
