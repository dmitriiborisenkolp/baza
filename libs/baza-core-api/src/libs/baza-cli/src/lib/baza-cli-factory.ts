import * as process from 'process';
import { BazaLogger } from '../../../baza-logger/src';
import { NestExpressApplication } from '@nestjs/platform-express';

export interface CliApplicationOptions {
    name: string;
    startMessage?: string;
    finishedMessage?: string;
}

const DEFAULT_START_MESSAGE = 'Run...';
const DEFAULT_FINISHED_MESSAGE = 'Completed!';

type Callback = (app: NestExpressApplication, logger: BazaLogger, exitWithError: (message: string) => void) => Promise<void>;

export { Callback as BazaCLICallback };

export function bazaCliFactory(apiFactory: () => Promise<NestExpressApplication>, options: CliApplicationOptions, callback: Callback) {
    const run = async () => {
        const app = await apiFactory();
        const logger = await app.resolve(BazaLogger);

        await app.init();

        logger.log(`[${options.name}] ${options.startMessage || DEFAULT_START_MESSAGE}`);

        await callback(app, logger, (message: string) => {
            if (message) {
                logger.error(message);
            }

            process.exit();
        });

        logger.log(`[${options.name}] ${options.finishedMessage || DEFAULT_FINISHED_MESSAGE}`);

        process.exit(0);
    };

    run().catch((err) => console.error(err));
}
