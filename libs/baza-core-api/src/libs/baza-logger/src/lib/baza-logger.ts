import { Injectable, Scope } from '@nestjs/common';
import { EventBus } from '@nestjs/cqrs';
import { BazaLoggerLogEvent } from './events/baza-logger-log.event';
import { BazaLoggerVerboseEvent } from './events/baza-logger-verbose.event';
import { BazaLoggerErrorEvent } from './events/baza-logger-error.event';
import { BazaLoggerDebugEvent } from './events/baza-logger-debug.event';
import { BazaLoggerWarnEvent } from './events/baza-logger-warn.event';
import { PinoLogger } from 'nestjs-pino';
import { EnvService } from '../../../baza-env/src';
import { deepMask } from './utils/deep-mask.util';
import { bazaLoggerConfig } from './config/baza-logger-config';
import { pinoLoggerConfigBuilder } from './pino-logger-config-builder';

@Injectable({
    scope: Scope.TRANSIENT,
})
export class BazaLogger extends PinoLogger {
    static eventBus: EventBus;

    constructor() {
        const envService = new EnvService();
        super(pinoLoggerConfigBuilder(envService));
    }

    log(message: unknown, context?: string) {
        if (context) {
            this.setContext(context);
        }

        super.info(deepMask(message, bazaLoggerConfig().masked));

        if (BazaLogger.eventBus) {
            BazaLogger.eventBus.publish(
                new BazaLoggerLogEvent({
                    createdAt: new Date(),
                    context,
                    message,
                }),
            );
        }
    }

    verbose(message: unknown, context?: string) {
        super.trace(deepMask(message, bazaLoggerConfig().masked));

        if (BazaLogger.eventBus) {
            BazaLogger.eventBus.publish(
                new BazaLoggerVerboseEvent({
                    createdAt: new Date(),
                    context,
                    message,
                }),
            );
        }
    }

    error(message: unknown, trace?: string, context?: string) {
        super.error(deepMask(message, bazaLoggerConfig().masked), trace);

        if (BazaLogger.eventBus) {
            BazaLogger.eventBus.publish(
                new BazaLoggerErrorEvent({
                    createdAt: new Date(),
                    context,
                    message,
                }),
            );
        }
    }

    debug(message: unknown, context?: string) {
        super.debug(message);

        if (BazaLogger.eventBus) {
            BazaLogger.eventBus.publish(
                new BazaLoggerDebugEvent({
                    createdAt: new Date(),
                    context,
                    message,
                }),
            );
        }
    }

    warn(message: unknown, context?: string) {
        super.warn(deepMask(message, bazaLoggerConfig().masked));

        if (BazaLogger.eventBus) {
            BazaLogger.eventBus.publish(
                new BazaLoggerWarnEvent({
                    createdAt: new Date(),
                    context,
                    message,
                }),
            );
        }
    }

    info(message: any, obj?: unknown, context?: string) {
        super.info(deepMask(obj, bazaLoggerConfig().masked), deepMask(message, bazaLoggerConfig().masked));

        if (BazaLogger.eventBus) {
            BazaLogger.eventBus.publish(
                new BazaLoggerLogEvent({
                    createdAt: new Date(),
                    context,
                    message,
                }),
            );
        }
    }
}
