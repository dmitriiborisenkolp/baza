import { Global, Module } from '@nestjs/common';
import { BazaLogger } from '../lib/baza-logger';

@Global()
@Module({
    providers: [BazaLogger],
    exports: [BazaLogger],
})
export class BazaLoggerModule {}
