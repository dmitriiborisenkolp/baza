import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { v4 as uuid, validate } from "uuid";

export const CorrelationId = createParamDecorator((data: unknown, context: ExecutionContext) => {
    try {
        const req = context.switchToHttp().getRequest();
        const correlationId = req?.headers?.['correlation-id'];
        return validate(correlationId) ? correlationId : uuid();
    } catch {
        return uuid();
    }
});
