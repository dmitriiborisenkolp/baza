import { EnvService } from '../../../baza-env/src';
import { BazaLoggerEnv } from './baza-logger.env';
import { bazaLoggerConfig } from './config/baza-logger-config';
import { deepMask } from './utils/deep-mask.util';

export const pinoLoggerConfigBuilder = (env: EnvService) => {
    const withPrettyLogs = env.getAsBoolean<BazaLoggerEnv>('BAZA_PRETTY_LOGS', {
        defaultValue: false,
    });

    const level = env.getAsString<BazaLoggerEnv>('BAZA_LOGGER_MIN_LEVEL', {
        defaultValue: 'debug',
    });

    return {
        pinoHttp: {
            transport: withPrettyLogs
                ? {
                      target: 'pino-pretty',
                  }
                : undefined,
            level,
            serializers: {
                req: (req) => deepMask(req, bazaLoggerConfig().masked),
                res: (res) => deepMask(res, bazaLoggerConfig().masked),
            },
        },
    };
};
