import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param accountName
 * @returns
 * Mask account name - will mask all but first letter of every word
 */
export const accountNameMask: StrategyFunction = (accountName: string): string => {
    return !accountName
        ? accountName
        : accountName
              .split(' ')
              .map((word) => {
                  return word.slice(0, 1) + DEFAULT_MASK_CHARACTER.repeat(word.length - 1);
              })
              .join(' ');
};
