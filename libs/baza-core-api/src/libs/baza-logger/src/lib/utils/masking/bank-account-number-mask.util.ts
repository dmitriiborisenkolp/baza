import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param bankAccountNumber
 * @returns
 * Mask bank account number - will mask all but last 4 digits
 */
export const bankAccountNumberMask: StrategyFunction = (bankAccountNumber: string): string => {
    return !bankAccountNumber || bankAccountNumber.length < 4
        ? bankAccountNumber
        : DEFAULT_MASK_CHARACTER.repeat(bankAccountNumber.length - 4) + bankAccountNumber.slice(bankAccountNumber.length - 4);
};
