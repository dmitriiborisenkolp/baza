import { maskString } from '@scaliolabs/baza-core-shared';
import { BazaLoggerMaskConfig } from '../config/baza-logger-config';

/**
 *
 * @param input
 * @param fields
 * @returns
 * will loop through a nested array/object and mask every key passed in the fields param
 */
export function deepMask(input: any, fields: Array<BazaLoggerMaskConfig>): any {
    if (Array.isArray(input) && input.length > 0) {
        const cloned = [];

        for (let i = 0; i < input.length; i++) {
            cloned[i] = deepMask(input[i], fields);
        }

        return cloned;
    } else if (Array.isArray(input) && input.length === 0) {
        return [];
    } else if (!!input && typeof input === 'object') {
        const cloned = {};

        for (const field of Object.keys(input)) {
            const fieldConfig: BazaLoggerMaskConfig = fields.find((c) => c.field.toLowerCase() === field.toLowerCase());

            if (fieldConfig) {
                cloned[field] = maskString((input[field] || '').toString(), {
                    strategy: fieldConfig.strategy,
                });
            } else {
                cloned[field] = deepMask(input[field], fields);
            }
        }

        return cloned;
    } else {
        return input;
    }
}
