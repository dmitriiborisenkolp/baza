import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param cvv
 * @returns
 * Mask CVV - will mask everything
 */
export const CVVMask: StrategyFunction = (cvv: string): string => {
    return cvv.replace(/./g, DEFAULT_MASK_CHARACTER);
};
