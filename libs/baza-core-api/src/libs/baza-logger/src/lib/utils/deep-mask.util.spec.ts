import 'reflect-metadata';
import { deepMask } from './deep-mask.util';
import { BazaLoggerMaskConfig } from '../config/baza-logger-config';
import { bankAccountNumberMask, bankAccountRoutingMask, fullNameMask, ssnMask } from './masking';

interface TestCase {
    input: any;
    output: any;
}

describe('@scaliolabs/baza-core-api/src/libs/baza-logger/src/lib/utils/deep-mask.util.spec.ts', () => {
    const config: Array<BazaLoggerMaskConfig> = [
        {
            field: 'ssn',
            strategy: ssnMask,
        },
        {
            field: 'fullname',
            strategy: fullNameMask,
        },
        {
            field: 'accountNumber',
            strategy: bankAccountNumberMask,
        },
        {
            field: 'accountRoutingNumber',
            strategy: bankAccountRoutingMask,
        },
    ];

    it('will correctly works with static values', () => {
        const testCases: Array<TestCase> = [
            {
                input: undefined,
                output: undefined,
            },
            {
                input: null,
                output: null,
            },
            {
                input: 1,
                output: 1,
            },
            {
                input: 'string',
                output: 'string',
            },
            {
                input: [],
                output: [],
            },
            {
                input: {},
                output: {},
            },
            {
                input: [{}],
                output: [{}],
            },
        ];

        for (const testCase of testCases) {
            expect(deepMask(testCase.input, config)).toEqual(testCase.output);
        }
    });

    it('will correctly work with simple objects', () => {
        const testCases: Array<TestCase> = [
            {
                input: {
                    foo: 'bar',
                },
                output: {
                    foo: 'bar',
                },
            },
            {
                input: {
                    foo: 'bar',
                    bar: null,
                },
                output: {
                    foo: 'bar',
                    bar: null,
                },
            },
            {
                input: {
                    foo: 'bar',
                    ssn: '123456789',
                    fullname: 'John Doe',
                },
                output: {
                    foo: 'bar',
                    ssn: '*****6789',
                    fullname: 'J*** D**',
                },
            },
        ];

        for (const testCase of testCases) {
            expect(deepMask(testCase.input, config)).toEqual(testCase.output);
        }
    });

    it('will correctly works with deep objects', () => {
        const testCases: Array<TestCase> = [
            {
                input: {
                    foo: {
                        bar: 'baz',
                    },
                },
                output: {
                    foo: {
                        bar: 'baz',
                    },
                },
            },
            {
                input: {
                    foo: {
                        bar: 'baz',
                        baz: {
                            foo: 'bar',
                            fullname: 'John Doe',
                        },
                    },
                },
                output: {
                    foo: {
                        bar: 'baz',
                        baz: {
                            foo: 'bar',
                            fullname: 'J*** D**',
                        },
                    },
                },
            },
            {
                input: {
                    foo: {
                        bar: 'baz',
                        baz: [
                            {
                                foo: 'bar',
                            },
                            {
                                baz: 'foo',
                            },
                        ],
                    },
                },
                output: {
                    foo: {
                        bar: 'baz',
                        baz: [
                            {
                                foo: 'bar',
                            },
                            {
                                baz: 'foo',
                            },
                        ],
                    },
                },
            },
            {
                input: {
                    foo: {
                        bar: 'baz',
                        baz: [
                            {
                                ssn: '123456789',
                                fullname: 'John Doe',
                                accountNumber: '8311750045',
                            },
                            {
                                baz: 'foo',
                                FullName: 'John Doe',
                                AccountRoutingNumber: '053208066',
                            },
                        ],
                    },
                },
                output: {
                    foo: {
                        bar: 'baz',
                        baz: [
                            {
                                ssn: '*****6789',
                                fullname: 'J*** D**',
                                accountNumber: '******0045',
                            },
                            {
                                baz: 'foo',
                                FullName: 'J*** D**',
                                AccountRoutingNumber: '*****8066',
                            },
                        ],
                    },
                },
            },
        ];

        for (const testCase of testCases) {
            expect(deepMask(testCase.input, config)).toEqual(testCase.output);
        }
    });
});
