import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param ssn
 * @returns
 * Mask Social security number - Will mask the all but last 4 digits
 */
export const ssnMask: StrategyFunction = (ssn: string) => {
    return !ssn || ssn.length < 4 ? ssn : DEFAULT_MASK_CHARACTER.repeat(ssn.length - 4) + ssn.slice(ssn.length - 4);
};
