import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param creditCardName
 * @returns
 * Mask the credit card name - will mask everything except first letter of every word
 */
export const creditCardNameMask: StrategyFunction = (creditCardName: string) => {
    if (!creditCardName) {
        return creditCardName;
    }

    return creditCardName
        .split(' ')
        .map((word) => {
            return word.slice(0, 1) + DEFAULT_MASK_CHARACTER.repeat(word.length - 1);
        })
        .join(' ');
};
