import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param firstName
 * @returns
 * Mask first name - will mask first letter
 */
export const firstNameMask: StrategyFunction = (firstName: string) => {
    if (!firstName) {
        return firstName;
    }

    return firstName
        .split(' ')
        .map((word) => {
            return word.slice(0, 1) + DEFAULT_MASK_CHARACTER.repeat(word.length - 1);
        })
        .join(' ');
};
