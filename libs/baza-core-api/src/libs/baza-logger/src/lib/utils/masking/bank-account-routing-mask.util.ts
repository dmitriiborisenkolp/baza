import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param bankAccountRoutingNumber
 * @returns
 * Mask bank account routing number - will mask  all but last 4 digits
 */
export const bankAccountRoutingMask: StrategyFunction = (bankAccountRoutingNumber: string) => {
    return !bankAccountRoutingNumber || bankAccountRoutingNumber.length < 4
        ? bankAccountRoutingNumber
        : DEFAULT_MASK_CHARACTER.repeat(bankAccountRoutingNumber.length - 4) +
              bankAccountRoutingNumber.slice(bankAccountRoutingNumber.length - 4);
};
