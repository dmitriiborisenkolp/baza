import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param fullName
 * @returns
 * Mask full name - will mask first letter of each word
 */
export const fullNameMask: StrategyFunction = (fullName: string) => {
    if (!fullName) {
        return fullName;
    }

    return fullName
        .split(' ')
        .map((word) => {
            return word.slice(0, 1) + DEFAULT_MASK_CHARACTER.repeat(word.length - 1);
        })
        .join(' ');
};
