import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param jwtToken
 * @returns
 * Mask jwt token in request headers - will basically return only 20 mask characters
 */
export const jwtTokenMask: StrategyFunction = (jwtToken: string) => {
    return DEFAULT_MASK_CHARACTER.repeat(20);
};
