import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param dob
 * @returns
 * Mask date of birth only day and month will be masked e.g **-**-2016 - assuming the input format is MM-DD-YYYY e.g 01-30-2016
 */
export const dateOfBirthMask: StrategyFunction = (dob: string) => {
    const match = dob.split('-');

    if (!match?.length) {
        return dob;
    }

    return [DEFAULT_MASK_CHARACTER.repeat(2), DEFAULT_MASK_CHARACTER.repeat(2), match[2]].join('-');
};
