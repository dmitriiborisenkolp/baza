import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param lastName
 * @returns
 * Mask last name - will mask first letter
 */
export const lastNameMask: StrategyFunction = (lastName: string) => {
    if (!lastName) {
        return lastName;
    }

    return lastName
        .split(' ')
        .map((word) => {
            return word.slice(0, 1) + DEFAULT_MASK_CHARACTER.repeat(word.length - 1);
        })
        .join(' ');
};
