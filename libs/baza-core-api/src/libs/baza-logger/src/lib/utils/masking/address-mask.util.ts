import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param address
 * @returns
 * Mask address - will fully mask the address input
 */
export const addressMask: StrategyFunction = (address: string) => {
    return address.replace(/./g, DEFAULT_MASK_CHARACTER);
};
