import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param creditCardNumber
 * @returns
 * Mask Credit card number - mask all but the last 4 digits
 */
export const creditCardNumberMask: StrategyFunction = (creditCardNumber: string) => {
    return !creditCardNumber || creditCardNumber.length < 4
        ? creditCardNumber
        : DEFAULT_MASK_CHARACTER.repeat(creditCardNumber.length - 4) + creditCardNumber.slice(creditCardNumber.length - 4);
};
