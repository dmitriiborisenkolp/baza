import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param email
 * @returns
 * Mask the email - everything between first and last letter of email address e.g b********h@gmail.com
 */
export const emailMask: StrategyFunction = (email: string) => {
    const emailSplit = email.split('@');
    return [
        emailSplit[0].slice(0, 1),
        emailSplit[0].slice(1, emailSplit[0].length - 1).replace(/./g, DEFAULT_MASK_CHARACTER),
        emailSplit[0].slice(emailSplit[0].length - 1),
        '@',
        emailSplit[1],
    ].join('');
};
