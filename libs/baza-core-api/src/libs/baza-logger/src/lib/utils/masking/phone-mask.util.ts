import { DEFAULT_MASK_CHARACTER } from '@scaliolabs/baza-core-shared';
import { StrategyFunction } from '../../config/baza-logger-config';

/**
 *
 * @param phone
 * @returns
 * will mask all the digits except the last 4 digits
 */
export const phoneMask: StrategyFunction = (phone: string) => {
    return !phone || phone.length < 4 ? phone : DEFAULT_MASK_CHARACTER.repeat(phone.length - 4) + phone.slice(phone.length - 4);
};
