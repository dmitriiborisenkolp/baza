export interface BazaLoggerEnv {
    /**
     * Enables Pretty Logs
     * Could be useful for local (development) environments
     */
    BAZA_PRETTY_LOGS: boolean;

    /**
     * Minimum level for logging to console
     * One of 'fatal', 'error', 'warn', 'info', 'debug', 'trace' or 'silent'.
     * @see https://github.com/pinojs/pino/blob/master/docs/api.md#level-string
     * Default value is 'debug'
     */
    BAZA_LOGGER_MIN_LEVEL?: string;
}
