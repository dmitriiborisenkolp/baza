export class BazaLoggerDebugEvent {
    constructor(
        public readonly payload: {
            createdAt: Date;
            message: unknown;
            context: string | undefined;
        },
    ) {}
}
