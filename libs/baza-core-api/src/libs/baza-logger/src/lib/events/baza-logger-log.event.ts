export class BazaLoggerLogEvent {
    constructor(
        public readonly payload: {
            createdAt: Date;
            message: unknown;
            context: string | undefined;
        },
    ) {}
}
