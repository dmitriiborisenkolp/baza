export interface BazaLoggerConfig {
    masked: Array<BazaLoggerMaskConfig>;
}

export interface StrategyFunction {
    (key: string): string;
}

export interface BazaLoggerMaskConfig {
    /**
     * JSON field which should be masked
     */
    field: string;

    /**
     * Optional Maximum visible characters for masked fields
     */
    maxCharacters?: number;

    /**
     * Optional Override function for masking the field
     */
    strategy?: StrategyFunction;
}

const BAZA_LOGGER_CONFIG: BazaLoggerConfig = {
    masked: [],
};

export function bazaLoggerMaskField(fields: Array<BazaLoggerMaskConfig>): void {
    BAZA_LOGGER_CONFIG.masked.push(...fields);
}

export function bazaLoggerConfig(): BazaLoggerConfig {
    return BAZA_LOGGER_CONFIG;
}
