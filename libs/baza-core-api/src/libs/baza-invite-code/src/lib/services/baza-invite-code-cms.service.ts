import { Injectable } from "@nestjs/common";
import { BazaInviteCodeRepository } from '../repository/baza-invite-code-repository.service';
import { BazaInviteCodeEntity } from '../entities/baza-invite-code.entity';
import { Connection, Repository } from 'typeorm';
import { BazaInviteCodeOwnerNotFoundException } from '../exceptions/baza-invite-code-owner-not-found.exception';
import { BazaInviteCodeWithGivenCodeAlreadyExistsException } from '../exceptions/baza-invite-code-with-given-code-already-exists.exception';
import { AccountEntity } from '../../../../baza-account/src';
import { BazaCreateInviteCodeRequest, BazaDeleteInviteCodeRequest, BazaInviteCodeCmsEntityBody, BazaInviteCodeGetByIdRequest, BazaInviteCodeModels, BazaListOfSharedInviteCodesRequest, BazaListOfSharedInviteCodesResponse, BazaUpdateInviteCodeRequest } from '@scaliolabs/baza-core-shared';
import { BazaInviteCodeCmsMapper } from '../mappers/baza-invite-code-cms-mapper.service';

@Injectable()
export class BazaInviteCodeCmsService
{
    constructor(
        private readonly connection: Connection,
        private readonly repository: BazaInviteCodeRepository,
        private readonly mapper: BazaInviteCodeCmsMapper,
    ) {}

    private get userRepository(): Repository<AccountEntity> {
        return this.connection.getRepository(AccountEntity) as Repository<AccountEntity>;
    }

    async createInviteCode(request: BazaCreateInviteCodeRequest): Promise<BazaInviteCodeEntity> {
        const entity = new BazaInviteCodeEntity();

        await this.populate(entity, request);
        await this.repository.createInviteCode(entity);

        return entity;
    }

    async updateInviteCode(request: BazaUpdateInviteCodeRequest): Promise<BazaInviteCodeEntity> {
        const entity = await this.repository.getInviteCodeById({
            id: request.id,
        });

        await this.populate(entity, request);
        await this.repository.saveInviteCode(entity);

        return entity;
    }

    async deleteInviteCode(request: BazaDeleteInviteCodeRequest): Promise<void> {
        const entity = await this.repository.getInviteCodeById({
            id: request.id,
        });

        await this.repository.deleteInviteCode(entity);
    }

    async getInviteCodeById(request: BazaInviteCodeGetByIdRequest): Promise<BazaInviteCodeEntity> {
        return this.repository.getInviteCodeById({
            id: request.id,
        });
    }

    async listOfSharedInviteCodes(request: BazaListOfSharedInviteCodesRequest): Promise<BazaListOfSharedInviteCodesResponse> {
        const total = await this.repository.countOfSharedInviteCodes();
        const items = await this.repository.listOfSharedInviteCodes(request);

        return {
            items: await this.mapper.entitiesToDTOs(items),
            pager: {
                index: request.index,
                size: request.size,
                total,
            },
        };
    }

    private async populate(entity: BazaInviteCodeEntity, input: BazaInviteCodeCmsEntityBody): Promise<void> {
        const code = input.code.toLowerCase();

        if (! entity.id || entity.code.toLowerCase() !== input.code.toLowerCase()) {
            const found = await this.repository.findInviteCodeByCode({
                code,
            });

            if (found) {
                throw new BazaInviteCodeWithGivenCodeAlreadyExistsException();
            }
        }

        if (input.ownerUserId) {
            const owner = await this.userRepository.findOne({
                where: [{
                    id: input.ownerUserId,
                }],
            });

            if (! owner) {
                throw new BazaInviteCodeOwnerNotFoundException();
            }

            entity.owner = owner;
        } else {
            entity.owner = null;
        }

        if (entity.id) {
            entity.dateUpdatedAt = new Date();
        } else {
            entity.dateCreatedAt = new Date();
            entity.dateUpdatedAt = entity.dateCreatedAt;
        }

        entity.code = code;
        entity.codeDisplay = input.code;
        entity.consumeLimit = input.consumeLimit
            ? input.consumeLimit
            : BazaInviteCodeModels.UNLIMITED_CONSUME_LIMIT;
    }
}
