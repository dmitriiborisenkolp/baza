import { Injectable } from '@nestjs/common';
import { BazaInviteCodeRepository } from '../repository/baza-invite-code-repository.service';
import { BazaInviteCodeAlreadyCompletelyConsumedException } from '../exceptions/baza-invite-code-already-completely-consumed.exception';
import { Connection, Repository } from 'typeorm';
import { BazaInviteCodeUserIsAlreadyRegisteredException } from '../exceptions/baza-invite-code-user-is-already-registered.exception';
import {
    BazaRequestForInviteCodesRequest,
    BazaRequestForInviteCodesResponse,
    BazaValidateInviteCodeResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaRegistryNodeEmailRecipientValue } from '@scaliolabs/baza-core-shared';
import { MailService } from '../../../../baza-mail/src';
import { BazaRegistryService } from '../../../../baza-registry/src';
import { AccountEntity } from '../../../../baza-account/src';
import { BazaCoreMail } from '../../../../../constants/baza-core.mail';

@Injectable()
export class BazaInviteCodeService {
    constructor(
        private readonly connection: Connection,
        private readonly repository: BazaInviteCodeRepository,
        private readonly mailService: MailService,
        private readonly registry: BazaRegistryService,
    ) {}

    private get userRepository(): Repository<AccountEntity> {
        return this.connection.getRepository(AccountEntity) as Repository<AccountEntity>;
    }

    async incrementInviteCodeUsage(code: string): Promise<void> {
        const inviteCode = await this.repository.getInviteCodeByCode({
            code,
        });

        if (inviteCode.isConsumed) {
            throw new BazaInviteCodeAlreadyCompletelyConsumedException();
        }

        if (inviteCode.consumeLimit > 0 && inviteCode.usedCount >= inviteCode.consumeLimit) {
            throw new BazaInviteCodeAlreadyCompletelyConsumedException();
        }

        inviteCode.usedCount++;

        if (inviteCode.consumeLimit > 0 && inviteCode.usedCount >= inviteCode.consumeLimit) {
            inviteCode.isConsumed = true;
        }

        await this.repository.saveInviteCode(inviteCode);
    }

    async validateInviteCode(request: { code: string }): Promise<void> {
        const inviteCode = await this.repository.getInviteCodeByCode({
            code: request.code,
        });

        if (inviteCode.isConsumed) {
            throw new BazaInviteCodeAlreadyCompletelyConsumedException();
        }
    }

    async hasInviteCode(request: { code: string }): Promise<boolean> {
        return !!(await this.repository.findInviteCodeByCode({
            code: request.code,
        }));
    }

    async getInviteCodeStats(request: { code: string }): Promise<BazaValidateInviteCodeResponse> {
        const inviteCode = await this.repository.findInviteCodeByCode({
            code: request.code.toLowerCase(),
        });

        if (!inviteCode) {
            return {
                canBeUsed: false,
                isExists: false,
            };
        } else {
            return {
                canBeUsed: !inviteCode.isConsumed,
                isExists: true,
                isConsumed: inviteCode.isConsumed,
            };
        }
    }

    async requestForInviteCodes(request: BazaRequestForInviteCodesRequest): Promise<BazaRequestForInviteCodesResponse> {
        const found = await this.userRepository.findOne({
            where: [
                {
                    email: request.email,
                },
            ],
        });

        if (found) {
            throw new BazaInviteCodeUserIsAlreadyRegisteredException();
        }

        await this.mailService.send({
            name: BazaCoreMail.BazaInviteCodeRequest,
            to: [this.registry.getValue('adminEmail') as BazaRegistryNodeEmailRecipientValue],
            subject: 'baza-invite-code.inviteCodeRequest.subject',
            variables: () => ({
                email: request.email,
            }),
        });

        return {
            isOrgNotifiedWithEmail: true,
        };
    }
}
