import { forwardRef, Module } from '@nestjs/common';
import { BazaInviteCodeApiModule } from './baza-invite-code-api.module';
import { BazaInviteCodeFixture } from './integration-tests/fixtures/baza-invite-code-fixture.service';

const E2E_FIXTURES = [
    BazaInviteCodeFixture,
];

@Module({
    imports: [
        forwardRef(() => BazaInviteCodeApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaInviteCodeE2eModule {}
