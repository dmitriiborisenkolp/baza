import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '../../../../baza-account/src';
import { AccountSettingsDto } from '@scaliolabs/baza-core-shared';
import { BazaInviteCodeModels } from '@scaliolabs/baza-core-shared';

@Entity({
    name: 'invite_code_rev3',
})
export class BazaInviteCodeEntity<T = AccountSettingsDto> {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    dateCreatedAt: Date = new Date();

    @Column()
    dateUpdatedAt: Date = new Date();

    @ManyToOne(() => AccountEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        nullable: true,
    })
    owner: AccountEntity<T>;

    @Column({
        unique: true,
    })
    code: string;

    @Column()
    codeDisplay: string;

    @Column({
        default: BazaInviteCodeModels.UNLIMITED_CONSUME_LIMIT,
    })
    consumeLimit: number;

    @Column({
        default: 0,
    })
    usedCount: number;

    @Column({
        default: false,
    })
    isConsumed: boolean;
}
