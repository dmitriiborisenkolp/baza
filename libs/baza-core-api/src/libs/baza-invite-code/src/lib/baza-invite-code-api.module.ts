import { Global, Module } from '@nestjs/common';
import { BazaInviteCodeController } from './controllers/baza-invite-code.controller';
import { BazaInviteCodeRepository } from './repository/baza-invite-code-repository.service';
import { BazaInviteCodeMapper } from './mappers/baza-invite-code.mapper';
import { BazaInviteCodeService } from './services/baza-invite-code.service';
import { BazaInviteCodeCmsController } from './controllers/baza-invite-code-cms.controller';
import { BazaInviteCodeCmsMapper } from './mappers/baza-invite-code-cms-mapper.service';
import { BazaInviteCodeCmsService } from './services/baza-invite-code-cms.service';
import { BazaRegistryApiModule } from '../../../baza-registry/src';
import { BazaCrudApiModule } from '../../../baza-crud/src';

@Global()
@Module({
    imports: [
        BazaRegistryApiModule,
        BazaCrudApiModule,
    ],
    controllers: [
        BazaInviteCodeController,
        BazaInviteCodeCmsController,
    ],
    providers: [
        BazaInviteCodeRepository,
        BazaInviteCodeMapper,
        BazaInviteCodeService,
        BazaInviteCodeCmsMapper,
        BazaInviteCodeCmsService,
    ],
    exports: [
        BazaInviteCodeRepository,
        BazaInviteCodeMapper,
        BazaInviteCodeService,
        BazaInviteCodeCmsMapper,
        BazaInviteCodeCmsService,
    ],
})
export class BazaInviteCodeApiModule
{}
