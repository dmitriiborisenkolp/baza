import { Injectable } from '@nestjs/common';
import { BazaInviteCodeIsInvalidException } from '../exceptions/baza-invite-code-is-invalid.exception';
import { Connection, IsNull, Repository } from 'typeorm';
import { BazaInviteCodeEntity } from '../entities/baza-invite-code.entity';
import { AccountEntity } from '../../../../baza-account/src';
import { BazaListOfSharedInviteCodesRequest } from '@scaliolabs/baza-core-shared';
import { CrudService } from '../../../../baza-crud/src';

@Injectable()
export class BazaInviteCodeRepository
{
    constructor(
        private readonly connection: Connection,
        private readonly crud: CrudService,
    ) {}

    private get inviteCodeRepository(): Repository<BazaInviteCodeEntity> {
        return this.connection.getRepository(BazaInviteCodeEntity) as Repository<BazaInviteCodeEntity>;
    }

    async createInviteCode(entity: BazaInviteCodeEntity): Promise<void> {
        if (entity.id) {
            throw new Error('Invite code is already created');
        }

        entity.code = entity.code.toLowerCase();
        entity.dateCreatedAt = new Date();
        entity.dateUpdatedAt = entity.dateCreatedAt;

        await this.inviteCodeRepository.save(entity);
    }

    async saveInviteCode(entity: BazaInviteCodeEntity): Promise<void> {
        if (! entity.id) {
            throw new Error('Invite code is not created yet');
        }

        if (entity.usedCount === entity.consumeLimit) {
            entity.isConsumed = true;
        }

        entity.code = entity.code.toLowerCase();
        entity.dateUpdatedAt = new Date();

        await this.inviteCodeRepository.save(entity);
    }

    async deleteInviteCode(entity: BazaInviteCodeEntity): Promise<void> {
        await this.inviteCodeRepository.remove(entity);
    }

    async countOfUserInviteCodes(request: {
        user: AccountEntity;
    }): Promise<number> {
        return this.inviteCodeRepository.count({
            where: [{
                owner: request.user,
            }],
        });
    }

    async listOfUserInviteCodes(request: {
        user: AccountEntity;
        take?: number;
        skip?: number;
        order?: string;
    }): Promise<Array<BazaInviteCodeEntity>> {
        const order: any = {};

        if (request.order === 'id') {
            order.id = 'ASC';
        } else if (request.order === '-id') {
            order.id = 'DESC';
        }

        return this.inviteCodeRepository.find({
            where: [{
                owner: request.user,
            }],
            order,
            take: request.take,
            skip: request.skip,
        });
    }

    async countOfSharedInviteCodes(): Promise<number> {
        return this.inviteCodeRepository.count({
            where: [{
                owner: IsNull(),
            }],
        });
    }

    async listOfSharedInviteCodes(request: BazaListOfSharedInviteCodesRequest): Promise<Array<BazaInviteCodeEntity>> {
        const findOptions = await this.crud.findOptions({}, request);

        return this.inviteCodeRepository.find(findOptions);
    }

    async getInviteCodeById(request: {
        id: number;
    }): Promise<BazaInviteCodeEntity> {
        const inviteCode = await this.inviteCodeRepository.findOne({
            where: [{
                id: request.id,
            }],
        })

        if (! inviteCode) {
            throw new BazaInviteCodeIsInvalidException();
        }

        return inviteCode;
    }

    async getInviteCodeByCode(request: {
        code: string;
    }): Promise<BazaInviteCodeEntity> {
        const inviteCode = await this.inviteCodeRepository.findOne({
            where: [{
                code: request.code.toLowerCase(),
            }],
        })

        if (! inviteCode) {
            throw new BazaInviteCodeIsInvalidException();
        }

        return inviteCode;
    }

    async findInviteCodeByCode(request: {
        code: string;
    }): Promise<BazaInviteCodeEntity | undefined> {
        return this.inviteCodeRepository.findOne({
            where: [{
                code: (request.code || '').toString().trim().toLowerCase(),
            }],
        });
    }
}
