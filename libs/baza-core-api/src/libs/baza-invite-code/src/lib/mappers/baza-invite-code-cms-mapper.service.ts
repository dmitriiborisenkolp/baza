import { Injectable } from "@nestjs/common";
import { BazaInviteCodeEntity } from '../entities/baza-invite-code.entity';
import { Connection, Repository } from 'typeorm';
import { AccountEntity } from '../../../../baza-account/src';
import { BazaInviteCodeCmsDto } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaInviteCodeCmsMapper
{
    constructor(
        private readonly connection: Connection,
    ) {}

    private get userRepository(): Repository<AccountEntity> {
        return this.connection.getRepository(AccountEntity) as Repository<AccountEntity>;
    }

    async entityToDTO(input: BazaInviteCodeEntity): Promise<BazaInviteCodeCmsDto> {
        return {
            id: input.id,
            code: input.code,
            codeDisplay: input.codeDisplay,
            consumeLimit: input.consumeLimit,
            isConsumed: input.isConsumed,
            usedCount: input.usedCount,
            ownerUserId: input.owner
                ? input.owner.id
                : undefined,
            ownerUserFullName: input.owner
                ? `${input.owner.firstName} ${input.owner.lastName}`
                : undefined,
        };
    }

    async entitiesToDTOs(input: Array<BazaInviteCodeEntity>): Promise<Array<BazaInviteCodeCmsDto>> {
        const result: Array<BazaInviteCodeCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
