import { BazaInviteCodeEntity } from '../entities/baza-invite-code.entity';
import { Injectable } from '@nestjs/common';
import { BazaInviteCodeDto } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaInviteCodeMapper
{
    entityToDTO(input: BazaInviteCodeEntity): BazaInviteCodeDto {
        return {
            code: input.codeDisplay,
            isConsumed: input.isConsumed,
        };
    }

    entitiesToDTOs(input: Array<BazaInviteCodeEntity>): Array<BazaInviteCodeDto> {
        return input.map((e) => this.entityToDTO(e));
    }
}
