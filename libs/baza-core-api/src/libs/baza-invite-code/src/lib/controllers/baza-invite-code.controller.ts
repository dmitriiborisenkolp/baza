import { Controller, Post, Body } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaInviteCodeService } from '../services/baza-invite-code.service';
import {
    BazaInviteCodeEndpoint,
    BazaInviteCodeEndpointPaths,
    BazaRequestForInviteCodesRequest,
    BazaRequestForInviteCodesResponse,
    BazaValidateInviteCodeRequest,
    BazaValidateInviteCodeResponse,
    CoreOpenApiTags,
} from '@scaliolabs/baza-core-shared';

@Controller()
@ApiTags(CoreOpenApiTags.BazaReferralCode)
export class BazaInviteCodeController implements BazaInviteCodeEndpoint {
    constructor(private readonly service: BazaInviteCodeService) {}

    @Post(BazaInviteCodeEndpointPaths.validateInviteCode)
    @ApiOperation({
        summary: 'validateInviteCode',
        description: 'Validate invite code',
    })
    @ApiOkResponse({
        type: BazaValidateInviteCodeResponse,
    })
    async validateInviteCode(@Body() request: BazaValidateInviteCodeRequest): Promise<BazaValidateInviteCodeResponse> {
        return this.service.getInviteCodeStats(request);
    }

    @Post(BazaInviteCodeEndpointPaths.requestForInviteCodes)
    @ApiOperation({
        summary: 'requestForInviteCodes',
        description:
            'Send email to admins that the user request for invite codes. Endpoint will return BazaInviteCodeUserIsAlreadyRegistered error code if user is already signed up',
    })
    @ApiOkResponse({
        type: BazaValidateInviteCodeResponse,
    })
    async requestForInviteCodes(@Body() request: BazaRequestForInviteCodesRequest): Promise<BazaRequestForInviteCodesResponse> {
        return this.service.requestForInviteCodes(request);
    }
}
