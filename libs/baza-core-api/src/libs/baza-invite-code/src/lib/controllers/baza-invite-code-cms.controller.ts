import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaInviteCodeCmsMapper } from '../mappers/baza-invite-code-cms-mapper.service';
import { BazaInviteCodeCmsService } from '../services/baza-invite-code-cms.service';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src';
import { AclNodes } from '../../../../baza-acl/src';
import {
    BazaCreateInviteCodeRequest,
    BazaDeleteInviteCodeRequest,
    BazaInviteCodeAcl,
    BazaInviteCodeCmsDto,
    BazaInviteCodeCmsEndpoint,
    BazaInviteCodeCmsEndpointPaths,
    BazaInviteCodeGetByIdRequest,
    BazaListOfSharedInviteCodesRequest,
    BazaListOfSharedInviteCodesResponse,
    BazaUpdateInviteCodeRequest,
    CoreOpenApiTags,
} from '@scaliolabs/baza-core-shared';

@Controller()
@ApiBearerAuth()
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaInviteCodeAcl.BazaInviteCodes])
@ApiTags(CoreOpenApiTags.BazaInviteCodeCMS)
export class BazaInviteCodeCmsController implements BazaInviteCodeCmsEndpoint {
    constructor(private readonly service: BazaInviteCodeCmsService, private readonly mapper: BazaInviteCodeCmsMapper) {}

    @Post(BazaInviteCodeCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns invite code by id',
    })
    @ApiOkResponse({
        type: BazaInviteCodeCmsDto,
    })
    async getById(@Body() request: BazaInviteCodeGetByIdRequest): Promise<BazaInviteCodeCmsDto> {
        const entity = await this.service.getInviteCodeById(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaInviteCodeCmsEndpointPaths.listOfSharedInviteCodes)
    @ApiOperation({
        summary: 'listOfSharedInviteCodes',
        description: 'List of shared invite codes',
    })
    @ApiOkResponse({
        type: BazaListOfSharedInviteCodesResponse,
    })
    async listOfSharedInviteCodes(@Body() request: BazaListOfSharedInviteCodesRequest): Promise<BazaListOfSharedInviteCodesResponse> {
        return this.service.listOfSharedInviteCodes(request);
    }

    @Post(BazaInviteCodeCmsEndpointPaths.createInviteCode)
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaInviteCodeAcl.BazaInviteCodesManagement])
    @ApiOperation({
        summary: 'createInviteCode',
        description: 'Create new invite code',
    })
    @ApiOkResponse({
        type: BazaInviteCodeCmsDto,
    })
    async createInviteCode(@Body() request: BazaCreateInviteCodeRequest): Promise<BazaInviteCodeCmsDto> {
        const entity = await this.service.createInviteCode(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaInviteCodeCmsEndpointPaths.updateInviteCode)
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaInviteCodeAcl.BazaInviteCodesManagement])
    @ApiOperation({
        summary: 'updateInviteCode',
        description: 'Update invite code',
    })
    @ApiOkResponse({
        type: BazaInviteCodeCmsDto,
    })
    async updateInviteCode(@Body() request: BazaUpdateInviteCodeRequest): Promise<BazaInviteCodeCmsDto> {
        const entity = await this.service.updateInviteCode(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaInviteCodeCmsEndpointPaths.deleteInviteCode)
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaInviteCodeAcl.BazaInviteCodesManagement])
    @ApiOperation({
        summary: 'deleteInviteCode',
        description: 'Delete invite code',
    })
    @ApiOkResponse({
        type: undefined,
    })
    async deleteInviteCode(@Body() request: BazaDeleteInviteCodeRequest): Promise<void> {
        await this.service.deleteInviteCode(request);
    }
}
