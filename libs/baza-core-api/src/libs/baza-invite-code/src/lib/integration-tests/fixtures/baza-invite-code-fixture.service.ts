import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '../../../../../baza-e2e-fixtures/src';
import { BazaCreateInviteCodeRequest } from '@scaliolabs/baza-core-shared';
import { BazaInviteCodeCmsService } from '../../services/baza-invite-code-cms.service';
import { BazaInviteCodeFixtures } from '../baza-invite-code.fixtures';

const FIXTURES: Array<BazaCreateInviteCodeRequest> = [
    {
        code: 'ExampleCode1',
        consumeLimit: 1,
    },
    {
        code: 'ExampleCode2',
        consumeLimit: 2,
    },
    {
        code: 'ExampleCode3',
        consumeLimit: -1,
    },
];

@Injectable()
export class BazaInviteCodeFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaInviteCodeCmsService,
    ) {}

    async up(): Promise<void> {
        for (const createRequest of FIXTURES) {
            await this.service.createInviteCode(createRequest);
        }
    }
}

defineE2eFixtures<BazaInviteCodeFixtures>([{
    fixture: BazaInviteCodeFixtures.BazaInviteCodeFixture,
    provider: BazaInviteCodeFixture,
}]);

