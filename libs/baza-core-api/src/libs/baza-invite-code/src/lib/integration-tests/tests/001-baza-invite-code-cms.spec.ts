import 'reflect-metadata';
import { BazaDataAccessNode, BazaInviteCodesCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

jest.setTimeout(240000);

describe('@baza-core-api/baza-invite-code/integration-tests/tests/001-baza-nc-integration-invite-code-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInviteCodeCms = new BazaInviteCodesCmsNodeAccess(http);

    let INVITE_CODE_1: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create invite code', async () => {
        const response = await dataAccessInviteCodeCms.createInviteCode({
            code: 'ExampleCode1',
            consumeLimit: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.codeDisplay).toBe('ExampleCode1');
        expect(response.code).toBe('examplecode1');

        INVITE_CODE_1 = response.id;
    });

    it('will display created code in list', async () => {
        const response = await dataAccessInviteCodeCms.listOfSharedInviteCodes({
            index: 1,
            size: -1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);
        expect(response.items.length).toBe(1);
        expect(response.items[0].id).toBe(INVITE_CODE_1);
    });

    it('will returns created code with getById method', async () => {
        const response = await dataAccessInviteCodeCms.getById({
            id: INVITE_CODE_1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(INVITE_CODE_1);
    });

    it('will successfully update code', async () => {
        const response = await dataAccessInviteCodeCms.updateInviteCode({
            id: INVITE_CODE_1,
            code: 'ExampleCode2',
            consumeLimit: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.codeDisplay).toBe('ExampleCode2');
        expect(response.code).toBe('examplecode2');
    });

    it('will display updates in list', async () => {
        const response = await dataAccessInviteCodeCms.listOfSharedInviteCodes({
            index: 1,
            size: -1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);
        expect(response.items.length).toBe(1);
        expect(response.items[0].id).toBe(INVITE_CODE_1);
        expect(response.items[0].code).toBe('examplecode2');
        expect(response.items[0].codeDisplay).toBe('ExampleCode2');
    });

    it('will successfully delete code', async () => {
        const response = await dataAccessInviteCodeCms.deleteInviteCode({
            id: INVITE_CODE_1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted code in list', async () => {
        const response = await dataAccessInviteCodeCms.listOfSharedInviteCodes({
            index: 1,
            size: -1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(0);
        expect(response.items.length).toBe(0);
    });
});
