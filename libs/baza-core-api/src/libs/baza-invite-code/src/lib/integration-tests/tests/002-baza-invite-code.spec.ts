import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaAccountNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { AccountRole, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaInviteCodesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaInviteCodeFixtures } from '../baza-invite-code.fixtures';
import { BazaInviteCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { asyncExpect } from '../../../../../baza-test-utils/src';

jest.setTimeout(240000);

describe('@baza-core-api/baza-invite-code/integration-tests/tests/001-baza-nc-integration-invite-code-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInviteCode = new BazaInviteCodesNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaInviteCodeFixtures.BazaInviteCodeFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        http.noAuth();
    });

    it('will successfully request for invite codes', async () => {
        const response = await dataAccessInviteCode.requestForInviteCodes({
            email: 'example-1@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display mail notification about invite code request', async () => {
        const response = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].messageBodyText).toContain('example-1@scal.io');
    });

    it('will successfully request for invite codes (duplicate request)', async () => {
        const response = await dataAccessInviteCode.requestForInviteCodes({
            email: 'example-1@scal.io',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display mail notification about invite code request (duplicate request)', async () => {
        const response = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);
        expect(response[0].messageBodyText).toContain('example-1@scal.io');
        expect(response[1].messageBodyText).toContain('example-1@scal.io');
    });

    it('will successfully validate existing code', async () => {
        const response = await dataAccessInviteCode.validateInviteCode({
            code: 'examplecode1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isExists).toBeTruthy();
        expect(response.canBeUsed).toBeTruthy();
        expect(response.isConsumed).toBeFalsy();
    });

    it('will successfully invalidate code which is not exists', async () => {
        const response = await dataAccessInviteCode.validateInviteCode({
            code: 'examplecode999',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isExists).toBeFalsy();
        expect(response.canBeUsed).toBeFalsy();
        expect(response.isConsumed).toBeFalsy();
    });

    it('will successfully register account with invite code ExampleCode1', async () => {
        const response = await dataAccessAccount.registerAccount({
            email: 'example-1@scal.io',
            firstName: 'Example',
            lastName: 'Account',
            password: 'Scalio#1337!',
            referralCode: 'ExampleCode1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully validate existing code', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessInviteCode.validateInviteCode({
                    code: 'examplecode1',
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.isExists).toBeTruthy();
                expect(response.canBeUsed).toBeTruthy();
                expect(response.isConsumed).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully confirm account', async () => {
        await http.authE2eAdmin();

        const listResponse = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
            size: -1,
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        const registeredAccount = listResponse.items.find((a) => a.email === 'example-1@scal.io');

        expect(registeredAccount).toBeDefined();

        const response = await dataAccessAccountCms.confirmEmailAccountById({
            id: registeredAccount.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully validate existing code - code should not be used anymore', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessInviteCode.validateInviteCode({
                    code: 'examplecode1',
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.isExists).toBeTruthy();
                expect(response.canBeUsed).toBeFalsy();
                expect(response.isConsumed).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will not register user 2 with ExampleCode1', async () => {
        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'example-2@scal.io',
            firstName: 'Example',
            lastName: 'Account',
            password: 'Scalio#1337!',
            referralCode: 'ExampleCode1',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaInviteCodeErrorCodes.BazaInviteCodeIsAlreadyCompletelyConsumed);
    });

    it('will successfully register user 2 with ExampleCode2', async () => {
        const response = await dataAccessAccount.registerAccount({
            email: 'example-2@scal.io',
            firstName: 'Example',
            lastName: 'Account',
            password: 'Scalio#1337!',
            referralCode: 'ExampleCode2',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully confirm account 2', async () => {
        await http.authE2eAdmin();

        const listResponse = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
            size: -1,
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        const registeredAccount = listResponse.items.find((a) => a.email === 'example-2@scal.io');

        expect(registeredAccount).toBeDefined();

        const response = await dataAccessAccountCms.confirmEmailAccountById({
            id: registeredAccount.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully validate existing code ExampleCode2 - it still should be available to use', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessInviteCode.validateInviteCode({
                    code: 'examplecode2',
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.isExists).toBeTruthy();
                expect(response.canBeUsed).toBeTruthy();
                expect(response.isConsumed).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully register user 3 with ExampleCode2', async () => {
        const response = await dataAccessAccount.registerAccount({
            email: 'example-3@scal.io',
            firstName: 'Example',
            lastName: 'Account',
            password: 'Scalio#1337!',
            referralCode: 'ExampleCode2',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully confirm account 3', async () => {
        await http.authE2eAdmin();

        const listResponse = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
            size: -1,
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        const registeredAccount = listResponse.items.find((a) => a.email === 'example-3@scal.io');

        expect(registeredAccount).toBeDefined();

        const response = await dataAccessAccountCms.confirmEmailAccountById({
            id: registeredAccount.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully validate existing code ExampleCode2 - it should not be available to use', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessInviteCode.validateInviteCode({
                    code: 'examplecode2',
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.isExists).toBeTruthy();
                expect(response.canBeUsed).toBeFalsy();
                expect(response.isConsumed).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will works correctly with unlimited invite code', async () => {
        for (let i = 1; i <= 5; i++) {
            const email = `example-unlimited-${i}@scal.io`;

            // Sign out
            await http.noAuth();

            // Validate code
            const validateCodeBeforeResponse = await dataAccessInviteCode.validateInviteCode({
                code: 'examplecode3',
            });

            expect(isBazaErrorResponse(validateCodeBeforeResponse)).toBeFalsy();

            expect(validateCodeBeforeResponse.isExists).toBeTruthy();
            expect(validateCodeBeforeResponse.canBeUsed).toBeTruthy();
            expect(validateCodeBeforeResponse.isConsumed).toBeFalsy();

            // Register account
            const registerAccountResponse = await dataAccessAccount.registerAccount({
                email,
                firstName: 'Example',
                lastName: 'Account',
                password: 'Scalio#1337!',
                referralCode: 'ExampleCode3',
            });

            expect(isBazaErrorResponse(registerAccountResponse)).toBeFalsy();

            // Confirm account
            await http.authE2eAdmin();

            const listResponse = await dataAccessAccountCms.listAccounts({
                roles: [AccountRole.User],
                size: -1,
                index: 1,
            });

            expect(isBazaErrorResponse(listResponse)).toBeFalsy();

            const registeredAccount = listResponse.items.find((a) => a.email === email);

            expect(registeredAccount).toBeDefined();

            const response = await dataAccessAccountCms.confirmEmailAccountById({
                id: registeredAccount.id,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            await asyncExpect(
                async () => {
                    // Invite Code should be still available to use
                    // Validate code
                    const validateCodeAfterResponse = await dataAccessInviteCode.validateInviteCode({
                        code: 'examplecode3',
                    });

                    expect(isBazaErrorResponse(validateCodeAfterResponse)).toBeFalsy();

                    expect(validateCodeAfterResponse.isExists).toBeTruthy();
                    expect(validateCodeAfterResponse.canBeUsed).toBeTruthy();
                    expect(validateCodeAfterResponse.isConsumed).toBeFalsy();
                },
                null,
                { intervalMillis: 2000 },
            );
        }
    });
});
