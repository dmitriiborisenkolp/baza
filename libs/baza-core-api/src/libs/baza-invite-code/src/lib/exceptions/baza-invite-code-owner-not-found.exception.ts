import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaInviteCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaInviteCodeOwnerNotFoundException extends BazaAppException {
    constructor() {
        super(BazaInviteCodeErrorCodes.BazaInviteCodeOwnerNotFound, 'User was not found', HttpStatus.BAD_REQUEST);
    }
}
