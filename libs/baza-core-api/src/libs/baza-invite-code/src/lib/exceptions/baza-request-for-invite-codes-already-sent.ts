import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaInviteCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaRequestForInviteCodesAlreadySent extends BazaAppException {
    constructor() {
        super(BazaInviteCodeErrorCodes.BazaRequestForInviteCodesAlreadySent, 'Request for invite codes is already sent', HttpStatus.CONFLICT);
    }
}
