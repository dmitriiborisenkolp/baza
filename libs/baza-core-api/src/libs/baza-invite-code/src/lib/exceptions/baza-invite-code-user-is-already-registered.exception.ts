import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaInviteCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaInviteCodeUserIsAlreadyRegisteredException extends BazaAppException {
    constructor() {
        super(BazaInviteCodeErrorCodes.BazaInviteCodeUserIsAlreadyRegistered, 'You have already created an account with this email.', HttpStatus.CONFLICT);
    }
}
