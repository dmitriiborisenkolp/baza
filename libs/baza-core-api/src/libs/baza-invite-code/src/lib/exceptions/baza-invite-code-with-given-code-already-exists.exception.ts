import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaInviteCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaInviteCodeWithGivenCodeAlreadyExistsException extends BazaAppException {
    constructor() {
        super(BazaInviteCodeErrorCodes.BazaInviteCodeWithGivenCodeAlreadyExists, 'Invite code with given code already exists', HttpStatus.CONFLICT);
    }
}
