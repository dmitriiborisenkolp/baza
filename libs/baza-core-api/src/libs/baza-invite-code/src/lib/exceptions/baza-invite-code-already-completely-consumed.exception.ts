import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaInviteCodeErrorCodes } from '@scaliolabs/baza-core-shared';

export class BazaInviteCodeAlreadyCompletelyConsumedException extends BazaAppException {
    constructor() {
        super(BazaInviteCodeErrorCodes.BazaInviteCodeIsAlreadyCompletelyConsumed, 'Invite code is already completely consumed', HttpStatus.CONFLICT);
    }
}
