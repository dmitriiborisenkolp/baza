import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaInviteCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaInviteCodeIsInvalidException extends BazaAppException {
    constructor() {
        super(BazaInviteCodeErrorCodes.BazaInviteCodeIsInvalid, 'Invalid code', HttpStatus.NOT_FOUND);
    }
}
