import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ApiCqrsEvent, AuthApiEvent, AuthApiEvents, BazaAuthSessionRecordType } from '@scaliolabs/baza-core-shared';
import { BazaAuthSessionLogService } from '../services/baza-auth-session-log.service';
import { cqrs } from '../../../../baza-common/src';

@EventsHandler(ApiCqrsEvent)
export class AuthSignInEventHandler implements IEventHandler<ApiCqrsEvent<AuthApiEvent, AuthApiEvents>> {
    constructor(private readonly service: BazaAuthSessionLogService) {}

    handle(event: ApiCqrsEvent<AuthApiEvent, AuthApiEvents>): void {
        cqrs(AuthSignInEventHandler.name, async () => {
            switch (event.apiEvent.event.topic) {
                case AuthApiEvent.BazaAuth: {
                    await this.service.increment({
                        type: BazaAuthSessionRecordType.SignIn,
                        accountId: event.apiEvent.event.payload.accountId,
                        ip: event.apiEvent.event.payload.ip,
                        userAgent: event.apiEvent.event.payload.userAgent,
                    });

                    break;
                }

                case AuthApiEvent.BazaAuth2FAFailed:
                case AuthApiEvent.BazaAuthFailed: {
                    if (event.apiEvent.event.payload.accountId) {
                        await this.service.increment({
                            type: BazaAuthSessionRecordType.Attempt,
                            accountId: event.apiEvent.event.payload.accountId,
                            ip: event.apiEvent.event.payload.ip,
                            userAgent: event.apiEvent.event.payload.userAgent,
                        });
                    }

                    break;
                }
            }
        });
    }
}
