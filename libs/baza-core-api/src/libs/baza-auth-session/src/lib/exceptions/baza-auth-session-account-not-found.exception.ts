import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaAuthSessionErrorCodes, bazaAuthSessionErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaAuthSessionAccountNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaAuthSessionErrorCodes.BazaAuthSessionAccountNotFound,
            bazaAuthSessionErrorCodesI18n[BazaAuthSessionErrorCodes.BazaAuthSessionAccountNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
