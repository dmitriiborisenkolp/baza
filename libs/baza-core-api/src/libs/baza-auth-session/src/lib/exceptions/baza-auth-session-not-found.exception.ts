import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaAuthSessionErrorCodes, bazaAuthSessionErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaAuthSessionNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaAuthSessionErrorCodes.BazaAuthSessionNotFound,
            bazaAuthSessionErrorCodesI18n[BazaAuthSessionErrorCodes.BazaAuthSessionNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
