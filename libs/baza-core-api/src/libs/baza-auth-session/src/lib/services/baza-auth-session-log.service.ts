import { Injectable } from '@nestjs/common';
import { BazaAuthSessionAccountRepository } from '../repositories/baza-auth-session-account.repository';
import { BazaAuthSessionRepository } from '../repositories/baza-auth-session.repository';
import { BazaAuthSessionMapper } from '../mappers/baza-auth-session.mapper';
import { BazaAuthSessionAccountMapper } from '../mappers/baza-auth-session-account.mapper';
import { BazaAccountMapper, BazaAccountRepository } from '../../../../baza-account/src';
import { BazaAuthSessionAccountDto, BazaAuthSessionCmsAccountsRequest, BazaAuthSessionCmsAccountsResponse, BazaAuthSessionCmsSessionsRequest, BazaAuthSessionCmsSessionsResponse, BazaAuthSessionDto, BazaAuthSessionRecordType, BazaAuthSessionsIncrement } from '@scaliolabs/baza-core-shared';
import { BAZA_AUTH_SESSION_ACCOUNT_RELATIONS, BazaAuthSessionAccountEntity } from '../entities/baza-auth-session-account.entity';
import { BAZA_AUTH_SESSION_RELATIONS, BazaAuthSessionEntity } from '../entities/baza-auth-session.entity';
import { CrudService } from '../../../../baza-crud/src';
import { BazaAuthSessionConfig } from '../baza-auth-session.config';

@Injectable()
export class BazaAuthSessionLogService {
    constructor(
        private readonly crud: CrudService,
        private readonly moduleConfig: BazaAuthSessionConfig,
        private readonly accountMapper: BazaAccountMapper,
        private readonly accountRepository: BazaAccountRepository,
        private readonly authAccountsRepository: BazaAuthSessionAccountRepository,
        private readonly authSessionsRepository: BazaAuthSessionRepository,
        private readonly authAccountsMapper: BazaAuthSessionAccountMapper,
        private readonly authSessionsMapper: BazaAuthSessionMapper,
    ) {}

    async increment(request: BazaAuthSessionsIncrement): Promise<void> {
        const session = new BazaAuthSessionEntity();

        session.ip = request.ip;
        session.userAgent = request.userAgent;
        session.date = new Date();
        session.account = await this.accountRepository.getActiveAccountWithId(request.accountId);
        session.type = request.type;

        await this.authSessionsRepository.save([session]);

        const sessionAccount = await this.authAccountsRepository.touch(session.account);

        if (session.type === BazaAuthSessionRecordType.Attempt) {
            sessionAccount.countAuthAttempts++;
        } else if (session.type === BazaAuthSessionRecordType.SignIn) {
            sessionAccount.countAuthSignIn++;
        }

        await this.authAccountsRepository.save([sessionAccount]);
    }

    async listAccounts(request: BazaAuthSessionCmsAccountsRequest): Promise<BazaAuthSessionCmsAccountsResponse> {
        return this.crud.find<BazaAuthSessionAccountEntity, BazaAuthSessionAccountDto>({
            request,
            entity: BazaAuthSessionAccountEntity,
            mapper: async (items) => this.authAccountsMapper.entitiesToDTOs(items),
            findOptions: {
                relations: BAZA_AUTH_SESSION_ACCOUNT_RELATIONS,
                order: {
                    account: 'DESC',
                },
            },
        });
    }

    async listSessions(request: BazaAuthSessionCmsSessionsRequest): Promise<BazaAuthSessionCmsSessionsResponse> {
        const account = await this.accountRepository.getActiveAccountWithId(request.accountId);

        const listResponse = await this.crud.find<BazaAuthSessionEntity, BazaAuthSessionDto>({
            request,
            entity: BazaAuthSessionEntity,
            mapper: async (items) => this.authSessionsMapper.entitiesToDTOs(items),
            findOptions: {
                relations: BAZA_AUTH_SESSION_RELATIONS,
                where: [{
                    account,
                }],
                order: {
                    id: 'DESC',
                },
            },
        });

        return {
            ...listResponse,
            account: this.accountMapper.entityToDto(account),
        };
    }

    async cleanUpOutdatedRecords(): Promise<void> {
        await this.authSessionsRepository.cleanUpOutdatedRecords(
            this.moduleConfig.autoCleanUpAfterNDays,
        );
    }
}
