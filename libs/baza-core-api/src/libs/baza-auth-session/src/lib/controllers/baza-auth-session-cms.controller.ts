import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    AccountAcl,
    BazaAuthSessionCmsAccountsRequest,
    BazaAuthSessionCmsAccountsResponse,
    BazaAuthSessionCmsEndpoint,
    BazaAuthSessionCmsEndpointPaths,
    BazaAuthSessionCmsSessionsResponse,
    BazaAuthSessionCmsSessionsRequest,
    CoreOpenApiTags,
} from '@scaliolabs/baza-core-shared';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AclNodes } from '../../../../baza-acl/src';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src';
import { BazaAuthSessionLogService } from '../services/baza-auth-session-log.service';

@Controller()
@ApiTags(CoreOpenApiTags.BazaAuthSessionCMS)
@AclNodes([AccountAcl.AccountsAuthSession, AccountAcl.AccountsAdminCreate])
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
export class BazaAuthSessionCmsController implements BazaAuthSessionCmsEndpoint {
    constructor(private readonly service: BazaAuthSessionLogService) {}

    @ApiBearerAuth()
    @Post(BazaAuthSessionCmsEndpointPaths.accounts)
    @ApiOperation({
        summary: 'accounts',
        description: 'List of accounts with registered sessions',
    })
    @ApiOkResponse({
        type: BazaAuthSessionCmsAccountsResponse,
    })
    async accounts(@Body() request: BazaAuthSessionCmsAccountsRequest): Promise<BazaAuthSessionCmsAccountsResponse> {
        return this.service.listAccounts(request);
    }

    @ApiBearerAuth()
    @Post(BazaAuthSessionCmsEndpointPaths.sessions)
    @ApiOperation({
        summary: 'sessions',
        description: 'List of sessions for account',
    })
    @ApiOkResponse({
        type: BazaAuthSessionCmsSessionsResponse,
    })
    async sessions(@Body() request: BazaAuthSessionCmsSessionsRequest): Promise<BazaAuthSessionCmsSessionsResponse> {
        return this.service.listSessions(request);
    }
}
