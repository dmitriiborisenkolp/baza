import { DynamicModule, Global, Module, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaCrudApiModule } from '../../../baza-crud/src';
import { AuthSignInEventHandler } from './event-handlers/auth-sign-in.event-handler';
import { BazaAuthSessionMapper } from './mappers/baza-auth-session.mapper';
import { BazaAuthSessionAccountMapper } from './mappers/baza-auth-session-account.mapper';
import { BazaAuthSessionRepository } from './repositories/baza-auth-session.repository';
import { BazaAuthSessionAccountRepository } from './repositories/baza-auth-session-account.repository';
import { BazaAuthSessionLogService } from './services/baza-auth-session-log.service';
import { BazaAuthSessionCmsController } from './controllers/baza-auth-session-cms.controller';
import { BazaAuthSessionConfig } from './baza-auth-session.config';
import { interval, Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { BazaLogger } from '../../../baza-logger/src';
import { EnvService } from '../../../baza-env/src';

interface AsyncOptions {
    imports: Array<any>;
    inject: Array<any>;
    useFactory: (...args: Array<any>) => Promise<BazaAuthSessionConfig>;
}

export { AsyncOptions as BazaAuthSessionAsyncOptions };

const EVENT_HANDLERS = [AuthSignInEventHandler];

@Module({})
export class BazaAuthSessionModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaAuthSessionGlobalModule,
            imports: options.imports,
            providers: [
                {
                    provide: BazaAuthSessionConfig,
                    useFactory: options.useFactory,
                    inject: options.inject,
                },
            ],
            exports: [BazaAuthSessionConfig],
        };
    }
}

@Global()
@Module({
    imports: [CqrsModule, BazaCrudApiModule],
    controllers: [BazaAuthSessionCmsController],
    providers: [
        ...EVENT_HANDLERS,
        BazaAuthSessionMapper,
        BazaAuthSessionAccountMapper,
        BazaAuthSessionRepository,
        BazaAuthSessionAccountRepository,
        BazaAuthSessionLogService,
    ],
    exports: [
        BazaAuthSessionMapper,
        BazaAuthSessionAccountMapper,
        BazaAuthSessionRepository,
        BazaAuthSessionAccountRepository,
        BazaAuthSessionLogService,
    ],
})
class BazaAuthSessionGlobalModule implements OnApplicationBootstrap, OnApplicationShutdown {
    private onApplicationShutdown$ = new Subject<void>();

    constructor(
        private readonly env: EnvService,
        private readonly logger: BazaLogger,
        private readonly moduleConfig: BazaAuthSessionConfig,
        private readonly service: BazaAuthSessionLogService,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    async onApplicationBootstrap(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            interval(this.moduleConfig.cleanUpIntervalHours * 60 /* minutes */ * 60 /* seconds */ * 1000 /* ms */)
                .pipe(startWith(0))
                .subscribe(() => {
                    this.service.cleanUpOutdatedRecords().catch((err) => {
                        this.logger.error(err);
                        this.logger.error('Failed to clean up outdated auth session records');
                    });
                });
        }
    }

    onApplicationShutdown(): void {
        if (this.env.areWatchersAllowed) {
            this.onApplicationShutdown$.next();
        }
    }
}
