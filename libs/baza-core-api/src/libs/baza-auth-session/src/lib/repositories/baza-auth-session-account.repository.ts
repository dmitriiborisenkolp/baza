import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BAZA_AUTH_SESSION_ACCOUNT_RELATIONS, BazaAuthSessionAccountEntity } from '../entities/baza-auth-session-account.entity';
import { AccountEntity } from '../../../../baza-account/src';

@Injectable()
export class BazaAuthSessionAccountRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaAuthSessionAccountEntity> {
        return this.connection.getRepository(BazaAuthSessionAccountEntity);
    }

    async save(entities: Array<BazaAuthSessionAccountEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaAuthSessionAccountEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async touch(account: AccountEntity): Promise<BazaAuthSessionAccountEntity | undefined> {
        const entity = await this.repository.findOne({
            where: [{
                account,
            }],
            relations: BAZA_AUTH_SESSION_ACCOUNT_RELATIONS,
        });

        if (! entity) {
            const newEntity = new BazaAuthSessionAccountEntity();

            newEntity.account = account;
            newEntity.countAuthAttempts = 0;

            await this.save([newEntity]);

            return newEntity;
        } else {
            return entity;
        }
    }
}
