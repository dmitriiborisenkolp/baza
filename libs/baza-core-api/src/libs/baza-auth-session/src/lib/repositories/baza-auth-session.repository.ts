import { Injectable } from '@nestjs/common';
import { Connection, LessThanOrEqual, Repository } from 'typeorm';
import { BazaAuthSessionEntity } from '../entities/baza-auth-session.entity';
import * as moment from 'moment';

@Injectable()
export class BazaAuthSessionRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaAuthSessionEntity> {
        return this.connection.getRepository(BazaAuthSessionEntity);
    }

    async save(entities: Array<BazaAuthSessionEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaAuthSessionEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async cleanUpOutdatedRecords(days: number): Promise<void> {
        await this.repository.delete({
            date: LessThanOrEqual(moment(new Date()).add(-1 * days, 'days')),
        });
    }
}
