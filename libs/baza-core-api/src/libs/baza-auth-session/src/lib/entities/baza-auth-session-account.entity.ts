import { Column, Entity, JoinTable, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '../../../../baza-account/src';

export const BAZA_AUTH_SESSION_ACCOUNT_RELATIONS = [
    'account',
];

@Entity()
export class BazaAuthSessionAccountEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => AccountEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinTable()
    account: AccountEntity;

    @Column({
        default: 0,
    })
    countAuthAttempts: number;

    @Column({
        default: 0,
    })
    countAuthSignIn: number;
}
