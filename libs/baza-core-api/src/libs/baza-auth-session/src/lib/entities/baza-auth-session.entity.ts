import { Column, Entity, JoinTable, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '../../../../baza-account/src';
import { BazaAuthSessionRecordType } from '@scaliolabs/baza-core-shared';

export const BAZA_AUTH_SESSION_RELATIONS = [
    'account',
];

@Entity()
export class BazaAuthSessionEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'varchar',
        length: 12,
    })
    type: BazaAuthSessionRecordType;

    @ManyToOne(() => AccountEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinTable()
    account: AccountEntity;

    @Column()
    date: Date;

    @Column()
    ip: string;

    @Column()
    userAgent: string;
}
