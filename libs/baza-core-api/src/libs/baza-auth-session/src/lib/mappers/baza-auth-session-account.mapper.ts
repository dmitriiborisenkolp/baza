import { Injectable } from '@nestjs/common';
import { BazaAuthSessionAccountEntity } from '../entities/baza-auth-session-account.entity';
import { BazaAuthSessionAccountDto } from '@scaliolabs/baza-core-shared';
import { BazaAccountMapper } from '../../../../baza-account/src';

@Injectable()
export class BazaAuthSessionAccountMapper {
    constructor(
        private readonly accountMapper: BazaAccountMapper,
    ) {
    }

    entityToDTO(input: BazaAuthSessionAccountEntity): BazaAuthSessionAccountDto {
        return {
            id: input.id,
            account: this.accountMapper.entityToDto(input.account),
            createdAt: input.account.dateCreated.toISOString(),
            lastSignIn: input.account.lastSignIn?.toISOString(),
            authCountAttempts: input.countAuthAttempts,
            authCountSignIn: input.countAuthSignIn,
        };
    }

    entitiesToDTOs(input: Array<BazaAuthSessionAccountEntity>): Array<BazaAuthSessionAccountDto> {
        return input.map((e) => this.entityToDTO(e));
    }
}
