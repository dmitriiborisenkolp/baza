import { Injectable } from '@nestjs/common';
import { BazaAuthSessionEntity } from '../entities/baza-auth-session.entity';
import { BazaAuthSessionDto } from '@scaliolabs/baza-core-shared';
import { UAParser } from 'ua-parser-js';

@Injectable()
export class BazaAuthSessionMapper {
    entityToDTO(input: BazaAuthSessionEntity): BazaAuthSessionDto {
        const uaParser = new UAParser(input.userAgent);

        return {
            id: input.id,
            ip: input.ip,
            type: input.type,
            date: input.date.toISOString(),
            userAgent: input.userAgent,
            os: uaParser.getOS().name,
            browser: uaParser.getBrowser().name,
        };
    }

    entitiesToDTOs(input: Array<BazaAuthSessionEntity>): Array<BazaAuthSessionDto> {
        return input.map((e) => this.entityToDTO(e));
    }
}
