import 'reflect-metadata';
import {
    BazaAuthNodeAccess,
    BazaAuthSessionCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaAuthSessionRecordType, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { asyncExpect } from '../../../../../baza-test-utils/src';

describe('@scaliolabs/baza-core-api/baza-auth-sessions/integration-tests/001-baza-auth-sessions-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAuthSessions = new BazaAuthSessionCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will display sign in status after sign in as administrator', async () => {
        await http.authE2eAdmin();

        const response = await asyncExpect(
            async () => {
                const response = await dataAccessAuthSessions.accounts({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.items[0].account.fullName).toBe('E2E Admin');
                expect(response.items[0].authCountAttempts).toBe(0);
                expect(response.items[0].authCountSignIn).toBe(1);

                return response;
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const sessions = await dataAccessAuthSessions.sessions({
                    accountId: response.items[0].account.id,
                });

                expect(isBazaErrorResponse(sessions)).toBeFalsy();

                expect(sessions.items.length).toBe(1);
                expect(sessions.items[0].type).toBe(BazaAuthSessionRecordType.SignIn);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will display another sign in after additional sign in call', async () => {
        await http.authE2eAdmin();

        const response = await asyncExpect(
            async () => {
                const response = await dataAccessAuthSessions.accounts({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.items[0].account.fullName).toBe('E2E Admin');
                expect(response.items[0].authCountAttempts).toBe(0);
                expect(response.items[0].authCountSignIn).toBe(2);

                return response;
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const sessions = await dataAccessAuthSessions.sessions({
                    accountId: response.items[0].account.id,
                });

                expect(isBazaErrorResponse(sessions)).toBeFalsy();

                expect(sessions.items.length).toBe(2);
                expect(sessions.items[0].type).toBe(BazaAuthSessionRecordType.SignIn);
                expect(sessions.items[1].type).toBe(BazaAuthSessionRecordType.SignIn);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will display failed attempt to sign in', async () => {
        const authResponse = await dataAccessAuth.auth({
            email: 'e2e-admin@scal.io',
            password: 'SomeRandomPassword',
        });

        expect(isBazaErrorResponse(authResponse)).toBeTruthy();

        const response = await asyncExpect(
            async () => {
                const response = await dataAccessAuthSessions.accounts({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.items[0].account.fullName).toBe('E2E Admin');
                expect(response.items[0].authCountAttempts).toBe(1);
                expect(response.items[0].authCountSignIn).toBe(2);

                return response;
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const sessions = await dataAccessAuthSessions.sessions({
                    accountId: response.items[0].account.id,
                });

                expect(isBazaErrorResponse(sessions)).toBeFalsy();

                expect(sessions.items.length).toBe(3);
                expect(sessions.items[0].type).toBe(BazaAuthSessionRecordType.Attempt);
                expect(sessions.items[1].type).toBe(BazaAuthSessionRecordType.SignIn);
                expect(sessions.items[2].type).toBe(BazaAuthSessionRecordType.SignIn);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will display new user record after sign in as user', async () => {
        const authResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();

        const response = await asyncExpect(
            async () => {
                const response = await dataAccessAuthSessions.accounts({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(2);

                expect(response.items[0].account.fullName).toBe('E2E Admin');
                expect(response.items[0].authCountAttempts).toBe(1);
                expect(response.items[0].authCountSignIn).toBe(2);

                expect(response.items[1].account.fullName).toBe('E2E User');
                expect(response.items[1].authCountAttempts).toBe(0);
                expect(response.items[1].authCountSignIn).toBe(1);

                return response;
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const sessionsAdmin = await dataAccessAuthSessions.sessions({
                    accountId: response.items[0].account.id,
                });

                expect(isBazaErrorResponse(sessionsAdmin)).toBeFalsy();

                expect(sessionsAdmin.items.length).toBe(3);
                expect(sessionsAdmin.items[0].type).toBe(BazaAuthSessionRecordType.Attempt);
                expect(sessionsAdmin.items[1].type).toBe(BazaAuthSessionRecordType.SignIn);
                expect(sessionsAdmin.items[2].type).toBe(BazaAuthSessionRecordType.SignIn);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const sessionsUser = await dataAccessAuthSessions.sessions({
                    accountId: response.items[1].account.id,
                });

                expect(isBazaErrorResponse(sessionsUser)).toBeFalsy();

                expect(sessionsUser.items.length).toBe(1);
                expect(sessionsUser.items[0].type).toBe(BazaAuthSessionRecordType.SignIn);
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
