import { Injectable } from '@nestjs/common';

@Injectable()
export class BazaAuthSessionConfig {
    autoCleanUpAfterNDays: number;
    cleanUpIntervalHours: number;
}
