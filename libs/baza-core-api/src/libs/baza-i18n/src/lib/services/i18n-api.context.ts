import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { I18nApiService } from './i18n-api.service';
import { I18nLanguageService } from './i18n-language.service';
import { Application } from '@scaliolabs/baza-core-shared';

@Injectable({
    scope: Scope.REQUEST,
})
export class I18nApiContext {
    constructor(
        @Inject(REQUEST) private request: Request,
        private readonly service: I18nApiService,
        private readonly language: I18nLanguageService,
    ) {}

    async translate(path: string, replaces?: any): Promise<string> {
        return this.service.get(path, {
            app: Application.API,
            language: await this.language.detectLanguage(Application.API, this.request),
            replaces,
        });
    }
}
