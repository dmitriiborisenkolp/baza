import { Injectable } from '@nestjs/common';
import { BazaI18nDetector } from '../baza-i18n-detector';
import { Request as ExpressRequest } from 'express';
import { Application, ProjectLanguage } from '@scaliolabs/baza-core-shared';

@Injectable()
export class I18nLanguageService {
    private _detectors: Array<BazaI18nDetector> = [];

    async detectLanguage(app: Application, expressRequest: ExpressRequest): Promise<ProjectLanguage> {
        for (const detector of this._detectors) {
            const language = await detector.detectLanguage(app, expressRequest);

            if (language) {
                return language;
            }
        }

        return undefined;
    }

    get detectors(): Array<BazaI18nDetector> {
        return [...this._detectors];
    }

    set detectors(detectors: Array<BazaI18nDetector>) {
        this._detectors = detectors;
    }
}
