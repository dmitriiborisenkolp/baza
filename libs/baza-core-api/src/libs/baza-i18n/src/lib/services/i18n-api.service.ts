import { Inject, Injectable } from '@nestjs/common';
import type { BazaI18nGetRequest, BazaI18nGetResponse } from '@scaliolabs/baza-core-shared';
import { BAZA_I18N_API_CONFIG } from '../baza-i18n-api.config';
import type { BazaI18nApiConfig } from '../baza-i18n-api.config';
import { I18nTranslationNotFoundException } from '../exceptions/i18n-translation-not-found.exception';
import { Request as ExpressRequest } from 'express';
import { I18nLanguageNotFoundException } from '../exceptions/i18n-language-not-found.exception';
import * as _ from 'lodash';
import { Application, BAZA_COMMON_I18N_CONFIG, ProjectLanguage, replaceTags } from '@scaliolabs/baza-core-shared';
import { I18nLanguageService } from './i18n-language.service';

export interface I18nApiTranslateContext {
    app?: Application;
    language?: ProjectLanguage;
    replaces?: any;
}

@Injectable()
export class I18nApiService {
    constructor(
        @Inject(BAZA_I18N_API_CONFIG) private readonly moduleConfig: BazaI18nApiConfig,
        private readonly i18nLanguage: I18nLanguageService,
    ) {}

    async getTranslations(request: BazaI18nGetRequest, expressRequest: ExpressRequest): Promise<BazaI18nGetResponse> {
        const i18nConfig = this.moduleConfig.applications.find((c) => c.app === request.app);

        if (! i18nConfig) {
            throw new I18nTranslationNotFoundException();
        }

        const language = request.language
            ? request.language
            : await this.i18nLanguage.detectLanguage(request.app, expressRequest);

        if (! Object.values(ProjectLanguage).includes(language)) {
            throw new I18nLanguageNotFoundException();
        }

        const translations = i18nConfig.translations.find((t) => t.language === language);

        if (! translations) {
            throw new I18nTranslationNotFoundException();
        }

        return translations.resources;
    }

    get(path: string, context: I18nApiTranslateContext = {
        app: Application.API,
    }): string {
        const i18nConfig = this.moduleConfig.applications.find((c) => c.app === context.app || Application.API);

        if (! i18nConfig) {
            throw new I18nTranslationNotFoundException();
        }

        const translations = i18nConfig.translations.find((t) => t.language === (context.language || BAZA_COMMON_I18N_CONFIG.defaultLanguage));

        if (! translations) {
            throw new I18nTranslationNotFoundException();
        }

        const result = _.get(translations.resources, path, path);

        return context && context.replaces
            ? replaceTags(result, context.replaces)
            : result;
    }

    has(path: string, context: I18nApiTranslateContext): boolean {
        const i18nConfig = this.moduleConfig.applications.find((c) => c.app === context.app);

        if (! i18nConfig) {
            throw new I18nTranslationNotFoundException();
        }

        const translations = i18nConfig.translations.find((t) => t.language === (context.language || BAZA_COMMON_I18N_CONFIG.defaultLanguage));

        if (! translations) {
            throw new I18nTranslationNotFoundException();
        }

        return _.get(translations.resources, path) !== undefined;
    }
}
