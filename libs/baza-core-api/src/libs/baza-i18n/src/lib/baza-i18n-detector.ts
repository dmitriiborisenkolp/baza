import { Request as ExpressRequest } from 'express';
import { Application, ProjectLanguage } from '@scaliolabs/baza-core-shared';

export interface BazaI18nDetector {
    detectLanguage(
        app: Application,
        expressRequest: ExpressRequest,
    ): Promise<ProjectLanguage | undefined>;
}
