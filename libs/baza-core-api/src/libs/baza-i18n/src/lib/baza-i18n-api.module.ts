import { DynamicModule, Global, Module } from '@nestjs/common';
import { BAZA_I18N_API_CONFIG, BazaI18nApiConfig } from './baza-i18n-api.config';
import { I18nApiService } from './services/i18n-api.service';
import { I18nController } from './controllers/i18n.controller';
import { I18nLanguageService } from './services/i18n-language.service';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaI18nApiConfig>;
}

export { AsyncOptions as BazaI18nApiModuleAsyncOptions };

@Module({})
export class BazaI18nApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaI18nApiGlobalModule,
            providers: [{
                provide: BAZA_I18N_API_CONFIG,
                inject: options.injects,
                useFactory: async (...injects) => await options.useFactory(...injects),
            }],
            exports: [
                BAZA_I18N_API_CONFIG,
            ],
        };
    }
}

@Global()
@Module({
    controllers: [
        I18nController,
    ],
    providers: [
        I18nApiService,
        I18nLanguageService,
    ],
    exports: [
        I18nApiService,
        I18nLanguageService,
    ],
})
class BazaI18nApiGlobalModule {}
