import { Body, Controller, Post, Req } from '@nestjs/common';
import { I18nEndpoint, BazaI18nEndpointPaths, BazaI18nGetRequest, BazaI18nGetResponse } from '@scaliolabs/baza-core-shared';
import { I18nApiService } from '../services/i18n-api.service';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { Request } from 'express';

@ApiTags(CoreOpenApiTags.BazaI18n)
@Controller()
@ApiBearerAuth()
export class I18nController implements I18nEndpoint {
    constructor(private readonly service: I18nApiService) {}

    @Post(BazaI18nEndpointPaths.get)
    @ApiOperation({
        summary: 'translations',
        description: 'Return project-specific i18n resources',
    })
    @ApiOkResponse()
    async get(@Body() request: BazaI18nGetRequest, @Req() req: Request): Promise<BazaI18nGetResponse> {
        return this.service.getTranslations(request, req);
    }
}
