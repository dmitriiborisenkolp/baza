import { Application, ProjectLanguage } from '@scaliolabs/baza-core-shared';

export interface BazaI18nApiConfig {
    applications: Array<BazaI18nApiApplicationConfig>;
}

export interface BazaI18nApiApplicationConfig {
    app: Application;
    defaultLanguage: ProjectLanguage;
    translations: Array<BazaI18nApiTranslationConfig>
}

export interface BazaI18nApiTranslationConfig {
    language: ProjectLanguage;
    resources: any;
}

export const BAZA_I18N_API_CONFIG = Symbol();
