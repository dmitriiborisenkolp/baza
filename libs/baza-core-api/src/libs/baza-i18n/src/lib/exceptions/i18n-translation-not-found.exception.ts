import { I18nErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Translation file was not found';

export class I18nTranslationNotFoundException extends BazaAppException {
    constructor() {
        super(I18nErrorCodes.I18nTranslationNotFound, ERR_MESSAGE, HttpStatus.NOT_FOUND);
    }
}
