import { I18nErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Language was not found';

export class I18nLanguageNotFoundException extends BazaAppException {
    constructor() {
        super(I18nErrorCodes.I18nLanguageNotFound, ERR_MESSAGE, HttpStatus.NOT_FOUND);
    }
}
