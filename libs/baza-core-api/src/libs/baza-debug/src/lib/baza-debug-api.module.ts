import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { BazaDebugRequestLogInterceptor } from './interceptors/baza-debug-request-log.interceptor';
import { BazaRegistryApiModule } from '../../../baza-registry/src';
import { BazaLoggerModule } from '../../../baza-logger/src';

@Module({
    imports: [
        BazaRegistryApiModule,
        BazaLoggerModule,
    ],
    providers: [
        {
            provide: APP_INTERCEPTOR,
            useClass: BazaDebugRequestLogInterceptor,
        },
    ],
})
export class BazaDebugApiModule {}
