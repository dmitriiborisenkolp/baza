import { CallHandler, ExecutionContext, Injectable, NestInterceptor, Scope } from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { Request } from 'express';
import { catchError, tap } from 'rxjs/operators';
import { BazaLogger } from '../../../../baza-logger/src';
import { BazaRegistryService } from '../../../../baza-registry/src';
import * as jwt from 'jsonwebtoken';
import { JwtPayload } from '@scaliolabs/baza-core-shared';
import { EnvService } from '../../../../baza-env/src';

@Injectable({
    scope: Scope.REQUEST,
})
export class BazaDebugRequestLogInterceptor implements NestInterceptor {
    constructor(private readonly env: EnvService, private readonly logger: BazaLogger, private readonly registry: BazaRegistryService) {
        this.logger.setContext('baza-debug-log');
    }

    intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        const isEnabled = this.env.isTestEnvironment || this.registry.getValue('api.requestLogDebugMode');

        if (!isEnabled) {
            return next.handle();
        } else {
            const logEntry: Record<string, unknown> = {};

            const request: Request = context.switchToHttp().getRequest();
            const accessToken = request.get('Authorization');

            const logAccessToken = () => {
                if (accessToken) {
                    logEntry.accessToken = `[baza-debug-log] [${request.url}] [access-token]: ${accessToken}`;

                    const decoded = jwt.decode(accessToken.slice(`Bearer `.length), {
                        json: true,
                        complete: true,
                    });

                    if (decoded) {
                        const payload: JwtPayload = (decoded || {}).payload as any;

                        logEntry.userId = `[baza-debug-log] [${request.url}] [user-id]: ${payload.accountId}`;
                        logEntry.userEmail = `[baza-debug-log] [${request.url}] [user-email]: ${payload.accountEmail}`;
                    }
                }
            };

            return next.handle().pipe(
                tap((response) => {
                    logAccessToken();

                    logEntry.type = 'success';
                    logEntry.request = request.body;
                    logEntry.response = response;

                    this.logger.log(JSON.stringify(logEntry));
                }),
                catchError((err) => {
                    logAccessToken();

                    logEntry.type = 'error';
                    logEntry.request = request.body;
                    logEntry.response = err;

                    this.logger.log(JSON.stringify(logEntry));

                    return throwError(err);
                }),
            );
        }
    }
}
