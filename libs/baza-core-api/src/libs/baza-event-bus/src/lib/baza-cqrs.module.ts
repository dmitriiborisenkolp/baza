import { DynamicModule, Global, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';

@Module({})
export class BazaCqrsModule {
    static forRootAsync(): DynamicModule {
        return {
            module: BazaCqrsGlobalModule,
        };
    }
}

@Global()
@Module({
    imports: [
        CqrsModule,
    ],
    exports: [
        CqrsModule,
    ],
})
class BazaCqrsGlobalModule {}
