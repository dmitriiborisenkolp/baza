import { DynamicModule, Global, Inject, Module, OnApplicationBootstrap, OnApplicationShutdown, OnModuleInit } from '@nestjs/common';
import { ApiEventBus } from './services/api-event-bus.service';
import { BAZA_EVENT_BUS_API_CONFIG } from './baza-event-bus-api.config';
import type { BazaEventBusApiConfig } from './baza-event-bus-api.config';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BazaCqrsModule } from './baza-cqrs.module';
import { BazaKafkaApiModule } from '../../../baza-kafka/src';
import { BazaLogger } from '../../../baza-logger/src';
import { bazaCqrsUseBazaLogger, bazaCqrsUseMockLogger } from '../../../baza-common/src';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaEventBusApiConfig>;
}

const services = [ApiEventBus];

export { AsyncOptions as BazaEventBusApiModuleAsyncOptions };

@Module({})
export class BazaEventBusApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaEventBusApiGlobalModule,
            providers: [
                {
                    provide: BAZA_EVENT_BUS_API_CONFIG,
                    inject: options.injects,
                    useFactory: async (...injects) => options.useFactory(...injects),
                },
            ],
            exports: [BAZA_EVENT_BUS_API_CONFIG],
        };
    }
}

@Global()
@Module({
    imports: [BazaCqrsModule, BazaKafkaApiModule],
    providers: [...services],
    exports: [BazaCqrsModule, ...services],
})
class BazaEventBusApiGlobalModule implements OnModuleInit, OnApplicationBootstrap, OnApplicationShutdown {
    private onApplicationShutdown$: Subject<void> = new Subject<void>();

    constructor(
        @Inject(BAZA_EVENT_BUS_API_CONFIG) private readonly moduleConfig: BazaEventBusApiConfig,
        private readonly bazaEventBus: ApiEventBus<any, any>,
        private readonly logger: BazaLogger,
    ) {}

    onModuleInit(): void {
        this.moduleConfig.enableDebugLog ? bazaCqrsUseBazaLogger(this.logger) : bazaCqrsUseMockLogger();
    }

    async onApplicationBootstrap(): Promise<void> {
        if (this.moduleConfig.enableDebugLog) {
            this.bazaEventBus.currentNodeEvents$.pipe(takeUntil(this.onApplicationShutdown$)).subscribe((e) => {
                this.logger.log(`[BazaEventBusApiGlobalModule] [NEW EVENT] [CURRENT_NODE] ${e.event.topic}`);
                this.logger.log(e);
            });

            this.bazaEventBus.everyNodeEvents$.pipe(takeUntil(this.onApplicationShutdown$)).subscribe((e) => {
                this.logger.log(`[BazaEventBusApiGlobalModule] [NEW EVENT] [EVERY_NODE] ${e.event.topic}`);
                this.logger.log(e);
            });
        }

        if (this.moduleConfig.withKafkaFeature) {
            this.bazaEventBus.initPipeFromKafkaEvents(this.onApplicationShutdown$);
        }
    }

    async onApplicationShutdown(): Promise<void> {
        this.onApplicationShutdown$.next();
    }
}
