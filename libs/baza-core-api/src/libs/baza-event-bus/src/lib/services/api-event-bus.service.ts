import { filter, takeUntil } from 'rxjs/operators';
import { Inject, Injectable } from '@nestjs/common';
import { Observable, Subject } from 'rxjs';
import {
    ApiCqrsEvent,
    ApiEvent,
    ApiEventPayload,
    ApiEventSource,
    BazaEventBusKafkaMessage,
    BazaEventBusKafkaTopic,
    generateRandomHexString,
} from '@scaliolabs/baza-core-shared';
import { EventBus } from '@nestjs/cqrs';
import type { BazaEventBusApiConfig } from '../baza-event-bus-api.config';
import { BAZA_EVENT_BUS_API_CONFIG } from '../baza-event-bus-api.config';
import { KafkaService } from '../../../../baza-kafka/src';
import { BazaLogger } from '../../../../baza-logger/src';

const UNIQUE_EVENT_HEX_ID_LENGTH = 8;

interface SubscribeRequest<TOPIC, PAYLOAD extends ApiEvent<TOPIC>> {
    callback(event: ApiEventPayload<TOPIC, PAYLOAD>): Promise<void>;
    source: ApiEventSource;
    filterByEvent?: Array<TOPIC>;
    takeUntil$?: Subject<void>;
}

interface PublishOptions {
    /**
     * Events Pipe
     *
     * - ApiEventSource.CurrentNode - Event will be processed within current node, w/o publishing to Kafka
     * - ApiEventSource.CurrentNode - Event will be published to Kafka and will be processed by every node in cluster
     */
    target: ApiEventSource;

    /**
     * By defaults all events will be filtered by namespace.
     * Set true if every projects / every deployment target should receive target within same Kafka instance.
     */
    shared?: boolean;
}

const defaultPublishOptions: PublishOptions = {
    target: ApiEventSource.CurrentNode,
    shared: false,
};

export { SubscribeRequest as AppBusServiceSubscribeRequest };
export { PublishOptions as AppBusServicePublishOptions };

@Injectable()
export class ApiEventBus<TOPIC, PAYLOAD extends ApiEvent<TOPIC>> {
    private _currentNodeEvents$: Subject<ApiEventPayload<TOPIC, PAYLOAD>> = new Subject<ApiEventPayload<TOPIC, PAYLOAD>>();
    private _everyNodeEvents$: Subject<ApiEventPayload<TOPIC, PAYLOAD>> = new Subject<ApiEventPayload<TOPIC, PAYLOAD>>();

    constructor(
        @Inject(BAZA_EVENT_BUS_API_CONFIG) private readonly moduleConfig: BazaEventBusApiConfig,
        private readonly logger: BazaLogger,
        private readonly cqrsEventBus: EventBus,
        private readonly kafka: KafkaService<BazaEventBusKafkaTopic, BazaEventBusKafkaMessage<TOPIC, PAYLOAD>>,
    ) {}

    get currentNodeEvents$(): Observable<ApiEventPayload<TOPIC, PAYLOAD>> {
        return this._currentNodeEvents$.asObservable();
    }

    get everyNodeEvents$(): Observable<ApiEventPayload<TOPIC, PAYLOAD>> {
        return this._everyNodeEvents$.asObservable();
    }

    initPipeFromKafkaEvents(takeUntil$: Subject<void>): void {
        this.kafka.kafkaEvents$.pipe(takeUntil(takeUntil$)).subscribe((e) => {
            switch (e.topic) {
                case BazaEventBusKafkaTopic.ApiEvent: {
                    this._everyNodeEvents$.next(e.payload.payload);
                }
            }
        });
    }

    async publish(event: PAYLOAD, options: PublishOptions = defaultPublishOptions): Promise<ApiEventPayload<TOPIC, PAYLOAD>> {
        const id = generateRandomHexString(UNIQUE_EVENT_HEX_ID_LENGTH);

        const eventBody: ApiEventPayload<TOPIC, PAYLOAD> = {
            id,
            source: options.target,
            event: {
                topic: event.topic,
                payload: event.payload,
            } as any,
        };

        if (options.target === ApiEventSource.EveryNode) {
            if (this.moduleConfig.withKafkaFeature) {
                await this.kafka.publish(
                    {
                        topic: BazaEventBusKafkaTopic.ApiEvent,
                        payload: {
                            topic: BazaEventBusKafkaTopic.ApiEvent,
                            payload: eventBody,
                        },
                    },
                    {
                        shared: options.shared,
                    },
                );
            } else {
                this.logger.error(`[ApiEventBus] [${event.topic}] Failed to publish event via Kafka; withKafkaFeature flag is disabled`);
            }
        } else if (options.target === ApiEventSource.CurrentNode) {
            this._currentNodeEvents$.next(eventBody);
            this.cqrsEventBus.publish(new ApiCqrsEvent(eventBody));
        } else {
            this.logger.error(`[ApiEventBus] [${event.topic}] Failed to publish event; Unknown target "${options.target}"`);
        }

        return eventBody;
    }

    subscribe(request: SubscribeRequest<TOPIC, PAYLOAD>): void {
        switch (request.source) {
            default: {
                this.logger.error(`[ApiEventBus] Failed to subscribe for events; Unknown target "${request.source}"`);

                break;
            }

            case ApiEventSource.CurrentNode: {
                this.subscribeCurrentNode(request);

                return;
            }

            case ApiEventSource.EveryNode: {
                this.subscribeEveryNode(request);

                return;
            }
        }
    }

    private subscribeCurrentNode(request: SubscribeRequest<TOPIC, PAYLOAD>): void {
        let observable = this.currentNodeEvents$;

        if (request.filterByEvent) {
            observable = observable.pipe(filter((e) => request.filterByEvent.includes(e.event.topic)));
        }

        if (request.takeUntil$) {
            observable = observable.pipe(takeUntil(request.takeUntil$));
        }

        observable.subscribe((next) => {
            request.callback(next).catch((err) => this.logger.error(`[BazaApiEventBusService] [subscribePromise] ${err}`));
        });
    }

    private subscribeEveryNode(request: SubscribeRequest<TOPIC, PAYLOAD>): void {
        let observable = this.everyNodeEvents$;

        if (request.filterByEvent) {
            observable = observable.pipe(filter((e) => request.filterByEvent.includes(e.event.topic)));
        }

        if (request.takeUntil$) {
            observable = observable.pipe(takeUntil(request.takeUntil$));
        }

        observable.subscribe((next) => {
            request.callback(next).catch((err) => this.logger.error(`[BazaApiEventBusService] [subscribePromise] ${err}`));
        });
    }
}
