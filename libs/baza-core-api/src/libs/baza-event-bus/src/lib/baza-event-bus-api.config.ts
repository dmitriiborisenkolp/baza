export interface BazaEventBusApiConfig {
    withKafkaFeature: boolean;
    enableDebugLog: boolean;
}

export const BAZA_EVENT_BUS_API_CONFIG = Symbol();
