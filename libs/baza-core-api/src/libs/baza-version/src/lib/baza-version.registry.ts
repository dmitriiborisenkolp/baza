import { RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';
import { BAZA_VERSION_DEFAULT_MESSAGE } from '@scaliolabs/baza-core-shared';

export const bazaVersionRegistry: RegistrySchema = {
    bazaCoreVersion: {
        message: {
            name: 'App Is Outdated Message',
            type: RegistryType.String,
            defaults: BAZA_VERSION_DEFAULT_MESSAGE,
            hiddenFromList: false,
            public: false,
        },
    }
};
