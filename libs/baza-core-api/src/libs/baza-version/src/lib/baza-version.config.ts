import { Application } from '@scaliolabs/baza-core-shared';

export interface BazaVersionModuleConfig {
    packageJSONPath: string;
    forceCheckXApiHeaders: (app: Application | undefined) => boolean;
    httpHeaderVersion: string;
    httpHeaderSupportedVersion: string;
    httpHeaderApplication: string;
    httpHeaderApplicationId: string;
    httpHeaderVersionSkipValue: string;
    ignoreVersionCheckForEndpoints: Array<string>;
}

export const BAZA_VERSION_CONFIG = Symbol();
