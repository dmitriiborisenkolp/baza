import { Inject, Injectable } from '@nestjs/common';
import { Version } from '../models/version';
import * as jsonfile from 'jsonfile';
import type { BazaVersionModuleConfig } from '../baza-version.config';
import { BAZA_VERSION_CONFIG } from '../baza-version.config';

@Injectable()
export class VersionService {
    private packageJSON: {
        version: Version;
        supported: Array<Version>;
    };

    constructor(
        @Inject(BAZA_VERSION_CONFIG) private readonly moduleConfig: BazaVersionModuleConfig,
    ) {
        this.packageJSON = jsonfile.readFileSync(moduleConfig.packageJSONPath)
    }

    get current(): Version {
        return this.packageJSON.version;
    }

    get supported(): Array<Version> {
        return this.packageJSON.supported;
    }

    isVersionSupported(input: Version): boolean {
        return [this.current, ...this.supported].includes(input);
    }
}
