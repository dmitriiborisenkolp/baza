import { Controller, Get } from '@nestjs/common';
import { VersionService } from '../services/version.service';
import { VersionDto, VersionEndpoint, VersionEndpointPaths } from '@scaliolabs/baza-core-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaEnvironments, CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { RequestContextService } from '../../../../baza-common/src';
import { EnvService } from '../../../../baza-env/src';

@ApiTags(CoreOpenApiTags.BazaVersion)
@Controller()
export class VersionController implements VersionEndpoint {
    constructor(
        private readonly service: VersionService,
        private readonly requestContext: RequestContextService,
        private readonly envService: EnvService,
    ) {}

    @Get(VersionEndpointPaths.version)
    @ApiOperation({
        summary: 'version',
        description: 'Returns API version details',
    })
    @ApiOkResponse({
        type: VersionDto,
    })
    async version(): Promise<VersionDto> {
        const context = await this.requestContext.current();

        return {
            current: this.service.current,
            supported: this.service.supported,
            bazaEnv: this.envService.getAsEnum<BazaEnvironments>('BAZA_ENV', {
                enum: BazaEnvironments,
            }) as BazaEnvironments,
            application: context.application,
        };
    }
}
