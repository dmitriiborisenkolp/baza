import { HttpStatus } from '@nestjs/common';
import { BAZA_VERSION_DEFAULT_MESSAGE, VersionErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class VersionIsNotSupportedException extends BazaAppException {
    constructor(args: {
        project: string;
        current: string;
        requested: string;
    }, message: string = BAZA_VERSION_DEFAULT_MESSAGE) {
        super(VersionErrorCodes.VersionIsOutdated, message, HttpStatus.NOT_ACCEPTABLE, args);
    }
}
