import { CallHandler, ExecutionContext, Inject, Injectable, NestInterceptor, Scope } from '@nestjs/common';
import { Observable } from 'rxjs';
import { VersionService } from '../services/version.service';
import { Request as ExpressRequest, Request, Response } from 'express';
import { VersionIsNotSupportedException } from '../exceptions/version-is-not-supported.exception';
import type { BazaVersionModuleConfig } from '../baza-version.config';
import { BAZA_VERSION_CONFIG } from '../baza-version.config';
import { BAZA_VERSION_DEFAULT_MESSAGE_REGISTRY_KEY, getBazaProjectName, VersionEndpointPaths } from '@scaliolabs/baza-core-shared';
import { BazaI18nEndpointPaths } from '@scaliolabs/baza-core-shared';
import { Application, DEFAULT_APPLICATION } from '@scaliolabs/baza-core-shared';
import { RequestContextService } from '../../../../baza-common/src';
import { BazaRegistryService } from '../../../../baza-registry/src';

export const SKIP_FOR_ENDPOINTS = [VersionEndpointPaths.version, BazaI18nEndpointPaths.get];

@Injectable({
    scope: Scope.REQUEST,
})
export class XApiVersionInterceptor implements NestInterceptor {
    constructor(
        @Inject(BAZA_VERSION_CONFIG) private readonly moduleConfig: BazaVersionModuleConfig,
        private readonly service: VersionService,
        private readonly requestContext: RequestContextService,
        private readonly registry: BazaRegistryService,
    ) {}

    intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        const request: Request = context.switchToHttp().getRequest();
        const path = context.switchToHttp().getRequest<ExpressRequest>().path;
        const headerUserAgent = request.header('User-Agent');

        const isKube = (headerUserAgent || '').toLowerCase().includes('kube');

        if (isKube || SKIP_FOR_ENDPOINTS.includes(path as any) || this.moduleConfig.ignoreVersionCheckForEndpoints.includes(path as any)) {
            return this.skipVersionCheck(next);
        } else {
            return this.performVersionCheck(context, next);
        }
    }

    private skipVersionCheck(next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        return next.handle();
    }

    private performVersionCheck(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        const request: Request = context.switchToHttp().getRequest();
        const response: Response = context.switchToHttp().getResponse();

        const requestedApplication = (request.header(this.moduleConfig.httpHeaderApplication) || DEFAULT_APPLICATION) as Application;
        const requestedApplicationId = request.header(this.moduleConfig.httpHeaderApplicationId);
        const requestedApiVersion = request.header(this.moduleConfig.httpHeaderVersion);

        const forceCheckVersion = this.moduleConfig.forceCheckXApiHeaders(requestedApplication as any);

        const message = this.registry.getValue(BAZA_VERSION_DEFAULT_MESSAGE_REGISTRY_KEY);
        const args = {
            project: getBazaProjectName(),
            current: this.service.current,
            requested: requestedApiVersion,
        };

        if (!requestedApiVersion && forceCheckVersion) {
            throw new VersionIsNotSupportedException(args, message);
        }

        this.requestContext.patch = {
            application: Object.values(Application).includes(requestedApplication) ? requestedApplication : DEFAULT_APPLICATION,
            applicationId: requestedApplicationId,
            version: requestedApiVersion,
        };

        if (
            (requestedApiVersion || '') == '' ||
            (requestedApiVersion || '').toUpperCase() === this.moduleConfig.httpHeaderVersionSkipValue.toUpperCase()
        ) {
            return next.handle();
        } else {
            if (!requestedApiVersion || !this.service.isVersionSupported(requestedApiVersion)) {
                throw new VersionIsNotSupportedException(args, message);
            }

            response.setHeader(this.moduleConfig.httpHeaderApplication, requestedApplication);
            response.setHeader(this.moduleConfig.httpHeaderVersion, this.service.current);
            response.setHeader(this.moduleConfig.httpHeaderSupportedVersion, this.service.supported.join(','));

            return next.handle();
        }
    }
}
