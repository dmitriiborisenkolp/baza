import { DynamicModule, Global, Module } from '@nestjs/common';
import { VersionService } from './services/version.service';
import { VersionController } from './controllers/version.controller';
import { BAZA_VERSION_CONFIG, BazaVersionModuleConfig } from './baza-version.config';
import { XApiVersionInterceptor } from './interceptors/x-api-version.interceptor';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { BazaApiCommonModule } from '../../../baza-common/src';
import { BazaEnvModule } from '../../../baza-env/src';
import { BazaRegistryApiModule } from '../../../baza-registry/src';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaVersionModuleConfig>;
}

export { AsyncOptions as BazaVersionModuleAsyncOptions };

@Module({})
export class BazaVersionModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaVersionGlobalModule,
            providers: [
                {
                    provide: BAZA_VERSION_CONFIG,
                    inject: options.injects,
                    useFactory: async (...injects) => await options.useFactory(...injects),
                },
            ],
            exports: [
                BAZA_VERSION_CONFIG,
            ],
        };
    }
}

@Global()
@Module({
    imports: [
        BazaEnvModule,
        BazaApiCommonModule,
        BazaRegistryApiModule,
    ],
    controllers: [
        VersionController,
    ],
    providers: [
        VersionService,
        XApiVersionInterceptor,
        {
            provide: APP_INTERCEPTOR,
            useClass: XApiVersionInterceptor,
        },
    ],
    exports: [
        VersionService,
        XApiVersionInterceptor,
    ],
})
class BazaVersionGlobalModule {
}
