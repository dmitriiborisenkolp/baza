import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaVersionNodeAccess } from '@scaliolabs/baza-core-node-access';
import { Application, BazaError, BazaHttpHeaders, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BAZA_VERSION_DEFAULT_MESSAGE_REGISTRY_KEY, VersionErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaRegistryNodeAccess } from '@scaliolabs/baza-core-node-access';

// Register account flow
describe('@scaliolabs/baza-version/integration-tests/01-baza-version.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessVersion = new BazaVersionNodeAccess(http);
    const dataAccessRegistry = new BazaRegistryNodeAccess(http);

    let CURRENT_VERSION: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will correctly returns current version', async () => {
        const response = await dataAccessVersion.version();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(typeof response.current === 'string').toBeTruthy();

        CURRENT_VERSION = response.current;
    });

    it('will fail if user provides outdated version', async () => {
        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_VERSION]: '0.0.0',
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(VersionErrorCodes.VersionIsOutdated);
    });

    it('will success if user provides current version', async () => {
        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_VERSION]: CURRENT_VERSION,
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will success if user provides SKIP version', async () => {
        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_VERSION]: BazaHttpHeaders.HTTP_HEADER_VERSION_SKIP,
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display custom version error message', async () => {
        await http.authE2eAdmin();

        const updateResponse = await dataAccessRegistry.updateSchemaRecord({
            path: BAZA_VERSION_DEFAULT_MESSAGE_REGISTRY_KEY,
            value: 'Version E2E Outdated',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_VERSION]: '0.0.0',
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(VersionErrorCodes.VersionIsOutdated);
        expect(response.message).toBe('Version E2E Outdated');
    });

    it('will success if user is in kube environment', async () => {
        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_VERSION]: '',
                'User-Agent': 'example-kube-v1.0.0',
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
