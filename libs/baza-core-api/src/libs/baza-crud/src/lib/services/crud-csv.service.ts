import { Injectable } from '@nestjs/common';
import { BAZA_CRUD_CONSTANTS, CrudExportToCsvField, CrudListRequestDto } from '@scaliolabs/baza-core-shared';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { CrudService } from './crud.service';

const FETCH_PAGE_SIZE = 1000;

export interface CrudExportToCsvServiceRequest<T, DTO = T> {
    entity: any;
    fields: Array<CrudExportToCsvField<DTO>>;
    delimiter?: string;
    findOptions?: FindManyOptions<T> | false; // Use false to return empty CSV file
    request?: Exclude<CrudListRequestDto<DTO>, 'includeFields' | 'excludeFields'>;
    mapper?: (items: Array<T>) => Promise<Array<DTO>>;
}

function csvEscape(input: string, delimiter: string): string {
    let result = input;

    if (result.includes(delimiter) || result.includes('"')) {
        result = result.split('"').join('""');

        return `"${result}"`;
    } else {
        return result;
    }
}

@Injectable()
export class CrudCsvService {
    constructor(private readonly crud: CrudService) {}

    async exportToCsv<T, DTO = T>(request: CrudExportToCsvServiceRequest<T, DTO>): Promise<string> {
        const source = await this.fetchSource<T, DTO>(request);

        const lines: Array<string> = [];

        if (!request.delimiter) {
            request.delimiter = BAZA_CRUD_CONSTANTS.csvDefaultDelimiter;
        }

        lines.push(
            request.fields
                .filter((field) => !field.disabled)
                .map((field) => {
                    return csvEscape((field.title || '').toString(), request.delimiter);
                })
                .join(request.delimiter),
        );

        for (const line of source) {
            const fields: Array<string> = [];

            for (const fieldConfig of request.fields) {
                // eslint-disable-next-line no-prototype-builtins
                if (line.hasOwnProperty(fieldConfig.field)) {
                    const value = line[fieldConfig.field] as any;

                    if (value === true || value === false) {
                        fields.push(value ? 'Yes' : 'No');
                    } else {
                        fields.push(csvEscape((value || '').toString(), request.delimiter));
                    }
                } else {
                    fields.push('');
                }
            }

            lines.push(fields.join(request.delimiter));
        }

        return lines.join('\n');
    }

    private async fetchSource<T, DTO = T>(request: CrudExportToCsvServiceRequest<T, DTO>): Promise<Array<DTO>> {
        if (request.findOptions) {
            let index = 1;
            let fetched = -1;

            const result: Array<DTO> = [];

            do {
                const listResponse = await this.crud.find<T, DTO>({
                    entity: request.entity,
                    findOptions: request.findOptions,
                    mapper: request.mapper,
                    request: {
                        queryString: request.request?.queryString,
                        sort: request.request?.sort,
                        index,
                        size: FETCH_PAGE_SIZE,
                    },
                });

                fetched = listResponse.items.length;
                result.push(...listResponse.items);

                index++;
            } while (fetched > 0);

            return result;
        } else {
            return [];
        }
    }
}
