import { Injectable } from '@nestjs/common';
import { CrudSetSortOrderByUlidResponseDto, CrudSetSortOrderResponseDto, CrudSortableEntity, CrudSortableUlidEntity } from '@scaliolabs/baza-core-shared';
import { Connection } from 'typeorm';

export class SetSortOrderRequest<T = any, DTO = T> {
    id: number;
    setSortOrder: number;
    entities: Array<CrudSortableEntity>;
    mapper?: (items: Array<T>) => Promise<Array<DTO>>;
}

export class SetSortOrderByUlidRequest<T = any, DTO = T> {
    ulid: string;
    setSortOrder: number;
    entities: Array<CrudSortableUlidEntity>;
    mapper?: (items: Array<T>) => Promise<Array<DTO>>;
}

@Injectable()
export class CrudSortService {
    constructor(
        private readonly connection: Connection,
    ) {}

    async setSortOrder<T extends CrudSortableEntity, DTO extends CrudSortableEntity = T>(request: SetSortOrderRequest): Promise<CrudSetSortOrderResponseDto<DTO>> {
        const entity = request.entities.find((e) => e.id === request.id) as T;
        const entities = request.entities.filter((e) => e.id !== request.id);

        await this.normalize(entities);

        entities
            .filter((e) => e.sortOrder >= request.setSortOrder)
            .forEach((e) => e.sortOrder++);

        entities
            .filter((e) => e.sortOrder <= request.setSortOrder)
            .forEach((e) => e.sortOrder--);

        entity.sortOrder = request.setSortOrder;

        await this.normalize([...entities, entity]);

        if (request.mapper) {
            return {
                entity: (await request.mapper([entity]))[0],
                affected: await request.mapper([...entities, entity] as Array<T>),
            };
        } else {
            return {
                entity: entity as any,
                affected: [...entities, entity] as Array<DTO>,
            };
        }
    }

    async setSortOrderUlidEntities<T extends CrudSortableUlidEntity, DTO extends CrudSortableUlidEntity = T>(request: SetSortOrderByUlidRequest): Promise<CrudSetSortOrderByUlidResponseDto<DTO>> {
        const entity = request.entities.find((e) => e.ulid === request.ulid) as T;
        const entities = request.entities.filter((e) => e.ulid !== request.ulid);

        await this.normalize(entities);

        entities
            .filter((e) => e.sortOrder >= request.setSortOrder)
            .forEach((e) => e.sortOrder++);

        entities
            .filter((e) => e.sortOrder <= request.setSortOrder)
            .forEach((e) => e.sortOrder--);

        entity.sortOrder = request.setSortOrder;

        await this.normalize([...entities, entity]);

        if (request.mapper) {
            return {
                entity: (await request.mapper([entity]))[0],
                affected: await request.mapper([...entities, entity] as Array<T>),
            };
        } else {
            return {
                entity: entity as any,
                affected: [...entities, entity] as Array<DTO>,
            };
        }
    }

    async getLastSortOrder(entity: any): Promise<number> {
        const repository = await this.connection.getRepository(entity);

        const result: { max: number } = await repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .getRawOne();

        return (result.max + 1) || 1;
    }

    async normalize<T extends CrudSortableEntity | CrudSortableUlidEntity>(entities: Array<T>): Promise<Array<T>> {
        entities.sort((a, b) => a.sortOrder - b.sortOrder);

        for (let i = 0; i < entities.length; i++) {
            entities[i].sortOrder = i + 1;
        }

        return entities;
    }
}
