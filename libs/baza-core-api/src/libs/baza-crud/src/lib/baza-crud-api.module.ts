import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CrudService } from './services/crud.service';
import { CrudSortService } from './services/crud-sort.service';
import { CrudCsvService } from './services/crud-csv.service';

@Module({
    imports: [
        TypeOrmModule,
    ],
    providers: [
        CrudService,
        CrudSortService,
        CrudCsvService,
    ],
    exports: [
        CrudService,
        CrudSortService,
        CrudCsvService,
    ],
})
export class BazaCrudApiModule {}
