import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { Request as ExpressRequest } from 'express';

export class BazaCrudInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
        const request: ExpressRequest = context.switchToHttp().getRequest();
        const requestBody: CrudListRequestDto<any> = request.body;

        const shouldFilterFields = !! requestBody
            && typeof requestBody === 'object'
            && (Array.isArray(requestBody.excludeFields) || Array.isArray(requestBody.includeFields))
            && ((requestBody.excludeFields || []).length > 0 || (requestBody.includeFields || []).length > 0);

        return next.handle().pipe(
            map((response: CrudListResponseDto<any>) => {
                if (shouldFilterFields && Array.isArray(response.items) && response.items.length > 0) {
                    const fields = Object.keys(response.items[0])
                        .filter((key) => (requestBody.includeFields || []).length === 0 || (requestBody.includeFields || []).includes(key))
                        .filter((key) => (requestBody.excludeFields || []).length === 0 || ! (requestBody.excludeFields || []).includes(key));

                    const items: Array<any> = [];

                    for (const item of response.items) {
                        const rebuild = {};

                        for (const field of fields) {
                            rebuild[field] = item[field];
                        }

                        items.push(rebuild);
                    }

                    return {
                        items,
                        pager: response.pager,
                    };
                } else {
                    return response;
                }
            }),
        );
    }
}
