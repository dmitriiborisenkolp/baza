import { Module } from '@nestjs/common';
import { AccountController } from './controllers/account.controller';
import { AccountCmsController } from './controllers/account-cms.controller';
import { AccountCmsResetPasswordController } from './controllers/account-cms-reset-password.controller';
import { BazaAccountApiModule } from '../../../baza-account/src';

@Module({
    imports: [BazaAccountApiModule],
    controllers: [AccountController, AccountCmsController, AccountCmsResetPasswordController],
})
export class BazaAccountApiControllersModule {}
