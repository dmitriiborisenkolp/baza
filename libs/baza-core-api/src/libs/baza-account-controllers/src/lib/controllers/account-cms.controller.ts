import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    AccountAcl,
    AccountCmsDto,
    AccountCmsEndpoint,
    AccountCmsEndpointPaths,
    AccountRole,
    AssignAdminAccessCmsRequest,
    AssignAdminAccessCmsResponse,
    ChangeEmailAccountCmsRequest,
    ChangeEmailAccountCmsResponse,
    ConfirmEmailAccountByIdCmsRequest,
    ConfirmEmailAccountByIdCmsResponse,
    CoreAcl,
    DeactivateAccountCmsRequest,
    DeactivateAccountCmsResponse,
    DeleteAccountByIdCmsRequest,
    DeleteAccountByIdCmsResponse,
    ExportToCsvAccountRequest,
    GetAccountByEmailCmsRequest,
    GetAccountByEmailCmsResponse,
    GetAccountByIdCmsRequest,
    GetAccountByIdCmsResponse,
    ListAccountsCmsRequest,
    ListAccountsCmsResponse,
    RegisterAdminAccountCmsRequest,
    RegisterAdminAccountCmsResponse,
    RegisterUserAccountCmsRequest,
    RegisterUserAccountCmsResponse,
    RevokeAdminAccessCmsRequest,
    RevokeAdminAccessCmsResponse,
    UpdateAccountCmsRequest,
    UpdateAccountCmsResponse,
} from '@scaliolabs/baza-core-shared';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthRequireAdminRoleGuard, ReqAccount } from '../../../../baza-auth/src';
import {
    BazaAccountCmsService,
    AccountNoAccessToRevokeAccountException,
    BazaAccountRepository,
    BazaAccountCmsMapper,
    AccountEntity,
} from '../../../../baza-account/src';
import { AclNodes, AclService } from '../../../../baza-acl/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src';

@Controller()
@ApiTags(CoreOpenApiTags.BazaAccountCMS)
export class AccountCmsController implements AccountCmsEndpoint {
    constructor(
        private readonly service: BazaAccountCmsService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly mapper: BazaAccountCmsMapper,
        private readonly aclService: AclService,
    ) {}

    @Post(AccountCmsEndpointPaths.registerAdminAccount)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsAdminCreate])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'registerAdminAccount',
        description: 'Register new admin account',
    })
    @ApiOkResponse({
        type: AccountCmsDto,
    })
    async registerAdminAccount(@Body() request: RegisterAdminAccountCmsRequest): Promise<RegisterAdminAccountCmsResponse> {
        const entity = await this.service.registerAdminAccount(request, {
            withPasswordValidation: true,
            ignoreManagedUserAccountRestrictions: false,
            destroyJwtSessions: true,
        });

        return this.mapper.entityToDTO(entity);
    }

    @Post(AccountCmsEndpointPaths.registerUserAccount)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsCreate])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'registerUserAccount',
        description: 'Register new user account',
    })
    @ApiOkResponse({
        type: AccountCmsDto,
    })
    async registerUserAccount(@Body() request: RegisterUserAccountCmsRequest): Promise<RegisterUserAccountCmsResponse> {
        const entity = await this.service.registerUserAccount(request, {
            withPasswordValidation: true,
            ignoreManagedAccountRestrictions: false,
        });

        return this.mapper.entityToDTO(entity);
    }

    @Post(AccountCmsEndpointPaths.listAccounts)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'listAdminAccounts',
        description: 'List of admin accounts',
    })
    @ApiOkResponse({
        type: ListAccountsCmsResponse,
    })
    async listAccounts(@Body() request: ListAccountsCmsRequest): Promise<ListAccountsCmsResponse> {
        const response = await this.service.listAccounts(request);

        for (const account of response.items) {
            if (account.role === AccountRole.Admin) {
                const acl = await this.aclService.getAclOfAccount(account.id);

                account.isSystemRoot = acl.acl.includes(CoreAcl.SystemRoot);
            }
        }

        return response;
    }

    @Post(AccountCmsEndpointPaths.assignAdminAccess)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsAssign])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'assignAdminAccess',
        description: 'Assign admin access to account',
    })
    @ApiOkResponse()
    async assignAdminAccess(@Body() request: AssignAdminAccessCmsRequest): Promise<AssignAdminAccessCmsResponse> {
        await this.service.assignAdminRole(request);
    }

    @Post(AccountCmsEndpointPaths.revokeAdminAccess)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsRevoke])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'revokeAdminAccess',
        description: 'Revoke admin access from account',
    })
    @ApiOkResponse()
    async revokeAdminAccess(
        @Body() request: RevokeAdminAccessCmsRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<RevokeAdminAccessCmsResponse> {
        const revokerAcl = await this.aclService.getAclOfAccount(account);

        const target = await this.accountRepository.getActiveAccountWithEmail(request.email);
        const targetAcl = await this.aclService.getAclOfAccount(target);

        if (targetAcl.acl.includes(CoreAcl.SystemRoot) && !revokerAcl.acl.includes(CoreAcl.SystemRoot)) {
            throw new AccountNoAccessToRevokeAccountException();
        }

        await this.service.revokeAdminRole(request);
    }

    @Post(AccountCmsEndpointPaths.getAccountByEmail)
    @AclNodes([AccountAcl.Accounts])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getAccountByEmail',
        description: 'Search for account by email and returns the account DTO ',
    })
    @ApiOkResponse({
        type: AccountCmsDto,
    })
    async getAccountByEmail(@Body() request: GetAccountByEmailCmsRequest): Promise<GetAccountByEmailCmsResponse> {
        const account = await this.service.getAccountByEmail(request);

        return this.mapper.entityToDTO(account);
    }

    @Post(AccountCmsEndpointPaths.getAccountById)
    @AclNodes([AccountAcl.Accounts])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getAccountById',
        description: 'Search for account by id and returns the account DTO ',
    })
    @ApiOkResponse({
        type: AccountCmsDto,
    })
    async getAccountById(@Body() request: GetAccountByIdCmsRequest): Promise<GetAccountByIdCmsResponse> {
        const account = await this.service.getAccountById(request);

        return this.mapper.entityToDTO(account);
    }

    @Post(AccountCmsEndpointPaths.deleteAccountById)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsDelete])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'deleteAccountById',
        description: 'Search for account by id and delete the account',
    })
    @ApiOkResponse()
    async deleteAccountById(@Body() request: DeleteAccountByIdCmsRequest): Promise<DeleteAccountByIdCmsResponse> {
        await this.service.deleteAccountById(request);
    }

    @Post(AccountCmsEndpointPaths.confirmEmailAccountById)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsActivate])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'confirmEmailAccountById',
        description: 'Confirm account email address',
    })
    @ApiOkResponse({
        type: AccountCmsDto,
    })
    async confirmEmailAccountById(
        @Body() request: ConfirmEmailAccountByIdCmsRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<ConfirmEmailAccountByIdCmsResponse> {
        const confirmedAccount = await this.service.confirmEmailAccountById(request, account);

        return this.mapper.entityToDTO(confirmedAccount);
    }

    @Post(AccountCmsEndpointPaths.updateAccount)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsUpdate])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'updateAccount',
        description: 'Update accounts details',
    })
    @ApiOkResponse({
        type: AccountCmsDto,
    })
    async updateAccount(@Body() request: UpdateAccountCmsRequest): Promise<UpdateAccountCmsResponse> {
        const account = await this.service.updateAccount(request);

        return this.mapper.entityToDTO(account);
    }

    @Post(AccountCmsEndpointPaths.exportToCsv)
    @AclNodes([AccountAcl.Accounts])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'exportToCsv',
        description: 'Export accounts to CSV',
    })
    @ApiOkResponse()
    async exportToCsv(@Body() request: ExportToCsvAccountRequest): Promise<string> {
        return this.service.exportToCsv(request);
    }

    @Post(AccountCmsEndpointPaths.deactivateAccount)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsDeactivate])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'deactivateAccount',
        description: 'Deactivate account and data related with account will be anonymized',
    })
    @ApiOkResponse({
        type: AccountCmsDto,
    })
    async deactivateAccount(@Body() request: DeactivateAccountCmsRequest): Promise<DeactivateAccountCmsResponse> {
        const account = await this.accountRepository.getActiveAccountWithEmail(request.email);

        await this.service.deactivateAccount(account);

        return this.mapper.entityToDTO(account);
    }

    @Post(AccountCmsEndpointPaths.changeEmail)
    @AclNodes([AccountAcl.Accounts, AccountAcl.AccountsChangeEmail])
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'changeEmail',
        description: 'Change email of account. Target Account will not receive confirmation emails.',
    })
    @ApiOkResponse({
        type: AccountCmsDto,
    })
    async changeEmail(@Body() request: ChangeEmailAccountCmsRequest): Promise<ChangeEmailAccountCmsResponse> {
        const entity = await this.service.changeEmail(request);

        return this.mapper.entityToDTO(entity);
    }
}
