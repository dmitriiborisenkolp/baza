import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
    AccountAcl,
    AccountCmsResetPasswordEndpoint,
    AccountCmsResetPasswordEndpointPaths,
    RequestAllAccountsResetPasswordCmsAccountRequest,
    RequestResetPasswordCmsAccountRequest,
} from '@scaliolabs/baza-core-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';
import { AclNodes } from '../../../../baza-acl/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src';
import { BazaAccountCmsResetPasswordService } from '../../../../baza-account/src';

@Controller()
@ApiBearerAuth()
@ApiTags(CoreOpenApiTags.BazaAccountCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([AccountAcl.Accounts, AccountAcl.AccountsResetPassword])
export class AccountCmsResetPasswordController implements AccountCmsResetPasswordEndpoint {
    constructor(private readonly service: BazaAccountCmsResetPasswordService) {}

    @Post(AccountCmsResetPasswordEndpointPaths.requestResetPassword)
    @ApiOperation({
        summary: 'requestResetPassword',
        description: 'Reset password request for account',
    })
    @ApiOkResponse()
    async requestResetPassword(@Body() request: RequestResetPasswordCmsAccountRequest): Promise<void> {
        await this.service.requestResetPassword(request);
    }

    @Post(AccountCmsResetPasswordEndpointPaths.requestResetPasswordAllAccounts)
    @ApiOperation({
        summary: 'requestResetPasswordAllAccounts',
        description: 'Reset password request for all existing user accounts',
    })
    @ApiOkResponse()
    async requestResetPasswordAllAccounts(@Body() request: RequestAllAccountsResetPasswordCmsAccountRequest): Promise<void> {
        await this.service.requestResetPasswordAllAccounts(request);
    }
}
