import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    AccountEndpoint,
    AccountExistsRequest,
    AccountExistsResponse,
    BazaAccountRegisterOptions,
    ChangeEmailRequest,
    ChangeEmailResponse,
    ChangePasswordRequest,
    ChangePasswordResponse,
    ConfirmEmailRequest,
    ConfirmEmailResponse,
    CurrentAccountResponse,
    DeactivateAccountRequest,
    extractDeviceTokensFromRequest,
    RegisterAccountRequest,
    RegisterAccountResponse,
    ResetPasswordRequest,
    ResetPasswordResponse,
    SendChangeEmailLinkRequest,
    SendConfirmEmailLinkRequest,
    SendConfirmEmailLinkResponse,
    SendDeactivateAccountLinkRequest,
    SendResetPasswordLinkRequest,
    SendResetPasswordLinkResponse,
    SetAccountSettingsResponse,
    UpdateProfileRequest,
    UpdateProfileResponse,
} from '@scaliolabs/baza-core-shared';
import type { SetAccountSettingsRequest } from '@scaliolabs/baza-core-shared';
import { AccountEndpointPaths, AccountDto } from '@scaliolabs/baza-core-shared';
import { AuthResponse } from '@scaliolabs/baza-core-shared';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { IpAddress, RequestContextService } from '../../../../baza-common/src';
import { AuthGuard, AuthOptionalGuard, AuthService, ReqAccount } from '../../../../baza-auth/src';
import {
    AccountEntity,
    BazaAccountDeactivateService,
    BazaAccountFeatures,
    BazaAccountMapper,
    BazaAccountService,
} from '../../../../baza-account/src';
import { FeaturesRequired, FeaturesRequiredGuard } from '../../../../baza-feature/src';
import { Request } from 'express';

@ApiTags(CoreOpenApiTags.BazaAccount)
@Controller()
export class AccountController implements AccountEndpoint {
    constructor(
        private readonly service: BazaAccountService,
        private readonly deactivateService: BazaAccountDeactivateService,
        private readonly mapper: BazaAccountMapper,
        private readonly authService: AuthService,
        private readonly requestContext: RequestContextService,
    ) {}

    @Get(AccountEndpointPaths.currentAccount)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'currentAccount',
        description: 'Returns current account DTO',
    })
    @ApiOkResponse({
        type: AccountDto,
    })
    async currentAccount(@ReqAccount() account: AccountEntity): Promise<CurrentAccountResponse> {
        return this.mapper.entityToDto(account);
    }

    @Post(AccountEndpointPaths.registerAccount)
    @ApiOperation({
        summary: 'registerAccount',
        description: 'Register new account',
    })
    @ApiOkResponse()
    async registerAccount(@Body() request: RegisterAccountRequest): Promise<RegisterAccountResponse> {
        await this.service.registerAccount(request, await this.requestContext.current());
    }

    @Get(AccountEndpointPaths.registerAccountOptions)
    @UseGuards(AuthOptionalGuard)
    @ApiOperation({
        summary: 'registerAccountOptions',
        description: 'Returns register account options. If called from authenticated account, it could returns default referral code also',
    })
    @ApiOkResponse({
        type: BazaAccountRegisterOptions,
    })
    async registerAccountOptions(@ReqAccount() account: AccountEntity): Promise<BazaAccountRegisterOptions> {
        return this.service.registerAccountOptions(account);
    }

    @Post(AccountEndpointPaths.confirmEmail)
    @ApiOperation({
        summary: 'confirmEmail',
        description: "Confirm account's email address with token (see @sendConfirmEmailLink endpoint)",
    })
    @ApiOkResponse({
        type: AuthResponse,
    })
    async confirmEmail(@Body() request: ConfirmEmailRequest, @IpAddress() ip: string, @Req() req: Request): Promise<ConfirmEmailResponse> {
        const account = await this.service.confirmEmail(request, await this.requestContext.current());
        const context = await this.requestContext.current();

        return this.authService.authAccount(account, {
            withRefreshToken: true,
            application: context.application,
            applicationId: context.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }

    @Post(AccountEndpointPaths.sendConfirmEmailLink)
    @ApiOperation({
        summary: 'sendConfirmEmailLink',
        description: "Send confirm link to account's email",
    })
    @ApiOkResponse()
    async sendConfirmEmailLink(@Body() request: SendConfirmEmailLinkRequest): Promise<SendConfirmEmailLinkResponse> {
        await this.service.sendConfirmEmailLink(request, await this.requestContext.current());
    }

    @Post(AccountEndpointPaths.changePassword)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'changePassword',
        description: 'Change password with previous & new passwords pair',
    })
    @ApiOkResponse()
    async changePassword(@Body() request: ChangePasswordRequest, @ReqAccount() account: AccountEntity): Promise<ChangePasswordResponse> {
        await this.service.changePassword(account, request, await this.requestContext.current());
    }

    @Post(AccountEndpointPaths.sendResetPasswordLink)
    @ApiOperation({
        summary: 'sendResetPasswordLink',
        description: 'Reset password with token (@see sendResetPasswordLink endpoint)',
    })
    @ApiOkResponse()
    async sendResetPasswordLink(@Body() request: SendResetPasswordLinkRequest): Promise<SendResetPasswordLinkResponse> {
        await this.service.sendResetPasswordLinkRequest(request, await this.requestContext.current());
    }

    @Post(AccountEndpointPaths.resetPassword)
    @ApiOperation({
        summary: 'resetPassword',
        description: 'Reset password with token (@see sendResetPasswordLink endpoint)',
    })
    @ApiOkResponse({
        type: AuthResponse,
    })
    async resetPassword(
        @Body() request: ResetPasswordRequest,
        @IpAddress() ip: string,
        @Req() req: Request,
    ): Promise<ResetPasswordResponse> {
        const account = await this.service.resetPassword(request, await this.requestContext.current());
        const context = await this.requestContext.current();

        return this.authService.authAccount(account, {
            withRefreshToken: true,
            application: context.application,
            applicationId: context.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }

    @Post(AccountEndpointPaths.sendChangeEmailLink)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'sendChangeEmailLink',
        description: 'Send change email link for current account',
    })
    @ApiOkResponse()
    async sendChangeEmailLink(@Body() request: SendChangeEmailLinkRequest, @ReqAccount() account: AccountEntity): Promise<void> {
        const context = await this.requestContext.current();

        await this.service.sendChangeEmailLink(account, request, context);
    }

    @Post(AccountEndpointPaths.changeEmail)
    @ApiOperation({
        summary: 'changeEmail',
        description: 'Change email with token sent from sendChangeEmailLink method',
    })
    @ApiOkResponse({
        type: AuthResponse,
    })
    async changeEmail(@Body() request: ChangeEmailRequest, @IpAddress() ip: string, @Req() req: Request): Promise<ChangeEmailResponse> {
        const account = await this.service.changeEmail(request);
        const context = await this.requestContext.current();

        return this.authService.authAccount(account, {
            withRefreshToken: true,
            application: context.application,
            applicationId: context.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }

    @Post(AccountEndpointPaths.updateAccountSettings)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'updateSettings',
        description: 'Update account settings',
    })
    @ApiOkResponse()
    async updateSettings(
        @Body() request: SetAccountSettingsRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<SetAccountSettingsResponse> {
        const updatedAccount = await this.service.setAccountSettings(account, request);

        return updatedAccount.settings;
    }

    @Post(AccountEndpointPaths.updateProfile)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'updateProfile',
        description: 'Update account profile',
    })
    @ApiOkResponse({
        type: AccountDto,
    })
    async updateProfile(@Body() request: UpdateProfileRequest, @ReqAccount() account: AccountEntity): Promise<UpdateProfileResponse> {
        const updatedAccount = await this.service.updateProfile(account, request);

        return this.mapper.entityToDto(updatedAccount);
    }

    @Post(AccountEndpointPaths.sendDeactivateAccountLink)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'sendDeactivateAccountLink',
        description: 'Send link to deactivate (delete) account',
    })
    @ApiOkResponse()
    async sendDeactivateAccountLink(
        @Body() request: SendDeactivateAccountLinkRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<void> {
        const context = await this.requestContext.current();

        await this.deactivateService.sendDeactivateAccountLink(account, request, context);
    }

    @Post(AccountEndpointPaths.deactivateAccount)
    @ApiOperation({
        summary: 'deactivateAccount',
        description: 'Deactivate (delete) account with token received with sendDeactivateAccountLink method',
    })
    @ApiOkResponse()
    async deactivateAccount(@Body() request: DeactivateAccountRequest): Promise<void> {
        await this.deactivateService.deactivateAccountWithToken(request);
    }

    @Post(AccountEndpointPaths.accountExists)
    @FeaturesRequired([BazaAccountFeatures.Exists])
    @UseGuards(FeaturesRequiredGuard)
    @ApiOperation({
        summary: 'accountExists',
        description: 'Returns information about existing active account or deactivated accounts',
    })
    @ApiOkResponse({
        type: AccountExistsResponse,
    })
    async accountExists(@Body() request: AccountExistsRequest): Promise<AccountExistsResponse> {
        return this.service.accountExists(request);
    }
}
