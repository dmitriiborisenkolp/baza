import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BazaDbStressTestEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    foo: string;

    @Column()
    bar: string;
}
