import { Module } from '@nestjs/common';
import { BazaDbStressTestsController } from './controllers/baza-db-stress-tests.controller';
import { BazaDbStressTestRepository } from './repositories/baza-db-stress-test.repository';
import { BazaDbStressTestsService } from './services/baza-db-stress-tests.service';
import { BazaEnvModule } from '../../../baza-env/src';
import { BazaLoggerModule } from '../../../baza-logger/src';

@Module({
    imports: [BazaEnvModule, BazaLoggerModule],
    controllers: [BazaDbStressTestsController],
    providers: [BazaDbStressTestRepository, BazaDbStressTestsService],
})
export class BazaDbStressTestsApiModule {}
