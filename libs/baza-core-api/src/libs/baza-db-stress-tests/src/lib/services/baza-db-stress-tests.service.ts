/* eslint-disable no-console */
import { Injectable } from '@nestjs/common';
import { EnvService } from '../../../../baza-env/src';
import { BazaDbStressTestRepository } from '../repositories/baza-db-stress-test.repository';

/**
 * Maximum amount of records which could be requested to create at once
 */
const MAX_DB_RECORDS = 10000;

/**
 * DB Stress Tests
 * These tests simulates have load to DB. You can enable it for non-Production
 * environments (could be enabled for Preprod/UAT) with `BAZA_DB_STRESS_TESTS`
 * environment variable
 */
@Injectable()
export class BazaDbStressTestsService {
    constructor(private readonly env: EnvService, private readonly repository: BazaDbStressTestRepository) {}

    /**
     * Validates that it's not Live (Production) environment and BAZA_DB_STRESS_TESTS
     * is enabled
     * @private
     */
    private validateEnvironment(): void {
        const isNotProdEnvironment = !this.env.isPublicProductionEnvironment;
        const isDbTestsEnabled = this.env.getAsBoolean('BAZA_DB_STRESS_TESTS');

        if (!(isNotProdEnvironment && isDbTestsEnabled)) {
            throw new Error('DB Stress Tests are not enabled for this environment or this is a production environment');
        }
    }

    /**
     * Creates N example records
     * @param count
     */
    async createNRecords(count: number): Promise<void> {
        this.validateEnvironment();

        if (count < 0 || count > MAX_DB_RECORDS) {
            throw new Error(`Invalid number of records requested. You can create up to ${MAX_DB_RECORDS} at once`);
        }

        console.debug(`DB Stress Test: Attempt to create [${count}] records`);

        for (let i = 1; i <= count; i++) {
            console.debug(`DB Stress Test [${i}/${count}]: creating record...`);

            const entity = await this.repository.createEntity();

            console.debug(`DB Stress Test [${i}/${count}]: record ${entity.id} created`);
        }

        console.debug(`DB Stress Test: Attempt to create [${count}] records finished`);
    }

    /**
     * Reads N example records
     * @param requestedCount
     */
    async readNRecords(requestedCount: number): Promise<void> {
        this.validateEnvironment();

        const count = Math.min(requestedCount, await this.count());

        if (count < 0 || count > MAX_DB_RECORDS) {
            throw new Error(`Invalid number of records requested. You can create up to ${MAX_DB_RECORDS} at once`);
        }

        console.debug('DB Stress Test: Fetching all Ids');

        const allIds = await this.repository.findAllIds();

        console.debug(`DB Stress Test: all Ids fetched`);
        console.debug(`DB Stress Test: running reading one-by-one ${count} records from DB`);

        for (let i = 1; i <= count; i++) {
            await this.repository.findEntityById(allIds[i]);

            console.debug(`DB Stress Test: fetched ${i} of ${count}`);
        }

        console.debug(`DB Stress Test: reading one-by-one ${count} records from DB finished`);
    }

    /**
     * Clean up all DB Stress Test entities
     */
    async flush(): Promise<void> {
        this.validateEnvironment();

        await this.repository.flush();
    }

    /**
     * Returns total count of DB Stress Test entities
     */
    async count(): Promise<number> {
        this.validateEnvironment();

        return this.repository.count();
    }
}
