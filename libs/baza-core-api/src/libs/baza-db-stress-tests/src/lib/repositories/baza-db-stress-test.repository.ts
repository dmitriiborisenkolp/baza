import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaDbStressTestEntity } from '../entities/baza-db-stress-test.entity';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';

/**
 * TypeORM Repository Service for BazaDbStressTestEntity
 */
@Injectable()
export class BazaDbStressTestRepository {
    constructor(private readonly connection: Connection) {}

    /**
     * TypeORM repository for BazaDbStressTestEntity
     */
    get repository(): Repository<BazaDbStressTestEntity> {
        return this.connection.getRepository(BazaDbStressTestEntity);
    }

    /**
     * Creates a random BazaDbStressTestEntity entity
     */
    async createEntity(): Promise<BazaDbStressTestEntity> {
        const entity = new BazaDbStressTestEntity();

        entity.foo = generateRandomHexString(256);
        entity.bar = generateRandomHexString(65535);

        await this.repository.save(entity);

        return entity;
    }

    /**
     * Returns BazaDbStressTestEntity by ID
     * @param id
     */
    async findEntityById(id: number): Promise<BazaDbStressTestEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    id,
                },
            ],
        });
    }

    /**
     * Returns all IDs of BazaDbStressTestEntity
     */
    async findAllIds(): Promise<Array<number>> {
        const qb = await this.repository.createQueryBuilder('entity').select(['id']).getRawMany();

        return (qb || []).map((next) => next.id);
    }

    /**
     * Clean up BazaDbStressTestEntity DB table
     */
    async flush(): Promise<void> {
        await this.repository.delete({});
    }

    /**
     * Returns total count of BazaDbStressTestEntity entities in DB
     */
    async count(): Promise<number> {
        return this.repository.count();
    }
}
