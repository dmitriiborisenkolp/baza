import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { BazaDbStressTestsService } from '../services/baza-db-stress-tests.service';

/**
 * We return simple JSON just to let know that request was actually processed
 * NGINX which is used at front of api applications may return 504 request timeout error still
 */
class SuccessResponse {
    success: true;
}

/**
 * DB Stress Tests
 * These tests simulates have load to DB. You can enable it for non-Production
 * environments (could be enabled for Preprod/UAT) with `BAZA_DB_STRESS_TESTS`
 * environment variable
 *
 * This is a TypeORM set of tests; there is also a set of tests which works directly
 * with postgres NPM library
 *
 * All methods are available by GET method to make it simply to debug by devops
 */
@Controller('/baza-db-stress-tests/typeorm')
export class BazaDbStressTestsController {
    constructor(private readonly service: BazaDbStressTestsService) {}

    @Get('createNRecords/:count')
    async createNRecords(@Param('count', ParseIntPipe) count: number): Promise<SuccessResponse> {
        await this.service.createNRecords(count);

        return {
            success: true,
        };
    }

    @Get('readNRecords/:count')
    async readNRecords(@Param('count', ParseIntPipe) count: number): Promise<SuccessResponse> {
        await this.service.readNRecords(count);

        return {
            success: true,
        };
    }

    @Get('flush')
    async flush(): Promise<SuccessResponse> {
        await this.service.flush();

        return {
            success: true,
        };
    }

    @Get('count')
    async count(): Promise<number> {
        return this.service.count();
    }
}
