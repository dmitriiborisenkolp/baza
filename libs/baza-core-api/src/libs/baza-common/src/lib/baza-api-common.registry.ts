import {
    BazaEnvironments,
    defaultBazaEnvironmentsConfig,
    getBazaClientName,
    RegistrySchema,
    RegistryType,
    withoutEndingSlash,
} from '@scaliolabs/baza-core-shared';

export const bazaApiCommonRegistry: RegistrySchema = {
    bazaCommon: {
        clientName: {
            name: 'Client Name',
            type: RegistryType.String,
            defaults: (env: BazaEnvironments) => {
                return defaultBazaEnvironmentsConfig().productionEnvironments.includes(env)
                    ? getBazaClientName()
                    : `${getBazaClientName()} ${env}`;
            },
            hiddenFromList: false,
            public: true,
        },
        links: {
            termsOfService: {
                name: 'Link to Terms of Service',
                type: RegistryType.HttpsLink,
                public: true,
                hiddenFromList: false,
                defaults: (_, env) => `${withoutEndingSlash(env['BAZA_APP_WEB_URL'])}/terms-of-service`,
            },
            privacyPolicy: {
                name: 'Link to Privacy Policy',
                type: RegistryType.HttpsLink,
                public: true,
                hiddenFromList: false,
                defaults: (_, env) => `${withoutEndingSlash(env['BAZA_APP_WEB_URL'])}/privacy-policy`,
            },
        },
    },
};
