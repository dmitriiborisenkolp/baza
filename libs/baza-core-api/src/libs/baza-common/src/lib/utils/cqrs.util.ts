import { BazaLogger } from '../../../../baza-logger/src';

/**
 * Native `console` logger for CQRS util
 */
const CONSOLE_LOGGER: Partial<BazaLogger> = {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setContext: () => {},
    // eslint-disable-next-line no-console
    debug: (message) => console.debug(message),
    error: (message) => console.error(message),
};

/**
 * Disabled logger for CQRS util
 */
const MOCK_LOGGER: Partial<BazaLogger> = {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setContext: () => {},
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    debug: () => {},
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    error: () => {},
};

/**
 * Uses native `console` for CQRS util logger
 */
export function bazaCqrsUseConsoleLogger(): void {
    BAZA_CQRS_UTILS_INJECTIONS.logger = CONSOLE_LOGGER as BazaLogger;
}

/**
 * Disables logger for CQRS util
 */
export function bazaCqrsUseMockLogger(): void {
    BAZA_CQRS_UTILS_INJECTIONS.logger = MOCK_LOGGER as BazaLogger;
}

/**
 * Use BazaLogger for CQRS util logger
 * @param bazaLogger
 */
export function bazaCqrsUseBazaLogger(bazaLogger: BazaLogger): void {
    BAZA_CQRS_UTILS_INJECTIONS.logger = bazaLogger;
}

/**
 * Injections for `cqrs` helper
 */
const BAZA_CQRS_UTILS_INJECTIONS: {
    logger: BazaLogger;
} = {
    logger: CONSOLE_LOGGER as BazaLogger,
};

/**
 * CQRS EventHandler Helper
 * All Nest.JS CQRS Event Handlers should be wrapped with this helper
 * This helper prevents Nest.JS CQRS Event Handler from crashes, logs useful information (if BAZA_EVENT_BUS_LOG_EVENTS is
 * enabled) and allows execute Promises within EventHandlers.
 * @param context
 * @param eventHandlerImplementation
 */
export function cqrs<T = unknown>(context: string, eventHandlerImplementation: () => Promise<T>): void {
    BAZA_CQRS_UTILS_INJECTIONS.logger.setContext('cqrs');

    eventHandlerImplementation()
        .then((result) => {
            BAZA_CQRS_UTILS_INJECTIONS.logger.debug({
                type: 'cqrs-event',
                success: true,
                eventHandler: context,
                result,
            });
        })
        .catch((error) => {
            BAZA_CQRS_UTILS_INJECTIONS.logger.error({
                type: 'cqrs-event',
                success: true,
                eventHandler: context,
                error,
            });
        });
}

/**
 * CQRS Command Helper
 * All Nest.JS CQRS Command Handlers should be wrapped with this helper
 * This helper logs usefull information if BAZA_EVENT_BUS_LOG_EVENTS is enabled
 * @param context
 * @param commandHandlerImplementation
 */
export function cqrsCommand<T = unknown>(context: string, commandHandlerImplementation: () => Promise<T>): Promise<T> {
    BAZA_CQRS_UTILS_INJECTIONS.logger.setContext('cqrs');

    return commandHandlerImplementation()
        .then((result) => {
            BAZA_CQRS_UTILS_INJECTIONS.logger.debug({
                type: 'cqrs-command',
                success: true,
                eventHandler: context,
                result,
            });

            return result;
        })
        .catch((error) => {
            BAZA_CQRS_UTILS_INJECTIONS.logger.error({
                type: 'cqrs-command',
                success: true,
                eventHandler: context,
                error,
            });

            throw error;
        });
}
