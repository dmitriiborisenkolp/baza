// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace BazaCommonEnvironments {
    export interface CombinedBazaEnvironments
        extends NodeEnvironment,
            CommonEnvironment,
            ExceptionHandlerEnvironment,
            AppEndpointsEnvironment,
            ExpressEnvironment,
            EventBusEnvironment,
            TypeOrmEnvironment,
            RedisEnvironment,
            KafkaEnvironment,
            MailEnvironment,
            AwsEnvironment,
            AwsProxyEnvironment,
            AuthEnvironment,
            SentryEnvironment,
            SwaggerEnvironment,
            RedocEnvironment,
            AuthManagedUserEnvironment,
            GitEnvironment,
            ThrottlerEnvironment,
            HealthCheckEnvironment,
            DeviceTokenEnvironment {}

    export interface NodeEnvironment {
        /**
         * Node.JS Environment
         * The value is used to bootstrap specific `${NODE_ENV}.env` file
         * @see BazaEnvironments
         * @type string
         */
        NODE_ENV: string;

        /**
         * Baza Environment
         * Defines Deployment Target (Local/Test/Stage/Production/E2e)
         * @see BazaEnvironments
         * @type string
         */
        BAZA_ENV: string;

        /**
         * Base URL for API endpoints
         * Base URL will be prepended for all existing endpoints
         * @default /
         * @type string
         */
        BAZA_API_PREFIX?: string;

        /**
         * Flag to activate single intance node or cluster environment
         * @default /
         * @type boolean
         */
        BAZA_MASTER: boolean;

        /**
         * Enables DB stress tests for environment
         * DB Stress Tests could be run with /baza-db-stress-tests endpoints
         * DB Stress Tests could be enabled for Preprod environment, but cannot
         * be run for Prod environment
         */
        BAZA_DB_STRESS_TESTS: boolean;
    }

    export interface CommonEnvironment {
        /**
         * BCrypt Salt Rounds
         * @see BAZA_API_CORE_BCRYPT_DEFAULT_SALT_ROUNDS
         * @default 10
         * @see https://stackoverflow.com/questions/46693430/what-are-salt-rounds-and-how-are-salts-stored-in-bcryp
         * @type number
         */
        BCRYPT_SALT_ROUNDS?: string;
    }

    export interface ExceptionHandlerEnvironment {
        /**
         * Enables or Disabled Error Log
         * Error Log can create some pressure to your logs
         * @type boolean
         */
        BAZA_DEBUG_ERROR_LOG?: string;

        /**
         * Displays JWT in Error Logs
         * Consider to disable this option for Production environment in order to increase security
         * @type boolean
         */
        BAZA_DEBUG_ERROR_LOG_JWT?: string;
    }

    export interface AppEndpointsEnvironment {
        /**
         * API URL (with base URL)
         * API URL could be used to generate links for some specific features
         * @type string
         */
        BAZA_APP_API_URL: string;

        /**
         * CMS URL
         * CMS URL are used to generate links in email templates
         * @type string
         */
        BAZA_APP_CMS_URL?: string;

        /**
         * WEB URL
         * WEB URL is used to generate links in email templates
         * @type string
         */
        BAZA_APP_WEB_URL?: string;
    }

    export interface ExpressEnvironment {
        /**
         * HTTP Port to bind (listen)
         * @type number
         */
        BAZA_API_HTTP_PORT: string;
    }

    export interface EventBusEnvironment {
        /**
         * Enables of Disabled Kafka integration for Baza
         * @type boolean
         */
        BAZA_EVENT_BUS_ENABLE_KAFKA?: string;

        /**
         * Enables of Disabled additional log of events (CQRS or Kafka)
         * The option creates huge pressure to logs
         * @type boolean
         */
        BAZA_EVENT_BUS_LOG_EVENTS?: string;
    }

    export interface TypeOrmEnvironment {
        /**
         * Database Host
         * @see https://typeorm.io/data-source-options#postgres--cockroachdb-data-source-options
         * @type string
         */
        DB_HOST: string;

        /**
         * Database Username
         * @see https://typeorm.io/data-source-options#postgres--cockroachdb-data-source-options
         * @type string
         */
        DB_USERNAME: string;

        /**
         * Database Password
         * @see https://typeorm.io/data-source-options#postgres--cockroachdb-data-source-options
         * @type string
         */
        DB_PASSWORD: string;

        /**
         * Database Name
         * @see https://typeorm.io/data-source-options#postgres--cockroachdb-data-source-options
         * @type string
         */
        DB_NAME: string;

        /**
         * Database Type
         * Baza is designed to work with PostgreSQL. It may be possible to work with different other databases, but it's
         * not recommended and it should be properly tested
         * @see https://typeorm.io/data-source-options#postgres--cockroachdb-data-source-options
         * @example postgres
         * @type string
         */
        DB_TYPE: string;

        /**
         * Database Port
         * @see https://typeorm.io/data-source-options#postgres--cockroachdb-data-source-options
         * @example 5411
         * @type number
         */
        DB_PORT: string;

        /**
         * Automatically drop Database Schema when application is starting
         * Usually it's used for e2e environments. DO NOT enable it for Test/Stage/Production environments
         * @see https://typeorm.io/data-source-options#postgres--cockroachdb-data-source-options
         * @example 0
         * @type boolean
         */
        DB_AUTO_DROP_SCHEMA: string;

        /**
         * Automatically updates Database Schema when application is starting
         * By default it should enabled for all environments
         * @type boolean
         * @example 1
         */
        DB_SYNCHRONIZE: string;

        /**
         * Enables or disables SQL logs
         * The option creates HUGE pressure to logs and contains a lot of sensentive data. Consider to not enable it for
         * Stage and Production environments. For other environments the option should be enabled only if it was requested
         * by development team.
         * @example 0
         * @type boolean
         */
        DB_LOGGING: string;
    }

    export interface RedisEnvironment {
        /**
         * Redis Host
         * @see https://ioredis.readthedocs.io/en/latest/API/#Redis+connect
         * @type string
         */
        REDIS_DB_HOST: string;

        /**
         * Redis Port
         * @see https://ioredis.readthedocs.io/en/latest/API/#Redis+connect
         * @example 6379
         * @type number
         */
        REDIS_DB_PORT: string;

        /**
         * Redis Password
         * @see https://ioredis.readthedocs.io/en/latest/API/#Redis+connect
         * @type string
         */
        REDIS_DB_PASSWORD: string;

        /**
         * Redis Database Index
         * @see https://ioredis.readthedocs.io/en/latest/API/#Redis+connect
         * @example 1
         * @type number
         */
        REDIS_DB_NUM: string;

        /**
         * Optional prefix for all Redis keys
         * @see https://ioredis.readthedocs.io/en/latest/API/#Redis+connect
         * @type string
         */
        REDIS_DB_KEY_PREFIX?: string;
    }

    export interface KafkaEnvironment {
        /**
         * Enables of Disable Kafka Integration
         * @example 1
         * @type boolean
         */
        KAFKA_ENABLED: string;

        /**
         * Enables or disables additional logs from Kafka
         * The option create some pressure to logs
         * @example 0
         * @type boolean
         */
        KAFKA_LOG_EVENTS: string;

        /**
         * Kafka Client ID
         * @see https://docs.confluent.io/platform/current/clients/consumer.html
         * @example baza-dev-consumer
         * @type string
         */
        KAFKA_CLIENT_ID: string;

        /**
         * Kafka Group ID
         * @see https://docs.confluent.io/platform/current/clients/consumer.html
         * @example baza-dev-group
         * @type string
         */
        KAFKA_CLIENT_GROUP_ID: string;

        /**
         * Kafka Brokers (List of URLs where Kafka is available)
         * Keep Kafka instances under private networks. Kafka does not have authorization at all and Kafka instance
         * should be available from API instances.
         * For Test environment you should make Kafka instance available outside private network for development reasons
         * @example 11.22.33.44:9094
         * @type string
         */
        KAFKA_CLIENT_BROKERS: string;

        /**
         * Kafka Namespace
         * Optional feature which allows Baza to use same Kafka instance for multiple projects and multiple environments
         * Consider to use same value as for BAZA_ENV
         * @see BazaEnvironments
         * @example test
         */
        KAFKA_NAMESPACE?: string;
    }

    export interface MailEnvironment
        extends MailSmtpEnvironment,
            MailMailgunEnvironment,
            MailSendPulseEnvironment,
            MailCustomerioEnvironment {
        /**
         * Debug Mode
         * Enables Debug Log for each Send Mail Request
         * Disabled by  default
         */
        MAIL_DEBUG?: string;

        /**
         * Mail Transport
         * Baza can use different services to send emails. The environment variables defines which transport (service)
         * will be used. You should set additional environment variables depends on MAIL_TRANSPORT value
         * Possible values are `smtp`, `mailgun`, `memory` (for e2e only), `sendpulse` and `customerio`
         * @see MailTransportType
         * @example smtp
         * @type string
         */
        MAIL_TRANSPORT: string;

        /**
         * FROM (Name) for emails sent to users. It should be no-reply email address, because it's mostly used for
         * Account Email Confirmation, Reset Password and any other service emails
         * @type string
         */
        MAIL_FROM_NAME: string;

        /**
         * FROM (Email) for emails sent to users. It should be no-reply email address, because it's mostly used for
         * Account Email Confirmation, Reset Password and any other service emails
         * @type string
         */
        MAIL_FROM_ADDRESS: string;

        /**
         * Allowed Mail Domains for sending emails
         * Contains list of domains separated by comma or '*' value
         * @example *
         * @example example.com
         * @example example.com,foo.bar
         */
        MAIL_ALLOWED_DOMAINS: string;

        /**
         * (for `smtp` MAIL_TRANSPORT only)
         * Enables or Disables TLS
         * @example 1
         * @type boolean
         */
        MAIL_SMTP_SECURE?: string;
    }

    export interface MailSmtpEnvironment {
        /**
         * (for `smtp` MAIL_TRANSPORT only)
         * SMTP Host
         * @example 127.0.0.1
         * @type string
         */
        MAIL_SMTP_HOST?: string;

        /**
         * (for `smtp` MAIL_TRANSPORT only)
         * SMTP Port
         * @example 1025
         * @type number
         */
        MAIL_SMTP_PORT?: string;

        /**
         * (for `smtp` MAIL_TRANSPORT only)
         * SMTP User
         * @example smtp-user
         * @type string
         */
        MAIL_SMTP_USER?: string;

        /**
         * (for `smtp` MAIL_TRANSPORT only)
         * SMTP Password
         * @example smtp-user-password
         * @type string
         */
        MAIL_SMTP_PASS?: string;
    }

    export interface MailMailgunEnvironment {
        /**
         * (for `mailgun` MAIL_TRANSPORT only)
         * Mailgun Domain
         * @see https://documentation.mailgun.com/en/latest/quickstart-sending.html
         * @type string
         */
        MAIL_MAILGUN_DOMAIN?: string;

        /**
         * (for `mailgun` MAIL_TRANSPORT only)
         * Mailgun API Key
         * Use Domain Sending Keys and do not provide Primary Account key for security reasons
         * @see https://help.mailgun.com/hc/en-us/articles/203380100-Where-Can-I-Find-My-API-Key-and-SMTP-Credentials-
         * @see https://documentation.mailgun.com/en/latest/quickstart-sending.html
         * type string
         */
        MAIL_MAILGUN_API_KEY?: string;
    }

    export interface MailSendPulseEnvironment {
        /**
         * (for `sendpulse` MAIL_TRANSPORT only)
         * SendPulse Client ID (OAuth)
         * @see https://sendpulse.com/knowledge-base/smtp/setup-smtp-server
         * @type string
         */
        MAIL_SEND_PULSE_CLIENT_ID?: string;

        /**
         * (for `sendpulse` MAIL_TRANSPORT only)
         * SendPulse Client Secret (OAuth)
         * @see https://sendpulse.com/knowledge-base/smtp/setup-smtp-server
         * @type string
         */
        MAIL_SEND_PULSE_CLIENT_SECRET?: string;
    }

    export interface MailCustomerioEnvironment {
        /**
         * (for `customerio` MAIL_TRANSPORT only)
         * CustomerIO APP Key
         * @see https://www.customer.io/docs/api/#section/Authentication
         * @type string
         */
        MAIL_CUSTOMER_IO_APP_KEY?: string;

        /**
         * (for `customerio` MAIL_TRANSPORT only)
         * CustomerIO Region
         * @see https://www.customer.io/docs/api/#section/Authentication
         * @type string
         */
        MAIL_CUSTOMER_IO_REGION?: string;
    }

    export interface AwsEnvironment {
        /**
         * AWS S3 Bucket
         * @example scalio-non-prod
         * @type string
         */
        AWS_S3_BUCKET: string;

        /**
         * AWS S3 URL
         * @example https://scalio-non-prod.s3.amazonaws.com
         * @type string
         */
        AWS_S3_URL: string;

        /**
         * Additional path (directory) for S3 Bucket
         * @example baza-dev/cms
         * @type string
         */
        AWS_S3_PATH?: string;

        /**
         * AWS Access Key
         * Do not store AWS Access Key in repository and keep it in secure storage
         * @type string
         */
        AWS_ACCESS_KEY_ID: string;

        /**
         * AWS Secret Key
         * Do not store AWS Secret Key in repository and keep it in secure storage
         * @type string
         */
        AWS_SECRET_KEY: string;

        /**
         * Enables or Disables strict check for AWS resources
         * Baza will throw an exception for all AWS resources which are not actully placed in specific S3 Bucket + additional
         * AWS_S3_PATH.
         * Consider to enable it by default for all environments
         * @see AWS_S3_PATH
         * @example 1
         * @type boolean
         */
        AWS_STRICT: string;
    }

    export interface AwsProxyEnvironment {
        /**
         * URL where Baza API instance is deployed and which should be used to proxy AWS uploads
         * @deprecated
         * @type string
         */
        AWS_PROXY_BASE_URL: string;

        /**
         * Admin Account Email which should be used to proxy AWS uploads
         * @deprecated
         * @type string
         */
        AWS_PROXY_AUTH_EMAIL: string;

        /**
         * Admin Account Password which should be used to proxy AWS uploads
         * @deprecated
         * @type string
         */
        AWS_PROXY_AUTH_PASSWORD: string;
    }

    export interface AuthEnvironment {
        /**
         * Disables 2FA for CMS
         * Consider to not use it for non-Development (Local) environments. If you would like to disable 2FA for
         * Test/Stage/Production environments, you should use `bazaApiBundleConfigBuilder` helper
         * @see bazaApiBundleConfigBuilder
         * @type boolean
         */
        AUTH_2FA_DISABLED: string;

        /**
         * JWT Secret which is used to sign JWT tokens
         * Use unique values for each deployment target and keep it in secret place
         * @type string
         */
        AUTH_JWT_SECRET: string;
    }

    export interface SentryEnvironment {
        /**
         * Sentry DSN
         * @see https://docs.sentry.io/
         * @see https://docs.sentry.io/product/sentry-basics/dsn-explainer/
         * @type string
         */
        SENTRY_DSN?: string;
    }

    export interface SwaggerEnvironment {
        /**
         * Enable of Disabled Swagger API Documentation
         * Swagger Documentation will be available at GET /api-docs
         * @type boolean
         */
        BAZA_SWAGGER: string;
    }

    export interface RedocEnvironment {
        /**
         * Enable or Disable ReDoc API Documentation
         * ReDoc Documentation will be available at GET /redoc
         * @type boolean
         */
        BAZA_REDOC: string;
    }

    export interface AuthManagedUserEnvironment {
        /**
         * Baza Managed Users
         * BAZA_MANAGED_USERS should have following format: `[user-config-1],[user-config-2],...`, where `[user-config-N]` should
         * follows `${email} ${role} ${password}` format
         * If BAZA_MANAGED_USERS is not set, Baza will created scalio-admin@scal.io and scalio-user@scal.io accounts with `Scalio#1337!` passwords
         * You should not store BAZA_MANAGED_USERS in repository for Test/Stage/Production environments; keep it in secure place
         * @example BAZA_MANAGED_USERS=admin scalio-admin@scal.io Scalio#1337!, user scalio-user@scal.io Scalio#1337!
         * @type string
         */
        BAZA_MANAGED_USERS?: string;

        /**
         * Lock Baza Managed Users
         * When it's set, Baza Managed Users will not be updated with BAZA_MANAGED_USERS changes
         * Use this as additional security point & Disable /baza-account/cms/bootstrap endpoint
         * @type boolean
         */
        BAZA_MANAGED_USERS_LOCK?: string;
    }

    export interface GitEnvironment {
        /**
         * Deployment version
         * The value will be displayed for GET / endpoint
         * It could be any string, but don't make it too long - 3~7 symbols is more than enough
         */
        GIT_VERSION: string;
    }

    export interface ThrottlerEnvironment {
        /**
         * Nest.JS Throttler TTL
         * A client could take up to THROTTLER_LIMIT requests within THROTTLER_TTL window
         * @see https://docs.nestjs.com/security/rate-limiting
         * @type number
         */
        THROTTLER_TTL: string;

        /**
         * Nest.JS Throttler Limit
         * A client could take up to THROTTLER_LIMIT requests within THROTTLER_TTL window
         * @see https://docs.nestjs.com/security/rate-limiting
         * @type number
         */
        THROTTLER_LIMIT: string;
    }

    export interface DeviceTokenEnvironment {
        /**
         * Specify time to live in minutes of stored FCM device tokens.
         * It's used to clean up outdated FCM device tokens on a time basis.
         *
         * @type number
         */
        BAZA_DEVICE_TOKEN_FCM_TTL_MINUTES: string;

        /**
         * Specify time to live in minutes of stored Apple device tokens.
         * It's used to clean up outdated Apple device tokens on a time basis.
         *
         * @type number
         */
        BAZA_DEVICE_TOKEN_APPLE_TTL_MINUTES: string;

        /**
         * Enables or disables logs related to device tokens.
         *
         * @type boolean
         */
        BAZA_DEVICE_TOKEN_LOGS: boolean;
    }

    export interface HealthCheckEnvironment {
        /**
         * Specify the maximum HEAP memory usage in Mega-Bytes.
         * Should use for health-check module.
         * It's used to clean up outdated Apple device tokens on a time basis.
         *
         * @type number
         */
        BAZA_HEALTHCHECK_MEMORY_HEAP_MAX_MBYTES: number;

        /**
         * Specify the maximum memory RSS usage in Mega-Bytes.
         * Should use for health-check module.
         * It's used to clean up outdated Apple device tokens on a time basis.
         *
         * @type number
         */
        BAZA_HEALTHCHECK_MEMORY_RSS_MAX_MBYTES: number;
    }
}
