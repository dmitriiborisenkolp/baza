import { Observable, throwError, timer } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

export const apiGenericRetryStrategy = ({
    maxAttempts = -1,
    retryDuration = 1000,
    excludedStatusCodes = [],
}: {
    maxAttempts?: number;
    retryDuration?: number;
    excludedStatusCodes?: Array<number>;
} = {}) => (attempts: Observable<any>) => {
    let numAttempts = 0;

    return attempts.pipe(
        mergeMap((error) => {
            numAttempts++;

            if (maxAttempts && numAttempts >= maxAttempts) {
                return throwError(error);
            }

            if (error.status) {
                if (! excludedStatusCodes.find(e => e === error.status)) {
                    return throwError(error);
                }
            }

            return timer(retryDuration);
        }),
    );
};
