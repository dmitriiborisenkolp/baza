import { DynamicModule, Global, Module } from '@nestjs/common';
import { BcryptService } from './services/bcrypt.service';
import { BAZA_API_CORE_COMMON_CONFIG, BazaApiCommonConfig } from './baza-api-common.config';
import { ProjectService } from './services/project.service';
import { RequestContextService } from './services/request-context.service';
import { CsvService } from './services/csv.service';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaApiCommonConfig>;
}

export { AsyncOptions as BazaApiCommonModuleAsyncOptions };

const services = [BcryptService, ProjectService, RequestContextService, CsvService];

@Module({})
export class BazaApiCommonModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaApiCoreGlobalModule,
            providers: [
                {
                    provide: BAZA_API_CORE_COMMON_CONFIG,
                    inject: options.injects,
                    useFactory: async (...injects) => options.useFactory(...injects),
                },
            ],
            exports: [BAZA_API_CORE_COMMON_CONFIG],
        };
    }
}

@Global()
@Module({
    providers: [...services],
    exports: [...services],
})
class BazaApiCoreGlobalModule {}
