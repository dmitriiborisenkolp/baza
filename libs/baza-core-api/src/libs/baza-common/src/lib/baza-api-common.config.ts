import { Application } from '@scaliolabs/baza-core-shared';

export const BAZA_API_CORE_COMMON_CONFIG = Symbol();

export interface BazaApiCommonConfig {
    bcryptSaltRounds: number;
    endUserAppUrls: Array<{
        app: Application;
        url: string;
    }>;
}

export const BAZA_API_CORE_BCRYPT_DEFAULT_SALT_ROUNDS = 10;
