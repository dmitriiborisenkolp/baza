import { PrimaryColumn, PrimaryColumnOptions } from 'typeorm';
import { ULID_LENGTH } from '@scaliolabs/baza-core-shared';

export type PrimaryUlidColumnOptions = Omit<PrimaryColumnOptions, 'type' | 'primary' | 'unique' | 'nullable' | 'length'>;

/**
 * PrimaryColumn decorator is a custom decorator , used to create primary columns that are of type <ulid>.
 * This decorator is a wrapper over TypeOrm original <PrimaryColumn>.
 * Since this decorator is only used to create the primary column, the <type>, <primary>, <unique>, <nullable> options were omitted from PrimaryColumnOptions type,
 * - And they are set with default values.
 * <length> is omitted from PrimaryColumnOptions type, because we do not want to pass String Literal to this decorator, and we are creating primary column with type <char>.
 * - So we do not need to pass <length> option to this decorator.
 * @param options
 * @returns {PropertyDecorator}
 * @constructor
 */
export function PrimaryUlidColumn(options: PrimaryUlidColumnOptions = {}): PropertyDecorator {
    const typeOrmOptions: PrimaryColumnOptions = options;

    typeOrmOptions.type = 'char';
    typeOrmOptions.length = ULID_LENGTH;
    typeOrmOptions.primary = true;
    typeOrmOptions.unique = true;
    typeOrmOptions.nullable = false;

    return PrimaryColumn(typeOrmOptions);
}
