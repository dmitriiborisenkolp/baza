import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import * as requestIp from 'request-ip';

const DEFAULT_IP_ADDRESS = '0.0.0.0';

export const IpAddress = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
    const req = ctx.switchToHttp().getRequest();

    return requestIp.getClientIp(req) || DEFAULT_IP_ADDRESS;
});
