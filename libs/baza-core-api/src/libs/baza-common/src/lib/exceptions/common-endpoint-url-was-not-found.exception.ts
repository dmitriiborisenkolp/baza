import { HttpStatus } from '@nestjs/common';
import { Application, BazaCommonErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Endpoint URL for application "%s" is not defined'

export class CommonEndpointUrlWasNotFoundException extends BazaAppException {
    constructor(application: Application) {
        super(
            BazaCommonErrorCodes.BazaCoreEndpointUrlWasNotFound,
            ERR_MESSAGE.replace('%s', application),
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
