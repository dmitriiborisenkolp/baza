import { HttpStatus } from '@nestjs/common';
import { BazaCommonErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class CommonFeatureDisabledException extends BazaAppException {
    constructor(feature: string) {
        super(
            BazaCommonErrorCodes.BazaFeatureDisabled,
            `Requested feature "${feature}" is disabled`,
            HttpStatus.BAD_REQUEST,
        );
    }
}
