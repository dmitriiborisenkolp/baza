import 'reflect-metadata';
import { BazaDataAccessNode, BazaRegistryNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@baza-core-api/baza-common/integration-tests/tests/001-baza-core-common-terms-and-policy-links.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessRegistry = new BazaRegistryNodeAccess(http);

    it('will return terms of service link', async () => {
        const response = await dataAccessRegistry.publicSchema();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const link = response.find((next) => next.id === 'bazaCommon.links.termsOfService');

        expect(link).toBeDefined();
        expect(link.node?.value).toContain('/terms-of-service');
    });

    it('will return privacy policy link', async () => {
        const response = await dataAccessRegistry.publicSchema();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const link = response.find((next) => next.id === 'bazaCommon.links.privacyPolicy');

        expect(link).toBeDefined();
        expect(link.node?.value).toContain('/privacy-policy');
    });
});
