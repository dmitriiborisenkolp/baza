import { Inject, Injectable, Scope } from '@nestjs/common';
import { BehaviorSubject } from 'rxjs';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { Application, BAZA_COMMON_I18N_CONFIG, DEFAULT_APPLICATION, ProjectLanguage } from '@scaliolabs/baza-core-shared';

interface RequestContext {
    version: string;
    application: Application;
    applicationId: string;
    language: ProjectLanguage;
}

export { RequestContext };

@Injectable({
    scope: Scope.REQUEST,
})
export class RequestContextService {
    constructor(
        @Inject(REQUEST) private request: Request,
    ) {}

    private _current$: BehaviorSubject<RequestContext> = new BehaviorSubject<RequestContext>({
        version: undefined,
        application: DEFAULT_APPLICATION,
        applicationId: undefined,
        language: BAZA_COMMON_I18N_CONFIG.defaultLanguage,
    });

    async current(): Promise<RequestContext> {
        const current = this._current$.getValue();

        return {
            ...current,
            // TODO: Fix language: await this.i18nLanguage.detectLanguage(current.application, this.request),
        };
    }

    set patch(requestContext: Partial<RequestContext>) {
        this._current$.next({
            ...this._current$.getValue(),
            ...requestContext,
        });
    }
}
