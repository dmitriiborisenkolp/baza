import { Injectable } from '@nestjs/common';
import { parse } from 'csv-parse';
import { BAZA_CRUD_CONSTANTS } from '@scaliolabs/baza-core-shared';

/**
 * Service Request for Exporting to CSV
 */
export class BazaCsvExportRequest {
    rows: Array<Array<string>>;
    delimiter?: string;
}

/**
 * Escapes special symbols for CSV
 * @param input
 * @param delimiter
 */
function csvEscape(input: string, delimiter: string): string {
    let result = (input || '').toString();

    if (result.includes(delimiter) || result.includes('"')) {
        result = result.split('"').join('""');

        return `"${result}"`;
    } else {
        return result;
    }
}

/**
 * CSV Service
 */
@Injectable()
export class CsvService {
    /**
     * Exports input to CSV
     * @param request
     */
    exportToCSV(request: BazaCsvExportRequest): string {
        const lines: Array<string> = [];

        if (!request.delimiter) {
            request.delimiter = BAZA_CRUD_CONSTANTS.csvDefaultDelimiter;
        }

        for (const line of request.rows) {
            lines.push(line.map((next) => csvEscape(next, request.delimiter)).join(request.delimiter));
        }

        return lines.join('\n');
    }

    /**
     * Parses CSV to Array of Array of Strings
     * @param file
     * @param delimiter
     */
    async parseCsvFile(file: Express.Multer.File, delimiter = BAZA_CRUD_CONSTANTS.csvDefaultDelimiter): Promise<Array<Array<string>>> {
        return new Promise((resolve, reject) => {
            const parser = parse(file.buffer.toString('utf8'), {
                trim: true,
                skip_empty_lines: true,
                delimiter,
            });

            const records: Array<Array<string>> = [];

            parser.on('readable', function () {
                let record;

                while ((record = parser.read()) !== null) {
                    records.push(record);
                }
            });

            parser.on('error', function (err) {
                reject(err);
            });

            parser.on('end', function () {
                resolve(records);
            });
        });
    }
}
