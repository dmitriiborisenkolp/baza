import { Inject, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { BAZA_API_CORE_BCRYPT_DEFAULT_SALT_ROUNDS, BAZA_API_CORE_COMMON_CONFIG } from '../baza-api-common.config';
import type { BazaApiCommonConfig } from '../baza-api-common.config';

@Injectable()
export class BcryptService {
    constructor(@Inject(BAZA_API_CORE_COMMON_CONFIG) private readonly moduleConfig: BazaApiCommonConfig) {}

    async hash(input: string): Promise<string> {
        const salt = await bcrypt.genSalt(this.moduleConfig.bcryptSaltRounds || BAZA_API_CORE_BCRYPT_DEFAULT_SALT_ROUNDS);

        return bcrypt.hash(input, salt);
    }

    async compare(input: string, expected: string): Promise<boolean> {
        return bcrypt.compare(input, expected);
    }
}
