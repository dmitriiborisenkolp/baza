import { Inject, Injectable } from '@nestjs/common';
import type { BazaApiCommonConfig } from '../baza-api-common.config';
import { BAZA_API_CORE_COMMON_CONFIG } from '../baza-api-common.config';
import { CommonEndpointUrlWasNotFoundException } from '../exceptions/common-endpoint-url-was-not-found.exception';
import {
    Application,
    getBazaProjectCodeName,
    getBazaProjectName,
    withoutEndingSlash,
    withoutTrailingSlash,
} from '@scaliolabs/baza-core-shared';
import { BazaRegistryService } from '../../../../baza-registry/src';

@Injectable()
export class ProjectService {
    constructor(
        @Inject(BAZA_API_CORE_COMMON_CONFIG) private readonly moduleConfig: BazaApiCommonConfig,
        private readonly registry: BazaRegistryService,
    ) {}

    /**
     * Returns API BaseUrl
     * API Base URL is set with BAZA_APP_API_URL environment variable
     */
    get apiBaseUrl(): string {
        return withoutEndingSlash(this.moduleConfig.endUserAppUrls.find((def) => def.app === Application.API).url);
    }

    /**
     * Returns CMS BaseUrl
     * CMS Base URL is set with BAZA_APP_CMS_URL environment variable
     */
    get cmsBaseUrl(): string {
        return withoutEndingSlash(this.moduleConfig.endUserAppUrls.find((def) => def.app === Application.CMS).url);
    }

    /**
     * Returns WEB BaseUrl
     * WEB Base URL is set with BAZA_APP_WEB_URL environment variable
     */
    get webBaseUrl(): string {
        return withoutEndingSlash(this.moduleConfig.endUserAppUrls.find((def) => def.app === Application.WEB).url);
    }

    /**
     * Returns link to application (Base URL of application + custom URL)
     * @param request
     */
    createLinkToApplication(request: { application: Application; uri?: string }): string {
        const found = this.moduleConfig.endUserAppUrls.find((endpoint) => endpoint.app === request.application);

        if (!found) {
            throw new CommonEndpointUrlWasNotFoundException(request.application);
        }

        return `${found.url}/${withoutTrailingSlash(request.uri)}`;
    }

    /**
     * Returns Project Name
     * Project Name is human-readable name of Project
     * Project Name can be set with `configureBazaProject` helper
     *
     * @see configureBazaProject
     */
    get projectName(): string {
        return getBazaProjectName();
    }

    /**
     * Returns Project Code Name
     * Project Code Name is computer-readable name of Project. Use it as part of
     * local storage keys, variables & anything which requires project name
     * with ASCII/no-spaces symbols
     * Project Code Name can be set with `configureBazaProject` helper
     *
     * @see configureBazaProject
     */
    get projectCodeName(): string {
        return getBazaProjectCodeName();
    }

    /**
     * Returns Client Name
     * Client Name usually is used for Mail Templates with {{ clientName }}
     * template variable
     *
     * @see configureBazaProject
     */
    get clientName(): string {
        return this.registry.getValue('bazaCommon.clientName');
    }
}
