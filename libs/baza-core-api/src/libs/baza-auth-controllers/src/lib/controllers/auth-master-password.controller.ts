import { Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreAcl, CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { AuthMasterPasswordDto, AuthMasterPasswordEndpoint, AuthMasterPasswordEndpointPaths } from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthMasterPasswordService, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';
import { AclNodes } from '../../../../baza-acl/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src/lib/guards/with-access.guard';

@Controller()
@ApiTags(CoreOpenApiTags.BazaAuthMasterPassword)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@ApiBearerAuth()
@AclNodes([CoreAcl.BazaAuthMasterPassword])
export class AuthMasterPasswordController implements AuthMasterPasswordEndpoint {
    constructor(private readonly service: AuthMasterPasswordService) {}

    @Post(AuthMasterPasswordEndpointPaths.enable)
    @ApiOperation({
        summary: 'enable',
        description: 'Enable Master Password Feature',
    })
    @ApiOkResponse()
    async enable(): Promise<void> {
        await this.service.enable();
    }

    @Post(AuthMasterPasswordEndpointPaths.disable)
    @ApiOperation({
        summary: 'disable',
        description: 'Disable Master Password Feature',
    })
    @ApiOkResponse()
    async disable(): Promise<void> {
        await this.service.disable();
    }

    @Post(AuthMasterPasswordEndpointPaths.current)
    @ApiOperation({
        summary: 'current',
        description: 'Returns current Master Password',
    })
    @ApiOkResponse({
        type: AuthMasterPasswordDto,
    })
    async current(): Promise<AuthMasterPasswordDto> {
        const enabled = await this.service.isEnabled;

        return {
            enabled,
            current: enabled ? await this.service.current() : undefined,
        };
    }

    @Post(AuthMasterPasswordEndpointPaths.refresh)
    @ApiOperation({
        summary: 'refresh',
        description: 'Manually Refresh Master Password',
    })
    @ApiOkResponse({
        type: AuthMasterPasswordDto,
    })
    async refresh(): Promise<AuthMasterPasswordDto> {
        await this.service.refresh();

        const enabled = await this.service.isEnabled;

        return {
            enabled,
            current: enabled ? await this.service.current() : undefined,
        };
    }
}
