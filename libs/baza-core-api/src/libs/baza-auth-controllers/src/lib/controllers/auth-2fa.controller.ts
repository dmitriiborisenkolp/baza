import { Body, Controller, Post, Req } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    Auth2FAEndpoint,
    Auth2FAEndpointPaths,
    Auth2FAMethodsRequest,
    Auth2FAMethodsResponse,
    Auth2FARequest,
    Auth2FAVerifyRequest,
    AuthGoogle2FAQrCodeRequest,
    AuthGoogle2FAQrCodeResponse,
    AuthResponse,
    CoreOpenApiTags,
    extractDeviceTokensFromRequest,
} from '@scaliolabs/baza-core-shared';
import { Auth2FAService, AuthService } from '../../../../baza-auth/src';
import { IpAddress, RequestContextService } from '../../../../baza-common/src';
import { Request } from 'express';
import { toDataURL } from 'qrcode';

@Controller()
@ApiTags(CoreOpenApiTags.BazaAuth2FA)
export class Auth2FAController implements Auth2FAEndpoint {
    constructor(
        private readonly service: Auth2FAService,
        private readonly authService: AuthService,
        private readonly requestContext: RequestContextService,
    ) {}

    @Post(Auth2FAEndpointPaths.auth2FA)
    @ApiOperation({
        summary: 'auth2FA',
        description: 'Validate credentials and sends 2FA verification code',
    })
    @ApiOkResponse()
    async auth2FA(@Body() request: Auth2FARequest, @IpAddress() ip: string, @Req() req: Request): Promise<void> {
        const requestContext = await this.requestContext.current();

        await this.service.auth2FA(request, {
            application: requestContext.application,
            applicationId: requestContext.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }

    @Post(Auth2FAEndpointPaths.auth2FAVerify)
    @ApiOperation({
        summary: 'auth2FAVerify',
        description: 'Validate Verification Code sent by auth2FA method. If verification is successful, API will authorize user',
    })
    @ApiOkResponse({
        type: AuthResponse,
    })
    async auth2FAVerify(@Body() request: Auth2FAVerifyRequest, @IpAddress() ip: string, @Req() req: Request): Promise<AuthResponse> {
        const requestContext = await this.requestContext.current();

        return this.service.auth2FAVerify(request, {
            application: requestContext.application,
            applicationId: requestContext.applicationId,
            withRefreshToken: true,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }

    @Post(Auth2FAEndpointPaths.auth2FAMethods)
    @ApiOperation({
        summary: 'auth2FAMethods',
        description: 'Returns list of active 2FA methods for given account',
    })
    @ApiOkResponse()
    async auth2FAMethods(
        @Body() request: Auth2FAMethodsRequest,
        @IpAddress() ip: string,
        @Req() req: Request,
    ): Promise<Auth2FAMethodsResponse> {
        const requestContext = await this.requestContext.current();

        const account = await this.authService.authVerifyCredentials(
            {
                email: request.email,
                password: request.password,
            },
            {
                application: requestContext.application,
                applicationId: requestContext.applicationId,
                ip,
                userAgent: req.header('User-Agent'),
                deviceTokens: extractDeviceTokensFromRequest(req),
            },
        );

        const methods = await this.service.auth2FAMethods(account);

        return {
            methods,
        };
    }

    @Post(Auth2FAEndpointPaths.generate2FAGoogleQrCode)
    @ApiOperation({
        summary: 'generate2FAGoogleQrCode',
        description: 'Generate QR Code for Google Authenticator to link current account.',
    })
    @ApiOkResponse()
    async generate2FAGoogleQrCode(@Body() request: AuthGoogle2FAQrCodeRequest): Promise<AuthGoogle2FAQrCodeResponse> {
        const otpauthUrl = await this.service.generateGoogle2FAQrCode(request);

        return { qrcodeDataURL: await toDataURL(otpauthUrl) };
    }
}
