import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    AuthEndpoint,
    AuthEndpointPaths,
    AuthRequest,
    AuthResponse,
    AuthVerifyCredentials,
    extractDeviceTokensFromRequest,
    InvalidateAllTokensResponse,
    InvalidateByRefreshTokenRequest,
    InvalidateByRefreshTokenResponse,
    InvalidateTokenRequest,
    InvalidateTokenResponse,
    RefreshTokenRequest,
    RefreshTokenResponse,
    VerifyRequest,
    VerifyResponse,
} from '@scaliolabs/baza-core-shared';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthService } from '../../../../baza-auth/src';
import { IpAddress, RequestContextService } from '../../../../baza-common/src';
import { Request } from 'express';

@Controller()
@ApiTags(CoreOpenApiTags.BazaAuth)
export class AuthController implements AuthEndpoint {
    constructor(private readonly service: AuthService, private readonly requestContext: RequestContextService) {}

    @Post(AuthEndpointPaths.auth)
    @ApiOperation({
        summary: 'auth',
        description: 'Authentication',
    })
    @ApiOkResponse({
        type: AuthResponse,
    })
    async auth(@Body() request: AuthRequest, @IpAddress() ip: string, @Req() req: Request): Promise<AuthResponse> {
        const requestContext = await this.requestContext.current();

        const response = await this.service.auth(request, {
            ip,
            userAgent: req.header('User-Agent'),
            application: requestContext.application,
            applicationId: requestContext.applicationId,
            deviceTokens: extractDeviceTokensFromRequest(req),
        });

        return {
            accessToken: response.accessToken,
            refreshToken: response.refreshToken,
            jwtPayload: response.jwtPayload,
            accountSettings: response.accountSettings,
        };
    }

    @Post(AuthEndpointPaths.refreshToken)
    @ApiOperation({
        summary: 'refreshToken',
        description: 'Receive new accessToken. The method will not generate additional refreshToken',
    })
    @ApiOkResponse({
        type: RefreshTokenResponse,
    })
    async refreshToken(@Body() request: RefreshTokenRequest, @IpAddress() ip: string, @Req() req: Request): Promise<RefreshTokenResponse> {
        const requestContext = await this.requestContext.current();

        const response = await this.service.refreshToken(request, {
            application: requestContext.application,
            applicationId: requestContext.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });

        return {
            accessToken: response.accessToken,
            refreshToken: response.refreshToken,
            jwtPayload: response.jwtPayload,
            accountSettings: response.accountSettings,
        };
    }

    @Post(AuthEndpointPaths.verifyCredentials)
    @ApiOperation({
        summary: 'verifyCredentials',
        description: 'Verify email-password combination. Method does not actually authenticate user',
    })
    @ApiOkResponse()
    async verifyCredentials(@Body() request: AuthVerifyCredentials, @IpAddress() ip: string, @Req() req: Request): Promise<boolean> {
        const requestContext = await this.requestContext.current();

        try {
            await this.service.authVerifyCredentials(request, {
                application: requestContext.application,
                applicationId: requestContext.applicationId,
                ip,
                userAgent: req.header('User-Agent'),
                deviceTokens: extractDeviceTokensFromRequest(req),
            });

            return true;
        } catch (err) {
            return false;
        }
    }

    @Post(AuthEndpointPaths.verify)
    @ApiOperation({
        summary: 'verify',
        description: 'Verify JWT',
    })
    @ApiOkResponse()
    async verify(@Body() request: VerifyRequest): Promise<VerifyResponse> {
        await this.service.verify(request);
    }

    @Post(AuthEndpointPaths.invalidateAllTokens)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'invalidateAllTokens',
        description: 'Invalidated and sign-out user from all devices',
    })
    @ApiOkResponse()
    @UseGuards(AuthGuard)
    async invalidateAllTokens(@Req() req: Request): Promise<InvalidateAllTokensResponse> {
        const deviceTokens = extractDeviceTokensFromRequest(req);

        await this.service.invalidateAllTokens(deviceTokens);
    }

    @Post(AuthEndpointPaths.invalidateToken)
    @ApiOperation({
        summary: 'invalidateToken',
        description: 'Invalidated given token and sigh out user from specific device',
    })
    @ApiOkResponse()
    async invalidateToken(@Body() request: InvalidateTokenRequest, @Req() req: Request): Promise<InvalidateTokenResponse> {
        const deviceTokens = extractDeviceTokensFromRequest(req);

        await this.service.invalidateToken(request, deviceTokens);
    }

    @Post(AuthEndpointPaths.invalidateByRefreshToken)
    @ApiOperation({
        summary: 'invalidateByRefreshToken',
        description: 'Invalidate all access tokens bound to given refreshToken',
    })
    @ApiOkResponse()
    async invalidateByRefreshToken(
        @Body() request: InvalidateByRefreshTokenRequest,
        @Req() req: Request,
    ): Promise<InvalidateByRefreshTokenResponse> {
        const deviceTokens = extractDeviceTokensFromRequest(req);

        await this.service.invalidateByRefreshToken(request, deviceTokens);
    }
}
