import { Body, Controller, HttpCode, HttpStatus, Post, Req, UseGuards } from '@nestjs/common';
import {
    Auth2FAConfigResponse,
    Auth2FASettingsEndpoint,
    Auth2FASettingsEndpointPaths,
    Auth2FAUpdateSettingsRequest,
    CoreOpenApiTags,
    extractDeviceTokensFromRequest,
} from '@scaliolabs/baza-core-shared';
import { IpAddress, RequestContextService } from '../../../../baza-common/src';
import { Auth2FASettingsService, AuthGuard, ReqAccount } from '../../../../baza-auth/src';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { AccountEntity } from '../../../../../../../baza-core-api/src';

@Controller()
@ApiTags(CoreOpenApiTags.BazaAuth2FASettings)
@UseGuards(AuthGuard)
export class Auth2FASettingsController implements Auth2FASettingsEndpoint {
    constructor(private readonly requestContext: RequestContextService, private readonly service: Auth2FASettingsService) {}

    @Post(Auth2FASettingsEndpointPaths.config)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'config',
        description: 'Returns current 2FA configuration',
    })
    @ApiOkResponse()
    async config(@ReqAccount() account: AccountEntity): Promise<Auth2FAConfigResponse> {
        return this.service.config(account);
    }

    @Post(Auth2FASettingsEndpointPaths.enable2FAEmail)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'enable2FAEmail',
        description: 'Enable 2FA via Email for current account. Action requires current password to proceed.',
    })
    @ApiOkResponse()
    async enable2FAEmail(
        @Body() request: Auth2FAUpdateSettingsRequest,
        @IpAddress() ip: string,
        @Req() req: Request,
        @ReqAccount() account: AccountEntity,
    ): Promise<void> {
        const requestContext = await this.requestContext.current();

        await this.service.enable2FAEmail(account, request, {
            application: requestContext.application,
            applicationId: requestContext.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }

    @Post(Auth2FASettingsEndpointPaths.disable2FAEmail)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'disable2FAEmail',
        description: 'Disable 2FA via Email for current account. Action requires current password to proceed.',
    })
    @ApiOkResponse()
    async disable2FAEmail(
        @Body() request: Auth2FAUpdateSettingsRequest,
        @IpAddress() ip: string,
        @Req() req: Request,
        @ReqAccount() account: AccountEntity,
    ): Promise<void> {
        const requestContext = await this.requestContext.current();

        await this.service.disable2FAEmail(account, request, {
            application: requestContext.application,
            applicationId: requestContext.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }

    @Post(Auth2FASettingsEndpointPaths.enable2FAGoogle)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'enable2FAGoogle',
        description: 'Enable 2FA via Google Authenticator for current account. Action requires current password to proceed.',
    })
    @ApiOkResponse()
    async enable2FAGoogle(
        @Body() request: Auth2FAUpdateSettingsRequest,
        @IpAddress() ip: string,
        @Req() req: Request,
        @ReqAccount() account: AccountEntity,
    ): Promise<void> {
        const requestContext = await this.requestContext.current();

        await this.service.enable2FAGoogle(account, request, {
            application: requestContext.application,
            applicationId: requestContext.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }

    @Post(Auth2FASettingsEndpointPaths.disable2FAGoogle)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'disable2FAGoogle',
        description: 'Disable 2FA via Google Authenticator for current account. Action requires current password to proceed.',
    })
    @ApiOkResponse()
    async disable2FAGoogle(
        @Body() request: Auth2FAUpdateSettingsRequest,
        @IpAddress() ip: string,
        @Req() req: Request,
        @ReqAccount() account: AccountEntity,
    ): Promise<void> {
        const requestContext = await this.requestContext.current();

        await this.service.disable2FAGoogle(account, request, {
            application: requestContext.application,
            applicationId: requestContext.applicationId,
            ip,
            userAgent: req.header('User-Agent'),
            deviceTokens: extractDeviceTokensFromRequest(req),
        });
    }
}
