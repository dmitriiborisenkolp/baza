import 'reflect-metadata';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { awaitTimeout, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { AccountRole } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-auth/integration-tests/01-baza-auth.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('sign in successfully for e2e@scal.io user and correct password', async () => {
        const authResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(authResponse)).toBe(false);
        expect(typeof authResponse.accessToken).toBe('string');
        expect(typeof authResponse.refreshToken).toBe('string');
        expect(typeof authResponse.jwtPayload.accountId).toBe('number');
        expect(authResponse.jwtPayload.accountEmail).toEqual('e2e@scal.io');
        expect(authResponse.jwtPayload.accountRole).toEqual(AccountRole.User);
    });

    it('sign in fails for e2e@scal.io user with incorrect password', async () => {
        const authResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password-fail',
        })) as any;

        expect(isBazaErrorResponse(authResponse)).toBe(true);
        expect(authResponse.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('sign in fails for unknown user', async () => {
        const authResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-some-random-user@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(authResponse)).toBe(true);
        expect(authResponse.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('verify token successfully with correct access token', async () => {
        await http.authE2eUser();

        const verifyResponse = await dataAccessAuth.verify({
            jwt: http.authResponse.accessToken,
        });

        expect(isBazaErrorResponse(verifyResponse)).toBe(false);
    });

    it('verify token fail with incorrect access token', async () => {
        await http.authE2eUser();

        const verifyResponse = await dataAccessAuth.verify({
            jwt: 'some-incorrect-jwt',
        });

        expect(isBazaErrorResponse(verifyResponse)).toBe(true);
    });

    it('successfully refresh token', async () => {
        await http.authE2eUser();

        const accessToken = http.authResponse.accessToken;
        const refreshToken = http.authResponse.refreshToken;

        await awaitTimeout(1000); // Unless same token will be generated

        const refreshTokenResponse = await dataAccessAuth.refreshToken({
            refreshToken,
        });

        expect(isBazaErrorResponse(refreshTokenResponse)).toBe(false);

        expect(accessToken).not.toEqual(refreshTokenResponse.accessToken);
        expect(refreshToken).not.toEqual(refreshTokenResponse.refreshToken);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessToken,
                }),
            ),
        ).toBe(false);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: refreshTokenResponse.accessToken,
                }),
            ),
        ).toBe(false);
    });

    it('successfully invalidate token with correct access token', async () => {
        await http.authE2eUser();

        const accessTokenFirst = http.authResponse.accessToken;

        await awaitTimeout(1000); // Unless same token will be generated
        await http.authE2eUser();

        const accessTokenSecond = http.authResponse.accessToken;

        expect(accessTokenFirst).not.toBe(accessTokenSecond);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessTokenFirst,
                }),
            ),
        ).toBe(false);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessTokenSecond,
                }),
            ),
        ).toBe(false);

        const invalidateResponse = await dataAccessAuth.invalidateToken({
            jwt: accessTokenFirst,
        });

        expect(isBazaErrorResponse(invalidateResponse)).toBe(false);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessTokenFirst,
                }),
            ),
        ).toBe(true);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessTokenSecond,
                }),
            ),
        ).toBe(false);
    });

    it('invalidate token successfully even with incorrect access token', async () => {
        await http.authE2eUser();

        const accessToken = http.authResponse.accessToken;

        const invalidateResponse = await dataAccessAuth.invalidateToken({
            jwt: 'some-invalid-token',
        });

        expect(isBazaErrorResponse(invalidateResponse)).toBe(false);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessToken,
                }),
            ),
        ).toBe(false);
    });

    it('successfully invalidate all tokens with correct access token', async () => {
        await http.authE2eUser();

        const accessTokenFirst = http.authResponse.accessToken;

        await awaitTimeout(1000); // Unless same token will be generated
        await http.authE2eUser();

        const accessTokenSecond = http.authResponse.accessToken;

        expect(accessTokenFirst).not.toBe(accessTokenSecond);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessTokenFirst,
                }),
            ),
        ).toBe(false);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessTokenSecond,
                }),
            ),
        ).toBe(false);

        const invalidateResponse = await dataAccessAuth.invalidateAllTokens();

        expect(isBazaErrorResponse(invalidateResponse)).toBe(false);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessTokenFirst,
                }),
            ),
        ).toBe(true);

        expect(
            isBazaErrorResponse(
                await dataAccessAuth.verify({
                    jwt: accessTokenSecond,
                }),
            ),
        ).toBe(true);
    });
});
