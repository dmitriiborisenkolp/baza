import 'reflect-metadata';
import {
    BazaAuth2FANodeAccess,
    BazaAuth2FASettingsNodeAccess,
    BazaAuthNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import {
    Auth2FAErrorCodes,
    Auth2FAVerificationMethod,
    AuthErrorCodes,
    BazaCoreE2eFixtures,
    BazaError,
    isBazaErrorResponse,
} from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-auth/integration-tests/06-baza-auth-2fa-settings.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAuth2FA = new BazaAuth2FANodeAccess(http);
    const dataAccessAuth2FASettings = new BazaAuth2FASettingsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will successfully auth without 2FA before enabling 2FA', async () => {
        const authUserResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(authUserResponse)).toBe(false);
    });

    it('will display that no 2FA enabled', async () => {
        await http.authE2eUser();

        const response = await dataAccessAuth2FASettings.config();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.allowed2FAMethods.includes(Auth2FAVerificationMethod.Email)).toBeTruthy();
        expect(response.forced2FAMethods.length).toBe(0);
        expect(response.enabled2FAMethods.length).toBe(0);
    });

    it('will not enable 2FA via Email with incorrect password', async () => {
        const response: BazaError = (await dataAccessAuth2FASettings.enable2FAEmail({
            password: 'SomeRandomPassword#12345',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will display that no 2FA enabled after attemts to enable it with incorrect password', async () => {
        const response = await dataAccessAuth2FASettings.config();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.allowed2FAMethods.includes(Auth2FAVerificationMethod.Email)).toBeTruthy();
        expect(response.forced2FAMethods.length).toBe(0);
        expect(response.enabled2FAMethods.length).toBe(0);
    });

    it('will enable 2FA via Email with correct password', async () => {
        const response = await dataAccessAuth2FASettings.enable2FAEmail({
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that 2FA via Email is enabled', async () => {
        const response = await dataAccessAuth2FASettings.config();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.allowed2FAMethods.includes(Auth2FAVerificationMethod.Email)).toBeTruthy();
        expect(response.forced2FAMethods.length).toBe(0);
        expect(response.enabled2FAMethods.length).toBe(1);
        expect(response.enabled2FAMethods).toEqual([Auth2FAVerificationMethod.Email]);
    });

    it('will display Email method as enabled for 2FA', async () => {
        const response = await dataAccessAuth2FA.auth2FAMethods({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.methods.length).toBe(1);
        expect(response.methods).toEqual([Auth2FAVerificationMethod.Email]);
    });

    it('will not allow to sign in without 2FA request', async () => {
        const authUserResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(authUserResponse)).toBeTruthy();
        expect(authUserResponse.code).toBe(Auth2FAErrorCodes.Auth2FARequired);
    });

    it('will not allow to disable 2FA via Email with incorrect password', async () => {
        const response: BazaError = (await dataAccessAuth2FASettings.disable2FAEmail({
            password: 'SomeRandomPassword#12345',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will disable 2FA via Email with correct password', async () => {
        const response = await dataAccessAuth2FASettings.disable2FAEmail({
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that 2FA via Email is disabled', async () => {
        const response = await dataAccessAuth2FASettings.config();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.allowed2FAMethods.includes(Auth2FAVerificationMethod.Email)).toBeTruthy();
        expect(response.forced2FAMethods.length).toBe(0);
        expect(response.enabled2FAMethods.length).toBe(0);
    });

    it('will successfully auth without 2FA before disabling 2FA via Email', async () => {
        const authUserResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(authUserResponse)).toBe(false);
    });
});
