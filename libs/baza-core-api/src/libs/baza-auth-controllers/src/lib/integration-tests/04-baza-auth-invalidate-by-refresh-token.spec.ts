import 'reflect-metadata';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { awaitTimeout, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import * as _ from 'underscore';

describe('@scaliolabs/baza-core-api/baza-auth/integration-tests/04-baza-auth-invalidate-by-refresh-token.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    let REFRESH_TOKEN_1: string;
    let REFRESH_TOKEN_2: string;

    let ACCESS_TOKEN_1_1: string;
    let ACCESS_TOKEN_1_2: string;
    let ACCESS_TOKEN_2_1: string;
    let ACCESS_TOKEN_2_2: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will successfully sign in with different tokens', async () => {
        const auth1 = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        await awaitTimeout(1500);

        const auth2 = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        REFRESH_TOKEN_1 = auth1.refreshToken;
        REFRESH_TOKEN_2 = auth2.refreshToken;

        ACCESS_TOKEN_1_1 = auth1.accessToken;
        ACCESS_TOKEN_2_1 = auth2.accessToken;

        const refresh1_2 = await dataAccessAuth.refreshToken({
            refreshToken: REFRESH_TOKEN_1,
        });

        const refresh2_2 = await dataAccessAuth.refreshToken({
            refreshToken: REFRESH_TOKEN_2,
        });

        ACCESS_TOKEN_1_2 = refresh1_2.accessToken;
        ACCESS_TOKEN_2_2 = refresh2_2.accessToken;

        const allTokens = [REFRESH_TOKEN_1, REFRESH_TOKEN_2, ACCESS_TOKEN_1_1, ACCESS_TOKEN_1_2, ACCESS_TOKEN_2_1, ACCESS_TOKEN_2_2];

        expect(_.uniq(allTokens).length).toBe(allTokens.length);
    });

    it('will verify with all access tokens', async () => {
        for (const jwt of [ACCESS_TOKEN_1_1, ACCESS_TOKEN_1_2, ACCESS_TOKEN_2_1, ACCESS_TOKEN_2_2]) {
            const response = dataAccessAuth.verify({
                jwt,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
        }
    });

    it('will successfully sign out by refresh token', async () => {
        const response = await dataAccessAuth.invalidateByRefreshToken({
            refreshToken: REFRESH_TOKEN_1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
