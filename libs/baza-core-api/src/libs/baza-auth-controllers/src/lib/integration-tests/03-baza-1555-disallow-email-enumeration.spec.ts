// https://scalio.atlassian.net/browse/CMNW-1555

import 'reflect-metadata';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-auth/integration-tests/03-baza-1555-disallow-email-enumeration.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will response with AuthInvalidCredentials for invalid password', async () => {
        const authResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password-invalid',
        })) as any;

        expect(isBazaErrorResponse(authResponse)).toBe(true);
        expect(authResponse.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will response with AuthInvalidCredentials for invalid email', async () => {
        const authResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-unknown-user@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(authResponse)).toBe(true);
        expect(authResponse.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });
});
