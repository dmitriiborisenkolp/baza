import 'reflect-metadata';
import {
    BazaAuth2FANodeAccess,
    BazaAuthNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import {
    AccountRole,
    Auth2FAErrorCodes,
    Auth2FAVerificationMethod,
    BazaCoreE2eFixtures,
    BazaError,
    isBazaErrorResponse,
} from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-auth/integration-tests/05-baza-auth-2fa-force-for-admin-users.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAuth2FA = new BazaAuth2FANodeAccess(http);

    const REGEX_CODE = /<code>(\d+)<\/code>/;

    let VERIFICATION_CODE: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will successfully auth without 2FA before enabling 2FA with E2E helpers', async () => {
        const authUserResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(authUserResponse)).toBe(false);

        const authAdminResponse = await dataAccessAuth.auth({
            email: 'e2e-admin@scal.io',
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(authAdminResponse)).toBe(false);
    });

    it('will successfully enable 2FA for admin accounts and users will still be able to sign in', async () => {
        await dataAccessE2e.enable2FAForAccountRoles({
            accountRoles: [AccountRole.Admin],
        });

        const authUserResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(authUserResponse)).toBe(false);

        const auth2FAMethodsResponseUser = await dataAccessAuth2FA.auth2FAMethods({
            email: 'e2e@scal.io',
            password: 'e2e-user-password',
        });

        expect(isBazaErrorResponse(auth2FAMethodsResponseUser)).toBeFalsy();
        expect(auth2FAMethodsResponseUser.methods.length).toBe(0);

        const auth2FAMethodsResponseAdmin = await dataAccessAuth2FA.auth2FAMethods({
            email: 'e2e-admin@scal.io',
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(auth2FAMethodsResponseAdmin)).toBeFalsy();
        expect(auth2FAMethodsResponseAdmin.methods.length).toBe(1);
        expect(auth2FAMethodsResponseAdmin.methods).toEqual([Auth2FAVerificationMethod.Email]);
    });

    it('will not allow for admin user to sign in without 2FA', async () => {
        const authAdminResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(authAdminResponse)).toBeTruthy();
        expect(authAdminResponse.code).toBe(Auth2FAErrorCodes.Auth2FARequired);
    });

    it('will allow to send 2FA Verification code', async () => {
        const response = await dataAccessAuth2FA.auth2FA({
            email: 'e2e-admin@scal.io',
            password: 'e2e-admin-password',
            method: Auth2FAVerificationMethod.Email,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will have verification mail in mailbox', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);
    });

    it('will found a verification code inside', async () => {
        const mailbox = await dataAccessE2e.mailbox();
        const mail = mailbox[0];

        const matches = mail.messageBodyHtml.match(REGEX_CODE);

        expect(matches).toBeDefined();
        expect(matches.length).toBe(2);

        VERIFICATION_CODE = matches[1];
    });

    it('will not allow to verify with random code', async () => {
        const response: BazaError = (await dataAccessAuth2FA.auth2FAVerify({
            email: 'e2e-admin@scal.io',
            code: VERIFICATION_CODE === '123456' ? '654321' : '123456',
            method: Auth2FAVerificationMethod.Email,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(Auth2FAErrorCodes.Auth2FAInvalidToken);
    });

    it('will allow to verify valid code', async () => {
        const response = await dataAccessAuth2FA.auth2FAVerify({
            email: 'e2e-admin@scal.io',
            code: VERIFICATION_CODE,
            method: Auth2FAVerificationMethod.Email,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const verifyJwtResponse = await dataAccessAuth.verify({
            jwt: response.accessToken,
        });

        expect(isBazaErrorResponse(verifyJwtResponse)).toBeFalsy();
    });

    it('will not allow to verify same code again', async () => {
        const response: BazaError = (await dataAccessAuth2FA.auth2FAVerify({
            email: 'e2e-admin@scal.io',
            code: VERIFICATION_CODE,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });
});
