import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaAuthMasterPasswordNodeAccess, BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaError, isBazaErrorResponse, isEmpty } from '@scaliolabs/baza-core-shared';
import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-core-api/baza-auth/integration-tests/02-baza-auth-master-password.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessAuthMasterPassword = new BazaAuthMasterPasswordNodeAccess(http);

    let masterPassword: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('enable master password feature fails for user', async () => {
        await http.authE2eUser();

        const response = await dataAccessAuthMasterPassword.enable();

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('enable master password feature successfully for root', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAuthMasterPassword.enable();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(isEmpty(response)).toBeTruthy();
    });

    it('disable master password feature successfully', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAuthMasterPassword.disable();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(isEmpty(response)).toBeTruthy();

        const dto = await dataAccessAuthMasterPassword.current();

        expect(dto.enabled).toBeFalsy();
        expect(isEmpty(dto.current)).toBeTruthy();
    });

    it('does not returns master password to root', async () => {
        await http.authE2eUser();

        await dataAccessAuthMasterPassword.enable();

        const current = await dataAccessAuthMasterPassword.current();

        expect(isBazaErrorResponse(current)).toBeTruthy();
    });

    it('returns master password to root', async () => {
        await http.authE2eAdmin();

        await dataAccessAuthMasterPassword.enable();

        const dto = await dataAccessAuthMasterPassword.current();

        expect(isBazaErrorResponse(dto)).toBeFalsy();
        expect(dto.enabled).toBeTruthy();
        expect(typeof dto.current).toBe('string');
        expect(dto.current.length).toBe(32);

        masterPassword = dto.current;
    });

    it('successful auth user with master password', async () => {
        const authResponse = await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: masterPassword,
        });

        expect(isBazaErrorResponse(authResponse)).toBeFalsy();
    });

    it('still will not auth user with invalid password', async () => {
        const authResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: 'some-random-pass',
        })) as any;

        expect(isBazaErrorResponse(authResponse)).toBe(true);
        expect(authResponse.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will not auth root with invalid password', async () => {
        const authResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin@scal.io',
            password: masterPassword,
        })) as any;

        expect(isBazaErrorResponse(authResponse)).toBeTruthy();
        expect(authResponse.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });

    it('will not auth user with master password after disabling master password feature', async () => {
        await dataAccessAuthMasterPassword.disable();

        const authResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e@scal.io',
            password: masterPassword,
        })) as any;

        expect(isBazaErrorResponse(authResponse)).toBeTruthy();
        expect(authResponse.code).toBe(AuthErrorCodes.AuthInvalidCredentials);
    });
});
