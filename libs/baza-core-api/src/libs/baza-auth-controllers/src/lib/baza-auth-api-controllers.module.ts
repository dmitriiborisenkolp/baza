import { Module } from '@nestjs/common';
import { AuthController } from './controllers/auth.controller';
import { AuthMasterPasswordController } from './controllers/auth-master-password.controller';
import { BazaAuthMasterPasswordModule, BazaAuthModule } from '../../../baza-auth/src';
import { Auth2FAController } from './controllers/auth-2fa.controller';
import { Auth2FASettingsController } from './controllers/auth-2fa-settings.controller';

@Module({
    imports: [
        BazaAuthModule,
        BazaAuthMasterPasswordModule,
    ],
    controllers: [
        AuthController,
        AuthMasterPasswordController,
        Auth2FAController,
        Auth2FASettingsController,
    ],
})
export class BazaAuthApiControllersModule {
}
