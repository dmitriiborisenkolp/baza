import { Connection, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { RegistryRecordEntity } from '../entity/registry-record.entity';

@Injectable()
export class RegistryRecordRepository {
    constructor(private readonly connection: Connection) {}

    get repository(): Repository<RegistryRecordEntity> {
        return this.connection.getRepository(RegistryRecordEntity);
    }

    async saveEntity<T = any>(entity: RegistryRecordEntity<T>): Promise<void> {
        await this.repository.save(entity);
    }

    async getAll() {
        return this.repository.find();
    }

    async upsert<T = any>(entity: Pick<RegistryRecordEntity<T>, 'path' | 'value' | 'label'>) {
        const record = await this.repository.findOne({ path: entity.path });

        if (!record) {
            return this.saveEntity(entity as RegistryRecordEntity);
        }

        record.label = entity.label;
        record.value = entity.value;

        return this.saveEntity(record);
    }

    async getEntity<T = any>(record: RegistryRecordEntity<T> | number): Promise<RegistryRecordEntity<T>> {
        const entity = await this.repository.findOne({
            where: [{ record }],
        });

        if (!entity) {
            throw new NotFoundException();
        }

        return entity;
    }
}
