import { RegistrySchema } from '@scaliolabs/baza-core-shared';

export interface BazaRegistryApiConfig {
    schema: () => RegistrySchema;
}

export const BAZA_REGISTRY_CONFIG = Symbol();
