import { Inject, Injectable } from '@nestjs/common';
import { RegistryRecordRepository } from '../repositories/registry-record.repository';
import { RegistryRecordEntity } from '../entity/registry-record.entity';
import { BehaviorSubject, Observable } from 'rxjs';
import { RegistryApiEvent, RegistryApiEvents, RegistryNode, RegistryPublicNode, RegistrySchema } from '@scaliolabs/baza-core-shared';
import { BAZA_REGISTRY_CONFIG } from '../baza-registry-api.config';
import type { BazaRegistryApiConfig } from '../baza-registry-api.config';
import { ApiEventSource } from '@scaliolabs/baza-core-shared';
import { EnvService } from '../../../../baza-env/src';
import { ApiEventBus } from '../../../../baza-event-bus/src';
import { bazaRegistryValue } from '../utils/baza-registry.utils';
import { RegistryNodeNotFoundException } from '../exceptions/registry-node-not-found.exception';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const cloneDeep = require('clone-deep');

@Injectable()
export class BazaRegistryService {
    private readonly _current$: BehaviorSubject<RegistrySchema> = new BehaviorSubject<RegistrySchema>(this.defaultSchema);

    constructor(
        @Inject(BAZA_REGISTRY_CONFIG) private readonly moduleConfig: BazaRegistryApiConfig,
        private readonly env: EnvService,
        private readonly repository: RegistryRecordRepository,
        private readonly bazaEventBus: ApiEventBus<RegistryApiEvent, RegistryApiEvents>,
    ) {}

    get defaultSchema(): RegistrySchema {
        return this.moduleConfig.schema();
    }

    get current$(): Observable<RegistrySchema> {
        return this._current$.asObservable();
    }

    get current(): RegistrySchema {
        return this._current$.getValue();
    }

    getNode(path: string): RegistryNode {
        const deep = (parts: Array<string>, payload: RegistrySchema | RegistryNode): any => {
            if (!payload) {
                throw new RegistryNodeNotFoundException(path, parts.join(', '));
            }

            if (parts.length) {
                return deep(parts, (payload as RegistrySchema)[parts.shift()]);
            } else {
                return payload as RegistryNode;
            }
        };

        return deep(path.split('.'), this._current$.getValue());
    }

    getValue(path: string): any {
        const node = this.getNode(path);

        if (node.forEnvironments && !node.forEnvironments.includes(this.env.current)) {
            return bazaRegistryValue(node.defaults, {
                env: this.env.current,
                environment: this.env.all,
            });
        } else {
            const nodeValue = node.value === undefined ? node.defaults : node.value;

            return bazaRegistryValue(nodeValue, {
                env: this.env.current,
                environment: this.env.all,
            });
        }
    }

    getNodes(paths: Array<string>): Array<RegistryNode> {
        return paths.map((path) => this.getNode(path));
    }

    async loadSchema(): Promise<RegistrySchema> {
        const saved = await this.repository.getAll();
        const newSchema = cloneDeep(this.defaultSchema);
        const savedMap: { [path: string]: RegistryRecordEntity } = {};

        saved.forEach((record) => {
            savedMap[record.path] = {
                ...record,
            };
        });

        const deep = (schema: RegistrySchema, path: Array<string> = []) => {
            Object.keys(schema).forEach((nodeName) => {
                const node = schema[nodeName];
                const nodePath = [...path, nodeName];
                const sNodePath = nodePath.join('.');

                if ('name' in node && 'type' in node) {
                    node.defaults = bazaRegistryValue(node.defaults, {
                        env: this.env.current,
                        environment: this.env.all,
                    });

                    if (
                        Array.isArray((node as RegistryNode).forEnvironments) &&
                        !(node as RegistryNode).forEnvironments.includes(this.env.current)
                    ) {
                        (node as RegistryNode).hiddenFromList = true;
                    }

                    if ((node as RegistryNode).value === undefined) {
                        (node as RegistryNode).value = (node as RegistryNode).defaults;
                    }

                    if (sNodePath in savedMap) {
                        (node as RegistryNode).value = savedMap[sNodePath].value;
                        if (savedMap[sNodePath]?.label) (node as RegistryNode).label = savedMap[sNodePath].label;
                    }
                } else {
                    deep(node as RegistrySchema, [...path, nodeName]);
                }
            });
        };

        deep(newSchema);

        this._current$.next(newSchema);

        return newSchema;
    }

    async publicSchema(): Promise<Array<RegistryPublicNode>> {
        const entries: Array<RegistryPublicNode> = [];

        const deep = (node: RegistrySchema | RegistryNode, path: Array<string>) => {
            if ('name' in node && 'type' in node) {
                if ((node as RegistryNode).public) {
                    entries.push({
                        id: path.join('.'),
                        node: node as any,
                    });
                }
            } else {
                Object.keys(node).forEach((key) => {
                    if (!key.startsWith('_')) {
                        deep(node[key] as any, [...path, key]);
                    }
                });
            }
        };

        deep(this._current$.getValue(), []);

        return entries
            .filter((entry) => entry.node.public)
            .map((entry) => {
                const nodeValue = entry.node.value || entry.node.defaults;

                return {
                    ...entry,
                    node: {
                        ...entry.node,
                        value: bazaRegistryValue(nodeValue, {
                            env: this.env.current,
                            environment: this.env.all,
                        }),
                    },
                };
            });
    }

    async updateSchemaRecord(
        request: Pick<RegistryRecordEntity, 'path' | 'value' | 'label'>,
        options: { noPublish?: boolean } = {},
    ): Promise<void> {
        const entity = new RegistryRecordEntity();

        entity.path = request.path;
        entity.value = request.value;
        entity.label = request?.label;

        await this.repository.upsert(request);

        if (!options?.noPublish) {
            await this.bazaEventBus.publish(
                {
                    topic: RegistryApiEvent.BazaRegistryNodeUpdated,
                    payload: {
                        path: entity.path,
                        label: entity?.label,
                        value: entity.value,
                    },
                },
                {
                    target: ApiEventSource.EveryNode,
                },
            );
        }

        await this.loadSchema();
    }
}
