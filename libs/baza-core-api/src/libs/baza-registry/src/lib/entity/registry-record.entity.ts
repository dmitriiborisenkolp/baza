import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class RegistryRecordEntity<T = any> {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    path: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    label?: string;

    @Column({
        type: 'json',
        nullable: true,
    })
    value: T;
}
