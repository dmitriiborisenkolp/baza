import { HttpStatus } from '@nestjs/common';
import { BazaRegistryErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Record not found';

export class RegistryRecordNotFoundException extends BazaAppException {
  constructor() {
    super(BazaRegistryErrorCodes.BazaRegistryRecordNotFound, ERR_MESSAGE, HttpStatus.NOT_FOUND);
  }
}
