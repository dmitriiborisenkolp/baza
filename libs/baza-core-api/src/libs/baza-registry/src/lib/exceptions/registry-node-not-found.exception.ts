import { HttpStatus } from '@nestjs/common';
import { BazaRegistryErrorCodes, replaceTags } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Registry node "{{ path }}" (parents: {{ parents }}) not found';

export class RegistryNodeNotFoundException extends BazaAppException {
    constructor(path: string, parents: string) {
        super(
            BazaRegistryErrorCodes.BazaRegistryNodeNotFound,
            replaceTags(ERR_MESSAGE, { path, parents }),
            HttpStatus.NOT_FOUND,
        );
    }
}
