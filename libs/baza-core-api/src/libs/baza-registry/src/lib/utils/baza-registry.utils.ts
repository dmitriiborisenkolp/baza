import { BazaEnvironments, RegistryDefaultValueCallable } from '@scaliolabs/baza-core-shared';

export function bazaRegistryValue(
    input: RegistryDefaultValueCallable<any> | any,
    options: {
        env: BazaEnvironments;
        environment: any;
    },
): any {
    return typeof input === 'function' ? input(options.env, options.environment) : input;
}
