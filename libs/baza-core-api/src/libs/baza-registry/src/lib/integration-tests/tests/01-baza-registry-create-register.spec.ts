import 'reflect-metadata';
import { BazaDataAccessNode, BazaRegistryNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BAZA_VERSION_DEFAULT_MESSAGE_REGISTRY_KEY, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

jest.setTimeout(240000);

describe('@baza-core-api/baza-registry/integration-tests/tests/01-baza-registry-create-register.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaRegistryNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => await http.authE2eAdmin());

    it('will successfully create new register record', async () => {
        const label = 'Registry Label Test';
        const value = 'Version E2E Outdated';

        let response = await dataAccess.updateSchemaRecord({
            path: BAZA_VERSION_DEFAULT_MESSAGE_REGISTRY_KEY,
            value,
            label,
        });

        response = JSON.parse(response.toString());

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.label).toEqual(label);
        expect(response.value).toEqual(value);
    });

    it('will successfully update register record', async () => {
        const label = 'Registry Label Test Update';
        const value = 'Version E2E Outdated Update';

        let response = await dataAccess.updateSchemaRecord({
            path: BAZA_VERSION_DEFAULT_MESSAGE_REGISTRY_KEY,
            value,
            label,
        });

        response = JSON.parse(response.toString());

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.label).toEqual(label);
        expect(response.value).toEqual(value);
    });

    it('should fail to insert or update record', async () => {
        const value = 'Version E2E Outdated Update';

        const response = await dataAccess.updateSchemaRecord({
            path: null,
            value,
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });
});
