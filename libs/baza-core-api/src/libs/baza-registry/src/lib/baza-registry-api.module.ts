import { DynamicModule, Global, Module, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { BazaRegistryService } from './services/baza-registry.service';
import { RegistryRecordRepository } from './repositories/registry-record.repository';
import { BAZA_REGISTRY_CONFIG, BazaRegistryApiConfig } from './baza-registry-api.config';
import { Subject } from 'rxjs';
import { ApiEventBus } from '../../../baza-event-bus/src';
import { ApiEventSource, RegistryApiEvent, RegistryApiEvents } from '@scaliolabs/baza-core-shared';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaRegistryApiConfig>;
}

export { AsyncOptions as BazaRegistryApiModuleAsyncOptions };

@Module({})
export class BazaRegistryApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaRegistryApiGlobalModule,
            providers: [{
                provide: BAZA_REGISTRY_CONFIG,
                inject: options.injects,
                useFactory: options.useFactory,
            }],
            exports: [
                BAZA_REGISTRY_CONFIG,
            ],
        };
    }
}

@Global()
@Module({
    providers: [
        BazaRegistryService,
        RegistryRecordRepository,
    ],
    exports: [
        BazaRegistryService,
    ],
})
class BazaRegistryApiGlobalModule implements OnModuleInit, OnModuleDestroy {
    private readonly onModuleDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private readonly service: BazaRegistryService,
        private readonly bazaEventBus: ApiEventBus<RegistryApiEvent, RegistryApiEvents>,
    ) {}

    async onModuleInit(): Promise<void> {
        await this.service.loadSchema();

        this.bazaEventBus.subscribe({
            source: ApiEventSource.EveryNode,
            filterByEvent: [RegistryApiEvent.BazaRegistryNodeUpdated],
            takeUntil$: this.onModuleDestroy$,
            callback: async () => {
                await this.service.loadSchema();
            },
        });
    }

    async onModuleDestroy(): Promise<void> {
        this.onModuleDestroy$.next();
    }
}
