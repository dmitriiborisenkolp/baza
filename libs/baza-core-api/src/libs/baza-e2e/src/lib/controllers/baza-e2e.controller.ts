import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
    BazaE2eDisableFeatureRequest,
    BazaE2eEnable2FAForAccountRolesRequest,
    BazaE2eEnableFeatureRequest,
    CoreOpenApiTags,
} from '@scaliolabs/baza-core-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaE2eEndpoint, BazaE2eEndpointPaths, BazaE2eMailDto } from '@scaliolabs/baza-core-shared';
import { BazaE2eService } from '../services/baza-e2e.service';
import { BazaE2eGuard } from '../guards/baza-e2e.guard';

@Controller()
@ApiTags(CoreOpenApiTags.BazaE2e)
@UseGuards(BazaE2eGuard)
export class BazaE2eController implements BazaE2eEndpoint {
    constructor(private readonly service: BazaE2eService) {}

    @Post(BazaE2eEndpointPaths.flush)
    @ApiOperation({
        summary: 'flush',
        description: 'Flush everything',
    })
    @ApiOkResponse()
    async flush(): Promise<void> {
        await this.service.flush();
    }

    @Post(BazaE2eEndpointPaths.flushDatabase)
    @ApiOperation({
        summary: 'flushDatabase',
        description: 'Flush database',
    })
    @ApiOkResponse()
    async flushDatabase(): Promise<void> {
        await this.service.flushDatabase();
    }

    @Post(BazaE2eEndpointPaths.flushRedis)
    @ApiOperation({
        summary: 'flushRedis',
        description: 'Flush redis',
    })
    @ApiOkResponse()
    async flushRedis(): Promise<void> {
        await this.service.flushRedis();
    }

    @Post(BazaE2eEndpointPaths.flushMailbox)
    @ApiOperation({
        summary: 'flushMailbox',
        description: 'Flush mailbox',
    })
    @ApiOkResponse()
    async flushMailbox(): Promise<void> {
        await this.service.flushMailbox();
    }

    @Get(BazaE2eEndpointPaths.mailbox)
    @ApiOperation({
        summary: 'mailbox',
        description: 'Returns mailbox',
    })
    @ApiOkResponse({
        type: BazaE2eMailDto,
        isArray: true,
    })
    async mailbox(): Promise<Array<BazaE2eMailDto>> {
        return this.service.mailbox();
    }

    @Post(BazaE2eEndpointPaths.enable2FAForAccountRoles)
    @ApiOperation({
        summary: 'enable2FAForAccountRoles',
        description: 'Enable 2FA for specific account roles',
    })
    @ApiOkResponse()
    async enable2FAForAccountRoles(@Body() request: BazaE2eEnable2FAForAccountRolesRequest): Promise<void> {
        await this.service.enable2FAForAccountRoles(request);
    }

    @Post(BazaE2eEndpointPaths.enableFeature)
    @ApiOperation({
        summary: 'enableFeature',
        description: 'Enable Baza feature',
    })
    @ApiOkResponse()
    async enableFeature(@Body() request: BazaE2eEnableFeatureRequest): Promise<void> {
        await this.service.enableFeature(request);
    }

    @Post(BazaE2eEndpointPaths.disableFeature)
    @ApiOperation({
        summary: 'disableFeature',
        description: 'Disable Baza feature',
    })
    @ApiOkResponse()
    async disableFeature(@Body() request: BazaE2eDisableFeatureRequest): Promise<void> {
        await this.service.disableFeature(request);
    }

    @Post(BazaE2eEndpointPaths.setEnv)
    @ApiOperation({
        summary: 'setEnv',
        description: 'Updates .env configuration',
    })
    @ApiOkResponse()
    async setEnv(@Body() request: Record<string, string>): Promise<void> {
        await this.service.setEnv(request);
    }

    @Post(BazaE2eEndpointPaths.resetEnv)
    @ApiOperation({
        summary: 'resetEnv',
        description: 'Resets .env configuration',
    })
    @ApiOkResponse()
    async resetEnv(): Promise<void> {
        await this.service.resetEnv();
    }
}
