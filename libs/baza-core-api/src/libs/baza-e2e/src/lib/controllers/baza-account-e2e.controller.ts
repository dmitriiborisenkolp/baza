import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaAccountE2eEndpoint,
    BazaAccountE2eEndpointPaths,
    BazaAccountE2eChangeEmailConfirmationExpiryStrategyRequest,
    BazaAccountE2eExpireConfirmationTokenRequest,
    BazaAccountE2eGetConfirmationTokenRequest,
    BazaAccountE2eConfirmationTokenDto,
} from '@scaliolabs/baza-core-shared';
import { BazaE2eGuard } from '../guards/baza-e2e.guard';
import { BazaAccountE2eService } from '../services/baza-account-e2e.service';

@Controller()
@ApiTags(CoreOpenApiTags.BazaE2e)
@UseGuards(BazaE2eGuard)
export class BazaAccountE2eController implements BazaAccountE2eEndpoint {
    constructor(private readonly service: BazaAccountE2eService) {}

    @Post(BazaAccountE2eEndpointPaths.changeEmailConfirmationExpiryStrategy)
    @ApiOperation({
        summary: 'Change strategy',
        description: 'Will change the default value for email confirmation strategy',
    })
    @ApiOkResponse()
    async changeEmailConfirmationExpiryStrategy(
        @Body() request: BazaAccountE2eChangeEmailConfirmationExpiryStrategyRequest,
    ): Promise<void> {
        await this.service.changeEmailConfirmationExpiryStrategy(request.strategy);
    }

    @Post(BazaAccountE2eEndpointPaths.expireConfirmationToken)
    @ApiOperation({
        summary: 'Expire confirmation token',
        description: 'Will expire the newly generate confirmation token',
    })
    @ApiOkResponse()
    async expireConfirmationToken(@Body() request: BazaAccountE2eExpireConfirmationTokenRequest): Promise<void> {
        await this.service.expireConfirmationToken(request.token);
    }

    @Post(BazaAccountE2eEndpointPaths.getAccountConfirmationToken)
    @ApiOperation({
        summary: 'Return email Confirmation token',
        description:
            'Will return the confirmation token which is sent to the user by email, since we cant access to email we need to fetch it from db',
    })
    @ApiOkResponse()
    async getAccountConfirmationToken(
        @Body() request: BazaAccountE2eGetConfirmationTokenRequest,
    ): Promise<BazaAccountE2eConfirmationTokenDto> {
        return await this.service.getAccountConfirmationToken(request.email);
    }
}
