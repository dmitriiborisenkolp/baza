import { Inject, Injectable } from '@nestjs/common';
import { BazaLogger } from '../../../../baza-logger/src';

import {
    BazaAccountRepository,
    BazaAccountConfirmEmailRequestRepository,
    BAZA_ACCOUNT_CONFIG,
    BazaAccountConfig,
} from '../../../../baza-account/src';

import * as moment from 'moment';
import { ConfirmationEmailExpiryResendStrategy } from '@scaliolabs/baza-core-shared';
@Injectable()
export class BazaAccountE2eService {
    constructor(
        private readonly logger: BazaLogger,
        private readonly accountRepository: BazaAccountRepository,
        private readonly accountConfirmEmailRequestRepository: BazaAccountConfirmEmailRequestRepository,
        @Inject(BAZA_ACCOUNT_CONFIG) private readonly moduleConfig: BazaAccountConfig,
    ) {}

    async changeEmailConfirmationExpiryStrategy(strategy: ConfirmationEmailExpiryResendStrategy): Promise<void> {
        this.logger.debug(`[BazaAccountE2eService] change Email Confirmation Expiry Strategy`);
        this.moduleConfig.confirmationEmailExpiryResendStrategy = strategy;
    }

    async expireConfirmationToken(token: string): Promise<void> {
        this.logger.debug(`[BazaAccountE2eService] expire Confirmation Token`);

        const confirmRequest = await this.accountConfirmEmailRequestRepository.repository.findOne({
            token,
        });

        await this.accountConfirmEmailRequestRepository.repository.update(
            {
                token,
            },
            {
                dateExpiresAt: moment(confirmRequest.dateCreatedAt).subtract({ hour: 2 }),
            },
        );
    }

    async getAccountConfirmationToken(email: string): Promise<{ token: string }> {
        this.logger.debug(`[BazaAccountE2eService] get Account Confirmation Token`);

        const account = await this.accountRepository.getActiveAccountWithEmail(email);

        const confirmRequest = await this.accountConfirmEmailRequestRepository.findConfirmationByAccount(account);

        return {
            token: confirmRequest?.token ?? undefined,
        };
    }
}
