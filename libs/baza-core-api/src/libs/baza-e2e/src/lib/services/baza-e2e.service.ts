import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { RedisService } from '@liaoliaots/nestjs-redis';
import {
    awaitTimeout,
    BazaE2eDisableFeatureRequest,
    BazaE2eEnable2FAForAccountRolesRequest,
    BazaE2eEnableFeatureRequest,
    BazaE2eMailDto,
} from '@scaliolabs/baza-core-shared';
import { BazaLogger } from '../../../../baza-logger/src';
import { BazaRegistryService } from '../../../../baza-registry/src';
import { MailMemoryTransport } from '../../../../baza-mail/src';
import { BAZA_AUTH_E2E_HELPERS_INJECTS } from '../../../../baza-auth/src';
import { BazaFeatureService } from '../../../../baza-feature/src';
import { EnvService } from '../../../../baza-env/src';
import * as process from 'process';
import { BazaCoreAuthUniqueUsersFixture, BazaCoreAuthUsersFixture } from '../../../../baza-e2e-fixtures/src';

@Injectable()
export class BazaE2eService {
    private readonly _env = this.env.all;

    constructor(
        private readonly env: EnvService,
        private readonly logger: BazaLogger,
        private readonly connection: Connection,
        private readonly redis: RedisService,
        private readonly mailMemory: MailMemoryTransport,
        private readonly registry: BazaRegistryService,
        private readonly features: BazaFeatureService,
    ) {}

    async flush(): Promise<void> {
        this.logger.debug(`[BazaE2eService] Flush all`);

        await this.resetEnv();
        await this.flushDatabase();
        await this.flushRedis();
        await this.flushMailbox();
        await this.resetFeatures();

        BAZA_AUTH_E2E_HELPERS_INJECTS.enable2FAForAccountRoles = [];

        BazaCoreAuthUsersFixture.reset();
        BazaCoreAuthUniqueUsersFixture.reset();
    }

    async flushDatabase(): Promise<void> {
        this.logger.debug(`[BazaE2eService] Flush Database`);

        await awaitTimeout(1500);

        await this.connection.synchronize(true);
        await this.registry.loadSchema();
    }

    async flushRedis(): Promise<void> {
        this.logger.debug(`[BazaE2eService] Flush Redis`);

        await this.redis.getClient().flushall();
    }

    async flushMailbox(): Promise<void> {
        this.logger.debug(`[BazaE2eService] Flush Mailbox`);

        await this.mailMemory.clear();
    }

    async resetFeatures(): Promise<void> {
        this.logger.debug(`[BazaE2eService] Reset features`);

        this.features.reset();
    }

    async mailbox(): Promise<Array<BazaE2eMailDto>> {
        const pool = this.mailMemory.pool;

        pool.reverse();

        return pool.map((mail) => ({
            to: mail.to.map((s) => s.email),
            subject: mail.subject,
            messageBodyText: mail.bodyText,
            messageBodyHtml: mail.bodyHtml,
            attachments: mail.attachments,
        }));
    }

    async enable2FAForAccountRoles(request: BazaE2eEnable2FAForAccountRolesRequest): Promise<void> {
        BAZA_AUTH_E2E_HELPERS_INJECTS.enable2FAForAccountRoles = request.accountRoles;
    }

    async enableFeature(request: BazaE2eEnableFeatureRequest): Promise<void> {
        this.features.enable(request.feature);
    }

    async disableFeature(request: BazaE2eDisableFeatureRequest): Promise<void> {
        this.features.disable(request.feature);
    }

    async setEnv(request: Record<string, string>): Promise<void> {
        Object.assign(process.env, request);
    }

    async resetEnv(): Promise<void> {
        Object.assign(process.env, this._env);
    }
}
