import { BazaE2eErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class BazaE2eIsNotTestEnvironmentException extends BazaAppException {
    constructor() {
        super(BazaE2eErrorCodes.BazaE2eIsNotTestEnvironment, 'You\'re not able to run E2E helpers at non-test environments', HttpStatus.BAD_REQUEST);
    }
}
