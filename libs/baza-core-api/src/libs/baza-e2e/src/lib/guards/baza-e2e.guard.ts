import { CanActivate, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { BazaE2eIsNotTestEnvironmentException } from '../exceptions/baza-e2e-is-not-test-environment.exception';
import { EnvService } from '../../../../baza-env/src';
import { BazaCommonEnvironments } from '../../../../baza-common/src';

@Injectable()
export class BazaE2eGuard implements CanActivate {
    constructor(
        private readonly env: EnvService,
    ) {}

    canActivate(): boolean | Promise<boolean> | Observable<boolean> {
        const dbName = this.env.getAsString<BazaCommonEnvironments.TypeOrmEnvironment>('DB_NAME');

        if (! this.env.isTestEnvironment || ! dbName.includes('e2e')) {
            throw new BazaE2eIsNotTestEnvironmentException();
        }

        return true;
    }
}
