import { Module } from '@nestjs/common';
import { BazaE2eController } from './controllers/baza-e2e.controller';
import { BazaAccountE2eController } from './controllers/baza-account-e2e.controller';
import { BazaE2eService } from './services/baza-e2e.service';
import { BazaAccountE2eService } from './services/baza-account-e2e.service';
import { BazaE2eGuard } from './guards/baza-e2e.guard';
import { BazaRegistryApiModule } from '../../../baza-registry/src';

@Module({
    controllers: [BazaE2eController, BazaAccountE2eController],
    imports: [BazaRegistryApiModule],
    providers: [BazaE2eService, BazaAccountE2eService, BazaE2eGuard],
    exports: [BazaE2eGuard],
})
export class BazaE2eApiModule {}
