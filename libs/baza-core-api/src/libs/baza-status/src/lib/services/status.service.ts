import { Injectable } from '@nestjs/common';
import type { StatusDto } from '@scaliolabs/baza-core-shared';
import { StatusLive } from '@scaliolabs/baza-core-shared';
import { VersionService } from '../../../../baza-version/src';
import { EnvService } from '../../../../baza-env/src';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { LogStartCounterModel } from '../../../../../bundle/src/lib/models/log-start-counter.model';
import * as moment from 'moment';

@Injectable()
export class StatusService {
    constructor(private readonly env: EnvService, private readonly version: VersionService, private readonly redis: RedisService) {}

    async status(clientIp: string): Promise<StatusDto> {
        const apiLastStartedDate = await this.redis.getClient().get(LogStartCounterModel.BAZA_API_LAST_STARTED_DATE);
        const apiUpTime = moment.duration(moment().diff(apiLastStartedDate));

        return {
            live: StatusLive.OK,
            memoryUsageMB: Math.ceil(process.memoryUsage().heapUsed / 1024 / 1024),
            clientIp,
            apiVersion: this.version.current,
            supportedApiVersions: this.version.supported,
            gitVersion: this.env.getAsString('GIT_VERSION', {
                defaultValue: 'NOT_CONFIGURED',
            }),
            bazaEnvironment: this.env.current,
            isProductionEnvironment: this.env.isProductionEnvironment,
            isE2EEnvironment: this.env.isTestEnvironment,
            startsCounter: +(await this.redis.getClient().get(LogStartCounterModel.BAZA_API_STARTS_COUNTER)),
            lastStartedDate: apiLastStartedDate,
            upTime: {
                days: apiUpTime.days(),
                hours: apiUpTime.hours(),
                minutes: apiUpTime.minutes(),
                seconds: apiUpTime.seconds(),
                milliseconds: apiUpTime.milliseconds(),
            },
        };
    }
}
