import { Module } from '@nestjs/common';
import { StatusService } from './services/status.service';
import { StatusController } from './controllers/status.controller';

@Module({
    controllers: [
        StatusController,
    ],
    providers: [
        StatusService,
    ],
})
export class BazaStatusModule {
}
