import { Controller, Get } from '@nestjs/common';
import type { StatusEndpoint, BazaStatusIndexResponse } from '@scaliolabs/baza-core-shared';
import { StatusService } from '../services/status.service';
import { StatusDto, BazaStatusEndpointPaths } from '@scaliolabs/baza-core-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { IpAddress } from '../../../../baza-common/src';

@ApiTags(CoreOpenApiTags.BazaStatus)
@Controller()
export class StatusController implements StatusEndpoint {
    constructor(private readonly service: StatusService) {}

    @Get(BazaStatusEndpointPaths.index)
    @ApiOperation({
        summary: 'status',
        description: 'Display live status of API',
    })
    @ApiOkResponse({
        type: StatusDto,
    })
    async index(@IpAddress() ip: string): Promise<BazaStatusIndexResponse> {
        return this.service.status(ip);
    }
}
