import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { HealthService } from '../services/health.service';
import { BazaHealthCheckEndpointPaths, BazaHealthCheckResponse, HealthCheckEndpoint, HealthDto } from '@scaliolabs/baza-core-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { HealthCheckResponseMapper } from '../mappers/health-check-response.mapper';
import { HealthCheckInterceptor } from '../interceptors/health-check.interceptor';

@ApiTags(CoreOpenApiTags.BazaHealthCheck)
@UseInterceptors(HealthCheckInterceptor)
@Controller()
export class HealthController implements HealthCheckEndpoint {
    constructor(private readonly service: HealthService, private readonly mapper: HealthCheckResponseMapper) {}

    @Get(BazaHealthCheckEndpointPaths.check)
    @ApiOperation({
        summary: 'health-check',
        description: 'Display Application Health-Check',
    })
    @ApiOkResponse({
        type: HealthDto,
    })
    async check(): Promise<BazaHealthCheckResponse> {
        return this.mapper.healthCheckResponseToDto(await this.service.check());
    }
}
