import { HttpStatus, Injectable } from '@nestjs/common';
import { HealthCheckResult } from '@nestjs/terminus';
import { HealthDto } from '@scaliolabs/baza-core-shared';
import { HealthCheckStatus } from '../services/health.service';

@Injectable()
export class HealthCheckResponseMapper {
    healthCheckResponseToDto(response: HealthCheckResult): HealthDto {
        return {
            status: this.getResponseStatusCode(response),
            info: response.info,
            error: response.error,
        };
    }

    private getResponseStatusCode(response: HealthCheckResult) {
        let httpStatus = HttpStatus.OK;

        for (const key in response.info) {
            if (Object.prototype.hasOwnProperty.call(response.info, key)) {
                if (response.info[key].status == HealthCheckStatus.Down) {
                    httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
                }
            }
        }

        return httpStatus;
    }
}
