import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { BazaHealthCheckResponse } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { Response as ExpressResponse } from 'express';

export class HealthCheckInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler<BazaHealthCheckResponse>): Observable<void> {
        return next.handle().pipe(
            map((data: BazaHealthCheckResponse) => {
                const ctx = context.switchToHttp();
                const response: ExpressResponse = ctx.getResponse();
                response.status(data.status).send(data);
            }),
        );
    }
}
