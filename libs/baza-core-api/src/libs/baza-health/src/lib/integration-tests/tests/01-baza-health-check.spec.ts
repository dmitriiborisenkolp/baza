import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, healthCheckItems, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaHealthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { HttpStatus } from '@nestjs/common';

describe('@scaliolabs/baza-core-api/baza-health/integration-tests/01-baza-health-check.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const bazaHealthNodeAccess = new BazaHealthNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will get health check result successfully', async () => {
        const response = await bazaHealthNodeAccess.check();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(Object.keys(response)).toEqual(['status', 'info', 'error']);
        expect(Object.keys(response.info)).toEqual(Object.values(healthCheckItems));
        expect(response.status).toBe(HttpStatus.OK);

        for (const item of Object.values(healthCheckItems)) {
            expect(response.info[String(item)].status).toBe('up');
        }
    });
});
