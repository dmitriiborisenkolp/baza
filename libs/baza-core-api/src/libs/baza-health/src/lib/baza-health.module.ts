import { Module } from '@nestjs/common';
import { HealthService } from './services/health.service';
import { HealthController } from './controllers/health.controller';
import { TerminusModule } from '@nestjs/terminus';
import { BazaEnvModule } from '../../../baza-env/src';
import { DirectoryWritableHealthIndicator } from './services/directory-writable-health-indicator.service';
import { KafkaHealthIndicator } from './services/kafka-health-indicator.service';
import { HealthCheckResponseMapper } from './mappers/health-check-response.mapper';

@Module({
    controllers: [HealthController],
    providers: [HealthService, DirectoryWritableHealthIndicator, KafkaHealthIndicator, HealthCheckResponseMapper],
    imports: [TerminusModule, BazaEnvModule],
})
export class BazaHealthModule {}
