import { Injectable, Scope } from '@nestjs/common';
import {
    HealthCheckResult,
    HealthCheckService,
    HttpHealthIndicator,
    MemoryHealthIndicator,
    TypeOrmHealthIndicator,
} from '@nestjs/terminus';
import { EnvService } from '../../../../baza-env/src';
import { BazaCommonEnvironments } from '../../../../baza-common/src/lib/models/common-environments';
import { DirectoryWritableHealthIndicator } from './directory-writable-health-indicator.service';
import { KafkaHealthIndicator } from './kafka-health-indicator.service';
import { healthCheckItems } from '@scaliolabs/baza-core-shared';
import { BazaLogger } from '../../../../baza-logger/src';

export enum HealthCheckStatus {
    Up = 'up',
    Down = 'down',
}

@Injectable({
    scope: Scope.REQUEST,
})
export class HealthService {
    private healcheckErrors = {};

    constructor(
        private readonly health: HealthCheckService,
        private readonly httpHealthIndicator: HttpHealthIndicator,
        private readonly memoryHealthIndicator: MemoryHealthIndicator,
        private readonly typeOrmHealthIndicator: TypeOrmHealthIndicator,
        private readonly env: EnvService<BazaCommonEnvironments.HealthCheckEnvironment>,
        private readonly directoryWritableHealthIndicator: DirectoryWritableHealthIndicator,
        private readonly kafkaHealthIndicator: KafkaHealthIndicator,
        private readonly logger: BazaLogger,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    async check(): Promise<HealthCheckResult> {
        const BAZA_APP_API_URL = 'http://127.0.0.1:3000';

        const docsUrl = `${BAZA_APP_API_URL}/docs`;
        const reDocUrl = `${BAZA_APP_API_URL}/redoc`;
        const tmpDirPath = `${process.cwd()}/tmp/`;
        const memoryHeapSize =
            this.env.getAsNumeric<BazaCommonEnvironments.HealthCheckEnvironment>('BAZA_HEALTHCHECK_MEMORY_HEAP_MAX_MBYTES') *
                1024 *
                1024 /* Convert To Bytes */ || 2 * 1024 * 1024 * 1024; // Default : 2GB
        const memoryRssSize =
            this.env.getAsNumeric<BazaCommonEnvironments.HealthCheckEnvironment>('BAZA_HEALTHCHECK_MEMORY_RSS_MAX_MBYTES') *
                1024 *
                1024 /* Convert To Bytes */ || 2 * 1024 * 1024 * 1024; // Default : 2GB

        const result = await this.health.check([
            () =>
                this.httpHealthIndicator
                    .pingCheck(healthCheckItems.baza_docs, docsUrl)
                    .catch((err) => this.handleHealthCheckExceptions(err, healthCheckItems.baza_docs)),
            () =>
                this.httpHealthIndicator
                    .pingCheck(healthCheckItems.baza_redoc, reDocUrl)
                    .catch((err) => this.handleHealthCheckExceptions(err, healthCheckItems.baza_redoc)),
            () =>
                this.directoryWritableHealthIndicator
                    .pingCheck(healthCheckItems.writable_tmp_dir, tmpDirPath)
                    .catch((err) => this.handleHealthCheckExceptions(err, healthCheckItems.writable_tmp_dir)),
            () =>
                this.memoryHealthIndicator
                    .checkHeap(healthCheckItems.memory_heap, memoryHeapSize)
                    .catch((err) => this.handleHealthCheckExceptions(err, healthCheckItems.memory_heap)),
            () =>
                this.memoryHealthIndicator
                    .checkRSS(healthCheckItems.memory_rss, memoryRssSize)
                    .catch((err) => this.handleHealthCheckExceptions(err, healthCheckItems.memory_rss)),
            () =>
                this.typeOrmHealthIndicator
                    .pingCheck(healthCheckItems.database)
                    .catch((err) => this.handleHealthCheckExceptions(err, healthCheckItems.database)),
            () =>
                this.kafkaHealthIndicator
                    .pingCheck(healthCheckItems.kafka)
                    .catch((err) => this.handleHealthCheckExceptions(err, healthCheckItems.kafka)),
        ]);

        return {
            ...result,
            error: this.healcheckErrors,
        };
    }

    /**
     *
     * @param err
     * @param healthCheckItem
     * @returns
     * will handle the thrown exception , basically added to prevent whole request crash
     */
    private handleHealthCheckExceptions(err: Error, healthCheckItem: healthCheckItems) {
        this.logger.error(err);

        this.healcheckErrors[healthCheckItem] = err;

        return {
            [healthCheckItem]: {
                status: HealthCheckStatus.Down,
            },
        };
    }
}
