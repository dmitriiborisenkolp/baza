import { HealthCheckError, HealthIndicator, HealthIndicatorResult } from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';
import { EnvService } from '../../../../baza-env/src';
import { BazaCommonEnvironments } from '../../../../baza-common/src/lib/models/common-environments';
import { Kafka } from 'kafkajs';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';

@Injectable()
export class KafkaHealthIndicator extends HealthIndicator {
    private readonly kafkaClient: Kafka;

    constructor(private readonly env: EnvService<BazaCommonEnvironments.KafkaEnvironment>) {
        super();

        const kafkaEnabled = this.env.getAsBoolean<BazaCommonEnvironments.KafkaEnvironment>('KAFKA_ENABLED');

        if (kafkaEnabled) {
            const kafkaClientId = this.env.getAsString<BazaCommonEnvironments.KafkaEnvironment>('KAFKA_CLIENT_ID');
            const kafkaClientBrokers = this.env.getAsString<BazaCommonEnvironments.KafkaEnvironment>('KAFKA_CLIENT_BROKERS');

            const UNIQUE_HEX_ID_LENGTH = 8;
            const uniqueClientId = generateRandomHexString(UNIQUE_HEX_ID_LENGTH);

            this.kafkaClient = new Kafka({
                clientId: `${kafkaClientId}-${uniqueClientId}`,
                brokers: kafkaClientBrokers.split(','),
            });
        }
    }

    /**
     * Checks if given directory is writable or not.
     * @param healthKey
     * @returns {Promise<HealthIndicatorResult>}
     */
    async pingCheck(healthKey: string): Promise<HealthIndicatorResult> {
        try {
            if (!this.kafkaClient) return null;

            const producer = this.kafkaClient.producer();
            await producer.connect();
            producer.disconnect();

            return this.getStatus(healthKey, true);
        } catch (e) {
            throw new HealthCheckError(healthKey, this.getStatus(healthKey, false));
        }
    }
}
