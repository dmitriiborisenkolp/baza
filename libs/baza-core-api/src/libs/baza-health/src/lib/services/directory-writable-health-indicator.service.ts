import { HealthCheckError, HealthIndicator, HealthIndicatorResult } from '@nestjs/terminus';
import { Injectable } from '@nestjs/common';
import * as fs from 'fs';

@Injectable()
export class DirectoryWritableHealthIndicator extends HealthIndicator {
    constructor() {
        super();
    }

    /**
     * Checks if given directory is writable or not.
     * @param healthKey
     * @param dirPath
     * @returns {Promise<HealthIndicatorResult>}
     */
    async pingCheck(healthKey: string, dirPath: string): Promise<HealthIndicatorResult> {
        try {
            fs.accessSync(dirPath, fs.constants.W_OK);
            return this.getStatus(healthKey, true);
        } catch (e) {
            throw new HealthCheckError(healthKey, this.getStatus(healthKey, false));
        }
    }
}
