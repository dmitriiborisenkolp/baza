import { Module } from '@nestjs/common';
import { BazaSessionLockService } from './services/baza-session-lock.service';

@Module({
    providers: [BazaSessionLockService],
    exports: [BazaSessionLockService],
})
export class BazaCoreSessionLockApiModule {}
