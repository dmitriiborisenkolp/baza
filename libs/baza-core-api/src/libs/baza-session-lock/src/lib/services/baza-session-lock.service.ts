import { Injectable } from '@nestjs/common';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { BazaSessionLockedException } from '../exceptions/baza-session-locked.exception';
import { BazaSessionUpdateIdMismatchException } from '../exceptions/baza-session-update-id-mismatch.exception';

interface Payload {
    prefix: string;
    accountId: number;
    sessionTtlSeconds: number;
    sessionUpdateId?: string;
}

const redisKeyFactory = (payload: Payload) => `${payload.prefix}_SESSION_LOCK_${payload.accountId}`;
const redisUpdateIdKeyFactory = (payload: Payload) => `${payload.prefix}_SESSION_UPDATE_ID_${payload.accountId}`;

@Injectable()
export class BazaSessionLockService {
    constructor(private readonly redis: RedisService) {}

    async run<T>(
        callback: () => Promise<T>,
        payload: Payload,
    ): Promise<{
        result: T;
        sessionUpdateId: string;
    }> {
        if (payload.sessionUpdateId) {
            await this.validateSessionUpdateId(payload);
        }

        await this.validateSessionLock(payload);
        await this.lockSession(payload);

        try {
            const result = await callback();
            const sessionUpdateId = await this.setSessionUpdateId(payload);

            return {
                result,
                sessionUpdateId,
            };
        } finally {
            await this.unlockSession(payload);
        }
    }

    async lockSession(payload: Payload): Promise<void> {
        const client = this.redis.getClient();

        await client.set(redisKeyFactory(payload), '1', 'EX', payload.sessionTtlSeconds);
    }

    async unlockSession(payload: Payload): Promise<void> {
        const client = this.redis.getClient();

        await client.del(redisKeyFactory(payload));
    }

    async validateSessionLock(payload: Payload): Promise<void> {
        const client = this.redis.getClient();

        const exists = await client.get(redisKeyFactory(payload));

        if (exists === '1') {
            throw new BazaSessionLockedException();
        }
    }

    async setSessionUpdateId(payload: Payload): Promise<string> {
        const client = this.redis.getClient();
        const updateId = generateRandomHexString(8);

        await client.set(redisUpdateIdKeyFactory(payload), updateId, 'EX', payload.sessionTtlSeconds);

        return updateId;
    }

    async validateSessionUpdateId(payload: Payload): Promise<void> {
        const client = this.redis.getClient();

        const updateId = await client.get(redisUpdateIdKeyFactory(payload));

        if (updateId !== payload.sessionUpdateId) {
            throw new BazaSessionUpdateIdMismatchException();
        }
    }
}
