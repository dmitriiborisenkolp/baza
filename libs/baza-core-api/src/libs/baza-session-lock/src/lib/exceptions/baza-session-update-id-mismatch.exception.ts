import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaCommonErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaSessionUpdateIdMismatchException extends BazaAppException {
    constructor() {
        super(
            BazaCommonErrorCodes.BazaSessionUpdateIdMismatch,
            'Session is started on different device',
            HttpStatus.CONFLICT,
        );
    }
}
