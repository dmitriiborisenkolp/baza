import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaCommonErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaSessionLockedException extends BazaAppException {
    constructor() {
        super(
            BazaCommonErrorCodes.BazaSessionIsLocked,
            'Session is locked',
            HttpStatus.CONFLICT,
        );
    }
}
