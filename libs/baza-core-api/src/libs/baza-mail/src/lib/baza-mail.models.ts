import { Application, BazaMailTemplate, ProjectLanguage } from '@scaliolabs/baza-core-shared';

/**
 * Supported Mail Transport Types
 */
export enum MailTransportType {
    SMTP = 'smtp',
    Mailgun = 'mailgun',
    Memory = 'memory',
    SendPulse = 'sendpulse',
    Customerio = 'customerio',
}

/**
 * Injection Token for Mail API Module Configuration
 */
export const MAIL_MODULE_CONFIG = Symbol();

export enum MAILGUN_API_RESOURCES {
    Bounces = 'bounces',
}

/**
 * Mail API Module Configuration
 */
export interface MailModuleConfig {
    /**
     * Enables Debug Log
     */
    debug: boolean;

    /**
     * Base Path for Template files
     * Usually it's apps/i18n/en/mail-templates
     */
    templatePath: string;

    /**
     * Partials which could be imported and used in handlebars templates
     */
    templatePartials: Array<{
        name: string;
        path: string;
    }>;

    /**
     * Transport which will be used for sending emails
     * @see MailTransportType
     * @see MailTransportSMTPConfig
     * @see MailTransportMailgunConfig
     * @see SendPulseConfig
     * @see SendMailCustomerIoConfig
     */
    transport: MailTransportType;

    /**
     * Default sender information for emails (MAIL_FROM_NAME, MAIL_FROM_ADDRESS)
     */
    sender: {
        name: string;
        email: string;
    };

    /**
     * List of domains which are allowed for sending emails
     * If contains ['*'], all recipients will be allowed
     */
    allowedMailDomains: Array<string>;

    /**
     * Application Map
     * Usually IOS/ Android-identified requests should be handled like Web application
     * @param app
     */
    getApplicationForEmail: (app: Application) => Application;

    /**
     * Enables Template Validation
     * When enabled, API will check that all template variables which are used in send mail request has full documentation
     */
    enableTemplateValidation?: boolean;

    /**
     * SMTP Configuration
     * Mandatory for transport: MailTransportType.SMTP
     */
    smtpConfig?: MailTransportSMTPConfig;

    /**
     * Mailgun Configuration
     * Mandatory for transport: MailTransportType.Mailgun
     */
    mailgunConfig?: MailTransportMailgunConfig;

    /**
     * SendPulse Configuration
     * Mandatory for transport: MailTransportType.SendPulse
     */
    sendPulseConfig?: SendPulseConfig;

    /**
     * Customer.IO Configuration
     * Mandatory for transport: MailTransportType.Customerio
     */
    customerIoConfig?: SendMailCustomerIoConfig;
}

/**
 * Mail API Module Configuration - SMTP
 */
export interface MailTransportSMTPConfig {
    host: string;
    port: number;
    username?: string;
    password?: string;
    secure?: boolean;
}

/**
 * Mail API Module Configuration - Mailgun
 */
export interface MailTransportMailgunConfig {
    domain: string;
    apiKey: string;
}

/**
 * Mail API Module Configuration - SendPulse
 */
export interface SendPulseConfig {
    clientId: string;
    clientSecret: string;
}

/**
 * Mail API Module Configuration - CustomerIO
 */
export interface SendMailCustomerIoConfig {
    appKey: string;
    region?: string;
    useCustomerIoTemplates?: boolean;
    customerIoLinksBootstrap?: Array<SendMailCustomerIoBootstrap>;
}

export interface SendMailCustomerIoBootstrap {
    /**
     * Baza Template Name
     */
    name: string;

    /**
     * ID of Customer Transaction Email
     * (Displayed on "Transactional" page)
     */
    customerIoId: number;
}

/**
 * Send Mail Request
 * @deprecated
 */
export class SendMailServiceRequest<T = any> {
    app: Application;
    language?: ProjectLanguage;
    to: Array<SendMailRecipient>;
    subject: string;
    body: SendMailBody;
    uniqueId?: string;
    variables: () => T;
    attachments?: Array<SendMailAttachmentRequest>;
}

/**
 * Send Mail Request (using Mail Templates)
 */
export class BazaSendMailServiceRequest<T = unknown> {
    /**
     * Mail Template Name (Id)
     */
    name: string;

    /**
     * Recipients
     */
    to: Array<SendMailRecipient>;

    /**
     * Subject
     */
    subject: string;

    /**
     * Unique ID. Unique ID is used to prevent duplicate mails to be sent
     */
    uniqueId?: string;

    /**
     * Callback which will returns variables for Mail Template
     */
    variables: () => T;

    /**
     * Attachments for Email
     */
    attachments?: Array<SendMailAttachmentRequest>;
}

/**
 * Recipients used for Send Mail Request
 */
export class SendMailRecipient {
    name?: string;
    email: string;
    cc?: Array<SendMailCCRecipient>;
}

/**
 * CC Recipient used for Send Mail Request
 */
export class SendMailCCRecipient {
    name?: string;
    email: string;
}

/**
 * Body used for Send Mail Request
 */
export interface SendMailBody {
    asTextSourcePath: string;
    asHtmlSourcePath: string;
}

/**
 * Attachments used for Send Mail Request
 */
export class SendMailAttachmentRequest {
    filename: string;
    content: string;
    contentType: string;
}

/**
 * Defines source which will be used for reading or using Templates
 */
export type IMailTransportMode = 'file-templates' | 'external-service-templates';

/**
 * Interface which should be implement by Transports used for MailService
 */
export interface IMailTransport {
    /**
     * Returns strategy used for sending emails
     * IMailTransport can works with 2 different straregies:
     *  - `file-templates` - using Handlebars files stored in repostory
     *  - `external-service-templates` - using External Services like Customer.IO Transaction Email Templates
     */
    mode: IMailTransportMode;

    /**
     * Should returns true if Transport can be used to send every known Mail Template
     */
    isReadyToUse: Promise<boolean>;

    /**
     * Should returns true if 2FA should be disable because it's not configured
     * yet to send 2FA-related emails
     */
    shouldDisable2Fa: Promise<boolean>;

    /**
     * Sends email using the Transport
     * @param request
     */
    sendMail(request: SendMailViaTransportServiceRequest): Promise<void>;
}

/**
 * Request object for Send Mail
 * Used by IMailTransport implementations
 */
export class SendMailViaTransportServiceRequest {
    /**
     * Reference Mail Template Definition which was used for Request
     */
    template: BazaMailTemplate;

    /**
     * Recipients (TO, CC)
     */
    to: Array<SendMailRecipient>;
    /**
     * Subject for Email
     * Subject will not be set if 'external-service-templates' is used as Transport Mode
     */
    subject?: string;

    /**
     * Body (for Text variant of Email) for Email
     * Body Text will not be set if 'external-service-templates' is used as Transport Mode
     */
    bodyText?: string;

    /**
     * Body (for HTML variant of Email) for Email
     * Body HTML will not be set if 'external-service-templates' is used as Transport Mode
     */
    bodyHtml?: string;

    /**
     * Variables which could be used both for Subject or Body / External Service Template
     */
    variables: Record<string, unknown>;

    /**
     * Attachments which should be associated with Email
     */
    attachments?: Array<SendMailAttachmentRequest>;
}

/**
 * Customer.IO allowed regions
 */
export enum CustomerIoRegions {
    EU = 'EU',
    US = 'US',
}

/**
 * Converts Recipients to String
 * @param input
 */
export function mailRecipientsToTOField(input: Array<SendMailRecipient | SendMailCCRecipient>): string {
    return input.map((recipient) => (recipient.name ? `"${recipient.name}" <${recipient.email}>` : recipient.email)).join(',');
}

/**
 * Returns default application for mails
 * Application is used to choose template wrapper (i.e. first directory in list)
 * @param app
 */
export function defaultApplicationForEmail(app: Application): Application {
    return [Application.ANDROID, Application.IOS, Application.WEB].includes(app) ? Application.WEB : app;
}
