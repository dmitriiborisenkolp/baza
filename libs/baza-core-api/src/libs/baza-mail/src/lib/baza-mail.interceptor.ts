import { BazaSendMailServiceRequest, IMailTransport, SendMailViaTransportServiceRequest } from './baza-mail.models';

/**
 * Interceptor Result
 * If `ignore`, Send Mail request will be ignored with `WARN` message in logs
 * If `fail`, Send Mail request will be ignored and Exception will be throw
 */
export type BazaMailInterceptorMode = 'ignore' | 'fail';

/**
 * Payload for Baza Mail Interceptor
 * You can modify input payload during interception
 * Interceptor should returns a `true` to allow sending email. False result or exceptions will result to not triggering
 * sending email by mail transports
 */
export interface BazaMailInterceptorPayload {
    request: BazaSendMailServiceRequest;
    transport: IMailTransport;
    mailTransportRequest: SendMailViaTransportServiceRequest;
    variables: Record<string, unknown>;
}

/**
 * Baza Mail Interceptor
 * Baza allows to intercept emails, allow/disallow sending emails or modify request or template variables
 */
export interface BazaMailInterceptor {
    /**
     * Defines how failed / exception result will be processed
     * By default it's 'ignore'
     */
    mode?: BazaMailInterceptorMode;

    /**
     * Intercepts Send Mail Request
     * @param payload
     */
    intercept(payload: BazaMailInterceptorPayload): Promise<boolean>;
}
