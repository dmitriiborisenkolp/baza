import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess, BazaMailNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaCoreMail, BazaCoreMailTags } from '../../../../../../constants/baza-core.mail';

describe('@baza-core-api/baza-mail/integration-tests/tests/001-baza-mail.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessMail = new BazaMailNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will fetch mail templates and it will contains all mail templates defined for baza-core-api package', async () => {
        const response = await dataAccessMail.mailTemplates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(Array.isArray(response)).toBeTruthy();

        for (const name of Object.values(BazaCoreMail)) {
            expect(response.find((next) => next.name === name)).toBeDefined();
        }
    });

    it('will fetch mail templates by Tag', async () => {
        const response = await dataAccessMail.mailTemplatesOfTag(BazaCoreMailTags.BazaAuthAndAccounts);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBeGreaterThan(0);
        expect(response.every((next) => next.tag === BazaCoreMailTags.BazaAuthAndAccounts)).toBeTruthy();
    });

    it('will return mail template by name', async () => {
        const response = await dataAccessMail.mailTemplate(BazaCoreMail.BazaAccountCmsChangeEmail);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe(BazaCoreMail.BazaAccountCmsChangeEmail);
        expect(response.tag).toBe(BazaCoreMailTags.BazaCmsAuthAndAccounts);
    });

    it('will returns list of Mail Template Tags', async () => {
        const response = await dataAccessMail.mailTemplatesTags();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(Object.values(BazaCoreMailTags).every((next) => response.includes(next))).toBeTruthy();
    });
});
