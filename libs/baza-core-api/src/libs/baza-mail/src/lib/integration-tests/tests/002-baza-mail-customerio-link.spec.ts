import 'reflect-metadata';
import {
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
    BazaMailCustomerioLinksNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaCoreMail, BazaCoreMailTags } from '../../../../../../constants/baza-core.mail';

describe('@baza-core-api/baza-mail/integration-tests/tests/002-baza-mail-customerio-link.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCustomerIo = new BazaMailCustomerioLinksNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will display list of customer.io links with all mail templates of Baza Core package', async () => {
        const response = await dataAccessCustomerIo.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length > 0).toBeTruthy();

        const mapped = response.map((next) => next.template.name);

        expect(Object.values(BazaCoreMail).every((next) => mapped.includes(next))).toBeTruthy();
    });

    it('will set customerio id for mail template', async () => {
        const response = await dataAccessCustomerIo.set({
            templateName: BazaCoreMail.BazaAccountCmsChangeEmail,
            customerTemplateId: 999,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not set customerio id for unknown mail template', async () => {
        const response = await dataAccessCustomerIo.set({
            templateName: 'UnknownMailTemplate',
            customerTemplateId: 998,
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will correctly displays updates', async () => {
        const response = await dataAccessCustomerIo.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length > 0).toBeTruthy();

        const mapped = response.map((next) => next.template.name);

        expect(Object.values(BazaCoreMail).every((next) => mapped.includes(next))).toBeTruthy();

        const target = response.find((next) => next.template.name === BazaCoreMail.BazaAccountCmsChangeEmail);

        expect(target.customerIoId === 999).toBeTruthy();
    });

    it('will filter customerio links by Mail Template tag', async () => {
        const response = await dataAccessCustomerIo.listByTag(BazaCoreMailTags.BazaCmsAuthAndAccounts);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length > 0).toBeTruthy();
        expect(response.every((next) => next.template.tag === BazaCoreMailTags.BazaCmsAuthAndAccounts)).toBeTruthy();
    });

    it('will unset customerio link', async () => {
        const response = await dataAccessCustomerIo.unset({
            templateName: BazaCoreMail.BazaAccountCmsChangeEmail,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that customerio link does not exists anymore', async () => {
        const response = await dataAccessCustomerIo.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length > 0).toBeTruthy();

        const target = response.find((next) => next.template.name === BazaCoreMail.BazaAccountCmsChangeEmail);

        expect(target.customerIoId).not.toBeDefined();
    });
});
