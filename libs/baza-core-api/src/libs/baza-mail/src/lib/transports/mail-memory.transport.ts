import { Injectable } from '@nestjs/common';
import type { IMailTransport, SendMailViaTransportServiceRequest } from '../baza-mail.models';
import { IMailTransportMode } from '../baza-mail.models';

type Mails = Array<SendMailViaTransportServiceRequest>;

@Injectable()
export class MailMemoryTransport implements IMailTransport {
    private _pool: Mails = [];

    /**
     * This Transport works with file-templates only
     */
    get mode(): IMailTransportMode {
        return 'file-templates';
    }

    /**
     * Returns true if Mail Transport is ready to use
     */
    get isReadyToUse(): Promise<boolean> {
        return Promise.resolve(true);
    }

    /**
     * Returns true if 2FA should be disable because it's not configured
     * yet to send 2FA-related emails
     */
    get shouldDisable2Fa(): Promise<boolean> {
        return Promise.resolve(false);
    }

    /**
     * Returns in-memory pool
     */
    get pool(): Mails {
        return [...this._pool];
    }

    /**
     * Clean up in-memory pool
     */
    clear(): void {
        this._pool = [];
    }

    /**
     * Returns number of emails stored currently in in-memory pool
     */
    count(): number {
        return this._pool.length;
    }

    /**
     * Removes & returns last message from in-memory pool
     */
    pop(): SendMailViaTransportServiceRequest | undefined {
        return this._pool.pop();
    }

    /**
     * Put Email to in-memory pool
     * @param request
     */
    sendMail(request: SendMailViaTransportServiceRequest): Promise<void> {
        return new Promise((resolve) => {
            this._pool.push(request);

            resolve();
        });
    }
}
