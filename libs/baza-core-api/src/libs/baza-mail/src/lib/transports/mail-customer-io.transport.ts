import { Inject, Injectable } from '@nestjs/common';
import {
    CustomerIoRegions,
    IMailTransport,
    IMailTransportMode,
    MailModuleConfig,
    SendMailRecipient,
    SendMailViaTransportServiceRequest,
} from '../baza-mail.models';
import { MAIL_MODULE_CONFIG, mailRecipientsToTOField } from '../baza-mail.models';
import { APIClient, SendEmailRequest, RegionEU, RegionUS } from 'customerio-node';
import { MailCustomerioLinkService } from '../services/mail-customerio-link.service';
import { MailTemplateService } from '../services/mail-template.service';
import { BazaCoreMail } from '../../../../../constants/baza-core.mail';
import { BazaLogger } from '../../../../baza-logger/src';
import { isEmail } from 'class-validator';
import { BazaRegistryService } from '../../../../baza-registry/src';

/**
 * CustomerIO Transform for MailService
 */
@Injectable()
export class MailCustomerIoTransport implements IMailTransport {
    constructor(
        @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig,
        private readonly logger: BazaLogger,
        private readonly customerIoLinksService: MailCustomerioLinkService,
        private readonly mailTemplatesService: MailTemplateService,
        private readonly registry: BazaRegistryService,
    ) {}

    /**
     * Returns true if Mail Templates are set for every Mail Template
     */
    get isReadyToUse(): Promise<boolean> {
        return (async () => {
            const customerIoLinks = await this.customerIoLinksService.list();
            const allEmails = await this.mailTemplatesService.mailTemplates;

            return allEmails.every((next) => {
                const existingLink = customerIoLinks.find((ciol) => ciol.template.name === next.name);

                return !!existingLink?.customerIoId;
            });
        })();
    }

    /**
     * Returns true if 2FA should be disable because it's not configured
     * yet to send 2FA-related emails
     */
    get shouldDisable2Fa(): Promise<boolean> {
        return (async () => {
            const customerIoLinks = await this.customerIoLinksService.list();

            return !customerIoLinks.find((next) => next.template.name === BazaCoreMail.BazaAuthTwoFactorAuth && !!next.customerIoId);
        })();
    }

    /**
     * Mode will be selected depends on `useCustomerIoTemplates` value
     */
    get mode(): IMailTransportMode {
        return this.moduleConfig.customerIoConfig.useCustomerIoTemplates ? 'external-service-templates' : 'file-templates';
    }

    /**
     * Returns Customer.IO API Client
     * @constructor
     * @private
     */
    private get CIO_API(): APIClient {
        return new APIClient(this.moduleConfig.customerIoConfig.appKey, {
            region: this.moduleConfig.customerIoConfig.region === CustomerIoRegions.EU ? RegionEU : RegionUS,
        });
    }

    /**
     * Sends Email using CustomerIo
     * @param request
     */
    async sendMail(request: SendMailViaTransportServiceRequest): Promise<void> {
        for (const to of request.to) {
            await this.performSendMail(request, to);
        }
    }

    /**
     * Performs sending email using Customer.IO
     * Depends on `useCustomerIoTemplates` config uses diferrent strategies
     * @see sendUsingTransactionEmailTemplates
     * @see sendUsingFileTemplates
     * @param request
     * @param to
     * @private
     */
    private async performSendMail(request: SendMailViaTransportServiceRequest, to: SendMailRecipient): Promise<void> {
        return this.sendUsingTransactionEmailTemplates(request, to);
        return this.moduleConfig.customerIoConfig.useCustomerIoTemplates
            ? this.sendUsingTransactionEmailTemplates(request, to)
            : this.sendUsingFileTemplates(request, to);
    }

    /**
     * Sends Email using Customer.IO Transactional Email Templates
     * @param request
     * @param to
     * @private
     */
    private async sendUsingTransactionEmailTemplates(request: SendMailViaTransportServiceRequest, to: SendMailRecipient): Promise<void> {
        // TODO: Following option has invalid type in library and fake_bcc is enabled by default on API causing issues
        //       when users are added to CC/BCC having altered subjects with parent recipients name and email.
        //       Sent a fix on library and should be updated after following is merged and released - https://github.com/customerio/customerio-node/pull/92
        const overwriteOptions: any = {
            fake_bcc: false,
        };

        const templateId = await this.customerIoLinksService.findCustomerIoIdForTemplate(request.template.name);

        if (!templateId) {
            this.logger.warn(
                {
                    warning: 'Customer.IO template is not ready to use for sending email',
                    mailTemplate: request.template.name,
                },
                MailCustomerIoTransport.name,
            );

            return;
        }

        const mailRequest = new SendEmailRequest({
            to: mailRecipientsToTOField(request.to),
            bcc: this.getBccEmails(to),
            transactional_message_id: templateId,
            subject: request.subject,
            identifiers: (() =>
                request.to.length > 1 ? { id: mailRecipientsToTOField(request.to) } : { email: mailRecipientsToTOField(request.to) })(),
            message_data: request.variables,
            ...overwriteOptions,
        });

        if (request.attachments) {
            for (const attachment of request.attachments) {
                mailRequest.attach(attachment.filename, attachment.content);
            }
        }

        await new Promise((resolve, reject) => {
            this.CIO_API.sendEmail(mailRequest)
                .then(() => resolve(true))
                .catch((error) => reject(error));
        });
    }

    /**
     * Sends Email using File Templates
     * @private
     */
    private async sendUsingFileTemplates(request: SendMailViaTransportServiceRequest, to: SendMailRecipient): Promise<void> {
        // TODO: Following option has invalid type in library and fake_bcc is enabled by default on API causing issues
        //       when users are added to CC/BCC having altered subjects with parent recipients name and email.
        //       Sent a fix on library and should be updated after following is merged and released - https://github.com/customerio/customerio-node/pull/92
        const overwriteOptions: any = {
            fake_bcc: false,
        };

        const mailRequest = new SendEmailRequest({
            from: `${this.moduleConfig.sender.name} <${this.moduleConfig.sender.email}>`,
            to: mailRecipientsToTOField(request.to),
            bcc: this.getBccEmails(to),
            body: request.bodyHtml,
            plaintext_body: request.bodyText,
            subject: request.subject,
            identifiers: (() =>
                request.to.length > 1 ? { id: mailRecipientsToTOField(request.to) } : { email: mailRecipientsToTOField(request.to) })(),
            message_data: request.variables,
            ...overwriteOptions,
        });

        if (request.attachments) {
            for (const attachment of request.attachments) {
                mailRequest.attach(attachment.filename, attachment.content);
            }
        }

        await new Promise((resolve, reject) => {
            this.CIO_API.sendEmail(mailRequest)
                .then(() => resolve(true))
                .catch((error) => reject(error));
        });
    }

    /**
     * will get the bcc emails from registry and combine them with cc values from mail recipient object
     * @private
     */
    private getBccEmails(mailRecipient: SendMailRecipient): string | undefined {
        const bcc =
            Array.isArray(mailRecipient.cc) && mailRecipient.cc.length > 0 ? mailRecipient.cc.map((recipient) => recipient.email) : [];

        let bccRegistryValue = this.registry.getValue('bcc');

        if (bccRegistryValue) {
            bccRegistryValue = bccRegistryValue.split(',').filter((email) => !!email && isEmail(email));

            bcc.push(...bccRegistryValue);
        }

        return bcc && bcc.length ? bcc.join(',') : undefined;
    }
}
