import { Inject, Injectable } from '@nestjs/common';
import {
    IMailTransport,
    IMailTransportMode,
    MAILGUN_API_RESOURCES,
    MailModuleConfig,
    SendMailCCRecipient,
    SendMailViaTransportServiceRequest,
} from '../baza-mail.models';
import { MAIL_MODULE_CONFIG, mailRecipientsToTOField, SendMailRecipient } from '../baza-mail.models';
import { BazaLogger } from '../../../../baza-logger/src';
import { Mailgun } from 'mailgun-js';
import { BazaRegistryService } from '../../../../baza-registry/src';
import { isEmail } from 'class-validator';

const MESSAGE_DELETE_BOUNCED_EMAIL_NOT_FOUND = 'Address not found in bounces table';

// tslint:disable-next-line:no-require-imports no-var-requires
// eslint-disable-next-line @typescript-eslint/no-var-requires
const mailgun = require('mailgun-js');

/**
 * MailGun Transport for MailService
 */
@Injectable()
export class MailMailgunTransport implements IMailTransport {
    constructor(
        @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig,
        private readonly logger: BazaLogger,
        private readonly registry: BazaRegistryService,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Returns true if Mail Transport is ready to use
     */
    get isReadyToUse(): Promise<boolean> {
        return Promise.resolve(true);
    }

    /**
     * Returns true if 2FA should be disable because it's not configured
     * yet to send 2FA-related emails
     */
    get shouldDisable2Fa(): Promise<boolean> {
        return Promise.resolve(false);
    }

    /**
     * This Transport works with file-templates only
     */
    get mode(): IMailTransportMode {
        return 'file-templates';
    }

    /**
     * Returns Mailgun Module Configuration
     * @private
     */
    private get mailgun(): Mailgun {
        return mailgun({
            domain: this.moduleConfig.mailgunConfig.domain,
            apiKey: this.moduleConfig.mailgunConfig.apiKey,
        });
    }

    /**
     * To send a mail with the given request
     * @param request SendMailViaTransportServiceRequest
     * @returns Promise<void>
     */
    async sendMail(request: SendMailViaTransportServiceRequest): Promise<void> {
        for (const to of request.to) {
            await this.deleteBouncedEmailAddress(to);

            if (to && Array.isArray(to.cc) && to.cc.length > 0) {
                await this.deleteBouncedEmailAddress(to.cc);
            }

            await this.performSendMail(request, to);
        }
    }

    /**
     * To delete the bounced email address from suppression list
     * @param input SendMailRecipient | Array<SendMailCCRecipient>
     */
    private async deleteBouncedEmailAddress(input: SendMailRecipient | Array<SendMailCCRecipient>): Promise<void> {
        try {
            const url = `/${this.moduleConfig.mailgunConfig.domain}/${MAILGUN_API_RESOURCES.Bounces}`;

            if (input && Array.isArray(input) && input.length > 0) {
                await Promise.all(
                    input.map((toRecipient) => {
                        return this.mailgun.delete(`${url}/${toRecipient.email}`, undefined);
                    }),
                );
            } else {
                await this.mailgun.delete(`${url}/${(input as SendMailRecipient).email}`, undefined);
            }
        } catch (err) {
            if (err?.message !== MESSAGE_DELETE_BOUNCED_EMAIL_NOT_FOUND) {
                this.logger.debug('Failed to delete bounced email address');
                this.logger.debug(err);
            }
        }
    }

    /**
     * Sends email with Mailgun transport
     * @param request SendMailViaTransportServiceRequest
     * @param to SendMailRecipient
     * @private
     */
    private async performSendMail(request: SendMailViaTransportServiceRequest, to: SendMailRecipient): Promise<void> {
        const attachments = request.attachments
            ? request.attachments.map(
                  (next) =>
                      new this.mailgun.Attachment({
                          data: Buffer.from(next.content),
                          filename: next.filename,
                          contentType: next.contentType,
                      }),
              )
            : undefined;

        await new Promise((resolve, reject) => {
            this.mailgun.messages().send(
                {
                    from: `${this.moduleConfig.sender.name} <${this.moduleConfig.sender.email}>`,
                    to: mailRecipientsToTOField(request.to),
                    cc: Array.isArray(to.cc) && to.cc.length > 0 ? mailRecipientsToTOField(to.cc) : undefined,
                    bcc: this.getBccEmails(),
                    subject: request.subject,
                    html: request.bodyHtml,
                    text: request.bodyText,
                    attachment: attachments,
                },
                (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(true);
                    }
                },
            );
        });
    }

    /**
     * will get the bcc emails from registry  and return array of emails or undefined
     * @private
     */
    private getBccEmails(): string[] | undefined {
        let bcc = this.registry.getValue('bcc');

        if (bcc) {
            bcc = bcc.split(',').filter((email) => !!email && isEmail(email));
        }

        return bcc && bcc.length ? bcc : undefined;
    }
}
