import { Inject, Injectable } from '@nestjs/common';
import type { IMailTransport, MailModuleConfig, SendMailViaTransportServiceRequest } from '../baza-mail.models';
import { IMailTransportMode, MAIL_MODULE_CONFIG } from '../baza-mail.models';
import { map } from 'rxjs/operators';
import { HttpService } from '@nestjs/axios';

// https://sendpulse.com/integrations/api
// https://sendpulse.com/integrations/api/smtp#send-email-smtp

const SEND_PULSE_ACCESS_TOKEN_ENDPOINT = 'https://api.sendpulse.com/oauth/access_token';
const SEND_PULSE_SEND_MAIL_ENDPOINT = 'https://api.sendpulse.com/smtp/emails';

interface SendPulseAccessTokenRequest {
    grant_type: 'client_credentials';
    client_id: string;
    client_secret: string;
}

interface SendPulseAccessTokenResponse {
    access_token: string;
    token_type: string;
    expires_in: string;
}

interface SendPulseSendMailRequest {
    email: {
        html: string;
        text: string;
        subject: string;
        from: {
            name: string;
            email: string;
        };
        to: Array<{
            name: string;
            email: string;
        }>;
        attachments?: Record<string, string>;
    };
}

interface SendPulseSendMailResponse {
    id: string;
    result: boolean;
}

/**
 * SendPulse Transport for MailService
 */
@Injectable()
export class MailSendpulseTransport implements IMailTransport {
    constructor(private httpService: HttpService, @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig) {}

    /**
     * This Transport works with file-templates only
     */
    get mode(): IMailTransportMode {
        return 'file-templates';
    }

    /**
     * Returns true if Mail Transport is ready to use
     */
    get isReadyToUse(): Promise<boolean> {
        return Promise.resolve(true);
    }

    /**
     * Returns true if 2FA should be disable because it's not configured
     * yet to send 2FA-related emails
     */
    get shouldDisable2Fa(): Promise<boolean> {
        return Promise.resolve(false);
    }

    /**
     * Sends emails using SendPulse API
     * @param request
     */
    async sendMail(request: SendMailViaTransportServiceRequest): Promise<void> {
        const accessToken = await this.accessToken();

        const sendMailRequest: SendPulseSendMailRequest = {
            email: {
                subject: request.subject,
                html: (() => {
                    const buffer = new Buffer(request.bodyHtml);

                    return buffer.toString('base64');
                })(),
                text: request.bodyText,
                from: {
                    name: this.moduleConfig.sender.name,
                    email: this.moduleConfig.sender.email,
                },
                to: request.to.map((recipient) => ({
                    email: recipient.email,
                    name: recipient.name,
                })),
                attachments: request.attachments
                    ? (() => {
                          const attachments: Record<string, string> = {};

                          for (const attachment of request.attachments) {
                              attachment[attachment.filename] = attachment.content;
                          }

                          return attachments;
                      })()
                    : undefined,
            },
        };

        await this.httpService
            .post(SEND_PULSE_SEND_MAIL_ENDPOINT, sendMailRequest, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            .pipe(
                map((axiosResponse) => {
                    const sendMaiLResponse: SendPulseSendMailResponse = axiosResponse.data;

                    if (sendMaiLResponse.result === false) {
                        throw new Error('SendPulse responsed with { "result": false }');
                    }

                    return;
                }),
            )
            .toPromise();
    }

    private async accessToken(): Promise<string> {
        const request: SendPulseAccessTokenRequest = {
            grant_type: 'client_credentials',
            client_id: this.moduleConfig.sendPulseConfig.clientId,
            client_secret: this.moduleConfig.sendPulseConfig.clientSecret,
        };

        return this.httpService
            .post(SEND_PULSE_ACCESS_TOKEN_ENDPOINT, request)
            .pipe(
                map((response) => response.data as SendPulseAccessTokenResponse),
                map((response) => response.access_token),
            )
            .toPromise();
    }
}
