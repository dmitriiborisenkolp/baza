import { Inject, Injectable } from '@nestjs/common';
import type { IMailTransport, MailModuleConfig, SendMailViaTransportServiceRequest } from './../baza-mail.models';
import { IMailTransportMode, MAIL_MODULE_CONFIG, mailRecipientsToTOField, SendMailRecipient } from './../baza-mail.models';
import * as nodemailer from 'nodemailer';
import * as Mail from 'nodemailer/lib/mailer';
import * as smtpTransport from 'nodemailer-smtp-transport';
import { isEmail } from 'class-validator';
import { BazaRegistryService } from '../../../../baza-registry/src';

/**
 * SMTP Transport for MailService
 */
@Injectable()
export class MailSmtpTransport implements IMailTransport {
    private _transport: Mail;

    constructor(
        @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig,
        private readonly registry: BazaRegistryService,
    ) {}

    /**
     * This Transport works with file-templates only
     */
    get mode(): IMailTransportMode {
        return 'file-templates';
    }

    /**
     * Returns true if Mail Transport is ready to use
     */
    get isReadyToUse(): Promise<boolean> {
        return Promise.resolve(true);
    }

    /**
     * Returns true if 2FA should be disable because it's not configured
     * yet to send 2FA-related emails
     */
    get shouldDisable2Fa(): Promise<boolean> {
        return Promise.resolve(false);
    }

    /**
     * Returns Module Configuration for SMTP Transport
     * @private
     */
    private get transport(): Mail {
        if (!this._transport) {
            this._transport = nodemailer.createTransport(
                smtpTransport({
                    host: this.moduleConfig.smtpConfig.host,
                    port: this.moduleConfig.smtpConfig.port,
                    auth: {
                        user: this.moduleConfig.smtpConfig.username,
                        pass: this.moduleConfig.smtpConfig.password,
                    },
                    secure: this.moduleConfig.smtpConfig.secure,
                }),
            );
        }

        return this._transport;
    }

    /**
     * Sends emails using SMTP method
     * @param request
     */
    async sendMail(request: SendMailViaTransportServiceRequest): Promise<void> {
        for (const to of request.to) {
            await this.performSendMail(request, to);
        }
    }

    /**
     * Sends email using SMTP method
     * @param request
     * @param to
     */
    private async performSendMail(request: SendMailViaTransportServiceRequest, to: SendMailRecipient): Promise<void> {
        await this.transport.sendMail({
            from: `${this.moduleConfig.sender.name} <${this.moduleConfig.sender.email}>`,
            to: mailRecipientsToTOField([to]),
            cc: Array.isArray(to.cc) && to.cc.length > 0 ? mailRecipientsToTOField(to.cc) : undefined,
            bcc: '',
            subject: request.subject,
            text: request.bodyText,
            html: request.bodyHtml,
            attachments: request.attachments,
        });
    }

    /**
     * will get the bcc emails from registry and combine them with cc values from mail recipient object
     * @private
     */
    private getBccEmails(mailRecipient: SendMailRecipient): string | undefined {
        const bcc =
            Array.isArray(mailRecipient.cc) && mailRecipient.cc.length > 0 ? mailRecipient.cc.map((recipient) => recipient.email) : [];

        let bccRegistryValue = this.registry.getValue('bcc');

        if (bccRegistryValue) {
            bccRegistryValue = bccRegistryValue.split(',').filter((email) => !!email && isEmail(email));

            bcc.push(...bccRegistryValue);
        }

        return bcc && bcc.length ? bcc.join(',') : undefined;
    }
}
