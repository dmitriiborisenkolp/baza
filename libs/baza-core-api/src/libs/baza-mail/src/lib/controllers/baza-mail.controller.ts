import { Controller, Get, Param } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import {
    BazaMailEndpoint,
    BazaMailEndpointPaths,
    BazaMailIsReadyToUseResponse,
    BazaMailTemplate,
    CoreOpenApiTags,
} from '@scaliolabs/baza-core-shared';
import { MailTemplateService } from '../services/mail-template.service';
import { MailService } from '../services/mail.service';

@Controller()
@ApiTags(CoreOpenApiTags.BazaMail)
export class BazaMailController implements BazaMailEndpoint {
    constructor(private readonly mailService: MailService, private readonly mailTemplatesService: MailTemplateService) {}

    @Get(BazaMailEndpointPaths.isReadyToUse)
    @ApiOperation({
        summary: 'isReadyToUse',
        description: 'Returns Ready status for current selected Mail Transport',
    })
    @ApiOkResponse({
        type: BazaMailIsReadyToUseResponse,
    })
    async isReadyToUse(): Promise<BazaMailIsReadyToUseResponse> {
        return {
            ready: await this.mailService.transport.isReadyToUse,
            disable2FA: await this.mailService.transport.shouldDisable2Fa,
        };
    }

    @Get(BazaMailEndpointPaths.mailTemplates)
    @ApiOperation({
        summary: 'mailTemplates',
        description: 'Returns list of Mail Templates registered currently in system',
    })
    @ApiOkResponse({
        type: BazaMailTemplate,
        isArray: true,
    })
    async mailTemplates(): Promise<Array<BazaMailTemplate>> {
        return this.mailTemplatesService.mailTemplates;
    }

    @Get(BazaMailEndpointPaths.mailTemplatesOfTag)
    @ApiOperation({
        summary: 'mailTemplatesOfTag',
        description: 'Returns list of Mail Templates registered currently in system and which are assigned to given Tag',
    })
    @ApiOkResponse({
        type: BazaMailTemplate,
        isArray: true,
    })
    @ApiParam({
        name: 'tag',
        type: 'string',
    })
    async mailTemplatesOfTag(@Param('tag') tag: string): Promise<Array<BazaMailTemplate>> {
        return this.mailTemplatesService.getMailTemplatesOfTag(tag);
    }

    @Get(BazaMailEndpointPaths.mailTemplatesTags)
    @ApiOperation({
        summary: 'mailTemplatesTags',
        description: 'Returns all Mail Template Tags',
    })
    @ApiOkResponse({
        type: String,
        isArray: true,
    })
    async mailTemplatesTags(): Promise<Array<string>> {
        return this.mailTemplatesService.tags;
    }

    @Get(BazaMailEndpointPaths.mailTemplate)
    @ApiOperation({
        summary: 'mailTemplate',
        description: 'Returns Mail Template by Name',
    })
    @ApiOkResponse({
        type: BazaMailTemplate,
        isArray: true,
    })
    @ApiParam({
        name: 'name',
        type: 'string',
    })
    async mailTemplate(@Param('name') name: string): Promise<BazaMailTemplate> {
        return this.mailTemplatesService.getMailTemplateByName(name);
    }
}
