import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import {
    BazaMailCustomerioLinkDto,
    BazaMailCustomerioLinkEndpoint,
    BazaMailCustomerioLinEndpointPaths,
    BazaMailCustomerioLinkSetRequest,
    CoreOpenApiTags,
    BazaMailACL,
    BazaMailCustomerioLinksUnsetRequest,
} from '@scaliolabs/baza-core-shared';
import { MailCustomerioLinkService } from '../services/mail-customerio-link.service';
import { AclNodes } from '../../../../baza-acl/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';

@Controller()
@ApiTags(CoreOpenApiTags.BazaMailCustomerIoLinkCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaMailACL.BazaMailCustomerIoLink])
export class BazaMailCustomerioLinkController implements BazaMailCustomerioLinkEndpoint {
    constructor(private readonly service: MailCustomerioLinkService) {}

    @Get(BazaMailCustomerioLinEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list if Mail Templates with Customer.IO Link associations',
    })
    @ApiOkResponse({
        type: BazaMailCustomerioLinkDto,
        isArray: true,
    })
    async list(): Promise<Array<BazaMailCustomerioLinkDto>> {
        return this.service.list();
    }

    @Get(BazaMailCustomerioLinEndpointPaths.listByTag)
    @ApiOperation({
        summary: 'listByTag',
        description: 'Returns list if Mail Templates with Customer.IO Link associations. Filtered by Tag',
    })
    @ApiParam({
        name: 'tag',
        description: 'Tag',
    })
    @ApiOkResponse({
        type: BazaMailCustomerioLinkDto,
        isArray: true,
    })
    async listByTag(@Param('tag') tag: string): Promise<Array<BazaMailCustomerioLinkDto>> {
        return (await this.service.list()).filter((next) => next.template.tag === tag);
    }

    @Post(BazaMailCustomerioLinEndpointPaths.set)
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaMailACL.BazaMailCustomerIoLinkManagement])
    @ApiOperation({
        summary: 'set',
        description: 'Set Customer.IO Template Id for Mail Template',
    })
    @ApiOkResponse({
        type: BazaMailCustomerioLinkDto,
    })
    async set(@Body() request: BazaMailCustomerioLinkSetRequest): Promise<BazaMailCustomerioLinkDto> {
        return this.service.set(request);
    }

    @Post(BazaMailCustomerioLinEndpointPaths.unset)
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaMailACL.BazaMailCustomerIoLinkManagement])
    @ApiOperation({
        summary: 'unset',
        description: 'Removes Customer.IO Link for given Mail Template',
    })
    @ApiOkResponse()
    async unset(@Body() request: BazaMailCustomerioLinksUnsetRequest): Promise<void> {
        await this.service.unset(request);
    }
}
