import { HttpStatus } from '@nestjs/common';
import { BazaMailErrorCodes, bazaMailErrorCodesI18nEn } from '../error-codes/baza-mail-error.codes';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class MailUnknownMailTransportException extends BazaAppException {
    constructor(name: string) {
        super(
            BazaMailErrorCodes.BazaMailTemplateNotFound,
            bazaMailErrorCodesI18nEn[BazaMailErrorCodes.BazaMailTemplateNotFound],
            HttpStatus.INTERNAL_SERVER_ERROR,
            { name },
        );
    }
}
