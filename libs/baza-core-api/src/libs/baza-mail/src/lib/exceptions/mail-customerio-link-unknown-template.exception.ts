import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaMailCustomerioLinkErrorCodes, bazaMailCustomerioLinkErrorCodesI18nEn } from '@scaliolabs/baza-core-shared';

export class MailCustomerioLinkUnknownTemplateException extends BazaAppException {
    constructor(name: string) {
        super(
            BazaMailCustomerioLinkErrorCodes.BazaMailCustomerioLinkUnknownTemplate,
            bazaMailCustomerioLinkErrorCodesI18nEn[BazaMailCustomerioLinkErrorCodes.BazaMailCustomerioLinkUnknownTemplate],
            HttpStatus.INTERNAL_SERVER_ERROR,
            { name },
        );
    }
}
