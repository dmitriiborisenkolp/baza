import { HttpStatus } from '@nestjs/common';
import { BazaMailErrorCodes, bazaMailErrorCodesI18nEn } from '../error-codes/baza-mail-error.codes';
import { BazaAppException } from '../../../../baza-exceptions/src';

const ERR_MESSAGE = 'Mail module has incorrect configuration: unknown transport (should be memory, smtp, mailgun or sendpulse)';

export class MailUnknownMailTransportException extends BazaAppException {
    constructor() {
        super(
            BazaMailErrorCodes.BazaMailModuleUnknownTransport,
            bazaMailErrorCodesI18nEn[BazaMailErrorCodes.BazaMailModuleUnknownTransport],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
