import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaMailCustomerioLinkErrorCodes, bazaMailCustomerioLinkErrorCodesI18nEn } from '@scaliolabs/baza-core-shared';

export class MailCustomerioLinkNotAvailableException extends BazaAppException {
    constructor(name: string) {
        super(
            BazaMailCustomerioLinkErrorCodes.BazaMailCustomerioLinkIsNotAvailable,
            bazaMailCustomerioLinkErrorCodesI18nEn[BazaMailCustomerioLinkErrorCodes.BazaMailCustomerioLinkIsNotAvailable],
            HttpStatus.INTERNAL_SERVER_ERROR,
            { name },
        );
    }
}
