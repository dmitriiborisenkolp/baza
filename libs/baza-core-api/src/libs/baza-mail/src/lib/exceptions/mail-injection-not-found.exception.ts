import { HttpStatus } from '@nestjs/common';
import { BazaMailErrorCodes, bazaMailErrorCodesI18nEn } from '../error-codes/baza-mail-error.codes';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class MailInjectionNotFoundException extends BazaAppException {
    constructor(relativePath: string) {
        super(
            BazaMailErrorCodes.BazaMailInjectionNotFound,
            bazaMailErrorCodesI18nEn[BazaMailErrorCodes.BazaMailInjectionNotFound],
            HttpStatus.INTERNAL_SERVER_ERROR,
            { relativePath },
        );
    }
}
