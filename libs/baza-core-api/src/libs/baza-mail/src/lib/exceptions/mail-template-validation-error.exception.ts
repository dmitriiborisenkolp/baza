import { HttpStatus } from '@nestjs/common';
import { BazaMailErrorCodes, bazaMailErrorCodesI18nEn } from '../error-codes/baza-mail-error.codes';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class MailTemplateValidationErrorException extends BazaAppException {
    constructor(message: string) {
        super(
            BazaMailErrorCodes.BazaMailTemplateValidationError,
            bazaMailErrorCodesI18nEn[BazaMailErrorCodes.BazaMailTemplateValidationError],
            HttpStatus.INTERNAL_SERVER_ERROR,
            { message },
        );
    }
}
