import { HttpStatus } from '@nestjs/common';
import { BazaMailErrorCodes, bazaMailErrorCodesI18nEn } from '../error-codes/baza-mail-error.codes';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class MailInterceptedException extends BazaAppException {
    constructor() {
        super(
            BazaMailErrorCodes.BazaMailTemplateIntercepted,
            bazaMailErrorCodesI18nEn[BazaMailErrorCodes.BazaMailTemplateIntercepted],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
