import { Inject, Injectable } from '@nestjs/common';
import { BazaLogger } from '../../../../baza-logger/src';
import { BazaMailInterceptor, BazaMailInterceptorMode, BazaMailInterceptorPayload } from '../baza-mail.interceptor';
import { MAIL_MODULE_CONFIG, MailModuleConfig } from '../baza-mail.models';
import { MailTemplateService } from '../services/mail-template.service';

/**
 * Mail Interceptor which will validation Send Mail Request for all Template Variables are defined and has proper
 * documentation for Baza Documentation Portal / Baza CMS
 * You can enable it with `bazaApiBundleConfigBuilder.withMailTemplateValidation()
 */
@Injectable()
export class BazaMailTemplateValidationInterceptor implements BazaMailInterceptor {
    constructor(
        private readonly logger: BazaLogger,
        @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig,
        private readonly service: MailTemplateService,
    ) {}

    get mode(): BazaMailInterceptorMode {
        return 'fail';
    }

    async intercept(payload: BazaMailInterceptorPayload): Promise<boolean> {
        if (this.moduleConfig.enableTemplateValidation) {
            this.service.validateTemplateVariablesSchema(
                payload.variables,
                this.service.getMailTemplateByName(payload.request.name).variables,
            );
        }

        return true;
    }
}
