import { Inject, Injectable } from '@nestjs/common';
import { BazaLogger } from '../../../../baza-logger/src';
import { BazaMailInterceptor, BazaMailInterceptorPayload } from '../baza-mail.interceptor';
import { MAIL_MODULE_CONFIG, MailModuleConfig } from '../baza-mail.models';

/**
 * Mail Interceptor which displays Debug Log for each Send Mail Request
 * The Interceptor will be enabled if MAIL_DEBUG environment flag is enabled
 */
@Injectable()
export class BazaDebugMailInterceptor implements BazaMailInterceptor {
    constructor(private readonly logger: BazaLogger, @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig) {}

    async intercept(payload: BazaMailInterceptorPayload): Promise<boolean> {
        if (this.moduleConfig.debug) {
            this.logger.debug(
                {
                    message: 'Sending an email with request:',
                    request: payload.request,
                    variables: payload.variables,
                },
                BazaDebugMailInterceptor.name,
            );
        }

        return true;
    }
}
