import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';

const isRecord = (input: unknown) => !!input && typeof input === 'object';

/**
 * Validates template variables object against schema
 * Returns string (message) with validation error if validation fails
 * Returns undefined when validation is passed
 * @param variables
 * @param schema
 */
export function bazaMailTemplateVariablesValidator(
    variables: Record<string, unknown>,
    schema: Array<BazaMailTemplateVariable>,
): string | undefined {
    for (const [key, value] of Object.entries(variables)) {
        const schemaDefinition = schema.find((next) => next.field === key);

        if (!schemaDefinition) {
            return `Variable "${key}" is not described in schema`;
        }

        if (!schemaDefinition.description) {
            return `Missing variable description for variable "${key}"`;
        }

        const isArray = schemaDefinition.isArray;
        const isObject = Array.isArray(schemaDefinition.variables);

        if (Array.isArray(value)) {
            if (!isArray) {
                return `Value for "${key}" should not be an array`;
            }

            for (const subValue of value) {
                if (!isRecord(value)) {
                    return `Array item for "${key}" should be an object`;
                }

                const response = bazaMailTemplateVariablesValidator(subValue, schemaDefinition.variables);

                if (response) {
                    return response;
                }
            }
        } else {
            if (isArray) {
                return `Value for "${key}" should be an array`;
            }

            if (isObject && !isRecord(value)) {
                return `Value for "${key}" should be an object`;
            }

            if (!isObject && isRecord(value)) {
                return `Value for "${key}" should not be an object`;
            }

            if (isObject) {
                const response = bazaMailTemplateVariablesValidator(value as Record<string, unknown>, schemaDefinition.variables);

                if (response) {
                    return response;
                }
            }
        }
    }

    const required = schema.filter((next) => !next.optional);

    for (const requiredSchema of required) {
        if (variables[requiredSchema.field] === undefined) {
            return `Variable "${requiredSchema.field}" is not defined in request`;
        }
    }

    return undefined;
}
