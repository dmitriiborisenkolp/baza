import { bazaMailTemplateVariablesValidator } from './baza-mail-template-variables-validator.util';

describe('@baza-core-api/baza-mail/utils/baza-mail-template-variables-validator.util.spec.ts', () => {
    it('[CASE-1] will pass validation for correct schema', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'baz',
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
            ],
        );

        expect(response).toBeUndefined();
    });

    it('[CASE-1] will not pass validation for correct schema and missing variable in request', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
            ],
        );

        expect(response).toBe('Variable "bar" is not defined in request');
    });

    it('[CASE-1] will not pass validation for unknown variable in request', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: 'baz',
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
            ],
        );

        expect(response).toBe('Variable "baz" is not described in schema');
    });

    it('[CASE-1] will not pass validation with missing optional variable in request', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: true,
                },
            ],
        );

        expect(response).toBeUndefined();
    });

    it('[CASE-1] will pass validation with optional variable in request', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: 'baz',
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: true,
                },
            ],
        );

        expect(response).toBeUndefined();
    });

    it('[CASE-1] will not pass validation with missing optional variable in request, but defined as Object', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: {
                    foo: 'bar',
                },
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: true,
                },
            ],
        );

        expect(response).toBe('Value for "baz" should not be an object');
    });

    it('[CASE-1] will not pass validation with missing optional variable in request, but defined as Array (empty)', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        foo: 'bar',
                    },
                ],
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: true,
                },
            ],
        );

        expect(response).toBe('Value for "baz" should not be an array');
    });

    it('[CASE-1] will not pass validation with missing optional variable in request, but defined as Array (with items)', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [],
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: true,
                },
            ],
        );

        expect(response).toBe('Value for "baz" should not be an array');
    });

    it('[CASE-2] will pass validation with Nested Non-Array object', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: {
                    varN: 'N',
                    varM: 'M',
                },
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: false,
                        },
                    ],
                },
            ],
        );

        expect(response).toBeUndefined();
    });

    it('[CASE-2] will not pass validation with Nested Non-Array object if Nested object is passed as Array', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        varN: 'N',
                        varM: 'M',
                    },
                ],
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: false,
                        },
                    ],
                },
            ],
        );

        expect(response).toBe('Value for "baz" should not be an array');
    });

    it('[CASE-2] will not pass validation with Nested Non-Array object if Nested object has not some variables defined', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: {
                    varN: 'N',
                },
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: false,
                        },
                    ],
                },
            ],
        );

        expect(response).toBe('Variable "varM" is not defined in request');
    });

    it('[CASE-2] will pass validation with Nested Non-Array object if Nested object has not some optional variables defined', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: {
                    varN: 'N',
                },
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: true,
                        },
                    ],
                },
            ],
        );

        expect(response).toBeUndefined();
    });

    it('[CASE-3] will pass validation with Nested Array object with correct request', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        varN: 'N1',
                        varM: 'M1',
                    },
                    {
                        varN: 'N2',
                        varM: 'M2',
                    },
                    {
                        varN: 'N3',
                        varM: 'M3',
                    },
                ],
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    isArray: true,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: false,
                        },
                    ],
                },
            ],
        );

        expect(response).toBeUndefined();
    });

    it('[CASE-3] will now pass validation with Nested Array object is some element is missing required variable', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        varN: 'N1',
                        varM: 'M1',
                    },
                    {
                        varN: 'N2',
                    },
                    {
                        varN: 'N3',
                        varM: 'M3',
                    },
                ],
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    isArray: true,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: false,
                        },
                    ],
                },
            ],
        );

        expect(response).toBe('Variable "varM" is not defined in request');
    });

    it('[CASE-3] will now pass validation with Nested Array object is an unknown schema variable will be found', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        varN: 'N1',
                        varM: 'M1',
                    },
                    {
                        varN: 'N2',
                        varM: 'M1',
                    },
                    {
                        varN: 'N3',
                        varM: 'M3',
                        varK: 'K1',
                    },
                ],
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    isArray: true,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: false,
                        },
                    ],
                },
            ],
        );

        expect(response).toBe('Variable "varK" is not described in schema');
    });

    it('[CASE-3] will pass validation with Nested Array object is some element is missing optional variable', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        varN: 'N1',
                        varM: 'M1',
                    },
                    {
                        varN: 'N2',
                    },
                    {
                        varN: 'N3',
                        varM: 'M3',
                    },
                ],
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    isArray: true,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: true,
                        },
                    ],
                },
            ],
        );

        expect(response).toBeUndefined();
    });

    it('[CASE-4] will pass validation with multi-nested example', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        varN: 'N1',
                        varM: [
                            {
                                M1: 'M1 Passed',
                                M2: 'M2 Optional',
                                M3: {
                                    M3d1: 'REQUIRED',
                                    M3d2: 'OPTIONAL',
                                },
                            },
                            {
                                M1: 'M1 Passed',
                                M3: {
                                    M3d1: 'REQUIRED',
                                    M3d2: 'OPTIONAL',
                                },
                            },
                            {
                                M1: 'M1 Passed',
                                M3: {
                                    M3d1: 'REQUIRED',
                                },
                            },
                        ],
                    },
                    {
                        varN: 'N2',
                    },
                ],
                far: {
                    varF1: 'F1 REQUIRED',
                    varF2: 'F2 REQUIRED',
                },
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'far',
                    description: 'Far description',
                    optional: false,
                    isArray: false,
                    variables: [
                        {
                            field: 'varF1',
                            description: 'varF1 description',
                            optional: false,
                        },
                        {
                            field: 'varF2',
                            description: 'varF2 description',
                            optional: true,
                        },
                    ],
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    isArray: true,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: true,
                            isArray: true,
                            variables: [
                                {
                                    field: 'M1',
                                    description: 'M1 Description',
                                    optional: false,
                                },
                                {
                                    field: 'M2',
                                    description: 'M2 Description',
                                    optional: true,
                                },
                                {
                                    field: 'M3',
                                    description: 'M1 Description',
                                    optional: false,
                                    variables: [
                                        {
                                            field: 'M3d1',
                                            description: 'M3d1 Description',
                                            optional: false,
                                        },
                                        {
                                            field: 'M3d2',
                                            description: 'M3d2 Description',
                                            optional: true,
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        );

        expect(response).toBeUndefined();
    });

    it('[CASE-4] will not pass validation with multi-nested example (1)', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        varN: 'N1',
                        varM: [
                            {
                                M1: 'M1 Passed',
                                M2: 'M2 Optional',
                                M3: {
                                    M3d2: 'OPTIONAL',
                                },
                            },
                            {
                                M1: 'M1 Passed',
                                M3: {
                                    M3d1: 'REQUIRED',
                                    M3d2: 'OPTIONAL',
                                },
                            },
                            {
                                M1: 'M1 Passed',
                                M3: {
                                    M3d1: 'REQUIRED',
                                },
                            },
                        ],
                    },
                    {
                        varN: 'N2',
                    },
                ],
                far: {
                    varF1: 'F1 REQUIRED',
                    varF2: 'F2 REQUIRED',
                },
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'far',
                    description: 'Far description',
                    optional: false,
                    isArray: false,
                    variables: [
                        {
                            field: 'varF1',
                            description: 'varF1 description',
                            optional: false,
                        },
                        {
                            field: 'varF2',
                            description: 'varF2 description',
                            optional: true,
                        },
                    ],
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    isArray: true,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: true,
                            isArray: true,
                            variables: [
                                {
                                    field: 'M1',
                                    description: 'M1 Description',
                                    optional: false,
                                },
                                {
                                    field: 'M2',
                                    description: 'M2 Description',
                                    optional: true,
                                },
                                {
                                    field: 'M3',
                                    description: 'M1 Description',
                                    optional: false,
                                    variables: [
                                        {
                                            field: 'M3d1',
                                            description: 'M3d1 Description',
                                            optional: false,
                                        },
                                        {
                                            field: 'M3d2',
                                            description: 'M3d2 Description',
                                            optional: true,
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        );

        expect(response).toBe('Variable "M3d1" is not defined in request');
    });

    it('[CASE-4] will not pass validation with multi-nested example (2)', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'bar',
                baz: [
                    {
                        varN: 'N1',
                        varM: [
                            {
                                M1: 'M1 Passed',
                                M2: 'M2 Optional',
                                M3: {
                                    M3d1: 'REQUIRED',
                                    M3d2: 'OPTIONAL',
                                },
                            },
                            {
                                M1: 'M1 Passed',
                                M3: {
                                    M3d1: 'REQUIRED',
                                    M3d2: 'OPTIONAL',
                                    M3d3: 'UNKNOWN!',
                                },
                            },
                            {
                                M1: 'M1 Passed',
                                M3: {
                                    M3d1: 'REQUIRED',
                                },
                            },
                        ],
                    },
                    {
                        varN: 'N2',
                    },
                ],
                far: {
                    varF1: 'F1 REQUIRED',
                    varF2: 'F2 REQUIRED',
                },
            },
            [
                {
                    field: 'foo',
                    description: 'Foo description',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
                {
                    field: 'far',
                    description: 'Far description',
                    optional: false,
                    isArray: false,
                    variables: [
                        {
                            field: 'varF1',
                            description: 'varF1 description',
                            optional: false,
                        },
                        {
                            field: 'varF2',
                            description: 'varF2 description',
                            optional: true,
                        },
                    ],
                },
                {
                    field: 'baz',
                    description: 'Bar description',
                    optional: false,
                    isArray: true,
                    variables: [
                        {
                            field: 'varN',
                            description: 'Variable N Description',
                            optional: false,
                        },
                        {
                            field: 'varM',
                            description: 'Variable M Description',
                            optional: true,
                            isArray: true,
                            variables: [
                                {
                                    field: 'M1',
                                    description: 'M1 Description',
                                    optional: false,
                                },
                                {
                                    field: 'M2',
                                    description: 'M2 Description',
                                    optional: true,
                                },
                                {
                                    field: 'M3',
                                    description: 'M1 Description',
                                    optional: false,
                                    variables: [
                                        {
                                            field: 'M3d1',
                                            description: 'M3d1 Description',
                                            optional: false,
                                        },
                                        {
                                            field: 'M3d2',
                                            description: 'M3d2 Description',
                                            optional: true,
                                        },
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        );

        expect(response).toBe('Variable "M3d3" is not described in schema');
    });

    it('[CASE-5] will not pass validation with correct schema, but schema has fields w/o description', () => {
        const response = bazaMailTemplateVariablesValidator(
            {
                foo: 'bar',
                bar: 'baz',
            },
            [
                {
                    field: 'foo',
                    description: '',
                    optional: false,
                },
                {
                    field: 'bar',
                    description: 'Bar description',
                    optional: false,
                },
            ],
        );

        expect(response).toBe('Missing variable description for variable "foo"');
    });
});
