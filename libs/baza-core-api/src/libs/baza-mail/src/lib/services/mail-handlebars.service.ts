import { Inject, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import * as fs from 'fs';
import * as Handlebars from 'handlebars';
import { MAIL_MODULE_CONFIG } from '../baza-mail.models';
import type { MailModuleConfig } from '../baza-mail.models';
import { MailDefaultTemplateVariablesService } from './mail-default-template-variables.service';
import { Application, BAZA_COMMON_I18N_CONFIG, capitalizeFirstLetter, ProjectLanguage } from '@scaliolabs/baza-core-shared';

function replaceArg(input: string, key: string, replace: string): string {
    while (input.includes(key)) {
        input = input.replace(key, replace);
    }

    return input;
}

export interface MailHandlebarsRenderAgrs {
    templatePath?: string;
    app?: Application;
    lang?: ProjectLanguage;
    langCamelCase?: boolean;
    variables?: any;
}

export interface MailHandlebarsRenderRequest {
    relativeTemplatePath: string;
    lang?: ProjectLanguage;
    variables?: any;
}

/**
 * Handlebar helpers for generation Mail Templates
 */
@Injectable()
export class MailHandlebarsService implements OnApplicationBootstrap {
    constructor(
        @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig,
        private readonly defaultTemplateVariables: MailDefaultTemplateVariablesService,
    ) {}

    async onApplicationBootstrap(): Promise<void> {
        await this.setupPartials();
    }

    /**
     * Set up Partials for Handlebar
     */
    private async setupPartials(): Promise<void> {
        const defaultTemplateVariables = await this.defaultTemplateVariables.getDefaultTemplateVariables();

        this.moduleConfig.templatePartials.forEach((partial) => {
            for (const language of Object.keys(ProjectLanguage)) {
                for (const app of Object.keys(Application)) {
                    const args: MailHandlebarsRenderAgrs = {
                        lang: language as ProjectLanguage,
                        langCamelCase: true,
                        app: app as Application,
                        templatePath: this.replaceArgs(this.moduleConfig.templatePath, {
                            app: app as Application,
                            lang: language as ProjectLanguage,
                            langCamelCase: false,
                        }),
                    };

                    Handlebars.registerPartial(this.replaceArgs(partial.name, args), (context, options) => {
                        const source = fs.readFileSync(this.replaceArgs(partial.path, args), 'utf8').toString();
                        const template = Handlebars.compile(source);

                        return template(
                            {
                                ...defaultTemplateVariables,
                                ...context.variables,
                            },
                            options,
                        );
                    });
                }
            }
        });
    }

    /**
     * Renders Mail Template (Text or HTML) using Handlebars
     * @param request
     */
    render(request: MailHandlebarsRenderRequest): string {
        const args: MailHandlebarsRenderAgrs = {
            lang: request.lang || BAZA_COMMON_I18N_CONFIG.defaultLanguage,
        };

        const source = fs
            .readFileSync(
                this.replaceArgs(request.relativeTemplatePath, {
                    templatePath: this.replaceArgs(this.moduleConfig.templatePath, args),
                    lang: request.lang || BAZA_COMMON_I18N_CONFIG.defaultLanguage,
                    variables: request.variables,
                }),
                'utf8',
            )
            .toString();

        const template = Handlebars.compile(source);

        return template(request.variables);
    }

    /**
     * Renders Mail Template (Text or HTML) using Handlebars
     * @param request
     */
    renderFromString(request: { template: string; variables: any }): string {
        const template = Handlebars.compile(request.template);

        return template(request.variables);
    }

    /**
     * Replaces arguments (`{templatePath}`, `{app}`, `{lang}`) with actual values
     * @param input
     * @param args
     */
    replaceArgs(input: string, args: MailHandlebarsRenderAgrs): string {
        input = replaceArg(
            input,
            '{lang}',
            args.langCamelCase
                ? capitalizeFirstLetter((args.lang || BAZA_COMMON_I18N_CONFIG.defaultLanguage).toLowerCase())
                : (args.lang || BAZA_COMMON_I18N_CONFIG.defaultLanguage).toLowerCase(),
        );

        input = replaceArg(input, '{templatePath}', args.templatePath);
        input = replaceArg(input, '{app}', this.moduleConfig.getApplicationForEmail(args.app || Application.WEB));

        return input;
    }
}
