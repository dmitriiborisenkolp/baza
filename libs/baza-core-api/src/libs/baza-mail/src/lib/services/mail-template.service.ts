import { Injectable } from '@nestjs/common';
import { BazaMailTemplate, BazaMailTemplateRootVariable, BazaMailTemplateVariable, naturalCompare } from '@scaliolabs/baza-core-shared';
import { MailUnknownMailTransportException } from '../exceptions/mail-template-not-found.exception';
import * as _ from 'lodash';
import { MailTemplateValidationErrorException } from '../exceptions/mail-template-validation-error.exception';
import { bazaMailTemplateVariablesValidator } from '../utils/baza-mail-template-variables-validator.util';
import { OmitType } from '@nestjs/swagger';

/**
 * Collection of Mail Template Definitions
 * @private
 */
const MAIL_TEMPLATES: Array<BazaMailTemplate> = [];

/**
 * Collection of Default Variable Definitions
 * @private
 */
const MAIL_DEFAULT_VARIABLES: Array<BazaMailTemplateRootVariable> = [];

/**
 * Request object for Registering Mail Templates
 */
class BazaMailTemplateRegisterRequest extends OmitType(BazaMailTemplate, ['variables']) {
    variables: Array<BazaMailTemplateVariable>;
}

/**
 * Register Mail Templates
 * Mail Templates should be defined before using it. Mail Templates defines information for Mail Transports and
 * Documentation which is used for Baza Documentaton Portal and Baza CMS
 * @param definitions
 */
export function registerMailTemplates(definitions: Array<BazaMailTemplateRegisterRequest>): void {
    MAIL_TEMPLATES.push(
        ...definitions.map((next) => ({
            ...next,
            variables: next.variables ? next.variables.map((v) => ({ ...v, isDefault: false })) : undefined,
        })),
    );
}

/**
 * Unregister Mail Templates
 * If you would like to not use some Mail Templates like Change Email / Change Email Notification, use this helper
 * to unregister specific Mail Templates
 * @param mailTemplateNames
 */
export function unregisterMailTemplates(mailTemplateNames: Array<string>): void {
    mailTemplateNames.forEach((next) => {
        const index = MAIL_TEMPLATES.findIndex((mailTemplate) => mailTemplate.name === next);

        if (index) {
            MAIL_TEMPLATES.splice(index, 1);
        }
    });
}

/**
 * Register Default Variables which are applied to every Mail Template
 * These variables will be pre-pended for every Mail Template and will be displayed bot for Baza Documenation Portal
 * and Baza CMS
 * @param definitions
 */
export function registerMailDefaultVariables(definitions: Array<BazaMailTemplateVariable>): void {
    MAIL_DEFAULT_VARIABLES.push(...definitions.map((next) => ({ ...next, isDefault: true })));
}

@Injectable()
export class MailTemplateService {
    /**
     * Returns all registed Mail Templates
     */
    get mailTemplates(): Array<BazaMailTemplate> {
        return [...MAIL_TEMPLATES].map((next) => ({
            ...next,
            variables: [...this.defaultTemplateVariables, ...(next.variables || [])],
        }));
    }

    /**
     * Returns all Tags used in registered Mail Templates
     * Tags will be sorted by Natural Compare algorithm
     */
    get tags(): Array<string> {
        return _.uniq(this.mailTemplates.map((next) => next.tag)).sort((a, b) => naturalCompare(a, b));
    }

    /**
     * Returns all Default Variables
     */
    get defaultTemplateVariables(): Array<BazaMailTemplateRootVariable> {
        return [...MAIL_DEFAULT_VARIABLES];
    }

    /**
     * Registers Mail Templates
     * This is a Service-way to use `registerMailTemplates` helper
     * @see registerMailTemplates
     * @param definitions
     */
    registerMailTemplates(definitions: Array<Omit<BazaMailTemplate, 'isDefault'>>): void {
        registerMailTemplates(definitions);
    }

    /**
     * Register Default Variables which are applied to every Mail Template
     * This is a Service-way to use `registerMailDefaultVariables` helper
     * @see registerMailDefaultVariables
     * @param definitions
     */
    registerMailDefaultVariables(definitions: Array<BazaMailTemplateVariable>): void {
        registerMailDefaultVariables(definitions);
    }

    /**
     * Unregister Mail Templates
     * If you would like to not use some Mail Templates like Change Email / Change Email Notification, use this method
     * to unregister specific Mail Templates
     * @param mailTemplateNames
     */
    unregisterMailTemplate(mailTemplateNames: Array<string>): void {
        unregisterMailTemplates(mailTemplateNames);
    }

    /**
     * Registers additional variables for Injection
     * @param name
     * @param variables
     */
    registerInjectedVariables(name: string, variables: Array<BazaMailTemplateVariable>): void {
        const result = MAIL_TEMPLATES.find((next) => next.name === name);

        if (!result) {
            throw new MailUnknownMailTransportException(name);
        }

        result.variables = [
            ...result.variables,
            ...variables.map((next) => ({
                ...next,
                isDefault: false,
            })),
        ];
    }

    /**
     * Returns Mail Template by Name
     * If not found MailUnknownMailTransportException will be thrown
     * @param name
     */
    getMailTemplateByName(name: string): BazaMailTemplate {
        const result = MAIL_TEMPLATES.find((next) => next.name === name);

        if (!result) {
            throw new MailUnknownMailTransportException(name);
        }

        return {
            ...result,
            variables: [...this.defaultTemplateVariables, ...(result.variables || [])],
        };
    }

    /**
     * Returns Mail Template with given Tag
     * List will be sorted by Title with Natural Compare algorithm
     * Default Template Variables will be prepended
     * @param tag
     */
    getMailTemplatesOfTag(tag: string): Array<BazaMailTemplate> {
        return this.mailTemplates.filter((next) => next.tag === tag).sort((a, b) => naturalCompare(a.title, b.title));
    }

    /**
     * Validates template variables object against schema
     * Throw errors if validation is failed
     * @see BazaMailTemplateValidationInterceptor
     * @param variables
     * @param schema
     */
    validateTemplateVariablesSchema(variables: Record<string, unknown>, schema: Array<BazaMailTemplateVariable>): void {
        const message = bazaMailTemplateVariablesValidator(variables, schema);

        if (message) {
            throw new MailTemplateValidationErrorException(
                `${message}, INPUT: ${JSON.stringify(variables)}, SCHEMA: ${JSON.stringify(schema)}`,
            );
        }
    }
}
