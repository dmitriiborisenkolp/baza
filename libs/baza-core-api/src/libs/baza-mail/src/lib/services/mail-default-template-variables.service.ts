import { Injectable } from '@nestjs/common';
import { withoutEndingSlash } from '@scaliolabs/baza-core-shared';
import { EnvService } from '../../../../baza-env/src';
import { BazaCommonEnvironments, ProjectService } from '../../../../baza-common/src';
import { MailInjectVariablesService } from './mail-inject-variables.service';

/**
 * Default Template Variables Service
 */
@Injectable()
export class MailDefaultTemplateVariablesService {
    private defaults: Record<string, unknown>;

    constructor(
        private readonly env: EnvService<BazaCommonEnvironments.AppEndpointsEnvironment>,
        private readonly project: ProjectService,
        private readonly injectVariables: MailInjectVariablesService,
    ) {}

    /**
     * Returns initial default template variables (w/o injections)
     */
    getPartialVariables(): Record<string, unknown> {
        if (!this.defaults) {
            this.defaults = {
                project: this.project.projectName,
                projectCodeName: this.project.projectCodeName,
                clientName: this.project.clientName,
                apiUrl: withoutEndingSlash(this.env.getAsString('BAZA_APP_API_URL')),
                cmsUrl: withoutEndingSlash(this.env.getAsString('BAZA_APP_CMS_URL')),
                cmsAssetsUrl: `${withoutEndingSlash(this.env.getAsString('BAZA_APP_CMS_URL'))}/assets`,
                cmsLogoUrl: `${withoutEndingSlash(this.env.getAsString('BAZA_APP_CMS_URL'))}/assets/images/logo-theme-mail.png`,
                webUrl: withoutEndingSlash(this.env.getAsString('BAZA_APP_WEB_URL')),
                webAssetsUrl: `${withoutEndingSlash(this.env.getAsString('BAZA_APP_WEB_URL'))}/assets`,
                webLogoUrl: `${withoutEndingSlash(this.env.getAsString('BAZA_APP_WEB_URL'))}/assets/images/logo-theme-mail.png`,
            };
        }

        return this.defaults;
    }

    /**
     * Returns default template variables (with injections)
     */
    async getDefaultTemplateVariables(): Promise<Record<string, unknown>> {
        const result = { ...this.getPartialVariables() };

        for (const injection of this.injectVariables.getGlobalInjections()) {
            Object.assign(result, await injection.callback(this.getPartialVariables()));
        }

        return result;
    }
}
