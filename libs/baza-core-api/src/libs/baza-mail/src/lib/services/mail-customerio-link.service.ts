import { Injectable } from '@nestjs/common';
import { BazaMailCustomerioLinkRepository } from '../repositories/baza-mail-customerio-link.repository';
import {
    BazaMailCustomerioLinkDto,
    BazaMailCustomerioLinkSetRequest,
    BazaMailCustomerioLinksUnsetRequest,
} from '@scaliolabs/baza-core-shared';
import { MailTemplateService } from './mail-template.service';
import { BazaMailCustomerioLinkEntity } from '../entities/baza-mail-customerio-link.entity';
import { BazaLogger } from '../../../../baza-logger/src';
import { MailCustomerioLinkNotAvailableException } from '../exceptions/mail-customerio-link-not-available.exception';

/**
 * Baza Mail Customerio Link Service
 * Manages links between Baza Mail Templates and Customer.IO Transaction Mail Templates
 */
@Injectable()
export class MailCustomerioLinkService {
    constructor(
        private readonly logger: BazaLogger,
        private readonly repository: BazaMailCustomerioLinkRepository,
        private readonly mailTemplates: MailTemplateService,
    ) {}

    /**
     * Returns list of all Baza Mail Template with optionally linked Customer.IO
     * Transaction Email Template details
     */
    async list(): Promise<Array<BazaMailCustomerioLinkDto>> {
        const result: Array<BazaMailCustomerioLinkDto> = [];
        const existing = await this.repository.list();
        const templates = this.mailTemplates.mailTemplates;

        for (const template of templates) {
            const link = existing.find((next) => next.mailTemplate === template.name);

            result.push({
                template,
                customerIoId: link?.customerIoId,
                customerIoHardcodedId: link?.customerIoHardcodedId,
            });
        }

        return result;
    }

    /**
     * Links Customer.IO Transactional Email Template with Baza Mail Template
     * @param request
     */
    async set(request: BazaMailCustomerioLinkSetRequest): Promise<BazaMailCustomerioLinkDto> {
        const template = this.mailTemplates.getMailTemplateByName(request.templateName);
        const entity = await this.repository.set(request.templateName, request.customerTemplateId);

        return {
            template,
            customerIoId: entity.customerIoId,
            customerIoHardcodedId: entity.customerIoHardcodedId,
        };
    }

    /**
     * Removes Customer.IO Link for given Mail Template
     * @param request
     */
    async unset(request: BazaMailCustomerioLinksUnsetRequest): Promise<void> {
        await this.repository.unset(request.templateName);
    }

    /**
     * Updates Customer.IO Id for Customer.IO Mail Template Link
     * Customer.IO Id will be updated in cases if:
     *  - There is no Customer.IO Link Entity created before
     *  - Hardcoded Customer Id stored in Link before is different
     *  @param name
     *  @param customerIoId
     */
    async syncCustomerIoLink(name: string, customerIoId: number): Promise<void> {
        const template = await this.mailTemplates.getMailTemplateByName(name);
        const existing = await this.repository.findByMailTemplateName(name);

        if (!existing) {
            const entity = new BazaMailCustomerioLinkEntity<string>();

            entity.mailTemplate = template.name;
            entity.customerIoId = customerIoId;
            entity.customerIoHardcodedId = customerIoId;

            await this.repository.save(entity);

            this.logger.log(`Linked Mail Template "${template.name}" with CustomerIo ID ${customerIoId}`, MailCustomerioLinkService.name);
        } else if (existing.customerIoHardcodedId !== customerIoId) {
            existing.customerIoId = customerIoId;
            existing.customerIoHardcodedId = customerIoId;

            await this.repository.save(existing);

            this.logger.log(
                `Updated Mail Template Link "${template.name}" with CustomerIo ID ${customerIoId}`,
                MailCustomerioLinkService.name,
            );
        }
    }

    /**
     * Returns Customer.IO Transactional Email Id
     * If ID is not set for template, MailCustomerioLinkNotAvailableException will be thrown
     * @param name
     */
    async getCustomerIoIdForTemplate(name: string): Promise<number> {
        const link = await this.repository.getByMailTemplateName(name);

        if (!link.customerIoId) {
            throw new MailCustomerioLinkNotAvailableException(name);
        }

        return link.customerIoId;
    }

    /**
     * Returns Customer.IO Transactional Email Id
     * If ID is not set for template, `undefined` value will be returned
     * @param name
     */
    async findCustomerIoIdForTemplate(name: string): Promise<number | undefined> {
        return (await this.repository.getByMailTemplateName(name)).customerIoId;
    }
}
