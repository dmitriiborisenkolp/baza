import { Injectable } from '@nestjs/common';
import { MailInjectionNotFoundException } from '../exceptions/mail-injection-not-found.exception';
import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';
import { MailTemplateService } from './mail-template.service';

interface Injection {
    mailTemplateName: string;
    callback: (variables: Record<string, unknown>) => Promise<Record<string, unknown>>;
    documentation: Array<BazaMailTemplateVariable>;
}

interface GlobalInjection {
    callback: (variables: Record<string, unknown>) => Promise<Record<string, unknown>>;
    documentation: Array<BazaMailTemplateVariable>;
}

/**
 * Mail Injection Service
 * The service is allowing to register Injections to template variable. Use it to add additional template variables
 * for mail templates which are bundled with Baza libraries
 *
 * How to use it:
 *
 * 1. Register injection in `app.module.ts` or anywhere else in your modules:
 *
 * ```typescript
 *  @Global()
 @Module({})
 export class ApiModule implements OnModuleInit {
    constructor(
        private readonly mailInjections: MailInjectVariablesService,
    ) {
    }

    onModuleInit(): any {
        // Register injections for specific templates
        this.mailInjections.register([
            {
                relativePath: 'baza-account/{app}/confirm-email/confirm-email.html.hbs',
                callback: async (initialVariables) => {
                    return {
                        foo: `Example of custom injections (HTML Template): ${initialVariables.link}`,
                    };
                }
            },
            {
                relativePath: 'baza-account/{app}/confirm-email/confirm-email.text.hbs',
                callback: async (initialVariables) => {
                    return {
                        foo: `Example of custom injections (TEXT Template): ${initialVariables.link}`,
                    };
                }
            },
        ]);

        // Register injections for all templates
        this.mailInjections.registerGlobal([
            {
                callback: async (defaultVariables) => ({
                    bar: `BAR Global Variable: ${defaultVariables.clientName}`,
                })
            },
        ]);
    }
 * ```
 *
 * 2. Add new variables to your templates or partials:
 *
 * ```handlebars
 *  {{#> webLayoutHtmlEn }}
    <p>Hello {{../fullName}},</p>
    <p>Please confirm your email by clicking the following link: <a href='{{ ../link }}'>Verify Account</a></p>

    <p>{{ ../foo }} </p> <!-- New custom variable -->
    <p>{{ ../bar }} </p> <!-- New custom variable (global) -->

    <p>Thank You,</p>
    <p>{{ clientName }}</p>
    <p>
        <img src="{{ ../webLogoUrl }}" alt="logo" width="200px">
    </p>
{{/webLayoutHtmlEn}}
 * ```
 */
@Injectable()
export class MailInjectVariablesService {
    private readonly injections: Array<Injection> = [];
    private readonly globalInjections: Array<GlobalInjection> = [];

    constructor(private readonly mailTemplateService: MailTemplateService) {}

    /**
     * Registers new Injection for specific templates
     * Relative path could contains `{app}` instead of `cms` of `web` keywords
     * @param injections
     */
    register(injections: Array<Injection>): void {
        this.injections.push(...injections);

        for (const injection of injections) {
            this.mailTemplateService.registerInjectedVariables(injection.mailTemplateName, injection.documentation);
        }
    }

    /**
     * Registers new Injection for all templates
     * Global injections works both for partials and templates
     * @param injections
     */
    registerGlobal(injections: Array<GlobalInjection>): void {
        this.globalInjections.push(...injections);

        for (const injection of injections) {
            this.mailTemplateService.registerMailDefaultVariables(injection.documentation);
        }
    }

    /**
     * Returns true if there is an Injection available for specified relative path
     * @param relativePath
     */
    hasInjection(relativePath: string): boolean {
        return !!this.injections.find((next) => next.mailTemplateName === relativePath);
    }

    /**
     * Returns an Injection for specified relative path
     * @param relativePath
     */
    getInjection(relativePath: string): Injection {
        const injection = this.injections.find((next) => next.mailTemplateName === relativePath);

        if (!injection) {
            throw new MailInjectionNotFoundException(relativePath);
        }

        return injection;
    }

    /**
     * Returns all registered non-global Injections
     */
    getInjections(): Array<Injection> {
        return this.injections;
    }

    /**
     * Returns all registered global Injections
     */
    getGlobalInjections(): Array<GlobalInjection> {
        return this.globalInjections;
    }

    /**
     * Creates & returns Template Variables object for Handlebars
     * @param relativePath
     * @param initialVariables
     */
    async injectionVariables(relativePath: string, initialVariables: Record<string, unknown>): Promise<Record<string, unknown>> {
        const injection = this.injections.find((next) => next.mailTemplateName === relativePath);

        return (await injection?.callback(initialVariables)) || {};
    }
}
