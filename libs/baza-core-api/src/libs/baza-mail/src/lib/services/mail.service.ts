import { Inject, Injectable } from '@nestjs/common';
import * as path from 'path';
import { ModuleRef } from '@nestjs/core';
import type { IMailTransport, MailModuleConfig, SendMailServiceRequest, SendMailViaTransportServiceRequest } from '../baza-mail.models';
import { BazaSendMailServiceRequest, MAIL_MODULE_CONFIG } from '../baza-mail.models';
import { MailSendpulseTransport } from '../transports/mail-sendpulse.transport';
import { MailUnknownMailTransportException } from '../exceptions/mail-unknown-mail-transport.exception';
import { MailMailgunTransport } from '../transports/mail-mailgun.transport';
import { MailSmtpTransport } from '../transports/mail-smtp.transport';
import { MailMemoryTransport } from '../transports/mail-memory.transport';
import { MailHandlebarsService } from './mail-handlebars.service';
import { MailDefaultTemplateVariablesService } from './mail-default-template-variables.service';
import { I18nApiService } from '../../../../baza-i18n/src';
import { BazaMailSentRepository } from '../repositories/baza-mail-sent.repository';
import { MailCustomerIoTransport } from '../transports/mail-customer-io.transport';
import { MailInjectVariablesService } from './mail-inject-variables.service';
import { BazaMailInterceptor } from '../baza-mail.interceptor';
import { BAZA_COMMON_I18N_CONFIG, ComponentType, withoutTrailingSlash } from '@scaliolabs/baza-core-shared';
import { BazaLogger } from '../../../../baza-logger/src';
import { MailTemplateService } from './mail-template.service';
import { MailInterceptedException } from '../exceptions/mail-intercepted.exception';

@Injectable()
export class MailService {
    private _disabled: boolean;
    private _interceptors: Array<BazaMailInterceptor> = [];

    constructor(
        @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig,
        private readonly bazaLogger: BazaLogger,
        private readonly moduleRef: ModuleRef,
        private readonly handlebars: MailHandlebarsService,
        private readonly i18n: I18nApiService,
        private readonly defaultVariables: MailDefaultTemplateVariablesService,
        private readonly mailSentRepository: BazaMailSentRepository,
        private readonly injectVariables: MailInjectVariablesService,
        private readonly mailTemplates: MailTemplateService,
    ) {}

    /**
     * Disables Service (mail sending)
     */
    haltService(): void {
        this._disabled = true;
    }

    /**
     * Enables Service (mail sending)
     */
    restoreService(): void {
        this._disabled = false;
    }

    /**
     * Returns true if service is enabled
     */
    get isServiceEnabled(): boolean {
        return !this._disabled;
    }

    /**
     * Returns current Mail transport
     */
    get transport(): IMailTransport {
        switch (this.moduleConfig.transport) {
            default: {
                throw new MailUnknownMailTransportException();
            }

            case 'memory': {
                return this.moduleRef.get(MailMemoryTransport);
            }

            case 'smtp': {
                return this.moduleRef.get(MailSmtpTransport);
            }

            case 'mailgun': {
                return this.moduleRef.get(MailMailgunTransport);
            }

            case 'sendpulse': {
                return this.moduleRef.get(MailSendpulseTransport);
            }

            case 'customerio': {
                return this.moduleRef.get(MailCustomerIoTransport);
            }
        }
    }

    /**
     * Attaches Interceptor
     * Interceptors allows to intercept emails, allow/disallow sending emails or modify request or template variables
     * @param interceptor
     */
    attachInterceptor(interceptor: BazaMailInterceptor): void {
        this._interceptors.push(interceptor);
    }

    /**
     * Detaches Interceptor (by Instance)
     * @param interceptor
     */
    detachInterceptor(interceptor: BazaMailInterceptor): void {
        this._interceptors = this._interceptors.filter((next) => next !== interceptor);
    }

    /**
     * Detach Interceptor (by Type)
     * @param interceptorType
     */
    detachInterceptorByType(interceptorType: ComponentType<BazaMailInterceptor>): void {
        this._interceptors = this._interceptors.filter((next) => !(next instanceof interceptorType));
    }

    /**
     * Returns list of Baza Mail Interceptors
     * You can manually edit list of Interceptors
     */
    listInterceptors(): Array<BazaMailInterceptor> {
        return this._interceptors;
    }

    /**
     * Returns true if current Transport can be used to send every known Mail Template
     */
    get isReadyToUse(): Promise<boolean> {
        return this.transport.isReadyToUse;
    }

    /**
     * Sends an email
     * This is a new method to send email. This method uses new Mail Templates approach and supports
     * Customer.IO Transaction Messages API
     * @param request
     */
    async send<T = unknown>(request: BazaSendMailServiceRequest<T>): Promise<void> {
        if (this._disabled) {
            return;
        }

        if (request.uniqueId) {
            if (await this.mailSentRepository.hasMailSentWithUniqueId(request.uniqueId)) {
                return;
            }
        }

        const mailTemplate = this.mailTemplates.getMailTemplateByName(request.name);

        const nonInjectedVariables = {
            ...(await this.defaultVariables.getDefaultTemplateVariables()),
            ...request.variables(),
        };

        const variables = {
            ...nonInjectedVariables,
            ...(await this.injectVariables.injectionVariables(mailTemplate.name, nonInjectedVariables)),
        };

        const templatePathText = path.join(mailTemplate.filePath).replace('{type}', 'text');
        const templatePathHtml = path.join(mailTemplate.filePath).replace('{type}', 'html');

        const mailTransportRequest: SendMailViaTransportServiceRequest = {
            template: mailTemplate,
            variables: variables,
            to: request.to
                .filter((next) => this.couldBeSentTo(next.email))
                .map((next) => {
                    if (next.cc && next.cc.length > 0) {
                        return {
                            ...next,
                            cc: next.cc.filter((ccNext) => this.couldBeSentTo(ccNext.email)),
                        };
                    } else {
                        return next;
                    }
                }),
            subject:
                this.transport.mode === 'file-templates'
                    ? this.i18n.get(request.subject, {
                          replaces: variables,
                          language: BAZA_COMMON_I18N_CONFIG.defaultLanguage,
                      })
                    : undefined,
            bodyText:
                this.transport.mode === 'file-templates'
                    ? this.handlebars.render({
                          lang: BAZA_COMMON_I18N_CONFIG.defaultLanguage,
                          relativeTemplatePath: `{templatePath}/${withoutTrailingSlash(templatePathText)}`,
                          variables: variables,
                      })
                    : undefined,
            bodyHtml:
                this.transport.mode === 'file-templates'
                    ? this.handlebars.render({
                          lang: BAZA_COMMON_I18N_CONFIG.defaultLanguage,
                          relativeTemplatePath: `{templatePath}/${withoutTrailingSlash(templatePathHtml)}`,
                          variables: variables,
                      })
                    : undefined,
            attachments: request.attachments,
        };

        for (const interceptor of this._interceptors.filter((next) => !!next)) {
            const interceptorMode = interceptor.mode || 'ignore';

            try {
                const result = await interceptor.intercept({
                    mailTransportRequest,
                    transport: this.transport,
                    request,
                    variables,
                });

                if (!result) {
                    if (interceptorMode === 'fail') {
                        throw new MailInterceptedException();
                    } else {
                        this.bazaLogger.warn(`Email was not sent because interceptor restricted it`, MailService.name);

                        return;
                    }
                }
            } catch (error) {
                if (interceptorMode === 'fail') {
                    throw error;
                } else {
                    this.bazaLogger.warn(`Email was not sent because interceptor restricted it`, MailService.name);
                    this.bazaLogger.warn(error, MailService.name);

                    return;
                }
            }
        }

        await this.transport.sendMail(mailTransportRequest);

        if (request.uniqueId) {
            await this.mailSentRepository.markUniqueIdAsSent(request.uniqueId);
        }
    }

    /**
     * Sends an email
     * This is a deprecated method to send emails. Use new MailService.send method which is using new Mail Templates
     * approach.
     * Also this method DOES NOT support Mail Interception and Injection Variable features!
     * This method will be removed somewhere around Baza 1.32.0
     * @deprecated
     * @see MailService.send
     * @param request
     */
    async sendMail(request: SendMailServiceRequest): Promise<void> {
        if (this._disabled) {
            return;
        }

        if (request.uniqueId) {
            if (await this.mailSentRepository.hasMailSentWithUniqueId(request.uniqueId)) {
                return;
            }
        }

        const templatePathText = path.join(request.body.asTextSourcePath);
        const templatePathHtml = path.join(request.body.asHtmlSourcePath);

        const variables = {
            ...(await this.defaultVariables.getDefaultTemplateVariables()),
            ...request.variables(),
        };

        const mailTransportRequest: SendMailViaTransportServiceRequest = {
            template: undefined,
            variables,
            subject: this.i18n.get(request.subject, {
                replaces: variables,
                language: request.language,
            }),
            to: request.to
                .filter((next) => this.couldBeSentTo(next.email))
                .map((next) => {
                    if (next.cc && next.cc.length > 0) {
                        return {
                            ...next,
                            cc: next.cc.filter((ccNext) => this.couldBeSentTo(ccNext.email)),
                        };
                    } else {
                        return next;
                    }
                }),
            bodyText: this.handlebars.render({
                lang: request.language,
                relativeTemplatePath: `{templatePath}/${withoutTrailingSlash(templatePathText)}`,
                variables: variables,
            }),
            bodyHtml: this.handlebars.render({
                lang: request.language,
                relativeTemplatePath: `{templatePath}/${withoutTrailingSlash(templatePathHtml)}`,
                variables: variables,
            }),
            attachments: request.attachments,
        };

        await this.transport.sendMail(mailTransportRequest);

        if (request.uniqueId) {
            await this.mailSentRepository.markUniqueIdAsSent(request.uniqueId);
        }
    }

    /**
     * Returns true if it's allowed to send an email to recipient with current environment configuration
     * @param email
     * @private
     */
    private couldBeSentTo(email: string): boolean {
        return this.moduleConfig.allowedMailDomains.length === 0 || this.moduleConfig.allowedMailDomains.includes(email.split('@')[1]);
    }
}
