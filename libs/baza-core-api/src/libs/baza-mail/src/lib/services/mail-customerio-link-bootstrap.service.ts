import { Inject, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { MAIL_MODULE_CONFIG, MailModuleConfig, MailTransportType } from '../baza-mail.models';
import { MailCustomerioLinkService } from './mail-customerio-link.service';

/**
 * Bootstraps Baza Mail Templates <> Customer.IO Transaction Email Templates
 * during application start
 */
@Injectable()
export class MailCustomerioLinkBootstrapService implements OnApplicationBootstrap {
    constructor(
        @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig,
        private readonly service: MailCustomerioLinkService,
    ) {}

    async onApplicationBootstrap(): Promise<void> {
        if (this.moduleConfig.transport === MailTransportType.Customerio && this.moduleConfig.enableTemplateValidation) {
            await this.bootstrap();
        }
    }

    async bootstrap(): Promise<void> {
        const requests = this.moduleConfig.customerIoConfig.customerIoLinksBootstrap;

        for (const request of requests) {
            await this.service.syncCustomerIoLink(request.name, request.customerIoId);
        }
    }
}
