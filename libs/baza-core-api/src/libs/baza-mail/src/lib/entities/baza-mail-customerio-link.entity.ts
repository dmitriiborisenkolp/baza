import { Column, Entity, PrimaryColumn } from 'typeorm';
import { BazaCoreMail } from '../../../../../constants/baza-core.mail';

/**
 * Baza Mail Customerio Link Entity
 */
@Entity()
export class BazaMailCustomerioLinkEntity<T extends string = BazaCoreMail> {
    /**
     * Name (ID) of associated Mail Template
     * @example BazaCoreMail.BazaAccountWebConfirmEmail
     */
    @PrimaryColumn({
        type: 'varchar',
    })
    mailTemplate: T;

    /**
     * Transactional Message Customer.Io Id which is currently set & used
     * (Displayed at "Transactional" section)
     */
    @Column({
        type: 'int',
        nullable: true,
    })
    customerIoId?: number;

    /**
     * Transactional Message Customer.IO Id which is defined in code
     * Can be set using `bazaApiBundleConfigBuilder.withCustomerIoLinks` helper
     * Updating values in `bazaApiBundleConfigBuilder.withCustomerIoLinks` will
     * results to automatically updates it during application start
     * @see BazaApiBundleConfigBuilder.withCustomerIoLinks
     */
    @Column({
        type: 'int',
        nullable: true,
    })
    customerIoHardcodedId?: number;
}
