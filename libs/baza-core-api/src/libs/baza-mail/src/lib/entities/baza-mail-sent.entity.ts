import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BazaMailSentEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        type: 'varchar',
        length: 127,
    })
    uniqueId: string;
}
