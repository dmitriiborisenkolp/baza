export enum BazaMailErrorCodes {
    BazaMailModuleUnknownTransport = 'BazaMailModuleUnknownTransport',
    BazaMailInjectionNotFound = 'BazaMailInjectionNotFound',
    BazaMailTemplateNotFound = 'BazaMailTemplateNotFound',
    BazaMailTemplateValidationError = 'BazaMailTemplateValidationError',
    BazaMailTemplateIntercepted = 'BazaMailTemplateIntercepted',
}

export const bazaMailErrorCodesI18nEn = {
    [BazaMailErrorCodes.BazaMailModuleUnknownTransport]:
        'Mail module has incorrect configuration: unknown transport (should be memory, smtp, mailgun or sendpulse)',
    [BazaMailErrorCodes.BazaMailInjectionNotFound]: 'Mail Injection was not found for Relative Path: {{ relativePath }}',
    [BazaMailErrorCodes.BazaMailTemplateNotFound]: 'Mail Template {{ name }} was not found',
    [BazaMailErrorCodes.BazaMailTemplateValidationError]: 'Mail Template Validation Error: {{ message }}',
    [BazaMailErrorCodes.BazaMailTemplateIntercepted]: 'Mail was not sent due of negative checks from interceptor',
};
