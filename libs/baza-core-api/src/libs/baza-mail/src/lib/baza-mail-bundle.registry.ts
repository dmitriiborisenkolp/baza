import { RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';

export const bazaMailBundleRegistry: RegistrySchema = {
    bcc: {
        name: 'List of Bcc emails (comma , seperated)',
        type: RegistryType.String,
        hiddenFromList: false,
        public: false,
        defaults: undefined,
    },
};
