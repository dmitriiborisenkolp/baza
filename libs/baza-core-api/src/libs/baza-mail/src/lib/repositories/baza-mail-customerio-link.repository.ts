import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaMailCustomerioLinkEntity } from '../entities/baza-mail-customerio-link.entity';
import { naturalCompare } from '@scaliolabs/baza-core-shared';
import { MailCustomerioLinkUnknownTemplateException } from '../exceptions/mail-customerio-link-unknown-template.exception';

/**
 * Repository for BazaMailCustomerioLinkEntity
 */
@Injectable()
export class BazaMailCustomerioLinkRepository {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeORM Repository for BazaMailCustomerioLinkEntity
     */
    get repository(): Repository<BazaMailCustomerioLinkEntity<string>> {
        return this.connection.getRepository<BazaMailCustomerioLinkEntity<string>>(BazaMailCustomerioLinkEntity);
    }

    /**
     * Saves entities of BazaMailCustomerioLinkEntity to DB
     * @param entity
     */
    async save<T extends string>(entity: BazaMailCustomerioLinkEntity<T>): Promise<void> {
        await this.repository.save(entity);
    }

    /**
     * Returns sorted list of current Customer IO Links
     */
    async list(): Promise<Array<BazaMailCustomerioLinkEntity<string>>> {
        return (await this.repository.find()).sort((a, b) => naturalCompare(a.mailTemplate, b.mailTemplate));
    }

    /**
     * Returns BazaMailCustomerioLinkEntity by Mail Template (Name)
     * If entity was not found, returns undefined
     * @param mailTemplateName
     */
    async findByMailTemplateName(mailTemplateName: string): Promise<BazaMailCustomerioLinkEntity<string> | undefined> {
        return this.repository.findOne({
            where: [
                {
                    mailTemplate: mailTemplateName,
                },
            ],
        });
    }

    /**
     * Returns BazaMailCustomerioLinkEntity by Mail Template (Name)
     * If entity was not found, an MailCustomerioLinkUnknownTemplateException
     * error will be thrown
     * @param mailTemplateName
     */
    async getByMailTemplateName(mailTemplateName: string): Promise<BazaMailCustomerioLinkEntity<string>> {
        const entity = await this.findByMailTemplateName(mailTemplateName);

        if (!entity) {
            throw new MailCustomerioLinkUnknownTemplateException(mailTemplateName);
        }

        return entity;
    }

    /**
     * Set (Link) Transactional Message Customer IO with Mail Template
     * @param mailTemplateName
     * @param customerIoId
     */
    async set(mailTemplateName: string, customerIoId: number): Promise<BazaMailCustomerioLinkEntity<string>> {
        const existing = await this.findByMailTemplateName(mailTemplateName);

        if (existing) {
            existing.customerIoId = customerIoId;

            await this.repository.save(existing);

            return existing;
        } else {
            const entity = new BazaMailCustomerioLinkEntity<string>();

            entity.mailTemplate = mailTemplateName;
            entity.customerIoId = customerIoId;

            await this.repository.save(entity);

            return entity;
        }
    }

    /**
     * Removes all Customer.IO Links with given Mail Template Name
     * @param mailTemplateName
     */
    async unset(mailTemplateName: string): Promise<void> {
        await this.repository.delete({
            mailTemplate: mailTemplateName,
        });
    }
}
