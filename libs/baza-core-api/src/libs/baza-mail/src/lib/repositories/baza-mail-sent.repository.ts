import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaMailSentEntity } from '../entities/baza-mail-sent.entity';

@Injectable()
export class BazaMailSentRepository {
    constructor(private readonly connection: Connection) {}

    get repository(): Repository<BazaMailSentEntity> {
        return this.connection.getRepository(BazaMailSentEntity);
    }

    async hasMailSentWithUniqueId(uniqueId: string): Promise<boolean> {
        return !!(await this.repository.findOne({
            where: [
                {
                    uniqueId,
                },
            ],
        }));
    }

    async markUniqueIdAsSent(uniqueId: string): Promise<void> {
        const entity = new BazaMailSentEntity();

        entity.uniqueId = uniqueId;

        await this.repository.save(entity);
    }

    async unmarkUniqueIdAsSent(uniqueId: string): Promise<void> {
        await this.repository.delete({
            uniqueId,
        });
    }
}
