import { HttpModule } from '@nestjs/axios';
import { DynamicModule, Global, Inject, Module, OnModuleInit } from '@nestjs/common';
import { MailService } from './services/mail.service';
import { MAIL_MODULE_CONFIG, MailModuleConfig } from './baza-mail.models';
import { MailSendpulseTransport } from './transports/mail-sendpulse.transport';
import { MailMailgunTransport } from './transports/mail-mailgun.transport';
import { MailSmtpTransport } from './transports/mail-smtp.transport';
import { MailMemoryTransport } from './transports/mail-memory.transport';
import { MailHandlebarsService } from './services/mail-handlebars.service';
import { MailDefaultTemplateVariablesService } from './services/mail-default-template-variables.service';
import { BazaI18nApiModule } from '../../../baza-i18n/src';
import { BazaMailSentRepository } from './repositories/baza-mail-sent.repository';
import { MailCustomerIoTransport } from './transports/mail-customer-io.transport';
import { BazaEnvModule, EnvService } from '../../../baza-env/src';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { MailInjectVariablesService } from './services/mail-inject-variables.service';
import { BazaDebugMailInterceptor } from './interceptors/baza-debug.mail-interceptor';
import { ModuleRef } from '@nestjs/core';
import { MailTemplateService } from './services/mail-template.service';
import { BazaMailController } from './controllers/baza-mail.controller';
import { BazaMailTemplateValidationInterceptor } from './interceptors/baza-mail-template-validation.interceptor';
import { BazaMailCustomerioLinkController } from './controllers/baza-mail-customerio-link.controller';
import { BazaMailCustomerioLinkRepository } from './repositories/baza-mail-customerio-link.repository';
import { MailCustomerioLinkService } from './services/mail-customerio-link.service';
import { MailCustomerioLinkBootstrapService } from './services/mail-customerio-link-bootstrap.service';

interface AsyncOptions {
    inject: Array<any>;
    useFactory(...args: Array<any>): Promise<MailModuleConfig>;
}

const TRANSPORTS = [MailSmtpTransport, MailMailgunTransport, MailMemoryTransport, MailSendpulseTransport, MailCustomerIoTransport];
const BUILT_IN_INTERCEPTORS = [BazaDebugMailInterceptor, BazaMailTemplateValidationInterceptor];

export { AsyncOptions as BazaMailModuleAsyncOptions };

@Module({})
export class BazaMailModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaMailGlobalModule,
            providers: [
                {
                    provide: MAIL_MODULE_CONFIG,
                    useFactory: (...inject) => options.useFactory(...inject),
                    inject: options.inject,
                },
            ],
            exports: [MAIL_MODULE_CONFIG],
        };
    }
}

@Global()
@Module({
    controllers: [BazaMailController, BazaMailCustomerioLinkController],
    imports: [HttpModule, BazaI18nApiModule, BazaEnvModule],
    providers: [
        MailService,
        MailHandlebarsService,
        MailDefaultTemplateVariablesService,
        BazaMailSentRepository,
        MailInjectVariablesService,
        MailTemplateService,
        BazaMailCustomerioLinkRepository,
        MailCustomerioLinkService,
        MailCustomerioLinkBootstrapService,
        ...TRANSPORTS,
        ...BUILT_IN_INTERCEPTORS,
    ],
    exports: [
        MailService,
        MailMemoryTransport,
        BazaMailSentRepository,
        MailInjectVariablesService,
        MailTemplateService,
        BazaMailCustomerioLinkRepository,
        MailCustomerioLinkService,
        MailCustomerioLinkBootstrapService,
        ...TRANSPORTS,
        ...BUILT_IN_INTERCEPTORS,
    ],
})
class BazaMailGlobalModule implements OnModuleInit {
    constructor(
        private readonly env: EnvService,
        @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig,
        private readonly moduleRef: ModuleRef,
        private readonly mailService: MailService,
    ) {}

    onModuleInit(): void {
        if (
            [BazaEnvironments.UAT, BazaEnvironments.Preprod].includes(this.env.current) &&
            this.moduleConfig.allowedMailDomains.length === 0
        ) {
            throw new Error('It\'s not allowed to run API with "MAIL_ALLOWED_DOMAINS=*" configuration for UAT and Preprod environments');
        }

        for (const interceptorType of BUILT_IN_INTERCEPTORS) {
            this.mailService.attachInterceptor(this.moduleRef.get(interceptorType));
        }
    }
}
