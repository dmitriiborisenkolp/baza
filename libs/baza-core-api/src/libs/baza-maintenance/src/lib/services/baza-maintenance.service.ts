import { Inject, Injectable } from '@nestjs/common';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { awaitTimeout, BazaMaintenanceDto, getBazaProjectCodeName, Application, BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BAZA_MAINTENANCE_CONFIG } from '../baza-maintenance-api.config';
import type { BazaMaintenanceApiConfig } from '../baza-maintenance-api.config';
import * as _ from 'underscore';
import { BazaMaintenanceServiceCannotBeUnderMaintenanceException } from '../exceptions/baza-maintenance-service-cannot-be-under-maintenance.exception';
import { EnvService } from '../../../../baza-env/src';

const factoryRedisKey = (env: BazaEnvironments) => `baza-${getBazaProjectCodeName()}-maintenance-mode-${env || 'local'}`;

/**
 * Maintenance Service
 * Allows to get status of Maintenance Mode and Enable / Disable Maintenance Mode
 */
@Injectable()
export class BazaMaintenanceService {
    constructor(
        @Inject(BAZA_MAINTENANCE_CONFIG) private readonly moduleConfig: BazaMaintenanceApiConfig,
        private readonly env: EnvService,
        private readonly redis: RedisService,
    ) {}

    /**
     * Returns REDIS key used for storing information abount Maintenance Mode
     */
    get redisKey(): string {
        return factoryRedisKey(this.env.current);
    }

    /**
     * Returns Maintenance status
     * Maintenance Status is cached in Redis (`baza-${getBazaProjectCodeName()}-maintenance-mode` key). In case if
     * scopes cached in Redis is not matching actual configuration, API will resets Redis cache
     * API looks for X-Baza-Application header to detect application type. The default application is Web. It's not
     * possible to disable CMS application.
     * @see Application
     */
    async status(): Promise<BazaMaintenanceDto> {
        const saved = await this.redis.getClient().get(this.redisKey);

        if (saved) {
            try {
                const parsed: BazaMaintenanceDto = JSON.parse(saved);

                const cachedApplications = (parsed?.scope || []).sort((a, b) => (a > b ? -1 : 1));
                const actualApplications = this.moduleConfig.scope.sort((a, b) => (a > b ? -1 : 1));

                if (cachedApplications.join('') !== actualApplications.join('')) {
                    return this.resetStatus();
                } else {
                    return parsed;
                }
            } catch (err) {
                return this.resetStatus();
            }
        } else {
            return {
                scope: this.moduleConfig.scope,
                enabled: [],
            };
        }
    }

    /**
     * Resets & Disable Maintenance for all applications
     * Also destroys Redis cache
     * API looks for X-Baza-Application header to detect application type. The default application is Web. It's not
     * possible to disable CMS application.
     * @see Application
     */
    async resetStatus(): Promise<BazaMaintenanceDto> {
        await this.redis.getClient().del(this.redisKey);

        const newStatus: BazaMaintenanceDto = {
            scope: [...this.moduleConfig.scope],
            enabled: [],
        };

        await this.redis.getClient().set(this.redisKey, JSON.stringify(newStatus));

        return newStatus;
    }

    /**
     * Returns true if application is disabled (i.e. under Maintenance mode)
     * API looks for X-Baza-Application header to detect application type. The default application is Web. It's not
     * possible to disable CMS application.
     * @see Application
     * @param application
     */
    async isApplicationDisabled(application: Application): Promise<boolean> {
        const status = await this.status();

        return status.scope.includes(application) && status.enabled.includes(application);
    }

    /**
     * Enables Maintenance Mode for specific application
     * API looks for X-Baza-Application header to detect application type. The default application is Web. It's not
     * possible to disable CMS application.
     * @see Application
     * @param applications
     */
    async enable(applications: Array<Application>): Promise<BazaMaintenanceDto> {
        const status = await this.status();

        if (applications.some((app) => !this.moduleConfig.scope.includes(app))) {
            throw new BazaMaintenanceServiceCannotBeUnderMaintenanceException();
        }

        status.enabled = _.uniq([...status.enabled, ...applications.filter((app) => this.moduleConfig.scope.includes(app))]);

        await this.redis.getClient().set(this.redisKey, JSON.stringify(status));

        await awaitTimeout(3 * 1000); // Wait for updates from other nodes

        return status;
    }

    /**
     * Disables Maintenance Mode for specific application
     * API looks for X-Baza-Application header to detect application type. The default application is Web. It's not
     * possible to disable CMS application.
     * @see Application
     * @param applications
     */
    async disable(applications: Array<Application>): Promise<BazaMaintenanceDto> {
        const status = await this.status();

        status.enabled = status.enabled.filter((app) => !applications.includes(app));

        await this.redis.getClient().set(this.redisKey, JSON.stringify(status));

        await awaitTimeout(3 * 1000); // Wait for updates from other nodes

        return status;
    }
}
