import { Injectable } from '@nestjs/common';
import { BAZA_MAINTENANCE_REGISTRY_KEY, BazaMaintenanceMessageDto, BazaMaintenanceSetMessageRequest } from '@scaliolabs/baza-core-shared';
import { BazaRegistryService } from '../../../../baza-registry/src';

@Injectable()
export class BazaMaintenanceMessageService {
    constructor(private readonly registry: BazaRegistryService) {}

    /**
     * Returns message which should be displayed in application for Maintenance mode.
     */
    async getMessage(): Promise<BazaMaintenanceMessageDto> {
        const node = await this.registry.getNode(BAZA_MAINTENANCE_REGISTRY_KEY);

        return {
            message: (node.value || node.defaults) as string,
        };
    }

    /**
     * Set message which should be displayed in application for Maintenance mode.
     * Message is stored in Registry
     */
    async setMessage(request: BazaMaintenanceSetMessageRequest): Promise<void> {
        await this.registry.updateSchemaRecord({
            path: BAZA_MAINTENANCE_REGISTRY_KEY,
            value: request.message,
        });
    }
}
