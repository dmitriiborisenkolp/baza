import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import {
    BazaMaintenanceAcl,
    BazaMaintenanceCmsDisableRequest,
    BazaMaintenanceCmsEnableRequest,
    BazaMaintenanceCmsEndpoint,
    BazaMaintenanceCmsEndpointPaths,
    BazaMaintenanceDto,
    BazaMaintenanceMessageDto,
    BazaMaintenanceSetMessageRequest,
} from '@scaliolabs/baza-core-shared';
import { BazaMaintenanceService } from '../services/baza-maintenance.service';
import { BazaMaintenanceMessageService } from '../services/baza-maintenance-message.service';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';
import { AclNodes } from '../../../../baza-acl/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src/lib/guards/with-access.guard';

@Controller()
@ApiTags(CoreOpenApiTags.BazaMaintenance)
export class BazaMaintenanceCmsController implements BazaMaintenanceCmsEndpoint {
    constructor(private readonly service: BazaMaintenanceService, private readonly messageService: BazaMaintenanceMessageService) {}

    @Get(BazaMaintenanceCmsEndpointPaths.status)
    @ApiOperation({
        summary: 'status',
        description: 'Returns maintenance status',
    })
    @ApiOkResponse({
        type: BazaMaintenanceDto,
    })
    async status(): Promise<BazaMaintenanceDto> {
        return this.service.status();
    }

    @Post(BazaMaintenanceCmsEndpointPaths.enable)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @AclNodes([BazaMaintenanceAcl.BazaMaintenance])
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'enable',
        description: 'Enable maintenance status for given applications',
    })
    @ApiOkResponse({
        type: BazaMaintenanceDto,
    })
    async enable(@Body() request: BazaMaintenanceCmsEnableRequest): Promise<BazaMaintenanceDto> {
        return this.service.enable(request.applications);
    }

    @Post(BazaMaintenanceCmsEndpointPaths.disable)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @AclNodes([BazaMaintenanceAcl.BazaMaintenance])
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'disable',
        description: 'Disable maintenance status for given applications',
    })
    @ApiOkResponse({
        type: BazaMaintenanceDto,
    })
    async disable(@Body() request: BazaMaintenanceCmsDisableRequest): Promise<BazaMaintenanceDto> {
        return this.service.disable(request.applications);
    }

    @Get(BazaMaintenanceCmsEndpointPaths.getMessage)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @AclNodes([BazaMaintenanceAcl.BazaMaintenance])
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getMessage',
        description: 'Returns message for maintenance mode',
    })
    @ApiOkResponse({
        type: BazaMaintenanceMessageDto,
    })
    async getMessage(): Promise<BazaMaintenanceMessageDto> {
        return this.messageService.getMessage();
    }

    @Post(BazaMaintenanceCmsEndpointPaths.setMessage)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @AclNodes([BazaMaintenanceAcl.BazaMaintenance])
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'setMessage',
        description: 'Set message for maintenance mode',
    })
    @ApiOkResponse({
        type: BazaMaintenanceMessageDto,
    })
    async setMessage(@Body() request: BazaMaintenanceSetMessageRequest): Promise<BazaMaintenanceMessageDto> {
        await this.messageService.setMessage(request);

        return this.messageService.getMessage();
    }
}
