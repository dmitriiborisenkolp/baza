import { BAZA_MAINTENANCE_DEFAULT_MESSAGE, BazaMaintenanceErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class BazaMaintenanceServiceUnavailableException extends BazaAppException {
    constructor(message: string = BAZA_MAINTENANCE_DEFAULT_MESSAGE) {
        super(BazaMaintenanceErrorCodes.BazaMaintenanceServiceUnavailable, message, HttpStatus.SERVICE_UNAVAILABLE);
    }
}
