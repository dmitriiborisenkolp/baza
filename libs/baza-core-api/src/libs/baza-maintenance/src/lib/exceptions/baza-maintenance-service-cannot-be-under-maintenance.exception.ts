import { BazaMaintenanceErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class BazaMaintenanceServiceCannotBeUnderMaintenanceException extends BazaAppException {
    constructor() {
        super(BazaMaintenanceErrorCodes.BazaMaintenanceServiceCannotBeUnderMaintenance, 'Application cannot be put under maintenance mode', HttpStatus.BAD_REQUEST);
    }
}
