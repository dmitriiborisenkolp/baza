import { DynamicModule, Module } from '@nestjs/common';
import { BAZA_MAINTENANCE_CONFIG, BazaMaintenanceApiConfig } from './baza-maintenance-api.config';
import { BazaMaintenanceCmsController } from './controllers/baza-maintenance-cms.controller';
import { BazaMaintenanceService } from './services/baza-maintenance.service';
import { XBazaMaintenanceInterceptor } from './interceptors/x-baza-maintenance.interceptor';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { BazaMaintenanceMessageService } from './services/baza-maintenance-message.service';
import { BazaEnvModule } from '../../../baza-env/src';
import { BazaRegistryApiModule } from '../../../baza-registry/src';

interface AsyncOptions {
    imports?: Array<any>;
    injects?: Array<any>;
    useFactory: (...injects) => Promise<BazaMaintenanceApiConfig>;
}

export { AsyncOptions as BazaMaintenanceApiModuleAsyncOptions };

@Module({})
export class BazaMaintenanceApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaMaintenanceApiGlobalModule,
            imports: options.imports,
            providers: [
                {
                    provide: BAZA_MAINTENANCE_CONFIG,
                    inject: options.injects,
                    useFactory: options.useFactory,
                },
            ],
            exports: [BAZA_MAINTENANCE_CONFIG],
        };
    }
}

@Module({
    controllers: [BazaMaintenanceCmsController],
    providers: [
        BazaMaintenanceService,
        BazaMaintenanceMessageService,
        XBazaMaintenanceInterceptor,
        {
            provide: APP_INTERCEPTOR,
            useClass: XBazaMaintenanceInterceptor,
        },
    ],
    imports: [BazaEnvModule, BazaRegistryApiModule],
    exports: [BazaMaintenanceService, BazaMaintenanceMessageService],
})
class BazaMaintenanceApiGlobalModule {}
