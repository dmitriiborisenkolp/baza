import { CallHandler, ExecutionContext, Inject, Injectable, NestInterceptor, Scope } from '@nestjs/common';
import { BAZA_MAINTENANCE_CONFIG } from '../baza-maintenance-api.config';
import type { BazaMaintenanceApiConfig } from '../baza-maintenance-api.config';
import { BazaMaintenanceService } from '../services/baza-maintenance.service';
import { Observable } from 'rxjs';
import { BazaMaintenanceServiceUnavailableException } from '../exceptions/baza-maintenance-service-unavailable.exception';
import { Request as ExpressRequest, Request } from 'express';
import { Application, BazaHttpHeaders, withTrailingSlash } from '@scaliolabs/baza-core-shared';
import { BazaMaintenanceMessageService } from '../services/baza-maintenance-message.service';
import { EnvService } from '../../../../baza-env/src';

@Injectable({
    scope: Scope.REQUEST,
})
export class XBazaMaintenanceInterceptor implements NestInterceptor {
    constructor(
        @Inject(BAZA_MAINTENANCE_CONFIG) private readonly moduleConfig: BazaMaintenanceApiConfig,
        private readonly service: BazaMaintenanceService,
        private readonly messageService: BazaMaintenanceMessageService,
        private readonly envService: EnvService,
    ) {}

    /**
     * The interceptor check is application under Maintenance Mode. API looks for X-Baza-Application HTTP header
     * to decide requested application. If X-Baza-Application is not set, API considers Web as default application
     * It's possible to ignore Maintenance mode with X-Baza-Skip-Maintenance HTTP header.
     * Also, Kube environments (health-check) will also be ignore by the interceptor. API checks User-Agent HTTP Header
     * for `kube` substring.
     * @see Application
     * @param context
     * @param next
     */
    async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
        const request: Request = context.switchToHttp().getRequest();
        const headerUserAgent = request.header('User-Agent');
        const headerSkipMaintenance = request.header(BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE);
        const headerApplication = request.header(BazaHttpHeaders.HTTP_HEADER_APP) || Application.WEB;

        const path = context.switchToHttp().getRequest<ExpressRequest>().path;
        const apiPrefix = this.envService.getAsString('BAZA_API_PREFIX', { defaultValue: '' }).trim();
        const normalizedPath = this.normalizePath(path, apiPrefix);

        const isKube = (headerUserAgent || '').toLowerCase().includes('kube');
        const shouldSkip = !!headerSkipMaintenance;
        const shouldIgnoreForEndpoint = this.moduleConfig.ignoreMaintenanceForEndpoints.includes(normalizedPath);

        if (!isKube && !shouldSkip && !shouldIgnoreForEndpoint) {
            if (await this.service.isApplicationDisabled(headerApplication as Application)) {
                throw new BazaMaintenanceServiceUnavailableException((await this.messageService.getMessage()).message);
            }
        }

        return next.handle();
    }

    /**
     * Removes API prefix from the request path if there's any
     * @param path path sent on the request context
     * @param apiPrefix BAZA_API_PREFIX value
     * @returns path without API prefix
     */
    private normalizePath(path: string, apiPrefix: string): string {
        if (!apiPrefix) {
            return path;
        }

        return withTrailingSlash(path.slice(apiPrefix.length + 1, path.length));
    }
}
