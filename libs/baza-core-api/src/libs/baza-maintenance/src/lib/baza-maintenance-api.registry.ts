import { RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';
import { BAZA_MAINTENANCE_DEFAULT_MESSAGE } from '@scaliolabs/baza-core-shared';

export const bazaMaintenanceRegistry: RegistrySchema = {
    bazaCoreMaintenance: {
        message: {
            name: 'Maintenance Message',
            type: RegistryType.String,
            defaults: BAZA_MAINTENANCE_DEFAULT_MESSAGE,
            hiddenFromList: true,
            public: false,
        },
    }
};
