import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { Application, BazaError, BazaHttpHeaders, generateRandomHexString, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaMaintenanceCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaAuthNodeAccess } from '@scaliolabs/baza-core-node-access';
import { HttpStatus } from '@nestjs/common';
import { BAZA_MAINTENANCE_DEFAULT_MESSAGE, BazaMaintenanceErrorCodes } from '@scaliolabs/baza-core-shared';

// Register account flow
describe('@scaliolabs/baza-core-api/integration-tests/01-baza-maintenance.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAuth = new BazaAuthNodeAccess(http);
    const dataAccessMaintenance = new BazaMaintenanceCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('correctly displays that no maintenance mode set yet', async () => {
        await http.authE2eAdmin();

        const status = await dataAccessMaintenance.status();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.enabled).toEqual([]);
    });

    it('correctly enables maintenance mode for WEB', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessMaintenance.enable({
            applications: [Application.WEB],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessMaintenance.status();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.enabled).toEqual([Application.WEB]);
    });

    it('will response with 503 SERVICE_UNAVAILABLE for WEB status for user', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.statusCode).toBe(HttpStatus.SERVICE_UNAVAILABLE);
        expect(response.code).toBe(BazaMaintenanceErrorCodes.BazaMaintenanceServiceUnavailable);
        expect(response.message).toBe(BAZA_MAINTENANCE_DEFAULT_MESSAGE);
    });

    it('will not response with 503 SERVICE_UNAVAILABLE for IOS status for user', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.IOS,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not response with 503 SERVICE_UNAVAILABLE if SKIP header is provided', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '1',
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not response with 503 SERVICE_UNAVAILABLE for WEB for admin (without SKIP header)', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin-2@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not response with 503 SERVICE_UNAVAILABLE for IOS for admin (without SKIP header)', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin-2@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not response with 503 SERVICE_UNAVAILABLE for WEB for admin (with SKIP header)', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '1',
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin-2@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not response with 503 SERVICE_UNAVAILABLE for IOS for admin (with SKIP header)', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '1',
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin-2@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('correctly enables maintenance mode for WEB and IOS', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessMaintenance.enable({
            applications: [Application.WEB, Application.IOS],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessMaintenance.status();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.enabled).toEqual([Application.WEB, Application.IOS]);
    });

    it('will response with 503 SERVICE_UNAVAILABLE for WEB amd IOS status for user', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const webResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(webResponse)).toBeTruthy();
        expect(webResponse.statusCode).toBe(HttpStatus.SERVICE_UNAVAILABLE);
        expect(webResponse.code).toBe(BazaMaintenanceErrorCodes.BazaMaintenanceServiceUnavailable);

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.IOS,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const iosResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(iosResponse)).toBeTruthy();
        expect(iosResponse.statusCode).toBe(HttpStatus.SERVICE_UNAVAILABLE);
        expect(iosResponse.code).toBe(BazaMaintenanceErrorCodes.BazaMaintenanceServiceUnavailable);
    });

    it('correctly disable maintenance mode for WEB', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessMaintenance.disable({
            applications: [Application.WEB],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessMaintenance.status();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.enabled).toEqual([Application.IOS]);
    });

    it('will response with 503 SERVICE_UNAVAILABLE status for WEB and allow IOS for user', async () => {
        http.noAuth();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.WEB,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const webResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(webResponse)).toBeFalsy();

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.IOS,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const iosResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(iosResponse)).toBeTruthy();
        expect(iosResponse.statusCode).toBe(HttpStatus.SERVICE_UNAVAILABLE);
        expect(iosResponse.code).toBe(BazaMaintenanceErrorCodes.BazaMaintenanceServiceUnavailable);
    });

    it('will correctly returns maintenance message', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessMaintenance.getMessage();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.message).toBe(BAZA_MAINTENANCE_DEFAULT_MESSAGE);
    });

    it('will correctly returns maintenance message for user', async () => {
        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.IOS,
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const iosResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(iosResponse)).toBeTruthy();
        expect(iosResponse.statusCode).toBe(HttpStatus.SERVICE_UNAVAILABLE);
        expect(iosResponse.code).toBe(BazaMaintenanceErrorCodes.BazaMaintenanceServiceUnavailable);
        expect(iosResponse.message).toBe(BAZA_MAINTENANCE_DEFAULT_MESSAGE);
    });

    it('will correctly set maintenance message', async () => {
        const setResponse = await dataAccessMaintenance.setMessage({
            message: 'Maintenance E2E test',
        });

        expect(isBazaErrorResponse(setResponse)).toBeFalsy();

        const response = await dataAccessMaintenance.getMessage();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.message).toBe('Maintenance E2E test');
    });

    it('will correctly returns new maintenance message to user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessMaintenance.getMessage();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.message).toBe('Maintenance E2E test');
    });

    it('will not allow to put CMS under maintenance mode', async () => {
        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessMaintenance.enable({
            applications: [Application.CMS],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaMaintenanceErrorCodes.BazaMaintenanceServiceCannotBeUnderMaintenance);

        const status = await dataAccessMaintenance.status();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.enabled).toEqual([Application.IOS]);
    });

    it('will allow admin to use CMS after failed attempt to put CMS under maintenance mode', async () => {
        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_APP]: Application.CMS,
            },
        };

        const response: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin-2@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not fail for kube environment', async () => {
        await dataAccessMaintenance.enable({
            applications: [Application.WEB, Application.IOS],
        });

        http.optionsForNextRequest = {
            withHeaders: {
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const webResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-2@scal.io',
            password: 'e2e-user-password',
        })) as any;

        expect(isBazaErrorResponse(webResponse)).toBeTruthy();
        expect(webResponse.statusCode).toBe(HttpStatus.SERVICE_UNAVAILABLE);
        expect(webResponse.code).toBe(BazaMaintenanceErrorCodes.BazaMaintenanceServiceUnavailable);

        http.optionsForNextRequest = {
            withHeaders: {
                'User-Agent': 'example-kube-v1.0.0',
                [BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE]: '',
            },
        };

        const kubeResponse: BazaError = (await dataAccessAuth.auth({
            email: 'e2e-admin-2@scal.io',
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(kubeResponse)).toBeFalsy();
    });
});
