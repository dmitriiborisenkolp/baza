import { Application } from '@scaliolabs/baza-core-shared';

export interface BazaMaintenanceApiConfig {
    /**
     * Allowed applications to put under Maintenance mode.
     * The list is affecting Maintenance CMS
     */
    scope: Array<Application>;

    /**
     * HTTP Header for force ignoring Maintenance mode
     */
    headerIgnoreMaintenanceMode: string;

    /**
     * Specify endpoints which will still work even if application is under Maintenance mode.
     */
    ignoreMaintenanceForEndpoints: Array<string>;
}

export const BAZA_MAINTENANCE_CONFIG = Symbol();
