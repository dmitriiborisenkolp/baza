import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaAwsErrorCodes, bazaAwsErrorCodesI18n } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class AwsAccessDeniedException extends BazaAppException {
    constructor() {
        super(
            BazaAwsErrorCodes.BazaAwsAccessDefined,
            bazaAwsErrorCodesI18n[BazaAwsErrorCodes.BazaAwsAccessDefined],
            HttpStatus.FORBIDDEN,
        );
    }
}
