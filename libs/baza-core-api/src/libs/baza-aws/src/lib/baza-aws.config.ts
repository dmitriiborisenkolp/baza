export interface BazaAwsConfig {
    /**
     * Default TTL for presigned urls.
     */
    defaultTtlSeconds: number;

    /**
     * All presigned urls are being cached by Redis.
     * New presigned url will be generated if presigned urls will be expire within `defaultTtlExpireWindowSeconds`
     */
    defaultTtlExpireWindowSeconds: number;

    /**
     * Use AWS directly from BE
     */
    direct?: BazaAwsDirectConfig;

    /**
     * Use another deployed BE to generate URL's
     */
    proxy?: BazaAwsProxyConfig;

    /**
     * If enabled, AWS will have strict rules about fetching resources
     * from unknown resources
     */
    strict?: boolean;
}

export interface BazaAwsDirectConfig {
    bucket: string;
    url: string;
    accessKeyId: string;
    secretAccessKey: string;
    folder?: string;
}

export interface BazaAwsProxyConfig {
    baseUrl: string;
    auth: {
        email: string;
        password: string;
    };
}

export const AWS_S3_SERVICE_CONFIG = Symbol();
