import { Controller, Get } from '@nestjs/common';
import { BazaAwsE2eEndpoint, BazaAwsE2eEndpointPaths } from '@scaliolabs/baza-core-shared';

@Controller()
export class AwsE2eController implements BazaAwsE2eEndpoint{
    @Get(BazaAwsE2eEndpointPaths.exampleDocFile)
    async exampleDocFile(): Promise<string> {
        return 'Example AWS E2E Document';
    }
}
