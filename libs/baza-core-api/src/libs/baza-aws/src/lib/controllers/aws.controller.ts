import { Body, Controller, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AwsService } from '../services/aws.service';
import {
    BazaAwsDeleteRequest,
    BazaAwsEndpoint,
    BazaAwsEndpointPaths,
    BazaAwsFileDto,
    BazaAwsPresignedUrlRequest,
    BazaAwsUploadResponse,
} from '@scaliolabs/baza-core-shared';
import {} from 'multer';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { FileInterceptor } from '@nestjs/platform-express';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';

@ApiTags(CoreOpenApiTags.BazaAws)
@Controller()
export class AwsController implements BazaAwsEndpoint {
    constructor(private readonly service: AwsService) {}

    @Post(BazaAwsEndpointPaths.upload)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'upload',
        description: 'Upload file directly to AWS',
    })
    @ApiOkResponse({
        type: BazaAwsFileDto,
    })
    @UseInterceptors(FileInterceptor('file'))
    async upload(@UploadedFile() file: Express.Multer.File): Promise<BazaAwsUploadResponse> {
        return this.service.uploadFromBuffer({
            fileName: file.originalname,
            contentType: file.mimetype,
            fileBuffer: file.buffer,
        });
    }

    @Post(BazaAwsEndpointPaths.delete)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'delete',
        description: 'Delete document by s3ObjectId',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaAwsDeleteRequest): Promise<void> {
        await this.service.deleteWithAwsKey(request.s3ObjectId);
    }

    @Post(BazaAwsEndpointPaths.presignedUrl)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'presignedUrl',
        description: 'Generates presigned url',
    })
    @ApiOkResponse()
    async presignedUrl(@Body() request: BazaAwsPresignedUrlRequest): Promise<string> {
        return this.service.presignedUrl(request);
    }
}
