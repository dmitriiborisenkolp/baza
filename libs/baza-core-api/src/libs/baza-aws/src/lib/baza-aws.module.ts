import { DynamicModule, Global, Module } from '@nestjs/common';
import { AWS_S3_SERVICE_CONFIG, BazaAwsConfig } from './baza-aws.config';
import { AwsService } from './services/aws.service';
import { AwsController } from './controllers/aws.controller';
import { AwsDirectService } from './services/strategies/aws-direct.service';
import { AwsProxyService } from './services/strategies/aws-proxy.service';
import { AwsHtmlHelper } from './helpers/aws-html.helper';
import { AwsE2eController } from './controllers/aws-e2e.controller';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaAwsConfig>;
}

export { AsyncOptions as BazaAwsModuleAsyncOptions };

@Module({})
export class BazaAwsModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaAwsGlobalModule,
            providers: [
                {
                    provide: AWS_S3_SERVICE_CONFIG,
                    inject: options.injects,
                    useFactory: (...injects) => options.useFactory(...injects),
                },
            ],
            exports: [AWS_S3_SERVICE_CONFIG],
        };
    }
}

@Global()
@Module({
    controllers: [AwsController, AwsE2eController],
    providers: [AwsService, AwsDirectService, AwsProxyService, AwsHtmlHelper],
    exports: [AwsService, AwsHtmlHelper],
})
class BazaAwsGlobalModule {}
