import { Injectable } from '@nestjs/common';
import * as jsdom from 'jsdom';
import * as url from 'url';
import { AwsService } from '../services/aws.service';
import { Application, BAZA_AWS_FIELD_PREFIX } from '@scaliolabs/baza-core-shared';

@Injectable()
export class AwsHtmlHelper {
    constructor(private readonly aws: AwsService) {}

    async process(html: string, application: Application = Application.CMS): Promise<string> {
        if (!html) {
            return undefined;
        }

        const dom = new jsdom.JSDOM(html);

        const images = dom.window.document.getElementsByTagName('img');
        const sources = dom.window.document.getElementsByTagName('source');
        const videos = dom.window.document.getElementsByTagName('video');

        for (let i = 0; i < images.length; i++) {
            const img = images.item(i);

            img.src = await this.processSrc(img.src, application);
        }

        for (let i = 0; i < sources.length; i++) {
            const source = sources.item(i);

            source.src = await this.processSrc(source.src, application);
        }

        for (let i = 0; i < videos.length; i++) {
            const video = videos.item(i);

            video.src = await this.processSrc(video.src, application);
        }

        return dom.serialize();
    }

    private async processSrc(src: string, application: Application): Promise<string> {
        const parsed = url.parse(src);

        if (parsed.hash && `#${parsed.hash.startsWith(BAZA_AWS_FIELD_PREFIX)}`) {
            const s3ObjectId = parsed.hash.slice(BAZA_AWS_FIELD_PREFIX.length + 2);
            const presignedUrl = await this.aws.presignedUrl({
                s3ObjectId,
            });

            return application === Application.CMS ? `${presignedUrl}${parsed.hash}` : presignedUrl;
        } else {
            return src;
        }
    }
}
