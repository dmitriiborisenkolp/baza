import { Inject, Injectable } from '@nestjs/common';
import { AwsStrategy } from '../aws.strategy';
import { Application, BazaAwsDeleteRequest, BazaAwsEndpointPaths, BazaAwsFileDto, BazaAwsUploadResponse, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AWS_S3_SERVICE_CONFIG, BazaAwsConfig } from '../../baza-aws.config';
import { BazaAwsPresignedUrlRequest, BazaAwsUploadFromBufferRequest, BazaAwsUploadFromLocalFileRequest } from '../aws.service';
import * as fs from 'fs';
import * as path from 'path';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-data-access';
import { BazaAppException } from '../../../../../baza-exceptions/src';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const FormData = require('form-data');

class AwsProxyEndpoint {
    public readonly http: BazaDataAccessNode = new BazaDataAccessNode();

    constructor(
        private readonly baseUrl: string,
    ) {
        this.http.baseUrl = baseUrl;
    }

    async upload(request: {
        fileBuffer: any;
        fileName: string;
    }): Promise<BazaAwsUploadResponse> {
        const formData = new FormData();

        formData.append('file', request.fileBuffer, {
            filename: request.fileName,
        });

        return this.pipe(this.http.post(BazaAwsEndpointPaths.upload, formData, {
            withJwt: true,
            asFormData: true,
            withApplication: Application.API,
        }));
    }

    async delete(request: BazaAwsDeleteRequest): Promise<void> {
        return this.pipe(this.http.post(BazaAwsEndpointPaths.delete, request, {
            withJwt: true,
            asJsonResponse: false,
            withApplication: Application.API,
        }));
    }

    async presignedUrl(request: BazaAwsPresignedUrlRequest): Promise<string> {
        if (! request.s3ObjectId) {
            return '';
        }

        return this.pipe(this.http.post(BazaAwsEndpointPaths.presignedUrl, request, {
            withJwt: true,
            asJsonResponse: false,
            withApplication: Application.API,
        }));
    }

    private async pipe<T = any>(input: Promise<T>): Promise<T> {
        return input.then((response) => {
            if (isBazaErrorResponse(response)) {
                throw new BazaAppException(
                    ((response as any) as BazaError).code,
                    ((response as any) as BazaError).message,
                    ((response as any) as BazaError).statusCode,
                );
            } else {
                return response;
            }
        });
    }
}

@Injectable()
export class AwsProxyService implements AwsStrategy {
    private readonly endpoint: AwsProxyEndpoint;

    constructor(
        @Inject(AWS_S3_SERVICE_CONFIG) private readonly config: BazaAwsConfig,
    ) {
        this.endpoint = new AwsProxyEndpoint(config.proxy
            ? config.proxy.baseUrl
            : undefined);
    }

    async uploadFromBuffer(request: BazaAwsUploadFromBufferRequest): Promise<BazaAwsFileDto> {
        await this.auth();

        return this.endpoint.upload({
            fileName: request.fileName,
            fileBuffer: request.fileBuffer,
        });
    }

    async uploadFromLocalFile(request: BazaAwsUploadFromLocalFileRequest): Promise<BazaAwsFileDto> {
        await this.auth();

        const parsed = path.parse(request.filePath);

        return this.endpoint.upload({
            fileName: parsed.base,
            fileBuffer: fs.createReadStream(request.filePath),
        });
    }

    async deleteWithAwsKey(awsKey: string): Promise<void> {
        await this.auth();

        return this.endpoint.delete({
            s3ObjectId: awsKey,
        });
    }

    async presignedUrl(request: BazaAwsPresignedUrlRequest): Promise<string> {
        await this.auth();

        return this.endpoint.presignedUrl(request);
    }

    private async auth(): Promise<void> {
        await this.endpoint.http.auth({
            email: this.config.proxy.auth.email,
            password: this.config.proxy.auth.password,
        });
    }
}
