import { Inject, Injectable } from '@nestjs/common';
import { AwsStrategy } from '../aws.strategy';
import * as aws from 'aws-sdk';
import { BazaAwsFileDto } from '@scaliolabs/baza-core-shared';
import * as path from 'path';
import { withoutEndingSlash } from '@scaliolabs/baza-core-shared';
import * as mime from 'mime';
import * as url from 'url';
import * as fs from 'fs';
import { AWS_S3_SERVICE_CONFIG, BazaAwsConfig } from '../../baza-aws.config';
import { BazaAwsPresignedUrlRequest, BazaAwsUploadFromBufferRequest, BazaAwsUploadFromLocalFileRequest } from '../aws.service';

function factoryAwsFileName(fileName: string): string {
    const outputString = fileName
        .replace(/([~!@#$%^&*()_+=`{}[\]|\\:;'<>,./? ])+/g, '-')
        .replace(/^(-)+|(-)+$/g, '');

    return `${Date.now().toString()}-${outputString}`;
}

@Injectable()
export class AwsDirectService implements AwsStrategy {
    constructor(
        @Inject(AWS_S3_SERVICE_CONFIG) private readonly config: BazaAwsConfig,
    ) { this.init() }

    private init() {
        if (this.config.direct) {
            aws.config.update({
                accessKeyId: this.config.direct.accessKeyId,
                secretAccessKey: this.config.direct.secretAccessKey,
            });
        }
    }

    async uploadFromBuffer(request: BazaAwsUploadFromBufferRequest): Promise<BazaAwsFileDto> {
        const parsed = path.parse(request.fileName)
        const awsFileName = factoryAwsFileName(`${parsed.base}.${parsed.ext}`);

        const key = this.config.direct.folder
            ? `${withoutEndingSlash(this.config.direct.folder)}/${awsFileName}`
            : awsFileName;

        const s3 = new aws.S3();
        await s3
            .putObject({
                Bucket: this.config.direct.bucket,
                Key: key,
                Body: request.fileBuffer,
                ContentType: request.contentType,
                ACL: 'public-read',
            })
            .promise();

        return {
            s3ObjectId: key,
            presignedTimeLimitedUrl: await this.presignedUrl({
                ttlSeconds: this.config.defaultTtlSeconds,
                s3ObjectId: key,
            }),
        };
    }

    async uploadFromLocalFile(request: BazaAwsUploadFromLocalFileRequest): Promise<BazaAwsFileDto> {
        const contentType = mime.getType(request.filePath);
        const parsed = path.parse(request.filePath);

        const key = (this.config.direct.folder
            ? url.resolve(this.config.direct.folder, parsed.name)
            : parsed.name) + parsed.ext;

        const s3 = new aws.S3();
        await s3
            .putObject({
                Bucket: this.config.direct.bucket,
                Key: key,
                Body: fs.createReadStream(request.filePath, {
                    autoClose: true,
                }),
                ContentType: contentType || 'application/octet-stream',
                ACL: 'public-read',
            })
            .promise();

        return {
            s3ObjectId: key,
            presignedTimeLimitedUrl: await this.presignedUrl({
                ttlSeconds: this.config.defaultTtlSeconds,
                s3ObjectId: key,
            }),
        };
    }

    async presignedUrl(request: BazaAwsPresignedUrlRequest): Promise<string> {
        const s3 = new aws.S3();

        return s3.getSignedUrl('getObject', {
            Bucket: this.config.direct.bucket,
            Key: request.s3ObjectId,
            Expires: request.ttlSeconds || this.config.defaultTtlSeconds,
        });
    }

    async deleteWithAwsKey(awsKey: string): Promise<void> {
        const s3 = new aws.S3();

        await s3
            .deleteObject({
                Bucket: this.config.direct.bucket,
                Key: awsKey,
            })
            .promise();
    }
}
