import { Inject, Injectable } from '@nestjs/common';
import type { BazaAwsConfig } from '../baza-aws.config';
import { AWS_S3_SERVICE_CONFIG } from '../baza-aws.config';
import {
    Application,
    BazaAwsE2eEndpointPaths,
    BazaAwsFileDto,
    withoutEndingSlash,
    withoutTrailingSlash,
} from '@scaliolabs/baza-core-shared';
import { AwsStrategy } from './aws.strategy';
import { AwsDirectService } from './strategies/aws-direct.service';
import { AwsProxyService } from './strategies/aws-proxy.service';
import { RedisService } from '@liaoliaots/nestjs-redis';
import * as md5 from 'md5';
import { EnvService } from '../../../../baza-env/src';
import { ProjectService } from '../../../../baza-common/src';
import { AwsAccessDeniedException } from '../exceptions/aws-access-denied.exception';

export interface BazaAwsUploadFromBufferRequest {
    fileBuffer: any;
    fileName: string;
    contentType: string;
}

export interface BazaAwsUploadFromLocalFileRequest {
    filePath: string;
}

export interface BazaAwsPresignedUrlRequest {
    /**
     * S3 Object Id
     */
    s3ObjectId: string;

    /**
     * Custom TTL for presigned URL
     */
    ttlSeconds?: number;
}

@Injectable()
export class AwsService implements AwsStrategy {
    constructor(
        @Inject(AWS_S3_SERVICE_CONFIG) private readonly config: BazaAwsConfig,
        private readonly env: EnvService,
        private readonly project: ProjectService,
        private readonly redis: RedisService,
        private readonly awsDirect: AwsDirectService,
        private readonly awsProxy: AwsProxyService,
    ) {}

    get strategy(): AwsStrategy {
        return this.config.proxy && this.config.proxy.baseUrl ? this.awsProxy : this.awsDirect;
    }

    async uploadFromBuffer(request: BazaAwsUploadFromBufferRequest): Promise<BazaAwsFileDto> {
        return this.strategy.uploadFromBuffer(request);
    }

    async uploadFromLocalFile(request: BazaAwsUploadFromLocalFileRequest): Promise<BazaAwsFileDto> {
        return this.strategy.uploadFromLocalFile(request);
    }

    async deleteWithAwsKey(s3ObjectId: string): Promise<void> {
        this.validateS3ObjectId(s3ObjectId);

        return this.strategy.deleteWithAwsKey(s3ObjectId);
    }

    async presignedUrl(request: BazaAwsPresignedUrlRequest): Promise<string> {
        if (this.env.isTestEnvironment) {
            return `${this.project.apiBaseUrl}/${withoutTrailingSlash(BazaAwsE2eEndpointPaths.exampleDocFile)}`;
        }

        if (!request.s3ObjectId) {
            return undefined;
        }

        try {
            this.validateS3ObjectId(request.s3ObjectId);

            const ttl = request.ttlSeconds || this.config.defaultTtlSeconds;

            const md5Key = md5(`${request.s3ObjectId}_${ttl}`);
            const redisKey = `BAZA_AWS_PRESIGNED_URL_${md5Key}`;

            const cached = await this.redis.getClient().get(redisKey);

            if (cached) {
                return cached;
            } else {
                const presignedUrl = await this.strategy.presignedUrl(request);
                const redisTtl = request.ttlSeconds ? request.ttlSeconds : ttl - this.config.defaultTtlExpireWindowSeconds;

                await this.redis.getClient().set(redisKey, presignedUrl, 'EX', redisTtl);

                return presignedUrl;
            }
        } catch (err) {
            if (!this.config.strict && err instanceof AwsAccessDeniedException) {
                return undefined;
            } else {
                throw err;
            }
        }
    }

    async resource(s3ObjectId: string, application: Application = Application.WEB): Promise<string> {
        if (!s3ObjectId) {
            return undefined;
        }

        this.validateS3ObjectId(s3ObjectId);

        return application === Application.CMS ? s3ObjectId : await this.presignedUrl({ s3ObjectId });
    }

    private validateS3ObjectId(s3ObjectId: string): void {
        if (!s3ObjectId) {
            return;
        }

        if (this.config.direct && this.config.direct.folder) {
            if (!(s3ObjectId || '').startsWith(`${withoutEndingSlash(this.config.direct.folder)}/`)) {
                throw new AwsAccessDeniedException();
            }
        }
    }
}
