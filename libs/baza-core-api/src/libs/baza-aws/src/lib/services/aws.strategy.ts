import { BazaAwsFileDto } from '@scaliolabs/baza-core-shared';
import { BazaAwsPresignedUrlRequest, BazaAwsUploadFromLocalFileRequest } from './aws.service';
import { BazaAwsUploadFromBufferRequest } from './aws.service';

export interface AwsStrategy {
    uploadFromBuffer(request: BazaAwsUploadFromBufferRequest): Promise<BazaAwsFileDto>;
    uploadFromLocalFile(request: BazaAwsUploadFromLocalFileRequest): Promise<BazaAwsFileDto>;
    deleteWithAwsKey(awsKey: string): Promise<void>;
    presignedUrl(request: BazaAwsPresignedUrlRequest): Promise<string>;
}
