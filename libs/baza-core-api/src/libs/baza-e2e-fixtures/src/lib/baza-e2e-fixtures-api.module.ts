import { Module } from '@nestjs/common';
import { bazaE2eFixtures } from './baza-e2e-fixtures';
import { BazaE2eFixturesService } from './services/baza-e2e-fixtures.service';
import { BazaE2eFixturesController } from './controllers/baza-e2e-fixtures.controller';
import { BazaE2eApiModule } from '../../../baza-e2e/src';

@Module({
    imports: [
        BazaE2eApiModule,
    ],
    controllers: [
        BazaE2eFixturesController,
    ],
    providers: [
        BazaE2eFixturesService,
        ...bazaE2eFixtures.map((f) => f.provider)
    ],
    exports: [
        ...bazaE2eFixtures.map((f) => f.provider)
    ],
})
export class BazaE2eFixturesApiModule {
}
