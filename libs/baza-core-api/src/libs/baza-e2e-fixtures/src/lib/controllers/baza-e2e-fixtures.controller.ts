import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { BazaE2eFixturesEndpoint, BazaE2eFixturesEndpointPaths, BazaE2eFixturesUpRequest } from '@scaliolabs/baza-core-shared';
import { BazaE2eFixturesService } from '../services/baza-e2e-fixtures.service';
import { BazaE2eGuard } from '../../../../baza-e2e/src';

@Controller()
@ApiTags(CoreOpenApiTags.BazaE2eFixtures)
@UseGuards(BazaE2eGuard)
export class BazaE2eFixturesController implements BazaE2eFixturesEndpoint {
    constructor(private readonly service: BazaE2eFixturesService) {}

    @Post(BazaE2eFixturesEndpointPaths.up)
    @ApiOperation({
        summary: 'up',
        description: 'Up E2E fixtures',
    })
    @ApiOkResponse()
    async up(@Body() request: BazaE2eFixturesUpRequest): Promise<void> {
        await this.service.up(request);
    }
}
