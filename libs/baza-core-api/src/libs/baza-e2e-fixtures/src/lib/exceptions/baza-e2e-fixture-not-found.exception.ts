import { BazaCoreE2eFixtures, BazaE2eFixturesEndpointErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class BazaE2eFixtureNotFoundException<T = BazaCoreE2eFixtures> extends BazaAppException {
    constructor(fixture: T) {
        super(
            BazaE2eFixturesEndpointErrorCodes.BazaE2eFixturesNotFound,
            'Fixture {{ fixture }} not found',
            HttpStatus.NOT_FOUND,
            { fixture },
        );
    }
}
