import { Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { BazaE2eFixturesUpRequest } from '@scaliolabs/baza-core-shared';
import { bazaE2eFixtures } from '../baza-e2e-fixtures';
import { BazaE2eFixtureNotFoundException } from '../exceptions/baza-e2e-fixture-not-found.exception';
import { BazaLogger } from '../../../../baza-logger/src';
import * as path from 'path';

@Injectable()
export class BazaE2eFixturesService {
    constructor(private readonly moduleRef: ModuleRef, private readonly bazaLogger: BazaLogger) {
        this.bazaLogger.setContext(BazaE2eFixturesService.name);
    }

    async up(request: BazaE2eFixturesUpRequest): Promise<void> {
        this.bazaLogger.log(`[UP] Running ${path.basename(request.specFile)} spec`);

        for (const fixture of request.fixtures) {
            this.bazaLogger.log(`[UP] [${fixture}]`);

            const fixtureDef = bazaE2eFixtures.find((f) => f.fixture === fixture);

            if (!fixtureDef) {
                throw new BazaE2eFixtureNotFoundException(fixture);
            }

            const provided = await this.moduleRef.create(fixtureDef.provider);

            await provided.up(request.fixtures, request.additionalOptions);
        }
    }
}
