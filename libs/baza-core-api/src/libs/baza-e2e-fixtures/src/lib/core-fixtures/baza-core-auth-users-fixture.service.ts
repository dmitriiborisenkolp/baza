import { Injectable } from '@nestjs/common';
import { BazaCoreE2eFixtures, CoreAcl } from '@scaliolabs/baza-core-shared';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { BcryptService } from '../../../../baza-common/src';
import { BazaAccountRepository } from '../../../../baza-account/src/lib/repositories/baza-account.repository';
import { AccountAclRepository } from '../../../../baza-acl/src/lib/repositories/account-acl.repository';
import { AccountAclEntity } from '../../../../baza-acl/src/lib/entity/account-acl.entity';
import { AccountEntity } from '../../../../baza-account/src/lib/entities/account.entity';
import { BazaE2eFixture } from '../baza-e2e-fixture';

export const BAZA_CORE_AUTH_USERS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    email: string;
    firstName: string;
    lastName: string;
    role: AccountRole;
    password: string;
}> = [
    {
        $refId: 1,
        email: 'e2e@scal.io',
        firstName: 'E2E',
        lastName: 'User',
        role: AccountRole.User,
        password: 'e2e-user-password',
    },
    {
        $refId: 2,
        email: 'e2e-2@scal.io',
        firstName: 'E2E',
        lastName: 'User 2',
        role: AccountRole.User,
        password: 'e2e-user-password',
    },
    {
        $refId: 3,
        email: 'e2e-admin@scal.io',
        firstName: 'E2E',
        lastName: 'Admin',
        role: AccountRole.Admin,
        password: 'e2e-admin-password',
    },
    {
        $refId: 4,
        email: 'e2e-admin-2@scal.io',
        firstName: 'E2E',
        lastName: 'Admin 2',
        role: AccountRole.Admin,
        password: 'e2e-admin-password',
    },
];

@Injectable()
export class BazaCoreAuthUsersFixture implements BazaE2eFixture {
    public static _e2eUser: AccountEntity;
    public static _e2eUser2: AccountEntity;
    public static _e2eAdmin: AccountEntity;
    public static _e2eAdmin2: AccountEntity;

    constructor(
        private readonly repository: BazaAccountRepository,
        private readonly aclRepository: AccountAclRepository,
        private readonly bcrypt: BcryptService,
    ) {}

    static reset(): void {
        BazaCoreAuthUsersFixture._e2eUser = undefined;
        BazaCoreAuthUsersFixture._e2eUser2 = undefined;
        BazaCoreAuthUsersFixture._e2eAdmin = undefined;
        BazaCoreAuthUsersFixture._e2eAdmin2 = undefined;
    }

    async up<T = BazaCoreE2eFixtures>(): Promise<void> {
        for (const request of BAZA_CORE_AUTH_USERS_FIXTURE) {
            const existing = await this.repository.findActiveAccountWithEmail(request.email);

            if (existing) {
                BazaCoreAuthUsersFixture._e2eUser = existing;
            } else {
                const accountEntity = new AccountEntity();

                accountEntity.email = request.email;
                accountEntity.firstName = request.firstName;
                accountEntity.lastName = request.lastName;
                accountEntity.fullName = `${request.firstName} ${request.lastName}`;
                accountEntity.role = request.role;
                accountEntity.isEmailConfirmed = true;
                accountEntity.password = await this.bcrypt.hash(request.password);

                await this.repository.save(accountEntity);

                if (accountEntity.role === AccountRole.Admin) {
                    const aclEntity = new AccountAclEntity();

                    aclEntity.account = accountEntity;
                    aclEntity.acl = [CoreAcl.SystemRoot];

                    await this.aclRepository.saveEntity(aclEntity);
                }

                request.$id = accountEntity.id;
            }
        }

        BazaCoreAuthUsersFixture._e2eUser = await this.repository.findActiveAccountWithEmail('e2e@scal.io');
        BazaCoreAuthUsersFixture._e2eUser2 = await this.repository.findActiveAccountWithEmail('e2e-2@scal.io');
        BazaCoreAuthUsersFixture._e2eAdmin = await this.repository.findActiveAccountWithEmail('e2e-admin@scal.io');
        BazaCoreAuthUsersFixture._e2eAdmin2 = await this.repository.findActiveAccountWithEmail('e2e-admin-2@scal.io');
    }

    get e2eUser(): AccountEntity {
        return BazaCoreAuthUsersFixture._e2eUser;
    }

    get e2eUser2(): AccountEntity {
        return BazaCoreAuthUsersFixture._e2eUser2;
    }

    get e2eAdmin(): AccountEntity {
        return BazaCoreAuthUsersFixture._e2eAdmin;
    }

    get e2eAdmin2(): AccountEntity {
        return BazaCoreAuthUsersFixture._e2eAdmin2;
    }
}
