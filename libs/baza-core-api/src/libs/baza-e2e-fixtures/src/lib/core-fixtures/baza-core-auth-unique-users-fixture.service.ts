import { Injectable } from '@nestjs/common';
import { AccountRole, BazaCoreE2eFixtures, CoreAcl, generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { BcryptService } from '../../../../baza-common/src';
import { BazaAccountRepository } from '../../../../baza-account/src/lib/repositories/baza-account.repository';
import { AccountAclRepository } from '../../../../baza-acl/src/lib/repositories/account-acl.repository';
import { AccountAclEntity } from '../../../../baza-acl/src/lib/entity/account-acl.entity';
import { AccountEntity } from '../../../../baza-account/src/lib/entities/account.entity';
import { BazaE2eFixture } from '../baza-e2e-fixture';

export const BAZA_CORE_AUTH_UNIQUE_USERS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    email: () => string;
    firstName: string;
    lastName: string;
    role: AccountRole;
    password: string;
}> = [
    {
        $refId: 1,
        email: () => `user-${generateRandomHexString()}@scal.io`,
        firstName: 'E2E',
        lastName: 'User',
        role: AccountRole.User,
        password: 'e2e-user-password',
    },
    {
        $refId: 3,
        email: () => 'e2e-admin@scal.io',
        firstName: 'E2E',
        lastName: 'Admin',
        role: AccountRole.Admin,
        password: 'e2e-admin-password',
    },
];

@Injectable()
export class BazaCoreAuthUniqueUsersFixture implements BazaE2eFixture {
    public static _e2eUser: AccountEntity;
    public static _e2eAdmin: AccountEntity;

    constructor(
        private readonly repository: BazaAccountRepository,
        private readonly aclRepository: AccountAclRepository,
        private readonly bcrypt: BcryptService,
    ) {}

    static reset(): void {
        BazaCoreAuthUniqueUsersFixture._e2eUser = undefined;
        BazaCoreAuthUniqueUsersFixture._e2eAdmin = undefined;
    }

    async up<T = BazaCoreE2eFixtures>(): Promise<void> {
        for (const request of BAZA_CORE_AUTH_UNIQUE_USERS_FIXTURE) {
            const accountEntity = new AccountEntity();

            accountEntity.email = request.email();
            accountEntity.firstName = request.firstName;
            accountEntity.lastName = request.lastName;
            accountEntity.fullName = `${request.firstName} ${request.lastName}`;
            accountEntity.role = request.role;
            accountEntity.isEmailConfirmed = true;
            accountEntity.password = await this.bcrypt.hash(request.password);

            await this.repository.save(accountEntity);

            if (accountEntity.role === AccountRole.Admin) {
                const aclEntity = new AccountAclEntity();

                aclEntity.account = accountEntity;
                aclEntity.acl = [CoreAcl.SystemRoot];

                await this.aclRepository.saveEntity(aclEntity);
            }

            request.$id = accountEntity.id;

            request.role === AccountRole.User
                ? (BazaCoreAuthUniqueUsersFixture._e2eUser = accountEntity)
                : (BazaCoreAuthUniqueUsersFixture._e2eAdmin = accountEntity);
        }
    }

    get e2eUser(): AccountEntity {
        return BazaCoreAuthUniqueUsersFixture._e2eUser;
    }

    get e2eAdmin(): AccountEntity {
        return BazaCoreAuthUniqueUsersFixture._e2eAdmin;
    }
}
