import { Injectable } from '@nestjs/common';
import { BazaCoreE2eFixtures, CoreAcl } from '@scaliolabs/baza-core-shared';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { BcryptService } from '../../../../baza-common/src';
import { BazaAccountRepository } from '../../../../baza-account/src/lib/repositories/baza-account.repository';
import { AccountAclRepository } from '../../../../baza-acl/src/lib/repositories/account-acl.repository';
import { AccountAclEntity } from '../../../../baza-acl/src/lib/entity/account-acl.entity';
import { AccountEntity } from '../../../../baza-account/src/lib/entities/account.entity';
import { BazaE2eFixture } from '../baza-e2e-fixture';

export const BAZA_CORE_AUTH_ADDITIONAL_ADMINS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
}> = [
    {
        $refId: 1,
        email: 'e2e-admin-1@scal.io',
        firstName: 'Admin',
        lastName: 'No1',
        password: 'e2e-admin-1-password',
    },
    {
        $refId: 2,
        email: 'e2e-admin-2@scal.io',
        firstName: 'Admin',
        lastName: 'No2',
        password: 'e2e-admin-2-password',
    },
];

@Injectable()
export class BazaCoreAuthAdditionalAdminsFixture implements BazaE2eFixture {
    constructor(
        private readonly repository: BazaAccountRepository,
        private readonly bcrypt: BcryptService,
        private readonly aclRepository: AccountAclRepository,
    ) {}

    async up<T = BazaCoreE2eFixtures>(): Promise<void> {
        for (const request of BAZA_CORE_AUTH_ADDITIONAL_ADMINS_FIXTURE) {
            const accountEntity = new AccountEntity();

            accountEntity.email = request.email;
            accountEntity.firstName = request.firstName;
            accountEntity.lastName = request.lastName;
            accountEntity.fullName = `${request.firstName} ${request.lastName}`;
            accountEntity.role = AccountRole.Admin;
            accountEntity.isEmailConfirmed = true;
            accountEntity.password = await this.bcrypt.hash(request.password);

            await this.repository.save(accountEntity);

            if (accountEntity.role === AccountRole.Admin) {
                const aclEntity = new AccountAclEntity();

                aclEntity.account = accountEntity;
                aclEntity.acl = [CoreAcl.SystemRoot];

                await this.aclRepository.saveEntity(aclEntity);
            }

            request.$id = accountEntity.id;
        }
    }
}
