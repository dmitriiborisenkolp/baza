import { BazaCoreE2eFixtures, BazaCoreE2eFixturesAdditionalOptions } from '@scaliolabs/baza-core-shared';

export interface BazaE2eFixture {
    up<T = BazaCoreE2eFixtures>(requestedFixtures: Array<T>, additionalOptions: BazaCoreE2eFixturesAdditionalOptions): Promise<void>;
}
