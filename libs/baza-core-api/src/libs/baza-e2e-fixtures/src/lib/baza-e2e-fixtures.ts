import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaE2eFixture } from './baza-e2e-fixture';
import { BazaCoreAuthUsersFixture } from './core-fixtures/baza-core-auth-users-fixture.service';
import { BazaCoreAuthAdditionalAdminsFixture } from './core-fixtures/baza-core-auth-additional-admins-fixture.service';
import { BazaCoreAuthUniqueUsersFixture } from './core-fixtures/baza-core-auth-unique-users-fixture.service';

export interface FixtureType {
    new (...args: any[]): BazaE2eFixture;
}

export interface E2eFixtureDefinition<T = BazaCoreE2eFixtures> {
    fixture: T;
    provider: FixtureType;
    module?: any;
}

export const bazaE2eFixtures: Array<E2eFixtureDefinition<any>> = [
    { fixture: BazaCoreE2eFixtures.BazaCoreAuthExampleUser, provider: BazaCoreAuthUsersFixture },
    { fixture: BazaCoreE2eFixtures.BazaCoreAuthAdditionalExampleAdmins, provider: BazaCoreAuthAdditionalAdminsFixture },
    { fixture: BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser, provider: BazaCoreAuthUniqueUsersFixture },
];

export function defineE2eFixtures<T = BazaCoreE2eFixtures>(fixtures: Array<E2eFixtureDefinition<T>>): void {
    bazaE2eFixtures.push(...fixtures);
}
