import { Module } from '@nestjs/common';
import { BazaRegistryController } from './controllers/baza-registry.controller';
import { BazaRegistryApiModule } from '../../../baza-registry/src';

@Module({
    imports: [
        BazaRegistryApiModule,
    ],
    controllers: [
        BazaRegistryController,
    ],
})
export class BazaRegistryApiControllersModule {
}
