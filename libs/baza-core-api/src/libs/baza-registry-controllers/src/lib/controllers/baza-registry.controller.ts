import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreAcl, CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import {
    BazaRegistryGetSchemaRecordRequest,
    RegistryBaseNode,
    BazaRegistryEndpoint,
    BazaRegistryEndpointPaths,
    RegistryNode,
    RegistrySchema,
    BazaRegistryUpdateSchemaRecordRequest,
    BazaRegistryGetSchemaRecordResponse,
    BazaRegistryUpdateSchemaRecordResponse,
    BazaRegistryPublicSchema,
} from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';
import { BazaRegistryService } from '../../../../baza-registry/src';
import { AclNodes } from '../../../../baza-acl/src';
import { WithAccessGuard } from '../../../../baza-acl-guards/src/lib/guards/with-access.guard';

@Controller()
@ApiTags(CoreOpenApiTags.BazaRegistry)
export class BazaRegistryController implements BazaRegistryEndpoint {
    constructor(private readonly service: BazaRegistryService) {}

    @Get(BazaRegistryEndpointPaths.publicSchema)
    @ApiOperation({
        summary: 'publicSchema',
        description: 'Returns public registry schema (WEB, IOS, ANDROID)',
    })
    @ApiOkResponse()
    async publicSchema(): Promise<BazaRegistryPublicSchema> {
        return this.service.publicSchema();
    }

    @Get(BazaRegistryEndpointPaths.currentSchema)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @AclNodes([CoreAcl.BazaRegistry])
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'currentSchema',
        description: 'Returns current registry schema',
    })
    @ApiOkResponse()
    async currentSchema(): Promise<RegistrySchema> {
        return this.service.loadSchema();
    }

    @Post(BazaRegistryEndpointPaths.getSchemaRecord)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @AclNodes([CoreAcl.BazaRegistry])
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getSchemaRecord',
        description: 'Returns registry node by path',
    })
    @ApiOkResponse({
        type: RegistryBaseNode,
    })
    async getSchemaRecord<T extends RegistryNode>(
        @Body() request: BazaRegistryGetSchemaRecordRequest,
    ): Promise<BazaRegistryGetSchemaRecordResponse> {
        const node = this.service.getNode(request.path);

        return {
            ...node,
            value: this.service.getValue(request.path),
        };
    }

    @Post(BazaRegistryEndpointPaths.updateSchemaRecord)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
    @AclNodes([CoreAcl.BazaRegistry, CoreAcl.BazaRegistryManagement])
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'updateSchemaRecord',
        description: 'Update registry node',
    })
    @ApiOkResponse({
        type: RegistryBaseNode,
    })
    async updateSchemaRecord(@Body() request: BazaRegistryUpdateSchemaRecordRequest): Promise<BazaRegistryUpdateSchemaRecordResponse> {
        await this.service.updateSchemaRecord({
            path: request.path,
            label: request?.label,
            value: request.value,
        });

        return this.service.getNode(request.path);
    }
}
