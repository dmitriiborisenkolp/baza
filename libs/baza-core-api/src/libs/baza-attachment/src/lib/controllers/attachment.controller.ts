import { Body, Controller, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import * as path from 'path';
import * as process from 'process';
import { Request } from 'express';
import { AttachmentService } from '../services/attachment.service';
import {
    AttachmentDto,
    AttachmentEndpoint,
    AttachmentEndpointPaths,
    AttachmentFile,
    AttachmentRequest,
    AttachmentType,
    AttachmentUploadResponse,
    CoreOpenApiTags,
    generateRandomHexString,
} from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../baza-auth/src';

const RANDOM_FILE_NAME_LENGTH = 8;

@ApiTags(CoreOpenApiTags.BazaAttachment)
@ApiBearerAuth()
@Controller()
export class AttachmentController implements AttachmentEndpoint {
    constructor(private readonly service: AttachmentService) {}

    @Post(AttachmentEndpointPaths.upload)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'upload',
        description: 'Upload new attachment',
    })
    @ApiOkResponse({
        type: AttachmentDto,
    })
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                destination: path.resolve(process.cwd(), `tmp/`),
                filename(req: Request, file: AttachmentFile, callback: (error: Error | null, filename: string) => void): void {
                    const randomName: string = generateRandomHexString(RANDOM_FILE_NAME_LENGTH);

                    callback(null, `${randomName}${path.extname(file.originalname)}`);
                },
            }),
        }),
    )
    upload(@UploadedFile() file: AttachmentFile, @Body() request: AttachmentRequest): Promise<AttachmentUploadResponse> {
        const formDataRequest = (request as any)['request'];
        const parsedRequest: AttachmentRequest = formDataRequest
            ? JSON.parse(formDataRequest)
            : {
                  type: AttachmentType.None,
                  payload: {},
              };

        return this.service.upload(file, parsedRequest);
    }
}
