import { Module } from "@nestjs/common";
import { AttachmentController } from './controllers/attachment.controller';
import { AttachmentService } from './services/attachment.service';
import { NoneStrategy } from './strategies/none.strategy';
import { ImageStrategy } from './strategies/image.strategy';
import { BazaAwsModule } from '../../../baza-aws/src';
import { ImageWithThumbnailStrategy } from './strategies/image-with-thumbnail.strategy';
import { ImageSetStrategy } from './strategies/image-set.strategy';
import { AttachmentImageService } from './services/attachment-image.service';

const STRATEGIES = [
    NoneStrategy,
    ImageStrategy,
    ImageWithThumbnailStrategy,
    ImageSetStrategy,
];

@Module({
    imports: [
        BazaAwsModule,
    ],
    controllers: [
        AttachmentController,
    ],
    providers: [
        AttachmentService,
        AttachmentImageService,
        ...STRATEGIES,
    ],
    exports: [
        AttachmentService,
        AttachmentImageService,
    ],
})
export class BazaAttachmentModule
{}
