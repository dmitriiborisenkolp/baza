import { Application, AttachmentDto, AttachmentFile, AttachmentPayload, AttachmentRequest } from '@scaliolabs/baza-core-shared';

export interface AttachmentProcessFileToUpload {
    id?: string;
    filePath: string;
    mimeType: string;
    imageWidthPx?: number;
    imageHeightPx?: number;
}

export interface AttachmentProcessResponse<T = any> {
    filesToUpload: Array<AttachmentProcessFileToUpload>;
    additionalPayload?: T;
}

export interface AttachmentPayloadRequest {
    process: AttachmentProcessResponse;
    uploaded: Array<AttachmentUploadFile>;
}

export interface AttachmentUploadFile {
    id?: string;
    mime: string;
    filePath: string;
    s3ObjectId: string;
    imageWidthPx?: number;
    imageHeightPx?: number;
}

export interface AttachmentPayloadResponse {
    s3ObjectId: string;
    payload: AttachmentPayload;
}

export interface BazaAttachmentStrategy {
    validate(file: AttachmentFile, request: AttachmentRequest): Promise<void>;
    process(request: AttachmentRequest, tmpFilePath: string, tmpDir: string): Promise<AttachmentProcessResponse>;
    payload(request: AttachmentRequest, uploadPayload: AttachmentPayloadRequest): Promise<AttachmentPayloadResponse>;
    postprocess(input: AttachmentDto, application?: Application /* Application.WEB is default */): Promise<AttachmentDto>;
    isProcessableWithAttachment(input: AttachmentDto): boolean;
}
