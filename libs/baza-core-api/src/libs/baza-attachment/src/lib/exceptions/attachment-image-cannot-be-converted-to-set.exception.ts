import { AttachmentErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentImageCannotBeConvertedToSetException extends BazaAppException {
    constructor() {
        super(AttachmentErrorCodes.AttachmentImageCannotBeConvertedToSet, 'Attachment cannot be converted to image set', HttpStatus.BAD_REQUEST);
    }
}
