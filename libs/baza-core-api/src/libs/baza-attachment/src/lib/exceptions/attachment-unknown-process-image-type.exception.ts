import { AttachmentErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentUnknownProcessImageTypeException extends BazaAppException {
    constructor() {
        super(AttachmentErrorCodes.AttachmentUnknownProcessImageType, 'Unknown image type', HttpStatus.BAD_REQUEST);
    }
}
