import { AttachmentErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentFileIsTooLargeException extends BazaAppException {
    constructor(message: string) {
        super(AttachmentErrorCodes.AttachmentFileIsTooLarge, message, HttpStatus.BAD_REQUEST);
    }
}
