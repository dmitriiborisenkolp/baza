import { AttachmentErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentMimeTypeMismatchException extends BazaAppException {
    constructor(message: string) {
        super(AttachmentErrorCodes.AttachmentMIMETypeMismatch, message, HttpStatus.BAD_REQUEST);
    }
}
