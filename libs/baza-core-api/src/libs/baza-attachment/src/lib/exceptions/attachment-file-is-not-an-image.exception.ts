import { HttpStatus } from '@nestjs/common';
import { AttachmentErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentFileIsNotAnImageException extends BazaAppException {
    constructor() {
        super(AttachmentErrorCodes.AttachmentIsNotAnImage, 'File is not am image', HttpStatus.BAD_REQUEST);
    }
}
