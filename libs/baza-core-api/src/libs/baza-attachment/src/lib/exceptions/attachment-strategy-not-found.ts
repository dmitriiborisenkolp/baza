import { AttachmentErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentStrategyNotFound extends BazaAppException {
    constructor() {
        super(AttachmentErrorCodes.AttachmentStrategyNotFound, 'Unknown attachment strategy', HttpStatus.BAD_REQUEST);
    }
}
