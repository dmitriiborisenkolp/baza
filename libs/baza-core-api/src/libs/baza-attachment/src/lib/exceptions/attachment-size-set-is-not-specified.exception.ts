import { AttachmentErrorCodes, AttachmentImageSize } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentSizeSetIsNotSpecifiedException extends BazaAppException {
    constructor(size: AttachmentImageSize) {
        super(AttachmentErrorCodes.AttachmentSizeSetIsNotSpecified, 'Size "{{ size }}" is not specified', HttpStatus.BAD_REQUEST, {
            size,
        });
    }
}
