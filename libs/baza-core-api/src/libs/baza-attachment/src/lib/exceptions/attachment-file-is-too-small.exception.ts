import { AttachmentErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentFileIsTooSmallException extends BazaAppException {
    constructor(mesage: string) {
        super(AttachmentErrorCodes.AttachmentFileIsTooSmall, mesage, HttpStatus.BAD_REQUEST);
    }
}
