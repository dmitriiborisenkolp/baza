import { AttachmentErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class AttachmentFileIsNotProvidedException extends BazaAppException {
    constructor() {
        super(AttachmentErrorCodes.AttachmentFileIsNotProvided, 'File is not provided', HttpStatus.BAD_REQUEST);
    }
}
