import * as fs from 'fs'
import * as _ from 'underscore';
import * as mime from 'mime';
import { Injectable } from "@nestjs/common";
import { NoneStrategy } from '../strategies/none.strategy';
import { ImageStrategy } from '../strategies/image.strategy';
import { ModuleRef } from '@nestjs/core';
import { AttachmentStrategyNotFound } from '../exceptions/attachment-strategy-not-found';
import { AttachmentPayloadRequest, AttachmentUploadFile, BazaAttachmentStrategy } from '../baza-attachment-strategy';
import { Application, ATTACHMENT_IMAGE_MIME_TYPES, AttachmentDto, AttachmentFile, AttachmentRequest, AttachmentType } from '@scaliolabs/baza-core-shared';
import { AwsService } from '../../../../baza-aws/src';
import { ImageWithThumbnailStrategy } from '../strategies/image-with-thumbnail.strategy';
import * as nodeUtil from 'util';
import { ImageSetStrategy } from '../strategies/image-set.strategy';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const imageSize = nodeUtil.promisify(require('image-size'));

const strategies = [
    { type: AttachmentType.None, strategy: NoneStrategy },
    { type: AttachmentType.Image, strategy: ImageStrategy },
    { type: AttachmentType.ImageWithThumbnail, strategy: ImageWithThumbnailStrategy },
    { type: AttachmentType.ImageSet, strategy: ImageSetStrategy },
];

const defaultStrategy = strategies.find((strategy) => strategy.type === AttachmentType.None);

@Injectable()
export class AttachmentService
{
    constructor(
        private readonly moduleRef: ModuleRef,
        private readonly aws: AwsService,
    ) {}

    async upload(
        file: AttachmentFile,
        request: AttachmentRequest,
    ): Promise<AttachmentDto> {
        if (! request || ! request.payload) {
            request = {
                type: AttachmentType.None,
                payload: {},
            };
        }

        const strategyConfig = strategies.find((s) => s.type === request.type) || defaultStrategy;

        if (! strategyConfig) {
            throw new AttachmentStrategyNotFound();
        }

        const strategy = this.moduleRef.get<BazaAttachmentStrategy>(strategyConfig.strategy);
        await strategy.validate(file, request);

        const strategyResponse = await strategy.process(
            request,
            file.filename,
            file.destination,
        );

        const payloadRequest: AttachmentPayloadRequest = {
            process: strategyResponse,
            uploaded: [],
        };

        for (const uploadRequest of strategyResponse.filesToUpload) {
            const mimeType = mime.getType(uploadRequest.filePath);

            const awsResponse = await this.aws.uploadFromLocalFile({
                filePath: uploadRequest.filePath,
            });

            const uploaded: AttachmentUploadFile = {
                id: uploadRequest.id,
                filePath: uploadRequest.filePath,
                s3ObjectId: awsResponse.s3ObjectId,
                mime: mimeType,
            };

            if (ATTACHMENT_IMAGE_MIME_TYPES.includes(mimeType)) {
                const fileImageSizes: {
                    width: number;
                    height: number;
                } = await imageSize(uploadRequest.filePath);

                uploaded.imageWidthPx = fileImageSizes.width;
                uploaded.imageHeightPx = fileImageSizes.height;
            }

            payloadRequest.uploaded.push(uploaded);
        }

        const responsePayload = await strategy.payload(request, payloadRequest);

        for (const fileToDelete of _.uniq([...strategyResponse.filesToUpload.map((ftu) => ftu.filePath), file.path])) {
            fs.unlinkSync(fileToDelete);
        }

        return {
            presignedUrl: await this.aws.presignedUrl({
                s3ObjectId: responsePayload.s3ObjectId,
            }),
            s3ObjectId: responsePayload.s3ObjectId,
            payload: responsePayload.payload,
        };
    }

    async postprocess(input: AttachmentDto, application = Application.WEB): Promise<AttachmentDto> {
        for (const strategyType of Object.values(AttachmentType)) {
            const strategyConfig = strategies.find((s) => s.type === strategyType) || defaultStrategy;

            if (! strategyConfig) {
                throw new AttachmentStrategyNotFound();
            }

            const strategy = this.moduleRef.get<BazaAttachmentStrategy>(strategyConfig.strategy);

            if (strategy.isProcessableWithAttachment(input)) {
                return strategy.postprocess(input, application);
            }
        }

        throw new AttachmentStrategyNotFound();
    }
}
