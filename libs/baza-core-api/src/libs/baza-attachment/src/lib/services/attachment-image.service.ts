import { AttachmentService } from './attachment.service';
import { Application, AttachmentConstants, AttachmentDto, AttachmentImageSetDto, AttachmentImageSize, AttachmentThumbnailResponse, AttachmentType } from '@scaliolabs/baza-core-shared';
import { AttachmentImageCannotBeConvertedToSetException } from '../exceptions/attachment-image-cannot-be-converted-to-set.exception';
import { Injectable } from '@nestjs/common';
import { AttachmentSizeSetIsNotSpecifiedException } from '../exceptions/attachment-size-set-is-not-specified.exception';
import { AwsService } from '../../../../baza-aws/src';
import { EnvService } from '../../../../baza-env/src';

const closest = (size: number, images: Array<AttachmentThumbnailResponse>) => {
    let current: AttachmentThumbnailResponse;

    for (const next of images) {
        if (next.width <= size) {
            current = next;
        } else {
            break;
        }
    }

    return current || images[0];
};

@Injectable()
export class AttachmentImageService {
    constructor(
        private readonly env: EnvService,
        private readonly service: AttachmentService,
        private readonly awsHelper: AwsService,
    ) {}

    async toImageSet(attachment: AttachmentDto): Promise<AttachmentImageSetDto> {
        if (this.env.isTestEnvironment) {
            return {
                xs: 'xs.jpeg',
                sm: 'sm.jpeg',
                md: 'md.jpeg',
                xl: 'xs.jpeg',
            };
        }

        switch (attachment.payload.type) {
            default: {
                throw new AttachmentImageCannotBeConvertedToSetException();
            }

            case AttachmentType.ImageSet: {
                const processed = await this.service.postprocess(attachment, Application.WEB);

                return processed.payload.payload as AttachmentImageSetDto;
            }

            case AttachmentType.ImageWithThumbnail: {
                const images = [...attachment.payload.payload.original].sort((a, b) => a.width - b.width);

                const result: AttachmentImageSetDto = {
                    xs: '',
                    sm: '',
                    md: '',
                    xl: '',
                };

                for (const size of Object.values(AttachmentImageSize)) {
                    const def = AttachmentConstants.DEFAULT_IMAGE_SIZES.find((d) => d.size === size);

                    if (! size) {
                        throw new AttachmentSizeSetIsNotSpecifiedException(size);
                    }

                    result[size] = await this.awsHelper.presignedUrl({
                        s3ObjectId: closest(def.maxWidth, images).s3ObjectId,
                    });
                }

                return result;
            }
        }
    }
}
