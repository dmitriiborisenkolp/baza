import * as mime from 'mime';
import { AttachmentPayloadRequest, AttachmentPayloadResponse, AttachmentProcessResponse, BazaAttachmentStrategy } from '../baza-attachment-strategy';
import { Injectable } from '@nestjs/common';
import { AttachmentMimeTypeMismatchException } from '../exceptions/attachment-mime-type-mismatch.exception';
import { AttachmentFileIsTooSmallException } from '../exceptions/attachment-file-is-too-small.exception';
import { AttachmentFileIsTooLargeException } from '../exceptions/attachment-file-is-too-large.exception';
import { AttachmentDto, AttachmentFile, AttachmentNoneUpload, AttachmentRequest, AttachmentType } from '@scaliolabs/baza-core-shared';
import { AwsService } from '../../../../baza-aws/src';

@Injectable()
export class NoneStrategy implements BazaAttachmentStrategy {
    constructor(
        private readonly aws: AwsService,
    ) {}

    async validate(file: AttachmentFile, request:AttachmentRequest): Promise<void> {
        const payload: AttachmentNoneUpload = request.payload;

        const mimeType = file.mimetype;
        const fileSize = file.size;

        if (Array.isArray(payload.acceptMimeTypes) && payload.acceptMimeTypes.length > 0 && ! payload.acceptMimeTypes.includes(mimeType)) {
            throw new AttachmentMimeTypeMismatchException(`MIME mismatch, expected [${payload.acceptMimeTypes.join(', ')}], got [${mimeType}]`);
        }

        if (request.payload.minFileSize !== undefined && request.payload.minFileSize > 0 && fileSize < payload.minFileSize) {
            throw new AttachmentFileIsTooSmallException(`File should have at least ${payload.minFileSize} size`);
        }

        if (request.payload.maxFileSize !== undefined && request.payload.maxFileSize > 0 && fileSize > payload.maxFileSize) {
            throw new AttachmentFileIsTooLargeException(`File should have less than ${payload.maxFileSize} size`);
        }
    }

    async process(request: AttachmentRequest, tmpFile: string, tmpDir: string): Promise<AttachmentProcessResponse> {
        return {
            filesToUpload: [{
                filePath: `${tmpDir}/${tmpFile}`,
                mimeType: mime.getType(tmpFile),
            }],
        };
    }

    async payload(request: AttachmentRequest, uploadedPayload: AttachmentPayloadRequest): Promise<AttachmentPayloadResponse> {
        return {
            s3ObjectId: uploadedPayload.uploaded[0].s3ObjectId,
            payload: {
                type: AttachmentType.None,
            },
        }
    }

    async postprocess(input: AttachmentDto): Promise<AttachmentDto> {
        const processed: AttachmentDto = JSON.parse(JSON.stringify(input));

        if (input.s3ObjectId) {
            processed.presignedUrl = await this.aws.presignedUrl({
                s3ObjectId: input.s3ObjectId,
            });
        }

        return processed;
    }

    isProcessableWithAttachment(input: AttachmentDto): boolean {
        return input.payload.type === AttachmentType.None;
    }
}
