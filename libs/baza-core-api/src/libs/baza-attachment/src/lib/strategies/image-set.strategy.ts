import {
    AttachmentPayloadRequest,
    AttachmentPayloadResponse,
    AttachmentProcessFileToUpload,
    AttachmentProcessResponse,
    BazaAttachmentStrategy,
} from '../baza-attachment-strategy';
import { Injectable } from '@nestjs/common';
import { AttachmentMimeTypeMismatchException } from '../exceptions/attachment-mime-type-mismatch.exception';
import { AttachmentFileIsTooSmallException } from '../exceptions/attachment-file-is-too-small.exception';
import { AttachmentFileIsTooLargeException } from '../exceptions/attachment-file-is-too-large.exception';
import * as nodeUtil from 'util';
import * as sharp from 'sharp';
import * as mime from 'mime';
import {
    Application,
    AttachmentConstants,
    AttachmentDto,
    AttachmentFile,
    AttachmentImageSetDto,
    AttachmentImageSetPayload,
    AttachmentImageSetUpload,
    AttachmentImageSize,
    AttachmentImageType,
    AttachmentRequest,
    AttachmentType,
    generateRandomHexString,
} from '@scaliolabs/baza-core-shared';
import { AwsService } from '../../../../baza-aws/src';
import { AttachmentUnknownProcessImageTypeException } from '../exceptions/attachment-unknown-process-image-type.exception';
import { AttachmentSizeSetIsNotSpecifiedException } from '../exceptions/attachment-size-set-is-not-specified.exception';

const RANDOM_FILE_NAME_LENGTH = 8;

// eslint-disable-next-line @typescript-eslint/no-var-requires
const imageSize = nodeUtil.promisify(require('image-size'));

const ACCEPTED_MIME_TYPES = ['image/png', 'image/gif', 'image/jpeg'];

@Injectable()
export class ImageSetStrategy implements BazaAttachmentStrategy {
    constructor(private readonly aws: AwsService) {}

    async validate(file: AttachmentFile, request: AttachmentRequest): Promise<void> {
        const payload: AttachmentImageSetUpload = request.payload;

        const mimeType = file.mimetype;
        const fileSize = file.size;

        if (!ACCEPTED_MIME_TYPES.includes(mimeType)) {
            throw new AttachmentMimeTypeMismatchException(`MIME mismatch, expected [${ACCEPTED_MIME_TYPES.join(', ')}], got [${mimeType}]`);
        }

        if (request.payload.minFileSize !== undefined && request.payload.minFileSize > 0 && fileSize < payload.minFileSize) {
            throw new AttachmentFileIsTooSmallException(`File should have at least ${payload.minFileSize} size`);
        }

        if (request.payload.maxFileSize !== undefined && request.payload.maxFileSize > 0 && fileSize > payload.maxFileSize) {
            throw new AttachmentFileIsTooLargeException(`File should have less than ${payload.maxFileSize} size`);
        }
    }

    async process(request: AttachmentRequest, tmpFile: string, tmpDir: string): Promise<AttachmentProcessResponse> {
        const tmpFilePath = `${tmpDir}/${tmpFile}`;
        const payload: AttachmentImageSetUpload = request.payload;

        const sizes: {
            width: number;
            height: number;
        } = await imageSize(tmpFilePath);

        const sizesSet = payload.sizesSet || AttachmentConstants.DEFAULT_IMAGE_SIZES;

        const processImage = async (size: AttachmentImageSize) => {
            const newFileType = payload.type || AttachmentConstants.DEFAULT_IMAGE_FORMAT;
            const newFilePath = `${tmpDir}/${generateRandomHexString(RANDOM_FILE_NAME_LENGTH)}-${size}.${newFileType}`;

            const sharpImage = sharp(tmpFilePath);

            switch (newFileType) {
                default: {
                    throw new AttachmentUnknownProcessImageTypeException();
                }

                case undefined:
                case AttachmentImageType.PNG: {
                    sharpImage.png({
                        progressive: !payload.withoutProgressive,
                        quality: payload.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                    });

                    break;
                }

                case AttachmentImageType.JPEG: {
                    sharpImage
                        .flatten({
                            background: AttachmentConstants.DEFAULT_JPEG_BG_COLOR,
                        })
                        .jpeg({
                            progressive: !payload.withoutProgressive,
                            quality: payload.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                        });

                    break;
                }

                case AttachmentImageType.WEBP: {
                    sharpImage.webp({
                        quality: payload.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                    });

                    break;
                }
            }

            const sizeDef = sizesSet.find((def) => def.size === size);

            if (!sizeDef) {
                throw new AttachmentSizeSetIsNotSpecifiedException(size);
            }

            if (sizes.width > sizeDef.maxWidth) {
                sharpImage.resize(sizeDef.maxWidth);
            }

            if (payload.ratio) {
                const metadata = await sharpImage.metadata();

                const currentWidth = metadata.width;

                sharpImage.resize({
                    width: currentWidth,
                    height: Math.ceil(currentWidth / payload.ratio),
                    fit: 'cover',
                });
            }

            await sharpImage.toFile(newFilePath);

            const newImageSizes: {
                width: number;
                height: number;
            } = await imageSize(newFilePath);

            const response: AttachmentProcessFileToUpload = {
                id: size,
                filePath: newFilePath,
                mimeType: mime.getType(newFilePath),
                imageWidthPx: newImageSizes.width,
                imageHeightPx: newImageSizes.height,
            };

            return response;
        };

        const filesToUpload: Array<AttachmentProcessFileToUpload> = [];

        for (const size of Object.values(AttachmentImageSize)) {
            filesToUpload.push(await processImage(size));
        }

        return {
            filesToUpload,
        };
    }

    async payload(request: AttachmentRequest, uploadedPayload: AttachmentPayloadRequest): Promise<AttachmentPayloadResponse> {
        const payload: AttachmentImageSetPayload = {
            xs: {
                s3ObjectId: uploadedPayload.uploaded[0].s3ObjectId,
            },
            sm: {
                s3ObjectId: uploadedPayload.uploaded[1].s3ObjectId,
            },
            md: {
                s3ObjectId: uploadedPayload.uploaded[2].s3ObjectId,
            },
            xl: {
                s3ObjectId: uploadedPayload.uploaded[3].s3ObjectId,
            },
        };

        return {
            s3ObjectId: uploadedPayload.uploaded[3].s3ObjectId,
            payload: {
                type: AttachmentType.ImageSet,
                payload,
            },
        };
    }

    async postprocess(input: AttachmentDto, application = Application.WEB): Promise<AttachmentDto> {
        const processed: AttachmentDto = JSON.parse(JSON.stringify(input));
        const payload: AttachmentImageSetPayload = processed.payload.payload as any;

        if (application !== Application.CMS) {
            const newPayload: AttachmentImageSetDto = {
                xs: await this.aws.presignedUrl({ s3ObjectId: payload.xs.s3ObjectId }),
                sm: await this.aws.presignedUrl({ s3ObjectId: payload.sm.s3ObjectId }),
                md: await this.aws.presignedUrl({ s3ObjectId: payload.md.s3ObjectId }),
                xl: await this.aws.presignedUrl({ s3ObjectId: payload.xl.s3ObjectId }),
            };

            processed.payload.payload = newPayload;
        } else {
            payload.xs.url = await this.aws.presignedUrl({ s3ObjectId: payload.xs.s3ObjectId });
            payload.sm.url = await this.aws.presignedUrl({ s3ObjectId: payload.sm.s3ObjectId });
            payload.md.url = await this.aws.presignedUrl({ s3ObjectId: payload.md.s3ObjectId });
            payload.xl.url = await this.aws.presignedUrl({ s3ObjectId: payload.xl.s3ObjectId });
        }

        if (input.s3ObjectId) {
            processed.presignedUrl = await this.aws.presignedUrl({
                s3ObjectId: input.s3ObjectId,
            });
        }

        return processed;
    }

    isProcessableWithAttachment(input: AttachmentDto): boolean {
        return input.payload.type === AttachmentType.ImageSet;
    }
}
