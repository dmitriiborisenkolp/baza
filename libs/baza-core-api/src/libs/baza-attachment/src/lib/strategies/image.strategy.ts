import { AttachmentPayloadRequest, AttachmentPayloadResponse, AttachmentProcessResponse, BazaAttachmentStrategy } from '../baza-attachment-strategy';
import { Injectable } from '@nestjs/common';
import { AttachmentMimeTypeMismatchException } from '../exceptions/attachment-mime-type-mismatch.exception';
import { AttachmentFileIsTooSmallException } from '../exceptions/attachment-file-is-too-small.exception';
import { AttachmentFileIsTooLargeException } from '../exceptions/attachment-file-is-too-large.exception';
import * as nodeUtil from 'util';
import * as sharp from 'sharp';
import * as mime from 'mime';
import { AttachmentConstants, AttachmentDto, AttachmentFile, AttachmentImageType, AttachmentImageUpload, AttachmentRequest, AttachmentType, generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { AwsService } from '../../../../baza-aws/src';
import { AttachmentUnknownProcessImageTypeException } from '../exceptions/attachment-unknown-process-image-type.exception';

const RANDOM_FILE_NAME_LENGTH = 8;

// eslint-disable-next-line @typescript-eslint/no-var-requires
const imageSize = nodeUtil.promisify(require('image-size'));

const ACCEPTED_MIME_TYPES = [
    'image/png',
    'image/gif',
    'image/jpeg',
];

@Injectable()
export class ImageStrategy implements BazaAttachmentStrategy {
    constructor(
        private readonly aws: AwsService,
    ) {}

    async validate(file: AttachmentFile, request: AttachmentRequest): Promise<void> {
        const payload: AttachmentImageUpload = request.payload;

        const mimeType = file.mimetype;
        const fileSize = file.size;

        if (! ACCEPTED_MIME_TYPES.includes(mimeType)) {
            throw new AttachmentMimeTypeMismatchException(`MIME mismatch, expected [${ACCEPTED_MIME_TYPES.join(', ')}], got [${mimeType}]`);
        }

        if (request.payload.minFileSize !== undefined && request.payload.minFileSize > 0 && fileSize < payload.minFileSize) {
            throw new AttachmentFileIsTooSmallException(`File should have at least ${payload.minFileSize} size`);
        }

        if (request.payload.maxFileSize !== undefined && request.payload.maxFileSize > 0 && fileSize > payload.maxFileSize) {
            throw new AttachmentFileIsTooLargeException(`File should have less than ${payload.maxFileSize} size`);
        }
    }

    async process(request: AttachmentRequest, tmpFile: string, tmpDir: string): Promise<AttachmentProcessResponse> {
        const tmpFilePath = `${tmpDir}/${tmpFile}`;
        const payload: AttachmentImageUpload = request.payload;

        const sizes: {
            width: number;
            height: number;
        } = await imageSize(tmpFilePath);

        const newFileType = payload.type || AttachmentConstants.DEFAULT_IMAGE_FORMAT;
        const newFilePath = `${tmpDir}/${generateRandomHexString(RANDOM_FILE_NAME_LENGTH)}.${newFileType}`;

        const sharpImage = sharp(tmpFilePath);

        switch (newFileType) {
            default: {
                throw new AttachmentUnknownProcessImageTypeException();
            }

            case undefined:
            case AttachmentImageType.PNG: {
                sharpImage.png({
                    progressive: ! payload.withoutProgressive,
                    quality: payload.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                });

                break;
            }

            case AttachmentImageType.JPEG: {
                sharpImage.flatten({
                    background: AttachmentConstants.DEFAULT_JPEG_BG_COLOR,
                }).jpeg({
                    progressive: ! payload.withoutProgressive,
                    quality: payload.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                })

                break;
            }

            case AttachmentImageType.WEBP: {
                sharpImage.webp({
                    quality: payload.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                });

                break;
            }
        }

        if (payload.maxWidth) {
            if (sizes.width > payload.maxWidth) {
                sharpImage.resize(payload.maxWidth);
            }
        }

        await sharpImage
            .toFile(newFilePath);

        return {
            filesToUpload: [{
                filePath: newFilePath,
                mimeType: mime.getType(newFilePath),
            }],
        };
    }

    async payload(request: AttachmentRequest, uploadedPayload: AttachmentPayloadRequest): Promise<AttachmentPayloadResponse> {
        const sizes: {
            width: number;
            height: number;
        } = await imageSize(uploadedPayload.uploaded[0].filePath);

        return {
            s3ObjectId: uploadedPayload.uploaded[0].s3ObjectId,
            payload: {
                type: AttachmentType.Image,
                payload: {
                    width: sizes.width,
                    height: sizes.height,
                },
            },
        }
    }

    async postprocess(input: AttachmentDto): Promise<AttachmentDto> {
        const processed: AttachmentDto = JSON.parse(JSON.stringify(input));

        if (input.s3ObjectId) {
            processed.presignedUrl = await this.aws.presignedUrl({
                s3ObjectId: input.s3ObjectId,
            });
        }

        return processed;
    }

    isProcessableWithAttachment(input: AttachmentDto): boolean {
        return input.payload.type === AttachmentType.Image;
    }
}
