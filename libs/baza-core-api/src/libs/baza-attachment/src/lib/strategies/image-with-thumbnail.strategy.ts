import { AttachmentPayloadRequest, AttachmentPayloadResponse, AttachmentProcessResponse, BazaAttachmentStrategy } from '../baza-attachment-strategy';
import { Injectable } from '@nestjs/common';
import { AttachmentMimeTypeMismatchException } from '../exceptions/attachment-mime-type-mismatch.exception';
import { AttachmentFileIsTooSmallException } from '../exceptions/attachment-file-is-too-small.exception';
import { AttachmentFileIsTooLargeException } from '../exceptions/attachment-file-is-too-large.exception';
import * as nodeUtil from 'util';
import * as sharp from 'sharp';
import * as mime from 'mime';
import { ATTACHMENT_IMAGE_MIME_TYPES, AttachmentDto, AttachmentFile, AttachmentImageType, AttachmentImageUpload, AttachmentImageWithThumbnailPayload, AttachmentImageWithThumbnailUpload, AttachmentRequest, AttachmentThumbnailRequest, AttachmentType, generateRandomHexString, AttachmentConstants } from '@scaliolabs/baza-core-shared';
import { AwsService } from '../../../../baza-aws/src';
import { AttachmentUnknownProcessImageTypeException } from '../exceptions/attachment-unknown-process-image-type.exception';

const RANDOM_FILE_NAME_LENGTH = 8;

// eslint-disable-next-line @typescript-eslint/no-var-requires
const imageSize = nodeUtil.promisify(require('image-size'));

const ACCEPTED_MIME_TYPES = ATTACHMENT_IMAGE_MIME_TYPES;

/**
 * Image-With-Thumbnails strategy is designed to use with <picture>/<source> modern way to work with images.
 * API generates set of sources (image variants), and Web should render all available sources inside <picture> tag
 * group. Same <picture> group should be used for every layout (desktop, tablet, mobile, ...) - everything else
 * will be correctly handled by browser.
 *
 * Additional, the strategy generates 2 sets of images:
 *
 * - `original` - which should be used to display "big" image
 * - `thumbnail` - limited set of images which should be used for thumbnails-like containers.
 */
@Injectable()
export class ImageWithThumbnailStrategy implements BazaAttachmentStrategy {
    constructor(
        private readonly aws: AwsService,
    ) {}

    async validate(file: AttachmentFile, request: AttachmentRequest): Promise<void> {
        const payload: AttachmentImageUpload = request.payload;

        const mimeType = file.mimetype;
        const fileSize = file.size;

        if (! ACCEPTED_MIME_TYPES.includes(mimeType)) {
            throw new AttachmentMimeTypeMismatchException(`MIME mismatch, expected [${ACCEPTED_MIME_TYPES.join(', ')}], got [${mimeType}]`);
        }

        if (request.payload.minFileSize !== undefined && request.payload.minFileSize > 0 && fileSize < payload.minFileSize) {
            throw new AttachmentFileIsTooSmallException(`File should have at least ${payload.minFileSize} size`);
        }

        if (request.payload.maxFileSize !== undefined && request.payload.maxFileSize > 0 && fileSize > payload.maxFileSize) {
            throw new AttachmentFileIsTooLargeException(`File should have less than ${payload.maxFileSize} size`);
        }
    }

    async process(
        request: AttachmentRequest,
        tmpFile: string,
        tmpDir: string,
    ): Promise<AttachmentProcessResponse> {
        const tmpFilePath = `${tmpDir}/${tmpFile}`;

        const originalImageSizes: {
            width: number;
            height: number;
        } = await imageSize(tmpFilePath);

        const payload: AttachmentImageWithThumbnailUpload = request.payload as any;

        const processImage: (thumbnailRequest: AttachmentThumbnailRequest) => Promise<AttachmentProcessResponse> = async (thumbnailRequest) => {
            const newFileType = thumbnailRequest.type || AttachmentConstants.DEFAULT_IMAGE_FORMAT;
            const newFilePath = `${tmpDir}/${generateRandomHexString(RANDOM_FILE_NAME_LENGTH)}.${newFileType.toLowerCase()}`;

            const sharpImage = sharp(tmpFilePath);

            const imageWidthPx = originalImageSizes.width;
            const imageHeightPx = originalImageSizes.height;

            switch (newFileType) {
                default: {
                    throw new AttachmentUnknownProcessImageTypeException();
                }

                case undefined:
                case AttachmentImageType.PNG: {
                    sharpImage.png({
                        progressive: ! payload.withoutProgressive,
                        quality: thumbnailRequest.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                    });

                    break;
                }

                case AttachmentImageType.JPEG: {
                    sharpImage.flatten({
                        background: AttachmentConstants.DEFAULT_JPEG_BG_COLOR,
                    }).jpeg({
                        progressive: ! payload.withoutProgressive,
                        quality: thumbnailRequest.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                    })

                    break;
                }

                case AttachmentImageType.WEBP: {
                    sharpImage.webp({
                        quality: thumbnailRequest.quality || AttachmentConstants.DEFAULT_IMAGE_QUALITY,
                    });

                    break;
                }
            }

            if (thumbnailRequest.maxWidth || thumbnailRequest.maxHeight) {
                sharpImage.resize({
                    width: thumbnailRequest.maxWidth || imageWidthPx,
                    height: thumbnailRequest.maxHeight || imageHeightPx,
                    fit: thumbnailRequest.fit || 'cover',
                    ...thumbnailRequest,
                });
            }

            await sharpImage.toFile(newFilePath);

            const newImageSizes: {
                width: number;
                height: number;
            } = await imageSize(newFilePath);

            return {
                filesToUpload: [{
                    id: thumbnailRequest.id,
                    filePath: newFilePath,
                    mimeType: mime.getType(newFilePath),
                    imageWidthPx: newImageSizes.width,
                    imageHeightPx: newImageSizes.height,
                }],
            };
        };

        const processedImages = async (requests: Array<AttachmentThumbnailRequest>) => {
            const response: AttachmentProcessResponse = {
                filesToUpload: [],
            };

            for (const attachmentRequest of requests) {
                const attachmentResponse = await processImage(attachmentRequest);

                response.filesToUpload.push(
                    ...attachmentResponse.filesToUpload,
                );
            }

            return response;
        };

        return {
            filesToUpload: [
                ...(await processedImages(payload.original)).filesToUpload,
                ...(await processedImages(payload.thumbnails)).filesToUpload,
            ],
        };
    }

    async payload(
        request: AttachmentRequest,
        uploadPayload: AttachmentPayloadRequest,
    ): Promise<AttachmentPayloadResponse> {
        const sizes: {
            width: number;
            height: number;
        } = await imageSize(uploadPayload.uploaded[0].filePath);

        const payload: AttachmentImageWithThumbnailUpload = request.payload as any;

        const original = uploadPayload.uploaded.slice(0, payload.original.length);
        const thumbnails = uploadPayload.uploaded.slice(payload.original.length, payload.original.length + payload.thumbnails.length);

        return {
            s3ObjectId: uploadPayload.uploaded[0].s3ObjectId,
            payload: {
                type: AttachmentType.ImageWithThumbnail,
                payload: {
                    width: sizes.width,
                    height: sizes.height,
                    original: original.map((image) => ({
                        id: image.id,
                        mime: image.mime,
                        s3ObjectId: image.s3ObjectId,
                        width: image.imageWidthPx,
                        height: image.imageHeightPx,
                    })),
                    thumbnails: thumbnails.map((image) => ({
                        id: image.id,
                        mime: image.mime,
                        s3ObjectId: image.s3ObjectId,
                        width: image.imageWidthPx,
                        height: image.imageHeightPx,
                    })),
                },
            },
        }
    }

    async postprocess(input: AttachmentDto): Promise<AttachmentDto> {
        const processed: AttachmentDto = JSON.parse(JSON.stringify(input));

        if (input.s3ObjectId) {
            processed.presignedUrl = await this.aws.presignedUrl({
                s3ObjectId: input.s3ObjectId,
            });
        }

        const payload: AttachmentImageWithThumbnailPayload = input.payload.payload as any;

        for (const image of [...payload.original, ...payload.thumbnails]) {
            if (image.s3ObjectId) {
                image.url = await this.aws.presignedUrl({
                    s3ObjectId: image.s3ObjectId,
                });
            }
        }

        return processed;
    }

    isProcessableWithAttachment(input: AttachmentDto): boolean {
        return input.payload.type === AttachmentType.ImageWithThumbnail;
    }
}
