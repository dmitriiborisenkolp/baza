import { Request } from 'express';
import { EnvService } from '../../../../baza-env/src';
import { BazaCommonEnvironments } from '../../../../baza-common/src';
import { BazaLogger } from '../../../../baza-logger/src';

export interface BazaDebugLogPayload {
    handlerName: string;
    expressRequest: Request;
    exception: any;
    withJwt?: boolean;
}

export function bazaDebugLog(payload: BazaDebugLogPayload): void {
    if (
        EnvService.hasInstance() &&
        EnvService.getInstance().getAsBoolean<BazaCommonEnvironments.ExceptionHandlerEnvironment>('BAZA_DEBUG_ERROR_LOG', {
            defaultValue: false,
        })
    ) {
        bazaPrintDebugLog({
            ...payload,
            withJwt:
                payload.withJwt === undefined
                    ? EnvService.getInstance().getAsBoolean<BazaCommonEnvironments.ExceptionHandlerEnvironment>(
                          'BAZA_DEBUG_ERROR_LOG_JWT',
                          {
                              defaultValue: false,
                          },
                      )
                    : payload.withJwt,
        });
    }
}

export function bazaPrintDebugLog(payload: BazaDebugLogPayload): void {
    const logger = new BazaLogger();

    const requestUrl = payload.expressRequest.url;
    const requestUA = payload.expressRequest.header('User-Agent');
    const requestIp = payload.expressRequest.headers['x-forwarded-for'] || payload.expressRequest.connection.remoteAddress;
    const requestJwt = payload.withJwt ? payload.expressRequest.header('Authorization') : undefined;

    logger.setContext('baza-debug-log');

    logger.error({
        requestUrl,
        requestUA,
        requestIp,
        requestJwt,
        requestBody: payload.expressRequest.body,
        error: payload.exception,
    });
}
