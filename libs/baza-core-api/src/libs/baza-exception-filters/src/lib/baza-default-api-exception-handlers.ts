import { NestjsValidationExceptionHandler } from './exception-handlers/nestjs-validation.exception-handler';
import { HttpExceptionExceptionHandler } from './exception-handlers/http-exception.exception-handler';
import { JsExceptionExceptionHandler } from './exception-handlers/js-exception.exception-handler';
import { I18nApiService } from '../../../baza-i18n/src';

export const bazaDefaultApiExceptionHandlers = (i18n: I18nApiService) => [
    new NestjsValidationExceptionHandler(),
    new HttpExceptionExceptionHandler(i18n),
    new JsExceptionExceptionHandler(),
];
