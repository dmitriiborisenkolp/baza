import { ArgumentsHost } from '@nestjs/common';

export interface BazaExceptionHandler<T extends Error = Error> {
  isMatched(exception: T): boolean;
  handle(exception: T, host: ArgumentsHost): void;
}
