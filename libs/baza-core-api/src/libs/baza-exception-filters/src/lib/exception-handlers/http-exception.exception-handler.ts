import { ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { bazaDebugLog } from '../exception-util/baza-debug-log.util';
import { BazaExceptionHandler } from '../baza-exception-handler';
import { Application, BAZA_COMMON_I18N_CONFIG, BazaCommonErrorCodes, BazaHttpHeaders, ProjectLanguage } from '@scaliolabs/baza-core-shared';
import { Request as ExpressRequest, Response as ExpressResponse } from 'express';
import { I18nApiService, I18nApiTranslateContext } from '../../../../baza-i18n/src';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class HttpExceptionExceptionHandler<T extends HttpException> implements BazaExceptionHandler {
    constructor(
        private readonly i18n: I18nApiService,
    ) {}

    isMatched(exception: Error): boolean {
        return exception instanceof HttpException;
    }

    handle(exception: Error, host: ArgumentsHost): void {
        const ctx = host.switchToHttp();
        const response: ExpressResponse = ctx.getResponse();
        const request: ExpressRequest = ctx.getRequest();

        const status =
            exception instanceof HttpException
                ? exception.getStatus()
                : HttpStatus.INTERNAL_SERVER_ERROR;

        const code =
            exception instanceof BazaAppException
                ? exception.errorCode
                : BazaCommonErrorCodes.BazaInternalServerError;

        const details =
            exception instanceof BazaAppException
                ? exception.details
                : undefined;

        const translatedMessage = (() => {
            const i18n = this.i18n;

            if (i18n && (exception instanceof BazaAppException)) {
                const path = `__errorCodes.${code}`;
                const context: I18nApiTranslateContext = {
                    app: Application.API,
                    language: (request.header(BazaHttpHeaders.HTTP_HEADER_I18N_LANGUAGE) || BAZA_COMMON_I18N_CONFIG.defaultLanguage).toLowerCase() as ProjectLanguage,
                };

                if (i18n.has(path, context)) {
                    return i18n.get(path, context);
                } else {
                    return exception.message;
                }
            } else {
                return exception.message;
            }
        })();

        response.status(status).json({
            statusCode: status,
            timestamp: new Date().toISOString(),
            path: request.url,
            code: code,
            message: translatedMessage,
            details,
        });

        bazaDebugLog({
            handlerName: 'BazaHttpExceptionExceptionHandler',
            expressRequest: request,
            exception,
        });
    }
}
