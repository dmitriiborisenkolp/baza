import { ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { BazaExceptionHandler } from '../baza-exception-handler';
import { bazaDebugLog } from '../exception-util/baza-debug-log.util';
import { BazaCommonErrorCodes } from '@scaliolabs/baza-core-shared';
import { BAZA_DEFAULT_EXCEPTION_ERROR_MESSAGE } from '../../../../baza-exceptions/src';

const JS_ERROR_MESSAGE = BAZA_DEFAULT_EXCEPTION_ERROR_MESSAGE;

export class JsExceptionExceptionHandler<T = Error> implements BazaExceptionHandler<Error> {
    isMatched(exception: Error): boolean {
        return true;
    }

    handle(exception: Error, host: ArgumentsHost): void {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();

        const status =
            exception instanceof HttpException
                ? exception.getStatus()
                : HttpStatus.INTERNAL_SERVER_ERROR;

        const code = BazaCommonErrorCodes.BazaJSError;

        response.status(status).json({
            statusCode: status,
            timestamp: new Date().toISOString(),
            path: request.url,
            code: code,
            message: exception.message,
            jsMessage: JS_ERROR_MESSAGE,
        });

        bazaDebugLog({
            handlerName: 'BazaHttpExceptionExceptionHandler',
            expressRequest: request,
            exception,
        });
    }
}
