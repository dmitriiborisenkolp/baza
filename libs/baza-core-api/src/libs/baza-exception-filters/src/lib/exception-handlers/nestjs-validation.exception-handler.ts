import { ArgumentsHost, BadRequestException, HttpStatus } from '@nestjs/common';
import { bazaDebugLog } from '../exception-util/baza-debug-log.util';
import { BazaExceptionHandler } from '../baza-exception-handler';
import { BazaCommonErrorCodes, capitalizeFirstLetter } from '@scaliolabs/baza-core-shared';

export class NestjsValidationExceptionHandler<T extends BadRequestException> implements BazaExceptionHandler {
    isMatched(exception: T): boolean {
        return exception instanceof BadRequestException
            && !!(exception as any).response
            && !!(exception as any).response.message
            && Array.isArray((exception as any).response.message)
            && (exception as any).response.message.length > 0;
    }

    handle(exception: T, host: ArgumentsHost): void {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();

        const status =
            exception instanceof BadRequestException
                ? exception.getStatus()
                : HttpStatus.BAD_REQUEST;

        const code = BazaCommonErrorCodes.BazaValidationError;
        const validationDetails = (exception as any).response.message;

        response.status(status).json({
            statusCode: status,
            timestamp: new Date().toISOString(),
            path: request.url,
            code: code,
            message: capitalizeFirstLetter(validationDetails[0]),
            validationDetails,
        });

        bazaDebugLog({
            handlerName: 'BazaHttpExceptionExceptionHandler',
            expressRequest: request,
            exception,
        });
    }
}
