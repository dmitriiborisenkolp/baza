import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaPasswordNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BAZA_PASSWORD_MAX_LENGTH, BAZA_PASSWORD_MIN_LENGTH } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-password/api/integration-tests/baza-password-api.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccess = new BazaPasswordNodeAccess(http);

    it('correctly returns resources about password validation', async () => {
        const response = await dataAccess.resources();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.minLength).toBe(BAZA_PASSWORD_MIN_LENGTH);
        expect(response.maxLength).toBe(BAZA_PASSWORD_MAX_LENGTH);
        expect(Array.isArray(response.validators)).toBeTruthy();
    });

    it('correctly invalidate "123456" password', async () => {
        const response = await dataAccess.validate({
            password: '123456',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeFalsy();
    });

    it('correctly invalidate "aA123456" password', async () => {
        const response = await dataAccess.validate({
            password: 'aA123456',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeFalsy();
    });

    it('correctly validate "Scalio#1337!" password', async () => {
        const response = await dataAccess.validate({
            password: 'Scalio#1337!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeTruthy();
    });
});
