import { Module } from '@nestjs/common';
import { BazaPasswordController } from './controllers/baza-password.controller';
import { BazaPasswordService } from './services/baza-password.service';
import { BazaEnvModule } from '../../../baza-env/src';

@Module({
    imports: [BazaEnvModule],
    controllers: [BazaPasswordController],
    providers: [BazaPasswordService],
    exports: [BazaPasswordService],
})
export class BazaPasswordApiModule {}
