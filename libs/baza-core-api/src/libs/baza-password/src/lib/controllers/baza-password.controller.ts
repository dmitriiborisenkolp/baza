import { Body, Controller, Get, Post } from '@nestjs/common';
import {
    BazaPasswordEndpoint,
    BazaPasswordEndpointPaths,
    BazaPasswordResourcesDto,
    BazaPasswordValidateDto,
    BazaPasswordValidateRequest,
} from '@scaliolabs/baza-core-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { BazaPasswordService } from '../services/baza-password.service';

@Controller()
@ApiTags(CoreOpenApiTags.BazaPassword)
export class BazaPasswordController implements BazaPasswordEndpoint {
    constructor(private readonly service: BazaPasswordService) {}

    @Get(BazaPasswordEndpointPaths.resources)
    @ApiOperation({
        summary: 'resources',
        description: 'Returns models/enums used for password validation',
    })
    @ApiOkResponse({
        type: BazaPasswordResourcesDto,
    })
    async resources(): Promise<BazaPasswordResourcesDto> {
        return this.service.resources();
    }

    @Post(BazaPasswordEndpointPaths.validate)
    @ApiOperation({
        summary: 'validate',
        description: 'Validate password',
    })
    @ApiOkResponse({
        type: BazaPasswordValidateDto,
    })
    async validate(@Body() request: BazaPasswordValidateRequest): Promise<BazaPasswordValidateDto> {
        return this.service.validate(request);
    }
}
