import { Injectable } from '@nestjs/common';
import { BazaPasswordResourcesDto, BazaPasswordValidateDto, BazaPasswordValidateRequest } from '@scaliolabs/baza-core-shared';
import { bazaValidatePassword } from '@scaliolabs/baza-core-shared';
import { bazaValidatePasswordResources } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaPasswordService {
    async resources(): Promise<BazaPasswordResourcesDto> {
        return bazaValidatePasswordResources();
    }

    async validate(request: BazaPasswordValidateRequest): Promise<BazaPasswordValidateDto> {
        return bazaValidatePassword(request.password);
    }
}
