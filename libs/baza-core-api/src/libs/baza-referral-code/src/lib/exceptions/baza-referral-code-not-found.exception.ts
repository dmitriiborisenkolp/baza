import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaReferralCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaReferralCodeNotFoundException extends BazaAppException {
    constructor() {
        super(BazaReferralCodeErrorCodes.BazaReferralCodeNotFound, 'Referral Code not found', HttpStatus.NOT_FOUND);
    }
}
