import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaReferralCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaReferralCodeIsNotAvailableAnymoreException extends BazaAppException {
    constructor() {
        super(BazaReferralCodeErrorCodes.BazaReferralCodeIsNotAvailableAnymore, 'Referral code is not available anymore', HttpStatus.NOT_FOUND);
    }
}
