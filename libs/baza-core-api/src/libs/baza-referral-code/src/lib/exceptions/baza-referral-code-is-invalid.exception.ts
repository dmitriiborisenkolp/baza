import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaReferralCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaReferralCodeIsInvalidException extends BazaAppException {
    constructor() {
        super(BazaReferralCodeErrorCodes.BazaReferralCodeIsInvalid, 'Referral Code is empty', HttpStatus.BAD_REQUEST);
    }
}
