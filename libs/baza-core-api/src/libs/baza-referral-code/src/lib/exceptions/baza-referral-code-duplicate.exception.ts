import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaReferralCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaReferralCodeDuplicateException extends BazaAppException {
    constructor() {
        super(BazaReferralCodeErrorCodes.BazaReferralCodeDuplicate, 'Referral Code or Invite Code with same code already exists', HttpStatus.CONFLICT);
    }
}
