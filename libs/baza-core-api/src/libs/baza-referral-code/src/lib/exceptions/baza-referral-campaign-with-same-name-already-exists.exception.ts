import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaReferralCampaignErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaReferralCampaignWithSameNameAlreadyExistsException extends BazaAppException {
    constructor() {
        super(BazaReferralCampaignErrorCodes.BazaReferralCampaignWithSameNameAlreadyExists, 'Referral Campaign with same name already exists', HttpStatus.CONFLICT);
    }
}
