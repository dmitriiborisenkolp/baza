import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaReferralCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaReferralCodeNoAssociationsSetException extends BazaAppException {
    constructor() {
        super(BazaReferralCodeErrorCodes.BazaReferralCodeNoAssociationsSetException, 'Referral Code no associations set', HttpStatus.BAD_REQUEST);
    }
}
