import { BazaAppException } from '../../../../baza-exceptions/src';
import { BazaReferralCampaignErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaReferralCampaignNotFoundException extends BazaAppException {
    constructor() {
        super(BazaReferralCampaignErrorCodes.BazaReferralCampaignNotFound, 'Referral Campaign not found', HttpStatus.NOT_FOUND);
    }
}
