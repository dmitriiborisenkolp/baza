import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BazaReferralCampaignEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: new Date(),
        nullable: false,
    })
    dateCreatedAt: Date = new Date();

    @Column({
        nullable: true,
    })
    dateLastJoinedByReferralCode?: Date;

    @Column()
    name: string;

    @Column({
        default: false,
        nullable: false,
    })
    isActive?: boolean;
}
