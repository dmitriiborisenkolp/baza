import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '../../../../baza-account/src';
import { BazaReferralCodeEntity } from './baza-referral-code.entity';
import { Application } from '@scaliolabs/baza-core-shared';

@Entity()
export class BazaReferralCodeUsageEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    dateJoinedAt: Date = new Date();

    @JoinColumn()
    @ManyToOne(() => BazaReferralCodeEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    referralCode?: BazaReferralCodeEntity;

    @JoinColumn()
    @ManyToOne(() => AccountEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    joinedAccount?: AccountEntity;

    @Column({
        type: 'varchar',
    })
    joinedWithClient: Application;
}
