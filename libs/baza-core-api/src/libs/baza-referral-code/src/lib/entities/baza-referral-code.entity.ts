import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '../../../../baza-account/src';
import { BazaReferralCampaignEntity } from './baza-referral-campaign.entity';

@Entity()
export class BazaReferralCodeEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: false,
        nullable: false,
        type: 'boolean',
    })
    isActive = false;

    @Column({
        default: false,
        nullable: false,
        type: 'boolean',
    })
    isDefault = false;

    @Column({
        default: new Date(),
        nullable: false,
    })
    dateCreatedAt: Date = new Date();

    @Column({
        nullable: true,
    })
    dateLastUsedAt?: Date;

    @JoinColumn()
    @ManyToOne(() => AccountEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    associatedWithAccount?: AccountEntity;

    @JoinColumn()
    @ManyToOne(() => BazaReferralCampaignEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    associatedWithCampaign?: BazaReferralCampaignEntity;

    @Column()
    code: string;

    @Column()
    codeDisplay: string;

    @Column({
        default: 0,
        nullable: false,
        type: 'int',
    })
    usedCount = 0;
}
