import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ApiCqrsEvent } from '@scaliolabs/baza-core-shared';
import { AccountApiEvent, AccountApiEvents, AccountRole } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeService } from '../services/baza-referral-code.service';
import { BazaAccountRepository } from '../../../../baza-account/src';
import { cqrs } from '../../../../baza-common/src';

@EventsHandler(ApiCqrsEvent)
export class CreateReferralCodeOnAccountSignupEventHandler implements IEventHandler<ApiCqrsEvent<AccountApiEvent, AccountApiEvents>> {
    constructor(private readonly account: BazaAccountRepository, private readonly referralCodes: BazaReferralCodeService) {}

    handle(event: ApiCqrsEvent<AccountApiEvent, AccountApiEvents>): void {
        cqrs(CreateReferralCodeOnAccountSignupEventHandler.name, async () => {
            switch (event.apiEvent.event.topic) {
                case AccountApiEvent.BazaAccountRegistered: {
                    if (event.apiEvent.event.payload.accountRole !== AccountRole.User) {
                        return;
                    }

                    const account = await this.account.findActiveAccountWithId(event.apiEvent.event.payload.accountId);

                    if (account) {
                        await this.referralCodes.generateReferralCodeForAccount(account);
                    }

                    break;
                }
            }
        });
    }
}
