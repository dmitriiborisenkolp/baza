import { forwardRef, Module } from '@nestjs/common';
import { BazaReferralCampaignsE2eFixture } from './integration-tests/fixtures/baza-referral-campaigns-e2e-fixture.service';
import { BazaReferralCodesE2eFixture } from './integration-tests/fixtures/baza-referral-codes-e2e-fixture.service';
import { BazaReferralCodesApiModule } from './baza-referral-codes-api.module';

const E2E_FIXTURES = [
    BazaReferralCampaignsE2eFixture,
    BazaReferralCodesE2eFixture,
];

@Module({
    imports: [
        forwardRef(() => BazaReferralCodesApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaReferralCodesE2eModule {}
