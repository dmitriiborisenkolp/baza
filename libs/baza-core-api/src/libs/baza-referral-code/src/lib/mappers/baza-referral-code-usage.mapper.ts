import { Injectable } from '@nestjs/common';
import { BazaReferralCodeUsageEntity } from '../entities/baza-referral-code-usage.entity';
import { BazaReferralCodeUsageDto } from '@scaliolabs/baza-core-shared';
import { BazaAccountMapper } from '../../../../baza-account/src';
import { BazaReferralCodeMapper } from './baza-referral-code.mapper';

@Injectable()
export class BazaReferralCodeUsageMapper {
    constructor(
        private readonly accountMapper: BazaAccountMapper,
        private readonly referralCodeMapper: BazaReferralCodeMapper,
    ) {}

    async entityToDTO(input: BazaReferralCodeUsageEntity): Promise<BazaReferralCodeUsageDto> {
        return {
            dateJoinedAt: input.dateJoinedAt.toISOString(),
            referralCode: await this.referralCodeMapper.entityToDTO(input.referralCode),
            joinedAccount: this.accountMapper.entityToDto(input.joinedAccount),
            joinedWithClient: input.joinedWithClient,
        };
    }

    async entitiesToDTOs(input: Array<BazaReferralCodeUsageEntity>): Promise<Array<BazaReferralCodeUsageDto>> {
        const results: Array<BazaReferralCodeUsageDto> = [];

        for (const entity of input) {
            results.push(await this.entityToDTO(entity));
        }

        return results;
    }
}
