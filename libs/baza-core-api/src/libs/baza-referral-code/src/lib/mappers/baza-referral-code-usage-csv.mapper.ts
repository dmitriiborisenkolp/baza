import { Injectable } from '@nestjs/common';
import { BazaReferralCodeUsageEntity } from '../entities/baza-referral-code-usage.entity';
import { BazaReferralCodeUsageCsvDto } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';

@Injectable()
export class BazaReferralCodeUsageCsvMapper {
    entityToDTO(input: BazaReferralCodeUsageEntity): BazaReferralCodeUsageCsvDto {
        return {
            dateJoinedAt: moment(input.dateJoinedAt).format('LLL'),
            referralCode: input.referralCode.codeDisplay,
            joinedAccount: `${input.joinedAccount.email} (${input.joinedAccount.fullName})`,
            joinedWithClient: input.joinedWithClient,
        };
    }

    entitiesToDTOs(input: Array<BazaReferralCodeUsageEntity>): Array<BazaReferralCodeUsageCsvDto> {
        return input.map((e) => this.entityToDTO(e));
    }
}
