import { Injectable } from '@nestjs/common';
import { AccountEntity, BazaAccountMapper } from '../../../../baza-account/src';
import { BazaReferralCodeMapper } from './baza-referral-code.mapper';
import { BazaReferralCodeService } from '../services/baza-referral-code.service';
import { BazaReferralAccountDto, BazaReferralCodeAccountMetadata } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaReferralAccountMapper {
    constructor(
        private readonly accountMapper: BazaAccountMapper,
        private readonly referralCodeMapper: BazaReferralCodeMapper,
        private readonly referralCodeService: BazaReferralCodeService,
    ) {
    }

    async entityToDTO(input: AccountEntity): Promise<BazaReferralAccountDto> {
        const referralCode = await this.referralCodeService.findReferralCodeOfAccount(input);

        const metadata: BazaReferralCodeAccountMetadata = input.metadata;

        return {
            ...this.accountMapper.entityToDto(input),
            referralCode: referralCode
                ? await this.referralCodeMapper.entityToDTO(referralCode)
                : undefined,
            signedUpWithReferralCode: metadata.signedUpWithReferralCode
                ? metadata.signedUpWithReferralCode
                : undefined,
        };
    }

    async entitiesToDTOs(input: Array<AccountEntity>): Promise<Array<BazaReferralAccountDto>> {
        const result: Array<BazaReferralAccountDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
