import { Injectable } from '@nestjs/common';
import { BazaAccountCsvMapper, AccountEntity } from '../../../../baza-account/src';
import { BazaReferralCodeService } from '../services/baza-referral-code.service';
import { BazaReferralAccountCsvDto, BazaReferralCodeAccountMetadata } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaReferralAccountCsvMapper {
    constructor(private readonly accountCsvMapper: BazaAccountCsvMapper, private readonly referralCodeService: BazaReferralCodeService) {}

    async entityToDTO(input: AccountEntity): Promise<BazaReferralAccountCsvDto> {
        const accountCsv = this.accountCsvMapper.entityToCSVRow(input);

        const referralCode = await this.referralCodeService.findReferralCodeOfAccount(input);

        const metadata: BazaReferralCodeAccountMetadata = input.metadata;

        return {
            ...accountCsv,
            referralCode: referralCode ? referralCode.codeDisplay : undefined,
            signedUpWithReferralCode: metadata.signedUpWithReferralCode ? metadata.signedUpWithReferralCode : undefined,
        };
    }

    async entitiesToDTOs(input: Array<AccountEntity>): Promise<Array<BazaReferralAccountCsvDto>> {
        const result: Array<BazaReferralAccountCsvDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
