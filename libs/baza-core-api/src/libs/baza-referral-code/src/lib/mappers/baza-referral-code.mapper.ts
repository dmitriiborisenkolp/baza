import { Injectable } from '@nestjs/common';
import { BazaReferralCodeEntity } from '../entities/baza-referral-code.entity';
import { BazaReferralCodeDto } from '@scaliolabs/baza-core-shared';
import { BazaAccountMapper } from '../../../../baza-account/src';
import { BazaReferralCampaignMapper } from './baza-referral-campaign.mapper';

@Injectable()
export class BazaReferralCodeMapper {
    constructor(
        private readonly accountMapper: BazaAccountMapper,
        private readonly campaignMapper: BazaReferralCampaignMapper,
    ) {}

    async entityToDTO(input: BazaReferralCodeEntity): Promise<BazaReferralCodeDto> {
        return {
            id: input.id,
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            dateLastUsedAt: input.dateLastUsedAt
                ? input.dateLastUsedAt.toISOString()
                : undefined,
            associatedWithAccount: input.associatedWithAccount
                ? this.accountMapper.entityToDto(input.associatedWithAccount)
                : undefined,
            associatedWithCampaign: input.associatedWithCampaign
                ? await this.campaignMapper.entityToDTO(input.associatedWithCampaign)
                : undefined,
            code: input.code,
            codeDisplay: input.codeDisplay,
            usedCount: input.usedCount,
            isActive: input.associatedWithAccount
                ? (input.associatedWithAccount.isDeactivated ? false : input.isActive)
                : input.isActive,
            isDefault: input.isDefault,
        };
    }

    async entitiesToDTOs(input: Array<BazaReferralCodeEntity>): Promise<Array<BazaReferralCodeDto>> {
        const results: Array<BazaReferralCodeDto> = [];

        for (const entity of input) {
            results.push(await this.entityToDTO(entity));
        }

        return results;
    }
}
