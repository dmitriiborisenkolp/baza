import { Injectable } from '@nestjs/common';
import { BazaReferralCampaignEntity } from '../entities/baza-referral-campaign.entity';
import { BazaReferralCampaignDto } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeRepository } from '../repositories/baza-referral-code.repository';

@Injectable()
export class BazaReferralCampaignMapper {
    constructor(
        private readonly codesRepository: BazaReferralCodeRepository,
    ) {
    }

    async entityToDTO(input: BazaReferralCampaignEntity): Promise<BazaReferralCampaignDto> {
        const codes = await this.codesRepository.findByCampaign(input.id);

        let usedCount = 0;

        for (const code of codes) {
            usedCount += code.usedCount;
        }

        return {
            id: input.id,
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            dateLastJoinedByReferralCode: input.dateLastJoinedByReferralCode
                ? input.dateLastJoinedByReferralCode.toISOString()
                : undefined,
            name: input.name,
            isActive: input.isActive,
            usedCount,
        };
    }

    async entitiesToDTOs(input: Array<BazaReferralCampaignEntity>): Promise<Array<BazaReferralCampaignDto>> {
        const results: Array<BazaReferralCampaignDto> = [];

        for (const entity of input) {
            results.push(await this.entityToDTO(entity));
        }

        return results;
    }
}
