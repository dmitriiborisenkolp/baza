import { Injectable } from '@nestjs/common';
import {
    bazaNormalizeReferralCode,
    bazaNormalizeReferralCodeDisplay,
    BazaReferralCodeCmsCreateRequest,
    BazaReferralCodeCmsDeleteRequest,
    BazaReferralCodeCmsEditRequest,
    BazaReferralCodeCmsEntityBody,
    BazaReferralCodeCmsExportListToCSVRequest,
    BazaReferralCodeCmsListRequest,
    BazaReferralCodeCmsListResponse,
    BazaReferralCodeDto,
} from '@scaliolabs/baza-core-shared';
import { REFERRAL_CODE_RELATIONS, BazaReferralCodeRepository } from '../repositories/baza-referral-code.repository';
import { BazaReferralCampaignRepository } from '../repositories/baza-referral-campaign.repository';
import { BazaReferralCodeEntity } from '../entities/baza-referral-code.entity';
import { BazaReferralCodeNoAssociationsSetException } from '../exceptions/baza-referral-code-no-associations-set.exception';
import { BazaReferralCodeDuplicateException } from '../exceptions/baza-referral-code-duplicate.exception';
import { BazaReferralCodeMapper } from '../mappers/baza-referral-code.mapper';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { BazaReferralCodeIsNotAvailableAnymoreException } from '../exceptions/baza-referral-code-is-not-available-anymore.exception';
import { BazaReferralCodeIsInvalidException } from '../exceptions/baza-referral-code-is-invalid.exception';
import { BazaReferralCampaignEntity } from '../entities/baza-referral-campaign.entity';
import { CrudCsvService, CrudService } from '../../../../baza-crud/src';
import { AccountEntity, BazaAccountRepository } from '../../../../baza-account/src';
import { BazaInviteCodeRepository } from '../../../../baza-invite-code/src';

@Injectable()
export class BazaReferralCodeService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudCsv: CrudCsvService,
        private readonly codesRepository: BazaReferralCodeRepository,
        private readonly codesMapper: BazaReferralCodeMapper,
        private readonly campaignsRepository: BazaReferralCampaignRepository,
        private readonly accountRepository: BazaAccountRepository,
        private readonly inviteCodeRepository: BazaInviteCodeRepository,
    ) {}

    async list(request: BazaReferralCodeCmsListRequest): Promise<BazaReferralCodeCmsListResponse> {
        const findOptions: FindManyOptions<BazaReferralCodeEntity> = {
            order: {
                id: 'DESC',
            },
            relations: REFERRAL_CODE_RELATIONS,
        };

        if (request.accountId) {
            findOptions.where = [
                {
                    associatedWithAccount: request.accountId,
                },
            ];
        } else if (request.campaignId) {
            findOptions.where = [
                {
                    associatedWithCampaign: request.campaignId,
                },
            ];
        } else {
            throw new BazaReferralCodeNoAssociationsSetException();
        }

        return this.crud.find<BazaReferralCodeEntity, BazaReferralCodeDto>({
            request: request as any,
            entity: BazaReferralCodeEntity,
            mapper: async (items) => this.codesMapper.entitiesToDTOs(items),
            findOptions,
        });
    }

    async exportListToCSV(request: BazaReferralCodeCmsExportListToCSVRequest): Promise<string> {
        const findOptions: FindManyOptions<BazaReferralCodeEntity> = {
            order: {
                id: 'DESC',
            },
            relations: REFERRAL_CODE_RELATIONS,
        };

        if (request.listRequest.accountId) {
            findOptions.where = [
                {
                    associatedWithAccount: request.listRequest.accountId,
                },
            ];
        } else if (request.listRequest.campaignId) {
            findOptions.where = [
                {
                    associatedWithCampaign: request.listRequest.campaignId,
                },
            ];
        } else {
            throw new BazaReferralCodeNoAssociationsSetException();
        }

        return this.crudCsv.exportToCsv<BazaReferralCodeEntity, BazaReferralCodeDto>({
            entity: BazaReferralCodeEntity,
            request: request.listRequest as any,
            delimiter: request.delimiter,
            mapper: async (items) => this.codesMapper.entitiesToDTOs(items),
            fields: request.fields,
            findOptions,
        });
    }

    async create(request: BazaReferralCodeCmsCreateRequest): Promise<BazaReferralCodeEntity> {
        const entity = new BazaReferralCodeEntity();

        if (request.associatedWithAccountId) {
            entity.associatedWithAccount = await this.accountRepository.getActiveAccountWithId(request.associatedWithAccountId);
        } else if (request.associatedWithCampaignId) {
            entity.associatedWithCampaign = await this.campaignsRepository.getById(request.associatedWithCampaignId);
        } else {
            throw new BazaReferralCodeNoAssociationsSetException();
        }

        if (
            await this.inviteCodeRepository.findInviteCodeByCode({
                code: bazaNormalizeReferralCode(request.codeDisplay),
            })
        ) {
            throw new BazaReferralCodeDuplicateException();
        }

        if (await this.codesRepository.findByCode(bazaNormalizeReferralCode(request.codeDisplay))) {
            throw new BazaReferralCodeDuplicateException();
        }

        await this.populate(entity, request);

        await this.codesRepository.save(entity);

        if (entity.associatedWithAccount) {
            await this.assignDefaultCodeForAccount(entity.associatedWithAccount);
        } else if (entity.associatedWithCampaign) {
            await this.assignDefaultCodeForCampaign(entity.associatedWithCampaign);
        }

        return entity;
    }

    async edit(request: BazaReferralCodeCmsEditRequest): Promise<BazaReferralCodeEntity> {
        const entity = await this.codesRepository.getById(request.id);
        const existing = await this.codesRepository.findByCode(bazaNormalizeReferralCode(request.codeDisplay));

        if (existing && existing.id !== entity.id) {
            throw new BazaReferralCodeDuplicateException();
        }

        if (
            await this.inviteCodeRepository.findInviteCodeByCode({
                code: bazaNormalizeReferralCode(request.codeDisplay),
            })
        ) {
            throw new BazaReferralCodeDuplicateException();
        }

        await this.populate(entity, request);

        await this.codesRepository.save(entity);

        if (entity.associatedWithAccount) {
            await this.assignDefaultCodeForAccount(entity.associatedWithAccount);
        } else if (entity.associatedWithCampaign) {
            await this.assignDefaultCodeForCampaign(entity.associatedWithCampaign);
        }

        return entity;
    }

    async delete(request: BazaReferralCodeCmsDeleteRequest): Promise<void> {
        const entity = await this.codesRepository.getById(request.id);

        const associatedWithAccount = entity.associatedWithAccount;
        const associatedWithCampaign = entity.associatedWithCampaign;

        await this.codesRepository.remove(entity);

        if (associatedWithAccount) {
            await this.assignDefaultCodeForAccount(associatedWithAccount);
        } else if (associatedWithCampaign) {
            await this.assignDefaultCodeForCampaign(associatedWithCampaign);
        }
    }

    async hasReferralCode(code: string): Promise<boolean> {
        return !!(await this.codesRepository.findByCode(bazaNormalizeReferralCode(code)));
    }

    async validateReferralCode(request: { code: string }): Promise<void> {
        const referralCode = await this.codesRepository.getByCode(request.code);

        if (!referralCode.isActive) {
            throw new BazaReferralCodeIsNotAvailableAnymoreException();
        }

        if (referralCode.associatedWithCampaign) {
            if (!referralCode.associatedWithCampaign.isActive) {
                throw new BazaReferralCodeIsNotAvailableAnymoreException();
            }
        } else if (referralCode.associatedWithAccount) {
            if (!referralCode.associatedWithAccount.isEmailConfirmed || referralCode.associatedWithAccount.isDeactivated) {
                throw new BazaReferralCodeIsNotAvailableAnymoreException();
            }
        }
    }

    async generateReferralCodeForAccount(account: AccountEntity): Promise<BazaReferralCodeEntity> {
        let code: string;
        let exists = false;

        do {
            code = bazaNormalizeReferralCodeDisplay(
                `Join${account.firstName}${Math.floor(Math.random() * 10000)
                    .toString()
                    .padStart(4, '0')}`,
            );

            exists = !!(await this.codesRepository.findByCode(bazaNormalizeReferralCode(code)));
        } while (exists);

        return this.create({
            associatedWithAccountId: account.id,
            isActive: true,
            isDefault: true,
            codeDisplay: code,
        });
    }

    async findReferralCodeOfAccount(account: AccountEntity): Promise<BazaReferralCodeEntity | undefined> {
        return (await this.codesRepository.findByAccount(account))[0];
    }

    async findDefaultOrActiveReferralCodeOfAccount(account: AccountEntity): Promise<BazaReferralCodeEntity | undefined> {
        const allCodes = await this.codesRepository.findByAccount(account);

        allCodes.sort((a, b) => a.id - b.id);

        const defaultReferralCode = allCodes.find((c) => c.isDefault);
        const activeReferralCode = allCodes.find((c) => c.isActive);

        return defaultReferralCode || activeReferralCode;
    }

    async assignDefaultCodeForAccount(account: AccountEntity): Promise<void> {
        const allCodes = await this.codesRepository.findByAccount(account);

        if (!allCodes.length) {
            return;
        }

        const hasActiveCode = allCodes.find((c) => c.isActive && c.isDefault);

        if (!hasActiveCode) {
            allCodes.sort((a, b) => a.id - b.id);

            for (const code of allCodes) {
                if (code.isDefault) {
                    code.isDefault = false;

                    await this.codesRepository.save(code);
                }
            }

            for (const code of allCodes) {
                if (code.isActive) {
                    code.isDefault = true;

                    await this.codesRepository.save(code);

                    break;
                }
            }
        }
    }

    async assignDefaultCodeForCampaign(campaign: BazaReferralCampaignEntity): Promise<void> {
        const allCodes = await this.codesRepository.findByCampaign(campaign);

        if (!allCodes.length) {
            return;
        }

        const hasActiveCode = allCodes.find((c) => c.isActive && c.isDefault);

        if (!hasActiveCode) {
            allCodes.sort((a, b) => a.id - b.id);

            for (const code of allCodes) {
                if (code.isDefault) {
                    code.isDefault = false;

                    await this.codesRepository.save(code);
                }
            }

            for (const code of allCodes) {
                if (code.isActive) {
                    code.isDefault = true;

                    await this.codesRepository.save(code);

                    break;
                }
            }
        }
    }

    private async populate(target: BazaReferralCodeEntity, entityBody: BazaReferralCodeCmsEntityBody): Promise<void> {
        const code = bazaNormalizeReferralCode(entityBody.codeDisplay);
        const codeDisplay = bazaNormalizeReferralCodeDisplay(entityBody.codeDisplay);

        if (!code.length || !codeDisplay.length) {
            throw new BazaReferralCodeIsInvalidException();
        }

        if (entityBody.isDefault && !entityBody.isActive) {
            entityBody.isDefault = false;
        }

        target.isActive = entityBody.isActive;
        target.code = code;
        target.codeDisplay = codeDisplay;

        if (entityBody.isDefault !== undefined) {
            target.isDefault = entityBody.isDefault;
        }

        if (target.associatedWithCampaign) {
            const referralCodesOfCampaign = await this.codesRepository.findByCampaign(target.associatedWithCampaign);

            for (const code of referralCodesOfCampaign) {
                if (target.id !== code.id && code.isDefault) {
                    code.isDefault = false;

                    await this.codesRepository.save(code);
                }
            }
        }

        if (target.associatedWithAccount) {
            const referralCodesOfAccounts = await this.codesRepository.findByAccount(target.associatedWithAccount);

            for (const code of referralCodesOfAccounts) {
                if (target.id !== code.id && code.isDefault) {
                    code.isDefault = false;

                    await this.codesRepository.save(code);
                }
            }
        }
    }
}
