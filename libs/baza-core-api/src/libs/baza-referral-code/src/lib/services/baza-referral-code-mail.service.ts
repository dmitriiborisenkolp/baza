import { Injectable } from '@nestjs/common';
import { MailService } from '../../../../baza-mail/src';
import { BazaCoreMail } from '../../../../../constants/baza-core.mail';

@Injectable()
export class BazaReferralCodeMailService {
    constructor(private readonly mail: MailService) {}

    async sendSignedUpWithReferralCodeNotification(request: {
        referralCodeOwnerFullName: string;
        referralCodeOwnerFirstName: string;
        referralCodeOwnerEmail: string;
    }): Promise<void> {
        await this.mail.send({
            name: BazaCoreMail.BazaReferralCodeSignedUpWithReferralCode,
            subject: 'baza-nc-integration.referralCodes.mailing.signedUpWithReferralCode.subject',
            to: [
                {
                    email: request.referralCodeOwnerEmail,
                    name: request.referralCodeOwnerFullName,
                },
            ],
            variables: () => ({
                referralCodeOwnerFullName: request.referralCodeOwnerFullName,
                referralCodeOwnerFirstName: request.referralCodeOwnerFirstName,
                referralCodeOwnerEmail: request.referralCodeOwnerEmail,
            }),
        });
    }
}
