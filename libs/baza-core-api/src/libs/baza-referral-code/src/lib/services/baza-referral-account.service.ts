import { Injectable } from '@nestjs/common';
import { CrudCsvService, CrudService } from '../../../../baza-crud/src';
import { BazaReferralAccountMapper } from '../mappers/baza-referral-account.mapper';
import {
    BazaReferralAccountCmsExportToCsvListRequest,
    BazaReferralAccountCmsListRequest,
    BazaReferralAccountCmsListResponse,
    BazaReferralAccountCsvDto,
    BazaReferralAccountDto,
} from '@scaliolabs/baza-core-shared';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { AccountEntity } from '../../../../baza-account/src';
import { ILike, In } from 'typeorm';
import { postgresLikeEscape } from '@scaliolabs/baza-core-shared';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { BazaReferralAccountCsvMapper } from '../mappers/baza-referral-account-csv.mapper';

@Injectable()
export class BazaReferralAccountService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudCsv: CrudCsvService,
        private readonly referralAccountMapper: BazaReferralAccountMapper,
        private readonly referralAccountCsvMapper: BazaReferralAccountCsvMapper,
    ) {}

    async list(request: BazaReferralAccountCmsListRequest): Promise<BazaReferralAccountCmsListResponse> {
        const where: Array<FindConditions<AccountEntity>> = [];

        const baseFindCondition = () => {
            return {
                role: In([AccountRole.User]),
            };
        };

        if (request.queryString) {
            where.push(
                { ...baseFindCondition(), fullName: ILike(`%${postgresLikeEscape(request.queryString)}%`) },
                { ...baseFindCondition(), email: ILike(`%${postgresLikeEscape(request.queryString)}%`) },
            );
        } else {
            where.push(baseFindCondition());
        }

        return this.crud.find<AccountEntity, BazaReferralAccountDto>({
            request,
            entity: AccountEntity,
            findOptions: {
                where,
                order: {
                    id: 'DESC',
                },
            },
            mapper: (accounts) => Promise.resolve(this.referralAccountMapper.entitiesToDTOs(accounts)),
        });
    }

    async exportListToCSV(request: BazaReferralAccountCmsExportToCsvListRequest): Promise<string> {
        const where: Array<FindConditions<AccountEntity>> = [];

        const baseFindCondition = () => {
            return {
                role: In([AccountRole.User]),
            };
        };

        if (request.listRequest.queryString) {
            where.push(
                { ...baseFindCondition(), fullName: ILike(`%${postgresLikeEscape(request.listRequest.queryString)}%`) },
                { ...baseFindCondition(), email: ILike(`%${postgresLikeEscape(request.listRequest.queryString)}%`) },
            );
        } else {
            where.push(baseFindCondition());
        }

        return this.crudCsv.exportToCsv<AccountEntity, BazaReferralAccountCsvDto>({
            request: request.listRequest as any,
            fields: request.fields,
            delimiter: request.delimiter,
            findOptions: {
                where,
                order: {
                    id: 'DESC',
                },
            },
            entity: AccountEntity,
            mapper: async (items) => this.referralAccountCsvMapper.entitiesToDTOs(items),
        });
    }
}
