import { Injectable } from '@nestjs/common';
import { REFERRAL_CODE_USAGE_RELATIONS, BazaReferralCodeUsageRepository } from '../repositories/baza-referral-code-usage.repository';
import { CrudCsvService, CrudService } from '../../../../baza-crud/src';
import { BazaReferralCodeUsageMapper } from '../mappers/baza-referral-code-usage.mapper';
import {
    bazaNormalizeReferralCode,
    BazaReferralCodeCmsUsageByAccountExportListToCSVRequest,
    BazaReferralCodeCmsUsageByAccountListRequest,
    BazaReferralCodeCmsUsageByAccountListResponse,
    BazaReferralCodeCmsUsageByCampaignExportListToCSVRequest,
    BazaReferralCodeCmsUsageByCampaignListRequest,
    BazaReferralCodeCmsUsageByCampaignListResponse,
    BazaReferralCodeCmsUsageByCodeExportListToCSVRequest,
    BazaReferralCodeCmsUsageByCodeListRequest,
    BazaReferralCodeCmsUsageByCodeListResponse,
    BazaReferralCodeUsageCsvDto,
    BazaReferralCodeUsageDto,
} from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeUsageEntity } from '../entities/baza-referral-code-usage.entity';
import { BazaReferralCodeRepository } from '../repositories/baza-referral-code.repository';
import { BazaReferralCampaignRepository } from '../repositories/baza-referral-campaign.repository';
import { In } from 'typeorm';
import { BazaReferralCodeUsageCsvMapper } from '../mappers/baza-referral-code-usage-csv.mapper';
import { AccountEntity, BazaAccountRepository } from '../../../../baza-account/src';
import { Application } from '@scaliolabs/baza-core-shared';

/**
 * Referral Code Usage Service
 */
@Injectable()
export class BazaReferralCodeUsageService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudCsv: CrudCsvService,
        private readonly codeRepository: BazaReferralCodeRepository,
        private readonly codeUsageRepository: BazaReferralCodeUsageRepository,
        private readonly campaignRepository: BazaReferralCampaignRepository,
        private readonly accountRepository: BazaAccountRepository,
        private readonly codeUsageMapper: BazaReferralCodeUsageMapper,
        private readonly codeUsageCsvMapper: BazaReferralCodeUsageCsvMapper,
    ) {}

    /**
     * Create a record about Referral Code Usage
     * @param account
     * @param referralCode
     */
    async writeReferralCodeUsage(account: AccountEntity, referralCode: string): Promise<void> {
        const code = await this.codeRepository.getByCode(referralCode);

        const usageEntity = new BazaReferralCodeUsageEntity();

        usageEntity.joinedAccount = account;
        usageEntity.referralCode = code;
        usageEntity.dateJoinedAt = new Date();
        usageEntity.joinedWithClient = account.signedUpWithClient;

        await this.codeUsageRepository.save(usageEntity);

        code.usedCount++;

        await this.codeRepository.save(code);
    }

    /**
     * Returns true if given account was already joined by Referral Code
     * @param joinedAccount
     */
    async hasReferralCodeUsageFor(joinedAccount: AccountEntity): Promise<boolean> {
        const existing = await this.codeUsageRepository.findAllByJoinedAccount(joinedAccount);

        return existing.length > 0;
    }

    /**
     * Returns list of Referral Code Usage (by Referral Code)
     * @param request
     */
    async listByReferralCode(request: BazaReferralCodeCmsUsageByCodeListRequest): Promise<BazaReferralCodeCmsUsageByCodeListResponse> {
        const referralCode = await this.codeRepository.getByCode(bazaNormalizeReferralCode(request.referralCode));

        return this.crud.find<BazaReferralCodeUsageEntity, BazaReferralCodeUsageDto>({
            request: request as any,
            entity: BazaReferralCodeUsageEntity,
            mapper: async (items) => this.codeUsageMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
                where: [
                    {
                        referralCode,
                    },
                ],
                relations: REFERRAL_CODE_USAGE_RELATIONS,
            },
        });
    }

    /**
     * Returns CVS with list of Referral Code Usage (by Referral Code)
     * @param request
     */
    async exportListByReferralCodeToCSV(request: BazaReferralCodeCmsUsageByCodeExportListToCSVRequest): Promise<string> {
        const referralCode = await this.codeRepository.getByCode(bazaNormalizeReferralCode(request.referralCode));

        return this.crudCsv.exportToCsv<BazaReferralCodeUsageEntity, BazaReferralCodeUsageCsvDto>({
            request: request.listRequest as any,
            entity: BazaReferralCodeUsageEntity,
            fields: request.fields,
            delimiter: request.delimiter,
            mapper: async (items) => this.codeUsageCsvMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
                where: [
                    {
                        referralCode,
                    },
                ],
                relations: REFERRAL_CODE_USAGE_RELATIONS,
            },
        });
    }

    /**
     * Returns list of Referral Code Usage (by Referral Campaign)
     * @param request
     */
    async listByReferralCampaign(
        request: BazaReferralCodeCmsUsageByCampaignListRequest,
    ): Promise<BazaReferralCodeCmsUsageByCampaignListResponse> {
        const campaign = await this.campaignRepository.getById(request.campaignId);
        const codesInCampaign = await this.codeRepository.findByCampaign(campaign);

        return this.crud.find<BazaReferralCodeUsageEntity, BazaReferralCodeUsageDto>({
            request: request as any,
            entity: BazaReferralCodeUsageEntity,
            mapper: async (items) => this.codeUsageMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
                where: [
                    {
                        referralCode: In(
                            codesInCampaign
                                .map((c) => c.id)
                                .filter((id) => !request.filterByReferralCodeIds || request.filterByReferralCodeIds.includes(id)),
                        ),
                    },
                ],
                relations: REFERRAL_CODE_USAGE_RELATIONS,
            },
        });
    }

    /**
     * Returns CSV with list of Referral Code Usage (by Referral Campaign)
     * @param request
     */
    async exportListByReferralCampaignToCSV(request: BazaReferralCodeCmsUsageByCampaignExportListToCSVRequest): Promise<string> {
        const campaign = await this.campaignRepository.getById(request.listRequest.campaignId);
        const codesInCampaign = await this.codeRepository.findByCampaign(campaign);

        return this.crudCsv.exportToCsv<BazaReferralCodeUsageEntity, BazaReferralCodeUsageCsvDto>({
            request: request.listRequest as any,
            entity: BazaReferralCodeUsageEntity,
            fields: request.fields,
            delimiter: request.delimiter,
            mapper: async (items) => this.codeUsageCsvMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
                where: [
                    {
                        referralCode: In(
                            codesInCampaign
                                .map((c) => c.id)
                                .filter(
                                    (id) =>
                                        !request.listRequest.filterByReferralCodeIds ||
                                        request.listRequest.filterByReferralCodeIds.includes(id),
                                ),
                        ),
                    },
                ],
                relations: REFERRAL_CODE_USAGE_RELATIONS,
            },
        });
    }

    /**
     * Returns list of Referral Code Usage (by Referral Codes attached with given Account)
     * @param request
     */
    async listByReferralAccount(
        request: BazaReferralCodeCmsUsageByAccountListRequest,
    ): Promise<BazaReferralCodeCmsUsageByAccountListResponse> {
        const account = await this.accountRepository.getActiveAccountWithId(request.accountId);
        const codesInAccount = await this.codeRepository.findByAccount(account);

        return this.crud.find<BazaReferralCodeUsageEntity, BazaReferralCodeUsageDto>({
            request: request as any,
            entity: BazaReferralCodeUsageEntity,
            mapper: async (items) => this.codeUsageMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
                where: [
                    {
                        referralCode: In(
                            codesInAccount
                                .map((c) => c.id)
                                .filter((id) => !request.filterByReferralCodeIds || request.filterByReferralCodeIds.includes(id)),
                        ),
                    },
                ],
                relations: REFERRAL_CODE_USAGE_RELATIONS,
            },
        });
    }

    /**
     * Returns CSV with list of Referral Code Usage (by Referral Codes attached with given Account)
     * @param request
     */
    async exportListByReferralAccountToCSV(request: BazaReferralCodeCmsUsageByAccountExportListToCSVRequest): Promise<string> {
        const account = await this.accountRepository.getActiveAccountWithId(request.listRequest.accountId);
        const codesInAccount = await this.codeRepository.findByAccount(account);

        return this.crudCsv.exportToCsv<BazaReferralCodeUsageEntity, BazaReferralCodeUsageCsvDto>({
            request: request.listRequest as any,
            entity: BazaReferralCodeUsageEntity,
            fields: request.fields,
            delimiter: request.delimiter,
            mapper: async (items) => this.codeUsageCsvMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
                where: [
                    {
                        referralCode: In(
                            codesInAccount
                                .map((c) => c.id)
                                .filter(
                                    (id) =>
                                        !request.listRequest.filterByReferralCodeIds ||
                                        request.listRequest.filterByReferralCodeIds.includes(id),
                                ),
                        ),
                    },
                ],
                relations: REFERRAL_CODE_USAGE_RELATIONS,
            },
        });
    }
}
