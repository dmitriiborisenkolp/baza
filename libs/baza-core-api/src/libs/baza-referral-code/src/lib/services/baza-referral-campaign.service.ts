import { Injectable } from '@nestjs/common';
import {
    bazaNormalizeReferralCampaignName,
    ReferralCampaignCmsCreateRequest,
    ReferralCampaignCmsDeleteRequest,
    ReferralCampaignCmsEditRequest,
    ReferralCampaignCmsEntityBody,
    ReferralCampaignCmsExportListToCsvRequest,
    ReferralCampaignCmsListRequest,
    ReferralCampaignCmsListResponse,
    BazaReferralCampaignDto,
} from '@scaliolabs/baza-core-shared';
import { BazaReferralCampaignEntity } from '../entities/baza-referral-campaign.entity';
import { REFERRAL_CAMPAIGN_RELATIONS, BazaReferralCampaignRepository } from '../repositories/baza-referral-campaign.repository';
import { BazaReferralCampaignWithSameNameAlreadyExistsException } from '../exceptions/baza-referral-campaign-with-same-name-already-exists.exception';
import { CrudCsvService, CrudService } from '../../../../baza-crud/src';
import { BazaReferralCampaignMapper } from '../mappers/baza-referral-campaign.mapper';

@Injectable()
export class BazaReferralCampaignService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudCsv: CrudCsvService,
        private readonly campaignRepository: BazaReferralCampaignRepository,
        private readonly campaignMapper: BazaReferralCampaignMapper,
    ) {}

    async create(request: ReferralCampaignCmsCreateRequest): Promise<BazaReferralCampaignEntity> {
        const entity = new BazaReferralCampaignEntity();

        if (await this.campaignRepository.findByCampaignName(request.name)) {
            throw new BazaReferralCampaignWithSameNameAlreadyExistsException();
        }

        await this.populate(entity, request);
        await this.campaignRepository.save(entity);

        return entity;
    }

    async edit(request: ReferralCampaignCmsEditRequest): Promise<BazaReferralCampaignEntity> {
        const existingCampaign = await this.campaignRepository.findByCampaignName(request.name);

        if (existingCampaign && existingCampaign.id !== request.id) {
            throw new BazaReferralCampaignWithSameNameAlreadyExistsException();
        }

        const entity = await this.campaignRepository.getById(request.id);

        await this.populate(entity, request);
        await this.campaignRepository.save(entity);

        return entity;
    }

    async delete(request: ReferralCampaignCmsDeleteRequest): Promise<void> {
        const entity = await this.campaignRepository.getById(request.id);

        await this.campaignRepository.remove(entity);
    }

    async list(request: ReferralCampaignCmsListRequest): Promise<ReferralCampaignCmsListResponse> {
        return this.crud.find<BazaReferralCampaignEntity, BazaReferralCampaignDto>({
            request,
            entity: BazaReferralCampaignEntity,
            mapper: async (items) => this.campaignMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
            },
        });
    }

    async exportListToCSV(request: ReferralCampaignCmsExportListToCsvRequest): Promise<string> {
        return this.crudCsv.exportToCsv<BazaReferralCampaignEntity, BazaReferralCampaignDto>({
            entity: BazaReferralCampaignEntity,
            request: request.listRequest,
            fields: request.fields,
            delimiter: request.delimiter,
            mapper: async (items) => this.campaignMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
                relations: REFERRAL_CAMPAIGN_RELATIONS,
            },
        });
    }

    private async populate(target: BazaReferralCampaignEntity, entityBody: ReferralCampaignCmsEntityBody): Promise<void> {
        target.name = bazaNormalizeReferralCampaignName(entityBody.name);
        target.isActive = entityBody.isActive;
    }
}
