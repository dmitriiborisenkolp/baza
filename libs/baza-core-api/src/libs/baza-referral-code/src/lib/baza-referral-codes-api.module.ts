import { Global, Module } from '@nestjs/common';
import { BazaReferralCampaignCmsController } from './controllers/cms/baza-referral-campaign-cms.controller';
import { BazaReferralCodeCmsController } from './controllers/cms/baza-referral-code-cms.controller';
import { BazaReferralCodeRepository } from './repositories/baza-referral-code.repository';
import { BazaReferralCodeService } from './services/baza-referral-code.service';
import { BazaReferralCampaignRepository } from './repositories/baza-referral-campaign.repository';
import { BazaReferralCampaignService } from './services/baza-referral-campaign.service';
import { BazaReferralCampaignMapper } from './mappers/baza-referral-campaign.mapper';
import { BazaReferralCodeMapper } from './mappers/baza-referral-code.mapper';
import { BazaReferralCodeUsageRepository } from './repositories/baza-referral-code-usage.repository';
import { BazaReferralCodeUsageMapper } from './mappers/baza-referral-code-usage.mapper';
import { BazaCrudApiModule } from '../../../baza-crud/src';
import { BazaReferralCodeUsageCmsController } from './controllers/cms/baza-referral-code-usage-cms.controller';
import { BazaReferralCodeUsageService } from './services/baza-referral-code-usage.service';
import { CqrsModule } from '@nestjs/cqrs';
import { CreateReferralCodeOnAccountSignupEventHandler } from './event-handlers/create-referral-code-on-account-signup.event-handler';
import { BazaReferralAccountService } from './services/baza-referral-account.service';
import { BazaReferralCodeUsageCsvMapper } from './mappers/baza-referral-code-usage-csv.mapper';
import { BazaReferralAccountCmsController } from './controllers/cms/baza-referral-account-cms.controller';
import { BazaReferralAccountMapper } from './mappers/baza-referral-account.mapper';
import { BazaReferralAccountCsvMapper } from './mappers/baza-referral-account-csv.mapper';
import { BazaReferralCodeMailService } from './services/baza-referral-code-mail.service';
import { BazaInviteCodeApiModule } from '../../../baza-invite-code/src';

const REPOSITORIES = [
    BazaReferralCampaignRepository,
    BazaReferralCodeRepository,
    BazaReferralCodeUsageRepository,
];

const MAPPERS = [
    BazaReferralCampaignMapper,
    BazaReferralCodeMapper,
    BazaReferralCodeUsageMapper,
    BazaReferralCodeUsageCsvMapper,
    BazaReferralAccountMapper,
    BazaReferralAccountCsvMapper,
];

const SERVICES = [
    BazaReferralCampaignService,
    BazaReferralCodeService,
    BazaReferralCodeUsageService,
    BazaReferralAccountService,
    BazaReferralCodeMailService,
];

const EVENT_HANDLERS = [
    CreateReferralCodeOnAccountSignupEventHandler,
];

@Global()
@Module({
    imports: [
        CqrsModule,
        BazaCrudApiModule,
        BazaInviteCodeApiModule,
    ],
    controllers: [
        BazaReferralCampaignCmsController,
        BazaReferralCodeCmsController,
        BazaReferralCodeUsageCmsController,
        BazaReferralAccountCmsController,
    ],
    providers: [
        ...REPOSITORIES,
        ...MAPPERS,
        ...SERVICES,
        ...EVENT_HANDLERS,
    ],
    exports: [
        ...REPOSITORIES,
        ...MAPPERS,
        ...SERVICES,
    ],
})
export class BazaReferralCodesApiModule {
}
