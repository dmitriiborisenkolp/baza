import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaReferralCampaignDto, BazaReferralCodeDto } from '@scaliolabs/baza-core-shared';
import { BazaReferralCampaignCmsNodeAccess, BazaReferralCodeCmsNodeAccess } from '@scaliolabs/baza-core-node-access';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/04-baza-referral-codes-cms-export-to-csv.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCampaign = new BazaReferralCampaignCmsNodeAccess(http);
    const dataAccessCodes = new BazaReferralCodeCmsNodeAccess(http);

    const E2E_CURSOR: {
        campaigns: Array<BazaReferralCampaignDto>;
        codes: Array<BazaReferralCodeDto>;
    } = {
        campaigns: [],
        codes: [],
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCodes],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        E2E_CURSOR.campaigns = (
            await dataAccessCampaign.list({
                index: 1,
            })
        ).items;

        for (const campaign of E2E_CURSOR.campaigns) {
            E2E_CURSOR.codes.push(
                ...(
                    await dataAccessCodes.list({
                        index: 1,
                        campaignId: campaign.id,
                    })
                ).items,
            );
        }

        http.noAuth();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully exports referral codes to CSV (campaign 1)', async () => {
        const campaign = E2E_CURSOR.campaigns.find((c) => c.name === 'Example Campaign 1');

        const response = await dataAccessCodes.exportListToCSV({
            fields: [
                {
                    field: 'codeDisplay',
                    title: 'Code',
                },
            ],
            listRequest: {
                campaignId: campaign.id,
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(typeof response === 'string').toBeTruthy();
        expect(response.split('\n').length).toBe(3);
    });

    it('will successfully exports referral codes to CSV (campaign 2)', async () => {
        const campaign = E2E_CURSOR.campaigns.find((c) => c.name === 'Example Campaign 2');

        const response = await dataAccessCodes.exportListToCSV({
            fields: [
                {
                    field: 'codeDisplay',
                    title: 'Code',
                },
            ],
            listRequest: {
                campaignId: campaign.id,
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(typeof response === 'string').toBeTruthy();
        expect(response.split('\n').length).toBe(4);
    });

    it('will successfully exports referral codes to CSV (account)', async () => {
        const response = await dataAccessCodes.exportListToCSV({
            fields: [
                {
                    field: 'codeDisplay',
                    title: 'Code',
                },
            ],
            listRequest: {
                accountId: http.authResponse.jwtPayload.accountId,
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(typeof response === 'string').toBeTruthy();
        expect(response.split('\n').length).toBe(1);
    });

    it('will successfully exports referral codes to CSV (account with added referral code)', async () => {
        const createResponse = await dataAccessCodes.create({
            associatedWithAccountId: http.authResponse.jwtPayload.accountId,
            codeDisplay: 'ExampleAdminCode',
            isActive: true,
        });

        expect(isBazaErrorResponse(createResponse)).toBeFalsy();

        const response = await dataAccessCodes.exportListToCSV({
            fields: [
                {
                    field: 'codeDisplay',
                    title: 'Code',
                },
            ],
            listRequest: {
                accountId: http.authResponse.jwtPayload.accountId,
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(typeof response === 'string').toBeTruthy();
        expect(response.split('\n').length).toBe(2);
    });
});
