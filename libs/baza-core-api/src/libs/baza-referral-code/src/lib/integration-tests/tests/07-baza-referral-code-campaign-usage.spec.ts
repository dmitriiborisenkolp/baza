import 'reflect-metadata';
import { BazaDataAccessNode, BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { BazaReferralCampaignDto, BazaReferralCodeDto } from '@scaliolabs/baza-core-shared';
import { Application, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaReferralCampaignCmsNodeAccess,
    BazaReferralCodeCmsNodeAccess,
    BazaReferralCodeUsageCmsNodeAccess,
} from '@scaliolabs/baza-core-node-access';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/07-baza-nc-integration-referral-code-campaign-usage.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCampaign = new BazaReferralCampaignCmsNodeAccess(http);
    const dataAccessCodes = new BazaReferralCodeCmsNodeAccess(http);
    const dataAccessCodesUsage = new BazaReferralCodeUsageCmsNodeAccess(http);
    const dataAccessAccountNC = new BazaAccountNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    const E2E_CURSOR: {
        campaigns: Array<BazaReferralCampaignDto>;
        codes: Array<BazaReferralCodeDto>;
        currentCampaign?: BazaReferralCampaignDto;
        currentCode?: BazaReferralCodeDto;
    } = {
        campaigns: [],
        codes: [],
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCodes],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        E2E_CURSOR.campaigns = (
            await dataAccessCampaign.list({
                index: 1,
            })
        ).items;

        for (const campaign of E2E_CURSOR.campaigns) {
            E2E_CURSOR.codes.push(
                ...(
                    await dataAccessCodes.list({
                        index: 1,
                        campaignId: campaign.id,
                    })
                ).items,
            );
        }

        E2E_CURSOR.currentCampaign = E2E_CURSOR.campaigns.find(
            (c) =>
                c.isActive &&
                E2E_CURSOR.codes
                    .filter((code) => code.associatedWithCampaign && code.associatedWithCampaign.id === c.id)
                    .some((code) => code.isActive),
        );

        E2E_CURSOR.currentCode = E2E_CURSOR.codes.find(
            (code) => code.isActive && code.associatedWithCampaign && code.associatedWithCampaign.id === E2E_CURSOR.currentCampaign.id,
        );

        http.noAuth();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will have no referral code usages for current campaign', async () => {
        const response = await dataAccessCodesUsage.listByReferralCampaign({
            campaignId: E2E_CURSOR.currentCampaign.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(0);

        const referralCodeResponse = await dataAccessCodes.getById({
            id: E2E_CURSOR.currentCode.id,
        });

        expect(isBazaErrorResponse(referralCodeResponse)).toBeFalsy();
        expect(referralCodeResponse.referralCode.usedCount).toBe(0);
    });

    it('will successfully register new account with active campaign and active code and confirm its email', async () => {
        const registerAccountResponse = await dataAccessAccountNC.registerAccount({
            email: 'e2e-example-referral-code-usage-01@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
            referralCode: E2E_CURSOR.currentCode.codeDisplay,
        });

        expect(isBazaErrorResponse(registerAccountResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(mailbox.length).toBe(1);

        const token = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        expect(token).not.toBeUndefined();

        await dataAccessAccount.confirmEmail({ token });

        expect(isBazaErrorResponse(registerAccountResponse)).toBeFalsy();
    });

    it('will have new entry in referral code campaign usage after user email confirmation', async () => {
        const response = await dataAccessCodesUsage.listByReferralCampaign({
            campaignId: E2E_CURSOR.currentCampaign.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);
        expect(response.items[0].referralCode.codeDisplay).toBe(E2E_CURSOR.currentCode.codeDisplay);
        expect(response.items[0].joinedAccount.email).toBe('e2e-example-referral-code-usage-01@scal.io');
        expect(response.items[0].joinedWithClient).toBe(Application.WEB);
    });

    it('will add usedCount counter in referral code after user email confirmation', async () => {
        const referralCodeResponse = await dataAccessCodes.getById({
            id: E2E_CURSOR.currentCode.id,
        });

        expect(isBazaErrorResponse(referralCodeResponse)).toBeFalsy();
        expect(referralCodeResponse.referralCode.usedCount).toBe(1);
    });

    it('will add usedCount counter in referral campaign after user email confirmation', async () => {
        const referralCodeResponse = await dataAccessCampaign.getById({
            id: E2E_CURSOR.currentCampaign.id,
        });

        expect(isBazaErrorResponse(referralCodeResponse)).toBeFalsy();
        expect(referralCodeResponse.campaign.usedCount).toBe(1);
    });

    it('will not have usage entries in different campaigns', async () => {
        const anotherCampaign = E2E_CURSOR.campaigns.find(
            (c) =>
                c.isActive &&
                c.id !== E2E_CURSOR.currentCampaign.id &&
                E2E_CURSOR.codes
                    .filter((code) => code.associatedWithCampaign && code.associatedWithCampaign.id === c.id)
                    .some((code) => code.isActive),
        );

        expect(anotherCampaign).toBeDefined();

        const response = await dataAccessCodesUsage.listByReferralCampaign({
            campaignId: anotherCampaign.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(0);
    });

    it('will successfully register new account with active campaign and active code without email confirmation', async () => {
        const registerAccountResponse = await dataAccessAccountNC.registerAccount({
            email: 'e2e-example-referral-code-usage-02@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
            referralCode: E2E_CURSOR.currentCode.codeDisplay,
        });

        expect(isBazaErrorResponse(registerAccountResponse)).toBeFalsy();
    });

    it('will not increment referral code campaign usage without email confirmation', async () => {
        const response = await dataAccessCodesUsage.listByReferralCampaign({
            campaignId: E2E_CURSOR.currentCampaign.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);
    });

    it('usedCount counter will not increment in referral code without email confirmation', async () => {
        const referralCodeResponse = await dataAccessCodes.getById({
            id: E2E_CURSOR.currentCode.id,
        });

        expect(isBazaErrorResponse(referralCodeResponse)).toBeFalsy();
        expect(referralCodeResponse.referralCode.usedCount).toBe(1);
    });

    it('usedCount counter will not increment in referral campaign without user email confirmation', async () => {
        const referralCodeResponse = await dataAccessCampaign.getById({
            id: E2E_CURSOR.currentCampaign.id,
        });

        expect(isBazaErrorResponse(referralCodeResponse)).toBeFalsy();
        expect(referralCodeResponse.campaign.usedCount).toBe(1);
    });

    it('will add another account but this time will confirm it as well', async () => {
        const registerAccountResponse = await dataAccessAccountNC.registerAccount({
            email: 'e2e-example-referral-code-usage-03@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
            referralCode: E2E_CURSOR.currentCode.codeDisplay,
        });

        const mailbox = await dataAccessE2e.mailbox();

        const token = mailbox[0].messageBodyText.match(/([abcdefABCDEF0123456789]{16})/)[1];

        expect(token).not.toBeUndefined();

        await dataAccessAccount.confirmEmail({ token });

        expect(isBazaErrorResponse(registerAccountResponse)).toBeFalsy();
    });

    it('will have new entry in referral code campaign usage after user email confirmation', async () => {
        const response = await dataAccessCodesUsage.listByReferralCampaign({
            campaignId: E2E_CURSOR.currentCampaign.id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(2);
        expect(response.items[0].referralCode.codeDisplay).toBe(E2E_CURSOR.currentCode.codeDisplay);
        expect(response.items[0].joinedAccount.email).toBe('e2e-example-referral-code-usage-03@scal.io');
        expect(response.items[0].joinedWithClient).toBe(Application.WEB);
    });

    it('will add usedCount counter in referral code after user email confirmation', async () => {
        const referralCodeResponse = await dataAccessCodes.getById({
            id: E2E_CURSOR.currentCode.id,
        });

        expect(isBazaErrorResponse(referralCodeResponse)).toBeFalsy();
        expect(referralCodeResponse.referralCode.usedCount).toBe(2);
    });

    it('will add usedCount counter in referral campaign after user email confirmation', async () => {
        const referralCodeResponse = await dataAccessCampaign.getById({
            id: E2E_CURSOR.currentCampaign.id,
        });

        expect(isBazaErrorResponse(referralCodeResponse)).toBeFalsy();
        expect(referralCodeResponse.campaign.usedCount).toBe(2);
    });
});
