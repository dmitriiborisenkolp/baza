import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsNodeAccess, BazaAccountNodeAccess } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { asyncExpect } from '../../../../../baza-test-utils/src';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/05-baza-nc-integration-referral-code-will-be-created-for-new-registered-user.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessCodes = new BazaReferralCodeCmsNodeAccess(http);

    const E2E_CURSOR: {
        accountId?: number;
    } = {};

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully register new user', async () => {
        const response = await dataAccessAccount.registerAccount({
            email: 'example-e2e-ref-codes@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const accounts = await dataAccessAccountCms.listAccounts({
            index: 1,
            roles: [AccountRole.User],
        });

        const registeredAccount = accounts.items.find((a) => a.email === 'example-e2e-ref-codes@scal.io');

        expect(registeredAccount).not.toBeUndefined();

        E2E_CURSOR.accountId = registeredAccount.id;
    });

    it('will creates automatically new referral code for registered account', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessCodes.list({
                    index: 1,
                    accountId: E2E_CURSOR.accountId,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(1);
                expect(response.items[0].codeDisplay.startsWith('JoinExample')).toBeTruthy();
                expect(response.items[0].code.startsWith('joinexample')).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
