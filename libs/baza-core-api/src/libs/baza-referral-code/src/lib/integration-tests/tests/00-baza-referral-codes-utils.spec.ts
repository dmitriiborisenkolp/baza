import 'reflect-metadata';
import { bazaIsReferralCodeValid, bazaNormalizeReferralCode, bazaNormalizeReferralCodeDisplay } from '@scaliolabs/baza-core-shared';

describe('@baza-core-api/baza-referral-code/integration-tests/tests/00-baza-referral-codes-utils.spec.ts', () => {
    it('will correctly filter referral code values', async () => {
        const testCases: Array<[string, string, string, boolean]> = [
            ['joinCommonwealth', 'joinCommonwealth', 'joincommonwealth', true],
            ['joinCommonwealth1', 'joinCommonwealth1', 'joincommonwealth1', true],
            ['join Commonwealth 1', 'joinCommonwealth1', 'joincommonwealth1', false],
            ['joinCommonwealth!', 'joinCommonwealth!', 'joincommonwealth!', true],
            ['join.!.Commonwealth', 'join!Commonwealth', 'join!commonwealth', false],
            ['join.  !.Commo nwealth', 'join!Commonwealth', 'join!commonwealth', false],
        ];

        for (const testCase of testCases) {
            expect(bazaNormalizeReferralCode(testCase[0])).toBe(testCase[2]);
            expect(bazaNormalizeReferralCodeDisplay(testCase[0])).toBe(testCase[1]);
            expect(bazaIsReferralCodeValid(testCase[0])).toBe(testCase[3]);
        }
    });
});
