import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaReferralCampaignCmsNodeAccess, BazaReferralCodeCmsNodeAccess } from '@scaliolabs/baza-core-node-access';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/03-baza-referral-codes-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCampaign = new BazaReferralCampaignCmsNodeAccess(http);
    const dataAccessCodes = new BazaReferralCodeCmsNodeAccess(http);

    const E2E_CURSOR: {
        campaignId?: number;
        campaignIds?: Array<number>;
        codeId?: number;
    } = {};

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCampaigns],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const listResponse = await dataAccessCampaign.list({
            index: 1,
        });

        expect(listResponse.items.length).toBe(3);

        E2E_CURSOR.campaignIds = listResponse.items.map((item) => item.id);
        E2E_CURSOR.campaignId = E2E_CURSOR.campaignIds[2];
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will have no referral codes in Example Campaign 1', async () => {
        const listResponse = await dataAccessCodes.list({
            index: 1,
            campaignId: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(0);
    });

    it('will successfully create new referral code', async () => {
        const response = await dataAccessCodes.create({
            codeDisplay: 'ExampleCode1',
            associatedWithCampaignId: E2E_CURSOR.campaignId,
            isActive: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessCodes.list({
            index: 1,
            campaignId: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].code).toBe('examplecode1');
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode1');
        expect(listResponse.items[0].associatedWithCampaign).toBeDefined();
        expect(listResponse.items[0].associatedWithCampaign.id).toBe(E2E_CURSOR.campaignId);
        expect(listResponse.items[0].associatedWithAccount).not.toBeDefined();

        E2E_CURSOR.codeId = listResponse.items[0].id;
    });

    it('will not allow to create referral code with same name (same campaign)', async () => {
        const samples = [
            'ExampleCode1',
            'ExampleCode1 ',
            'ExampleCode1  ',
            ' ExampleCode1',
            '  ExampleCode1',
            ' ExampleCode1 ',
            '  ExampleCode1  ',
        ];

        for (const codeDisplay of samples) {
            const response: BazaError = (await dataAccessCodes.create({
                codeDisplay,
                associatedWithCampaignId: E2E_CURSOR.campaignId,
                isActive: true,
            })) as any;

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect(response.code).toBe(BazaReferralCodeErrorCodes.BazaReferralCodeDuplicate);

            const listResponse = await dataAccessCodes.list({
                index: 1,
                campaignId: E2E_CURSOR.campaignId,
            });

            expect(isBazaErrorResponse(listResponse)).toBeFalsy();
            expect(listResponse.items.length).toBe(1);
            expect(listResponse.items[0].code).toBe('examplecode1');
            expect(listResponse.items[0].codeDisplay).toBe('ExampleCode1');
        }
    });

    it('will not allow to create referral code with same name (different campaign)', async () => {
        const samples = [
            'ExampleCode1',
            'ExampleCode1 ',
            'ExampleCode1  ',
            ' ExampleCode1',
            '  ExampleCode1',
            ' ExampleCode1 ',
            '  ExampleCode1  ',
        ];

        const campaignId = E2E_CURSOR.campaignIds[0];

        expect(campaignId).not.toBe(E2E_CURSOR.campaignId);

        for (const codeDisplay of samples) {
            const response: BazaError = (await dataAccessCodes.create({
                codeDisplay,
                associatedWithCampaignId: campaignId,
                isActive: true,
            })) as any;

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect(response.code).toBe(BazaReferralCodeErrorCodes.BazaReferralCodeDuplicate);

            const listResponse = await dataAccessCodes.list({
                index: 1,
                campaignId: E2E_CURSOR.campaignId,
            });

            expect(isBazaErrorResponse(listResponse)).toBeFalsy();
            expect(listResponse.items.length).toBe(1);
            expect(listResponse.items[0].code).toBe('examplecode1');
            expect(listResponse.items[0].codeDisplay).toBe('ExampleCode1');
        }
    });

    it('will not allow to create referral code with same name (account)', async () => {
        const samples = [
            'ExampleCode1',
            'ExampleCode1 ',
            'ExampleCode1  ',
            ' ExampleCode1',
            '  ExampleCode1',
            ' ExampleCode1 ',
            '  ExampleCode1  ',
        ];

        for (const codeDisplay of samples) {
            const response: BazaError = (await dataAccessCodes.create({
                codeDisplay,
                associatedWithAccountId: http.authResponse.jwtPayload.accountId,
                isActive: true,
            })) as any;

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect(response.code).toBe(BazaReferralCodeErrorCodes.BazaReferralCodeDuplicate);

            const listResponse = await dataAccessCodes.list({
                index: 1,
                campaignId: E2E_CURSOR.campaignId,
            });

            expect(isBazaErrorResponse(listResponse)).toBeFalsy();
            expect(listResponse.items.length).toBe(1);
            expect(listResponse.items[0].code).toBe('examplecode1');
            expect(listResponse.items[0].codeDisplay).toBe('ExampleCode1');
        }
    });

    it('will successfully create one more code in same campaign', async () => {
        const response = await dataAccessCodes.create({
            codeDisplay: 'ExampleCode2',
            associatedWithCampaignId: E2E_CURSOR.campaignId,
            isActive: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessCodes.list({
            index: 1,
            campaignId: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].code).toBe('examplecode2');
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode2');
        expect(listResponse.items[1].code).toBe('examplecode1');
        expect(listResponse.items[1].codeDisplay).toBe('ExampleCode1');
    });

    it('will successfully create one more code in different campaign', async () => {
        const campaignId = E2E_CURSOR.campaignIds[0];

        expect(campaignId).not.toBe(E2E_CURSOR.campaignId);

        const response = await dataAccessCodes.create({
            codeDisplay: 'ExampleCode3',
            associatedWithCampaignId: campaignId,
            isActive: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponseCampaign1 = await dataAccessCodes.list({
            index: 1,
            campaignId: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(listResponseCampaign1)).toBeFalsy();
        expect(listResponseCampaign1.items.length).toBe(2);
        expect(listResponseCampaign1.items[0].code).toBe('examplecode2');
        expect(listResponseCampaign1.items[0].codeDisplay).toBe('ExampleCode2');
        expect(listResponseCampaign1.items[1].code).toBe('examplecode1');
        expect(listResponseCampaign1.items[1].codeDisplay).toBe('ExampleCode1');

        const listResponseCampaign2 = await dataAccessCodes.list({
            index: 1,
            campaignId: campaignId,
        });

        expect(isBazaErrorResponse(listResponseCampaign2)).toBeFalsy();
        expect(listResponseCampaign2.items.length).toBe(1);
        expect(listResponseCampaign2.items[0].code).toBe('examplecode3');
        expect(listResponseCampaign2.items[0].codeDisplay).toBe('ExampleCode3');
    });

    it('will successfully edit existing code (keep same name)', async () => {
        const response = await dataAccessCodes.edit({
            id: E2E_CURSOR.codeId,
            codeDisplay: 'ExampleCode1',
            isActive: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessCodes.list({
            index: 1,
            campaignId: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].code).toBe('examplecode2');
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode2');
        expect(listResponse.items[1].code).toBe('examplecode1');
        expect(listResponse.items[1].codeDisplay).toBe('ExampleCode1');
        expect(listResponse.items[1].isActive).toBeFalsy();
    });

    it('will successfully edit existing code (with new name)', async () => {
        const response = await dataAccessCodes.edit({
            id: E2E_CURSOR.codeId,
            codeDisplay: 'examplecode1_!',
            isActive: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessCodes.list({
            index: 1,
            campaignId: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].code).toBe('examplecode2');
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode2');
        expect(listResponse.items[1].code).toBe('examplecode1_!');
        expect(listResponse.items[1].codeDisplay).toBe('examplecode1_!');
        expect(listResponse.items[1].isActive).toBeTruthy();
    });

    it('will not allow to edit existing code and set name with existing one', async () => {
        const samples = [
            'ExampleCode2',
            'ExampleCode2 ',
            'ExampleCode2  ',
            ' ExampleCode2',
            '  ExampleCode2',
            ' ExampleCode2 ',
            '  ExampleCode2  ',
        ];

        for (const codeDisplay of samples) {
            const response: BazaError = (await dataAccessCodes.edit({
                id: E2E_CURSOR.codeId,
                codeDisplay,
                isActive: true,
            })) as any;

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect(response.code).toBe(BazaReferralCodeErrorCodes.BazaReferralCodeDuplicate);

            const listResponse = await dataAccessCodes.list({
                index: 1,
                campaignId: E2E_CURSOR.campaignId,
            });

            expect(isBazaErrorResponse(listResponse)).toBeFalsy();
            expect(listResponse.items.length).toBe(2);
            expect(listResponse.items[0].code).toBe('examplecode2');
            expect(listResponse.items[0].codeDisplay).toBe('ExampleCode2');
            expect(listResponse.items[1].code).toBe('examplecode1_!');
            expect(listResponse.items[1].codeDisplay).toBe('examplecode1_!');
            expect(listResponse.items[1].isActive).toBeTruthy();
        }
    });

    it('will successfully delete existing code', async () => {
        const response = await dataAccessCodes.delete({
            id: E2E_CURSOR.codeId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessCodes.list({
            index: 1,
            campaignId: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].code).toBe('examplecode2');
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode2');
    });
});
