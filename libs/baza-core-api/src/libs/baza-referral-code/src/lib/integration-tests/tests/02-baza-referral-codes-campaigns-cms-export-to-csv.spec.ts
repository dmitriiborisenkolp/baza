import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { BazaReferralCampaignCmsNodeAccess } from '@scaliolabs/baza-core-node-access';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/02-baza-referral-codes-campaigns-cms-export-to-csv.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCampaign = new BazaReferralCampaignCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCampaigns],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully export list of campaigns to CSV', async () => {
        const response = await dataAccessCampaign.exportListToCSV({
            fields: [
                {
                    field: 'id',
                    title: 'ID',
                },
                {
                    field: 'name',
                    title: 'Name',
                },
                {
                    field: 'isActive',
                    title: 'Is Active?',
                },
                {
                    field: 'dateCreatedAt',
                    title: 'Created At',
                },
                {
                    field: 'dateLastJoinedByReferralCode',
                    title: 'Last Joined At',
                },
            ],
            listRequest: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(typeof response === 'string').toBeTruthy();
        expect(response.split('\n').length).toBe(4);
    });
});
