import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaReferralAccountCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { asyncExpect } from '../../../../../baza-test-utils/src';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/01-baza-referral-codes-campaigns-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessReferralAccounts = new BazaReferralAccountCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will display list of referral accounts', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessReferralAccounts.list({
                    index: 1,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(2);
                expect(response.items[0].email).toBe('e2e-2@scal.io');
                expect(response.items[1].email).toBe('e2e@scal.io');
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will allow to search in list', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessReferralAccounts.list({
                    index: 1,
                    queryString: 'User 2',
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(1);
                expect(response.items[0].email).toBe('e2e-2@scal.io');
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
