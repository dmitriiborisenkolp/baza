import { BazaE2eFixture, defineE2eFixtures } from '../../../../../baza-e2e-fixtures/src';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { Injectable } from '@nestjs/common';
import { BazaReferralCampaignService } from '../../services/baza-referral-campaign.service';
import { BazaReferralCampaignEntity } from '../../entities/baza-referral-campaign.entity';
import { ReferralCampaignCmsCreateRequest } from '@scaliolabs/baza-core-shared';

export const bazaNcIntegrationReferralCampaignsE2eFixture: Array<ReferralCampaignCmsCreateRequest> = [
    {
        isActive: true,
        name: 'Example Campaign 1',
    },
    {
        isActive: false,
        name: 'Example Campaign 2',
    },
    {
        isActive: true,
        name: 'Example Campaign 3',
    },
];

@Injectable()
export class BazaReferralCampaignsE2eFixture implements BazaE2eFixture {
    static CAMPAIGNS: Array<BazaReferralCampaignEntity> = [];

    constructor(
        private readonly campaignsService: BazaReferralCampaignService,
    ) {}

    async up(): Promise<void> {
        BazaReferralCampaignsE2eFixture.CAMPAIGNS = [];

        for (const request of bazaNcIntegrationReferralCampaignsE2eFixture) {
            const entity = await this.campaignsService.create(request);

            BazaReferralCampaignsE2eFixture.CAMPAIGNS.push(entity);
        }
    }
}

defineE2eFixtures<BazaReferralCodesE2eFixtures>([{
    fixture: BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCampaigns,
    provider: BazaReferralCampaignsE2eFixture,
}]);
