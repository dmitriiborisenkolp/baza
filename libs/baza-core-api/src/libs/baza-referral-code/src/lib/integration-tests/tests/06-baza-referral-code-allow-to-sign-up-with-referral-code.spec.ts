import 'reflect-metadata';
import { BazaAccountNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AccountErrorCodes, BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { BazaReferralCampaignDto, BazaReferralCodeDto, BazaReferralCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaReferralCampaignCmsNodeAccess, BazaReferralCodeCmsNodeAccess } from '@scaliolabs/baza-core-node-access';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/06-baza-nc-integration-referral-code-allow-to-sign-up-with-referral-code.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCampaign = new BazaReferralCampaignCmsNodeAccess(http);
    const dataAccessCodes = new BazaReferralCodeCmsNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    const E2E_CURSOR: {
        campaigns: Array<BazaReferralCampaignDto>;
        codes: Array<BazaReferralCodeDto>;
    } = {
        campaigns: [],
        codes: [],
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCodes],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        E2E_CURSOR.campaigns = (
            await dataAccessCampaign.list({
                index: 1,
            })
        ).items;

        for (const campaign of E2E_CURSOR.campaigns) {
            E2E_CURSOR.codes.push(
                ...(
                    await dataAccessCodes.list({
                        index: 1,
                        campaignId: campaign.id,
                    })
                ).items,
            );
        }

        http.noAuth();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will fail to register user with unknown referral code', async () => {
        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'e2e-ref-codes@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
            referralCode: 'something-unknown',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AccountErrorCodes.BazaAccountRegisterOrInviteCodeNotFound);
    });

    it('will fail to register user with inactive referral code', async () => {
        const inactive = E2E_CURSOR.codes.find((c) => !c.isActive);

        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'e2e-ref-codes@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
            referralCode: inactive.code,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaReferralCodeErrorCodes.BazaReferralCodeIsNotAvailableAnymore);
    });

    it('will fail to register user with inactive referral campaign and active referral code', async () => {
        const inactiveCampaign = E2E_CURSOR.campaigns.find((c) => !c.isActive);
        const inactiveCode = E2E_CURSOR.codes.find(
            (c) => c.isActive && !!c.associatedWithCampaign && c.associatedWithCampaign.id === inactiveCampaign.id,
        );

        expect(inactiveCode).toBeDefined();

        const response: BazaError = (await dataAccessAccount.registerAccount({
            email: 'e2e-ref-codes@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
            referralCode: inactiveCode.code,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaReferralCodeErrorCodes.BazaReferralCodeIsNotAvailableAnymore);
    });

    it('will successfully register user with active referral code (by codeDisplay)', async () => {
        const activeCampaign = E2E_CURSOR.campaigns.find((c) => c.isActive);
        const active = E2E_CURSOR.codes.find(
            (c) => c.isActive && !!c.associatedWithCampaign && c.associatedWithCampaign.id === activeCampaign.id,
        );

        const response = await dataAccessAccount.registerAccount({
            email: 'e2e-ref-codes-1@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
            referralCode: active.codeDisplay,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully register user with active referral code (by code)', async () => {
        const activeCampaign = E2E_CURSOR.campaigns.find((c) => c.isActive);
        const active = E2E_CURSOR.codes.find(
            (c) => c.isActive && c.associatedWithCampaign && c.associatedWithCampaign.id === activeCampaign.id,
        );

        const response = await dataAccessAccount.registerAccount({
            email: 'e2e-ref-codes-2@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
            referralCode: active.code,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
