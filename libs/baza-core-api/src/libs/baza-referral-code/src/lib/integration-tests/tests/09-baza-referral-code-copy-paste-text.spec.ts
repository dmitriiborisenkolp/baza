import 'reflect-metadata';
import {
    BazaAccountNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
    BazaReferralCodeCmsNodeAccess,
    BazaRegistryNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { asyncExpect } from '../../../../../baza-test-utils/src';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/09-baza-nc-integration-referral-code-copy-paste-text.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessReferralCodes = new BazaReferralCodeCmsNodeAccess(http);
    const dataAccessRegistry = new BazaRegistryNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCodes],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser2();
    });

    it('will add referral code', async () => {
        const accountId = http.authResponse.jwtPayload.accountId;

        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                return await dataAccessReferralCodes.create({
                    associatedWithAccountId: accountId,
                    isActive: true,
                    codeDisplay: 'JoinMyproject2021',
                });
            },
            (response) => {
                return isBazaErrorResponse(response);
            },
            { intervalMillis: 2000 },
        );
    });

    it('will contains referral code in bootstrap (web)', async () => {
        const response = await dataAccessAccount.registerAccountOptions();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.referralCode).toBe('JoinMyproject2021');
        expect(response.referralCodeCopyText).toBe('Join MyProject with my unique code and help me earn shares: JoinMyproject2021');
    });

    it('will contains referral code in bootstrap (mobile)', async () => {
        const response = await dataAccessAccount.registerAccountOptions();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.referralCode).toBe('JoinMyproject2021');
        expect(response.referralCodeCopyText).toBe('Join MyProject with my unique code and help me earn shares: JoinMyproject2021');
    });

    it('will allow to change copy-paste message with registry', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaAccounts.referralCodes.copyText',
            value: 'Join E2E server with {{ code }} referral code!',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will contains referral code in bootstrap with new message (web)', async () => {
        const response = await dataAccessAccount.registerAccountOptions();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.referralCode).toBe('JoinMyproject2021');
        expect(response.referralCodeCopyText).toBe('Join E2E server with JoinMyproject2021 referral code!');
    });

    it('will contains referral code in bootstrap with new message (mobile)', async () => {
        const response = await dataAccessAccount.registerAccountOptions();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.referralCode).toBe('JoinMyproject2021');

        // TODO: Fix it with next force update release
        // expect(response.referralCodeCopyText).toBe('Join E2E server with JoinMyproject2021 referral code!');
    });
});
