import 'reflect-metadata';
import { BazaAccountNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaReferralCodeCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { asyncExpect } from '../../../../../baza-test-utils/src';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/11-baza-nc-integration-default-or-active-referral-code-of-account.spec.ts', () => {
    let DEFAULT_CODE_ID: number;
    let NEW_CODE_ID: number;

    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessReferralCodes = new BazaReferralCodeCmsNodeAccess(http);
    const dataAccessAccount = new BazaAccountNodeAccess(http);
    const dataAccessAccountCMS = new BazaAccountCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCodes],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        await dataAccessAccount.registerAccount({
            email: 'e2e-ref-codes-11@scal.io',
            password: 'Scalio#1337!',
            firstName: 'Example',
            lastName: 'User',
        });

        const listResponse = await dataAccessAccountCMS.listAccounts({
            index: 1,
            roles: [AccountRole.User],
        });

        const registeredAccount = listResponse.items.find((a) => a.email === 'e2e-ref-codes-11@scal.io');

        await dataAccessAccountCMS.confirmEmailAccountById({
            id: registeredAccount.id,
            confirmed: true,
        });
    });

    beforeEach(async () => {
        await http.auth({
            email: 'e2e-ref-codes-11@scal.io',
            password: 'Scalio#1337!',
        });
    });

    it('will returns current active referral code and it will be marked as default', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessAccount.registerAccountOptions();

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.referralCode).toBeDefined();
            },
            null,
            { intervalMillis: 2000 },
        );

        // TODO: Fix it with next force update release
        // expect(bootstrapResponse.referralCode.startsWith('JoinExample')).toBeTruthy();
    });

    it('will create new default referral code and will set previous one as non-default', async () => {
        const accountId = http.authResponse.jwtPayload.accountId;

        await http.authE2eAdmin();

        const createResponse = await dataAccessReferralCodes.create({
            associatedWithAccountId: accountId,
            isDefault: true,
            isActive: true,
            codeDisplay: 'ExampleCode1',
        });

        expect(isBazaErrorResponse(createResponse)).toBeFalsy();

        const listResponse = await dataAccessReferralCodes.list({
            accountId: accountId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode1');
        expect(listResponse.items[0].isActive).toBeTruthy();
        expect(listResponse.items[0].isDefault).toBeTruthy();
        expect(listResponse.items[1].codeDisplay.startsWith('JoinExample')).toBeTruthy();
        expect(listResponse.items[1].isActive).toBeTruthy();
        expect(listResponse.items[1].isDefault).toBeFalsy();

        NEW_CODE_ID = listResponse.items[0].id;
        DEFAULT_CODE_ID = listResponse.items[1].id;
    });

    it('will set previous code as default and new code will not be default anymore', async () => {
        const accountId = http.authResponse.jwtPayload.accountId;

        await http.authE2eAdmin();

        const editResponse = await dataAccessReferralCodes.edit({
            id: DEFAULT_CODE_ID,
            codeDisplay: 'JoinMyproject2021',
            isDefault: true,
            isActive: true,
        });

        expect(isBazaErrorResponse(editResponse)).toBeFalsy();

        const listResponse = await dataAccessReferralCodes.list({
            accountId: accountId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode1');
        expect(listResponse.items[0].isActive).toBeTruthy();
        expect(listResponse.items[0].isDefault).toBeFalsy();
        expect(listResponse.items[1].codeDisplay).toBe('JoinMyproject2021');
        expect(listResponse.items[1].isActive).toBeTruthy();
        expect(listResponse.items[1].isDefault).toBeTruthy();
    });

    it('will display new code as default for bootstrap', async () => {
        const response = await dataAccessAccount.registerAccountOptions();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.referralCode).toBeDefined();

        expect(response.referralCode).toBe('JoinMyproject2021');
    });

    it('will add new active non-default referral code to account without affecting existing codes', async () => {
        const accountId = http.authResponse.jwtPayload.accountId;

        await http.authE2eAdmin();

        const createResponse = await dataAccessReferralCodes.create({
            associatedWithAccountId: accountId,
            isDefault: false,
            isActive: true,
            codeDisplay: 'ExampleCode2',
        });

        expect(isBazaErrorResponse(createResponse)).toBeFalsy();

        const listResponse = await dataAccessReferralCodes.list({
            accountId: accountId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(3);
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode2');
        expect(listResponse.items[0].isActive).toBeTruthy();
        expect(listResponse.items[0].isDefault).toBeFalsy();
        expect(listResponse.items[1].codeDisplay).toBe('ExampleCode1');
        expect(listResponse.items[1].isActive).toBeTruthy();
        expect(listResponse.items[1].isDefault).toBeFalsy();
        expect(listResponse.items[2].codeDisplay).toBe('JoinMyproject2021');
        expect(listResponse.items[2].isActive).toBeTruthy();
        expect(listResponse.items[2].isDefault).toBeTruthy();
    });

    it('will find & set default referral code for account after disabling existing one', async () => {
        const accountId = http.authResponse.jwtPayload.accountId;

        await http.authE2eAdmin();

        const createResponse = await dataAccessReferralCodes.edit({
            id: DEFAULT_CODE_ID,
            isDefault: true,
            isActive: false,
            codeDisplay: 'JoinMyproject2021',
        });

        expect(isBazaErrorResponse(createResponse)).toBeFalsy();

        const listResponse = await dataAccessReferralCodes.list({
            accountId: accountId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(3);
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode2');
        expect(listResponse.items[0].isActive).toBeTruthy();
        expect(listResponse.items[0].isDefault).toBeFalsy();
        expect(listResponse.items[1].codeDisplay).toBe('ExampleCode1');
        expect(listResponse.items[1].isActive).toBeTruthy();
        expect(listResponse.items[1].isDefault).toBeTruthy();
        expect(listResponse.items[2].codeDisplay).toBe('JoinMyproject2021');
        expect(listResponse.items[2].isActive).toBeFalsy();
        expect(listResponse.items[2].isDefault).toBeFalsy();
    });

    it('will find & set default referral code after removing existing default code', async () => {
        const accountId = http.authResponse.jwtPayload.accountId;

        await http.authE2eAdmin();

        const deleteResponse = await dataAccessReferralCodes.delete({
            id: NEW_CODE_ID,
        });

        expect(isBazaErrorResponse(deleteResponse)).toBeFalsy();

        const listResponse = await dataAccessReferralCodes.list({
            accountId: accountId,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].codeDisplay).toBe('ExampleCode2');
        expect(listResponse.items[0].isActive).toBeTruthy();
        expect(listResponse.items[0].isDefault).toBeTruthy();
        expect(listResponse.items[1].codeDisplay).toBe('JoinMyproject2021');
        expect(listResponse.items[1].isActive).toBeFalsy();
        expect(listResponse.items[1].isDefault).toBeFalsy();
    });
});
