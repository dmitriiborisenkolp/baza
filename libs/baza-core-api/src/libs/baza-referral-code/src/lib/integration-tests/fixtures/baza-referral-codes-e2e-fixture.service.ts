import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '../../../../../baza-e2e-fixtures/src';
import { ReferralCampaignCmsCreateRequest, BazaReferralCodeCmsCreateRequest } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeService } from '../../services/baza-referral-code.service';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { BazaReferralCampaignService } from '../../services/baza-referral-campaign.service';

interface BazaNcIntegrationReferralCodeFixture {
    campaign: ReferralCampaignCmsCreateRequest;
    codes: Array<BazaReferralCodeCmsCreateRequest>;
}

const bazaNcIntegrationReferralCodesE2eFixtures: Array<BazaNcIntegrationReferralCodeFixture> = [
    {
        campaign: {
            isActive: true,
            name: 'Example Campaign 1',
        },
        codes: [
            {
                codeDisplay: 'ExampleCode1_1',
                isActive: true,
            },
            {
                codeDisplay: 'ExampleCode1_2',
                isActive: true,
            },
        ],
    },
    {
        campaign: {
            isActive: false,
            name: 'Example Campaign 2',
        },
        codes: [
            {
                codeDisplay: 'ExampleCode2_1',
                isActive: false,
            },
            {
                codeDisplay: 'ExampleCode2_2',
                isActive: true,
            },
            {
                codeDisplay: 'ExampleCode2_3',
                isActive: false,
            },
        ],
    },
    {
        campaign: {
            isActive: true,
            name: 'Example Campaign 3',
        },
        codes: [
            {
                codeDisplay: 'ExampleCode3_1',
                isActive: false,
            },
            {
                codeDisplay: 'ExampleCode3_2',
                isActive: true,
            },
        ],
    },
];

@Injectable()
export class BazaReferralCodesE2eFixture implements BazaE2eFixture {
    constructor(
        private readonly campaignsService: BazaReferralCampaignService,
        private readonly codesService: BazaReferralCodeService,
    ) {}

    async up(): Promise<void> {
        for (const request of bazaNcIntegrationReferralCodesE2eFixtures) {
            const campaign = await this.campaignsService.create(request.campaign);

            for (const codeRequest of request.codes) {
                await this.codesService.create({
                    ...codeRequest,
                    associatedWithCampaignId: campaign.id,
                });
            }
        }
    }
}

defineE2eFixtures<BazaReferralCodesE2eFixtures>([{
    fixture: BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCodes,
    provider: BazaReferralCodesE2eFixture,
}]);
