import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaReferralCampaignErrorCodes } from '@scaliolabs/baza-core-shared';
import { BAZA_CRUD_DEFAULT_INDEX, BAZA_CRUD_DEFAULT_SIZE } from '../../../../../baza-crud/src';
import { BazaReferralCampaignCmsNodeAccess } from '@scaliolabs/baza-core-node-access';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/01-baza-referral-codes-campaigns-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCampaign = new BazaReferralCampaignCmsNodeAccess(http);

    const E2E_CURSOR: {
        campaignId?: number;
    } = {};

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will contains no campaigns at start', async () => {
        const listResponse = await dataAccessCampaign.list({
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(0);
    });

    it('will successfully create new campaign', async () => {
        const response = await dataAccessCampaign.create({
            name: 'Example Campaign',
            isActive: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe('Example Campaign');
        expect(response.isActive).toBeTruthy();

        E2E_CURSOR.campaignId = response.id;

        const listResponse = await dataAccessCampaign.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(1);
        expect(listResponse.pager.index).toBe(BAZA_CRUD_DEFAULT_INDEX);
        expect(listResponse.pager.size).toBe(BAZA_CRUD_DEFAULT_SIZE);
        expect(listResponse.items[0].id).toBe(E2E_CURSOR.campaignId);
        expect(listResponse.items[0].name).toBe('Example Campaign');
    });

    it('will not allow to create duplicate campaign', async () => {
        const samples = [
            'Example Campaign',
            'Example Campaign ',
            ' Example Campaign',
            'Example Campaign  ',
            '  Example Campaign',
            '  Example Campaign  ',
            ' Example Campaign ',
        ];

        for (const name of samples) {
            const response: BazaError = (await dataAccessCampaign.create({
                name,
                isActive: true,
            })) as any;

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect(response.code).toBe(BazaReferralCampaignErrorCodes.BazaReferralCampaignWithSameNameAlreadyExists);

            const listResponse = await dataAccessCampaign.list({
                index: 1,
            });

            expect(isBazaErrorResponse(listResponse)).toBeFalsy();
            expect(listResponse.items.length).toBe(1);
            expect(listResponse.items[0].id).toBe(E2E_CURSOR.campaignId);
            expect(listResponse.items[0].name).toBe('Example Campaign');
        }
    });

    it('will allow to create one more campaign', async () => {
        const response = await dataAccessCampaign.create({
            name: 'Example Campaign 2',
            isActive: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe('Example Campaign 2');
        expect(response.isActive).toBeTruthy();

        const listResponse = await dataAccessCampaign.list({
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].name).toBe('Example Campaign 2');
        expect(listResponse.items[1].id).toBe(E2E_CURSOR.campaignId);
        expect(listResponse.items[1].name).toBe('Example Campaign');
    });

    it('will allow to edit campaign', async () => {
        const response = await dataAccessCampaign.edit({
            id: E2E_CURSOR.campaignId,
            name: 'Example Campaign 1_modified',
            isActive: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe('Example Campaign 1_modified');
        expect(response.isActive).toBeTruthy();

        const listResponse = await dataAccessCampaign.list({
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].name).toBe('Example Campaign 2');
        expect(listResponse.items[1].id).toBe(E2E_CURSOR.campaignId);
        expect(listResponse.items[1].name).toBe('Example Campaign 1_modified');
    });

    it('will not allow to set campaign name as existing one if there is another name with new name exists', async () => {
        const samples = [
            'Example Campaign 2',
            'Example Campaign 2 ',
            ' Example Campaign 2',
            'Example Campaign 2  ',
            '  Example Campaign 2',
            '  Example Campaign 2  ',
            ' Example Campaign 2 ',
        ];

        for (const name of samples) {
            const response: BazaError = (await dataAccessCampaign.edit({
                id: E2E_CURSOR.campaignId,
                name,
                isActive: true,
            })) as any;

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect(response.code).toBe(BazaReferralCampaignErrorCodes.BazaReferralCampaignWithSameNameAlreadyExists);

            const listResponse = await dataAccessCampaign.list({
                index: 1,
            });

            expect(isBazaErrorResponse(listResponse)).toBeFalsy();
            expect(listResponse.items.length).toBe(2);
            expect(listResponse.items[0].name).toBe('Example Campaign 2');
            expect(listResponse.items[1].id).toBe(E2E_CURSOR.campaignId);
            expect(listResponse.items[1].name).toBe('Example Campaign 1_modified');
        }
    });

    it('will allow to set campaign name as same as it was', async () => {
        const response = await dataAccessCampaign.edit({
            id: E2E_CURSOR.campaignId,
            name: 'Example Campaign 1_modified',
            isActive: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe('Example Campaign 1_modified');
        expect(response.isActive).toBeFalsy();

        const listResponse = await dataAccessCampaign.list({
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].name).toBe('Example Campaign 2');
        expect(listResponse.items[1].id).toBe(E2E_CURSOR.campaignId);
        expect(listResponse.items[1].name).toBe('Example Campaign 1_modified');
    });

    it('will successfully delete existing campaign', async () => {
        const response = await dataAccessCampaign.delete({
            id: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessCampaign.list({
            index: 1,
        });

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(1);
        expect(listResponse.items[0].name).toBe('Example Campaign 2');

        E2E_CURSOR.campaignId = listResponse.items[0].id;
    });

    it('will returns referral campaign by id', async () => {
        const response = await dataAccessCampaign.getById({
            id: E2E_CURSOR.campaignId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.campaign).toBeDefined();
        expect(response.campaign.id).toBe(E2E_CURSOR.campaignId);
    });
});
