import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeCmsNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaReferralCodesE2eFixtures } from '../baza-referral-codes-e2e-fixtures';
import { asyncExpect } from '../../../../../baza-test-utils/src';

jest.setTimeout(240000);

describe('@baza-core-api/baza-referral-code/integration-tests/tests/10-baza-referral-codes-will-filter-invalid-symbols.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessReferralCodes = new BazaReferralCodeCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaReferralCodesE2eFixtures.BazaNcIntegrationReferralCodes],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser2();
    });

    it('will not allow to create referral code with empty symbols', async () => {
        await http.authE2eAdmin();

        const testCases: Array<string> = ['.', '..', '.!.', 'jo.DnnCommwal2021'];

        await asyncExpect(
            async () => {
                for (const referralCode of testCases) {
                    const response: BazaError = (await dataAccessReferralCodes.create({
                        associatedWithAccountId: http.authResponse.jwtPayload.accountId,
                        isActive: true,
                        codeDisplay: referralCode,
                    })) as any;

                    expect(isBazaErrorResponse(response)).toBeTruthy();
                    expect(response.code).toBe(BazaReferralCodeErrorCodes.BazaReferralCodeIsInvalid);
                }
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
