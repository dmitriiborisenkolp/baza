import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaCoreACL,
    BazaReferralAccountCmsEndpoint,
    BazaReferralAccountCmsEndpointPaths,
    BazaReferralAccountCmsExportToCsvListRequest,
    BazaReferralAccountCmsListRequest,
    BazaReferralAccountCmsListResponse,
    CoreOpenApiTags,
} from '@scaliolabs/baza-core-shared';
import { BazaReferralAccountService } from '../../services/baza-referral-account.service';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../../baza-auth/src';
import { WithAccessGuard } from '../../../../../baza-acl-guards/src';
import { AclNodes } from '../../../../../baza-acl/src';

@Controller()
@ApiTags(CoreOpenApiTags.BazaReferralCodeCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaCoreACL.ReferralCodes])
export class BazaReferralAccountCmsController implements BazaReferralAccountCmsEndpoint {
    constructor(private readonly service: BazaReferralAccountService) {}

    @Post(BazaReferralAccountCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Referral Accounts',
    })
    @ApiOkResponse({
        type: BazaReferralAccountCmsListResponse,
    })
    async list(@Body() request: BazaReferralAccountCmsListRequest): Promise<BazaReferralAccountCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaReferralAccountCmsEndpointPaths.exportListToCSV)
    @ApiOperation({
        summary: 'exportListToCSV',
        description: 'Exports Referral Accounts to CSV',
    })
    @ApiOkResponse()
    async exportListToCSV(@Body() request: BazaReferralAccountCmsExportToCsvListRequest): Promise<string> {
        return this.service.exportListToCSV(request);
    }
}
