import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaCoreACL, CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import {
    ReferralCampaignCmsCreateRequest,
    ReferralCampaignCmsDeleteRequest,
    ReferralCampaignCmsEditRequest,
    BazaReferralCampaignCmsEndpoint,
    BazaReferralCampaignCmsEndpointPaths,
    ReferralCampaignCmsExportListToCsvRequest,
    ReferralCampaignCmsGetByIdRequest,
    ReferralCampaignCmsGetByIdResponse,
    ReferralCampaignCmsListRequest,
    ReferralCampaignCmsListResponse,
    BazaReferralCampaignDto,
} from '@scaliolabs/baza-core-shared';
import { BazaReferralCampaignService } from '../../services/baza-referral-campaign.service';
import { BazaReferralCampaignMapper } from '../../mappers/baza-referral-campaign.mapper';
import { BazaReferralCampaignRepository } from '../../repositories/baza-referral-campaign.repository';
import { BazaReferralCodeMapper } from '../../mappers/baza-referral-code.mapper';
import { BazaReferralCodeRepository } from '../../repositories/baza-referral-code.repository';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../../baza-auth/src';
import { WithAccessGuard } from '../../../../../baza-acl-guards/src';
import { AclNodes } from '../../../../../baza-acl/src';

@Controller()
@ApiTags(CoreOpenApiTags.BazaReferralCodeCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaCoreACL.ReferralCodes])
export class BazaReferralCampaignCmsController implements BazaReferralCampaignCmsEndpoint {
    constructor(
        private readonly campaignService: BazaReferralCampaignService,
        private readonly campaignRepository: BazaReferralCampaignRepository,
        private readonly campaignMapper: BazaReferralCampaignMapper,
        private readonly codesMapper: BazaReferralCodeMapper,
        private readonly codesRepository: BazaReferralCodeRepository,
    ) {}

    @Post(BazaReferralCampaignCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Referral Campaigns',
    })
    @ApiOkResponse({
        type: ReferralCampaignCmsListResponse,
    })
    async list(@Body() request: ReferralCampaignCmsListRequest): Promise<ReferralCampaignCmsListResponse> {
        return this.campaignService.list(request);
    }

    @Post(BazaReferralCampaignCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns Referral Campaign by ID',
    })
    @ApiOkResponse({
        type: ReferralCampaignCmsGetByIdResponse,
    })
    async getById(@Body() request: ReferralCampaignCmsGetByIdRequest): Promise<ReferralCampaignCmsGetByIdResponse> {
        const { withFullListOfReferralCodes, id } = request;
        const entity = await this.campaignRepository.getById(id);

        const referralCodes = withFullListOfReferralCodes ? await this.codesRepository.findByCampaign(id) : null;

        return {
            campaign: await this.campaignMapper.entityToDTO(entity),
            referralCodes: referralCodes ? await this.codesMapper.entitiesToDTOs(referralCodes) : undefined,
        };
    }

    @Post(BazaReferralCampaignCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
        description: 'Create new Referral Campaign',
    })
    @ApiOkResponse({
        type: BazaReferralCampaignDto,
    })
    async create(@Body() request: ReferralCampaignCmsCreateRequest): Promise<BazaReferralCampaignDto> {
        const entity = await this.campaignService.create(request);

        return this.campaignMapper.entityToDTO(entity);
    }

    @Post(BazaReferralCampaignCmsEndpointPaths.edit)
    @ApiOperation({
        summary: 'edit',
        description: 'Edit existing Referral Campaign',
    })
    @ApiOkResponse({
        type: BazaReferralCampaignDto,
    })
    async edit(@Body() request: ReferralCampaignCmsEditRequest): Promise<BazaReferralCampaignDto> {
        const entity = await this.campaignService.edit(request);

        return this.campaignMapper.entityToDTO(entity);
    }

    @Post(BazaReferralCampaignCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
        description: 'Delete existing Referral Campaign and all referral codes inside Referral Campaign',
    })
    @ApiOkResponse()
    async delete(@Body() request: ReferralCampaignCmsDeleteRequest): Promise<void> {
        await this.campaignService.delete(request);
    }

    @Post(BazaReferralCampaignCmsEndpointPaths.exportListToCSV)
    @ApiOperation({
        summary: 'exportListToCSV',
        description: 'Export Referral Campaign list to CSV',
    })
    @ApiOkResponse()
    async exportListToCSV(@Body() request: ReferralCampaignCmsExportListToCsvRequest): Promise<string> {
        return this.campaignService.exportListToCSV(request);
    }
}
