import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaCoreACL, CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../../baza-auth/src';
import { WithAccessGuard } from '../../../../../baza-acl-guards/src';
import { AclNodes } from '../../../../../baza-acl/src';
import {
    BazaReferralCodeCmsCreateRequest,
    BazaReferralCodeCmsDeleteRequest,
    BazaReferralCodeCmsEditRequest,
    BazaReferralCodeCmsEndpoint,
    BazaReferralCodeCmsEndpointPaths,
    BazaReferralCodeCmsExportListToCSVRequest,
    BazaReferralCodeCmsGetByIdRequest,
    BazaReferralCodeCmsGetByIdResponse,
    BazaReferralCodeCmsListResponse,
    BazaReferralCodeDto,
    BazaReferralCodeCmsListRequest,
    bazaIsReferralCodeValid,
} from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeService } from '../../services/baza-referral-code.service';
import { BazaReferralCodeRepository } from '../../repositories/baza-referral-code.repository';
import { BazaReferralCodeMapper } from '../../mappers/baza-referral-code.mapper';
import { BazaReferralCodeUsageMapper } from '../../mappers/baza-referral-code-usage.mapper';
import { BazaReferralCodeUsageRepository } from '../../repositories/baza-referral-code-usage.repository';
import { BazaReferralCodeIsInvalidException } from '../../exceptions/baza-referral-code-is-invalid.exception';

@Controller()
@ApiTags(CoreOpenApiTags.BazaReferralCodeCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaCoreACL.ReferralCodes])
export class BazaReferralCodeCmsController implements BazaReferralCodeCmsEndpoint {
    constructor(
        private readonly codesService: BazaReferralCodeService,
        private readonly codesRepository: BazaReferralCodeRepository,
        private readonly codesUsageRepository: BazaReferralCodeUsageRepository,
        private readonly codesMapper: BazaReferralCodeMapper,
        private readonly codesUsageMapper: BazaReferralCodeUsageMapper,
    ) {}

    @Post(BazaReferralCodeCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List of Referral Codes',
    })
    @ApiOkResponse({
        type: BazaReferralCodeCmsListResponse,
    })
    async list(@Body() request: BazaReferralCodeCmsListRequest): Promise<BazaReferralCodeCmsListResponse> {
        return this.codesService.list(request);
    }

    @Post(BazaReferralCodeCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns Referral Code by ID',
    })
    @ApiOkResponse({
        type: BazaReferralCodeCmsGetByIdResponse,
    })
    async getById(@Body() request: BazaReferralCodeCmsGetByIdRequest): Promise<BazaReferralCodeCmsGetByIdResponse> {
        const { withFullStatistics, id } = request;
        const code = await this.codesRepository.getById(id);

        const referralCodeStatistics = withFullStatistics ? await this.codesUsageRepository.findAllByReferralCode(code) : null;

        return {
            referralCode: await this.codesMapper.entityToDTO(code),
            referralCodeStatistics: referralCodeStatistics ? await this.codesUsageMapper.entitiesToDTOs(referralCodeStatistics) : undefined,
        };
    }

    @Post(BazaReferralCodeCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
        description: 'Create new Referral Code associated with Account or Campaign',
    })
    @ApiOkResponse({
        type: BazaReferralCodeDto,
    })
    async create(@Body() request: BazaReferralCodeCmsCreateRequest): Promise<BazaReferralCodeDto> {
        if (!bazaIsReferralCodeValid(request.codeDisplay)) {
            throw new BazaReferralCodeIsInvalidException();
        }

        const entity = await this.codesService.create(request);

        return this.codesMapper.entityToDTO(entity);
    }

    @Post(BazaReferralCodeCmsEndpointPaths.edit)
    @ApiOperation({
        summary: 'edit',
        description: 'Edit existing Referral Code',
    })
    @ApiOkResponse({
        type: BazaReferralCodeDto,
    })
    async edit(@Body() request: BazaReferralCodeCmsEditRequest): Promise<BazaReferralCodeDto> {
        if (!bazaIsReferralCodeValid(request.codeDisplay)) {
            throw new BazaReferralCodeIsInvalidException();
        }

        const entity = await this.codesService.edit(request);

        return this.codesMapper.entityToDTO(entity);
    }

    @Post(BazaReferralCodeCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
        description: 'Delete existing Referral Code',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaReferralCodeCmsDeleteRequest): Promise<void> {
        await this.codesService.delete(request);
    }

    @Post(BazaReferralCodeCmsEndpointPaths.exportListToCSV)
    @ApiOperation({
        summary: 'exportListToCSV',
        description: 'Exports Referral Code list to CSV',
    })
    @ApiOkResponse()
    async exportListToCSV(@Body() request: BazaReferralCodeCmsExportListToCSVRequest): Promise<string> {
        return this.codesService.exportListToCSV(request);
    }
}
