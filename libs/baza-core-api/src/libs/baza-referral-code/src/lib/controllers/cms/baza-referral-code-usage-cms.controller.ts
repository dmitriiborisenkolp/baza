import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaCoreACL, CoreOpenApiTags } from '@scaliolabs/baza-core-shared';
import { AuthGuard, AuthRequireAdminRoleGuard } from '../../../../../baza-auth/src';
import { WithAccessGuard } from '../../../../../baza-acl-guards/src';
import { AclNodes } from '../../../../../baza-acl/src';
import {
    BazaReferralCodeCmsUsageByAccountExportListToCSVRequest,
    BazaReferralCodeCmsUsageByAccountListRequest,
    BazaReferralCodeCmsUsageByAccountListResponse,
    BazaReferralCodeCmsUsageByCampaignExportListToCSVRequest,
    BazaReferralCodeCmsUsageByCampaignListRequest,
    BazaReferralCodeCmsUsageByCampaignListResponse,
    BazaReferralCodeCmsUsageByCodeExportListToCSVRequest,
    BazaReferralCodeCmsUsageByCodeListRequest,
    BazaReferralCodeCmsUsageByCodeListResponse,
    BazaReferralCodeUsageCmsEndpoint,
    ReferralCodeUsageCmsEndpointPaths,
} from '@scaliolabs/baza-core-shared';
import { BazaReferralCodeUsageService } from '../../services/baza-referral-code-usage.service';

@Controller()
@ApiTags(CoreOpenApiTags.BazaReferralCodeCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaCoreACL.ReferralCodes])
export class BazaReferralCodeUsageCmsController implements BazaReferralCodeUsageCmsEndpoint {
    constructor(private readonly service: BazaReferralCodeUsageService) {}

    @Post(ReferralCodeUsageCmsEndpointPaths.listByReferralCode)
    @ApiOperation({
        summary: 'listByReferralCode',
        description: 'Returns referral code usage statistics',
    })
    @ApiOkResponse({
        type: BazaReferralCodeCmsUsageByCodeListResponse,
    })
    async listByReferralCode(
        @Body() request: BazaReferralCodeCmsUsageByCodeListRequest,
    ): Promise<BazaReferralCodeCmsUsageByCodeListResponse> {
        return this.service.listByReferralCode(request);
    }

    @Post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralCodeToCSV)
    @ApiOperation({
        summary: 'exportListByReferralCodeToCSV',
        description: 'Export list of referral code usages (By Referral Code) to CSV',
    })
    @ApiOkResponse()
    async exportListByReferralCodeToCSV(@Body() request: BazaReferralCodeCmsUsageByCodeExportListToCSVRequest): Promise<string> {
        return this.service.exportListByReferralCodeToCSV(request);
    }

    @Post(ReferralCodeUsageCmsEndpointPaths.listByReferralCampaign)
    @ApiOperation({
        summary: 'listByReferralCampaign',
        description: 'Returns usage statistics of referral codes inside campaign',
    })
    @ApiOkResponse({
        type: BazaReferralCodeCmsUsageByCampaignListResponse,
    })
    async listByReferralCampaign(
        @Body() request: BazaReferralCodeCmsUsageByCampaignListRequest,
    ): Promise<BazaReferralCodeCmsUsageByCampaignListResponse> {
        return this.service.listByReferralCampaign(request);
    }

    @Post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralCampaignToCSV)
    @ApiOperation({
        summary: 'exportListByReferralCampaignToCSV',
        description: 'Export list of referral code usages (By Campaign) to CSV',
    })
    @ApiOkResponse()
    async exportListByReferralCampaignToCSV(@Body() request: BazaReferralCodeCmsUsageByCampaignExportListToCSVRequest): Promise<string> {
        return this.service.exportListByReferralCampaignToCSV(request);
    }

    @Post(ReferralCodeUsageCmsEndpointPaths.listByReferralAccount)
    @ApiOperation({
        summary: 'listByReferralAccount',
        description: 'Export list of referral code usages (By Account) to CSV',
    })
    @ApiOkResponse()
    async listByReferralAccount(
        @Body() request: BazaReferralCodeCmsUsageByAccountListRequest,
    ): Promise<BazaReferralCodeCmsUsageByAccountListResponse> {
        return this.service.listByReferralAccount(request);
    }

    @Post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralAccountToCSV)
    @ApiOperation({
        summary: 'exportListByReferralAccountToCSV',
        description: 'Export list of referral code usages (By Account) to CSV',
    })
    @ApiOkResponse()
    async exportListByReferralAccountToCSV(@Body() request: BazaReferralCodeCmsUsageByAccountExportListToCSVRequest): Promise<string> {
        return this.service.exportListByReferralAccountToCSV(request);
    }
}
