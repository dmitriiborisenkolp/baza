import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaReferralCodeEntity } from '../entities/baza-referral-code.entity';
import { BazaReferralCodeNotFoundException } from '../exceptions/baza-referral-code-not-found.exception';
import { BazaReferralCampaignEntity } from '../entities/baza-referral-campaign.entity';
import { AccountEntity } from '../../../../baza-account/src';
import { bazaNormalizeReferralCode } from '@scaliolabs/baza-core-shared';

export const REFERRAL_CODE_RELATIONS: Array<keyof BazaReferralCodeEntity> = [
    'associatedWithAccount',
    'associatedWithCampaign',
];

@Injectable()
export class BazaReferralCodeRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaReferralCodeEntity> {
        return this.connection.getRepository(BazaReferralCodeEntity);
    }

    async save(entity: BazaReferralCodeEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async remove(entity: BazaReferralCodeEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async findById(id: number): Promise<BazaReferralCodeEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: REFERRAL_CODE_RELATIONS,
        });
    }

    async getById(id: number): Promise<BazaReferralCodeEntity> {
        const found = await this.findById(id);

        if (! found) {
            throw new BazaReferralCodeNotFoundException();
        }

        return found;
    }

    async findByCode(code: string): Promise<BazaReferralCodeEntity | undefined> {
        return this.repository.findOne({
            where: [{
                code: bazaNormalizeReferralCode(code),
            }],
            relations: REFERRAL_CODE_RELATIONS,
        });
    }

    async getByCode(code: string): Promise<BazaReferralCodeEntity> {
        const found = await this.findByCode(code);

        if (! found) {
            throw new BazaReferralCodeNotFoundException();
        }

        return found;
    }

    async findByCampaign(campaign: number | BazaReferralCampaignEntity): Promise<Array<BazaReferralCodeEntity>> {
        return this.repository.find({
            where: [{
                associatedWithCampaign: campaign,
            }],
            order: {
                id: 'ASC',
            },
            relations: REFERRAL_CODE_RELATIONS,
        });
    }

    async findByAccount(account: number | AccountEntity): Promise<Array<BazaReferralCodeEntity>> {
        return this.repository.find({
            where: [{
                associatedWithAccount: account,
            }],
            order: {
                id: 'ASC',
            },
            relations: REFERRAL_CODE_RELATIONS,
        });
    }
}
