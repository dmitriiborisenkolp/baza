import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaReferralCampaignEntity } from '../entities/baza-referral-campaign.entity';
import { BazaReferralCampaignNotFoundException } from '../exceptions/baza-referral-campaign-not-found.exception';
import { bazaNormalizeReferralCampaignName } from '@scaliolabs/baza-core-shared';

export const REFERRAL_CAMPAIGN_RELATIONS: Array<keyof BazaReferralCampaignEntity> = [];

@Injectable()
export class BazaReferralCampaignRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaReferralCampaignEntity> {
        return this.connection.getRepository(BazaReferralCampaignEntity);
    }

    async save(entity: BazaReferralCampaignEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async remove(entity: BazaReferralCampaignEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async findAll(): Promise<Array<BazaReferralCampaignEntity>> {
        return this.repository.find({
            relations: REFERRAL_CAMPAIGN_RELATIONS,
        });
    }

    async findById(id: number): Promise<BazaReferralCampaignEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: REFERRAL_CAMPAIGN_RELATIONS,
        });
    }

    async getById(id: number): Promise<BazaReferralCampaignEntity> {
        const found = await this.findById(id);

        if (! found) {
            throw new BazaReferralCampaignNotFoundException();
        }

        return found;
    }

    async findByCampaignName(campaignName: string): Promise<BazaReferralCampaignEntity> {
        return this.repository.findOne({
            where: [{
                name: bazaNormalizeReferralCampaignName(campaignName),
            }],
            relations: REFERRAL_CAMPAIGN_RELATIONS,
        });
    }
}
