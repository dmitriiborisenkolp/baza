import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaReferralCodeUsageEntity } from '../entities/baza-referral-code-usage.entity';
import { BazaReferralCodeEntity } from '../entities/baza-referral-code.entity';
import { AccountEntity } from '../../../../baza-account/src';

export const REFERRAL_CODE_USAGE_RELATIONS: Array<keyof BazaReferralCodeUsageEntity> = ['joinedAccount', 'referralCode'];

/**
 * Repository Service for BazaReferralCodeUsageEntity
 */
@Injectable()
export class BazaReferralCodeUsageRepository {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeORM Repository for BazaReferralCodeUsageEntity
     */
    get repository(): Repository<BazaReferralCodeUsageEntity> {
        return this.connection.getRepository(BazaReferralCodeUsageEntity);
    }

    /**
     * Returns all relations of BazaReferralCodeUsageEntity
     */
    get relations(): Array<keyof BazaReferralCodeUsageEntity> {
        return REFERRAL_CODE_USAGE_RELATIONS;
    }

    /**
     * Saves BazaReferralCodeUsageEntity to DB
     * @param usageEntity
     */
    async save(usageEntity: BazaReferralCodeUsageEntity): Promise<void> {
        await this.repository.save(usageEntity);
    }

    /**
     * Returns all existing Referral Code Usages by Referral Code (Entity or ID)
     * @param referralCode
     */
    async findAllByReferralCode(referralCode: BazaReferralCodeEntity | number): Promise<Array<BazaReferralCodeUsageEntity>> {
        return this.repository.find({
            where: [
                {
                    referralCode,
                },
            ],
            relations: this.relations,
        });
    }

    /**
     * Returns all existing Referral Code Usages by Joined Account (Entity or ID)
     * @param joinedAccount
     */
    async findAllByJoinedAccount(joinedAccount: AccountEntity | number): Promise<Array<BazaReferralCodeUsageEntity>> {
        return this.repository.find({
            where: [
                {
                    joinedAccount,
                },
            ],
            relations: this.relations,
        });
    }
}
