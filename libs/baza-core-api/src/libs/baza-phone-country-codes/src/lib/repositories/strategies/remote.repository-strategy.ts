import { Inject, Injectable } from '@nestjs/common';
import { PhoneCountryCodesRepositoryStrategy } from '../phone-country-codes.repository-strategy';
import { DEFAULT_REMOTE_REPOSITORY_URL } from '@scaliolabs/baza-core-shared';
import type { PhoneCountryCodesRepositoryDto } from '@scaliolabs/baza-core-shared';
import { BAZA_PHONE_COUNTRY_CODES } from '../../baza-phone-country-codes-api.config';
import type { BazaPhoneCountryCodesConfig, BazaPhoneCountryRemoteRepository } from '../../baza-phone-country-codes-api.config';
import { map } from 'rxjs/operators';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class RemoteRepositoryStrategy implements PhoneCountryCodesRepositoryStrategy {
    constructor(
        @Inject(BAZA_PHONE_COUNTRY_CODES) private readonly moduleConfig: BazaPhoneCountryCodesConfig,
        private readonly httpService: HttpService,
    ) {}

    async fetch(): Promise<PhoneCountryCodesRepositoryDto> {
        const custom = (this.moduleConfig.repository as BazaPhoneCountryRemoteRepository).url;
        const url = custom ? await custom() : DEFAULT_REMOTE_REPOSITORY_URL;

        return this.httpService
            .get<PhoneCountryCodesRepositoryDto>(url)
            .pipe(map((axiosResponse) => axiosResponse.data))
            .toPromise();
    }
}
