import { Inject, Injectable } from '@nestjs/common';
import { PhoneCountryCodesRepositoryStrategy } from '../phone-country-codes.repository-strategy';
import { phoneCountryCodesLocalRepository, PhoneCountryCodesRepositoryDto } from '@scaliolabs/baza-core-shared';
import { BAZA_PHONE_COUNTRY_CODES } from '../../baza-phone-country-codes-api.config';
import type { BazaPhoneCountryCodesConfig, BazaPhoneCountryLocalRepository } from '../../baza-phone-country-codes-api.config';

@Injectable()
export class LocalRepositoryStrategy implements PhoneCountryCodesRepositoryStrategy {
    constructor(
        @Inject(BAZA_PHONE_COUNTRY_CODES) private readonly moduleConfig: BazaPhoneCountryCodesConfig,
    ) {}

    async fetch(): Promise<PhoneCountryCodesRepositoryDto> {
        const custom = (this.moduleConfig.repository as BazaPhoneCountryLocalRepository).custom;

        return custom
            ? await custom()
            : phoneCountryCodesLocalRepository;
    }
}
