import { Inject, Injectable } from '@nestjs/common';
import { BAZA_PHONE_COUNTRY_CODE_STRATEGY, BAZA_PHONE_COUNTRY_CODES, BazaPhoneCountryRepositoryStrategy } from '../baza-phone-country-codes-api.config';
import type { BazaPhoneCountryCodesConfig } from '../baza-phone-country-codes-api.config';
import type { PhoneCountryCodesRepositoryStrategy } from './phone-country-codes.repository-strategy';
import { LocalRepositoryStrategy } from './strategies/local.repository-strategy';
import { RemoteRepositoryStrategy } from './strategies/remote.repository-strategy';
import { PhoneCountryCodesDto, PhoneCountryCodesRepositoryDto } from '@scaliolabs/baza-core-shared';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { BazaPccInvalidModuleConfigurationException } from '../exceptions/baza-pcc-invalid-module-configuration.exception';
import { BazaPccNotFoundException } from '../exceptions/baza-pcc-not-found.exception';

@Injectable()
export class PhoneCountryCodesRepository {
    private _source$: BehaviorSubject<PhoneCountryCodesRepositoryDto> = new BehaviorSubject(undefined);

    constructor(
        @Inject(BAZA_PHONE_COUNTRY_CODES) private readonly moduleConfig: BazaPhoneCountryCodesConfig,
        @Inject(BAZA_PHONE_COUNTRY_CODE_STRATEGY) private readonly customStrategy: PhoneCountryCodesRepositoryStrategy,
        private readonly localStrategy: LocalRepositoryStrategy,
        private readonly remoteStrategy: RemoteRepositoryStrategy,
    ) {}

    get strategy(): PhoneCountryCodesRepositoryStrategy {
        if (this.customStrategy) {
            return this.customStrategy;
        } else {
            switch (this.moduleConfig.repository.type) {
                default: {
                    throw new BazaPccInvalidModuleConfigurationException();
                }

                case BazaPhoneCountryRepositoryStrategy.Local: {
                    return this.localStrategy;
                }

                case BazaPhoneCountryRepositoryStrategy.Remote: {
                    return this.remoteStrategy;
                }
            }
        }
    }

    get source$(): Observable<PhoneCountryCodesRepositoryDto> {
        return this._source$.pipe(
            filter((s) => !! s),
        );
    }

    async refresh(): Promise<void> {
        this._source$.next(await this.strategy.fetch());
    }

    async repository(): Promise<PhoneCountryCodesRepositoryDto> {
        return this.source$.pipe(
            take(1),
        ).toPromise();
    }

    async getByNumericCode(numericCode: string): Promise<PhoneCountryCodesDto> {
        return this.source$.pipe(
            take(1),
            map((source) => {
                const found = source.find((e) => e.numericCode === numericCode);

                if (! found) {
                    throw new BazaPccNotFoundException();
                }

                return found;
            }),
        ).toPromise();
    }
}
