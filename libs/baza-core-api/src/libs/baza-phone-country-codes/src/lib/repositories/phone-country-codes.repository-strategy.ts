import { PhoneCountryCodesRepositoryDto } from '@scaliolabs/baza-core-shared';

export interface PhoneCountryCodesRepositoryStrategy {
    fetch(): Promise<PhoneCountryCodesRepositoryDto>;
}
