import { PhoneCountryCodesRepositoryDto } from '@scaliolabs/baza-core-shared';

export enum BazaPhoneCountryRepositoryStrategy {
    Local = 'local',
    Remote = 'remote',
}

export type BazaPhoneCountryRepository =
    { type: BazaPhoneCountryRepositoryStrategy.Local; custom?: () => Promise<PhoneCountryCodesRepositoryDto> } |
    { type: BazaPhoneCountryRepositoryStrategy.Remote, url?: () => Promise<string> }
;

export interface BazaPhoneCountryLocalRepository { type: 'local'; custom?: () => Promise<PhoneCountryCodesRepositoryDto> }
export interface BazaPhoneCountryRemoteRepository { type: 'remote', url?: () => Promise<string> }

export interface BazaPhoneCountryCodesConfig<CUSTOM = any> {
    repository: BazaPhoneCountryRepository | CUSTOM;
    ttl: number;
}

export const BAZA_PHONE_COUNTRY_CODES = Symbol();
export const BAZA_PHONE_COUNTRY_CODE_STRATEGY = Symbol();
