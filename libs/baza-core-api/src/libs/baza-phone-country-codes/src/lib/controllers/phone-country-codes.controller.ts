import { Controller, Get, Param } from '@nestjs/common';
import {
    BazaPhoneCountryCodesEndpoint,
    BazaPhoneCountryCodesEndpointPaths,
    PhoneCountryCodesDto,
    PhoneCountryCodesRepositoryDto,
} from '@scaliolabs/baza-core-shared';
import { PhoneCountryCodesRepository } from '../repositories/phone-country-codes.repository';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { CoreOpenApiTags } from '@scaliolabs/baza-core-shared';

@Controller()
@ApiTags(CoreOpenApiTags.BazaPhoneCountryCodes)
export class PhoneCountryCodesController implements BazaPhoneCountryCodesEndpoint {
    constructor(private readonly pcc: PhoneCountryCodesRepository) {}

    @Get(BazaPhoneCountryCodesEndpointPaths.repository)
    @ApiOperation({
        summary: 'repository',
        description: 'Returns full repository of phone country codes',
    })
    @ApiOkResponse({
        type: PhoneCountryCodesDto,
        isArray: true,
    })
    async repository(): Promise<PhoneCountryCodesRepositoryDto> {
        return this.pcc.repository();
    }

    @Get(BazaPhoneCountryCodesEndpointPaths.getByNumericCode)
    @ApiOperation({
        summary: 'getByNumericCode',
        description: 'Returns PhoneCountryCode DTO by numeric code (ID)',
    })
    @ApiOkResponse({
        type: PhoneCountryCodesDto,
    })
    async getByNumericCode(@Param('numericCode') numericCode: string): Promise<PhoneCountryCodesDto> {
        return this.pcc.getByNumericCode(numericCode);
    }
}
