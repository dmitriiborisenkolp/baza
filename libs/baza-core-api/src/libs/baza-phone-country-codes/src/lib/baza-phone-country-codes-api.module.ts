import { DynamicModule, Global, Inject, Module, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { BAZA_PHONE_COUNTRY_CODE_STRATEGY, BAZA_PHONE_COUNTRY_CODES } from './baza-phone-country-codes-api.config';
import type { BazaPhoneCountryCodesConfig } from './baza-phone-country-codes-api.config';
import { PhoneCountryCodesRepositoryStrategy } from './repositories/phone-country-codes.repository-strategy';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import { PhoneCountryCodesController } from './controllers/phone-country-codes.controller';
import { PhoneCountryCodesRepository } from './repositories/phone-country-codes.repository';
import { interval, Subject } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';
import { LocalRepositoryStrategy } from './repositories/strategies/local.repository-strategy';
import { RemoteRepositoryStrategy } from './repositories/strategies/remote.repository-strategy';
import { HttpModule } from '@nestjs/axios';
import { EnvService } from '../../../baza-env/src';

export interface StrategyType<T> {
    new (...args: any[]): T;
}

interface AsyncOptions {
    injects: Array<any>;
    useStrategy?: StrategyType<PhoneCountryCodesRepositoryStrategy>;
    useFactory(...args: Array<any>): Promise<BazaPhoneCountryCodesConfig>;
}

export { AsyncOptions as BazaPhoneCountryCodesApiModuleAsyncOptions };

@Module({})
export class BazaPhoneCountryCodesApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        const providers: Array<Provider> = [
            {
                provide: BAZA_PHONE_COUNTRY_CODES,
                inject: options.injects,
                useFactory: options.useFactory,
            },
        ];

        if (options.useStrategy) {
            providers.push({
                provide: BAZA_PHONE_COUNTRY_CODE_STRATEGY,
                useClass: options.useStrategy,
            });
        } else {
            providers.push({
                provide: BAZA_PHONE_COUNTRY_CODE_STRATEGY,
                useValue: null,
            });
        }

        return {
            module: BazaPhoneCountryCodesApiGlobalModule,
            providers,
            exports: [BAZA_PHONE_COUNTRY_CODES],
        };
    }
}

@Global()
@Module({
    imports: [HttpModule],
    controllers: [PhoneCountryCodesController],
    providers: [PhoneCountryCodesRepository, LocalRepositoryStrategy, RemoteRepositoryStrategy],
})
class BazaPhoneCountryCodesApiGlobalModule implements OnModuleInit, OnModuleDestroy {
    private stop$: Subject<void> = new Subject<void>();

    constructor(
        @Inject(BAZA_PHONE_COUNTRY_CODES) private readonly moduleConfig: BazaPhoneCountryCodesConfig,
        private readonly env: EnvService,
        private readonly repository: PhoneCountryCodesRepository,
    ) {}

    async onModuleInit(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            interval(this.moduleConfig.ttl)
                .pipe(startWith(0), takeUntil(this.stop$))
                .subscribe(() => {
                    this.repository.refresh();
                });
        } else {
            await this.repository.refresh();
        }
    }

    async onModuleDestroy(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            this.stop$.next();
        }
    }
}
