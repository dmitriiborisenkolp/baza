import { BazaPhoneCountryCodesErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class BazaPccInvalidModuleConfigurationException extends BazaAppException {
    constructor() {
        super(BazaPhoneCountryCodesErrorCodes.BazaPCCInvalidAPIModuleConfiguration, 'Invalid BazaPhoneCountryCodesApiModule configuration', HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
