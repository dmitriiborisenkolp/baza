import { BazaPhoneCountryCodesErrorCodes } from '@scaliolabs/baza-core-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '../../../../baza-exceptions/src';

export class BazaPccNotFoundException extends BazaAppException {
    constructor() {
        super(BazaPhoneCountryCodesErrorCodes.BazaPCCNotFound, 'Phone country code definition was not found', HttpStatus.NOT_FOUND);
    }
}
