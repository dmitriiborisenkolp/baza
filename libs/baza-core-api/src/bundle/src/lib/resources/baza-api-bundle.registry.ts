import { RegistrySchema } from '@scaliolabs/baza-core-shared';
import { bazaMaintenanceRegistry } from '../../../../libs/baza-maintenance/src';
import { bazaAuthRegistry } from '../../../../libs/baza-auth/src';
import { bazaVersionRegistry } from '../../../../libs/baza-version/src';
import { bazaApiCommonRegistry } from '../../../../libs/baza-common/src';
import { bazaAccountRegistry } from '../../../../libs/baza-account/src';

export const bazaApiBundleRegistry: RegistrySchema = {
    ...bazaApiCommonRegistry,
    ...bazaAuthRegistry,
    ...bazaAccountRegistry,
    ...bazaMaintenanceRegistry,
    ...bazaVersionRegistry,
}
