import './api-factory/api.unhandled-handlers';

import * as path from 'path';
import * as process from 'process';
import * as moment from 'moment';
import { HttpModule } from '@nestjs/axios';
import { DynamicModule, Global, Module, OnModuleInit } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RedisModule, RedisService } from '@liaoliaots/nestjs-redis';
import { APP_API_EXCEPTION_FILTERS } from './api-factory/api.exception-filters';
import { BazaApiBundleConfig } from './baza-api-bundle.config';
import { defaultBazaApiBundleModulesConfig } from './baza-api-bundle.module-configs';
import { BAZA_COMMON_I18N_CONFIG } from '@scaliolabs/baza-core-shared';
import { BazaE2eFixturesApiModule } from '../../../libs/baza-e2e-fixtures/src';
import { BazaStatusModule } from '../../../libs/baza-status/src';
import { BazaE2eApiModule } from '../../../libs/baza-e2e/src';
import { BazaAuthModule } from '../../../libs/baza-auth/src';
import { BazaCreditCardApiModule } from '../../../libs/baza-credit-card/src';
import { BazaSentryApiModule } from '../../../libs/baza-sentry/src';
import { BazaEnvModule, EnvService } from '../../../libs/baza-env/src';
import { BazaCqrsModule, BazaEventBusApiModule } from '../../../libs/baza-event-bus/src';
import { BazaRegistryApiModule } from '../../../libs/baza-registry/src';
import {
    accountNameMask,
    addressMask,
    bankAccountRoutingMask,
    bazaLoggerMaskField,
    BazaLoggerModule,
    creditCardNameMask,
    creditCardNumberMask,
    CVVMask,
    dateOfBirthMask,
    emailMask,
    firstNameMask,
    fullNameMask,
    jwtTokenMask,
    lastNameMask,
    phoneMask,
    pinoLoggerConfigBuilder,
    ssnMask,
} from '../../../libs/baza-logger/src';
import { BazaAclModule } from '../../../libs/baza-acl/src';
import { BazaI18nApiModule, I18nLanguageService } from '../../../libs/baza-i18n/src';
import { BazaAccountApiModule } from '../../../libs/baza-account/src';
import { BazaPasswordApiModule } from '../../../libs/baza-password/src';
import { BazaPhoneCountryCodesApiModule } from '../../../libs/baza-phone-country-codes/src';
import { BazaApiCommonModule } from '../../../libs/baza-common/src';
import { BazaAwsModule } from '../../../libs/baza-aws/src';
import { BazaKafkaApiModule } from '../../../libs/baza-kafka/src';
import { BazaMaintenanceApiModule } from '../../../libs/baza-maintenance/src';
import { BazaMailModule } from '../../../libs/baza-mail/src';
import { BazaAttachmentModule } from '../../../libs/baza-attachment/src';
import { BazaVersionModule } from '../../../libs/baza-version/src';
import { BazaI18nApiDetectorsModule, BazaI18nApiDetectorsService } from '../../../libs/baza-i18n-detectors/src';
import { BazaAclApiControllersModule } from '../../../libs/baza-acl-controllers/src/lib/baza-acl-api-controllers.module';
import { BazaAuthApiControllersModule } from '../../../libs/baza-auth-controllers/src';
import { BazaAclApiGuardsModule } from '../../../libs/baza-acl-guards/src';
import { BazaRegistryApiControllersModule } from '../../../libs/baza-registry-controllers/src';
import { BazaAccountApiControllersModule } from '../../../libs/baza-account-controllers/src';
import { BazaHtmlApiModule } from '../../../libs/baza-html/src';
import { BazaDebugApiModule } from '../../../libs/baza-debug/src';
import { ThrottlerGuard, ThrottlerModule } from '@nestjs/throttler';
import { APP_GUARD } from '@nestjs/core';
import { BazaApiFeatureModule } from '../../../libs/baza-feature/src';
import { BazaWhitelistAccountApiModule } from '../../../libs/baza-whitelist-account/src';
import { BazaAuthSessionModule } from '../../../libs/baza-auth-session/src';
import { BazaCoreSessionLockApiModule } from '../../../libs/baza-session-lock/src';
import { BazaInviteCodeApiModule } from '../../../libs/baza-invite-code/src';
import { BazaReferralCodesApiModule } from '../../../libs/baza-referral-code/src';
import { BazaAccountRegisterApiModule } from '../../../libs/baza-account-register/src';
import { BazaAccountBootstrapApiModule } from '../../../libs/baza-account-bootstrap/src';
import { BazaDeviceTokenApiModule } from '../../../libs/baza-device-token/src';
import { LogStartCounterModel } from './models/log-start-counter.model';
import { BazaHealthModule } from '../../../libs/baza-health/src';
import { LoggerModule } from 'nestjs-pino';
import { BazaDbStressTestsApiModule } from '../../../libs/baza-db-stress-tests/src';

moment.locale(BAZA_COMMON_I18N_CONFIG.defaultMomentLanguage);

const envFileConfigPath = path.join(process.cwd(), 'envs', process.env.ENV_FILE_PATH || `${process.env.NODE_ENV || ''}.env`);

EnvService.importEnvFile({
    filePath: envFileConfigPath,
});

export const API_ENV_CONFIG_PATH = Symbol();

@Module({})
export class BazaApiBundleModule {
    static forRootAsync(config: BazaApiBundleConfig): DynamicModule {
        const moduleConfigs = {
            ...defaultBazaApiBundleModulesConfig,
            ...config.modulesConfig,
        };

        const NEST_MODULES = [
            HttpModule,
            TypeOrmModule.forRootAsync(moduleConfigs.TypeOrmModule(config)),
            RedisModule.forRootAsync(moduleConfigs.RedisModule(config)),
            ThrottlerModule.forRootAsync({
                imports: [BazaEnvModule],
                inject: [EnvService],
                useFactory: async (env: EnvService) => {
                    if (env.isTestEnvironment) {
                        return {
                            ttl: 0,
                            limit: 65535,
                        };
                    } else {
                        return {
                            ttl: env.getAsNumeric('THROTTLER_TTL', {
                                defaultValue: 0,
                            }),
                            limit: env.getAsNumeric('THROTTLER_LIMIT', {
                                defaultValue: 655535,
                            }),
                            ...config.throttlerOptions,
                        };
                    }
                },
            }),
        ];

        const BAZA_MODULES = [
            BazaEnvModule,
            BazaLoggerModule,
            BazaCqrsModule.forRootAsync(),
            BazaAclApiControllersModule,
            BazaAclApiGuardsModule,
            BazaStatusModule,
            BazaHealthModule,
            BazaAttachmentModule,
            BazaAccountApiControllersModule,
            BazaAccountRegisterApiModule,
            BazaAccountBootstrapApiModule,
            BazaE2eApiModule,
            BazaE2eFixturesApiModule,
            BazaApiCommonModule.forRootAsync(moduleConfigs.BazaApiCommonModule(config)),
            BazaKafkaApiModule.forRootAsync(moduleConfigs.BazaKafkaApiModule(config)),
            BazaVersionModule.forRootAsync(moduleConfigs.BazaVersionModule(config)),
            BazaEventBusApiModule.forRootAsync(moduleConfigs.BazaEventBusApiModule(config)),
            BazaMailModule.forRootAsync(moduleConfigs.BazaMailModule(config)),
            BazaAwsModule.forRootAsync(moduleConfigs.BazaAwsModule(config)),
            BazaI18nApiModule.forRootAsync(moduleConfigs.BazaI18nApiModule(config)),
            BazaI18nApiDetectorsModule.forRootAsync(moduleConfigs.BazaI18nApiDetectorsModule(config)),
            BazaAccountApiModule.forRootAsync(moduleConfigs.BazaAccountModule(config)),
            BazaAclModule.forRootAsync(moduleConfigs.BazaAclModule(config)),
            BazaAuthModule.forRootAsync(moduleConfigs.BazaAuthModule(config)),
            BazaAuthApiControllersModule,
            BazaPhoneCountryCodesApiModule.forRootAsync(moduleConfigs.BazaPhoneCountryCodesApiModule(config)),
            BazaRegistryApiModule.forRootAsync(moduleConfigs.BazaRegistryApiModule(config)),
            BazaRegistryApiControllersModule,
            BazaWhitelistAccountApiModule.forRootAsync(),
            BazaMaintenanceApiModule.forRootAsync(moduleConfigs.BazaMaintenanceApiModule(config)),
            BazaSentryApiModule.forRootAsync(moduleConfigs.BazaSentryApiModule(config)),
            BazaPasswordApiModule,
            BazaCreditCardApiModule,
            BazaHtmlApiModule,
            BazaDebugApiModule,
            BazaApiFeatureModule.forRootAsync(moduleConfigs.BazaApiFeatureModule(config)),
            BazaAuthSessionModule.forRootAsync(moduleConfigs.BazaAuthSessionModule(config)),
            BazaCoreSessionLockApiModule,
            BazaInviteCodeApiModule,
            BazaReferralCodesApiModule,
            BazaDeviceTokenApiModule,
            BazaDbStressTestsApiModule,
            // pino logger module
            LoggerModule.forRootAsync({
                inject: [EnvService],
                useFactory: async (env: EnvService) => pinoLoggerConfigBuilder(env),
            }),
        ];

        return {
            module: BazaApiBundleGlobalModule,
            imports: [...NEST_MODULES, ...BAZA_MODULES],
            providers: [
                {
                    provide: API_ENV_CONFIG_PATH,
                    useValue: envFileConfigPath,
                },
                {
                    provide: APP_GUARD,
                    useClass: ThrottlerGuard,
                },
                ...APP_API_EXCEPTION_FILTERS,
            ],
        };
    }
}

@Global()
@Module({})
class BazaApiBundleGlobalModule implements OnModuleInit {
    constructor(
        private readonly i18nLanguage: I18nLanguageService,
        private readonly i18nDefaultDetectors: BazaI18nApiDetectorsService,
        private readonly redis: RedisService,
    ) {}

    /**
     * Setting up Baza API Core Bundle
     */
    async onModuleInit(): Promise<void> {
        this.initI18nDetectors();
        this.initBazaLoggerConfig();
        await this.logStartCounter();
    }

    /**
     * Init i18n library
     */
    initI18nDetectors(): void {
        this.i18nLanguage.detectors = this.i18nDefaultDetectors.defaultDetectors;
    }

    /**
     * Setting up uptime data to Redis
     */
    async logStartCounter(): Promise<void> {
        await this.redis.getClient().incr(LogStartCounterModel.BAZA_API_STARTS_COUNTER);
        await this.redis.getClient().set(LogStartCounterModel.BAZA_API_LAST_STARTED_DATE, new Date().toISOString());
    }

    /**
     * Initial masking configuration for Baza Logger
     */
    initBazaLoggerConfig(): void {
        bazaLoggerMaskField([
            { field: 'firstName', strategy: firstNameMask },
            { field: 'lastName', strategy: lastNameMask },
            { field: 'fullName', strategy: fullNameMask },
            { field: 'accountName', strategy: accountNameMask },
            { field: 'dob', strategy: dateOfBirthMask },
            { field: 'address1', strategy: addressMask },
            { field: 'address2', strategy: addressMask },
            { field: 'primAddress1', strategy: addressMask },
            { field: 'primAddress2', strategy: addressMask },
            { field: 'address2', strategy: addressMask },
            { field: 'email', strategy: emailMask },
            { field: 'emailAddress', strategy: emailMask },
            { field: 'phone', strategy: phoneMask },
            { field: 'ssn', strategy: ssnMask },
            { field: 'socialSecurityNumber', strategy: ssnMask },
            { field: 'creditCardName', strategy: creditCardNameMask },
            { field: 'creditCardNumber', strategy: creditCardNumberMask },
            { field: 'cvvNumber', strategy: CVVMask },
            { field: 'accountNumber', strategy: bankAccountRoutingMask },
            { field: 'accountRoutingNumber', strategy: bankAccountRoutingMask },
            /* Jwt token */
            { field: 'authorization', strategy: jwtTokenMask },
            { field: 'refreshToken', strategy: jwtTokenMask },
            { field: 'jwt', strategy: jwtTokenMask },
            { field: 'accessToken', strategy: jwtTokenMask },
        ]);
    }
}
