import { NestExpressApplication } from '@nestjs/platform-express';
import * as process from 'process';
import * as path from 'path';
import {
    AccessTokenExpiresSettingsMany,
    AccountRole,
    Application,
    AttachmentConstants,
    AttachmentImageType,
    AttachmentSizeSetItem,
    Auth2FAVerificationMethod,
    BazaAccountConstants,
    generateRandomHexString,
    HexColor,
    ProjectLanguage,
    RefreshTokenExpiresSettingsMany,
    RegistrySchema,
    ConfirmationEmailExpiryResendStrategy,
    BazaEnvironments,
} from '@scaliolabs/baza-core-shared';
import { BazaApiBundleModulesConfig } from './baza-api-bundle.module-configs';
import { NodeOptions } from '@sentry/node';
import { BazaExceptionHandler } from '../../../libs/baza-exception-filters/src';
import { AuthJwtDeviceStrategies } from '../../../libs/baza-auth/src';
import { ThrottlerModuleOptions } from '@nestjs/throttler/dist/throttler-module-options.interface';
import { SwaggerCustomOptions } from '@nestjs/swagger/dist/interfaces';
import { DocumentBuilder } from '@nestjs/swagger';
import { BazaFeatures } from '../../../libs/baza-feature/src';
import { AccountEntity, BazaAccountConfigFeatures, BazaAccountFeatures } from '../../../libs/baza-account/src';
import { SendMailCustomerIoBootstrap } from '../../../libs/baza-mail/src';
import { bazaLoggerConfig, BazaLoggerMaskConfig, bazaLoggerMaskField } from '../../../libs/baza-logger/src';

type BazaApiConfig2FAOptions = {
    role: AccountRole;
    methods: Array<Auth2FAVerificationMethod>;
};

export interface BazaApiBundleConfig {
    /**
     * API APP Factory
     */
    apiFactory: () => Promise<NestExpressApplication>;

    /**
     * List of allowed applications
     */
    applications: Array<Application>;

    /**
     * Features configuration
     */
    features: BazaFeatures;

    /**
     * Sentry Configuration
     */
    sentry?: {
        dsn: string;
        nodeOptions?: Partial<NodeOptions>;
    };

    /**
     * Base path for mail template paths
     */
    mailTemplatePaths: string;

    /**
     * Additional exception handlers
     */
    exceptionHandlers: (defaultExceptionHandlers: Array<BazaExceptionHandler>) => Array<BazaExceptionHandler>;

    /**
     * Registry Schema Factory
     */
    registrySchemaFactory: (defaults: RegistrySchema) => RegistrySchema;

    /**
     * Array of i18n resources (as objects)
     */
    i18n: Array<BazaApiBundleI18nConfig>;

    /**
     * Device strategy for Auth (Redis or Psql)
     */
    authDevicesStrategy: AuthJwtDeviceStrategies;

    /**
     * Expiration settings for JWT access tokens
     */
    authAccessTokenExpiresSettings: AccessTokenExpiresSettingsMany;

    /**
     * Expiration settings for JWT refresh tokens
     */
    authRefreshTokenExpiresSettings: RefreshTokenExpiresSettingsMany;

    /**
     * Swagger Tags for API documentation
     */
    tagGroupsFactory: (defaults: Array<TagGroup>) => Array<TagGroup>;

    /**
     * Endpoint blacklist
     * Add endpoints to ban usage
     */
    endpointsBlacklist: Array<any>;

    /**
     * Partial update default module configuration
     */
    modulesConfig: Partial<BazaApiBundleModulesConfig>;

    /**
     * Ignore requesting X-Baza-* headers for specific endpoints
     */
    ignoreHeadersForEndpoints: Array<string>;

    /**
     * Ignore reporting errors to Sentry for specific endpoints
     */
    ignoreReportErrorsCodesToSentry: Array<string>;

    /**
     * Nest.JS throttler options
     */
    throttlerOptions: Exclude<ThrottlerModuleOptions, 'limit' | 'ttl'>;

    /**
     * Custom swagger options
     */
    swaggerCustomOptions?: SwaggerCustomOptions;

    /**
     * Custom API Swagger documentation builder
     */
    documentationBuilder?: (builder: DocumentBuilder) => void;

    /**
     * Custom tokenizer for verification emails
     */
    customEmailVerificationTokenizer?: (account: AccountEntity<any>) => string;

    /**
     * Automatically clean up Auth Sessions logs after N days
     */
    autoCleanUpAuthSessionLogsAfterNDays: number;

    /**
     * 2FA Configuration
     */
    with2FAMethods: Array<BazaApiConfig2FAOptions>;

    /**
     * Force 2FA for specific roles (by default Admin only)
     */
    force2FAForAccountRoles: Array<AccountRole>;

    /**
     * Email Resend strategy for expired confirmation emails
     */
    confirmationEmailExpiryResendStrategy: ConfirmationEmailExpiryResendStrategy;

    /**
     * List of endpoints which should ignore Maintenance Mode
     */
    ignoreMaintenanceModeForEndpoints: Array<string>;

    /**
     * Account Features Configuration
     */
    accountFeatures: BazaAccountConfigFeatures;

    /**
     * Enable NestJs WhiteList Option
     * Disabled by default
     */
    enableWhiteList: boolean;

    /**
     * Enables Mail Template Validation
     * When enabled, API will check that all template variables which are used in send mail request has full documentation
     */
    enableMailTemplateValidation: boolean;

    /**
     * Enables Customer IO Templates
     * Works for Mail Configuration with Customer.IO Mail Transport
     */
    enableCustomerIoTemplates: boolean;

    /**
     * Pre-define Customer.IO Transaction Email Id's for Baza Mail Templates
     * Works for Mail Configuration with Customer.IO Mail Transport
     */
    customerIoTemplatesBootstrap: (env: BazaEnvironments) => Array<SendMailCustomerIoBootstrap>;

    loggerMaskConfig: Array<BazaLoggerMaskConfig>;
}

export interface BazaApiBundleI18nConfig {
    app: Application;
    factory: (language: ProjectLanguage, defaults: (language: ProjectLanguage) => any) => any;
}

interface TagGroup {
    name: string;
    tags: Array<string>;
}

export class BazaApiBundleConfigBuilder {
    private _config: BazaApiBundleConfig = {
        modulesConfig: {},
        apiFactory: () => {
            console.error('[baza-api-bundle] No API factory provided');
            console.error('Please inject your API factory with bazaApuBundleConfigBuilder.withApiFactory method');

            throw new Error();
        },
        applications: Object.values(Application),
        features: {},
        mailTemplatePaths: path.join(process.cwd(), 'apps/api/i18n/{lang}/mail-templates'),
        exceptionHandlers: (defaultExceptionHandlers) => [...defaultExceptionHandlers],
        registrySchemaFactory: (defaults: RegistrySchema) => ({
            ...defaults,
        }),
        i18n: Object.values(Application).map((app) => ({
            app,
            factory: (language, defaults) => ({ ...defaults(language) }),
        })),
        authDevicesStrategy: AuthJwtDeviceStrategies.Psql,
        authAccessTokenExpiresSettings: [
            { role: AccountRole.Admin, expiresIn: 0.5 * /* hours */ 60 /* min */ * 60 /* seconds */ },
            { role: AccountRole.User, expiresIn: 8 * /* hours */ 60 /* min */ * 60 /* seconds */ },
        ],
        authRefreshTokenExpiresSettings: [
            { role: AccountRole.Admin, expiresIn: 4 * /* hours */ 60 /* min */ * 60 /* seconds */ },
            { role: AccountRole.User, expiresIn: 30 /* days */ * 24 * /* hours */ 60 /* min */ * 60 /* seconds */ },
        ],
        tagGroupsFactory: (defaults) => [...defaults],
        endpointsBlacklist: [],
        ignoreHeadersForEndpoints: [],
        ignoreReportErrorsCodesToSentry: [],
        throttlerOptions: {},
        autoCleanUpAuthSessionLogsAfterNDays: 90,
        with2FAMethods: [
            {
                role: AccountRole.User,
                methods: [Auth2FAVerificationMethod.Email],
            },
            {
                role: AccountRole.Admin,
                methods: [Auth2FAVerificationMethod.Email, Auth2FAVerificationMethod.GoogleAuthenticator],
            },
        ],
        force2FAForAccountRoles: [AccountRole.Admin],
        confirmationEmailExpiryResendStrategy: ConfirmationEmailExpiryResendStrategy.AUTO,
        ignoreMaintenanceModeForEndpoints: [],
        accountFeatures: [
            {
                role: AccountRole.User,
                enabled: [
                    BazaAccountFeatures.ChangeEmail,
                    BazaAccountFeatures.ChangePassword,
                    BazaAccountFeatures.ResetPassword,
                    BazaAccountFeatures.Deactivate,
                ],
            },
            {
                role: AccountRole.Admin,
                enabled: [BazaAccountFeatures.ChangePassword, BazaAccountFeatures.ResetPassword],
            },
        ],
        enableWhiteList: true,
        enableMailTemplateValidation: true,
        enableCustomerIoTemplates: false,
        customerIoTemplatesBootstrap: () => [],
        loggerMaskConfig: bazaLoggerConfig().masked,
    };

    /**
     * Returns current API configuration
     */
    get config(): BazaApiBundleConfig {
        return this._config;
    }

    /**
     * Set up API features
     * @param features
     */
    withFeatures(features: BazaFeatures): BazaApiBundleConfigBuilder {
        Object.assign(this._config.features, features);

        return this;
    }

    /**
     * [REQUIRED] Provide API factory. API factory should returns NestExpressApplication
     */
    withApiFactory(factory: () => Promise<NestExpressApplication>): BazaApiBundleConfigBuilder {
        this._config.apiFactory = factory;

        return this;
    }

    /**
     * Set up list of project applications.
     * This configuration affects Maintenance CMS
     */
    withApplications(applications: Array<Application>): BazaApiBundleConfigBuilder {
        this._config.applications = applications;

        return this;
    }

    /**
     * Provides custom module configuration for Baza Core modules
     */
    withModuleConfigs(moduleConfigs: Partial<BazaApiBundleModulesConfig>): BazaApiBundleConfigBuilder {
        this._config.modulesConfig = {
            ...this._config.modulesConfig,
            ...moduleConfigs,
        };

        return this;
    }

    /**
     * Specify Nest.JS Exception Handlers for API
     */
    withExceptionHandlers(exceptionHandlers: (defaultExceptionHandlers: Array<BazaExceptionHandler>) => Array<BazaExceptionHandler>) {
        this._config.exceptionHandlers = exceptionHandlers;

        return this;
    }

    /**
     * Set up path where mail templates are stored
     */
    withMailTemplatePaths(mailTemplatePath: string): BazaApiBundleConfigBuilder {
        this._config.mailTemplatePaths = mailTemplatePath;

        return this;
    }

    /**
     * Set up additional Registry CMS entries
     */
    withRegistrySchema(registrySchemaFactory: (defaults: RegistrySchema) => RegistrySchema): BazaApiBundleConfigBuilder {
        this._config.registrySchemaFactory = registrySchemaFactory;

        return this;
    }

    /**
     * Set up i18n resource factories.
     * These resources are used for API, CMS, IOS, Android and Web applications
     */
    withI18n(
        commands: Array<{ app: Application; factory: (language: ProjectLanguage, defaults: (language: ProjectLanguage) => any) => any }>,
    ): BazaApiBundleConfigBuilder {
        for (const command of commands) {
            const found = this._config.i18n.find((i18n) => i18n.app === command.app);

            if (found) {
                found.factory = command.factory;
            } else {
                this._config.i18n.push({
                    app: command.app,
                    factory: command.factory,
                });
            }
        }

        return this;
    }

    /**
     * Set up auth devices strategy
     *  - AuthJwtDeviceStrategies.Psql stores JWT tokens in Psql database
     *  - AuthJwtDeviceStrategies.Redis stores JWT tokens in Redis
     */
    withAuthDevicesStrategy(strategy: AuthJwtDeviceStrategies): BazaApiBundleConfigBuilder {
        this._config.authDevicesStrategy = strategy;

        return this;
    }

    /**
     * Set up / override expirations for JWT Access Tokens
     */
    withAuthAccessTokenExpiresSettings(settings: AccessTokenExpiresSettingsMany): BazaApiBundleConfigBuilder {
        this._config.authAccessTokenExpiresSettings = settings;

        return this;
    }

    /**
     * Set up / override expirations for JWT Refresh Tokens
     */
    withAuthRefreshTokenExpiresSettings(settings: AccessTokenExpiresSettingsMany): BazaApiBundleConfigBuilder {
        this._config.authRefreshTokenExpiresSettings = settings;

        return this;
    }

    /**
     * Set Up Swagger OpenAPI tags
     */
    withTagGroups(tagGroupsFactory: (defaults: Array<TagGroup>) => Array<TagGroup>): BazaApiBundleConfigBuilder {
        this._config.tagGroupsFactory = tagGroupsFactory;

        return this;
    }

    /**
     * Add certan endpoints to blacklist
     * Blacklist endpoints cannot be called once added
     */
    withEndpointsBlacklist(blacklist: Array<any>): BazaApiBundleConfigBuilder {
        this._config.endpointsBlacklist = blacklist;

        return this;
    }

    /**
     * Set Up Sentry DSN and optionally other Sentry options
     */
    withSentry(dsn: string, nodeOptions?: Partial<NodeOptions>): BazaApiBundleConfigBuilder {
        this._config.sentry = {
            dsn,
            nodeOptions,
        };

        return this;
    }

    /**
     * Disables all Baza guards with Version, JWT or any other checks.
     */
    withIgnoreHeadersForEndpoints(endpoints: Array<string>): BazaApiBundleConfigBuilder {
        this._config.ignoreHeadersForEndpoints = endpoints;

        return this;
    }

    /**
     * Disable error reporting to sentry for some specific error codes
     */
    withIgnoreReportErrorsToSentry(additionalErrors: Array<string>): BazaApiBundleConfigBuilder {
        this._config.ignoreReportErrorsCodesToSentry = additionalErrors;

        return this;
    }

    /**
     * Set Up default options for uploaded images
     */
    withImageUploadOptions(
        options: Partial<{
            defaultImageQuality: number;
            defaultImageFormat: AttachmentImageType;
            defaultImageSizes: Array<AttachmentSizeSetItem>;
            defaultJpegBgColor: HexColor;
        }>,
    ): BazaApiBundleConfigBuilder {
        if (options.defaultImageQuality !== undefined) {
            AttachmentConstants.DEFAULT_IMAGE_QUALITY = options.defaultImageQuality;
        }

        if (options.defaultImageFormat !== undefined) {
            AttachmentConstants.DEFAULT_IMAGE_FORMAT = options.defaultImageFormat;
        }

        if (options.defaultImageSizes !== undefined) {
            AttachmentConstants.DEFAULT_IMAGE_SIZES = options.defaultImageSizes;
        }

        if (options.defaultJpegBgColor !== undefined) {
            AttachmentConstants.DEFAULT_JPEG_BG_COLOR = options.defaultJpegBgColor;
        }

        return this;
    }

    /**
     * Set Up Nest.JS Throttle module options
     */
    withThrottleOptions(options: Exclude<ThrottlerModuleOptions, 'limit' | 'ttl'>): BazaApiBundleConfigBuilder {
        this._config.throttlerOptions = options;

        return this;
    }

    /**
     * Set Up additional Swagger UI options
     */
    withSwaggerCustomOptions(options: SwaggerCustomOptions): BazaApiBundleConfigBuilder {
        this._config.swaggerCustomOptions = options;

        return this;
    }

    /**
     * Set Up Swagger OpenAPI builder
     */
    withDocumentationBuilder(callback: (builder: DocumentBuilder) => void): BazaApiBundleConfigBuilder {
        this._config.documentationBuilder = callback;

        return this;
    }

    /**
     * Set Up token generator for Email Verification emails with N-length HEX characters tokenizer
     */
    withRandomHexStringEmailVerificationTokenizer(tokenLength = BazaAccountConstants.TOKEN_DIGITS_ONLY_LENGTH): BazaApiBundleConfigBuilder {
        this._config.customEmailVerificationTokenizer = () => generateRandomHexString(tokenLength);

        return this;
    }

    /**
     * Set Up token generator for Email Verification emails with N-length digits only tokenizer
     */
    withNDigitsEmailVerificationTokenizer(tokenLength = BazaAccountConstants.TOKEN_LENGTH): BazaApiBundleConfigBuilder {
        this._config.customEmailVerificationTokenizer = () => {
            let base = '1';
            let base9 = '9';

            for (let i = 0; i < tokenLength - 1; i++) {
                base += '0';
                base9 += '9';
            }

            return (Math.floor(Math.random() * (parseInt(base9, 10) - parseInt(base, 10) + 1)) + parseInt(base, 10)).toString();
        };

        return this;
    }

    /**
     * Set Up / Override token generator for Email Verification emails
     */
    withCustomEmailVerificationTokenizer(tokenizer: (account: AccountEntity) => string): BazaApiBundleConfigBuilder {
        this._config.customEmailVerificationTokenizer = tokenizer;

        return this;
    }

    /**
     * Set the strategy for how to send confirmation emails
     */
    withConfirmationEmailExpiryResendStrategy(strategy: ConfirmationEmailExpiryResendStrategy): BazaApiBundleConfigBuilder {
        this._config.confirmationEmailExpiryResendStrategy = strategy;

        return this;
    }

    /**
     * Set Up 2FA
     */
    withEnabled2FAForCMS(with2FAMethods: Array<BazaApiConfig2FAOptions>): BazaApiBundleConfigBuilder {
        this._config.with2FAMethods = with2FAMethods;

        return this;
    }

    /**
     * Completely disable 2FA for all users
     */
    disable2FA(): BazaApiBundleConfigBuilder {
        this._config.with2FAMethods = [];

        return this;
    }

    /**
     * Force specific Account Roles to have 2FA enabled
     * By default 2FA via Email is required
     */
    withForced2FAForAccountRoles(accountRoles: Array<AccountRole>): BazaApiBundleConfigBuilder {
        this._config.force2FAForAccountRoles = accountRoles;

        return this;
    }

    /**
     * Sets list of endpoints which should ignore Maintenance Mode
     * @param endpoints
     */
    withIgnoreMaintenanceModeForEndpoints(endpoints: Array<string>): BazaApiBundleConfigBuilder {
        this._config.ignoreMaintenanceModeForEndpoints = endpoints;

        return this;
    }

    /**
     * Sets Account Features configuration
     * @see BazaAccountFeatures
     * @param configuration
     */
    withAccountFeatures(configuration: BazaAccountConfigFeatures): BazaApiBundleConfigBuilder {
        this._config.accountFeatures = configuration;

        return this;
    }

    /**
     * Enables whitelist option for ValidationPipe
     * @returns {this}
     */
    withWhitelist(): BazaApiBundleConfigBuilder {
        this._config.enableWhiteList = true;

        return this;
    }

    /**
     * Disables whitelist option for ValidationPipe
     * @returns {this}
     */
    withoutWhitelist(): BazaApiBundleConfigBuilder {
        this._config.enableWhiteList = false;

        return this;
    }

    /**
     * Enables Mail Template Validation
     * When enabled, API will check that all template variables which are used in send mail request has full documentation
     */
    withMailTemplateValidation(): BazaApiBundleConfigBuilder {
        this._config.enableMailTemplateValidation = true;

        return this;
    }

    /**
     * Disables Mail Template Validation
     * When enabled, API will check that all template variables which are used in send mail request has full documentation
     */
    withoutMailTemplateValidation(): BazaApiBundleConfigBuilder {
        this._config.enableMailTemplateValidation = false;

        return this;
    }

    /**
     * Enables Customer IO Templates
     * Works for Mail Configuration with Customer.IO Mail Transport
     */
    withCustomerIoTemplates(bootstrap: (env: BazaEnvironments) => Array<SendMailCustomerIoBootstrap>): BazaApiBundleConfigBuilder {
        this._config.enableCustomerIoTemplates = true;
        this._config.customerIoTemplatesBootstrap = bootstrap;

        return this;
    }

    /**
     * Disables Customer IO Templates
     * Works for Mail Configuration with Customer.IO Mail Transport
     */
    withoutCustomerIoTemplates(): BazaApiBundleConfigBuilder {
        this._config.enableCustomerIoTemplates = false;

        return this;
    }

    withLoggerMaskedConfig(fields: Array<BazaLoggerMaskConfig>) {
        this._config.loggerMaskConfig = {
            ...fields,
            ...this._config.loggerMaskConfig,
        };

        bazaLoggerMaskField(fields);

        return this;
    }
}

const configBuilder = new BazaApiBundleConfigBuilder();

export function bazaApiBundleConfigBuilder(): BazaApiBundleConfigBuilder {
    return configBuilder;
}
