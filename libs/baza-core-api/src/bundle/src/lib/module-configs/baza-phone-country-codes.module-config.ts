import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaPhoneCountryCodesApiModuleAsyncOptions, BazaPhoneCountryRepositoryStrategy } from '../../../../libs/baza-phone-country-codes/src';

export const BAZA_PHONE_COUNTRY_CODES_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaPhoneCountryCodesApiModuleAsyncOptions = () => ({
    injects: [],
    useFactory: async () => ({
        repository: {
            type: BazaPhoneCountryRepositoryStrategy.Remote,
        },
        ttl: 60 /* minutes */ * 60 /* seconds */ * 1000 /* ms */
    }),
});

