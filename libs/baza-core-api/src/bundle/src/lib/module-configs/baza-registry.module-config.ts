import { defaultsRegistrySchema } from '@scaliolabs/baza-core-shared';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { bazaApiBundleRegistry } from '../resources/baza-api-bundle.registry';
import { BazaRegistryApiModuleAsyncOptions } from '../../../../libs/baza-registry/src';

export const BAZA_REGISTRY_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaRegistryApiModuleAsyncOptions = (config) => ({
    injects: [],
    useFactory: async () => ({
        schema: () => config.registrySchemaFactory({
            ...defaultsRegistrySchema(),
            ...bazaApiBundleRegistry,
        }),
    }),
});

