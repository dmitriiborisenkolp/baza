import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaAuthSessionAsyncOptions } from '../../../../libs/baza-auth-session/src';

export const BAZA_AUTH_SESSION_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaAuthSessionAsyncOptions = (config) => ({
    imports: [],
    inject: [],
    useFactory: async () => ({
        autoCleanUpAfterNDays: config.autoCleanUpAuthSessionLogsAfterNDays,
        cleanUpIntervalHours: 3,
    }),
});
