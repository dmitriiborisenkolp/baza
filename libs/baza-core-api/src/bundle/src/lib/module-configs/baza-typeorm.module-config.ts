import { getMetadataArgsStorage } from 'typeorm';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { EnvService } from '../../../../libs/baza-env/src';
import { BazaCommonEnvironments } from '../../../../libs/baza-common/src';

export const BAZA_TYPEORM_MODULE_CONFIG: (config: BazaApiBundleConfig) => TypeOrmModuleAsyncOptions = () => ({
    inject: [EnvService],
    useFactory: (env: EnvService<BazaCommonEnvironments.TypeOrmEnvironment>) => {
        return {
            type: 'postgres',
            host: env.getAsString('DB_HOST'),
            port: env.getAsNumeric('DB_PORT'),
            username: env.getAsString('DB_USERNAME'),
            password: env.getAsString('DB_PASSWORD'),
            database: env.getAsString('DB_NAME'),
            entities: getMetadataArgsStorage().tables.map(tbl => tbl.target),
            dropSchema: env.getAsBoolean('DB_AUTO_DROP_SCHEMA', {
                defaultValue: false,
            }),
            synchronize: env.getAsBoolean('DB_SYNCHRONIZE', {
                defaultValue: true,
            }),
            logging: env.getAsBoolean('DB_LOGGING', {
                defaultValue: false,
            }) ? 'all' : false,
            bigNumberStrings: false,
            trace: true,
        };
    },
});
