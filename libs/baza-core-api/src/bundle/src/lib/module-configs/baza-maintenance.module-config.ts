import { Application, BazaHttpHeaders } from '@scaliolabs/baza-core-shared';
import { BazaStatusEndpointPaths } from '@scaliolabs/baza-core-shared';
import { VersionEndpointPaths } from '@scaliolabs/baza-core-shared';
import { BazaMaintenanceCmsEndpointPaths } from '@scaliolabs/baza-core-shared';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaMaintenanceApiModuleAsyncOptions } from '../../../../libs/baza-maintenance/src';

export const BAZA_MAINTENANCE_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaMaintenanceApiModuleAsyncOptions = (config) => ({
    useFactory: async () => ({
        scope: [Application.WEB, Application.IOS, Application.ANDROID].filter((app) => config.applications.includes(app)),
        headerIgnoreMaintenanceMode: BazaHttpHeaders.HTTP_HEADER_SKIP_MAINTENANCE,
        ignoreMaintenanceForEndpoints: [
            BazaStatusEndpointPaths.index,
            VersionEndpointPaths.version,
            BazaMaintenanceCmsEndpointPaths.status,
            ...config.ignoreHeadersForEndpoints,
            ...config.ignoreMaintenanceModeForEndpoints,
        ],
    }),
});
