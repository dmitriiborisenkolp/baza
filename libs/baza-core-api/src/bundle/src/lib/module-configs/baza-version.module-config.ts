import * as path from "path";
import * as process from "process";
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { Application, BazaHttpHeaders, BazaMaintenanceCmsEndpointPaths, BazaStatusEndpointPaths, VersionEndpointPaths } from '@scaliolabs/baza-core-shared';
import { BazaVersionModuleAsyncOptions } from '../../../../libs/baza-version/src';

export const BAZA_VERSION_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaVersionModuleAsyncOptions = (config) => ({
    useFactory: async () => {
        return {
            packageJSONPath: path.join(process.cwd(), 'package.json'),
            forceCheckXApiHeaders: (app) => [
                Application.ANDROID,
                Application.IOS,
            ].includes(app),
            httpHeaderVersion: BazaHttpHeaders.HTTP_HEADER_VERSION,
            httpHeaderSupportedVersion: BazaHttpHeaders.HTTP_HEADER_VERSION_SUPPORTED,
            httpHeaderApplication: BazaHttpHeaders.HTTP_HEADER_APP,
            httpHeaderApplicationId: BazaHttpHeaders.HTTP_HEADER_APP_ID,
            httpHeaderVersionSkipValue: BazaHttpHeaders.HTTP_HEADER_VERSION_SKIP,
            ignoreVersionCheckForEndpoints: [
                BazaStatusEndpointPaths.index,
                VersionEndpointPaths.version,
                BazaMaintenanceCmsEndpointPaths.status,
                ...config.ignoreHeadersForEndpoints,
            ],
        };
    },
    injects: [],
});
