import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { Application } from '@scaliolabs/baza-core-shared';
import { EnvService } from '../../../../libs/baza-env/src';
import {
    BAZA_API_CORE_BCRYPT_DEFAULT_SALT_ROUNDS,
    BazaApiCommonModuleAsyncOptions,
    BazaCommonEnvironments,
} from '../../../../libs/baza-common/src';

export const BAZA_API_COMMON_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaApiCommonModuleAsyncOptions = () => ({
    injects: [EnvService],
    useFactory: async (env: EnvService<BazaCommonEnvironments.AppEndpointsEnvironment>) => ({
        bcryptSaltRounds: env.getAsNumeric('BCRYPT_SALT_ROUNDS', {
            defaultValue: BAZA_API_CORE_BCRYPT_DEFAULT_SALT_ROUNDS,
        }),
        endUserAppUrls: [
            {
                app: Application.API,
                url: env.getAsString('BAZA_APP_API_URL'),
            },
            {
                app: Application.CMS,
                url: env.getAsString('BAZA_APP_CMS_URL'),
            },
            {
                app: Application.WEB,
                url: env.getAsString('BAZA_APP_WEB_URL'),
            },
            {
                app: Application.IOS,
                url: env.getAsString('BAZA_APP_WEB_URL'),
            },
            {
                app: Application.ANDROID,
                url: env.getAsString('BAZA_APP_WEB_URL'),
            },
        ],
    }),
});
