import * as path from 'path';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaAccountConstants } from '@scaliolabs/baza-core-shared';
import { Application } from '@scaliolabs/baza-core-shared';
import { EnvService } from '../../../../libs/baza-env/src';
import { ProjectService } from '../../../../libs/baza-common/src';
import { BazaAccountConfig } from '../../../../libs/baza-account/src';
import { BazaAccountModuleAsyncOptions } from '../../../../libs/baza-account/src/lib/baza-account-api.module';

export const BAZA_ACCOUNT_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaAccountModuleAsyncOptions = (config) => ({
    injects: [EnvService, ProjectService],
    useFactory: async (env: EnvService, project: ProjectService) =>
        <BazaAccountConfig>{
            features: config.accountFeatures,
            ttlTokensSeconds: {
                changeEmail: 24 /* hours */ * 60 /* minutes */ * 60 /* seconds */,
                confirmEmail: 24 /* hours */ * 60 /* minutes */ * 60 /* seconds */,
                resetPassword: 24 /* hours */ * 60 /* minutes */ * 60 /* seconds */,
                deactivateAccount: 60 /* minutes */ * 60 /* seconds */,
            },
            sendConfirmEmailLink: {
                mailTemplatePaths: {
                    asHtml: () => path.join('{templatePath}', 'baza-account', '{app}', 'confirm-email', 'confirm-email.html.hbs'),
                    asText: () => path.join('{templatePath}', 'baza-account', '{app}', 'confirm-email', 'confirm-email.text.hbs'),
                },
                urlGenerator: (application, request) => {
                    switch (application) {
                        default: {
                            return project.createLinkToApplication({
                                application: Application.WEB,
                                uri: BazaAccountConstants.mailLinkGenerators.confirmEmailWebLink(request.token),
                            });
                        }

                        case Application.CMS: {
                            return project.createLinkToApplication({
                                application: Application.CMS,
                                uri: BazaAccountConstants.mailLinkGenerators.confirmEmailCmsLink(request.token),
                            });
                        }
                    }
                },
                mailVariablesGenerator: () => ({}),
            },
            resetPasswordRequest: {
                mailTemplatePaths: {
                    asHtml: () => path.join('{templatePath}', 'baza-account', '{app}', 'reset-password', 'reset-password.html.hbs'),
                    asText: () => path.join('{templatePath}', 'baza-account', '{app}', 'reset-password', 'reset-password.text.hbs'),
                },
                urlGenerator: (application, request) => {
                    switch (application) {
                        default: {
                            return project.createLinkToApplication({
                                application: Application.WEB,
                                uri: BazaAccountConstants.mailLinkGenerators.resetPasswordWebLink(request.token),
                            });
                        }

                        case Application.CMS: {
                            return project.createLinkToApplication({
                                application: Application.CMS,
                                uri: BazaAccountConstants.mailLinkGenerators.resetPasswordCmsLink(request.token),
                            });
                        }
                    }
                },
                mailVariablesGenerator: () => ({}),
            },
            cmsRequestResetPassword: {
                mailTemplatePaths: {
                    asHtml: () =>
                        path.join('{templatePath}', 'baza-account', '{app}', 'request-reset-password', 'request-reset-password.html.hbs'),
                    asText: () =>
                        path.join('{templatePath}', 'baza-account', '{app}', 'request-reset-password', 'request-reset-password.text.hbs'),
                },
                urlGenerator: (application, request) => {
                    switch (application) {
                        default: {
                            return project.createLinkToApplication({
                                application: Application.WEB,
                                uri: BazaAccountConstants.mailLinkGenerators.resetPasswordWebLink(request.token),
                            });
                        }

                        case Application.CMS: {
                            return project.createLinkToApplication({
                                application: Application.CMS,
                                uri: BazaAccountConstants.mailLinkGenerators.resetPasswordCmsLink(request.token),
                            });
                        }
                    }
                },
                mailVariablesGenerator: () => ({}),
            },
            passwordChangedNotification: {
                mailTemplatePaths: {
                    asHtml: () => path.join('{templatePath}', 'baza-account', '{app}', 'password-changed', 'password-changed.html.hbs'),
                    asText: () => path.join('{templatePath}', 'baza-account', '{app}', 'password-changed', 'password-changed.text.hbs'),
                },
                mailVariablesGenerator: () => ({}),
            },
            alreadyRegistered: {
                mailTemplatePaths: {
                    asHtml: () => path.join('{templatePath}', 'baza-account', '{app}', 'already-registered', 'already-registered.html.hbs'),
                    asText: () => path.join('{templatePath}', 'baza-account', '{app}', 'already-registered', 'already-registered.text.hbs'),
                },
                mailVariablesGenerator: () => ({}),
            },
            changeEmailRequest: {
                mailTemplatePaths: {
                    asHtml: () => path.join('{templatePath}', 'baza-account', '{app}', 'change-email', 'change-email.html.hbs'),
                    asText: () => path.join('{templatePath}', 'baza-account', '{app}', 'change-email', 'change-email.text.hbs'),
                },
                urlGenerator: (application, request) => {
                    switch (application) {
                        default: {
                            return project.createLinkToApplication({
                                application: Application.WEB,
                                uri: BazaAccountConstants.mailLinkGenerators.changeEmailWebLink(request.token),
                            });
                        }

                        case Application.CMS: {
                            return project.createLinkToApplication({
                                application: Application.CMS,
                                uri: BazaAccountConstants.mailLinkGenerators.changeEmailCmsLink(request.token),
                            });
                        }
                    }
                },
                mailVariablesGenerator: () => ({}),
            },
            changeEmailRequestNotification: {
                mailTemplatePaths: {
                    asHtml: () =>
                        path.join(
                            '{templatePath}',
                            'baza-account',
                            '{app}',
                            'change-email-notification',
                            'change-email-notification.html.hbs',
                        ),
                    asText: () =>
                        path.join(
                            '{templatePath}',
                            'baza-account',
                            '{app}',
                            'change-email-notification',
                            'change-email-notification.text.hbs',
                        ),
                },
                mailVariablesGenerator: () => ({}),
            },
            accountDeactivated: {
                mailTemplatePaths: {
                    asHtml: () =>
                        path.join('{templatePath}', 'baza-account', '{app}', 'account-deactivated', 'account-deactivated.html.hbs'),
                    asText: () =>
                        path.join('{templatePath}', 'baza-account', '{app}', 'account-deactivated', 'account-deactivated.text.hbs'),
                },
                mailVariablesGenerator: () => ({}),
            },
            deactivateAccountLink: {
                mailTemplatePaths: {
                    asHtml: () => path.join('{templatePath}', 'baza-account', '{app}', 'deactivate-account', 'deactivate-account.html.hbs'),
                    asText: () => path.join('{templatePath}', 'baza-account', '{app}', 'deactivate-account', 'deactivate-account.text.hbs'),
                },
                urlGenerator: (application, request) => {
                    return project.createLinkToApplication({
                        application: Application.WEB,
                        uri: BazaAccountConstants.mailLinkGenerators.deactivateAccountLink(request.token),
                    });
                },
                mailVariablesGenerator: () => ({}),
            },
            customEmailVerificationTokenizer: config.customEmailVerificationTokenizer,
            confirmationEmailExpiryResendStrategy: config.confirmationEmailExpiryResendStrategy,
        },
});
