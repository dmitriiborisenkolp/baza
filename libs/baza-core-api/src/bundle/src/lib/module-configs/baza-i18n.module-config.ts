import { bazaCmsBundleEnI18n } from '@scaliolabs/baza-core-shared';
import { bazaApiBundleEnI18n } from '@scaliolabs/baza-core-shared';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { Application, BAZA_COMMON_I18N_CONFIG, ProjectLanguage } from '@scaliolabs/baza-core-shared';
import { BazaI18nApiModuleAsyncOptions } from '../../../../libs/baza-i18n/src';

const defaultsApi: Array<{ language: ProjectLanguage, resources: any }> = [
    { language: ProjectLanguage.EN, resources: bazaApiBundleEnI18n },
];

const defaultsCms: Array<{ language: ProjectLanguage, resources: any }> = [
    { language: ProjectLanguage.EN, resources: bazaCmsBundleEnI18n },
];

export const BAZA_I18N_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaI18nApiModuleAsyncOptions = (config) => {
    const i18nApi = config.i18n.find((i18n) => i18n.app === Application.API);
    const i18nCms = config.i18n.find((i18n) => i18n.app === Application.CMS);
    const i18nWeb = config.i18n.find((i18n) => i18n.app === Application.WEB);

    return {
        injects: [],
        useFactory: async () => ({
            applications: [
                {
                    app: Application.API,
                    defaultLanguage: BAZA_COMMON_I18N_CONFIG.defaultLanguage,
                    translations: BAZA_COMMON_I18N_CONFIG.languages
                        .map((language) => ({
                            language,
                            resources: i18nApi.factory(
                                language, (language) => defaultsApi.find((d) => d.language === language).resources,
                            ),
                        })),
                },
                {
                    app: Application.CMS,
                    defaultLanguage: BAZA_COMMON_I18N_CONFIG.defaultLanguage,
                    translations: BAZA_COMMON_I18N_CONFIG.languages
                        .map((language) => {
                            const defaults = defaultsCms.find((d) => d.language === language).resources;
                            const resources = i18nCms.factory(language, () => defaults);

                            return {
                                language,
                                resources,
                            };
                        })
                    ,
                },
                {
                    app: Application.WEB,
                    defaultLanguage: BAZA_COMMON_I18N_CONFIG.defaultLanguage,
                    translations: BAZA_COMMON_I18N_CONFIG.languages
                        .map((language) => ({
                            language,
                            resources: i18nWeb.factory(language, () => ({})),
                        })),
                },
            ],
        }),
    };
};

