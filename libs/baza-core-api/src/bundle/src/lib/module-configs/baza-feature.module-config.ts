import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaApiFeatureModuleAsyncOptions } from '../../../../libs/baza-feature/src';

export const BAZA_FEATURE_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaApiFeatureModuleAsyncOptions = (config) => ({
    imports: [],
    inject: [],
    useFactory: async () => ({
        enabled: config.features,
    }),
});
