import * as process from 'process';
import * as path from 'path';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { EnvService } from '../../../../libs/baza-env/src';
import {
    BazaMailModuleAsyncOptions,
    CustomerIoRegions,
    defaultApplicationForEmail,
    MailTransportType,
} from '../../../../libs/baza-mail/src';

export const BAZA_MAIL_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaMailModuleAsyncOptions = (config) => ({
    inject: [EnvService],
    useFactory: async (env: EnvService) => {
        const transport: MailTransportType = env.getAsEnum<MailTransportType>('MAIL_TRANSPORT', {
            enum: MailTransportType,
        });

        const allowedDomains = env.getAsString('MAIL_ALLOWED_DOMAINS');

        const useCustomerIoTemplates = !!config.enableCustomerIoTemplates;

        return {
            transport,
            templatePath: path.join(process.cwd(), 'apps/api/i18n/{lang}/mail-templates'),
            templatePartials: useCustomerIoTemplates
                ? []
                : [
                      {
                          name: 'cmsLayoutHtml{lang}',
                          path: path.join('{templatePath}/base-layout-cms-html.partial.hbs'),
                      },
                      {
                          name: 'cmsLayoutText{lang}',
                          path: path.join('{templatePath}/base-layout-cms-text.partial.hbs'),
                      },
                      {
                          name: 'webLayoutHtml{lang}',
                          path: path.join('{templatePath}/base-layout-web-html.partial.hbs'),
                      },
                      {
                          name: 'webLayoutText{lang}',
                          path: path.join('{templatePath}/base-layout-web-text.partial.hbs'),
                      },
                  ],
            sender: {
                name: env.getAsString('MAIL_FROM_NAME'),
                email: env.getAsString('MAIL_FROM_ADDRESS'),
            },
            allowedMailDomains: allowedDomains === '*' ? [] : allowedDomains.split(','),
            smtpConfig:
                transport === 'smtp'
                    ? {
                          host: env.getAsString('MAIL_SMTP_HOST'),
                          port: env.getAsNumeric('MAIL_SMTP_PORT'),
                          username: env.getAsString('MAIL_SMTP_USER'),
                          password: env.getAsString('MAIL_SMTP_PASS'),
                          secure: env.getAsBoolean('MAIL_SMTP_SECURE', {
                              defaultValue: true,
                          }),
                      }
                    : undefined,
            mailgunConfig:
                transport === 'mailgun'
                    ? {
                          domain: env.getAsString('MAIL_MAILGUN_DOMAIN'),
                          apiKey: env.getAsString('MAIL_MAILGUN_API_KEY'),
                      }
                    : undefined,
            sendPulseConfig:
                transport === 'sendpulse'
                    ? {
                          clientId: env.getAsString('MAIL_SEND_PULSE_CLIENT_ID'),
                          clientSecret: env.getAsString('MAIL_SEND_PULSE_CLIENT_SECRET'),
                      }
                    : undefined,
            customerIoConfig:
                transport === 'customerio'
                    ? {
                          appKey: env.getAsString('MAIL_CUSTOMER_IO_APP_KEY'),
                          region: env.getAsString('MAIL_CUSTOMER_IO_REGION', { defaultValue: CustomerIoRegions.US }),
                          useCustomerIoTemplates,
                          customerIoLinksBootstrap: useCustomerIoTemplates ? config.customerIoTemplatesBootstrap(env.current) : [],
                      }
                    : undefined,
            getApplicationForEmail: defaultApplicationForEmail,
            debug: env.getAsBoolean('MAIL_DEBUG'),
            enableTemplateValidation: config.enableMailTemplateValidation,
        };
    },
});
