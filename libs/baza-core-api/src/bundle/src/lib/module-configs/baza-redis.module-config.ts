import { RedisModuleAsyncOptions } from '@liaoliaots/nestjs-redis';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { EnvService } from '../../../../libs/baza-env/src';
import { BazaCommonEnvironments } from '../../../../libs/baza-common/src';

export const BAZA_REDIS_MODULE_CONFIG: (config: BazaApiBundleConfig) => RedisModuleAsyncOptions = () => ({
    inject: [EnvService],
    useFactory: async (env: EnvService<BazaCommonEnvironments.RedisEnvironment>) => ({
        config: {
            host: env.getAsString('REDIS_DB_HOST'),
            port: env.getAsNumeric('REDIS_DB_PORT', {
                defaultValue: 6379,
            }),
            db: env.getAsNumeric('REDIS_DB_NUM', {
                defaultValue: undefined,
            }),
            password: env.getAsString('REDIS_DB_PASSWORD', {
                defaultValue: undefined,
            }),
            keyPrefix: env.getAsString('REDIS_DB_KEY_PREFIX', {
                defaultValue: undefined,
            }),
        },
    }),
});
