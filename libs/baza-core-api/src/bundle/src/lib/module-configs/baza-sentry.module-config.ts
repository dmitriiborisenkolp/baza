import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { BazaEnvModule, EnvService } from '../../../../libs/baza-env/src';
import { BazaCommonEnvironments } from '../../../../libs/baza-common/src';
import { BazaSentryApiModuleAsyncOptions } from '../../../../libs/baza-sentry/src';

export const BAZA_SENTRY_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaSentryApiModuleAsyncOptions = (config) => ({
    imports: [
        BazaEnvModule,
    ],
    injects: [
        EnvService,
    ],
    useFactory: async (env: EnvService) => ({
        dsn: (config.sentry && config.sentry.dsn) || env.getAsString<BazaCommonEnvironments.SentryEnvironment>('SENTRY_DSN', {
            defaultValue: undefined,
        }),
        nodeOptions: {
            environment: env.current,
            ...(config.sentry || {}).nodeOptions,
        },
        ignoreErrors: [
            ...Object.values(AuthErrorCodes),
            ...config.ignoreReportErrorsCodesToSentry,
        ],
    }),
});
