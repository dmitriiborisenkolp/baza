import { BazaEventBusKafkaTopic } from '@scaliolabs/baza-core-shared';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaKafkaApiModuleAsyncOptions } from '../../../../libs/baza-kafka/src';
import { EnvService } from '../../../../libs/baza-env/src';

export const BAZA_KAFKA_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaKafkaApiModuleAsyncOptions = () => ({
    injects: [EnvService],
    useFactory: async (env: EnvService) => {
        const isEnabled = env.getAsBoolean('KAFKA_ENABLED', {
            defaultValue: false,
        });

        if (isEnabled) {
            return {
                enabled: true,
                logEvents: env.getAsBoolean('KAFKA_LOG_EVENTS', {
                    defaultValue: false,
                }),
                kafkaConnectionOptions: {
                    clientId: env.getAsString('KAFKA_CLIENT_ID'),
                    clientGroupId: env.getAsString('KAFKA_CLIENT_GROUP_ID'),
                    brokers: env.getAsString('KAFKA_CLIENT_BROKERS'),
                },
                kafkaTopics: [
                    BazaEventBusKafkaTopic.ApiEvent,
                ],
                kafkaNamespace: env.getAsString('KAFKA_NAMESPACE', {
                    defaultValue: 'dev',
                }),
            };
        } else {
            return {
                enabled: false,
            };
        }
    },
});
