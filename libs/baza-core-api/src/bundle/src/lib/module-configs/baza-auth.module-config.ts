import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaAuthModuleAsyncOptions } from '../../../../libs/baza-auth/src';
import { EnvService } from '../../../../libs/baza-env/src';
import { BazaCommonEnvironments } from '../../../../libs/baza-common/src';

export const BAZA_AUTH_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaAuthModuleAsyncOptions = (config) => ({
    injects: [EnvService],
    useFactory: async(env: EnvService) => ({
        jwtSecret: env.getAsString<BazaCommonEnvironments.AuthEnvironment>('AUTH_JWT_SECRET'),
        accessTokenExpiresSettings: config.authAccessTokenExpiresSettings,
        refreshTokenExpiresSettings: config.authRefreshTokenExpiresSettings,
        skipAuthGuardForEndpoints: [],
        withMasterPasswordFeature: true,
        masterPasswordTTLSeconds: 5 /* min */ * 60 /* seconds */,
        authDeviceStrategy: config.authDevicesStrategy,
        allowed2FAMethods: (role) => {
            const found = config.with2FAMethods.find((def) => def.role === role);

            return found?.methods || [];
        },
        auth2FAMailTemplateTextPath: 'baza-auth/two-factor-auth/two-factor-auth.text.hbs',
        auth2FAMailTemplateHtmlPath: 'baza-auth/two-factor-auth/two-factor-auth.html.hbs',
        auth2FAVerificationTokenTTLSeconds: 5 /* seconds */ * 60 /* seconds */,
        require2FAEmailVerificationForAccountRoles: config.force2FAForAccountRoles,
    }),
});
