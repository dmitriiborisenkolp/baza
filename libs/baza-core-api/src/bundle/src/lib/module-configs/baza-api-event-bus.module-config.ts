import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { EnvService } from '../../../../libs/baza-env/src';
import { BazaCommonEnvironments } from '../../../../libs/baza-common/src';
import { BazaEventBusApiModuleAsyncOptions } from '../../../../libs/baza-event-bus/src';

export const BAZA_API_EVENT_BUS_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaEventBusApiModuleAsyncOptions = () => ({
    injects: [EnvService],
    useFactory: async (env: EnvService<BazaCommonEnvironments.EventBusEnvironment>) => ({
        withKafkaFeature: env.getAsBoolean('BAZA_EVENT_BUS_ENABLE_KAFKA', {
            defaultValue: false,
        }),
        enableDebugLog: env.getAsBoolean('BAZA_EVENT_BUS_LOG_EVENTS', {
            defaultValue: false,
        }),
    }),
});
