import { Application, BAZA_COMMON_I18N_CONFIG, BazaHttpHeaders } from '@scaliolabs/baza-core-shared';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaI18nApiDetectorsModuleAsyncOptions } from '../../../../libs/baza-i18n-detectors/src';

export const BAZA_I18N_DETECTORS_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaI18nApiDetectorsModuleAsyncOptions = () => ({
    injects: [],
    useFactory: async () => ({
        accountDetectorConfig: {
            getLanguage: (app, account) => {
                if (! Array.isArray(account.settings)) {
                    return undefined;
                }

                const found = account.settings.language.find((l) => l.application === app);

                return found
                    ? found.language
                    : undefined;
            },
        },
        headerDetectorConfig: {
            xI18nLanguageHeader: BazaHttpHeaders.HTTP_HEADER_I18N_LANGUAGE,
        },
        defaultProjectLanguage: {
            defaults: Object.values(Application).map((app) => ({
                app,
                defaultLanguage: BAZA_COMMON_I18N_CONFIG.defaultLanguage,
            })),
        },
    }),
});
