import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { BazaAclModuleAsyncOptions } from '../../../../libs/baza-acl/src';
import { defaultBazaAclConfig } from '@scaliolabs/baza-core-shared';

export const BAZA_ACL_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaAclModuleAsyncOptions = () => ({
    inject: [],
    useFactory: async () => defaultBazaAclConfig() as any,
});
