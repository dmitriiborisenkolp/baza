import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { EnvService } from '../../../../libs/baza-env/src';
import { BazaCommonEnvironments } from '../../../../libs/baza-common/src';
import { BazaAwsConfig, BazaAwsModuleAsyncOptions } from '../../../../libs/baza-aws/src';

export const BAZA_AWS_MODULE_CONFIG: (config: BazaApiBundleConfig) => BazaAwsModuleAsyncOptions = () => ({
    injects: [EnvService],
    useFactory: async (env: EnvService) => {
        const proxyBaseUrl = env.getAsString<BazaCommonEnvironments.AwsProxyEnvironment>('AWS_PROXY_BASE_URL', {
            defaultValue: undefined,
        });

        const baseConfig: Pick<BazaAwsConfig, 'defaultTtlSeconds' | 'defaultTtlExpireWindowSeconds' | 'strict'> = {
            defaultTtlSeconds: 24 /* hours */  * 60 /* minutes */ * 60 /* seconds */, /* TODO: Revert! */
            defaultTtlExpireWindowSeconds: 8 /* hours */  * 60 /* minutes */ * 60 /* seconds */, /* TODO: Revert! */
            strict: env.getAsBoolean('AWS_STRICT', {
                defaultValue: true,
            }),
        };

        if (proxyBaseUrl) {
            return {
                ...baseConfig,
                proxy: {
                    baseUrl: env.getAsString<BazaCommonEnvironments.AwsProxyEnvironment>('AWS_PROXY_BASE_URL'),
                    auth: {
                        email: env.getAsString<BazaCommonEnvironments.AwsProxyEnvironment>('AWS_PROXY_AUTH_EMAIL'),
                        password: env.getAsString<BazaCommonEnvironments.AwsProxyEnvironment>('AWS_PROXY_AUTH_PASSWORD'),
                    },
                },
            };
        } else {
            return {
                ...baseConfig,
                direct: {
                    url: env.getAsString<BazaCommonEnvironments.AwsEnvironment>('AWS_S3_URL'),
                    bucket: env.getAsString<BazaCommonEnvironments.AwsEnvironment>('AWS_S3_BUCKET'),
                    accessKeyId: env.getAsString<BazaCommonEnvironments.AwsEnvironment>('AWS_ACCESS_KEY_ID'),
                    secretAccessKey: env.getAsString<BazaCommonEnvironments.AwsEnvironment>('AWS_SECRET_KEY'),
                    folder: env.getAsString<BazaCommonEnvironments.AwsEnvironment>('AWS_S3_PATH', {
                        defaultValue: undefined,
                    }),
                },
            };
        }
    },
});
