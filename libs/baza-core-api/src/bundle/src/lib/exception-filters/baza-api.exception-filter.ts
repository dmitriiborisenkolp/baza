import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { I18nApiService } from '../../../../libs/baza-i18n/src';
import { bazaDefaultApiExceptionHandlers } from '../../../../libs/baza-exception-filters/src';

@Catch()
export class BazaApiExceptionFilter implements ExceptionFilter
{
    constructor(
        private readonly i18n: I18nApiService,
    ) {}

    catch(exception: any, host: ArgumentsHost): void {
        for (const exceptionHandler of bazaDefaultApiExceptionHandlers(this.i18n)) {
            if (exceptionHandler.isMatched(exception)) {
                exceptionHandler.handle(exception, host);

                break;
            }
        }
    }
}
