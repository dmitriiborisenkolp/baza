import { BazaApiBundleConfig } from './baza-api-bundle.config';
import { BAZA_ACL_MODULE_CONFIG } from './module-configs/baza-acl.module-config';
import { BAZA_ACCOUNT_MODULE_CONFIG } from './module-configs/baza-account.module-config';
import { BAZA_API_COMMON_MODULE_CONFIG } from './module-configs/baza-api-common.module-config';
import { BAZA_AUTH_MODULE_CONFIG } from './module-configs/baza-auth.module-config';
import { BAZA_API_EVENT_BUS_MODULE_CONFIG } from './module-configs/baza-api-event-bus.module-config';
import { BAZA_AWS_MODULE_CONFIG } from './module-configs/baza-aws.module-config';
import { BAZA_I18N_DETECTORS_MODULE_CONFIG } from './module-configs/baza-i18n-detectors.module-config';
import { BAZA_I18N_MODULE_CONFIG } from './module-configs/baza-i18n.module-config';
import { BAZA_KAFKA_MODULE_CONFIG } from './module-configs/baza-kafka.module-config';
import { BAZA_MAIL_MODULE_CONFIG } from './module-configs/baza-mail.module-config';
import { BAZA_REDIS_MODULE_CONFIG } from './module-configs/baza-redis.module-config';
import { BAZA_TYPEORM_MODULE_CONFIG } from './module-configs/baza-typeorm.module-config';
import { BAZA_PHONE_COUNTRY_CODES_MODULE_CONFIG } from './module-configs/baza-phone-country-codes.module-config';
import { BAZA_VERSION_MODULE_CONFIG } from './module-configs/baza-version.module-config';
import { BAZA_REGISTRY_MODULE_CONFIG } from './module-configs/baza-registry.module-config';
import { BAZA_MAINTENANCE_MODULE_CONFIG } from './module-configs/baza-maintenance.module-config';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';
import { BAZA_SENTRY_MODULE_CONFIG } from './module-configs/baza-sentry.module-config';
import { BazaI18nApiModuleAsyncOptions } from '../../../libs/baza-i18n/src';
import { BazaAuthModuleAsyncOptions } from '../../../libs/baza-auth/src';
import { BazaRegistryApiModuleAsyncOptions } from '../../../libs/baza-registry/src';
import { BazaPhoneCountryCodesApiModuleAsyncOptions } from '../../../libs/baza-phone-country-codes/src';
import { BazaApiCommonModuleAsyncOptions } from '../../../libs/baza-common/src';
import { BazaMailModuleAsyncOptions } from '../../../libs/baza-mail/src';
import { BazaAwsModuleAsyncOptions } from '../../../libs/baza-aws/src';
import { BazaAclModuleAsyncOptions } from '../../../libs/baza-acl/src';
import { BazaKafkaApiModuleAsyncOptions } from '../../../libs/baza-kafka/src';
import { BazaSentryApiModuleAsyncOptions } from '../../../libs/baza-sentry/src';
import { BazaEventBusApiModuleAsyncOptions } from '../../../libs/baza-event-bus/src';
import { BazaMaintenanceApiModuleAsyncOptions } from '../../../libs/baza-maintenance/src';
import { BazaAccountModuleAsyncOptions } from '../../../libs/baza-account/src/lib/baza-account-api.module';
import { BazaVersionModuleAsyncOptions } from '../../../libs/baza-version/src';
import { BazaI18nApiDetectorsModuleAsyncOptions } from '../../../libs/baza-i18n-detectors/src';
import { BAZA_FEATURE_MODULE_CONFIG } from './module-configs/baza-feature.module-config';
import { BazaApiFeatureModuleAsyncOptions } from '../../../libs/baza-feature/src';
import { BazaAuthSessionAsyncOptions } from '../../../libs/baza-auth-session/src';
import { BAZA_AUTH_SESSION_MODULE_CONFIG } from './module-configs/baza-auth-session.module-config';
import { RedisModuleAsyncOptions } from '@liaoliaots/nestjs-redis';

export interface BazaApiBundleModulesConfig {
    TypeOrmModule: (config: BazaApiBundleConfig) => TypeOrmModuleAsyncOptions;
    RedisModule: (config: BazaApiBundleConfig) => RedisModuleAsyncOptions;
    BazaApiCommonModule: (config: BazaApiBundleConfig) => BazaApiCommonModuleAsyncOptions;
    BazaApiFeatureModule: (config: BazaApiBundleConfig) => BazaApiFeatureModuleAsyncOptions;
    BazaKafkaApiModule: (config: BazaApiBundleConfig) => BazaKafkaApiModuleAsyncOptions;
    BazaVersionModule: (config: BazaApiBundleConfig) => BazaVersionModuleAsyncOptions;
    BazaEventBusApiModule: (config: BazaApiBundleConfig) => BazaEventBusApiModuleAsyncOptions;
    BazaMailModule: (config: BazaApiBundleConfig) => BazaMailModuleAsyncOptions;
    BazaAwsModule: (config: BazaApiBundleConfig) => BazaAwsModuleAsyncOptions;
    BazaI18nApiModule: (config: BazaApiBundleConfig) => BazaI18nApiModuleAsyncOptions;
    BazaI18nApiDetectorsModule: (config: BazaApiBundleConfig) => BazaI18nApiDetectorsModuleAsyncOptions;
    BazaAccountModule: (config: BazaApiBundleConfig) => BazaAccountModuleAsyncOptions;
    BazaAclModule: (config: BazaApiBundleConfig) => BazaAclModuleAsyncOptions;
    BazaAuthModule: (config: BazaApiBundleConfig) => BazaAuthModuleAsyncOptions;
    BazaPhoneCountryCodesApiModule: (config: BazaApiBundleConfig) => BazaPhoneCountryCodesApiModuleAsyncOptions;
    BazaRegistryApiModule: (config: BazaApiBundleConfig) => BazaRegistryApiModuleAsyncOptions;
    BazaMaintenanceApiModule: (config: BazaApiBundleConfig) => BazaMaintenanceApiModuleAsyncOptions;
    BazaSentryApiModule: (config: BazaApiBundleConfig) => BazaSentryApiModuleAsyncOptions;
    BazaAuthSessionModule: (config: BazaApiBundleConfig) => BazaAuthSessionAsyncOptions;
}

export const defaultBazaApiBundleModulesConfig: BazaApiBundleModulesConfig = {
    TypeOrmModule: BAZA_TYPEORM_MODULE_CONFIG,
    RedisModule: BAZA_REDIS_MODULE_CONFIG,
    BazaApiCommonModule: BAZA_API_COMMON_MODULE_CONFIG,
    BazaApiFeatureModule: BAZA_FEATURE_MODULE_CONFIG,
    BazaKafkaApiModule: BAZA_KAFKA_MODULE_CONFIG,
    BazaVersionModule: BAZA_VERSION_MODULE_CONFIG,
    BazaEventBusApiModule: BAZA_API_EVENT_BUS_MODULE_CONFIG,
    BazaMailModule: BAZA_MAIL_MODULE_CONFIG,
    BazaAwsModule: BAZA_AWS_MODULE_CONFIG,
    BazaI18nApiModule: BAZA_I18N_MODULE_CONFIG,
    BazaI18nApiDetectorsModule: BAZA_I18N_DETECTORS_MODULE_CONFIG,
    BazaAccountModule: BAZA_ACCOUNT_MODULE_CONFIG,
    BazaAclModule: BAZA_ACL_MODULE_CONFIG,
    BazaAuthModule: BAZA_AUTH_MODULE_CONFIG,
    BazaPhoneCountryCodesApiModule: BAZA_PHONE_COUNTRY_CODES_MODULE_CONFIG,
    BazaRegistryApiModule: BAZA_REGISTRY_MODULE_CONFIG,
    BazaMaintenanceApiModule: BAZA_MAINTENANCE_MODULE_CONFIG,
    BazaSentryApiModule: BAZA_SENTRY_MODULE_CONFIG,
    BazaAuthSessionModule: BAZA_AUTH_SESSION_MODULE_CONFIG,
};
