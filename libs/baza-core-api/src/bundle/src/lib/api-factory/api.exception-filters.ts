import { BazaApiExceptionFilter } from '../exception-filters/baza-api.exception-filter';

export const APP_API_EXCEPTION_FILTERS = [
    BazaApiExceptionFilter,
];
