import { NestExpressApplication } from '@nestjs/platform-express';
import { EventBus } from '@nestjs/cqrs';
import * as helmet from 'helmet';
import * as compression from 'compression';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { RedocModule } from 'nestjs-redoc';
import { HttpStatus, ValidationPipe } from '@nestjs/common';
import { Request, Response } from 'express';
import {
    BazaCommonErrorCodes,
    CoreOpenApiTags,
    getBazaProjectCodeName,
    getBazaProjectName,
    isEmpty,
    withoutEndingSlash,
    withoutTrailingSlash,
} from '@scaliolabs/baza-core-shared';
import { APP_API_EXCEPTION_FILTERS } from './api.exception-filters';
import { API_ENV_CONFIG_PATH } from './../baza-api-bundle.module';
import { OpenAPIObject } from '@nestjs/swagger/dist/interfaces';
import * as url from 'url';
import { BazaApiBundleConfig } from '../baza-api-bundle.config';
import { EnvService } from '../../../../libs/baza-env/src';
import { BazaCommonEnvironments } from '../../../../libs/baza-common/src';
import { BazaLogger } from '../../../../libs/baza-logger/src';
import { BazaCrudInterceptor } from '../../../../libs/baza-crud/src';
import { VersionService } from '../../../../libs/baza-version/src';
import { Logger } from 'nestjs-pino';

export class ApiBootstrap {
    private app: NestExpressApplication;
    private apiPrefix: string;

    constructor(private readonly config: BazaApiBundleConfig) {}

    async bootstrap(): Promise<NestExpressApplication> {
        this.app = await this.config.apiFactory();

        BazaLogger.eventBus = this.app.get(EventBus);

        await this.setupGzip();
        await this.setupHelmet();
        await this.setupTrustedProxy();

        await this.bootstrapApiPrefix();
        await this.bootstrapApiDocumentation();
        await this.bootstrapDefaultExceptionFilters();
        await this.bootstrapValidationPipe();
        await this.bootstrapEndpointsBlacklist();
        await this.bootstrapGlobalInterceptors();
        await this.enableShutdownHooks();

        await this.app.init();

        await this.startHttpServer();

        return this.app;
    }

    async setupGzip(): Promise<void> {
        this.app.use(compression());
    }

    async setupHelmet(): Promise<void> {
        this.app.use(helmet());
    }

    async setupTrustedProxy(): Promise<void> {
        this.app.set('trust proxy', true);
    }

    async bootstrapApiPrefix(): Promise<void> {
        const dotenvService = this.app.get(EnvService);

        const apiPrefix = withoutTrailingSlash(
            withoutEndingSlash(
                dotenvService.getAsString('BAZA_API_PREFIX', {
                    defaultValue: undefined,
                }) || '',
            ),
        );

        if (!isEmpty(apiPrefix)) {
            const logger = this.app.get(Logger);

            logger.log(`[app.bootstrap.ts] Set API prefix as ${apiPrefix}`);

            this.app.setGlobalPrefix(apiPrefix);

            this.apiPrefix = apiPrefix;
        }
    }

    async bootstrapApiDocumentation(): Promise<void> {
        const version = this.app.get<VersionService>(VersionService);

        const documentBuilder = new DocumentBuilder()
            .setTitle(`${getBazaProjectName()} API`)
            .setDescription('API documentation for developers')
            .setVersion(version.current)
            .addBearerAuth();

        if (this.config.documentationBuilder) {
            this.config.documentationBuilder(documentBuilder);
        }

        const document = SwaggerModule.createDocument(this.app, documentBuilder.build());

        await this.bootstrapSwaggerUI(document);
        await this.bootstrapRedoc(document);
    }

    async bootstrapRedoc(document: OpenAPIObject): Promise<void> {
        // Redoc also publish public JSON endpoint with OpenAPI definition
        // It may be useful for Postman users

        const dotenvService = this.app.get(EnvService);

        if (
            !dotenvService.getAsBoolean<BazaCommonEnvironments.RedocEnvironment>('BAZA_REDOC', {
                defaultValue: false,
            })
        ) {
            return;
        }

        await RedocModule.setup('/redoc', this.app as any, document, {
            title: `${getBazaProjectName()} API`,
            sortPropsAlphabetically: false,
            hideDownloadButton: false,
            hideHostname: false,
            onlyRequiredInSamples: true,
            tagGroups: this.config.tagGroupsFactory([
                {
                    name: 'Baza',
                    tags: Object.values(CoreOpenApiTags),
                },
            ]),
        });
    }

    async bootstrapSwaggerUI(document: OpenAPIObject): Promise<void> {
        const dotenvService = this.app.get(EnvService);

        if (
            !dotenvService.getAsBoolean<BazaCommonEnvironments.SwaggerEnvironment>('BAZA_SWAGGER', {
                defaultValue: false,
            })
        ) {
            return;
        }

        await SwaggerModule.setup('/docs', this.app as any, document, this.config.swaggerCustomOptions);
    }

    async bootstrapDefaultExceptionFilters(): Promise<void> {
        this.app.useGlobalFilters(...APP_API_EXCEPTION_FILTERS.map((exceptionFilter) => this.app.get(exceptionFilter)));
    }

    async bootstrapValidationPipe(): Promise<void> {
        // Do not change it just true/false flag here; Nest.JS has unexpected behaviour here!
        this.config.enableWhiteList
            ? this.app.useGlobalPipes(
                  new ValidationPipe({
                      whitelist: true,
                  }),
              )
            : this.app.useGlobalPipes(new ValidationPipe());
    }

    async bootstrapGlobalInterceptors(): Promise<void> {
        this.app.useGlobalInterceptors(new BazaCrudInterceptor());
    }

    async enableShutdownHooks(): Promise<void> {
        const dotenvService = this.app.get(EnvService);

        if (!dotenvService.isTestEnvironment && !dotenvService.isLocalEnvironment) {
            this.app.enableShutdownHooks();
        }
    }

    async bootstrapEndpointsBlacklist(): Promise<void> {
        // eslint-disable-next-line @typescript-eslint/ban-types
        this.app.use('*', (req: Request, res: Response, next: Function) => {
            const parsed = url.parse(req.originalUrl);

            const endpoint = this.apiPrefix && !isEmpty(parsed.pathname) ? (parsed.pathname || '').slice(this.apiPrefix.length + 1) : url;

            if (this.config.endpointsBlacklist.includes(endpoint)) {
                res.status(HttpStatus.NOT_FOUND).send({
                    code: BazaCommonErrorCodes.BazaEndpointBlacklisted,
                    message: `Endpoint "${req.baseUrl}" is blacklisted`,
                });
            } else {
                next();
            }
        });
    }

    async startHttpServer(): Promise<void> {
        const logger = this.app.get(Logger);
        const versionService = this.app.get(VersionService);
        const dotenvService = this.app.get(EnvService);
        const envConfigFilePath = this.app.get(API_ENV_CONFIG_PATH);

        const httpPort = dotenvService.getAsNumeric<BazaCommonEnvironments.ExpressEnvironment>('BAZA_API_HTTP_PORT', {
            defaultValue: 3000,
        });

        logger.log(
            `[app.bootstrap.ts] Starting ${getBazaProjectName()} [HTTP_PORT: ${httpPort}] [${getBazaProjectCodeName()}:${
                versionService.current
            }] [${envConfigFilePath}]`,
        );

        await this.app.listen(httpPort);
    }
}
