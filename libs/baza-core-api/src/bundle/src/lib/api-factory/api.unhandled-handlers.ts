import * as process from "process";

process.on('unhandledRejection', (err: any) => {
    if (err) {
        console.error(`[main.ts] [unhandledRejection] ${err.toString()}`);
        console.error(err);
    } else {
        console.error(`[main.ts] [unhandledRejection] Unknown unhandled rejection`);
    }
});

process.on('uncaughtException', (err: any) => {
    if (err) {
        console.error(`[main.ts] [uncaughtException] ${err.toString()}`);
        console.error(err);
    } else {
        console.error(`[main.ts] [uncaughtException] uncaught exception`);
    }
});
