import { registerMailDefaultVariables, registerMailTemplates } from '../libs/baza-mail/src';

/**
 * Names (IDs) of Mail Templates for baza-core-api package
 */
export enum BazaCoreMail {
    BazaAccountCmsChangeEmail = 'BazaAccountCmsChangeEmail',
    BazaAccountCmsChangeEmailNotification = 'BazaAccountCmsChangeEmailNotification',
    BazaAccountCmsPasswordChanged = 'BazaAccountCmsPasswordChanged',
    BazaAccountCmsResetPassword = 'BazaAccountCmsResetPassword',
    BazaAccountWebAccountDeactivated = 'BazaAccountWebAccountDeactivated',
    BazaAccountWebAlreadyRegistered = 'BazaAccountWebAlreadyRegistered',
    BazaAccountWebChangeEmail = 'BazaAccountWebChangeEmail',
    BazaAccountWebChangeEmailNotification = 'BazaAccountWebChangeEmailNotification',
    BazaAccountWebConfirmEmail = 'BazaAccountWebConfirmEmail',
    BazaAccountWebDeactivateAccount = 'BazaAccountWebDeactivateAccount',
    BazaAccountWebPasswordChanged = 'BazaAccountWebPasswordChanged',
    BazaAccountWebRequestResetPassword = 'BazaAccountWebRequestResetPassword',
    BazaAccountWebResetPassword = 'BazaAccountWebResetPassword',
    BazaAuthTwoFactorAuth = 'BazaAuthTwoFactorAuth',
    BazaInviteCodeRequest = 'BazaInviteCodeRequest',
    BazaReferralCodeSignedUpWithReferralCode = 'BazaReferralCodeSignedUpWithReferralCode',
}

/**
 * Mail Template Tags for baza-core-api package
 */
export enum BazaCoreMailTags {
    BazaAuthAndAccounts = 'Authorization and Accounts',
    BazaCmsAuthAndAccounts = 'Authorization and Accounts (CMS)',
    BazaReferralCodes = 'Referral Codes',
    BazaInviteCodes = 'Invite Codes',
}
