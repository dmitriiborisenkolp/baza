import { registerMailDefaultVariables, registerMailTemplates } from '../libs/baza-mail/src';
import { BazaCoreMail, BazaCoreMailTags } from './baza-core.mail';

/**
 * Register Default Mail Template Variables
 */
registerMailDefaultVariables([
    {
        field: 'project',
        description: 'Project Name (Full, Human-Readable)',
        optional: false,
    },
    {
        field: 'projectCodeName',
        description: 'Project Code Name (for using as part of variables)',
        optional: false,
    },
    {
        field: 'clientName',
        description: 'Client Name',
        optional: false,
    },
    {
        field: 'apiUrl',
        description: 'API URL',
        optional: false,
    },
    {
        field: 'cmsUrl',
        description: 'CMS URL',
        optional: false,
    },
    {
        field: 'cmsAssetsUrl',
        description: 'CMS Assets URL',
        optional: false,
    },
    {
        field: 'cmsLogoUrl',
        description: 'CMS Logo URL',
        optional: false,
    },
    {
        field: 'webUrl',
        description: 'Web Application URL',
        optional: false,
    },
    {
        field: 'webAssetsUrl',
        description: 'Web Application Assets URL',
        optional: false,
    },
    {
        field: 'webLogoUrl',
        description: 'Web Application Logo URL',
        optional: false,
    },
]);

/**
 * Baza-core-api package registers sets of Mail Templates
 * @see BazaCoreMail
 * @see BazaCoreMailTags
 */
registerMailTemplates([
    {
        name: BazaCoreMail.BazaAccountCmsChangeEmail,
        title: 'Change Email – CMS',
        description: 'Contains link to proceed with Change Email Flow (CMS)',
        tag: BazaCoreMailTags.BazaCmsAuthAndAccounts,
        filePath: 'baza-account/cms/change-email/change-email.{type}.hbs',
        variables: [
            {
                field: 'oldEmail',
                description: 'Email Address (Current)',
                optional: false,
            },
            {
                field: 'newEmail',
                description: 'Email Address (New)',
                optional: false,
            },
            {
                field: 'token',
                description: 'Token which is used to proceed with changing email address of Account',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to page which is used to proceed with changing email address of Account',
                optional: false,
            },
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address (Current)',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountCmsChangeEmailNotification,
        title: 'Change Email Notification – CMS',
        description: 'Notification about successful change of Email Address for Account (CMS)',
        tag: BazaCoreMailTags.BazaCmsAuthAndAccounts,
        filePath: 'baza-account/cms/change-email-notification/change-email-notification.{type}.hbs',
        variables: [
            {
                field: 'oldEmail',
                description: 'Email Address (Current)',
                optional: false,
            },
            {
                field: 'newEmail',
                description: 'Email Address (New)',
                optional: false,
            },
            {
                field: 'token',
                description: 'Token which is used to proceed with changing email address of Account',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to page which is used to proceed with changing email address of Account',
                optional: false,
            },
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address (Current)',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountCmsPasswordChanged,
        title: 'Password Changed Notification – CMS',
        description: 'An notification which will be sent after changing password of Account',
        tag: BazaCoreMailTags.BazaCmsAuthAndAccounts,
        filePath: 'baza-account/cms/password-changed/password-changed.{type}.hbs',
        variables: [
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address (Current)',
                optional: false,
            },
            {
                field: 'emailUrlEncoded',
                description: 'Email Address (urlencoded, for using as query params)',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountCmsResetPassword,
        title: 'Reset Password – Confirmation – CMS',
        description: 'Confirmation email for reset password. Contains token or link to process reset password procedure',
        tag: BazaCoreMailTags.BazaCmsAuthAndAccounts,
        filePath: 'baza-account/cms/reset-password/reset-password.{type}.hbs',
        variables: [
            {
                field: 'token',
                description: 'Token which is used to confirm Reset Password request',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to CMS page which is used to proceed with resetting password',
                optional: false,
            },
            {
                field: 'site',
                description: 'CMS Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebAccountDeactivated,
        title: 'Account Deactivated Notification',
        description: 'Notification Email which is sending after Deactivating Account',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/account-deactivated/account-deactivated.{type}.hbs',
        variables: [
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
            {
                field: 'emailUrlEncoded',
                description: 'Email Address (urlencoded, for using as query params)',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebAlreadyRegistered,
        title: 'Account Already Registered',
        description:
            'An email which is being sent when user is trying to register account which is already registered with same email address',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/already-registered/already-registered.{type}.hbs',
        variables: [
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebChangeEmail,
        title: 'Change Email – Confirm',
        description: 'Confirmation email for changing email address. Contains token and/or link to proceed change email procedure',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/change-email/change-email.{type}.hbs',
        variables: [
            {
                field: 'oldEmail',
                description: 'Email Address (Current)',
                optional: false,
            },
            {
                field: 'newEmail',
                description: 'Email Address (New)',
                optional: false,
            },
            {
                field: 'token',
                description: 'Token which is used to proceed with changing email address of Account',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to page which is used to proceed with changing email address of Account',
                optional: false,
            },
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address (Current)',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebChangeEmailNotification,
        title: 'Change Email – Notification',
        description: 'A notification email which will be sent after finishing change email procedure',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/change-email-notification/change-email-notification.{type}.hbs',
        variables: [
            {
                field: 'oldEmail',
                description: 'Email Address (Current)',
                optional: false,
            },
            {
                field: 'newEmail',
                description: 'Email Address (New)',
                optional: false,
            },
            {
                field: 'token',
                description: 'Token which is used to proceed with changing email address of Account',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to page which is used to proceed with changing email address of Account',
                optional: false,
            },
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address (Current)',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebConfirmEmail,
        title: 'Confirm Email',
        description: 'Confirmation email which contains token and/or link to finish registration',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/confirm-email/confirm-email.{type}.hbs',
        variables: [
            {
                field: 'token',
                description: 'Token which is used to send email confirmation request',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to page which is used to confirm email',
                optional: false,
            },
            {
                field: 'site',
                description: 'Web URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebDeactivateAccount,
        title: 'Deactivate Account – Confirm',
        description: 'Confirmation email which contains token and/or link to finish account deactivation procedure',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/deactivate-account/deactivate-account.{type}.hbs',
        variables: [
            {
                field: 'token',
                description: 'Token which is used to proceed with deactivating account',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to page which is used to proceed with deactivating account',
                optional: false,
            },
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebPasswordChanged,
        title: 'Password Changed – Notification',
        description: 'A notification email which will be sent after changing password',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/password-changed/password-changed.{type}.hbs',
        variables: [
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
            {
                field: 'emailUrlEncoded',
                description: 'Email Address (urlencoded, for using as query params)',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebRequestResetPassword,
        title: 'Reset Password Request – Confirm',
        description: 'Email notification with custom message which will be sent to user after requesting password reset from CMS',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/request-reset-password/request-reset-password.{type}.hbs',
        variables: [
            {
                field: 'subject',
                description: 'Subject (specified in CMS Form when requesting user to reset password)',
                optional: false,
            },
            {
                field: 'message',
                description: 'Message (specified in CMS Form when requesting user to reset password)',
                optional: false,
            },
            {
                field: 'token',
                description: 'Token which is used to proceed with resetting password',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to page which is used to proceed with resetting password',
                optional: false,
            },
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAccountWebResetPassword,
        title: 'Reset Password – Confirm',
        description: 'Confirmation email which contains token and/or link to proceed with reset password procedure',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-account/web/reset-password/reset-password.{type}.hbs',
        variables: [
            {
                field: 'token',
                description: 'Token which is used to confirm Reset Password request',
                optional: false,
            },
            {
                field: 'link',
                description: 'Link to page which is used to proceed with resetting password',
                optional: false,
            },
            {
                field: 'site',
                description: 'Web Application URL',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaAuthTwoFactorAuth,
        title: 'Two-Factor Auth',
        description: 'Contains code which will be used to Sign In',
        tag: BazaCoreMailTags.BazaAuthAndAccounts,
        filePath: 'baza-auth/two-factor-auth/two-factor-auth.{type}.hbs',
        variables: [
            {
                field: 'code',
                description: 'Unique generated code to pass Authentication',
                optional: false,
            },
            {
                field: 'firstName',
                description: 'First Name',
                optional: false,
            },
            {
                field: 'lastName',
                description: 'Last Name',
                optional: false,
            },
            {
                field: 'fullName',
                description: 'Full Name (First Name + Last Name)',
                optional: false,
            },
            {
                field: 'email',
                description: 'Email Address',
                optional: false,
            },
            {
                field: 'validity',
                description: 'TTL',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaInviteCodeRequest,
        title: 'Invite Code Request',
        description: 'A notification email for requesting Invite Code(s) from user. This email will be send to CMS Admin',
        tag: BazaCoreMailTags.BazaInviteCodes,
        filePath: 'baza-invite-code/invite-code-request/invite-code-request.{type}.hbs',
        variables: [
            {
                field: 'email',
                description: 'Email Address (Requested)',
                optional: false,
            },
        ],
    },
    {
        name: BazaCoreMail.BazaReferralCodeSignedUpWithReferralCode,
        title: 'Referral Code – Signed Up With Referral Code Notification',
        description: 'Notification which is sent to owner of Referral Code when someone is using Referral Code to register account',
        tag: BazaCoreMailTags.BazaReferralCodes,
        filePath: 'baza-referral-code/signed-up-with-referral-code/signed-up-with-referral-code.{type}.hbs',
        variables: [
            {
                field: 'referralCodeOwnerFullName',
                description: 'Full Name (Referral Code Owner)',
                optional: false,
            },
            {
                field: 'referralCodeOwnerFirstName',
                description: 'First Name (Referral Code Owner)',
                optional: false,
            },
            {
                field: 'referralCodeOwnerEmail',
                description: 'Last Name (Referral Code Owner)',
                optional: false,
            },
        ],
    },
]);
