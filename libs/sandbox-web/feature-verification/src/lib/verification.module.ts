import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaNcWebVerificationFlowModule } from '@scaliolabs/baza-nc-web-verification-flow';
import { VerificationCustomComponent } from './verification-custom.component';
import { VerificationRoutingCustomModule } from './verification-routing-custom.module';

@NgModule({
    declarations: [VerificationCustomComponent],
    imports: [CommonModule, BazaNcWebVerificationFlowModule, VerificationRoutingCustomModule],
})
export class VerificationModule {}
