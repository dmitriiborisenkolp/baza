import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
    selector: 'app-feature-verification-custom',
    templateUrl: './verification-custom.component.html',
    styleUrls: ['./verification-custom.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerificationCustomComponent {
    public currentTab: number;

    constructor(private readonly route: ActivatedRoute) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });
    }
}
