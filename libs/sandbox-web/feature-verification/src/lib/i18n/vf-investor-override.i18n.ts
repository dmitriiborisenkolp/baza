export const NCVFInvestorEnI18nOverride = {
    title: 'Investor Profile - override',
    form: {
        fields: {
            isAccreditedInvestor: {
                label: 'Are you an accredited investor? - override',
                validators: {
                    required: 'Accreditor investor option is required - override',
                },
                yesOption: {
                    label: 'Yes - override',
                    descr: 'I have an annual income greater than $200k, joint income greater than $300k, or net worth greater than $1M (excluding primary residence) - override',
                },
                noOption: {
                    label: 'No - override',
                    descr: 'I acknowledge that my investment(s) in this offering is less than 10% of my net worth or annual gross income. - override',
                },
            },
            isAssociatedWithFINRA: {
                label: 'Are you or anyone in your household associated with a FINRA member, organization or the SEC? - override',
                validators: {
                    required: 'FINRA association option is required - override',
                },
                yesOption: {
                    label: 'Yes - override',
                    descr: 'I have an annual income greater than $200k, joint income greater than $300k, or net worth greater than $1M (excluding primary residence) - override',
                },
                noOption: {
                    label: 'No - override',
                    descr: 'I acknowledge that my investment(s) in this offering is less than 10% of my net worth or annual gross income. - override',
                },
            },
            currentAnnualHouseholdIncome: {
                subheading: 'What is your current annual household income (including your spouse)? - override',
                label: 'Income ($) - override',
                placeholder: '$0 - override',
                validators: {
                    required: 'Monthly income is required - override',
                },
            },
            netWorth: {
                subheading: 'What is your net worth (excluding primary residence)? - override',
                label: 'Worth ($) - override',
                placeholder: '$0 - override',
                validators: {
                    required: 'Net worth is required - override',
                },
            },
        },
    },
    steps: {
        backBtnLabel: 'Back - override',
        confirmBtnLabel: 'Confirm - override',
    },
};
