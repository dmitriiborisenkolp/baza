export const NCVFInfoEnI18nOverride = {
    title: 'Personal Information - override',
    descr: 'We collect your information to perform Know Your Customer (KYC) and Anti-Money Laundering (AML) compliance. Please provide correct legal information to verify your account. - override',
    subtitle: 'You may only use characters A to Z, 0 to 9, and simple punctuation to fill in information. - override',
    form: {
        fields: {
            firstName: {
                label: 'First Name - override',
            },
            lastName: {
                label: 'Last Name - override',
            },
            phone: {
                label: 'Phone Number - override',
                subtitle:
                    'We need your phone number as a backup means of contact. It will not be shared with any third party services. - override',
                validators: {
                    onlynumbers: 'Only numbers are accepted - override',
                    maxlength: 'Phone Number has to be maximum of {{ length }} digits - override',
                },
            },
            residentialCountry: {
                label: 'Country - override',
                selectPlaceholder: 'Select One - override',
            },
            residentialStreetAddress1: {
                label: 'Street Address - override',
            },
            residentialStreetAddress2: {
                label: 'Street Address 2 - override',
                hint: '(optional) - override',
            },
            residentialCity: {
                label: 'City - override',
            },
            residentialState: {
                label: 'State - override',
                selectPlaceholder: 'Select One - override',
            },
            residentialZipCode: {
                label: 'Zip Code - override',
                validators: {
                    minlength: 'Your ZIP code does not have {{ length }} numeric digits - override',
                    maxlength: 'Your ZIP code does not have {{ length }} numeric digits - override',
                    onlynumbers: 'Your ZIP code does not have {{ length }} numeric digits - override',
                    whiteSpace: 'Your ZIP code does not have {{ length }} numeric digits - override',
                },
            },
            dateOfBirth: {
                label: 'Date of Birth - override',
                validators: {
                    isAdult: 'Investor should be {{ age }} or older - override',
                },
            },
            citizenship: {
                label: 'Citizenship - override',
                selectPlaceholder: 'Select One - override',
            },
            ssn: {
                label: 'Social Security Number - override',
                validators: {
                    minlength: 'Social Security Number has to be a minimum of {{ length }} symbols - override',
                },
                hint: 'We need your SSN for identity verification and to protect against fraud and money laundering. It will be securely stored, never shared, and used only for verification purposes. - override',
            },
            ssnCheckbox: {
                label: `I don't have a SSN - override`,
            },
            fileUpload: {
                descr: 'International investors are required to provide a passport to verify their identity. Please upload an image (png, jpg, jpeg, pdf) of your passport below. - override',
                docLabel: 'Uploaded Document: - override',
                btnText: 'Upload - override',
            },
            submitBtn: {
                label: 'Next - override',
            },
        },
        genericValidators: {
            required: '{{ fieldLabel }} is required - override',
            restrictedChars: 'Special characters are not allowed, only spaces and hyphen (-) are permitted - override',
        },
        sections: {
            address: {
                title: 'Residential Address - override',
                descr: 'Your physical address. PO Boxes and mail drops are not valid. - override',
            },
        },
    },
};
