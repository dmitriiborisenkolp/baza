import { NCVFFileUploadEnI18nOverride } from './vf-file-upload-override.i18n';
import { NCVFInfoEnI18nOverride } from './vf-info-override.i18n';
import { NCVFInvestorEnI18nOverride } from './vf-investor-override.i18n';
import { NCVFEnI18nOverride } from './vf-override.18n';

// override translations can be provided optionally
export const bazaNCWebVFEnI18nOverride = {
    ncvf: {
        vf: NCVFEnI18nOverride,
        info: NCVFInfoEnI18nOverride,
        investor: NCVFInvestorEnI18nOverride,
        fileUpload: NCVFFileUploadEnI18nOverride,
    },
};
