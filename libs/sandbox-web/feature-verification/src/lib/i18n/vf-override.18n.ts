export const NCVFEnI18nOverride = {
    backLink: {
        confirmation: {
            title: 'Are you sure you want to exit? - override',
            text: 'All completed steps will be saved. You can continue verification before your next Purchase. - override',
        },
        linkText: 'Back to Listing Details - override',
    },
    steps: {
        info: 'Personal Information - override',
        profile: 'Investor Profile - override',
    },
    notifications: {
        apply_investor_profile_success: 'Investor Profile successfully saved - override',
        apply_personal_info_success: 'Personal Information successfully saved - override',
        load_verification_fail: 'There was an error loading the verification information. - override',
        upload_doc_success: 'Additional Document successfully uploaded. - override',
        upload_doc_fail: 'There was an error uploading the additional document. - override',
        verify_upload_doc: 'Please upload a passport to verify your identity. - override',
    },
};
