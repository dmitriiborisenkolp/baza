export const NCVFFileUploadEnI18nOverride = {
    title: 'Additional Information Required - override',
    descr: 'Please upload an image (png, jpg, jpeg, pdf) of a government issued ID (Drivers License, Passport, ID Card) to complete the verification. - override',
    actions: {
        btnUpload: {
            label: 'Upload - override',
            hint: 'New - override',
        },
        btnConfirm: {
            label: 'Confirm - override',
        },
    },
};
