import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { getConfigurableRoutes } from '@scaliolabs/baza-nc-web-verification-flow';
import { VerificationCustomComponent } from './verification-custom.component';

const routes: Routes = getConfigurableRoutes(VerificationCustomComponent);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class VerificationRoutingCustomModule {}
