import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { Message, catchErrorMessage, getErrorMessage } from '@scaliolabs/baza-web-utils';
import {
    ClearUser,
    Login,
    LoginData,
    LoginForm,
    RequestAppBootstrapInitData,
    ResendConfirmation,
} from '@scaliolabs/sandbox-web/data-access';
import { Observable, of } from 'rxjs';
import { catchError, mapTo, take, tap } from 'rxjs/operators';

@UntilDestroy()
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
    public isAccountActive = true;
    public loginForm: LoginForm;
    public message$: Observable<Message | undefined>;
    public redirectTo: string;

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly store: Store,
        private readonly route: ActivatedRoute,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
    ) {
        this.onFormReset();
        this.store.dispatch(new ClearUser());

        this.route.queryParams.pipe().subscribe((url) => {
            this.redirectTo = url.redirectUrl;
        });
    }

    // onActivationClick
    public onActivationClick(): void {
        this.message$ = this.store.dispatch(new ResendConfirmation({ email: this.loginForm.value.email })).pipe(
            take(1),
            mapTo(undefined),
            tap(() => this.onModalClose()),
            catchErrorMessage,
        );
    }

    // onFormReset
    public onFormReset(): void {
        this.loginForm = this.fb.group({
            email: ['', Validators.compose([Validators.required, Validators.email])],
            password: ['', [Validators.required]],
        }) as LoginForm;
    }

    // onFormSubmit
    public onFormSubmit(formEl, modalEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.loginForm, formEl, modalEl)) {
            const data: LoginData = { ...this.loginForm.value };

            this.message$ = this.store.dispatch(new Login(data)).pipe(
                take(1),
                mapTo(undefined),
                tap(() => {
                    this.store.dispatch(new RequestAppBootstrapInitData());
                    this.onModalClose();

                    if (this.redirectTo && this.redirectTo.length) {
                        this.router.navigateByUrl(this.redirectTo);
                    }
                }),
                catchError((error) => {
                    if (error && error.code === 'AuthEmailIsNotVerified') {
                        this.isAccountActive = false;
                        return of(undefined);
                    } else {
                        return of(getErrorMessage(error));
                    }
                }),
            );
        }
    }

    // onModalClose
    public onModalClose() {
        this.router.navigate(['', { outlets: { modal: null } }]);
    }
}
