import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RestorePasswordComponent } from './restore-password/restore-password.component';
import { SignUpComponent } from './signup/signup.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/(modal:auth/login)',
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'restore-password',
        component: RestorePasswordComponent,
    },
    {
        path: 'signup',
        component: SignUpComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AuthRoutingModule {}
