import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { mapTo, tap } from 'rxjs/operators';
import { UntilDestroy } from '@ngneat/until-destroy';
import { RegisterAccount, SignUpForm } from '@scaliolabs/sandbox-web/data-access';
import {
    catchErrorMessage,
    lowercaseValidator,
    Message,
    mismatchValidator,
    numberValidator,
    specialSymbolValidator,
    uppercaseValidator,
} from '@scaliolabs/baza-web-utils';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';

enum ModalStates {
    Form,
    Success,
}

@UntilDestroy()
@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpComponent {
    public message$: Observable<Message | undefined>;
    public modalStates = ModalStates;
    public modal$ = new BehaviorSubject<ModalStates>(ModalStates.Form);
    public signUpForm: SignUpForm;

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
    ) {
        this.onFormReset();
    }

    // onFormReset
    public onFormReset(): void {
        this.signUpForm = this.fb.group(
            {
                firstName: ['', [Validators.required]],
                lastName: ['', [Validators.required]],
                email: ['', Validators.compose([Validators.required, Validators.email])],
                password: [
                    '',
                    Validators.compose([
                        Validators.required,
                        Validators.minLength(8),
                        lowercaseValidator(),
                        numberValidator(),
                        specialSymbolValidator(),
                        uppercaseValidator(),
                    ]),
                ],
                confirmPassword: ['', Validators.compose([Validators.required])],
            },
            {
                validators: mismatchValidator('password', 'confirmPassword'),
            },
        ) as SignUpForm;
    }

    // onFormSubmit
    public onFormSubmit(formEl, modalEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.signUpForm, formEl, modalEl)) {
            this.message$ = this.store.dispatch(new RegisterAccount(this.signUpForm.value)).pipe(
                tap(() => {
                    this.modal$.next(ModalStates.Success);
                }),
                mapTo(''),
                catchErrorMessage,
            );
        }
    }

    // onModalClose
    public onModalClose() {
        this.router.navigate([{ outlets: { modal: null } }]);
    }
}
