import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { mapTo, tap } from 'rxjs/operators';
import { RestorePasswordForm, SendResetPasswordLink } from '@scaliolabs/sandbox-web/data-access';
import { catchErrorMessage, Message } from '@scaliolabs/baza-web-utils';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';

enum ModalStates {
    Form,
    Success,
}

@Component({
    selector: 'app-restore-password',
    templateUrl: './restore-password.component.html',
    styleUrls: ['./restore-password.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RestorePasswordComponent {
    public message$: Observable<Message | undefined>;
    public modal$ = new BehaviorSubject<ModalStates>(ModalStates.Form);
    public modalStates = ModalStates;
    public restorePasswordForm: RestorePasswordForm;

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
    ) {
        this.onFormReset();
    }

    // onFormReset
    public onFormReset(): void {
        this.restorePasswordForm = this.fb.group({
            email: ['', Validators.compose([Validators.required, Validators.email])],
        }) as RestorePasswordForm;
    }

    // onFormSubmit
    public onFormSubmit(formEl, modalEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.restorePasswordForm, formEl, modalEl)) {
            this.message$ = this.store.dispatch(new SendResetPasswordLink(this.restorePasswordForm.value)).pipe(
                tap(() => this.modal$.next(ModalStates.Success)),
                mapTo(''),
                catchErrorMessage,
            );
        }
    }

    // onModalClose
    public onModalClose() {
        this.router.navigate([{ outlets: { modal: null } }]);
    }
}
