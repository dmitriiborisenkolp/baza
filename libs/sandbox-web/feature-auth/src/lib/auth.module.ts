import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { AuthStateModule } from '@scaliolabs/sandbox-web/data-access';
import { PasswordValidatorModule } from '@scaliolabs/sandbox-web/ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { RestorePasswordComponent } from './restore-password/restore-password.component';
import { SignUpComponent } from './signup/signup.component';

@NgModule({
    declarations: [LoginComponent, SignUpComponent, RestorePasswordComponent],
    imports: [
        AuthRoutingModule,
        AuthStateModule,
        CommonModule,
        NzAlertModule,
        NzButtonModule,
        NzFormModule,
        NzInputModule,
        NzModalModule,
        PasswordValidatorModule,
        ReactiveFormsModule,
        UtilModule,
    ],
})
export class AuthModule {}
