import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { NotFoundComponent } from './not-found.component';

const routes: Routes = [
    {
        path: '',
        component: NotFoundComponent,
        data: {
            meta: {
                title: 'Page Not Found',
            },
        },
    },
];

@NgModule({
    declarations: [NotFoundComponent],
    imports: [CommonModule, RouterModule.forChild(routes), NzEmptyModule, NzButtonModule, UtilModule],
})
export class NotFoundModule {}
