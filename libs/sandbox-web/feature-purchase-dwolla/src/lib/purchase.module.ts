import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaDwollaPurchaseFlowModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { DwollaPurchaseCustomComponent } from './purchase-custom.component';
import { DwollaPurchaseRoutingCustomModule } from './purchase-routing-custom.module';

@NgModule({
    declarations: [DwollaPurchaseCustomComponent],
    imports: [CommonModule, BazaDwollaPurchaseFlowModule, DwollaPurchaseRoutingCustomModule],
    exports: [],
    providers: [],
})
export class DwollaPurchaseModule {}
