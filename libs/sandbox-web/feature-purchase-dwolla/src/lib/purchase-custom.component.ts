import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { PurchaseConfig } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { DwollaVerificationState } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { BootstrapState, RequestAppBootstrapInitData } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-purchase-custom-dwolla',
    templateUrl: './purchase-custom.component.html',
    styleUrls: ['./purchase-custom.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaPurchaseCustomComponent {
    initData$ = this.store.select(BootstrapState.initData);
    personal$ = this.store.select(DwollaVerificationState.personal);

    public currentTab: number;
    purchaseConfig: PurchaseConfig;

    constructor(private route: ActivatedRoute, private store: Store, private readonly wts: BazaWebUtilSharedService) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });

        this.purchaseConfig = {
            linksConfig: {
                buyShares: '/buy-shares-dwolla',
                verification: '/verification-dwolla',
            },
        };

        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }
}
