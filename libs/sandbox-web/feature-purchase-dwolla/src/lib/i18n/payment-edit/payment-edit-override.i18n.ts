import { DwollaPaymentEditAddEnI18nOverride } from './add-override.i18n';
import { DwollaPaymentEditListEnI18nOverride } from './list-override.i18n';

export const DwollaPaymentEditEnI18nOverride = {
    add: DwollaPaymentEditAddEnI18nOverride,
    list: DwollaPaymentEditListEnI18nOverride,
};
