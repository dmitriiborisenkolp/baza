export const DwollaPaymentEditListEnI18nOverride = {
    title: {
        singular: 'Payment Method - Override',
        plural: 'Payment Methods - Override',
    },
    balance: {
        title: 'Account Balance - Override',
        prefix: 'Current balance: - Override',
        hint: 'Fund your account - Override',
        errors: {
            insufficientFunds:
                'There are insufficient funds in your account to complete your purchase. Please choose another payment method below. - Override',
        },
    },
    bank: {
        title: 'Bank Account - Override',
        actions: {
            update: 'Update - Override',
        },
    },
    card: {
        title: 'Card - Override',
        feeSuffix: 'Card processing fee - Override',
        limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account - Override',
        actions: {
            update: 'Update - Override',
        },
    },
};
