export const DwollaMethodsEnI18nOverride = {
    title: 'Payment Method - Override',
    errors: {
        accountBalance: {
            notification: `There was a failed attempt to create an Account Balance. Please change your personal information in account, <a data-link="verificationLink"></a> or <a data-link="supportLink"></a> for technical support. Alternatively, you can proceed with purchases with a Card. - Override`,
            verificationLinkConfig: {
                key: 'verificationLink',
                text: 'investors profile - Override',
            },
            supportLinkConfig: {
                key: 'supportLink',
                text: 'contact us - Override',
            },
        },
        insufficientFunds:
            'There are insufficient funds in your account to complete your purchase. Please choose another payment method below. - Override',
        lowFunds: `This purchase is <strong>{{ amount }}</strong> higher than your current Account Balance. Please add funds to your balance. - Override`,
    },
    actions: {
        editMethod: 'Edit your payment method - Override',
        submit: 'Submit Payment - Override',
        back: 'Back - Override',
    },
};
