export const DwollaPaymentEnI18nOverride = {
    title: 'Submit Payment - Override',
    checklist: {
        accountVerified: 'Account verification complete - Override',
        documentSigned: 'Subscription document signed - Override',
        paymentMethodAdded: 'Payment method added - Override',
    },
    authorization: {
        label: `By clicking ‘Submit Payment’ you authorize this transaction and agree to our <a data-link="termsOfService"></a> and <a data-link="eftDisclosure"></a> - Override`,
        tosLinkConfig: {
            key: 'termsOfService',
            text: 'Terms of Service - Override',
        },
        eftLinkConfig: {
            key: 'eftDisclosure',
            text: 'EFT Disclosure - Override',
        },
    },
    actions: {
        submit: 'Submit Payment - Override',
        back: 'Back - Override',
    },
};
