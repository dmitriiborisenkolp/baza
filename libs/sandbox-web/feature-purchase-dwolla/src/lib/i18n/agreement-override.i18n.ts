export const DwollaAgreementEnI18nOverride = {
    title: 'Sign Agreement - Override',
    messages: {
        disclaimer:
            'We need to collect your signature for our purchase agreement to proceed with your purchase. You will be able to review your purchase details before submitting. - Override',
        pdfConsent: 'Please, read and sign PDF Doc - Override',
        success: 'You have successfully signed the Subscription Agreement! - Override',
    },
    actions: {
        sign: 'Sign Agreement - Override',
        back: 'Back - Override',
        next: 'Next - Override',
    },
};
