export const DwollaDetailsEnI18nOverride = {
    title: 'Purchase Details - Override',
    description: 'Pick number of shares you would like to purchase and review your personal details. - Override',
    subtitle: 'Listing Details - Override',
    shares: {
        min: 'Minimum {{ min }}. - Override',
        max: 'You can purchase up to {{ max }} - Override',
    },
    actions: {
        next: 'Next - Override',
    },
};
