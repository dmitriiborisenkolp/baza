export const DwollaDoneEnI18nOverride = {
    title: 'Your order has been submitted! - Override',
    descr: 'We are verifying the order and will notify you when it has been completed. This typically takes 24-48 hours. - Override',
    support: {
        descr: 'If you have any questions please reach out via - Override',
        linkText: 'Contact Us - Override',
    },
    actions: {
        proceedToPortfolio: 'Proceed to my portfolio - Override',
    },
};
