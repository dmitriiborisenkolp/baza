import { DwollaAgreementEnI18nOverride } from './agreement-override.i18n';
import { DwollaDetailsEnI18nOverride } from './details-override.i18n';
import { DwollaDoneEnI18nOverride } from './done-override.i18n';
import { DwollaMethodsEnI18nOverride } from './methods-override.i18n';
import { DwollaPaymentEditEnI18nOverride } from './payment-edit';
import { DwollaPaymentEnI18nOverride } from './payment-override.i18n';

export const BazaDwollaWebPFEnI18nOverride = {
    dwpf: {
        parent: {
            backlink: {
                text: 'Back to Listing Details - Override',
                confirmation: {
                    title: 'Are you sure you want to exit? - Override',
                    text: 'Your purchase will not be completed if you leave this page. - Override',
                },
            },
            steps: {
                detailsLabel: 'Purchase Details - Override',
                agreementLabel: 'Sign Agreement - Override',
                paymentLabel: 'Submit Payment - Override',
            },
            warnings: {
                pickShares: 'Please pick number of shares - Override',
            },
        },
        agreement: DwollaAgreementEnI18nOverride,
        details: DwollaDetailsEnI18nOverride,
        done: DwollaDoneEnI18nOverride,
        methods: DwollaMethodsEnI18nOverride,
        payment: DwollaPaymentEnI18nOverride,
        pymtEdit: DwollaPaymentEditEnI18nOverride,
        notifications: {
            cancel_purchase_fail: 'There was an error cancelling the Purchase. - Override',
            load_bank_account_fail: 'Failed to load Bank Account information - Override',
            load_account_balance_fail: 'Failed to load Account Balance information - Override',
            load_credit_card_fail: 'Failed to load Credit Card information - Override',
            load_plaid_link_fail: 'Failed to load Plaid link - Override',
            save_bank_account_fail: 'Failed to save Bank Account information - Override',
            save_credit_card_fail: 'Failed to save Credit Card information - Override',
            submit_purchase_fail: 'There was an error submitting the Purchase. - Override',
            submit_purchase_success: 'Purchase successfully submitted - Override',
            purchase_start_fail: 'Could not start the session - Override',
            purchase_reprocess_success: 'Payment successfully re-submitted - Override',
            purchase_reprocess_fail: 'Payment retry request failed. Please contact your bank for further details - Override',
            plaid_on_link_success: `The bank account you're using to add funds to your account balance will be utilized to receive dividends. You can change it later on Account page. - Override`,
            plaid_on_link_fail: 'There was an error linking bank account - Override',
        },
    },
};
