import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { getConfigurableRoutes, DwollaPurchaseDoneComponent } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { DwollaPurchaseCustomComponent } from './purchase-custom.component';

const routes: Routes = getConfigurableRoutes(DwollaPurchaseCustomComponent, DwollaPurchaseDoneComponent);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class DwollaPurchaseRoutingCustomModule {}
