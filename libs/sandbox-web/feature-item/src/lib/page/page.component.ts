import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MetaService } from '@ngx-meta/core';
import { BazaNcIntegrationSchemaDefinition } from '@scaliolabs/baza-nc-integration-shared';
import { AttachmentImageSetDto } from '@scaliolabs/baza-core-shared';
import { AccountState, GetItem, ItemDetail, ItemState, SchemaDto } from '@scaliolabs/sandbox-web/data-access';
import { LinkConfig } from '@scaliolabs/baza-web-utils';

@UntilDestroy()
@Component({
    selector: 'app-item-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemPageComponent {
    item$ = this.store.select(ItemState.current);
    user$ = this.store.select(AccountState.account);

    schema: SchemaDto;
    details: ItemDetail[] = [];
    backLinkConfig: LinkConfig = {
        appLink: {
            commands: ['/items'],
        },
    };

    constructor(
        private readonly meta: MetaService,
        private readonly route: ActivatedRoute,
        private readonly router: Router,
        private readonly store: Store,
    ) {
        this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
            const id = params.get('id');

            this.store.dispatch(new GetItem(Number(id))).subscribe(
                () => ({}),
                (err) => err.statusCode === 404 && this.gotoListing(),
            );
        });

        this.item$.pipe(untilDestroyed(this)).subscribe((response) => {
            if (!response) {
                return;
            } else {
                this.meta.setTitle(`Item ${response.id}`);
            }
            const { schema, properties } = response;
            this.schema = schema;

            if (schema) {
                this.setDetails(schema?.definitions || [], properties);
            }
        });
    }

    getImageSources(images: AttachmentImageSetDto[]): string[] {
        return images.map((image) => image.md);
    }

    formatNoText = () => ``;

    private setDetails(definitions: Array<BazaNcIntegrationSchemaDefinition> = [], properties: Record<string, string> = {}): void {
        const definitionPayloads = ((definitions || []) as Array<BazaNcIntegrationSchemaDefinition>)
            .map((definition) => definition.payload)
            .filter((payload) => payload.isDisplayedInDetails);

        this.details = definitionPayloads.reduce((details, { field, title }) => {
            const property = properties[`${field}`];

            if (property) {
                details.push({ title, value: property });
            }

            return details;
        }, []);
    }

    /**
     * Utility method to take user on listing page.
     */
    gotoListing(): void {
        this.router.navigate(['..'], {
            relativeTo: this.route,
        });
    }
}
