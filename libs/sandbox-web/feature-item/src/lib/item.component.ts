import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { UntilDestroy } from '@ngneat/until-destroy';
import { ChangeItemsPage, ClearItems, GetItems, ItemState } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemComponent implements OnInit {
    paging$ = this.store.select(ItemState.paging);
    items$ = this.store.select(ItemState.items);

    constructor(private route: ActivatedRoute, private router: Router, private store: Store) {}

    ngOnInit(): void {
        this.store.dispatch(new ClearItems());
        this.store.dispatch(new GetItems());
    }

    updateRoute(paramsUpdate: { [key: string]: unknown }) {
        this.router.navigate([], {
            queryParams: paramsUpdate,
            queryParamsHandling: 'merge',
            relativeTo: this.route,
            state: {
                // local: ask component to scroll to filters view
                propertiesScrollToView: true,
                // global: forbit router to scroll to top
                skipScroll: true,
            },
        });
    }

    onPageSelect(index: number) {
        window.scroll(0, 0);
        this.store.dispatch(new ChangeItemsPage(index));
    }
}
