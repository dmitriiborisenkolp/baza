import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { ClearPurchaseState, SelectEntity } from '@scaliolabs/baza-nc-web-purchase-flow';
import { VerificationState } from '@scaliolabs/baza-nc-web-verification-flow';
import { LinkConfig } from '@scaliolabs/baza-web-utils';

@UntilDestroy()
@Component({
    selector: 'app-property-check-account',
    templateUrl: './check-account.component.html',
    styleUrls: ['./check-account.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemCheckAccountComponent {
    verification$ = this.store.select(VerificationState.verification);

    public buttonLink: string[];
    public hasStarted: boolean;
    public propertyId: string;
    public showContainer: boolean;

    backLinkConfig?: LinkConfig;

    constructor(
        private readonly route: ActivatedRoute,
        private readonly router: Router,
        private readonly store: Store,
        private readonly cdr: ChangeDetectorRef,
    ) {
        this.showContainer = false;
        this.store.dispatch(new ClearPurchaseState());

        this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
            this.propertyId = params.get('id');

            this.backLinkConfig = {
                appLink: {
                    commands: ['/items', this.propertyId],
                },
            };

            this.store.dispatch(new SelectEntity(Number(this.propertyId)));
        });

        this.verification$.pipe(untilDestroyed(this)).subscribe((response) => {
            if (response && response !== null) {
                if (response.currentStep !== 'Completed') {
                    this.showContainer = true;
                }
                this.cdr.markForCheck();
            } else {
                this.showContainer = false;
            }

            if (response && response.currentStep) {
                switch (response.currentStep) {
                    case AccountVerificationStep.Completed:
                        this.router.navigate(['/buy-shares']);
                        break;

                    case AccountVerificationStep.PhoneNumber:
                        this.hasStarted = false;
                        this.buttonLink = ['/verification', 'info'];
                        break;

                    case AccountVerificationStep.Citizenship:
                    case AccountVerificationStep.DateOfBirth:
                    case AccountVerificationStep.LegalName:
                    case AccountVerificationStep.RequestSSNDocument:
                    case AccountVerificationStep.ResidentialAddress:
                    case AccountVerificationStep.SSN:
                        this.hasStarted = true;
                        this.buttonLink = ['/verification', 'info'];
                        break;

                    case AccountVerificationStep.RequestDocuments:
                    case AccountVerificationStep.InvestorProfile:
                        this.hasStarted = true;
                        this.buttonLink = ['/verification', 'investor'];
                        break;

                    default:
                        this.hasStarted = false;
                        this.buttonLink = ['/verification', 'info'];
                }
            }
        });
    }
}
