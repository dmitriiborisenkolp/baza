import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JwtRequireAuthGuard, JwtVerifyGuard } from '@scaliolabs/baza-core-ng';
import { VerificationResolver } from '@scaliolabs/baza-nc-web-verification-flow';
import { ItemComponent } from './item.component';
import { ItemCheckAccountComponent } from './check-account/check-account.component';
import { ItemPageComponent } from './page/page.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: ItemComponent,
                data: {
                    wrapper: true,
                },
            },
            {
                path: ':id',
                component: ItemPageComponent,
                data: {
                    wrapper: true,
                },
            },
            {
                path: ':id/buy',
                canActivate: [JwtRequireAuthGuard, JwtVerifyGuard],
                component: ItemCheckAccountComponent,
                data: {
                    layout: 'mini-center',
                    wrapper: true,
                },
                resolve: {
                    verification: VerificationResolver,
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ItemRoutingModule {}
