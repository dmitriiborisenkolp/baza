import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { Account, ItemDto } from '@scaliolabs/sandbox-web/data-access';

@Component({
    selector: 'app-item-page-stats',
    templateUrl: './page-stats.component.html',
    styleUrls: ['./page-stats.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemPageStatsComponent {
    @Input()
    item?: ItemDto;

    @Input()
    user?: Account;

    public OFFERING_STATUS = BazaNcOfferingStatus;

    public shareMapping: { [k: string]: string } = {
        '=0': 'No minimum',
        '=1': `# share minimum <span class='item-card__investment'>&nbsp;investment</span>`,
        other: `# shares minimum <span class='item-card__investment'>&nbsp;investment</span>`,
    };

    formatNoText = () => ``;
}
