import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { VerificationStateModule } from '@scaliolabs/baza-nc-web-verification-flow';
import { DwollaVerificationStateModule } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { BackLinkModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { ItemStateModule } from '@scaliolabs/sandbox-web/data-access';
import { ItemCardModule, FavoriteButtonModule, ImageCarouselModule } from '@scaliolabs/sandbox-web/ui-components';
import { ItemComponent } from './item.component';
import { ItemCheckAccountComponent } from './check-account/check-account.component';
import { ItemPageBuyComponent } from './page-buy/page-buy.component';
import { ItemPageComponent } from './page/page.component';
import { ItemPageStatsComponent } from './page-stats/page-stats.component';
import { ItemRoutingModule } from './item-routing.module';

@NgModule({
    declarations: [ItemComponent, ItemCheckAccountComponent, ItemPageBuyComponent, ItemPageComponent, ItemPageStatsComponent],
    imports: [
        BackLinkModule,
        CommonModule,
        FavoriteButtonModule,
        FormsModule,
        ImageCarouselModule,
        ItemCardModule,
        ItemRoutingModule,
        ItemStateModule,
        NzAlertModule,
        NzButtonModule,
        NzCardModule,
        NzCarouselModule,
        NzCheckboxModule,
        NzDividerModule,
        NzDropDownModule,
        NzEmptyModule,
        NzGridModule,
        NzInputModule,
        NzInputNumberModule,
        NzLayoutModule,
        NzPaginationModule,
        NzPopoverModule,
        NzProgressModule,
        NzSelectModule,
        NzSliderModule,
        NzSpinModule,
        NzTableModule,
        NzTabsModule,
        NzToolTipModule,
        ReactiveFormsModule,
        UtilModule,
        VerificationStateModule,
        DwollaVerificationStateModule,
    ],
})
export class ItemModule {}
