import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { DwollaClearPurchaseState, DwollaUpdateCart } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { BazaNcIntegrationSubscribeStatus } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { Account, AddNotification, ItemDto, RemoveNotification } from '@scaliolabs/sandbox-web/data-access';

@Component({
    selector: 'app-item-page-buy',
    templateUrl: './page-buy.component.html',
    styleUrls: ['./page-buy.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemPageBuyComponent {
    @Input()
    item?: ItemDto;

    @Input()
    user?: Account;

    public OFFERING_STATUS = BazaNcOfferingStatus;
    public NOTIFICATION_STATUS = BazaNcIntegrationSubscribeStatus;

    public propertyId: string;

    constructor(private readonly router: Router, private readonly store: Store, private readonly route: ActivatedRoute) {
        this.route.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
            this.propertyId = params.get('id');
        });
    }

    onBuyClickNcPf() {
        if (!this.user) {
            this.router.navigate(['', { outlets: { modal: ['auth', 'login'] } }], {
                queryParams: {
                    redirectUrl: `/items/${this.item?.id}/buy`,
                },
            });
        } else {
            this.router.navigate(['/items', this.item?.id, 'buy']);
        }
    }

    onBuyClickDwPf() {
        this.store.dispatch(new DwollaClearPurchaseState());
        this.store.dispatch(new DwollaUpdateCart(this.item));

        if (!this.user) {
            this.router.navigate(['', { outlets: { modal: ['auth', 'login'] } }], {
                queryParams: {
                    redirectUrl: this.getDwVfQueryParams(),
                },
            });
        } else {
            this.router.navigate(['/verification-dwolla'], {
                queryParams: {
                    redirect: this.router.url,
                    buy: true,
                },
            });
        }
    }

    private getDwVfQueryParams() {
        const urlTree = this.router.createUrlTree(['/verification-dwolla'], {
            queryParams: {
                redirect: this.router.url,
                buy: true,
            },
        });

        return this.router.serializeUrl(urlTree);
    }

    public onNotifyClick(): void {
        if (!this.user) {
            this.router.navigate(['', { outlets: { modal: ['auth', 'login'] } }]);
        } else {
            if (this.item?.subscribeStatus === 'Subscribed') {
                this.store.dispatch(new RemoveNotification(this.item?.id));
            } else {
                this.store.dispatch(new AddNotification(this.item?.id));
            }
        }
    }
}
