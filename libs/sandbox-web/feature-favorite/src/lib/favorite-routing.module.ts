import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavoriteComponent } from './favorite.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: FavoriteComponent,
                data: {
                    meta: {
                        title: 'Favorites',
                    },
                    wrapper: true,
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class FavoriteRoutingModule {}
