import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { UntilDestroy } from '@ngneat/until-destroy';
import { GetFavorites, ItemState } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-favorite',
    templateUrl: './favorite.component.html',
    styleUrls: ['./favorite.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FavoriteComponent {
    favorites$ = this.store.select(ItemState.favorites);

    constructor(private readonly store: Store) {
        this.store.dispatch(new GetFavorites());
    }
}
