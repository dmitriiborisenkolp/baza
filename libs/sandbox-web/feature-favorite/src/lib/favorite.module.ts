import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { ItemCardModule } from '@scaliolabs/sandbox-web/ui-components';
import { ItemStateModule } from '@scaliolabs/sandbox-web/data-access';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { FavoriteRoutingModule } from './favorite-routing.module';
import { FavoriteComponent } from './favorite.component';

@NgModule({
    declarations: [FavoriteComponent],
    imports: [
        CommonModule,
        FavoriteRoutingModule,
        ItemCardModule,
        ItemStateModule,
        NzButtonModule,
        NzCardModule,
        NzEmptyModule,
        NzGridModule,
        NzPopoverModule,
        NzProgressModule,
        NzStepsModule,
        NzToolTipModule,
        UtilModule,
    ],
})
export class FavoriteModule {}
