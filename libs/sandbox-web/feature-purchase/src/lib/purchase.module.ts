import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaNcWebPurchaseFlowModule } from '@scaliolabs/baza-nc-web-purchase-flow';
import { PurchaseCustomComponent } from './purchase-custom.component';
import { PurchaseRoutingCustomModule } from './purchase-routing-custom.module';

@NgModule({
    declarations: [PurchaseCustomComponent],
    imports: [CommonModule, BazaNcWebPurchaseFlowModule, PurchaseRoutingCustomModule],
    exports: [],
    providers: [],
})
export class PurchaseModule {}
