export * from './payment-bank-account-modal-override.i18n';
export * from './payment-bank-account-override.i18n';
export * from './payment-card-modal-override.i18n';
export * from './payment-card-override.i18n';
export * from './payment-methods-override.i18n';
