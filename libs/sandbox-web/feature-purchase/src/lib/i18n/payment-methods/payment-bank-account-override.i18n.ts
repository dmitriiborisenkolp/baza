export const NcPaymentBankAccountEnI18nOverride = {
    addMode: {
        plaidLinking: 'Link bank account - Override',
        manualLinking: 'Manually add bank details - Override',
    },
    editMode: {
        account: {
            name: 'Account holder name: - Override',
            number: 'Account number: - Override',
            type: 'Account type: - Override',
        },
    },
};
