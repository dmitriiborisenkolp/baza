import { NcPaymentBankAccountModalEnI18nOverride } from './payment-bank-account-modal-override.i18n';
import { NcPaymentBankAccountEnI18nOverride } from './payment-bank-account-override.i18n';
import { NcPaymentCardModalEnI18nOverride } from './payment-card-modal-override.i18n';
import { NcPaymentCardEnI18nOverride } from './payment-card-override.i18n';

export const NcPaymentMethodsEnI18nOverride = {
    bank: {
        details: NcPaymentBankAccountEnI18nOverride,
        modal: NcPaymentBankAccountModalEnI18nOverride,
    },
    card: {
        details: NcPaymentCardEnI18nOverride,
        modal: NcPaymentCardModalEnI18nOverride,
    },
};
