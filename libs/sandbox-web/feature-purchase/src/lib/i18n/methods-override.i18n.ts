export const NcMethodsEnI18nOverride = {
    title: {
        bank: 'Bank Account - Override',
        card: 'Card - Override',
        generic: 'Payment Method - Override',
    },
    tooltip: {
        caption:
            'This is your Payment Method. You can add a new Bank Account or Credit Card or switch between your linked payment methods. - Override',
        actions: {
            edit: 'Edit - Override',
        },
    },
    bank: {
        info: 'Your bank account will be utilized to receive dividends - Override',
    },
};
