export const NcPaymentEditUpdateEnI18nOverride = {
    title: 'Update Payment Method - Override',
    bank: {
        title: 'Bank Account - Override',
        plaid: {
            title: 'Link bank account - Override',
        },
        manual: {
            title: 'Manually add bank details - Override',
        },
    },
    card: {
        title: 'Card - Override',
        manual: {
            title: 'Add card - Override',
            feeSuffix: 'Fee - Override',
        },
        limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account - Override',
    },
};
