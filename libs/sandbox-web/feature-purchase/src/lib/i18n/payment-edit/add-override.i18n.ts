export const NcPaymentEditAddEnI18nOverride = {
    bank: {
        title: 'Bank Details - Override',
        actions: {
            add: 'Add Bank Account - Override',
        },
    },
    card: {
        title: 'Card Details - Override',
        actions: {
            add: 'Add Card - Override',
        },
    },
};
