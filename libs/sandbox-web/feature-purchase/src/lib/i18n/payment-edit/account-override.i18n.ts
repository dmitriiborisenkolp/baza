export const NcPaymentEditAccountEnI18nOverride = {
    bank: {
        title: 'Update Bank Account - Override',
        description:
            'You are about to begin adding a new bank account. If you complete this process, your current payment and dividend method account will be replaced. - Override',
        actions: {
            add: 'Add New Bank Account - Override',
        },
    },
    card: {
        title: 'Update Credit Card - Override',
        description:
            'You are about to begin adding a new сredit or debit сard. If you complete this process, your current card will be replaced. - Override',
        actions: {
            add: 'Add New Card - Override',
        },
    },
};
