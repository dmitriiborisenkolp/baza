import { NcPaymentEditAccountEnI18nOverride } from './account-override.i18n';
import { NcPaymentEditAddEnI18nOverride } from './add-override.i18n';
import { NcPaymentEditListEnI18nOverride } from './list-override.i18n';
import { NcPaymentEditUpdateEnI18nOverride } from './update-override.i18n';

export const NcPaymentEditEnI18nOverride = {
    account: NcPaymentEditAccountEnI18nOverride,
    add: NcPaymentEditAddEnI18nOverride,
    list: NcPaymentEditListEnI18nOverride,
    update: NcPaymentEditUpdateEnI18nOverride,
};
