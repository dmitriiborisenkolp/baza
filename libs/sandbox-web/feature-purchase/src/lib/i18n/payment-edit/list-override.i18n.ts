export const NcPaymentEditListEnI18nOverride = {
    title: {
        singular: 'Payment Method - Override',
        plural: 'Payment Methods - Override',
    },
    bank: {
        title: 'Bank Account - Override',
    },
    card: {
        title: 'Credit Сard - Override',
        feeSuffix: 'Card processing fee - Override',
        limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account - Override',
    },
    actions: {
        update: 'Update Payment Method - Override',
    },
};
