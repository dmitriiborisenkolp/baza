import { NcAgreementEnI18nOverride } from './agreement-override.i18n';
import { NcDetailsEnI18nOverride } from './details-override.i18n';
import { NcDoneEnI18nOverride } from './done-override.i18n';
import { NcMethodsEnI18nOverride } from './methods-override.i18n';
import { NcPaymentEnI18nOverride } from './payment-override.i18n';
import { NcPaymentEditEnI18nOverride } from './payment-edit/payment-edit-override.i18n';
import { NcPaymentMethodsEnI18nOverride } from './payment-methods/payment-methods-override.i18n';

export const BazaNcWebPFEnI18nOverride = {
    ncpf: {
        parent: {
            backlink: {
                text: 'Back to Listing Details - Override',
                confirmation: {
                    title: 'Are you sure you want to exit? - Override',
                    text: 'Your purchase will not be completed if you leave this page. - Override',
                },
            },
            steps: {
                detailsLabel: 'Purchase Details - Override',
                agreementLabel: 'Sign Agreement - Override',
                paymentLabel: 'Submit Payment - Override',
            },
            warnings: {
                pickShares: 'Please pick number of shares - Override',
            },
        },
        agreement: NcAgreementEnI18nOverride,
        details: NcDetailsEnI18nOverride,
        done: NcDoneEnI18nOverride,
        methods: NcMethodsEnI18nOverride,
        payment: NcPaymentEnI18nOverride,
        pymtEdit: NcPaymentEditEnI18nOverride,
        pymtMethods: NcPaymentMethodsEnI18nOverride,
        notifications: {
            cancel_purchase_fail: 'There was an error cancelling the Purchase. - Override',
            load_bank_account_fail: 'Failed to load Bank Account information - Override',
            load_credit_card_fail: 'Failed to load Credit Card information - Override',
            load_plaid_link_fail: 'Failed to load Plaid link - Override',
            save_bank_account_fail: 'Failed to save Bank Account information - Override',
            save_credit_card_fail: 'Failed to save Credit Card information - Override',
            set_manual_bank_account_fail: 'There was an error setting the Manual Bank Account. - Override',
            set_manual_bank_account_success: 'Manual Bank Account was successfully set - Override',
            submit_purchase_fail: 'There was an error submitting the Purchase. - Override',
            submit_purchase_success: 'Purchase successfully submitted - Override',
            purchase_start_fail: 'Could not start the session - Override',
            purchase_reprocess_success: 'Payment successfully re-submitted - Override',
            purchase_reprocess_fail: 'Payment retry request failed. Please contact your bank for further details - Override',
        },
    },
};
