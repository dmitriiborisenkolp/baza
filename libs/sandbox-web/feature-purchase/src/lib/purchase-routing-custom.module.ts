import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { getConfigurableRoutes, NcPurchaseDoneComponent } from '@scaliolabs/baza-nc-web-purchase-flow';
import { PurchaseCustomComponent } from './purchase-custom.component';

const routes: Routes = getConfigurableRoutes(PurchaseCustomComponent, NcPurchaseDoneComponent);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PurchaseRoutingCustomModule {}
