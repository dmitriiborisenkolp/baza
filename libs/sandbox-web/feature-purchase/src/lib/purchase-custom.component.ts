import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { VerificationState } from '@scaliolabs/baza-nc-web-verification-flow';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { BootstrapState, RequestAppBootstrapInitData } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-purchase-custom',
    templateUrl: './purchase-custom.component.html',
    styleUrls: ['./purchase-custom.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PurchaseCustomComponent {
    initData$ = this.store.select(BootstrapState.initData);
    personal$ = this.store.select(VerificationState.personal);
    public currentTab: number;

    constructor(private route: ActivatedRoute, private store: Store, private readonly wts: BazaWebUtilSharedService) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });

        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }
}
