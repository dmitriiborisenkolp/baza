import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { AccountState } from '@scaliolabs/sandbox-web/data-access';

@Component({
    selector: 'app-account-email',
    templateUrl: './email.component.html',
    styleUrls: ['./email.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountEmailComponent {
    user$ = this.store.select(AccountState.account);

    constructor(private store: Store) {}
}
