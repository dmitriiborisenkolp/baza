import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DwollaVerificationStateModule } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { VerificationStateModule } from '@scaliolabs/baza-nc-web-verification-flow';
import { DividendAlertStateModule, PortfolioStateModule } from '@scaliolabs/sandbox-web/data-access';
import { PasswordValidatorModule } from '@scaliolabs/sandbox-web/ui-components';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import { DeleteConfirmComponent } from './delete-confirm/delete-confirm.component';
import { AccountDeleteComponent } from './delete/delete.component';
import { AccountEmailComponent } from './email/email.component';
import { AccountInvestorProfileDwollaComponent } from './investor-profile-dwolla/investor-profile-dwolla.component';
import { AccountInvestorProfileComponent } from './investor-profile/investor-profile.component';
import { AccountPasswordComponent } from './password/password.component';
import { PaymentMethodModule } from './payment-method';
import { PaymentMethodDwollaModule } from './payment-method-dwolla/payment-method-dwolla.module';
import { AccountProfileComponent } from './profile/profile.component';
import { TaxDocumentsComponent } from './tax-documents/tax-documents.component';

@NgModule({
    declarations: [
        AccountComponent,
        AccountDeleteComponent,
        AccountEmailComponent,
        AccountInvestorProfileComponent,
        AccountInvestorProfileDwollaComponent,
        AccountPasswordComponent,
        AccountProfileComponent,
        DeleteConfirmComponent,
        TaxDocumentsComponent,
    ],
    imports: [
        PortfolioStateModule,
        AccountRoutingModule,
        CommonModule,
        FormsModule,
        NzAlertModule,
        NzButtonModule,
        NzCardModule,
        NzCheckboxModule,
        NzDatePickerModule,
        NzDividerModule,
        NzEmptyModule,
        NzFormModule,
        NzGridModule,
        NzInputModule,
        NzInputNumberModule,
        NzModalModule,
        NzPopoverModule,
        NzProgressModule,
        NzRadioModule,
        NzSelectModule,
        NzStepsModule,
        NzTableModule,
        NzTabsModule,
        NzToolTipModule,
        NzUploadModule,
        PasswordValidatorModule,
        PaymentMethodModule,
        PaymentMethodDwollaModule,
        ReactiveFormsModule,
        UtilModule,
        VerificationStateModule,
        DwollaVerificationStateModule,
        DividendAlertStateModule,
    ],
})
export class AccountModule {}
