import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { BehaviorSubject, EMPTY, Subject } from 'rxjs';
import { catchError, filter, finalize, tap } from 'rxjs/operators';
import { getErrorMessage, Message } from '@scaliolabs/baza-web-utils';
import { Account, AccountState, UpdateAccount } from '@scaliolabs/sandbox-web/data-access';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';

interface UpdateProfileForm extends FormGroup {
    controls: {
        firstName: AbstractControl;
        lastName: AbstractControl;
    };
    value: {
        firstName: string;
        lastName: string;
    };
}

@Component({
    selector: 'app-account-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountProfileComponent implements OnInit, OnDestroy {
    user$ = this.store.select(AccountState.account);

    unsubscribe$ = new Subject<void>();
    message$ = new Subject<Message | undefined>();
    isEditing$ = new BehaviorSubject(false);

    updateProfileForm = this.fb.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
    }) as UpdateProfileForm;

    userAccount: Account;

    constructor(
        private readonly fb: FormBuilder,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
    ) {}

    ngOnInit() {
        this.user$.pipe(filter((user) => !!user)).subscribe((response) => {
            this.userAccount = response;
            this.updateProfileForm.patchValue(this.userAccount);
        });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
    }

    toggleEdit() {
        this.isEditing$.next(!this.isEditing$.value);
        if (!this.isEditing$.value) {
            this.updateProfileForm.reset();
        } else {
            this.updateProfileForm.patchValue(this.userAccount);
        }
    }

    onFormSubmit(formEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.updateProfileForm, formEl)) {
            this.store
                .dispatch(new UpdateAccount(this.updateProfileForm.value))
                .pipe(
                    tap(() => this.isEditing$.next(false)),
                    catchError((error) => {
                        const message = getErrorMessage(error);
                        this.message$.next(message);
                        return EMPTY;
                    }),
                    finalize(() => {
                        this.message$.next(undefined);
                    }),
                )
                .subscribe();
        }
    }
}
