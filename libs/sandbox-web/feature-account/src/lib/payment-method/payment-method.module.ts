import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import {
    NcPaymentBankAccountModalModule,
    NcPaymentBankAccountModule,
    NcPaymentCardModalModule,
    NcPaymentCardModule,
} from '@scaliolabs/baza-nc-web-purchase-flow';
import { BankDetailsModule, CardDetailsModule, PaymentHeaderModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentMethodComponent } from './payment-method.component';

@NgModule({
    imports: [
        BankDetailsModule,
        CardDetailsModule,
        CommonModule,
        NzButtonModule,
        NzEmptyModule,
        NzGridModule,
        NzModalModule,
        NzSkeletonModule,
        NzAlertModule,
        NcPaymentBankAccountModalModule,
        NcPaymentBankAccountModule,
        NcPaymentCardModalModule,
        NcPaymentCardModule,
        PaymentHeaderModule,
        UtilModule,
    ],
    exports: [PaymentMethodComponent],
    declarations: [PaymentMethodComponent],
    providers: [],
})
export class PaymentMethodModule {}
