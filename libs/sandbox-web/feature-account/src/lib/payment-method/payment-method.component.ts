import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { GetLimits, LoadBankAccount, LoadCreditCard, PaymentMethodsService, PurchaseState } from '@scaliolabs/baza-nc-web-purchase-flow';
import { VerificationState } from '@scaliolabs/baza-nc-web-verification-flow';
import { DividendAlertState } from '@scaliolabs/sandbox-web/data-access';
import { BehaviorSubject, Subject } from 'rxjs';
import { take, tap } from 'rxjs/operators';

@UntilDestroy()
@Component({
    selector: 'app-payment-method',
    templateUrl: './payment-method.component.html',
    styleUrls: ['./payment-method.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentMethodComponent implements OnInit {
    card$ = this.store.select(PurchaseState.creditCard);
    bankAccount$ = this.store.select(PurchaseState.bankAccount);
    limits$ = this.store.select(PurchaseState.limits);
    verification$ = this.store.select(VerificationState.verification);
    showAlert$ = this.store.select(DividendAlertState.show);

    bankDetailsForm: FormGroup;
    cardDetailsForm: FormGroup;

    isBankDetailsFormVisible: boolean;
    isCardDetailsFormVisible: boolean;

    isBankDetailsFormWrapperVisible: boolean;
    isCardDetailsFormWrapperVisible: boolean;
    isBankMethodSelectionFormWrapperVisible: boolean;

    onSubmitManualBankDetailsForm: Subject<FormGroup> = new Subject();
    onSubmitCardForm: Subject<FormGroup> = new Subject();

    isCardLoading$ = new BehaviorSubject<boolean>(false);
    isBankAccountLoading$ = new BehaviorSubject<boolean>(false);

    constructor(private readonly store: Store, private readonly paymentMethodsService: PaymentMethodsService) {
        this.store.dispatch(new GetLimits());

        this.bankAccount$.pipe(take(1), untilDestroyed(this)).subscribe(() => {
            this.isBankAccountLoading$.next(true);

            this.store
                .dispatch(new LoadBankAccount())
                .pipe(untilDestroyed(this))
                .subscribe(() => {
                    this.isBankAccountLoading$.next(false);
                });
        });

        this.card$.pipe(take(1), untilDestroyed(this)).subscribe(() => {
            this.isCardLoading$.next(true);

            this.store
                .dispatch(new LoadCreditCard())
                .pipe(untilDestroyed(this))
                .subscribe(() => {
                    this.isCardLoading$.next(false);
                });
        });
    }

    public ngOnInit(): void {
        this.bankDetailsForm = this.paymentMethodsService.generateBankDetailsForm();
        this.cardDetailsForm = this.paymentMethodsService.generateCreditCardForm();

        this.onSubmitManualBankDetailsForm
            .pipe(
                tap((value) => this.processSubmitManualBankDetailsForm(value)),
                untilDestroyed(this),
            )
            .subscribe();

        this.onSubmitCardForm
            .pipe(
                tap((value) => this.processSubmitCardForm(value)),
                untilDestroyed(this),
            )
            .subscribe();
    }

    verificationCompleted(verification: { currentStep: AccountVerificationStep }) {
        return AccountVerificationStep.Completed === verification.currentStep;
    }

    showBankAccountDetails() {
        this.isBankDetailsFormVisible = true;
    }

    showCardDetails() {
        this.isCardDetailsFormVisible = true;
    }

    hideCardDetails() {
        this.isCardDetailsFormVisible = false;
    }

    hideBankAccountDetails() {
        this.isBankDetailsFormVisible = false;
    }

    showBankAccountDetailsWrapper() {
        this.isBankDetailsFormWrapperVisible = true;
    }

    showCardDetailsWrapper() {
        this.isCardDetailsFormWrapperVisible = true;
    }

    showBankMethodSelectionFormWrapper() {
        this.isBankMethodSelectionFormWrapperVisible = true;
    }

    hideBankAccountDetailsWrapper() {
        this.isBankDetailsFormWrapperVisible = false;
    }

    hideCardDetailsWrapper() {
        this.isCardDetailsFormWrapperVisible = false;
    }

    hideBankMethodSelectionFormWrapper() {
        this.isBankMethodSelectionFormWrapperVisible = false;
    }

    onBankLinkFromWrapper() {
        this.onBankLink();
        this.hideBankMethodSelectionFormWrapper();
    }

    showBankAccountDetailsFromWrapper() {
        this.showBankAccountDetails();
        this.hideBankMethodSelectionFormWrapper();
    }

    showBankMethodSelection() {
        this.hideBankAccountDetailsWrapper();
        this.showBankMethodSelectionFormWrapper();
    }

    showCardDetailsFromWrapper() {
        this.showCardDetails();
        this.hideCardDetailsWrapper();
    }

    public applyCardDetails() {
        this.onSubmitCardForm.next(this.cardDetailsForm);
    }

    public applyBankAccountDetails() {
        this.onSubmitManualBankDetailsForm.next(this.bankDetailsForm);
    }

    processSubmitManualBankDetailsForm = (form) =>
        this.paymentMethodsService.processSubmitManualBankDetailsForm(form, () => {
            this.bankDetailsForm.reset();
            this.hideBankAccountDetails();
            this.store.dispatch(new LoadBankAccount());
        });

    processSubmitCardForm = (form) =>
        this.paymentMethodsService.processSubmitCardForm(form, () => {
            this.cardDetailsForm.reset();
            this.hideCardDetails();
            this.store.dispatch(new LoadCreditCard());
        });

    async onBankLink() {
        this.paymentMethodsService.processPlaidDialog(() => {
            this.bankDetailsForm.reset();
            this.hideBankAccountDetails();
            this.store.dispatch(new LoadBankAccount());
        });
    }
}
