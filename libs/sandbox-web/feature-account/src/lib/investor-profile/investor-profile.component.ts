import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { PersonalInformation, VerificationState } from '@scaliolabs/baza-nc-web-verification-flow';

@UntilDestroy()
@Component({
    selector: 'app-account-investor-profile',
    templateUrl: './investor-profile.component.html',
    styleUrls: ['./investor-profile.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountInvestorProfileComponent {
    verification$ = this.store.select(VerificationState.verification);

    constructor(private readonly store: Store) {}

    public getFormattedAddressLine1(info: PersonalInformation): string {
        let address = `${info.residentialStreetAddress1}, `;

        if (info.residentialStreetAddress2) {
            address += `${info.residentialStreetAddress2}, `;
        }
        return address;
    }

    public getFormattedAddressLine2(info: PersonalInformation): string {
        let address = '';
        address += `${info.residentialCity}, ${info.residentialState ? info.residentialState : ''} ${info.residentialZipCode}`;
        return address;
    }
}
