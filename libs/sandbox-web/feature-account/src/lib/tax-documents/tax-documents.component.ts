import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BazaNcTaxDocumentDataAccess } from '@scaliolabs/baza-nc-data-access';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { BazaNcTaxDocumentListResponse } from '@scaliolabs/baza-nc-shared';

@UntilDestroy()
@Component({
    selector: 'app-tax-documents',
    templateUrl: './tax-documents.component.html',
    styleUrls: ['./tax-documents.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaxDocumentsComponent {
    taxDocuments$: Subject<BazaNcTaxDocumentListResponse> = new Subject();

    constructor(private taxDocumentAccessService: BazaNcTaxDocumentDataAccess) {
        this.loadTaxDocumentsInfo();
    }

    private loadTaxDocumentsInfo() {
        this.taxDocumentAccessService
            .list({ size: -1 })
            .pipe(untilDestroyed(this))
            .subscribe((taxDocuments) => {
                this.taxDocuments$.next(taxDocuments);
            });
    }
}
