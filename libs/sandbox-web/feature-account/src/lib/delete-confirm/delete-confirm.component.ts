import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { BazaRegistryNgService } from '@scaliolabs/baza-core-ng';
import { BazaNcIntegrationPortfolioTotalStatsDto } from '@scaliolabs/baza-nc-integration-shared';

@Component({
    selector: 'app-delete-confirm',
    templateUrl: './delete-confirm.component.html',
    styleUrls: ['./delete-confirm.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteConfirmComponent {
    @Input()
    totalPortfolio: Observable<BazaNcIntegrationPortfolioTotalStatsDto>;

    @Input()
    visible: boolean;

    @Output()
    onConfirm: EventEmitter<boolean> = new EventEmitter();

    @Output()
    onCancel: EventEmitter<boolean> = new EventEmitter();

    constructor(private readonly registry: BazaRegistryNgService) {}

    cancel(): void {
        this.onCancel.emit(true);
    }

    confirm(): void {
        this.onConfirm.emit(true);
    }

    public getContactUsEmail(): string {
        return this.registry.value('bazaContentTypes.contacts.email');
    }
}
