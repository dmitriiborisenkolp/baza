import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { mapTo, tap } from 'rxjs/operators';
import { UpdatePassword } from '@scaliolabs/sandbox-web/data-access';
import {
    catchErrorMessage,
    lowercaseValidator,
    Message,
    mismatchValidator,
    numberValidator,
    specialSymbolValidator,
    uppercaseValidator,
} from '@scaliolabs/baza-web-utils';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';

interface UpdatePasswordForm extends FormGroup {
    controls: {
        oldPassword: AbstractControl;
        newPassword: AbstractControl;
        password: AbstractControl;
    };
    values: {
        oldPassword: string;
        newPassword: string;
        password: string;
    };
}

@UntilDestroy()
@Component({
    selector: 'app-account-password',
    templateUrl: './password.component.html',
    styleUrls: ['./password.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountPasswordComponent {
    public message$: Observable<Message | undefined>;
    isEditing$ = new BehaviorSubject(false);
    public updatePasswordForm: UpdatePasswordForm = this.fb.group(
        {
            oldPassword: ['', Validators.required],
            newPassword: [
                '',
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    lowercaseValidator(),
                    numberValidator(),
                    specialSymbolValidator(),
                    uppercaseValidator(),
                ]),
            ],
            passwordConfirmation: ['', Validators.compose([Validators.required])],
        },
        {
            validators: mismatchValidator('newPassword', 'passwordConfirmation'),
        },
    ) as UpdatePasswordForm;

    constructor(
        private readonly fb: FormBuilder,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
    ) {}

    toggleEdit() {
        this.onFormReset();
        this.isEditing$.next(!this.isEditing$.value);
    }

    public onFormReset(): void {
        this.updatePasswordForm.reset();
        this.message$ = null;
    }

    public onFormSubmit(formEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.updatePasswordForm, formEl)) {
            const form = this.updatePasswordForm.value;

            this.message$ = this.store
                .dispatch(
                    new UpdatePassword({
                        oldPassword: form.oldPassword,
                        newPassword: form.newPassword,
                        confirmPassword: form.passwordConfirmation,
                    }),
                )
                .pipe(
                    untilDestroyed(this),
                    tap(() => this.isEditing$.next(false)),
                    mapTo(''),
                    catchErrorMessage,
                );
        }
    }
}
