import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { DwollaPaymentMethodsModule, DwollaPayoutMethodsModule, WithdrawModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { PaymentMethodDwollaComponent } from './payment-method-dwolla.component';

const NZ_MODULES = [NzEmptyModule, NzPopoverModule, NzButtonModule];

@NgModule({
    declarations: [PaymentMethodDwollaComponent],
    imports: [
        CommonModule,
        DwollaPaymentMethodsModule,
        DwollaPayoutMethodsModule,
        WithdrawModule,
        TranslateModule,
        UtilModule,
        ...NZ_MODULES,
    ],
    exports: [PaymentMethodDwollaComponent],
})
export class PaymentMethodDwollaModule {}
