export const DwollaPaymentBalanceEnI18n = {
    title: 'Account Balance - Override',
    details: {
        prefix: 'Current balance: - Override',
        hint: 'Fund your account - Override',
    },
    alerts: {
        createAccount: 'Create an Account Balance to receive dividends and invest from your balance - Override',
        walletSupport: {
            label: `There was an issue creating your wallet. We're sorry for the inconvenience. Please <a data-link="walletLink">{{walletLink}}</a> for technical support. - Override`,
            linkConfig: {
                key: 'walletLink',
                text: 'contact us - Override',
            },
        },
    },
    actions: {
        addFunds: 'Add funds - Override',
    },
    linkedBank: {
        title: 'Bank Account - Override',
        hint: 'Used to fund your account balance - Override',
        actions: {
            update: 'Update - Override',
        },
    },
};
