export const DwollaPaymentCardEnI18n = {
    title: 'Card - Override',
    details: {
        number: 'Card number: - Override',
        expiring: 'Expiring: - Override',
        fee: 'card processing fee - Override',
    },
    alerts: {
        limit: 'Purchases above {{ limit }} require the use of a linked bank account. - Override',
    },
    actions: {
        update: 'Update - Override',
    },
};
