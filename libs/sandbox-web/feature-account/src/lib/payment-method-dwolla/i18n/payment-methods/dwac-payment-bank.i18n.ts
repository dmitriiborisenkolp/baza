export const DwollaPaymentBankEnI18n = {
    title: 'Bank Account - Override',
    hint: 'Used to purchase shares - Override',
    details: {
        name: 'Account holder name: - Override',
        number: 'Account number: - Override',
        type: 'Account type: - Override',
    },
    actions: {
        update: 'Update - Override',
    },
};
