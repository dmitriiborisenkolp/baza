export const DwollaPayoutBankEnI18n = {
    title: 'Bank Account - Override',
    hint: 'Used to withdraw funds from your wallet - Override',
    alerts: {
        withdrawalSupport: {
            label: 'Reach out to us at <a data-link="withdrawalLink"></a> to get instructions on withdrawal. - Override',
            linkConfig: {
                key: 'withdrawalLink',
                text: 'baza-test-contact-us@scal.io - Override',
            },
        },
        linkBank: 'You need to have linked bank account to withdraw from your balance. - Override',
        walletSupport: {
            label: 'There was a failed attempt to create a Wallet. Please <a data-link="walletLink"></a> for technical support. - Override',
            linkConfig: {
                key: 'walletLink',
                text: 'contact us - Override',
            },
        },
    },
    actions: {
        update: 'Update - Override',
    },
};
