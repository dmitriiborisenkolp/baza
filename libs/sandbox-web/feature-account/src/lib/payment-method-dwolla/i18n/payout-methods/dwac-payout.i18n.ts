export const DwollaPayoutEnI18n = {
    title: 'Payout Methods - Override',
    popover: {
        content:
            'Your payout method is the account where you will receive deposits from distributions paid through the platform. A US-based bank account is required in order to receive distributions. - Override',
    },
};
