import { DwollaPaymentBalanceEnI18n } from './payment-methods/dwac-payment-balance.i18n';
import { DwollaPaymentBankEnI18n } from './payment-methods/dwac-payment-bank.i18n';
import { DwollaPaymentCardEnI18n } from './payment-methods/dwac-payment-card.i18n';
import { DwollaPaymentEnI18n } from './payment-methods/dwac-payment.i18n';
import { DwollaPayoutBankEnI18n } from './payout-methods/dwac-payout-bank.i18n';
import { DwollaPayoutEnI18n } from './payout-methods/dwac-payout.i18n';

export const BazaDwollaWebAccountEnI18nOverride = {
    dwacc: {
        payment: {
            ...DwollaPaymentEnI18n,
            balance: DwollaPaymentBalanceEnI18n,
            bank: DwollaPaymentBankEnI18n,
            card: DwollaPaymentCardEnI18n,
        },
        payout: {
            ...DwollaPayoutEnI18n,
            bank: DwollaPayoutBankEnI18n,
        },
    },
};
