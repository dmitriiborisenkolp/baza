import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { DwollaSharedService } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { DwollaVerificationState } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { BootstrapState, RequestAppBootstrapInitData } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-payment-method-dwolla',
    templateUrl: './payment-method-dwolla.component.html',
    styleUrls: ['./payment-method-dwolla.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentMethodDwollaComponent {
    initData$ = this.store.select(BootstrapState.initData);
    verification$ = this.store.select(DwollaVerificationState.verification);
    isWithdrawTriggerEnabled = false;

    constructor(private readonly store: Store, public readonly dss: DwollaSharedService, private readonly wts: BazaWebUtilSharedService) {
        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }

    verificationCompleted(verification: { currentStep: AccountVerificationStep }) {
        return AccountVerificationStep.Completed === verification?.currentStep;
    }
}
