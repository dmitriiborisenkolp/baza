import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { GetTotals, PortfolioState, SendDeactivateAccountLink } from '@scaliolabs/sandbox-web/data-access';
import { catchErrorMessage, Message } from '@scaliolabs/baza-web-utils';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';

interface AccountDeleteForm extends FormGroup {
    controls: {
        password: AbstractControl;
    };
    value: {
        password: string;
    };
}

@UntilDestroy()
@Component({
    selector: 'app-account-delete',
    templateUrl: './delete.component.html',
    styleUrls: ['./delete.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountDeleteComponent implements OnDestroy {
    deleteAccountConfirmVisible = false;

    unsubscribe$ = new Subject<void>();
    message$: Observable<Message | undefined>;
    isEditing$ = new BehaviorSubject(false);
    accountDeleteForm = this.fb.group({
        password: ['', Validators.required],
    }) as AccountDeleteForm;

    totalPortfolio$ = this.store.select(PortfolioState.totals);
    formEl: HTMLFormElement;

    constructor(
        private readonly fb: FormBuilder,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
    ) {
        this.store.dispatch(new GetTotals());
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
    }

    toggleEdit() {
        this.onFormReset();
        this.isEditing$.next(!this.isEditing$.value);
    }

    public onFormReset(): void {
        this.accountDeleteForm.reset();
        this.message$ = null;
    }

    confirmAccountDeletion(formEl): void {
        this.formEl = formEl;
        if (this.bazaFormValidatorService.isFormValid(this.accountDeleteForm, this.formEl)) {
            this.deleteAccountConfirmVisible = true;
        }
    }

    handleCancel(): void {
        this.deleteAccountConfirmVisible = false;
    }

    handleDelete(): void {
        this.handleCancel();
        this.message$ = this.store.dispatch(new SendDeactivateAccountLink(this.accountDeleteForm.value)).pipe(
            untilDestroyed(this),
            tap(() => this.isEditing$.next(false)),
            catchErrorMessage,
        );
    }
}
