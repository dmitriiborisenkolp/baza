import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DwollaVerificationResolver } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { VerificationResolver } from '@scaliolabs/baza-nc-web-verification-flow';
import { AccountComponent } from './account.component';
import { BootstrapResolver } from '@scaliolabs/sandbox-web/data-access';
const routes: Routes = [
    {
        path: '',
        data: {
            wrapper: true,
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'personal-info',
            },
            {
                path: 'personal-info',
                component: AccountComponent,
                data: {
                    tab: '0',
                    meta: {
                        title: 'Personal Information',
                    },
                },
            },
            {
                path: 'payment-method',
                component: AccountComponent,
                resolve: {
                    verification: VerificationResolver,
                },
                data: {
                    tab: '1',
                    meta: {
                        title: 'Payments & Payouts (NC)',
                    },
                },
            },
            {
                path: 'payment-method-dwolla',
                component: AccountComponent,
                resolve: {
                    verification: DwollaVerificationResolver,
                    bootstrap: BootstrapResolver,
                },
                data: {
                    tab: '2',
                    meta: {
                        title: 'Payments & Payouts (Dwolla)',
                    },
                },
            },
            {
                path: 'investor-account',
                component: AccountComponent,
                resolve: {
                    verification: VerificationResolver,
                },
                data: {
                    tab: '3',
                    meta: {
                        title: 'Investor Account (NC)',
                    },
                },
            },
            {
                path: 'investor-account-dwolla',
                component: AccountComponent,
                resolve: {
                    verification: DwollaVerificationResolver,
                    bootstrap: BootstrapResolver,
                },
                data: {
                    tab: '4',
                    meta: {
                        title: 'Investor Account (Dwolla)',
                    },
                },
            },
            {
                path: 'tax-documents',
                component: AccountComponent,
                data: {
                    tab: '5',
                    meta: {
                        title: 'Tax Documents',
                    },
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AccountRoutingModule {}
