import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { DividendAlertState } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountComponent {
    public currentTab: number;

    showAlert$ = this.store.select(DividendAlertState.show);

    constructor(private route: ActivatedRoute, private router: Router, private store: Store) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });
    }

    setCurrentTab(index: number) {
        switch (index) {
            case 0:
                this.router.navigate(['/account', 'personal-info']);
                break;
            case 1:
                this.router.navigate(['/account', 'payment-method']);
                break;
            case 2:
                this.router.navigate(['/account', 'payment-method-dwolla']);
                break;
            case 3:
                this.router.navigate(['/account', 'investor-account']);
                break;
            case 4:
                this.router.navigate(['/account', 'investor-account-dwolla']);
                break;
            case 5:
                this.router.navigate(['/account', 'tax-documents']);
                break;
            default:
                this.router.navigate(['/account', 'personal-info']);
                break;
        }
    }
}
