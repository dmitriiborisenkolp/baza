import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { DwollaVerificationState, PersonalInformation } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { BazaNcBootstrapDto } from '@scaliolabs/baza-nc-shared';
import { BootstrapState } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-account-investor-profile-dwolla',
    templateUrl: './investor-profile-dwolla.component.html',
    styleUrls: ['./investor-profile-dwolla.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountInvestorProfileDwollaComponent {
    verification$ = this.store.select(DwollaVerificationState.verification);
    initData$ = this.store.select(BootstrapState.initData);

    initData: BazaNcBootstrapDto;

    constructor(private readonly store: Store, private readonly router: Router) {
        this.initData$.pipe(untilDestroyed(this)).subscribe((res) => {
            this.initData = res;
        });
    }

    public getFormattedAddressLine1(info: PersonalInformation): string {
        let address = `${info.residentialStreetAddress1}, `;

        if (info.residentialStreetAddress2) {
            address += `${info.residentialStreetAddress2}, `;
        }

        return address;
    }

    public getFormattedAddressLine2(info: PersonalInformation): string {
        let address = '';
        address += `${info.residentialCity}, ${info.residentialState ? info.residentialState : ''} ${info.residentialZipCode}`;
        return address;
    }

    redirectToVF() {
        this.router.navigate([`/verification-dwolla${this.initData?.investorAccount.isAccountVerificationCompleted ? '/info' : ''}`], {
            queryParams: {
                redirect: this.router.url,
            },
        });
    }
}
