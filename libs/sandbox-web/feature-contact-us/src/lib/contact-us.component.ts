import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { customDefaultValidator, getErrorMessage, Message } from '@scaliolabs/baza-web-utils';
import { AccountState, ContactUsForm, ContactUsFx, ContactUsState } from '@scaliolabs/sandbox-web/data-access';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable, of } from 'rxjs';
import { catchError, filter, mapTo, tap } from 'rxjs/operators';

@Component({
    selector: 'app-contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['./contact-us.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactUsComponent implements OnInit {
    user$ = this.store.select(AccountState.account);
    subjects$ = this.store.select(ContactUsState.subjects);

    message$: Observable<Message | undefined>;

    isAccountActive = true;

    form: ContactUsForm = this.fb.group({
        fullName: ['', Validators.compose([Validators.required])],
        email: ['', Validators.compose([Validators.required, Validators.email])],
        subject: ['Default', Validators.compose([customDefaultValidator('Default')])],
        message: ['', Validators.compose([Validators.required])],
    }) as ContactUsForm;

    constructor(
        private readonly fb: FormBuilder,
        private readonly nzNotificationService: NzNotificationService,
        private readonly router: Router,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
    ) {}

    ngOnInit() {
        this.user$.pipe(filter((user) => !!user)).subscribe((response) => {
            this.form.patchValue(response);
        });
    }

    onFormSubmit(formEl, modalEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.form, formEl, modalEl)) {
            const form = this.form.value;

            this.message$ = this.store
                .dispatch(
                    new ContactUsFx({
                        name: form.fullName,
                        email: form.email,
                        subjectId: +(form.subject ?? 0),
                        message: form.message,
                    }),
                )
                .pipe(
                    mapTo(undefined),
                    tap(() => this.nzNotificationService.success('Success', 'Your message was successfully sent.')),
                    tap(() => this.onModalClose()),
                    catchError((err) => of(getErrorMessage(err))),
                );
        }
    }

    onModalClose() {
        this.router.navigate([{ outlets: { modal: null } }]);
    }
}
