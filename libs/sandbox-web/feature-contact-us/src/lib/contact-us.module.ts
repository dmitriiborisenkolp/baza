import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { ContactUsStateModule } from '@scaliolabs/sandbox-web/data-access';

import { ContactUsComponent } from './contact-us.component';

const routes: Routes = [
    {
        path: '',
        component: ContactUsComponent,
    },
];

@NgModule({
    declarations: [ContactUsComponent],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        ReactiveFormsModule,
        NzAlertModule,
        NzButtonModule,
        NzFormModule,
        NzInputModule,
        NzSelectModule,
        NzModalModule,
        ContactUsStateModule,
        UtilModule,
    ],
    exports: [RouterModule],
})
export class ContactUsModule {}
