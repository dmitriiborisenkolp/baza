export const WebUiPaymentBankAccountModalEnI18nOverride = {
    title: 'Bank Details - Override',
    actions: {
        add: 'Add Bank Account - Override',
    },
};
