export const WebUiAccountBalanceCardEnI18nOverride = {
    title: 'Account Balance - Override',
    prefix: 'Current balance: - Override',
    hint: 'Fund your account - Override',
    actions: {
        withdraw: 'Withdraw - Override',
        addFunds: 'Add Funds - Override',
    },
};
