export const WebUiAddFundsModalEnI18nOverride = {
    title: 'Transfer Funds To Account Balance - Override',
    cashIn: {
        heading: 'Transfer From - Override',
        actions: {
            edit: 'Edit - Override',
        },
    },
    actions: {
        transfer: 'Transfer Funds - Override',
    },
};
