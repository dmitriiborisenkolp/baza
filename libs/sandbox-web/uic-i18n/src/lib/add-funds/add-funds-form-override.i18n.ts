export const WebUiAddFundsFormEnI18nOverride = {
    fields: {
        amount: {
            label: 'Amount - Override',
            validators: {
                required: 'Please enter the amount to be transferred - Override',
            },
        },
    },
    alerts: {
        amountWarning: `Funds won't be immediately added to your balance. If you want to make a purchase right away, you can choose an alternative payment method. - Override`,
        amountError:
            'The funds were not added to the account. <br/> Please try again later or reach out to <a href="{{ link }}"> Technical support - Override',
    },
};
