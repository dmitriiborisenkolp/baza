import { WebUiAccountBalanceCardEnI18nOverride } from './account-balance-card/account-balance-card-override.i18n';
import { WebUiBankDetailsEnI18nOverride } from './bank-details/bank-details-override.i18n';
import { WebUiCardDetailsEnI18nOverride } from './card-details/card-details-override.i18n';
import { WebUiAddFundsFormEnI18nOverride } from './add-funds/add-funds-form-override.i18n';
import { WebUiAddFundsModalEnI18nOverride } from './add-funds/add-funds-modal-override.i18n';
import { WebUiAddFundsNotificationEnI18nOverride } from './add-funds/add-funds-notification-override.i18n';
import { WebUiWithdrawFundsFormEnI18nOverride } from './withdraw-funds/withdraw-funds-form-override.i18n';
import { WebUiWithdrawFundsModalEnI18nOverride } from './withdraw-funds/withdraw-funds-modal-override.i18n';
import { WebUiWithdrawFundsNotificationEnI18nOverride } from './withdraw-funds/withdraw-funds-notification-override.i18n';
import { WebUiPersonalInfoEnI18nOverride } from './personal/personal-override.i18n';
import { WebUiSummaryEnI18nOverride } from './summary/summary-override.i18n';
import { WebUiPaymentBankAccountEnI18nOverride } from './payment-bank-account/payment-bank-account-override.i18n';
import { WebUiPaymentBankAccountModalEnI18nOverride } from './payment-bank-account-modal/payment-bank-account-modal-override.i18n';
import { WebUiPaymentCardEnI18nOverride } from './payment-card/payment-card-override.i18n';
import { WebUiPaymentCardModalEnI18nOverride } from './payment-card-modal/payment-card-modal-override.i18n';

export const BazaWebUiEnI18nOverride = {
    uic: {
        balance: WebUiAccountBalanceCardEnI18nOverride,
        summary: WebUiSummaryEnI18nOverride,
        paymentBank: WebUiPaymentBankAccountEnI18nOverride,
        paymentBankModal: WebUiPaymentBankAccountModalEnI18nOverride,
        bankDetails: WebUiBankDetailsEnI18nOverride,
        paymentCard: WebUiPaymentCardEnI18nOverride,
        paymentCardModal: WebUiPaymentCardModalEnI18nOverride,
        cardDetails: WebUiCardDetailsEnI18nOverride,
        personal: WebUiPersonalInfoEnI18nOverride,
        addFunds: {
            form: WebUiAddFundsFormEnI18nOverride,
            modal: WebUiAddFundsModalEnI18nOverride,
            notification: WebUiAddFundsNotificationEnI18nOverride,
        },
        withdrawFunds: {
            form: WebUiWithdrawFundsFormEnI18nOverride,
            modal: WebUiWithdrawFundsModalEnI18nOverride,
            notification: WebUiWithdrawFundsNotificationEnI18nOverride,
        },
    },
};
