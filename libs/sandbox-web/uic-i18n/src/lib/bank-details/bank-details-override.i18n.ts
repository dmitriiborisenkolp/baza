export const WebUiBankDetailsEnI18nOverride = {
    form: {
        fields: {
            accountName: {
                label: 'Account Holder Full Name - Override',
                validators: {
                    restrictedChars: 'Special characters are not allowed, only spaces and hyphen (-) are permitted - Override',
                },
            },
            accountType: {
                label: 'Account Type - Override',
                options: {
                    checking: 'Checking - Override',
                    savings: 'Savings - Override',
                },
                placeholder: 'Select One - Override',
            },
            accountNumber: {
                label: 'Account Number - Override',
                validators: {
                    minlength: 'Account Number has to be a minimum of {{ length }} symbols - Override',
                },
            },
            accountRoutingNumber: {
                label: 'Routing Number - Override',
                validators: {
                    onlynumbers: 'Only numbers are accepted - Override',
                    minlength: 'The length should be exactly {{ min }} digits - Override',
                    maxlength: 'The length should be exactly {{ max }} digits - Override',
                    routingNumber: 'It should be a valid routing number - Override',
                },
            },
        },
        genericValidators: {
            required: '{{ fieldLabel }} is required - Override',
        },
    },
};
