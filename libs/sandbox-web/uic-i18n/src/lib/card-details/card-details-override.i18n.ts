export const WebUiCardDetailsEnI18nOverride = {
    form: {
        fields: {
            ccName: {
                label: 'Name on card - Override',
                validators: {
                    restrictedChars: 'Special characters are not allowed, only spaces and hyphen (-) are permitted - Override',
                },
            },
            ccNumber: {
                label: 'Card number - Override',
                validators: {
                    onlynumbers: 'Only numbers are accepted - Override',
                    minlength: 'Card Number has to be a minimum of {{ min }} digits - Override',
                    maxlength: 'Card Number has to be maximum of {{ max }} digits - Override',
                },
            },
            expireDate: {
                label: 'Expiry date - Override',
            },
            cvv: {
                label: 'CVV - Override',
                validators: {
                    onlynumbers: 'Only numbers are accepted - Override',
                    minlength: 'CVV has to be a minimum of {{ min }} digits - Override',
                    maxlength: 'CVV has to be maximum of {{ max }} digits - Override',
                },
            },
        },
        genericValidators: {
            required: '{{ fieldLabel }} is required - Override',
        },
    },
};
