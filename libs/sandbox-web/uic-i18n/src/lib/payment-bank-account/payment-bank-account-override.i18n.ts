export const WebUiPaymentBankAccountEnI18nOverride = {
    addMode: {
        plaidLinking: 'Link Bank Account - Override',
        manualLinking: 'Bank Account - Override',
    },
    editMode: {
        account: {
            name: 'Account holder name: - Override',
            number: 'Account number: - Override',
            type: 'Account type: - Override',
        },
    },
};
