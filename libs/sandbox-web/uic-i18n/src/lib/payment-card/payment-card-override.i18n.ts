export const WebUiPaymentCardEnI18nOverride = {
    addMode: {
        title: 'Card - Override',
        feeSuffix: 'Card processing fee - Override',
    },
    editMode: {
        title: 'Card - Override',
        expiring: 'Expiring - Override',
        cardDetails: {
            number: 'Card number: - Override',
            expiring: 'Expiring: - Override',
            feeSuffix: 'Card processing fee - Override',
        },
    },
    alerts: {
        limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account - Override',
    },
};
