export const WebUiWithdrawFundsModalEnI18nOverride = {
    title: 'Withdraw Funds - override',
    subtitle: 'Withdrawal Balance: {{ amount }} - override',
    cashOut: {
        heading: 'Withdraw To - override',
        actions: {
            edit: 'Edit - override',
        },
    },
    actions: {
        transfer: 'Withdraw Funds - override',
    },
};
