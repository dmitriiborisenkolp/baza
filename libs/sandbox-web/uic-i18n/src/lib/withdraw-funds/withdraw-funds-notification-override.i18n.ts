export const WebUiWithdrawFundsNotificationEnI18nOverride = {
    title: 'Funds Withdrawal Initiated - override',
    content: 'Your funds will be available shortly, and we will notify you through email. - override',
    actions: {
        accept: 'Got it - override',
    },
};
