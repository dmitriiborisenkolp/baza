export const WebUiSummaryEnI18nOverride = {
    title: 'Summary - Override',
    numOfShares: 'Number of shares - Override',
    pricePerShare: 'Price per share - Override',
    transactionFee: 'Transaction fee - Override',
    transactionFeePopover:
        'This is a due diligence fee paid to cover costs associated with investigating, evaluating and purchasing underlying portfolio assets - Override',
    paymentMethodFee: 'Payment method fee - Override',
    total: 'Total - Override',
};
