export const WebUiPersonalDwollaEnI18nOverride = {
    title: 'Personal Info - Override',
    details: {
        name: 'Name: - Override',
        dateOfBirth: 'Date of birth: - Override',
        address: 'Address: - Override',
        ssn: 'Social Security Number: - Override',
        document: 'Uploaded Document: - Override',
    },
    popover: {
        message: 'This is your Investor Account information. You can review it and make changes if needed. - Override',
        actions: {
            edit: 'Edit - Override',
        },
    },
};
