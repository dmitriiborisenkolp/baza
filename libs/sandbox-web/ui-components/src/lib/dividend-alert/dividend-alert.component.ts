import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { DividendAlertState, GetDividendAlert } from '@scaliolabs/sandbox-web/data-access';

@Component({
    selector: 'app-dividend-alert',
    templateUrl: './dividend-alert.component.html',
    styleUrls: ['./dividend-alert.component.less'],
})
export class DividendAlertComponent implements OnInit, OnDestroy {
    showAlert$ = this.store.select(DividendAlertState.show);

    @Output() openedAlertChange: EventEmitter<boolean> = new EventEmitter();

    constructor(private readonly router: Router, private readonly store: Store) {
        this.store.dispatch(new GetDividendAlert());
    }

    ngOnInit(): void {
        this.showAlert$.subscribe((resp) => this.openedAlertChange.emit(resp));
    }

    /**
     * My Account link click event handler.
     * Navigate user to payment method page.
     */
    navigateToAccount(): void {
        this.router.navigate(['/account', 'payment-method']);
    }

    ngOnDestroy(): void {
        this.openedAlertChange.emit(false);
    }
}
