import { Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Account, AccountState } from '@scaliolabs/sandbox-web/data-access';
import { Store } from '@ngxs/store';

@UntilDestroy()
@Component({
    selector: 'app-footer-menu',
    templateUrl: './footer-menu.component.html',
    styleUrls: ['./footer-menu.component.less'],
})
export class FooterMenuComponent {
    user$ = this.store.select(AccountState.account);

    public user: Account;

    constructor(private store: Store) {
        this.user$.pipe(untilDestroyed(this)).subscribe((user) => (this.user = user));
    }
}
