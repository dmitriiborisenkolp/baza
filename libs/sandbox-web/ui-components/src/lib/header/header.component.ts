import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AccountState, Logout } from '@scaliolabs/sandbox-web/data-access';
import { ClearPurchaseState } from '@scaliolabs/baza-nc-web-purchase-flow';
import { ClearVerificationState } from '@scaliolabs/baza-nc-web-verification-flow';
import { DwollaClearPurchaseState } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { DwollaClearVerificationState } from '@scaliolabs/baza-dwolla-web-verification-flow';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less'],
})
export class HeaderComponent {
    user$ = this.store.select(AccountState.account);

    @Input()
    openedMenu: boolean;

    @Output()
    openedMenuChange = new EventEmitter<boolean>();

    constructor(private readonly store: Store, private router: Router) {}

    // onLogout
    onLogout(): void {
        this.store.dispatch(new ClearPurchaseState());
        this.store.dispatch(new ClearVerificationState());

        this.store.dispatch(new DwollaClearPurchaseState());
        this.store.dispatch(new DwollaClearVerificationState());
        this.store.dispatch(new Logout()).subscribe(() => this.router.navigate(['/']));
    }

    // onMenuToggle
    onMenuToggle(): void {
        this.openedMenuChange.emit(!this.openedMenu);
    }
}
