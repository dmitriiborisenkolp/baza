import { Component } from '@angular/core';

@Component({
    selector: 'app-header-maintenance',
    templateUrl: './header-maintenance.component.html',
    styleUrls: ['./header-maintenance.component.less'],
})
export class HeaderMaintenanceComponent {}
