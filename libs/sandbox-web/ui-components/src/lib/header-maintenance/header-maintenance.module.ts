import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { HeaderMaintenanceComponent } from './header-maintenance.component';

@NgModule({
    declarations: [HeaderMaintenanceComponent],
    exports: [HeaderMaintenanceComponent],
    imports: [CommonModule, NzLayoutModule, RouterModule],
})
export class HeaderMaintenanceModule {}
