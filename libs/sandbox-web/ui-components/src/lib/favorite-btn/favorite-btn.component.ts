import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationFavoriteExcludeRequest, BazaNcIntegrationFavoriteIncludeRequest } from '@scaliolabs/baza-nc-integration-shared';
import { Account, AccountState, AddFavorite, ItemDto, RemoveFavorite } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-favorite-btn',
    templateUrl: './favorite-btn.component.html',
    styleUrls: ['./favorite-btn.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FavoriteButtonComponent {
    user$ = this.store.select(AccountState.account);

    @Input()
    item: ItemDto;

    @Input()
    btnType: 'large' | 'normal' = 'normal';

    user: Account;

    constructor(private readonly router: Router, private readonly store: Store) {
        this.user$.pipe(untilDestroyed(this)).subscribe((user) => (this.user = user));
    }

    public onLikeClick(event): void {
        event.stopPropagation();
        event.preventDefault();

        if (!this.user) {
            this.router.navigate(['', { outlets: { modal: ['auth', 'login'] } }]);
        } else {
            if (this.item.isFavorite) {
                this.store.dispatch(new RemoveFavorite({ listingId: this.item.id } as BazaNcIntegrationFavoriteIncludeRequest));
            } else {
                this.store.dispatch(new AddFavorite({ listingId: this.item.id } as BazaNcIntegrationFavoriteExcludeRequest));
            }
        }
    }
}
