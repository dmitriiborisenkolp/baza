import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-password-validator',
    templateUrl: './password-validator.component.html',
    styleUrls: ['./password-validator.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PasswordValidatorComponent {
    @Input()
    errors: any;
}
