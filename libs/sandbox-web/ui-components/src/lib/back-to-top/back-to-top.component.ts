import { Component, ViewEncapsulation } from '@angular/core';
@Component({
    selector: 'app-back-to-top',
    templateUrl: './back-to-top.component.html',
    styleUrls: ['./back-to-top.component.less'],
    encapsulation: ViewEncapsulation.None,
})
export class BackToTopComponent {}
