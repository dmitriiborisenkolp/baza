import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RetryNcPaymentComponent } from './retry-nc-payment.component';
import { RetryNcSummaryComponent } from './retry-nc-summary/retry-nc-summary.component';
import { RetryNcListComponent } from './retry-nc-list/retry-nc-list.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NcPaymentEditModule } from '@scaliolabs/baza-nc-web-purchase-flow';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { PaymentHeaderModule, PaymentRadioModule } from '@scaliolabs/baza-web-ui-components';

const COMPONENTS = [RetryNcPaymentComponent, RetryNcSummaryComponent, RetryNcListComponent];

const NZ_MODULES = [NzModalModule, NzButtonModule, NzCollapseModule, NzToolTipModule, NzPopoverModule];

@NgModule({
    declarations: [...COMPONENTS],
    imports: [CommonModule, RouterModule, PaymentHeaderModule, PaymentRadioModule, NcPaymentEditModule, UtilModule, ...NZ_MODULES],
    exports: [...COMPONENTS],
})
export class RetryNcPaymentModule {}
