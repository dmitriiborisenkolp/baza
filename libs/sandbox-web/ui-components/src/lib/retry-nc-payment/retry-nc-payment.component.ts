import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcBootstrapDto, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { GetLimits, LoadBankAccount, LoadCreditCard, PaymentMethodType, PurchaseState } from '@scaliolabs/baza-nc-web-purchase-flow';
import { TransactionUnifiedDto } from '@scaliolabs/sandbox-web/data-access';
import { BehaviorSubject, combineLatest, debounceTime, distinctUntilChanged, map, skipWhile } from 'rxjs';

@UntilDestroy()
@Component({
    selector: 'app-retry-nc-payment',
    templateUrl: './retry-nc-payment.component.html',
    styleUrls: ['./retry-nc-payment.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RetryNcPaymentComponent implements OnInit {
    @Input()
    set initData(value: BazaNcBootstrapDto) {
        this.initData$.next(value);
    }
    private initData$: BehaviorSubject<BazaNcBootstrapDto> = new BehaviorSubject<BazaNcBootstrapDto>(null);

    bankAccount$ = this.store.select(PurchaseState.bankAccount);
    creditCard$ = this.store.select(PurchaseState.creditCard);
    limits$ = this.store.select(PurchaseState.limits);

    @Input() isVisible!: boolean;
    @Input() transaction!: TransactionUnifiedDto;

    @Output() isVisibleChange = new EventEmitter<boolean>();
    @Output() retry = new EventEmitter<BazaNcPurchaseFlowTransactionType>();

    selectedPaymentMethod!: PaymentMethodType | null;

    isPurchaseAboveLimit = false;

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.store.dispatch(new LoadBankAccount());
        this.store.dispatch(new LoadCreditCard());
        this.store.dispatch(new GetLimits());

        combineLatest([this.initData$, this.bankAccount$, this.creditCard$])
            .pipe(
                untilDestroyed(this),
                map(([initData, bank, card]) => ({
                    initData,
                    bank,
                    card,
                })),
                skipWhile((pair) => !pair?.initData || !pair?.bank || !pair?.card),
                distinctUntilChanged(),
                debounceTime(0),
            )
            .subscribe((pair) => {
                const isForeignInvestor = pair?.initData?.investorAccount?.status?.isForeign ?? true;
                const isBankAvailable = pair?.bank?.isAvailable;
                const isCardAvailable = pair?.card?.isAvailable;

                if (!this.selectedPaymentMethod) {
                    this.selectedPaymentMethod =
                        isBankAvailable && !isForeignInvestor
                            ? PaymentMethodType.bankAccount
                            : isCardAvailable
                            ? PaymentMethodType.creditCard
                            : null;
                }
            });
    }

    onClosed() {
        this.isVisibleChange.emit(false);
    }

    onPaymentMethodChange(paymentMethod: PaymentMethodType) {
        this.selectedPaymentMethod = paymentMethod;
    }

    onPay() {
        const transactionTypes: Record<PaymentMethodType, BazaNcPurchaseFlowTransactionType> = {
            bankAccount: BazaNcPurchaseFlowTransactionType.ACH,
            creditCard: BazaNcPurchaseFlowTransactionType.CreditCard,
        };

        this.retry.emit(transactionTypes[this.selectedPaymentMethod]);
    }
}
