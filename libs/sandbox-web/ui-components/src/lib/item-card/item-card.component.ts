import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ItemDto } from '@scaliolabs/sandbox-web/data-access';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';

@Component({
    selector: 'app-item-card',
    templateUrl: './item-card.component.html',
    styleUrls: ['./item-card.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemCardComponent {
    @Input()
    item?: ItemDto;

    public OFFERING_STATUS = BazaNcOfferingStatus;

    formatNoText = () => ``;
    triggerTooltip = false;

    truncated(index: number) {
        this.triggerTooltip = index !== null;
    }
}
