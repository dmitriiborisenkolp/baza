import { Component, Input, ViewChild } from '@angular/core';
import { NzCarouselComponent } from 'ng-zorro-antd/carousel';
import { NzImageService } from 'ng-zorro-antd/image';

@Component({
    selector: 'app-image-carousel',
    templateUrl: './image-carousel.component.html',
    styleUrls: ['./image-carousel.component.less'],
})
export class ImageCarouselComponent {
    @Input() imageSources: string[] = [];

    @ViewChild('carousel') private carousel: NzCarouselComponent;

    imageIndex = 1;

    constructor(private nzImageService: NzImageService) {}

    openImageGallery(index: number): void {
        const nzImages = this.imageSources.map((src) => ({ src }));

        this.nzImageService.preview(nzImages, { nzMaskClosable: true }).switchTo(index);
    }

    updateImageIndex(index: number): void {
        this.imageIndex = ++index;
    }
}
