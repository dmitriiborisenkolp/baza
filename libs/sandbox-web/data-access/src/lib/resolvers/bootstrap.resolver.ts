import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcBootstrapDto } from '@scaliolabs/baza-nc-shared';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { BootstrapState, RequestAppBootstrapInitData } from '../store';

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class BootstrapResolver implements Resolve<BazaNcBootstrapDto> {
    constructor(private readonly store: Store) {}

    resolve(): Observable<BazaNcBootstrapDto> {
        const result$ = this.store.select(BootstrapState.initData).pipe(
            untilDestroyed(this),
            take(1),
            switchMap((response) => {
                if (!response) {
                    return this.store.dispatch(new RequestAppBootstrapInitData()).pipe(
                        switchMap(() => {
                            return of(this.store.selectSnapshot(BootstrapState.initData));
                        }),
                    );
                } else {
                    return of(response);
                }
            }),
        );

        return result$.pipe(
            map((response) => {
                return response;
            }),
        );
    }
}
