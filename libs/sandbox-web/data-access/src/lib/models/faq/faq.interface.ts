import { FaqDto } from '@scaliolabs/baza-content-types-shared';

export type Faq = FaqDto;
