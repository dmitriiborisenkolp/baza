import { AccountDto } from '@scaliolabs/baza-core-shared';

export type Account = AccountDto;
