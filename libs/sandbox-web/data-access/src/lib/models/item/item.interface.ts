import { BazaNcIntegrationListingsDto, BazaNcIntegrationSchemaDto } from '@scaliolabs/baza-nc-integration-shared';
import { CrudListPaginatorDto } from '@scaliolabs/baza-core-shared';

export type Paginator = CrudListPaginatorDto;
export type ItemDto = BazaNcIntegrationListingsDto;
export type SchemaDto = BazaNcIntegrationSchemaDto;

export interface ItemDetail {
    title: string;
    value: string;
}
