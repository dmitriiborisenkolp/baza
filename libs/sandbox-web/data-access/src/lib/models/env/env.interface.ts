import { BazaWebBundleEnvironment } from '@scaliolabs/baza-core-web';

export interface Environment extends BazaWebBundleEnvironment {}
