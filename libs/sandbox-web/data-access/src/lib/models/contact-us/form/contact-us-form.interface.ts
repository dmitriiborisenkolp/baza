import { FormGroup, AbstractControl } from '@angular/forms';

export interface ContactUsForm extends FormGroup {
    controls: {
        fullName: AbstractControl;
        email: AbstractControl;
        subject: AbstractControl;
        message: AbstractControl;
    };
    values: {
        fullName: string;
        email: string;
        subject: string;
        message: string;
    };
}
