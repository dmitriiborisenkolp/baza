export interface ConfirmEmailData {
    token: string;
}

export interface LoginData {
    email: string;
    password: string;
}

export interface DeactivateAccountData {
    token: string;
}

export interface RegisterAccountData {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
}

export interface ResetPasswordData {
    token: string;
    newPassword: string;
    confirmPassword?: string;
}

export interface RestorePasswordData {
    email: string;
}

export interface SendDeactivateAccountLinkData {
    password: string;
}

export interface UpdateAccountData {
    firstName: string;
    lastName: string;
}

export interface UpdatePasswordData {
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
}
