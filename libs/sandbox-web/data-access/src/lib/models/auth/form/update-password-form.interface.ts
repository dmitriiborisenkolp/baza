import { AbstractControl, FormGroup } from '@angular/forms';

// Update Password form interface
export interface UpdatePasswordForm extends FormGroup {
    controls: {
        oldPassword: AbstractControl;
        newPassword: AbstractControl;
        password: AbstractControl;
    };
}
