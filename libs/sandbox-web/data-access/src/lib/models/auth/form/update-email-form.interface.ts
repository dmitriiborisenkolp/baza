import { AbstractControl, FormGroup } from '@angular/forms';

// Update Email form interface
export interface UpdateEmailForm extends FormGroup {
    controls: {
        password: AbstractControl;
        newEmail: AbstractControl;
        confirmEmail: AbstractControl;
    };
}
