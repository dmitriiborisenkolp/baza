import { AbstractControl, FormGroup } from '@angular/forms';

// Restore password form interface
export interface RestorePasswordForm extends FormGroup {
    controls: {
        email: AbstractControl;
    };
}
