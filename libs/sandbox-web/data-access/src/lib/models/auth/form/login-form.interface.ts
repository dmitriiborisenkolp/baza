import { AbstractControl, FormGroup } from '@angular/forms';

// Login form interface
export interface LoginForm extends FormGroup {
    controls: {
        email: AbstractControl;
        password: AbstractControl;
    };
}
