import { AbstractControl, FormGroup } from '@angular/forms';

// Reset password form interface
export interface ResetPasswordForm extends FormGroup {
    controls: {
        password: AbstractControl;
        confirmPassword: AbstractControl;
    };
}
