import { AbstractControl, FormGroup } from '@angular/forms';

// SignUp form interface
export interface SignUpForm extends FormGroup {
    controls: {
        firstName: AbstractControl;
        lastName: AbstractControl;
        email: AbstractControl;
        password: AbstractControl;
        confirmPassword: AbstractControl;
    };
}
