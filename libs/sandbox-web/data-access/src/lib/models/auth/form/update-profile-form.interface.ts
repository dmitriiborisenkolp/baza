import { AbstractControl, FormGroup } from '@angular/forms';

// Update Profile form interface
export interface UpdateProfileForm extends FormGroup {
    controls: {
        firstName: AbstractControl;
        lastName: AbstractControl;
    };
}
