export const CART_IS_EMPTY = 'CART_IS_EMPTY';
export const NAVIGATION_CANCELING_ERROR = 'ngNavigationCancelingError';

export function makeCancelingError(error: Error) {
    error[`${NAVIGATION_CANCELING_ERROR}`] = true;
    return error;
}
