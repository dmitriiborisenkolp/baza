import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable, of, throwError } from 'rxjs';
import { flatMap, skipWhile } from 'rxjs/operators';
import { DwollaPurchaseState, DwollaStartCart } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { CART_IS_EMPTY, makeCancelingError } from './cart.constants';

@Injectable()
export class DwollaCartGuard implements CanActivate {
    constructor(private readonly store: Store) {
        this.store.dispatch(new DwollaStartCart());
    }

    canActivate(): Observable<boolean> {
        return this.store.select(DwollaPurchaseState.cart).pipe(
            skipWhile((p) => p === undefined),
            flatMap((response) => {
                return response !== null ? of(true) : throwError(makeCancelingError(new Error(CART_IS_EMPTY)));
            }),
        );
    }
}
