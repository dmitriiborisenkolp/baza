export const LOAD_ASSETS_FAIL = 'Error occured while loading of portfolio-assets.';
export const LOAD_TRANSACTIONS_FAIL = 'Error occured while loading of portfolio-transactions.';
export const LOAD_STATEMENTS_FAIL = 'Error occured while loading of portfolio-statements.';
export const LOAD_TOTALS_FAIL = 'Error occured while loading of portfolio-totals.';
