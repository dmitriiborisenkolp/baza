import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { BazaNcIntegrationPortfolioDataAccessModule } from '@scaliolabs/baza-nc-integration-data-access';
import { PortfolioState } from './state';

@NgModule({
    imports: [NgxsModule.forFeature([PortfolioState]), BazaNcIntegrationPortfolioDataAccessModule],
})
export class PortfolioStateModule {}
