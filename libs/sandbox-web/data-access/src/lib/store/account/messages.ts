export const DEACTIVATE_ACCOUNT_FAIL = 'Something went wrong, please try again';
export const DEACTIVATE_ACCOUNT_SUCCESS = 'Your account was successfully deleted!';
export const DEACTIVATE_ACCOUNT_TOKEN_NOT_FOUND =
    'The link you used did not contain a token. Please click on the deletion link in your email to delete your account, or proceed to your account to request a new deletion email.';
export const RESEND_CONFIRMATION_SUCCESS = "Check your inbox for our verification email, once you're verified you can buy shares!";
