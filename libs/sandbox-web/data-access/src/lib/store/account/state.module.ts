import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { BazaAccountDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { AccountState } from './state';

@NgModule({
    imports: [NgxsModule.forFeature([AccountState]), BazaAccountDataAccessModule],
})
export class AccountStateModule {}
