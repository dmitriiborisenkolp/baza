import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { BazaNcDataAccessModule } from '@scaliolabs/baza-nc-data-access';
import { BootstrapState } from './state';

@NgModule({
    imports: [NgxsModule.forFeature([BootstrapState]), BazaNcDataAccessModule],
})
export class BootstrapStateModule {}
