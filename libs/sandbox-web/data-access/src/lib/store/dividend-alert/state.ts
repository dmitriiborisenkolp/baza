import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { BazaNcPurchaseFlowGetCreditCardResponse, PurchaseFlowBankAccountDto } from '@scaliolabs/baza-nc-shared';
import { LoadBankAccount, LoadCreditCard, PurchaseState } from '@scaliolabs/baza-nc-web-purchase-flow';
import { concat } from 'lodash';
import { EMPTY } from 'rxjs';
import { Account } from '../../models';
import { AccountState } from '../account';
import { GetDividendAlert } from './actions';

@State({
    name: 'DividentAlert',
})
@Injectable()
export class DividendAlertState {
    @Selector([AccountState.account, PurchaseState.creditCard, PurchaseState.bankAccount])
    static show(
        _: StateContext<unknown>,
        account: Account,
        cardDetails: BazaNcPurchaseFlowGetCreditCardResponse,
        bankDetails: PurchaseFlowBankAccountDto,
    ): boolean {
        return (account && cardDetails?.isAvailable && !bankDetails?.isAvailable) ?? false;
    }

    constructor(private readonly store: Store) {}

    @Action(GetDividendAlert, { cancelUncompleted: true })
    getDividendDetails() {
        const account = this.store.selectSnapshot(AccountState.account);
        return account ? concat(this.store.dispatch(new LoadBankAccount()), this.store.dispatch(new LoadCreditCard())) : EMPTY;
    }
}
