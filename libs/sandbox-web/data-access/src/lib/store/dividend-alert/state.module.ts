import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { DividendAlertState } from './state';

@NgModule({
    imports: [NgxsModule.forFeature([DividendAlertState])],
})
export class DividendAlertStateModule {}
