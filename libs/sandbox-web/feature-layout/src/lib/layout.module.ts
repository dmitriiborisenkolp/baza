import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { BackToTopModule, DividendAlertModule, FooterMiniModule, FooterModule, HeaderModule } from '@scaliolabs/sandbox-web/ui-components';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { LayoutComponent } from './layout.component';

@NgModule({
    declarations: [LayoutComponent],
    imports: [
        CommonModule,
        FooterMiniModule,
        FooterModule,
        HeaderModule,
        DividendAlertModule,
        NzLayoutModule,
        ReactiveFormsModule,
        RouterModule,
        UtilModule,
        BackToTopModule,
    ],
})
export class LayoutModule {}
