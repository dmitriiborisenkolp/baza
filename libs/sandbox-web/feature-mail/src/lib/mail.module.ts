import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { AuthStateModule, AccountStateModule } from '@scaliolabs/sandbox-web/data-access';
import { PasswordValidatorModule } from '@scaliolabs/sandbox-web/ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { MailRoutingModule } from './mail-routing.module';
import { MailComponent } from './mail.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { DeactivateAccountComponent } from './deactivate-account/deactivate-account.component';

@NgModule({
    declarations: [MailComponent, ConfirmEmailComponent, ResetPasswordComponent, DeactivateAccountComponent],
    imports: [
        AccountStateModule,
        AuthStateModule,
        CommonModule,
        MailRoutingModule,
        NzAlertModule,
        NzButtonModule,
        NzCardModule,
        NzFormModule,
        NzInputModule,
        NzLayoutModule,
        PasswordValidatorModule,
        ReactiveFormsModule,
        UtilModule,
    ],
})
export class MailModule {}
