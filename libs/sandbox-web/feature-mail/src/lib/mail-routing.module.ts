import { NgModule } from '@angular/core';
import { Routes, RouterModule, NavigationEnd, Router } from '@angular/router';
import { MetaGuard } from '@ngx-meta/core';
import { MailComponent } from './mail.component';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { DeactivateAccountComponent } from './deactivate-account/deactivate-account.component';
import { JwtRequireNoAuthGuard } from '@scaliolabs/baza-core-ng';

const routes: Routes = [
    {
        path: '',
        component: MailComponent,
        canActivateChild: [MetaGuard],
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: '/(modal:auth/login)',
            },
            {
                path: 'reset-password',
                component: ResetPasswordComponent,
                canActivate: [JwtRequireNoAuthGuard],
                data: {
                    meta: {
                        title: 'Reset Password',
                    },
                },
            },
            {
                path: 'confirm-email',
                component: ConfirmEmailComponent,
                canActivate: [JwtRequireNoAuthGuard],
                data: {
                    meta: {
                        title: 'Confirm Email',
                    },
                },
            },
            {
                path: 'deactivate-account',
                component: DeactivateAccountComponent,
                data: {
                    meta: {
                        title: 'Deactivate Account',
                    },
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MailRoutingModule {}
