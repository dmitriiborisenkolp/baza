import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { mapTo, take, tap } from 'rxjs/operators';
import { ResetPassword, ResetPasswordForm } from '@scaliolabs/sandbox-web/data-access';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
    catchErrorMessage,
    getErrorMessage,
    lowercaseValidator,
    Message,
    mismatchValidator,
    numberValidator,
    specialSymbolValidator,
    uppercaseValidator,
} from '@scaliolabs/baza-web-utils';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';

enum State {
    Loading,
    Success,
    Error,
}

@UntilDestroy()
@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResetPasswordComponent {
    message$: Observable<Message | undefined>;
    resetPasswordForm: ResetPasswordForm;
    token: string;

    state$ = new BehaviorSubject<State>(State.Loading);
    states = State;

    constructor(
        private readonly fb: FormBuilder,
        private readonly route: ActivatedRoute,
        private readonly router: Router,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
    ) {
        this.onFormReset();
        this.route.queryParams.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.token = params.token;

            if (!this.token || this.token === '') {
                this.message$ = of(getErrorMessage('Reset Password token not found!'));
                this.resetPasswordForm.disable();
            }
        });
    }

    onFormReset(): void {
        this.resetPasswordForm = this.fb.group(
            {
                password: [
                    '',
                    Validators.compose([
                        Validators.required,
                        Validators.minLength(8),
                        lowercaseValidator(),
                        numberValidator(),
                        specialSymbolValidator(),
                        uppercaseValidator(),
                    ]),
                ],
                confirmPassword: ['', Validators.compose([Validators.required])],
            },
            {
                validators: mismatchValidator('password', 'confirmPassword'),
            },
        ) as ResetPasswordForm;
    }

    onFormSubmit(formEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.resetPasswordForm, formEl)) {
            const data = {
                token: this.token,
                newPassword: this.resetPasswordForm.value.password,
                confirmPassword: this.resetPasswordForm.value.confirmPassword,
            };

            this.message$ = this.store.dispatch(new ResetPassword(data)).pipe(
                take(1),
                tap(() => {
                    this.state$.next(State.Success);
                }),
                mapTo(undefined),
                catchErrorMessage,
            );
        }
    }

    onSignin() {
        this.router.navigate(['/']).then(() => this.router.navigate(['.', { outlets: { modal: ['auth', 'login'] } }]));
    }
}
