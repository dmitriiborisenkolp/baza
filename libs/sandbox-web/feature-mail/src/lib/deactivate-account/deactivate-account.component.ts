import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { BehaviorSubject, EMPTY, Observable, of, throwError } from 'rxjs';
import { catchError, mapTo, mergeMap, pluck, take, tap } from 'rxjs/operators';
import { UntilDestroy } from '@ngneat/until-destroy';
import { getErrorMessage, Message } from '@scaliolabs/baza-web-utils';
import { DEACTIVATE_ACCOUNT_TOKEN_NOT_FOUND, DeactivateAccount, Logout } from '@scaliolabs/sandbox-web/data-access';

enum State {
    Loading,
    Success,
    Error,
}

@UntilDestroy()
@Component({
    selector: 'app-deactivate-account',
    templateUrl: './deactivate-account.component.html',
    styleUrls: ['./deactivate-account.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeactivateAccountComponent {
    public message$: Observable<Message | undefined>;
    public state$ = new BehaviorSubject<State>(State.Loading);
    public states = State;
    public token: string;

    constructor(private readonly route: ActivatedRoute, private readonly store: Store) {
        this.route.queryParams
            .pipe(
                take(1),
                pluck('token'),
                mergeMap(this.checkForToken),
                mergeMap(this.onDeactivateAccountConfirm),
                tap(() => {
                    this.state$.next(State.Success);
                }),
                catchError((error: Error) => {
                    this.state$.next(State.Error);
                    this.message$ = of(getErrorMessage(error));
                    return EMPTY;
                }),
            )
            .subscribe(() => {
                this.store.dispatch(new Logout());
            });
    }

    // on Not Existing Toke
    private checkForToken = (token: string) => {
        if (!token) {
            return throwError(new Error(DEACTIVATE_ACCOUNT_TOKEN_NOT_FOUND));
        } else {
            return of(token);
        }
    };

    // on Email Confirm
    private onDeactivateAccountConfirm = (token: string) =>
        this.store.dispatch(new DeactivateAccount({ token })).pipe(take(1), mapTo(undefined));
}
