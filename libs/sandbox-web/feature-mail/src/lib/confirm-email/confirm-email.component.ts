import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { BehaviorSubject, EMPTY, Observable, of, throwError } from 'rxjs';
import { catchError, mergeMap, pluck, take, tap } from 'rxjs/operators';
import { UntilDestroy } from '@ngneat/until-destroy';
import { ConfirmEmail } from '@scaliolabs/sandbox-web/data-access';
import { getErrorMessage, Message } from '@scaliolabs/baza-web-utils';

enum State {
    Loading,
    Success,
    Error,
}

@UntilDestroy()
@Component({
    selector: 'app-reset-password',
    templateUrl: './confirm-email.component.html',
    styleUrls: ['./confirm-email.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmEmailComponent implements OnDestroy {
    message$: Observable<Message | undefined>;
    state$ = new BehaviorSubject<State>(State.Loading);
    states = State;

    constructor(private readonly route: ActivatedRoute, private readonly store: Store, private readonly router: Router) {
        this.route.queryParams
            .pipe(
                take(1),
                pluck('token'),
                mergeMap(this.checkForToken),
                mergeMap(this.confirmEmail),
                tap(() => {
                    this.state$.next(State.Success);
                }),
                catchError((error: Error) => {
                    this.state$.next(State.Error);
                    this.message$ = of(getErrorMessage(error));
                    return EMPTY;
                }),
            )
            .subscribe();
    }

    // eslint-disable-next-line @angular-eslint/no-empty-lifecycle-method,@typescript-eslint/no-empty-function
    ngOnDestroy(): void {}

    processSignInClick() {
        this.router.navigate(['/items']).then(() => this.router.navigate(['.', { outlets: { modal: ['auth', 'login'] } }]));
    }

    private checkForToken = (token: string) => {
        if (!token) {
            return throwError(
                new Error(
                    'The link you used did not contain a token. Please click on the activation link in your email to activate your account, or proceed to login to request a new activation email.',
                ),
            );
        } else {
            return of(token);
        }
    };

    private confirmEmail = (token: string) => this.store.dispatch(new ConfirmEmail({ token }));
}
