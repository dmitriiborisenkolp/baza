import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { TextImageConfig, TextPlacement } from '@scaliolabs/baza-web-ui-components';

@UntilDestroy()
@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
    txtImgS2Config: TextImageConfig = {
        img: `'/assets/images/section2.jpeg'`,
        txtPlacement: TextPlacement.Right,
    };
}
