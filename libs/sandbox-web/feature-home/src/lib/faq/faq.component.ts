import { Component, OnInit } from '@angular/core';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular/ckeditor';
import { Store } from '@ngxs/store';
import { FaqState, GetFaq } from '@scaliolabs/sandbox-web/data-access';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.less'],
})
export class HomeFaqComponent implements OnInit {
    editor: CKEditor5.EditorConstructor;
    isEditorDisplayed = false;

    faq$ = this.store.select(FaqState.items);

    constructor(private store: Store) {}

    ngOnInit(): void {
        this.store.dispatch(new GetFaq());
    }
}
