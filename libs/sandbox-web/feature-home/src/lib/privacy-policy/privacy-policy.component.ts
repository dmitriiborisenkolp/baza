import { Component } from '@angular/core';
import { BazaRegistryNgService } from '@scaliolabs/baza-core-ng';

@Component({
    selector: 'app-privacy-policy',
    templateUrl: './privacy-policy.component.html',
    styleUrls: ['./privacy-policy.component.less'],
})
export class HomePrivacyPolicyComponent {
    constructor(private readonly registry: BazaRegistryNgService) {}

    public get contactEmail(): string {
        return this.registry.value('bazaContentTypes.contacts.email');
    }
}
