import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeEftDisclosureComponent } from './eft-disclosure/eft-disclosure.component';
import { HomeFaqComponent } from './faq/faq.component';
import { HomeComponent } from './home.component';
import { HomePrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { HomeTermsOfServiceComponent } from './terms-of-service/terms-of-service.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                component: HomeComponent,
            },
            {
                path: 'eft-disclosure',
                component: HomeEftDisclosureComponent,
                data: {
                    meta: {
                        title: 'EFT Disclosure',
                    },
                    wrapper: true,
                },
            },
            {
                path: 'faq',
                component: HomeFaqComponent,
                data: {
                    meta: {
                        title: 'Frequently Asked Questions',
                        description:
                            'Questions about BAZA Investment Platform? Please let us know through the support portal if you have any questions not answered here.',
                    },
                    wrapper: true,
                },
            },
            {
                path: 'privacy-policy',
                component: HomePrivacyPolicyComponent,
                data: {
                    meta: {
                        title: 'Privacy Policy',
                    },
                    wrapper: true,
                },
            },
            {
                path: 'terms-of-service',
                component: HomeTermsOfServiceComponent,
                data: {
                    meta: {
                        title: 'Terms of Service',
                    },
                    wrapper: true,
                },
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomeRoutingModule {}
