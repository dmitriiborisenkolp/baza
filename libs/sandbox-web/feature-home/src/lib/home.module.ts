import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { FaqStateModule } from '@scaliolabs/sandbox-web/data-access';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { HomeComponent } from './home.component';
import { HomeEftDisclosureComponent } from './eft-disclosure/eft-disclosure.component';
import { HomeFaqComponent } from './faq/faq.component';
import { HomePrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { HomeRoutingModule } from './home-routing.module';
import { HomeTermsOfServiceComponent } from './terms-of-service/terms-of-service.component';

import {
    HeroModule,
    TextImgModule,
    BulletsTextModule,
    BulletsImageModule,
    TextSectionModule,
    BannerModule,
    WhyInvestModule,
    HowItWorksModule,
    CTAModule,
} from '@scaliolabs/baza-web-ui-components';
const marketingModules = [
    HeroModule,
    TextImgModule,
    BulletsTextModule,
    BulletsImageModule,
    TextSectionModule,
    BannerModule,
    WhyInvestModule,
    HowItWorksModule,
    CTAModule,
];

@NgModule({
    declarations: [HomeComponent, HomeEftDisclosureComponent, HomeFaqComponent, HomePrivacyPolicyComponent, HomeTermsOfServiceComponent],
    imports: [CommonModule, FaqStateModule, HomeRoutingModule, NzGridModule, NzSkeletonModule, UtilModule, ...marketingModules],
})
export class HomeModule {}
