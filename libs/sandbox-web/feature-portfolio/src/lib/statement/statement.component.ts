import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { UntilDestroy } from '@ngneat/until-destroy';
import { GetStatements, PortfolioState } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-portfolio-statement',
    templateUrl: './statement.component.html',
    styleUrls: ['./statement.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioStatementComponent {
    statements$ = this.store.select(PortfolioState.statements);

    constructor(private readonly store: Store) {
        this.store.dispatch(new GetStatements());
    }
}
