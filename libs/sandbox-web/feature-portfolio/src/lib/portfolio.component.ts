import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetTotals, PortfolioState } from '@scaliolabs/sandbox-web/data-access';
import { Router } from '@angular/router';

@Component({
    selector: 'app-portfolio',
    templateUrl: './portfolio.component.html',
    styleUrls: ['./portfolio.component.less'],
})
export class PortfolioComponent {
    totals$ = this.store.select(PortfolioState.totals);

    constructor(private readonly store: Store, private router: Router) {
        this.store.dispatch(new GetTotals());
    }
}
