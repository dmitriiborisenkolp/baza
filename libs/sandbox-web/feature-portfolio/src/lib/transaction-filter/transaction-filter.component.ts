import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationPortfolioTransactionType, portfolioTransactionTableStates } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcDividendPaymentStatus, TransactionState } from '@scaliolabs/baza-nc-shared';
import { ChangeTransactionFilterBindings, DividendsStatusMapping, PortfolioState } from '@scaliolabs/sandbox-web/data-access';
import { DefaultFilterValue, TransactionFilter, TransactionFilterBindings } from '@scaliolabs/sandbox-web/data-access';
import { compact, concat, uniq } from 'lodash';
import { take } from 'rxjs';

@Component({
    selector: 'app-portfolio-transaction-filter',
    templateUrl: './transaction-filter.component.html',
    styleUrls: ['./transaction-filter.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioTransactionFilterComponent implements OnInit {
    transactionFilter: TransactionFilter = {
        scopes: [],
        states: null,
    };

    transactionFilterBindings: TransactionFilterBindings = {
        scope: DefaultFilterValue.All,
        state: DefaultFilterValue.All,
        asset: DefaultFilterValue.All,
        timePeriod: DefaultFilterValue.All,
    };

    scopeLabels: string[];
    stateLabels: string[];

    @Output()
    transactionFilterChange = new EventEmitter<Partial<TransactionFilter>>();

    preExistingFilterBindings$ = this.store.select(PortfolioState.transactionFilterBindings);

    mobileFilterApplied = false;
    isMobileFilterActive = false;
    mobileFilterHeading = 'Filters';
    filtersApplied = 0;

    constructor(private store: Store) {
        this.getFilterResources();
    }

    ngOnInit(): void {
        this.preExistingFilterBindings$.pipe(untilDestroyed(this), take(1)).subscribe((res) => {
            if (res) {
                this.transactionFilterBindings.state = res?.state ?? DefaultFilterValue.All;
                this.store.dispatch(new ChangeTransactionFilterBindings(null));
            }
            this.transactionFilterChanged();
        });
    }

    getFilterResources() {
        this.resetTransactionScopeLabels();
        this.resetTransactionStatusLabels();
    }

    transactionFilterChanged(valueChanged: string = null) {
        if (valueChanged === 'scope') {
            this.cascadeChangeState();
        }

        let transactionStates: TransactionState[];
        let dividendStates: BazaNcDividendPaymentStatus[];

        const scope = this.transactionFilterBindings.scope === DefaultFilterValue.All ? [] : this.getSelectedScope;
        this.transactionFilter.states = undefined;

        if (this.transactionFilterBindings.state !== DefaultFilterValue.All) {
            if (this.transactionFilterBindings.scope === DefaultFilterValue.All) {
                transactionStates = this.getTransactionStatesFromMapping();
                dividendStates = this.getDividendStatesFromMapping();
            } else {
                switch (this.getSelectedScope) {
                    case BazaNcIntegrationPortfolioTransactionType.Investment:
                        transactionStates = this.getTransactionStatesFromMapping();
                        break;

                    case BazaNcIntegrationPortfolioTransactionType.Dividends:
                        dividendStates = this.getDividendStatesFromMapping();
                        break;
                }
            }

            this.transactionFilter.states = compact([...(transactionStates ?? []), ...(dividendStates ?? [])]) as (
                | TransactionState
                | BazaNcDividendPaymentStatus
            )[];
        }

        this.transactionFilter.scopes = scope ? concat(scope) : undefined;
        if (!this.mobileFilterApplied) {
            this.transactionFilterChange.emit(this.transactionFilter);
        }
    }

    getTransactionStatesFromMapping() {
        return portfolioTransactionTableStates[this.transactionFilterBindings.state];
    }

    getDividendStatesFromMapping() {
        const dividendMappingKey = this.getEnumKeyByValue(DividendsStatusMapping, this.transactionFilterBindings.state);
        const dividendStateKey = this.getEnumKeyByValue(BazaNcDividendPaymentStatus, dividendMappingKey);
        return concat(BazaNcDividendPaymentStatus[dividendStateKey]);
    }

    cascadeChangeState() {
        const selectedTransactionScope = BazaNcIntegrationPortfolioTransactionType[this.transactionFilterBindings.scope];

        switch (selectedTransactionScope) {
            case BazaNcIntegrationPortfolioTransactionType.Investment:
                this.stateLabels = this.getTransactionStateLabels;
                break;

            case BazaNcIntegrationPortfolioTransactionType.Dividends:
                this.stateLabels = this.getDividendStateLabels;
                break;

            default:
                this.resetTransactionStatusLabels();
                break;
        }

        this.transactionFilterBindings.state = DefaultFilterValue.All;
    }

    resetTransactionScopeLabels() {
        this.scopeLabels = Object.values(this.getAllScopes);
    }

    resetTransactionStatusLabels() {
        this.stateLabels = uniq(concat(this.getTransactionStateLabels, this.getDividendStateLabels));
    }

    getEnumKeyByValue(enumValue: any, value: any) {
        const indexOfS = Object.values(enumValue).indexOf(value);
        return Object.keys(enumValue)[indexOfS];
    }

    get getAllScopes() {
        return [BazaNcIntegrationPortfolioTransactionType.Investment, BazaNcIntegrationPortfolioTransactionType.Dividends];
    }

    get getSelectedScope() {
        return BazaNcIntegrationPortfolioTransactionType[this.transactionFilterBindings.scope];
    }

    get getTransactionStateLabels() {
        return Object.keys(portfolioTransactionTableStates);
    }

    get getDividendStateLabels() {
        return Object.values(DividendsStatusMapping);
    }

    public applyFilters() {
        if (this.mobileFilterApplied) {
            this.filtersApplied = 0;

            Object.values(this.transactionFilterBindings).forEach((filter: string) => {
                if (filter !== DefaultFilterValue.All) {
                    this.filtersApplied++;
                }
            });

            this.mobileFilterHeading = this.filtersApplied === 0 ? 'Filters' : `Filters (${this.filtersApplied})`;
        }
        this.transactionFilterChange.emit(this.transactionFilter);
    }

    public collapseFilter() {
        this.isMobileFilterActive = false;
    }

    public onFilterActiveChange() {
        this.mobileFilterApplied = true;
        this.isMobileFilterActive = !this.isMobileFilterActive;
    }
}
