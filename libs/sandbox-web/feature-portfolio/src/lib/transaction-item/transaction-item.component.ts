import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import {
    BazaNcDividendPaymentSource,
    BazaNcDividendPaymentStatus,
    BazaNcOperationType,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';
import { Transaction, TransactionUnifiedDto } from '@scaliolabs/sandbox-web/data-access';
import { TransactionService } from '../transaction/transaction.service';
import { BazaDwollaPaymentStatus } from '@scaliolabs/baza-dwolla-shared';

@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: '[app-portfolio-transaction-item]',
    templateUrl: './transaction-item.component.html',
    styleUrls: ['./transaction-item.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioTransactionItemComponent {
    @Input() transaction: Transaction;
    @Input() isFirstRow = false;

    @Output() retryNcPayment = new EventEmitter<TransactionUnifiedDto>();

    PAYMENT_SOURCE = BazaNcDividendPaymentSource;

    get title(): string {
        return this.transactService.formatTitle(this.transaction);
    }

    get transactionState(): string {
        return this.transactService.mapStatusToClient(this.transaction.state);
    }

    get transactionStateClass(): string {
        switch (this.transaction.state) {
            case TransactionState.PendingPayment:
            case BazaDwollaPaymentStatus.Pending:
                return 'cell-pending';

            case TransactionState.PaymentCancelled:
            case TransactionState.PaymentRejected:
            case TransactionState.PaymentReturned:
            case BazaDwollaPaymentStatus.Cancelled:
            case BazaDwollaPaymentStatus.Failed:
                return 'cell-canceled';

            case TransactionState.UnwindSettled:
            case TransactionState.PaymentFunded:
            case BazaNcDividendPaymentStatus.Cancelled:
            case BazaNcDividendPaymentStatus.Failed:
                return 'cell-refunded';

            case TransactionState.PaymentConfirmed:
            case BazaNcDividendPaymentStatus.Processed:
            case BazaDwollaPaymentStatus.Processed:
                return 'cell-completed';

            case TransactionState.UnwindPending:
                return 'cell-pending-refund';

            default:
                return '';
        }
    }

    get transactionStateIcon(): string {
        switch (this.transaction.type) {
            // TODO: DWOLLA Integration, Status will be implemented once backend is done.
            // case BazaNcIntegrationPortfolioTransactionType.SellOrder:
            //     return 'dividend-sell';

            case BazaNcOperationType.Dividend:
                return 'dividend-claim';

            case BazaNcOperationType.Investment:
                return 'dividend-investment';

            case BazaNcOperationType.Transfer:
                return 'cash-in';

            case BazaNcOperationType.Withdraw:
                return 'cash-out';

            default:
                return 'dividend-investment';
        }
    }

    constructor(private transactService: TransactionService) {}

    retryPayment(source: BazaNcDividendPaymentSource) {
        const { type } = this.transaction;

        if (source === BazaNcDividendPaymentSource.NC && type === BazaNcOperationType.Investment) {
            this.retryNcPayment.emit(this.transaction);
        }
    }

    public isPaymentReturnedStatus(transaction: Transaction) {
        return transaction?.state === TransactionState.PaymentReturned;
    }
}
