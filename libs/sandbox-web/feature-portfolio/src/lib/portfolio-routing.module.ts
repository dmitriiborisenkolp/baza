import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortfolioAssetComponent } from './asset/asset.component';
import { PortfolioComponent } from './portfolio.component';
import { PortfolioStatementComponent } from './statement/statement.component';
import { PortfolioTransactionComponent } from './transaction/transaction.component';
import { BootstrapResolver } from '@scaliolabs/sandbox-web/data-access';

const routes: Routes = [
    {
        path: '',
        component: PortfolioComponent,
        data: {
            meta: {
                title: 'Portfolio',
            },
            wrapper: true,
        },
        children: [
            {
                path: '',
                redirectTo: 'assets',
                pathMatch: 'full',
            },
            {
                path: 'assets',
                component: PortfolioAssetComponent,
            },
            {
                path: 'transactions',
                component: PortfolioTransactionComponent,
                resolve: {
                    bootstrap: BootstrapResolver,
                },
            },
            {
                path: 'statements',
                component: PortfolioStatementComponent,
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PortfolioRoutingModule {}
