import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationPortfolioAssetDto, portfolioTransactionStates } from '@scaliolabs/baza-nc-integration-shared';
import { ChangeTransactionFilterBindings } from '@scaliolabs/sandbox-web/data-access';

@Component({
    selector: 'app-portfolio-asset-card',
    templateUrl: './asset-card.component.html',
    styleUrls: ['./asset-card.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioAssetCardComponent implements OnInit {
    @Input()
    asset: BazaNcIntegrationPortfolioAssetDto = null;

    imageCover: string;
    public transactionState = portfolioTransactionStates;

    public shareMapping: { [k: string]: string } = {
        '=0': 'No shares',
        '=1': '# share',
        other: '# shares',
    };

    triggerTooltip: boolean = false;
    visible = false;

    constructor(private router: Router, private readonly store: Store) {}

    ngOnInit(): void {
        this.setImageCover();
    }

    setImageCover(): void {
        const { cover } = this.asset.listing;

        this.imageCover = cover?.md || '/assets/images/property-placeholder.svg';
    }

    truncated(index: any) {
        this.triggerTooltip = index !== null;
    }

    navigateToTransactions() {
        this.visible = false;
        
        // filter by 'Failed' status by default on transaction filter after redirection
        const failedStateLabel = this.getObjectKeyByValue(portfolioTransactionStates, portfolioTransactionStates.Failed);
        this.store.dispatch(new ChangeTransactionFilterBindings({state: failedStateLabel}));
        
        this.router.navigateByUrl('portfolio/transactions');
    }

    getObjectKeyByValue(obj: any, value: any) {
        const indexOfS = Object.values(obj).indexOf(value);
        return Object.keys(obj)[indexOfS];
    }
}
