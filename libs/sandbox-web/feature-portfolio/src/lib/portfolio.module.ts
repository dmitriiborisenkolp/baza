import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzPaginationModule } from 'ng-zorro-antd/pagination';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NestedEllipsisModule } from 'ngx-nested-ellipsis';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PortfolioStateModule } from '@scaliolabs/sandbox-web/data-access';
import { PortfolioComponent } from './portfolio.component';
import { PortfolioAssetCardComponent } from './asset-card/asset-card.component';
import { PortfolioAssetComponent } from './asset/asset.component';
import { PortfolioRoutingModule } from './portfolio-routing.module';
import { PortfolioStatementComponent } from './statement/statement.component';
import { PortfolioTransactionComponent } from './transaction/transaction.component';
import { PortfolioTransactionItemComponent } from './transaction-item/transaction-item.component';
import { PortfolioTransactionFilterComponent } from './transaction-filter/transaction-filter.component';
import { RetryNcPaymentModule } from '@scaliolabs/sandbox-web/ui-components';
@NgModule({
    declarations: [
        PortfolioComponent,
        PortfolioAssetCardComponent,
        PortfolioAssetComponent,
        PortfolioStatementComponent,
        PortfolioTransactionComponent,
        PortfolioTransactionItemComponent,
        PortfolioTransactionFilterComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        NestedEllipsisModule,
        NzButtonModule,
        NzCardModule,
        NzDividerModule,
        NzEmptyModule,
        NzGridModule,
        NzPaginationModule,
        NzTableModule,
        NzTabsModule,
        NzTagModule,
        NzToolTipModule,
        NzPopoverModule,
        NzCollapseModule,
        NzSelectModule,
        PortfolioRoutingModule,
        PortfolioStateModule,
        ReactiveFormsModule,
        RetryNcPaymentModule,
        UtilModule,
    ],
})
export class PortfolioModule {}
