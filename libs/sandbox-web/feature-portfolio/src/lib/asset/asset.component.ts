import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { UntilDestroy } from '@ngneat/until-destroy';
import { GetAssets, PortfolioState } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-portfolio-asset',
    templateUrl: './asset.component.html',
    styleUrls: ['./asset.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioAssetComponent {
    assets$ = this.store.select(PortfolioState.assets);

    public investmentMapping: { [k: string]: string } = {
        '=0': 'No Investment',
        '=1': '# Investment',
        other: '# Investments',
    };

    constructor(private store: Store) {
        this.store.dispatch(new GetAssets());
    }
}
