import { Injectable } from '@angular/core';
import { DividendsStatusMapping, Transaction, TransactionStatusMappins } from '@scaliolabs/sandbox-web/data-access';
import { BazaNcOperationType } from '@scaliolabs/baza-nc-shared';

@Injectable({
    providedIn: 'root',
})
export class TransactionService {
    // TODO: CB:22Oct2021: TECH:DEBT: Some provided statuses are still missing from documentation:
    // TODO: CB:22Oct2021: https://www.notion.so/scalio/5cd15f99080f4958be0d53b47b22a93e?v=3c6727863b2d45509b312733f36c61bb
    // TODO: CB:22Oct2021: https://scalio.atlassian.net/browse/MIP-1883

    public mapStatusToClient(src: string): string {
        return TransactionStatusMappins[src] ?? DividendsStatusMapping[src] ?? 'Unknown';
    }

    formatTitle({ type, shares, name }: Transaction): string {
        switch (type) {
            case BazaNcOperationType.Investment:
                return `Purchased ${shares} shares of ${name}`;

            case BazaNcOperationType.Dividend:
                return `Dividend from ${name}`;

            case BazaNcOperationType.Transfer:
                return `Cash-in`;

            case BazaNcOperationType.Withdraw:
                return `Cash-out`;

            default:
                return '';
        }
    }
}
