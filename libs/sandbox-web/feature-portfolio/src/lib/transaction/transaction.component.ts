import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { ReprocessPayment } from '@scaliolabs/baza-nc-web-purchase-flow';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import {
    BootstrapState,
    ChangeTransactionFilter,
    ChangeTransactionsPage,
    PortfolioState,
    RequestAppBootstrapInitData,
    TransactionFilter,
    TransactionUnifiedDto,
} from '@scaliolabs/sandbox-web/data-access';
import { finalize } from 'rxjs';

@UntilDestroy()
@Component({
    selector: 'app-portfolio-transaction',
    templateUrl: './transaction.component.html',
    styleUrls: ['./transaction.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioTransactionComponent {
    initData$ = this.store.select(BootstrapState.initData);
    transactions$ = this.store.select(PortfolioState.transactions);
    paging$ = this.store.select(PortfolioState.transactionsPaging);
    transactionFilter: TransactionFilter;

    selectedTransaction!: TransactionUnifiedDto;
    isRetryPaymentMethodVisible = false;

    constructor(private store: Store, private readonly wts: BazaWebUtilSharedService, private readonly route: ActivatedRoute) {
        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }

    onPageSelect(index: number): void {
        window.scroll(0, 0);
        this.store.dispatch(new ChangeTransactionsPage(index));
    }

    onTransactionFilterChanged(filter: TransactionFilter) {
        this.transactionFilter = filter;
        this.getTransactions();
    }

    showNcRetryPopup(transaction: TransactionUnifiedDto) {
        this.selectedTransaction = transaction;
        this.isRetryPaymentMethodVisible = true;
    }

    retryPurchaseTransaction(transactionType: BazaNcPurchaseFlowTransactionType) {
        this.store
            .dispatch(
                new ReprocessPayment({
                    id: this.selectedTransaction.id,
                    transactionType,
                }),
            )
            .pipe(
                finalize(() => (this.isRetryPaymentMethodVisible = false)),
                untilDestroyed(this),
            )
            .subscribe(() => this.getTransactions());
    }

    getTransactions() {
        this.store.dispatch(new ChangeTransactionFilter(this.transactionFilter));
    }
}
