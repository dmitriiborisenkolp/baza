import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaDwollaWebVerificationFlowModule } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { VerificationRoutingCustomModule } from './verification-routing-custom.module';
import { VerificationCustomComponent } from './verification-custom/verification-custom.component';
import { VerificationStepperCustomComponent } from './verification-stepper-custom/verification-stepper-custom.component';
import { VerificationCheckCustomComponent } from './verification-check-custom/verification-check-custom.component';

@NgModule({
    declarations: [VerificationCustomComponent, VerificationStepperCustomComponent, VerificationCheckCustomComponent],
    imports: [CommonModule, BazaDwollaWebVerificationFlowModule, VerificationRoutingCustomModule],
})
export class VerificationModule {}
