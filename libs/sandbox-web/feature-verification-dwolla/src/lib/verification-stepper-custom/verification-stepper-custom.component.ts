import { ChangeDetectorRef, Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BootstrapState, RequestAppBootstrapInitData } from '@scaliolabs/sandbox-web/data-access';
import { tap } from 'rxjs';

@UntilDestroy()
@Component({
    selector: 'app-verification-stepper-custom',
    templateUrl: './verification-stepper-custom.component.html',
    styleUrls: ['./verification-stepper-custom.component.less'],
})
export class VerificationStepperCustomComponent {
    initData$ = this.store.select(BootstrapState.initData);
    proceedToNextStep = false;
    public currentTab: number;

    constructor(private readonly route: ActivatedRoute, private readonly store: Store, private readonly cdr: ChangeDetectorRef) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });
    }

    onMoveToNextStep() {
        this.store
            .dispatch(new RequestAppBootstrapInitData())
            .pipe(
                untilDestroyed(this),
                tap(() => {
                    this.proceedToNextStep = true;
                    this.cdr.markForCheck();
                }),
            )
            .subscribe();
    }
}
