import { Component } from '@angular/core';

@Component({
    selector: 'app-verification-check-custom',
    templateUrl: './verification-check-custom.component.html',
    styleUrls: ['./verification-check-custom.component.less'],
})
export class VerificationCheckCustomComponent {}
