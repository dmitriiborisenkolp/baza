import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { DwollaVFLinksConfig } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { RequestAppBootstrapInitData } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-verification-custom',
    templateUrl: './verification-custom.component.html',
    styleUrls: ['./verification-custom.component.less'],
})
export class VerificationCustomComponent {
    linksConfig: DwollaVFLinksConfig;

    constructor(private readonly route: ActivatedRoute, private readonly store: Store, private readonly wts: BazaWebUtilSharedService) {
        this.linksConfig = {
            buyShares: '/buy-shares-dwolla',
            verification: '/verification-dwolla',
        };

        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }
}
