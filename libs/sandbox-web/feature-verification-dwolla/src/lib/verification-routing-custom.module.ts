import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerificationStepperCustomComponent } from './verification-stepper-custom/verification-stepper-custom.component';
import { VerificationCheckCustomComponent } from './verification-check-custom/verification-check-custom.component';
import { VerificationCustomComponent } from './verification-custom/verification-custom.component';
import { getConfigurableRoutes } from '@scaliolabs/baza-dwolla-web-verification-flow';

const routes: Routes = getConfigurableRoutes(
    VerificationCustomComponent,
    VerificationCheckCustomComponent,
    VerificationStepperCustomComponent,
);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class VerificationRoutingCustomModule {}
