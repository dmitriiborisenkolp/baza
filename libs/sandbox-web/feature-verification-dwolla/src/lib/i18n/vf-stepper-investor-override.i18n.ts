export const DwollaVFInvestorEnI18nOverride = {
    title: 'Investor Profile - override',
    form: {
        fields: {
            isAccreditedInvestor: {
                label: 'Are you an accredited investor? - override',
                validators: {
                    required: 'Accreditor investor option is required - override',
                },
                yesOption: {
                    label: 'Yes - override',
                    descr: 'I have an annual income greater than $200k, joint income greater than $300k, or net worth greater than $1M (excluding primary residence) - override',
                },
                noOption: {
                    label: 'No - override',
                    descr: 'I acknowledge that my investment(s) in this offering is less than 10% of my net worth or annual gross income. - override',
                },
            },
            isAssociatedWithFINRA: {
                label: 'Are you or anyone in your household associated with a FINRA member, organization or the SEC? - override',
                validators: {
                    required: 'FINRA association option is required - override',
                },
                yesOption: {
                    label: 'Yes - override',
                    descr: 'I have an annual income greater than $200k, joint income greater than $300k, or net worth greater than $1M (excluding primary residence) - override',
                },
                noOption: {
                    label: 'No - override',
                    descr: 'I acknowledge that my investment(s) in this offering is less than 10% of my net worth or annual gross income. - override',
                },
            },
            dwollaConsentProvided: {
                validators: {
                    required: 'Please provide the Dwolla consent before proceeding - override',
                },
                descr: `By checking this box you agree with our <a data-link="tos"></a> and <a data-link="privacy"></a>, as well as our partner Dwolla's  <a data-link="dwTos"></a> and <a data-link="dwPrivacy"></a>.`,
                tosLinkConfig: {
                    key: 'tos',
                    text: 'Terms of Service - override',
                },
                privacyLinkConfig: {
                    key: 'privacy',
                    text: 'Privacy Policy - override',
                },
                dwTosLinkConfig: {
                    key: 'dwTos',
                    text: 'Terms of Service - override',
                },
                dwPrivacyLinkConfig: {
                    key: 'dwPrivacy',
                    text: 'Privacy Policy - override',
                },
            },
        },
    },
    steps: {
        backBtnLabel: 'Back - override',
        confirmBtnLabel: 'Confirm - override',
    },
};
