import { DwollaVFFileUploadEnI18nOverride } from './vf-file-upload-override.i18n';
import { DwollaVFInfoEnI18nOverride } from './vf-stepper-info-override.i18n';
import { DwollaVFInvestorEnI18nOverride } from './vf-stepper-investor-override.i18n';
import { DwollaVFEnI18nOverride } from './vf-override.18n';
import { DwollaVFCheckEnI18nOverride } from './vf-check-override.i18n';

// override translations can be provided optionally
export const bazaDwollaWebVFEnI18nOverride = {
    dwvf: {
        vf: DwollaVFEnI18nOverride,
        check: DwollaVFCheckEnI18nOverride,
        info: DwollaVFInfoEnI18nOverride,
        investor: DwollaVFInvestorEnI18nOverride,
        fileUpload: DwollaVFFileUploadEnI18nOverride,
    },
};
