import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { HeaderMaintenanceModule } from '@scaliolabs/sandbox-web/ui-components';

import { MaintenanceComponent } from './maintenance.component';

const routes: Routes = [
    {
        path: '',
        component: MaintenanceComponent,
        data: {
            meta: {
                title: 'Maintenance',
            },
        },
    },
];

@NgModule({
    declarations: [MaintenanceComponent],
    imports: [CommonModule, HeaderMaintenanceModule, NzButtonModule, NzEmptyModule, RouterModule.forChild(routes), UtilModule],
})
export class MaintenanceModule {}
