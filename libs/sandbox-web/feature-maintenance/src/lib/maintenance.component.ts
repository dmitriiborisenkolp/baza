import { Component, OnInit } from '@angular/core';
import { Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { RequestAppBootstrapInitData } from '@scaliolabs/sandbox-web/data-access';

@UntilDestroy()
@Component({
    selector: 'app-maintenance',
    templateUrl: './maintenance.component.html',
    styleUrls: ['./maintenance.component.less'],
})
export class MaintenanceComponent implements OnInit {
    maintenanceMessage$ = new Subject<string | undefined>();

    constructor(private readonly store: Store) {}

    ngOnInit() {
        this.store
            .dispatch(new RequestAppBootstrapInitData())
            .pipe(
                catchError((err) => {
                    this.maintenanceMessage$.next(err.message);

                    return throwError(err);
                }),
                untilDestroyed(this),
            )
            .subscribe();
    }
}
