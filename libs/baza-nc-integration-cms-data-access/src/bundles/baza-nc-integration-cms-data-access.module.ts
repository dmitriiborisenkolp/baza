import { NgModule } from '@angular/core';
import { BazaNcIntegrationEliteInvestorsCmsDataAccessModule } from '../libs/elite-investors/src';
import { BazaNcIntegrationFeedCmsDataAccessModule } from '../libs/feed/src';
import { BazaNcIntegrationListingsCmsDataAccessModule } from '../libs/listings/src';
import { BazaNcIntegrationPerkCmsDataAccessModule } from '../libs/perk/src';
import { BazaNcIntegrationSchemaCmsDataAccessModule } from '../libs/schema/src';
import { BazaNcIntegrationSearchCmsDataAccessModule } from '../libs/search/src';
import { BazaNcIntegrationSubscriptionCmsDataAccessModule } from '../libs/subscription/src';
import { BazaNcIntegrationListingTestimonialCmsDataAccessModule } from '../libs/testimonial/src';

@NgModule({
    imports: [
        BazaNcIntegrationEliteInvestorsCmsDataAccessModule,
        BazaNcIntegrationFeedCmsDataAccessModule,
        BazaNcIntegrationListingsCmsDataAccessModule,
        BazaNcIntegrationPerkCmsDataAccessModule,
        BazaNcIntegrationSchemaCmsDataAccessModule,
        BazaNcIntegrationSearchCmsDataAccessModule,
        BazaNcIntegrationSubscriptionCmsDataAccessModule,
        BazaNcIntegrationListingTestimonialCmsDataAccessModule,
    ],
})
export class BazaNcIntegrationCmsDataAccessModule {}
