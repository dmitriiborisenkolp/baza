import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcIntegrationListingsCmsDataAccess } from './ng/baza-nc-integration-listings-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaNcIntegrationListingsCmsDataAccess],
})
export class BazaNcIntegrationListingsCmsDataAccessModule {}
