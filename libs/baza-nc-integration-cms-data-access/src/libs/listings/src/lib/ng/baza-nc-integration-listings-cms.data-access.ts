import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcIntegrationChangeStatusRequest,
    BazaNcIntegrationListingsCmsCreateRequest,
    BazaNcIntegrationListingsCmsDeleteRequest,
    BazaNcIntegrationListingsCmsDto,
    BazaNcIntegrationListingsCmsEndpoint,
    BazaNcIntegrationListingsCmsEndpointPaths,
    BazaNcIntegrationListingsCmsGetByIdRequest,
    BazaNcIntegrationListingsCmsGetByUlidRequest,
    BazaNcIntegrationListingsCmsListRequest,
    BazaNcIntegrationListingsCmsListResponse,
    BazaNcIntegrationListingsCmsSetSortOrderRequest,
    BazaNcIntegrationListingsCmsSortOrderResponse,
    BazaNcIntegrationListingsCmsUpdateRequest,
    BazaNcIntegrationListingsListItemDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcIntegrationListingsCmsDataAccess implements BazaNcIntegrationListingsCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    create(request: BazaNcIntegrationListingsCmsCreateRequest): Observable<BazaNcIntegrationListingsCmsDto> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.create, request);
    }

    update(request: BazaNcIntegrationListingsCmsUpdateRequest): Observable<BazaNcIntegrationListingsCmsDto> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.update, request);
    }

    delete(request: BazaNcIntegrationListingsCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.delete, request);
    }

    list(request: BazaNcIntegrationListingsCmsListRequest): Observable<BazaNcIntegrationListingsCmsListResponse> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.list, request);
    }

    listAll(): Observable<Array<BazaNcIntegrationListingsListItemDto>> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.listAll);
    }

    getById(request: BazaNcIntegrationListingsCmsGetByIdRequest): Observable<BazaNcIntegrationListingsCmsDto> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.getById, request);
    }

    getByUlid(request: BazaNcIntegrationListingsCmsGetByUlidRequest): Observable<BazaNcIntegrationListingsCmsDto> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.getByUlid, request);
    }

    changeStatus(request: BazaNcIntegrationChangeStatusRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.changeStatus, request);
    }

    setSortOrder(request: BazaNcIntegrationListingsCmsSetSortOrderRequest): Observable<BazaNcIntegrationListingsCmsSortOrderResponse> {
        return this.http.post(BazaNcIntegrationListingsCmsEndpointPaths.setSortOrder, request);
    }
}
