import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcIntegrationEliteInvestorsCmsEndpoint,
    BazaNcIntegrationEliteInvestorsCmsEndpointPaths,
    BazaNcIntegrationEliteInvestorsCmsExcludeRequest,
    BazaNcIntegrationEliteInvestorsCmsExcludeResponse,
    BazaNcIntegrationEliteInvestorsCmsGetByIdRequest,
    BazaNcIntegrationEliteInvestorsCmsGetByIdResponse,
    BazaNcIntegrationEliteInvestorsCmsIncludeRequest,
    BazaNcIntegrationEliteInvestorsCmsIncludeResponse,
    BazaNcIntegrationEliteInvestorsCmsListRequest,
    BazaNcIntegrationEliteInvestorsCmsListResponse,
    BazaNcIntegrationEliteInvestorsCmsSyncAccountRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcIntegrationEliteInvestorsCmsDataAccess implements BazaNcIntegrationEliteInvestorsCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    include(request: BazaNcIntegrationEliteInvestorsCmsIncludeRequest): Observable<BazaNcIntegrationEliteInvestorsCmsIncludeResponse> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.include, request);
    }

    exclude(request: BazaNcIntegrationEliteInvestorsCmsExcludeRequest): Observable<BazaNcIntegrationEliteInvestorsCmsExcludeResponse> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.exclude, request);
    }

    list(request: BazaNcIntegrationEliteInvestorsCmsListRequest): Observable<BazaNcIntegrationEliteInvestorsCmsListResponse> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.list, request);
    }

    getById(request: BazaNcIntegrationEliteInvestorsCmsGetByIdRequest): Observable<BazaNcIntegrationEliteInvestorsCmsGetByIdResponse> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.getById, request);
    }

    syncAccount(request: BazaNcIntegrationEliteInvestorsCmsSyncAccountRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.syncAccount, request);
    }

    syncAllAccounts(): Observable<void> {
        return this.http.post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.syncAllAccounts);
    }
}
