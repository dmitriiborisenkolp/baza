import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcIntegrationEliteInvestorsCmsDataAccess } from './ng/baza-nc-integration-elite-investors-cms-data-access.service';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaNcIntegrationEliteInvestorsCmsDataAccess],
})
export class BazaNcIntegrationEliteInvestorsCmsDataAccessModule {}
