import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcIntegrationSchemaCmsCreateRequest,
    BazaNcIntegrationSchemaCmsDeleteRequest,
    BazaNcIntegrationSchemaCmsEndpoint,
    BazaNcIntegrationSchemaCmsEndpointPaths,
    BazaNcIntegrationSchemaCmsGetByIdRequest,
    BazaNcIntegrationSchemaCmsListRequest,
    BazaNcIntegrationSchemaCmsListResponse,
    BazaNcIntegrationSchemaCmsRenameRequest,
    BazaNcIntegrationSchemaCmsSetSortOrderRequest,
    BazaNcIntegrationSchemaCmsUpdateRequest,
    BazaNcIntegrationSchemaDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

/**
 * Data Access Service for Schema
 */
@Injectable()
export class BazaNcIntegrationSchemaCmsDataAccess implements BazaNcIntegrationSchemaCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    /**
     * Creates a new Schema
     * @param request
     */
    create(request: BazaNcIntegrationSchemaCmsCreateRequest): Observable<BazaNcIntegrationSchemaDto> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.create, request);
    }

    /**
     * Updates existing Schema
     * @param request
     */
    update(request: BazaNcIntegrationSchemaCmsUpdateRequest): Observable<BazaNcIntegrationSchemaDto> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.update, request);
    }

    /**
     * Renames existing Schema
     * @param request
     */
    rename(request: BazaNcIntegrationSchemaCmsRenameRequest): Observable<BazaNcIntegrationSchemaDto> {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.rename, request);
    }

    /**
     * Deletes existing Schema
     * @param request
     */
    delete(request: BazaNcIntegrationSchemaCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.delete, request);
    }

    /**
     * Returns list of Schemas
     * @param request
     */
    list(request: BazaNcIntegrationSchemaCmsListRequest): Observable<BazaNcIntegrationSchemaCmsListResponse> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.list, request);
    }

    /**
     * Returns Schema by ID
     * @param request
     */
    getById(request: BazaNcIntegrationSchemaCmsGetByIdRequest): Observable<BazaNcIntegrationSchemaDto> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.getById, request);
    }

    /**
     * Set Sort Order for Schema
     * @param request
     */
    setSortOrder(request: BazaNcIntegrationSchemaCmsSetSortOrderRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationSchemaCmsEndpointPaths.setSortOrder, request);
    }
}
