import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcIntegrationSchemaCmsDataAccess } from './ng/baza-nc-integration-schema-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaNcIntegrationSchemaCmsDataAccess],
})
export class BazaNcIntegrationSchemaCmsDataAccessModule {}
