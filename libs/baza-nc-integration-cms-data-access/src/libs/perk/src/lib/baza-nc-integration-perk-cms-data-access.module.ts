import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcIntegrationPerkCmsDataAccess } from './ng/baza-nc-integration-perk-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaNcIntegrationPerkCmsDataAccess],
})
export class BazaNcIntegrationPerkCmsDataAccessModule {}
