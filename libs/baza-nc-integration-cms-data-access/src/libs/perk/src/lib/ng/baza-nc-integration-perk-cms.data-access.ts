import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcIntegrationPerkCmsCreateRequest,
    BazaNcIntegrationPerkCmsDeleteRequest,
    BazaNcIntegrationPerkCmsEndpoint,
    BazaNcIntegrationPerkCmsEndpointPaths,
    BazaNcIntegrationPerkCmsGetByIdRequest,
    BazaNcIntegrationPerkCmsListRequest,
    BazaNcIntegrationPerkCmsListResponse,
    BazaNcIntegrationPerkCmsSetSortOrderRequest,
    BazaNcIntegrationPerkCmsUpdateRequest,
    BazaNcIntegrationPerkDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcIntegrationPerkCmsDataAccess implements BazaNcIntegrationPerkCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    create(request: BazaNcIntegrationPerkCmsCreateRequest): Observable<BazaNcIntegrationPerkDto> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.create, request);
    }

    update(request: BazaNcIntegrationPerkCmsUpdateRequest): Observable<BazaNcIntegrationPerkDto> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.update, request);
    }

    delete(request: BazaNcIntegrationPerkCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.delete, request);
    }

    getById(request: BazaNcIntegrationPerkCmsGetByIdRequest): Observable<BazaNcIntegrationPerkDto> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.getById, request);
    }

    list(request: BazaNcIntegrationPerkCmsListRequest): Observable<BazaNcIntegrationPerkCmsListResponse> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.list, request);
    }

    setSortOrder(request: BazaNcIntegrationPerkCmsSetSortOrderRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationPerkCmsEndpointPaths.setSortOrder, request);
    }
}
