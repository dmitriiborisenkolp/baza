import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcIntegrationSubscriptionCmsEndpoint,
    BazaNcIntegrationSubscriptionCmsEndpointPaths,
    BazaNcIntegrationSubscriptionCmsExcludeRequest,
    BazaNcIntegrationSubscriptionCmsExcludeResponse,
    BazaNcIntegrationSubscriptionCmsExportToCsvRequest,
    BazaNcIntegrationSubscriptionCmsIncludeRequest,
    BazaNcIntegrationSubscriptionCmsIncludeResponse,
    BazaNcIntegrationSubscriptionCmsListRequest,
    BazaNcIntegrationSubscriptionCmsListResponse,
    BazaNcIntegrationSubscriptionCmsNotifyUsersRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcIntegrationSubscriptionCmsDataAccess implements BazaNcIntegrationSubscriptionCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    list(request: BazaNcIntegrationSubscriptionCmsListRequest): Observable<BazaNcIntegrationSubscriptionCmsListResponse> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.list, request);
    }

    include(request: BazaNcIntegrationSubscriptionCmsIncludeRequest): Observable<BazaNcIntegrationSubscriptionCmsIncludeResponse> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.include, request);
    }

    exclude(request: BazaNcIntegrationSubscriptionCmsExcludeRequest): Observable<BazaNcIntegrationSubscriptionCmsExcludeResponse> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.exclude, request);
    }

    exportToCsv(request: BazaNcIntegrationSubscriptionCmsExportToCsvRequest): Observable<string> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.exportToCsv, request, {
            responseType: 'text',
        });
    }

    notifyUsers(request: BazaNcIntegrationSubscriptionCmsNotifyUsersRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationSubscriptionCmsEndpointPaths.notifyUsers, request, { asJsonResponse: false });
    }
}
