import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcIntegrationSubscriptionCmsDataAccess } from './ng/baza-nc-integration-subscription-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaNcIntegrationSubscriptionCmsDataAccess],
})
export class BazaNcIntegrationSubscriptionCmsDataAccessModule {}
