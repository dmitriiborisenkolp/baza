import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';
import { BazaNcSearchCmsEndpoint, BazaNcSearchCmsEndpointPaths } from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

/**
 * Data Access Service for Search CMS API
 */
@Injectable()
export class BazaNcIntegrationSearchCmsDataAccess implements BazaNcSearchCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    /**
     * Reindex specific Listing for Search API
     * @param ulid
     */
    reindex(ulid: string): Observable<void> {
        return this.http.post(replacePathArgs(BazaNcSearchCmsEndpointPaths.reindex, { ulid }));
    }

    /**
     * Reindex all Listings for Search API
     */
    reindexAll(): Observable<void> {
        return this.http.post(BazaNcSearchCmsEndpointPaths.reindexAll);
    }
}
