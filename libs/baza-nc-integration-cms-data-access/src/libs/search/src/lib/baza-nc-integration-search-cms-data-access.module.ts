import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcIntegrationSearchCmsDataAccess } from './ng/baza-nc-integration-search-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaNcIntegrationSearchCmsDataAccess],
})
export class BazaNcIntegrationSearchCmsDataAccessModule {}
