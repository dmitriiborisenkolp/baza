import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcIntegrationListingTestimonialCmsDataAccess } from './ng/baza-nc-integration-listing-testimonial-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaNcIntegrationListingTestimonialCmsDataAccess],
})
export class BazaNcIntegrationListingTestimonialCmsDataAccessModule {}
