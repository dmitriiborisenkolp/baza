import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcIntegrationListingTestimonialCmsCreateRequest,
    BazaNcIntegrationListingTestimonialCmsDeleteRequest,
    BazaNcIntegrationListingTestimonialCmsDto,
    BazaNcIntegrationListingTestimonialCmsEndpoint,
    BazaNcIntegrationListingTestimonialCmsEndpointPaths,
    BazaNcIntegrationListingTestimonialCmsGetByUlidRequest,
    BazaNcIntegrationListingTestimonialCmsListRequest,
    BazaNcIntegrationListingTestimonialCmsListResponse,
    BazaNcIntegrationListingTestimonialCmsSetSortOrderRequest,
    BazaNcIntegrationListingTestimonialCmsUpdateRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcIntegrationListingTestimonialCmsDataAccess implements BazaNcIntegrationListingTestimonialCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    create(request: BazaNcIntegrationListingTestimonialCmsCreateRequest): Observable<BazaNcIntegrationListingTestimonialCmsDto> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.create, request);
    }

    update(request: BazaNcIntegrationListingTestimonialCmsUpdateRequest): Observable<BazaNcIntegrationListingTestimonialCmsDto> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.update, request);
    }

    delete(request: BazaNcIntegrationListingTestimonialCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.delete, request);
    }

    list(request: BazaNcIntegrationListingTestimonialCmsListRequest): Observable<BazaNcIntegrationListingTestimonialCmsListResponse> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.list, request);
    }

    getByUlid(request: BazaNcIntegrationListingTestimonialCmsGetByUlidRequest): Observable<BazaNcIntegrationListingTestimonialCmsDto> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.getByUlid, request);
    }

    setSortOrder(request: BazaNcIntegrationListingTestimonialCmsSetSortOrderRequest): Observable<void> {
        return this.http.post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.setSortOrder, request);
    }
}
