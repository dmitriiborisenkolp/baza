import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNcIntegrationFeedCmsDataAccess } from './ng/baza-nc-integration-feed-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaNcIntegrationFeedCmsDataAccess],
})
export class BazaNcIntegrationFeedCmsDataAccessModule {}
