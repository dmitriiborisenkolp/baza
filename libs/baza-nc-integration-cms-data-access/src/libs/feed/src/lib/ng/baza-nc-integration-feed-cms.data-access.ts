import { Injectable } from '@angular/core';
import { BazaCmsDataAccessService } from '@scaliolabs/baza-core-cms-data-access';
import {
    BazaNcIntegrationFeedCmsEndpoint,
    FeedCmsPaths,
    FeedCmsPostsCreateRequest,
    FeedCmsPostsCreateResponse,
    FeedCmsPostsDeleteRequest,
    FeedCmsPostsDeleteResponse,
    FeedCmsPostsGetByIdRequest,
    FeedCmsPostsGetByIdResponse,
    FeedCmsPostsListRequest,
    FeedCmsPostsListResponse,
    FeedCmsPostsUpdateRequest,
    FeedCmsPostsUpdateResponse,
    FeedCmsSetSortOrderRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcIntegrationFeedCmsDataAccess implements BazaNcIntegrationFeedCmsEndpoint {
    constructor(private readonly ngEndpoint: BazaCmsDataAccessService) {}

    create(request: FeedCmsPostsCreateRequest): Observable<FeedCmsPostsCreateResponse> {
        return this.ngEndpoint.post(FeedCmsPaths.create, request);
    }

    update(request: FeedCmsPostsUpdateRequest): Observable<FeedCmsPostsUpdateResponse> {
        return this.ngEndpoint.post(FeedCmsPaths.update, request);
    }

    delete(request: FeedCmsPostsDeleteRequest): Observable<FeedCmsPostsDeleteResponse> {
        return this.ngEndpoint.post(FeedCmsPaths.delete, request);
    }

    list(request: FeedCmsPostsListRequest): Observable<FeedCmsPostsListResponse> {
        return this.ngEndpoint.post(FeedCmsPaths.list, request);
    }

    getById(request: FeedCmsPostsGetByIdRequest): Observable<FeedCmsPostsGetByIdResponse> {
        return this.ngEndpoint.post(FeedCmsPaths.getById, request);
    }

    setSortOrder(request: FeedCmsSetSortOrderRequest): Observable<void> {
        return this.ngEndpoint.post(FeedCmsPaths.setSortOrder, request);
    }
}
