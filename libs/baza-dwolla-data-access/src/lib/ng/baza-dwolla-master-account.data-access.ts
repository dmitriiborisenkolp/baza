import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import {
    BazaDwollaMasterAccountBalanceDto,
    BazaDwollaMasterAccountEndpoint,
    BazaDwollaMasterAccountEndpointPaths,
} from '@scaliolabs/baza-dwolla-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaDwollaMasterAccountDataAccess implements BazaDwollaMasterAccountEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    balance(): Observable<BazaDwollaMasterAccountBalanceDto> {
        return this.http.get(BazaDwollaMasterAccountEndpointPaths.balance);
    }
}
