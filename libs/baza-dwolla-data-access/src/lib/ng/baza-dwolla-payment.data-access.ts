import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import {
    BazaDwollaPaymentalListRequest,
    BazaDwollaPaymentalListResponse,
    BazaDwollaPaymentDto,
    BazaDwollaPaymentEndpoint,
    BazaDwollaPaymentEndpointPaths,
} from '@scaliolabs/baza-dwolla-shared';
import { Observable } from 'rxjs';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

/**
 * Dwolla Payments Data Access service
 */
@Injectable()
export class BazaDwollaPaymentDataAccess implements BazaDwollaPaymentEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    /**
     * Returns list of Dwolla Payments of current Investor
     * @param request
     */
    list(request: BazaDwollaPaymentalListRequest): Observable<BazaDwollaPaymentalListResponse> {
        return this.http.get(BazaDwollaPaymentEndpointPaths.list, request);
    }

    /**
     * Returns Dwolla Payment by ULID (only payments of current Investor)
     * @param ulid
     */
    get(ulid: string): Observable<BazaDwollaPaymentDto> {
        return this.http.get(replacePathArgs(BazaDwollaPaymentEndpointPaths.get, { ulid }));
    }
}
