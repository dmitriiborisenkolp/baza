import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import {
    BazaDwollaE2eEndpoint,
    BazaDwollaE2eEndpointPaths,
    BazaDwollaE2eGetTransferValueRequest,
    BazaDwollaE2eGetTransferValueResponse,
    BazaDwollaE2eTransferFundsFromMasterAccountRequest,
    BazaDwollaWebhookDto,
} from '@scaliolabs/baza-dwolla-shared';
import { Observable } from 'rxjs';

/**
 * E2E Helpers for Dwolla
 */
@Injectable()
export class BazaDwollaE2eDataAccess implements BazaDwollaE2eEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    /**
     * Run Sandbox Simulation
     * Dwolla Sandbox will not process any bank transfer by default. You should call this method to run simulations.
     */
    runSandboxSimulations(): Observable<void> {
        return this.http.get(BazaDwollaE2eEndpointPaths.runSandboxSimulations);
    }

    /**
     * Sends Webhook Event to Baza Event Bus
     * Use it to simulate webhook events from Dwolla
     * @param event
     */
    simulateWebhookEvent(event: BazaDwollaWebhookDto): Observable<void> {
        return this.http.post(BazaDwollaE2eEndpointPaths.simulateWebhookEvent, event);
    }

    /**
     * Transfers funds from Master Account to given Dwolla Customer
     * Automatically executes sandbox-simulations
     * @param request
     */
    transferFundsFromMasterAccount(request: BazaDwollaE2eTransferFundsFromMasterAccountRequest): Observable<void> {
        return this.http.post(BazaDwollaE2eEndpointPaths.transferFundsFromMasterAccount, request);
    }

    /**
     * Returns actual Transfer Value (Amount) by Dwolla Transfer Id
     * @param request
     */
    getTransferValue(request: BazaDwollaE2eGetTransferValueRequest): Observable<BazaDwollaE2eGetTransferValueResponse> {
        return this.http.post(BazaDwollaE2eEndpointPaths.getTransferValue, request);
    }
}
