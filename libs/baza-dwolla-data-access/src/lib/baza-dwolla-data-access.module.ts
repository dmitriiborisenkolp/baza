import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaDwollaE2eDataAccess } from './ng/baza-dwolla-e2e.data-access';
import { BazaDwollaMasterAccountDataAccess } from './ng/baza-dwolla-master-account.data-access';
import { BazaDwollaPaymentDataAccess } from './ng/baza-dwolla-payment.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaDwollaMasterAccountDataAccess, BazaDwollaE2eDataAccess, BazaDwollaPaymentDataAccess],
})
export class BazaDwollaDataAccessModule {}
