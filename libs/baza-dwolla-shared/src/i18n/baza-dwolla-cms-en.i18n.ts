import { bazaDwollaAclEnI18n } from '../bundle/baza-dwolla-acl';
import { BazaDwollaPaymentStatus, BazaDwollaPaymentType } from '../libs/dwolla-payment/src';
import { DwollaFundingSourceStatus, DwollaFundingSourceType } from '../libs/dwolla-api/src';

export const bazaDwollaCmsEnI18n = {
    __acl: bazaDwollaAclEnI18n,
    'baza-dwolla': {
        __types: {
            BazaDwollaPaymentType: {
                [BazaDwollaPaymentType.CashIn]: 'Cash-In',
                [BazaDwollaPaymentType.Dividend]: 'Dividend',
                [BazaDwollaPaymentType.PurchaseShares]: 'Purchasing Shares',
                [BazaDwollaPaymentType.Withdrawal]: 'Withdarawal Requests',
            },
            BazaDwollaPaymentStatus: {
                [BazaDwollaPaymentStatus.Processed]: 'Processed',
                [BazaDwollaPaymentStatus.Cancelled]: 'Cancelled',
                [BazaDwollaPaymentStatus.Pending]: 'Pending',
                [BazaDwollaPaymentStatus.Failed]: 'Failed',
            },
            DwollaFundingSourceType: {
                [DwollaFundingSourceType.Bank]: 'Bank',
                [DwollaFundingSourceType.Balance]: 'Balance',
            },
            DwollaFundingSourceStatus: {
                [DwollaFundingSourceStatus.Verified]: 'Verified',
                [DwollaFundingSourceStatus.Unverified]: 'Unverified',
            },
        },
        title: 'Dwolla',
        payment: {
            title: 'Dwolla Payments',
            components: {
                bazaDwollaPaymentCms: {
                    title: 'Dwolla Payments',
                    search: 'Search',
                    columns: {
                        ulid: 'ULID',
                        account: 'Account',
                        dateCreatedAt: 'Date Created At',
                        dateUpdatedAt: 'Date Updated At',
                        dateProcessedAt: 'Date Processed At',
                        type: 'Type',
                        correlationId: 'Correlation Id',
                        amount: 'Amount',
                        status: 'Status',
                        dwollaTransferId: 'Dwolla Transfer Id',
                    },
                    actions: {
                        refresh: 'Refresh',
                        sync: {
                            title: 'Sync Payment with Dwolla',
                            failed: 'Failed to sync payment status with Dwolla',
                            success: 'Payment status updated with Dwolla',
                        },
                    },
                },
            },
        },
        escrow: {
            title: 'Dwolla Escrow',
            components: {
                bazaDwollaEscrowCms: {
                    title: 'Dwolla Escrow',
                    balance: 'Account Balance',
                    actions: {
                        set: {
                            title: 'Select as Escrow Account',
                            confirm: 'Do you really want to set {{ name }} as Escrow Account?',
                            success: '{{ name }} set as Escrow Account',
                            failed: 'Failed to set {{ name }} as Escrow Account',
                        },
                    },
                    columns: {
                        name: 'Name',
                        isSelectedAsEscrow: 'Selected As Escrow',
                        type: 'Type',
                        status: 'Status',
                        created: 'Created',
                    },
                },
            },
        },
    },
};
