export enum BazaDwollaAclGroups {
    BazaDwolla = 'BazaDwolla',
}

export enum BazaDwollaAcl {
    BazaDwollaPayment = 'BazaDwollaPayment',
    BazaDwollaEscrow = 'BazaDwollaEscrow',
}

export const bazaDwollaAclEnI18n = {
    [BazaDwollaAclGroups.BazaDwolla]: {
        group: 'Dwolla',
        nodes: {
            [BazaDwollaAcl.BazaDwollaPayment]: 'Dwolla Payments List',
            [BazaDwollaAcl.BazaDwollaEscrow]: 'Dwolla Escrow',
        },
    },
};
