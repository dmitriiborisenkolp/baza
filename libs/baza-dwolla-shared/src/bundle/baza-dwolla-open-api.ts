export enum BazaDwollaOpenApi {
    BazaDwolla = 'Baza Dwolla',
    BazaDwollaCms = 'Baza Dwolla CMS',
}

export enum BazaDwollaPublicOpenApi {
    BazaDwollaPayment = 'Baza Dwolla Payment',
}

export enum BazaDwollaCmsOpenApi {
    BazaDwollaCmsWebhooks = 'Baza Dwolla CMS Webhooks',
    BazaDwollaCmsMasterAccount = 'Baza Dwolla CMS Master Account',
    BazaDwollaCmsPayment = 'Baza Dwolla CMS Payment',
    BazaDwollaCmsE2e = 'Baza Dwolla E2E',
    BazaDwollaCmsEscrow = 'Baza Dwolla Escrow CMS',
}
