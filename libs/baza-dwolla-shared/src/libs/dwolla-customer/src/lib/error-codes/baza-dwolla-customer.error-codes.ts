export enum BazaDwollaCustomerErrorCodes {
    BazaDwollaCustomerNotFound = 'BazaDwollaCustomerNotFound',
    BazaDwollaCustomerNoBalanceAvailable = 'BazaDwollaCustomerNoBalanceAvailable',
}

export const bazaDwollaCustomerErrorCodesI18n = {
    [BazaDwollaCustomerErrorCodes.BazaDwollaCustomerNotFound]: 'Dwolla Customer was not found',
    [BazaDwollaCustomerErrorCodes.BazaDwollaCustomerNoBalanceAvailable]: 'Dwolla Customer is not verified',
};
