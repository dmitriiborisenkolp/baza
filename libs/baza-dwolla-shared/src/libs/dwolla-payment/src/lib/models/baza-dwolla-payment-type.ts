export enum BazaDwollaPaymentType {
    Dividend = 'Dividend',
    PurchaseShares = 'PurchaseShares',
    Withdrawal = 'Withdrawal',
    CashIn = 'CashIn',
}
