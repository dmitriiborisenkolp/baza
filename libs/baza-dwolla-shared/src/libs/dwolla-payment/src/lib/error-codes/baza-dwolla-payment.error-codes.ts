export enum BazaDwollaPaymentErrorCodes {
    BazaDwollaPaymentNotFound = 'BazaDwollaPaymentNotFound',
    BazaDwollaPaymentNoInsufficientFunds = 'BazaDwollaPaymentNoInsufficientFunds',
    BazaDwollaPaymentUnknownType = 'BazaDwollaPaymentUnknownType',
    BazaDwollaPaymentUnknownStatus = 'BazaDwollaPaymentUnknownStatus',
    BazaDwollaPaymentOperationNotPermitted = 'BazaDwollaPaymentOperationNotPermitted',
    BazaDwollaPaymentEscrowNotConfigured = 'BazaDwollaPaymentEscrowNotConfigured',
    BazaDwollaPaymentIsNotAchPayment = 'BazaDwollaPaymentIsNotAchPayment',
}

export const bazaDwollaPaymentErrorCodesI18n: Record<BazaDwollaPaymentErrorCodes, string> = {
    [BazaDwollaPaymentErrorCodes.BazaDwollaPaymentNotFound]: 'Dwolla Payment was not found',
    [BazaDwollaPaymentErrorCodes.BazaDwollaPaymentNoInsufficientFunds]: 'Insufficient Funds',
    [BazaDwollaPaymentErrorCodes.BazaDwollaPaymentUnknownType]: 'Unknown Dwolla Payment Type "{{ type }}"',
    [BazaDwollaPaymentErrorCodes.BazaDwollaPaymentUnknownStatus]: 'Unknown Dwolla Status "{{ status }}"',
    [BazaDwollaPaymentErrorCodes.BazaDwollaPaymentOperationNotPermitted]: 'Dwolla Payment operation not permitted',
    [BazaDwollaPaymentErrorCodes.BazaDwollaPaymentEscrowNotConfigured]: 'Dwolla Payments are not available yet',
    [BazaDwollaPaymentErrorCodes.BazaDwollaPaymentIsNotAchPayment]: 'Dwolla Payment is not an ACH payment',
};
