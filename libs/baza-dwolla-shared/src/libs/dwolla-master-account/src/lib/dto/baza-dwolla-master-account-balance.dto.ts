import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { IsNotEmpty, IsString } from 'class-validator';

export class BazaDwollaMasterAccountBalanceDto {
    @ApiModelProperty({
        description: 'The amount value',
        example: '0.00',
    })
    @IsString()
    @IsNotEmpty()
    value: string;

    @ApiModelProperty({
        description: 'The currency name',
        example: 'USD',
    })
    @IsString()
    @IsNotEmpty()
    currency: string;
}
