import { BazaDwollaMasterAccountBalanceDto } from '../dto/baza-dwolla-master-account-balance.dto';
import { Observable } from 'rxjs';

export enum BazaDwollaMasterAccountEndpointPaths {
    balance = '/baza-dwolla/master-account/balance',
}

export interface BazaDwollaMasterAccountEndpoint {
    balance(): Promise<BazaDwollaMasterAccountBalanceDto> | Observable<BazaDwollaMasterAccountBalanceDto>;
}
