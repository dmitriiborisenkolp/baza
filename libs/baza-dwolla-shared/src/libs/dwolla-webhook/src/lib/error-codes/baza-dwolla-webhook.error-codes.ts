export enum BazaDwollaWebhookErrorCodes {
    BazaDwollaWebhookInvalidSecretKey = 'BazaDwollaWebhookInvalidSecretKey',
}

export const bazaDwollaWebhookErrorCodesEnI18n = {
    [BazaDwollaWebhookErrorCodes.BazaDwollaWebhookInvalidSecretKey]: 'Invalid secret key',
};
