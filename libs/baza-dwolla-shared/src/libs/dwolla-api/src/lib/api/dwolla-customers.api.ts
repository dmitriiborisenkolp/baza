import { UpdateCustomerRequest } from '../dto/requests/update-customer.request';
import { GetCustomerListRequest } from '../dto/requests/get-customer-list.request';
import { GetCustomerDetailsResponse } from '../dto/responses/get-customer-details.response';
import { UpdateCustomerStatusRequest } from '../dto/requests/update-customer-status.request';
import {
    BazaDwollaCreatePersonalCustomerResponse,
    BusinessClassification,
    CreateBusinessCustomerRequest,
    CreatePersonalCustomerRequest,
    CreateReceiveOnlyCustomerRequest,
    DwollaCustomerId,
    GetBusinessClassificationList,
    GetCustomerRequest,
    GetCustomersResponse,
} from '../dto';
import { GetBusinessClassificationRequest } from '../dto/requests/get-business-classification.request';

export enum DwollaCustomersPaths {
    Customers = 'customers',
    BusinessClassifications = 'business-classifications',
}

export interface DwollaCustomersApi {
    getCustomer(request: GetCustomerRequest): Promise<GetCustomerDetailsResponse>;
    getCustomerList(request?: GetCustomerListRequest): Promise<GetCustomersResponse>;
    getBusinessClassification(request: GetBusinessClassificationRequest): Promise<BusinessClassification>;
    getBusinessClassificationList(): Promise<GetBusinessClassificationList>;
    createReceiveOnlyCustomer(request: CreateReceiveOnlyCustomerRequest): Promise<DwollaCustomerId>;
    createPersonalCustomer(request: CreatePersonalCustomerRequest): Promise<DwollaCustomerId>;
    createBusinessCustomer(request: CreateBusinessCustomerRequest): Promise<DwollaCustomerId>;
    updateCustomer(customerId: string, request: UpdateCustomerRequest): Promise<GetCustomerDetailsResponse>;
    updateCustomerStatus(customerId: string, request: UpdateCustomerStatusRequest): Promise<GetCustomerDetailsResponse>;
    retryVerificationForPersonalCustomer(
        customerId: string,
        request: CreatePersonalCustomerRequest,
    ): Promise<BazaDwollaCreatePersonalCustomerResponse>;
}
