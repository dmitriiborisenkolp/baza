import { CreateDocumentRequest, GetDocumentListResponse, GetDocumentResponse } from '../dto';

export enum DwollaDocumentsPaths {
    Documents = 'documents',
    Customers = 'customers',
    BeneficialOwners = 'beneficial-owners',
}

export interface DwollaDocumentsApi {
    getDocument(documentId: string): Promise<GetDocumentResponse>;
    createDocumentForCustomer(customerId: string, file: File | unknown, request: CreateDocumentRequest): Promise<void>;
    getDocumentListForCustomer(customerId: string): Promise<GetDocumentListResponse>;
    createDocumentForBeneficialOwner(beneficialOwnerId: string, file: File | unknown, request: CreateDocumentRequest): Promise<void>;
    getDocumentListForBeneficialOwner(beneficialOwnerId: string): Promise<GetDocumentListResponse>;
}
