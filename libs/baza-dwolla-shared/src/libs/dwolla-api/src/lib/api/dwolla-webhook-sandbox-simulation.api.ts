export enum DwollaWebhookSandboxSimulationApiPaths {
    SandboxWebhookSimulations = 'sandbox-simulations',
}

export interface DwollaWebhookSandboxSimulationApi {
    sandboxWebhookSimulations(): Promise<void>;
}
