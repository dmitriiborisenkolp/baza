import { GetAccountDetailsRequest } from '../dto/requests/get-account-details.request';
import { GetAccountDetailsResponse } from '../dto/responses/get-account-details.response';
import { GetMassPaymentsForAccountRequest } from '../dto/requests/get-mass-payments-for-account.request';
import { GetTransferListForAccountRequest } from '../dto/requests/get-transfer-list-for-account.request';
import { GetMassPaymentsForAccountResponse } from '../dto/responses/get-mass-payments-for-account.response';
import { GetTransferListResponse } from '../dto/responses/get-transfer-list.response';
import { GetFundingSourcesForAccountRequest } from '../dto/requests/get-funding-sources-for-account.request';
import { GetFundingSourceListResponse } from '../dto/responses/get-funding-source-list.response';
import { CreateFundingSourceForAccountRequest } from '../dto/requests/create-funding-source-for-account.request';
import { DwollaAccountId } from '../dto';

export enum DwollaAccountsPaths {
    GetAccountDetails = 'accounts',
    GetFundingSourcesForAccount = 'funding-sources',
    GetMassPaymentsForAccount = 'mass-payments',
    GetTransferListForAccount = 'transfers',
    CreateFundingSourcesForMasterDwollaAccount = 'funding-sources',
}

export interface DwollaAccountsApi {
    getAccountDetails(request: GetAccountDetailsRequest): Promise<GetAccountDetailsResponse>;
    getFundingSourcesForAccount(
        accountId: DwollaAccountId,
        request?: GetFundingSourcesForAccountRequest,
    ): Promise<GetFundingSourceListResponse>;
    getMassPaymentsForAccount(
        accountId: DwollaAccountId,
        request?: GetMassPaymentsForAccountRequest,
    ): Promise<GetMassPaymentsForAccountResponse>;
    getTransferListForAccount(accountId: DwollaAccountId, request?: GetTransferListForAccountRequest): Promise<GetTransferListResponse>;
    createFundingSourcesForMasterDwollaAccount(request: CreateFundingSourceForAccountRequest): Promise<unknown>;
}
