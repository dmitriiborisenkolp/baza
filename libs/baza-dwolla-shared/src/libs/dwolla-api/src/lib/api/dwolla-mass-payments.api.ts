import {
    CreateMassPaymentRequest,
    GetItemListForMassPaymentRequest,
    GetItemListForMassPaymentResponse,
    GetMassPaymentItemResponse,
    GetMassPaymentListForCustomerRequest,
    GetMassPaymentListForCustomerResponse,
    GetMassPaymentResponse,
    UpdateMassPaymentRequest,
} from '../dto';

export enum DwollaMassPaymentsPaths {
    MassPayments = 'mass-payments',
    Customers = 'customers',
    Items = 'items',
    MassPaymentItems = 'mass-payment-items',
}

export interface DwollaMassPaymentsApi {
    initiateMassPayment(request: CreateMassPaymentRequest): Promise<void>;
    getMassPayment(massPaymentId: string): Promise<GetMassPaymentResponse>;
    updateMassPayment(massPaymentId: string, request: UpdateMassPaymentRequest): Promise<unknown>;
    getItemListForMassPayment(
        massPaymentId: string,
        request?: GetItemListForMassPaymentRequest,
    ): Promise<GetItemListForMassPaymentResponse>;
    getMassPaymentItem(massPaymentItemId: string): Promise<GetMassPaymentItemResponse>;
    getMassPaymentListForCustomer(
        customerId: string,
        request?: GetMassPaymentListForCustomerRequest,
    ): Promise<GetMassPaymentListForCustomerResponse>;
}
