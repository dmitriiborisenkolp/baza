import {
    CancelTransferRequest,
    CreateTransferRequest,
    GetFeeListForTransferResponse,
    GetTransferDetailsResponse,
    GetTransferListForCustomerRequest,
    GetTransferListResponse,
    InitiateTransferResponse,
} from '../dto';

export enum DwollaTransfersPaths {
    Customers = 'customers',
    Transfers = 'transfers',
    Fees = 'fees',
    Failure = 'failure',
}

export interface DwollaTransfersApi {
    initiateTransfer(request: CreateTransferRequest): Promise<InitiateTransferResponse>;
    getTransfer(transferId: string): Promise<GetTransferDetailsResponse>;
    getTransferListForCustomer(customerId: string, request?: GetTransferListForCustomerRequest): Promise<GetTransferListResponse>;
    getFeeListForTransfer(transferId: string): Promise<GetFeeListForTransferResponse>;
    getFailureReasonForTransfer(transferId: string): Promise<unknown>;
    cancelTransfer(transferId: string, request: CancelTransferRequest): Promise<GetTransferDetailsResponse>;
}
