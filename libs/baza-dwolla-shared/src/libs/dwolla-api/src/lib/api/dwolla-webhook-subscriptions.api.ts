import { CreateWebhookSubscriptionRequest, GetWebhookListForWebhookSubscriptionRequest, UpdateWebhookSubscriptionRequest } from '../dto';

export enum DwollaWebhookSubscriptionsPaths {
    WebhookSubscriptions = 'webhook-subscriptions',
    Webhooks = 'webhooks',
}

export interface DwollaWebhookSubscriptionsApi {
    createWebhookSubscription(request: CreateWebhookSubscriptionRequest): Promise<void>;
    getWebhookSubscription(webhookSubscriptionId: string): Promise<unknown>;
    updateWebhookSubscription(webhookSubscriptionId: string, request: UpdateWebhookSubscriptionRequest): Promise<unknown>;
    getWebhookSubscriptionList(): Promise<unknown[]>;
    deleteWebhookSubscription(webhookSubscriptionId: string): Promise<void>;
    getWebhookListForWebhookSubscription(
        webhookSubscriptionId: string,
        request?: GetWebhookListForWebhookSubscriptionRequest,
    ): Promise<unknown[]>;
}
