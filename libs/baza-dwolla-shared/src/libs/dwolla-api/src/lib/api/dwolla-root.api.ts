import { DwollaRootResponse } from '../dto/responses/dwolla-root.response';

export enum DwollaRootPaths {
    Root = '/',
}

export interface DwollaRootApi {
    root(): Promise<DwollaRootResponse>;
}
