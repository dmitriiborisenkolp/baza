import { GetEventListRequest, GetEventListResponse, GetEventResponse } from '../dto';

export enum DwollaEventsPaths {
    Events = 'events',
}

export interface DwollaEventsApi {
    getEvent(applicationEventId: string): Promise<GetEventResponse>;
    getEventList(request?: GetEventListRequest): Promise<GetEventListResponse>;
}
