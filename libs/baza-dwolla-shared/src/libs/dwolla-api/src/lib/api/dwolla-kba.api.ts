import { VerifyKbaQuestionRequest } from '../dto';

export enum DwollaKbaPaths {
    Kba = 'kba',
    Customers = 'customers',
}

export interface DwollaKbaApi {
    initiateKbaSession(customerId: string): Promise<void>;
    getKbaQuestions(kbaSessionId: string): Promise<unknown>;
    verifyKbaQuestion(kbaSessionId: string, request: VerifyKbaQuestionRequest): Promise<void>;
}
