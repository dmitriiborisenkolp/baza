import {
    CreateBeneficialOwnerRequest,
    GetBeneficialOwnerListResponse,
    GetBeneficialOwnerResponse,
    GetBeneficialOwnershipStatusResponse,
    UpdateBeneficialOwnershipStatusRequest,
} from '../dto';

export enum DwollaBeneficialOwnersPaths {
    BeneficialOwners = 'beneficial-owners',
    BeneficialOwnership = 'beneficial-ownership',
    Customers = 'customers',
}

export interface DwollaBeneficialOwnersApi {
    createBeneficialOwner(customerId: string, request: CreateBeneficialOwnerRequest): Promise<void>;
    getBeneficialOwner(beneficialOwnerId: string): Promise<GetBeneficialOwnerResponse>;
    updateBeneficialOwner(beneficialOwnerId: string, request: CreateBeneficialOwnerRequest): Promise<unknown>;
    getBeneficialOwnerList(customerId: string): Promise<GetBeneficialOwnerListResponse>;
    deleteBeneficialOwner(beneficialOwnerId: string): Promise<GetBeneficialOwnerResponse>;
    getBeneficialOwnershipStatus(customerId: string): Promise<GetBeneficialOwnershipStatusResponse>;
    certifyBeneficialOwnership(
        customerId: string,
        request: UpdateBeneficialOwnershipStatusRequest,
    ): Promise<GetBeneficialOwnershipStatusResponse>;
}
