export enum DwollaWebhooksPaths {
    Webhooks = 'webhooks',
    Retries = 'retries',
}

export interface DwollaWebhooksApi {
    getWebhook(webhookId: string): Promise<unknown>;
    retryWebhook(webhookId: string): Promise<unknown>;
    getRetryListForWebhook(webhookId: string): Promise<unknown>;
}
