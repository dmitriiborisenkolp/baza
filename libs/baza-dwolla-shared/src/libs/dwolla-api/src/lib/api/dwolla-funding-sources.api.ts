import {
    CreateBankAccountFundingSourceRequest,
    CreateFundingSourceTokenResponse,
    CreateFundingSourceWithPlaidTokenRequest,
    CreateVANFundingSourceRequest,
    DwollaFundingSource,
    DwollaFundingSourceId,
    GetFundingSourceBalanceResponse,
    GetFundingSourceListResponse,
    GetMicroDepositsDetailResponse,
    RemoveFundingSourceRequest,
    UpdateFundingSourceRequest,
    VerifyMicroDepositsRequest,
} from '../dto';

export enum DwollaFundingSourcesPaths {
    FundingSources = 'funding-sources',
    CreateFundingSourcesToken = 'funding-sources-token',
    CreateIAVToken = 'iav-token',
    CreateCardFundingSourcesToken = 'card-funding-sources-token',
    MicroDeposits = 'micro-deposits',
    GetFundingSourceBalance = 'balance',
}

export interface DwollaFundingSourcesApi {
    createFundingSourceWithPlaidToken(
        customerId: string,
        request: CreateFundingSourceWithPlaidTokenRequest,
    ): Promise<DwollaFundingSourceId>;

    createBankAccountFundingSource(customerId: string, request: CreateBankAccountFundingSourceRequest): Promise<DwollaFundingSourceId>;

    createVANFundingSource(customerId: string, request: CreateVANFundingSourceRequest): Promise<DwollaFundingSourceId>;

    createFundingSourcesToken(customerId: string): Promise<CreateFundingSourceTokenResponse>;

    createIAVToken(customerId: string): Promise<CreateFundingSourceTokenResponse>;

    createCardFundingSourcesToken(customerId: string): Promise<CreateFundingSourceTokenResponse>;

    getFundingSource(fundingSourceId: string): Promise<DwollaFundingSource>;

    getFundingSourceListForCustomer(customerId: string, isRemoved?: boolean): Promise<GetFundingSourceListResponse>;

    updateFundingSource(fundingSourceId: string, request: UpdateFundingSourceRequest): Promise<DwollaFundingSource>;

    initiateMicroDeposits(fundingSourceId: string): Promise<void>;

    verifyMicroDeposits(fundingSourceId: string, request: VerifyMicroDepositsRequest): Promise<void>;

    getFundingSourceBalance(fundingSourceId: string): Promise<GetFundingSourceBalanceResponse>;

    getMicroDepositsDetail(fundingSourceId: string): Promise<GetMicroDepositsDetailResponse>;

    removeFundingSource(fundingSourceId: string, request: RemoveFundingSourceRequest): Promise<void>;
}
