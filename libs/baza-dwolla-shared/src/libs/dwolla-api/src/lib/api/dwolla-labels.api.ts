import { CreateLabelLedgerEntryRequest, CreateLabelReallocationRequest, GetLabelListFilterRequest } from '../dto';

export enum DwollaLabelsPaths {
    Labels = 'labels',
    LedgerEntries = 'ledger-entries',
    LabelReallocations = 'label-reallocations',
    Customers = 'customers',
}

export interface DwollaLabelsApi {
    createLabel(customerId: string): Promise<void>;
    getLabel(labelId: string): Promise<void>;
    createLabelLedgerEntry(labelId: string, request: CreateLabelLedgerEntryRequest): Promise<void>;
    getLabelLedgerEntry(ledgerEntryId: string): Promise<unknown>;
    getLabelLedgerEntryList(labelId: string, request?: GetLabelListFilterRequest): Promise<unknown[]>;
    getLabelListForCustomer(customerId: string, request?: GetLabelListFilterRequest): Promise<unknown[]>;
    createLabelReallocation(request: CreateLabelReallocationRequest): Promise<void>;
    getLabelReallocation(labelId: string): Promise<void>;
    deleteLabel(labelId: string): Promise<void>;
}
