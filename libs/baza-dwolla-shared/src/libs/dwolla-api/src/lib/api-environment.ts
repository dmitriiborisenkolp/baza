// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace DwollaApiEnvironments {
    export interface ApiEnvironment {
        BAZA_DWOLLA_API_KEY: string;
        BAZA_DWOLLA_API_SECRET: string;
        BAZA_DWOLLA_BASE_URL: string;
        BAZA_DWOLLA_SANDBOX: string;
        BAZA_DWOLLA_DEBUG?: string;
        BAZA_DWOLLA_DEBUG_NESTJS_LOG?: string;
        BAZA_DWOLLA_WEBHOOK_SECRET: string;
        BAZA_DWOLLA_WEBHOOK_KAFKA_SEND?: string;
        BAZA_DWOLLA_WEBHOOK_KAFKA_READ?: string;
        BAZA_DWOLLA_WEBHOOK_DEBUG?: string;
    }
}
