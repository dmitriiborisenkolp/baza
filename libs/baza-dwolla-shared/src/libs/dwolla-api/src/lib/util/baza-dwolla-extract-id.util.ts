/**
 * Extracts ID's from Dwolla URLs
 * @param url
 */
export function bazaDwollaExtractId(url: string): string {
    const parts = url.split('/');

    return parts[parts.length - 1];
}
