import { DwollaBaseResponse } from '../../common';

export class CreateFundingSourceTokenResponse extends DwollaBaseResponse {
    token: string;
}
