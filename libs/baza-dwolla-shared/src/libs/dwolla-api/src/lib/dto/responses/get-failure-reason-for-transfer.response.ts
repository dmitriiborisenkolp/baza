import { DwollaFailureCodes } from '../models/dwolla-failure-codes';

export class GetFailureReasonForTransferResponse {
    code: DwollaFailureCodes;
}
