import { DwollaBaseResponse } from '../../common';
import { DwollaEvent } from '../models/dwolla-event';

export class GetEventListResponse extends DwollaBaseResponse {
    __embedded: {
        events: Array<DwollaEvent>;
    };
    total: number;
}
