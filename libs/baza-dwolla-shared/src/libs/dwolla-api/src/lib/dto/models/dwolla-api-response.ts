export interface DwollaApiResponse {
    status: number;
    headers: Headers;
    body: unknown;
}
