import { DwollaBaseResponse } from '../../common';
import { AccountDetail } from '../models/account-details';

export type GetAccountDetailsResponse = DwollaBaseResponse & AccountDetail;
