import { IsEnum, IsNotEmpty } from 'class-validator';
import { DwollaCustomerStatus } from '../models/customer-status';

export class UpdateCustomerStatusRequest {
    @IsEnum(DwollaCustomerStatus)
    @IsNotEmpty()
    status: DwollaCustomerStatus;
}
