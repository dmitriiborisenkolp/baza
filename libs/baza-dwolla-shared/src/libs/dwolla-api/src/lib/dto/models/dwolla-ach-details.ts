export type DwollaACHAddendaInfo = Record<'addenda', { values: string[] }>;

export interface DwollaACHDetails {
    source?: DwollaACHAddendaInfo;
    destination?: DwollaACHAddendaInfo;
}
