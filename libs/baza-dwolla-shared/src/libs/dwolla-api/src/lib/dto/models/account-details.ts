import { DwollaAccountId } from './account-id';

export class AccountDetail {
    id: DwollaAccountId;
    name: string;
    timezoneOffset: number;
    type: string;
}
