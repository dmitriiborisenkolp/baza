import { DwollaBaseResponse } from '../../common';
import { TransferDetails } from '../models/transfer-details';

export class GetItemListForMassPaymentResponse extends DwollaBaseResponse {
    '_embedded': {
        items: Array<TransferDetails>;
    };
    total: number;
}
