export enum DwollaBeneficialOwnershipStatus {
    Uncertified = 'uncertified',
    Certified = 'certified',
}
