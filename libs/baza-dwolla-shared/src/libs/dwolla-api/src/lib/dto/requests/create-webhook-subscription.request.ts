import { IsNotEmpty, IsString } from 'class-validator';

export class CreateWebhookSubscriptionRequest {
    @IsString()
    @IsNotEmpty()
    url: string;

    @IsString()
    @IsNotEmpty()
    secret: string;
}
