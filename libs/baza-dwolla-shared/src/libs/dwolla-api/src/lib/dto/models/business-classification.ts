import { DwollaBaseResponse } from '../../common';

export interface IndustryClassification {
    id: string;
    name: string;
}

export class BusinessClassification extends DwollaBaseResponse {
    _embedded: {
        'industry-classifications': Array<IndustryClassification>;
    };
    id: string;
    name: string;
}
