import { IsEnum, IsNotEmpty } from 'class-validator';
import { DwollaMassPaymentStatus } from '../models/dwolla-mass-payment-status';

export class UpdateMassPaymentRequest {
    @IsEnum(DwollaMassPaymentStatus)
    @IsNotEmpty()
    status: DwollaMassPaymentStatus.Pending | DwollaMassPaymentStatus.Cancelled;
}
