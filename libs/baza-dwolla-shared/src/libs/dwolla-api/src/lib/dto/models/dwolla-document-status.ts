export enum DwollaDocumentStatus {
    Pending = 'pending',
    Reviewed = 'reviewed',
}
