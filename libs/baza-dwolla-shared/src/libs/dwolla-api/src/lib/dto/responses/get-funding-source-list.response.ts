import { DwollaBaseResponse } from '../../common';
import { DwollaFundingSource } from '../models/dwolla-funding-source';

export class GetFundingSourceListResponse extends DwollaBaseResponse {
    _embedded: {
        ['funding-sources']: DwollaFundingSource[];
    };
}
