import { DwollaBaseResponse } from '../../common';
import { LinkEntity } from '../models/link';

export class GetEventResponse extends DwollaBaseResponse {
    _links: LinkEntity;
    id: string;
    created: string;
    topic: string;
    resourceId: string;
}
