export enum DwollaDocumentType {
    Passport = 'passport',
    License = 'license',
    IdCard = 'idCard',
    Other = 'other',
}
