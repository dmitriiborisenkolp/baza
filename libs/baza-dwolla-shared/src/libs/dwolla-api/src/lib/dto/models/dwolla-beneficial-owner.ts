import { DwollaAddress } from './dwolla-address';
import { DwollaBeneficialOwnerVerificationStatus } from './dwolla-beneficial-owner-verification-status';
import { LinkEntity } from './link';

export type DwollaBeneficialOwner = {
    _links?: LinkEntity;
    id?: string;
    firstName: string;
    lastName: string;
    address: DwollaAddress;
    verificationStatus: DwollaBeneficialOwnerVerificationStatus;
    created?: string;
};
