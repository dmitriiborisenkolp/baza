import { IsNotEmpty, IsString } from 'class-validator';

export class CreateFundingSourceRequest {
    @IsString()
    @IsNotEmpty()
    name: string;
}
