export enum DwollaMicroDepositsStatus {
    Pending = 'pending',
    Processed = 'processed',
    Failed = 'failed',
}
