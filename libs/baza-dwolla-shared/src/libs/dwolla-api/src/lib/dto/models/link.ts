export type LinkType = 'application/vnd.dwolla.v1.hal+json';

export enum ResourceType {
    Balance = 'balance',
    Customer = 'customer',
    Transfer = 'transfer',
    Document = 'document',
    Event = 'event',
    Account = 'account',
    MassPayment = 'mass-payment',
    FundingSource = 'funding-source',
    MicroDeposits = 'micro-deposits',
    BeneficialOwner = 'beneficial-owner',
    BeneficialOwnership = 'beneficial-ownership',
}

export class Link {
    href: string;
    type?: LinkType;
    'resource-type'?: ResourceType;
}

export class LinkBalance {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.Balance;
}

export class LinkTransfer {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.Transfer;
}

export class LinkDocument {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.Document;
}

export class LinkEvent {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.Event;
}

export class LinkAccount {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.Account;
}

export class LinkMassPayment {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.MassPayment;
}

export class LinkFundingSource {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.FundingSource;
}

export class LinkMicroDeposits {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.MicroDeposits;
}

export class LinkBeneficialOwner {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.BeneficialOwner;
}

export class LinkBeneficialOwnership {
    href: string;
    type?: LinkType;
    'resource-type': ResourceType.BeneficialOwnership;
}

export type LinkEntity = Record<string, Link>;
