import { DwollaTransferId } from '../models/dwolla-transfer-id';

export class InitiateTransferResponse {
    dwollaTransferId: DwollaTransferId;
    idempotencyKey: string;
}
