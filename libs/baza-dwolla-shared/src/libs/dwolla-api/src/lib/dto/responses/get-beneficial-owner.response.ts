import { DwollaBaseResponse } from '../../common';
import { DwollaAddress } from '../models/dwolla-address';
import { DwollaBeneficialOwnerVerificationStatus } from '../models/dwolla-beneficial-owner-verification-status';

export class GetBeneficialOwnerResponse extends DwollaBaseResponse {
    id: string;
    firstName: string;
    lastName: string;
    address: DwollaAddress;
    verificationStatus: DwollaBeneficialOwnerVerificationStatus;
    created?: string;
}
