import { TransferDetails } from '../models/transfer-details';

export type GetMassPaymentItemResponse = TransferDetails;
