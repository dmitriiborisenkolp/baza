import { IsDefined, IsNotEmptyObject } from 'class-validator';
import { DwollaAmount } from '../models/dwolla-amount';

export class CreateLabelLedgerEntryRequest {
    @IsDefined()
    @IsNotEmptyObject()
    amount: DwollaAmount;
}
