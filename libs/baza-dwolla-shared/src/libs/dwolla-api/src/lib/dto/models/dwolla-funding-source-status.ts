export enum DwollaFundingSourceStatus {
    Verified = 'verified',
    Unverified = 'unverified',
}
