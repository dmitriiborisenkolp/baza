import { DwollaBaseResponse } from '../../common';
import { DwollaMassPaymentDetail } from '../models/dwolla-mass-payment-details';

export class GetMassPaymentListForCustomerResponse extends DwollaBaseResponse {
    '_embedded': {
        'mass-payments': Array<DwollaMassPaymentDetail>;
    };
    total: number;
}
