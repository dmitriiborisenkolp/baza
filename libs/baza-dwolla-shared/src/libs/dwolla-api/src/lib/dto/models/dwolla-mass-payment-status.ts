export enum DwollaMassPaymentStatus {
    Deferred = 'deferred',
    Pending = 'pending',
    Processing = 'processing',
    Complete = 'complete',
    Cancelled = 'cancelled',
}
