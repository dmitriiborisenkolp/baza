export enum DwollaBeneficialOwnerVerificationStatus {
    Verified = 'verified',
    Document = 'document',
    Incomplete = 'incomplete',
}
