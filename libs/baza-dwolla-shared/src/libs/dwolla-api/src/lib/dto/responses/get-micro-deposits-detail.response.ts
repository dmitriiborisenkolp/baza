import { DwollaBaseResponse } from '../../common';
import { MicroDepositsFailure } from '../models/dwolla-micro-deposits-failure';
import { DwollaMicroDepositsStatus } from '../models/dwolla-micro-deposits-status';

export class GetMicroDepositsDetailResponse extends DwollaBaseResponse {
    created: string;
    status: DwollaMicroDepositsStatus;
    failure?: MicroDepositsFailure;
}
