import { IsBoolean } from 'class-validator';

export class RemoveFundingSourceRequest {
    @IsBoolean()
    removed: boolean;
}
