import { LinkEntity } from './link';
import { DwollaAmount } from './dwolla-amount';
import { DwollaMassPaymentStatus } from './dwolla-mass-payment-status';

export class DwollaMassPaymentDetail {
    _links: LinkEntity;
    id: string;
    status: DwollaMassPaymentStatus;
    created: string;
    metadata?: {
        [key: string]: string;
    };
    total: DwollaAmount;
    totalFees: DwollaAmount;
}
