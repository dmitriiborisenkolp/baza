export type DwollaTransferId = string;

export const DWOLLA_TRANSFER_ID_LENGTH = 36;
