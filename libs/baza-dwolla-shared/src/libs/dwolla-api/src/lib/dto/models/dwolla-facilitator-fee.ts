import { Link } from './link';
import { DwollaAmount } from './dwolla-amount';

export interface DwollaFacilitatorFee {
    _links: Record<'charge-to', Link>;
    amount: DwollaAmount;
}
