export enum DwollaBusinessType {
    SoleProprietorship = 'soleProprietorship',
    LLC = 'llc',
}
