import { LinkEntity } from '../models/link';
import { DwollaAmount } from '../models/dwolla-amount';

export class CreateLabelReallocationRequest {
    _links: Record<'from' | 'to', LinkEntity>;

    amount: DwollaAmount;
}
