export enum DwollaCustomerStatus {
    Verified = 'verified',
    Unverified = 'unverified',
    Retry = 'retry',
    Document = 'document',
    DocumentProcessing = 'document_processing',
    Suspended = 'suspended',
    Deactivated = 'deactivated',
}
