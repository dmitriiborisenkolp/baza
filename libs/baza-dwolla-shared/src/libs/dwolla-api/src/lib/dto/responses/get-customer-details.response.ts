import { DwollaBaseResponse } from '../../common';
import { DwollaCustomerType } from '../models/customer-type';
import { DwollaBusinessType } from '../models/business-type';
import { DwollaCustomerStatus } from '../models/customer-status';
import { BusinessCustomerController } from '../requests/create-business-customer.request';

export class GetCustomerDetailsResponse extends DwollaBaseResponse {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    type: DwollaCustomerType;
    status: DwollaCustomerStatus;
    created: string;
    address1?: string;
    address2?: string;
    city?: string;
    state: string;
    postalCode: string;
    businessName?: string;
    controller?: BusinessCustomerController;
    businessType?: DwollaBusinessType;
    businessClassification?: string;
}
