import { DwollaBaseResponse } from '../../common';
import { BusinessClassification } from '../models/business-classification';

export class GetBusinessClassificationList extends DwollaBaseResponse {
    _embedded: {
        'business-classifications': Array<BusinessClassification>;
    };
    total: number;
}
