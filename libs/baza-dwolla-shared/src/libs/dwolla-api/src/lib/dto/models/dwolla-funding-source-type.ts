export enum DwollaFundingSourceType {
    Bank = 'bank',
    Balance = 'balance',
    Virtual = 'virtual', // virtual account number
    Card = 'card', // Debit card
}
