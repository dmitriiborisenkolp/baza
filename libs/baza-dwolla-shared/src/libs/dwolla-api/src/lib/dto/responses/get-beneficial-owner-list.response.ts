import { DwollaBaseResponse } from '../../common';
import { DwollaBeneficialOwner } from '../models/dwolla-beneficial-owner';

export class GetBeneficialOwnerListResponse extends DwollaBaseResponse {
    '_embedded': {
        'beneficial-owners': Array<DwollaBeneficialOwner>;
    };
    total: number;
}
