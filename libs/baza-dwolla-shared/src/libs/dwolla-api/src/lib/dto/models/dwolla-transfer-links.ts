import { Link } from './link';

export type DwollaTransferLinks = Record<'source' | 'destination', Link>;
