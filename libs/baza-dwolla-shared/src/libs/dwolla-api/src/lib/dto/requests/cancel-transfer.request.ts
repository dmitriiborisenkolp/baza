import { IsNotEmpty, IsString } from 'class-validator';
import { DwollaTransferStatus } from '../models/transfer-status';

export class CancelTransferRequest {
    @IsString()
    @IsNotEmpty()
    status: DwollaTransferStatus.Cancelled;
}
