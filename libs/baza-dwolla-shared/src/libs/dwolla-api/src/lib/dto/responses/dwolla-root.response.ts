import { DwollaBaseResponse } from '../../common';
import { Link } from '../models/link';

export class DwollaRootResponseLinks {
    account: Link;
    events: Link;
    'webhook-subscriptions': Link;
    customers: Link;
}

export class DwollaRootResponse extends DwollaBaseResponse<DwollaRootResponseLinks> {}
