import { IsOptional } from 'class-validator';

export class GetFundingSourcesForAccountRequest {
    @IsOptional()
    removed?: boolean = false;
}
