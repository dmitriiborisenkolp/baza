export interface MicroDepositsFailure {
    code: string;
    description: string;
}
