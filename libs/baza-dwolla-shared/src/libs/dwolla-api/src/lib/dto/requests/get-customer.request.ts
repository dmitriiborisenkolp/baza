import { IsNotEmpty, IsString } from 'class-validator';
import { DwollaCustomerId } from '../models/customer-id';

export class GetCustomerRequest {
    @IsString()
    @IsNotEmpty()
    customerId: DwollaCustomerId;
}
