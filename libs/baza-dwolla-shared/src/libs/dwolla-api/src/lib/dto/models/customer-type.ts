export enum DwollaCustomerType {
    Personal = 'personal',
    Business = 'business',
    Unverified = 'unverified',
    ReceiveOnly = 'receive-only'
}
