import { IsBoolean } from 'class-validator';

export class UpdateWebhookSubscriptionRequest {
    @IsBoolean()
    paused: boolean;
}
