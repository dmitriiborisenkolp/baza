import { DwollaUsStates } from './dwolla-us-states';

export type DwollaAddress = {
    address1: string;
    address2?: string;
    address3?: string;
    city: string;
    stateProvinceRegion: DwollaUsStates;
    postalCode: string;
    country: string;
};
