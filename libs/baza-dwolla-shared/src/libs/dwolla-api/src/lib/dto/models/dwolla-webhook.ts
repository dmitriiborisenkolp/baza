export enum DwollaTopic {
    AccountSuspended = 'account_suspended',
    AccountActivated = 'account_activated',
    FundingSourceAdded = 'funding_source_added',
    FundingSourceRemoved = 'funding_source_removed',
    FundingSourceVerified = 'funding_source_verified',
    FundingSourceUnverified = 'funding_source_unverified',
    FundingSourceNegative = 'funding_source_negative',
    FundingSourceUpdated = 'funding_source_updated',
    MicrodepositsAdded = 'microdeposits_added',
    MicrodepositsFailed = 'microdeposits_failed',
    MicrodepositsCompleted = 'microdeposits_completed',
    MicrodepositsMaxattempts = 'microdeposits_maxattempts',
    BankTransferCreated = 'bank_transfer_created',
    BankTransferCreationFailed = 'bank_transfer_creation_failed',
    BankTransferCancelled = 'bank_transfer_cancelled',
    BankTransferFailed = 'bank_transfer_failed',
    BankTransferCompleted = 'bank_transfer_completed',
    TransferCreated = 'transfer_created',
    TransferCancelled = 'transfer_cancelled',
    TransferFailed = 'transfer_failed',
    TransferCompleted = 'transfer_completed',
    CardTransferCreated = 'card_transfer_created',
    CardTransferCancelled = 'card_transfer_cancelled',
    CardTransferFailed = 'card_transfer_failed',
    CardTransferCompleted = 'card_transfer_completed',
    MassPaymentCreated = 'mass_payment_created',
    MassPaymentCompleted = 'mass_payment_completed',
    MassPaymentCancelled = 'mass_payment_cancelled',
    StatementCreated = 'statement_created',
    CustomerCreated = 'customer_created',
    CustomerKbaVerificationNeeded = 'customer_kba_verification_needed',
    CustomerKbaVerificationFailed = 'customer_kba_verification_failed',
    CustomerKbaVerificationPassed = 'customer_kba_verification_passed',
    CustomerVerificationDocumentNeeded = 'customer_verification_document_needed',
    CustomerVerificationDocumentUploaded = 'customer_verification_document_uploaded',
    CustomerVerificationDocumentFailed = 'customer_verification_document_failed',
    CustomerVerificationDocumentApproved = 'customer_verification_document_approved',
    CustomerReverificationNeeded = 'customer_reverification_needed',
    CustomerVerified = 'customer_verified',
    CustomerSuspended = 'customer_suspended',
    CustomerActivated = 'customer_activated',
    CustomerDeactivated = 'customer_deactivated',
    CustomerBeneficialOwnerCreated = 'customer_beneficial_owner_created',
    CustomerBeneficialOwnerRemoved = 'customer_beneficial_owner_removed',
    CustomerBeneficialOwnerVerificationDocumentNeeded = 'customer_beneficial_owner_verification_document_needed',
    CustomerBeneficialOwnerVerificationDocumentUploaded = 'customer_beneficial_owner_verification_document_uploaded',
    CustomerBeneficialOwnerVerificationDocumentFailed = 'customer_beneficial_owner_verification_document_failed',
    CustomerBeneficialOwnerVerificationDocumentApproved = 'customer_beneficial_owner_verification_document_approved',
    CustomerBeneficialOwnerReverificationNeeded = 'customer_beneficial_owner_reverification_needed',
    CustomerBeneficialOwnerVerified = 'customer_beneficial_owner_verified',
    CustomerFundingSourceAdded = 'customer_funding_source_added',
    CustomerFundingSourceRemoved = 'customer_funding_source_removed',
    CustomerFundingSourceVerified = 'customer_funding_source_verified',
    CustomerFundingSourceUnverified = 'customer_funding_source_unverified',
    CustomerFundingSourceNegative = 'customer_funding_source_negative',
    CustomerFundingSourceUpdated = 'customer_funding_source_updated',
    CustomerMicrodepositsAdded = 'customer_microdeposits_added',
    CustomerMicrodepositsFailed = 'customer_microdeposits_failed',
    CustomerMicrodepositsCompleted = 'customer_microdeposits_completed',
    CustomerMicrodepositsMaxattempts = 'customer_microdeposits_maxattempts',
    CustomerBankTransferCreated = 'customer_bank_transfer_created',
    CustomerBankTransferCreationFailed = 'customer_bank_transfer_creation_failed',
    CustomerBankTransferCancelled = 'customer_bank_transfer_cancelled',
    CustomerBankTransferFailed = 'customer_bank_transfer_failed',
    CustomerBankTransferCompleted = 'customer_bank_transfer_completed',
    CustomerTransferCreated = 'customer_transfer_created',
    CustomerTransferCancelled = 'customer_transfer_cancelled',
    CustomerTransferFailed = 'customer_transfer_failed',
    CustomerTransferCompleted = 'customer_transfer_completed',
    CustomerMassPaymentCreated = 'customer_mass_payment_created',
    CustomerMassPaymentCompleted = 'customer_mass_payment_completed',
    CustomerMassPaymentCancelled = 'customer_mass_payment_cancelled',
    CustomerBalanceInquiryCompleted = 'customer_balance_inquiry_completed',
    CustomerLabelCreated = 'customer_label_created',
    CustomerLabelLedgerEntryCreated = 'customer_label_ledger_entry_created',
    CustomerLabelRemoved = 'customer_label_removed',
}

export class DwollaWebhookAttemptResource {
    created: string;
    url: string;
    headers: Array<{ [key: string]: string }>;
    body: unknown;
}

export class DwollaWebhookAttempt {
    id: string;
    request: DwollaWebhookAttemptResource;
    response: DwollaWebhookAttemptResource;
}

export class DwollaWebhook {
    id?: string;
    topic: DwollaTopic;
    accountId: string;
    eventId: string;
    subscriptionId: string;
    attempts: Array<DwollaWebhookAttempt>;
}
