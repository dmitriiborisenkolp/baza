import { DwollaCustomerId } from './customer-id';
import { DwollaBusinessType } from './business-type';
import { DwollaCustomerType } from './customer-type';
import { DwollaCustomerStatus } from './customer-status';
import { LinkEntity } from './link';
import { BusinessCustomerController } from '../requests/create-business-customer.request';

export class CustomerDetails {
    id: DwollaCustomerId;
    firstName: string;
    lastName: string;
    email: string;
    type: DwollaCustomerType;
    status: DwollaCustomerStatus;
    created: string;
    address1?: string;
    city?: string;
    state?: string;
    postalCode?: string;
    businessName?: string;
    controller?: BusinessCustomerController;
    businessType?: DwollaBusinessType;
    businessClassification?: string;
    _links?: LinkEntity;
}
