import { LinkEntity } from './link';
import { DwollaEventTopic } from './dwolla-event-topic';

export class DwollaEvent {
    _links: LinkEntity;
    id: string;
    resourceId: string;
    created: string;
    topic: DwollaEventTopic;
}
