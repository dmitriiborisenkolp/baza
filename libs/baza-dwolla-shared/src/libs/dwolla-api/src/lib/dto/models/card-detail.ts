export interface CardDetail {
    brand: string;
    lastFour: string;
    expirationMonth: number;
    expirationYear: number;
    nameOnCard: string;
}
