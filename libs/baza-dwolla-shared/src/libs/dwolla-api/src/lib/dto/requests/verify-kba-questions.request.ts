import { DwollaKbaAnswer } from '../models/dwolla-kba-answer';

export class VerifyKbaQuestionRequest {
    answers: Array<DwollaKbaAnswer>;
}
