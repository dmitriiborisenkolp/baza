import { LinkEntity } from './link';
import { DwollaDocumentType } from './dwolla-document-type';
import { DwollaDocumentStatus } from './dwolla-document-status';
import { DwollaDocumentVerificationStatus } from './dwolla-document-verification-status';
import { DwollaDocumentFailureReason } from './dwolla-document-failure-reason';

export const DWOLLA_DOCUMENT_UPLOAD_FILE_SIZE = 1048576; // in bytes
export const DWOLLA_DOCUMENT_UPLOAD_FILE_SIZE_HR = '10MB';

export const DWOLLA_DOCUMENT_UPLOAD_MIME_IMAGE = ['image/png', 'image/jpeg'];

export type DwollaFailureReasonDetail = {
    reason: DwollaDocumentFailureReason;
    description: string;
};

export type DwollaDocument = {
    _links: LinkEntity;
    id: string;
    status: DwollaDocumentStatus;
    type: DwollaDocumentType;
    created: string;
    documentVerificationStatus: DwollaDocumentVerificationStatus;
    failureReason?: DwollaDocumentFailureReason;
    allFailureReasons?: Array<DwollaFailureReasonDetail>;
};
