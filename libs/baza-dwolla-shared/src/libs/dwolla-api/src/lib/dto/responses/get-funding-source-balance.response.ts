import { DwollaBaseResponse } from '../../common';
import { DwollaAmount } from '../models/dwolla-amount';

export class GetFundingSourceBalanceResponse extends DwollaBaseResponse {
    balance: DwollaAmount;
    total: DwollaAmount;
    lastUpdated: string;
}
