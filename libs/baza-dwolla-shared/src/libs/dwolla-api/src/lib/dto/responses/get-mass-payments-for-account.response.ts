import { DwollaBaseResponse } from '../../common';
import { DwollaMassPaymentDetail } from '../models/dwolla-mass-payment-details';

export class GetMassPaymentsForAccountResponse extends DwollaBaseResponse {
    _embedded: {
        'mass-payments': DwollaMassPaymentDetail[];
    };
    total: number;
}
