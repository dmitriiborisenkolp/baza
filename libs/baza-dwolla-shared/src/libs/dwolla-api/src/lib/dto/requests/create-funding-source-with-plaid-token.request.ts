import { IsNotEmpty, IsString } from 'class-validator';
import { CreateFundingSourceRequest } from './create-funding-source.request';

export class CreateFundingSourceWithPlaidTokenRequest extends CreateFundingSourceRequest {
    @IsString()
    @IsNotEmpty()
    plaidToken: string;
}
