import { DwollaBaseResponse } from '../../common';
import { CustomerDetails } from '../models/customer-details';

export class GetCustomersResponse extends DwollaBaseResponse {
    _embedded: {
        customers: [CustomerDetails];
    };
    total: number;
}
