import { IsNotEmpty, IsString } from 'class-validator';
import { DwollaAccountId } from '../models/account-id';

export class GetAccountDetailsRequest {
    @IsString()
    @IsNotEmpty()
    accountId: DwollaAccountId;
}
