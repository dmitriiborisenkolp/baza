export enum DwollaDocumentVerificationStatus {
    Pending = 'pending',
    Accepted = 'accepted',
    Rejected = 'rejected',
}
