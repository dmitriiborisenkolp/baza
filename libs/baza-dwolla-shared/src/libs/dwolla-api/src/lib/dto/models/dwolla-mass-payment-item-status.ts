export enum DwollaMassPaymentItemStatus {
    Failed = 'failed',
    Pending = 'pending',
    Success = 'success',
}
