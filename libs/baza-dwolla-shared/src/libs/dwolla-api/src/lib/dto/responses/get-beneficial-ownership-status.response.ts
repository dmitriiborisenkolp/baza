import { DwollaBaseResponse } from '../../common';
import { DwollaBeneficialOwnershipStatus } from '../models/dwolla-beneficial-ownership-status';

export class GetBeneficialOwnershipStatusResponse extends DwollaBaseResponse {
    status: DwollaBeneficialOwnershipStatus;
}
