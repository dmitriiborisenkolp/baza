import { DwollaMassPaymentDetail } from "../models/dwolla-mass-payment-details";

export type GetMassPaymentResponse = DwollaMassPaymentDetail;
