export enum DwollaBankAccountType {
    Checking = 'checking',
    Savings = 'savings',
    GeneralLedger = 'general-ledger',
    Loan = 'loan',
}
