export type DwollaCustomerId = string;

export const DWOLLA_CUSTOMER_ID_LENGTH = 36; // 07d59716-ef22-4fe6-98e8-f3190233dfb8
