export enum DwollaEnvironment {
    Sandbox = 'sandbox',
    Production = 'production',
}
