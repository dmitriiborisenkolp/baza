import { DwollaBaseResponse } from '../../common';
import { TransferId } from '../models/transfer-id';
import { DwollaAmount } from '../models/dwolla-amount';
import { DwollaTransferStatus } from '../models/transfer-status';
import { DwollaClearing } from '../models/transfer-details';
import { Link } from '../models/link';

export class GetTransferDetailsLinkResponse {
    'source': Link;
    'destination-funding-source': Link;
    'self': Link;
    'funded-transfer': Link;
    'funding-transfer': Link;
    'source-funding-source': Link;
    'destination': Link;
}

export class GetTransferDetailsResponse extends DwollaBaseResponse<GetTransferDetailsLinkResponse> {
    id: TransferId;
    status: DwollaTransferStatus;
    amount: DwollaAmount;
    created: string;
    clearing?: DwollaClearing;
    individualAchId?: string;
}
