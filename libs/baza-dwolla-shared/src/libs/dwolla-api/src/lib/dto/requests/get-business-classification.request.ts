import { IsNotEmpty, IsString } from 'class-validator';
import { BusinessClassificationId } from '../models/business-classification-id';

export class GetBusinessClassificationRequest {
    @IsString()
    @IsNotEmpty()
    businessClassificationId: BusinessClassificationId;
}
