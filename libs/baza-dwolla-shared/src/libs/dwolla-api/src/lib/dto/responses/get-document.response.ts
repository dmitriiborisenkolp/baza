import { DwollaDocument } from '../models/dwolla-document';

export type GetDocumentResponse = DwollaDocument;
