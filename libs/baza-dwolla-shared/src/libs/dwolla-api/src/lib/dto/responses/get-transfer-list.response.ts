import { DwollaBaseResponse } from '../../common';
import { TransferDetails } from '../models/transfer-details';

export class GetTransferListResponse extends DwollaBaseResponse {
    _embedded: {
        transfers: Array<TransferDetails>;
    };
    total: number;
}
