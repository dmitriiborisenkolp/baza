import { DwollaBaseResponse } from '../../common';
import { DwollaDocument } from '../models/dwolla-document';

export class GetDocumentListResponse extends DwollaBaseResponse {
    '_embedded': {
        documents: Array<DwollaDocument>;
    };
    total: number;
}
