import { DwollaAmount } from './dwolla-amount';
import { LinkEntity } from './link';
import { TransferId } from './transfer-id';
import { DwollaTransferStatus } from './transfer-status';

export enum DwollaTransferClearingSource {
    Standard = 'standard',
    NextAvailable = 'next-available',
}

export enum DwollaTransferClearingDestination {
    NextAvailable = 'next-available',
}

export interface DwollaClearing {
    source?: DwollaTransferClearingSource;
    destination?: DwollaTransferClearingDestination;
}

export class TransferDetails {
    _links: LinkEntity;
    id: TransferId;
    status: DwollaTransferStatus;
    amount: DwollaAmount;
    created: string;
    clearing?: DwollaClearing;
    individualAchId?: string;
}
