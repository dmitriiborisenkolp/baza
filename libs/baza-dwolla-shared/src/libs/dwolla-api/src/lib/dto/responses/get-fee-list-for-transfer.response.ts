import { DwollaBaseResponse } from '../../common';
import { TransferDetails } from '../models/transfer-details';

export class GetFeeListForTransferResponse extends DwollaBaseResponse {
    '_embedded': {
        fees: Array<TransferDetails>;
    };
    transactions: Array<TransferDetails>;
    total: number;
}
