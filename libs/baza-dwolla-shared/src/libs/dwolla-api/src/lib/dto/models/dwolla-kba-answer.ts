export type DwollaKbaAnswer = Record<'questionId' | 'answerId', string>;
