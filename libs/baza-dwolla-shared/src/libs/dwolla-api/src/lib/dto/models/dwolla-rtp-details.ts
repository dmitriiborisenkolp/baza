export type DwollaRTPDetails = Record<'destination', { remittanceData: string }>;
