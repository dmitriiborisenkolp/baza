export enum DwollaDocumentFailureReason {
    BusinessDocNotSupported = 'BusinessDocNotSupported',
    BusinessNameMismatch = 'BusinessNameMismatch',
    BusinessTypeMismatch = 'BusinessTypeMismatch',
    ScanDobMismatch = 'ScanDobMismatch',
    ScanFailedOther = 'ScanFailedOther',
    ScanIdExpired = 'ScanIdExpired',
    ScanIdTypeNotSupported = 'ScanIdTypeNotSupported',
    ScanIdUnrecognized = 'ScanIdUnrecognized',
    ScanNameMismatch = 'ScanNameMismatch',
    ScanNotReadable = 'ScanNotReadable',
    ScanNotUploaded = 'ScanNotUploaded',
}
