export enum DwollaFailureCodes {
    R01 = 'R01',
    R02 = 'R02',
    R03 = 'R03',
    R04 = 'R04',
    R05 = 'R05',
    R06 = 'R06',
    R07 = 'R07',
    R08 = 'R08',
    R09 = 'R09',
    R10 = 'R10',
    R16 = 'R16',
    R20 = 'R20',
    R29 = 'R29',
}

export const dwollaFailureCodesI18n: Array<{
    code: DwollaFailureCodes;
    reason: string;
    details: string;
}> = [
    {
        code: DwollaFailureCodes.R01,
        reason: 'Insufficient Funds',
        details: 'Available balance is not sufficient to cover the dollar value of the debit entry.',
    },
    {
        code: DwollaFailureCodes.R02,
        reason: 'Bank Account Closed',
        details: 'Previously active account has been closed.',
    },
    {
        code: DwollaFailureCodes.R03,
        reason: 'No Account/Unable to Locate Account ',
        details: 'Account number structure is valid, but doesn’t match individual identified in entry or is not an open account.',
    },
    {
        code: DwollaFailureCodes.R04,
        reason: 'Invalid Bank Account Number Structure ',
        details: 'Account number structure is not valid.',
    },
    {
        code: DwollaFailureCodes.R05,
        reason: 'Unauthorized debit to consumer account ',
        details:
            'A debit entry was transmitted to a consumer account that was not authorized by the Receiver. Written Statement is required.',
    },
    {
        code: DwollaFailureCodes.R06,
        reason: 'Authorization Revoked by Customer ',
        details: 'Consumer who previously authorized entries has revoked authorization with the Originator. Written Statement is required.',
    },
    {
        code: DwollaFailureCodes.R07,
        reason: 'Payment Stopped',
        details: 'The Receiver has requested the stop payment of a specific ACH debit entry.',
    },
    {
        code: DwollaFailureCodes.R08,
        reason: 'Uncollected Funds',
        details: 'Sufficient balance exists, but value of uncollected items brings available balance below amount of debit entry.',
    },
    {
        code: DwollaFailureCodes.R09,
        reason: 'Customer Advises Originator is Not Known to Receiver and/or Originator is Not Authorized by Receiver to Debit Receiver’s Account ',
        details:
            'Receiver has no relationship with the Originator or has not authorized the Originator to debit the account. Written Statement is required.',
    },
    {
        code: DwollaFailureCodes.R10,
        reason: 'Customer Advises Entry Not in Accordance with the Terms of the Authorization. ',
        details:
            'The debit entry was inaccurate or improperly initiated. Other reasons include source document was ineligible, notice was not provided to the receive or amount was inaccurately obtained. Written statement is required.',
    },
    {
        code: DwollaFailureCodes.R16,
        reason: 'Bank Account Frozen',
        details: 'Funds unavailable due to action by the RDFI or legal action.',
    },
    {
        code: DwollaFailureCodes.R20,
        reason: 'Non-Transaction Account',
        details: 'RDFI policies/regulations restrict activity to account.',
    },
    {
        code: DwollaFailureCodes.R29,
        reason: 'Corporate Customer Advises Not Authorized ',
        details: 'Receiver has notified RDFI that corporate debit entry transmitted to a corporate account is not authorized.',
    },
];
