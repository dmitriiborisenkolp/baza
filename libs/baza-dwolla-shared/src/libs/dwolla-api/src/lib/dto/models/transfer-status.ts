export enum DwollaTransferStatus {
    Pending = 'pending',
    Processed = 'processed',
    Failed = 'failed',
    Cancelled = 'cancelled',
}
