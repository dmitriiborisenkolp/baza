import { IsEnum, IsNotEmpty } from 'class-validator';
import { DwollaBaseResponse } from '../../common';
import { DwollaBeneficialOwnershipStatus } from '../models/dwolla-beneficial-ownership-status';

export class UpdateBeneficialOwnershipStatusRequest extends DwollaBaseResponse {
    @IsEnum(DwollaBeneficialOwnershipStatus)
    @IsNotEmpty()
    status: DwollaBeneficialOwnershipStatus;
}
