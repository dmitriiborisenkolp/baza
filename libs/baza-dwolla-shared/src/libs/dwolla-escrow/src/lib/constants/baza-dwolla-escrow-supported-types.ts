import { DwollaFundingSourceType } from '../../../../dwolla-api/src';

/**
 * This constant is used as placeholder for Balance Funding Source
 */
export const BAZA_DWOLLA_BALANCE_REGISTRY_VALUE = 'BALANCE';

/**
 * List of Dwolla FS Types which can be used as Escrow Funding Source
 */
export const bazaDwollaEscrowSupportedTypes: Array<DwollaFundingSourceType> = [
    DwollaFundingSourceType.Bank,
    DwollaFundingSourceType.Balance,
];
