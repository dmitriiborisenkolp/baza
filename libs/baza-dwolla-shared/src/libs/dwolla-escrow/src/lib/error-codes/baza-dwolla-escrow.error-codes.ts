export enum BazaDwollaEscrowErrorCodes {
    BazaDwollaEscrowProductionRestricts = 'BazaDwollaEscrowProductionRestricts',
    BazaDwollaEscrowUATRestricts = 'BazaDwollaEscrowUATRestricts',
    BazaDwollaEscrowNotFound = 'BazaDwollaEscrowNotFound',
    BazaDwollaEscrowUnsupportedType = 'BazaDwollaEscrowUnsupportedType',
}

export const bazaDwollaEscrowErrorCodesEnI18n = {
    [BazaDwollaEscrowErrorCodes.BazaDwollaEscrowProductionRestricts]: 'Production environment restricts to use this option',
    [BazaDwollaEscrowErrorCodes.BazaDwollaEscrowUATRestricts]: 'UAT environment restricts to use this option',
    [BazaDwollaEscrowErrorCodes.BazaDwollaEscrowNotFound]: 'Dwolla EScrow Funding Source was not found',
    [BazaDwollaEscrowErrorCodes.BazaDwollaEscrowUnsupportedType]: '{{ type }} Funding Source cannot be selected as Escrow Funding Source',
};
