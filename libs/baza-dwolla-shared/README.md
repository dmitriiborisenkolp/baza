# baza-dwolla-shared

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-dwolla-shared` to execute the unit tests via [Jest](https://jestjs.io).

## Build

Run `nx build baza-dwolla-shared`

## Publish

Run `yarn publish:baza-dwolla-shared`
