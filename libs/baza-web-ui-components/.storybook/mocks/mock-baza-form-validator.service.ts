import { FormGroup } from '@angular/forms';
import { NzModalComponent } from 'ng-zorro-antd/modal';

export class MockBazaFormValidatorService {
    public isFormFilledAndValid(formGroup: FormGroup): boolean {
        return false;
    }

    public isFormValid(
        formGroup: FormGroup,
        formEl: HTMLFormElement = null,
        invalidFormScrollContainerRef: NzModalComponent | HTMLElement = null,
    ) {
        const isFormValid = formGroup.valid;

        if (!isFormValid) {
            Object.values(formGroup.controls).forEach((control) => {
                if (control?.invalid) {
                    control?.markAsDirty();
                    control?.updateValueAndValidity();
                }
            });
        }

        return isFormValid;
    }
}
