import { NavigationLink } from '@scaliolabs/baza-web-utils';

export class MockBazaLinkUtilSharedService {
    public navigate(linkToNavigate: NavigationLink): void {
        return;
    }

    public getUrlText(linkToNavigate: NavigationLink): string {
        return 'Url text';
    }
}
