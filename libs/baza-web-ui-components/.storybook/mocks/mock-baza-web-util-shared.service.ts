import { AmountValidationConfig } from '@scaliolabs/baza-web-utils';
import { FormControl } from '@angular/forms';

export class MockBazaWebUtilSharedService {
    public amountValidationConfig: AmountValidationConfig = {
        minValue: 0.1,
        maxValue: 1000000,
        precision: 2,
        step: 0.1,
        precisionMode: 'toFixed',
    };

    public getDollarParse = (value: string): string => value.replace('$', '').replace(',', '');

    public dollarFormat = (value: number): string => value?.toString() ?? '';

    public dollarParse = (value: string): string => this.getDollarParse(value);

    public getI18nLabel(path: string, title: string): string {
        return title.split('.').pop();
    }

    public geti18nValidationErrorMessages(config: { control: FormControl }): string {
        const errors = Object.keys(config?.control?.errors).join(', ');

        return `Errors: ${errors}`;
    }

    public mergeConfig<ConfigType = Record<string, unknown>>(
        defaultConfig: ConfigType,
        overrideConfig: Partial<ConfigType> | Record<string, unknown>,
    ): ConfigType {
        return defaultConfig;
    }
}
