import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {
    AmountValidationConfig,
    BazaWebUtilSharedService,
    i18nValidationTypesEnum,
    Message,
    ValidationsPreset,
} from '@scaliolabs/baza-web-utils';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-withdraw-funds-form',
    templateUrl: './withdraw-funds-form.component.html',
    styleUrls: ['./withdraw-funds-form.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WithdrawFundsFormComponent implements OnInit {
    @Input()
    withdrawFundsForm?: FormGroup;

    @Input()
    withdrawAmountErrorMessage$?: Observable<Message | undefined>;

    @Input()
    walletAmount?: number;

    @ViewChild('withdrawFundsFormEl', { static: false }) withdrawFundsFormRef: ElementRef;

    i18nBasePath = 'uic.withdrawFunds.form';
    i18nFormFieldsPath = `${this.i18nBasePath}.fields`;

    amountValidationConfig: AmountValidationConfig;

    public get amountValidationMaxLength() {
        return this.wts.getDollarParse(this.wts.amountValidationConfig.maxValue.toString()).length;
    }

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit(): void {
        this.amountValidationConfig = {
            ...this.wts.amountValidationConfig,
            minValue: 0.01,
            step: 0.01,
            maxValue: this.walletAmount,
        };
    }

    public getErrorMessage(control: FormControl, controlName: string): string {
        const validationsPreset: ValidationsPreset = new Map([['amount', [{ key: i18nValidationTypesEnum.required }]]]);

        return this.wts.geti18nValidationErrorMessages({
            control,
            controlName,
            i18nFormFieldsPath: this.i18nFormFieldsPath,
            i18nGenericValidationsPath: `${this.i18nBasePath}.genericValidators`,
            validationsPreset,
        });
    }
}
