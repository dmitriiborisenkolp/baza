export const WebUiWithdrawFundsFormEnI18n = {
    fields: {
        amount: {
            label: 'Withdrawal Amount',
            validators: {
                required: 'Please enter the amount to be withdrawn',
            },
        },
    },
    alerts: {
        withdrawalError:
            'The funds were not withdrawn from the account. <br/> Please try again later or reach out to <a href="{{ link }}"> Technical support',
    },
};
