import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * Customization input config for banner component
 */
export interface BannerConfig {
    /**
     * Displays the subtitle for component
     */
    subtitle?: string;
    /**
     * Displays the heading for component
     */
    heading?: string;
    /**
     * Displays the sub-heading for component
     */
    subHeading?: string;
    /**
     * Used to configure internal or external route for button
     */
    btnLink?: LinkConfig;
}
