import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { BannerComponent } from './banner.component';

const NZ_Modules = [NzGridModule, NzButtonModule];

@NgModule({
    declarations: [BannerComponent],
    imports: [CommonModule, RouterModule, UtilModule, ...NZ_Modules],
    exports: [BannerComponent],
})
export class BannerModule {}
