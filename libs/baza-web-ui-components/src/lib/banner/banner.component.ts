import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { BannerConfig } from './models/banner-config.interface';

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BannerComponent implements OnInit {
    @Input()
    config!: BannerConfig;

    constructor(public readonly uts: BazaLinkUtilSharedService, private readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        this.checkDefaultConfig();
    }

    checkDefaultConfig() {
        const defaultConfig: BannerConfig = {
            subtitle: `Subtitle`,
            heading: `We analyze billions of dollars worth of deals on your behalf`,
            subHeading: `Ut dictum turpis vulputate nunc id eget mauris elit, pellentesque. Faucibus quis dignissim varius vel. Blandit.`,
            btnLink: {
                appLink: {
                    commands: ['/items'],
                    text: 'View Offering',
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
