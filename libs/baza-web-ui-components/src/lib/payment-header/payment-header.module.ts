import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentHeaderComponent } from './payment-header.component';

@NgModule({
    declarations: [PaymentHeaderComponent],
    exports: [PaymentHeaderComponent],
    imports: [CommonModule, RouterModule, UtilModule],
})
export class PaymentHeaderModule {}
