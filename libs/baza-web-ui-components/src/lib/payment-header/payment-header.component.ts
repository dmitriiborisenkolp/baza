import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'app-payment-header',
    templateUrl: './payment-header.component.html',
    styleUrls: ['./payment-header.component.less'],
    changeDetection: ChangeDetectionStrategy.Default,
})
export class PaymentHeaderComponent {
    @Input()
    isBackBtnVisible: boolean;

    @Output()
    backClicked: EventEmitter<void> = new EventEmitter();

    @Output()
    closeClicked: EventEmitter<void> = new EventEmitter();

    public onBackClick() {
        this.backClicked.emit();
    }

    public onCloseClick() {
        this.closeClicked.emit();
    }
}
