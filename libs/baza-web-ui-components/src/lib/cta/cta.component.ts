import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { CTAConfig } from './models/cta-config.interface';

@Component({
    selector: 'app-cta',
    templateUrl: './cta.component.html',
    styleUrls: ['./cta.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CTAComponent implements OnInit {
    @Input()
    config!: CTAConfig;

    constructor(public readonly uts: BazaLinkUtilSharedService, private readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        this.checkDefaultConfig();
    }

    checkDefaultConfig() {
        const defaultConfig: CTAConfig = {
            heading: `Start powering your portfolio today`,
            descr: `Go from zero, to "property owner" with just a few clicks`,
            btnLink: {
                appLink: {
                    commands: ['/items'],
                    text: 'Get Started',
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
