import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * Customization input config for CTA component
 */
export interface CTAConfig {
    /**
     * Displays the heading for component
     */
    heading?: string;
    /**
     * Displays the description for component
     */
    descr?: string;
    /**
     * Used to configure internal or external route for button
     */
    btnLink: LinkConfig;
}
