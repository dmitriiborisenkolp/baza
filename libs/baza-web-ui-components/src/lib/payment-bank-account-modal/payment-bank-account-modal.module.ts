import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { PaymentBankAccountModalComponent } from './payment-bank-account-modal.component';
import { BankDetailsModule } from '../bank-details';
import { PaymentHeaderModule } from '../payment-header';

@NgModule({
    declarations: [PaymentBankAccountModalComponent],
    imports: [BankDetailsModule, CommonModule, NzButtonModule, NzModalModule, PaymentHeaderModule],
    exports: [PaymentBankAccountModalComponent],
})
export class PaymentBankAccountModalModule {}
