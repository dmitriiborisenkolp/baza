import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BazaWebUtilSharedService, i18nValidationTypesEnum, Message, ValidationsPreset } from '@scaliolabs/baza-web-utils';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-add-funds-form',
    templateUrl: './add-funds-form.component.html',
    styleUrls: ['./add-funds-form.component.less'],
})
export class DwollaAddFundsFormComponent {
    @Input()
    addFundsForm: FormGroup;

    @Input()
    transferAmountErrorMessage$: Observable<Message | undefined>;

    @ViewChild('addFundsFormEl', { static: false }) addFundsFormRef: ElementRef;

    i18nBasePath = 'uic.addFunds.form';
    i18nFormFieldsPath = `${this.i18nBasePath}.fields`;

    public get amountValidationMaxLength() {
        return this.wts.getDollarParse(this.wts.amountValidationConfig.maxValue.toString()).length;
    }

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    public getErrorMessage(control: FormControl, controlName: string): string {
        const validationsPreset: ValidationsPreset = new Map([['amount', [{ key: i18nValidationTypesEnum.required }]]]);

        return this.wts.geti18nValidationErrorMessages({
            control,
            controlName,
            i18nFormFieldsPath: this.i18nFormFieldsPath,
            i18nGenericValidationsPath: `${this.i18nBasePath}.genericValidators`,
            validationsPreset,
        });
    }
}
