import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { DwollaAddFundsFormComponent } from './add-funds-form.component';

@NgModule({
    declarations: [DwollaAddFundsFormComponent],
    imports: [CommonModule, FormsModule, NzFormModule, NzInputNumberModule, ReactiveFormsModule, RouterModule, UtilModule, NzAlertModule],
    exports: [DwollaAddFundsFormComponent],
    providers: [],
})
export class DwollaAddFundsFormModule {}
