import { RangedArray } from '@scaliolabs/baza-web-utils';
/**
 * Displays the bullet text. A minimum of 2 and maximum of 4 string elements can be defined in array
 */
export type BulletsTextConfig = RangedArray<string, 2, 4>;
