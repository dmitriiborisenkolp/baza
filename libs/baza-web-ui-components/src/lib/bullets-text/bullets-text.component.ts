import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BulletsTextConfig } from './models/bullets-text-config.interface';

@Component({
    selector: 'app-bullets-text',
    templateUrl: './bullets-text.component.html',
    styleUrls: ['./bullets-text.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BulletsTextComponent {
    @Input()
    bullets: BulletsTextConfig = ['Arcu mauris lectus', 'Arcu mauris lectus', 'Arcu mauris lectus', 'Arcu mauris lectus'];
}
