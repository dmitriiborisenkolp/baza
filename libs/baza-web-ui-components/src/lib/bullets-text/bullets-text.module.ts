import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { BulletsTextComponent } from './bullets-text.component';

const NZ_Modules = [NzGridModule];

@NgModule({
    declarations: [BulletsTextComponent],
    imports: [CommonModule, RouterModule, UtilModule, ...NZ_Modules],
    exports: [BulletsTextComponent],
})
export class BulletsTextModule {}
