import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Output, Input, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { PhoneCountryCode, PhoneCodeState, SearchPhoneCodes } from './data-access';

@UntilDestroy()
@Component({
    selector: 'app-phone-code',
    templateUrl: './phone-code.component.html',
    styleUrls: ['./phone-code.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PhoneCodeComponent implements OnInit {
    codes$ = this.store.select(PhoneCodeState.items);

    @Input()
    numericCode: string;

    phoneCode: PhoneCountryCode;

    @Output()
    codeChange = new EventEmitter<PhoneCountryCode>();

    public searchBy: string;

    constructor(private readonly store: Store, private readonly cdRef: ChangeDetectorRef) {}

    ngOnInit(): void {
        this.codes$.pipe(untilDestroyed(this)).subscribe((codes) => {
            // set default phoneCode
            if (codes.length && !this.phoneCode) {
                this.phoneCode = codes.find((code) => code.numericCode === this.numericCode);
                this.codeChange.emit(this.phoneCode);
            }
        });
    }

    public onCodeChange(code: PhoneCountryCode) {
        this.phoneCode = code;
        this.codeChange.emit(this.phoneCode);
    }

    public onFocusOut() {
        // for some reason there is an issue with updating state of input
        // and we need to update it manually
        this.cdRef.detectChanges();
    }

    public onCompare = (o1: PhoneCountryCode, o2: PhoneCountryCode) => (o1 && o2 ? o1.countryCode === o2.countryCode : o1 === o2);

    public onSearch(search: string) {
        this.store.dispatch(new SearchPhoneCodes(search));
    }
}
