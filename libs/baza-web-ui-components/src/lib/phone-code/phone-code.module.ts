import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { GetPhoneCodes, PhoneCodeStateModule } from './data-access';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { PhoneCodeComponent } from './phone-code.component';
import { Store } from '@ngxs/store';

@NgModule({
    declarations: [PhoneCodeComponent],
    exports: [PhoneCodeComponent],
    imports: [CommonModule, FormsModule, NzEmptyModule, NzInputModule, NzSelectModule, PhoneCodeStateModule, RouterModule, UtilModule],
})
export class PhoneCodeModule {
    constructor(private readonly store: Store) {
        this.store.dispatch(new GetPhoneCodes());
    }
}
