import { PhoneCountryCodesDto } from '@scaliolabs/baza-core-shared';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface PhoneCountryCode extends PhoneCountryCodesDto {}
