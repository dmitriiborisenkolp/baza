export class GetPhoneCodes {
    static readonly type = '[PhoneCode] GetPhoneCodes';
}

export class SearchPhoneCodes {
    static readonly type = '[PhoneCode] SearchPhoneCodes';

    constructor(public search: string) {}
}
