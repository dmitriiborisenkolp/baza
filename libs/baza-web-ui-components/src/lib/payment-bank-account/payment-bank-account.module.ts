import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { PaymentBankAccountComponent } from './payment-bank-account.component';

@NgModule({
    declarations: [PaymentBankAccountComponent],
    imports: [CommonModule, NzButtonModule, NzGridModule, UtilModule],
    exports: [PaymentBankAccountComponent],
})
export class PaymentBankAccountModule {}
