export const WebUiPaymentBankAccountEnI18n = {
    addMode: {
        plaidLinking: 'Link Bank Account',
        manualLinking: 'Bank Account',
    },
    editMode: {
        account: {
            name: 'Account holder name:',
            number: 'Account number:',
            type: 'Account type:',
        },
    },
};
