import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BazaNcBankAccountAchDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

export type PaymentBankAccountStyle = 'Simple' | 'Bordered';

@Component({
    selector: 'app-payment-bank-account',
    templateUrl: './payment-bank-account.component.html',
    styleUrls: ['./payment-bank-account.component.less'],
})
export class PaymentBankAccountComponent {
    @Input()
    srcData: BazaNcBankAccountAchDto;

    @Input()
    style: PaymentBankAccountStyle = 'Simple';

    @Input()
    isEditModeOn: boolean;

    @Input()
    isAddModeOn: boolean;

    @Input()
    disablePlaidACHLinking: boolean;

    @Output()
    bankLinkClicked = new EventEmitter();

    @Output()
    manualLinkClicked = new EventEmitter();

    public i18nBasePath = 'uic.paymentBank';

    constructor(public readonly wts: BazaWebUtilSharedService) {}
}
