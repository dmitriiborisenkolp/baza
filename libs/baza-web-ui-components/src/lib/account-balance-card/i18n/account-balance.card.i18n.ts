export const WebUiAccountBalanceCardEnI18n = {
    title: 'Account Balance',
    prefix: 'Current balance:',
    hint: 'Fund your account',
    actions: {
        withdraw: 'Withdraw',
        addFunds: 'Add Funds',
    },
};
