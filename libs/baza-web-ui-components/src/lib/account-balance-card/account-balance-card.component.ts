import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { AccountBalanceCardConfig } from './models/account-balance-card.config';

@Component({
    selector: 'app-account-balance-card',
    templateUrl: './account-balance-card.component.html',
    styleUrls: ['./account-balance-card.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountBalanceCardComponent {
    @Input()
    config?: AccountBalanceCardConfig;

    @Output()
    accountBalanceClicked = new EventEmitter();

    @Output()
    withdrawClicked = new EventEmitter();

    @Output()
    addFundsClicked = new EventEmitter();

    public i18nBasePath = 'uic.balance';

    constructor(public readonly wts: BazaWebUtilSharedService) {}
}
