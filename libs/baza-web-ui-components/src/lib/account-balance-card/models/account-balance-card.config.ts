import { DwollaAmount } from '@scaliolabs/baza-dwolla-shared';

/**
 * Customize input config for AccountBalanceCard component
 */
export interface AccountBalanceCardConfig {
    /*
     * The source data to display in the AccountBalanceCard component.
     */
    srcData?: DwollaAmount;

    /**
     * A flag indicating whether the AccountBalanceCard component is disabled.
     */
    isDisabled?: boolean;

    /**
     * A flag indicating whether the AccountBalanceCard component is in add mode.
     */
    isAddModeOn?: boolean;

    /**
     * A flag indicating whether the AccountBalanceCard component is in edit mode.
     */
    isEditModeOn?: boolean;

    /**
     * A flag indicating whether to show the withdraw button in the AccountBalanceCard component.
     */
    showWithdrawButton?: boolean;

    /**
     * A flag indicating whether the withdraw button is disabled in the AccountBalanceCard component.
     */
    isWithdrawDisabled?: boolean;

    /**
     * A flag indicating whether the AccountBalanceCard component has enough funds.
     */
    hasEnoughFunds?: boolean;

    /**
     * A flag indicating whether to show the add funds button in the AccountBalanceCard component.
     */
    showAddFundsButton?: boolean;
}
