import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { AccountBalanceCardComponent } from './account-balance-card.component';

@NgModule({
    declarations: [AccountBalanceCardComponent],
    imports: [CommonModule, RouterModule, UtilModule, NzButtonModule],
    exports: [AccountBalanceCardComponent],
})
export class AccountBalanceCardModule {}
