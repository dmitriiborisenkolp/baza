import { WebUiAccountBalanceCardEnI18n } from '../account-balance-card/i18n/account-balance.card.i18n';
import { WebUiAddFundsFormEnI18n } from '../add-funds-form/i18n/webui-add-funds-form.i18n';
import { WebUiAddFundsModalEnI18n } from '../add-funds-modal/i18n/webui-add-funds-modal.i18n';
import { WebUiAddFundsNotificationEnI18n } from '../add-funds-notification/i18n/webui-add-funds-notification.i18n';
import { WebUiBankDetailsEnI18n } from '../bank-details/i18n/bank-details.i18n';
import { WebUiCardDetailsEnI18n } from '../card-details/i18n/card-details.i18n';
import { WebUiPaymentBankAccountModalEnI18n } from '../payment-bank-account-modal/i18n/payment-bank-account-modal.i18n';
import { WebUiPaymentBankAccountEnI18n } from '../payment-bank-account/i18n/payment-bank-account.i18n';
import { WebUiPaymentCardModalEnI18n } from '../payment-card-modal/i18n/payment-card-modal.i18n';
import { WebUiPaymentCardEnI18n } from '../payment-card/i18n/payment-card.i18n';
import { WebUiPersonalInfoEnI18n } from '../personal-info/i18n/personal-info.i18n';
import { WebUiSummaryEnI18n } from '../summary/i18n/summary.i18n';
import { WebUiWithdrawFundsFormEnI18n } from '../withdraw-funds-form/i18n/webui-withdraw-funds-form.i18n';
import { WebUiWithdrawFundsModalEnI18n } from '../withdraw-funds-modal/i18n/webui-withdraw-funds-modal.i18n';
import { WebUiWithdrawFundsNotificationEnI18n } from '../withdraw-funds-notification/i18n/webui-withdraw-funds-notification.i18n';

export const BazaWebUiEnI18n = {
    uic: {
        balance: WebUiAccountBalanceCardEnI18n,
        summary: WebUiSummaryEnI18n,
        paymentBank: WebUiPaymentBankAccountEnI18n,
        paymentBankModal: WebUiPaymentBankAccountModalEnI18n,
        bankDetails: WebUiBankDetailsEnI18n,
        paymentCard: WebUiPaymentCardEnI18n,
        paymentCardModal: WebUiPaymentCardModalEnI18n,
        cardDetails: WebUiCardDetailsEnI18n,
        personal: WebUiPersonalInfoEnI18n,
        addFunds: {
            form: WebUiAddFundsFormEnI18n,
            modal: WebUiAddFundsModalEnI18n,
            notification: WebUiAddFundsNotificationEnI18n,
        },
        withdrawFunds: {
            form: WebUiWithdrawFundsFormEnI18n,
            modal: WebUiWithdrawFundsModalEnI18n,
            notification: WebUiWithdrawFundsNotificationEnI18n,
        },
    },
};
