import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { DwollaAddFundsModalComponent } from './add-funds-modal.component';
import { FormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { DwollaAddFundsFormModule } from '../add-funds-form/add-funds-form.module';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentHeaderModule } from '../payment-header';

@NgModule({
    imports: [
        DwollaAddFundsFormModule,
        CommonModule,
        UtilModule,
        FormsModule,
        NzInputModule,
        NzButtonModule,
        NzModalModule,
        PaymentHeaderModule,
    ],
    exports: [DwollaAddFundsModalComponent],
    declarations: [DwollaAddFundsModalComponent],
    providers: [],
})
export class DwollaAddFundsModalModule {}
