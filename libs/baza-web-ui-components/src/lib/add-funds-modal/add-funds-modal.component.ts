import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { BazaNcBankAccountGetDefaultByTypeResponse } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService, Message } from '@scaliolabs/baza-web-utils';
import { Observable } from 'rxjs';
import { DwollaAddFundsFormComponent } from '../add-funds-form/add-funds-form.component';

@Component({
    selector: 'app-add-funds-modal',
    templateUrl: './add-funds-modal.component.html',
    styleUrls: ['./add-funds-modal.component.less'],
})
export class DwollaAddFundsModalComponent {
    @Input()
    addFundsForm: FormGroup;

    @Input()
    isAddFundsFormVisible: boolean;

    @Input()
    dwollaDefaultCashInAccount: BazaNcBankAccountGetDefaultByTypeResponse;

    @Input()
    transferAmountErrorMessage$: Observable<Message | undefined>;

    @Output()
    handleAddFundsCancel = new EventEmitter();

    @Output()
    handleEditCashInAccount = new EventEmitter();

    @Output()
    submitAddFundsForm = new EventEmitter();

    @ViewChild('addFundsFormComp', { static: false }) addFundsFormCompRef: DwollaAddFundsFormComponent;

    i18nBasePath = 'uic.addFunds.modal';

    constructor(public readonly bazaFormValidatorService: BazaFormValidatorService, public readonly wts: BazaWebUtilSharedService) {}

    onFormSubmit(modalEl = null): void {
        const formElRef = this.addFundsFormCompRef?.addFundsFormRef?.nativeElement ?? null;

        if (this.bazaFormValidatorService.isFormValid(this.addFundsForm, formElRef, modalEl)) {
            this.submitAddFundsForm.emit();
        }
    }
}
