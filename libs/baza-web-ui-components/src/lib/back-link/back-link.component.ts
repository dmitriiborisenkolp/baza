import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { BazaLinkUtilSharedService, LinkConfig } from '@scaliolabs/baza-web-utils';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { LeaveConfirmationComponent } from '../leave-confirmation';

@Component({
    selector: 'app-back-link',
    templateUrl: './back-link.component.html',
    styleUrls: ['./back-link.component.less'],
})
export class BackLinkComponent {
    @Input()
    text?: string;

    @Input()
    linkConfig?: LinkConfig;

    @Input()
    showLeaveConfirmation: boolean;

    @Input()
    leaveConfirmationTitle: string;

    @Input()
    leaveConfirmationText: string;

    @Output()
    backEvent: EventEmitter<void> = new EventEmitter();

    constructor(
        private readonly modalService: NzModalService,
        private readonly router: Router,
        private readonly uts: BazaLinkUtilSharedService,
    ) {}

    public back() {
        if (this.showLeaveConfirmation) {
            const modalRef: NzModalRef = this.modalService.create({
                nzAutofocus: null,
                nzClassName: 'ant-modal-sm',
                nzClosable: true,
                nzContent: LeaveConfirmationComponent,
                nzComponentParams: {
                    text: this.leaveConfirmationText,
                } as LeaveConfirmationComponent,
                nzFooter: [
                    {
                        label: 'Cancel',
                        type: 'default',
                        onClick: () => modalRef.destroy(),
                    },
                    {
                        label: 'Exit',
                        type: 'primary',
                        onClick: () => {
                            modalRef.destroy();
                            this.navigate();
                        },
                    },
                ],
                nzTitle: this.leaveConfirmationTitle ? this.leaveConfirmationTitle : 'Are you sure you want to exit?',
                nzWidth: 500,
            });
        } else {
            this.navigate();
        }
    }

    private navigate() {
        this.backEvent.emit();

        if (this.linkConfig) {
            this.uts.navigate(this.linkConfig);
        }
    }
}
