import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { BackLinkComponent } from './back-link.component';

@NgModule({
    declarations: [BackLinkComponent],
    exports: [BackLinkComponent],
    imports: [CommonModule, RouterModule, UtilModule],
    providers: [NzModalService],
})
export class BackLinkModule {}
