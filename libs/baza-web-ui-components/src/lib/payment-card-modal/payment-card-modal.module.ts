import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { PaymentCardModalComponent } from './payment-card-modal.component';
import { CardDetailsModule } from '../card-details';
import { PaymentHeaderModule } from '../payment-header';

@NgModule({
    declarations: [PaymentCardModalComponent],
    imports: [CommonModule, CardDetailsModule, PaymentHeaderModule, NzButtonModule, NzModalModule],
    exports: [PaymentCardModalComponent],
})
export class PaymentCardModalModule {}
