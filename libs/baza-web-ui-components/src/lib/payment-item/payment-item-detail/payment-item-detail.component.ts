import { ChangeDetectionStrategy, Component, TemplateRef, ViewChild } from '@angular/core';

@Component({
    selector: 'app-payment-item-detail',
    templateUrl: './payment-item-detail.component.html',
    styleUrls: ['./payment-item-detail.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentItemDetailComponent {
    @ViewChild(TemplateRef)
    content: TemplateRef<HTMLElement>;
}
