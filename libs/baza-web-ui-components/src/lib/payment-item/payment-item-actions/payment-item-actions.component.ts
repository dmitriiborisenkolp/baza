import { ChangeDetectionStrategy, Component, TemplateRef, ViewChild } from '@angular/core';

@Component({
    selector: 'app-payment-item-actions',
    templateUrl: './payment-item-actions.component.html',
    styleUrls: ['./payment-item-actions.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentItemActionsComponent {
    @ViewChild(TemplateRef)
    content: TemplateRef<HTMLElement>;
}
