import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { WhyInvestCardModule } from '../why-invest-card';
import { WhyInvestComponent } from './why-invest.component';

const NZ_Modules = [NzGridModule];

@NgModule({
    declarations: [WhyInvestComponent],
    imports: [CommonModule, RouterModule, UtilModule, WhyInvestCardModule, ...NZ_Modules],
    exports: [WhyInvestComponent],
})
export class WhyInvestModule {}
