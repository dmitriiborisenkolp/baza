/**
 * Customization input config for Why Invest component
 */
export interface WhyInvestConfig {
    /**
     * Displays the heading for component
     */
    heading?: string;
    /**
     * Displays the sub-heading for component
     */
    subHeading?: string;
    /**
     * Customization config for sections of How It Works component
     */
    sectionsConfig?: Array<WISectionConfig>;
}

export interface WISectionConfig {
    /**
     * Displays the SVG icon sprite
     */
    icon?: string;
    /**
     * Displays the title for section
     */
    title?: string;
    /**
     * Displays the description for section
     */
    descr?: string;
}
