import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { WhyInvestConfig } from './models/why-invest-config.interface';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

@Component({
    selector: 'app-why-invest',
    templateUrl: './why-invest.component.html',
    styleUrls: ['./why-invest.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhyInvestComponent implements OnInit {
    @Input()
    config!: WhyInvestConfig;

    constructor(private wts: BazaWebUtilSharedService) {}

    ngOnInit(): void {
        this.checkDefaultConfig();
    }

    checkDefaultConfig() {
        const defaultConfig: WhyInvestConfig = {
            heading: 'Your key to exclusive private market investing',
            subHeading: 'Nunc, dictumst odio aenean et nec ipsum sed mollis.',
            sectionsConfig: [],
        };

        const defaultSectionConfig = {
            icon: 'point',
            title: 'Morbi nunc, consectetur ac gravida here.',
            descr: 'Lectus odio at felis faucibus a tincidunt duis nunc.',
        };

        for (let i = 0; i < 6; i++) {
            defaultConfig.sectionsConfig.push(defaultSectionConfig);
        }

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
