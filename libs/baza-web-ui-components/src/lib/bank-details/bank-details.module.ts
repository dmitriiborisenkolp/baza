import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { BankDetailsComponent } from './bank-details.component';

@NgModule({
    declarations: [BankDetailsComponent],
    exports: [BankDetailsComponent],
    imports: [CommonModule, FormsModule, NzFormModule, NzInputModule, NzSelectModule, ReactiveFormsModule, RouterModule, UtilModule],
})
export class BankDetailsModule {}
