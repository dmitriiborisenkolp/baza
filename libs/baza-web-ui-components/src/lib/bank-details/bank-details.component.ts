import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BazaWebUtilSharedService, i18nValidationTypesEnum, ValidationsPreset } from '@scaliolabs/baza-web-utils';

@Component({
    selector: 'app-bank-details',
    templateUrl: './bank-details.component.html',
    styleUrls: ['./bank-details.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BankDetailsComponent implements OnInit {
    @Input()
    bankDetailsForm: FormGroup;

    @ViewChild('bankDetailsFormEl', { static: false }) bankDetailsFormRef: ElementRef;

    public readonly minLengthAccountNumber = 6;
    public readonly lengthRoutingNumber = 9;

    public i18nBasePath = 'uic.bankDetails';
    public i18nFormPath = `${this.i18nBasePath}.form`;
    public i18nFormFieldsPath = `${this.i18nBasePath}.form.fields`;

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        if (!this.bankDetailsForm.pristine) {
            this.bankDetailsForm.reset();
        }
    }

    public getErrorMessage(control: FormControl, controlName: string): string {
        const validationsPreset: ValidationsPreset = new Map([
            ['accountName', [{ key: i18nValidationTypesEnum.restrictedChars }]],
            ['accountNumber', [{ key: i18nValidationTypesEnum.minlength, params: { length: this.minLengthAccountNumber } }]],
            [
                'accountRoutingNumber',
                [
                    { key: i18nValidationTypesEnum.onlynumbers },
                    { key: i18nValidationTypesEnum.minlength, params: { min: this.lengthRoutingNumber } },
                    { key: i18nValidationTypesEnum.maxlength, params: { max: this.lengthRoutingNumber } },
                    { key: i18nValidationTypesEnum.routingNumber },
                ],
            ],
        ]);

        return this.wts.geti18nValidationErrorMessages({
            control,
            controlName,
            i18nFormFieldsPath: this.i18nFormFieldsPath,
            i18nGenericValidationsPath: `${this.i18nFormPath}.genericValidators`,
            validationsPreset,
        });
    }
}
