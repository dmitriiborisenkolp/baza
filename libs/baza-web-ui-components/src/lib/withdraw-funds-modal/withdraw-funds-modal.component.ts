import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { DwollaAmount } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcBankAccountGetDefaultByTypeResponse } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService, Message, PriceCentsPipe } from '@scaliolabs/baza-web-utils';
import { Observable } from 'rxjs';
import { WithdrawFundsFormComponent } from '../withdraw-funds-form/withdraw-funds-form.component';

@Component({
    selector: 'app-withdraw-funds-modal',
    templateUrl: './withdraw-funds-modal.component.html',
    styleUrls: ['./withdraw-funds-modal.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WithdrawFundsModalComponent implements OnInit {
    @Input()
    withdrawFundsForm?: FormGroup;

    @Input()
    dwollaDefaultCashOutAccount?: BazaNcBankAccountGetDefaultByTypeResponse;

    @Input()
    withdrawAmountErrorMessage$?: Observable<Message | undefined>;

    @Input()
    dwollaAmount?: DwollaAmount;

    @Output()
    withdrawFundsCancel = new EventEmitter();

    @Output()
    submitWithdrawFundsForm = new EventEmitter();

    @ViewChild('withdrawFundsFormComp', { static: false }) withdrawFundsFormCompRef: WithdrawFundsFormComponent;

    walletAmount = 0;
    showWithdrawForm = false;
    i18nBasePath = 'uic.withdrawFunds.modal';

    constructor(
        public readonly bazaFormValidatorService: BazaFormValidatorService,
        public readonly wts: BazaWebUtilSharedService,
        private readonly pricePipe: PriceCentsPipe,
    ) {}

    ngOnInit(): void {
        this.walletAmount = +(this.dwollaAmount?.value ?? '0');
        this.showWithdrawForm = true;
    }

    onFormSubmit(modalEl = null): void {
        const formElRef = this.withdrawFundsFormCompRef?.withdrawFundsFormRef?.nativeElement ?? null;

        if (this.bazaFormValidatorService.isFormValid(this.withdrawFundsForm, formElRef, modalEl)) {
            this.submitWithdrawFundsForm.emit();
        }
    }

    get balanceLabel() {
        return this.wts.getI18nLabel(this.i18nBasePath, 'subtitle', { amount: this.pricePipe.transform(this.walletAmount) });
    }
}
