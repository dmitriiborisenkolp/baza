import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { WithdrawFundsModalComponent } from './withdraw-funds-modal.component';
import { FormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { WithdrawFundsFormModule } from '../withdraw-funds-form/withdraw-funds-form.module';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentHeaderModule } from '../payment-header';

@NgModule({
    imports: [
        WithdrawFundsFormModule,
        CommonModule,
        UtilModule,
        FormsModule,
        NzInputModule,
        NzButtonModule,
        NzModalModule,
        PaymentHeaderModule,
    ],
    exports: [WithdrawFundsModalComponent],
    declarations: [WithdrawFundsModalComponent],
    providers: [],
})
export class WithdrawFundsModalModule {}
