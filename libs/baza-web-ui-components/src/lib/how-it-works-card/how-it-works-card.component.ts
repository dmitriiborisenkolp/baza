import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-how-it-works-card',
    templateUrl: './how-it-works-card.component.html',
    styleUrls: ['./how-it-works-card.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HowItWorksCardComponent {
    @Input()
    index!: number;

    @Input()
    title!: string;

    @Input()
    descr!: string;
}
