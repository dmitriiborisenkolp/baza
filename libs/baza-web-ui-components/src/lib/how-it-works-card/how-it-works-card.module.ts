import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzCardModule } from 'ng-zorro-antd/card';
import { HowItWorksCardComponent } from './how-it-works-card.component';

const NZ_Modules = [NzCardModule];

@NgModule({
    declarations: [HowItWorksCardComponent],
    imports: [CommonModule, RouterModule, UtilModule, ...NZ_Modules],
    exports: [HowItWorksCardComponent],
})
export class HowItWorksCardModule {}
