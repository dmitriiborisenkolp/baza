import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PersonalInfoComponent } from './personal-info.component';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzAlertModule } from 'ng-zorro-antd/alert';

@NgModule({
    declarations: [PersonalInfoComponent],
    imports: [CommonModule, NzPopoverModule, RouterModule, UtilModule, NzSkeletonModule, NzAlertModule],
    exports: [PersonalInfoComponent],
})
export class PersonalInfoModule {}
