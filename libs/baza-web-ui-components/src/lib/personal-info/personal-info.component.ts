import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountVerificationPersonalInformationDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

@Component({
    selector: 'app-personal-info',
    templateUrl: './personal-info.component.html',
    styleUrls: ['./personal-info.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersonalInfoComponent {
    @Input()
    info$: Observable<AccountVerificationPersonalInformationDto>;

    @Output()
    editPersonalInfo: EventEmitter<boolean> = new EventEmitter<boolean>();

    public i18nBasePath = 'uic.personal';

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    public getFormattedAddress(info: AccountVerificationPersonalInformationDto): string {
        let address = `${info.residentialStreetAddress1}, `;

        if (info.residentialStreetAddress2) {
            address += `${info.residentialStreetAddress2}, `;
        }

        address += `${info.residentialCity}, ${info.residentialState ? info.residentialState : ''} ${info.residentialZipCode} ${
            info.residentialCountry
        }`;

        return address;
    }

    public editPersonalInformation() {
        this.editPersonalInfo.emit(true);
    }
}
