import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { HeroComponent } from './hero.component';

const NZ_Modules = [NzGridModule, NzButtonModule];

@NgModule({
    declarations: [HeroComponent],
    imports: [CommonModule, RouterModule, UtilModule, ...NZ_Modules],
    exports: [HeroComponent],
})
export class HeroModule {}
