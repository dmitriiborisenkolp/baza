import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { HeroConfig } from './models/hero-config.interface';

@Component({
    selector: 'app-hero',
    templateUrl: './hero.component.html',
    styleUrls: ['./hero.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeroComponent implements OnInit {
    @Input()
    config!: HeroConfig;

    constructor(public readonly uts: BazaLinkUtilSharedService, private readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        this.checkDefaultConfig();
    }

    checkDefaultConfig() {
        const defaultConfig: HeroConfig = {
            heading: `Invest & Earn Passive Income`,
            subHeading: `For Investors who need a reliable platform to browse and purchase offerings, Scalio provides a robust Investment Platform.`,
            btnLink: {
                appLink: {
                    commands: ['/items'],
                    text: 'Get Started',
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
