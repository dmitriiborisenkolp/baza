import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * Customization input config for hero banner component
 */
export interface HeroConfig {
    /**
     * Displays the heading for component
     */
    heading?: string;
    /**
     * Displays the sub-heading for component
     */
    subHeading?: string;
    /**
     * Background image for banner section
     */
    img?: string;
    /**
     * Used to configure internal or external route for button
     */
    btnLink?: LinkConfig;
}
