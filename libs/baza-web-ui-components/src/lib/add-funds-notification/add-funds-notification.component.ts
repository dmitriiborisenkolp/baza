import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

@Component({
    selector: 'app-add-funds-notification',
    templateUrl: './add-funds-notification.component.html',
    styleUrls: ['./add-funds-notification.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaAddFundsNotificationComponent {
    @Input()
    isVisible: boolean;

    @Output()
    isVisibleChange = new EventEmitter<boolean>();

    i18nBasePath = 'uic.addFunds.notification';

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    closeNotification(): void {
        this.isVisibleChange.emit(false);
    }
}
