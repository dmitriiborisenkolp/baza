import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { DwollaAddFundsFormModule } from '../add-funds-form/add-funds-form.module';
import { PaymentHeaderModule } from '../payment-header';
import { DwollaAddFundsNotificationComponent } from './add-funds-notification.component';

@NgModule({
    declarations: [DwollaAddFundsNotificationComponent],
    imports: [DwollaAddFundsFormModule, CommonModule, UtilModule, PaymentHeaderModule, NzButtonModule, NzModalModule],
    exports: [DwollaAddFundsNotificationComponent],
})
export class DwollaAddFundsNotificationModule {}
