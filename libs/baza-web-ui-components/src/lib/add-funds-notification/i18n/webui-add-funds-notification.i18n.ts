export const WebUiAddFundsNotificationEnI18n = {
    title: 'Funds Transfer Initiated',
    content: 'This transfer has been initiated. Your funds will be available shortly, and we will notify you through email.',
    actions: {
        accept: 'Got it',
    },
};
