/**
 * Customization input config for text section component
 */
export interface TextSectionConfig {
    /**
     * Displays the heading for component
     */
    heading?: string;
    /**
     * Displays the sub-heading for component
     */
    subHeading?: string;
    /**
     * Displays the subtitle for component
     */
    subtitle?: string;
    /**
     * Displays the description for component
     */
    descr?: string;
}
