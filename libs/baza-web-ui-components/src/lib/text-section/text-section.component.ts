import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { TextSectionConfig } from './models/text-section-config.interface';

@Component({
    selector: 'app-text-section',
    templateUrl: './text-section.component.html',
    styleUrls: ['./text-section.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextSectionComponent implements OnInit {
    @Input()
    config!: TextSectionConfig;

    constructor(private readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        this.checkDefaultConfig();
    }

    checkDefaultConfig() {
        const defaultConfig: TextSectionConfig = {
            heading: `Get exclusive access to private market investments`,
            subHeading: `Access to institutional-quality offers made easy through our low-cost, reliable investment platform.`,
            subtitle: `Dui sit aliquam sed nunc, volutpat. Elementum platea vel velit proin iaculis auctor erat. Nulla erat cursus sed et magna tristique.`,
            descr: `<p> Ornare in mattis nisi congue orci. Pulvinar netus eget quam maecenas non dui a. Tellus vitae et tortor nunc proin morbi gravida aliquam. Eu sit posuere neque odio eu sapien adipiscing eget. Orci amet, id cursus arcu facilisi. Non ultricies. </p> <p> Lobortis vel, vestibulum odio laoreet massa ut amet sed. Ultrices odio integer aliquet montes, eget arcu, massa. Tristique adipiscing nulla montes, faucibus. Nisi faucibus a laoreet leo, est. Et consequat justo, mattis risus ipsum posuere a nibh. Lectus ut eget eget faucibus donec quam morbi. Risus eu, sit lectus at faucibus. Dictumst ultrices diam at feugiat sed eget pulvinar convallis nibh. Quis varius amet, porttitor bibendum. Sodales tellus vel, turpis fermentum nulla elementum. Quam pellentesque vestibulum, vulputate ac turpis sit eleifend nisl. Gravida. </p>`,
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
