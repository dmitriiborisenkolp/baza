import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { TextSectionComponent } from './text-section.component';
const NZ_Modules = [NzGridModule];

@NgModule({
    declarations: [TextSectionComponent],
    imports: [CommonModule, RouterModule, UtilModule, ...NZ_Modules],
    exports: [TextSectionComponent],
})
export class TextSectionModule {}
