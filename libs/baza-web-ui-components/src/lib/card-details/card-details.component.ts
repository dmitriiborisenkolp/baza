import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BazaWebUtilSharedService, i18nValidationTypesEnum, ValidationsPreset } from '@scaliolabs/baza-web-utils';
import IMask from 'imask';

@Component({
    selector: 'app-card-details',
    templateUrl: './card-details.component.html',
    styleUrls: ['./card-details.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardDetailsComponent implements OnInit {
    @Input()
    cardDetailsForm: FormGroup;

    @ViewChild('cardDetailsFormEl', { static: false }) cardDetailsFormRef: ElementRef;

    public maskDate = {
        mask: 'mm/00',
        lazy: false,
        overwrite: true,
        autofix: true,
        blocks: {
            mm: { mask: IMask.MaskedRange, from: 1, to: 12, maxLength: 2 },
        },
    };

    public readonly creditCardNumberLength = 16;
    public readonly cvvCodeLength = 3;

    public i18nBasePath = 'uic.cardDetails';
    public i18nFormPath = `${this.i18nBasePath}.form`;
    public i18nFormFieldsPath = `${this.i18nBasePath}.form.fields`;

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit(): void {
        if (!this.cardDetailsForm.pristine) {
            this.cardDetailsForm.reset();
        }
    }

    public getErrorMessage(control: FormControl, controlName: string): string {
        const validationsPreset: ValidationsPreset = new Map([
            ['ccName', [{ key: i18nValidationTypesEnum.restrictedChars }]],
            [
                'ccNumber',
                [
                    { key: i18nValidationTypesEnum.onlynumbers },
                    { key: i18nValidationTypesEnum.minlength, params: { min: this.creditCardNumberLength } },
                    { key: i18nValidationTypesEnum.maxlength, params: { max: this.creditCardNumberLength } },
                ],
            ],
            [
                'cvv',
                [
                    { key: i18nValidationTypesEnum.onlynumbers },
                    { key: i18nValidationTypesEnum.minlength, params: { min: this.cvvCodeLength } },
                    { key: i18nValidationTypesEnum.maxlength, params: { max: this.cvvCodeLength } },
                ],
            ],
        ]);

        return this.wts.geti18nValidationErrorMessages({
            control,
            controlName,
            i18nFormFieldsPath: this.i18nFormFieldsPath,
            i18nGenericValidationsPath: `${this.i18nFormPath}.genericValidators`,
            validationsPreset,
        });
    }
}
