import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { IMaskModule } from 'angular-imask';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { CardDetailsComponent } from './card-details.component';

@NgModule({
    declarations: [CardDetailsComponent],
    exports: [CardDetailsComponent],
    imports: [
        CommonModule,
        FormsModule,
        IMaskModule,
        NzFormModule,
        NzGridModule,
        NzInputModule,
        ReactiveFormsModule,
        RouterModule,
        UtilModule,
    ],
})
export class CardDetailsModule {}
