import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentRadioComponent } from './payment-radio.component';

@NgModule({
    declarations: [PaymentRadioComponent],
    imports: [CommonModule, RouterModule, UtilModule],
    exports: [PaymentRadioComponent],
})
export class PaymentRadioModule {}
