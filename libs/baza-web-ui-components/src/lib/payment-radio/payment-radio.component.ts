import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-payment-radio',
    templateUrl: './payment-radio.component.html',
    styleUrls: ['./payment-radio.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentRadioComponent {
    @Input()
    isDisabled: boolean;

    @Input()
    isSelected: boolean;
}
