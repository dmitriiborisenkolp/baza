/**
 * Customization input config for Text-Image component
 */
export interface TextImageConfig {
    /**
     * Displays the subtitle for component
     */
    subtitle?: string;
    /**
     * Displays the heading for component
     */
    heading?: string;
    /**
     * Displays the description for component
     */
    descr?: string;
    /**
     * Displays the image for component
     */
    img?: string;
    /**
     * Set position for text placement i.e. Left or Right
     */
    txtPlacement?: TextPlacement;
}

export enum TextPlacement {
    Left = 'left',
    Right = 'right',
}
