import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { TextImgComponent } from './text-img.component';

const NZ_Modules = [NzGridModule];

@NgModule({
    declarations: [TextImgComponent],
    imports: [CommonModule, RouterModule, UtilModule, ...NZ_Modules],
    exports: [TextImgComponent],
})
export class TextImgModule {}
