import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { TextImageConfig, TextPlacement } from './models/text-img-config.interface';

@Component({
    selector: 'app-text-img',
    templateUrl: './text-img.component.html',
    styleUrls: ['./text-img.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextImgComponent implements OnInit {
    @Input()
    config!: TextImageConfig;

    constructor(public readonly sanitizer: DomSanitizer, private readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        this.checkDefaultConfig();
    }

    checkDefaultConfig() {
        const defaultConfig: TextImageConfig = {
            subtitle: `Subtitle`,
            heading: `Easy to build a diversified investing strategy`,
            descr: `Mauris bibendum neque sed mauris tristique, at cursus purus volutpat. Duis ac neque gravida, condimentum mauris eget, aliquet magna. Maecenas varius sed libero id eleifend. Maecenas tempus finibus suscipit. Fusce venenatis nibh massa, in condimentum lacus auctor fringilla.`,
            img: `'/assets/images/section1.jpeg'`,
            txtPlacement: TextPlacement.Left,
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }

    get bgImage() {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${this.config?.img})`);
    }

    get isTextOnRight() {
        return this.config?.txtPlacement === TextPlacement.Right;
    }
}
