/**
 * Customization input config for How It Works component
 */
export interface HowItWorksConfig {
    /**
     * Displays the heading for component
     */
    heading?: string;
    /**
     * Displays the sub-heading for component
     */
    subHeading?: string;
    /**
     * Customization config for 3 sections of How It Works component
     */
    sectionsConfig: SectionConfig;
}

type SectionConfig = [HIWSectionConfig, HIWSectionConfig, HIWSectionConfig];

interface HIWSectionConfig {
    /**
     * Displays the title for section
     */
    title?: string;
    /**
     * Displays the description for section
     */
    descr?: string;
}
