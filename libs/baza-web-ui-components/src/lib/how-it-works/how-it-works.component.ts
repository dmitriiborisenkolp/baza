import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { HowItWorksConfig } from './models/hiw-config.interface';

@Component({
    selector: 'app-how-it-works',
    templateUrl: './how-it-works.component.html',
    styleUrls: ['./how-it-works.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HowItWorksComponent implements OnInit {
    @Input()
    config!: HowItWorksConfig;

    constructor(private wts: BazaWebUtilSharedService) {}

    ngOnInit(): void {
        this.checkDefaultConfig();
    }

    checkDefaultConfig() {
        const defaultConfig: HowItWorksConfig = {
            heading: 'From the market to your portfolio',
            subHeading:
                'Mauris bibendum neque sed mauris tristique, at cursus purus volutpat. Duis ac neque gravida, condimentum mauris eget.',
            sectionsConfig: [
                {
                    title: 'Nulla metus tincidunt faucibus.',
                    descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
                },
                {
                    title: 'Nulla metus tincidunt faucibus.',
                    descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
                },
                {
                    title: 'Nulla metus tincidunt faucibus.',
                    descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
                },
            ],
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
