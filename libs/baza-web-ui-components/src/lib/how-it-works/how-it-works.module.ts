import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { HowItWorksCardModule } from '../how-it-works-card';
import { HowItWorksComponent } from './how-it-works.component';

const NZ_Modules = [NzGridModule];

@NgModule({
    declarations: [HowItWorksComponent],
    imports: [CommonModule, RouterModule, UtilModule, HowItWorksCardModule, ...NZ_Modules],
    exports: [HowItWorksComponent],
})
export class HowItWorksModule {}
