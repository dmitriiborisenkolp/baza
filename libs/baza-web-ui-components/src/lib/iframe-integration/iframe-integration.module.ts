import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { IframeIntegrationComponent } from './iframe-integration.component';

@NgModule({
    declarations: [IframeIntegrationComponent],
    exports: [IframeIntegrationComponent],
    imports: [CommonModule, UtilModule],
})
export class IframeIntegrationModule {}
