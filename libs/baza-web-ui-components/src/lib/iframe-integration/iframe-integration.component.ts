import { isPlatformBrowser } from '@angular/common';
import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Inject,
    Input,
    OnDestroy,
    Output,
    PLATFORM_ID,
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
    templateUrl: './iframe-integration.component.html',
    styleUrls: ['./iframe-integration.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IframeIntegrationComponent<T extends string = string> implements AfterViewInit, OnDestroy {
    @Input() url: string;
    @Input() events: Array<T>;

    @Input() width: string;
    @Input() maxWidth: string;
    @Input() height: string;
    @Input() maxHeight: string;
    @Input() iframeClassName: string;

    @Output() iframeEvent = new EventEmitter<T>();

    private _listener;

    constructor(
        // eslint-disable-next-line @typescript-eslint/ban-types
        @Inject(PLATFORM_ID) private platformId: Object,
        private readonly sanitizer: DomSanitizer,
    ) {}

    get safeUrl(): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    }

    get iframeNgStyle(): Record<string, unknown> {
        return {
            width: this.width,
            'max-width': this.maxWidth,
            height: this.height,
            'max-height': this.maxHeight,
        };
    }

    ngAfterViewInit(): void {
        if (isPlatformBrowser(this.platformId)) {
            const eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';

            const eventer = window[`${eventMethod}`];
            const messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';

            this._listener = (e) => {
                if (this.events.length === 0 && e.origin.indexOf('norcapsecurities.com') > -1) {
                    this.iframeEvent.next(e.data);
                } else {
                    if (this.events.includes(e.data)) {
                        this.iframeEvent.next(e.data);
                    }
                }
            };

            this._listener = eventer(messageEvent, this._listener);
        }
    }

    ngOnDestroy(): void {
        if (isPlatformBrowser(this.platformId)) {
            const detachMethod = window.removeEventListener ? 'removeEventListener' : 'detachEvent';

            const eventer = window[`${detachMethod}`];

            const messageEvent = detachMethod === 'removeEventListener' ? 'onmessage' : 'message';

            eventer(messageEvent, this._listener);
        }
    }
}
