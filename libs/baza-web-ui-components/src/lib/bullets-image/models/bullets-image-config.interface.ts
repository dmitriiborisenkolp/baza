import { RangedArray } from '@scaliolabs/baza-web-utils';
/**
 * Displays the bullet image. A minimum of 2 and maximum of 4 string elements can be defined in array for images along with a label
 */
export interface BulletsImageConfig {
    label?: string;
    images?: RangedArray<string, 2, 4>;
}
