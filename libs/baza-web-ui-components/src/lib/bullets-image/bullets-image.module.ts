import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { BulletsImageComponent } from './bullets-image.component';

const NZ_Modules = [NzGridModule];

@NgModule({
    declarations: [BulletsImageComponent],
    imports: [CommonModule, RouterModule, UtilModule, ...NZ_Modules],
    exports: [BulletsImageComponent],
})
export class BulletsImageModule {}
