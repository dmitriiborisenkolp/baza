import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BulletsImageConfig } from './models/bullets-image-config.interface';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-bullets-image',
    templateUrl: './bullets-image.component.html',
    styleUrls: ['./bullets-image.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BulletsImageComponent {
    @Input()
    config: BulletsImageConfig = {
        label: 'As seen on',
        images: [
            `/assets/images/marketwatch.png`,
            `/assets/images/yahoo-finance.png`,
            `/assets/images/bloomberg.png`,
            `/assets/images/benzinga.png`,
        ],
    };

    constructor(public readonly sanitizer: DomSanitizer) {}

    getBulletImg(imgPath: string) {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${imgPath})`);
    }
}
