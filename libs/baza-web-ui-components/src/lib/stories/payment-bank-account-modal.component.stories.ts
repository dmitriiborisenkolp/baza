import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    BazaWebUtilSharedService,
    onlyNumbersValidator,
    restrictedCharsValidator,
    routingNumberValidator,
} from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { PaymentBankAccountModalComponent } from '../payment-bank-account-modal';
import { BankDetailsModule } from '../bank-details';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { PaymentHeaderModule } from '../payment-header';
import { MockBazaFormValidatorService } from '../../../.storybook/mocks/mock-baza-form-validator.service';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { action } from '@storybook/addon-actions';
import { FormControl, FormGroup, Validators } from '@angular/forms';

export default {
    title: 'Payment Bank Account Modal',
    component: PaymentBankAccountModalComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, BankDetailsModule, CommonModule, NzButtonModule, NzModalModule, PaymentHeaderModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
                {
                    provide: BazaFormValidatorService,
                    useClass: MockBazaFormValidatorService,
                },
            ],
        }),
    ],
} as Meta<PaymentBankAccountModalComponent>;

const Template: Story<PaymentBankAccountModalComponent> = (args: PaymentBankAccountModalComponent) => ({
    component: PaymentBankAccountModalComponent,
    props: {
        ...args,
        bankDetailsForm: new FormGroup({
            accountName: new FormControl(null, Validators.compose([Validators.required, restrictedCharsValidator({ bypassChars: ['-'] })])),
            accountType: new FormControl(null, [Validators.required]),
            accountNumber: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(6)])),
            accountRoutingNumber: new FormControl(
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(9),
                    Validators.maxLength(9),
                    onlyNumbersValidator(),
                    routingNumberValidator(),
                ]),
            ),
        }),
        handleBankDetailsCancel: action('handleBankDetailsCancel'),
        submitManualBankDetailsForm: action('submitManualBankDetailsForm'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    isBankDetailsFormVisible: true,
};
