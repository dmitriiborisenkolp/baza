import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentHeaderComponent } from '../payment-header';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Payment Header',
    component: PaymentHeaderComponent,
    decorators: [
        moduleMetadata({
            imports: [CommonModule, UtilModule],
        }),
    ],
} as Meta<PaymentHeaderComponent>;

const Template: Story<PaymentHeaderComponent> = (args: PaymentHeaderComponent) => ({
    component: PaymentHeaderComponent,
    props: {
        ...args,
        backClicked: action('backClicked'),
        closeClicked: action('closeClicked'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    isBackBtnVisible: true,
};
