import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { CTAComponent } from '../cta';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { MockBazaLinkUtilSharedService } from '../../../.storybook/mocks/mock-baza-link-util-shared.service';

export default {
    title: 'CTA',
    component: CTAComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, UtilModule, NzGridModule, NzButtonModule],
            providers: [
                {
                    provide: BazaLinkUtilSharedService,
                    useClass: MockBazaLinkUtilSharedService,
                },
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<CTAComponent>;

const Template: Story<CTAComponent> = (args: CTAComponent) => ({
    component: CTAComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        heading: `Start powering your portfolio today`,
        descr: `Go from zero, to "property owner" with just a few clicks`,
        btnLink: {
            appLink: {
                commands: ['/items'],
                text: 'Get Started',
            },
        },
    },
};
