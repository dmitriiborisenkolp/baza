import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { of } from 'rxjs';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { BazaNcOfferingStatus, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { SummaryComponent } from '../summary';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDividerModule } from 'ng-zorro-antd/divider';

export default {
    title: 'Summary',
    component: SummaryComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, NzCardModule, NzPopoverModule, NzDividerModule, UtilModule, NzSkeletonModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<SummaryComponent>;

const Template: Story<SummaryComponent> = (args: SummaryComponent) => ({
    component: SummaryComponent,
    props: {
        ...args,
        entity$: of({
            isPublished: true,
            cover: {
                xs: '/cover-url',
                sm: '/cover-url',
                md: '/cover-url',
                xl: '/cover-url',
            },
            images: [
                {
                    xs: '/cover-url',
                    sm: '/cover-url',
                    md: '/cover-url',
                    xl: '/cover-url',
                },
            ],
            timeline: [
                {
                    id: 1,
                    isPublished: true,
                    tags: [
                        {
                            id: 1,
                            sortOrder: 1,
                            title: 'Tag title',
                            seo: {
                                title: 'Head title',
                                urn: 'urn',
                                meta: [
                                    {
                                        name: 'meta name',
                                        contents: 'meta contents',
                                    },
                                ],
                            },
                        },
                    ],
                    categories: [
                        {
                            id: 1,
                            isActive: true,
                            sortOrder: 1,
                            title: 'category title',
                        },
                    ],
                    date: '31-01-2016',
                    title: 'timeline title',
                    textDescription: 'timeline description',
                    metadata: { foo: 'bar', bar: 'baz' },
                    seo: {
                        title: 'Head title',
                        urn: 'urn',
                        meta: [
                            {
                                name: 'meta name',
                                contents: 'meta contents',
                            },
                        ],
                    },
                },
            ],
            isFavorite: true,
            isSubscribed: false,
            status: BazaNcOfferingStatus.Open,
            offeringId: '123',
            pricePerShareCents: '5000',
            numSharesAvailable: 4,
            percentFunded: 5,
            totalSharesValueCents: '20000',
            numSharesMin: 1,
            statements: [],
        }),
        numberOfShares$: of(1),
        purchaseStart$: of({
            id: '1',
            updateId: '1',
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
            areDocumentsSigned: true,
            amount: 5000,
            fee: 50,
            total: 5050,
            numberOfShares: 2,
            willBeActiveUpTo: '31-01-2025',
            transactionFeesCents: '500',
        }),
        stats$: of({
            message: 'Additional message',
            canPurchase: 20,
            numSharesMax: 1,
            totalPurchasedAmount: 50,
            totalPurchasedAmountCents: 5000,
            totalOfferingSharesRemaining: 30,
            numSharesAvailableForUser: 20,
            numSharesAlreadyPurchasedByUser: 0,
            numSharesPurchasedByOtherUsers: 0,
            numSharesReservedByOtherUsers: 0,
            numMinPurchaseAbleShares: 1,
            isValidPurchase: true,
        }),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    numberOfShares: 5,
    withImage: true,
};
