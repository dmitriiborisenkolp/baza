import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { DwollaAddFundsNotificationComponent } from '../add-funds-notification';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { DwollaAddFundsFormModule } from '../add-funds-form';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentHeaderModule } from '../payment-header';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Dwolla Add Funds Notification',
    component: DwollaAddFundsNotificationComponent,
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                DwollaAddFundsFormModule,
                CommonModule,
                UtilModule,
                PaymentHeaderModule,
                NzButtonModule,
                NzModalModule,
            ],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<DwollaAddFundsNotificationComponent>;

const Template: Story<DwollaAddFundsNotificationComponent> = (args: DwollaAddFundsNotificationComponent) => ({
    component: DwollaAddFundsNotificationComponent,
    props: {
        ...args,
        isVisibleChange: action('isVisibleChange'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    isVisible: true,
};
