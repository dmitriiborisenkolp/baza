import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { TextImgComponent, TextPlacement } from '../text-img';
import { NzGridModule } from 'ng-zorro-antd/grid';

export default {
    title: 'Text Img',
    component: TextImgComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, UtilModule, NzGridModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<TextImgComponent>;

const Template: Story<TextImgComponent> = (args: TextImgComponent) => ({
    component: TextImgComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        subtitle: `Subtitle`,
        heading: `Easy to build a diversified investing strategy`,
        descr: `Mauris bibendum neque sed mauris tristique, at cursus purus volutpat. Duis ac neque gravida, condimentum mauris eget, aliquet magna. Maecenas varius sed libero id eleifend. Maecenas tempus finibus suscipit. Fusce venenatis nibh massa, in condimentum lacus auctor fringilla.`,
        img: `'/assets/images/section1.jpeg'`,
        txtPlacement: TextPlacement.Left,
    },
};
