import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { HowItWorksCardComponent } from '../how-it-works-card';
import { NzCardModule } from 'ng-zorro-antd/card';

export default {
    title: 'How It Works Card',
    component: HowItWorksCardComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, UtilModule, NzCardModule],
        }),
    ],
} as Meta<HowItWorksCardComponent>;

const Template: Story<HowItWorksCardComponent> = (args: HowItWorksCardComponent) => ({
    component: HowItWorksCardComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    index: 1,
    title: 'Title',
    descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
};
