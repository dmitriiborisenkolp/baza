import { Story, Meta } from '@storybook/angular';
import { BulletsTextComponent } from '../bullets-text';

export default {
    title: 'Bullets Text',
    component: BulletsTextComponent,
} as Meta<BulletsTextComponent>;

const Template: Story<BulletsTextComponent> = (args: BulletsTextComponent) => ({
    component: BulletsTextComponent,
    props: args,
});

export const Full = Template.bind({});
Full.args = {
    bullets: ['Arcu mauris lectus', 'Arcu mauris lectus', 'Arcu mauris lectus', 'Arcu mauris lectus'],
};

export const Partial = Template.bind({});
Partial.args = {
    bullets: ['Bullet 1', 'Bullet 2'],
};
