import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { WithdrawFundsFormModule } from '../withdraw-funds-form';
import { FormControl, FormGroup, FormsModule, Validators } from '@angular/forms';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { of } from 'rxjs';
import { WithdrawFundsModalComponent } from '../withdraw-funds-modal';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { PaymentHeaderModule } from '../payment-header';
import { MockBazaFormValidatorService } from '../../../.storybook/mocks/mock-baza-form-validator.service';
import { action } from '@storybook/addon-actions';
import {
    AccountTypeCheckingSaving,
    BazaNcBankAccountExport,
    BazaNcBankAccountSource,
    BazaNcBankAccountType,
} from '@scaliolabs/baza-nc-shared';

export default {
    title: 'Withdraw Funds Modal',
    component: WithdrawFundsModalComponent,
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                WithdrawFundsFormModule,
                CommonModule,
                UtilModule,
                FormsModule,
                NzInputModule,
                NzButtonModule,
                NzModalModule,
                PaymentHeaderModule,
            ],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
                {
                    provide: BazaFormValidatorService,
                    useClass: MockBazaFormValidatorService,
                },
            ],
        }),
    ],
} as Meta<WithdrawFundsModalComponent>;

const Template: Story<WithdrawFundsModalComponent> = (args: WithdrawFundsModalComponent) => ({
    component: WithdrawFundsModalComponent,
    props: {
        ...args,
        withdrawFundsForm: new FormGroup({
            withdrawAmount: new FormControl(null, [Validators.required]),
        }),
        withdrawAmountErrorMessage$: of({
            text: 'Error message',
            type: 'error',
        }),
        withdrawFundsCancel: action('withdrawFundsCancel'),
        submitWithdrawFundsForm: action('submitWithdrawFundsForm'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    dwollaDefaultCashOutAccount: {
        isAvailable: true,
        result: {
            ulid: '123',
            type: BazaNcBankAccountType.CashIn,
            exported: [BazaNcBankAccountExport.NC],
            source: BazaNcBankAccountSource.Manual,
            isDefault: true,
            name: 'Name',
            accountName: 'Account Name',
            accountNickName: 'Account Nickname',
            accountNumber: '123456',
            accountRoutingNumber: '123456',
            accountType: AccountTypeCheckingSaving.Checking,
        },
    },
    dwollaAmount: {
        value: '50.00',
        currency: 'USD',
    },
};
