import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, onlyNumbersValidator, restrictedCharsValidator } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { action } from '@storybook/addon-actions';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { PaymentCardModalComponent } from '../payment-card-modal';
import { CardDetailsModule } from '../card-details';
import { PaymentHeaderModule } from '../payment-header';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { MockBazaFormValidatorService } from '../../../.storybook/mocks/mock-baza-form-validator.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

export default {
    title: 'Payment Card Modal',
    component: PaymentCardModalComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, CardDetailsModule, PaymentHeaderModule, NzButtonModule, NzModalModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
                {
                    provide: BazaFormValidatorService,
                    useClass: MockBazaFormValidatorService,
                },
            ],
        }),
    ],
} as Meta<PaymentCardModalComponent>;

const Template: Story<PaymentCardModalComponent> = (args: PaymentCardModalComponent) => ({
    component: PaymentCardModalComponent,
    props: {
        ...args,
        cardDetailsForm: new FormGroup({
            creditCardholderName: new FormControl(
                null,
                Validators.compose([Validators.required, restrictedCharsValidator({ bypassChars: ['-'] })]),
            ),
            creditCardNumber: new FormControl(
                null,
                Validators.compose([Validators.required, Validators.minLength(16), onlyNumbersValidator()]),
            ),
            expireDate: new FormControl(null, [Validators.required]),
            creditCardCvv: new FormControl(
                null,
                Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(4), onlyNumbersValidator()]),
            ),
        }),
        handleCardDetailsCancel: action('handleCardDetailsCancel'),
        submitCardForm: action('submitCardForm'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    isCardDetailsFormVisible: true,
};
