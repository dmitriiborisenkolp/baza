import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { WithdrawFundsFormComponent } from '../withdraw-funds-form';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { of } from 'rxjs';

export default {
    title: 'Withdraw Funds Form',
    component: WithdrawFundsFormComponent,
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                UtilModule,
                NzAlertModule,
                NzFormModule,
                NzInputNumberModule,
            ],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<WithdrawFundsFormComponent>;

const Template: Story<WithdrawFundsFormComponent> = (args: WithdrawFundsFormComponent) => ({
    component: WithdrawFundsFormComponent,
    props: {
        ...args,
        withdrawFundsForm: new FormGroup({
            withdrawAmount: new FormControl(null, [Validators.required]),
        }),
        withdrawAmountErrorMessage$: of({
            text: 'Warning message',
            type: 'warning',
        }),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    walletAmount: 50,
};
