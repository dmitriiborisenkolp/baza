import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentBankAccountComponent } from '../payment-bank-account';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { action } from '@storybook/addon-actions';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { AccountTypeCheckingSaving } from '@scaliolabs/baza-nc-shared';

export default {
    title: 'Payment Bank Account',
    component: PaymentBankAccountComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, UtilModule, NzButtonModule, NzGridModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<PaymentBankAccountComponent>;

const Template: Story<PaymentBankAccountComponent> = (args: PaymentBankAccountComponent) => ({
    component: PaymentBankAccountComponent,
    props: {
        ...args,
        bankLinkClicked: action('bankLinkClicked'),
        manualLinkClicked: action('manualLinkClicked'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    srcData: {
        isAvailable: true,
        details: {
            accountName: 'Account name',
            accountNickName: 'Account nickname',
            accountRoutingNumber: '123456789',
            accountNumber: '123456789',
            accountType: AccountTypeCheckingSaving.Checking,
        },
    },
    style: 'Bordered',
    isEditModeOn: true,
    isAddModeOn: true,
    disablePlaidACHLinking: true,
};
