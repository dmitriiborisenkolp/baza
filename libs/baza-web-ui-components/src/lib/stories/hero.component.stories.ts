import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { RouterModule } from '@angular/router';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { MockBazaLinkUtilSharedService } from '../../../.storybook/mocks/mock-baza-link-util-shared.service';
import { HeroComponent } from '../hero';

export default {
    title: 'Hero',
    component: HeroComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, RouterModule, UtilModule, NzGridModule, NzButtonModule],
            providers: [
                {
                    provide: BazaLinkUtilSharedService,
                    useClass: MockBazaLinkUtilSharedService,
                },
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<HeroComponent>;

const Template: Story<HeroComponent> = (args: HeroComponent) => ({
    component: HeroComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        heading: `Invest & Earn Passive Income`,
        subHeading: `For Investors who need a reliable platform to browse and purchase offerings, Scalio provides a robust Investment Platform.`,
        btnLink: {
            appLink: {
                commands: ['/items'],
                text: 'Get Started',
            },
        },
    },
};
