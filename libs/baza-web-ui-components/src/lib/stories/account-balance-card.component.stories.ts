import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { AccountBalanceCardComponent } from '../account-balance-card';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { action } from '@storybook/addon-actions';
import { NzButtonModule } from 'ng-zorro-antd/button';

export default {
    title: 'Account Balance Card',
    component: AccountBalanceCardComponent,
    decorators: [
        moduleMetadata({
            imports: [UtilModule, NzButtonModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<AccountBalanceCardComponent>;

const Template: Story<AccountBalanceCardComponent> = (args: AccountBalanceCardComponent) => ({
    component: AccountBalanceCardComponent,
    props: {
        ...args,
        accountBalanceClicked: action('accountBalanceClicked'),
        withdrawClicked: action('withdrawClicked'),
        addFundsClicked: action('addFundsClicked'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        srcData: { value: '100.00', currency: 'USD' },
        isDisabled: false,
        isAddModeOn: true,
        isEditModeOn: true,
        showWithdrawButton: true,
        isWithdrawDisabled: false,
        hasEnoughFunds: true,
        showAddFundsButton: true,
    },
};
