import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentRadioComponent } from '../payment-radio';

export default {
    title: 'Payment Radio',
    component: PaymentRadioComponent,
    decorators: [
        moduleMetadata({
            imports: [CommonModule, UtilModule],
        }),
    ],
} as Meta<PaymentRadioComponent>;

const Template: Story<PaymentRadioComponent> = (args: PaymentRadioComponent) => ({
    component: PaymentRadioComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    isDisabled: false,
    isSelected: false,
};
