import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { BackLinkComponent } from '../back-link';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, RouterModule } from '@angular/router';
import { BazaLinkUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaLinkUtilSharedService } from '../../../.storybook/mocks/mock-baza-link-util-shared.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { RouterTestingModule } from '@angular/router/testing';
import { action } from '@storybook/addon-actions';

export default {
    title: 'Back Link',
    component: BackLinkComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, RouterModule, UtilModule],
            providers: [
                NzModalService,
                {
                    provide: Router,
                    useClass: RouterTestingModule,
                },
                {
                    provide: BazaLinkUtilSharedService,
                    useClass: MockBazaLinkUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<BackLinkComponent>;

const Template: Story<BackLinkComponent> = (args: BackLinkComponent) => ({
    component: BackLinkComponent,
    props: {
        ...args,
        backEvent: action('backEvent'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    text: 'Back',
    showLeaveConfirmation: true,
    leaveConfirmationTitle: 'Leave confirmation title',
    leaveConfirmationText: 'Leave confirmation text',
};
