import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    BazaWebUtilSharedService,
    onlyNumbersValidator,
    restrictedCharsValidator,
    routingNumberValidator,
    UtilModule,
} from '@scaliolabs/baza-web-utils';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { BankDetailsComponent } from '../bank-details';

export default {
    title: 'Bank Details',
    component: BankDetailsComponent,
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                FormsModule,
                NzFormModule,
                NzInputModule,
                NzSelectModule,
                ReactiveFormsModule,
                UtilModule,
            ],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<BankDetailsComponent>;

const Template: Story<BankDetailsComponent> = (args: BankDetailsComponent) => ({
    component: BankDetailsComponent,
    props: {
        bankDetailsForm: new FormGroup({
            accountName: new FormControl(null, Validators.compose([Validators.required, restrictedCharsValidator({ bypassChars: ['-'] })])),
            accountType: new FormControl(null, [Validators.required]),
            accountNumber: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(6)])),
            accountRoutingNumber: new FormControl(
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(9),
                    Validators.maxLength(9),
                    onlyNumbersValidator(),
                    routingNumberValidator(),
                ]),
            ),
        }),
    },
});

export const Primary = Template.bind({});
