import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { WithdrawFundsFormModule } from '../withdraw-funds-form';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { PaymentHeaderModule } from '../payment-header';
import { action } from '@storybook/addon-actions';
import { WithdrawFundsNotificationComponent } from '../withdraw-funds-notification';

export default {
    title: 'Withdraw Funds Notification',
    component: WithdrawFundsNotificationComponent,
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                WithdrawFundsFormModule,
                UtilModule,
                PaymentHeaderModule,
                NzButtonModule,
                NzModalModule,
            ],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<WithdrawFundsNotificationComponent>;

const Template: Story<WithdrawFundsNotificationComponent> = (args: WithdrawFundsNotificationComponent) => ({
    component: WithdrawFundsNotificationComponent,
    props: {
        isVisibleChange: action('isVisibleChange'),
    },
});

export const Primary = Template.bind({});
