import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { BannerComponent } from '../banner';
import { RouterModule } from '@angular/router';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { MockBazaLinkUtilSharedService } from '../../../.storybook/mocks/mock-baza-link-util-shared.service';

export default {
    title: 'Banner',
    component: BannerComponent,
    decorators: [
        moduleMetadata({
            imports: [CommonModule, RouterModule, UtilModule, NzGridModule, NzButtonModule],
            providers: [
                {
                    provide: BazaLinkUtilSharedService,
                    useClass: MockBazaLinkUtilSharedService,
                },
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<BannerComponent>;

const Template: Story<BannerComponent> = (args: BannerComponent) => ({
    component: BannerComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        subtitle: `Subtitle`,
        heading: `We analyze billions of dollars worth of deals on your behalf`,
        subHeading: `Ut dictum turpis vulputate nunc id eget mauris elit, pellentesque. Faucibus quis dignissim varius vel. Blandit.`,
        btnLink: {
            appLink: {
                commands: ['/items'],
                text: 'View Offering',
            },
        },
    },
};
