import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { action } from '@storybook/addon-actions';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { of } from 'rxjs';
import { PersonalInfoComponent } from '../personal-info';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { Domicile } from '@scaliolabs/baza-nc-shared';

export default {
    title: 'Personal Info',
    component: PersonalInfoComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, NzPopoverModule, UtilModule, NzSkeletonModule, NzAlertModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<PersonalInfoComponent>;

const Template: Story<PersonalInfoComponent> = (args: PersonalInfoComponent) => ({
    component: PersonalInfoComponent,
    props: {
        info$: of({
            firstName: 'Firstname',
            lastName: 'Lastname',
            dateOfBirth: '31-01-2016',
            hasSsn: true,
            ssn: 'ssn',
            phone: '123456789',
            phoneCountryCode: 'phoneCountryCode',
            phoneCountryNumericCode: '+1',
            citizenship: Domicile.USResident,
            residentialStreetAddress1: 'residentialStreetAddress1',
            residentialStreetAddress2: 'residentialStreetAddress2',
            residentialCity: 'New York',
            residentialState: 'N/A',
            residentialZipCode: 'N/A',
            residentialCountry: 'USA',
            hasAdditionalDocument: true,
            hasSsnDocument: true,
            ssnDocumentFileName: 'ssnDocumentFileName',
            ssnDocumentUrl: '/ssnDocumentUrl',
            ssnDocumentAwsKey: 'ssnDocumentAwsKey',
            ssnDocumentId: 'ssnDocumentId',
            additionalDocumentUrl: '/additionalDocumentUrl',
            additionalDocumentFileName: 'additionalDocumentFileName',
            additionalDocumentId: 'additionalDocumentId',
        }),
        editPersonalInfo: action('editPersonalInfo'),
    },
});

export const Primary = Template.bind({});
