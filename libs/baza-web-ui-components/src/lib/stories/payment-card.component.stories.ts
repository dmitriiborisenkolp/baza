import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { action } from '@storybook/addon-actions';
import { BazaCreditCardType } from '@scaliolabs/baza-core-shared';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { PaymentCardComponent } from '../payment-card';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { of } from 'rxjs';

export default {
    title: 'Payment Card',
    component: PaymentCardComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, NzButtonModule, NzGridModule, NzPopoverModule, UtilModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<PaymentCardComponent>;

const Template: Story<PaymentCardComponent> = (args: PaymentCardComponent) => ({
    component: PaymentCardComponent,
    props: {
        ...args,
        limits$: of({
            maxACHTransferAmountCents: 10000000,
            maxCreditCardTransferAmountCents: 500000,
            creditCardPaymentsFee: 3.5,
            maxDwollaTransferAmount: 1000000,
        }),
        handleAddCard: action('handleAddCard'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    srcData: {
        isAvailable: true,
        creditCard: {
            creditCardType: BazaCreditCardType.Visa,
            creditCardNumber: '12345567890123456',
            creditCardholderName: 'JOHN DOE',
            creditCardExpireMonth: 1,
            creditCardExpireYear: 20,
        },
    },
    isPurchaseAboveLimit: false,
    style: 'Bordered',
    isEditModeOn: true,
    isAddModeOn: true,
    isForeignInvestor: true,
};
