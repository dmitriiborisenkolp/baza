import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { TextSectionComponent } from '../text-section';

export default {
    title: 'Text Section',
    component: TextSectionComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, UtilModule, NzGridModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<TextSectionComponent>;

const Template: Story<TextSectionComponent> = (args: TextSectionComponent) => ({
    component: TextSectionComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        heading: `Get exclusive access to private market investments`,
        subHeading: `Access to institutional-quality offers made easy through our low-cost, reliable investment platform.`,
        subtitle: `Dui sit aliquam sed nunc, volutpat. Elementum platea vel velit proin iaculis auctor erat. Nulla erat cursus sed et magna tristique.`,
        descr: `<p> Ornare in mattis nisi congue orci. Pulvinar netus eget quam maecenas non dui a. Tellus vitae et tortor nunc proin morbi gravida aliquam. Eu sit posuere neque odio eu sapien adipiscing eget. Orci amet, id cursus arcu facilisi. Non ultricies. </p> <p> Lobortis vel, vestibulum odio laoreet massa ut amet sed. Ultrices odio integer aliquet montes, eget arcu, massa. Tristique adipiscing nulla montes, faucibus. Nisi faucibus a laoreet leo, est. Et consequat justo, mattis risus ipsum posuere a nibh. Lectus ut eget eget faucibus donec quam morbi. Risus eu, sit lectus at faucibus. Dictumst ultrices diam at feugiat sed eget pulvinar convallis nibh. Quis varius amet, porttitor bibendum. Sodales tellus vel, turpis fermentum nulla elementum. Quam pellentesque vestibulum, vulputate ac turpis sit eleifend nisl. Gravida. </p>`,
    },
};
