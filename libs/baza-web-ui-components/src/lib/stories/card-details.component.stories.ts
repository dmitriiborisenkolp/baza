import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, onlyNumbersValidator, restrictedCharsValidator, UtilModule } from '@scaliolabs/baza-web-utils';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { CardDetailsComponent } from '../card-details';
import { IMaskModule } from 'angular-imask';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { RouterModule } from '@angular/router';

export default {
    title: 'Card Details',
    component: CardDetailsComponent,
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                FormsModule,
                IMaskModule,
                NzFormModule,
                NzGridModule,
                NzInputModule,
                ReactiveFormsModule,
                RouterModule,
                UtilModule,
            ],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<CardDetailsComponent>;

const Template: Story<CardDetailsComponent> = (args: CardDetailsComponent) => ({
    component: CardDetailsComponent,
    props: {
        cardDetailsForm: new FormGroup({
            creditCardholderName: new FormControl(
                null,
                Validators.compose([Validators.required, restrictedCharsValidator({ bypassChars: ['-'] })]),
            ),
            creditCardNumber: new FormControl(
                null,
                Validators.compose([Validators.required, Validators.minLength(16), onlyNumbersValidator()]),
            ),
            expireDate: new FormControl(null, [Validators.required]),
            creditCardCvv: new FormControl(
                null,
                Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(4), onlyNumbersValidator()]),
            ),
        }),
    },
});

export const Primary = Template.bind({});
