import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { DwollaAddFundsModalComponent } from '../add-funds-modal';
import { MockBazaFormValidatorService } from '../../../.storybook/mocks/mock-baza-form-validator.service';
import { DwollaAddFundsFormModule } from '../add-funds-form';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup, FormsModule, Validators } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { PaymentHeaderModule } from '../payment-header';
import { of } from 'rxjs';
import { action } from '@storybook/addon-actions';
import {
    AccountTypeCheckingSaving,
    BazaNcBankAccountExport,
    BazaNcBankAccountSource,
    BazaNcBankAccountType,
} from '@scaliolabs/baza-nc-shared';

export default {
    title: 'Dwolla Add Funds Modal',
    component: DwollaAddFundsModalComponent,
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                DwollaAddFundsFormModule,
                CommonModule,
                UtilModule,
                FormsModule,
                NzInputModule,
                NzButtonModule,
                NzModalModule,
                PaymentHeaderModule,
            ],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
                {
                    provide: BazaFormValidatorService,
                    useClass: MockBazaFormValidatorService,
                },
            ],
        }),
    ],
} as Meta<DwollaAddFundsModalComponent>;

const Template: Story<DwollaAddFundsModalComponent> = (args: DwollaAddFundsModalComponent) => ({
    component: DwollaAddFundsModalComponent,
    props: {
        ...args,
        addFundsForm: new FormGroup({
            transferAmount: new FormControl(null, [Validators.required]),
        }),
        transferAmountErrorMessage$: of({ text: 'success message', type: 'success' }),
        handleAddFundsCancel: action('handleAddFundsCancel'),
        handleEditCashInAccount: action('handleEditCashInAccount'),
        submitAddFundsForm: action('submitAddFundsForm'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    isAddFundsFormVisible: true,
    dwollaDefaultCashInAccount: {
        isAvailable: true,
        result: {
            ulid: '123',
            type: BazaNcBankAccountType.CashIn,
            exported: [BazaNcBankAccountExport.NC],
            source: BazaNcBankAccountSource.Manual,
            isDefault: true,
            name: 'Name',
            accountName: 'Account Name',
            accountNickName: 'Account Nickname',
            accountNumber: '123456',
            accountRoutingNumber: '123456',
            accountType: AccountTypeCheckingSaving.Checking,
        },
    },
};
