import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { DwollaAddFundsFormComponent } from '../add-funds-form';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { of } from 'rxjs';

export default {
    title: 'Dwolla Add Funds Form',
    component: DwollaAddFundsFormComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, ReactiveFormsModule, UtilModule, NzInputNumberModule, NzFormModule, NzAlertModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<DwollaAddFundsFormComponent>;

const Template: Story<DwollaAddFundsFormComponent> = (args: DwollaAddFundsFormComponent) => ({
    component: DwollaAddFundsFormComponent,
    props: {
        addFundsForm: new FormGroup({
            transferAmount: new FormControl(null, [Validators.required]),
        }),
        transferAmountErrorMessage$: of({ text: 'success message', type: 'success' }),
    },
});

export const Primary = Template.bind({});
