import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { HowItWorksComponent } from '../how-it-works';
import { HowItWorksCardModule } from '../how-it-works-card';

export default {
    title: 'How It Works',
    component: HowItWorksComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, UtilModule, HowItWorksCardModule, NzGridModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<HowItWorksComponent>;

const Template: Story<HowItWorksComponent> = (args: HowItWorksComponent) => ({
    component: HowItWorksComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        heading: 'From the market to your portfolio',
        subHeading: 'Mauris bibendum neque sed mauris tristique, at cursus purus volutpat. Duis ac neque gravida, condimentum mauris eget.',
        sectionsConfig: [
            {
                title: 'Nulla metus tincidunt faucibus.',
                descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
            },
            {
                title: 'Nulla metus tincidunt faucibus.',
                descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
            },
            {
                title: 'Nulla metus tincidunt faucibus.',
                descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
            },
        ],
    },
};
