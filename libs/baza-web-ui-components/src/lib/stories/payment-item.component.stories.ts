import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { action } from '@storybook/addon-actions';
import { PaymentItemComponent } from '../payment-item';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzNoAnimationModule } from 'ng-zorro-antd/core/no-animation';

export default {
    title: 'Payment Item',
    component: PaymentItemComponent,
    decorators: [
        moduleMetadata({
            imports: [
                BrowserAnimationsModule,
                CommonModule,
                RouterModule,
                UtilModule,
                NzButtonModule,
                NzCollapseModule,
                NzNoAnimationModule,
            ],
        }),
    ],
} as Meta<PaymentItemComponent>;

const Template: Story<PaymentItemComponent> = (args: PaymentItemComponent) => ({
    component: PaymentItemComponent,
    props: {
        ...args,
        clickContainer: action('clickContainer'),
    },
});

export const Primary = Template.bind({});
Primary.args = {
    isDisabled: false,
    isPaymentLinked: true,
};
