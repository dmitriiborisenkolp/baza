import { Story, Meta } from '@storybook/angular';
import { BulletsImageComponent } from '../bullets-image';

export default {
    title: 'Bullets Image',
    component: BulletsImageComponent,
} as Meta<BulletsImageComponent>;

const Template: Story<BulletsImageComponent> = (args: BulletsImageComponent) => ({
    component: BulletsImageComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        label: 'As seen on',
        images: [
            `/assets/images/marketwatch.png`,
            `/assets/images/yahoo-finance.png`,
            `/assets/images/bloomberg.png`,
            `/assets/images/benzinga.png`,
        ],
    },
};
