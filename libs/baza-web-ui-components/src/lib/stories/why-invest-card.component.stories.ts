import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { WhyInvestCardComponent } from '../why-invest-card';
import { IconSpriteModule } from 'ng-svg-icon-sprite';
import { NzCardModule } from 'ng-zorro-antd/card';

export default {
    title: 'Why Invest Card',
    component: WhyInvestCardComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, UtilModule, NzCardModule, IconSpriteModule],
        }),
    ],
} as Meta<WhyInvestCardComponent>;

const Template: Story<WhyInvestCardComponent> = (args: WhyInvestCardComponent) => ({
    component: WhyInvestCardComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        icon: 'point',
        title: 'Morbi nunc, consectetur ac gravida here.',
        descr: 'Lectus odio at felis faucibus a tincidunt duis nunc.',
    },
};
