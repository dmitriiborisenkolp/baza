import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BazaWebUtilSharedService, UtilModule } from '@scaliolabs/baza-web-utils';
import { MockBazaWebUtilSharedService } from '../../../.storybook/mocks/mock-baza-web-util-shared.service';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { WhyInvestComponent } from '../why-invest';
import { WhyInvestCardModule } from '../why-invest-card';

export default {
    title: 'Why Invest',
    component: WhyInvestComponent,
    decorators: [
        moduleMetadata({
            imports: [BrowserAnimationsModule, CommonModule, UtilModule, NzGridModule, WhyInvestCardModule],
            providers: [
                {
                    provide: BazaWebUtilSharedService,
                    useClass: MockBazaWebUtilSharedService,
                },
            ],
        }),
    ],
} as Meta<WhyInvestComponent>;

const Template: Story<WhyInvestComponent> = (args: WhyInvestComponent) => ({
    component: WhyInvestComponent,
    props: args,
});

export const Primary = Template.bind({});
Primary.args = {
    config: {
        heading: 'Your key to exclusive private market investing',
        subHeading: 'Nunc, dictumst odio aenean et nec ipsum sed mollis.',
        sectionsConfig: [
            {
                icon: 'point',
                title: 'Morbi nunc, consectetur ac gravida here.',
                descr: 'Lectus odio at felis faucibus a tincidunt duis nunc.',
            },
        ],
    },
};
