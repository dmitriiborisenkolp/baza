export const WebUiWithdrawFundsNotificationEnI18n = {
    title: 'Funds Withdrawal Initiated',
    content: 'Your funds will be available shortly, and we will notify you through email.',
    actions: {
        accept: 'Got it',
    },
};
