import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

@Component({
    selector: 'app-withdraw-funds-notification',
    templateUrl: './withdraw-funds-notification.component.html',
    styleUrls: ['./withdraw-funds-notification.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WithdrawFundsNotificationComponent {
    @Output()
    isVisibleChange = new EventEmitter<boolean>();

    i18nBasePath = 'uic.withdrawFunds.notification';

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    closeNotification(): void {
        this.isVisibleChange.emit(false);
    }
}
