import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { WithdrawFundsFormModule } from '../withdraw-funds-form/withdraw-funds-form.module';
import { PaymentHeaderModule } from '../payment-header';
import { WithdrawFundsNotificationComponent } from './withdraw-funds-notification.component';

@NgModule({
    declarations: [WithdrawFundsNotificationComponent],
    imports: [WithdrawFundsFormModule, CommonModule, UtilModule, PaymentHeaderModule, NzButtonModule, NzModalModule],
    exports: [WithdrawFundsNotificationComponent],
})
export class WithdrawFundsNotificationModule {}
