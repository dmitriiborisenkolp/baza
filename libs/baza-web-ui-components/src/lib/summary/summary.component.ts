import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { PurchaseFlowDto, StatsDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

@UntilDestroy()
@Component({
    selector: 'app-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SummaryComponent implements OnInit {
    @Input()
    entity$: Observable<BazaNcIntegrationListingsDto>;

    @Input()
    numberOfShares$: Observable<number>;

    @Input()
    purchaseStart$: Observable<PurchaseFlowDto>;

    @Input()
    numberOfShares: number;

    @Input()
    withImage = true;

    @Input()
    stats$: Observable<StatsDto>;

    transactionFeeCents?: number;
    transactionFee: number;
    maxSharesPerUserLimitReached: boolean;

    fee: number;
    // TODO: CB:15Mar2022: Do we load total from BE or is it calculated?
    total: number;

    public i18nBasePath = 'uic.summary';

    constructor(private readonly cdr: ChangeDetectorRef, public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        this.numberOfShares$.pipe(untilDestroyed(this)).subscribe((numberOfShares) => {
            this.numberOfShares = this.numberOfShares || numberOfShares || 1;
        });

        this.purchaseStart$?.pipe(untilDestroyed(this)).subscribe((purchaseStart) => {
            if (purchaseStart) {
                this.fee = purchaseStart.fee;
                this.total = purchaseStart.total;

                this.transactionFee = purchaseStart.transactionFees;
                this.transactionFeeCents = purchaseStart.transactionFeesCents / 100;

                this.cdr.detectChanges();
            }
        });

        this.stats$?.pipe(untilDestroyed(this)).subscribe((stats) => {
            if (stats) {
                const { availableTransactionTypes = [] } = stats || {};
                this.transactionFee = availableTransactionTypes.length && availableTransactionTypes[0].transactionFees;

                if (stats?.canPurchase === 0 || this.numberOfShares > stats?.canPurchase) {
                    this.numberOfShares = 0;
                    this.maxSharesPerUserLimitReached = true;
                }

                this.cdr.detectChanges();
            }
        });
    }

    calculateTotal(fee: number, amount: number) {
        return fee ? (fee + amount) / 100 : amount / 100;
    }

    calculateTransactionFee(amount: number, percentage: number): number {
        return (amount * percentage) / 100;
    }
}
