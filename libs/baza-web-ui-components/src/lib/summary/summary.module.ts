import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { SummaryComponent } from './summary.component';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';

@NgModule({
    declarations: [SummaryComponent],
    exports: [SummaryComponent],
    imports: [CommonModule, NzCardModule, NzPopoverModule, NzDividerModule, RouterModule, UtilModule, NzSkeletonModule],
})
export class SummaryModule {}
