import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    templateUrl: './leave-confirmation.component.html',
    styleUrls: ['./leave-confirmation.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LeaveConfirmationComponent {
    @Input()
    text: string;

    @Input()
    width: string;

    @Input()
    maxWidth: string;

    @Input()
    height: string;

    @Input()
    maxHeight: string;
}
