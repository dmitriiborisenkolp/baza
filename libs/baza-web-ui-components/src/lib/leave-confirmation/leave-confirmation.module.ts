import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { LeaveConfirmationComponent } from './leave-confirmation.component';

@NgModule({
    declarations: [LeaveConfirmationComponent],
    exports: [LeaveConfirmationComponent],
    imports: [CommonModule, UtilModule],
})
export class LeaveConfirmationModule {}
