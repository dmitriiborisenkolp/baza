import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { WISectionConfig } from '../why-invest/models/why-invest-config.interface';

@Component({
    selector: 'app-why-invest-card',
    templateUrl: './why-invest-card.component.html',
    styleUrls: ['./why-invest-card.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhyInvestCardComponent {
    @Input()
    config!: WISectionConfig;
}
