import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { IconSpriteModule } from 'ng-svg-icon-sprite';
import { NzCardModule } from 'ng-zorro-antd/card';
import { WhyInvestCardComponent } from './why-invest-card.component';

const NZ_Modules = [NzCardModule];

@NgModule({
    declarations: [WhyInvestCardComponent],
    imports: [CommonModule, RouterModule, UtilModule, IconSpriteModule, ...NZ_Modules],
    exports: [WhyInvestCardComponent],
})
export class WhyInvestCardModule {}
