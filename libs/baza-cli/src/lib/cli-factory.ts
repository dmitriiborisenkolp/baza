// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { apiFactory } from '@scaliolabs/app/api';
import { BazaCLICallback, bazaCliFactory, CliApplicationOptions } from '@scaliolabs/baza-core-api';

export function cliFactory(options: CliApplicationOptions, callback: BazaCLICallback) {
    bazaCliFactory(
        apiFactory,
        options,
        callback,
    );
}
