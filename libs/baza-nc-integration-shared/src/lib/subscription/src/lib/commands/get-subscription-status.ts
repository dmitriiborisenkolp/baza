import { BazaNcIntegrationSubscribeStatus } from '../models/baza-nc-integration-subscribe-status';

export class GetSubscriptionStatusCommand {
    constructor(
        public payload: {
            accountId: number;
            listingId: number;
        },
    ) {
    }
}

export class GetSubscriptionStatusCommandResponse {
    status: BazaNcIntegrationSubscribeStatus;
}
