import { BazaNcIntegrationSubscriptionCmsNotifyUsersRequest } from '../endpoints/baza-nc-integration-subscription-cms.endpoint';

export class BazaNcMailNotificationOfferingStatusCommand {
    constructor(public readonly request: BazaNcIntegrationSubscriptionCmsNotifyUsersRequest) {}
}
