export const bazaNcIntegrationSubscriptionsCmsEnI18n = {
    subscription: {
        title: '{{ listing.title }} / Subscriptions',
        navigation: 'Subscriptions',
        exportToCsv: 'Export to CSV',
        columns: {
            id: 'ID',
            email: 'Email',
            account: 'Account',
            status: 'Status',
            subscribed: 'Subscribed',
            sent: 'Sent',
        },
        actions: {
            add: 'Add subscription',
            notify: {
                label: 'Notify Subscribers',
                confirm:
                    'You are about to send Notification Email to all the users subscribed to this offering. Are you sure you want to proceed?',
                okText: 'Send',
            },
            exclude: {
                label: 'Unsubscribe account',
                confirm: 'Do you really want to unsubscribe account?',
                success: 'Account unsubscribed',
            },
            include: {
                label: 'Subscribe account',
                confirm: 'Do you really want to subscribe account?',
                success: 'Account subscribed',
            },
        },
        form: {
            fields: {
                email: 'Email',
            },
        },
    },
};
