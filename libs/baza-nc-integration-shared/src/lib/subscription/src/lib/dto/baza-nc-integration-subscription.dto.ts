import { AccountDto } from '@scaliolabs/baza-core-shared';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BazaNcIntegrationSubscribeStatus } from '../models/baza-nc-integration-subscribe-status';
import { BazaNcIntegrationSubscribeMessages } from '../models/baza-nc-integration-subscribe-messages';
import { BazaNcIntegrationListingsCmsDto } from '../../../../listings/src';

export class BazaNcIntegrationSubscriptionDto {
    @ApiModelProperty()
    id: number;

    @ApiModelProperty()
    account: AccountDto;

    @ApiModelProperty()
    listing: BazaNcIntegrationListingsCmsDto;

    @ApiModelProperty({
        type: 'string',
        enum: Object.values(BazaNcIntegrationSubscribeStatus),
    })
    status: BazaNcIntegrationSubscribeStatus;

    @ApiModelProperty({
        type: 'string',
        enum: Object.values(BazaNcIntegrationSubscribeMessages),
        isArray: true,
    })
    sent: Array<BazaNcIntegrationSubscribeMessages>
}
