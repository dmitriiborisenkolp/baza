export enum BazaNcIntegrationSubscribeResponse {
    Subscribed = 'Subscribed',
    AlreadySubscribed = 'AlreadySubscribed',
}
