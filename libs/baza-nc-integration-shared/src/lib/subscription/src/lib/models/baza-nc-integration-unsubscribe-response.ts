export enum BazaNcIntegrationUnsubscribeResponse {
    Unsubscribed = 'Unsubscribed',
    AlreadyUnsubscribed = 'AlreadyUnsubscribed',
    NotSubscribed = 'NotSubscribed',
}
