export enum BazaNcIntegrationSubscribeStatus {
    NotSubscribed = 'NotSubscribed',
    Subscribed = 'Subscribed',
    Unsubscribed = 'Unsubscribed',
}
