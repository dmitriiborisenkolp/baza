export enum BazaNcIntegrationOpenApi {
    BazaNcIntegration = 'Baza NC Integration',
    BazaNcIntegrationCms = 'Baza NC Integration CMS',
}

export enum BazaNcIntegrationPublicOpenApi {
    Account = 'Baza NC Integration Account',
    Listings = 'Baza NC Integration Listings',
    ListingTestimonials = 'Baza NC Integration Listing Testimonials',
    Feed = 'Baza NC Integration Feed',
    Investments = 'Baza NC Integration Investments',
    Subscription = 'Baza NC Integration Subscription',
    Perk = 'Baza NC Integration Perk',
    Favorite = 'Baza NC Integration Favorite',
    Portfolio = 'Baza NC Integration Portfolio',
    Search = 'Baza NC Integration Search',
    Schema = 'Baza NC Integration Schema',
    Operation = 'Baza NC Integration Operations',
}

export enum BazaNcIntegrationCmsOpenApi {
    Listings = 'Baza NC Integration Listings CMS',
    ListingTestimonials = 'Baza NC Integration Listing Testimonials CMS',
    Feed = 'Baza NC Integration Feed CMS',
    EliteInvestors = 'Baza NC Integration Elite Investors CMS',
    Subscription = 'Baza NC Integration Subscription CMS',
    Schema = 'Baza NC Schema CMS',
    Perk = 'Baza NC Integration Perk CMS',
    Search = 'Baza NC Integration Search CMS',
}
