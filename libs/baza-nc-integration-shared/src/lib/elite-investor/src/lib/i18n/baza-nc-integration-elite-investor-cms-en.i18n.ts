export const bazaNcIntegrationEliteInvestorCmsEnI18n = {
    eliteInvestors: {
        title: '{{ listing.title }} / Elite Investors',
        navigation: 'Elite Investors',
        columns: {
            id: 'ID',
            account: 'Account',
            dateJoinedAt: 'Date Joined',
            email: 'Email',
        },
        actions: {
            add: 'Add account to Elite Investors',
            exclude: {
                title: 'Exclude account from Elite Investors',
                confirm: 'Do you really want to exclude account from Elite Investors?',
            },
            syncAccount: {
                title: 'Sync Elite Investors Status',
                confirm: 'Do you really wan to update EliteInvestors status for account "{{ fullName }}"?',
                success: 'Elite Investors Status for account "{{ fullName }}" updated',
                failed: 'Failed to update Elite Investors Status for account "{{ fullName }}"',
            },
            syncAllAccounts: {
                title: 'Update Elite Investors',
                confirm: 'Do you really want to start updating Elite Investors status for ALL accounts?',
                success: 'Updating Elite Investors started',
                failed: 'Failed to start updating Elite Investors',
            },
        },
        form: {
            fields: {
                account: 'Account',
            },
            placeholders: {
                account: 'Search for account...',
            },
        },
    },
};
