export enum BazaNcIntegrationAclGroups {
    BazaNcIntegration = 'BazaNcIntegration',
}

export enum BazaNcIntegrationAcl {
    BazaNcIntegrationFeed = 'BazaNcIntegrationFeed',
    BazaNcIntegrationFeedManagement = 'BazaNcIntegrationFeedManagement',
    BazaNcIntegrationListing = 'BazaNcIntegrationListing',
    BazaNcIntegrationListingManagement = 'BazaNcIntegrationListingManagement',
    BazaNcIntegrationListingChangeStatus = 'BazaNcIntegrationListingChangeStatus',
    BazaNcIntegrationListingSubscription = 'BazaNcIntegrationListingSubscription',
    BazaNcIntegrationListingSubscriptionManagement = 'BazaNcIntegrationListingSubscriptionManagement',
    BazaNcIntegrationListingsEliteInvestors = 'BazaNcIntegrationListingsEliteInvestors',
    BazaNcIntegrationListingsTestimonials = 'BazaNcIntegrationListingsTestimonials',
    BazaNcIntegrationListingsTestimonialsManagement = 'BazaNcIntegrationListingsTestimonialsManagement',
    BazaNcIntegrationSchemaManagement = 'BazaNcIntegrationSchemaManagement',
    BazaNcIntegrationPerk = 'BazaNcIntegrationPerk',
    BazaNcIntegrationPerkManagement = 'BazaNcIntegrationPerkManagement',
}

export const bazaNcIntegrationAclEnI18n = {
    BazaNcIntegration: {
        group: 'Baza NC Integration',
        nodes: {
            [BazaNcIntegrationAcl.BazaNcIntegrationFeed]: 'Feed',
            [BazaNcIntegrationAcl.BazaNcIntegrationFeedManagement]: ' Feed management',
            [BazaNcIntegrationAcl.BazaNcIntegrationListing]: 'Listings',
            [BazaNcIntegrationAcl.BazaNcIntegrationListingManagement]: 'Listings management',
            [BazaNcIntegrationAcl.BazaNcIntegrationListingChangeStatus]: 'Listings management - Change Status',
            [BazaNcIntegrationAcl.BazaNcIntegrationListingSubscription]: 'Listing subscriptions',
            [BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement]: 'Listing subscriptions management',
            [BazaNcIntegrationAcl.BazaNcIntegrationListingsEliteInvestors]: 'Listing Elite Investors',
            [BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonials]: 'Listing Testimonials',
            [BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement]: 'Listing Testimonials management',
            [BazaNcIntegrationAcl.BazaNcIntegrationSchemaManagement]: 'Schema Management',
            [BazaNcIntegrationAcl.BazaNcIntegrationPerk]: 'Perks',
            [BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement]: 'Perks Management',
        },
    },
};
