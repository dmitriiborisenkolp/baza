export const bazaNcIntegrationListingTestimonialsCmsI18n = {
    testimonial: {
        components: {
            bazaIntegrationListingTestimonialCms: {
                title: '{{ listing.title }} / Testimonials',
                breadcrumbs: {
                    listings: 'Listings',
                    testimonials: 'Testimonials',
                },
                columns: {
                    id: 'ID',
                    isPublished: 'Is Published?',
                    dateCreatedAt: 'Date Created',
                    dateUpdatedAt: 'Date Updated',
                    image: 'Image',
                    name: 'Name',
                    role: 'Role',
                },
                actions: {
                    create: 'Add Testimonial',
                    update: 'Update Testimonial',
                    delete: {
                        title: 'Delete Testimonial',
                        confirm: 'Do you really want to delete testimonial "{{ name }} ({{ role }})"?',
                        success: 'Testimonial "{{ name }} ({{ role }})" deleted',
                    },
                    moveUp: 'Move Testimonial Up',
                    moveDown: 'Move Testimonial Down',
                },
                form: {
                    fields: {
                        isPublished: 'Is Published?',
                        image: 'Image',
                        name: 'Name',
                        role: 'Role',
                        contents: 'Contents',
                    },
                },
            },
        },
    },
};
