export enum BazaNcIntegrationListingTestimonialErrorCodes {
    BazaNcIntegrationListingTestimonialNotFound = 'BazaNcIntegrationListingTestimonialNotFound',
}

export const bazaNcIntegrationListingTestimonialErrorCodesI18n = {
    [BazaNcIntegrationListingTestimonialErrorCodes.BazaNcIntegrationListingTestimonialNotFound]: 'Listing Testimonial not found',
};
