export enum BazaNcIntegrationFeedPostType {
    Text = 'Text',
    Image = 'Image',
    Video = 'Video',
    Article = 'Article',
}
