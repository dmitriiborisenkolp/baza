export enum BazaNcIntegrationFeedDisplay {
    Public = 'Public',
    LockedForNonInvestors = 'LockedForNonInvestors',
    HiddenForNonInvestors = 'HiddenForNonInvestors',
}
