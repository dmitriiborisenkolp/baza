export const bazaNcIntegrationFeedCmsEnI18n = {
    feed: {
        title: 'Feed',
        types: {
            BazaNcIntegrationFeedPostType: {
                Text: 'Text',
                Image: 'Image',
                Video: 'Video',
                Article: 'Article',
            },
            BazaNcIntegrationFeedDisplay: {
                Public: 'Public',
                LockedForNonInvestors: 'Locked for Non-Investors',
                HiddenForNonInvestors: 'Hidden for Non-Investors',
            },
        },
        components: {
            feed: {
                title: 'Feed',
                video: 'You can upload MPEG-4 videos only up to {{ maxSizeMb }}MB size',
                columns: {
                    id: 'ID',
                    type: 'Type',
                    title: 'Title',
                    listing: 'Associated Listing',
                    applause: 'Applause',
                },
                actions: {
                    add: 'Add new post',
                    edit: 'Edit post',
                    delete: {
                        title: 'Delete post',
                        confirm: 'Do you really want to delete post?',
                    },
                    moveUp: 'Move Up',
                    moveDown: 'Move Down',
                },
                form: {
                    tabs: {
                        common: 'Common',
                        seo: 'SEO',
                    },
                    fields: {
                        isPublished: 'Published',
                        allowPublicAccess: 'Display for Non-Authenticated Users',
                        type: 'Type',
                        listing: {
                            label: 'Associated Listing',
                            none: '– None –',
                            item: '{{ listing.title }}',
                        },
                        title: 'Title',
                        intro: 'Text on post card',
                        previewImageUrl: 'Cover (Image)',
                        previewVideoUrl: 'Cover (Video)',
                        contents: 'Contents (Read more...)',
                        tags: 'Tags',
                        display: 'Display Mode',
                    },
                },
                types: {
                    common: 'Common',
                    listings: '{{ listing.title }}',
                },
                noTitle: '(No title specified)',
                imagePost: '(Image post without title)',
                videoPost: '(Video post without title)',
            },
        },
    },
};
