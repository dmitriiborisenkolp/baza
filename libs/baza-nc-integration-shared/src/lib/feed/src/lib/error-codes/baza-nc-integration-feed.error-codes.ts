export enum BazaNcIntegrationFeedErrorCodes {
    FeedPostNotFound = 'FeedPostNotFound',
    FeedPostApplauseNotFound = 'FeedPostApplauseNotFound',
    FeedPostAlreadyApplause = 'FeedPostAlreadyApplause',
    FeedListingsNotFound = 'FeedListingsNotFound',
}
