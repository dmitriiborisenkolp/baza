export enum BazaNcIntegrationFeedCmsErrorCodes {
    FeedImageForVideoIsNotProvided = 'FeedImageForVideoIsNotProvided',
    FeedCMSInvalidPostPayload = 'FeedCMSInvalidPostPayload',
    FeedCMSImageFileIsNotSupported = 'FeedCMSImageFileIsNotSupported',
    FeedCMSVideoFileIsNotSupported = 'FeedCMSVideoFileIsNotSupported',
}
