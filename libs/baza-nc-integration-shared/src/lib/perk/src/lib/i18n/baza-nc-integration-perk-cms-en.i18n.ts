export const bazaNcIntegrationPerkCmsEnI18n = {
    perk: {
        components: {
            bazaIntegrationPerkCms: {
                title: '{{ listings.title }} / Perks',
                navigation: 'Perks',
                columns: {
                    id: 'ID',
                    title: 'Title',
                    type: 'Type',
                    isForEliteInvestors: 'Requires to be in Elite Investors?',
                    isPublished: 'Is Published?',
                },
                fields: {
                    title: 'Title',
                    type: 'Type',
                    isForEliteInvestors: 'Requires to be in Elite Investors?',
                    metadata: 'Metadata',
                },
                actions: {
                    create: 'Add Perk',
                    update: 'Edit Perk',
                    delete: {
                        title: 'Delete Perk',
                        confirm: 'Do you really want to remove perk "{{ title }}"?',
                        success: 'Perk {{ title }} removed',
                    },
                    moveUp: 'Move Perk Up',
                    moveDown: 'Move Perk Down',
                },
            },
        },
    },
};
