export enum BazaNcIntegrationPerkErrorCodes {
    BazaNcIntegrationPerkNotFound = 'BazaNcIntegrationPerkNotFound',
}

export const bazaNcIntegrationPerkNotFoundI18n = {
    [BazaNcIntegrationPerkErrorCodes.BazaNcIntegrationPerkNotFound]: 'Perk not found',
};
