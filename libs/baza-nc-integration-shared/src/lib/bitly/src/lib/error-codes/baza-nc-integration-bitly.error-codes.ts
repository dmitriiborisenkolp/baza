export enum BazaNcIntegrationBitlyErrorCodes {
    BitlyNoAccessToken = 'BitlyNoAccessToken',
    BitlyFailedToCreateLink = 'BitlyFailedToCreateLink',
}
