import { Observable } from 'rxjs';

export enum BazaNcSearchCmsEndpointPaths {
    reindex = '/baza-nc-integration/cms/search/reindex/:ulid',
    reindexAll = '/baza-nc-integration/cms/search/reindex',
}

export interface BazaNcSearchCmsEndpoint {
    reindex(ulid: string): Promise<void> | Observable<void>;
    reindexAll(): Promise<void> | Observable<void>;
}
