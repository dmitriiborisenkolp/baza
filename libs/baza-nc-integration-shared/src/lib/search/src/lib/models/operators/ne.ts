import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BazaNcSearchOperator } from '../baza-nc-search-operator';
import { IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { BazaNcSearchField, bazaNcSearchFieldSwagger } from '../baza-nc-search-field';

export class BazaNcSearchOperatorNotEqual<T = any> {
    @ApiModelProperty({
        description: 'Field',
        ...bazaNcSearchFieldSwagger(),
    })
    @IsString()
    @IsNotEmpty()
    field: BazaNcSearchField;

    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcSearchOperator.NotEqual],
    })
    @IsEnum([BazaNcSearchOperator.NotEqual])
    @IsNotEmpty()
    type: BazaNcSearchOperator.NotEqual;

    @ApiModelProperty()
    @IsNotEmpty()
    value: T;
}
