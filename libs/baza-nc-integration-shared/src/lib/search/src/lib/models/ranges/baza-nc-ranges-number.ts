import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class BazaNcRangesNumber {
    @ApiModelProperty()
    min: number;

    @ApiModelProperty()
    max: number;
}
