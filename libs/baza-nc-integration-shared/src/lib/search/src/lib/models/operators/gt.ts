import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BazaNcSearchOperator } from '../baza-nc-search-operator';
import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { BazaNcSearchField, bazaNcSearchFieldSwagger } from '../baza-nc-search-field';

export class BazaNcSearchOperatorGreaterThan {
    @ApiModelProperty({
        description: 'Field',
        ...bazaNcSearchFieldSwagger(),
    })
    @IsString()
    @IsNotEmpty()
    field: BazaNcSearchField;

    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcSearchOperator.GreaterThan],
    })
    @IsEnum([BazaNcSearchOperator.GreaterThan])
    @IsNotEmpty()
    type: BazaNcSearchOperator.GreaterThan;

    @ApiModelProperty()
    @IsNumber()
    @IsNotEmpty()
    value: number;
}
