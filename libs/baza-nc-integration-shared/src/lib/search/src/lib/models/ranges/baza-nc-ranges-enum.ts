import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class BazaNcRangesEnum<T = string> {
    @ApiModelProperty({
        type: 'string',
        isArray: true,
    })
    values: Array<T>;
}
