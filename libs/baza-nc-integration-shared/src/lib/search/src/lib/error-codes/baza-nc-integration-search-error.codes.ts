export enum BazaNcIntegrationSearchErrorCodes {
    BazaNcIntegrationSearchDisabled = 'BazaNcIntegrationSearchDisabled',
    BazaNcIntegrationSearchFieldCannotBeSorted = 'BazaNcIntegrationSearchFieldCannotBeSorted',
    BazaNcIntegrationSearchUnknownOperator = 'BazaNcIntegrationSearchUnknownOperator',
    BazaNcIntegrationSearchFieldUnsupportedOperation = 'BazaNcIntegrationSearchFieldInvalidDate',
    BazaNcIntegrationSearchFieldInvalidDate = 'BazaNcIntegrationSearchFieldUnsupportedOperation',
}

export const bazaNcListingSearchErrorCodesEni18n = {
    [BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchDisabled]: 'Search API is disabled',
    [BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchFieldCannotBeSorted]: 'Field "{{ field }}" does not supports sort requests',
    [BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchUnknownOperator]: 'Unknown operator "{{ operator }}"',
    [BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchFieldUnsupportedOperation]:
        'Filter query contains unsupported combination of operator and values (Field Type: {{ fieldType }}, Operator: {{ operator }}, Input Value: {{ value }})',
    [BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchFieldInvalidDate]: 'Invalid Date',
};
