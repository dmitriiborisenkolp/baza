import { BazaNcIntegrationSchemaDefinition } from '../models/baza-nc-integration-schema-definition';
import * as md5 from 'md5';
import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { SchemaDefinitionPayload } from '../schema-definitions/_.schema-definition';

export function bazaNcIntegrationSchemaId(definitions: Array<BazaNcIntegrationSchemaDefinition>): string {
    const sorted = [...(definitions || [])].sort((a, b) => a.payload.sortId - b.payload.sortId);

    const updateIdResource = sorted.map((next) => ({
        type: next.type,
        payload: {
            ...next.payload,
            id: undefined,
            sortId: undefined,
            title: undefined,
            isDisplayedInDetails: undefined,
            tabId: undefined,
        },
    }));

    return md5(JSON.stringify(updateIdResource));
}

export function bazaNcIntegrationSchemaFieldId(type: BazaNcIntegrationSchemaType, payload: SchemaDefinitionPayload): string {
    return md5(
        JSON.stringify({
            type,
            payload: {
                ...payload,
                id: undefined,
                sortId: undefined,
                tabId: undefined,
                title: undefined,
                isDisplayedInDetails: undefined,
            },
        }),
    );
}
