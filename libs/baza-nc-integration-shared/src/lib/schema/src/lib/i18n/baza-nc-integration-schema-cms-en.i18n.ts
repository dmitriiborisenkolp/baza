export const bazaNcIntegrationSchemaCmsEnI18n = {
    schema: {
        title: 'Schema',
        __types: {
            BazaNcIntegrationSchemaType: {
                Text: 'Text',
                TextArea: 'Textarea',
                HTML: 'HTML',
                Email: 'Email',
                Select: 'Select',
                Checkbox: 'Checkbox',
                Date: 'Date',
                DateRange: 'Date Range',
                Time: 'Time',
                Number: 'Number',
                Rate: 'Rate',
                Slider: 'Slider',
                Switcher: 'Switcher',
                File: 'File',
                FileMulti: 'Files',
                Image: 'Image',
                ImageMulti: 'Images',
                KeyValue: 'Key / Value',
            },
        },
        components: {
            bazaNcIntegrationSchemaCms: {
                title: 'Schema',
                columns: {
                    id: 'ID',
                    title: 'Title',
                    displayName: 'Display Name',
                },
                actions: {
                    add: 'Create Schema',
                    edit: 'Edit Schema',
                    definitions: 'Tabs & Fields',
                    moveUp: 'Move Up Schema',
                    moveDown: 'Move Down Schema',
                    delete: {
                        title: 'Delete Schema',
                        confirm: 'Do you really want to remove Schema? Associated Listings will not be affected',
                        success: 'Schema "{{ title }}" deleted',
                        failed: 'Failed to delete Schema "{{ title }}"',
                    },
                },
                forms: {
                    create: {
                        title: 'New Schema',
                        fields: {
                            title: 'Title',
                            published: {
                                label: 'Is Published?',
                                hint: 'When enabled, Schema may appear in the UI elements, like filters, etc.',
                            },
                            displayName: 'Display Name',
                        },
                    },
                    edit: {
                        title: 'Edit Schema',
                    },
                },
            },
            bazaNcIntegrationSchemaEditorTabCms: {
                title: 'Schema - {{ title }}',
                defaultTab: 'Properties (Default)',
                breadcrumbs: {
                    schema: 'Schema',
                },
                columns: {
                    title: 'Title',
                    fields: 'Fields',
                    definitions: 'Fields',
                },
                actions: {
                    add: 'Add Tab',
                    moveUp: 'Move Up',
                    moveDown: 'Move Down',
                    delete: {
                        title: 'Delete Tab',
                        confirm: 'Do you really want to delete "{{ title }}" tab?',
                    },
                    edit: 'Edit Tab',
                },
                forms: {
                    title: {
                        add: 'Add Tab',
                        edit: 'Edit Tab',
                    },
                    fields: {
                        title: 'Title',
                    },
                },
            },
            bazaNcIntegrationSchemaEditorCms: {
                title: 'Schema – {{ title }}',
                defaultTab: 'Properties (Default)',
                breadcrumbs: {
                    schema: 'Schema',
                },
                columns: {
                    title: 'Title',
                    type: 'Type',
                    required: 'Required?',
                    isDisplayedInDetails: 'Display in Details?',
                    field: 'Field',
                },
                actions: {
                    editTitle: 'Edit Title',
                    addDefinition: 'Add Field',
                    moveUp: 'Move Up',
                    moveDown: 'Move Down',
                    delete: {
                        title: 'Delete Field',
                        confirm: 'Do you really want to delete field "{{ title }}"?',
                    },
                    edit: 'Edit Field',
                },
                forms: {
                    editTitle: {
                        title: 'Edit Title',
                        fields: {
                            title: 'Title',
                        },
                    },
                    addDefinition: {
                        title: 'Add Field',
                        fields: {
                            type: 'Type',
                            title: 'Title',
                            field: 'Field',
                        },
                    },
                    definition: {
                        title: 'Field Configuration',
                        fields: {
                            title: 'Title',
                            field: 'Field',
                            type: 'Type',
                            required: 'Required?',
                            isDisplayedInDetails: 'Display in Details?',
                            min: 'Minimum',
                            max: 'Maximum',
                            step: 'Step',
                            values: 'Values',
                            stars: 'Number of stars',
                            mode: 'Mode',
                            showToday: 'Show "Today" button',
                            showTime: 'Show time picker',
                            showNow: 'Show "Now" button',
                        },
                        hints: {
                            type: 'Field type cannot be changed',
                            title: 'This value is the name of the field. It’ll be displayed on Properties tab of Listing and on the UI of your application.',
                            field: 'This is a technical name of the field for JSON file, will not appear on Listing details in CMS or in the application',
                            mode: 'Select the scope of the date range selection',
                            showToday: 'Display a button to quickly select the current date',
                            showTime: 'Allow selection of specific time in addition to date',
                            showNow: 'Display a button to select the current time when "Show time picker" is enabled',
                        },
                    },
                },
                messages: {
                    unsupported: 'Field type "{{ type }}" is unsupported',
                },
            },
        },
    },
};
