export enum BazaNcIntegrationSchemaErrorCodes {
    BazaNcIntegrationSchemaNotFound = 'BazaNcIntegrationSchemaNotFound',
    BazaNcIntegrationSchemaDuplicateTitle = 'BazaNcIntegrationSchemaDuplicateTitle',
    BazaNcIntegrationSchemaLocked = 'BazaNcIntegrationSchemaLocked',
}

export const bazaNcIntegrationSchemaErrorCodesI18n = {
    [BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaNotFound]: 'Schema not found',
    [BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaDuplicateTitle]: 'Schema with given Title already exists',
    [BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaLocked]: 'Schema is Locked',
};
