import { Observable } from 'rxjs';

export enum BazaNcIntegrationSchemaEndpointPaths {
    list = '/baza-nc-integration/schema/list',
}

export interface BazaNcIntegrationSchemaEndpoint {
    list(): Promise<Array<string>> | Observable<Array<string>>;
}
