import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class FileMultiSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class FileMultiSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.FileMulti],
    })
    type: BazaNcIntegrationSchemaType.FileMulti;

    @ApiModelProperty()
    payload: FileMultiSchemaDefinitionPayload;
}

