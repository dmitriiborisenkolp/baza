import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class EmailSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class EmailSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.Email],
    })
    type: BazaNcIntegrationSchemaType.Email;

    @ApiModelProperty()
    payload: EmailSchemaDefinitionPayload;
}
