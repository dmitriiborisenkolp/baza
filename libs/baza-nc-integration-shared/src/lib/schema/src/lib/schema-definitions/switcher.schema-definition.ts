import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class SwitcherSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class SwitcherSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.Switcher],
    })
    type: BazaNcIntegrationSchemaType.Switcher;

    @ApiModelProperty()
    payload: SwitcherSchemaDefinitionPayload;
}
