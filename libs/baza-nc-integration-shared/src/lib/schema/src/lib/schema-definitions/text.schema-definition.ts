import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class TextSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class TextSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.Text],
    })
    type: BazaNcIntegrationSchemaType.Text;

    @ApiModelProperty()
    payload: TextSchemaDefinitionPayload;
}
