import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { ulid } from 'ulid';

export class SchemaDefinitionPayload {
    @ApiModelProperty({
        description: 'Unique auto-generated id',
    })
    id?: string;

    @ApiModelProperty({
        description: 'Internal ID used for sorting. Definitions array is already sorted, do not sort by this ID!',
    })
    sortId?: number;

    @ApiModelProperty({
        description: 'Tab Id. If undefined, field will be displayed in common "Properties" tab',
        required: false,
        example: ulid(),
    })
    tabId?: string;

    @ApiModelProperty({
        description: 'Field Name',
    })
    field: string;

    @ApiModelProperty({
        description: 'Title for Field',
    })
    title: string;

    @ApiModelProperty({
        description: 'Mark Field as Required',
    })
    required: boolean;

    @ApiModelProperty({
        description: 'Should be displayed in Details tab?',
    })
    isDisplayedInDetails: boolean;
}
