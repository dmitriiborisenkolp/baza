import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class TextAreaSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class TextAreaSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.TextArea],
    })
    type: BazaNcIntegrationSchemaType.TextArea;

    @ApiModelProperty()
    payload: TextAreaSchemaDefinitionPayload;
}
