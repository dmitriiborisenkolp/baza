import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class TimeSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class TimeSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.Time],
    })
    type: BazaNcIntegrationSchemaType.Time;

    @ApiModelProperty()
    payload: TimeSchemaDefinitionPayload;
}
