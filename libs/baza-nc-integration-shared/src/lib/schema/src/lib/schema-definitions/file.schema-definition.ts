import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class FileSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class FileSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.File],
    })
    type: BazaNcIntegrationSchemaType.File;

    @ApiModelProperty()
    payload: FileSchemaDefinitionPayload;
}
