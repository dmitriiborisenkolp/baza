import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class KeyValueSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class KeyValueSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.KeyValue],
    })
    type: BazaNcIntegrationSchemaType.KeyValue;

    @ApiModelProperty()
    payload: KeyValueSchemaDefinitionPayload;
}
