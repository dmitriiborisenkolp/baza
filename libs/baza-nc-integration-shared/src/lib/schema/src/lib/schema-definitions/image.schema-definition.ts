import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class ImageSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class ImageSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.Image],
    })
    type: BazaNcIntegrationSchemaType.Image;

    @ApiModelProperty()
    payload: ImageSchemaDefinitionPayload;
}
