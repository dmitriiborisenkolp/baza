import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class DateSchemaDefinitionPayload extends SchemaDefinitionPayload {
}

export class DateSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.Date],
    })
    type: BazaNcIntegrationSchemaType.Date;

    @ApiModelProperty()
    payload: DateSchemaDefinitionPayload;
}
