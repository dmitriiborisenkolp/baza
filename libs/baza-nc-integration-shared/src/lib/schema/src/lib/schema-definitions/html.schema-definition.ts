import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BazaNcIntegrationSchemaType } from '../models/baza-nc-integration-schema-type';
import { SchemaDefinitionPayload } from './_.schema-definition';

export class HtmlSchemaDefinitionPayload extends SchemaDefinitionPayload {}

export class HtmlSchemaDefinition {
    @ApiModelProperty({
        type: 'string',
        enum: [BazaNcIntegrationSchemaType.HTML],
    })
    type: BazaNcIntegrationSchemaType.HTML;

    @ApiModelProperty()
    payload: HtmlSchemaDefinitionPayload;
}
