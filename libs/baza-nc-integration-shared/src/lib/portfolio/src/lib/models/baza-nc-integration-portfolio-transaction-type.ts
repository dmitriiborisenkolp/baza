export enum BazaNcIntegrationPortfolioTransactionType {
    Investment = 'Investment',
    Dividends = 'Dividends',
    SellOrder = 'Sell Order',
    Transfer = 'Transfer',
}
