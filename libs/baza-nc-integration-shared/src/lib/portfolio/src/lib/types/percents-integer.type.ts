import { SchemaObjectMetadata } from '@nestjs/swagger/dist/interfaces/schema-object-metadata.interface';

export type PercentsIntegerType = number; // 3, 5, 85

export const PercentsIntegerSchemaDefinition: () => SchemaObjectMetadata = () => ({
    type: 'number',
    example: 3,
    description: 'Percents as integer value (3 => 3, 3.6 => 4, 3.65 => 4, 3.657788 => 4)',
});
