import { SchemaObjectMetadata } from '@nestjs/swagger/dist/interfaces/schema-object-metadata.interface';

export type MoneyAsCents = number; // Example: 15.55$ => 1555

export const moneyAsCentsSchemaDefinition: () => SchemaObjectMetadata = () => ({
    type: 'number',
    example: 15055,
    description: 'Money as cents / integer value ($150.55 => 15055, $150.00 => 15000)',
});
