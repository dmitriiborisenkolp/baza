import { SchemaObjectMetadata } from '@nestjs/swagger/dist/interfaces/schema-object-metadata.interface';

export type DateType = string; // UTC - 2020-08-27T11:35:19.987Z

export const dateTypeSchemaDefinition: () => SchemaObjectMetadata = () => ({
    type: 'string',
    example: '2020-08-27T11:35:19.987Z',
    description: 'Date',
});
