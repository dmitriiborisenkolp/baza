import { SchemaObjectMetadata } from '@nestjs/swagger/dist/interfaces/schema-object-metadata.interface';

export type sharesType = number; // 1, 2, 41

export const sharesSchemaDefinition: () => SchemaObjectMetadata = () => ({
    type: 'number',
    example: 1,
    description: 'Number of shares',
});
