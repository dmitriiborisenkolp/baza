import { BazaNcDividendDto } from '@scaliolabs/baza-nc-shared';

export class BazaNcIntegrationPortfolioTransactionDividendEntityDto extends BazaNcDividendDto {}
