export const bazaNcIntegrationListingCmsEnI18n = {
    listings: {
        title: 'Listings',
        types: {
            offeringStatus: {
                open: 'Open',
                'coming-soon': 'Coming Soon',
                closed: 'Closed',
            },
            subscribeStatus: {
                NotSubscribed: 'Not Subscribed',
                Subscribed: 'Subscribed',
                Unsubscribed: 'Unsubscribed',
            },
            subscribeMessages: {
                ComingSoonMovedToOpen: 'Opened',
            },
        },
        components: {
            listings: {
                title: 'Listings',
                tabs: {
                    general: 'General',
                    images: 'Images',
                    properties: 'Properties',
                    offering: 'Offering',
                    statements: 'Statements',
                },
                legend: {
                    invalid: 'Listing should be re-saved',
                    inactive: 'Listing is not published',
                },
                actions: {
                    add: 'Add Listings',
                    edit: 'Edit Listings',
                    delete: {
                        title: 'Delete Listings',
                        confirm: 'Do you really want to delete listings?',
                        success: 'Listings "{{ title }}" deleted',
                        failed: 'Failed to delete listings "{{ title }}"',
                    },
                    changeStatus: 'Change Status',
                    moveUp: 'Move Up',
                    moveDown: 'Move Down',
                    reindex: 'Reindex Listing',
                    reindexAll: {
                        title: 'Reindex all Listings',
                        confirm: 'Do you really want to Reindex (Update) all Listings?',
                        success: 'Listings will be shortly updated',
                    },
                },
                columns: {
                    id: 'ID',
                    title: 'Title',
                    ncOfferingId: 'NC Offering ID',
                    status: 'Status',
                    eliteInvestors: 'Elite Investors',
                    subscriptions: 'Subscriptions',
                    perks: 'Perks',
                    timeline: 'Timeline',
                    testimonials: 'Testimonials',
                },
                form: {
                    fields: {
                        isPublished: 'Is Published?',
                        title: 'Title',
                        description: 'Description',
                        cover: 'Cover',
                        images: 'Images',
                        metadata: 'Metadata',
                        sharesToJoinEliteInvestors: 'Shares to Join Elite Investors',
                        schema: 'Type',
                    },
                    hints: {
                        schemaUpdatedHint:
                            'Schema was updated and Listings is not synced with changes. You can keep same properties & re-save entity or you can select actual schema in list and update properties depends on new updates',
                    },
                    values: {
                        noSchema: '– None –',
                    },
                },
                control: {
                    noSchema: {
                        title: 'No Schema selected',
                        message: 'Select Type on General type',
                    },
                    noFields: {
                        title: 'No fields defined in Schema',
                        message: 'Schema has no defined fields. Go to Schema CMS and add fields.',
                    },
                },
                statements: {
                    fields: {
                        name: 'Document Name',
                        description: 'Description',
                        attachment: 'Document Attachment',
                    },
                    actions: {
                        add: 'Add Attachment',
                        remove: 'Remove Statement',
                        moveUp: 'Move Up',
                        moveDown: 'Move Down',
                    },
                },
            },
            listingsTimeline: {
                title: '{{ title }} / Timeline',
                navigation: 'Timeline',
            },
        },
    },
};
