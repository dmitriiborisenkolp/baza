export enum BazaNcIntegrationListingsErrorCodes {
    BazaNcIntegrationListingsNotFound = 'BazaNcIntegrationListingsNotFound',
    BazaNcIntegrationListingRoundFailure = 'BazaNcIntegrationListingRoundFailure',
    BazaNcIntegrationListingOfferingDuplicate = 'BazaNcIntegrationListingOfferingDuplicate',
}

export const bazaNcIntegrationListingsErrorCodesI18n = {
    [BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound]: 'Listing not found',
    [BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingRoundFailure]: 'Some values are not Integers',
    [BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingOfferingDuplicate]: 'Listing with this offering already exists',
};
