import { ApiModelProperty } from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { AttachmentDto } from "@scaliolabs/baza-core-shared";
import { IsNotEmpty, IsObject, IsString } from "class-validator";

export class BazaNcIntegrationListingCmsDocumentsDto {
    @ApiModelProperty({
        type: 'string',
        description: 'Document Name',
    })
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiModelProperty({
        type: 'string',
        description: 'Document Description',
    })
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiModelProperty({
        type: AttachmentDto,
        isArray: true,
        description: 'Uploaded File',
    })
    @IsObject()
    @IsNotEmpty()
    attachment: AttachmentDto;
}
