import { ApiModelProperty } from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import { IsNotEmpty, IsString } from "class-validator";

export class BazaNcIntegrationListingDocumentsDto {
    @ApiModelProperty({
        type: 'string',
        description: 'Document Name',
    })
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiModelProperty({
        type: 'string',
        description: 'Document Description',
    })
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiModelProperty({
        type: 'string',
        description: 'Document Attachment URL',
    })
    @IsString()
    @IsNotEmpty()
    documentUrl: string;
}
