// eslint-disable-next-line @typescript-eslint/no-namespace
export const BazaNcIntegrationListingsConstants = {
    LISTINGS_COVER_RATIO: 1.6,
    LISTINGS_IMAGES_RATIO: 1.6,
};
