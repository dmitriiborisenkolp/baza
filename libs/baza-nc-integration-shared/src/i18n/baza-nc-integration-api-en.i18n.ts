import { bazaNcIntegrationAclEnI18n } from '../lib/baza-nc-integration-acl';

export const bazaNcIntegrationApiEnI18n: Record<string, unknown> = {
    __acl: {
        ...bazaNcIntegrationAclEnI18n,
    },
};
