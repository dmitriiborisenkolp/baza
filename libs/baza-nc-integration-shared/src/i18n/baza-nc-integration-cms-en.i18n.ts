import { bazaNcIntegrationAclEnI18n } from '../lib/baza-nc-integration-acl';
import { bazaNcIntegrationSchemaCmsEnI18n } from '../lib/schema/src';
import { bazaNcIntegrationListingTestimonialsCmsI18n } from '../lib/testimonials/src';
import { bazaNcIntegrationPerkCmsEnI18n } from '../lib/perk/src';
import { bazaNcIntegrationFeedCmsEnI18n } from '../lib/feed/src';
import { bazaNcIntegrationEliteInvestorCmsEnI18n } from '../lib/elite-investor/src';
import { bazaNcIntegrationListingCmsEnI18n } from '../lib/listings/src';
import { bazaNcIntegrationSubscriptionsCmsEnI18n } from '../lib/subscription/src';

export const bazaNcIntegrationCmsEnI18n = {
    __acl: {
        ...bazaNcIntegrationAclEnI18n,
    },
    bazaNcIntegration: {
        __types: {
            BazaNcIntegrationSubscribeStatus: {
                NotSubscribed: 'Not Subscribed',
                Subscribed: 'Subscribed',
                Unsubscribed: 'Unsubscribed',
            },
            BazaNcIntegrationSubscribeMessages: {
                ComingSoonMovedToOpen: 'Opened',
            },
        },
        sidebar: {
            title: 'Baza NC Integration',
        },
        ...bazaNcIntegrationListingCmsEnI18n,
        ...bazaNcIntegrationEliteInvestorCmsEnI18n,
        ...bazaNcIntegrationFeedCmsEnI18n,
        ...bazaNcIntegrationPerkCmsEnI18n,
        ...bazaNcIntegrationSchemaCmsEnI18n,
        ...bazaNcIntegrationListingTestimonialsCmsI18n,
        ...bazaNcIntegrationSubscriptionsCmsEnI18n,
    },
};
