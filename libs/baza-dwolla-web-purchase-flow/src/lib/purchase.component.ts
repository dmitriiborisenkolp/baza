import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { DestroySessionDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { skipWhile, take } from 'rxjs';
import { DwollaCancelPurchase, DwollaPurchaseState, DwollaSharedService, PurchaseConfig } from './data-access';

@UntilDestroy()
@Component({
    selector: 'app-purchase-dwolla',
    templateUrl: './purchase.component.html',
    styleUrls: ['./purchase.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaPurchaseComponent implements OnInit {
    @Input()
    config?: PurchaseConfig;

    cart$ = this.store.select(DwollaPurchaseState.cart);
    defaultConfig?: PurchaseConfig;

    public currentTab: number;
    public item: BazaNcIntegrationListingsDto;
    public i18nBasePath = 'dwpf.parent';

    constructor(
        private readonly route: ActivatedRoute,
        private readonly store: Store,
        private readonly notification: NzNotificationService,
        private readonly router: Router,
        private readonly dss: DwollaSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    ngOnInit(): void {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;

            if (params.numberOfShares === null) {
                this.notification.warning(this.wts.getI18nLabel(this.i18nBasePath, 'warnings.pickShares'), '');
                this.router.navigate([this.config.linksConfig.buyShares, 'details']);
            }
        });

        this.cart$
            .pipe(
                untilDestroyed(this),
                skipWhile((res) => !res),
                take(1),
            )
            .subscribe((response) => {
                this.item = response;
                this.checkPurchaseConfig();
            });
    }

    cancelPurchase() {
        if (this.item) {
            const data: DestroySessionDto = {
                offeringId: this.item.offeringId,
            };
            this.store.dispatch(new DwollaCancelPurchase(data));
        }
    }

    checkPurchaseConfig() {
        this.defaultConfig = {
            linksConfig: {
                buyShares: '/buy-shares',
                verification: '/verification',
                backLink: {
                    appLink: {
                        commands: ['/items', this.item.id.toString()],
                    },
                },
            },
        };

        this.config = this.wts.mergeConfig(this.defaultConfig, this.config);
        this.dss.pfLinksConfig = this.config.linksConfig;
    }
}
