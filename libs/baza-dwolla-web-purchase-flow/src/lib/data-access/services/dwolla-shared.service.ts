import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { untilDestroyed } from '@ngneat/until-destroy';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngxs/store';
import { BazaRegistryNgService } from '@scaliolabs/baza-core-ng';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcBankAccountAchDto,
    BazaNcBankAccountAddRequest,
    BazaNcBankAccountExport,
    BazaNcBankAccountIdentity,
    BazaNcBankAccountLinkOnSuccessRequest,
    BazaNcBankAccountType,
    BazaNcBootstrapDto,
    BazaNcCreditCardDto,
    BazaNcDwollaCustomerDetailDto,
    BazaNcPurchaseFlowGetCreditCardResponse,
    BazaNcPurchaseFlowSetCreditCardRequest,
    BazaNcTransferRequest,
    BazaNcWithdrawRequest,
} from '@scaliolabs/baza-nc-shared';
import { BazaPlaidLinkResponseDto } from '@scaliolabs/baza-plaid-shared';
import { EffectsUtil, Message, onlyNumbersValidator, restrictedCharsValidator, routingNumberValidator } from '@scaliolabs/baza-web-utils';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { NgxPlaidLinkService, PlaidConfig, PlaidLinkHandler } from 'ngx-plaid-link';
import { catchError, Observable, of, Subject, tap } from 'rxjs';
import { AccountBalanceExitReason, AddFundsForm, BankAccountForm, CardDetailsForm, PaymentMethodType, PFLinksConfig } from '../models';
import { WithdrawFundsForm } from '../models/interfaces/forms/withdraw-funds-form.interface';
import {
    DwollaInvestorAccountTouch,
    DwollaLoadPlaidLink,
    DwollaPlaidOnSuccessLink,
    DwollaSaveBankAccount,
    DwollaSaveCreditCard,
    DwollaTransferFunds,
    WithdrawFunds,
    PurchaseStateModel,
} from '../store';

/**
 * A shared service for Dwolla related functionality
 */
@Injectable({
    providedIn: 'root',
})
export class DwollaSharedService {
    private plaidLinkHandler: PlaidLinkHandler;

    private config: PlaidConfig = {
        apiVersion: 'v2',
        env: 'sandbox',
        selectAccount: false,
        token: null,
        webhook: '',
        product: ['auth'],
        key: null,
        onSuccess: () => null,
        onExit: () => null,
    };

    showDwollaAccountCreationError$: Subject<boolean> = new Subject<boolean>();
    updateCashOutAccount$: Subject<void> = new Subject<void>();
    pfLinksConfiguration: PFLinksConfig;

    constructor(
        private readonly fb: FormBuilder,
        private readonly store: Store,
        private readonly notification: NzNotificationService,
        private readonly effectsUtil: EffectsUtil,
        private readonly plaidLinkService: NgxPlaidLinkService,
        private readonly registry: BazaRegistryNgService,
        private readonly translateService: TranslateService,
    ) {}

    // #region Accessors
    get techSupportEmail(): string {
        return this.registry.value('bazaContentTypes.contacts.email') ?? '';
    }

    public get pfLinksConfig() {
        return this.pfLinksConfiguration;
    }

    public set pfLinksConfig(config: PFLinksConfig) {
        this.pfLinksConfiguration = config;
    }
    // #endregion

    // #region Utility functions
    isForeignInvestor(initData: BazaNcBootstrapDto): boolean {
        return initData?.investorAccount?.status?.isForeign ?? true;
    }

    isDwollaAvailable(source: BazaNcBootstrapDto | BazaNcDwollaCustomerDetailDto): boolean {
        if (!source || typeof source !== 'object') {
            return false;
        }

        if ('investorAccount' in source) {
            return (source as BazaNcBootstrapDto)?.investorAccount?.isDwollaAvailable ?? false;
        } else {
            return (source as BazaNcDwollaCustomerDetailDto)?.isDwollaAvailable ?? false;
        }
    }

    isDwollaCashInAccountLinked(initData: BazaNcBootstrapDto): boolean {
        return initData?.investorAccount?.isBankAccountCashInLinked ?? false;
    }

    isDwollaCashOutAccountLinked(initData: BazaNcBootstrapDto): boolean {
        return initData?.investorAccount?.isBankAccountCashOutLinked ?? false;
    }

    isNCBankAccountAvailable(source: BazaNcBootstrapDto | BazaNcBankAccountAchDto): boolean {
        if (!source || typeof source !== 'object') {
            return false;
        }

        if ('investorAccount' in source) {
            return (source as BazaNcBootstrapDto)?.investorAccount?.isBankAccountNcAchLinked ?? false;
        } else {
            return (source as BazaNcBankAccountAchDto)?.isAvailable ?? false;
        }
    }

    isCardAvailable(source: BazaNcBootstrapDto | BazaNcPurchaseFlowGetCreditCardResponse): boolean {
        if (!source || typeof source !== 'object') {
            return false;
        }

        if ('investorAccount' in source) {
            return (source as BazaNcBootstrapDto)?.investorAccount?.isCreditCardLinked ?? false;
        } else {
            return (source as BazaNcPurchaseFlowGetCreditCardResponse)?.isAvailable ?? false;
        }
    }

    isAccBalanceSelectedAsPaymentMethod(selectedPaymentMethod: PaymentMethodType): boolean {
        return selectedPaymentMethod === PaymentMethodType.accountBalance;
    }

    isNCBankAccSelectedAsPaymentMethod(selectedPaymentMethod: PaymentMethodType): boolean {
        return selectedPaymentMethod === PaymentMethodType.bankAccount;
    }

    isCCSelectedAsPaymentMethod(selectedPaymentMethod: PaymentMethodType): boolean {
        return selectedPaymentMethod === PaymentMethodType.creditCard;
    }

    getDwollaBalanceAmount(dwollaCustomerDetail: BazaNcDwollaCustomerDetailDto): number {
        return parseFloat(dwollaCustomerDetail?.balance?.value ?? '0');
    }

    getMinNumberOfSharesForEntity(number: number, entity: BazaNcIntegrationListingsDto) {
        let numberOfShares: number;

        if (entity?.numSharesMin && entity?.numSharesMin > number) {
            numberOfShares = entity.numSharesMin;
        } else {
            numberOfShares = number || entity.numSharesMin;
        }

        return numberOfShares;
    }
    // #endregion

    // #region Forms initialization
    public generateCreditCardForm = (): CardDetailsForm =>
        this.fb.group({
            creditCardholderName: [null, Validators.compose([Validators.required, restrictedCharsValidator({ bypassChars: ['-'] })])],
            creditCardNumber: [null, Validators.compose([Validators.required, Validators.minLength(16), onlyNumbersValidator()])],
            expireDate: [null, [Validators.required]],
            creditCardCvv: [
                null,
                Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(4), onlyNumbersValidator()]),
            ],
        }) as CardDetailsForm;

    public generateBankDetailsForm = (): BankAccountForm =>
        this.fb.group({
            accountName: [null, Validators.compose([Validators.required, restrictedCharsValidator({ bypassChars: ['-'] })])],
            accountType: [null, [Validators.required]],
            accountNumber: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            accountRoutingNumber: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(9),
                    Validators.maxLength(9),
                    onlyNumbersValidator(),
                    routingNumberValidator(),
                ]),
            ],
        }) as BankAccountForm;

    public generateAddFundsForm = (): AddFundsForm =>
        this.fb.group({
            transferAmount: [null, [Validators.required]],
        }) as AddFundsForm;

    public generateWithdrawFundsForm = (): WithdrawFundsForm =>
        this.fb.group({
            withdrawAmount: [null, [Validators.required]],
        }) as WithdrawFundsForm;
    // #endregion

    // #region Forms submission
    public submitCardForm = (cardDetailsForm: FormGroup): Observable<BazaNcCreditCardDto> => {
        if (!cardDetailsForm) {
            throw new Error('Source parameter is not defined: CardDetailsForm');
        }

        const expireDate = cardDetailsForm.get('expireDate').value.split('/');
        const month = parseInt(expireDate[0], 10),
            year = parseInt(expireDate[1], 10);
        const data: BazaNcPurchaseFlowSetCreditCardRequest = {
            creditCardholderName: cardDetailsForm.get('creditCardholderName').value,
            creditCardNumber: cardDetailsForm.get('creditCardNumber').value,
            creditCardCvv: cardDetailsForm.get('creditCardCvv').value,
            creditCardExpireMonth: month,
            creditCardExpireYear: year,
        };
        return this.store.dispatch(new DwollaSaveCreditCard(data));
    };

    processSubmitCardForm = (srcForm: FormGroup, onSuccess) => {
        this.updateCardFormValidity(srcForm);
        if (srcForm) {
            this.submitCardForm(srcForm)
                .pipe(this.effectsUtil.tryCatchNoRethrow$('', ''))
                .subscribe(() => {
                    onSuccess();
                });
        }
    };

    public submitManualBankDetailsForm = (bankDetailsForm: FormGroup) => {
        if (!bankDetailsForm) {
            throw new Error('Source parameter is not defined: BankAccountForm');
        }

        const data: BazaNcBankAccountAddRequest = {
            ...bankDetailsForm.value,
            accountNickName: bankDetailsForm.get('accountName').value,
            type: BazaNcBankAccountType.CashIn,
            export: [BazaNcBankAccountExport.NC],
            setAsDefault: false,
            static: false,
            secure: true,
        };

        return this.store.dispatch(new DwollaSaveBankAccount(data));
    };

    processSubmitManualBankDetailsForm = (srcForm: FormGroup, onSuccess) => {
        this.updateBankAccountFormValidity(srcForm);
        if (srcForm.valid) {
            this.submitManualBankDetailsForm(srcForm)
                .pipe(this.effectsUtil.tryCatchNoRethrow$('', ''))
                .subscribe(() => {
                    onSuccess();
                });
        }
    };

    processSubmitAddFundsForm = (
        addFundsForm: FormGroup,
        options: { contactLink?: string } = {
            contactLink: `mailto:${this.techSupportEmail}`,
        },
    ) => {
        if (!addFundsForm) {
            throw new Error('Source parameter is not defined: AddFundsForm');
        }

        const transferRequest: BazaNcTransferRequest = {
            amount: addFundsForm.get('transferAmount').value.toString(),
        };

        return this.store.dispatch(new DwollaTransferFunds(transferRequest)).pipe(
            untilDestroyed(this),
            catchError(() =>
                of({
                    type: 'error',
                    text: this.translateService.instant('uic.addFunds.form.alerts.amountError', { link: options.contactLink }),
                } as Message),
            ),
        );
    };

    processSubmitWithdrawFundsForm = (
        withdrawFundsForm: FormGroup,
        options: { contactLink?: string } = {
            contactLink: `mailto:${this.techSupportEmail}`,
        },
    ) => {
        if (!withdrawFundsForm) {
            throw new Error('Source parameter is not defined: WithdrawFundsForm');
        }

        const withdrawRequest: BazaNcWithdrawRequest = {
            amount: withdrawFundsForm.get('withdrawAmount').value.toString(),
        };

        return this.store.dispatch(new WithdrawFunds(withdrawRequest)).pipe(
            untilDestroyed(this),
            catchError(() =>
                of({
                    type: 'error',
                    text: this.translateService.instant('uic.withdrawFunds.form.alerts.withdrawalError', { link: options.contactLink }),
                } as Message),
            ),
        );
    };
    // #endregion

    // #region Forms validity checks
    public updateCardFormValidity = (cardDetailsForm: FormGroup): void => {
        for (const key of Object.keys(cardDetailsForm?.controls)) {
            cardDetailsForm.controls[`${key}`].markAsDirty();
            cardDetailsForm.controls[`${key}`].updateValueAndValidity();
        }
    };

    public updateBankAccountFormValidity = (bankDetailsForm: FormGroup): void => {
        for (const key of Object.keys(bankDetailsForm?.controls)) {
            bankDetailsForm.controls[`${key}`].markAsDirty();
            bankDetailsForm.controls[`${key}`].updateValueAndValidity();
        }
    };
    // #endregion

    // #region Plaid utilities
    public getDwollaPlaidLink = () => {
        return this.store.dispatch(new DwollaLoadPlaidLink());
    };

    /**
     *This method is used to process the Dwolla Plaid flow using the ngx-plaid-link library.
     * It retrieves the Dwolla Plaid link and creates a Plaid link using the ngx-plaid-link library.
     *
     * @param config - An object containing the following properties:
     * onSuccess: (optional) a callback function that will be called with
     *            the public token and identity of the linked bank account,
     *            after the Plaid flow is completed successfully.
     *
     * onExit: (optional) a callback function that will be called with the error
     *         and identity of the linked bank account, after the Plaid flow is
     *         completed or cancelled.
     *
     * @example
     * processDwollaPlaidFlow({
     *   onSuccess: (publicToken, identity) => {
     *     console.log(`Successfully linked bank account with public token: ${publicToken} and identity ${identity}`);
     *   },
     *   onExit: () => {
     *     console.log(`Plaid flow closed`);
     *   },
     * });
     *
     * @see https://www.npmjs.com/package/ngx-plaid-link
     */
    public processDwollaPlaidFlow(config?: {
        onSuccess?: (publicToken: string, identity: BazaNcBankAccountIdentity) => void;
        onExit?: () => void;
    }) {
        this.getDwollaPlaidLink()
            .pipe(
                untilDestroyed(this),
                tap(({ dwollaPurchase }) => {
                    const plaidResponse = dwollaPurchase?.dwollaPlaidResponse as BazaPlaidLinkResponseDto;

                    if (plaidResponse) {
                        this.plaidLinkService
                            .createPlaid(
                                Object.assign(
                                    {},
                                    {
                                        ...this.config,
                                        token: plaidResponse?.linkToken,
                                        env: plaidResponse?.environment,
                                        onSuccess: (token: string, identity: BazaNcBankAccountIdentity) => {
                                            config?.onSuccess && config.onSuccess(token, identity);
                                        },
                                        onExit: () => {
                                            config?.onExit && config.onExit();
                                        },
                                    },
                                ),
                            )
                            .then((handler: PlaidLinkHandler) => {
                                this.plaidLinkHandler = handler;
                                this.plaidLinkHandler.open();
                            });
                    } else {
                        this.notification.error('Could not link payment', '');
                    }
                }),
            )
            .subscribe();
    }

    public processDwollaLinkBankAccount(data: BazaNcBankAccountLinkOnSuccessRequest, onSuccess: () => void): void {
        this.store
            .dispatch(new DwollaPlaidOnSuccessLink(data))
            .pipe(untilDestroyed(this))
            .subscribe(() => {
                onSuccess();
            });
    }
    // #endregion

    // #region Event handlers
    public processAccountBalanceCreation({
        isDwollaAvailable,
        onExit,
        onInvestorTouched,
    }: {
        isDwollaAvailable: boolean;
        onExit: (reason: AccountBalanceExitReason) => void;
        onInvestorTouched: (dwollaPurchase: PurchaseStateModel | null) => void;
    }): void {
        if (isDwollaAvailable) {
            return onExit(AccountBalanceExitReason.DwollaBalanceAlreadyAvailable);
        }

        this.store
            .dispatch(new DwollaInvestorAccountTouch())
            .pipe(
                untilDestroyed(this),
                tap(({ dwollaPurchase }) => onInvestorTouched(dwollaPurchase)),
            )
            .subscribe();
    }
    // #endregion
}
