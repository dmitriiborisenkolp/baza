export enum PaymentEditModalsContentType {
    paymentAdd = 'paymentAdd',
    paymentList = 'paymentList',
    paymentUpdate = 'paymentUpdate',
    paymentUpdateAccount = 'paymentUpdateAccount',
    paymentUpdateDetails = 'paymentUpdateDetails',
}

export enum PaymentMethodType {
    bankAccount = 'bankAccount',
    creditCard = 'creditCard',
    accountBalance = 'accountBalance',
}

export enum PaymentUpdateAccountType {
    creditCard = 'creditCard',
    fundAccountBalance = 'fundAccountBalance',
    linkBankAccount = 'linkBankAccount',
    manuallyAddBankAccount = 'manuallyAddBankAccount',
}
