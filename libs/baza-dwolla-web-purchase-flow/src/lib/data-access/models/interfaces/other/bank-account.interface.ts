export interface BankAccount {
    isAvailable: boolean;
    details?: BankAccountDetails;
}

export interface BankAccountDetails {
    accountName: string;
    accountNickName: string;
    accountRoutingNumber: string;
    accountNumber: string;
    accountType: string;
}
