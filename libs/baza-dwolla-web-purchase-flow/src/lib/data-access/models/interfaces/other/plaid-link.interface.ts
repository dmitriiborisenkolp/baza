export interface DwollaPlaidLinkResponse {
    linkToken: string;
    environment: string;
}
