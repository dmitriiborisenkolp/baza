import { AbstractControl, FormGroup } from '@angular/forms';

// Card Details form interface
export interface CardDetailsForm extends FormGroup {
    controls: {
        creditCardholderName: AbstractControl;
        creditCardNumber: AbstractControl;
        expireDate: AbstractControl;
        creditCardCvv: AbstractControl;
    };
}
