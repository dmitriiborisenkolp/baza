import { AbstractControl, FormGroup } from '@angular/forms';

// Bank Account form interface
export interface BankAccountForm extends FormGroup {
    controls: {
        accountName: AbstractControl;
        accountType: AbstractControl;
        accountNumber: AbstractControl;
        accountRoutingNumber: AbstractControl;
    };
}
