import { AbstractControl, FormGroup } from '@angular/forms';

export interface AddFundsForm extends FormGroup {
    controls: {
        transferAmount: AbstractControl;
    };
}
