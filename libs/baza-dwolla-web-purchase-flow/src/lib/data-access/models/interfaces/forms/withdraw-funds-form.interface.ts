import { AbstractControl, FormGroup } from '@angular/forms';

export interface WithdrawFundsForm extends FormGroup {
    controls: {
        transferAmount: AbstractControl;
    };
}
