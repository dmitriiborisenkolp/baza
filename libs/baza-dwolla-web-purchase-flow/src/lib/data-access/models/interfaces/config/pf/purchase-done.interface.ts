import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * Interface for configuring the purchase done page
 */
export interface PurchaseDoneConfig {
    /**
     * The link config for the cta button
     * @see LinkConfig
     */
    ctaBtnLink?: LinkConfig;

    /**
     * The config for the cta
     * @see LinkConfig
     */
    cta?: LinkConfig;
}
