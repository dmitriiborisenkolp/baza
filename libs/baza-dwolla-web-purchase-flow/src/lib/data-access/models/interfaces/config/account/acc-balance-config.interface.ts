import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * AccBalanceConfig interface for account balance configuration
 * @see {@link https://baza-docs.test.scaliolabs.com/web-development/customizations/account-components/01-account-balance-config}
 */
export interface AccBalanceConfig {
    links?: ABLinksConfig;
}

/**
 * Interface for navigation links in the account balance component
 */
interface ABLinksConfig {
    /**
     *  Navigation object used for internal or external link configuration
     */
    contact?: LinkConfig;
}
