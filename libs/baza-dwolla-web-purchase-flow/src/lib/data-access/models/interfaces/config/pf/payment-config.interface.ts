import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * Interface for configuring the payment method
 */
export interface PaymentMethodConfig {
    /**
     * Indicates whether to show the purchase dividend message or not
     */
    showPurchaseDividendMessage?: boolean;
}

/**
 * Interface for configuring the payment steps
 */
export interface PaymentStepsConfig {
    /**
     * The config for the step links
     * @see StepLinksConfig
     */
    links?: StepLinksConfig;
}

/**
 * Interface for configuring the payment
 */
export interface PaymentConfig {
    /**
     * The config for the payment methods
     * @see PaymentMethodConfig
     */
    methods?: PaymentMethodConfig;

    /**
     * The config for the payment steps
     * @see PaymentStepsConfig
     */
    steps?: PaymentStepsConfig;
}

/**
 * Interface for configuring the step links
 */
export interface StepLinksConfig {
    /**
     * The link config for the terms of service
     * @see LinkConfig
     */
    termsOfService?: LinkConfig;

    /**
     * The link config for the EFT disclosure
     * @see LinkConfig
     */
    eftDisclosure?: LinkConfig;
}
