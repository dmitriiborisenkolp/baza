/**
 * DwollaPaymentMethodsConfig interface for control showing diff. sections within payment methods
 * @see {@link https://baza-docs.test.scaliolabs.com/web-development/customizations/account-components/05-payment-methods-config}
 */
export interface DwollaPaymentMethodsConfig {
    /**
     * Show/hide account balance section, true by default
     */
    showAccountBalance?: boolean;
    /**
     * Show/hide bank account section, true by default
     */
    showBankAccount?: boolean;
    /**
     * Show/hide credit card section, true by default
     */
    showCreditCard?: boolean;
}
