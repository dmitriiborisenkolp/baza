import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * Interface for configuring the purchase links config
 */
export interface PFLinksConfig {
    /**
     * The link for buying shares
     */
    buyShares?: string;
    /**
     * The link for verification
     */
    verification?: string;
    /**
     * The link for the back link
     */
    backLink?: LinkConfig;
}

/**
 * Interface for configuring the purchase
 */
export interface PurchaseConfig {
    /**
     * The config for custom links
     */
    linksConfig?: PFLinksConfig;
}
