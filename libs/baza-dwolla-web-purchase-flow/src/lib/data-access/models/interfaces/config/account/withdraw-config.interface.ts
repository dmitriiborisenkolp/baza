import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * WithdrawConfig interface for Withdraw configuration
 * @see {@link https://baza-docs.test.scaliolabs.com/web-development/exportable-features/withdraw-funds/01-index}
 */
export interface WithdrawConfig {
    /**
     * Links configuration
     */
    links?: WthdLinksConfig;
}

/**
 * Interface for navigation links in the payout component
 */
interface WthdLinksConfig {
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    contact?: LinkConfig;
}
