import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * AccPayoutConfig interface for payout configuration
 * @see {@link https://baza-docs.test.scaliolabs.com/web-development/customizations/account-components/04-bank-account-payout-config}
 */
export interface AccPayoutConfig {
    links?: PayoutLinksConfig;
}

/**
 * Interface for navigation links in the payout component
 */
interface PayoutLinksConfig {
    /**
     *  Navigation object used for internal or external link configuration
     */
    contact?: LinkConfig;
}
