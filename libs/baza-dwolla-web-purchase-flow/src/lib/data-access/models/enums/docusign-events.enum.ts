export enum DocuSignEvents {
    SigningComplete = 'signing_complete',
    Cancel = 'cancel',
    Decline = 'decline',
}
