import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcBankAccountAddRequest,
    BazaNcBankAccountGetDefaultByTypeRequest,
    BazaNcBankAccountLinkOnSuccessRequest,
    BazaNcBankAccountType,
    BazaNcPurchaseFlowSetCreditCardRequest,
    BazaNcTransferRequest,
    BazaNcWithdrawRequest,
    DestroySessionDto,
    PurchaseFlowDto,
    PurchaseFlowSessionDto,
    ReProcessPaymentDto,
} from '@scaliolabs/baza-nc-shared';

export class DwollaCancelPurchase {
    static readonly type = '[Purchase] Dwolla Cancel Purchase';

    constructor(public data: DestroySessionDto) {}
}

export class DwollaClearPurchaseState {
    static readonly type = '[Purchase] DwollaClearPurchaseState';
}

export class DwollaGetLimits {
    static readonly type = '[Purchase] DwollaGetLimits';
}

export class DwollaGetLimitsForPurchase {
    static readonly type = '[Purchase] DwollaGetLimitsForPurchase';

    constructor(public requestedAmountCents: number, public offeringId: string) {}
}

export class DwollaGetPurchaseStats {
    static readonly type = '[Purchase] Dwolla Get Purchase Stats';

    constructor(public offeringId: string, public requestedAmountCents: number) {}
}
export class DwollaLoadAccountBalance {
    static readonly type = '[Purchase] DwollaLoadAccountBalance';
}

export class DwollaLoadBankAccount {
    static readonly type = '[Purchase] DwollaLoadBankAccount';
}

export class DwollaLoadCreditCard {
    static readonly type = '[Purchase] DwollaLoadCreditCard';
}

export class DwollaLoadPlaidLink {
    static readonly type = '[Purchase] DwollaLoadPlaidLink';
}

export class DwollaPatchStartPurchase {
    static readonly type = '[Purchase] Dwolla Patch DwollaStartPurchase';

    constructor(public purchaseStart: PurchaseFlowDto) {}
}

export class DwollaDeleteStartPurchase {
    static readonly type = '[Purchase] Dwolla Delete NumberOfShares';

    constructor(public numberOfShares: null) {}
}
export class DwollaSaveBankAccount {
    static readonly type = '[Purchase] DwollaSaveBankAccount';

    constructor(public srcAccount: BazaNcBankAccountAddRequest) {}
}

export class DwollaSaveCreditCard {
    static readonly type = '[Purchase] DwollaSaveCreditCard';

    constructor(public srcCard: BazaNcPurchaseFlowSetCreditCardRequest) {}
}

export class DwollaSelectEntity {
    static readonly type = '[Purchase] Dwolla Select Entity';

    constructor(public entityId: number) {}
}

export class DwollaUpdateCart {
    static readonly type = '[Purchase] Dwolla Update Cart';

    constructor(public entity: BazaNcIntegrationListingsDto) {}
}

export class DwollaStartCart {
    static readonly type = '[Purchase] Dwolla Start Cart';
}

export class DwollaStartPurchase {
    static readonly type = '[Purchase] Dwolla Start Purchase';

    constructor(public data: PurchaseFlowSessionDto) {}
}

export class DwollaSubmitPurchase {
    static readonly type = '[Purchase] Dwolla Submit Purchase';

    constructor(public tradeId: string) {}
}

export class DwollaTogglePaymentEditModal {
    static readonly type = '[Purchase] DwollaTogglePaymentEditModal';

    constructor(public isOpen: boolean) {}
}

export class DwollaReprocessPayment {
    static readonly type = '[Purchase] Dwolla Reprocess Payment';

    constructor(public data: ReProcessPaymentDto) {}
}

export class DwollaInvestorAccountTouch {
    static readonly type = '[Purchase] Dwolla InvestorAccountTouch';
}

export class DwollaPlaidOnSuccessLink {
    static readonly type = '[Purchase] DwollaPlaidOnSuccessLink';

    constructor(public data: BazaNcBankAccountLinkOnSuccessRequest) {}
}

export class DwollaGetDefaultBankAccount {
    static readonly type = '[Purchase] DwollaGetDefaultBankAccount';
    constructor(public type: BazaNcBankAccountType, public request: BazaNcBankAccountGetDefaultByTypeRequest) {}
}

export class DwollaTransferFunds {
    static readonly type = '[Purchase] DwollaTransferFunds';

    constructor(public transferRequest: BazaNcTransferRequest) {}
}

export class WithdrawFunds {
    static readonly type = '[Purchase] WithdrawFunds';

    constructor(public withdrawRequest: BazaNcWithdrawRequest) {}
}
