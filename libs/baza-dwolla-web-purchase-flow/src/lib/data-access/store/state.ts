import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { JwtService } from '@scaliolabs/baza-core-ng';
import { BazaDwollaPaymentDto, DwollaAmount } from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcBankAccountsDataAccess,
    BazaNcDwollaDataAccess,
    BazaNcPurchaseFlowCreditCardDataAccess,
    BazaNcPurchaseFlowDataAccess,
    BazaNcPurchaseFlowLimitsDataAccess,
    BazaNcTransferDataAccess,
    BazaNcWithdrawalDataAccess,
} from '@scaliolabs/baza-nc-data-access';
import { BazaNcIntegrationListingsDataAccess } from '@scaliolabs/baza-nc-integration-data-access';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcBankAccountAchDto,
    BazaNcBankAccountDto,
    BazaNcBankAccountGetDefaultByTypeResponse,
    BazaNcBankAccountLinkOnSuccessResponse,
    BazaNcBankAccountType,
    BazaNcCreditCardDto,
    BazaNcDwollaCustomerDetailDto,
    BazaNcDwollaTouchResponse,
    BazaNcLimitsDto,
    BazaNcPurchaseFlowGetCreditCardResponse,
    BazaNcPurchaseFlowLimitsForPurchaseResponse,
    PurchaseFlowDestroyResponse,
    PurchaseFlowDto,
    PurchaseFlowSubmitResponse,
    ReProcessPaymentResponse,
    StatsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaPlaidLinkResponseDto } from '@scaliolabs/baza-plaid-shared';
import { BazaWebUtilSharedService, EffectsUtil, StorageService } from '@scaliolabs/baza-web-utils';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PaymentMethodType } from '../models';
import {
    DwollaCancelPurchase,
    DwollaClearPurchaseState,
    DwollaDeleteStartPurchase,
    DwollaGetDefaultBankAccount,
    DwollaGetLimits,
    DwollaGetLimitsForPurchase,
    DwollaGetPurchaseStats,
    DwollaInvestorAccountTouch,
    DwollaLoadAccountBalance,
    DwollaLoadBankAccount,
    DwollaLoadCreditCard,
    DwollaLoadPlaidLink,
    DwollaUpdateCart,
    DwollaPatchStartPurchase,
    DwollaPlaidOnSuccessLink,
    DwollaReprocessPayment,
    DwollaSaveBankAccount,
    DwollaSaveCreditCard,
    DwollaSelectEntity,
    DwollaStartCart,
    DwollaStartPurchase,
    DwollaSubmitPurchase,
    DwollaTogglePaymentEditModal,
    DwollaTransferFunds,
    WithdrawFunds,
} from './actions';

export interface PurchaseStateModel {
    cart: BazaNcIntegrationListingsDto | null;
    purchaseStart: PurchaseFlowDto | null;
    stats: StatsDto | null;
    numberOfShares: number | null;
    limits: BazaNcLimitsDto;
    limitForPurchase: BazaNcPurchaseFlowLimitsForPurchaseResponse;
    isPaymentEditModalOpen: boolean;

    // WARN: SEC - NOT SAFE TO TRANSFER RAW DATA!
    creditCard: BazaNcPurchaseFlowGetCreditCardResponse;
    bankAccount: BazaNcBankAccountAchDto;

    dwollaCustomerDetail: BazaNcDwollaCustomerDetailDto;
    dwollaAccountBalance: DwollaAmount;
    dwollaInvestorTouchResponse: BazaNcDwollaTouchResponse;
    dwollaPlaidResponse: BazaPlaidLinkResponseDto;
    dwollaDefaultCashInAccount: BazaNcBankAccountGetDefaultByTypeResponse;
    dwollaDefaultCashOutAccount: BazaNcBankAccountGetDefaultByTypeResponse;
    selectedPaymentMethod: PaymentMethodType;
}

const initState = {
    name: 'dwollaPurchase',
    defaults: {
        bankAccount: null,
        cart: undefined,
        creditCard: null,
        isMoveToPayment: null,
        isOpenAgreementModal: null,
        isPaymentEditModalOpen: false,
        limitForPurchase: null,
        limits: null,
        numberOfShares: null,
        dwollaPlaidResponse: null,
        purchaseStart: null,
        stats: null,
        dwollaCustomerDetail: null,
        dwollaAccountBalance: null,
        dwollaInvestorTouchResponse: null,
        dwollaDefaultCashInAccount: null,
        dwollaDefaultCashOutAccount: null,
        selectedPaymentMethod: null,
    },
};

const i18nBasePath = 'dwpf.notifications';

@State<PurchaseStateModel>(initState)
@Injectable()
export class DwollaPurchaseState {
    constructor(
        private readonly bazaNcBankAccountsDataAccess: BazaNcBankAccountsDataAccess,
        public readonly dwollaDataAccess: BazaNcDwollaDataAccess,
        private readonly creditCardDataAccessService: BazaNcPurchaseFlowCreditCardDataAccess,
        private readonly dataAccess: BazaNcPurchaseFlowDataAccess,
        private readonly effectsUtil: EffectsUtil,
        private readonly jwtService: JwtService,
        private readonly limitsDataAccess: BazaNcPurchaseFlowLimitsDataAccess,
        private readonly storageService: StorageService,
        private readonly itemDataAccess: BazaNcIntegrationListingsDataAccess,
        private readonly bazaNcTransferDataAccess: BazaNcTransferDataAccess,
        private readonly bazaNcWithdrawDataAccess: BazaNcWithdrawalDataAccess,
        private readonly wts: BazaWebUtilSharedService,
    ) {}

    @Selector()
    static bankAccount(state: PurchaseStateModel): BazaNcBankAccountAchDto {
        return state.bankAccount;
    }

    @Selector()
    static dwollaCustomerDetail(state: PurchaseStateModel): BazaNcDwollaCustomerDetailDto {
        return state.dwollaCustomerDetail;
    }

    @Selector()
    static accountBalance(state: PurchaseStateModel): DwollaAmount {
        return state.dwollaAccountBalance;
    }

    @Selector()
    static dwollaDefaultCashInAccount(state: PurchaseStateModel): BazaNcBankAccountGetDefaultByTypeResponse {
        return state.dwollaDefaultCashInAccount;
    }

    @Selector()
    static dwollaDefaultCashOutAccount(state: PurchaseStateModel): BazaNcBankAccountGetDefaultByTypeResponse {
        return state.dwollaDefaultCashOutAccount;
    }

    @Selector()
    static cart(state: PurchaseStateModel): BazaNcIntegrationListingsDto {
        return state.cart;
    }

    @Selector()
    static creditCard(state: PurchaseStateModel): BazaNcPurchaseFlowGetCreditCardResponse {
        return state.creditCard;
    }

    @Selector()
    static isPaymentEditModalOpen(state: PurchaseStateModel): boolean {
        return state.isPaymentEditModalOpen;
    }

    @Selector()
    static limits(state: PurchaseStateModel): BazaNcLimitsDto {
        return state.limits;
    }

    @Selector()
    static limitsForPurchase(state: PurchaseStateModel): BazaNcPurchaseFlowLimitsForPurchaseResponse {
        return state.limitForPurchase;
    }

    @Selector()
    static numberOfShares(state: PurchaseStateModel): number {
        return state.numberOfShares;
    }

    @Selector()
    static dwollaPlaidResponse(state: PurchaseStateModel): BazaPlaidLinkResponseDto {
        return state.dwollaPlaidResponse;
    }

    @Selector()
    static purchaseStart(state: PurchaseStateModel): PurchaseFlowDto {
        return state.purchaseStart;
    }

    @Selector()
    static stats(state: PurchaseStateModel): StatsDto {
        return state.stats;
    }

    @Selector()
    static dwollaInvestorTouchResponse(state: PurchaseStateModel): BazaNcDwollaTouchResponse {
        return state.dwollaInvestorTouchResponse;
    }

    @Action(DwollaGetPurchaseStats, { cancelUncompleted: true })
    DwollaGetPurchaseStats(ctx: StateContext<PurchaseStateModel>, action: DwollaGetPurchaseStats): Observable<StatsDto> {
        return this.dataAccess
            .stats({
                offeringId: action.offeringId,
                requestedAmountCents: action.requestedAmountCents,
                withReservedShares: false,
                withReservedByOtherUsersShares: false,
            })
            .pipe(
                tap((response: StatsDto) => {
                    ctx.patchState({ stats: response });
                }),
                this.effectsUtil.tryCatchNone$(),
            );
    }

    @Action(DwollaCancelPurchase, { cancelUncompleted: true })
    DwollaCancelPurchase(ctx: StateContext<PurchaseStateModel>, action: DwollaCancelPurchase): Observable<PurchaseFlowDestroyResponse> {
        return this.dataAccess.destroy(action.data).pipe(
            tap(() => ctx.patchState({ purchaseStart: null })),
            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'cancel_purchase_fail')),
        );
    }

    @Action(DwollaClearPurchaseState, { cancelUncompleted: true })
    cleanState(ctx: StateContext<PurchaseStateModel>): void {
        ctx.setState(initState.defaults);
    }

    @Action(DwollaGetLimits, { cancelUncompleted: true })
    DwollaGetLimits(ctx: StateContext<PurchaseStateModel>): Observable<BazaNcLimitsDto> {
        return this.limitsDataAccess.limits().pipe(
            tap((response: BazaNcLimitsDto) => {
                ctx.patchState({ limits: response });
            }),
            this.effectsUtil.tryCatchNone$(),
        );
    }

    @Action(DwollaGetLimitsForPurchase, { cancelUncompleted: true })
    DwollaGetLimitsForPurchase(
        ctx: StateContext<PurchaseStateModel>,
        { requestedAmountCents, offeringId },
    ): Observable<BazaNcPurchaseFlowLimitsForPurchaseResponse> {
        return this.limitsDataAccess.limitsForPurchase({ requestedAmountCents, offeringId }).pipe(
            tap((response: BazaNcPurchaseFlowLimitsForPurchaseResponse) => {
                ctx.patchState({ limitForPurchase: response });
            }),
            this.effectsUtil.tryCatchNone$(),
        );
    }

    @Action(DwollaLoadAccountBalance, { cancelUncompleted: true })
    DwollaLoadAccountBalance(ctx: StateContext<PurchaseStateModel>): Observable<BazaNcDwollaCustomerDetailDto> {
        return this.dwollaDataAccess.current().pipe(
            tap((response: BazaNcDwollaCustomerDetailDto) => {
                ctx.patchState({ dwollaCustomerDetail: response, dwollaAccountBalance: response?.balance ?? null });
            }),
            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_account_balance_fail')),
        );
    }

    @Action(DwollaLoadBankAccount, { cancelUncompleted: true })
    DwollaLoadBankAccount(ctx: StateContext<PurchaseStateModel>): Observable<BazaNcBankAccountAchDto> {
        return this.bazaNcBankAccountsDataAccess.ncAchBankAccount().pipe(
            tap((response: BazaNcBankAccountAchDto) => {
                ctx.patchState({ bankAccount: response });
            }),
            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_bank_account_fail')),
        );
    }

    @Action(DwollaLoadCreditCard, { cancelUncompleted: true })
    DwollaLoadCreditCard(ctx: StateContext<PurchaseStateModel>): Observable<BazaNcPurchaseFlowGetCreditCardResponse> {
        return this.creditCardDataAccessService.getCreditCard().pipe(
            tap((response: BazaNcPurchaseFlowGetCreditCardResponse) => {
                ctx.patchState({ creditCard: response });
            }),

            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_credit_card_fail')),
        );
    }

    @Action(DwollaLoadPlaidLink, { cancelUncompleted: true })
    DwollaLoadPlaidLink(ctx: StateContext<PurchaseStateModel>): Observable<BazaPlaidLinkResponseDto> {
        return this.bazaNcBankAccountsDataAccess
            .link({
                withRedirectUrl: false,
            })
            .pipe(
                tap((response: BazaPlaidLinkResponseDto) => {
                    ctx.patchState({ dwollaPlaidResponse: response });
                }),
                this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_plaid_link_fail')),
            );
    }

    @Action(DwollaPatchStartPurchase, { cancelUncompleted: true })
    DwollaPatchStartPurchase(ctx: StateContext<PurchaseStateModel>, action: DwollaPatchStartPurchase): void {
        ctx.patchState({ numberOfShares: action.purchaseStart.numberOfShares });

        const purchaseStart = { ...ctx.getState().purchaseStart, ...action.purchaseStart };
        ctx.patchState({ purchaseStart: purchaseStart });
    }

    @Action(DwollaDeleteStartPurchase, { cancelUncompleted: true })
    DwollaDeleteStartPurchase(ctx: StateContext<PurchaseStateModel>, action: DwollaDeleteStartPurchase): void {
        ctx.patchState({ numberOfShares: action.numberOfShares });
    }

    @Action(DwollaSaveBankAccount, { cancelUncompleted: true })
    DwollaSaveBankAccount(ctx: StateContext<PurchaseStateModel>, action: DwollaSaveBankAccount): Observable<BazaNcBankAccountDto> {
        return this.bazaNcBankAccountsDataAccess
            .add(action.srcAccount)
            .pipe(this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'save_bank_account_fail')));
    }

    @Action(DwollaSaveCreditCard, { cancelUncompleted: true })
    DwollaSaveCreditCard(ctx: StateContext<PurchaseStateModel>, { srcCard }): Observable<BazaNcCreditCardDto> {
        return this.creditCardDataAccessService
            .setCreditCard(srcCard)
            .pipe(this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'save_credit_card_fail')));
    }

    @Action(DwollaSelectEntity, { cancelUncompleted: true })
    DwollaSelectEntity(ctx: StateContext<PurchaseStateModel>, action: DwollaSelectEntity): Observable<BazaNcIntegrationListingsDto> {
        return this.itemDataAccess.getById({ id: action.entityId }).pipe(
            tap((response: BazaNcIntegrationListingsDto) => {
                this.storageService.setObject('dwollaCart', action.entityId);

                return ctx.patchState({ cart: response });
            }),
        );
    }

    @Action(DwollaUpdateCart, { cancelUncompleted: true })
    DwollaUpdateCart(ctx: StateContext<PurchaseStateModel>, action: DwollaUpdateCart): void {
        this.storageService.setObject('dwollaCart', action.entity.id);
        ctx.patchState({ cart: action.entity });
    }

    @Action(DwollaStartCart, { cancelUncompleted: true })
    DwollaStartCart(ctx: StateContext<PurchaseStateModel>): void {
        const entityId = this.storageService.getObject<string>('dwollaCart') || null;

        if (this.jwtService.hasJwt() && entityId !== null) {
            ctx.dispatch(new DwollaSelectEntity(Number(entityId)));
        } else {
            ctx.patchState({ cart: null });
        }
    }

    @Action(DwollaStartPurchase, { cancelUncompleted: true })
    DwollaStartPurchase(ctx: StateContext<PurchaseStateModel>, action: DwollaStartPurchase): Observable<PurchaseFlowDto> {
        ctx.patchState({ numberOfShares: action.data?.numberOfShares });

        return this.dataAccess.session(action.data).pipe(
            tap((response: PurchaseFlowDto) => {
                ctx.patchState({ purchaseStart: response });
            }),
            this.effectsUtil.tryCatchError$(''),
        );
    }

    @Action(DwollaSubmitPurchase, { cancelUncompleted: true })
    DwollaSubmitPurchase(ctx: StateContext<PurchaseStateModel>, action: DwollaSubmitPurchase): Observable<PurchaseFlowSubmitResponse> {
        return this.dataAccess
            .submit({ id: action.tradeId })
            .pipe(this.effectsUtil.tryCatch$(this.wts.getI18nLabel(i18nBasePath, 'submit_purchase_success'), ''));
    }

    @Action(DwollaTogglePaymentEditModal, { cancelUncompleted: true })
    DwollaTogglePaymentEditModal(ctx: StateContext<PurchaseStateModel>, action: DwollaTogglePaymentEditModal): void {
        ctx.patchState({ isPaymentEditModalOpen: action.isOpen });
    }

    @Action(DwollaReprocessPayment, { cancelUncompleted: true })
    DwollaReprocessPayment(ctx: StateContext<PurchaseStateModel>, action: DwollaReprocessPayment): Observable<ReProcessPaymentResponse> {
        return this.dataAccess
            .reprocessPayment(action.data)
            .pipe(
                this.effectsUtil.tryCatch$(
                    this.wts.getI18nLabel(i18nBasePath, 'purchase_reprocess_success'),
                    this.wts.getI18nLabel(i18nBasePath, 'purchase_reprocess_fail'),
                ),
            );
    }

    @Action(DwollaInvestorAccountTouch, { cancelUncompleted: true })
    DwollaInvestorAccountTouch(ctx: StateContext<PurchaseStateModel>): Observable<BazaNcDwollaTouchResponse> {
        return this.dwollaDataAccess.touch().pipe(
            tap((response: BazaNcDwollaTouchResponse) => {
                ctx.patchState({ dwollaInvestorTouchResponse: response });
            }),
            this.effectsUtil.tryCatchNone$(),
        );
    }

    @Action(DwollaPlaidOnSuccessLink, { cancelUncompleted: false })
    DwollaPlaidOnSuccessLink(
        ctx: StateContext<PurchaseStateModel>,
        action: DwollaPlaidOnSuccessLink,
    ): Observable<BazaNcBankAccountLinkOnSuccessResponse> {
        return this.bazaNcBankAccountsDataAccess.linkOnSuccess(action.data).pipe(this.effectsUtil.tryCatchNone$());
    }

    @Action(DwollaGetDefaultBankAccount, { cancelUncompleted: false })
    DwollaGetDefaultBankAccount(
        ctx: StateContext<PurchaseStateModel>,
        action: DwollaGetDefaultBankAccount,
    ): Observable<BazaNcBankAccountGetDefaultByTypeResponse> {
        return this.bazaNcBankAccountsDataAccess.default(action.type, action.request).pipe(
            tap((response: BazaNcBankAccountGetDefaultByTypeResponse) => {
                switch (action.type) {
                    case BazaNcBankAccountType.CashIn:
                        ctx.patchState({ dwollaDefaultCashInAccount: response });
                        break;
                    case BazaNcBankAccountType.CashOut:
                        ctx.patchState({ dwollaDefaultCashOutAccount: response });
                        break;
                }
            }),
            this.effectsUtil.tryCatchNone$(),
        );
    }

    @Action(DwollaTransferFunds, { cancelUncompleted: true })
    DwollaTransferFunds(ctx: StateContext<PurchaseStateModel>, action: DwollaTransferFunds): Observable<BazaDwollaPaymentDto> {
        return this.bazaNcTransferDataAccess.transfer(action.transferRequest).pipe(this.effectsUtil.tryCatchNone$());
    }

    @Action(WithdrawFunds, { cancelUncompleted: true })
    WithdrawFunds(ctx: StateContext<PurchaseStateModel>, action: WithdrawFunds): Observable<BazaDwollaPaymentDto> {
        return this.bazaNcWithdrawDataAccess.withdraw(action.withdrawRequest).pipe(this.effectsUtil.tryCatchNone$());
    }
}
