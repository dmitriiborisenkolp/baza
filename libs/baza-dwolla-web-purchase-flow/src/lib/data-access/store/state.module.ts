import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { BazaNcDataAccessModule } from '@scaliolabs/baza-nc-data-access';
import { DwollaPurchaseState } from './state';
import { BazaNcIntegrationListingsDataAccessModule } from '@scaliolabs/baza-nc-integration-data-access';

@NgModule({
    imports: [NgxsModule.forFeature([DwollaPurchaseState]), BazaNcDataAccessModule, BazaNcIntegrationListingsDataAccessModule],
})
export class DwollaPurchaseStateModule {}
