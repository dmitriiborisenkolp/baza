import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Store } from '@ngxs/store';
import { EMPTY, Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import { DwollaPurchaseState } from '../store';

@Injectable({ providedIn: 'root' })
export class DwollaSharepickerResolver implements Resolve<number | void> {
    constructor(private readonly store: Store) {}

    resolve(): Observable<number | void> {
        return this.store.select(DwollaPurchaseState.numberOfShares).pipe(
            take(1),
            tap((numberOfShares) => {
                return numberOfShares ?? EMPTY;
            }),
        );
    }
}
