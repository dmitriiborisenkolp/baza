import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { StorageService } from '@scaliolabs/baza-web-utils';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { DwollaPurchaseState, DwollaSelectEntity } from '../store';

@Injectable({ providedIn: 'root' })
export class DwollaCartResolver implements Resolve<BazaNcIntegrationListingsDto> {
    constructor(private readonly store: Store, private readonly storageService: StorageService) {}

    resolve(): Observable<BazaNcIntegrationListingsDto> {
        const result$ = this.store.select(DwollaPurchaseState.cart).pipe(
            take(1),
            switchMap((response) => {
                const entityId = this.storageService.getObject<number>('dwollaCart');
                if (!response && entityId) {
                    return this.store.dispatch(new DwollaSelectEntity(entityId)).pipe(
                        switchMap(() => {
                            return of(this.store.selectSnapshot(DwollaPurchaseState.cart));
                        }),
                    );
                } else {
                    return of(response);
                }
            }),
        );

        return result$.pipe(
            map((response) => {
                return response;
            }),
        );
    }
}
