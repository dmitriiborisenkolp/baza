export const DwollaMethodsEnI18n = {
    title: 'Payment Method',
    errors: {
        accountBalance: {
            notification: `There was a failed attempt to create an Account Balance. Please change your personal information in account, <a data-link="verificationLink"></a> or <a data-link="supportLink"></a> for technical support. Alternatively, you can proceed with purchases with a Card.`,
            verificationLinkConfig: {
                key: 'verificationLink',
                text: 'investors profile',
            },
            supportLinkConfig: {
                key: 'supportLink',
                text: 'contact us',
            },
        },
        insufficientFunds:
            'There are insufficient funds in your account to complete your purchase. Please choose another payment method below.',
        lowFunds: `This purchase is <strong>{{ amount }}</strong> higher than your current Account Balance. Please add funds to your balance.`,
    },
    actions: {
        editMethod: 'Edit your payment method',
        submit: 'Submit Payment',
        back: 'Back',
    },
};
