import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcBankAccountAchDto,
    BazaNcBankAccountGetDefaultByTypeResponse,
    BazaNcBankAccountType,
    BazaNcBootstrapDto,
    BazaNcDwollaCustomerDetailDto,
    BazaNcPurchaseFlowGetCreditCardResponse,
    BazaNcPurchaseFlowTransactionType,
    PurchaseFlowDto,
    PurchaseFlowSessionDto,
} from '@scaliolabs/baza-nc-shared';
import { AccountBalanceCardConfig } from '@scaliolabs/baza-web-ui-components';
import { BazaWebUtilSharedService, I18nLinkifyConfig, Message, PriceCentsPipe } from '@scaliolabs/baza-web-utils';
import {
    BehaviorSubject,
    Observable,
    Subject,
    combineLatest,
    concat,
    debounceTime,
    distinctUntilChanged,
    finalize,
    map,
    skipWhile,
    tap,
} from 'rxjs';
import {
    DwollaGetDefaultBankAccount,
    DwollaLoadAccountBalance,
    DwollaLoadBankAccount,
    DwollaLoadCreditCard,
    DwollaPurchaseState,
    DwollaSharedService,
    DwollaStartPurchase,
    DwollaTogglePaymentEditModal,
    PaymentMethodConfig,
    PaymentMethodType,
} from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-purchase-methods-dwolla',
    templateUrl: './methods.component.html',
    styleUrls: ['./methods.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaPurchaseMethodsComponent implements OnInit {
    @Input()
    set initData(value: BazaNcBootstrapDto) {
        this.initData$.next(value);
    }
    private initData$: BehaviorSubject<BazaNcBootstrapDto> = new BehaviorSubject<BazaNcBootstrapDto>(null);

    @Input()
    isPurchaseAboveLimit: boolean;

    @Input()
    config?: PaymentMethodConfig;

    @Output()
    updateSubmitBtnDisableStatus = new EventEmitter<boolean>();

    @Output()
    public readonly paymentMethodAdded = new EventEmitter<boolean>();

    // store selectors
    cart$ = this.store.select(DwollaPurchaseState.cart);
    purchaseStart$ = this.store.select(DwollaPurchaseState.purchaseStart);
    numberOfShares$ = this.store.select(DwollaPurchaseState.numberOfShares);
    bankAccount$ = this.store.select(DwollaPurchaseState.bankAccount);
    creditCard$ = this.store.select(DwollaPurchaseState.creditCard);
    dwollaCustomerDetail$ = this.store.select(DwollaPurchaseState.dwollaCustomerDetail);
    limits$ = this.store.select(DwollaPurchaseState.limits);
    dwollaDefaultCashInAccount$ = this.store.select(DwollaPurchaseState.dwollaDefaultCashInAccount);
    transferAmountErrorMessage$: Observable<Message | undefined>;

    bootstrapData: BazaNcBootstrapDto;
    dwollaCustomerDetail: BazaNcDwollaCustomerDetailDto;
    creditCardResponse: BazaNcPurchaseFlowGetCreditCardResponse;
    bankAccountResponse: BazaNcBankAccountAchDto;

    purchaseStart: PurchaseFlowDto;
    selectedPaymentMethod: PaymentMethodType = null;
    dwollaDefaultCashInAccount: BazaNcBankAccountGetDefaultByTypeResponse;
    entity: BazaNcIntegrationListingsDto;
    numberOfShares: number;

    cardDetailsForm: FormGroup;
    onSubmitCardForm: Subject<FormGroup> = new Subject();
    isCardDetailsFormVisible: boolean;

    bankDetailsForm: FormGroup;
    onSubmitManualBankDetailsForm: Subject<FormGroup> = new Subject();
    isBankDetailsFormVisible: boolean;

    showDwollaAccountCreationError = false;
    purchaseSessionStarted = false;

    isForeignInvestor = false;
    isDwollaAvailable = false;
    dwollaDetailsLoaded = false;
    isCardAvailable = false;
    isNCBankAccountAvailable = false;
    dwollaBalanceAmount = 0;
    isPaymentEditModalVisible = false;
    dataLoaded = false;

    public i18nBasePath = 'dwpf.methods';

    constructor(
        private readonly store: Store,
        private readonly priceCents: PriceCentsPipe,
        private readonly sanitizer: DomSanitizer,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
        public readonly cdr: ChangeDetectorRef,
        public readonly dss: DwollaSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {
        concat(
            this.store.dispatch(new DwollaLoadCreditCard()),
            this.store.dispatch(new DwollaLoadBankAccount()),
            this.store.dispatch(new DwollaGetDefaultBankAccount(BazaNcBankAccountType.CashIn, { throwError: false })),
            this.store.dispatch(new DwollaLoadAccountBalance()),
        );
    }

    ngOnInit(): void {
        this.resetBankDetailsForm();
        this.resetCardDetailsForm();
        this.resetVariables();
        this.dwollaDetailsLoaded = false;

        this.onSubmitManualBankDetailsForm
            .pipe(
                tap((value) => {
                    this.processSubmitManualBankDetailsForm(value);
                }),
                untilDestroyed(this),
            )
            .subscribe();

        this.onSubmitCardForm
            .pipe(
                tap((value) => this.processSubmitCardForm(value)),
                untilDestroyed(this),
            )
            .subscribe();

        this.initializeDataStream();
    }

    private initializeDataStream() {
        combineLatest([
            this.initData$,
            this.dwollaDefaultCashInAccount$,
            this.bankAccount$,
            this.creditCard$,
            this.purchaseStart$,
            this.cart$,
            this.numberOfShares$,
            this.dwollaCustomerDetail$,
        ])
            .pipe(
                untilDestroyed(this),
                map(([initData, defaultCashInAccount, bank, card, purchaseStart, cart, numberOfShares, dwollaCustomerDetail]) => ({
                    initData,
                    defaultCashInAccount,
                    bank,
                    card,
                    purchaseStart,
                    cart,
                    numberOfShares,
                    dwollaCustomerDetail,
                })),
                skipWhile(
                    (pair) =>
                        !pair?.initData ||
                        !pair?.bank ||
                        !pair?.card ||
                        !pair?.defaultCashInAccount ||
                        !pair?.purchaseStart ||
                        !pair?.cart ||
                        !pair?.numberOfShares ||
                        !pair?.dwollaCustomerDetail,
                ),
                distinctUntilChanged(),
                debounceTime(0),
            )
            .subscribe((pair) => {
                this.bootstrapData = pair?.initData;
                this.dwollaDefaultCashInAccount = pair?.defaultCashInAccount;
                this.purchaseStart = pair?.purchaseStart;
                this.creditCardResponse = pair?.card;
                this.bankAccountResponse = pair?.bank;
                this.entity = pair?.cart;
                this.numberOfShares = pair?.numberOfShares || 1;
                this.dwollaCustomerDetail = pair?.dwollaCustomerDetail;

                this.onDataStreamCompletion();
            });
    }

    private onDataStreamCompletion() {
        this.resetVariables();

        if (!this.selectedPaymentMethod) {
            this.setDefaultPaymentMethod();
        }

        if (!this.purchaseSessionStarted) {
            this.startPurchaseSession();
        }

        this.paymentMethodAdded.emit(this.isCardAvailable || this.isNCBankAccountAvailable || this.isDwollaAvailable);
    }

    private updateTransactionType(transactionType: BazaNcPurchaseFlowTransactionType) {
        const purchase: PurchaseFlowSessionDto = {
            amount: this.entity?.pricePerShareCents * this.numberOfShares,
            numberOfShares: this.numberOfShares,
            offeringId: this.entity?.offeringId,
            transactionType: transactionType,
        };

        this.updateSubmitBtnDisableStatus.emit(this.isSubmitBtnDisabled);
        this.store.dispatch(new DwollaStartPurchase(purchase));
    }

    private resetBankDetailsForm(): void {
        this.bankDetailsForm = this.dss.generateBankDetailsForm();
    }

    private resetCardDetailsForm(): void {
        this.cardDetailsForm = this.dss.generateCreditCardForm();
    }

    private resetVariables() {
        this.isForeignInvestor = this.dss.isForeignInvestor(this.bootstrapData);
        this.isDwollaAvailable = this.dss.isDwollaAvailable(this.dwollaCustomerDetail);
        this.isCardAvailable = this.dss.isCardAvailable(this.creditCardResponse);
        this.isNCBankAccountAvailable = this.dss.isNCBankAccountAvailable(this.bankAccountResponse);
        this.dwollaBalanceAmount = this.dss.getDwollaBalanceAmount(this.dwollaCustomerDetail);
        this.showDwollaAccountCreationError = false;
        this.updateSubmitBtnDisableStatus.emit(this.isSubmitBtnDisabled);
    }

    // add debit or credit card
    public handleAddCard() {
        this.isCardDetailsFormVisible = true;
    }

    // submit debit or credit card form
    public submitCardForm() {
        this.onSubmitCardForm.next(this.cardDetailsForm);
    }

    // cancel debit or credit card form
    handleCardDetailsCancel() {
        this.isCardDetailsFormVisible = false;
    }

    // on open payment edit modals
    public onOpenPaymentEditModals() {
        this.store.dispatch(new DwollaTogglePaymentEditModal(true));
        this.isPaymentEditModalVisible = true;
    }

    // on close payment edit bank modal
    private onClosePaymentEditBankModal() {
        this.resetBankDetailsForm();
        this.handleBankDetailsCancel();
        this.isPaymentEditModalVisible = false;
        return this.store.dispatch(new DwollaTogglePaymentEditModal(false));
    }

    // on close payment edit card modal
    private onClosePaymentEditCardModal() {
        this.resetCardDetailsForm();
        this.handleCardDetailsCancel();
        this.isPaymentEditModalVisible = false;
        return this.store.dispatch(new DwollaTogglePaymentEditModal(false));
    }

    public onSelectPaymentMethod(paymentMethod: PaymentMethodType) {
        this.selectedPaymentMethod = paymentMethod;
        const transactionType = this.mapPaymentMethodToTransactionType(paymentMethod);
        return this.updateTransactionType(transactionType);
    }

    // on submit account details from modal from modal
    public onSubmitAccountDetailsFromModal(accountDetailsType: PaymentMethodType) {
        if (accountDetailsType === PaymentMethodType.bankAccount) {
            this.submitManualBankDetailsForm();
        } else {
            this.submitCardForm();
        }
    }

    public onSelectPaymentMethodAndClose(paymentMethod: PaymentMethodType) {
        this.onSelectPaymentMethod(paymentMethod);
        this.store.dispatch(new DwollaTogglePaymentEditModal(false));
    }

    processSubmitManualBankDetailsForm = (form) =>
        this.dss.processSubmitManualBankDetailsForm(form, () => {
            this.wts.refreshInitData$.next();
            this.store.dispatch(new DwollaLoadBankAccount());
            this.onSelectPaymentMethod(PaymentMethodType.bankAccount);

            this.onClosePaymentEditBankModal();
        });

    processSubmitCardForm = (form) =>
        this.dss.processSubmitCardForm(form, () => {
            this.wts.refreshInitData$.next();
            this.store.dispatch(new DwollaLoadCreditCard());
            this.onSelectPaymentMethod(PaymentMethodType.creditCard);

            this.onClosePaymentEditCardModal();
        });

    public onManualLink() {
        this.isBankDetailsFormVisible = true;
    }

    public submitManualBankDetailsForm() {
        this.onSubmitManualBankDetailsForm.next(this.bankDetailsForm);
    }

    public handleBankDetailsCancel() {
        this.isBankDetailsFormVisible = false;
    }

    public get isPaymentMethodEditVisible() {
        let isVisible = true;

        if (this.isForeignInvestor) {
            isVisible = this.isNCBankAccountAvailable || (this.isCardAvailable && !this.isPurchaseAboveLimit);
        } else {
            isVisible =
                this.isNCBankAccountAvailable ||
                (this.isDwollaAvailable && this.hasEnoughFunds) ||
                (this.isCardAvailable && !this.isPurchaseAboveLimit);
        }

        return isVisible;
    }

    public get purchasePopupTooltipText() {
        return `This is your Payment Method. You can add a new Bank Account or Credit Card or switch between your linked payment methods.`;
    }

    public get accountBalanceAddMode() {
        return !this.isDwollaAvailable && !this.isNCBankAccountAvailable && (!this.isCardAvailable || this.isPurchaseAboveLimit);
    }

    public get accountBalanceEditMode() {
        return (
            (this.isDwollaAvailable && this.isAccBalancePaymentMethod) ||
            (this.isDwollaAvailable &&
                !this.hasEnoughFunds &&
                ((!this.isNCBankAccountAvailable && !this.isCardAvailable) ||
                    (!this.isNCBankAccountAvailable && this.isCardAvailable && this.isPurchaseAboveLimit)))
        );
    }

    public get manualBankAccountAddMode() {
        return (
            !this.isNCBankAccountAvailable &&
            (((!this.isCardAvailable || this.isPurchaseAboveLimit) && this.isDwollaAvailable && !this.hasEnoughFunds) ||
                ((!this.isCardAvailable || this.isPurchaseAboveLimit) && !this.isDwollaAvailable))
        );
    }

    public get manualBankAccountEditMode() {
        return this.isNCBankAccountAvailable && this.isNCBankAccPaymentMethod;
    }

    public get cardAddMode() {
        if (this.isForeignInvestor) {
            return !this.isNCBankAccountAvailable && !this.isCardAvailable;
        } else {
            return !this.isNCBankAccountAvailable && (!this.isDwollaAvailable || !this.hasEnoughFunds) && !this.isCardAvailable;
        }
    }

    public get cardEditMode() {
        const cardEditMode = this.isCardAvailable && this.isCCPaymentMethod;
        return cardEditMode;
    }

    startPurchaseSession() {
        const purchase: PurchaseFlowSessionDto = {
            amount: this.entity?.pricePerShareCents * this.numberOfShares,
            numberOfShares: this.numberOfShares,
            offeringId: this.entity?.offeringId,
            transactionType: this.mapPaymentMethodToTransactionType(this.selectedPaymentMethod),
        };

        this.checkSessionDefaultTransactionType(purchase);
        this.updateSubmitBtnDisableStatus.emit(this.isSubmitBtnDisabled);
        this.purchaseSessionStarted = true;

        this.store
            .dispatch(new DwollaStartPurchase(purchase))
            .pipe(
                finalize(() => {
                    this.dataLoaded = true;
                    this.cdr.detectChanges();
                }),
            )
            .subscribe();
    }

    private setDefaultPaymentMethod() {
        if (this.isForeignInvestor) {
            this.selectedPaymentMethod = this.isNCBankAccountAvailable
                ? PaymentMethodType.bankAccount
                : this.isCardAvailable
                ? PaymentMethodType.creditCard
                : null;
        } else {
            this.selectedPaymentMethod =
                this.isDwollaAvailable && this.hasEnoughFunds
                    ? PaymentMethodType.accountBalance
                    : this.isNCBankAccountAvailable
                    ? PaymentMethodType.bankAccount
                    : this.isCardAvailable
                    ? PaymentMethodType.creditCard
                    : null;
        }
    }

    private checkSessionDefaultTransactionType(purchase: PurchaseFlowSessionDto) {
        // if CC is selected & purchase is above limit, select ACH/TBD as default transaction type for 1st time session start on payment page (BAZA-1686)
        if (this.dss.isCCSelectedAsPaymentMethod(this.selectedPaymentMethod) && this.isPurchaseAboveLimit) {
            const paymentMethod = this.isForeignInvestor ? PaymentMethodType.bankAccount : PaymentMethodType.accountBalance;
            purchase.transactionType = this.mapPaymentMethodToTransactionType(paymentMethod);
        }
    }

    private mapPaymentMethodToTransactionType(paymentMethod: PaymentMethodType) {
        const mappedType =
            paymentMethod === PaymentMethodType.creditCard
                ? BazaNcPurchaseFlowTransactionType.CreditCard
                : paymentMethod === PaymentMethodType.accountBalance
                ? BazaNcPurchaseFlowTransactionType.AccountBalance
                : BazaNcPurchaseFlowTransactionType.ACH;

        return mappedType;
    }

    public get isCCPaymentMethod(): boolean {
        return this.selectedPaymentMethod === PaymentMethodType.creditCard;
    }

    public get isAccBalancePaymentMethod(): boolean {
        return this.selectedPaymentMethod === PaymentMethodType.accountBalance;
    }

    public get isNCBankAccPaymentMethod(): boolean {
        return this.selectedPaymentMethod === PaymentMethodType.bankAccount;
    }

    public get isSubmitBtnDisabled() {
        let isDisabled = true;

        if (this.isForeignInvestor) {
            isDisabled =
                (!this.isNCBankAccountAvailable && !this.isCardAvailable) ||
                (!this.isNCBankAccountAvailable && this.isCardAvailable && this.isPurchaseAboveLimit);
        } else {
            isDisabled =
                (!this.isNCBankAccountAvailable &&
                    !this.isCardAvailable &&
                    (!this.isDwollaAvailable || (this.isDwollaAvailable && !this.hasEnoughFunds))) ||
                (!this.isNCBankAccountAvailable &&
                    this.isCardAvailable &&
                    this.isPurchaseAboveLimit &&
                    (!this.isDwollaAvailable || (this.isDwollaAvailable && !this.hasEnoughFunds)));
        }
        return isDisabled;
    }

    public get hasEnoughFunds() {
        return this.dss.getDwollaBalanceAmount(this.dwollaCustomerDetail) >= this.getTotalPurchaseAmount;
    }

    public get getTotalPurchaseAmount() {
        return (this.purchaseStart?.total ?? 0) / 100;
    }

    public get showDwollaLowFundsError() {
        return this.isDwollaAvailable && !this.hasEnoughFunds;
    }

    public get lowFundsDifferenceAmount() {
        return this.getTotalPurchaseAmount - this.dwollaBalanceAmount;
    }

    public get isAccountBalanceDisabled() {
        return this.showDwollaAccountCreationError || !this.hasEnoughFunds || !this.dss.isDwollaAvailable(this.dwollaCustomerDetail);
    }

    get accountBalanceLinksConfig(): Array<I18nLinkifyConfig> {
        return this.wts.getI18nLinksConfig([
            {
                path: `${this.i18nBasePath}.errors.accountBalance.verificationLinkConfig`,
                config: {
                    appLink: {
                        commands: [this.dss.pfLinksConfig?.verification],
                    },
                },
            },
            {
                path: `${this.i18nBasePath}.errors.accountBalance.supportLinkConfig`,
                config: {
                    extLink: {
                        link: `mailto:${this.dss.techSupportEmail}`,
                    },
                },
            },
        ]);
    }

    get dwollaAvailableError(): SafeHtml {
        const output = this.wts.getI18nLabel(this.i18nBasePath, 'errors.accountBalance.notification');

        return this.sanitizer.bypassSecurityTrustHtml(output);
    }

    get dwollaLowFundsError(): SafeHtml {
        const output = this.wts.getI18nLabel(this.i18nBasePath, 'errors.lowFunds', {
            amount: this.priceCents.transform(this.lowFundsDifferenceAmount),
        });

        return this.sanitizer.bypassSecurityTrustHtml(output);
    }

    get accountBalanceConfig(): AccountBalanceCardConfig {
        return {
            srcData: this.dwollaCustomerDetail?.balance,
            isDisabled: this.isAccountBalanceDisabled,
            isAddModeOn: this.accountBalanceAddMode,
            isEditModeOn: this.accountBalanceEditMode,
            hasEnoughFunds: this.hasEnoughFunds,
            showAddFundsButton: false,
        };
    }
}
