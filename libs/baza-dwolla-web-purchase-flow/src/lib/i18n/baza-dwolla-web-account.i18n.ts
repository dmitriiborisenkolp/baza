import { DwollaPaymentBalanceEnI18n } from '../ui-components/account/payment-methods/account-balance/i18n/dwac-payment-balance.i18n';
import { DwollaPaymentBankEnI18n } from '../ui-components/account/payment-methods/bank-account/i18n/dwac-payment-bank.i18n';
import { DwollaPaymentCardEnI18n } from '../ui-components/account/payment-methods/credit-card/i18n/dwac-payment-card.i18n';
import { DwollaPaymentEnI18n } from '../ui-components/account/payment-methods/i18n/dwac-payment.i18n';
import { DwollaPayoutEnI18n } from '../ui-components/account/payout-methods/18n/dwac-payout.i18n';
import { DwollaPayoutBankEnI18n } from '../ui-components/account/payout-methods/bank-account/18n/dwac-payout-bank.i18n';

export const BazaDwollaWebAccountEnI18n = {
    dwacc: {
        payment: {
            ...DwollaPaymentEnI18n,
            balance: DwollaPaymentBalanceEnI18n,
            bank: DwollaPaymentBankEnI18n,
            card: DwollaPaymentCardEnI18n,
        },
        payout: {
            ...DwollaPayoutEnI18n,
            bank: DwollaPayoutBankEnI18n,
        },
    },
};
