import { DwollaAgreementEnI18n } from '../agreement/i18n/agreement.i18n';
import { DwollaDetailsEnI18n } from '../details/i18n/details.i18n';
import { DwollaDoneEnI18n } from '../done/i18n/done.i18n';
import { DwollaMethodsEnI18n } from '../methods/i18n/methods.i18n';
import { DwollaPaymentEnI18n } from '../payment/i18n/payment.i18n';
import { DwollaPaymentEditEnI18n } from '../ui-components/pf/i18n';

export const BazaDwollaWebPFEnI18n = {
    dwpf: {
        parent: {
            backlink: {
                text: 'Back to Listing Details',
                confirmation: {
                    title: 'Are you sure you want to exit?',
                    text: 'Your purchase will not be completed if you leave this page.',
                },
            },
            steps: {
                detailsLabel: 'Purchase Details',
                agreementLabel: 'Sign Agreement',
                paymentLabel: 'Submit Payment',
            },
            warnings: {
                pickShares: 'Please pick number of shares',
            },
        },
        agreement: DwollaAgreementEnI18n,
        details: DwollaDetailsEnI18n,
        done: DwollaDoneEnI18n,
        methods: DwollaMethodsEnI18n,
        payment: DwollaPaymentEnI18n,
        pymtEdit: DwollaPaymentEditEnI18n,
        notifications: {
            cancel_purchase_fail: 'There was an error cancelling the Purchase.',
            load_bank_account_fail: 'Failed to load Bank Account information',
            load_account_balance_fail: 'Failed to load Account Balance information',
            load_credit_card_fail: 'Failed to load Credit Card information',
            load_plaid_link_fail: 'Failed to load Plaid link',
            save_bank_account_fail: 'Failed to save Bank Account information',
            save_credit_card_fail: 'Failed to save Credit Card information',
            submit_purchase_fail: 'There was an error submitting the Purchase.',
            submit_purchase_success: 'Purchase successfully submitted',
            purchase_start_fail: 'Could not start the session',
            purchase_reprocess_success: 'Payment successfully re-submitted',
            purchase_reprocess_fail: 'Payment retry request failed. Please contact your bank for further details',
            plaid_on_link_success: `The bank account you're using to add funds to your account balance will be utilized to receive dividends. You can change it later on Account page.`,
            plaid_on_link_fail: 'There was an error linking bank account',
        },
    },
};
