export const DwollaDoneEnI18n = {
    title: 'Your order has been submitted!',
    descr: 'We are verifying the order and will notify you when it has been completed. This typically takes 24-48 hours.',
    support: {
        descr: 'If you have any questions please reach out via',
        linkText: 'Contact Us',
    },
    actions: {
        proceedToPortfolio: 'Proceed to my portfolio',
    },
};
