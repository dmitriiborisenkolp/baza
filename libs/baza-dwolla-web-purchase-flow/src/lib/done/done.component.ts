import { AfterViewInit, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { PurchaseFlowDto } from '@scaliolabs/baza-nc-shared';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { DwollaDeleteStartPurchase, DwollaPatchStartPurchase, DwollaPurchaseState, PurchaseDoneConfig } from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-purchase-done-dwolla',
    templateUrl: './done.component.html',
    styleUrls: ['./done.component.less'],
})
export class DwollaPurchaseDoneComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input()
    config?: PurchaseDoneConfig;

    cart$ = this.store.select(DwollaPurchaseState.cart);
    numberOfShares$ = this.store.select(DwollaPurchaseState.numberOfShares);
    purchaseStart$ = this.store.select(DwollaPurchaseState.purchaseStart);

    public i18nBasePath = 'dwpf.done';

    constructor(
        private readonly store: Store,
        public readonly uts: BazaLinkUtilSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    ngOnInit() {
        this.checkDefaultConfig();
    }

    ngAfterViewInit() {
        this.store.dispatch(new DwollaDeleteStartPurchase(null));
    }

    ngOnDestroy() {
        this.store.dispatch(new DwollaPatchStartPurchase({ fee: null } as PurchaseFlowDto));
    }

    checkDefaultConfig() {
        const defaultConfig: PurchaseDoneConfig = {
            ctaBtnLink: {
                appLink: {
                    commands: ['/portfolio'],
                },
            },
            cta: {
                appLink: {
                    commands: ['', { outlets: { modal: ['contact-us'] } }],
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
