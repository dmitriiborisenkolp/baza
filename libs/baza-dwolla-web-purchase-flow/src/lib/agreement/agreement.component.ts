import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { PurchaseFlowDto, PurchaseFlowSessionDto } from '@scaliolabs/baza-nc-shared';
import { IframeIntegrationComponent } from '@scaliolabs/baza-web-ui-components';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { NzModalService } from 'ng-zorro-antd/modal';
import { combineLatest, distinctUntilChanged, map, skipWhile } from 'rxjs';
import { DocuSignEvents, DwollaPatchStartPurchase, DwollaPurchaseState, DwollaSharedService, DwollaStartPurchase } from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-purchase-agreement-dwolla',
    templateUrl: './agreement.component.html',
    styleUrls: ['./agreement.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaPurchaseAgreementComponent {
    @Output()
    DwollaCancelPurchase: EventEmitter<void> = new EventEmitter();

    purchaseStart$ = this.store.select(DwollaPurchaseState.purchaseStart);
    cart$ = this.store.select(DwollaPurchaseState.cart);
    numberOfShares$ = this.store.select(DwollaPurchaseState.numberOfShares);

    public purchaseStart: PurchaseFlowDto;
    public entity: BazaNcIntegrationListingsDto;
    showAgreementPopup = false;
    currentDocusignLastEvent: DocuSignEvents;

    public i18nBasePath = 'dwpf.agreement';

    constructor(
        private readonly modalService: NzModalService,
        private readonly router: Router,
        private readonly store: Store,
        private readonly dss: DwollaSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {
        combineLatest([this.purchaseStart$, this.cart$])
            .pipe(
                untilDestroyed(this),
                map(([purchaseStart, cart]) => ({
                    purchaseStart,
                    cart,
                })),
                skipWhile((pair) => !pair?.purchaseStart || !pair?.cart),
                distinctUntilChanged(),
            )
            .subscribe((pair) => {
                this.entity = pair.cart;
                this.purchaseStart = pair.purchaseStart;

                if (this.showAgreementPopup) {
                    this.showAgreementPopup = false;
                    this.loadAgreementModal();
                }
            });
    }

    public openAgreementModal() {
        this.showAgreementPopup = true;
        this.refreshSession(true);
    }

    private loadAgreementModal() {
        const modalRef = this.modalService.create({
            nzClassName: 'ant-modal-transparent',
            nzClosable: false,
            nzContent: IframeIntegrationComponent,
            nzComponentParams: {
                events: [DocuSignEvents.SigningComplete, DocuSignEvents.Decline, DocuSignEvents.Cancel],
                height: '80vh',
                maxWidth: '1030',
                maxHeight: '790',
                width: '100%',
                url: this.purchaseStart.docuSignUrl,
            } as Partial<IframeIntegrationComponent<DocuSignEvents>>,
            nzFooter: null,
            nzWidth: 1030,
        });

        modalRef.componentInstance.iframeEvent.pipe(untilDestroyed(this)).subscribe((result) => {
            this.currentDocusignLastEvent = result;
            modalRef.close();

            if (this.currentDocusignLastEvent === DocuSignEvents.SigningComplete) {
                this.router.navigate([this.dss.pfLinksConfig?.buyShares, 'payment']);
            }
        });
    }

    public onFormSubmit(): void {
        this.router.navigate([this.dss.pfLinksConfig?.buyShares, 'payment']);
    }

    public onBackClick(): void {
        if (this.purchaseStart) {
            this.purchaseStart.fee = null;
            this.store.dispatch(new DwollaPatchStartPurchase(this.purchaseStart));
        }
        this.router.navigate([this.dss.pfLinksConfig?.buyShares, 'details']);
    }

    private refreshSession(requestDocuSignUrl = false) {
        const purchase: PurchaseFlowSessionDto = {
            offeringId: this.entity.offeringId,
            numberOfShares: this.purchaseStart.numberOfShares,
            amount: this.purchaseStart.amount,
            requestDocuSignUrl: requestDocuSignUrl,
        };

        this.store.dispatch(new DwollaStartPurchase(purchase));
    }
}
