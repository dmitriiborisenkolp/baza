import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { BankDetailsModule, CardDetailsModule, PaymentHeaderModule, PaymentRadioModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { PaymentEditAddComponent } from './add/add.component';
import { PaymentEditComponent } from './payment-edit.component';
import { PaymentEditListComponent } from './list/list.component';
import { BazaNgCoreModule } from '@scaliolabs/baza-core-ng';

@NgModule({
    declarations: [PaymentEditComponent, PaymentEditAddComponent, PaymentEditListComponent],
    exports: [PaymentEditComponent, PaymentEditAddComponent, PaymentEditListComponent],
    imports: [
        BankDetailsModule,
        CardDetailsModule,
        CommonModule,
        NzButtonModule,
        NzModalModule,
        PaymentHeaderModule,
        PaymentRadioModule,
        RouterModule,
        UtilModule,
        BazaNgCoreModule,
    ],
})
export class PaymentEditModule {}
