import { DwollaPaymentEditAddEnI18n } from '../add/i18n/add.i18n';
import { DwollaPaymentEditListEnI18n } from '../list/i18n/list.i18n';

export const DwollaPaymentEditEnI18n = {
    add: DwollaPaymentEditAddEnI18n,
    list: DwollaPaymentEditListEnI18n,
};
