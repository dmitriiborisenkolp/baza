export const DwollaPaymentEditAddEnI18n = {
    bank: {
        title: 'Bank Details',
        actions: {
            add: 'Add Bank Account',
        },
    },
    card: {
        title: 'Card Details',
        actions: {
            add: 'Add Card',
        },
    },
};
