import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngxs/store';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
    BazaNcBankAccountAchDto,
    BazaNcDwollaCustomerDetailDto,
    BazaNcPurchaseFlowGetCreditCardResponse,
} from '@scaliolabs/baza-nc-shared';

import {
    PaymentEditModalsContentType,
    PaymentMethodType,
    PaymentUpdateAccountType,
    DwollaPurchaseState,
    DwollaTogglePaymentEditModal,
    PaymentMethodConfig,
} from '../../data-access';

@UntilDestroy()
@Component({
    selector: 'app-payment-edit',
    templateUrl: './payment-edit.component.html',
    styleUrls: ['./payment-edit.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentEditComponent implements OnInit {
    isPaymentEditModalOpen$ = this.store.select(DwollaPurchaseState.isPaymentEditModalOpen);

    @Input()
    config?: PaymentMethodConfig;

    @Input()
    selectedPaymentMethod: PaymentMethodType;

    @Input()
    creditCardResponse: BazaNcPurchaseFlowGetCreditCardResponse;

    @Input()
    bankAccountResponse: BazaNcBankAccountAchDto;

    @Input()
    dwollaCustomerDetail: BazaNcDwollaCustomerDetailDto;

    @Input()
    isPurchaseAboveLimit: boolean;

    @Input()
    cardDetailsForm: FormGroup;

    @Input()
    bankDetailsForm: FormGroup;

    @Input()
    isForeignInvestor = true;

    @Input()
    hasEnoughFunds: boolean;

    @Output()
    public selectPaymentMethod: EventEmitter<PaymentMethodType> = new EventEmitter();

    @Output()
    public submitAccountDetailsForm: EventEmitter<PaymentMethodType> = new EventEmitter();

    public modalContentType: PaymentEditModalsContentType = PaymentEditModalsContentType.paymentList;
    public modalTitle: string;
    public paymentMethodType: PaymentMethodType;
    public paymentUpdateAccountType: PaymentUpdateAccountType;

    constructor(private store: Store) {
        this.isPaymentEditModalOpen$.pipe(untilDestroyed(this)).subscribe((isPaymentEditModalOpen) => {
            if (!isPaymentEditModalOpen) {
                this.onCloseModal();
            }
        });
    }

    ngOnInit() {
        this.setInitModalData();
        this.modalTitle = 'Modal Window';
    }

    private setInitModalData() {
        this.modalContentType = PaymentEditModalsContentType.paymentList;
    }

    public onCloseModal() {
        this.setInitModalData();
        this.store.dispatch(new DwollaTogglePaymentEditModal(false));
    }

    public onBackClick() {
        if (this.modalContentType === PaymentEditModalsContentType.paymentAdd) {
            this.modalContentType = PaymentEditModalsContentType.paymentList;
        }
    }

    public onModalTitleChange(title: string) {
        this.modalTitle = title;
    }

    public onSubmitAccountDetailsForm(accountFormType: PaymentMethodType) {
        this.submitAccountDetailsForm.emit(accountFormType);
    }

    public onPaymentMethodChange(paymentMethod: PaymentMethodType) {
        this.selectPaymentMethod.emit(paymentMethod);
    }

    public onAddUpdatePaymentMethodClick(paymentMethodType: PaymentMethodType) {
        this.paymentMethodType = paymentMethodType;
        this.modalContentType = PaymentEditModalsContentType.paymentAdd;
    }
}
