import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import {
    BazaNcBankAccountAchDto,
    BazaNcDwollaCustomerDetailDto,
    BazaNcPurchaseFlowGetCreditCardResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { PaymentMethodType, DwollaPurchaseState, DwollaSharedService, PaymentMethodConfig } from '../../../data-access';

@Component({
    selector: 'app-payment-edit-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentEditListComponent implements OnInit {
    limits$ = this.store.select(DwollaPurchaseState.limits);

    @Input()
    config?: PaymentMethodConfig;

    @Input()
    selectedPaymentMethod: PaymentMethodType;

    @Input()
    creditCardResponse: BazaNcPurchaseFlowGetCreditCardResponse;

    @Input()
    dwollaCustomerDetailResponse: BazaNcDwollaCustomerDetailDto;

    @Input()
    bankAccountResponse: BazaNcBankAccountAchDto;

    @Input()
    isPurchaseAboveLimit: boolean;

    @Input()
    isForeignInvestor = true;

    @Input()
    hasEnoughFunds = true;

    @Output()
    modalTitleChange: EventEmitter<string> = new EventEmitter();

    @Output()
    paymentMethodChange: EventEmitter<PaymentMethodType> = new EventEmitter();

    @Output()
    addUpdatePaymentMethodClick: EventEmitter<PaymentMethodType> = new EventEmitter();

    isDwollaAvailable = false;
    isNCBankAccountAvailable = false;
    isCardAvailable = false;

    public i18nBasePath = 'dwpf.pymtEdit.list';

    constructor(private store: Store, public dss: DwollaSharedService, public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit(): void {
        // set modal title in parent component
        const modalTitle =
            this.dss.isCardAvailable(this.creditCardResponse) &&
            (this.dss.isNCBankAccountAvailable(this.bankAccountResponse) || this.dss.isDwollaAvailable(this.dwollaCustomerDetailResponse))
                ? this.wts.getI18nLabel(this.i18nBasePath, 'title.plural')
                : this.wts.getI18nLabel(this.i18nBasePath, 'title.singular');

        this.modalTitleChange.emit(modalTitle);

        this.isDwollaAvailable = this.dss.isDwollaAvailable(this.dwollaCustomerDetailResponse);
        this.isNCBankAccountAvailable = this.dss.isNCBankAccountAvailable(this.bankAccountResponse);
        this.isCardAvailable = this.dss.isCardAvailable(this.creditCardResponse);
    }

    onAccountBalanceSelect() {
        if (this.showAccBalanceRadioBtn) {
            this.paymentMethodChange.emit(PaymentMethodType.accountBalance);
        }
    }

    onBankAccountSelect() {
        if (!this.dss.isNCBankAccountAvailable(this.bankAccountResponse)) {
            this.onBankAccountUpdate();
        } else if (this.dss.isNCBankAccountAvailable(this.bankAccountResponse) && this.showACHRadioBtn) {
            this.paymentMethodChange.emit(PaymentMethodType.bankAccount);
        }
    }

    onBankAccountUpdate() {
        this.addUpdatePaymentMethodClick.emit(PaymentMethodType.bankAccount);
    }

    onCreditCardSelect() {
        if (!this.dss.isCardAvailable(this.creditCardResponse)) {
            this.onCreditCardUpdate();
        } else if (!this.isPurchaseAboveLimit && this.dss.isCardAvailable(this.creditCardResponse) && this.showCCRadioBtn) {
            this.paymentMethodChange.emit(PaymentMethodType.creditCard);
        }
    }

    onCreditCardUpdate() {
        this.addUpdatePaymentMethodClick.emit(PaymentMethodType.creditCard);
    }

    public getPurchaseLimitWarningMsg(maxAmount: string) {
        return this.wts.getI18nLabel(this.i18nBasePath, 'card.limitsWarning', { maxAmount });
    }

    public get isAccBalanceDisabled(): boolean {
        return !this.dss.isDwollaAvailable(this.dwollaCustomerDetailResponse) || !this.hasEnoughFunds;
    }

    public get showAccBalanceRadioBtn() {
        return this.dss.isDwollaAvailable(this.dwollaCustomerDetailResponse);
    }

    public get showACHRadioBtn() {
        let showRadioBtn = false;

        if (this.isForeignInvestor) {
            showRadioBtn = this.dss.isNCBankAccountAvailable(this.bankAccountResponse) && this.dss.isCardAvailable(this.creditCardResponse);
        } else {
            showRadioBtn =
                this.dss.isNCBankAccountAvailable(this.bankAccountResponse) &&
                (this.dss.isCardAvailable(this.creditCardResponse) ||
                    (this.dss.isDwollaAvailable(this.dwollaCustomerDetailResponse) && this.hasEnoughFunds));
        }

        return showRadioBtn;
    }

    public get showCCRadioBtn() {
        let showRadioBtn = false;

        if (this.isForeignInvestor) {
            showRadioBtn = this.dss.isNCBankAccountAvailable(this.bankAccountResponse) && this.dss.isCardAvailable(this.creditCardResponse);
        } else {
            showRadioBtn =
                this.dss.isCardAvailable(this.creditCardResponse) &&
                (this.dss.isNCBankAccountAvailable(this.bankAccountResponse) ||
                    (this.dss.isDwollaAvailable(this.dwollaCustomerDetailResponse) && this.hasEnoughFunds));
        }

        return showRadioBtn;
    }
}
