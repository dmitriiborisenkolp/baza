export const DwollaPaymentEditListEnI18n = {
    title: {
        singular: 'Payment Method',
        plural: 'Payment Methods',
    },
    balance: {
        title: 'Account Balance',
        prefix: 'Current balance:',
        hint: 'Fund your account',
        errors: {
            insufficientFunds:
                'There are insufficient funds in your account to complete your purchase. Please choose another payment method below.',
        },
    },
    bank: {
        title: 'Bank Account',
        actions: {
            update: 'Update',
        },
    },
    card: {
        title: 'Card',
        feeSuffix: 'Card processing fee',
        limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account',
        actions: {
            update: 'Update',
        },
    },
};
