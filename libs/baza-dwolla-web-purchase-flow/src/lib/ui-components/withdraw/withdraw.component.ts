import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcBankAccountType, BazaNcBootstrapDto, BazaNcDwollaCustomerDetailDto } from '@scaliolabs/baza-nc-shared';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService, Message } from '@scaliolabs/baza-web-utils';
import { BehaviorSubject, Observable, combineLatest, concat, map, skipWhile, tap } from 'rxjs';
import {
    DwollaGetDefaultBankAccount,
    DwollaLoadAccountBalance,
    DwollaPurchaseState,
    DwollaSharedService,
    WithdrawConfig,
} from '../../data-access';

@UntilDestroy()
@Component({
    selector: 'app-dwolla-withdraw',
    templateUrl: './withdraw.component.html',
    styleUrls: ['./withdraw.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WithdrawComponent implements OnInit {
    @Input()
    set initData(value: BazaNcBootstrapDto) {
        this.initData$.next(value);
    }
    public initData$: BehaviorSubject<BazaNcBootstrapDto> = new BehaviorSubject<BazaNcBootstrapDto>(null);

    @Input()
    config?: WithdrawConfig;

    @Output()
    withdrawTriggerEnableToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

    withdrawAmountErrorMessage$: Observable<Message | undefined>;

    withdrawFundsForm: FormGroup;
    isWithdrawFundsModalVisible = false;
    isWithdrawFundsNotificationVisible = false;

    dwollaDefaultCashOutAccount$ = this.store.select(DwollaPurchaseState.dwollaDefaultCashOutAccount);
    dwollaCustomerDetail$ = this.store.select(DwollaPurchaseState.dwollaCustomerDetail);

    constructor(
        public readonly dss: DwollaSharedService,
        private readonly store: Store,
        public readonly uts: BazaLinkUtilSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {
        this.withdrawTriggerEnableToggle.emit(false);
        this.loadData();
    }

    ngOnInit(): void {
        this.checkDefaultConfig();
        this.resetWithdrawFundsForm();
        this.setDefaultTriggerEnableStatus();
    }

    setDefaultTriggerEnableStatus() {
        combineLatest([this.initData$, this.dwollaCustomerDetail$])
            .pipe(
                untilDestroyed(this),
                map(([initData, dwollaCustomerDetail]) => ({
                    initData,
                    dwollaCustomerDetail,
                })),
                skipWhile((pair) => !pair?.initData || !pair?.dwollaCustomerDetail),
            )
            .subscribe((pair) => {
                if (this.dss.isDwollaCashOutAccountLinked(pair?.initData) && +(pair?.dwollaCustomerDetail.balance?.value ?? '0') >= 0.01) {
                    this.withdrawTriggerEnableToggle.emit(true);
                }
            });
    }

    loadData() {
        concat(
            this.store.dispatch(new DwollaLoadAccountBalance()),
            this.store.dispatch(new DwollaGetDefaultBankAccount(BazaNcBankAccountType.CashOut, { throwError: false })),
        );
    }

    onWithdrawFundsClicked() {
        this.loadData();
        this.openWithdrawFundsPopup();
    }

    onWithdrawFundsCancel() {
        this.isWithdrawFundsModalVisible = false;
        this.resetWithdrawFundsForm();
    }

    onSubmitWithdrawFundsForm() {
        this.withdrawAmountErrorMessage$ = this.dss
            .processSubmitWithdrawFundsForm(this.withdrawFundsForm, {
                contactLink: this.uts.getUrl(this.config?.links?.contact),
            })
            .pipe(
                tap((result) => {
                    if (result?.type === 'error') return;

                    this.store.dispatch(new DwollaLoadAccountBalance());
                    this.onWithdrawFundsCancel();
                    this.displayWithdrawFundsNotification();
                }),
            );
    }

    displayWithdrawFundsNotification() {
        this.isWithdrawFundsNotificationVisible = true;
    }

    private openWithdrawFundsPopup() {
        this.resetWithdrawFundsForm();
        this.isWithdrawFundsModalVisible = true;
    }

    private resetWithdrawFundsForm(): void {
        this.withdrawFundsForm = this.dss.generateWithdrawFundsForm();
        this.withdrawAmountErrorMessage$ = null;
    }

    private checkDefaultConfig() {
        const defaultConfig: WithdrawConfig = {
            links: {
                contact: {
                    extLink: {
                        link: 'bazaContentTypes.contacts.email',
                        text: 'contact us',
                        isCMSLink: true,
                        isMailLink: true,
                    },
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
