import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { WithdrawComponent } from './withdraw.component';
import { WithdrawFundsModalModule, WithdrawFundsNotificationModule } from '@scaliolabs/baza-web-ui-components';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';

@NgModule({
    declarations: [WithdrawComponent],
    imports: [CommonModule, RouterModule, UtilModule, WithdrawFundsModalModule, WithdrawFundsNotificationModule, NzSkeletonModule],
    exports: [WithdrawComponent],
})
export class WithdrawModule {}
