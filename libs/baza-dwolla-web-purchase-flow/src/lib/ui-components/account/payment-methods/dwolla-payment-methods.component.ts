import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcBankAccountType, BazaNcBootstrapDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { concat } from 'rxjs';
import {
    AccBalanceConfig,
    DwollaGetDefaultBankAccount,
    DwollaGetLimits,
    DwollaLoadAccountBalance,
    DwollaLoadBankAccount,
    DwollaLoadCreditCard,
    DwollaPaymentMethodsConfig,
    DwollaPurchaseState,
    DwollaSharedService,
} from '../../../data-access';

@UntilDestroy()
@Component({
    selector: 'app-dwolla-payment-methods',
    templateUrl: './dwolla-payment-methods.component.html',
    styleUrls: ['./dwolla-payment-methods.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaPaymentMethodsComponent implements OnInit {
    @Input()
    initData: BazaNcBootstrapDto;

    @Input()
    paymentMethodsConfig?: DwollaPaymentMethodsConfig;

    @Input()
    accountBalanceConfig?: AccBalanceConfig;

    dwollaCustomerDetail$ = this.store.select(DwollaPurchaseState.dwollaCustomerDetail);
    dwollaDefaultCashInAccount$ = this.store.select(DwollaPurchaseState.dwollaDefaultCashInAccount);
    bankAccount$ = this.store.select(DwollaPurchaseState.bankAccount);
    creditCard$ = this.store.select(DwollaPurchaseState.creditCard);
    limits$ = this.store.select(DwollaPurchaseState.limits);

    constructor(public readonly dss: DwollaSharedService, private readonly store: Store, private readonly wts: BazaWebUtilSharedService) {}

    ngOnInit(): void {
        this.checkDefaultConfig();
        this.loadData();
    }

    private loadData() {
        if (this.paymentMethodsConfig.showAccountBalance) {
            concat(
                this.store.dispatch(new DwollaLoadAccountBalance()),
                this.store.dispatch(new DwollaGetDefaultBankAccount(BazaNcBankAccountType.CashIn, { throwError: false })),
            );
        }

        if (this.paymentMethodsConfig.showBankAccount) {
            this.store.dispatch(new DwollaLoadBankAccount());
        }

        if (this.paymentMethodsConfig.showCreditCard) {
            concat(this.store.dispatch(new DwollaGetLimits()), this.store.dispatch(new DwollaLoadCreditCard()));
        }
    }

    private checkDefaultConfig() {
        const defaultPaymentMethodsConfig: DwollaPaymentMethodsConfig = {
            showAccountBalance: true,
            showBankAccount: true,
            showCreditCard: true,
        };

        this.paymentMethodsConfig = this.wts.mergeConfig(defaultPaymentMethodsConfig, this.paymentMethodsConfig);

        const defaultAccBalanceConfig: AccBalanceConfig = {
            links: {
                contact: {
                    extLink: {
                        link: 'bazaContentTypes.contacts.email',
                        text: 'contact us',
                        isCMSLink: true,
                        isMailLink: true,
                    },
                },
            },
        };

        this.accountBalanceConfig = this.wts.mergeConfig(defaultAccBalanceConfig, this.accountBalanceConfig);
    }

    refreshInitData() {
        this.wts.refreshInitData$.next();
    }

    onRefreshCashInData() {
        this.refreshInitData();
        concat(
            this.store.dispatch(new DwollaLoadAccountBalance()),
            this.store.dispatch(new DwollaGetDefaultBankAccount(BazaNcBankAccountType.CashIn, { throwError: false })),
        );
    }

    onRefreshBankData() {
        this.refreshInitData();
        this.store.dispatch(new DwollaLoadBankAccount());
    }

    onRefreshCardData() {
        this.refreshInitData();
        this.store.dispatch(new DwollaLoadCreditCard());
    }
}
