import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UntilDestroy } from '@ngneat/until-destroy';
import {
    BazaNcBankAccountExport,
    BazaNcBankAccountGetDefaultByTypeResponse,
    BazaNcBankAccountLinkOnSuccessRequest,
    BazaNcBankAccountType,
    BazaNcBootstrapDto,
    BazaNcDwollaCustomerDetailDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService, I18nLinkifyConfig, Message } from '@scaliolabs/baza-web-utils';
import { Observable, tap } from 'rxjs';
import { AccBalanceConfig, AccountBalanceExitReason, DwollaSharedService } from '../../../../data-access';

@UntilDestroy()
@Component({
    selector: 'app-dwolla-account-balance',
    templateUrl: './account-balance.component.html',
    styleUrls: ['./account-balance.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaAccountBalanceComponent implements OnInit {
    @Input()
    initData: BazaNcBootstrapDto;

    @Input()
    config?: AccBalanceConfig;

    @Input()
    dwollaCustomerDetail!: BazaNcDwollaCustomerDetailDto;

    @Input()
    dwollaDefaultCashInAccount!: BazaNcBankAccountGetDefaultByTypeResponse;

    @Output()
    refreshCashInData = new EventEmitter<void>();

    transferAmountErrorMessage$: Observable<Message | undefined>;
    addFundsForm: FormGroup;
    isAddFundsFormVisible = false;
    reopenAddFundsForm = false;
    isAddFundsNotificationVisible = false;

    public readonly showDwollaAccountCreationError$ = this.dss.showDwollaAccountCreationError$.pipe(
        tap((showError) => (this.showDwollaAccountCreationError = showError)),
    );

    showDwollaAccountCreationError = false;
    openTransferFundsModal = false;

    isPanelOpen = false;
    i18nBasePath = 'dwacc.payment.balance';

    constructor(
        public readonly dss: DwollaSharedService,
        private readonly cdr: ChangeDetectorRef,
        public readonly uts: BazaLinkUtilSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    public get walletSupportLinkConfig(): Array<I18nLinkifyConfig> {
        return this.wts.getI18nLinksConfig([
            { path: `${this.i18nBasePath}.alerts.walletSupport.linkConfig`, config: this.config?.links?.contact },
        ]);
    }

    ngOnInit(): void {
        this.resetAddFundsForm();
    }

    onAccountBalanceClicked() {
        this.dss.processAccountBalanceCreation({
            isDwollaAvailable: this.dss.isDwollaAvailable(this.initData),
            onExit: (reason) => {
                switch (reason) {
                    case AccountBalanceExitReason.DwollaBalanceAlreadyAvailable:
                        this.triggerDwollaPlaidFlow();
                        break;
                }
            },
            onInvestorTouched: (dwollaPurchase) => {
                const isDwollaAvailable = dwollaPurchase?.dwollaInvestorTouchResponse?.isDwollaAvailable ?? false;

                if (this.dwollaCustomerDetail) {
                    this.dwollaCustomerDetail.isDwollaAvailable = isDwollaAvailable;
                }

                this.dss.showDwollaAccountCreationError$.next(!isDwollaAvailable);
                this.cdr.detectChanges();

                if (isDwollaAvailable) {
                    this.triggerDwollaPlaidFlow();
                }
            },
        });
    }

    onAddFundsClicked() {
        if (this.dss.isDwollaCashInAccountLinked(this.initData)) {
            this.openTransferFundsPopup();
        } else {
            this.triggerDwollaPlaidFlow();
        }
    }

    handleAddFundsCancel() {
        this.isAddFundsFormVisible = false;
        this.resetAddFundsForm();
    }

    displayAddFundsNotification() {
        this.isAddFundsNotificationVisible = true;
    }

    handleEditCashInAccount() {
        this.isAddFundsFormVisible = false;
        this.reopenAddFundsForm = true;
        this.triggerDwollaPlaidFlow();
    }

    submitAddFundsForm() {
        this.transferAmountErrorMessage$ = this.dss
            .processSubmitAddFundsForm(this.addFundsForm, {
                contactLink: this.uts.getUrl(this.config.links.contact),
            })
            .pipe(
                tap((result) => {
                    if (result?.type === 'error') return;

                    this.wts.refreshInitData$.next();
                    this.handleAddFundsCancel();
                    this.displayAddFundsNotification();
                }),
            );
    }

    triggerDwollaPlaidFlow(triggerAddFundsOnCompletion = true) {
        this.dss.processDwollaPlaidFlow({
            onSuccess: (publicToken, identity) => {
                // link cash-in account
                this.linkBankAccount(
                    {
                        publicToken,
                        type: BazaNcBankAccountType.CashIn,
                        export: [BazaNcBankAccountExport.Dwolla],
                        setAsDefault: true,
                        identity,
                    },
                    () => {
                        // cash-in not linked already, attach cash-in as default cash-out
                        if (!this.dss.isDwollaCashOutAccountLinked(this.initData)) {
                            this.linkBankAccount(
                                {
                                    publicToken,
                                    type: BazaNcBankAccountType.CashOut,
                                    export: [BazaNcBankAccountExport.Dwolla],
                                    setAsDefault: true,
                                    identity,
                                },
                                () => {
                                    this.dss.updateCashOutAccount$.next();
                                    this.handleOnPlaidCompletionFlow(triggerAddFundsOnCompletion);
                                },
                            );
                        } else {
                            this.handleOnPlaidCompletionFlow(triggerAddFundsOnCompletion);
                        }
                    },
                );
            },
            onExit: () => {
                this.handleOnPlaidCompletionFlow(this.reopenAddFundsForm);
            },
        });
    }

    private handleOnPlaidCompletionFlow(triggerAddFundsOnCompletion) {
        // trigger add funds flow if/when required
        if (triggerAddFundsOnCompletion) {
            this.openTransferFundsModal = true;
            this.reopenAddFundsForm = false;
        }

        this.refreshCashInData.emit();
    }

    private linkBankAccount(request: BazaNcBankAccountLinkOnSuccessRequest, onSuccess: () => void = null) {
        this.dss.processDwollaLinkBankAccount(request, onSuccess);
    }

    private openTransferFundsPopup() {
        this.resetAddFundsForm();
        this.isAddFundsFormVisible = true;
    }

    private resetAddFundsForm(): void {
        this.addFundsForm = this.dss.generateAddFundsForm();
        this.transferAmountErrorMessage$ = null;
    }

    public get showDwollaAccountWarning() {
        return (
            !this.showDwollaAccountCreationError &&
            !this.dss.isDwollaAvailable(this.initData) &&
            this.dss.isNCBankAccountAvailable(this.initData) &&
            this.dss.isCardAvailable(this.initData)
        );
    }
}
