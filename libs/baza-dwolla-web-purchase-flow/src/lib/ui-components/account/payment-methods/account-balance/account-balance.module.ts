import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PaymentItemModule, DwollaAddFundsModalModule, DwollaAddFundsNotificationModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { DwollaAccountBalanceComponent } from './account-balance.component';

const NZ_MODULES = [NzAlertModule, NzButtonModule, NzCollapseModule];

@NgModule({
    declarations: [DwollaAccountBalanceComponent],
    imports: [
        CommonModule,
        RouterModule,
        UtilModule,
        PaymentItemModule,
        DwollaAddFundsModalModule,
        DwollaAddFundsNotificationModule,
        NZ_MODULES,
    ],
    exports: [DwollaAccountBalanceComponent],
})
export class DwollaAccountBalanceModule {}
