import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { DwollaAccountBalanceModule } from './account-balance/account-balance.module';
import { DwollaBankAccountModule } from './bank-account/bank-account.module';
import { DwollaCreditCardModule } from './credit-card/credit-card.module';
import { DwollaPaymentMethodsComponent } from './dwolla-payment-methods.component';

const NZ_MODULES = [NzGridModule, NzSkeletonModule];

@NgModule({
    declarations: [DwollaPaymentMethodsComponent],
    imports: [
        CommonModule,
        RouterModule,
        UtilModule,
        DwollaAccountBalanceModule,
        DwollaBankAccountModule,
        DwollaCreditCardModule,
        NZ_MODULES,
    ],
    exports: [DwollaPaymentMethodsComponent],
})
export class DwollaPaymentMethodsModule {}
