export const DwollaPaymentEnI18n = {
    title: 'Payment Methods',
    popover: {
        content:
            'These are the payment sources used to purchase shares. Click below to add a bank account, credit card or to update your current payment methods.',
    },
};
