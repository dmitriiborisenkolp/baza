import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { BazaNcLimitsDto, BazaNcPurchaseFlowGetCreditCardResponse } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { DwollaSharedService } from '../../../../data-access';

@UntilDestroy()
@Component({
    selector: 'app-dwolla-credit-card',
    templateUrl: './credit-card.component.html',
    styleUrls: ['./credit-card.component.less'],
})
export class DwollaCreditCardComponent implements OnInit {
    @Input()
    creditCardResponse?: BazaNcPurchaseFlowGetCreditCardResponse;

    @Input()
    limits?: BazaNcLimitsDto;

    @Output()
    refreshCardData = new EventEmitter<void>();

    cardDetailsForm: FormGroup;

    isCardDetailsFormVisible = false;
    i18nBasePath = 'dwacc.payment.card';

    constructor(
        public readonly dss: DwollaSharedService,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    ngOnInit() {
        this.resetCardDetailsForm();
    }

    updateCreditCard() {
        this.isCardDetailsFormVisible = true;
    }

    addCreditCard() {
        this.processSubmitCardForm(this.cardDetailsForm);
    }

    submitCardForm() {
        this.processSubmitCardForm(this.cardDetailsForm);
    }

    processSubmitCardForm = (form) =>
        this.dss.processSubmitCardForm(form, () => {
            this.refreshCardData.emit();
            this.onClosePaymentEditCardModal();
        });

    handleCardDetailsCancel() {
        this.isCardDetailsFormVisible = false;
        this.resetCardDetailsForm();
    }

    private resetCardDetailsForm(): void {
        this.cardDetailsForm = this.dss.generateCreditCardForm();
    }

    private onClosePaymentEditCardModal() {
        this.resetCardDetailsForm();
        this.handleCardDetailsCancel();
        this.isCardDetailsFormVisible = false;
    }
}
