import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PaymentCardModalModule, PaymentItemModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { DwollaCreditCardComponent } from './credit-card.component';

const NZ_MODULES = [NzButtonModule];

@NgModule({
    declarations: [DwollaCreditCardComponent],
    imports: [CommonModule, RouterModule, UtilModule, PaymentItemModule, PaymentCardModalModule, NZ_MODULES],
    exports: [DwollaCreditCardComponent],
})
export class DwollaCreditCardModule {}
