export const DwollaPaymentCardEnI18n = {
    title: 'Card',
    details: {
        number: 'Card number:',
        expiring: 'Expiring:',
        fee: 'card processing fee',
    },
    alerts: {
        limit: 'Purchases above {{ limit }} require the use of a linked bank account.',
    },
    actions: {
        update: 'Update',
    },
};
