import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PaymentBankAccountModalModule, PaymentItemModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { DwollaBankAccountComponent } from './bank-account.component';

const NZ_MODULES = [NzButtonModule];

@NgModule({
    declarations: [DwollaBankAccountComponent],
    imports: [CommonModule, RouterModule, UtilModule, PaymentItemModule, PaymentBankAccountModalModule, NZ_MODULES],
    exports: [DwollaBankAccountComponent],
})
export class DwollaBankAccountModule {}
