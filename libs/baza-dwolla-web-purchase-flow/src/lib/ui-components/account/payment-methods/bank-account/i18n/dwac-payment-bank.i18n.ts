export const DwollaPaymentBankEnI18n = {
    title: 'Bank Account',
    hint: 'Used to purchase shares',
    details: {
        name: 'Account holder name:',
        number: 'Account number:',
        type: 'Account type:',
    },
    actions: {
        update: 'Update',
    },
};
