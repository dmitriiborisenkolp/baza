import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BazaNcBankAccountAchDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { Subject } from 'rxjs';
import { DwollaSharedService } from '../../../../data-access';

@UntilDestroy()
@Component({
    selector: 'app-dwolla-bank-account',
    templateUrl: './bank-account.component.html',
    styleUrls: ['./bank-account.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaBankAccountComponent implements OnInit {
    @Input()
    bankAccountResponse?: BazaNcBankAccountAchDto;

    @Output()
    refreshBankData = new EventEmitter<void>();

    bankDetailsForm: FormGroup;
    onSubmitManualBankDetailsForm: Subject<FormGroup> = new Subject();
    isBankDetailsFormVisible: boolean;
    i18nBasePath = 'dwacc.payment.bank';

    constructor(public readonly dss: DwollaSharedService, public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        this.resetBankDetailsForm();
    }

    updateBankAccount() {
        this.isBankDetailsFormVisible = true;
    }

    submitManualBankDetailsForm() {
        this.processSubmitManualBankDetailsForm(this.bankDetailsForm);
    }

    processSubmitManualBankDetailsForm = (form) =>
        this.dss.processSubmitManualBankDetailsForm(form, () => {
            this.refreshBankData.emit();
            this.onClosePaymentEditBankModal();
        });

    handleBankDetailsCancel() {
        this.isBankDetailsFormVisible = false;
        this.resetBankDetailsForm();
    }

    private resetBankDetailsForm(): void {
        this.bankDetailsForm = this.dss.generateBankDetailsForm();
    }

    private onClosePaymentEditBankModal() {
        this.resetBankDetailsForm();
        this.handleBankDetailsCancel();
    }
}
