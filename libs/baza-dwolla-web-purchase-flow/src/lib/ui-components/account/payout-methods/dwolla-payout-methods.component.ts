import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcBankAccountType, BazaNcBootstrapDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import {
    AccPayoutConfig,
    DwollaGetDefaultBankAccount,
    DwollaLoadAccountBalance,
    DwollaPurchaseState,
    DwollaSharedService,
} from '../../../data-access';
import { concat } from 'rxjs';

@UntilDestroy()
@Component({
    selector: 'app-dwolla-payout-methods',
    templateUrl: './dwolla-payout-methods.component.html',
    styleUrls: ['./dwolla-payout-methods.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaPayoutMethodsComponent implements OnInit {
    @Input()
    initData: BazaNcBootstrapDto;

    @Input()
    config?: AccPayoutConfig;

    dwollaDefaultCashOutAccount$ = this.store.select(DwollaPurchaseState.dwollaDefaultCashOutAccount);

    constructor(public readonly dss: DwollaSharedService, private readonly store: Store, private readonly wts: BazaWebUtilSharedService) {
        this.loadData();
    }

    ngOnInit(): void {
        this.checkDefaultConfig();

        this.dss.updateCashOutAccount$.pipe(untilDestroyed(this)).subscribe(() => {
            this.loadData();
        });
    }

    loadData() {
        this.store.dispatch(new DwollaGetDefaultBankAccount(BazaNcBankAccountType.CashOut, { throwError: false }));
    }

    checkDefaultConfig() {
        const defaultConfig: AccPayoutConfig = {
            links: {
                contact: {
                    extLink: {
                        link: 'bazaContentTypes.contacts.email',
                        isCMSLink: true,
                        isMailLink: true,
                    },
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }

    onRefreshCashOutData() {
        this.wts.refreshInitData$.next();
        this.loadData();
    }
}
