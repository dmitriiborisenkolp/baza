import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PaymentItemModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { DwollaBankAccountPayoutModule } from './bank-account/bank-account-payout.module';
import { DwollaPayoutMethodsComponent } from './dwolla-payout-methods.component';

const NZ_MODULES = [NzGridModule, NzSkeletonModule];

@NgModule({
    declarations: [DwollaPayoutMethodsComponent],
    imports: [CommonModule, RouterModule, UtilModule, PaymentItemModule, DwollaBankAccountPayoutModule, NZ_MODULES],
    exports: [DwollaPayoutMethodsComponent],
})
export class DwollaPayoutMethodsModule {}
