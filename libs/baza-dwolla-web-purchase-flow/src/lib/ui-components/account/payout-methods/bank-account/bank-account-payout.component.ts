import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import {
    BazaNcBankAccountExport,
    BazaNcBankAccountGetDefaultByTypeResponse,
    BazaNcBankAccountLinkOnSuccessRequest,
    BazaNcBankAccountType,
    BazaNcBootstrapDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService, I18nLinkifyConfig } from '@scaliolabs/baza-web-utils';
import { AccountBalanceExitReason, AccPayoutConfig, DwollaSharedService } from '../../../../data-access';

@UntilDestroy()
@Component({
    selector: 'app-dwolla-bank-account-payout',
    templateUrl: './bank-account-payout.component.html',
    styleUrls: ['./bank-account-payout.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaBankAccountPayoutComponent {
    @Input()
    config?: AccPayoutConfig;

    @Input()
    initData?: BazaNcBootstrapDto;

    @Input()
    dwollaDefaultCashOutAccount?: BazaNcBankAccountGetDefaultByTypeResponse;

    @Output()
    refreshCashOutData = new EventEmitter<void>();

    i18nBasePath = 'dwacc.payout.bank';

    constructor(
        public readonly dss: DwollaSharedService,
        public readonly uts: BazaLinkUtilSharedService,
        private readonly cdr: ChangeDetectorRef,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    get walletSupportLinkConfig(): Array<I18nLinkifyConfig> {
        return this.wts.getI18nLinksConfig([
            { path: `${this.i18nBasePath}.alerts.walletSupport.linkConfig`, config: this.config?.links?.contact },
        ]);
    }

    get withdrawalSupportLinkConfig(): Array<I18nLinkifyConfig> {
        return this.wts.getI18nLinksConfig([
            { path: `${this.i18nBasePath}.alerts.withdrawalSupport.linkConfig`, config: this.config?.links?.contact },
        ]);
    }

    public onPayoutContainerClicked() {
        this.dss.processAccountBalanceCreation({
            isDwollaAvailable: this.dss.isDwollaAvailable(this.initData),
            onExit: (reason) => {
                switch (reason) {
                    case AccountBalanceExitReason.DwollaBalanceAlreadyAvailable:
                        this.triggerDwollaPlaidFlow();
                        break;
                }
            },
            onInvestorTouched: (dwollaPurchase) => {
                const isDwollaAvailable = dwollaPurchase?.dwollaInvestorTouchResponse?.isDwollaAvailable ?? false;

                this.dss.showDwollaAccountCreationError$.next(!isDwollaAvailable);
                this.cdr.detectChanges();

                if (isDwollaAvailable) {
                    this.triggerDwollaPlaidFlow();
                }
            },
        });
    }

    triggerDwollaPlaidFlow() {
        this.dss.processDwollaPlaidFlow({
            onSuccess: (publicToken, identity) => {
                const linkAccountReq: BazaNcBankAccountLinkOnSuccessRequest = {
                    publicToken: publicToken,
                    type: BazaNcBankAccountType.CashOut,
                    export: [BazaNcBankAccountExport.Dwolla],
                    setAsDefault: true,
                    static: true,
                    secure: true,
                    identity: identity,
                };

                this.dss.processDwollaLinkBankAccount(linkAccountReq, () => {
                    this.refreshCashOutData.emit();
                });
            },
            onExit: () => {
                this.refreshCashOutData.emit();
            },
        });
    }
}
