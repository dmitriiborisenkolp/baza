import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PaymentItemModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { DwollaBankAccountPayoutComponent } from './bank-account-payout.component';

const NZ_MODULES = [NzAlertModule, NzButtonModule];

@NgModule({
    declarations: [DwollaBankAccountPayoutComponent],
    imports: [CommonModule, RouterModule, UtilModule, PaymentItemModule, NZ_MODULES],
    exports: [DwollaBankAccountPayoutComponent],
})
export class DwollaBankAccountPayoutModule {}
