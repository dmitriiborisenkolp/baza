import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import {
    BazaNcBootstrapDto,
    BazaNcPurchaseFlowLimitsForPurchaseResponse,
    BazaNcPurchaseFlowTransactionType,
    PurchaseFlowDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService, I18nLinkifyConfig } from '@scaliolabs/baza-web-utils';
import { combineLatest, debounceTime, distinctUntilChanged, map, of, skipWhile, switchMap, withLatestFrom } from 'rxjs';
import {
    DwollaGetLimits,
    DwollaGetLimitsForPurchase,
    DwollaPatchStartPurchase,
    DwollaPurchaseState,
    DwollaSharedService,
    DwollaSubmitPurchase,
    PaymentConfig,
} from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-purchase-payment-dwolla',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaPurchasePaymentComponent implements OnInit {
    @Input()
    initData: BazaNcBootstrapDto;

    @Input()
    config?: PaymentConfig;

    cart$ = this.store.select(DwollaPurchaseState.cart);
    purchaseStart$ = this.store.select(DwollaPurchaseState.purchaseStart);
    numberOfShares$ = this.store.select(DwollaPurchaseState.numberOfShares);
    limitsForPurchase$ = this.store.select(DwollaPurchaseState.limitsForPurchase);

    purchaseStart: PurchaseFlowDto;
    limitsForPurchase: BazaNcPurchaseFlowLimitsForPurchaseResponse;
    isPurchaseAboveLimit = false;
    isSubmitBtnDisabled = true;
    purchaseLimitsLoaded = false;
    loadMethods = false;
    public paymentMethodAdded = false;

    public i18nBasePath = 'dwpf.payment';

    constructor(
        private readonly router: Router,
        private readonly store: Store,
        private readonly dss: DwollaSharedService,
        public readonly uts: BazaLinkUtilSharedService,
        public readonly wts: BazaWebUtilSharedService,
        private readonly cdr: ChangeDetectorRef,
    ) {
        this.store.dispatch(new DwollaGetLimits());
    }

    ngOnInit(): void {
        this.loadMethods = false;
        this.purchaseLimitsLoaded = false;
        this.checkDefaultConfig();
        this.initializeDataStream();
    }

    initializeDataStream() {
        const result$ = combineLatest([this.purchaseStart$, this.numberOfShares$, this.cart$]).pipe(
            untilDestroyed(this),
            map(([purchaseStart, number, entity]) => ({
                purchaseStart,
                number,
                entity,
            })),
            skipWhile((pair) => !pair || !pair.entity || !pair.number || !pair.purchaseStart),
            distinctUntilChanged(),
            debounceTime(0),
            switchMap((pair) => {
                this.purchaseStart = pair.purchaseStart;
                const total = (pair.number || 1) * pair.entity.pricePerShareCents;

                if (!this.purchaseLimitsLoaded) {
                    return this.store
                        .dispatch(new DwollaGetLimitsForPurchase(total, pair.entity.offeringId))
                        .pipe(withLatestFrom(this.limitsForPurchase$));
                } else {
                    return of([]);
                }
            }),
        );

        result$
            .pipe(
                untilDestroyed(this),
                skipWhile((res) => !res || res?.length === 0),
            )
            .subscribe(([, res]) => {
                if (res) {
                    this.limitsForPurchase = res;
                    this.purchaseLimitsLoaded = true;
                    this.validateCCLimitForPurchase();
                }
            });
    }

    // onFormSubmit
    public onFormSubmit(): void {
        this.store.dispatch(new DwollaSubmitPurchase(this.purchaseStart.id)).subscribe(() => {
            this.router.navigate([this.dss.pfLinksConfig?.buyShares, 'done']);
        });
    }

    /**
     * Utility method to validate the purchaseLimit
     */
    private validateCCLimitForPurchase() {
        const limitsForPurchase = this.limitsForPurchase;
        const purchaseStart = this.purchaseStart;
        if (limitsForPurchase && purchaseStart) {
            const { amount, transactionFeesCents } = purchaseStart;
            const totalCents = amount + transactionFeesCents;

            const ccLimits = limitsForPurchase.find((limit) => limit.transactionType === BazaNcPurchaseFlowTransactionType.CreditCard);
            this.isPurchaseAboveLimit =
                !ccLimits?.isAvailable ||
                // Based on active Payment method CC fees are added and Total changes.
                // So, Manually verifying if total goes above limit? Yes, restrict the CC.
                totalCents + ccLimits?.feeCents > ccLimits?.maximumAmountCents;
        }

        this.loadMethods = true;
        this.cdr.markForCheck();
    }

    public onBackClick(): void {
        if (this.purchaseStart) {
            this.purchaseStart.fee = null;
            this.store.dispatch(new DwollaPatchStartPurchase(this.purchaseStart));
        }

        this.router.navigate([this.dss.pfLinksConfig?.buyShares, 'agreement']);
    }

    onUpdateSubmitBtnDisableStatus(isDisabled: boolean) {
        this.isSubmitBtnDisabled = isDisabled;
    }

    checkDefaultConfig() {
        const defaultConfig: PaymentConfig = {
            methods: {
                showPurchaseDividendMessage: true,
            },
            steps: {
                links: {
                    termsOfService: {
                        extLink: {
                            link: 'bazaCommon.links.termsOfService',
                            isCMSLink: true,
                        },
                    },
                    eftDisclosure: {
                        appLink: {
                            commands: ['/eft-disclosure'],
                        },
                    },
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }

    get authorizationLinksConfig(): Array<I18nLinkifyConfig> {
        return this.wts.getI18nLinksConfig([
            {
                path: `${this.i18nBasePath}.authorization.tosLinkConfig`,
                config: this.config?.steps?.links?.termsOfService,
            },
            {
                path: `${this.i18nBasePath}.authorization.eftLinkConfig`,
                config: this.config?.steps?.links?.eftDisclosure,
            },
        ]);
    }
}
