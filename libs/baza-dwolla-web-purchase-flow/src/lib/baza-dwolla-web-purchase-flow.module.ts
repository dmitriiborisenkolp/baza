import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { BazaNcDataAccessModule } from '@scaliolabs/baza-nc-data-access';
import { DwollaVerificationStateModule } from '@scaliolabs/baza-dwolla-web-verification-flow';
import {
    BackLinkModule,
    BankDetailsModule,
    CardDetailsModule,
    IframeIntegrationModule,
    SummaryModule,
    AccountBalanceCardModule,
    PersonalInfoModule,
    PaymentHeaderModule,
    DwollaAddFundsModalModule,
    PaymentBankAccountModalModule,
    PaymentCardModalModule,
    PaymentBankAccountModule,
    PaymentCardModule,
} from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { DwollaPurchaseAgreementComponent } from './agreement/agreement.component';
import { DwollaPurchaseStateModule } from './data-access';
import { DwollaPurchaseDetailsComponent } from './details/details.component';
import { DwollaPurchaseDoneComponent } from './done/done.component';
import { DwollaPurchaseMethodsComponent } from './methods/methods.component';
import { DwollaPurchasePaymentComponent } from './payment/payment.component';
import { DwollaPurchaseComponent } from './purchase.component';

import { WithdrawModule, PaymentEditModule } from './ui-components';

const NZ_MODULES = [
    NzAlertModule,
    NzButtonModule,
    NzGridModule,
    NzInputNumberModule,
    NzModalModule,
    NzPopoverModule,
    NzSkeletonModule,
    NzStepsModule,
];

@NgModule({
    declarations: [
        DwollaPurchaseAgreementComponent,
        DwollaPurchaseComponent,
        DwollaPurchaseDetailsComponent,
        DwollaPurchaseDoneComponent,
        DwollaPurchaseMethodsComponent,
        DwollaPurchasePaymentComponent,
    ],
    imports: [
        ...NZ_MODULES,
        BackLinkModule,
        BankDetailsModule,
        BazaNcDataAccessModule,
        CardDetailsModule,
        CommonModule,
        FormsModule,
        IframeIntegrationModule,
        PaymentBankAccountModalModule,
        PaymentBankAccountModule,
        PaymentCardModalModule,
        PaymentCardModule,
        PaymentEditModule,
        PaymentHeaderModule,
        PersonalInfoModule,
        DwollaPurchaseStateModule,
        DwollaAddFundsModalModule,
        AccountBalanceCardModule,
        ReactiveFormsModule,
        RouterModule,
        SummaryModule,
        UtilModule,
        DwollaVerificationStateModule,
        WithdrawModule,
    ],
    exports: [
        DwollaPurchaseAgreementComponent,
        DwollaPurchaseComponent,
        DwollaPurchaseDetailsComponent,
        DwollaPurchaseDoneComponent,
        DwollaPurchaseMethodsComponent,
        DwollaPurchasePaymentComponent,
    ],
})
export class BazaDwollaPurchaseFlowModule {}
