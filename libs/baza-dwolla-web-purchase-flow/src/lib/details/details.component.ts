import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    AccountVerificationPersonalInformationDto,
    BazaNcPurchaseFlowTransactionType,
    DestroySessionDto,
    PurchaseFlowDto,
    PurchaseFlowSessionDto,
    StatsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, skipWhile, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import {
    DwollaCancelPurchase,
    DwollaGetPurchaseStats,
    DwollaPatchStartPurchase,
    DwollaPurchaseState,
    DwollaSharedService,
    DwollaStartPurchase,
} from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-purchase-details-dwolla',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DwollaPurchaseDetailsComponent implements OnInit {
    @Input()
    personal$: Observable<AccountVerificationPersonalInformationDto>;

    cart$ = this.store.select(DwollaPurchaseState.cart);
    stats$ = this.store.select(DwollaPurchaseState.stats);
    limitsForPurchase$ = this.store.select(DwollaPurchaseState.limitsForPurchase);
    numberOfShares$ = this.store.select(DwollaPurchaseState.numberOfShares);
    purchaseStart$ = this.store.select(DwollaPurchaseState.purchaseStart);

    numberOfShares: number;
    stats: StatsDto;
    numberOfSharesMin: number;
    entity: BazaNcIntegrationListingsDto;
    statsLoaded = false;
    public shareMapping: { [k: string]: string } = {
        '=1': '# share',
        other: '# shares',
    };

    public i18nBasePath = 'dwpf.details';

    constructor(
        private readonly dss: DwollaSharedService,
        private readonly router: Router,
        private readonly store: Store,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    ngOnInit(): void {
        this.statsLoaded = false;
        this.destroyPreviousSessions();

        const result$ = combineLatest([this.numberOfShares$, this.cart$]).pipe(
            untilDestroyed(this),
            map(([number, entity]) => ({
                number,
                entity,
            })),
            distinctUntilChanged(),
            debounceTime(0),
            skipWhile((pair) => !pair || !pair.entity),
            switchMap((pair) => {
                this.entity = pair.entity;

                const priceAmount = pair.entity.pricePerShareCents * pair.entity.numSharesMin;
                this.numberOfShares = this.dss.getMinNumberOfSharesForEntity(pair.number, pair.entity);
                this.numberOfSharesMin = pair.entity.numSharesMin;

                if (!this.statsLoaded) {
                    return this.store
                        .dispatch(new DwollaGetPurchaseStats(pair.entity.offeringId, priceAmount))
                        .pipe(withLatestFrom(this.stats$));
                }

                return of([]);
            }),
        );

        result$.pipe(untilDestroyed(this)).subscribe(([, res]) => {
            if (res) {
                this.stats = res;
                this.statsLoaded = true;
            }
        });
    }

    public decreaseShares(): void {
        if (this.numberOfShares - 1 >= 1) {
            this.numberOfShares--;
        }
    }

    public increaseShares(): void {
        if (this.numberOfSharesMin > +this.numberOfShares + 1) {
            this.numberOfShares = this.numberOfSharesMin;
        } else if (this.numberOfShares + 1 <= this.stats.canPurchase) {
            this.numberOfShares++;
        }
    }

    // onEditPersonalInfo
    public onEditPersonalInfo(): void {
        this.store.dispatch(
            new DwollaPatchStartPurchase({
                numberOfShares: this.numberOfShares,
            } as PurchaseFlowDto),
        );

        this.router.navigate([this.dss.pfLinksConfig.verification, 'info'], {
            queryParams: {
                redirect: this.dss.pfLinksConfig?.buyShares,
                buy: true,
            },
        });
    }

    // onFormSubmit
    public onFormSubmit(entity: BazaNcIntegrationListingsDto): void {
        const purchase: PurchaseFlowSessionDto = {
            amount: this.numberOfShares * entity.pricePerShareCents,
            numberOfShares: this.numberOfShares,
            offeringId: entity.offeringId,
            requestDocuSignUrl: true,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        };

        this.store
            .dispatch(new DwollaStartPurchase(purchase))
            .pipe(withLatestFrom(this.purchaseStart$))
            .subscribe(([, res]) => {
                this.store.dispatch(new DwollaPatchStartPurchase(res));
                this.router.navigate([this.dss.pfLinksConfig?.buyShares, 'agreement']);
            });
    }

    checkNumberOfShares(minNumber: number, maxNumber: number): void {
        if (this.numberOfShares < minNumber) {
            this.numberOfShares = minNumber;
        } else if (this.numberOfShares > maxNumber) {
            this.numberOfShares = maxNumber;
        }
    }

    destroyPreviousSessions() {
        this.cart$
            .pipe(
                untilDestroyed(this),
                skipWhile((res) => !res),
                take(1),
            )
            .subscribe((item) => {
                if (item) {
                    const data: DestroySessionDto = {
                        offeringId: item.offeringId,
                    };
                    this.store.dispatch(new DwollaCancelPurchase(data));
                }
            });
    }
}
