import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BazaRegistryNgService } from '@scaliolabs/baza-core-ng';
import { AppLink, ExtLink, LinkConfig, NavigationLink } from '../models';

@Injectable()
export class BazaLinkUtilSharedService {
    constructor(private readonly registry: BazaRegistryNgService, private readonly router: Router) {}

    public navigate(linkToNavigate: NavigationLink): void {
        this.isAppLink(linkToNavigate) ? this.openAppLink(linkToNavigate) : this.openExtLink(linkToNavigate);
    }

    public getUrl(linkToNavigate: NavigationLink): string {
        let url: string;
        if (linkToNavigate) {
            url = this.isAppLink(linkToNavigate) ? this.getUrlAppLink(linkToNavigate) : this.getUrlExtLink(linkToNavigate);
        }
        return (url ?? '').trim();
    }

    public getUrlText(linkToNavigate: NavigationLink): string {
        const urlText = this.isAppLink(linkToNavigate) ? this.getUrlTextAppLink(linkToNavigate) : this.getUrlTextExtLink(linkToNavigate);
        return this.cleanUrlText(urlText);
    }

    // #region InternalLogic
    openAppLink(linkToNavigate: NavigationLink): void {
        const appLink = this.getAppLink(linkToNavigate);
        const url = this.getUrlAppLink(linkToNavigate);

        if (appLink.target) window.open(url, appLink.target ?? '_blank');
        else this.router.navigate(appLink.commands, appLink.navExtras);
    }

    getUrlTextAppLink(linkToNavigate: NavigationLink): string {
        const appLink = this.getAppLink(linkToNavigate);

        if (!appLink.text) {
            const urlTree = this.router.createUrlTree(appLink.commands, appLink.navExtras);
            return this.router.serializeUrl(urlTree) ?? '';
        }

        return appLink.text;
    }

    getUrlAppLink(linkToNavigate: NavigationLink): string {
        const appLink = this.getAppLink(linkToNavigate);
        const urlTree = this.router.createUrlTree(appLink.commands, appLink.navExtras);

        return this.router.serializeUrl(urlTree) ?? '';
    }

    openExtLink(linkToNavigate: NavigationLink): void {
        const extLink = this.getExtLink(linkToNavigate);
        const url = this.getUrlExtLink(extLink);

        window.open(url, extLink.target ?? '_blank');
    }

    getUrlExtLink(linkToNavigate: NavigationLink): string {
        const extLink = this.getExtLink(linkToNavigate);
        let url = this.isCMSLink(extLink) ? this.getLinkFromReg(extLink.link) : extLink.link;

        if (this.isMailLink(extLink)) url = this.getMailLink(url);

        return url;
    }

    getUrlTextExtLink(linkToNavigate: NavigationLink): string {
        const extLink = this.getExtLink(linkToNavigate);
        const urlText = extLink.text ?? this.getUrlExtLink(linkToNavigate);
        return this.cleanUrlText(urlText);
    }

    getLinkFromReg(linkId: string): string {
        return this.registry.value(linkId);
    }

    getMailLink(link: string): string {
        return `mailto:${link}`;
    }

    cleanUrlText(url: string) {
        return (url ?? '').replace('mailto:', '').trim();
    }

    getAppLink(linkToNavigate: NavigationLink): AppLink {
        return 'appLink' in linkToNavigate ? (linkToNavigate as LinkConfig).appLink : (linkToNavigate as AppLink);
    }

    getExtLink(linkToNavigate: NavigationLink): ExtLink {
        return 'extLink' in linkToNavigate ? (linkToNavigate as LinkConfig).extLink : (linkToNavigate as ExtLink);
    }

    isAppLink(linkToNavigate: NavigationLink): boolean {
        return 'appLink' in linkToNavigate || 'commands' in linkToNavigate ? true : false;
    }

    isCMSLink(extLink: ExtLink): boolean {
        return 'isCMSLink' in extLink ?? false;
    }

    isMailLink(extLink: ExtLink): boolean {
        return 'isMailLink' in extLink ?? false;
    }
    // #endregion
}
