import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe, DatePipe, DecimalPipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { IconSpriteModule } from 'ng-svg-icon-sprite';
import { AutocompleteOffDirective, I18nLinkifyDirective, PasswordInputDirective, SortElementsDirective } from './directives';
import {
    CapitalizePipe,
    MediaEmbedPipe,
    LastDigitsPipe,
    LastThreeDigitsPipe,
    LeadingZeroMonthPipe,
    NumberPipe,
    NumberTwoDigitsPipe,
    PriceCentsPipe,
    PriceNoRoundPipe,
    PricePipe,
    SanitizeHtmlPipe,
} from './pipes';
import { EffectsUtil } from './util';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { DateMaskDirective } from './directives/date-mask/date-mask.directive';
import { AmountMaskDirective } from './directives/amount-mask/amount-mask.directive';
import { BazaLinkUtilSharedService } from './services/baza-link-util-shared.service';
import { BazaWebUtilSharedService } from './services/baza-web-util-shared.service';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        AutocompleteOffDirective,
        LastDigitsPipe,
        LastThreeDigitsPipe,
        LeadingZeroMonthPipe,
        NumberPipe,
        PasswordInputDirective,
        PriceCentsPipe,
        PriceNoRoundPipe,
        PricePipe,
        SanitizeHtmlPipe,
        CapitalizePipe,
        NumberTwoDigitsPipe,
        DateMaskDirective,
        AmountMaskDirective,
        MediaEmbedPipe,
        SortElementsDirective,
        I18nLinkifyDirective,
    ],
    imports: [CommonModule, IconSpriteModule, NzNotificationModule, NzModalModule, RouterModule, TranslateModule],
    exports: [
        AutocompleteOffDirective,
        IconSpriteModule,
        LastDigitsPipe,
        LastThreeDigitsPipe,
        LeadingZeroMonthPipe,
        NumberPipe,
        PasswordInputDirective,
        PriceCentsPipe,
        PriceNoRoundPipe,
        PricePipe,
        SanitizeHtmlPipe,
        CapitalizePipe,
        NumberTwoDigitsPipe,
        DateMaskDirective,
        AmountMaskDirective,
        MediaEmbedPipe,
        SortElementsDirective,
        TranslateModule,
        I18nLinkifyDirective,
    ],
    providers: [
        CapitalizePipe,
        CurrencyPipe,
        DatePipe,
        DecimalPipe,
        EffectsUtil,
        LastDigitsPipe,
        LastThreeDigitsPipe,
        LeadingZeroMonthPipe,
        PriceCentsPipe,
        PriceNoRoundPipe,
        PricePipe,
        SanitizeHtmlPipe,
        NumberTwoDigitsPipe,
        MediaEmbedPipe,
        BazaLinkUtilSharedService,
        BazaWebUtilSharedService,
    ],
})
export class UtilModule {}
