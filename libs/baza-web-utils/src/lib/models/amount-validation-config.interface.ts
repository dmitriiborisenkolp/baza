/**
 * Enum for precision mode of the amount
 */
export type AmountPrecisionMode = 'cut' | 'toFixed';

/**
 * Interface for configuring amount validation
 */
export interface AmountValidationConfig {
    /**
     * The minimum allowed value for the amount
     */
    minValue: number;

    /**
     * The maximum allowed value for the amount
     */
    maxValue: number;

    /**
     * The step value for the amount
     */
    step: number;

    /**
     * The precision for the amount
     */
    precision: number;

    /**
     * The precision mode for the amount
     */
    precisionMode: AmountPrecisionMode;
}
