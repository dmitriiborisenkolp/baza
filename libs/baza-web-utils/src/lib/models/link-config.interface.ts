import { NavigationExtras } from '@angular/router';

/**
 * Base interface for link configuration
 */
interface BaseLink {
    /**
     * The text to be displayed for the link
     */
    text?: string;

    /**
     * The target attribute for the link
     */
    target?: string;
}

/**
 * Interface for a link that navigates to an internal route in the app
 */
export interface AppLink extends BaseLink {
    /**
     * The commands to be passed to the router for navigation
     */
    commands: unknown[];

    /**
     * Additional options for the navigation
     * @see {@link https://angular.io/api/router/NavigationExtras}
     */
    navExtras?: NavigationExtras;
}

/**
 * Interface for a link that navigates to an external url
 */
export interface ExtLink extends BaseLink {
    /**
     * The url to navigate to
     */
    link: string;

    /**
     * Indicates if the link is a CMS link
     */
    isCMSLink?: boolean;

    /**
     * Indicates if the link is a mail link
     */
    isMailLink?: boolean;
}

/**
 * Interface for a link configuration that can be either an AppLink or an ExtLink
 */
interface LinkConfiguration {
    /**
     * The AppLink configuration
     */
    appLink?: AppLink;

    /**
     * The ExtLink configuration
     */
    extLink?: ExtLink;
}

/**
 * Utility type that requires one of the keys of T to be present and required.
 */
type RequireOnlyOne<T, Keys extends keyof T = keyof T> = Pick<T, Exclude<keyof T, Keys>> &
    {
        [K in Keys]-?: Required<Pick<T, K>> & Partial<Record<Exclude<Keys, K>, undefined>>;
    }[Keys];

/**
 * Type that represents a LinkConfig, which can be either an AppLink or an ExtLink
 */
export type LinkConfig = RequireOnlyOne<LinkConfiguration>;

/**
 * Type that represents a NavigationLink, which can be a LinkConfig, AppLink or ExtLink
 */
export type NavigationLink = LinkConfig | AppLink | ExtLink;
export enum LinkTypeEnum {
    AppLink = 'appLink',
    ExtLink = 'extLink',
}
