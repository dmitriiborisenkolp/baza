export type RangedArray<
    T,
    Min extends number,
    Max extends number,
    A extends (T | undefined)[] = [],
    O extends boolean = false,
> = O extends false
    ? Min extends A['length']
        ? RangedArray<T, Min, Max, A, true>
        : RangedArray<T, Min, Max, [...A, T], false>
    : Max extends A['length']
    ? A
    : RangedArray<T, Min, Max, [...A, T?], false>;
