import { LinkConfig } from './link-config.interface';

/**
 * LinkPathConfig interface for configuration i18n links
 */
export interface LinkPathConfig {
    /**
     * i18n path
     */
    path?: string;
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    config?: LinkConfig;
}

/**
 * I18nLinkifyConfig interface for configuration I18nLinkifyDirective
 */
export interface I18nLinkifyConfig {
    /**
     * Link key
     */
    key: string;
    /**
     * Link node text content
     */
    text: string;
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    value: LinkConfig;
}
