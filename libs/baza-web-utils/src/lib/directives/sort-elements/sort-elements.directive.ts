import { AfterViewInit, Directive, ElementRef, Renderer2 } from '@angular/core';

/**
 * The SortElementsDirective sorts elements within the same parent element according to the value of the "order" attribute
 * of each element. The directive will try to sort all the immediate children.
 *
 * @example
 * <div appSortElements>
 *   <div data-order="3">Third</div>
 *   <div data-order="2">Second</div>
 *   <div data-order="1">First</div>
 * </div>
 * // Output:
 * <div appSortElements>
 *   <div data-order="1">First</div>
 *   <div data-order="2">Second</div>
 *   <div data-order="3">Third</div>
 * </div>
 */
@Directive({
    selector: '[appSortElements]',
})
export class SortElementsDirective implements AfterViewInit {
    private readonly attributeName = 'order';

    constructor(private readonly parent: ElementRef, private readonly renderer: Renderer2) {}

    ngAfterViewInit() {
        if (!this.parent) {
            throw new Error(
                `It wasn't possible to attach the directive appSortElements to a container element. Make sure you use a container element like DIV or SECTION`,
            );
        }

        const elements: HTMLElement[] = Array.from(this.parent.nativeElement?.children || []);

        if (elements.some((element) => !element.dataset[this.attributeName])) {
            throw new Error(`All elements to be sorted must contain the attribute "${this.attributeName}"`);
        }

        elements.forEach((element) => this.renderer.removeChild(this.parent, element));

        elements
            .sort((a, b) => {
                const aOrder = +a.dataset[this.attributeName];
                const bOrder = +b.dataset[this.attributeName];

                if (isNaN(aOrder) || isNaN(bOrder)) {
                    throw new Error(`Attribute value for "${this.attributeName}" must be a number`);
                }

                return aOrder - bOrder;
            })
            .forEach((element) => this.renderer.appendChild(element.parentElement, element));
    }
}
