import { AfterViewInit, Directive, ElementRef } from '@angular/core';

/**
 * The PasswordInputDirective is used to add a toggle button to a password input field.
 * When the button is clicked, the input's type will switch between 'password' and 'text',
 * allowing the user to view or hide the password they have entered.
 *
 * @example
 * <input type="password" appPasswordInput>
 */
@Directive({
    selector: '[appPasswordInput]',
})
export class PasswordInputDirective implements AfterViewInit {
    private isShown = false;

    constructor(private readonly element: ElementRef) {}

    ngAfterViewInit() {
        let wrapper = this.element.nativeElement.parentNode;

        // check if parent is input-group
        if (` ${wrapper.className} `.replace(/[\n\t]/g, ' ').indexOf(' ant-input-affix-wrapper ') === -1) {
            wrapper = document.createElement('div');
            wrapper.className = 'ant-input-affix-wrapper';

            if (` ${this.element.nativeElement.className} `.replace(/[\n\t]/g, ' ').indexOf(' ant-input-lg ') > -1) {
                wrapper.className = 'ant-input-affix-wrapper ant-input-affix-wrapper-lg';
            }

            this.element.nativeElement.parentNode.insertBefore(wrapper, this.element.nativeElement);
            wrapper.appendChild(this.element.nativeElement);
        }

        // add button
        const span = document.createElement('a');
        span.className = 'ant-input-suffix';
        span.innerHTML = `<i class="icon icon-eye"></i>`;

        span.addEventListener('click', () => {
            this.onPasswordToggle(span);
        });

        wrapper.appendChild(span);
    }

    // onPasswordToggle
    onPasswordToggle(span: HTMLElement) {
        if (this.isShown) {
            this.element.nativeElement.setAttribute('type', 'password');
            span.innerHTML = `<i class="icon icon-eye"></i>`;
        } else {
            this.element.nativeElement.setAttribute('type', 'text');
            span.innerHTML = `<i class="icon icon-eye-invisible"></i>`;
        }

        this.isShown = !this.isShown;
    }
}
