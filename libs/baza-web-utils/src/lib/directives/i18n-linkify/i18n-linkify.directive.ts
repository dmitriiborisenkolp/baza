import { AfterViewInit, Directive, ElementRef, Input, OnDestroy, Renderer2 } from '@angular/core';
import { I18nLinkifyConfig } from '../../models/i18n-links-interface';
import { BazaLinkUtilSharedService } from '../../services';

@Directive({
    selector: '[appLinkify]',
})
export class I18nLinkifyDirective implements AfterViewInit, OnDestroy {
    @Input()
    links: Array<I18nLinkifyConfig>;

    // Reference article: https://medium.com/ngconf/reapplying-an-angular-directive-on-dom-changes-6cce7c390896
    private observer = new MutationObserver(() => this.handleDynamicLinks());
    private processing = false;

    constructor(
        private readonly renderer: Renderer2,
        private readonly element: ElementRef<HTMLElement>,
        private readonly uts: BazaLinkUtilSharedService,
    ) {}

    ngAfterViewInit() {
        this.handleDynamicLinks();
        this.registerListenerForDomChanges();
    }

    ngOnDestroy() {
        this.observer.disconnect();
    }

    handleDynamicLinks() {
        if (!this.processing) {
            this.processing = true;

            const thisEl = this.element.nativeElement;

            this.links.forEach((linkEl: I18nLinkifyConfig) => {
                const linkNode = thisEl.querySelector('[data-link="' + linkEl.key + '"]');

                if (linkNode) {
                    const newLinkNode = this.renderer.createElement('a') as HTMLAnchorElement;
                    newLinkNode.textContent = linkEl.text;

                    newLinkNode.addEventListener('click', () => {
                        this.uts.navigate(linkEl.value);
                    });

                    thisEl.replaceChild(newLinkNode, linkNode);
                }
            });

            this.processing = false;
        }
    }

    private registerListenerForDomChanges() {
        const attributes = false;
        const childList = true;
        const subtree = true;
        this.observer.observe(this.element.nativeElement, { attributes, childList, subtree });
    }
}
