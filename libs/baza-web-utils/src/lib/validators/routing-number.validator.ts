import { AbstractControl, ValidationErrors } from '@angular/forms';

export const routingNumberValidator = (): ((AbstractControl) => ValidationErrors | null) => {
    return (control: AbstractControl): ValidationErrors | null => {
        const targetValue = control.value;

        if (!targetValue) {
            return null;
        }

        const digits = targetValue.split('').map((d) => parseInt(d, 10));

        /*
            formula: 3(d[1]+d[4]+d[7])+7(d[2]+d[5]+d[8])+(d[3]+d[6]+d[9]) mod 10=0
            reference: https://en.wikipedia.org/wiki/ABA_routing_transit_number#Check_digit
        */
        const checksum =
            3 * (digits[0] + digits[3] + digits[6]) + 7 * (digits[1] + digits[4] + digits[7]) + (digits[2] + digits[5] + digits[8]);

        return checksum % 10 === 0 ? null : { routingNumber: true };
    };
};
