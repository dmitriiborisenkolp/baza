import { AbstractControl, ValidationErrors } from '@angular/forms';

export const specialSymbolValidator = (): ((AbstractControl) => ValidationErrors | null) => {
    return (control: AbstractControl): ValidationErrors | null => {
        const targetValue = control.value;

        if (!targetValue) {
            return null;
        }

        const regex = RegExp(/\W/gm);

        return regex.test(targetValue) ? null : { specialsymbol: true };
    };
};
