import { AbstractControl, ValidationErrors } from '@angular/forms';

export const uppercaseValidator = (): ((AbstractControl) => ValidationErrors | null) => {
    return (control: AbstractControl): ValidationErrors | null => {
        const targetValue = control.value;

        if (!targetValue) {
            return null;
        }

        const regex = RegExp(/[A-Z]/gm);

        return regex.test(targetValue) ? null : { uppercase: true };
    };
};
