import { AbstractControl, ValidationErrors } from '@angular/forms';

export const whitespaceValidator = (): ((AbstractControl) => ValidationErrors | null) => {
    return (control: AbstractControl): ValidationErrors | null => {
        const targetValue = control.value;

        if (!targetValue) {
            return null;
        }
        const isWhitespace = (control.value || '').trim().length === 0;
        return !isWhitespace ? null : { required: true };
    };
};
