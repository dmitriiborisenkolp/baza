import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function customDefaultValidator(defaultValue): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const forbidden = control.value === defaultValue;
        return forbidden ? { required: true } : null;
    };
}
