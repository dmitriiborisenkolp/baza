import { NCVFFileUploadEnI18n } from '../file-upload/i18n/vf-file-upload-i18n';
import { NCVFInfoEnI18n } from '../info/i18n/vf-info.i18n';
import { NCVFInvestorEnI18n } from '../investor/i18n/vf-investor.i18n';
import { NCVFEnI18n } from './vf.18n';

export const bazaNCWebVFEnI18n = {
    ncvf: {
        vf: NCVFEnI18n,
        info: NCVFInfoEnI18n,
        investor: NCVFInvestorEnI18n,
        fileUpload: NCVFFileUploadEnI18n,
    },
};
