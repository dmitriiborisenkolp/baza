import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { Observable, of } from 'rxjs';
import { VerificationConfig } from './data-access';

@UntilDestroy()
@Component({
    selector: 'app-nc-verification',
    templateUrl: './verification.component.html',
    styleUrls: ['./verification.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcVerificationComponent implements OnInit {
    @Input()
    config?: VerificationConfig;

    cart$?: Observable<BazaNcIntegrationListingsDto>;
    currentTab: number;
    showCustomVerificationInfo = false;
    item: BazaNcIntegrationListingsDto;
    i18nBasePath = 'ncvf.vf';

    constructor(private readonly route: ActivatedRoute, public readonly wts: BazaWebUtilSharedService) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
            this.cart$ = of(params.purchase);
        });
    }

    ngOnInit(): void {
        this.cart$?.pipe(untilDestroyed(this)).subscribe((response) => {
            this.item = response;
            this.checkDefaultVerificationConfig();
        });
    }

    checkDefaultVerificationConfig() {
        const defaultConfig: VerificationConfig = {
            backLink: {
                appLink: {
                    commands: ['/items', this.item?.id?.toString()],
                    text: 'Back to Listing Details',
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }
}
