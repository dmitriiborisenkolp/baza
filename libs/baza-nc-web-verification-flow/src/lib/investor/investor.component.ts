import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService, i18nValidationTypesEnum, ValidationsPreset } from '@scaliolabs/baza-web-utils';
import { take } from 'rxjs/operators';
import { ApplyInvestorProfile, InvestorProfile, VerificationInvestorForm, VerificationState } from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-nc-verification-investor',
    templateUrl: './investor.component.html',
    styleUrls: ['./investor.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcVerificationInvestorComponent implements OnInit {
    verification$ = this.store.select(VerificationState.verification);

    public fileUploading: boolean;
    public verificationInvestorForm: VerificationInvestorForm;

    i18nBasePath = 'ncvf.investor';
    i18nFormPath = `${this.i18nBasePath}.form`;
    i18nFormFieldsPath = `${this.i18nBasePath}.form.fields`;

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
        public readonly wts: BazaWebUtilSharedService,
    ) {
        this.onFormReset();
    }

    public ngOnInit(): void {
        this.verification$.pipe(untilDestroyed(this)).subscribe((response) => {
            // check if we need to request additional docs
            this.fileUploading = response && response.currentStep === AccountVerificationStep.RequestDocuments ? true : false;

            if (response && response.investorProfile) {
                this.verificationInvestorForm.patchValue(response.investorProfile as InvestorProfile);
            }

            this.cdr.detectChanges();
        });
    }

    // onFormReset
    public onFormReset(): void {
        this.verificationInvestorForm = this.fb.group({
            isAccreditedInvestor: ['', Validators.required],
            isAssociatedWithFINRA: ['', Validators.required],
            currentAnnualHouseholdIncome: [0, Validators.required],
            netWorth: [0, Validators.required],
        }) as VerificationInvestorForm;
    }

    // onFormSubmit
    public onFormSubmit(investorFormEl: HTMLFormElement): void {
        if (this.bazaFormValidatorService.isFormValid(this.verificationInvestorForm, investorFormEl)) {
            const data: InvestorProfile = {
                ...this.verificationInvestorForm.value,
            };

            this.store
                .dispatch(new ApplyInvestorProfile(data))
                .pipe(take(1))
                .subscribe(() => {
                    this.router.navigate(['/buy-shares']);
                });
        }
    }

    public getErrorMessage(control: FormControl, controlName: string): string {
        const validationsPreset: ValidationsPreset = new Map([
            ['isAccreditedInvestor', [{ key: i18nValidationTypesEnum.required }]],
            ['isAssociatedWithFINRA', [{ key: i18nValidationTypesEnum.required }]],
            ['currentAnnualHouseholdIncome', [{ key: i18nValidationTypesEnum.required }]],
            ['netWorth', [{ key: i18nValidationTypesEnum.required }]],
        ]);

        return this.wts.geti18nValidationErrorMessages({
            control,
            controlName,
            i18nFormFieldsPath: this.i18nFormFieldsPath,
            i18nGenericValidationsPath: `${this.i18nFormPath}.genericValidators`,
            validationsPreset,
        });
    }
}
