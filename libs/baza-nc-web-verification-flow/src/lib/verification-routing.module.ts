import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormResourcesResolver, VerificationResolver } from './data-access';
import { NcVerificationComponent } from './verification.component';

export const getConfigurableRoutes: (component) => Routes = (component) => [
    {
        path: '',
        data: {
            layout: 'mini',
        },
        resolve: {
            verification: VerificationResolver,
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'info',
            },
            {
                path: 'info',
                component: component,
                resolve: {
                    formResources: FormResourcesResolver,
                },
                data: {
                    tab: '0',
                    layout: 'mini',
                    meta: {
                        title: 'Personal Information',
                    },
                },
            },
            {
                path: 'investor',
                component: component,
                data: {
                    tab: '1',
                    layout: 'mini',
                    meta: {
                        title: 'Investor Profile',
                    },
                },
            },
        ],
    },
];

const routes: Routes = getConfigurableRoutes(NcVerificationComponent);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class VerificationRoutingModule {}
