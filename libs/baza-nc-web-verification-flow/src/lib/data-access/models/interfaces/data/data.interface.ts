import {
    AccountVerificationDto,
    AccountVerificationFormResourcesDto,
    AccountVerificationInvestorProfileDto,
    AccountVerificationPersonalInformationDto,
} from '@scaliolabs/baza-nc-shared';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface FormResources extends AccountVerificationFormResourcesDto {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface InvestorProfile extends AccountVerificationInvestorProfileDto {}

export interface PersonalInformation extends AccountVerificationPersonalInformationDto {
    hasntSsn?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Verification extends AccountVerificationDto {}

export interface UploadedSSN {
    ssnDocumentId: string;
    ssnDocumentFileName: string;
    ssnDocumentUrl: string;
}
