import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { FormResources } from '../models';
import { LoadFormResources, VerificationState } from '../store';

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class FormResourcesResolver implements Resolve<FormResources> {
    constructor(private readonly store: Store) {}

    resolve(): Observable<FormResources> {
        const result$ = this.store.select(VerificationState.formResources).pipe(
            untilDestroyed(this),
            take(1),
            switchMap((response) => {
                if (response === null) {
                    return this.store.dispatch(new LoadFormResources()).pipe(
                        switchMap(() => {
                            return of(this.store.selectSnapshot(VerificationState.formResources));
                        }),
                    );
                } else {
                    return of(response);
                }
            }),
        );

        return result$.pipe(
            map((response) => {
                return response;
            }),
        );
    }
}
