import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { StorageService } from '@scaliolabs/baza-web-utils';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { Verification } from '../models';
import { LoadVerification, VerificationState } from '../store';

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class VerificationResolver implements Resolve<Verification> {
    constructor(private readonly store: Store, private readonly storageService: StorageService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Verification> {
        const entityId = route.params?.id ?? null;

        entityId && this.storageService.setObject('cart', entityId);

        const result$ = this.store.select(VerificationState.verification).pipe(
            untilDestroyed(this),
            take(1),
            switchMap((response) => {
                if (!response) {
                    return this.store.dispatch(new LoadVerification()).pipe(
                        switchMap(() => {
                            return of(this.store.selectSnapshot(VerificationState.verification));
                        }),
                    );
                } else {
                    return of(response);
                }
            }),
        );

        return result$.pipe(
            map((response) => {
                return response;
            }),
        );
    }
}
