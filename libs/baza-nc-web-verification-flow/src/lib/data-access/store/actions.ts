import { NzUploadFile } from 'ng-zorro-antd/upload';
import { InvestorProfile, PersonalInformation } from '../models';

export class ApplyPersonalInfo {
    static readonly type = '[Verification] ApplyPersonalInfo';

    constructor(public data: PersonalInformation, public files: NzUploadFile[]) {}
}

export class ApplyInvestorProfile {
    static readonly type = '[Verification] ApplyInvestorProfile';

    constructor(public data: InvestorProfile) {}
}

export class ClearVerificationState {
    static readonly type = '[Verification] ClearVerificationState';
}

export class GetListStates {
    static readonly type = '[Verification] GetListStates';

    constructor(public country: string) {}
}

export class IsCompleted {
    static readonly type = '[Verification] IsCompleted';
}

export class LoadFormResources {
    static readonly type = '[Verification] LoadFormResources';
}

export class LoadVerification {
    static readonly type = '[Verification] LoadVerification';
}

export class UploadDocument {
    static readonly type = '[Verification] UploadDocument';

    constructor(public file: NzUploadFile) {}
}
