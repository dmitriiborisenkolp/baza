import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { VerificationState } from './state';
import { BazaNcDataAccessModule } from '@scaliolabs/baza-nc-data-access';

@NgModule({
    imports: [NgxsModule.forFeature([VerificationState]), BazaNcDataAccessModule],
})
export class VerificationStateModule {}
