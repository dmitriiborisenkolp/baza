/* eslint-disable no-useless-escape */
import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { BazaNcAccountVerificationDataAccess } from '@scaliolabs/baza-nc-data-access';
import { AccountVerificationDto, AccountVerificationIsCompletedDto, ListStatesResponseDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService, EffectsUtil } from '@scaliolabs/baza-web-utils';
import { empty, Observable } from 'rxjs';
import { concat, tap } from 'rxjs/operators';

import { TranslateService } from '@ngx-translate/core';
import { FormResources, PersonalInformation, Verification } from '../models';
import {
    ApplyInvestorProfile,
    ApplyPersonalInfo,
    ClearVerificationState,
    GetListStates,
    IsCompleted,
    LoadFormResources,
    LoadVerification,
    UploadDocument,
} from './actions';

export interface VerificationStateModel {
    formResources: FormResources;
    listStates: ListStatesResponseDto;
    isCompleted: boolean;
    verification: Verification | null;
}

const initState = {
    name: 'bncverification',
    defaults: {
        formResources: null,
        listStates: null,
        isCompleted: false,
        verification: null,
    },
};

const i18nBasePath = 'ncvf.vf.notifications';

@State<VerificationStateModel>(initState)
@Injectable()
export class VerificationState {
    constructor(
        private readonly dataAccess: BazaNcAccountVerificationDataAccess,
        private readonly datePipe: DatePipe,
        private readonly effectsUtil: EffectsUtil,
        private readonly ts: TranslateService,
        private readonly wts: BazaWebUtilSharedService,
    ) {}

    @Selector()
    static formResources(state: VerificationStateModel): FormResources {
        return state.formResources;
    }

    @Selector()
    static listStates(state: VerificationStateModel): ListStatesResponseDto {
        return state.listStates;
    }

    @Selector()
    static isCompleted(state: VerificationStateModel): boolean {
        return state.isCompleted;
    }

    @Selector()
    static personal(state: VerificationStateModel): PersonalInformation {
        return state.verification?.personalInformation;
    }

    @Selector()
    static verification(state: VerificationStateModel): Verification {
        return state.verification;
    }

    @Action(ApplyPersonalInfo, { cancelUncompleted: true })
    applyPersonalInfo(ctx: StateContext<VerificationStateModel>, action: ApplyPersonalInfo): Observable<AccountVerificationDto> {
        const uploadRequest =
            !action.data.hasSsn && action.files && action.files.length > 0
                ? this.dataAccess.uploadPersonalInformationSSNDocument(action.files[0] as unknown as File)
                : empty();

        // transform date into NC date format
        const info = {
            ...action.data,
            dateOfBirth: this.datePipe.transform(action.data.dateOfBirth, 'MM-dd-yyyy'),
        };

        return this.dataAccess.applyPersonalInformation(info).pipe(
            concat(uploadRequest),
            tap((response: Verification) => {
                return ctx.patchState({ verification: this.parseDateFromNc(response) });
            }),
            this.effectsUtil.tryCatch$(this.wts.getI18nLabel(i18nBasePath, 'apply_personal_info_success'), ''),
        );
    }

    @Action(ApplyInvestorProfile, { cancelUncompleted: true })
    applyInvestorProfile(ctx: StateContext<VerificationStateModel>, action: ApplyInvestorProfile): Observable<AccountVerificationDto> {
        return this.dataAccess.applyInvestorProfile(action.data).pipe(
            tap((response: Verification) => {
                return ctx.patchState({ verification: this.parseDateFromNc(response) });
            }),
            this.effectsUtil.tryCatch$(this.wts.getI18nLabel(i18nBasePath, 'apply_investor_profile_success'), ''),
        );
    }

    @Action(ClearVerificationState, { cancelUncompleted: true })
    clearVerificationState(ctx: StateContext<VerificationStateModel>): void {
        ctx.setState(initState.defaults);
    }

    @Action(GetListStates, { cancelUncompleted: true })
    getListStates(ctx: StateContext<VerificationStateModel>, action: GetListStates): Observable<ListStatesResponseDto> {
        return this.dataAccess.listStates({ country: action.country }).pipe(
            tap((response: ListStatesResponseDto) => {
                return ctx.patchState({ listStates: response });
            }),
        );
    }

    @Action(IsCompleted, { cancelUncompleted: true })
    isCompleted(ctx: StateContext<VerificationStateModel>): Observable<AccountVerificationIsCompletedDto> {
        return this.dataAccess.isCompleted().pipe(
            tap((response) => {
                return ctx.patchState({ isCompleted: response.isCompleted });
            }),
        );
    }

    @Action(LoadFormResources, { cancelUncompleted: true })
    loadFormResources(ctx: StateContext<VerificationStateModel>): Observable<FormResources> {
        return this.dataAccess.formResources().pipe(
            tap((response) => {
                return ctx.patchState({ formResources: response });
            }),
        );
    }

    @Action(LoadVerification, { cancelUncompleted: true })
    loadVerification(ctx: StateContext<VerificationStateModel>): Observable<Verification> {
        return this.dataAccess.index().pipe(
            tap((response: Verification) => {
                return ctx.patchState({ verification: this.parseDateFromNc(response) });
            }),
            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_verification_fail')),
        );
    }

    @Action(UploadDocument, { cancelUncompleted: true })
    uploadDocument(ctx: StateContext<VerificationStateModel>, action: UploadDocument): Observable<AccountVerificationDto> {
        return this.dataAccess.uploadPersonalInformationDocument(action.file as unknown as File).pipe(
            tap((response: Verification) => {
                return ctx.patchState({ verification: this.parseDateFromNc(response) });
            }),
            this.effectsUtil.tryCatch$(
                this.wts.getI18nLabel(i18nBasePath, 'upload_doc_success'),
                this.wts.getI18nLabel(i18nBasePath, 'upload_doc_fail'),
            ),
        );
    }

    // parse Date from NC format
    private parseDateFromNc(verification: Verification): Verification {
        const data = Object.assign({}, verification);

        if (data && data.personalInformation && data.personalInformation.dateOfBirth) {
            data.personalInformation = {
                ...data.personalInformation,
                dateOfBirth: data.personalInformation.dateOfBirth.replace(/\-/g, '/'),
            };
        }

        return data;
    }
}
