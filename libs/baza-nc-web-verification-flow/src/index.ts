// Baza-NC-WEB-Verification-Flow Exports.

export * from './lib/baza-nc-web-verification-flow.module';
export * from './lib/data-access';
export * from './lib/verification-routing.module';
export * from './lib/verification.component';
export * from './lib/file-upload/file-upload.component';
export * from './lib/info/info.component';
export * from './lib/investor/investor.component';
export * from './lib/i18n';
