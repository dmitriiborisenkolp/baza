import { DynamicModule, Global, Module } from '@nestjs/common';
import { BazaEnvModule, BazaLoggerModule } from '@scaliolabs/baza-core-api';
import { DwollaDataAccessService } from './services/dwolla-data-access.service';
import {
    DwollaConfigurationNestjsService,
    DwollaConfigurationNestjsServiceConfiguration,
} from './services/dwolla-configuration.nestjs-service';
import { DwollaCustomersApiNestjsService } from './services-dwolla/dwolla-customers-api.nestjs-service';
import { DwollaDocumentsApiNestjsService } from './services-dwolla/dwolla-documents-api.nestjs-service';
import { DwollaAccountsApiNestjsService } from './services-dwolla/dwolla-accounts-api.nestjs-service';
import { DwollaFundingSourcesApiNestjsService } from './services-dwolla/dwolla-funding-sources-api.nestjs-service';
import { DwollaBeneficialOwnersApiNestjsService } from './services-dwolla/dwolla-beneficial-owners-api.nestjs-service';
import { DwollaWebhookSubscriptionsApiNestjsService } from './services-dwolla/dwolla-webhook-subscriptions-api.nestjs-service';
import { DwollaMassPaymentsApiNestjsService } from './services-dwolla/dwolla-mass-payments-api.nestjs-service';
import { DwollaWebhooksApiNestjsService } from './services-dwolla/dwolla-webhooks-api.nestjs-service';
import { DwollaKbaApiNestjsService } from './services-dwolla/dwolla-kba-api.nestjs-service';
import { DwollaTransfersApiNestjsService } from './services-dwolla/dwolla-transfers-api.nestjs-service';
import { DwollaLabelsApiNestjsService } from './services-dwolla/dwolla-labels-api.nestjs-service';
import { DwollaEventsApiNestjsService } from './services-dwolla/dwolla-events-api.nestjs-service';
import { DwollaRootApiNestjsService } from './services-dwolla/dwolla-root-api.nestjs-service';
import { DwollaWebhookSandboxSimulationApiNestjsService } from './services-dwolla/dwolla-webhook-sandbox-simulation-api.nestjs-service';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<DwollaConfigurationNestjsServiceConfiguration>;
}

const apis = [
    DwollaDataAccessService,
    DwollaRootApiNestjsService,
    DwollaAccountsApiNestjsService,
    DwollaCustomersApiNestjsService,
    DwollaFundingSourcesApiNestjsService,
    DwollaTransfersApiNestjsService,
    DwollaDocumentsApiNestjsService,
    DwollaMassPaymentsApiNestjsService,
    DwollaKbaApiNestjsService,
    DwollaLabelsApiNestjsService,
    DwollaEventsApiNestjsService,
    DwollaBeneficialOwnersApiNestjsService,
    DwollaWebhooksApiNestjsService,
    DwollaWebhookSubscriptionsApiNestjsService,
    DwollaWebhookSandboxSimulationApiNestjsService,
];

@Module({})
export class BazaDwollaApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaDwollaApiGlobalModule,
            providers: [
                {
                    provide: DwollaConfigurationNestjsService,
                    inject: options.injects,
                    useFactory: async (...injects) => {
                        return new DwollaConfigurationNestjsService(await options.useFactory(...injects));
                    },
                },
            ],
            exports: [DwollaConfigurationNestjsService],
        };
    }
}

@Global()
@Module({
    imports: [BazaLoggerModule, BazaEnvModule],
    providers: [...apis],
    exports: [...apis],
})
class BazaDwollaApiGlobalModule {}
