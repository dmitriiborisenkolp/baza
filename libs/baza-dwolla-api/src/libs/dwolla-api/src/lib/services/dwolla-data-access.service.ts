import { Injectable } from '@nestjs/common';
import { Client, Response } from 'dwolla-v2';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import {
    BazaDwollaErrorCodes,
    DwollaEnvironment,
    DwollaErrorResponse,
    isDwollaError,
    isDwollaValidationError,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaFailedToEstablishConnectionException } from '../exception/baza-dwolla-failed-to-establish-connection.exception';
import { BazaDwollaFailedToParseResponseException } from '../exception/baza-dwolla-failed-to-parse-response.exception';
import { BazaDwollaApiErrorException } from '../exception/baza-dwolla-api-error.exception';
import { DwollaConfigurationNestjsService } from './dwolla-configuration.nestjs-service';

@Injectable()
export class DwollaDataAccessService {
    private _dwollaClient: Client;

    constructor(private readonly config: DwollaConfigurationNestjsService, private readonly logger: BazaLogger) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Creates Dwolla Instane on Application Bootstrap
     */
    init(): void {
        this._dwollaClient = this.getDwollaInstance();
    }

    /**
     * Initialize and returns Dwolla Instance
     */
    getDwollaInstance(): Client {
        if (this._dwollaClient) {
            return this._dwollaClient;
        }

        try {
            this._dwollaClient = new Client({
                key: this.config.configuration.credentials.apiKey,
                secret: this.config.configuration.credentials.apiSecret,
                environment: this.config.configuration.isSandbox ? DwollaEnvironment.Sandbox : DwollaEnvironment.Production,
            });

            return this._dwollaClient;
        } catch (error) {
            this.logger.error('Failed to create Dwolla Instance:');
            this.logger.error(error);

            throw new BazaDwollaFailedToEstablishConnectionException();
        }
    }

    /**
     * Process Promise from Dwolla
     */
    processDwollaPromise<T>(
        promise: Promise<Response>,
        options = {
            returnBody: true,
        },
    ): Promise<T> {
        return this.processDwollaPromiseReject(promise).then((result) => {
            if (result && result.status >= 200 && result.status <= 299) {
                if (this.config.configuration.debug) {
                    this.logger.log(`Dwolla Response:`);
                    this.logger.log(result.body);
                }

                return options.returnBody ? result.body : result;
            } else {
                throw new BazaDwollaFailedToParseResponseException();
            }
        });
    }

    /**
     * Process Errors from Dwolla
     * @param promise
     */
    processDwollaPromiseReject(promise: Promise<Response>): Promise<Response> {
        return promise.catch((exception) => {
            const error: DwollaErrorResponse =
                !!exception && typeof exception === 'object' && !!exception.body
                    ? exception.body
                    : {
                          code: BazaDwollaErrorCodes.BazaDwollaApiError,
                          message: 'Dwolla API Exception',
                      };

            if (this.config.configuration.debug) {
                this.logger.error(`Dwolla Error:`, DwollaDataAccessService.name);
                this.logger.error(JSON.stringify(error), DwollaDataAccessService.name);
            }

            if (isDwollaValidationError(error)) {
                throw new BazaDwollaApiErrorException(BazaDwollaErrorCodes.BazaDwollaApiValidationError, error._embedded.errors[0].message);
            } else if (isDwollaError(error)) {
                throw new BazaDwollaApiErrorException(error.code, error.message, error._embedded, error._links);
            } else {
                throw new BazaDwollaApiErrorException(BazaDwollaErrorCodes.BazaDwollaApiError, 'Dwolla API Exception');
            }
        });
    }
}
