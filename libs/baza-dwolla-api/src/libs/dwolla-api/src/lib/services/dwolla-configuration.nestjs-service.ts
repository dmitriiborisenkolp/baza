import { Injectable } from '@nestjs/common';

interface Configuration {
    credentials: {
        apiKey: string;
        apiSecret: string;
    };
    isSandbox: boolean;
    debug: boolean;
    webhooks: {
        debug: boolean;
        secret: string;
        kafkaPipe: {
            read: boolean;
            send: boolean;
        };
    };
    baseUrl: string;
}

export { Configuration as DwollaConfigurationNestjsServiceConfiguration };

@Injectable()
export class DwollaConfigurationNestjsService {
    constructor(private readonly _configuration: Configuration) {}

    get configuration(): Configuration {
        return {
            ...this._configuration,
        };
    }

    get isAvailable(): boolean {
        return !!this._configuration && !!this._configuration.credentials.apiKey && !!this._configuration.credentials.apiSecret;
    }
}
