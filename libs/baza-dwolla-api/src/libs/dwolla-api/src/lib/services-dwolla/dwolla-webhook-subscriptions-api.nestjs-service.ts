import { Injectable } from '@nestjs/common';
import {
    CreateWebhookSubscriptionRequest,
    DwollaWebhookSubscriptionsApi,
    DwollaWebhookSubscriptionsPaths,
    GetWebhookListForWebhookSubscriptionRequest,
    UpdateWebhookSubscriptionRequest,
} from '@scaliolabs/baza-dwolla-shared';
import * as qs from 'querystring';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaWebhookSubscriptionsApiNestjsService implements DwollaWebhookSubscriptionsApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async createWebhookSubscription(request: CreateWebhookSubscriptionRequest): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaWebhookSubscriptionsPaths.WebhookSubscriptions}`, request),
        );
    }

    async getWebhookSubscription(webhookSubscriptionId: string): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .get(`${DwollaWebhookSubscriptionsPaths.WebhookSubscriptions}/${webhookSubscriptionId}`),
        );
    }

    async updateWebhookSubscription(webhookSubscriptionId: string, request: UpdateWebhookSubscriptionRequest): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .post(`${DwollaWebhookSubscriptionsPaths.WebhookSubscriptions}/${webhookSubscriptionId}`, request),
        );
    }

    async getWebhookSubscriptionList(): Promise<unknown[]> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaWebhookSubscriptionsPaths.WebhookSubscriptions}`),
        );
    }

    async deleteWebhookSubscription(webhookSubscriptionId: string): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .delete(`${DwollaWebhookSubscriptionsPaths.WebhookSubscriptions}/${webhookSubscriptionId}`),
        );
    }

    async getWebhookListForWebhookSubscription(
        webhookSubscriptionId: string,
        request?: GetWebhookListForWebhookSubscriptionRequest,
    ): Promise<unknown[]> {
        let url = `${DwollaWebhookSubscriptionsPaths.WebhookSubscriptions}/${webhookSubscriptionId}/${DwollaWebhookSubscriptionsPaths.Webhooks}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }
}
