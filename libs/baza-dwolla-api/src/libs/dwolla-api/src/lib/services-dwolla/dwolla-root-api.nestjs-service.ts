import { Injectable } from '@nestjs/common';
import { DwollaRootApi, DwollaRootResponse } from '@scaliolabs/baza-dwolla-shared';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaRootApiNestjsService implements DwollaRootApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async root(): Promise<DwollaRootResponse> {
        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get('/'));
    }
}
