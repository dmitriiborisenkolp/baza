import { Injectable } from '@nestjs/common';
import {
    CreateFundingSourceForAccountRequest,
    DwollaAccountsApi,
    DwollaAccountsPaths,
    GetAccountDetailsRequest,
    GetAccountDetailsResponse,
    GetFundingSourcesForAccountRequest,
    GetFundingSourceListResponse,
    GetMassPaymentsForAccountRequest,
    GetMassPaymentsForAccountResponse,
    GetTransferListForAccountRequest,
    GetTransferListResponse,
    DwollaAccountId,
} from '@scaliolabs/baza-dwolla-shared';
import * as qs from 'querystring';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaAccountsApiNestjsService implements DwollaAccountsApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async getAccountDetails(request: GetAccountDetailsRequest): Promise<GetAccountDetailsResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaAccountsPaths.GetAccountDetails}/${request.accountId}`),
        );
    }

    async getFundingSourcesForAccount(
        accountId: DwollaAccountId,
        request: GetFundingSourcesForAccountRequest,
    ): Promise<GetFundingSourceListResponse> {
        let url = `${DwollaAccountsPaths.GetAccountDetails}/${accountId}/${DwollaAccountsPaths.GetFundingSourcesForAccount}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async getMassPaymentsForAccount(
        accountId: DwollaAccountId,
        request: GetMassPaymentsForAccountRequest,
    ): Promise<GetMassPaymentsForAccountResponse> {
        let url = `${DwollaAccountsPaths.GetAccountDetails}/${accountId}/${DwollaAccountsPaths.GetMassPaymentsForAccount}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async getTransferListForAccount(
        accountId: DwollaAccountId,
        request: GetTransferListForAccountRequest,
    ): Promise<GetTransferListResponse> {
        let url = `${DwollaAccountsPaths.GetAccountDetails}/${accountId}/${DwollaAccountsPaths.GetTransferListForAccount}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async createFundingSourcesForMasterDwollaAccount(request: CreateFundingSourceForAccountRequest): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaAccountsPaths.CreateFundingSourcesForMasterDwollaAccount}`, request),
        );
    }
}
