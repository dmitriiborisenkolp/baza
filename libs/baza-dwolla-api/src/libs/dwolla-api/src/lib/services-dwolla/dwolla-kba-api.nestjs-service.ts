import { Injectable } from '@nestjs/common';
import { DwollaKbaApi, DwollaKbaPaths, VerifyKbaQuestionRequest } from '@scaliolabs/baza-dwolla-shared';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaKbaApiNestjsService implements DwollaKbaApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async initiateKbaSession(customerId: string): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaKbaPaths.Customers}/${customerId}/${DwollaKbaPaths.Kba}`),
        );
    }

    async getKbaQuestions(kbaSessionId: string): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaKbaPaths.Kba}/${kbaSessionId}`),
        );
    }

    async verifyKbaQuestion(kbaSessionId: string, request: VerifyKbaQuestionRequest): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaKbaPaths.Kba}/${kbaSessionId}`, request),
        );
    }
}
