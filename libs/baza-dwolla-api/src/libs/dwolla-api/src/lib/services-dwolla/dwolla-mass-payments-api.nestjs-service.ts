import { Injectable } from '@nestjs/common';
import {
    CreateMassPaymentRequest,
    DwollaMassPaymentsApi,
    DwollaMassPaymentsPaths,
    GetItemListForMassPaymentRequest,
    GetItemListForMassPaymentResponse,
    GetMassPaymentListForCustomerRequest,
    GetMassPaymentListForCustomerResponse,
    DwollaMassPaymentDetail,
    TransferDetails,
    UpdateMassPaymentRequest,
} from '@scaliolabs/baza-dwolla-shared';
import * as qs from 'querystring';
import { ulid } from 'ulid';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaMassPaymentsApiNestjsService implements DwollaMassPaymentsApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async initiateMassPayment(request: CreateMassPaymentRequest): Promise<void> {
        const header = {
            'Idempotency-Key': ulid(),
        };

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaMassPaymentsPaths.MassPayments}`, request, header),
        );
    }

    async getMassPayment(massPaymentId: string): Promise<DwollaMassPaymentDetail> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaMassPaymentsPaths.MassPayments}/${massPaymentId}`),
        );
    }

    async updateMassPayment(massPaymentId: string, request: UpdateMassPaymentRequest): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaMassPaymentsPaths.MassPayments}/${massPaymentId}`, request),
        );
    }

    async getItemListForMassPayment(
        massPaymentId: string,
        request?: GetItemListForMassPaymentRequest,
    ): Promise<GetItemListForMassPaymentResponse> {
        let url = `${DwollaMassPaymentsPaths.MassPayments}/${massPaymentId}/${DwollaMassPaymentsPaths.Items}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async getMassPaymentItem(massPaymentItemId: string): Promise<TransferDetails> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaMassPaymentsPaths.MassPaymentItems}/${massPaymentItemId}`),
        );
    }

    async getMassPaymentListForCustomer(
        customerId: string,
        request?: GetMassPaymentListForCustomerRequest,
    ): Promise<GetMassPaymentListForCustomerResponse> {
        let url = `${DwollaMassPaymentsPaths.Customers}/${customerId}/${DwollaMassPaymentsPaths.MassPayments}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }
}
