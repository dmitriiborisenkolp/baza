import { Injectable } from '@nestjs/common';
import {
    DwollaCustomerId,
    DwollaCustomersApi,
    DwollaCustomerType,
    DwollaCustomersPaths,
    DwollaCustomerStatus,
    CreateBusinessCustomerRequest,
    CreatePersonalCustomerRequest,
    CreateReceiveOnlyCustomerRequest,
    UpdateCustomerRequest,
    UpdateCustomerStatusRequest,
    GetCustomerRequest,
    GetCustomerListRequest,
    GetCustomersResponse,
    GetCustomerDetailsResponse,
    BusinessClassification,
    GetBusinessClassificationList,
    GetBusinessClassificationRequest,
    BazaDwollaErrorCodes,
    bazaDwollaErrorCodesEnI18n,
    bazaDwollaExtractId,
    BazaDwollaCreatePersonalCustomerResponse,
} from '@scaliolabs/baza-dwolla-shared';
import * as qs from 'querystring';
import { Response } from 'dwolla-v2';
import { ulid } from 'ulid';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';
import { BazaDwollaApiErrorException } from '../exception/baza-dwolla-api-error.exception';
import { isValidSSN } from '@scaliolabs/baza-core-shared';

type UpdateCustomer = UpdateCustomerRequest | UpdateCustomerStatusRequest | CreatePersonalCustomerRequest;

@Injectable()
export class DwollaCustomersApiNestjsService implements DwollaCustomersApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async getCustomer(request: GetCustomerRequest): Promise<GetCustomerDetailsResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaCustomersPaths.Customers}/${request.customerId}`),
        );
    }

    getCustomerList(request: GetCustomerListRequest): Promise<GetCustomersResponse> {
        let url = `${DwollaCustomersPaths.Customers}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async getBusinessClassification(request: GetBusinessClassificationRequest): Promise<BusinessClassification> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .get(`${DwollaCustomersPaths.BusinessClassifications}/${request.businessClassificationId}`),
        );
    }

    async getBusinessClassificationList(): Promise<GetBusinessClassificationList> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaCustomersPaths.BusinessClassifications}`),
        );
    }

    async createReceiveOnlyCustomer(request: CreateReceiveOnlyCustomerRequest): Promise<DwollaCustomerId> {
        return this.createCustomer<CreateReceiveOnlyCustomerRequest>(DwollaCustomerType.ReceiveOnly, request);
    }

    async createPersonalCustomer(request: CreatePersonalCustomerRequest): Promise<DwollaCustomerId> {
        return this.createCustomer<CreatePersonalCustomerRequest>(DwollaCustomerType.Personal, request);
    }

    async createBusinessCustomer(request: CreateBusinessCustomerRequest): Promise<DwollaCustomerId> {
        return this.createCustomer<CreateBusinessCustomerRequest>(DwollaCustomerType.Business, request);
    }

    /**
     * To update the customer info
     * @param customerId Customer ID
     * @param request UpdateCustomerRequest
     * @returns GetCustomerDetailsResponse
     */
    async updateCustomer(customerId: string, request: UpdateCustomerRequest): Promise<GetCustomerDetailsResponse> {
        return this.updateCustomerDetail<UpdateCustomerRequest>(customerId, request);
    }

    /**
     * To update the customer status
     * @param customerId Customer ID
     * @param request Customer Input
     * @returns GetCustomerDetailsResponse
     */
    async updateCustomerStatus(customerId: string, request: UpdateCustomerStatusRequest): Promise<GetCustomerDetailsResponse> {
        return this.updateCustomerDetail<UpdateCustomerStatusRequest>(customerId, request);
    }

    /**
     * To retry the verification for dwolla customer whose status is "retry"
     * @param customerId Customer ID
     * @param request CreatePersonalCustomerRequest
     * @returns BazaDwollaCreatePersonalCustomerResponse
     */
    async retryVerificationForPersonalCustomer(
        customerId: DwollaCustomerId,
        request: CreatePersonalCustomerRequest,
    ): Promise<BazaDwollaCreatePersonalCustomerResponse> {
        const customer = await this.getCustomer({ customerId });

        if (customer.status === DwollaCustomerStatus.Retry) {
            if (isValidSSN(request?.ssn)) {
                const { ...details } = customer;
                const customerInput = {
                    ...details,
                    ...request,
                };
                const response = await this.updateCustomerDetail<CreatePersonalCustomerRequest>(customerId, customerInput);
                return { status: response.status };
            } else {
                throw new BazaDwollaApiErrorException(
                    BazaDwollaErrorCodes.BazaDwollaFullSSNRequired,
                    bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaFullSSNRequired],
                );
            }
        }

        throw new BazaDwollaApiErrorException(
            BazaDwollaErrorCodes.BazaDwollaCustomerStatusShouldBeInRetryStatus,
            bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaCustomerStatusShouldBeInRetryStatus],
        );
    }

    private async createCustomer<T extends CreateReceiveOnlyCustomerRequest>(
        customerType: DwollaCustomerType,
        request: T,
    ): Promise<DwollaCustomerId> {
        const updatedCustomerRequest: T = {
            ...request,
            type: customerType,
        };

        const promise = this.dwollaDataAccess
            .getDwollaInstance()
            .post(DwollaCustomersPaths.Customers, updatedCustomerRequest, { 'Idempotency-Key': ulid() });

        return this.dwollaDataAccess
            .processDwollaPromise<Response>(promise, {
                returnBody: false,
            })
            .then((result) => {
                return bazaDwollaExtractId(result.headers.get('location'));
            });
    }

    private async updateCustomerDetail<T extends UpdateCustomer>(customerId: string, request: T): Promise<GetCustomerDetailsResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaCustomersPaths.Customers}/${customerId}`, request),
        );
    }
}
