import { Injectable } from '@nestjs/common';
import {} from 'multer';
import {
    CreateDocumentRequest,
    DwollaCustomerStatus,
    DwollaCustomerType,
    DwollaDocument,
    DwollaDocumentsApi,
    DwollaDocumentsPaths,
    DwollaDocumentType,
    DWOLLA_DOCUMENT_UPLOAD_FILE_SIZE,
    DWOLLA_DOCUMENT_UPLOAD_MIME_IMAGE,
    GetDocumentListResponse,
    BazaDwollaErrorCodes,
    bazaDwollaErrorCodesEnI18n,
    DWOLLA_DOCUMENT_UPLOAD_FILE_SIZE_HR,
    DwollaCustomerId,
} from '@scaliolabs/baza-dwolla-shared';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';
import { BazaDwollaApiErrorException } from '../exception/baza-dwolla-api-error.exception';
import { DwollaCustomersApiNestjsService } from './dwolla-customers-api.nestjs-service';
import { replaceTags } from '@scaliolabs/baza-core-shared';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const FormData = require('form-data');

@Injectable()
export class DwollaDocumentsApiNestjsService implements DwollaDocumentsApi {
    constructor(
        private readonly dwollaDataAccess: DwollaDataAccessService,
        private readonly dwollaCustomerApi: DwollaCustomersApiNestjsService,
    ) {}

    async getDocument(documentId: string): Promise<DwollaDocument> {
        return this.getDocumentList<DwollaDocument>(`${DwollaDocumentsPaths.Documents}/${documentId}`);
    }

    async createDocumentForCustomer(customerId: string, file: Express.Multer.File, request: CreateDocumentRequest): Promise<void> {
        const customer = await this.dwollaCustomerApi.getCustomer({
            customerId,
        });

        if (customer && customer.status !== DwollaCustomerStatus.Document) {
            throw new BazaDwollaApiErrorException(
                BazaDwollaErrorCodes.BazaDwollaCustomerStatusShouldBeInDocument,
                bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaCustomerStatusShouldBeInDocument],
            );
        }

        if (customer.type === DwollaCustomerType.Personal) {
            if (request.documentType === DwollaDocumentType.Other) {
                throw new BazaDwollaApiErrorException(
                    BazaDwollaErrorCodes.BazaDwollaDocumentTypeIsNotAllowed,
                    bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaDocumentTypeIsNotAllowed],
                );
            }

            if (!DWOLLA_DOCUMENT_UPLOAD_MIME_IMAGE.includes(file.mimetype)) {
                throw new BazaDwollaApiErrorException(
                    BazaDwollaErrorCodes.BazaDwollaDocumentTypeIsNotAllowed,
                    replaceTags(bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaDocumentTypeIsNotAllowed], {
                        type: file.mimetype,
                    }),
                );
            }
        }

        return this.createDocument<CreateDocumentRequest>(
            `${DwollaDocumentsPaths.Customers}/${customerId}/${DwollaDocumentsPaths.Documents}`,
            file,
            request,
        );
    }

    async getDocumentListForCustomer(customerId: DwollaCustomerId): Promise<GetDocumentListResponse> {
        return this.getDocumentList<GetDocumentListResponse>(
            `${DwollaDocumentsPaths.Customers}/${customerId}/${DwollaDocumentsPaths.Documents}`,
        );
    }

    async createDocumentForBeneficialOwner(
        beneficialOwnerId: string,
        file: Express.Multer.File,
        request: CreateDocumentRequest,
    ): Promise<void> {
        return this.createDocument<CreateDocumentRequest>(
            `${DwollaDocumentsPaths.BeneficialOwners}/${beneficialOwnerId}/${DwollaDocumentsPaths.Documents}`,
            file,
            request,
        );
    }

    async getDocumentListForBeneficialOwner(beneficialOwnerId: string): Promise<GetDocumentListResponse> {
        return this.getDocumentList<GetDocumentListResponse>(
            `${DwollaDocumentsPaths.BeneficialOwners}/${beneficialOwnerId}/${DwollaDocumentsPaths.Documents}`,
        );
    }

    private async createDocument<T extends CreateDocumentRequest>(url: string, file: Express.Multer.File, request: T): Promise<void> {
        if (file.size > DWOLLA_DOCUMENT_UPLOAD_FILE_SIZE) {
            throw new BazaDwollaApiErrorException(
                BazaDwollaErrorCodes.BazaDwollaDocumentFileSizeExceed,
                replaceTags(bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaDocumentFileSizeExceed], {
                    size: DWOLLA_DOCUMENT_UPLOAD_FILE_SIZE_HR,
                }),
            );
        }

        const formData = new FormData();

        formData.append('file', file.buffer, file.originalname);
        formData.append('documentType', request.documentType);

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().post(url, formData));
    }

    private async getDocumentList<T>(url: string): Promise<T> {
        if (url) {
            return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
        } else {
            throw new BazaDwollaApiErrorException(
                BazaDwollaErrorCodes.BazaDwollaDocumentNoUrlSet,
                bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaDocumentNoUrlSet],
            );
        }
    }
}
