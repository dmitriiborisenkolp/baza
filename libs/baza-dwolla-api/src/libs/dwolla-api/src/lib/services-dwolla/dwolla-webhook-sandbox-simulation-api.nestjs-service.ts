import { Injectable } from '@nestjs/common';
import { DwollaWebhookSandboxSimulationApi, DwollaWebhookSandboxSimulationApiPaths } from '@scaliolabs/baza-dwolla-shared';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaWebhookSandboxSimulationApiNestjsService implements DwollaWebhookSandboxSimulationApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async sandboxWebhookSimulations(): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(DwollaWebhookSandboxSimulationApiPaths.SandboxWebhookSimulations),
        );
    }
}
