import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import {
    CreateBankAccountFundingSourceRequest,
    CreateFundingSourceRequest,
    CreateFundingSourceTokenResponse,
    CreateVANFundingSourceRequest,
    DwollaFundingSource,
    DwollaFundingSourcesApi,
    DwollaFundingSourcesPaths,
    DwollaFundingSourceStatus,
    GetFundingSourceBalanceResponse,
    GetFundingSourceListResponse,
    GetMicroDepositsDetailResponse,
    RemoveFundingSourceRequest,
    UpdateFundingSourceRequest,
    VerifyMicroDepositsRequest,
    DwollaBankAccountType,
    CreateFundingSourceWithPlaidTokenRequest,
    DwollaFundingSourceId,
    bazaDwollaExtractId,
} from '@scaliolabs/baza-dwolla-shared';
import { Response } from 'dwolla-v2';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaFundingSourcesApiNestjsService implements DwollaFundingSourcesApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService, private readonly logger: BazaLogger) {
        this.logger.setContext(this.constructor.name);
    }

    async createFundingSourceWithPlaidToken(
        customerId: string,
        request: CreateFundingSourceWithPlaidTokenRequest,
        correlationId?: string,
    ): Promise<DwollaFundingSourceId> {
        this.logger.info(`${this.constructor.name}.${this.createFundingSourceWithPlaidToken.name}`, {
            correlationId,
            customerId,
            request,
        });

        return this.createFundingSource<CreateFundingSourceWithPlaidTokenRequest>(customerId, request, correlationId);
    }

    async createBankAccountFundingSource(
        customerId: string,
        request: CreateBankAccountFundingSourceRequest,
        correlationId?: string,
    ): Promise<DwollaFundingSourceId> {
        this.logger.info(`${this.constructor.name}.${this.createBankAccountFundingSource.name}`, {
            correlationId,
            customerId,
            request,
        });

        return this.createFundingSource<CreateBankAccountFundingSourceRequest>(customerId, request, correlationId);
    }

    async createVANFundingSource(
        customerId: string,
        request: CreateVANFundingSourceRequest,
        correlationId?: string,
    ): Promise<DwollaFundingSourceId> {
        this.logger.info(`${this.constructor.name}.${this.createVANFundingSource.name}`, {
            correlationId,
            customerId,
            request,
        });

        if (request.bankAccountType !== DwollaBankAccountType.Checking) {
            throw new ForbiddenException();
        }

        return this.createFundingSource<CreateVANFundingSourceRequest>(customerId, request, correlationId);
    }

    async createFundingSourcesToken(customerId: string, correlationId?: string): Promise<CreateFundingSourceTokenResponse> {
        this.logger.info(`${this.constructor.name}.${this.createFundingSourcesToken.name}`, {
            correlationId,
            customerId,
        });

        return this.createFundingSourceToken(customerId, DwollaFundingSourcesPaths.CreateFundingSourcesToken, correlationId);
    }

    async createIAVToken(customerId: string, correlationId?: string): Promise<CreateFundingSourceTokenResponse> {
        this.logger.info(`${this.constructor.name}.${this.createIAVToken.name}`, {
            correlationId,
            customerId,
        });

        return this.createFundingSourceToken(customerId, DwollaFundingSourcesPaths.CreateIAVToken, correlationId);
    }

    async createCardFundingSourcesToken(customerId: string, correlationId?: string): Promise<CreateFundingSourceTokenResponse> {
        this.logger.info(`${this.constructor.name}.${this.createCardFundingSourcesToken.name}`, {
            correlationId,
            customerId,
        });

        return this.createFundingSourceToken(customerId, DwollaFundingSourcesPaths.CreateCardFundingSourcesToken);
    }

    async getFundingSource(fundingSourceId: string, correlationId?: string): Promise<DwollaFundingSource> {
        this.logger.info(`${this.constructor.name}.${this.getFundingSource.name}`, {
            correlationId,
            fundingSourceId,
        });

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaFundingSourcesPaths.FundingSources}/${fundingSourceId}`),
        );
    }

    async getFundingSourceListForCustomer(
        customerId: string,
        isRemoved = false,
        correlationId?: string,
    ): Promise<GetFundingSourceListResponse> {
        this.logger.info(`${this.constructor.name}.${this.getFundingSourceListForCustomer.name}`, {
            correlationId,
            customerId,
            isRemoved,
        });

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .get(`customers/${customerId}/${DwollaFundingSourcesPaths.FundingSources}?removed=${isRemoved}`),
        );
    }

    async updateFundingSource(
        fundingSourceId: string,
        request: UpdateFundingSourceRequest,
        correlationId?: string,
    ): Promise<DwollaFundingSource> {
        this.logger.info(`${this.constructor.name}.${this.updateFundingSource.name}`, {
            correlationId,
            fundingSourceId,
            request,
        });

        const fundingSource = await this.getFundingSource(fundingSourceId, correlationId);

        if (!fundingSource) {
            throw new NotFoundException();
        }

        if (fundingSource.status === DwollaFundingSourceStatus.Verified) {
            throw new ForbiddenException();
        }

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaFundingSourcesPaths.FundingSources}/${fundingSourceId}`, request),
        );
    }

    async initiateMicroDeposits(fundingSourceId: string, correlationId?: string): Promise<void> {
        this.logger.info(`${this.constructor.name}.${this.initiateMicroDeposits.name}`, {
            correlationId,
            fundingSourceId,
        });

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .post(`${DwollaFundingSourcesPaths.FundingSources}/${fundingSourceId}/${DwollaFundingSourcesPaths.MicroDeposits}`),
        );
    }

    async verifyMicroDeposits(fundingSourceId: string, request: VerifyMicroDepositsRequest, correlationId?: string): Promise<void> {
        this.logger.info(`${this.constructor.name}.${this.verifyMicroDeposits.name}`, {
            correlationId,
            fundingSourceId,
            request,
        });

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .post(`${DwollaFundingSourcesPaths.FundingSources}/${fundingSourceId}/${DwollaFundingSourcesPaths.MicroDeposits}`, request),
        );
    }

    async getFundingSourceBalance(fundingSourceId: string, correlationId?: string): Promise<GetFundingSourceBalanceResponse> {
        this.logger.info(`${this.constructor.name}.${this.getFundingSourceBalance.name}`, {
            correlationId,
            fundingSourceId,
        });

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .get(`${DwollaFundingSourcesPaths.FundingSources}/${fundingSourceId}/${DwollaFundingSourcesPaths.GetFundingSourceBalance}`),
        );
    }

    async getMicroDepositsDetail(fundingSourceId: string, correlationId?: string): Promise<GetMicroDepositsDetailResponse> {
        this.logger.info(`${this.constructor.name}.${this.getMicroDepositsDetail.name}`, {
            correlationId,
            fundingSourceId,
        });

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .get(`${DwollaFundingSourcesPaths.FundingSources}/${fundingSourceId}/${DwollaFundingSourcesPaths.MicroDeposits}`),
        );
    }

    async removeFundingSource(fundingSourceId: string, request: RemoveFundingSourceRequest, correlationId?: string): Promise<void> {
        this.logger.info(`${this.constructor.name}.${this.removeFundingSource.name}`, {
            correlationId,
            fundingSourceId,
        });

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaFundingSourcesPaths.FundingSources}/${fundingSourceId}`, request),
        );
    }

    private async createFundingSource<T extends CreateFundingSourceRequest>(
        customerId: string,
        request: T,
        correlationId?: string,
    ): Promise<DwollaFundingSourceId> {
        this.logger.info(`${this.constructor.name}.${this.createFundingSource.name}`, {
            correlationId,
            customerId,
            request,
        });

        return this.dwollaDataAccess
            .processDwollaPromise<Response>(
                this.dwollaDataAccess
                    .getDwollaInstance()
                    .post(`customers/${customerId}/${DwollaFundingSourcesPaths.FundingSources}`, request),
                { returnBody: false },
            )
            .then((result) => {
                return bazaDwollaExtractId(result.headers.get('location'));
            });
    }

    private async createFundingSourceToken(
        customerId: string,
        urlPath: string,
        correlationId?: string,
    ): Promise<CreateFundingSourceTokenResponse> {
        this.logger.info(`${this.constructor.name}.${this.createFundingSourceToken.name}`, {
            correlationId,
            customerId,
            urlPath,
        });

        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`customers/${customerId}/${urlPath}`),
        );
    }
}
