import { Injectable } from '@nestjs/common';
import { DwollaWebhooksApi, DwollaWebhooksPaths } from '@scaliolabs/baza-dwolla-shared';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaWebhooksApiNestjsService implements DwollaWebhooksApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async getWebhook(webhookId: string): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaWebhooksPaths.Webhooks}/${webhookId}`),
        );
    }

    async retryWebhook(webhookId: string): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaWebhooksPaths.Webhooks}/${webhookId}/${DwollaWebhooksPaths.Retries}`),
        );
    }

    async getRetryListForWebhook(webhookId: string): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaWebhooksPaths.Webhooks}/${webhookId}/${DwollaWebhooksPaths.Retries}`),
        );
    }
}
