import { Injectable } from '@nestjs/common';
import {
    DwollaEventsApi,
    DwollaEventsPaths,
    GetEventListRequest,
    GetEventListResponse,
    GetEventResponse,
} from '@scaliolabs/baza-dwolla-shared';
import * as qs from 'querystring';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaEventsApiNestjsService implements DwollaEventsApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async getEvent(applicationEventId: string): Promise<GetEventResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaEventsPaths.Events}/${applicationEventId}`),
        );
    }

    async getEventList(request?: GetEventListRequest): Promise<GetEventListResponse> {
        let url = `${DwollaEventsPaths.Events}`;

        if (request) {
            url = `${DwollaEventsPaths.Events}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }
}
