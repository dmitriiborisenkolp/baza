import { Injectable } from '@nestjs/common';
import {
    CreateBeneficialOwnerRequest,
    DwollaBeneficialOwnersApi,
    DwollaBeneficialOwnersPaths,
    GetBeneficialOwnerListResponse,
    GetBeneficialOwnerResponse,
    GetBeneficialOwnershipStatusResponse,
    UpdateBeneficialOwnershipStatusRequest,
} from '@scaliolabs/baza-dwolla-shared';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaBeneficialOwnersApiNestjsService implements DwollaBeneficialOwnersApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async createBeneficialOwner(customerId: string, request: CreateBeneficialOwnerRequest): Promise<void> {
        const url = `${DwollaBeneficialOwnersPaths.Customers}/${customerId}/${DwollaBeneficialOwnersPaths.BeneficialOwners}`;

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().post(url, request));
    }

    async getBeneficialOwner(beneficialOwnerId: string): Promise<GetBeneficialOwnerResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaBeneficialOwnersPaths.BeneficialOwners}/${beneficialOwnerId}`),
        );
    }

    async updateBeneficialOwner(beneficialOwnerId: string, request: CreateBeneficialOwnerRequest): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaBeneficialOwnersPaths.BeneficialOwners}/${beneficialOwnerId}`, request),
        );
    }

    async getBeneficialOwnerList(customerId: string): Promise<GetBeneficialOwnerListResponse> {
        const url = `${DwollaBeneficialOwnersPaths.Customers}/${customerId}/${DwollaBeneficialOwnersPaths.BeneficialOwners}`;

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async deleteBeneficialOwner(beneficialOwnerId: string): Promise<GetBeneficialOwnerResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().delete(`${DwollaBeneficialOwnersPaths.BeneficialOwners}/${beneficialOwnerId}`),
        );
    }

    async getBeneficialOwnershipStatus(customerId: string): Promise<GetBeneficialOwnershipStatusResponse> {
        const url = `${DwollaBeneficialOwnersPaths.Customers}/${customerId}/${DwollaBeneficialOwnersPaths.BeneficialOwnership}`;

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async certifyBeneficialOwnership(
        customerId: string,
        request: UpdateBeneficialOwnershipStatusRequest,
    ): Promise<GetBeneficialOwnershipStatusResponse> {
        const url = `${DwollaBeneficialOwnersPaths.Customers}/${customerId}/${DwollaBeneficialOwnersPaths.BeneficialOwnership}`;

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().post(url, request));
    }
}
