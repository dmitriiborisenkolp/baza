import { HttpStatus, Injectable } from '@nestjs/common';
import {
    BazaDwollaErrorCodes,
    bazaDwollaErrorCodesEnI18n,
    bazaDwollaExtractId,
    CancelTransferRequest,
    CreateTransferRequest,
    DwollaTransfersApi,
    DwollaTransfersPaths,
    GetFailureReasonForTransferResponse,
    GetFeeListForTransferResponse,
    GetTransferDetailsResponse,
    GetTransferListForCustomerRequest,
    GetTransferListResponse,
    InitiateTransferResponse,
} from '@scaliolabs/baza-dwolla-shared';
import * as qs from 'querystring';
import { ulid } from 'ulid';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';
import { Response } from 'dwolla-v2';
import { BazaDwollaApiErrorException } from '../exception/baza-dwolla-api-error.exception';

@Injectable()
export class DwollaTransfersApiNestjsService implements DwollaTransfersApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async initiateTransfer(request: CreateTransferRequest): Promise<InitiateTransferResponse> {
        const idempotencyKey = ulid();

        const header = {
            'Idempotency-Key': idempotencyKey,
        };

        return this.dwollaDataAccess
            .processDwollaPromise<Response>(
                this.dwollaDataAccess.getDwollaInstance().post(DwollaTransfersPaths.Transfers, request, header),
                {
                    returnBody: false,
                },
            )
            .then((response) => {
                if (response.status !== HttpStatus.CREATED) {
                    throw new BazaDwollaApiErrorException(
                        BazaDwollaErrorCodes.BazaDwollaTransferFailedToInitiate,
                        bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaTransferFailedToInitiate],
                    );
                }

                return {
                    dwollaTransferId: bazaDwollaExtractId(response.headers.get('location')),
                    idempotencyKey: idempotencyKey,
                };
            })
            .catch(() => {
                throw new BazaDwollaApiErrorException(
                    BazaDwollaErrorCodes.BazaDwollaTransferFailedToInitiate,
                    bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaTransferFailedToInitiate],
                );
            });
    }

    async getTransfer(transferId: string): Promise<GetTransferDetailsResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaTransfersPaths.Transfers}/${transferId}`),
        );
    }

    async getTransferListForCustomer(customerId: string, request: GetTransferListForCustomerRequest): Promise<GetTransferListResponse> {
        let url = `${DwollaTransfersPaths.Customers}/${customerId}/${DwollaTransfersPaths.Transfers}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async getFeeListForTransfer(transferId: string): Promise<GetFeeListForTransferResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaTransfersPaths.Transfers}/${transferId}/${DwollaTransfersPaths.Fees}`),
        );
    }

    async getFailureReasonForTransfer(transferId: string): Promise<GetFailureReasonForTransferResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .get(`${DwollaTransfersPaths.Transfers}/${transferId}/${DwollaTransfersPaths.Failure}`),
        );
    }

    async cancelTransfer(transferId: string, request: CancelTransferRequest): Promise<GetTransferDetailsResponse> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaTransfersPaths.Transfers}/${transferId}`, request),
        );
    }
}
