import { Injectable } from '@nestjs/common';
import {
    CreateLabelLedgerEntryRequest,
    CreateLabelReallocationRequest,
    DwollaLabelsApi,
    DwollaLabelsPaths,
    GetLabelListFilterRequest,
} from '@scaliolabs/baza-dwolla-shared';
import * as qs from 'querystring';
import { DwollaDataAccessService } from '../services/dwolla-data-access.service';

@Injectable()
export class DwollaLabelsApiNestjsService implements DwollaLabelsApi {
    constructor(private readonly dwollaDataAccess: DwollaDataAccessService) {}

    async createLabel(customerId: string): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaLabelsPaths.Customers}/${customerId}/${DwollaLabelsPaths.Labels}`),
        );
    }

    async getLabel(labelId: string): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaLabelsPaths.Labels}/${labelId}`),
        );
    }

    async createLabelLedgerEntry(labelId: string, request: CreateLabelLedgerEntryRequest): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess
                .getDwollaInstance()
                .post(`${DwollaLabelsPaths.Labels}/${labelId}/${DwollaLabelsPaths.LedgerEntries}`, request),
        );
    }

    async getLabelLedgerEntry(ledgerEntryId: string): Promise<unknown> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaLabelsPaths.LedgerEntries}/${ledgerEntryId}`),
        );
    }

    async getLabelLedgerEntryList(labelId: string, request?: GetLabelListFilterRequest): Promise<unknown[]> {
        let url = `${DwollaLabelsPaths.Labels}/${labelId}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async getLabelListForCustomer(customerId: string, request?: GetLabelListFilterRequest): Promise<unknown[]> {
        let url = `${DwollaLabelsPaths.Customers}/${customerId}/${DwollaLabelsPaths.Labels}`;

        if (request) {
            url = `${url}?${qs.stringify({ ...request })}`;
        }

        return this.dwollaDataAccess.processDwollaPromise(this.dwollaDataAccess.getDwollaInstance().get(url));
    }

    async createLabelReallocation(request: CreateLabelReallocationRequest): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().post(`${DwollaLabelsPaths.LabelReallocations}`, request),
        );
    }

    async getLabelReallocation(labelId: string): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().get(`${DwollaLabelsPaths.LabelReallocations}/${labelId}`),
        );
    }

    async deleteLabel(labelId: string): Promise<void> {
        return this.dwollaDataAccess.processDwollaPromise(
            this.dwollaDataAccess.getDwollaInstance().delete(`${DwollaLabelsPaths.Labels}/${labelId}`),
        );
    }
}
