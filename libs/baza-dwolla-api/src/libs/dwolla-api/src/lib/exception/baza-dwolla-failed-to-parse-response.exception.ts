import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaErrorCodes, bazaDwollaErrorCodesEnI18n } from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaFailedToParseResponseException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaErrorCodes.BazaDwollaFailedToParseResponse,
            bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaFailedToParseResponse],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
