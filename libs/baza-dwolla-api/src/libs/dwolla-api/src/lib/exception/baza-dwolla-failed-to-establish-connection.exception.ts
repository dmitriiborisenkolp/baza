import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaErrorCodes, bazaDwollaErrorCodesEnI18n } from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaFailedToEstablishConnectionException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaErrorCodes.BazaDwollaFailedToEstablishConnection,
            bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaFailedToEstablishConnection],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
