import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaErrorCodes, bazaDwollaErrorCodesEnI18n } from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaNoDataReceivedException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaErrorCodes.BazaDwollaNoDataReceived,
            bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaNoDataReceived],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
