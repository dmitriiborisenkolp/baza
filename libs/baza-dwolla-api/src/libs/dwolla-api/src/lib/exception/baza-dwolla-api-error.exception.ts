import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaErrorCodes, bazaDwollaErrorCodesEnI18n } from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaApiErrorException extends BazaAppException {
    constructor(
        public readonly dwollaErrorCode: string,
        message: string,
        details?: any,
        public readonly _embedded?: any,
        public readonly _links?: any,
    ) {
        super(
            BazaDwollaErrorCodes.BazaDwollaApiError,
            bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaApiError],
            HttpStatus.INTERNAL_SERVER_ERROR,
            { message },
            details,
        );
    }
}
