import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaWebhookErrorCodes, bazaDwollaWebhookErrorCodesEnI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaWebhookInvalidSecretKeyException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaWebhookErrorCodes.BazaDwollaWebhookInvalidSecretKey,
            bazaDwollaWebhookErrorCodesEnI18n[BazaDwollaWebhookErrorCodes.BazaDwollaWebhookInvalidSecretKey],
            HttpStatus.UNAUTHORIZED,
        );
    }
}
