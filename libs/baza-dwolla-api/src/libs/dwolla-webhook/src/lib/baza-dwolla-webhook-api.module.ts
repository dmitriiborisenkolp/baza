import { Module } from '@nestjs/common';
import { BazaDwollaApiModule } from '../../../dwolla-api/src';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaEventBusApiModule } from '@scaliolabs/baza-core-api';
import { BazaDwollaWebhookController } from './controllers/baza-dwolla-webhook.controller';
import { BazaDwollaWebhookService } from './services/baza-dwolla-webhook.service';

@Module({
    imports: [CqrsModule, BazaDwollaApiModule, BazaEventBusApiModule],
    controllers: [BazaDwollaWebhookController],
    providers: [BazaDwollaWebhookService],
    exports: [BazaDwollaWebhookService],
})
export class BazaDwollaWebhookApiModule {}
