export enum BazaDwollaKafkaEvent {
    BazaDwollaWebhookAccepted = 'BazaDwollaWebhookAccepted',
}

export type BazaDwollaWebhookEvents = { topic: BazaDwollaKafkaEvent.BazaDwollaWebhookAccepted; payload: BazaDwollaKafkaEventAccepted };

export class BazaDwollaKafkaEventAccepted<T = any> {
    topic: BazaDwollaKafkaEvent.BazaDwollaWebhookAccepted;
    responseBody: T;
}
