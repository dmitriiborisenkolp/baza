import { Body, Controller, Headers, Post, Req, RawBodyRequest } from '@nestjs/common';
import {
    BazaDwollaCmsOpenApi,
    BazaDwollaWebhookDto,
    BazaDwollaWebhookEndpoint,
    BazaDwollaWebhookEndpointPaths,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaWebhookService } from '../services/baza-dwolla-webhook.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';

@Controller()
@ApiTags(BazaDwollaCmsOpenApi.BazaDwollaCmsWebhooks)
export class BazaDwollaWebhookController implements BazaDwollaWebhookEndpoint {
    constructor(private readonly service: BazaDwollaWebhookService) {}

    @Post(BazaDwollaWebhookEndpointPaths.accept)
    @ApiOperation({
        summary: 'accept',
        description: 'Accepts webhook from Dwolla and re-publish it to CQRS or Kafka',
    })
    @ApiOkResponse()
    async accept(
        @Body() request: BazaDwollaWebhookDto,
        @Headers('X-Request-Signature-SHA-256') sha256Header: string,
        @Req() rawRequest: RawBodyRequest<Request>,
    ): Promise<void> {
        await this.service.accept(request, rawRequest.rawBody, sha256Header);
    }
}
