import { Injectable, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { EventBus } from '@nestjs/cqrs';
import { BazaDwollaWebhookDto } from '@scaliolabs/baza-dwolla-shared';
import { BazaKafkaDwollaWebhookEvent } from '../events/baza-kafka-dwolla-webhook.event';
import { DwollaConfigurationNestjsService } from '../../../../dwolla-api/src';
import { ApiEventBus, BazaLogger, EnvService } from '@scaliolabs/baza-core-api';
import { BazaDwollaKafkaEvent, BazaDwollaWebhookEvents } from '../kafka/baza-dwolla-kafka';
import { Subject } from 'rxjs';
import { ApiEventSource } from '@scaliolabs/baza-core-shared';
import { BazaDwollaWebhookInvalidSecretKeyException } from '../exceptions/baza-dwolla-webhook-invalid-secret-key.exception';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const crypto = require('crypto');

/**
 * Dwolla Webhook Service
 * The service is responsible to send Dwolla Webhook Events to Baza Event Bus. Read events with
 * `BazaDwollaKafkaEvent.BazaDwollaWebhookAccepted` filter in your services.
 * The Service is also reading / sending Webhook events to Kafka depends on environment variables. This is being
 * used on Local (READ) and Test (SEND) environments to allow local development with Dwolla features
 */
@Injectable()
export class BazaDwollaWebhookService implements OnApplicationBootstrap, OnApplicationShutdown {
    private onApplicationShutdown$ = new Subject<void>();

    constructor(
        private readonly moduleConfig: DwollaConfigurationNestjsService,
        private readonly env: EnvService,
        private readonly eventBus: EventBus,
        private readonly logger: BazaLogger,
        private readonly bazaApiBus: ApiEventBus<BazaDwollaKafkaEvent, BazaDwollaWebhookEvents>,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Starts listening for events from Kafka on Application Start
     */
    onApplicationBootstrap(): void {
        this.listen();
    }

    /**
     * Stops listening for events from Kafka
     */
    onApplicationShutdown(): void {
        this.onApplicationShutdown$.next();
    }

    /**
     * Returns configuration for Dwolla Webhooks
     */
    get config() {
        return this.moduleConfig.configuration.webhooks;
    }

    /**
     * Validates Webhook and sends it to Baza Event Bus
     * If Send configuration is enabled, the event will be also sent to Kafka
     * @param responseBody
     * @param rawBody
     * @param sha256Header
     */
    async accept(responseBody: BazaDwollaWebhookDto, rawBody: Buffer, sha256Header: string): Promise<void> {
        if (!this.env.isTestEnvironment) {
            this.validate(rawBody, sha256Header);
        }

        this.send(responseBody);

        if (this.config.kafkaPipe.send) {
            await this.bazaApiBus.publish(
                {
                    topic: BazaDwollaKafkaEvent.BazaDwollaWebhookAccepted,
                    payload: {
                        topic: BazaDwollaKafkaEvent.BazaDwollaWebhookAccepted,
                        responseBody,
                    },
                },
                {
                    target: ApiEventSource.EveryNode,
                },
            );
        }

        if (this.config.debug) {
            this.logger.log('Webhook accepted from Dwolla:');
            this.logger.log(JSON.stringify(responseBody));
        }
    }

    /**
     * Sends (w/o validation) Webhook to Baza Event Bus
     * @param responseBody
     */
    send(responseBody: BazaDwollaWebhookDto): void {
        this.eventBus.publish(
            new BazaKafkaDwollaWebhookEvent({
                responseBody: responseBody,
            }),
        );
    }

    /**
     * Listens and extracts Dwolla Webhook Events from Kafka Bus
     * Service will listen for Kafka only if READ configuration is enabled
     */
    listen(): void {
        if (!this.config.kafkaPipe.read) {
            return;
        }

        this.bazaApiBus.subscribe({
            source: ApiEventSource.EveryNode,
            takeUntil$: this.onApplicationShutdown$,
            callback: async (bazaEvent) => {
                if (bazaEvent.event.topic === BazaDwollaKafkaEvent.BazaDwollaWebhookAccepted) {
                    this.eventBus.publish(
                        new BazaKafkaDwollaWebhookEvent({
                            responseBody: bazaEvent.event.payload.responseBody,
                        }),
                    );

                    if (this.config.debug) {
                        this.logger.log('Webhook received from Kafka:');
                        this.logger.log(bazaEvent.event);
                    }
                }
            },
        });
    }

    /**
     * Validates Webhook body
     * @see https://developers.dwolla.com/guides/webhooks/process-validate#authentication
     * @param rawBody
     * @param sha256Header
     * @private
     */
    private validate(rawBody: Buffer, sha256Header: string): void {
        const isSignatureValue = (body, signature) =>
            signature === crypto.createHmac('sha256', this.moduleConfig.configuration.webhooks.secret).update(body).digest('hex');

        if (!isSignatureValue(rawBody, sha256Header)) {
            throw new BazaDwollaWebhookInvalidSecretKeyException();
        }
    }
}
