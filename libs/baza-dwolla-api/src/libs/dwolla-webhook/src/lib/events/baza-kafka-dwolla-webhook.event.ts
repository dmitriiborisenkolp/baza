import { BazaDwollaWebhookDto } from '@scaliolabs/baza-dwolla-shared';

export class BazaKafkaDwollaWebhookEvent<T extends BazaDwollaWebhookDto = BazaDwollaWebhookDto> {
    constructor(
        public readonly payload: {
            responseBody: T;
        },
    ) {}
}
