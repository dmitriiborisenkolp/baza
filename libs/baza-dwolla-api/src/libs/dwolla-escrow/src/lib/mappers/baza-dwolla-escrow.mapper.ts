import { Injectable } from '@nestjs/common';
import {
    BAZA_DWOLLA_BALANCE_REGISTRY_VALUE,
    BazaDwollaEscrowFundingSourceDto,
    DwollaFundingSource,
    DwollaFundingSourceId,
    DwollaFundingSourceType,
} from '@scaliolabs/baza-dwolla-shared';
import { EnvService } from '@scaliolabs/baza-core-api';

/**
 * DwollaFundingSource to BazaDwollaEscrowFundingSourceDto Mapper
 */
@Injectable()
export class BazaDwollaEscrowMapper {
    constructor(private readonly env: EnvService) {}

    /**
     * DwollaFundingSource to BazaDwollaEscrowFundingSourceDto Mapper
     * (Single Entity)
     * @param fundingSource
     * @param options
     */
    fundingSourceToDTO(
        fundingSource: DwollaFundingSource,
        options: {
            isDefault: boolean;
        },
    ): BazaDwollaEscrowFundingSourceDto {
        const isUAT = this.env.isUATEnvironment;
        const isProd = this.env.isProductionEnvironment;
        const isBalance = fundingSource.type === DwollaFundingSourceType.Balance;

        return {
            isSelectedAsEscrow: options.isDefault,
            canBeSelectedAsEscrow: (!isUAT && !isProd) || (isProd && !isBalance) || (isUAT && isBalance),
            dwollaFundingSourceId: fundingSource.id,
            name: fundingSource.name,
            type: fundingSource.type,
            status: fundingSource.status,
            created: fundingSource.created,
            bankName: fundingSource.bankName,
            bankAccountType: fundingSource.bankAccountType,
        };
    }

    /**
     * DwollaFundingSource to BazaDwollaEscrowFundingSourceDto Mapper
     * (Multiple Entities)
     * @param fundingSources
     * @param options
     */
    fundingSourcesToDTOs(
        fundingSources: Array<DwollaFundingSource>,
        options: {
            defaultFundingSourceId: DwollaFundingSourceId;
        },
    ): Array<BazaDwollaEscrowFundingSourceDto> {
        return fundingSources.map((next) =>
            this.fundingSourceToDTO(next, {
                isDefault:
                    ((!options.defaultFundingSourceId || options.defaultFundingSourceId === BAZA_DWOLLA_BALANCE_REGISTRY_VALUE) &&
                        next.type === DwollaFundingSourceType.Balance) ||
                    options.defaultFundingSourceId === next.id,
            }),
        );
    }
}
