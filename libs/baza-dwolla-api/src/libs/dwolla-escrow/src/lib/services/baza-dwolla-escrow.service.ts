import { Injectable } from '@nestjs/common';
import { BazaRegistryService, EnvService } from '@scaliolabs/baza-core-api';
import {
    BAZA_DWOLLA_BALANCE_REGISTRY_VALUE,
    BazaDwollaEscrowCmsSetRequest,
    BazaDwollaEscrowFundingSourceDto,
    bazaDwollaEscrowSupportedTypes,
    DwollaFundingSourceId,
    DwollaFundingSourceType,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaMasterAccountService } from '../../../../dwolla-master-account/src';
import { BazaDwollaEscrowNotFoundException } from '../exceptions/baza-dwolla-escrow-not-found.exception';
import { BazaDwollaEscrowMapper } from '../mappers/baza-dwolla-escrow.mapper';
import { BazaDwollaEscrowProductionRestrictionsException } from '../exceptions/baza-dwolla-escrow-production-restrictions.exception';
import { BazaDwollaEscrowUatRestrictionsException } from '../exceptions/baza-dwolla-escrow-uat-restrictions.exception';
import { BazaDwollaEscrowUnsupportedTypeException } from '../exceptions/baza-dwolla-escrow-unsupported-type.exception';

/**
 * Dwolla Escrow Service
 * Allows to fetch or set Dwolla Funding Source which is used as Escrow
 * Also allows to fetch list of potential Funding Sources which can be used as Escrow
 * @see BazaDwollaEscrowService.isProperlyConfigured
 */
@Injectable()
export class BazaDwollaEscrowService {
    public static readonly REGISTRY_KEY = 'bazaDwolla.escrowFundingSourceId';

    constructor(
        private readonly env: EnvService,
        private readonly registry: BazaRegistryService,
        private readonly masterAccount: BazaDwollaMasterAccountService,
        private readonly mapper: BazaDwollaEscrowMapper,
    ) {}

    /**
     * Returns true if Dwolla Escrow is properly configured and can be used
     * You must check for `isProperlyConfigured` before using any other methods
     * of the service
     */
    async isProperlyConfigured(): Promise<boolean> {
        const fundingSource = await this.current();

        const isUAT = this.env.isUATEnvironment;
        const isProd = this.env.isProductionEnvironment;
        const isBalance = fundingSource.type === DwollaFundingSourceType.Balance;

        return (!isUAT && !isProd) || (isProd && !isBalance) || (isUAT && isBalance);
    }

    /**
     * Returns current selected Dwolla Funding Source
     * If no Funding Source is set yet, an Account Balance FS will be returned
     * even for production environments. API should do additional checks for
     * prod / uat environments before using Escrow FS
     * @see BazaDwollaEscrowService.isProperlyConfigured
     */
    async current(): Promise<BazaDwollaEscrowFundingSourceDto> {
        if (this.isBalance) {
            const fundingSource = await this.masterAccount.balanceFundingSource();

            return this.mapper.fundingSourceToDTO(fundingSource, {
                isDefault: true,
            });
        } else {
            const fundingSources = await this.masterAccount.fundingSources();
            const fundingSource = fundingSources._embedded['funding-sources'].find((next) => next.id === this.currentFundingSourceId);

            if (!fundingSource) {
                throw new BazaDwollaEscrowNotFoundException();
            }

            return this.mapper.fundingSourceToDTO(fundingSource, {
                isDefault: true,
            });
        }
    }

    /**
     * Returns HREF of Escrow Funding Source
     */
    async currentFundingSourceHref(): Promise<string> {
        if (this.isBalance) {
            const fundingSource = await this.masterAccount.balanceFundingSource();

            return fundingSource._links.self.href;
        } else {
            const fundingSources = await this.masterAccount.fundingSources();
            const fundingSource = fundingSources._embedded['funding-sources'].find((next) => next.id === this.currentFundingSourceId);

            if (!fundingSource) {
                throw new BazaDwollaEscrowNotFoundException();
            }

            return fundingSource._links.self.href;
        }
    }

    /**
     * Returns ID of currently selected Dwolla Funding Source
     * If no FS was set or Dwolla Balance is set as FS, the method will returns undefined
     */
    get currentFundingSourceId(): DwollaFundingSourceId | undefined {
        return this.registry.getValue(BazaDwollaEscrowService.REGISTRY_KEY) || undefined;
    }

    /**
     * Returns true if Account Balance is selected as Dwolla Escrow FS
     */
    get isBalance(): boolean {
        return !this.currentFundingSourceId || this.currentFundingSourceId === BAZA_DWOLLA_BALANCE_REGISTRY_VALUE;
    }

    /**
     * Returns false if Account Balance is selected as Dwolla Escrow FS
     */
    get isBankAccount(): boolean {
        return !!this.currentFundingSourceId && this.currentFundingSourceId !== BAZA_DWOLLA_BALANCE_REGISTRY_VALUE;
    }

    /**
     * Returns true if given Dwolla FS Type can be selected as Dwolla Escrow FS
     * @param type
     */
    isSupported(type: DwollaFundingSourceType): boolean {
        return bazaDwollaEscrowSupportedTypes.includes(type);
    }

    /**
     * Returns list of Funding Source attached to Master Account
     * List will be filtered by items which could be used as Dwolla Escrow (ignoring
     * Production / UAT restrictions)
     * Please use `BazaDwollaEscrowFundingSourceDto.canBeSelectedAsEscrow` flag
     * for taking information about possibility to use FS as Escrow FS
     */
    async list(): Promise<Array<BazaDwollaEscrowFundingSourceDto>> {
        const fundingSources = (await this.masterAccount.fundingSources())._embedded['funding-sources'].filter((next) =>
            this.isSupported(next.type),
        );

        return this.mapper.fundingSourcesToDTOs(fundingSources, {
            defaultFundingSourceId: this.currentFundingSourceId,
        });
    }

    /**
     * Sets Funding Source as Dwolla Escrow FS
     * @param request
     */
    async set(request: BazaDwollaEscrowCmsSetRequest): Promise<BazaDwollaEscrowFundingSourceDto> {
        const fundingSources = await this.list();
        const fundingSource = fundingSources.find((next) => next.dwollaFundingSourceId === request.dwollaFundingSourceId);

        if (!fundingSource) {
            throw new BazaDwollaEscrowNotFoundException();
        }

        if (!this.isSupported(fundingSource.type)) {
            throw new BazaDwollaEscrowUnsupportedTypeException(fundingSource.type);
        }

        if (this.env.isPublicProductionEnvironment && fundingSource.type === DwollaFundingSourceType.Balance) {
            throw new BazaDwollaEscrowProductionRestrictionsException();
        }

        if (this.env.isUATEnvironment && fundingSource.type === DwollaFundingSourceType.Bank) {
            throw new BazaDwollaEscrowUatRestrictionsException();
        }

        await this.registry.updateSchemaRecord({
            path: BazaDwollaEscrowService.REGISTRY_KEY,
            value:
                fundingSource.type === DwollaFundingSourceType.Balance
                    ? BAZA_DWOLLA_BALANCE_REGISTRY_VALUE
                    : fundingSource.dwollaFundingSourceId,
        });

        return {
            ...fundingSource,
            isSelectedAsEscrow: true,
        };
    }
}
