import { Global, Module } from '@nestjs/common';
import { BazaEnvModule } from '@scaliolabs/baza-core-api';
import { BazaDwollaMasterAccountApiModule } from '../../../dwolla-master-account/src';
import { BazaDwollaEscrowCmsController } from './controllers/baza-dwolla-escrow-cms.controller';
import { BazaDwollaEscrowMapper } from './mappers/baza-dwolla-escrow.mapper';
import { BazaDwollaEscrowService } from './services/baza-dwolla-escrow.service';
import { BazaDwollaApiModule } from '../../../dwolla-api/src';

@Global()
@Module({
    imports: [BazaEnvModule, BazaDwollaMasterAccountApiModule, BazaDwollaApiModule],
    controllers: [BazaDwollaEscrowCmsController],
    providers: [BazaDwollaEscrowMapper, BazaDwollaEscrowService],
    exports: [BazaDwollaEscrowMapper, BazaDwollaEscrowService],
})
export class BazaDwollaEscrowApiModule {}
