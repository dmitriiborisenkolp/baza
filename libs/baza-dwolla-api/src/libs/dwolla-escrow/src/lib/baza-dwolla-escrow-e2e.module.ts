import { Module } from '@nestjs/common';
import { BazaDwollaEscrowFixture } from './integration-tests/fixtures/baza-dwolla-escrow.fixture';
import { BazaDwollaEscrowApiModule } from './baza-dwolla-escrow-api.module';
import { BazaDwollaMasterAccountApiModule } from '../../../dwolla-master-account/src';
import { BazaDwollaApiModule } from '../../../dwolla-api/src';

@Module({
    imports: [BazaDwollaEscrowApiModule, BazaDwollaMasterAccountApiModule, BazaDwollaApiModule],
    providers: [BazaDwollaEscrowFixture],
    exports: [BazaDwollaEscrowFixture],
})
export class BazaDwollaEscrowE2eModule {}
