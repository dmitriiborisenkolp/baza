import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaDwollaEscrowFixtures } from '../baza-dwolla-escrow.fixtures';
import { BazaDwollaMasterAccountService } from '../../../../../dwolla-master-account/src';
import {
    bazaDwollaExtractId,
    CreateBankAccountFundingSourceRequest,
    DwollaBankAccountType,
    DwollaFundingSourceType,
} from '@scaliolabs/baza-dwolla-shared';
import { DwollaFundingSourcesApiNestjsService } from '../../../../../dwolla-api/src';

const FIXTURES: Array<CreateBankAccountFundingSourceRequest> = [
    {
        bankAccountType: DwollaBankAccountType.Checking,
        name: 'E2E Bank Account',
        accountNumber: '1111222233330001',
        routingNumber: '011401533',
    },
];

@Injectable()
export class BazaDwollaEscrowFixture implements BazaE2eFixture {
    constructor(
        private readonly masterAccount: BazaDwollaMasterAccountService,
        private readonly fundingSourceApi: DwollaFundingSourcesApiNestjsService,
    ) {}

    async up(): Promise<void> {
        const fundingSources = await this.masterAccount.fundingSources();
        const bankAccountFundingSources = fundingSources._embedded['funding-sources'].filter(
            (next) => next.type === DwollaFundingSourceType.Bank,
        );

        if (bankAccountFundingSources.length < 0) {
            const root = await this.masterAccount.root();

            for (const fixtureRequest of FIXTURES) {
                await this.fundingSourceApi.createBankAccountFundingSource(bazaDwollaExtractId(root._links.customers.href), fixtureRequest);
            }
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaDwollaEscrowFixtures.BazaDwollaEscrowFixture,
        provider: BazaDwollaEscrowFixture,
    },
]);
