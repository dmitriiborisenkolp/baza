import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AuthErrorCodes, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDwollaEscrowCmsNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { BazaDwollaEscrowFixtures } from '../baza-dwolla-escrow.fixtures';
import { BazaDwollaEscrowFundingSourceDto, DwollaFundingSourceType } from '@scaliolabs/baza-dwolla-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-dwolla-api/dwolla-escrow/001-baza-dwolla-escrow.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessEscrow = new BazaDwollaEscrowCmsNodeAccess(http);

    let balanceFundingSource: BazaDwollaEscrowFundingSourceDto;
    let bankFundingSource: BazaDwollaEscrowFundingSourceDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaDwollaEscrowFixtures.BazaDwollaEscrowFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will display that Account Balance FS is available to use', async () => {
        const response = await dataAccessEscrow.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const fundSource = response.find((next) => next.type === DwollaFundingSourceType.Balance);

        expect(fundSource).toBeDefined();

        balanceFundingSource = fundSource;
    });

    it('will display that at least 1 Bank Account FS is available to use', async () => {
        const response = await dataAccessEscrow.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const fundSource = response.find((next) => next.type === DwollaFundingSourceType.Bank);

        expect(fundSource).toBeDefined();

        bankFundingSource = fundSource;
    });

    it('will show that master balance is currently selected as escrow account', async () => {
        expect(balanceFundingSource.isSelectedAsEscrow).toBeTruthy();
    });

    it('will show that master balance and bank account can be selected as escrow account', async () => {
        expect(bankFundingSource.canBeSelectedAsEscrow).toBeTruthy();
        expect(balanceFundingSource.canBeSelectedAsEscrow).toBeTruthy();
    });

    it('will select bank account as escrow', async () => {
        const response = await dataAccessEscrow.set({
            dwollaFundingSourceId: bankFundingSource.dwollaFundingSourceId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.type).toBe(DwollaFundingSourceType.Bank);
        expect(response.isSelectedAsEscrow).toBeTruthy();
    });

    it('will display that bank account is selected as escrow in list', async () => {
        const response = await dataAccessEscrow.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const bankFS = response.find((next) => next.dwollaFundingSourceId === bankFundingSource.dwollaFundingSourceId);
        const balanceFS = response.find((next) => next.dwollaFundingSourceId === balanceFundingSource.dwollaFundingSourceId);

        expect(bankFS.isSelectedAsEscrow).toBeTruthy();
        expect(balanceFS.isSelectedAsEscrow).toBeFalsy();
    });

    it('will allow to select balance back as escrow', async () => {
        const response = await dataAccessEscrow.set({
            dwollaFundingSourceId: balanceFundingSource.dwollaFundingSourceId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.type).toBe(DwollaFundingSourceType.Balance);
        expect(response.isSelectedAsEscrow).toBeTruthy();
    });

    it('will display that balance is selected as escrow in list', async () => {
        const response = await dataAccessEscrow.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const bankFS = response.find((next) => next.dwollaFundingSourceId === bankFundingSource.dwollaFundingSourceId);
        const balanceFS = response.find((next) => next.dwollaFundingSourceId === balanceFundingSource.dwollaFundingSourceId);

        expect(bankFS.isSelectedAsEscrow).toBeFalsy();
        expect(balanceFS.isSelectedAsEscrow).toBeTruthy();
    });

    it('[SECURITY] it will not allow to fetch list of FS by user', async () => {
        await http.authE2eUser();

        const response: BazaError = (await dataAccessEscrow.list()) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthAdminRoleIsRequired);
    });

    it('[SECURITY] it will not allow to set escrow by user', async () => {
        await http.authE2eUser();

        const response: BazaError = (await await dataAccessEscrow.set({
            dwollaFundingSourceId: bankFundingSource.dwollaFundingSourceId,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(AuthErrorCodes.AuthAdminRoleIsRequired);
    });

    it('[SECURITY] keep everything unchanged', async () => {
        const response = await dataAccessEscrow.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const bankFS = response.find((next) => next.dwollaFundingSourceId === bankFundingSource.dwollaFundingSourceId);
        const balanceFS = response.find((next) => next.dwollaFundingSourceId === balanceFundingSource.dwollaFundingSourceId);

        expect(bankFS.isSelectedAsEscrow).toBeFalsy();
        expect(balanceFS.isSelectedAsEscrow).toBeTruthy();
    });
});
