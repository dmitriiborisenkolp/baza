import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import {
    BazaDwollaAcl,
    BazaDwollaCmsOpenApi,
    BazaDwollaEscrowCmsEndpoint,
    BazaDwollaEscrowCmsEndpointPaths,
    BazaDwollaEscrowCmsSetRequest,
    BazaDwollaEscrowFundingSourceDto,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaEscrowService } from '../services/baza-dwolla-escrow.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaDwollaCmsOpenApi.BazaDwollaCmsEscrow)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaDwollaAcl.BazaDwollaEscrow])
export class BazaDwollaEscrowCmsController implements BazaDwollaEscrowCmsEndpoint {
    constructor(private readonly service: BazaDwollaEscrowService) {}

    @Post(BazaDwollaEscrowCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of available Dwolla Funding Sources',
    })
    @ApiOkResponse({
        type: BazaDwollaEscrowFundingSourceDto,
        isArray: true,
    })
    async list(): Promise<Array<BazaDwollaEscrowFundingSourceDto>> {
        return this.service.list();
    }

    @Post(BazaDwollaEscrowCmsEndpointPaths.set)
    @ApiOperation({
        summary: 'set',
        description:
            'Sets Dwolla Funding Source as Default Escrow Funding Source. \n' +
            'Production environments will not allow Account Balance as Escrow FS. \n' +
            'UAT environments will not allow Bank Account as Escrow FS',
    })
    @ApiOkResponse({
        type: BazaDwollaEscrowFundingSourceDto,
    })
    async set(@Body() request: BazaDwollaEscrowCmsSetRequest): Promise<BazaDwollaEscrowFundingSourceDto> {
        return this.service.set(request);
    }
}
