import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaEscrowErrorCodes, bazaDwollaEscrowErrorCodesEnI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaEscrowUatRestrictionsException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaEscrowErrorCodes.BazaDwollaEscrowUATRestricts,
            bazaDwollaEscrowErrorCodesEnI18n[BazaDwollaEscrowErrorCodes.BazaDwollaEscrowUATRestricts],
            HttpStatus.BAD_REQUEST,
        );
    }
}
