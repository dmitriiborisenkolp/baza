import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaEscrowErrorCodes, bazaDwollaEscrowErrorCodesEnI18n, DwollaFundingSourceType } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaEscrowUnsupportedTypeException extends BazaAppException {
    constructor(type: DwollaFundingSourceType) {
        super(
            BazaDwollaEscrowErrorCodes.BazaDwollaEscrowUnsupportedType,
            bazaDwollaEscrowErrorCodesEnI18n[BazaDwollaEscrowErrorCodes.BazaDwollaEscrowUnsupportedType],
            HttpStatus.BAD_REQUEST,
            { type },
        );
    }
}
