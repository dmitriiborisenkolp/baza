import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaEscrowErrorCodes, bazaDwollaEscrowErrorCodesEnI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaEscrowNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaEscrowErrorCodes.BazaDwollaEscrowNotFound,
            bazaDwollaEscrowErrorCodesEnI18n[BazaDwollaEscrowErrorCodes.BazaDwollaEscrowNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
