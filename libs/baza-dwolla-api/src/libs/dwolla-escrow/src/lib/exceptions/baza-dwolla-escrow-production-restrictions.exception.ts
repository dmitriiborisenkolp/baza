import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaEscrowErrorCodes, bazaDwollaEscrowErrorCodesEnI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaEscrowProductionRestrictionsException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaEscrowErrorCodes.BazaDwollaEscrowProductionRestricts,
            bazaDwollaEscrowErrorCodesEnI18n[BazaDwollaEscrowErrorCodes.BazaDwollaEscrowProductionRestricts],
            HttpStatus.BAD_REQUEST,
        );
    }
}
