import { Global, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaDwollaPaymentService } from './services/baza-dwolla-payment.service';
import { BazaDwollaPaymentRepository } from './repositories/baza-dwolla-payment.repository';
import { BazaDwollaApiModule } from '../../../dwolla-api/src';
import { BazaDwollaMasterAccountApiModule } from '../../../dwolla-master-account/src';
import { BazaDwollaCustomerApiModule } from '../../../dwolla-customer/src';
import { TransferCancelledEventHandler } from './event-handlers/transfer-cancelled.event-handler';
import { TransferCompletedEventHandler } from './event-handlers/transfer-completed.event-handler';
import { TransferFailedEventHandler } from './event-handlers/transfer-failed.event-handler';
import { BazaDwollaPaymentController } from './controllers/baza-dwolla-payment.controller';
import { BazaDwollaPaymentCmsController } from './controllers/baza-dwolla-payment-cms.controller';
import { BazaDwollaPaymentMapper } from './mappers/baza-dwolla-payment.mapper';
import { BazaDwollaPaymentCmsMapper } from './mappers/baza-dwolla-payment-cms.mapper';
import { BazaDwollaPaymentCmsService } from './services/baza-dwolla-payment-cms.service';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentRequestWithdrawalCommandHandler } from './command-handlers/baza-dwolla-payment-request-withdrawal.command-handler';
import { BazaDwollaPaymentReprocessWithdrawalCommandHandler } from './command-handlers/baza-dwolla-payment-reprocess-withdrawal.command-handler';
import { BazaDwollaPaymentRequestCashInCommandHandler } from './command-handlers/baza-dwolla-payment-request-cash-in.command-handler';
import { BazaDwollaPaymentReprocessCashInCommandHandler } from './command-handlers/baza-dwolla-payment-reprocess-cash-in.command-handler';
import { BankTransferCreatedEventHandler } from './event-handlers/bank-transfer-created.event-handler';
import { BankTransferCancelledEventHandler } from './event-handlers/bank-transfer-cancelled.event-handler';
import { BankTransferFailedEventHandler } from './event-handlers/bank-transfer-failed.event-handler';
import { BankTransferCompletedEventHandler } from './event-handlers/bank-transfer-completed.event-handler';
import { BazaDwollaEscrowApiModule } from '../../../dwolla-escrow/src';

const EVENT_HANDLERS = [
    TransferCancelledEventHandler,
    TransferCompletedEventHandler,
    TransferFailedEventHandler,
    BankTransferCreatedEventHandler,
    BankTransferCancelledEventHandler,
    BankTransferFailedEventHandler,
    BankTransferCompletedEventHandler,
];

const COMMAND_HANDLERS = [
    BazaDwollaPaymentRequestWithdrawalCommandHandler,
    BazaDwollaPaymentReprocessWithdrawalCommandHandler,
    BazaDwollaPaymentRequestCashInCommandHandler,
    BazaDwollaPaymentReprocessCashInCommandHandler,
];

@Global()
@Module({
    controllers: [BazaDwollaPaymentController, BazaDwollaPaymentCmsController],
    imports: [
        CqrsModule,
        BazaDwollaApiModule,
        BazaDwollaMasterAccountApiModule,
        BazaDwollaCustomerApiModule,
        BazaDwollaEscrowApiModule,
        BazaCrudApiModule,
    ],
    providers: [
        ...EVENT_HANDLERS,
        ...COMMAND_HANDLERS,
        BazaDwollaPaymentRepository,
        BazaDwollaPaymentService,
        BazaDwollaPaymentMapper,
        BazaDwollaPaymentCmsMapper,
        BazaDwollaPaymentCmsService,
    ],
    exports: [
        BazaDwollaPaymentRepository,
        BazaDwollaPaymentService,
        BazaDwollaPaymentMapper,
        BazaDwollaPaymentCmsMapper,
        BazaDwollaPaymentCmsService,
    ],
})
export class BazaDwollaPaymentApiModule {}
