import { Injectable } from '@nestjs/common';
import { CrudService } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentCmsMapper } from '../mappers/baza-dwolla-payment-cms.mapper';
import { BazaDwollaPaymentCmsDto, BazaDwollaPaymentCmsListRequest, BazaDwollaPaymentCmsListResponse } from '@scaliolabs/baza-dwolla-shared';
import { BAZA_NC_DWOLLA_PAYMENT_ENTITY_RELATIONS, BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';
import { Between, FindManyOptions, ILike } from 'typeorm';
import { convertToCents } from '@scaliolabs/baza-nc-shared';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import * as moment from 'moment';
import { postgresLikeEscape } from '@scaliolabs/baza-core-shared';

/**
 * Dwolla Payment CMS Service
 */
@Injectable()
export class BazaDwollaPaymentCmsService {
    constructor(private readonly crud: CrudService, private readonly mapper: BazaDwollaPaymentCmsMapper) {}

    /**
     * Returns list of Dwolla Payments (CMS)
     * @param request
     */
    async list(request: BazaDwollaPaymentCmsListRequest): Promise<BazaDwollaPaymentCmsListResponse> {
        return this.crud.find<BazaDwollaPaymentEntity, BazaDwollaPaymentCmsDto>({
            request,
            entity: BazaDwollaPaymentEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: this.findOptionsFactory(request),
        });
    }

    private findOptionsFactory(request: BazaDwollaPaymentCmsListRequest): FindManyOptions<BazaDwollaPaymentEntity> {
        const where: Array<FindConditions<BazaDwollaPaymentEntity>> = [];

        const findOptions: FindManyOptions<BazaDwollaPaymentEntity> = {
            order: {
                ulid: 'DESC',
            },
            relations: BAZA_NC_DWOLLA_PAYMENT_ENTITY_RELATIONS,
            where,
        };

        if (request.queryString) {
            if (!isNaN(parseFloat(request.queryString))) {
                where.push({
                    amountCents: convertToCents(parseFloat(request.queryString)),
                });
            } else {
                where.push(
                    ...[
                        {
                            ulid: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                        },
                        {
                            type: ILike(`%${postgresLikeEscape(request.queryString)}%` as any),
                        },
                        {
                            correlationId: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                        },
                        {
                            dwollaTransferId: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                        },
                    ],
                );
            }

            findOptions.where = where;
        }

        if ((request.dateTo || request.dateFrom) && !where.length) {
            where.push({});
        }

        if (request.dateTo && request.dateFrom) {
            for (const condition of where) {
                condition.dateCreatedAt = Between(
                    moment(request.dateFrom)
                        .set({
                            hours: 0,
                            minutes: 0,
                            seconds: 0,
                            milliseconds: 0,
                        })
                        .toDate(),
                    moment(request.dateTo)
                        .set({
                            hours: 23,
                            minutes: 59,
                            seconds: 59,
                            milliseconds: 999,
                        })
                        .toDate(),
                );
            }
        }

        return findOptions;
    }
}
