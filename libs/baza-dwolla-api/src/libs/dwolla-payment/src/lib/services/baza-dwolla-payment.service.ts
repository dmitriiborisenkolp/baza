import { Injectable } from '@nestjs/common';
import { BazaDwollaPaymentTransferRequest } from '../models/baza-dwolla-payment-transfer-request';
import { BazaDwollaPaymentUnknownTypeException } from '../exceptions/baza-dwolla-payment-unknown-type.exception';
import {
    bazaDwollaCorrelationId,
    bazaDwollaExtractId,
    BazaDwollaPaymentalListRequest,
    BazaDwollaPaymentalListResponse,
    BazaDwollaPaymentCashInPayload,
    BazaDwollaPaymentChannel,
    BazaDwollaPaymentCmsFetchAchDetailsRequest,
    BazaDwollaPaymentCmsFetchAchDetailsResponse,
    BazaDwollaPaymentDto,
    BazaDwollaPaymentHistoryAction,
    BazaDwollaPaymentPurchaseSharesPayload,
    BazaDwollaPaymentStatus,
    BazaDwollaPaymentType,
    BazaDwollaPaymentWithdrawalPayload,
    bazaDwollaToPaymentStatus,
    DwollaCurrency,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentInsufficientFundsException } from '../exceptions/baza-dwolla-payment-insufficient-funds.exception';
import { AccountEntity, BazaAccountRepository, CrudService } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { BAZA_NC_DWOLLA_PAYMENT_ENTITY_RELATIONS, BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';
import { convertFromCents, convertToCents } from '@scaliolabs/baza-nc-shared';
import { BazaDwollaMasterAccountService } from '../../../../dwolla-master-account/src';
import { DwollaTransfersApiNestjsService } from '../../../../dwolla-api/src';
import { BazaDwollaCustomerService } from '../../../../dwolla-customer/src';
import { CommandBus, EventBus } from '@nestjs/cqrs';
import { DwollaPaymentCreatedEvent } from '../events/dwolla-payment-created.event';
import { BazaDwollaPaymentMapper } from '../mappers/baza-dwolla-payment.mapper';
import { BazaDwollaPaymentUnknownStatusException } from '../exceptions/baza-dwolla-payment-unknown-status.exception';
import { DwollaPaymentCancelledEvent } from '../events/dwolla-payment-cancelled.event';
import { DwollaPaymentCompletedEvent } from '../events/dwolla-payment-completed.event';
import { DwollaPaymentFailedEvent } from '../events/dwolla-payment-failed.event';
import {
    BazaDwollaPaymentRequestWithdrawalCommand,
    BazaDwollaPaymentRequestWithdrawalCommandResult,
} from '../commands/baza-dwolla-payment-request-withdrawal.command';
import { BazaDwollaPaymentReprocessRequest } from '../models/baza-dwolla-payment-reprocess-request';
import { BazaDwollaPaymentNotFoundException } from '../exceptions/baza-dwolla-payment-not-found.exception';
import { BazaDwollaPaymentReprocessNotPermitted } from '../exceptions/baza-nc-dwolla-payment-reprocess-not-permitted.exception';
import {
    BazaDwollaPaymentReprocessWithdrawalCommand,
    BazaDwollaPaymentReprocessWithdrawalCommandResult,
} from '../commands/baza-dwolla-payment-reprocess-withdrawal.command';
import {
    BazaDwollaPaymentRequestCashInCommand,
    BazaDwollaPaymentRequestCashInCommandResult,
} from '../commands/baza-dwolla-payment-request-cashIn.command';
import {
    BazaDwollaPaymentReprocessCashInCommand,
    BazaDwollaPaymentReprocessCashInCommandResult,
} from '../commands/baza-dwolla-payment-reprocess-cashIn.command';
import { BazaDwollaEscrowService } from '../../../../dwolla-escrow/src';
import { BazaDwollaPaymentEscrowNotConfiguredException } from '../exceptions/baza-dwolla-payment-escrow-not-configured.exception';
import { BazaDwollaPaymentIsNotAchException } from '../exceptions/baza-dwolla-payment-is-not-ach.exception';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { In } from 'typeorm';

/**
 * Dwolla Payment Service
 * Allows transfer funds depends on Request Type
 * @see BazaDwollaPaymentTransferRequest
 * @see BazaDwollaPaymentType
 */
@Injectable()
export class BazaDwollaPaymentService {
    constructor(
        private readonly crud: CrudService,
        private readonly eventBus: EventBus,
        private readonly commandBus: CommandBus,
        private readonly mapper: BazaDwollaPaymentMapper,
        private readonly repository: BazaDwollaPaymentRepository,
        private readonly accountRepository: BazaAccountRepository,
        private readonly dwollaMasterAccount: BazaDwollaMasterAccountService,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
        private readonly dwollaCustomerService: BazaDwollaCustomerService,
        private readonly dwollaEscrowService: BazaDwollaEscrowService,
    ) {}

    /**
     * Returns Dwolla Payment by ULID
     * @param ulid
     */
    async getByUlid(ulid: string): Promise<BazaDwollaPaymentEntity> {
        return this.repository.getByUlid(ulid);
    }

    /**
     * Returns list of Dwolla Payments
     * @param accountId
     * @param request
     */
    async list(accountId: number, request: BazaDwollaPaymentalListRequest): Promise<BazaDwollaPaymentalListResponse> {
        const where: Array<FindConditions<BazaDwollaPaymentEntity>> = [
            {
                account: accountId as unknown,
            },
        ];

        if (request.status) {
            where.forEach((next) => (next.status = In(request.status)));
        }

        if (request.types) {
            where.forEach((next) => (next.type = In(request.types)));
        }

        if (request.ncTradeId) {
            where[0]['ncTradeId'] = request.ncTradeId;
        }

        return this.crud.find<BazaDwollaPaymentEntity, BazaDwollaPaymentDto>({
            request,
            entity: BazaDwollaPaymentEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                where,
                order: {
                    ulid: 'DESC',
                },
                relations: BAZA_NC_DWOLLA_PAYMENT_ENTITY_RELATIONS,
            },
        });
    }

    /**
     * Sync Dwolla Payment (Updates actual status of Dwolla Payment)
     * @param ulid
     * @param options
     */
    async sync(
        ulid: string,
        options = {
            triggerEvents: true,
        },
    ): Promise<BazaDwollaPaymentEntity> {
        const payment = await this.repository.getByUlid(ulid);

        if (payment.channel === BazaDwollaPaymentChannel.ACH && (!payment.achDwollaTransferId || !payment.individualAchId)) {
            const fetchResponse = await this.fetchAchDetails({
                dwollaTransferId: payment.dwollaTransferId,
            });

            Object.assign(payment, fetchResponse);
        }

        const statusDwollaTransferId =
            payment.channel === BazaDwollaPaymentChannel.ACH ? payment.achDwollaTransferId : payment.dwollaTransferId;

        if (!statusDwollaTransferId) {
            return payment;
        }

        const response = await this.dwollaTransferApi.getTransfer(statusDwollaTransferId);
        const status = bazaDwollaToPaymentStatus.find((next) => next.dwolla === response.status)?.status;

        if (!status) {
            throw new BazaDwollaPaymentUnknownStatusException(status);
        }

        if (payment.status !== status) {
            payment.status = status;

            await this.repository.save(payment);

            if (options.triggerEvents) {
                switch (status) {
                    case BazaDwollaPaymentStatus.Cancelled: {
                        this.eventBus.publish(
                            new DwollaPaymentCancelledEvent({
                                payment,
                            }),
                        );

                        break;
                    }

                    case BazaDwollaPaymentStatus.Processed: {
                        this.eventBus.publish(
                            new DwollaPaymentCompletedEvent({
                                payment,
                            }),
                        );

                        break;
                    }

                    case BazaDwollaPaymentStatus.Failed: {
                        this.eventBus.publish(
                            new DwollaPaymentFailedEvent({
                                payment,
                            }),
                        );

                        break;
                    }
                }
            }
        }

        return payment;
    }

    /**
     * Fetches and saves details of related Dwolla ACH Transfer
     * @param request
     */
    async fetchAchDetails(request: BazaDwollaPaymentCmsFetchAchDetailsRequest): Promise<BazaDwollaPaymentCmsFetchAchDetailsResponse> {
        const payment = await this.repository.getByDwollaTransferId(request.dwollaTransferId);

        if (payment.channel !== BazaDwollaPaymentChannel.ACH) {
            throw new BazaDwollaPaymentIsNotAchException();
        }

        const transfer = await this.dwollaTransferApi.getTransfer(payment.dwollaTransferId);
        const linkedFunded = transfer._links['funded-transfer'];

        if (linkedFunded) {
            const linkedFundedTransferId = bazaDwollaExtractId(linkedFunded.href);
            const linkedFundedTransfer = await this.dwollaTransferApi.getTransfer(linkedFundedTransferId);

            payment.individualAchId = linkedFundedTransfer.individualAchId || payment.individualAchId;
            payment.achDwollaTransferId = linkedFundedTransferId;

            await this.repository.save(payment);
        }

        return {
            individualAchId: payment.individualAchId,
            achDwollaTransferId: payment.achDwollaTransferId,
        };
    }

    /**
     * Transfers Funds
     * @see BazaDwollaPaymentTransferRequest
     * @param request
     */
    async transfer(request: BazaDwollaPaymentTransferRequest): Promise<BazaDwollaPaymentEntity> {
        const type = request.payload.type;
        const account = await this.accountRepository.getActiveOrDeactivatedAccountWithId(request.accountId);

        switch (type) {
            default: {
                throw new BazaDwollaPaymentUnknownTypeException(type);
            }

            case BazaDwollaPaymentType.PurchaseShares: {
                return this.transferFromAccountBalanceToEscrow(request, account);
            }

            case BazaDwollaPaymentType.Dividend: {
                return this.transferFromMasterBalanceToAccountBalance(request, account);
            }

            case BazaDwollaPaymentType.Withdrawal: {
                return this.withdrawAccountBalanceToBankAccount(request, account);
            }

            case BazaDwollaPaymentType.CashIn: {
                return this.requestCashInTransfer(request, account);
            }
        }
    }

    /**
     * Reprocess different transfer types
     * @see BazaDwollaPaymentReprocessRequest
     * @param request
     */
    async reprocess(request: BazaDwollaPaymentReprocessRequest): Promise<BazaDwollaPaymentEntity> {
        const entity = await this.repository.findByUlid(request.ulid);
        const account = await this.accountRepository.getActiveOrDeactivatedAccountWithId(request.accountId);

        if (!entity || entity.account.id !== account.id) {
            throw new BazaDwollaPaymentNotFoundException();
        }

        switch (entity.type) {
            default: {
                throw new BazaDwollaPaymentReprocessNotPermitted();
            }

            case BazaDwollaPaymentType.Withdrawal: {
                return this.reprocessWithdrawalRequest(request, account);
            }

            case BazaDwollaPaymentType.CashIn: {
                return this.reprocessCashInRequest(request, account);
            }
        }
    }

    /**
     * Transfers Funds from Dwolla Customer Balance to Master Account Balance
     * @param request
     * @param account
     * @private
     */
    private async transferFromAccountBalanceToMasterBalance(
        request: BazaDwollaPaymentTransferRequest,
        account: AccountEntity,
    ): Promise<BazaDwollaPaymentEntity> {
        const accountBalance = await this.dwollaCustomerService.getBalance(request.dwollaCustomerId);

        if (!accountBalance.isDwollaAvailable || parseFloat(accountBalance.balance.value) < convertFromCents(request.amountCents)) {
            throw new BazaDwollaPaymentInsufficientFundsException();
        }

        const accountFundingSource = await this.dwollaCustomerService.getBalanceFundingSource(request.dwollaCustomerId);
        const masterBalanceFundingSource = await this.dwollaMasterAccount.balanceFundingSource();

        const { dwollaTransferId } = await this.dwollaTransferApi.initiateTransfer({
            _links: {
                source: {
                    href: accountFundingSource._links.self.href,
                },
                destination: {
                    href: masterBalanceFundingSource._links.self.href,
                },
            },
            amount: {
                currency: DwollaCurrency.USD,
                value: convertFromCents(request.amountCents).toFixed(2),
            },
        });

        const entity = new BazaDwollaPaymentEntity();

        entity.correlationId = bazaDwollaCorrelationId(request.payload);
        entity.type = request.payload.type;
        entity.account = account;
        entity.amountCents = request.amountCents;
        entity.dwollaTransferId = dwollaTransferId;
        entity.payload = request.payload;
        entity.history = [
            {
                date: new Date().toISOString(),
                status: BazaDwollaPaymentStatus.Pending,
                action: {
                    type: BazaDwollaPaymentHistoryAction.DwollaTransferInitiated,
                    dwollaTransferId,
                    dwollaCustomerId: request.dwollaCustomerId,
                },
            },
        ];

        await this.repository.save(entity);

        this.eventBus.publish(
            new DwollaPaymentCreatedEvent({
                payment: entity,
            }),
        );

        return entity;
    }

    /**
     * Transfers Funds from Dwolla Customer Balance to Escrow
     * @param request
     * @param account
     * @private
     */
    private async transferFromAccountBalanceToEscrow(
        request: BazaDwollaPaymentTransferRequest,
        account: AccountEntity,
    ): Promise<BazaDwollaPaymentEntity> {
        const accountBalance = await this.dwollaCustomerService.getBalance(request.dwollaCustomerId);

        if (!accountBalance.isDwollaAvailable || parseFloat(accountBalance.balance.value) < convertFromCents(request.amountCents)) {
            throw new BazaDwollaPaymentInsufficientFundsException();
        }

        if (!(await this.dwollaEscrowService.isProperlyConfigured())) {
            throw new BazaDwollaPaymentEscrowNotConfiguredException();
        }

        const accountFundingSource = await this.dwollaCustomerService.getBalanceFundingSource(request.dwollaCustomerId);
        const escrowFundingSource = await this.dwollaEscrowService.currentFundingSourceHref();

        const { dwollaTransferId } = await this.dwollaTransferApi.initiateTransfer({
            _links: {
                source: {
                    href: accountFundingSource._links.self.href,
                },
                destination: {
                    href: escrowFundingSource,
                },
            },
            amount: {
                currency: DwollaCurrency.USD,
                value: convertFromCents(request.amountCents).toFixed(2),
            },
        });

        const entity = new BazaDwollaPaymentEntity();

        entity.correlationId = bazaDwollaCorrelationId(request.payload);
        entity.ncTradeId = (request.payload as BazaDwollaPaymentPurchaseSharesPayload).ncTradeId;
        entity.type = request.payload.type;
        entity.channel = this.dwollaEscrowService.isBankAccount ? BazaDwollaPaymentChannel.ACH : BazaDwollaPaymentChannel.Balance;
        entity.account = account;
        entity.amountCents = request.amountCents;
        entity.dwollaTransferId = dwollaTransferId;
        entity.payload = request.payload;
        entity.history = [
            {
                date: new Date().toISOString(),
                status: BazaDwollaPaymentStatus.Pending,
                action: {
                    type: BazaDwollaPaymentHistoryAction.DwollaTransferInitiated,
                    dwollaTransferId,
                    dwollaCustomerId: request.dwollaCustomerId,
                },
            },
        ];

        await this.repository.save(entity);

        this.eventBus.publish(
            new DwollaPaymentCreatedEvent({
                payment: entity,
            }),
        );

        return entity;
    }

    /**
     * Transfers Funds from Master Account Balance to Dwolla Customer Balance
     * @param request
     * @param account
     * @private
     */
    private async transferFromMasterBalanceToAccountBalance(
        request: BazaDwollaPaymentTransferRequest,
        account: AccountEntity,
    ): Promise<BazaDwollaPaymentEntity> {
        const masterAccountBalance = await this.dwollaMasterAccount.balance();

        if (parseFloat(masterAccountBalance.value) < convertFromCents(request.amountCents)) {
            throw new BazaDwollaPaymentInsufficientFundsException();
        }

        const accountFundingSource = await this.dwollaCustomerService.getBalanceFundingSource(request.dwollaCustomerId);
        const masterBalanceFundingSource = await this.dwollaMasterAccount.balanceFundingSource();

        const { dwollaTransferId } = await this.dwollaTransferApi.initiateTransfer({
            _links: {
                source: {
                    href: masterBalanceFundingSource._links.self.href,
                },
                destination: {
                    href: accountFundingSource._links.self.href,
                },
            },
            amount: {
                currency: DwollaCurrency.USD,
                value: convertFromCents(request.amountCents).toFixed(2),
            },
        });

        const entity = new BazaDwollaPaymentEntity();

        entity.correlationId = bazaDwollaCorrelationId(request.payload);
        entity.type = request.payload.type;
        entity.channel = BazaDwollaPaymentChannel.Balance;
        entity.account = account;
        entity.amountCents = request.amountCents;
        entity.dwollaTransferId = dwollaTransferId;
        entity.payload = request.payload;
        entity.history = [
            {
                date: new Date().toISOString(),
                status: BazaDwollaPaymentStatus.Pending,
                action: {
                    type: BazaDwollaPaymentHistoryAction.DwollaTransferInitiated,
                    dwollaTransferId,
                    dwollaCustomerId: request.dwollaCustomerId,
                },
            },
        ];

        await this.repository.save(entity);

        this.eventBus.publish(
            new DwollaPaymentCreatedEvent({
                payment: entity,
            }),
        );

        return entity;
    }

    /**
     *
     * @param request
     * @param account
     * @private
     */
    private async withdrawAccountBalanceToBankAccount(
        request: BazaDwollaPaymentTransferRequest,
        account: AccountEntity,
    ): Promise<BazaDwollaPaymentEntity> {
        const payload = request.payload as BazaDwollaPaymentWithdrawalPayload;
        const { amount, correlationId, dwollaTransferId }: BazaDwollaPaymentRequestWithdrawalCommandResult = await this.commandBus.execute(
            new BazaDwollaPaymentRequestWithdrawalCommand(
                {
                    amount: payload.amount,
                    dwollaCustomerId: request.dwollaCustomerId,
                    cashOutFundingSourceId: payload.cashOutFundingSourceId,
                },
                account,
            ),
        );

        const entity = new BazaDwollaPaymentEntity();

        entity.correlationId = correlationId;
        entity.type = request.payload.type;
        entity.channel = BazaDwollaPaymentChannel.Balance;
        entity.account = account;
        entity.amountCents = convertToCents(parseFloat(amount));
        entity.dwollaTransferId = dwollaTransferId;
        entity.payload = request.payload;
        entity.history = [
            {
                date: new Date().toISOString(),
                status: BazaDwollaPaymentStatus.Pending,
                action: {
                    type: BazaDwollaPaymentHistoryAction.DwollaTransferInitiated,
                    dwollaTransferId,
                    dwollaCustomerId: request.dwollaCustomerId,
                },
            },
        ];

        await this.repository.save(entity);

        this.eventBus.publish(
            new DwollaPaymentCreatedEvent({
                payment: entity,
            }),
        );

        return entity;
    }

    /**
     * Reprocess withdraw request
     * @param request
     * @param account
     */
    private async reprocessWithdrawalRequest(
        request: BazaDwollaPaymentReprocessRequest,
        account: AccountEntity,
    ): Promise<BazaDwollaPaymentEntity> {
        const { cashOutFundingSourceId } = request.payload;

        return this.commandBus.execute<BazaDwollaPaymentReprocessWithdrawalCommand, BazaDwollaPaymentReprocessWithdrawalCommandResult>(
            new BazaDwollaPaymentReprocessWithdrawalCommand(
                {
                    ulid: request.ulid,
                    dwollaCustomerId: request.dwollaCustomerId,
                    cashOutFundingSourceId: cashOutFundingSourceId,
                },
                account,
            ),
        );
    }

    /**
     * Requests to transfer cash from bank account to user dwolla balance
     * @param request
     * @param account
     * @returns
     */
    private async requestCashInTransfer(
        request: BazaDwollaPaymentTransferRequest,
        account: AccountEntity,
    ): Promise<BazaDwollaPaymentEntity> {
        const payload = request.payload as BazaDwollaPaymentCashInPayload;
        const { amount, correlationId, dwollaTransferId }: BazaDwollaPaymentRequestCashInCommandResult = await this.commandBus.execute(
            new BazaDwollaPaymentRequestCashInCommand(
                {
                    amount: payload.amount,
                    dwollaCustomerId: request.dwollaCustomerId,
                    cashInfundingSourceId: payload.cashInFundingSourceId,
                },
                account,
            ),
        );

        const entity = new BazaDwollaPaymentEntity();

        entity.correlationId = correlationId;
        entity.type = request.payload.type;
        entity.channel = BazaDwollaPaymentChannel.Balance;
        entity.account = account;
        entity.amountCents = convertToCents(parseFloat(amount));
        entity.dwollaTransferId = dwollaTransferId;
        entity.payload = request.payload;
        entity.history = [
            {
                date: new Date().toISOString(),
                status: BazaDwollaPaymentStatus.Pending,
                action: {
                    type: BazaDwollaPaymentHistoryAction.DwollaTransferInitiated,
                    dwollaTransferId,
                    dwollaCustomerId: request.dwollaCustomerId,
                },
            },
        ];

        await this.repository.save(entity);

        this.eventBus.publish(
            new DwollaPaymentCreatedEvent({
                payment: entity,
            }),
        );

        return entity;
    }

    /**
     * will execute reprocess Cash In command
     * @param request
     * @param account
     */
    private async reprocessCashInRequest(
        request: BazaDwollaPaymentReprocessRequest,
        account: AccountEntity,
    ): Promise<BazaDwollaPaymentEntity> {
        const { cashInfundingSourceId } = request.payload;

        return this.commandBus.execute<BazaDwollaPaymentReprocessCashInCommand, BazaDwollaPaymentReprocessCashInCommandResult>(
            new BazaDwollaPaymentReprocessCashInCommand(
                {
                    ulid: request.ulid,
                    dwollaCustomerId: request.dwollaCustomerId,
                    cashInfundingSourceId,
                },
                account,
            ),
        );
    }
}
