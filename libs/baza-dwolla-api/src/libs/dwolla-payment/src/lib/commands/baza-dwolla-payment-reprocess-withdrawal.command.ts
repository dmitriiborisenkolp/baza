import { AccountEntity } from '@scaliolabs/baza-core-api';
import { DwollaCustomerId } from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';

type CommandRequest = {
    ulid: string;
    dwollaCustomerId: DwollaCustomerId;
    cashOutFundingSourceId: string;
};

export class BazaDwollaPaymentReprocessWithdrawalCommand {
    constructor(public readonly request: CommandRequest, public readonly account: AccountEntity) {}
}

export type BazaDwollaPaymentReprocessWithdrawalCommandResult = BazaDwollaPaymentEntity;
