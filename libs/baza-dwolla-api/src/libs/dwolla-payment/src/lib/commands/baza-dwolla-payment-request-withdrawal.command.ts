import { AccountEntity } from '@scaliolabs/baza-core-api';

type CommandRequest = {
    amount?: string;
    dwollaCustomerId: string;
    cashOutFundingSourceId: string;
};

export class BazaDwollaPaymentRequestWithdrawalCommand {
    constructor(public readonly request: CommandRequest, public readonly account: AccountEntity) {}
}

export type BazaDwollaPaymentRequestWithdrawalCommandResult = {
    amount: string;
    dwollaTransferId: string;
    correlationId: string;
};
