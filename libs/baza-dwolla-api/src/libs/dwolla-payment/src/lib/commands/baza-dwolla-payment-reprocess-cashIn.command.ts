import { AccountEntity } from '@scaliolabs/baza-core-api';
import { DwollaCustomerId } from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';

type CommandRequest = {
    ulid: string;
    dwollaCustomerId: DwollaCustomerId;
    cashInfundingSourceId: string;
};

export class BazaDwollaPaymentReprocessCashInCommand {
    constructor(public readonly request: CommandRequest, public readonly account: AccountEntity) {}
}

export type BazaDwollaPaymentReprocessCashInCommandResult = BazaDwollaPaymentEntity;
