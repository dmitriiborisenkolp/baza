import { AccountEntity } from '@scaliolabs/baza-core-api';

type CommandRequest = {
    amount: string;
    dwollaCustomerId: string;
    cashInfundingSourceId: string;
};

export class BazaDwollaPaymentRequestCashInCommand {
    constructor(public readonly request: CommandRequest, public readonly account: AccountEntity) {}
}

export type BazaDwollaPaymentRequestCashInCommandResult = {
    amount: string;
    dwollaTransferId: string;
    correlationId: string;
};
