import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';

/**
 * The Event will be published when Dwolla Transfer is Created
 */
export class DwollaPaymentCreatedEvent {
    constructor(
        public readonly payload: {
            payment: BazaDwollaPaymentEntity;
        },
    ) {}
}
