import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';

/**
 * The Event will be published when ACH Transfer is initiated by Dwolla for recently created Dwolla Transfer
 */
export class DwollaPaymentAchInitiatedEvent {
    constructor(
        public readonly payload: {
            payment: BazaDwollaPaymentEntity;
        },
    ) {}
}
