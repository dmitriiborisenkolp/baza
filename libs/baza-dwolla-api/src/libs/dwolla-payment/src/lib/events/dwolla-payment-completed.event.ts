import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';

/**
 * The Event will be published when Dwolla Transfer is Completed (Successful)
 */
export class DwollaPaymentCompletedEvent {
    constructor(
        public readonly payload: {
            payment: BazaDwollaPaymentEntity;
        },
    ) {}
}
