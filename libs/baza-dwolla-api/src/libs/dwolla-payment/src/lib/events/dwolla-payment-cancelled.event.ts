import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';

/**
 * The Event will be published when Dwolla Transfer is Cancelled
 */
export class DwollaPaymentCancelledEvent {
    constructor(
        public readonly payload: {
            payment: BazaDwollaPaymentEntity;
        },
    ) {}
}
