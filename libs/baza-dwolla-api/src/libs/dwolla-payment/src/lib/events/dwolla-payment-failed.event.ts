import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';

/**
 * The Event will be published when Dwolla Transfer is Failed
 */
export class DwollaPaymentFailedEvent {
    constructor(
        public readonly payload: {
            payment: BazaDwollaPaymentEntity;
        },
    ) {}
}
