import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentErrorCodes, bazaDwollaPaymentErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaPaymentInsufficientFundsException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaPaymentErrorCodes.BazaDwollaPaymentNoInsufficientFunds,
            bazaDwollaPaymentErrorCodesI18n[BazaDwollaPaymentErrorCodes.BazaDwollaPaymentNoInsufficientFunds],
            HttpStatus.BAD_REQUEST,
        );
    }
}
