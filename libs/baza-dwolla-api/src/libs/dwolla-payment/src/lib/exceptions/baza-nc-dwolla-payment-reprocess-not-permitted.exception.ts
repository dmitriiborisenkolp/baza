import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaDwollaPaymentErrorCodes, bazaDwollaPaymentErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaPaymentReprocessNotPermitted extends BazaAppException {
    constructor() {
        super(
            BazaDwollaPaymentErrorCodes.BazaDwollaPaymentOperationNotPermitted,
            bazaDwollaPaymentErrorCodesI18n[BazaDwollaPaymentErrorCodes.BazaDwollaPaymentOperationNotPermitted],
            HttpStatus.FORBIDDEN,
        );
    }
}
