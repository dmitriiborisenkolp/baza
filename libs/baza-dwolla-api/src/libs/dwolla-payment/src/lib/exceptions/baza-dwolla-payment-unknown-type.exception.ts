import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentErrorCodes, bazaDwollaPaymentErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaPaymentUnknownTypeException extends BazaAppException {
    constructor(type: string) {
        super(
            BazaDwollaPaymentErrorCodes.BazaDwollaPaymentUnknownType,
            bazaDwollaPaymentErrorCodesI18n[BazaDwollaPaymentErrorCodes.BazaDwollaPaymentUnknownType],
            HttpStatus.BAD_REQUEST,
            { type },
        );
    }
}
