import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaDwollaPaymentErrorCodes, bazaDwollaPaymentErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaPaymentEscrowNotConfiguredException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaPaymentErrorCodes.BazaDwollaPaymentEscrowNotConfigured,
            bazaDwollaPaymentErrorCodesI18n[BazaDwollaPaymentErrorCodes.BazaDwollaPaymentEscrowNotConfigured],
            HttpStatus.BAD_REQUEST,
        );
    }
}
