import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentErrorCodes, bazaDwollaPaymentErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaPaymentUnknownStatusException extends BazaAppException {
    constructor(status: string) {
        super(
            BazaDwollaPaymentErrorCodes.BazaDwollaPaymentUnknownStatus,
            bazaDwollaPaymentErrorCodesI18n[BazaDwollaPaymentErrorCodes.BazaDwollaPaymentUnknownStatus],
            HttpStatus.INTERNAL_SERVER_ERROR,
            { status },
        );
    }
}
