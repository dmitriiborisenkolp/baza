import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaDwollaPaymentErrorCodes, bazaDwollaPaymentErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaPaymentIsNotAchException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaPaymentErrorCodes.BazaDwollaPaymentIsNotAchPayment,
            bazaDwollaPaymentErrorCodesI18n[BazaDwollaPaymentErrorCodes.BazaDwollaPaymentIsNotAchPayment],
            HttpStatus.BAD_REQUEST,
        );
    }
}
