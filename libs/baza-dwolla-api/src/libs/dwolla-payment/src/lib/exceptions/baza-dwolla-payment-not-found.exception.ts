import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentErrorCodes, bazaDwollaPaymentErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaPaymentNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaPaymentErrorCodes.BazaDwollaPaymentNotFound,
            bazaDwollaPaymentErrorCodesI18n[BazaDwollaPaymentErrorCodes.BazaDwollaPaymentNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
