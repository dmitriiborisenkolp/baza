import { Module } from '@nestjs/common';
import { BazaE2eFixturesApiModule } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentApiModule } from './baza-dwolla-payment-api.module';
import { BazaDwollaPaymentFixture } from './integration-tests/fixtures/baza-dwolla-payment.fixture';
import { BazaDwollaPaymentInitiatedFixture } from './integration-tests/fixtures/baza-dwolla-payment-initiated.fixture';
import { BazaDwollaPaymentSampleFixture } from './integration-tests/fixtures/baza-dwolla-payment-sample.fixture';

const E2E_FIXTURES = [BazaDwollaPaymentFixture, BazaDwollaPaymentInitiatedFixture, BazaDwollaPaymentSampleFixture];

@Module({
    imports: [BazaDwollaPaymentApiModule, BazaE2eFixturesApiModule],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaDwollaPaymentE2eModule {}
