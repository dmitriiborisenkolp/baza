import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '../../../../dwolla-webhook/src';
import {
    bazaDwollaExtractId,
    BazaDwollaPaymentChannel,
    BazaDwollaPaymentHistoryAction,
    BazaDwollaPaymentStatus,
    DwollaEventTopic,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { DwollaTransfersApiNestjsService } from '../../../../dwolla-api/src';
import { DwollaPaymentAchInitiatedEvent } from '../events/dwolla-payment-ach-initiated.event';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class BankTransferCreatedEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly repository: BazaDwollaPaymentRepository,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
    ) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(BankTransferCreatedEventHandler.name, async () => {
            if ([DwollaEventTopic.bank_transfer_created].includes(event.payload.responseBody.topic)) {
                const achTransferId = event.payload.responseBody.resourceId;
                const achTransfer = await this.dwollaTransferApi.getTransfer(achTransferId);

                if (!achTransfer._links['funding-transfer']) {
                    return;
                }

                const fundedTransferId = bazaDwollaExtractId(achTransfer._links['funding-transfer'].href);

                const payment = await this.repository.findByDwollaTransferId(fundedTransferId);

                if (!payment || payment.channel !== BazaDwollaPaymentChannel.ACH) {
                    return;
                }

                payment.achDwollaTransferId = achTransferId;
                payment.history.push({
                    date: new Date(event.payload.responseBody.created).toISOString(),
                    status: BazaDwollaPaymentStatus.Cancelled,
                    action: {
                        type: BazaDwollaPaymentHistoryAction.DwollaTransferAchInitiated,
                        dwollaTransferId: payment.dwollaTransferId,
                        achDwollaTransferId: payment.achDwollaTransferId,
                    },
                });
                payment.dateUpdatedAt = new Date();
                payment.status = BazaDwollaPaymentStatus.Pending;
                payment.failureReason = null;

                await this.repository.save(payment);

                this.eventBus.publish(
                    new DwollaPaymentAchInitiatedEvent({
                        payment,
                    }),
                );
            }
        });
    }
}
