import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '../../../../dwolla-webhook/src';
import {
    BazaDwollaErrorCodes,
    bazaDwollaErrorCodesEnI18n,
    BazaDwollaPaymentChannel,
    BazaDwollaPaymentFailureReasonDto,
    BazaDwollaPaymentHistoryAction,
    BazaDwollaPaymentStatus,
    DwollaEventTopic,
    dwollaFailureCodesI18n,
    DwollaTransferId,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { DwollaTransfersApiNestjsService } from '../../../../dwolla-api/src';
import { DwollaPaymentFailedEvent } from '../events/dwolla-payment-failed.event';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class TransferFailedEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly repository: BazaDwollaPaymentRepository,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
    ) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(TransferFailedEventHandler.name, async () => {
            /**
             * TODO: We should not look for both events here. Review here is required.
             */
            if (
                [DwollaEventTopic.transfer_failed, DwollaEventTopic.customer_bank_transfer_failed].includes(
                    event.payload.responseBody.topic,
                )
            ) {
                const transferId = event.payload.responseBody.resourceId;
                const payment = await this.repository.findByDwollaTransferId(transferId);

                if (!payment || payment.channel !== BazaDwollaPaymentChannel.Balance) {
                    return;
                }

                payment.history.push({
                    date: new Date(event.payload.responseBody.created).toISOString(),
                    status: BazaDwollaPaymentStatus.Failed,
                    action: {
                        type: BazaDwollaPaymentHistoryAction.DwollaTransferStatusUpdated,
                        previousStatus: payment.status,
                        newStatus: BazaDwollaPaymentStatus.Failed,
                    },
                });
                payment.dateUpdatedAt = new Date();
                payment.status = BazaDwollaPaymentStatus.Failed;
                payment.failureReason = await this.retrieveFailureReason(transferId);

                await this.repository.save(payment);

                this.eventBus.publish(
                    new DwollaPaymentFailedEvent({
                        payment,
                    }),
                );
            }
        });
    }

    /**
     * Retrieves Failure reason from Dwolla API
     * @param dwollaTransferId
     * @private
     */
    private async retrieveFailureReason(dwollaTransferId: DwollaTransferId): Promise<BazaDwollaPaymentFailureReasonDto> {
        try {
            const response = await this.dwollaTransferApi.getFailureReasonForTransfer(dwollaTransferId);
            const found = dwollaFailureCodesI18n.find((next) => next.code === response.code);

            return {
                code: response.code,
                reason: found?.reason,
                details: found?.details,
            };
        } catch (err) {
            return {
                code: BazaDwollaErrorCodes.BazaDwollaUnknownPaymentError,
                reason: bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaUnknownPaymentError],
            };
        }
    }
}
