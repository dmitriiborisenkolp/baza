import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '../../../../dwolla-webhook/src';
import {
    bazaDwollaExtractId,
    BazaDwollaPaymentChannel,
    BazaDwollaPaymentHistoryAction,
    BazaDwollaPaymentStatus,
    DwollaEventTopic,
    DwollaTransferStatus,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { DwollaPaymentCompletedEvent } from '../events/dwolla-payment-completed.event';
import { DwollaTransfersApiNestjsService } from '../../../../dwolla-api/src';
import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class TransferCompletedEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly repository: BazaDwollaPaymentRepository,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
    ) {}

    /**
     * Marks Payment as Processed
     * If Dwolla Transfer is processed using ACH, the Event Handler will first
     * check for 'funded-transfer' related transfer. If related transfer is not
     * processed yet, Dwolla Payment will not be marked as processed
     * @param event
     */
    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(TransferCompletedEventHandler.name, async () => {
            /**
             * TODO: We should not look for both events here. Review here is required.
             */
            if (
                [DwollaEventTopic.transfer_completed, DwollaEventTopic.customer_bank_transfer_completed].includes(
                    event.payload.responseBody.topic,
                )
            ) {
                const transferId = event.payload.responseBody.resourceId;
                const payment = await this.repository.findByDwollaTransferId(transferId);

                if (!payment || payment.channel !== BazaDwollaPaymentChannel.Balance) {
                    return;
                }

                const transfer = await this.dwollaTransferApi.getTransfer(payment.dwollaTransferId);

                if (transfer._links['funded-transfer']) {
                    const achTransfer = await this.dwollaTransferApi.getTransfer(
                        bazaDwollaExtractId(transfer._links['funded-transfer'].href),
                    );

                    if (achTransfer.status === DwollaTransferStatus.Processed) {
                        await this.markAsProcessed(payment, event);
                    }
                } else {
                    await this.markAsProcessed(payment, event);
                }
            }
        });
    }

    /**
     * Marks Payment as Processed
     * @param payment
     * @param event
     * @private
     */
    private async markAsProcessed(payment: BazaDwollaPaymentEntity, event: BazaKafkaDwollaWebhookEvent): Promise<void> {
        payment.history.push({
            date: new Date(event.payload.responseBody.created).toISOString(),
            status: BazaDwollaPaymentStatus.Processed,
            action: {
                type: BazaDwollaPaymentHistoryAction.DwollaTransferStatusUpdated,
                previousStatus: payment.status,
                newStatus: BazaDwollaPaymentStatus.Processed,
            },
        });
        payment.dateUpdatedAt = new Date();
        payment.dateProcessedAt = new Date();
        payment.status = BazaDwollaPaymentStatus.Processed;
        payment.failureReason = null;

        await this.repository.save(payment);

        this.eventBus.publish(
            new DwollaPaymentCompletedEvent({
                payment,
            }),
        );
    }
}
