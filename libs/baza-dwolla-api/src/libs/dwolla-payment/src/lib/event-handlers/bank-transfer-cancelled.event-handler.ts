import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '../../../../dwolla-webhook/src';
import {
    BazaDwollaPaymentChannel,
    BazaDwollaPaymentHistoryAction,
    BazaDwollaPaymentStatus,
    DwollaEventTopic,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { DwollaPaymentCancelledEvent } from '../events/dwolla-payment-cancelled.event';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class BankTransferCancelledEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(private readonly eventBus: EventBus, private readonly repository: BazaDwollaPaymentRepository) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(BankTransferCancelledEventHandler.name, async () => {
            if ([DwollaEventTopic.bank_transfer_cancelled].includes(event.payload.responseBody.topic)) {
                const transferId = event.payload.responseBody.resourceId;
                const payment = await this.repository.findByAchDwollaTransferId(transferId);

                if (!payment || payment.channel !== BazaDwollaPaymentChannel.ACH) {
                    return;
                }

                payment.history.push({
                    date: new Date(event.payload.responseBody.created).toISOString(),
                    status: BazaDwollaPaymentStatus.Cancelled,
                    action: {
                        type: BazaDwollaPaymentHistoryAction.DwollaTransferAchCancelled,
                        previousStatus: payment.status,
                        newStatus: BazaDwollaPaymentStatus.Cancelled,
                    },
                });
                payment.dateUpdatedAt = new Date();
                payment.status = BazaDwollaPaymentStatus.Cancelled;
                payment.failureReason = null;

                await this.repository.save(payment);

                this.eventBus.publish(
                    new DwollaPaymentCancelledEvent({
                        payment,
                    }),
                );
            }
        });
    }
}
