import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '../../../../dwolla-webhook/src';
import {
    BazaDwollaPaymentChannel,
    BazaDwollaPaymentHistoryAction,
    BazaDwollaPaymentStatus,
    DwollaEventTopic,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { DwollaPaymentCompletedEvent } from '../events/dwolla-payment-completed.event';
import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';
import { DwollaTransfersApiNestjsService } from '../../../../dwolla-api/src';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class BankTransferCompletedEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly repository: BazaDwollaPaymentRepository,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
    ) {}

    /**
     * Marks Payment as Processed
     * If Dwolla Transfer is processed using ACH, the Event Handler will first
     * check for 'funded-transfer' related transfer. If related transfer is not
     * processed yet, Dwolla Payment will not be marked as processed
     * @param event
     */
    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(BankTransferCompletedEventHandler.name, async () => {
            if ([DwollaEventTopic.bank_transfer_completed].includes(event.payload.responseBody.topic)) {
                const transferId = event.payload.responseBody.resourceId;
                const payment = await this.repository.findByAchDwollaTransferId(transferId);

                if (!payment || payment.channel !== BazaDwollaPaymentChannel.ACH) {
                    return;
                }

                await this.markAsProcessed(payment, event);
            }
        });
    }

    /**
     * Marks Payment as Processed
     * @param payment
     * @param event
     * @private
     */
    private async markAsProcessed(payment: BazaDwollaPaymentEntity, event: BazaKafkaDwollaWebhookEvent): Promise<void> {
        payment.history.push({
            date: new Date(event.payload.responseBody.created).toISOString(),
            status: BazaDwollaPaymentStatus.Processed,
            action: {
                type: BazaDwollaPaymentHistoryAction.DwollaTransferAchProcessed,
                previousStatus: payment.status,
                newStatus: BazaDwollaPaymentStatus.Processed,
                individualAchId: payment.individualAchId,
            },
        });

        payment.dateUpdatedAt = new Date();
        payment.dateProcessedAt = new Date();
        payment.status = BazaDwollaPaymentStatus.Processed;
        payment.failureReason = null;

        if (payment.achDwollaTransferId) {
            const transfer = await this.dwollaTransferApi.getTransfer(payment.achDwollaTransferId);

            payment.individualAchId = transfer.individualAchId;
        }

        await this.repository.save(payment);

        this.eventBus.publish(
            new DwollaPaymentCompletedEvent({
                payment,
            }),
        );
    }
}
