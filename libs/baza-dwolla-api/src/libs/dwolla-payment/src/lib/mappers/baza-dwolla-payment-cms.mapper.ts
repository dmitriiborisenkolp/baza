import { Injectable } from '@nestjs/common';
import { BazaDwollaPaymentMapper } from './baza-dwolla-payment.mapper';
import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';
import { BazaDwollaPaymentCmsDto } from '@scaliolabs/baza-dwolla-shared';
import { BazaAccountMapper } from '@scaliolabs/baza-core-api';

/**
 * BazaDwollaPaymentEntity to BazaDwollaPaymentCmsDto mapper
 */
@Injectable()
export class BazaDwollaPaymentCmsMapper {
    constructor(private readonly baseMapper: BazaDwollaPaymentMapper, private readonly accountMapper: BazaAccountMapper) {}

    /**
     * Converts BazaDwollaPaymentEntity to BazaDwollaPaymentCmsDto
     * @param entity
     */
    entityToDTO(entity: BazaDwollaPaymentEntity): BazaDwollaPaymentCmsDto {
        return {
            ...this.baseMapper.entityToDTO(entity),
            account: this.accountMapper.entityToDto(entity.account),
            dwollaTransferId: entity.dwollaTransferId,
            achDwollaTransferId: entity.achDwollaTransferId,
        };
    }

    /**
     * Converts BazaDwollaPaymentEntity entities to BazaDwollaPaymentCmsDto DTOs
     * @param entities
     */
    entitiesToDTOs(entities: Array<BazaDwollaPaymentEntity>): Array<BazaDwollaPaymentCmsDto> {
        return entities.map((next) => this.entityToDTO(next));
    }
}
