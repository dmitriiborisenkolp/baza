import { Injectable } from '@nestjs/common';
import { BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';
import { BazaDwollaPaymentDto } from '@scaliolabs/baza-dwolla-shared';
import { convertFromCents } from '@scaliolabs/baza-nc-shared';

/**
 * BazaDwollaPaymentEntity to BazaDwollaPaymentDto Mapper
 */
@Injectable()
export class BazaDwollaPaymentMapper {
    /**
     * Converts BazaDwollaPaymentEntity to BazaDwollaPaymentDto
     * @param entity
     */
    entityToDTO(entity: BazaDwollaPaymentEntity): BazaDwollaPaymentDto {
        return {
            ulid: entity.ulid,
            dateCreatedAt: entity.dateCreatedAt.toISOString(),
            dateUpdatedAt: entity.dateUpdatedAt?.toISOString(),
            dateProcessedAt: entity.dateProcessedAt?.toISOString(),
            type: entity.type,
            channel: entity.channel,
            correlationId: entity.correlationId,
            ncTradeId: entity.ncTradeId,
            payload: entity.payload,
            amount: convertFromCents(entity.amountCents).toFixed(2),
            amountCents: entity.amountCents,
            status: entity.status,
            history: entity.history,
            failureReason: entity.failureReason,
        };
    }

    /**
     * Converts BazaDwollaPaymentEntity entities to BazaDwollaPaymentDto DTOs
     * @param entities
     */
    entitiesToDTOs(entities: Array<BazaDwollaPaymentEntity>): Array<BazaDwollaPaymentDto> {
        return entities.map((next) => this.entityToDTO(next));
    }
}
