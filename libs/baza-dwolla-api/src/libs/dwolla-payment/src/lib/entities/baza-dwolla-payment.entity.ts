import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryColumn, UpdateDateColumn } from 'typeorm';
import { enumLength, ULID_LENGTH } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import {
    BazaDwollaPaymentChannel,
    BazaDwollaPaymentHistoryDto,
    BazaDwollaPaymentPayload,
    BazaDwollaPaymentStatus,
    BazaDwollaPaymentType,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentFailureReasonDto } from '@scaliolabs/baza-dwolla-shared';
import { ulid } from 'ulid';

/**
 * Length of Dwolla ACH ID
 */
export const BAZA_DWOLLA_ACH_ID_MAX_LENGTH = 36;
export const BAZA_DWOLLA_TRANSFER_ID_LENGTH = 36;

/**
 * Relations for BazaDwollaPaymentEntity
 */
export const BAZA_NC_DWOLLA_PAYMENT_ENTITY_RELATIONS = ['account'];

/**
 * TypeORM BazaDwollaPayment Entity
 */
@Entity()
export class BazaDwollaPaymentEntity {
    @PrimaryColumn({
        type: 'char',
        length: ULID_LENGTH,
    })
    ulid = ulid();

    @CreateDateColumn()
    dateCreatedAt = new Date();

    @UpdateDateColumn({
        nullable: true,
    })
    dateUpdatedAt: Date;

    @Column({
        nullable: true,
    })
    dateProcessedAt: Date;

    @Column({
        type: 'varchar',
        length: enumLength(BazaDwollaPaymentChannel),
        default: BazaDwollaPaymentChannel.Balance,
    })
    channel: BazaDwollaPaymentChannel;

    @Column({
        nullable: false,
        type: 'varchar',
    })
    correlationId: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    ncTradeId?: string;

    @Column({
        type: 'varchar',
    })
    type: BazaDwollaPaymentType;

    @Column({
        type: 'jsonb',
    })
    payload: BazaDwollaPaymentPayload;

    @ManyToOne(() => AccountEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity;

    @Column({
        type: 'int',
    })
    amountCents: number;

    @Column({
        unique: true,
    })
    dwollaTransferId: string;

    @Column({
        unique: true,
        type: 'varchar',
        length: BAZA_DWOLLA_TRANSFER_ID_LENGTH,
        nullable: true,
    })
    achDwollaTransferId?: string;

    @Column({
        type: 'varchar',
        default: BazaDwollaPaymentStatus.Pending,
    })
    status: BazaDwollaPaymentStatus;

    @Column({
        type: 'jsonb',
    })
    history: Array<BazaDwollaPaymentHistoryDto>;

    @Column({
        type: 'jsonb',
        nullable: true,
    })
    failureReason?: BazaDwollaPaymentFailureReasonDto;

    @Column({
        type: 'char',
        length: BAZA_DWOLLA_ACH_ID_MAX_LENGTH,
        nullable: true,
        unique: true,
    })
    individualAchId?: string;
}
