import { Injectable } from '@nestjs/common';
import { BazaAccountRepository, BazaCoreAuthUniqueUsersFixture, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentPayload, BazaDwollaPaymentStatus, BazaDwollaPaymentType, DwollaUsStates } from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentRepository } from '../../repositories/baza-dwolla-payment.repository';
import { BazaDwollaPaymentFixtures } from '../baza-dwolla-payment-fixtures';
import { BazaDwollaPaymentService } from '../../services/baza-dwolla-payment.service';
import { DwollaCustomersApiNestjsService } from '../../../../../dwolla-api/src';
import { ulid } from 'ulid';

const FIXTURE: Array<{
    amountCents: number;
    status: BazaDwollaPaymentStatus;
    payload: BazaDwollaPaymentPayload;
}> = [
    {
        amountCents: 2000,
        status: BazaDwollaPaymentStatus.Pending,
        payload: {
            type: BazaDwollaPaymentType.Dividend,
            dividendEntryUlid: ulid(),
        },
    },
    {
        amountCents: 1000,
        status: BazaDwollaPaymentStatus.Failed,
        payload: {
            type: BazaDwollaPaymentType.Dividend,
            dividendEntryUlid: ulid(),
        },
    },
];

@Injectable()
export class BazaDwollaPaymentInitiatedFixture implements BazaE2eFixture {
    constructor(
        private readonly repository: BazaDwollaPaymentRepository,
        private readonly accountRepository: BazaAccountRepository,
        private readonly dwollaPaymentService: BazaDwollaPaymentService,
        private readonly dwollaCustomerApi: DwollaCustomersApiNestjsService,
    ) {}

    async up(): Promise<void> {
        const dwollaCustomerId = await this.dwollaCustomerApi.createPersonalCustomer({
            firstName: 'JOHN',
            lastName: 'SMITH',
            email: BazaCoreAuthUniqueUsersFixture._e2eUser.email,
            dateOfBirth: `1975-02-28`,
            ssn: '112-22-3333',
            address1: '222333 PEACHTREE PLACE',
            state: DwollaUsStates.GA,
            city: 'ATLANTA',
            postalCode: '30033',
        });

        for (const fixtureRequest of FIXTURE) {
            await this.dwollaPaymentService.transfer({
                payload: fixtureRequest.payload,
                dwollaCustomerId,
                amountCents: fixtureRequest.amountCents,
                accountId: BazaCoreAuthUniqueUsersFixture._e2eUser.id,
            });
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaDwollaPaymentFixtures.BazaDwollaPaymentInitiatedFixture,
        provider: BazaDwollaPaymentInitiatedFixture,
    },
]);
