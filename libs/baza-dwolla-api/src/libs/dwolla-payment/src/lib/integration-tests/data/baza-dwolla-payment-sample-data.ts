import { BazaDwollaPaymentEntity } from '../../entities/baza-dwolla-payment.entity';
import { BazaDwollaPaymentChannel, BazaDwollaPaymentStatus, BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import * as moment from 'moment';

export const bazaDwollaPaymentSampleData: (account: AccountEntity) => Array<BazaDwollaPaymentEntity> = (account: AccountEntity) => {
    const date = moment(new Date()).add('-30', 'days');

    const factoryWithdraw = (dwollaTransferId: string, amount: string, status: BazaDwollaPaymentStatus) => {
        const entity = new BazaDwollaPaymentEntity();

        entity.dateCreatedAt = date.add('8', 'hours').toDate();
        entity.channel = BazaDwollaPaymentChannel.Balance;
        entity.correlationId = generateRandomHexString();
        entity.type = BazaDwollaPaymentType.Withdrawal;
        entity.payload = {
            type: BazaDwollaPaymentType.Withdrawal,
            amount,
            cashOutFundingSourceId: generateRandomHexString(),
        };
        entity.account = account;
        entity.amountCents = Math.floor(parseFloat(amount) * 100);
        entity.dwollaTransferId = dwollaTransferId;
        entity.status = status;
        entity.history = [];

        return entity;
    };

    const factoryTransfer = (dwollaTransferId: string, amount: string, status: BazaDwollaPaymentStatus) => {
        const entity = new BazaDwollaPaymentEntity();

        entity.dateCreatedAt = date.add('8', 'hours').toDate();
        entity.channel = BazaDwollaPaymentChannel.Balance;
        entity.correlationId = generateRandomHexString();
        entity.type = BazaDwollaPaymentType.CashIn;
        entity.payload = {
            type: BazaDwollaPaymentType.CashIn,
            amount,
            cashInFundingSourceId: generateRandomHexString(),
        };
        entity.account = account;
        entity.amountCents = Math.floor(parseFloat(amount) * 100);
        entity.dwollaTransferId = dwollaTransferId;
        entity.status = status;
        entity.history = [];

        return entity;
    };

    const factoryPurchase = (dwollaTransferId: string, amount: string, status: BazaDwollaPaymentStatus) => {
        const entity = new BazaDwollaPaymentEntity();

        entity.dateCreatedAt = date.add('8', 'hours').toDate();
        entity.channel = BazaDwollaPaymentChannel.Balance;
        entity.correlationId = generateRandomHexString();
        entity.type = BazaDwollaPaymentType.PurchaseShares;
        entity.payload = {
            type: BazaDwollaPaymentType.PurchaseShares,
            ncTradeId: '12345678',
        };
        entity.account = account;
        entity.amountCents = Math.floor(parseFloat(amount) * 100);
        entity.dwollaTransferId = dwollaTransferId;
        entity.status = status;
        entity.history = [];

        return entity;
    };

    return [
        factoryPurchase('144f3171-1d81-ed11-814b-f79e44742551', '51.00', BazaDwollaPaymentStatus.Pending),
        factoryPurchase('3844601d-1d81-ed11-814b-f79e44742551', '52.00', BazaDwollaPaymentStatus.Pending),
        factoryWithdraw('0cc7f506-457d-ed11-814a-d69963ad6ab7', '353.00', BazaDwollaPaymentStatus.Cancelled),
        factoryPurchase('6cc7f506-457d-ed11-814a-d69963ad6ab7', '354.00', BazaDwollaPaymentStatus.Cancelled),
        factoryTransfer('244f3171-1d81-ed11-814b-f79e44742551', '55.00', BazaDwollaPaymentStatus.Pending),
        factoryPurchase('7a3aed6e-6b80-ed11-814b-f79e44742551', '256.00', BazaDwollaPaymentStatus.Processed),
        factoryPurchase('c6a9f41b-0b7b-ed11-814a-d69963ad6ab7', '157.00', BazaDwollaPaymentStatus.Processed),
        factoryTransfer('b6a9f41b-0b7b-ed11-814a-d69963ad6ab7', '158.00', BazaDwollaPaymentStatus.Processed),
        factoryWithdraw('8844601d-1d81-ed11-814b-f79e44742551', '59.00', BazaDwollaPaymentStatus.Pending),
        factoryTransfer('4cc7f506-457d-ed11-814a-d69963ad6ab7', '351.00', BazaDwollaPaymentStatus.Cancelled),
        factoryTransfer('a01d7dc7-447d-ed11-814a-d69963ad6ab7', '152.00', BazaDwollaPaymentStatus.Failed),
        factoryWithdraw('644f3171-1d81-ed11-814b-f79e44742551', '53.00', BazaDwollaPaymentStatus.Pending),
        factoryTransfer('4844601d-1d81-ed11-814b-f79e44742551', '54.00', BazaDwollaPaymentStatus.Pending),
        factoryPurchase('401d7dc7-447d-ed11-814a-d69963ad6ab7', '155.00', BazaDwollaPaymentStatus.Failed),
        factoryWithdraw('76a9f41b-0b7b-ed11-814a-d69963ad6ab7', '156.00', BazaDwollaPaymentStatus.Processed),
        factoryTransfer('da3aed6e-6b80-ed11-814b-f79e44742551', '257.00', BazaDwollaPaymentStatus.Processed),
        factoryWithdraw('9a3aed6e-6b80-ed11-814b-f79e44742551', '258.00', BazaDwollaPaymentStatus.Processed),
        factoryWithdraw('101d7dc7-447d-ed11-814a-d69963ad6ab7', '159.00', BazaDwollaPaymentStatus.Failed),
    ];
};
