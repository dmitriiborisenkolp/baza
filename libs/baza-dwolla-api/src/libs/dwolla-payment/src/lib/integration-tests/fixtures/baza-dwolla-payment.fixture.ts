import { Injectable } from '@nestjs/common';
import { BazaAccountRepository, BazaCoreAuthUsersFixture, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import {
    bazaDwollaCorrelationId,
    BazaDwollaPaymentPayload,
    BazaDwollaPaymentStatus,
    BazaDwollaPaymentType,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentRepository } from '../../repositories/baza-dwolla-payment.repository';
import { BazaDwollaPaymentEntity } from '../../entities/baza-dwolla-payment.entity';
import { ulid } from 'ulid';
import { BazaDwollaPaymentFixtures } from '../baza-dwolla-payment-fixtures';

const FIXTURE: Array<{
    amountCents: number;
    status: BazaDwollaPaymentStatus;
    payload: BazaDwollaPaymentPayload;
    userId: number;
}> = [
    {
        amountCents: 2000,
        status: BazaDwollaPaymentStatus.Pending,
        payload: {
            type: BazaDwollaPaymentType.PurchaseShares,
            ncTradeId: 'T123456',
        },
        userId: 1,
    },
    {
        amountCents: 30000,
        status: BazaDwollaPaymentStatus.Processed,
        payload: {
            type: BazaDwollaPaymentType.PurchaseShares,
            ncTradeId: 'T123457',
        },
        userId: 2,
    },
    {
        amountCents: 1000,
        status: BazaDwollaPaymentStatus.Failed,
        payload: {
            type: BazaDwollaPaymentType.PurchaseShares,
            ncTradeId: 'T123458',
        },
        userId: 1,
    },
];

@Injectable()
export class BazaDwollaPaymentFixture implements BazaE2eFixture {
    constructor(
        private readonly repository: BazaDwollaPaymentRepository,
        private readonly accountFixture: BazaCoreAuthUsersFixture,
        private readonly accountRepository: BazaAccountRepository,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of FIXTURE) {
            const entity = new BazaDwollaPaymentEntity();

            const accountId = fixtureRequest.userId === 1 ? this.accountFixture.e2eUser.id : this.accountFixture.e2eUser2.id;

            entity.type = fixtureRequest.payload.type;
            entity.correlationId = bazaDwollaCorrelationId(fixtureRequest.payload);
            entity.payload = fixtureRequest.payload;
            entity.amountCents = fixtureRequest.amountCents;
            entity.account = await this.accountRepository.getActiveAccountWithId(accountId);
            entity.dwollaTransferId = ulid();
            entity.status = fixtureRequest.status;
            entity.history = [];

            await this.repository.save(entity);
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaDwollaPaymentFixtures.BazaDwollaPaymentFixture,
        provider: BazaDwollaPaymentFixture,
    },
]);
