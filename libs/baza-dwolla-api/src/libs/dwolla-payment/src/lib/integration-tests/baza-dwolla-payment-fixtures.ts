export enum BazaDwollaPaymentFixtures {
    BazaDwollaPaymentFixture = 'BazaDwollaPaymentFixture',
    BazaDwollaPaymentInitiatedFixture = 'BazaDwollaPaymentInitiatedFixture',
    BazaDwollaPaymentSampleFixture = 'BazaDwollaPaymentSampleFixture',
}
