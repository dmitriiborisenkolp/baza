import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDwollaPaymentFixtures } from '../baza-dwolla-payment-fixtures';
import { BazaDwollaPaymentNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { BazaDwollaPaymentErrorCodes, BazaDwollaPaymentStatus } from '@scaliolabs/baza-dwolla-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-dwolla-api/dwolla-payment/001-baza-dwolla-payment.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDwollaPayment = new BazaDwollaPaymentNodeAccess(http);

    let ulid: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaDwollaPaymentFixtures.BazaDwollaPaymentFixture],
            specFile: __filename,
        });
    });

    it('will fetch payments of User 1', async () => {
        await http.authE2eUser();

        const response = await dataAccessDwollaPayment.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].amount).toBe('10.00');
        expect(response.items[0].amountCents).toBe(1000);
        expect(response.items[0].correlationId).toBe('purchase_trade_T123458');
        expect(response.items[0].dateCreatedAt).toBeDefined();
        expect(response.items[0].dateUpdatedAt).toBeDefined();
        expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Failed);

        expect(response.items[1].amount).toBe('20.00');
        expect(response.items[1].amountCents).toBe(2000);
        expect(response.items[1].correlationId).toBe('purchase_trade_T123456');
        expect(response.items[1].dateCreatedAt).toBeDefined();
        expect(response.items[1].dateUpdatedAt).toBeDefined();
        expect(response.items[1].status).toBe(BazaDwollaPaymentStatus.Pending);

        ulid = response.items[0].ulid;
    });

    it('will fetch dwolla payment by ulid', async () => {
        await http.authE2eUser();

        const response = await dataAccessDwollaPayment.get(ulid);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.amount).toBe('10.00');
        expect(response.amountCents).toBe(1000);
        expect(response.correlationId).toBe('purchase_trade_T123458');
        expect(response.dateCreatedAt).toBeDefined();
        expect(response.dateUpdatedAt).toBeDefined();
        expect(response.status).toBe(BazaDwollaPaymentStatus.Failed);
    });

    it('will fetch list of payments of User 2', async () => {
        await http.authE2eUser2();

        const response = await dataAccessDwollaPayment.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);

        expect(response.items[0].amount).toBe('300.00');
        expect(response.items[0].amountCents).toBe(30000);
        expect(response.items[0].correlationId).toBe('purchase_trade_T123457');
        expect(response.items[0].dateCreatedAt).toBeDefined();
        expect(response.items[0].dateUpdatedAt).toBeDefined();
        expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Processed);
    });

    it('will not allow to get Dwolla Payment of User 1 by User 2', async () => {
        await http.authE2eUser2();

        const response: BazaError = (await dataAccessDwollaPayment.get(ulid)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaDwollaPaymentErrorCodes.BazaDwollaPaymentNotFound);
    });
});
