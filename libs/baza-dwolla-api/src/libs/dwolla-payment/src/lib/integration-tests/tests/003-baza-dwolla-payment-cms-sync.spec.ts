import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDwollaPaymentFixtures } from '../baza-dwolla-payment-fixtures';
import { BazaDwollaE2eNodeAccess, BazaDwollaPaymentCmsNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { BazaDwollaPaymentCmsDto, BazaDwollaPaymentStatus } from '@scaliolabs/baza-dwolla-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-dwolla-api/dwolla-payment/003-baza-dwolla-payment-cms-sync.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eDwolla = new BazaDwollaE2eNodeAccess(http);
    const dataAccessDwollaPaymentCms = new BazaDwollaPaymentCmsNodeAccess(http);

    let ulid: string;
    let list: Array<BazaDwollaPaymentCmsDto>;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser, BazaDwollaPaymentFixtures.BazaDwollaPaymentInitiatedFixture],
            specFile: __filename,
        });

        await dataAccessE2eDwolla.runSandboxSimulations();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will fetch list of dwolla payments', async () => {
        const response = await asyncExpect(
            async () => {
                const response = await dataAccessDwollaPaymentCms.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(2);
                expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Pending);
                expect(response.items[1].status).toBe(BazaDwollaPaymentStatus.Pending);

                return response;
            },
            null,
            { intervalMillis: 3000 },
        );

        list = response.items;
        ulid = response.items[0].ulid;
    });

    it('will check that dwolla payments has correct amounts', async () => {
        for (const next of list) {
            const amounts = await dataAccessE2eDwolla.getTransferValue({
                dwollaTransferId: next.dwollaTransferId,
            });

            expect(next.amount).toBe(amounts.amount);
            expect(next.amountCents).toBe(amounts.amountCents);
        }
    });

    it('will sync dwolla payment', async () => {
        const response = await dataAccessDwollaPaymentCms.sync({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.status).toBe(BazaDwollaPaymentStatus.Processed);
    });

    it('will updates dwolla payment status', async () => {
        const response = await dataAccessDwollaPaymentCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Processed);
        expect(response.items[1].status).toBe(BazaDwollaPaymentStatus.Pending);
    });
});
