import { Injectable } from '@nestjs/common';
import { BazaCoreAuthUsersFixture, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentRepository } from '../../repositories/baza-dwolla-payment.repository';
import { bazaDwollaPaymentSampleData } from '../data/baza-dwolla-payment-sample-data';
import { BazaDwollaPaymentFixtures } from '../baza-dwolla-payment-fixtures';
import { monotonicFactory } from 'ulid';

@Injectable()
export class BazaDwollaPaymentSampleFixture implements BazaE2eFixture {
    constructor(private readonly repository: BazaDwollaPaymentRepository) {}

    async up(): Promise<void> {
        // Wonder why? Check out this article: https://blog.tericcabrel.com/discover-ulid-the-sortable-version-of-uuid/
        const ulid = monotonicFactory();

        const sample = bazaDwollaPaymentSampleData(BazaCoreAuthUsersFixture._e2eUser);

        for (const entity of sample) {
            entity.ulid = ulid();

            await this.repository.save(entity);
        }
    }
}

defineE2eFixtures<BazaDwollaPaymentFixtures>([
    {
        fixture: BazaDwollaPaymentFixtures.BazaDwollaPaymentSampleFixture,
        provider: BazaDwollaPaymentSampleFixture,
    },
]);
