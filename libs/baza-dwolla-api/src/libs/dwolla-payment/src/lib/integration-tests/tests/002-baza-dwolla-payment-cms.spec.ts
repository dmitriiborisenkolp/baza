import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDwollaPaymentFixtures } from '../baza-dwolla-payment-fixtures';
import { BazaDwollaPaymentCmsNodeAccess } from '@scaliolabs/baza-dwolla-node-access';

jest.setTimeout(240000);

describe('@scaliolabs/baza-dwolla-api/dwolla-payment/002-baza-dwolla-payment-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDwollaPaymentCms = new BazaDwollaPaymentCmsNodeAccess(http);

    let ulid: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaDwollaPaymentFixtures.BazaDwollaPaymentFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will fetch list of dwolla payments', async () => {
        const response = await dataAccessDwollaPaymentCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);

        expect(response.items[0].correlationId).toBe('purchase_trade_T123458');
        expect(response.items[1].correlationId).toBe('purchase_trade_T123457');
        expect(response.items[2].correlationId).toBe('purchase_trade_T123456');

        expect(response.items[0].account.email).toBe('e2e@scal.io');
        expect(response.items[1].account.email).toBe('e2e-2@scal.io');
        expect(response.items[2].account.email).toBe('e2e@scal.io');

        ulid = response.items[0].ulid;
    });

    it('will get payment by ulid', async () => {
        const response = await dataAccessDwollaPaymentCms.getByUlid({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.correlationId).toBe('purchase_trade_T123458');
    });
});
