import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BAZA_NC_DWOLLA_PAYMENT_ENTITY_RELATIONS, BazaDwollaPaymentEntity } from '../entities/baza-dwolla-payment.entity';
import { BazaDwollaPaymentNotFoundException } from '../exceptions/baza-dwolla-payment-not-found.exception';
import { DwollaTransferId } from '@scaliolabs/baza-dwolla-shared';

/**
 * Repository service for BazaDwollaPaymentEntity
 */
@Injectable()
export class BazaDwollaPaymentRepository {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeORM repository for BazaDwollaPaymentEntity
     */
    get repository(): Repository<BazaDwollaPaymentEntity> {
        return this.connection.getRepository(BazaDwollaPaymentEntity);
    }

    /**
     * Saves BazaDwollaPaymentEntity entity to DB
     * @param entity
     */
    async save(entity: BazaDwollaPaymentEntity): Promise<void> {
        await this.repository.save(entity);
    }

    /**
     * Removes BazaDwollaPaymentEntity entity from DB
     * @param entity
     */
    async remove(entity: BazaDwollaPaymentEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    /**
     * Search for BazaDwollaPaymentEntity by ULID.
     * The method will return `undefined` in case if there is no record with given ULID
     * @param ulid
     */
    async findByUlid(ulid: string): Promise<BazaDwollaPaymentEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    ulid,
                },
            ],
            relations: BAZA_NC_DWOLLA_PAYMENT_ENTITY_RELATIONS,
        });
    }

    /**
     * Returns BazaDwollaPaymentEntity by ULID
     * The method will throw BazaDwollaPaymentNotFoundException error with BazaDwollaPaymentNotFound error code in
     * case if there is no record with given ULID
     * @param ulid
     */
    async getByUlid(ulid: string): Promise<BazaDwollaPaymentEntity> {
        const entity = await this.findByUlid(ulid);

        if (!entity) {
            throw new BazaDwollaPaymentNotFoundException();
        }

        return entity;
    }

    /**
     * Search for BazaDwollaPaymentEntity by correlationId.
     * The method will return `undefined` in case if there is no record with given correlationId
     * @param correlationId
     */
    async findByCorrelationId(correlationId: string): Promise<BazaDwollaPaymentEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    correlationId,
                },
            ],
            relations: BAZA_NC_DWOLLA_PAYMENT_ENTITY_RELATIONS,
        });
    }

    /**
     * Returns BazaDwollaPaymentEntity by correlationId
     * The method will throw BazaDwollaPaymentNotFoundException error with BazaDwollaPaymentNotFound error code in
     * case if there is no record with given correlationId
     * @param correlationId
     */
    async getByCorrelationId(correlationId: string): Promise<BazaDwollaPaymentEntity> {
        const entity = await this.findByCorrelationId(correlationId);

        if (!entity) {
            throw new BazaDwollaPaymentNotFoundException();
        }

        return entity;
    }

    /**
     * Returns Dwolla Payment by Dwolla Transfer Id
     * The method will return `undefined` in case if there is no record with given Dwolla Transfer Id
     * @param dwollaTransferId
     */
    async findByDwollaTransferId(dwollaTransferId: DwollaTransferId): Promise<BazaDwollaPaymentEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    dwollaTransferId,
                },
            ],
        });
    }

    /**
     * Returns BazaDwollaPaymentEntity by Dwolla Transfer Id
     * The method will throw BazaDwollaPaymentNotFoundException error with BazaDwollaPaymentNotFound error code in
     * case if there is no record with given Dwolla Transfer Id
     * @param dwollaTransferId
     */
    async getByDwollaTransferId(dwollaTransferId: DwollaTransferId): Promise<BazaDwollaPaymentEntity> {
        const entity = await this.findByDwollaTransferId(dwollaTransferId);

        if (!entity) {
            throw new BazaDwollaPaymentNotFoundException();
        }

        return entity;
    }

    /**
     * Returns Dwolla Payment by ACH Dwolla Transfer Id
     * The method will return `undefined` in case if there is no record with given Dwolla Transfer Id
     * @param achDwollaTransferId
     */
    async findByAchDwollaTransferId(achDwollaTransferId: DwollaTransferId): Promise<BazaDwollaPaymentEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    achDwollaTransferId,
                },
            ],
        });
    }

    /**
     * Returns BazaDwollaPaymentEntity by ACH Dwolla Transfer Id
     * The method will throw BazaDwollaPaymentNotFoundException error with BazaDwollaPaymentNotFound error code in
     * case if there is no record with given Dwolla Transfer Id
     * @param achDwollaTransferId
     */
    async getByAchDwollaTransferId(achDwollaTransferId: DwollaTransferId): Promise<BazaDwollaPaymentEntity> {
        const entity = await this.findByAchDwollaTransferId(achDwollaTransferId);

        if (!entity) {
            throw new BazaDwollaPaymentNotFoundException();
        }

        return entity;
    }
}
