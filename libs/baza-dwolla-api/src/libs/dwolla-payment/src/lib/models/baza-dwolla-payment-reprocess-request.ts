export interface BazaDwollaPaymentReprocessRequest {
    ulid: string;
    accountId: number;
    dwollaCustomerId: string;
    payload?: Record<string, any>;
}
