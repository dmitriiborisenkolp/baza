import { BazaDwollaPaymentPayload, DwollaCustomerId } from '@scaliolabs/baza-dwolla-shared';

export interface BazaDwollaPaymentTransferRequest {
    amountCents?: number;
    accountId: number;
    dwollaCustomerId: DwollaCustomerId;
    payload: BazaDwollaPaymentPayload;
}
