import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import {
    BazaDwollaAcl,
    BazaDwollaPaymentCmsEndpoint,
    BazaDwollaPaymentCmsEndpointPaths,
    BazaDwollaPaymentCmsListRequest,
    BazaDwollaPaymentCmsListResponse,
    BazaDwollaPaymentCmsGetByUlidRequest,
    BazaDwollaPaymentCmsDto,
    BazaDwollaPaymentCmsSyncRequest,
    BazaDwollaCmsOpenApi,
    BazaDwollaPaymentCmsFetchAchDetailsRequest,
    BazaDwollaPaymentCmsFetchAchDetailsResponse,
} from '@scaliolabs/baza-dwolla-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaDwollaPaymentCmsMapper } from '../mappers/baza-dwolla-payment-cms.mapper';
import { BazaDwollaPaymentCmsService } from '../services/baza-dwolla-payment-cms.service';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { BazaDwollaPaymentService } from '../services/baza-dwolla-payment.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaDwollaCmsOpenApi.BazaDwollaCmsPayment)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaDwollaAcl.BazaDwollaPayment])
export class BazaDwollaPaymentCmsController implements BazaDwollaPaymentCmsEndpoint {
    constructor(
        private readonly mapper: BazaDwollaPaymentCmsMapper,
        private readonly service: BazaDwollaPaymentService,
        private readonly cmsService: BazaDwollaPaymentCmsService,
        private readonly repository: BazaDwollaPaymentRepository,
    ) {}

    @Post(BazaDwollaPaymentCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Dwolla Payments (CMS)',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentCmsListResponse,
    })
    async list(@Body() request: BazaDwollaPaymentCmsListRequest): Promise<BazaDwollaPaymentCmsListResponse> {
        return this.cmsService.list(request);
    }

    @Post(BazaDwollaPaymentCmsEndpointPaths.getByUlid)
    @ApiOperation({
        summary: 'getByUlid',
        description: 'Returns Dwolla Payment by ULID (CMS)',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentCmsDto,
    })
    async getByUlid(@Body() request: BazaDwollaPaymentCmsGetByUlidRequest): Promise<BazaDwollaPaymentCmsDto> {
        const entity = await this.repository.getByUlid(request.ulid);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaDwollaPaymentCmsEndpointPaths.sync)
    @ApiOperation({
        summary: 'sync',
        description: 'Sync Dwolla Payment (Updates actual status of Dwolla Payment)',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentCmsDto,
    })
    async sync(@Body() request: BazaDwollaPaymentCmsSyncRequest): Promise<BazaDwollaPaymentCmsDto> {
        const entity = await this.service.sync(request.ulid);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaDwollaPaymentCmsEndpointPaths.fetchAchDetails)
    @ApiOperation({
        summary: 'fetchAchDetails',
        description: 'Fetches and saves details of related Dwolla ACH Transfer',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentCmsFetchAchDetailsResponse,
    })
    async fetchAchDetails(
        @Body() request: BazaDwollaPaymentCmsFetchAchDetailsRequest,
    ): Promise<BazaDwollaPaymentCmsFetchAchDetailsResponse> {
        return this.service.fetchAchDetails(request);
    }
}
