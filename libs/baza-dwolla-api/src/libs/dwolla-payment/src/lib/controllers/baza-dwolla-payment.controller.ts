import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiExtraModels, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaDwollaPaymentalListRequest,
    BazaDwollaPaymentalListResponse,
    BazaDwollaPaymentDto,
    BazaDwollaPaymentEndpoint,
    BazaDwollaPaymentEndpointPaths,
    bazaDwollaPaymentHistorySwaggerExportedDtos,
    bazaDwollaPaymentPayloadSwaggerExportedDtos,
    BazaDwollaPublicOpenApi,
} from '@scaliolabs/baza-dwolla-shared';
import { AuthGuard, ReqAccountId } from '@scaliolabs/baza-core-api';
import { BazaDwollaPaymentService } from '../services/baza-dwolla-payment.service';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { BazaDwollaPaymentNotFoundException } from '../exceptions/baza-dwolla-payment-not-found.exception';
import { BazaDwollaPaymentMapper } from '../mappers/baza-dwolla-payment.mapper';

@Controller()
@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiTags(BazaDwollaPublicOpenApi.BazaDwollaPayment)
@ApiExtraModels(...bazaDwollaPaymentPayloadSwaggerExportedDtos, ...bazaDwollaPaymentHistorySwaggerExportedDtos)
export class BazaDwollaPaymentController implements BazaDwollaPaymentEndpoint {
    constructor(
        private readonly service: BazaDwollaPaymentService,
        private readonly repository: BazaDwollaPaymentRepository,
        private readonly mapper: BazaDwollaPaymentMapper,
    ) {}

    @Get(BazaDwollaPaymentEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Dwolla Payments of current Investor',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentalListResponse,
    })
    async list(
        @Query() request: BazaDwollaPaymentalListRequest,
        @ReqAccountId() accountId: number,
    ): Promise<BazaDwollaPaymentalListResponse> {
        return this.service.list(accountId, request);
    }

    @Get(BazaDwollaPaymentEndpointPaths.get)
    @ApiOperation({
        summary: 'get',
        description: 'Returns Dwolla Payment by ULID (only payments of current Investor)',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentalListResponse,
    })
    async get(@Param('ulid') ulid: string, @ReqAccountId() accountId: number): Promise<BazaDwollaPaymentDto> {
        const entity = await this.repository.getByUlid(ulid);

        if (entity.account.id !== accountId) {
            throw new BazaDwollaPaymentNotFoundException();
        }

        return this.mapper.entityToDTO(entity);
    }
}
