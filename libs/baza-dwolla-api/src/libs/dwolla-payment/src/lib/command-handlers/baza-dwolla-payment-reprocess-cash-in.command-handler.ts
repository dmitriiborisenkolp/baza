import { CommandBus, CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaDwollaPaymentHistoryAction, BazaDwollaPaymentStatus } from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { BazaDwollaPaymentNotFoundException } from '../exceptions/baza-dwolla-payment-not-found.exception';
import { BazaDwollaPaymentReprocessNotPermitted } from '../exceptions/baza-nc-dwolla-payment-reprocess-not-permitted.exception';
import { convertFromCents } from '@scaliolabs/baza-nc-shared';
import {
    BazaDwollaPaymentReprocessCashInCommand,
    BazaDwollaPaymentReprocessCashInCommandResult,
} from '../commands/baza-dwolla-payment-reprocess-cashIn.command';
import {
    BazaDwollaPaymentRequestCashInCommand,
    BazaDwollaPaymentRequestCashInCommandResult,
} from '../commands/baza-dwolla-payment-request-cashIn.command';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaDwollaPaymentReprocessCashInCommand)
export class BazaDwollaPaymentReprocessCashInCommandHandler
    implements ICommandHandler<BazaDwollaPaymentReprocessCashInCommand, BazaDwollaPaymentReprocessCashInCommandResult>
{
    constructor(private readonly commandBus: CommandBus, private readonly repository: BazaDwollaPaymentRepository) {}

    async execute(command: BazaDwollaPaymentReprocessCashInCommand): Promise<BazaDwollaPaymentReprocessCashInCommandResult> {
        return cqrsCommand<BazaDwollaPaymentReprocessCashInCommandResult>(BazaDwollaPaymentReprocessCashInCommandHandler.name, async () => {
            const {
                request: { dwollaCustomerId, ulid, cashInfundingSourceId },
                account,
            } = command;

            const entity = await this.repository.findByUlid(ulid);

            if (!entity || entity.account.id !== account.id) {
                throw new BazaDwollaPaymentNotFoundException();
            }

            if (entity.status !== BazaDwollaPaymentStatus.Failed) {
                throw new BazaDwollaPaymentReprocessNotPermitted();
            }

            const { correlationId, dwollaTransferId }: BazaDwollaPaymentRequestCashInCommandResult = await this.commandBus.execute(
                new BazaDwollaPaymentRequestCashInCommand(
                    {
                        amount: convertFromCents(entity.amountCents).toString(),
                        dwollaCustomerId: dwollaCustomerId,
                        cashInfundingSourceId,
                    },
                    account,
                ),
            );

            entity.correlationId = correlationId;
            entity.dwollaTransferId = dwollaTransferId;
            entity.status = BazaDwollaPaymentStatus.Pending;
            entity.history = [
                {
                    date: new Date().toISOString(),
                    status: BazaDwollaPaymentStatus.Pending,
                    action: {
                        type: BazaDwollaPaymentHistoryAction.DwollaTransferInitiated,
                        dwollaTransferId,
                        dwollaCustomerId: dwollaCustomerId,
                    },
                },
            ];

            await this.repository.save(entity);

            return entity;
        });
    }
}
