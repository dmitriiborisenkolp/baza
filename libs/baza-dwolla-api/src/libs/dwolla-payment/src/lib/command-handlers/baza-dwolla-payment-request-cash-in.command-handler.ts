import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaLogger, cqrsCommand } from '@scaliolabs/baza-core-api';
import {
    DwollaAmount,
    DwollaCurrency,
    DwollaCustomerId,
    DwollaFundingSource,
    DwollaFundingSourceId,
    DwollaFundingSourcesPaths,
    DwollaFundingSourceStatus,
    InitiateTransferResponse,
} from '@scaliolabs/baza-dwolla-shared';
import { ulid } from 'ulid';
import {
    DwollaConfigurationNestjsService,
    DwollaFundingSourcesApiNestjsService,
    DwollaTransfersApiNestjsService,
} from '../../../../dwolla-api/src';
import { BazaDwollaCustomerService } from '../../../../dwolla-customer/src';
import {
    BazaDwollaPaymentRequestCashInCommand,
    BazaDwollaPaymentRequestCashInCommandResult,
} from '../commands/baza-dwolla-payment-request-cashIn.command';

@CommandHandler(BazaDwollaPaymentRequestCashInCommand)
export class BazaDwollaPaymentRequestCashInCommandHandler
    implements ICommandHandler<BazaDwollaPaymentRequestCashInCommand, BazaDwollaPaymentRequestCashInCommandResult>
{
    constructor(
        private readonly dwollaCustomerService: BazaDwollaCustomerService,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
        private readonly config: DwollaConfigurationNestjsService,
        private readonly dwollaFundingSourceApi: DwollaFundingSourcesApiNestjsService,
        private readonly logger: BazaLogger,
    ) {}

    async execute(command: BazaDwollaPaymentRequestCashInCommand): Promise<BazaDwollaPaymentRequestCashInCommandResult> {
        return cqrsCommand<BazaDwollaPaymentRequestCashInCommandResult>(BazaDwollaPaymentRequestCashInCommandHandler.name, async () => {
            const {
                request: { dwollaCustomerId, amount, cashInfundingSourceId },
            } = command;

            return this.requestCashIn(cashInfundingSourceId, dwollaCustomerId, amount);
        });
    }

    /**
     * will request withdrawal from user dwolla wallet into his bank account (defined by dwolla funding source id)
     * @param dwollaFundingSourceId
     * @param dwollaCustomerId
     * @param amount
     * @returns WithdrawalRequestResult
     */
    private async requestCashIn(
        dwollaCashInFundingSourceId: DwollaFundingSourceId,
        dwollaCustomerId: DwollaCustomerId,
        amount: string,
    ): Promise<BazaDwollaPaymentRequestCashInCommandResult> {
        const customerBalanceFundingSource = await this.dwollaCustomerService.getBalanceFundingSource(dwollaCustomerId);

        const {
            _embedded: { 'funding-sources': fundingSources },
        } = await this.dwollaFundingSourceApi.getFundingSourceListForCustomer(dwollaCustomerId);

        const targetCashInFindgSource = fundingSources.find((source: DwollaFundingSource) => source.id === dwollaCashInFundingSourceId);

        if (targetCashInFindgSource.status !== DwollaFundingSourceStatus.Verified) {
            await this.checkBankAccountMicroDeposit(dwollaCashInFundingSourceId);
        }

        const finalAmount: DwollaAmount = { value: amount, currency: DwollaCurrency.USD };
        const correlationId = ulid();

        const { dwollaTransferId }: InitiateTransferResponse = await this.dwollaTransferApi.initiateTransfer({
            _links: {
                source: {
                    href: `${this.config.configuration.baseUrl}/${DwollaFundingSourcesPaths.FundingSources}/${dwollaCashInFundingSourceId}`,
                },
                destination: {
                    href: `${this.config.configuration.baseUrl}/${DwollaFundingSourcesPaths.FundingSources}/${customerBalanceFundingSource.id}`,
                },
            },
            amount: finalAmount,
            correlationId,
        });

        return {
            correlationId,
            dwollaTransferId,
            amount: finalAmount.value,
        };
    }

    /**
     * @param dwollaFundingSourceId
     */
    private async checkBankAccountMicroDeposit(dwollaFundingSourceId: string): Promise<void> {
        await this.dwollaFundingSourceApi.initiateMicroDeposits(dwollaFundingSourceId).catch((err) => {
            this.logger.warn(
                `[BazaNcTransferService] [checkBankAccountMicroDeposit] initiate micro deposit for ${dwollaFundingSourceId} fundingSourceId failed with the following error`,
            );
            this.logger.warn(err?.message ?? 'Failed to initaite Micro Deposit');
        });

        await this.dwollaFundingSourceApi.getMicroDepositsDetail(dwollaFundingSourceId).catch((err) => {
            this.logger.warn(
                `[BazaNcTransferService] [checkBankAccountMicroDeposit] the micro deposits details for ${dwollaFundingSourceId} fundingSourceId failed  with the following error`,
            );
            this.logger.warn(err?.message ?? 'Failed to get Micro Deposit details');
        });

        const verifyRequest = {
            amount1: {
                value: '0.03',
                currency: DwollaCurrency.USD,
            },
            amount2: {
                value: '0.09',
                currency: DwollaCurrency.USD,
            },
        };

        await this.dwollaFundingSourceApi.verifyMicroDeposits(dwollaFundingSourceId, verifyRequest).catch((err) => {
            this.logger.warn(
                `[BazaNcTransferService] [checkBankAccountMicroDeposit] verify micro deposits request for ${dwollaFundingSourceId} fundingSourceId failed with the following error`,
            );
            this.logger.warn(err?.message ?? `Failed to verify Micro Deposit for this funding source ${dwollaFundingSourceId}`);
        });
    }
}
