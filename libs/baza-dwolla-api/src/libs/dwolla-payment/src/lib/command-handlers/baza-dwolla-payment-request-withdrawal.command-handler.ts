import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import {
    DwollaAmount,
    DwollaCurrency,
    DwollaCustomerId,
    DwollaFundingSourceId,
    DwollaFundingSourcesPaths,
    InitiateTransferResponse,
} from '@scaliolabs/baza-dwolla-shared';
import { ulid } from 'ulid';
import {
    BazaDwollaPaymentRequestWithdrawalCommand,
    BazaDwollaPaymentRequestWithdrawalCommandResult,
} from '../commands/baza-dwolla-payment-request-withdrawal.command';
import { DwollaConfigurationNestjsService, DwollaTransfersApiNestjsService } from '../../../../dwolla-api/src';
import { BazaDwollaCustomerService } from '../../../../dwolla-customer/src';
import { BazaDwollaPaymentInsufficientFundsException } from '../exceptions/baza-dwolla-payment-insufficient-funds.exception';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaDwollaPaymentRequestWithdrawalCommand)
export class BazaDwollaPaymentRequestWithdrawalCommandHandler
    implements ICommandHandler<BazaDwollaPaymentRequestWithdrawalCommand, BazaDwollaPaymentRequestWithdrawalCommandResult>
{
    constructor(
        private readonly dwollaCustomerService: BazaDwollaCustomerService,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
        private readonly config: DwollaConfigurationNestjsService,
    ) {}

    async execute(command: BazaDwollaPaymentRequestWithdrawalCommand): Promise<BazaDwollaPaymentRequestWithdrawalCommandResult> {
        return cqrsCommand(BazaDwollaPaymentRequestWithdrawalCommandHandler.name, async () => {
            const {
                request: { dwollaCustomerId, amount, cashOutFundingSourceId },
            } = command;

            return this.requestWithdraw(cashOutFundingSourceId, dwollaCustomerId, amount);
        });
    }

    /**
     * will request withdrawal from user dwolla wallet into his bank account (defined by dwolla funding source id)
     * @param dwollaFundingSourceId
     * @param dwollaCustomerId
     * @param amount
     * @returns WithdrawalRequestResult
     */
    private async requestWithdraw(
        dwollaCashoutFundingSourceId: DwollaFundingSourceId,
        dwollaCustomerId: DwollaCustomerId,
        amount?: string,
    ): Promise<BazaDwollaPaymentRequestWithdrawalCommandResult> {
        const customerBalanceFundingSource = await this.dwollaCustomerService.getBalanceFundingSource(dwollaCustomerId);
        const fundingSourceBalance = (await this.dwollaCustomerService.getBalance(dwollaCustomerId)).balance;

        if (amount && parseFloat(fundingSourceBalance.value) < parseFloat(amount)) {
            throw new BazaDwollaPaymentInsufficientFundsException();
        }

        if (amount && parseFloat(amount) <= 0) {
            throw new BazaDwollaPaymentInsufficientFundsException();
        }

        const finalAmount: DwollaAmount = amount ? { value: amount, currency: DwollaCurrency.USD } : fundingSourceBalance;
        const correlationId = ulid();

        const { dwollaTransferId }: InitiateTransferResponse = await this.dwollaTransferApi.initiateTransfer({
            _links: {
                source: {
                    href: `${this.config.configuration.baseUrl}/${DwollaFundingSourcesPaths.FundingSources}/${customerBalanceFundingSource.id}`,
                },
                destination: {
                    href: `${this.config.configuration.baseUrl}/${DwollaFundingSourcesPaths.FundingSources}/${dwollaCashoutFundingSourceId}`,
                },
            },
            amount: finalAmount,
            correlationId,
        });

        return {
            correlationId,
            dwollaTransferId,
            amount: finalAmount.value,
        };
    }
}
