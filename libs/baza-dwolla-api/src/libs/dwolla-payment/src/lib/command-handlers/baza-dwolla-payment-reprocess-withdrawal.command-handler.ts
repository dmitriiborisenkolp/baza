import { CommandBus, CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaDwollaPaymentHistoryAction, BazaDwollaPaymentStatus } from '@scaliolabs/baza-dwolla-shared';
import {
    BazaDwollaPaymentRequestWithdrawalCommand,
    BazaDwollaPaymentRequestWithdrawalCommandResult,
} from '../commands/baza-dwolla-payment-request-withdrawal.command';
import {
    BazaDwollaPaymentReprocessWithdrawalCommand,
    BazaDwollaPaymentReprocessWithdrawalCommandResult,
} from '../commands/baza-dwolla-payment-reprocess-withdrawal.command';
import { BazaDwollaPaymentRepository } from '../repositories/baza-dwolla-payment.repository';
import { BazaDwollaPaymentNotFoundException } from '../exceptions/baza-dwolla-payment-not-found.exception';
import { BazaDwollaPaymentReprocessNotPermitted } from '../exceptions/baza-nc-dwolla-payment-reprocess-not-permitted.exception';
import { convertFromCents } from '@scaliolabs/baza-nc-shared';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaDwollaPaymentReprocessWithdrawalCommand)
export class BazaDwollaPaymentReprocessWithdrawalCommandHandler
    implements ICommandHandler<BazaDwollaPaymentReprocessWithdrawalCommand, BazaDwollaPaymentReprocessWithdrawalCommandResult>
{
    constructor(private readonly commandBus: CommandBus, private readonly repository: BazaDwollaPaymentRepository) {}

    async execute(command: BazaDwollaPaymentReprocessWithdrawalCommand): Promise<BazaDwollaPaymentReprocessWithdrawalCommandResult> {
        return cqrsCommand<BazaDwollaPaymentReprocessWithdrawalCommandResult>(
            BazaDwollaPaymentReprocessWithdrawalCommandHandler.name,
            async () => {
                const {
                    request: { dwollaCustomerId, ulid, cashOutFundingSourceId },
                    account,
                } = command;

                const entity = await this.repository.findByUlid(ulid);

                if (!entity || entity.account.id !== account.id) {
                    throw new BazaDwollaPaymentNotFoundException();
                }

                if (entity.status !== BazaDwollaPaymentStatus.Failed) {
                    throw new BazaDwollaPaymentReprocessNotPermitted();
                }

                const { correlationId, dwollaTransferId }: BazaDwollaPaymentRequestWithdrawalCommandResult = await this.commandBus.execute(
                    new BazaDwollaPaymentRequestWithdrawalCommand(
                        {
                            amount: convertFromCents(entity.amountCents).toString(),
                            dwollaCustomerId: dwollaCustomerId,
                            cashOutFundingSourceId,
                        },
                        account,
                    ),
                );

                entity.correlationId = correlationId;
                entity.dwollaTransferId = dwollaTransferId;
                entity.status = BazaDwollaPaymentStatus.Pending;
                entity.history = [
                    {
                        date: new Date().toISOString(),
                        status: BazaDwollaPaymentStatus.Pending,
                        action: {
                            type: BazaDwollaPaymentHistoryAction.DwollaTransferInitiated,
                            dwollaTransferId,
                            dwollaCustomerId: dwollaCustomerId,
                        },
                    },
                ];

                await this.repository.save(entity);

                return entity;
            },
        );
    }
}
