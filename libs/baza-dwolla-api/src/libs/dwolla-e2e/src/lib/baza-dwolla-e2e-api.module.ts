import { Module } from '@nestjs/common';
import { BazaDwollaWebhookApiModule } from '../../../dwolla-webhook/src';
import { BazaDwollaApiModule } from '../../../dwolla-api/src';
import { BazaDwollaE2eController } from './controllers/baza-dwolla-e2e.controller';
import { BazaDwollaE2eService } from './services/baza-dwolla-e2e.service';
import { BazaDwollaMasterAccountApiModule } from '../../../dwolla-master-account/src';
import { BazaDwollaCustomerApiModule } from '../../../dwolla-customer/src';

@Module({
    imports: [BazaDwollaApiModule, BazaDwollaWebhookApiModule, BazaDwollaMasterAccountApiModule, BazaDwollaCustomerApiModule],
    controllers: [BazaDwollaE2eController],
    providers: [BazaDwollaE2eService],
    exports: [BazaDwollaE2eService],
})
export class BazaDwollaE2eApiModule {}
