import { Injectable } from '@nestjs/common';
import { BazaDwollaWebhookService } from '../../../../dwolla-webhook/src';
import { DwollaTransfersApiNestjsService, DwollaWebhookSandboxSimulationApiNestjsService } from '../../../../dwolla-api/src';
import {
    BazaDwollaE2eGetTransferValueRequest,
    BazaDwollaE2eGetTransferValueResponse,
    BazaDwollaE2eTransferFundsFromMasterAccountRequest,
    BazaDwollaWebhookDto,
    DwollaCurrency,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaMasterAccountService } from '../../../../dwolla-master-account/src';
import { BazaDwollaCustomerService } from '../../../../dwolla-customer/src';
import { convertToCents } from '@scaliolabs/baza-nc-shared';

/**
 * E2E Helpers for Dwolla
 */
@Injectable()
export class BazaDwollaE2eService {
    constructor(
        private readonly dwollaWebhookService: BazaDwollaWebhookService,
        private readonly dwollaWebhookSandboxSimulation: DwollaWebhookSandboxSimulationApiNestjsService,
        private readonly dwollaMasterAccount: BazaDwollaMasterAccountService,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
        private readonly dwollaCustomerService: BazaDwollaCustomerService,
    ) {}

    /**
     * Run Sandbox Simulation
     * Dwolla Sandbox will not process any bank transfer by default. You should call this method to run simulations.
     */
    async runSandboxSimulations(): Promise<void> {
        await this.dwollaWebhookSandboxSimulation.sandboxWebhookSimulations();
    }

    /**
     * Sends Webhook Event to Baza Event Bus
     * Use it to simulate webhook events from Dwolla
     * @param event
     */
    async simulateWebhookEvent(event: BazaDwollaWebhookDto): Promise<void> {
        this.dwollaWebhookService.send(event);
    }

    /**
     * Transfers funds from Master Account to given Dwolla Customer
     * Automatically executes sandbox-simulations
     * @param request
     */
    async transferFundsFromMasterAccount(request: BazaDwollaE2eTransferFundsFromMasterAccountRequest): Promise<void> {
        const accountFundingSource = await this.dwollaCustomerService.getBalanceFundingSource(request.dwollaCustomerId);
        const masterBalanceFundingSource = await this.dwollaMasterAccount.balanceFundingSource();

        await this.dwollaTransferApi.initiateTransfer({
            _links: {
                source: {
                    href: masterBalanceFundingSource._links.self.href,
                },
                destination: {
                    href: accountFundingSource._links.self.href,
                },
            },
            amount: {
                currency: DwollaCurrency.USD,
                value: request.amount,
            },
        });

        await this.dwollaWebhookSandboxSimulation.sandboxWebhookSimulations();
    }

    /**
     * Returns actual Transfer Value (Amount) by Dwolla Transfer Id
     * @param request
     */
    async getTransferValue(request: BazaDwollaE2eGetTransferValueRequest): Promise<BazaDwollaE2eGetTransferValueResponse> {
        const transfer = await this.dwollaTransferApi.getTransfer(request.dwollaTransferId);

        return {
            amount: transfer.amount.value,
            amountCents: convertToCents(parseFloat(transfer.amount.value)),
        };
    }
}
