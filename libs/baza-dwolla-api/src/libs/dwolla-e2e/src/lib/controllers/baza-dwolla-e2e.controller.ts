import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaE2eGuard } from '@scaliolabs/baza-core-api';
import {
    BazaDwollaCmsOpenApi,
    BazaDwollaE2eEndpoint,
    BazaDwollaE2eEndpointPaths,
    BazaDwollaE2eGetTransferValueRequest,
    BazaDwollaE2eGetTransferValueResponse,
    BazaDwollaE2eTransferFundsFromMasterAccountRequest,
    BazaDwollaWebhookDto,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaE2eService } from '../services/baza-dwolla-e2e.service';

@Controller()
@ApiTags(BazaDwollaCmsOpenApi.BazaDwollaCmsE2e)
@UseGuards(BazaE2eGuard)
export class BazaDwollaE2eController implements BazaDwollaE2eEndpoint {
    constructor(private readonly service: BazaDwollaE2eService) {}

    @Get(BazaDwollaE2eEndpointPaths.runSandboxSimulations)
    @ApiOperation({
        summary: 'runSandboxSimulations',
        description:
            'Run Sandbox Simulation. Dwolla Sandbox will not process any bank transfer by default. You ' +
            'should call this method to run simulations.',
    })
    @ApiOkResponse()
    async runSandboxSimulations(): Promise<void> {
        await this.service.runSandboxSimulations();
    }

    @Post(BazaDwollaE2eEndpointPaths.simulateWebhookEvent)
    @ApiOperation({
        summary: 'simulateWebhookEvent',
        description: 'Sends Webhook Event to Baza Event Bus. ' + 'Use it to simulate webhook events from Dwolla',
    })
    @ApiOkResponse()
    async simulateWebhookEvent(@Body() event: BazaDwollaWebhookDto): Promise<void> {
        await this.service.simulateWebhookEvent(event);
    }

    @Post(BazaDwollaE2eEndpointPaths.transferFundsFromMasterAccount)
    @ApiOperation({
        summary: 'transferFundsFromMasterAccount',
        description: 'Transfers funds from Master Account to given Dwolla Customer.\n' + 'Automatically executes sandbox-simulations',
    })
    @ApiOkResponse()
    async transferFundsFromMasterAccount(@Body() request: BazaDwollaE2eTransferFundsFromMasterAccountRequest): Promise<void> {
        await this.service.transferFundsFromMasterAccount(request);
    }

    @Post(BazaDwollaE2eEndpointPaths.getTransferValue)
    @ApiOperation({
        summary: 'getTransferValue',
        description: 'Returns actual Transfer Value (Amount) by Dwolla Transfer Id',
    })
    @ApiOkResponse()
    async getTransferValue(@Body() request: BazaDwollaE2eGetTransferValueRequest): Promise<BazaDwollaE2eGetTransferValueResponse> {
        return this.service.getTransferValue(request);
    }
}
