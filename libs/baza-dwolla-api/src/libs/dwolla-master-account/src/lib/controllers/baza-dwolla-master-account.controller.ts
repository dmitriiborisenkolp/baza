import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaDwollaCmsOpenApi,
    BazaDwollaMasterAccountBalanceDto,
    BazaDwollaMasterAccountEndpoint,
    BazaDwollaMasterAccountEndpointPaths,
} from '@scaliolabs/baza-dwolla-shared';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import { BazaDwollaMasterAccountService } from '../services/baza-dwolla-master-account.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaDwollaCmsOpenApi.BazaDwollaCmsMasterAccount)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
export class BazaDwollaMasterAccountController implements BazaDwollaMasterAccountEndpoint {
    constructor(private readonly service: BazaDwollaMasterAccountService) {}

    @Get(BazaDwollaMasterAccountEndpointPaths.balance)
    @ApiOperation({
        summary: 'balance',
        description: 'Returns balance of master account',
    })
    @ApiOkResponse({
        type: BazaDwollaMasterAccountBalanceDto,
    })
    async balance(): Promise<BazaDwollaMasterAccountBalanceDto> {
        return this.service.balance();
    }
}
