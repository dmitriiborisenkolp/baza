import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaDwollaMasterAccountNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-dwolla-api/dwolla-master-account/integration-tests/001-baza-dwolla-master-account-balance.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaDwollaMasterAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will returns dwolla master account balance for admin', async () => {
        await http.authE2eAdmin();

        const response = await dataAccess.balance();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.currency).toBe('USD');
        expect(response.value).toBeDefined();
    });

    it('will not return dwolla master account for user', async () => {
        await http.authE2eUser();

        const response = await dataAccess.balance();

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });

    it('will not return dwolla master account for visitor', async () => {
        await http.noAuth();

        const response = await dataAccess.balance();

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });
});
