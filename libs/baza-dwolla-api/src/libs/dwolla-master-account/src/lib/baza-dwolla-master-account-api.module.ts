import { Global, Module } from '@nestjs/common';
import { BazaDwollaApiModule } from '../../../dwolla-api/src';
import { BazaDwollaMasterAccountController } from './controllers/baza-dwolla-master-account.controller';
import { BazaDwollaMasterAccountService } from './services/baza-dwolla-master-account.service';

@Global()
@Module({
    imports: [BazaDwollaApiModule],
    controllers: [BazaDwollaMasterAccountController],
    providers: [BazaDwollaMasterAccountService],
    exports: [BazaDwollaMasterAccountService],
})
export class BazaDwollaMasterAccountApiModule {}
