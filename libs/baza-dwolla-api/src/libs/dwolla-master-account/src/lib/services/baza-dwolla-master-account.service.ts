import { Injectable } from '@nestjs/common';
import {
    BazaDwollaMasterAccountBalanceDto,
    DwollaFundingSource,
    DwollaFundingSourceType,
    DwollaRootResponse,
    GetFundingSourceListResponse,
} from '@scaliolabs/baza-dwolla-shared';
import { DwollaDataAccessService, DwollaFundingSourcesApiNestjsService, DwollaRootApiNestjsService } from '../../../../dwolla-api/src';

@Injectable()
export class BazaDwollaMasterAccountService {
    private _root: DwollaRootResponse;
    private _fundingSources: GetFundingSourceListResponse;

    constructor(
        private readonly dwollaDataAccess: DwollaDataAccessService,
        private readonly dwollaRootApi: DwollaRootApiNestjsService,
        private readonly dwollaFundingSourceApi: DwollaFundingSourcesApiNestjsService,
    ) {}

    async root(): Promise<DwollaRootResponse> {
        if (!this._root) {
            this._root = await this.dwollaRootApi.root();
        }

        return this._root;
    }

    async fundingSources(): Promise<GetFundingSourceListResponse> {
        const root = await this.root();

        if (!this._fundingSources) {
            this._fundingSources = await this.dwollaDataAccess.processDwollaPromise(
                this.dwollaDataAccess.getDwollaInstance().get(`${root._links.account.href}/funding-sources`),
            );
        }

        return this._fundingSources;
    }

    async balanceFundingSource(): Promise<DwollaFundingSource> {
        const fundingSources = await this.fundingSources();

        return fundingSources._embedded['funding-sources'].find((next) => next.type === DwollaFundingSourceType.Balance);
    }

    async balance(): Promise<BazaDwollaMasterAccountBalanceDto> {
        const balanceFundingSource = await this.balanceFundingSource();
        const balanceDto = await this.dwollaFundingSourceApi.getFundingSourceBalance(balanceFundingSource.id);

        return balanceDto.balance;
    }
}
