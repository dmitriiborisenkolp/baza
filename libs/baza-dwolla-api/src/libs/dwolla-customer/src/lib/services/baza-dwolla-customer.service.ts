import { Injectable } from '@nestjs/common';
import {
    BazaDwollaCustomerBalanceDto,
    DwollaCustomerId,
    DwollaFundingSource,
    DwollaFundingSourceType,
} from '@scaliolabs/baza-dwolla-shared';
import { DwollaFundingSourcesApiNestjsService } from '../../../../dwolla-api/src';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { BazaDwollaCustomerNoBalanceException } from '../exceptions/baza-dwolla-customer-no-balance.exception';

const customerBalanceHrefRedisKeyFactory = (dwollaCustomerId: DwollaCustomerId) =>
    `BAZA_DWOLLA_CUSTOMER_BALANCE_FS_HREF_${dwollaCustomerId}`;

/**
 * Collection of utility services to work with Dwolla Customers
 */
@Injectable()
export class BazaDwollaCustomerService {
    constructor(private readonly redis: RedisService, private readonly dwollaFundingSourceApi: DwollaFundingSourcesApiNestjsService) {}

    /**
     * Returns Balance for given Customer
     * @param dwollaCustomerId
     */
    async getBalance(dwollaCustomerId: DwollaCustomerId): Promise<BazaDwollaCustomerBalanceDto> {
        const dwollaFundingSource = await this.findBalanceFundingSource(dwollaCustomerId);

        if (!dwollaFundingSource) {
            return {
                isDwollaAvailable: false,
            };
        }

        const dwollaBalance = await this.dwollaFundingSourceApi.getFundingSourceBalance(dwollaFundingSource.id);

        if (!dwollaBalance) {
            return {
                isDwollaAvailable: false,
            };
        }

        return {
            isDwollaAvailable: true,
            balance: dwollaBalance.balance,
        };
    }

    /**
     * Returns Balance Funding Source of Customer
     * Method may return `undefined` if Balance FS is not available
     * @param dwollaCustomerId
     */
    async findBalanceFundingSource(dwollaCustomerId: DwollaCustomerId): Promise<DwollaFundingSource | undefined> {
        const fundingSourceList = await this.dwollaFundingSourceApi.getFundingSourceListForCustomer(dwollaCustomerId, false);

        return fundingSourceList?._embedded['funding-sources']?.find(
            (fundingSource) => fundingSource.type === DwollaFundingSourceType.Balance,
        );
    }

    /**
     * Returns Balance Funding Source of Customer
     * Method will throw a BazaDwollaCustomerNoBalanceException error with BazaDwollaCustomerErrorCodes.BazaDwollaCustomerNoBalanceAvailable
     * error code is case if Dwolla Customer don't have Balance FS (i.e. Dwolla Customer is not verified)
     * @param dwollaCustomerId
     */
    async getBalanceFundingSource(dwollaCustomerId: DwollaCustomerId): Promise<DwollaFundingSource> {
        const fs = await this.findBalanceFundingSource(dwollaCustomerId);

        if (!fs) {
            throw new BazaDwollaCustomerNoBalanceException();
        }

        return fs;
    }

    /**
     * Returns HREF of Balance Funding Source for Dwolla Customer
     * Result will be cached in Redis
     * @param dwollaCustomerId
     */
    async getBalanceFundingSourceHref(dwollaCustomerId: DwollaCustomerId): Promise<string> {
        const key = customerBalanceHrefRedisKeyFactory(dwollaCustomerId);
        const cached = await this.redis.getClient().get(key);

        if (cached) {
            return cached;
        } else {
            const balanceFS = await this.getBalanceFundingSource(dwollaCustomerId);
            const balanceLink = balanceFS._links.self.href;

            await this.redis.getClient().set(key, balanceLink);

            return balanceLink;
        }
    }
}
