import { Module } from '@nestjs/common';
import { BazaDwollaCustomerService } from './services/baza-dwolla-customer.service';
import { BazaDwollaApiModule } from '../../../dwolla-api/src';

@Module({
    imports: [BazaDwollaApiModule],
    providers: [BazaDwollaCustomerService],
    exports: [BazaDwollaCustomerService],
})
export class BazaDwollaCustomerApiModule {}
