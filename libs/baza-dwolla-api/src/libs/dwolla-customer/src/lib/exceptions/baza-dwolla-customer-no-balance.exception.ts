import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaCustomerErrorCodes, bazaDwollaCustomerErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaCustomerNoBalanceException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaCustomerErrorCodes.BazaDwollaCustomerNoBalanceAvailable,
            bazaDwollaCustomerErrorCodesI18n[BazaDwollaCustomerErrorCodes.BazaDwollaCustomerNoBalanceAvailable],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
