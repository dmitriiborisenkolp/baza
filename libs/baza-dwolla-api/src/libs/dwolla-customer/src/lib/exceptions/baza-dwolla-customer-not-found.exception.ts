import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaDwollaCustomerErrorCodes, bazaDwollaCustomerErrorCodesI18n } from '@scaliolabs/baza-dwolla-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaDwollaCustomerNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaDwollaCustomerErrorCodes.BazaDwollaCustomerNotFound,
            bazaDwollaCustomerErrorCodesI18n[BazaDwollaCustomerErrorCodes.BazaDwollaCustomerNotFound],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
