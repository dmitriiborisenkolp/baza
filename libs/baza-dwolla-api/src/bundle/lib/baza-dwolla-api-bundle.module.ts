import { DynamicModule, Global, Module, OnApplicationBootstrap } from '@nestjs/common';
import { BazaDwollaApiModule, DwollaDataAccessService } from '../../libs/dwolla-api/src';
import { BazaDwollaWebhookApiModule } from '../../libs/dwolla-webhook/src';
import { EnvService } from '@scaliolabs/baza-core-api';
import { DwollaApiEnvironments } from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaMasterAccountApiModule } from '../../libs/dwolla-master-account/src';
import { BazaDwollaE2eApiModule } from '../../libs/dwolla-e2e/src';
import { BazaDwollaCustomerApiModule } from '../../libs/dwolla-customer/src';
import { BazaDwollaEscrowApiModule } from '../../libs/dwolla-escrow/src';

@Module({})
export class BazaDwollaApiBundleModule {
    static forRootAsync(): DynamicModule {
        const modules = [
            BazaDwollaApiModule.forRootAsync({
                injects: [EnvService],
                useFactory: async (env: EnvService<DwollaApiEnvironments.ApiEnvironment>) => ({
                    isSandbox: env.getAsBoolean('BAZA_DWOLLA_SANDBOX', {
                        defaultValue: true,
                    }),
                    credentials: {
                        apiKey: env.getAsString('BAZA_DWOLLA_API_KEY'),
                        apiSecret: env.getAsString('BAZA_DWOLLA_API_SECRET'),
                    },
                    webhooks: {
                        debug: env.getAsBoolean('BAZA_DWOLLA_WEBHOOK_DEBUG', { defaultValue: false }),
                        secret: env.getAsString('BAZA_DWOLLA_WEBHOOK_SECRET'),
                        kafkaPipe: {
                            read: env.getAsBoolean('BAZA_DWOLLA_WEBHOOK_KAFKA_READ', { defaultValue: false }),
                            send: env.getAsBoolean('BAZA_DWOLLA_WEBHOOK_KAFKA_SEND', { defaultValue: false }),
                        },
                    },
                    debug: env.getAsBoolean('BAZA_DWOLLA_DEBUG', {
                        defaultValue: false,
                    }),
                    baseUrl: env.getAsString('BAZA_DWOLLA_BASE_URL', {
                        defaultValue: 'https://api-sandbox.dwolla.com',
                    }),
                }),
            }),
            BazaDwollaWebhookApiModule,
            BazaDwollaMasterAccountApiModule,
            BazaDwollaE2eApiModule,
            BazaDwollaCustomerApiModule,
            BazaDwollaEscrowApiModule,
        ];

        return {
            module: BazaDwollaApiBundleGlobalModule,
            imports: [...modules],
        };
    }
}

@Global()
@Module({})
class BazaDwollaApiBundleGlobalModule implements OnApplicationBootstrap {
    constructor(private readonly dwollaDataAccessService: DwollaDataAccessService) {}

    onApplicationBootstrap(): void {
        this.dwollaDataAccessService.init();
    }
}
