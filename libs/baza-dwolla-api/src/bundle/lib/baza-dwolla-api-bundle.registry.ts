import { BazaEnvironments, RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';
import { BAZA_DWOLLA_BALANCE_REGISTRY_VALUE } from '@scaliolabs/baza-dwolla-shared';
import { bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';

/**
 * Registry Schema for Baza Dwolla API Package
 * Include it to `api.registry.ts` or use it with `bazaApiBundleConfigBuilder.withRegistrySchema` config
 */
export const bazaDwollaApiBundleRegistry: RegistrySchema = {
    bazaDwolla: {
        escrowFundingSourceId: {
            name: 'Dwolla EScrow Funding Source ID',
            type: RegistryType.String,
            hiddenFromList: true,
            public: true,
            defaults: BAZA_DWOLLA_BALANCE_REGISTRY_VALUE,
        },
        enableWebhookSimulations: {
            name: 'Dwolla Testing - Enable autorun webhook simulations',
            type: RegistryType.Boolean,
            hiddenFromList: !bazaNcApiConfig().withDwollaFeatures,
            public: false,
            defaults: true,
            forEnvironments: [BazaEnvironments.Local, BazaEnvironments.E2e, BazaEnvironments.Stage, BazaEnvironments.Test],
        },
    },
};
