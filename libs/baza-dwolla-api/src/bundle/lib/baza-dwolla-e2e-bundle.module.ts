import { Module } from '@nestjs/common';
import { BazaDwollaPaymentE2eModule } from '../../libs/dwolla-payment/src';
import { BazaDwollaEscrowE2eModule } from '../../libs/dwolla-escrow/src';

const E2E_MODULES = [BazaDwollaPaymentE2eModule, BazaDwollaEscrowE2eModule];

@Module({
    imports: E2E_MODULES,
    exports: E2E_MODULES,
})
export class BazaDwollaE2eBundleModule {}
