# baza-dwolla-api

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-dwolla-api` to execute the unit tests via [Jest](https://jestjs.io).

## Build

Run `nx build baza-dwolla-api`

## Publish

Run `yarn publish:baza-dwolla-api`
