import { Observable } from 'rxjs';
import { ProjectLanguage } from '@scaliolabs/baza-core-shared';

export interface BazaI18nDetector {
    detect(): Observable<ProjectLanguage | undefined>;
}
