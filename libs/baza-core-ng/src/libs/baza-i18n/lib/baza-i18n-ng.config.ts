import { Injectable } from '@angular/core';

@Injectable()
export class BazaI18nNgConfig {
    localStorageKey: string;
    reloadBrowserOnLanguageChange: boolean;
}
