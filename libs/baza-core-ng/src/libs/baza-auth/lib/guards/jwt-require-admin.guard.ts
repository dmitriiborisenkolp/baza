import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, UrlTree } from '@angular/router';
import { AccountRole } from '@scaliolabs/baza-core-shared';
import { BazaAuthNgConfig } from '../baza-auth-ng.config';
import { JwtService } from '../services/jwt.service';
import { BazaJwtRequireRoleGuardConfig, BazaJwtRequireRoleGuardRouteConfig } from './jwt-require-role.guard';

/**
 * Helper Interface for Route Data object
 * Implement it in Route's Data object or use it just as documentation source
 */
export interface JwtRequireAdminGuardRouteConfig {
    jwtRequireAdminGuard: Partial<BazaJwtRequireRoleGuardConfig>;
}

/**
 * Route guard which will requires Admin role from current user to access.
 *
 * JwtRequireAdminGuard uses JwtRequireRoleGuard configuration. If you
 * need to change default redirects or any kind of configuration for JwtRequireRoleGuard,
 * you should do it with implementing JwtRequireAdminGuardRouteConfig interface
 * in route's Data object or with JwtRequireRoleGuard configuration.
 *
 * Guard can be used with canActivate or canActivateChild configurations.
 *
 * @see JwtRequireRoleGuard
 * @see BazaJwtRequireRoleGuardConfig
 * @see BazaJwtRequireRoleGuardRouteConfig
 * @see JwtRequireAdminGuardRouteConfig
 *
 * @example
 * Change redirect specifically for route and specifically for JwtRequireAdminGuard:
 *
 * ```typescript
 import { MyComponent } from './my/my.cpmponent';
 import { ActivatedRouteSnapshot, Routes } from '@angular/router';
 import { JwtRequireAdminGuard, JwtRequireAdminGuardRouteConfig } from './libs/baza-core-ng/src';

 * export const myRoutes: Routes = [
 * {
 *        path: 'my',
 *        component: MyComponent,
 *        canActivateChild: [JwtRequireAdminGuard],
 *        data: {
 *            jwtRequireAdminGuard: {
 *                redirect: (route: ActivatedRouteSnapshot) => ({
 *                    commands: ['/'],
 *                    navigationExtras: {
 *                        queryParams: {
 *                            login: 1,
 *                        },
 *                    },
 *                }),
 *            },
 *        } as JwtRequireAdminGuardRouteConfig,
 *    },
 * ];
 * ```
 *
 * @example
 * Change redirect on global level.
 *
 * ```typescript
 * import { BAZA_WEB_BUNDLE_GUARD_CONFIGS } from './libs/baza-core-web/src';
 * import { ActivatedRouteSnapshot } from '@angular/router';
 * import { AccountRole } from './libs/baza-core-shared/src';
 *
 * BAZA_WEB_BUNDLE_GUARD_CONFIGS.requireRoleGuardConfig.redirect= (route: ActivatedRouteSnapshot, requestedRoles: Array<AccountRole>) => {
 *    if (requestedRoles.includes(AccountRole.Admin)) {
 *        return {
 *            commands: ['/'],
 *            navigationExtras: {
 *                queryParams: {
 *                    login: 1,
 *                    admin: 1,
 *                },
 *            },
 *        };
 *    } else {
 *        return {
 *            commands: ['/'],
 *            navigationExtras: {
 *                queryParams: {
 *                    login: 1,
 *                    user: 1,
 *                },
 *            },
 *        };
 *    }
 * };
 * ```
 *
 * @deprecated
 */
@Injectable()
export class JwtRequireAdminGuard implements CanActivate, CanActivateChild {
    constructor(
        private readonly moduleConfig: BazaAuthNgConfig,
        private readonly jwtService: JwtService,
        private readonly router: Router,
    ) {}

    canActivate(route: ActivatedRouteSnapshot): boolean | UrlTree {
        return this.validatedRoles(route);
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot): boolean | UrlTree {
        return this.validatedRoles(childRoute);
    }

    private config(route: ActivatedRouteSnapshot): JwtRequireAdminGuardRouteConfig {
        const moduleConfig = this.moduleConfig.requireRoleGuardConfig;
        const customConfig = ((route.data || {}) as BazaJwtRequireRoleGuardRouteConfig).jwtRequireRoleGuard;

        return {
            jwtRequireAdminGuard: {
                ...moduleConfig,
                ...customConfig,
            },
        };
    }

    private validatedRoles(route: ActivatedRouteSnapshot): boolean | UrlTree {
        if (!this.jwtService.hasJwt()) {
            return this.fail(route);
        }

        if (this.jwtService.jwtPayload.accountRole !== AccountRole.Admin) {
            return this.fail(route);
        }

        return true;
    }

    private fail(route: ActivatedRouteSnapshot): UrlTree | false {
        const config = this.config(route);

        if (config.jwtRequireAdminGuard.redirect) {
            const url = config.jwtRequireAdminGuard.redirect(route, [AccountRole.Admin]);

            return this.router.createUrlTree(url.commands, url.navigationExtras);
        } else {
            return false;
        }
    }
}
