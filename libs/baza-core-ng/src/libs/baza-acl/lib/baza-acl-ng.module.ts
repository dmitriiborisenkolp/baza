import { NgModule } from '@angular/core';
import { BazaAuthNgModule } from '../../baza-auth/lib/baza-auth-ng.module';
import { BazaAclNgService } from './baza-acl-ng.service';

// @dynamic
@NgModule({
    imports: [
        BazaAuthNgModule,
    ],
    providers: [
        BazaAclNgService,
    ],
})
export class BazaAclNgModule {}
