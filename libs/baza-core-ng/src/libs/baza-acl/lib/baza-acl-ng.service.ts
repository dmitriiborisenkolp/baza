import { Injectable } from '@angular/core';
import { CoreAcl } from '@scaliolabs/baza-core-shared';
import { JwtService } from '../../baza-auth/lib/services/jwt.service';

@Injectable()
export class BazaAclNgService<T extends string = string> {
    constructor(private readonly jwtService: JwtService) {}

    hasAccess(nodes: T | Array<T>): boolean {
        if (!this.jwtService.hasJwt()) {
            return false;
        }

        const rNodes = Array.isArray(nodes) ? nodes : [nodes];
        const access = this.jwtService.jwtPayload.accountAcl || [];

        return access.includes(CoreAcl.SystemRoot) || rNodes.every((accessNode) => access.includes(accessNode));
    }
}
