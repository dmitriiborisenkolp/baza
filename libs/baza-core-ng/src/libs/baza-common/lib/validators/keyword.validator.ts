import { AbstractControl, ValidatorFn } from '@angular/forms';
import { isEmpty } from '@scaliolabs/baza-core-shared';

export const KEYWORD_VALIDATOR_REGEX = /^[a-zA-Z_][a-zA-Z_\d]*$/;

export const keywordValidator: ValidatorFn = (control: AbstractControl) => {
    if (isEmpty(control.value)) {
        return null;
    }

    if (KEYWORD_VALIDATOR_REGEX.test(control.value)) {
        return null;
    } else {
        return {
            keyword: true,
        };
    }
};
