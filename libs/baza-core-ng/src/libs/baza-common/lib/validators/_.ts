export const BAZA_CORE_VALIDATORS = [
    'required',
    'min',
    'max',
    'minlength',
    'maxlength',
    'email',
    'int',
    'float',
    'more_than_zero',
    'https_link',
    'password',
    'keyword',
];
