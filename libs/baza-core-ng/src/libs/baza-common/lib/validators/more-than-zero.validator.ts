import { AbstractControl, ValidatorFn } from '@angular/forms';

export const moreThanZeroValidator: ValidatorFn = (control: AbstractControl) => {
  if (! control.disabled && (control.value !== undefined && control.value !== null) && (isNaN(+control.value) || isNaN(+control.value) || +control.value <= 0)) {
    return {
      more_than_zero: true,
    };
  } else {
    return null;
  }
};
