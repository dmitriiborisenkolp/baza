import { AbstractControl, ValidatorFn } from '@angular/forms';

export const intValidator: ValidatorFn = (control: AbstractControl) => {
  if (! control.disabled && !! control.value && (isNaN(+control.value) || parseFloat(control.value) !== parseInt(control.value, 10) || ! (/^\d+$/.test(control.value)))) {
    return {
      int: true,
    };
  } else {
    return null;
  }
};
