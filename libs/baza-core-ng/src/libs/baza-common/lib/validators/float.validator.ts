import { AbstractControl, ValidatorFn } from '@angular/forms';

export const floatValidator: ValidatorFn = (control: AbstractControl) => {
  if (! control.disabled && !! control.value && isNaN(+control.value)) {
    return {
      float: true,
    };
  } else {
    return null;
  }
};
