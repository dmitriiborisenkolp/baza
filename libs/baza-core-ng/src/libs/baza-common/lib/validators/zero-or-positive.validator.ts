import { AbstractControl, ValidatorFn } from '@angular/forms';

export const zeroOrPositiveValidator: ValidatorFn = (control: AbstractControl) => {
  if (! control.disabled && !! control.value && (isNaN(+control.value) || +control.value < 0)) {
    return {
      zero_or_positive: true,
    };
  } else {
    return null;
  }
};
