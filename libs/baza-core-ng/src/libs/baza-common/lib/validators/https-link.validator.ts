import { AbstractControl, ValidatorFn } from '@angular/forms';

export const httpsLinkValidator: ValidatorFn = (control: AbstractControl) => {
    if (! control.disabled && (control.value !== undefined && control.value !== null) && ! (control.value || '').startsWith('https://')) {
        return {
            https_link: true,
        };
    } else {
        return null;
    }
};
