import { ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { BAZA_CORE_VALIDATORS } from '../validators/_';

@Pipe({
    name: 'bazaNzError',
})
export class BazaNzError implements PipeTransform {
    constructor(private readonly cdr: ChangeDetectorRef, private readonly translate: TranslateService) {}

    transform(control: AbstractControl, ...args: string[]): any {
        const context = new BazaNzErrorContext(control, this.translate);

        if (args[0]) {
            return this.translate.get(args[0]);
        } else {
            return context.i18nError().pipe(tap(() => this.cdr.markForCheck()));
        }
    }
}

class BazaNzErrorContext {
    constructor(private readonly control: AbstractControl, private readonly translate: TranslateService) {}

    get hasError(): boolean {
        return this.control.dirty && !!this.control.errors && Object.keys(this.control.errors).length > 0;
    }

    get error(): string {
        return Object.keys(this.control.errors)[0];
    }

    get errorDetails(): Record<string, unknown> {
        return this.control.errors[Object.keys(this.control.errors)[0]];
    }

    i18nError(): Observable<string> {
        return this.control.valueChanges.pipe(
            switchMap(() => {
                if (this.hasError) {
                    const key = BAZA_CORE_VALIDATORS.includes(this.error) ? `baza.core._validators.${this.error}` : this.error;

                    return this.translate.get(key, this.errorDetails);
                } else {
                    return of(undefined);
                }
            }),
        );
    }
}
