export function tsToTranslations(input: any, parentKeys: Array<string> = []): { [key: string]: string } {
    const result: { [key: string]: string } = {};

    const allKeys: { [key: string]: boolean } = {};

    for (const key in input) {
        // eslint-disable-next-line no-prototype-builtins
        if (input.hasOwnProperty(key)) {
            allKeys[`${key}`] = true;
        }
    }

    for (const key in allKeys) {
        // eslint-disable-next-line no-prototype-builtins
        if (allKeys.hasOwnProperty(key)) {
            if (typeof input[`${key}`] === 'string') {
                result[[...parentKeys, key].join('.')] = input[`${key}`];
            } else {
                const sub: { [key: string]: string } = tsToTranslations(input[`${key}`], [...parentKeys, key]);

                for (const sKey in sub) {
                    // eslint-disable-next-line no-prototype-builtins
                    if (sub.hasOwnProperty(sKey)) {
                        result[`${sKey}`] = sub[`${sKey}`];
                    }
                }
            }
        }
    }

    return result;
}
