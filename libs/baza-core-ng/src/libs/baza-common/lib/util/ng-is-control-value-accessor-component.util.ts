import { ControlValueAccessor } from '@angular/forms';

export function ngIsControlValueAccessorComponent(componentRef: unknown): boolean {
    return (
        !!componentRef &&
        !!(componentRef as ControlValueAccessor).writeValue &&
        !!(componentRef as ControlValueAccessor).registerOnChange &&
        !!(componentRef as ControlValueAccessor).registerOnTouched &&
        !!(componentRef as ControlValueAccessor).setDisabledState
    );
}
