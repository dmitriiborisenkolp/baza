import { AbstractControl } from '@angular/forms';

export function ngEmptyInputValue(control: AbstractControl): void {
    control.setValue(undefined);
}
