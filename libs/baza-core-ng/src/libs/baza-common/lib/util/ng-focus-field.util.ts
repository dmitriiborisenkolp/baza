import { ElementRef } from '@angular/core';

export function ngFocusField(elementRef: ElementRef<HTMLInputElement>): void {
    if (elementRef && elementRef.nativeElement && elementRef.nativeElement.focus) {
        setTimeout(() => elementRef.nativeElement.focus());
    }
}
