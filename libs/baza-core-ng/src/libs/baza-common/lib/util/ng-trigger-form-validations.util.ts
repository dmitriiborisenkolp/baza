import { FormGroup } from '@angular/forms';

export function ngTriggerFormValidations(form: FormGroup): void {
    for (const key of Object.keys(form.controls)) {
        form.controls[`${key}`].markAsDirty();
        form.controls[`${key}`].updateValueAndValidity();
    }
}
