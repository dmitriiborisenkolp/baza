export enum BazaNzButtonStyle {
    Default = 'default',
    Primary = 'primary',
    Dashed = 'dashed',
    Text = 'text',
    Link = 'link',
    Danger = 'danger',
}
