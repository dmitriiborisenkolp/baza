export enum NzTypographyStyle {
    Default = 'default',
    Secondary = 'secondary',
    Success = 'success',
    Warning = 'warning',
    Danger = 'danger',
    Disabled = 'disabled',
}
