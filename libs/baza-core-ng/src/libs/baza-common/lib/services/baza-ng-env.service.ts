import { Injectable } from '@angular/core';
import { BazaNgCoreConfig } from '../baza-ng-core.config';
import { NgEnvironment } from '../models/ng-environment';

@Injectable({
    providedIn: 'root',
})
export class BazaNgEnvService {
    constructor(
        private readonly moduleConfig: BazaNgCoreConfig,
    ) {}

    get current(): NgEnvironment {
        return this.moduleConfig.ngEnv;
    }
}
