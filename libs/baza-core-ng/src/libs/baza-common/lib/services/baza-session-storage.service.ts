import { Injectable } from '@angular/core';
import { BazaNgCoreConfig } from '../baza-ng-core.config';

@Injectable()
export class BazaSessionStorageService {
    private _fallback = {};

    constructor(private readonly moduleConfig: BazaNgCoreConfig) {}

    get(key: string): string {
        const prefixedKey = this.getPrefixedKey(key);

        if (window && window.sessionStorage) {
            return window.sessionStorage.getItem(prefixedKey);
        } else {
            return this._fallback[`${prefixedKey}`];
        }
    }

    set(key: string, value: string): void {
        const prefixedKey = this.getPrefixedKey(key);

        if (window && window.sessionStorage) {
            window.sessionStorage.setItem(prefixedKey, value);
        } else {
            return this._fallback[`${prefixedKey}`];
        }
    }

    remove(key: string): void {
        const prefixedKey = this.getPrefixedKey(key);

        if (window && window.sessionStorage) {
            window.sessionStorage.setItem(prefixedKey, null);
        } else {
            delete this._fallback[`${prefixedKey}`];
        }
    }

    private getPrefixedKey(key: string) {
        return `${this.moduleConfig.sessionStorageKeyPrefix}-${key}`;
    }
}
