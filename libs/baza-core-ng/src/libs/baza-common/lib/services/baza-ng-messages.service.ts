import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

export interface MessagesRequest {
    message: string;
    translate?: boolean;
    translateParams?: any;
}

export interface BazaNgMessage<T = BazaNgMessageLevel> {
    level: T;
    request: MessagesRequest;
}

export enum BazaNgMessageLevel {
    Success = 'success',
    Error = 'error',
    Info = 'info',
    Warning = 'warning',
    Verbose = 'verbose',
}

@Injectable({
    providedIn: 'root',
})
export class BazaNgMessagesService<T = BazaNgMessageLevel> {
    private _messages$: Subject<BazaNgMessage<T>> = new Subject<BazaNgMessage<T>>();

    constructor(private readonly translate: TranslateService) {}

    get messages$(): Observable<BazaNgMessage<T>> {
        return this._messages$.asObservable();
    }

    httpError(): void {
        this.error({
            message: '__http_error',
            translate: true,
        });
    }

    success(request: MessagesRequest): void {
        this.message(request, BazaNgMessageLevel.Success as any);
    }

    error(request: MessagesRequest): void {
        this.message(request, BazaNgMessageLevel.Error as any);
    }

    bazaError(error: BazaError | any): void {
        if (isBazaErrorResponse(error)) {
            this.error({
                message: `__baza_error`,
                translate: true,
                translateParams: {
                    error,
                },
            });
        } else {
            this.httpError();
        }
    }

    info(request: MessagesRequest): void {
        this.message(request, BazaNgMessageLevel.Info as any);
    }

    warning(request: MessagesRequest): void {
        this.message(request, BazaNgMessageLevel.Warning as any);
    }

    verbose(request: MessagesRequest): void {
        this.message(request, BazaNgMessageLevel.Verbose as any);
    }

    private message(request: MessagesRequest, level: T): void {
        const message = request.translate ? this.translate.instant(request.message, request.translateParams) : request.message;

        this._messages$.next({
            level,
            request,
        });
    }
}
