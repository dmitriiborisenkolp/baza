import { Injectable } from '@angular/core';

@Injectable()
export class BazaPhoneCountryCodesNgConfig {
    // TTL for phone country codes. Set as 0 to disable cache
    // Please note that due of huge side of repository response we're not able to store it in LocalStorage/SessionStorage/anywhere else.
    ttlMilliseconds: number;
}
