import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';
import {
    AttachmentDto,
    AttachmentImageWithThumbnailPayload,
    AttachmentThumbnailResponse,
    AttachmentType,
} from '@scaliolabs/baza-core-shared';

type ObjectFit = 'fill' | 'contain' | 'cover' | 'none' | 'scale-down';
type Loading = 'lazy' | 'eager';

export type BazaAttachmentImageSet = 'original' | 'thumbnail';

interface State {
    src: string;
    srcSet: string;
    srcSetSizes: string;
    srcSetWebp: string;
    srcSetWebpSizes: string;
    ngStyle: Record<string, unknown>;
}

/**
 * BazaAttachmentImageComponent should be used for DTO's uploaded with ImageWithThumbnails
 * strategy.
 *
 * The component automatically generates seo/pagespeed-friendly <picture> tag
 * with webp/png sources.
 */
@Component({
    // eslint-disable-next-line @angular-eslint/component-selector
    selector: 'baza-attachment-image',
    templateUrl: './baza-attachment-image.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class BazaAttachmentImageComponent implements OnChanges {
    // Required - Attachment DTO uploaded with ImageWithThumbnails strategy
    @Input() attachment: AttachmentDto;

    // Optional - Image Set - which images set (original images or thumbnails) should be used
    @Input() imagesSet: BazaAttachmentImageSet = 'original';

    // Optional - Additional filter images by specific ID's of uploaded variants
    @Input() ids: Array<string> = [];

    // Optional - Additional class for component container
    @Input() class?: string;

    // Optional - Object-fit css property for image
    @Input() objectFit?: ObjectFit = 'cover';

    // Optional - Filter images and displays image variants of images less or equal than maxImageWidth
    @Input() maxImageWidth?: number;

    // Optional - alt attribute for img
    @Input() alt: string;

    // Optional - loading strategy for image
    @Input() loading: Loading = 'lazy';

    public state: State;

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['attachment']) {
            this.update();
        }
    }

    get payload(): AttachmentImageWithThumbnailPayload {
        if (!this.attachment) {
            throw new Error('No attachment available');
        }

        if (this.attachment.payload.type !== AttachmentType.ImageWithThumbnail) {
            throw new Error('Attachment is not uploaded with ImageWithThumbnail strategy');
        }

        return this.attachment.payload.payload;
    }

    get ngClass() {
        return ['baza-attachment-image', this.class].filter((c) => !!c);
    }

    get imgNgStyle() {
        return {
            'object-fit': this.objectFit,
        };
    }

    update(): void {
        const payload = this.payload;
        const imagesSet = this.imagesSet === 'thumbnail' ? payload.thumbnails : payload.original;

        const sources = this.sources(
            imagesSet
                .filter((image) => image.mime !== 'image/webp')
                .filter((image) => (Array.isArray(this.ids) && this.ids.length > 0 ? this.ids.includes(image.id) : true)),
        );

        const sourcesWebp = this.sources(
            imagesSet
                .filter((image) => image.mime === 'image/webp')
                .filter((image) => (Array.isArray(this.ids) && this.ids.length > 0 ? this.ids.includes(image.id) : true)),
        );

        const ngStyle: Record<string, unknown> = {};

        if (this.objectFit) {
            ngStyle['object-fit'] = this.objectFit;
        }

        this.state = {
            ...this.state,
            srcSet: sources.srcSets.join(','),
            srcSetSizes: sources.sizes.join(','),
            srcSetWebp: sourcesWebp.srcSets.join(','),
            srcSetWebpSizes: sourcesWebp.sizes.join(','),
            src: imagesSet[0].url,
            ngStyle,
        };
    }

    private sources(input: Array<AttachmentThumbnailResponse>): {
        srcSets: Array<string>;
        sizes: Array<string>;
    } {
        if (Array.isArray(input) && input.length > 0) {
            input.sort((a, b) => a.width - b.width);

            const variants = input
                .filter((variant) => !this.maxImageWidth || variant.width <= this.maxImageWidth)
                .map((variant) => ({
                    src: variant.url,
                    width: variant.width,
                }));

            if (!variants.length) {
                [...variants]
                    .sort((a, b) => a.width - b.width)
                    .push({
                        src: input[0].url,
                        width: input[0].width,
                    });
            }

            return {
                srcSets: variants.map((variant) => `${variant.src} ${variant.width}w`),
                sizes: variants.map((variant) => `(max-width: ${variant.width}px) ${variant.width}px`),
            };
        } else {
            return {
                sizes: [],
                srcSets: [],
            };
        }
    }
}
