import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaAttachmentImageComponent } from './components/baza-attachment-image/baza-attachment-image.component';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        BazaAttachmentImageComponent,
    ],
    exports: [
        BazaAttachmentImageComponent,
    ],
})
export class BazaAttachmentNgModule {
}
