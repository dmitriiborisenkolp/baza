# baza-core-ng

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-core-ng` to execute the unit tests.

## Build

Run `nx build baza-core-ng`

## Publish

Run `yarn publish:baza-core-ng`
