# Changelog

All notable changes of @scaliolabs/baza-core-ng are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2021-06-15

### Added

Add latest changes from Baza.

## [1.1.0] - 2021-05-25

### Fixed

Add missing peerDependencies.

## [1.0.0] - 2021-05-25

### Added

Add initial version of `baza-core-ng`.
