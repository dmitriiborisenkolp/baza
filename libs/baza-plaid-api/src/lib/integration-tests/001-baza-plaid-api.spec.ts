import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

describe('@scaliolabs/baza-plaid-api/intergtation-tests/001-baza-plaid-api.spec.ts', () => {
    const http = new BazaDataAccessNode();

    it('will return redirect handler', async () => {
        const response = await http.get(
            '/baza-plaid/handle',
            {},
            {
                asJsonResponse: false,
            },
        );

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect((response as string).includes('postMessage')).toBeTruthy();
    });
});
