import { Injectable } from '@nestjs/common';

/**
 * Plaid Configuration
 * You should not use it in your application; instead use BazaPlaidService.client
 * @see BazaPlaidService.client
 */
@Injectable()
export class BazaPlaidApiConfig {
    constructor(
        /**
         * Enables Production environment instead of sandbox for Plaid API
         * @see https://plaid.com/docs/quickstart/
         */
        public readonly production: boolean,

        /**
         * Plaid Client ID
         * @see https://plaid.com/docs/quickstart/
         */
        public readonly clientId: string,

        /**
         * Plaid Client Secret
         * @see https://plaid.com/docs/quickstart/
         */
        public readonly clientSecret: string,

        /**
         * Client Name (Team identifier)
         * @see https://plaid.com/docs/quickstart/
         */
        public readonly clientName: string,

        /**
         * Redirect URL for Plaid Link widget (optional)
         * By default Baza provides default redirect endpoint which will calls `window.postMessage` with results
         * @see https://plaid.com/docs/quickstart/
         * @see https://plaid.com/docs/link/
         */
        public readonly redirectUrl?: string,
    ) {}
}
