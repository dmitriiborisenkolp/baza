/* eslint-disable prefer-const */
import { bazaPlaidRedirectHandlerHbs } from './views/baza-plaid-redirect-handler.hbs';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace BazaPlaidApiEnvironment {
    /**
     * Default Redirect URL
     * By default Baza provides default redirect endpoint which will calls `window.postMessage` with results
     * Use {{ api }}, {{ cms }} and {{ web }} tags for providing API, CMS or Web URLs from environment config
     * Redirect URL may use unsecured HTTP URL for Plaid Sandbox environment.
     * You can provide different URLs for different Plaid Usages
     * @see BazaPlaidService.getPlaidLink
     */
    export let BAZA_PLAID_DEFAULT_URL = undefined;

    /**
     * Default HTML template for Default Redirect Handler
     * You can provide your own template by updating this variable
     * @see BazaPlaidApiEnvironment.BAZA_PLAID_DEFAULT_URL
     */
    export let BAZA_PLAID_HTML_TEMPLATE = bazaPlaidRedirectHandlerHbs;

    export interface Plaid {
        /**
         * Enables Production environment instead of sandbox for Plaid API
         * @see https://plaid.com/docs/quickstart/
         */
        BAZA_PLAID_PRODUCTION: string;

        /**
         * Plaid Client ID
         * @see https://plaid.com/docs/quickstart/
         */
        BAZA_PLAID_CLIENT_ID: string;

        /**
         * Plaid Client Secret
         * @see https://plaid.com/docs/quickstart/
         */
        BAZA_PLAID_CLIENT_SECRET: string;

        /**
         * Client Name (Team identifier)
         * @see https://plaid.com/docs/quickstart/
         */
        BAZA_PLAID_CLIENT_NAME: string;

        /**
         * Redirect URL for Plaid Link widget (optional)
         * By default Baza provides default redirect endpoint which will calls `window.postMessage` with results
         * Use {{ api }}, {{ cms }} and {{ web }} tags for providing API, CMS or Web URLs from environment config
         * Also, http:// will be automatically replaced with https://
         * @see https://plaid.com/docs/quickstart/
         * @see https://plaid.com/docs/link/
         */
        BAZA_PLAID_REDIRECT_URL?: string;
    }
}
