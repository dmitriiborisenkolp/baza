import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaPlaidErrorCodes } from '@scaliolabs/baza-plaid-shared';
import { HttpStatus } from '@nestjs/common';

/**
 * All errors from Plaid API will be returned as BazaError with `BazaPlaidApiError` error code. Additionally,
 * you will find Plaid Error Code in Details object
 */
export class BazaPlaidApiErrorException extends BazaAppException {
    constructor(plaidErrorCode: string, plaidErrorMessage: string) {
        super(
            BazaPlaidErrorCodes.BazaPlaidApiError,
            plaidErrorMessage,
            HttpStatus.INTERNAL_SERVER_ERROR,
            {},
            { plaidErrorCode, plaidErrorMessage },
        );
    }
}
