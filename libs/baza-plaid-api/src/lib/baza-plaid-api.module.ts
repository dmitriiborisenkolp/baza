import { Module } from '@nestjs/common';
import { BazaPlaidApiConfig } from './baza-plaid-api.config';
import { BazaEnvModule, EnvService } from '@scaliolabs/baza-core-api';
import { BazaPlaidApiEnvironment } from './baza-plaid-api.environment';
import { BazaPlaidController } from './controllers/baza-plaid.controller';
import { BazaPlaidService } from './services/baza-plaid.service';

@Module({
    imports: [BazaEnvModule],
    controllers: [BazaPlaidController],
    providers: [
        {
            provide: BazaPlaidApiConfig,
            inject: [EnvService],
            useFactory: (env: EnvService) =>
                new BazaPlaidApiConfig(
                    env.getAsBoolean<BazaPlaidApiEnvironment.Plaid>('BAZA_PLAID_PRODUCTION'),
                    env.getAsString<BazaPlaidApiEnvironment.Plaid>('BAZA_PLAID_CLIENT_ID'),
                    env.getAsString<BazaPlaidApiEnvironment.Plaid>('BAZA_PLAID_CLIENT_SECRET'),
                    env.getAsString<BazaPlaidApiEnvironment.Plaid>('BAZA_PLAID_CLIENT_NAME'),
                    env.getAsString<BazaPlaidApiEnvironment.Plaid>('BAZA_PLAID_REDIRECT_URL', {
                        defaultValue: BazaPlaidApiEnvironment.BAZA_PLAID_DEFAULT_URL,
                    }),
                ),
        },
        BazaPlaidService,
    ],
    exports: [BazaPlaidApiConfig, BazaPlaidService],
})
export class BazaPlaidApiModule {}
