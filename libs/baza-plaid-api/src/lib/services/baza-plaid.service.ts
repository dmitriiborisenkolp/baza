import { Injectable } from '@nestjs/common';
import {
    Configuration,
    CountryCode,
    LinkTokenCreateRequest,
    PlaidApi,
    PlaidEnvironments,
    ProcessorTokenCreateRequestProcessorEnum,
    Products,
} from 'plaid';
import { BazaPlaidApiConfig } from '../baza-plaid-api.config';
import { AccountEntity, BazaAccountRepository, ProjectService } from '@scaliolabs/baza-core-api';
import { BazaPlaidEnvironment, BazaPlaidLinkRequestDto, BazaPlaidLinkResponseDto } from '@scaliolabs/baza-plaid-shared';
import { ulid } from 'ulid';
import { BazaPlaidApiErrorException } from '../exceptions/baza-plaid-api-error.exception';
import { replaceTags, withoutTrailingSlash } from '@scaliolabs/baza-core-shared';
import { AuthGetResponse, IdentityGetResponse } from 'plaid/api';

/**
 * Baza Plaid Service
 * The Services provides Plaid Client + Additional helper methods to work with Plaid Link
 * @see BazaPlaidService.client
 * @see BazaPlaidService.getPlaidLink
 * @see BazaPlaidService.getExchangeToken
 */
@Injectable()
export class BazaPlaidService {
    constructor(
        private readonly config: BazaPlaidApiConfig,
        private readonly accountRepository: BazaAccountRepository,
        private readonly projectsService: ProjectService,
    ) {}

    /**
     * Returns Plaid Client
     * Plaid Client is configured with Environment variables: BAZA_PLAID_PRODUCTION, BAZA_PLAID_CLIENT_ID,
     * BAZA_PLAID_CLIENT_SECRET, BAZA_PLAID_CLIENT_NAME and optionally BAZA_PLAID_REDIRECT_URL
     * @see BazaPlaidApiEnvironment
     */
    get client(): PlaidApi {
        const configuration = new Configuration({
            basePath: this.config.production ? PlaidEnvironments.production : PlaidEnvironments.sandbox,
            baseOptions: {
                headers: {
                    'PLAID-CLIENT-ID': this.config.clientId,
                    'PLAID-SECRET': this.config.clientSecret,
                },
            },
        });

        return new PlaidApi(configuration);
    }

    /**
     * Returns Plaid Environment
     * Only Production and Sandbox environments supported. "Development" environment is not supported
     * @see BazaPlaidEnvironment
     */
    get environment(): BazaPlaidEnvironment {
        return this.config.production ? BazaPlaidEnvironment.production : BazaPlaidEnvironment.sandbox;
    }

    /**
     * Executes Plaid Promise. If there is an error occured, an instance of BazaPlaidApiErrorException will
     * be throw instead of Error
     * @param zone
     * @private
     */
    async runPlaid<T>(zone: Promise<T>): Promise<T> {
        return zone.catch((error) => {
            const plaidErrorCode = error?.response?.data?.error_code;
            const plaidErrorMessage = error?.response?.data?.error_message;

            throw plaidErrorCode && plaidErrorMessage ? new BazaPlaidApiErrorException(plaidErrorCode, plaidErrorMessage) : error;
        });
    }

    /**
     * Returns Plaid Link token
     * Use method in your libraries to provide integration with Plaid Link. The Plaid Link will be configured to use
     * default redirect handler unless you don' provide custom BAZA_PLAID_REDIRECT_URL
     * You also should add a method to handle onSuccess callback from Plaid Link
     * @see BazaPlaidApiEnvironment.Plaid.BAZA_PLAID_REDIRECT_URL
     * @see BazaPlaidApiEnvironment.BAZA_PLAID_HTML_TEMPLATE
     * @param account
     * @param options
     */
    async getPlaidLink(account: AccountEntity, options: BazaPlaidLinkRequestDto = {}): Promise<BazaPlaidLinkResponseDto> {
        await this.setUpAccountUlid(account);

        const client = this.client;
        const redirectUri = replaceTags(withoutTrailingSlash(this.config.redirectUrl), {
            api: this.projectsService.apiBaseUrl,
            cms: this.projectsService.cmsBaseUrl,
            web: this.projectsService.webBaseUrl,
        });

        const request: LinkTokenCreateRequest = {
            user: {
                client_user_id: account.ulid,
            },
            client_name: this.config.clientName,
            products: [Products.Auth],
            language: 'en',
            redirect_uri: options.withRedirectUrl ? redirectUri : undefined,
            country_codes: [CountryCode.Us],
        };

        const createTokenResponse = await this.runPlaid(client.linkTokenCreate(request));

        return {
            linkToken: createTokenResponse.data.link_token,
            environment: this.environment,
        };
    }

    /**
     * Accepts Public Token generated by Plaid Link and returns Exchange (Access) Token
     * @see https://plaid.com/docs/api/tokens/#itempublic_tokenexchange
     * @param publicToken
     */
    async getExchangeToken(publicToken: string): Promise<string> {
        const response = await this.runPlaid(
            this.client.itemPublicTokenExchange({
                public_token: publicToken,
            }),
        );

        return response.data.access_token;
    }

    /**
     * Returns Bank Details with Access Token
     * Access Token should be generated with Public Token received from Plaid Link on-success callback
     * @see BazaPlaidService.getExchangeToken
     * @param accessToken
     */
    async getBankDetails(accessToken: string): Promise<AuthGetResponse> {
        const response = await this.runPlaid(
            this.client.authGet({
                access_token: accessToken,
            }),
        );

        return response.data;
    }

    /**
     * Returns Identity information
     * Access Token should be generated with Public Token received from Plaid Link on-success callback
     * @see BazaPlaidService.getExchangeToken
     * @param accessToken
     * @param plaidAccountIds
     */
    async getIdentity(accessToken: string, plaidAccountIds: Array<string>): Promise<IdentityGetResponse> {
        const response = await this.runPlaid(
            this.client.identityGet({
                access_token: accessToken,
                options: {
                    account_ids: plaidAccountIds,
                },
            }),
        );

        return response.data;
    }

    /**
     * Returns Procesor Token for Dwolla
     * Access Token should be generated with Public Token received from Plaid Link on-success callback
     * @see BazaPlaidService.getExchangeToken
     * @param accessToken
     * @param plaidAccountId
     */
    async getDwollaProcessorToken(accessToken: string, plaidAccountId: string): Promise<string> {
        const response = await this.runPlaid(
            this.client.processorTokenCreate({
                access_token: accessToken,
                account_id: plaidAccountId,
                processor: ProcessorTokenCreateRequestProcessorEnum.Dwolla,
            }),
        );

        return response.data.processor_token;
    }

    // TODO: Remove it after 1st Jan 2023
    /**
     * Set Up ULID for Account Entity. ULID is used as `client_user_id` for Plaid
     * @param account
     * @private
     */
    private async setUpAccountUlid(account: AccountEntity): Promise<void> {
        if (!account.ulid) {
            account.ulid = ulid();

            await this.accountRepository.save(account);
        }
    }
}
