import { Controller, Get, Header } from '@nestjs/common';
import * as Handlebars from 'handlebars';
import { BazaPlaidApiEnvironment } from '../baza-plaid-api.environment';

/**
 * Plaid Redirect Handler
 * The handler will communicate to client using `window.postMessage` calls. Check Baza Documentaton -> Investment
 * Platform -> Plaid Integration FE Guide for more details
 * The controller should not be displayed in API documentation
 */
@Controller()
export class BazaPlaidController {
    @Get('/baza-plaid/handle')
    @Header('content-type', 'text/html')
    async redirectHandler(): Promise<string> {
        const html = Handlebars.compile(BazaPlaidApiEnvironment.BAZA_PLAID_HTML_TEMPLATE);

        return html({});
    }
}
