import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaDwollaPaymentalListRequest,
    BazaDwollaPaymentalListResponse,
    BazaDwollaPaymentDto,
    BazaDwollaPaymentEndpoint,
    BazaDwollaPaymentEndpointPaths,
} from '@scaliolabs/baza-dwolla-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

/**
 * Dwolla Payments Node Access service
 */
export class BazaDwollaPaymentNodeAccess implements BazaDwollaPaymentEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of Dwolla Payments of current Investor
     * @param request
     */
    async list(request: BazaDwollaPaymentalListRequest): Promise<BazaDwollaPaymentalListResponse> {
        return this.http.get(BazaDwollaPaymentEndpointPaths.list, request);
    }

    /**
     * Returns Dwolla Payment by ULID (only payments of current Investor)
     * @param ulid
     */
    async get(ulid: string): Promise<BazaDwollaPaymentDto> {
        return this.http.get(replacePathArgs(BazaDwollaPaymentEndpointPaths.get, { ulid }));
    }
}
