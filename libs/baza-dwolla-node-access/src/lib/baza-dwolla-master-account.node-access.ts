import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaDwollaMasterAccountBalanceDto,
    BazaDwollaMasterAccountEndpoint,
    BazaDwollaMasterAccountEndpointPaths,
} from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaMasterAccountNodeAccess implements BazaDwollaMasterAccountEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async balance(): Promise<BazaDwollaMasterAccountBalanceDto> {
        return this.http.get(BazaDwollaMasterAccountEndpointPaths.balance);
    }
}
