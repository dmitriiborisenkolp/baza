import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaDwollaPaymentCmsEndpoint,
    BazaDwollaPaymentCmsEndpointPaths,
    BazaDwollaPaymentCmsListRequest,
    BazaDwollaPaymentCmsListResponse,
    BazaDwollaPaymentCmsGetByUlidRequest,
    BazaDwollaPaymentCmsDto,
    BazaDwollaPaymentCmsSyncRequest,
    BazaDwollaPaymentCmsFetchAchDetailsRequest,
    BazaDwollaPaymentCmsFetchAchDetailsResponse,
} from '@scaliolabs/baza-dwolla-shared';

/**
 * Dwolla Payments CMS Node Access service
 */
export class BazaDwollaPaymentCmsNodeAccess implements BazaDwollaPaymentCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of Dwolla Payments (CMS)
     * @param request
     */
    async list(request: BazaDwollaPaymentCmsListRequest): Promise<BazaDwollaPaymentCmsListResponse> {
        return this.http.post(BazaDwollaPaymentCmsEndpointPaths.list, request);
    }

    /**
     * Returns Dwolla Payment by ULID (CMS)
     * @param request
     */
    async getByUlid(request: BazaDwollaPaymentCmsGetByUlidRequest): Promise<BazaDwollaPaymentCmsDto> {
        return this.http.post(BazaDwollaPaymentCmsEndpointPaths.getByUlid, request);
    }

    /**
     * Sync Dwolla Payment (Updates actual status of Dwolla Payment)
     * @param request
     */
    async sync(request: BazaDwollaPaymentCmsSyncRequest): Promise<BazaDwollaPaymentCmsDto> {
        return this.http.post(BazaDwollaPaymentCmsEndpointPaths.sync, request);
    }

    /**
     * Fetches and saves details of related Dwolla ACH Transfer
     * @param request
     */
    async fetchAchDetails(request: BazaDwollaPaymentCmsFetchAchDetailsRequest): Promise<BazaDwollaPaymentCmsFetchAchDetailsResponse> {
        return this.http.post(BazaDwollaPaymentCmsEndpointPaths.fetchAchDetails, request);
    }
}
