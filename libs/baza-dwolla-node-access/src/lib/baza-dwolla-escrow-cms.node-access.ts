import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaDwollaEscrowCmsEndpoint,
    BazaDwollaEscrowCmsEndpointPaths,
    BazaDwollaEscrowCmsSetRequest,
    BazaDwollaEscrowFundingSourceDto,
} from '@scaliolabs/baza-dwolla-shared';

/**
 * Node-Access Service for Dwolla Escrow CMS
 */
export class BazaDwollaEscrowCmsNodeAccess implements BazaDwollaEscrowCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of available Dwolla Funding Sources
     */
    async list(): Promise<Array<BazaDwollaEscrowFundingSourceDto>> {
        return this.http.post(BazaDwollaEscrowCmsEndpointPaths.list);
    }

    /**
     * Sets Dwolla Funding Source as Default Escrow Funding Source
     * Production environments will not allow Account Balance as Escrow FS
     * UAT environments will not allow Bank Account as Escrow FS
     * @param request
     */
    async set(request: BazaDwollaEscrowCmsSetRequest): Promise<BazaDwollaEscrowFundingSourceDto> {
        return this.http.post(BazaDwollaEscrowCmsEndpointPaths.set, request);
    }
}
