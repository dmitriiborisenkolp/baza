import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaDwollaWebhookDto, BazaDwollaWebhookEndpoint, BazaDwollaWebhookEndpointPaths } from '@scaliolabs/baza-dwolla-shared';

export class BazaDwollaWebhookNodeAccess implements BazaDwollaWebhookEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async accept(request: BazaDwollaWebhookDto): Promise<void> {
        return this.http.post(BazaDwollaWebhookEndpointPaths.accept, request, {
            asJsonResponse: false,
        });
    }
}
