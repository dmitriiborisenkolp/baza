import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaDwollaE2eEndpoint,
    BazaDwollaE2eEndpointPaths,
    BazaDwollaE2eGetTransferValueRequest,
    BazaDwollaE2eGetTransferValueResponse,
    BazaDwollaE2eTransferFundsFromMasterAccountRequest,
    BazaDwollaWebhookDto,
} from '@scaliolabs/baza-dwolla-shared';

/**
 * E2E Helpers for Dwolla
 */
export class BazaDwollaE2eNodeAccess implements BazaDwollaE2eEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Run Sandbox Simulation
     * Dwolla Sandbox will not process any bank transfer by default. You should call this method to run simulations.
     */
    async runSandboxSimulations(): Promise<void> {
        return this.http.get(BazaDwollaE2eEndpointPaths.runSandboxSimulations, undefined, {
            asJsonResponse: false,
        });
    }

    /**
     * Sends Webhook Event to Baza Event Bus
     * Use it to simulate webhook events from Dwolla
     * @param event
     */
    async simulateWebhookEvent(event: BazaDwollaWebhookDto): Promise<void> {
        return this.http.post(BazaDwollaE2eEndpointPaths.simulateWebhookEvent, event, {
            asJsonResponse: false,
        });
    }

    /**
     * Transfers funds from Master Account to given Dwolla Customer
     * Automatically executes sandbox-simulations
     * @param request
     */
    async transferFundsFromMasterAccount(request: BazaDwollaE2eTransferFundsFromMasterAccountRequest): Promise<void> {
        return this.http.post(BazaDwollaE2eEndpointPaths.transferFundsFromMasterAccount, request, {
            asJsonResponse: false,
        });
    }

    /**
     * Returns actual Transfer Value (Amount) by Dwolla Transfer Id
     * @param request
     */
    async getTransferValue(request: BazaDwollaE2eGetTransferValueRequest): Promise<BazaDwollaE2eGetTransferValueResponse> {
        return this.http.post(BazaDwollaE2eEndpointPaths.getTransferValue, request);
    }
}
