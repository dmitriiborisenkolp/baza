# baza-core-cms

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-core-cms` to execute the unit tests.

## Build

Run `nx build baza-core-cms`

## Publish

Run `yarn publish:baza-core-cms`
