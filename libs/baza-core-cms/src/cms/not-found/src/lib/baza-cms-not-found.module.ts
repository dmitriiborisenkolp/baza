import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaNotFoundComponent } from './components/not-found/baza-not-found.component';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        RouterModule,
        NzLayoutModule,
        NzTypographyModule,
    ],
    declarations: [
        BazaNotFoundComponent,
    ],
    exports: [
        BazaNotFoundComponent,
    ],
})
export class BazaCmsNotFoundModule {}
