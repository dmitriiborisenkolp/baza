import { Routes } from '@angular/router';
import { BazaNotFoundComponent } from './components/not-found/baza-not-found.component';

export const bazaNotFoundCmsRoutes: Routes = [
    {
        path: 'not-found',
        pathMatch: 'full',
        component: BazaNotFoundComponent,
    },
];
