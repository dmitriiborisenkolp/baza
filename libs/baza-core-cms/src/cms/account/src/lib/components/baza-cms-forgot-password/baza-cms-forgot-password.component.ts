import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthValidatorsService } from '@scaliolabs/baza-core-ng';
import { BazaNgMessagesService, ngEmptyInputValue, ngFocusField, ngTriggerFormValidations } from '@scaliolabs/baza-core-ng';
import { ActivatedRoute, Router } from '@angular/router';
import { BazaAccountDataAccess } from '@scaliolabs/baza-core-data-access';
import { takeUntil } from 'rxjs/operators';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes } from '@scaliolabs/baza-core-shared';
import { TranslateService } from '@ngx-translate/core';

interface FormValue {
    email: string;
}

interface State {
    form: FormGroup;
    customErrorEmail?: any;
}

@Component({
    templateUrl: './baza-cms-forgot-password.component.html',
    styleUrls: ['./baza-cms-forgot-password.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsForgotPasswordComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('email') emailRef: ElementRef<HTMLInputElement>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            email: ['', this.authValidators.emailValidators()],
        }),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly authValidators: AuthValidatorsService,
        private readonly endpoint: BazaAccountDataAccess,
        private readonly bazaNgMessages: BazaNgMessagesService,
        private readonly router: Router,
        private readonly translate: TranslateService,
        private readonly activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        const email = this.activatedRoute.snapshot.queryParams['email'];

        if (email) {
            this.state.form.patchValue({
                email,
            } as Partial<FormValue>);
        }
    }

    ngAfterViewInit(): void {
        ngFocusField(this.emailRef);
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    i18n(key: string): string {
        return `baza.cms.account.components.bazaCmsForgotPassword.${key}`;
    }

    submit(): void {
        ngTriggerFormValidations(this.state.form);

        if (! this.state.form.valid) {
            return;
        }

        this.state = {
            ...this.state,
            customErrorEmail: undefined,
        };

        this.endpoint.sendResetPasswordLink({
            email: this.formValue.email,
        }).pipe(
            takeUntil(this.ngOnDestroy$),
        ).subscribe(
            () => {
                this.bazaNgMessages.success({
                    message: this.i18n('success'),
                    translate: true,
                });

                this.router.navigate(['/auth/sign-in']);
            },
            (err: BazaError) => {
                if (isBazaErrorResponse(err)) {
                    if (err.code === AccountErrorCodes.BazaAccountNotFound) {
                        this.state = {
                            ...this.state,
                            customErrorEmail: this.translate.instant(this.i18n(`errors.${AccountErrorCodes.BazaAccountNotFound}`)),
                        };

                        ngEmptyInputValue(this.state.form.get('email'));
                        ngFocusField(this.emailRef);
                    }
                }

                this.cdr.markForCheck();

                this.state.form.enable();
            },
        );

        this.state.form.disable();
    }
}
