import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BazaCms2faSettingsComponent } from './baza-cms-2fa-settings.component';

@Injectable()
export class BazaCms2faSettingsService {
    constructor(
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    open(parentVcr: ViewContainerRef): void {
        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaCms2faSettingsComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], parentVcr.injector);

        const component = factory.create(injector);

        parentVcr.insert(component.hostView);

        setTimeout(() => {
            component.instance.nzModalRef = this.nzModal.create({
                nzContent: component.instance.nzModal,
                nzFooter: null,
                nzWidth: 620,
            });

            component.instance.parentVcr = parentVcr;
        });
    }
}
