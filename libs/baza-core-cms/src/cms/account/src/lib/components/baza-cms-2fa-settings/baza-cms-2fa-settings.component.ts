import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    TemplateRef,
    ViewChild,
    ViewContainerRef,
} from '@angular/core';
import { BazaLoadingService, BazaNgMessagesService, genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { interval, Observable, Subject } from 'rxjs';
import { BazaAuth2FASettingsDataAccess } from '@scaliolabs/baza-core-data-access';
import {
    Auth2FAConfigResponse,
    Auth2FAUpdateSettingsRequest,
    Auth2FAVerificationMethod,
    isBazaErrorResponse,
} from '@scaliolabs/baza-core-shared';
import { finalize, retryWhen, take, takeUntil } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { BazaCmsPasswordModalService } from '../baza-cms-password-modal/baza-cms-password-modal.service';

const TTL_PASSWORD = 60 /* seconds */ * 1000;

interface State {
    settings?: Auth2FAConfigResponse;
    loading: {
        [Auth2FAVerificationMethod.Email]: boolean;
        [Auth2FAVerificationMethod.PhoneSMS]: boolean;
        [Auth2FAVerificationMethod.PhoneCall]: boolean;
        [Auth2FAVerificationMethod.GoogleAuthenticator]: boolean;
        [Auth2FAVerificationMethod.MicrosoftAuthenticator]: boolean;
    };
    password?: string;
}

@Component({
    templateUrl: './baza-cms-2fa-settings.component.html',
    styleUrls: ['./baza-cms-2fa-settings.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [BazaLoadingService],
})
export class BazaCms2faSettingsComponent implements OnInit, OnDestroy {
    @ViewChild('nzModal') nzModal: TemplateRef<any>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        loading: {
            [Auth2FAVerificationMethod.Email]: false,
            [Auth2FAVerificationMethod.PhoneSMS]: false,
            [Auth2FAVerificationMethod.PhoneCall]: false,
            [Auth2FAVerificationMethod.GoogleAuthenticator]: false,
            [Auth2FAVerificationMethod.MicrosoftAuthenticator]: false,
        },
    };

    public nzModalRef: NzModalRef<any>;
    public parentVcr: ViewContainerRef;

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly loading: BazaLoadingService,
        private readonly dataAccess: BazaAuth2FASettingsDataAccess,
        private readonly passwordModal: BazaCmsPasswordModalService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnInit(): void {
        this.load();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza.cms.account.components.bazaCms2FASettings.${key}`;
    }

    get isLoading$(): Observable<boolean> {
        return this.loading.isLoading$;
    }

    get hasAny2FAEnabled(): boolean {
        return this.state.settings.enabled2FAMethods.length > 0;
    }

    load(): void {
        const loading = this.loading.addLoading();

        this.dataAccess
            .config()
            .pipe(
                retryWhen(genericRetryStrategy()),
                finalize(() => loading.complete()),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe(
                (settings) => {
                    this.state = {
                        ...this.state,
                        settings,
                    };

                    this.cdr.markForCheck();
                },
                (err) => {
                    this.nzModalRef.close();

                    throw err;
                },
            );
    }

    methodIcon(method: Auth2FAVerificationMethod): string {
        switch (method) {
            default: {
                return 'mail';
            }

            case Auth2FAVerificationMethod.PhoneSMS: {
                return 'message';
            }

            case Auth2FAVerificationMethod.PhoneCall: {
                return 'phone';
            }

            case Auth2FAVerificationMethod.GoogleAuthenticator: {
                return 'google';
            }

            case Auth2FAVerificationMethod.MicrosoftAuthenticator: {
                return 'windows';
            }
        }
    }

    methodTitle(method: Auth2FAVerificationMethod): string {
        return this.i18n(`methods.${method}.title`);
    }

    methodDescription(method: Auth2FAVerificationMethod): string {
        return this.i18n(`methods.${method}.description`);
    }

    is2FAMethodEnabled(method: Auth2FAVerificationMethod): boolean {
        return this.state.settings.enabled2FAMethods.includes(method);
    }

    is2FAMethodForceEnabled(method: Auth2FAVerificationMethod): boolean {
        return this.state.settings.forced2FAMethods.includes(method);
    }

    async toggle2FAMethod($event: MouseEvent, method: Auth2FAVerificationMethod): Promise<void> {
        $event.preventDefault();

        if (this.is2FAMethodForceEnabled(method)) {
            return;
        }

        this.state.loading[`${method}`] = true;

        await this.getCurrentPassword();

        if (this.state.password) {
            const request: Auth2FAUpdateSettingsRequest = {
                password: this.state.password,
            };

            const observable = this.is2FAMethodEnabled(method)
                ? this.factory2FADisableObservable(method, request)
                : this.factory2FAEnableObservable(method, request);

            observable
                .pipe(
                    finalize(() => (this.state.loading[`${method}`] = false)),
                    finalize(() => this.cdr.markForCheck()),
                    takeUntil(this.ngOnDestroy$),
                )
                .subscribe(
                    () => {
                        this.load();
                    },
                    (err) => (isBazaErrorResponse(err) ? this.ngMessages.bazaError(err) : this.ngMessages.httpError()),
                );
        } else {
            this.state.loading[`${method}`] = false;
        }
    }

    async getCurrentPassword(): Promise<void> {
        if (this.state.password) {
            return;
        }

        this.state = {
            ...this.state,
            password: await this.passwordModal.open(this.parentVcr),
        };

        if (this.state.password) {
            interval(TTL_PASSWORD)
                .pipe(take(1), takeUntil(this.ngOnDestroy$))
                .subscribe(() => {
                    this.state = {
                        ...this.state,
                        password: undefined,
                    };
                });
        }
    }

    private factory2FAEnableObservable(method: Auth2FAVerificationMethod, request: Auth2FAUpdateSettingsRequest): Observable<void> {
        switch (method) {
            default: {
                throw new Error('Unsupported 2FA method');
            }

            case Auth2FAVerificationMethod.Email: {
                return this.dataAccess.enable2FAEmail(request);
            }

            case Auth2FAVerificationMethod.GoogleAuthenticator: {
                return this.dataAccess.enable2FAGoogle(request);
            }
        }
    }

    private factory2FADisableObservable(method: Auth2FAVerificationMethod, request: Auth2FAUpdateSettingsRequest): Observable<void> {
        switch (method) {
            default: {
                throw new Error('Unsupported 2FA method');
            }

            case Auth2FAVerificationMethod.Email: {
                return this.dataAccess.disable2FAEmail(request);
            }

            case Auth2FAVerificationMethod.GoogleAuthenticator: {
                return this.dataAccess.disable2FAGoogle(request);
            }
        }
    }
}
