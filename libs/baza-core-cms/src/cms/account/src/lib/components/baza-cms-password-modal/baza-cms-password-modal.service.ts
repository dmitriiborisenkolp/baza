import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BazaCmsPasswordModalComponent } from './baza-cms-password-modal.component';
import { take } from 'rxjs/operators';

@Injectable()
export class BazaCmsPasswordModalService {
    constructor(
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    open(parentVcr: ViewContainerRef): Promise<string | undefined> {
        return new Promise((resolve) => {
            const factory = this.componentFactoryResolver.resolveComponentFactory(BazaCmsPasswordModalComponent);
            const injector = ReflectiveInjector.fromResolvedProviders([], parentVcr.injector);

            const component = factory.create(injector);

            parentVcr.insert(component.hostView);

            setTimeout(() => {
                component.instance.nzModalRef = this.nzModal.create({
                    nzContent: component.instance.nzModal,
                    nzFooter: null,
                    nzWidth: 520,
                    nzAutofocus: null,
                    nzClosable: false,
                    nzKeyboard: false,
                });

                component.instance.nzModalRef.afterClose.pipe(
                    take(1),
                ).subscribe((result) => resolve(result));
            });
        });
    }
}
