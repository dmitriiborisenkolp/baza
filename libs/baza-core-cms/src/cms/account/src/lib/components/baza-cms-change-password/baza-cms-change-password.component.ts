import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaNgMessagesService, ngEmptyInputValue, ngFocusField, ngTriggerFormValidations } from '@scaliolabs/baza-core-ng';
import { BazaAccountDataAccess } from '@scaliolabs/baza-core-data-access';
import { Subject, throwError } from 'rxjs';
import { catchError, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes } from '@scaliolabs/baza-core-shared';
import { AuthValidatorsService } from '@scaliolabs/baza-core-ng';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';

interface FormValue {
    oldPassword: string;
    newPassword: string;
    repeatPassword: string;
}

interface State {
    form: FormGroup;
    customErrorOldPassword?: string;
    customErrorRepeatPassword?: string;
}

@Component({
    selector: 'baza-cms-change-password',
    templateUrl: './baza-cms-change-password.component.html',
    styleUrls: ['./baza-cms-change-password.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsChangePasswordComponent implements OnDestroy {
    @ViewChild('nzModal') nzModal: TemplateRef<any>;
    @ViewChild('oldPassword') oldPasswordRef: ElementRef<HTMLInputElement>;
    @ViewChild('newPassword') newPasswordRef: ElementRef<HTMLInputElement>;
    @ViewChild('repeatPassword') repeatPasswordRef: ElementRef<HTMLInputElement>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            oldPassword: ['', [Validators.required, ...this.authValidators.signInValidators()]],
            newPassword: ['', [Validators.required, ...this.authValidators.signUpValidators()]],
            repeatPassword: ['', [Validators.required, ...this.authValidators.signInValidators()]],
        }, {
            asyncValidators: [
                this.authValidators.newAndRepeatPasswordAsyncValidator(),
            ],
        }),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly endpoint: BazaAccountDataAccess,
        private readonly authValidators: AuthValidatorsService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza.cms.account.components.bazaCmsChangePassword.${key}`;
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    init(nzModal: NzModalRef): void {
        this.state.form.statusChanges.pipe(
            distinctUntilChanged(),
            takeUntil(this.ngOnDestroy$),
        ).subscribe(() => {
            const isOldPasswordValid = this.state.form.get('oldPassword').valid;
            const isNewPasswordValid = this.state.form.get('newPassword').valid;

            nzModal.updateConfig({
                nzOkDisabled: ! (isOldPasswordValid && isNewPasswordValid),
            });
        });
    }

    submit(): Promise<boolean> {
        return new Promise((resolve) => {
            ngTriggerFormValidations(this.state.form);

            setTimeout(() => {
                this.state = {
                    ...this.state,
                    customErrorOldPassword: undefined,
                    customErrorRepeatPassword: undefined,
                };

                if (! this.state.form.valid) {
                    if (this.state.form.errors['passwordsAreNotSame']) {
                        this.state = {
                            ...this.state,
                            customErrorRepeatPassword: this.i18n(`errors.passwordsAreNotSame`),
                        };

                        ngEmptyInputValue(this.state.form.get('repeatPassword'));

                        setTimeout(() => {
                            ngFocusField(this.repeatPasswordRef);
                        });

                        this.cdr.markForCheck();
                    }

                    resolve(false);

                    return;
                }

                const formValue = this.formValue;

                this.state.form.disable();

                this.endpoint.changePassword({
                    oldPassword: formValue.oldPassword,
                    newPassword: formValue.newPassword,
                }).pipe(
                    catchError((err: BazaError<AccountErrorCodes>) => {
                        if (isBazaErrorResponse(err)) {
                            if ([AccountErrorCodes.BazaAccountInvalidCredentials].includes(err.code)) {
                                this.state = {
                                    ...this.state,
                                    customErrorOldPassword: this.i18n(`errors.invalidPassword`),
                                };

                                setTimeout(() => {
                                    ngEmptyInputValue(this.state.form.get('oldPassword'));
                                    ngFocusField(this.oldPasswordRef);
                                });
                            }
                        }

                        return throwError(err);
                    }),
                    takeUntil(this.ngOnDestroy$),
                ).subscribe(
                    () => resolve(true),
                    (err) => {
                        this.ngMessages.bazaError(err);

                        this.state.form.enable();

                        resolve(false);
                    },
                );
            });
        })
    }
}
