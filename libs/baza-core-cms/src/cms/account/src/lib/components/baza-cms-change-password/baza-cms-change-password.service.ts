import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { BazaCmsChangePasswordComponent } from './baza-cms-change-password.component';
import { NzModalService } from 'ng-zorro-antd/modal';

@Injectable()
export class BazaCmsChangePasswordService {
    constructor(
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    open(parentVcr: ViewContainerRef): void {
        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaCmsChangePasswordComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], parentVcr.injector);

        const component = factory.create(injector);

        parentVcr.insert(component.hostView);

        setTimeout(() => {
            const nzModalRef = this.nzModal.create({
                nzContent: component.instance.nzModal,
                nzClosable: false,
                nzOnOk: () => component.instance.submit(),
                nzOkDisabled: true,
            });

            component.instance.init(nzModalRef);
        });
    }
}
