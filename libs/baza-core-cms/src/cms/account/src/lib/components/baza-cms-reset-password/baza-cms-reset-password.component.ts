import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { AuthValidatorsService } from '@scaliolabs/baza-core-ng';
import { BazaAccountDataAccess } from '@scaliolabs/baza-core-data-access';
import { BazaNgMessagesService, ngEmptyInputValue, ngFocusField, ngTriggerFormValidations } from '@scaliolabs/baza-core-ng';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/operators';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountErrorCodes } from '@scaliolabs/baza-core-shared';

interface FormValue {
    newPassword: string;
    repeatPassword: string;
}

interface State {
    form: FormGroup;
    customErrorNewPassword?: any;
    token?: string;
}

@Component({
    templateUrl: './baza-cms-reset-password.component.html',
    styleUrls: ['./baza-cms-reset-password.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsResetPasswordComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('newPassword') newPasswordRef: ElementRef<HTMLInputElement>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            newPassword: ['', [Validators.required, ...this.authValidators.signUpValidators()]],
            repeatPassword: ['', [Validators.required, ...this.authValidators.signInValidators()]],
        }, {
            asyncValidators: [
                this.authValidators.newAndRepeatPasswordAsyncValidator(),
            ],
        }),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly authValidators: AuthValidatorsService,
        private readonly endpoint: BazaAccountDataAccess,
        private readonly bazaNgMessages: BazaNgMessagesService,
        private readonly router: Router,
        private readonly translate: TranslateService,
        private readonly activatedRoute: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        const token = this.activatedRoute.snapshot.queryParams['token'];

        if (! token) {
            this.bazaNgMessages.error({
                message: this.i18n('noToken'),
                translate: true,
            });
        }

        this.state = {
            ...this.state,
            token,
        };
    }

    ngAfterViewInit(): void {
        ngFocusField(this.newPasswordRef);
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    i18n(key: string): string {
        return `baza.cms.account.components.bazaCmsResetPassword.${key}`;
    }

    submit(): void {
        ngTriggerFormValidations(this.state.form);

        setTimeout(() => {
            if (! this.state.form.valid) {
                if (this.state.form.errors['passwordsAreNotSame']) {
                    this.state = {
                        ...this.state,
                        customErrorNewPassword: this.i18n(`errors.passwordsAreNotSame`),
                    };

                    ngEmptyInputValue(this.state.form.get('newPassword'));
                    ngEmptyInputValue(this.state.form.get('oldPassword'));
                    ngFocusField(this.newPasswordRef);

                    this.cdr.markForCheck();
                }

                return;
            }

            this.state = {
                ...this.state,
                customErrorNewPassword: undefined,
            };

            this.endpoint.resetPassword({
                token: this.state.token,
                newPassword: this.formValue.newPassword,
            }).pipe(
                takeUntil(this.ngOnDestroy$),
            ).subscribe(
                () => {
                    this.bazaNgMessages.success({
                        message: this.i18n('success'),
                        translate: true,
                    });

                    this.router.navigate(['/auth/sign-in']);
                },
                (err: BazaError) => {
                    if (isBazaErrorResponse(err) && err.code === AccountErrorCodes.BazaAccountTokenNotFound) {
                        this.bazaNgMessages.error({
                            message: this.i18n('used'),
                            translate: true,
                        });

                        this.router.navigate(['/auth/sign-in']);
                    } else {
                        this.bazaNgMessages.error({
                            message: this.i18n('failed'),
                            translate: true,
                        });
                    }

                    this.state.form.enable();
                },
            );

            this.state.form.disable();
        });
    }
}
