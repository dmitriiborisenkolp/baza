import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, OnDestroy, Output, TemplateRef, ViewChild } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { BazaAuthDataAccess } from '@scaliolabs/baza-core-data-access';
import { BazaNgMessagesService, genericRetryStrategy, JwtService, ngEmptyInputValue, ngFocusField } from '@scaliolabs/baza-core-ng';
import { retryWhen, takeUntil } from 'rxjs/operators';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

interface FormValue {
    password: string;
}

interface State {
    form: FormGroup;
    customErrorPassword?: string;
}

@Component({
    templateUrl: './baza-cms-password-modal.component.html',
    styleUrls: ['./baza-cms-password-modal.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsPasswordModalComponent implements AfterViewInit, OnDestroy {
    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<any>;
    @ViewChild('focus') focusRef: ElementRef<HTMLInputElement>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public nzModalRef: NzModalRef<any>;

    public state: State = {
        form: this.fb.group({
            password: ['', [Validators.required]],
        }),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly jwt: JwtService,
        private readonly cdr: ChangeDetectorRef,
        private readonly dataAccess: BazaAuthDataAccess,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngAfterViewInit(): void {
        this.focus();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza.cms.account.components.bazaCmsPasswordModal.${key}`;
    }

    focus(): void {
        if (this.focusRef && this.focusRef.nativeElement) {
            this.focusRef.nativeElement.focus();
        }
    }

    submit(): void {
        if (! this.state.form.valid) {
            return;
        }

        const formValue: FormValue = this.state.form.value;

        this.state.form.disable();

        this.state = {
            ...this.state,
            customErrorPassword: undefined,
        };

        this.dataAccess.verifyCredentials({
            email: this.jwt.jwtPayload.accountEmail,
            password: formValue.password,
        }).pipe(
            retryWhen(genericRetryStrategy()),
            takeUntil(this.ngOnDestroy$),
        ).subscribe(
            (verified) => {
                if (verified) {
                    this.nzModalRef.close(formValue.password);
                } else {
                    this.state.form.enable();

                    this.state = {
                        ...this.state,
                        customErrorPassword: this.i18n('invalid'),
                    };

                    setTimeout(() => {
                        ngEmptyInputValue(this.state.form.get('password'));
                        ngFocusField(this.focusRef);
                    });

                    this.cdr.markForCheck();
                }
            },
            (err) => {
                this.state.form.enable();

                isBazaErrorResponse(err)
                    ? this.ngMessages.bazaError(err)
                    : this.ngMessages.httpError();
            },
        );
    }

    cancel(): void {
        this.nzModalRef.close();
    }
}
