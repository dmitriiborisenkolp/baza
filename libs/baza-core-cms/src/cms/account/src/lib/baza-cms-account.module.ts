import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { BazaCmsChangePasswordComponent } from './components/baza-cms-change-password/baza-cms-change-password.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { BazaCmsChangePasswordService } from './components/baza-cms-change-password/baza-cms-change-password.service';
import { NzFormModule } from 'ng-zorro-antd/form';
import { BazaNgCoreModule } from '@scaliolabs/baza-core-ng';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BazaAccountDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { BazaCmsForgotPasswordComponent } from './components/baza-cms-forgot-password/baza-cms-forgot-password.component';
import { RouterModule } from '@angular/router';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { BazaCmsResetPasswordComponent } from './components/baza-cms-reset-password/baza-cms-reset-password.component';
import { BazaCms2faSettingsComponent } from './components/baza-cms-2fa-settings/baza-cms-2fa-settings.component';
import { BazaCms2faSettingsService } from './components/baza-cms-2fa-settings/baza-cms-2fa-settings.service';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { BazaCmsPasswordModalService } from './components/baza-cms-password-modal/baza-cms-password-modal.service';
import { BazaCmsPasswordModalComponent } from './components/baza-cms-password-modal/baza-cms-password-modal.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        NzModalModule,
        NzFormModule,
        BazaNgCoreModule,
        NzInputModule,
        ReactiveFormsModule,
        BazaAccountDataAccessModule,
        NzTypographyModule,
        RouterModule,
        NzButtonModule,
        BazaNgCoreModule,
        NzAlertModule,
        NzIconModule,
        NzSwitchModule,
        NzToolTipModule,
    ],
    declarations: [
        BazaCmsChangePasswordComponent,
        BazaCmsForgotPasswordComponent,
        BazaCmsResetPasswordComponent,
        BazaCms2faSettingsComponent,
        BazaCmsPasswordModalComponent,
    ],
    providers: [
        BazaCmsChangePasswordService,
        BazaCms2faSettingsService,
        BazaCmsPasswordModalService,
    ],
    exports: [
        BazaCmsChangePasswordComponent,
        BazaCmsForgotPasswordComponent,
        BazaCmsResetPasswordComponent,
        BazaCms2faSettingsComponent,
        BazaCmsPasswordModalComponent,
    ]
})
export class BazaCmsAccountModule {
}
