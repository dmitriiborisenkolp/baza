import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCmsAuthComponent } from './components/baza-cms-auth/baza-cms-auth.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { BazaAuthDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { NzFormModule } from 'ng-zorro-antd/form';
import { BazaNgCoreModule } from '@scaliolabs/baza-core-ng';
import { RouterModule } from '@angular/router';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { BazaCmsAuthVerifyComponent } from './components/baza-cms-auth-verify/baza-cms-auth-verify.component';
import { BazaCmsAuthMethodComponent } from './components/baza-cms-auth-method/baza-cms-auth-method.component';
import { BazaCmsAuthMethodGoogleAuthenticatorComponent } from './components/baza-cms-auth-method-google-authenticator/baza-cms-auth-method-google-authenticator.component';
import { BazaCmsAuthMethodMicrosoftAuthenticatorComponent } from './components/baza-cms-auth-method-microsoft-authenticator/baza-cms-auth-method-microsoft-authenticator.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        ReactiveFormsModule,
        NzLayoutModule,
        NzInputModule,
        NzButtonModule,
        NzIconModule,
        BazaAuthDataAccessModule,
        NzFormModule,
        BazaNgCoreModule,
        RouterModule,
        NzMessageModule,
    ],
    declarations: [
        BazaCmsAuthComponent,
        BazaCmsAuthVerifyComponent,
        BazaCmsAuthMethodComponent,
        BazaCmsAuthMethodGoogleAuthenticatorComponent,
        BazaCmsAuthMethodMicrosoftAuthenticatorComponent,
    ],
})
export class BazaCmsAuthModule {
}
