import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    templateUrl: './baza-cms-auth-method-microsoft-authenticator.component.html',
    styleUrls: ['./baza-cms-auth-method-microsoft-authenticator.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsAuthMethodMicrosoftAuthenticatorComponent {}
