import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Auth2FAVerificationMethod, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { Router } from '@angular/router';
import { BazaAuth2FADataAccess } from '@scaliolabs/baza-core-data-access';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

interface State {
    resolvedData?: ResolvedData;
}

interface ResolvedData {
    email: string;
    password: string;
    methods: Array<Auth2FAVerificationMethod>;
}

@Component({
    templateUrl: './baza-cms-auth-method.component.html',
    styleUrls: ['./baza-cms-auth-method.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsAuthMethodComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {};

    constructor(
        private readonly router: Router,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly dataAccess: BazaAuth2FADataAccess,
        private readonly loading: BazaLoadingService,
    ) {
        // getCurrentNavigation is not accessible in ngOnInit hook
        this.state = {
            ...this.state,
            resolvedData: (this.router.getCurrentNavigation()?.extras?.state as ResolvedData) || ({} as ResolvedData),
        };
    }

    ngOnInit(): void {
        if (!this.state.resolvedData || !this.state.resolvedData.methods || !this.state.resolvedData.email) {
            this.router.navigate(['/auth']);

            return;
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next(undefined);
    }

    i18n(key: string): string {
        return `baza.cms.auth.components.bazaCmsAuthMethod.${key}`;
    }

    useMethod(method: Auth2FAVerificationMethod): void {
        const loading = this.loading.addLoading();

        const navigate = () => {
            switch (method) {
                case Auth2FAVerificationMethod.Email:
                case Auth2FAVerificationMethod.PhoneSMS:
                case Auth2FAVerificationMethod.PhoneCall: {
                    this.router.navigate(['/auth/verify'], {
                        state: {
                            method,
                            email: this.state.resolvedData.email,
                        },
                    });

                    break;
                }

                case Auth2FAVerificationMethod.GoogleAuthenticator: {
                    this.router.navigate(['/auth/google-authenticator'], {
                        state: {
                            method,
                            email: this.state.resolvedData.email,
                        },
                    });

                    break;
                }

                case Auth2FAVerificationMethod.MicrosoftAuthenticator: {
                    this.router.navigate(['/auth/microsoft-authenticator'], {
                        state: {
                            method,
                            email: this.state.resolvedData.email,
                        },
                    });

                    break;
                }
            }
        };

        this.dataAccess
            .auth2FA({
                method,
                email: this.state.resolvedData.email,
                password: this.state.resolvedData.password,
            })
            .pipe(
                finalize(() => loading.complete()),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe(
                () => {
                    navigate();
                },
                (err) => (isBazaErrorResponse(err) ? this.ngMessages.bazaError(err) : this.ngMessages.httpError()),
            );
    }

    methodIcon(method: Auth2FAVerificationMethod): string {
        switch (method) {
            default: {
                return 'mail';
            }

            case Auth2FAVerificationMethod.PhoneSMS: {
                return 'message';
            }

            case Auth2FAVerificationMethod.PhoneCall: {
                return 'phone';
            }

            case Auth2FAVerificationMethod.GoogleAuthenticator: {
                return 'google';
            }

            case Auth2FAVerificationMethod.MicrosoftAuthenticator: {
                return 'windows';
            }
        }
    }

    methodTitle(method: Auth2FAVerificationMethod): string {
        return this.i18n(`methods.${method}.title`);
    }
}
