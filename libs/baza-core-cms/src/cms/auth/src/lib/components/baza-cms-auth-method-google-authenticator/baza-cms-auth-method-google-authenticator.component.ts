import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Auth2FAErrorCodes, Auth2FAVerificationMethod, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { Router } from '@angular/router';
import {
    BazaLoadingService,
    BazaNgMessagesService,
    JwtService,
    ngEmptyInputValue,
    ngFocusField,
    ngTriggerFormValidations,
} from '@scaliolabs/baza-core-ng';
import { BazaAuth2FADataAccess } from '@scaliolabs/baza-core-data-access';
import { finalize, takeUntil, tap } from 'rxjs/operators';

interface ResolvedData {
    method: Auth2FAVerificationMethod;
    email: string;
}

interface State {
    form: FormGroup;
    message?: string;
    verificationError?: string;
    resolvedData?: ResolvedData;
}

interface FormValue {
    otp: string;
}
@Component({
    templateUrl: './baza-cms-auth-method-google-authenticator.component.html',
    styleUrls: ['./baza-cms-auth-method-google-authenticator.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsAuthMethodGoogleAuthenticatorComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('focus') focusRef: ElementRef<HTMLInputElement>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            otp: ['', [Validators.required]],
        }),
    };

    public qrCode: string;

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly router: Router,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly dataAccess: BazaAuth2FADataAccess,
        private readonly jwtService: JwtService,
        private readonly loading: BazaLoadingService,
    ) {
        // getCurrentNavigation is not accessible in ngOnInit hook
        this.state = {
            ...this.state,
            resolvedData: (this.router.getCurrentNavigation()?.extras?.state as ResolvedData) || ({} as ResolvedData),
        };
    }

    ngOnInit(): void {
        if (!this.state.resolvedData || !this.state.resolvedData.method || !this.state.resolvedData.email) {
            this.router.navigate(['/auth']);

            return;
        }

        switch (this.state.resolvedData.method) {
            default: {
                this.ngMessages.error({
                    message: this.i18n('unsupported'),
                    translate: true,
                });

                this.router.navigate(['/auth']);

                break;
            }

            case Auth2FAVerificationMethod.GoogleAuthenticator: {
                this.state = {
                    ...this.state,
                    message: 'message.google-authenticator',
                };

                break;
            }
        }

        this.generateGoogle2FAQrCode();
    }

    ngAfterViewInit(): void {
        if (this.focusRef && this.focusRef.nativeElement) {
            this.focusRef.nativeElement.focus();
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza.cms.auth.components.bazaCmsAuthVerify.${key}`;
    }

    get inputBoxFormValue(): FormValue {
        return this.state.form.value;
    }

    /**
     * To fetch the Google 2FA QR Code
     */
    generateGoogle2FAQrCode(): void {
        const loading = this.loading.addLoading();

        this.dataAccess
            .generate2FAGoogleQrCode({
                email: this.state.resolvedData?.email,
            })
            .pipe(
                tap(() => loading.complete()),
                finalize(() => loading.complete()),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe(({ qrcodeDataURL }) => {
                this.qrCode = qrcodeDataURL;
                this.cdr.detectChanges();
            });
    }

    submit(): void {
        ngTriggerFormValidations(this.state.form);

        if (!this.state.form.valid) {
            return;
        }

        const code = this.inputBoxFormValue.otp;
        const loading = this.loading.addLoading();

        this.state.form.disable();

        this.state = {
            ...this.state,
            verificationError: undefined,
        };

        this.dataAccess
            .auth2FAVerify({
                code,
                email: this.state.resolvedData.email,
                method: Auth2FAVerificationMethod.GoogleAuthenticator,
            })
            .pipe(
                finalize(() => this.loading.completeAll()),
                finalize(() => this.cdr.markForCheck()),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe(
                (response) => {
                    this.jwtService.setJwtWithAuthResponse(response);

                    loading.complete();

                    this.router.navigate(['/']);
                },
                (err: BazaError) => {
                    if (isBazaErrorResponse(err)) {
                        if (err.code === Auth2FAErrorCodes.AuthGoogle2FAInvalidToken) {
                            this.state = {
                                ...this.state,
                                verificationError: this.i18n('errors.AuthGoogle2FAInvalidToken'),
                            };

                            ngEmptyInputValue(this.state.form.get('otp'));
                            ngFocusField(this.focusRef);
                        } else {
                            this.state = {
                                ...this.state,
                                verificationError: '_unhandledError',
                            };
                        }
                    } else {
                        this.state = {
                            ...this.state,
                            verificationError: '_unhandledError',
                        };
                    }

                    this.state.form.enable();

                    loading.complete();

                    this.cdr.detectChanges();
                },
            );
    }
}
