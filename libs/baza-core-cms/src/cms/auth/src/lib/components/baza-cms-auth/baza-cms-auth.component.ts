import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthValidatorsService, BazaLoadingService, JwtService } from '@scaliolabs/baza-core-ng';
import { BazaAuth2FADataAccess, BazaAuthDataAccess } from '@scaliolabs/baza-core-data-access';
import { finalize, retryWhen, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { genericRetryStrategy, ngEmptyInputValue, ngFocusField, ngTriggerFormValidations } from '@scaliolabs/baza-core-ng';
import { Router } from '@angular/router';
import { Auth2FAVerificationMethod, BazaError, capitalizeFirstLetter, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AuthErrorCodes } from '@scaliolabs/baza-core-shared';
import { AccountRole } from '@scaliolabs/baza-core-shared';

interface FormValue {
    email: string;
    password: string;
}

interface State {
    form: FormGroup;
    customErrorEmail?: string;
    customErrorPassword?: string;
    unhandledError?: string;
}

@Component({
    selector: 'baza-cms-auth',
    templateUrl: './baza-cms-auth.component.html',
    styleUrls: ['./baza-cms-auth.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsAuthComponent implements OnDestroy, AfterViewInit {
    @ViewChild('email') emailRef: ElementRef<HTMLInputElement>;
    @ViewChild('password') passwordRef: ElementRef<HTMLInputElement>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            email: ['', [Validators.required, ...this.authValidators.emailValidators()]],
            password: ['', [Validators.required, ...this.authValidators.signInValidators()]],
        }),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly authValidators: AuthValidatorsService,
        private readonly dataAccessAuth: BazaAuthDataAccess,
        private readonly dataAccessAuth2FA: BazaAuth2FADataAccess,
        private readonly jwtService: JwtService,
        private readonly router: Router,
        private readonly cdr: ChangeDetectorRef,
        private readonly loading: BazaLoadingService,
    ) {}

    ngAfterViewInit(): void {
        ngFocusField(this.emailRef);
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    i18n(key: string): string {
        return `baza.cms.auth.components.bazaCmsAuthSignIn.${key}`;
    }

    submit(): void {
        ngTriggerFormValidations(this.state.form);

        if (! this.state.form.valid) {
            return;
        }

        const formValue = this.formValue;

        this.state.form.disable();

        this.state = {
            ...this.state,
            customErrorEmail: undefined,
            customErrorPassword: undefined,
            unhandledError: undefined,
        };

        this.dataAccessAuth2FA.auth2FAMethods({
            email: formValue.email,
            password: formValue.password,
        }).pipe(
            retryWhen(genericRetryStrategy()),
            finalize(() => {
                this.loading.completeAll();

                this.cdr.markForCheck();
            }),
        ).subscribe(
            (available2FAMethods) => {
                if (available2FAMethods.methods.length === 0) { // No 2-FA options enabled
                    this.authWithout2FA(formValue);
                } else if (available2FAMethods.methods.length === 1) {
                    const method = available2FAMethods.methods[0];

                    this.authWith2FA(formValue, method);
                } else {
                    this.goTo2FAMethodSelect(formValue, available2FAMethods.methods);
                }
            },
            (err) => this.authHandleError(err),
        );
    }

    private authWithout2FA(formValue: FormValue): void {
        this.dataAccessAuth.auth({
            email: formValue.email,
            password: formValue.password,
            requireRole: AccountRole.Admin,
        }).pipe(
            retryWhen(genericRetryStrategy()),
            takeUntil(this.ngOnDestroy$),
        ).subscribe(
            (response) => {
                this.jwtService.setJwtWithAuthResponse(response);

                this.loading.completeAll();

                this.router.navigate(['/']);
            },
            (err: BazaError) => {
                this.authHandleError(err);
            },
        );
    }

    private authWith2FA(formValue: FormValue, method: Auth2FAVerificationMethod): void {
        switch (method) {
            default: {
                this.state = {
                    ...this.state,
                    customErrorEmail: this.i18n(`unsupported`),
                };

                break;
            }

            case Auth2FAVerificationMethod.Email:
            case Auth2FAVerificationMethod.PhoneCall:
            case Auth2FAVerificationMethod.PhoneSMS: {
                const loading = this.loading.addLoading();

                this.state.form.disable();

                this.dataAccessAuth2FA.auth2FA({
                    method,
                    email: formValue.email,
                    password: formValue.password,
                }).pipe(
                    finalize(() => loading.complete()),
                    finalize(() => this.cdr.markForCheck()),
                    takeUntil(this.ngOnDestroy$),
                ).subscribe(
                    () => {
                        this.router.navigate(['/auth/verify'], {
                            state: {
                                method,
                                email: formValue.email,
                            },
                        });
                    },
                    (err) => {
                        this.authHandleError(err);
                    }
                );

                break;
            }
        }
    }

    private goTo2FAMethodSelect(formValue: FormValue, methods: Array<Auth2FAVerificationMethod>): void {
        this.router.navigate(['/auth/method'], {
            state: {
                methods,
                email: formValue.email,
                password: formValue.password,
            },
        });
    }

    private authHandleError(err: BazaError): void {
        if (isBazaErrorResponse(err)) {
            if ([
                AuthErrorCodes.AuthEmailIsNotVerified,
                AuthErrorCodes.AuthAdminRoleIsRequired,
            ].includes(err.code)) {
                this.state = {
                    ...this.state,
                    customErrorEmail: this.i18n(`errors.${err.code}`),
                };

                ngEmptyInputValue(this.state.form.get('email'));
                ngFocusField(this.emailRef);
            } else if ([
                AuthErrorCodes.AuthInvalidCredentials,
            ].includes(err.code)) {
                this.state = {
                    ...this.state,
                    customErrorPassword: this.i18n(`errors.${err.code}`),
                };

                ngEmptyInputValue(this.state.form.get('password'));
                ngFocusField(this.passwordRef);
            } else {
                this.state = {
                    ...this.state,
                    unhandledError: capitalizeFirstLetter(err.message),
                };

                ngEmptyInputValue(this.state.form.get('password'));
                ngFocusField(this.passwordRef);
            }
        } else {
            this.state = {
                ...this.state,
                unhandledError: '_unhandledError',
            };
        }

        this.state.form.enable();

        this.cdr.markForCheck();
    }
}
