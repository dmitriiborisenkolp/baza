import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'baza-cms-styles',
    styleUrls: ['./styles/index.less'],
    encapsulation: ViewEncapsulation.None,
    template: '',
})
export class BazaCmsStylesComponent {
}
