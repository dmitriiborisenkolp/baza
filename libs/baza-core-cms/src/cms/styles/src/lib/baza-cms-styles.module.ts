import { NgModule } from '@angular/core';
import { BazaCmsStylesComponent } from './baza-cms-styles.component';

@NgModule({
    declarations: [
        BazaCmsStylesComponent,
    ],
    exports: [
        BazaCmsStylesComponent,
    ],
})
export class BazaCmsStylesModule {
}
