import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { RouterModule } from '@angular/router';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { BazaCmsLayoutConfig } from './baza-cms-layout.config';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { TranslateModule } from '@ngx-translate/core';
import { BazaAuthNgModule, BazaNgCoreModule } from '@scaliolabs/baza-core-ng';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { BazaAuthDataAccessModule, BazaStatusDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaEntryLayoutComponent } from './layouts/entry-layout/components/baza-entry-layout.component';
import { BazaCmsFooterComponent } from './layouts/cms-layout/components/baza-cms-footer/baza-cms-footer.component';
import { BazaCmsAuthLayoutComponent } from './layouts/auth-layout/components/baza-cms-auth-layout/baza-cms-auth-layout.component';
import { BazaCmsNavBarComponent } from './layouts/cms-layout/components/baza-cms-nav-bar/baza-cms-nav-bar.component';
import { BazaCmsVerticalNavBarComponent } from './layouts/cms-layout/components/baza-cms-vertical-nav-bar/baza-cms-vertical-nav-bar.component';
import { BazaCmsLayoutComponent } from './layouts/cms-layout/components/baza-cms-layout/baza-cms-layout.component';
import { BazaLayoutLangComponent } from './shared/components/baza-layout-lang/baza-layout-lang.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { BazaCmsMessagesComponent } from './messages/baza-cms-messages.component';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzInputModule } from 'ng-zorro-antd/input';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaCmsCrudLayoutComponent } from './layouts/crud-layout/baza-cms-crud-layout.component';
import { BazaCmsLoadingIndicatorComponent } from './components/baza-cms-loading-indicator/baza-cms-loading-indicator.component';
import { BazaCmsAccountModule } from '../../../account/src';
import { BazaFormBuilderModule } from '../../../../libs/baza-form-builder/src';
import { BazaCrudCmsModule } from '../../../../libs/baza-crud/src';
import { BazaCmsStylesModule } from '../../../styles/src';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { BazaCmsLayoutResolve } from './layouts/cms-layout/components/baza-cms-layout/baza-cms-layout.resolve';
import { BazaCmsNotFoundModule } from '../../../not-found/src';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { BazaCmsBannerMessageComponent } from './layouts/cms-layout/components/baza-cms-banner-message/baza-cms-banner-message.component';
import { BazaCmsBannerMessageService } from './layouts/cms-layout/services/baza-cms-banner-message.service';

interface ForRootOptions {
    deps?: Array<unknown>;
    useFactory: (...deps) => BazaCmsLayoutConfig;
}

export { ForRootOptions as BazaCmsLayoutModuleForRootOptions };

const endpoints = [BazaAuthDataAccessModule];

@NgModule({
    imports: [
        ...endpoints,
        CommonModule,
        RouterModule,
        NzLayoutModule,
        NzBreadCrumbModule,
        NzIconModule,
        NzMenuModule,
        NzAlertModule,
        NzGridModule,
        TranslateModule,
        BazaNgCoreModule,
        NzButtonModule,
        NzDropDownModule,
        NzModalModule,
        BazaCmsAccountModule,
        BazaCmsStylesModule,
        BazaCmsNotFoundModule,
        NzMessageModule,
        NzTypographyModule,
        NzInputModule,
        ReactiveFormsModule,
        BazaCrudCmsModule,
        BazaFormBuilderModule,
        NzTagModule,
        BazaStatusDataAccessModule,
        BazaAuthNgModule,
        NzCollapseModule,
    ],
    declarations: [
        BazaCmsMessagesComponent,
        BazaEntryLayoutComponent,
        BazaCmsAuthLayoutComponent,
        BazaCmsLayoutComponent,
        BazaCmsBannerMessageComponent,
        BazaCmsFooterComponent,
        BazaCmsNavBarComponent,
        BazaCmsVerticalNavBarComponent,
        BazaLayoutLangComponent,
        BazaCmsCrudLayoutComponent,
        BazaCmsLoadingIndicatorComponent,
    ],
    exports: [
        BazaCmsMessagesComponent,
        BazaEntryLayoutComponent,
        BazaCmsAuthLayoutComponent,
        BazaCmsLayoutComponent,
        BazaCmsFooterComponent,
        BazaCmsNavBarComponent,
        BazaCmsVerticalNavBarComponent,
        BazaLayoutLangComponent,
        BazaCmsCrudLayoutComponent,
        BazaCmsLoadingIndicatorComponent,
    ],
    providers: [BazaCmsLayoutResolve, BazaCmsBannerMessageService],
})
export class BazaCmsLayoutModule {
    static forRoot(options: ForRootOptions): ModuleWithProviders<BazaCmsLayoutModule> {
        return {
            ngModule: BazaCmsLayoutModule,
            providers: [
                {
                    provide: BazaCmsLayoutConfig,
                    deps: options.deps,
                    useFactory: options.useFactory,
                },
            ],
        };
    }
}
