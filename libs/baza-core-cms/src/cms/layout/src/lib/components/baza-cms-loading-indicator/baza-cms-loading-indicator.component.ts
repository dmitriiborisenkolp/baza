import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { BazaLoadingService } from '@scaliolabs/baza-core-ng';
import { Observable, Subject } from 'rxjs';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { filter, map, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'baza-cms-loading-indicator',
    templateUrl: './baza-cms-loading-indicator.component.html',
    styleUrls: ['./baza-cms-loading-indicator.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsLoadingIndicatorComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private readonly router: Router,
        private readonly loading: BazaLoadingService,
    ) {}

    get isLoading$(): Observable<boolean> {
        return this.loading.isLoading$;
    }

    ngOnInit(): void {
        this.router.events.pipe(
            filter((event) => event instanceof NavigationStart),
            map(() => this.loading.addLoading()),
            takeUntil(this.ngOnDestroy$),
        ).subscribe(() => {
            this.loading.addLoading();
        });

        this.router.events.pipe(
            filter((event) => event instanceof NavigationEnd || event instanceof NavigationError),
            takeUntil(this.ngOnDestroy$),
        ).subscribe(() => {
            this.loading.completeAll();
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }
}
