export enum BazaCmsBannerMessageType {
    MasterPassword = 'masterPassword',
    CustomerIoIsNotReady = 'customerIoIsNotReady',
}

export interface BazaCmsBannerMessages {
    [BazaCmsBannerMessageType.MasterPassword]: BazaCmsBannerMessage;
    [BazaCmsBannerMessageType.CustomerIoIsNotReady]: BazaCmsBannerMessage;
}

export interface BazaCmsBannerMessage {
    message: {
        text: string;
        translate?: boolean;
        translateArgs?: any;
    };
    description?: {
        text: string;
        translate?: boolean;
        translateArgs?: any;
    };
    level: BazaCmsBannerMessageLevel;
    withIcon?: boolean;
    display: boolean;
}

export enum BazaCmsBannerMessageLevel {
    Success = 'success',
    Info = 'info',
    Warning = 'warning',
    Error = 'error',
}
