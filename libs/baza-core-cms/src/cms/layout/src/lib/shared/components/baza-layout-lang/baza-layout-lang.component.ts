import { ChangeDetectionStrategy, Component, Input, OnDestroy } from '@angular/core';
import { BazaI18nNgService } from '@scaliolabs/baza-core-ng';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProjectLanguage } from '@scaliolabs/baza-core-shared';

@Component({
    selector: 'baza-layout-lang',
    templateUrl: './baza-layout-lang.component.html',
    styleUrls: ['./baza-layout-lang.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaLayoutLangComponent implements OnDestroy {
    @Input() useCurrentLangAsTitle = false;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private readonly bazaI18n: BazaI18nNgService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get languages(): Array<ProjectLanguage> {
        return Object.values(ProjectLanguage);
    }

    get current$(): Observable<ProjectLanguage> {
        return this.bazaI18n.currentLanguage$;
    }

    get currentUppercase$(): Observable<string> {
        return this.current$.pipe(
            map((lang) => (lang as string).toUpperCase()),
        );
    }

    useLanguage(language: ProjectLanguage): void {
        this.bazaI18n.useLanguage(language);
    }

    i18n(key: string): string {
        return `baza.cms.layout.shared.components.bazaLayoutLang.${key}`;
    }

    i18nLanguage(language: ProjectLanguage): string {
        return `baza.core._languages.${language}`;
    }
}
