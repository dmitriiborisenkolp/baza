import { UrlCreationOptions } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class BazaCmsLayoutConfig {
    withI18nSupport: boolean;
    sidePanelAuthWidth: string;
    sidePanel: CmsSidePanelConfig;
    signOutRedirectUrlTree: {
        commands: any[];
        navigationExtras?: UrlCreationOptions;
    };
}

export interface CmsSidePanelConfig {
    baseWidth: string;
    groups: Array<CmsSidePanelMenuGroupConfig>;
}

export interface CmsSidePanelMenuGroupConfig {
    title: string;
    items: Array<CmsSidePanelMenuItemConfig>;
}

export interface CmsSidePanelMenuItemConfig<ACL = string> {
    title: string;
    translate?: boolean;
    routerLink: any;
    requiresACL?: Array<ACL>;
    icon: string;
    disabled?: boolean;
}
