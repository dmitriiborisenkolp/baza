import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { BazaCmsBannerMessage, BazaCmsBannerMessages } from '../../../../shared/model/baza-cms-banner-message.model';
import { BazaCmsBannerMessageService } from '../../services/baza-cms-banner-message.service';

@Component({
    selector: 'baza-cms-banner-message',
    templateUrl: './baza-cms-banner-message.component.html',
    styleUrls: ['./baza-cms-banner-message.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsBannerMessageComponent implements OnInit {
    messages$: Observable<BazaCmsBannerMessages>;

    constructor(private readonly translate: TranslateService, private readonly bazaCmsBannerMessageService: BazaCmsBannerMessageService) {}

    ngOnInit() {
        this.messages$ = this.bazaCmsBannerMessageService.messages$;
    }

    getMessages(bannerMessages: BazaCmsBannerMessages): Array<BazaCmsBannerMessage> {
        return Object.values(bannerMessages);
    }

    message$(input: BazaCmsBannerMessage): Observable<string> {
        if (input.message.translate) {
            return this.translate.get(input.message.text, input.message.translateArgs);
        } else {
            return of(input.message.text);
        }
    }

    description$(input: BazaCmsBannerMessage): Observable<string | undefined> {
        if (!input.description) {
            return undefined;
        }

        if (input.description.translate) {
            return this.translate.get(input.description.text, input.description.translateArgs);
        } else {
            return of(input.description.text);
        }
    }
}
