import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { StatusDto } from '@scaliolabs/baza-core-shared';
import { BazaStatusDataAccess } from '@scaliolabs/baza-core-data-access';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface BazaCmsLayoutResolveData {
    status: StatusDto;
}

@Injectable()
export class BazaCmsLayoutResolve implements Resolve<BazaCmsLayoutResolveData> {
    constructor(
        private readonly statusDataAccess: BazaStatusDataAccess,
    ) {}

    resolve(): Observable<BazaCmsLayoutResolveData> {
        const observables: [
            Observable<StatusDto>,
        ] = [
            this.statusDataAccess.index(),
        ];

        return combineLatest(observables).pipe(
            map(([status]) => ({
                status,
            })),
        );
    }
}
