import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, Output, ViewContainerRef } from '@angular/core';
import { BazaCmsLayoutConfig } from '../../../../baza-cms-layout.config';
import { JwtService } from '@scaliolabs/baza-core-ng';
import { BazaAuthDataAccess } from '@scaliolabs/baza-core-data-access';
import { combineLatest, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { BazaCms2faSettingsService, BazaCmsChangePasswordService } from '../../../../../../../account/src';
import { BazaCmsLayoutResolveData } from '../baza-cms-layout/baza-cms-layout.resolve';
import { NzPresetColor } from 'ng-zorro-antd/core/color';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';

@Component({
    selector: 'baza-cms-nav-bar',
    templateUrl: './baza-cms-nav-bar.component.html',
    styleUrls: ['./baza-cms-nav-bar.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsNavBarComponent implements OnDestroy {
    @Input() isSidebarCollapsed = false;

    @Output('toggleSideBar') toggleSideBarEvent: EventEmitter<void> = new EventEmitter<void>();

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private readonly moduleConfig: BazaCmsLayoutConfig,
        private readonly jwtService: JwtService,
        private readonly authEndpoint: BazaAuthDataAccess,
        private readonly router: Router,
        private readonly nzModal: NzModalService,
        private readonly translate: TranslateService,
        private readonly changePasswordModal: BazaCmsChangePasswordService,
        private readonly twoFactorAuthModal: BazaCms2faSettingsService,
        private readonly vcr: ViewContainerRef,
        private readonly activatedRoute: ActivatedRoute,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.cms.layout.layouts.cmsLayout.components.bazaCmsNavBar.${input}`;
    }

    get withI18nSupport(): boolean {
        return this.moduleConfig.withI18nSupport;
    }

    get isManagedUserAccount(): boolean {
        return this.jwtService.jwtPayload.isManagedUserAccount;
    }

    get resolvedData(): BazaCmsLayoutResolveData {
        return this.activatedRoute.snapshot.data[0];
    }

    get version(): string {
        return this.resolvedData.status.gitVersion === 'NOT_CONFIGURED'
            ? this.resolvedData.status.apiVersion
            : this.resolvedData.status.gitVersion;
    }

    get versionNzColor(): NzPresetColor {
        const mapNzColors: Array<{
            bazaEnv: BazaEnvironments;
            preset: NzPresetColor;
        }> = [
            { bazaEnv: BazaEnvironments.Production, preset: 'red' },
            { bazaEnv: BazaEnvironments.Preprod, preset: 'red' },
            { bazaEnv: BazaEnvironments.UAT, preset: 'red' },
            { bazaEnv: BazaEnvironments.Local, preset: 'geekblue' },
            { bazaEnv: BazaEnvironments.Test, preset: 'green' },
            { bazaEnv: BazaEnvironments.E2e, preset: 'cyan' },
            { bazaEnv: BazaEnvironments.Stage, preset: 'blue' },
        ];

        const found = mapNzColors.find((def) => def.bazaEnv === this.resolvedData.status.bazaEnvironment);

        return found ? found.preset : 'red';
    }

    toggleSideBar(): void {
        this.toggleSideBarEvent.next();
    }

    twoFactorAuthSettings(): void {
        this.twoFactorAuthModal.open(this.vcr);
    }

    changePassword(): void {
        this.changePasswordModal.open(this.vcr);
    }

    signOut(): void {
        this.nzModal.confirm({
            nzClassName: 'baza-cms-layout-sign-out-modal',
            nzTitle: this.translate.instant(this.i18n('signOutConfirm.nzTitle')),
            nzContent: this.translate.instant(this.i18n('signOutConfirm.nzContent')),
            nzOkText: this.translate.instant(this.i18n('signOutConfirm.nzOkText')),
            nzCancelText: this.translate.instant(this.i18n('signOutConfirm.nzCancelText')),
            nzOnOk: () =>
                combineLatest(
                    this.authEndpoint.invalidateToken({ jwt: this.jwtService.jwt.accessToken }),
                    this.authEndpoint.invalidateToken({ jwt: this.jwtService.jwt.refreshToken }),
                )
                    .pipe(takeUntil(this.ngOnDestroy$))
                    .pipe(
                        tap(() => {
                            this.jwtService.destroy();

                            this.router.navigate(
                                this.moduleConfig.signOutRedirectUrlTree.commands,
                                this.moduleConfig.signOutRedirectUrlTree.navigationExtras,
                            );
                        }),
                    )
                    .toPromise(),
        });
    }
}
