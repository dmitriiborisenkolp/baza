import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'baza-cms-footer',
    templateUrl: './baza-cms-footer.component.less',
    styleUrls: ['./baza-cms-footer.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsFooterComponent {}
