import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BazaCmsLayoutConfig, CmsSidePanelMenuGroupConfig, CmsSidePanelMenuItemConfig } from '../../../../baza-cms-layout.config';
import { BazaAclNgService } from '@scaliolabs/baza-core-ng';

@Component({
    selector: 'baza-cms-vertical-nav-bar',
    templateUrl: './baza-cms-vertical-nav-bar.component.html',
    styleUrls: ['./baza-cms-vertical-nav-bar.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsVerticalNavBarComponent implements OnInit, OnChanges {
    @Input() isSidebarCollapsed: boolean;

    menuGroups: CmsSidePanelMenuGroupConfig[] = [];
    sidebarGroupsCollapsing: { [key: string]: boolean } = {};

    constructor(private moduleConfig: BazaCmsLayoutConfig, private ngAcl: BazaAclNgService) {}

    ngOnInit(): void {
        const { groups } = this.moduleConfig.sidePanel;

        this.menuGroups = groups.reduce((acc, group) => {
            if (this.isGroupVisible(group)) {
                acc.push(group);
            }

            return acc;
        }, []);

        this.menuGroups.forEach(({ title }) => {
            this.sidebarGroupsCollapsing[`${title}`] = true;
        });
    }

    ngOnChanges({ isSidebarCollapsed }: SimpleChanges): void {
        if (isSidebarCollapsed.currentValue) {
            this.menuGroups.forEach(({ title }) => {
                this.sidebarGroupsCollapsing[`${title}`] = true;
            });
        }
    }

    isGroupVisible(group: CmsSidePanelMenuGroupConfig): boolean {
        if (!group.items.length) {
            return false;
        }

        return group.items.some((item) => this.isGroupItemVisible(item));
    }

    isGroupItemVisible(itemConfig: CmsSidePanelMenuItemConfig): boolean {
        return !itemConfig.disabled && itemConfig.requiresACL?.length && this.ngAcl.hasAccess(itemConfig.requiresACL);
    }

    setSidebarGroupCollapsing(groupName: string, isCollapsed: boolean): void {
        this.sidebarGroupsCollapsing[`${groupName}`] = isCollapsed;
    }
}
