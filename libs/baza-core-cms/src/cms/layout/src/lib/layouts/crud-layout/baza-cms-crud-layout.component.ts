import { AfterViewChecked, AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig } from '../../../../../../libs/baza-crud/src';

@Component({
    selector: 'baza-cms-crud-layout',
    templateUrl: './baza-cms-crud-layout.component.html',
    styleUrls: ['./baza-cms-crud-layout.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsCrudLayoutComponent<T = any> implements AfterViewInit, AfterViewChecked {
    @ViewChild(BazaCrudComponent) crud: BazaCrudComponent;

    @Input() config: BazaCrudConfig<T>;

    @Output() export: EventEmitter<BazaCrudComponent> = new EventEmitter();

    ngAfterViewInit(): void {
        this.export.emit(this.crud);
    }

    ngAfterViewChecked(): void {
        this.export.emit(this.crud);
    }
}
