import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'baza-entry-layout',
    templateUrl: './baza-entry-layout.component.html',
    styleUrls: ['./baza-entry-layout.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaEntryLayoutComponent {}
