import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef } from '@angular/core';
import { BazaCmsLayoutConfig } from '../../../../baza-cms-layout.config';
import { BazaFormLayoutModalService, BazaFormLayoutService } from '../../../../../../../../libs/baza-form-builder/src';


interface State {
    isSidebarCollapsed: boolean;
    siderWidth: string;
}

@Component({
    selector: 'baza-cms-layout',
    templateUrl: './baza-cms-layout.component.html',
    styleUrls: ['./baza-cms-layout.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsLayoutComponent implements AfterViewInit, OnDestroy {
    public state: State = {
        isSidebarCollapsed: false,
        siderWidth: this.moduleConfig.sidePanel.baseWidth,
    };

    public rightLayoutMargin = this.state.siderWidth;

    constructor(
        private readonly moduleConfig: BazaCmsLayoutConfig,
        private readonly vcr: ViewContainerRef,
        private readonly formLayout: BazaFormLayoutService,
        private readonly modalLayout: BazaFormLayoutModalService,
    ) {}

    ngAfterViewInit(): void {
        this.formLayout.vcr = this.vcr;
        this.modalLayout.vcr = this.vcr;
    }

    ngOnDestroy(): void {
        this.formLayout.vcr = undefined;
        this.modalLayout.vcr = undefined;
    }

    toggleSideBar(): void {
        this.state = {
            ...this.state,
            isSidebarCollapsed: ! this.state.isSidebarCollapsed,
        };

        this.rightLayoutMargin = this.state.isSidebarCollapsed
            ? `80px`
            : this.moduleConfig.sidePanel.baseWidth;
    }
}
