import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {
    BazaCmsBannerMessage,
    BazaCmsBannerMessageLevel,
    BazaCmsBannerMessages,
    BazaCmsBannerMessageType,
} from '../../../shared/model/baza-cms-banner-message.model';

const DefaultBazaCmsBannerMessages: BazaCmsBannerMessages = {
    [BazaCmsBannerMessageType.MasterPassword]: {
        message: { text: 'Warning' },
        description: { text: 'Master Password is enabled' },
        level: BazaCmsBannerMessageLevel.Warning,
        withIcon: true,
        display: false,
    },
    [BazaCmsBannerMessageType.CustomerIoIsNotReady]: {
        message: { text: 'Warning' },
        description: {
            text: 'There are missing configuration for Customer.IO Templates. Please go to Customer.IO CMS section and Link Mail Templates with Customer.IO',
        },
        level: BazaCmsBannerMessageLevel.Warning,
        withIcon: true,
        display: false,
    },
};

/**
 * TODO: tbh should be reworked completely. This solution is not scalable.
 */
@Injectable({ providedIn: 'root' })
export class BazaCmsBannerMessageService {
    bannerMessages = Object.assign({}, DefaultBazaCmsBannerMessages);

    private readonly messages: BehaviorSubject<BazaCmsBannerMessages> = new BehaviorSubject<BazaCmsBannerMessages>(this.bannerMessages);

    messages$ = this.messages.asObservable();

    setMessage(key: keyof BazaCmsBannerMessages, message: BazaCmsBannerMessage) {
        if (this.bannerMessages[`${key}`]) {
            this.bannerMessages[`${key}`] = message;
        }
    }

    displayMessage(key: keyof BazaCmsBannerMessages) {
        if (this.bannerMessages[`${key}`]) {
            this.bannerMessages[`${key}`].display = true;
            this.messages.next(this.bannerMessages);
        }
    }

    hideMessage(key: keyof BazaCmsBannerMessages) {
        if (this.bannerMessages[`${key}`]) {
            this.bannerMessages[`${key}`].display = false;
            this.messages.next(this.bannerMessages);
        }
    }

    clearMessagesByLevel(level: BazaCmsBannerMessageLevel) {
        for (const message of Object.values(this.bannerMessages) as Array<BazaCmsBannerMessage>) {
            level === message?.level && (message.display = false);
        }
        this.messages.next(this.bannerMessages);
    }

    clearAll() {
        for (const message of Object.values(this.bannerMessages) as Array<BazaCmsBannerMessage>) {
            message.display = false;
        }

        this.messages.next(this.bannerMessages);
    }
}
