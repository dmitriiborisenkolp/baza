import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BazaNgCurrentApplicationService } from '@scaliolabs/baza-core-ng';
import { BazaCmsLayoutConfig } from '../../../../baza-cms-layout.config';

@Component({
    selector: 'baza-cms-auth-layout',
    templateUrl: './baza-cms-auth-layout.component.html',
    styleUrls: ['./baza-cms-auth-layout.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCmsAuthLayoutComponent {
    constructor(private readonly moduleConfig: BazaCmsLayoutConfig, private readonly currentApplication: BazaNgCurrentApplicationService) {}

    i18n(key: string): string {
        return `baza.cms.layout.layouts.authLayout.components.bazaCmsAuthLayout.${key}`;
    }

    get project(): string {
        return this.currentApplication.project.name;
    }

    get withI18nSupport(): boolean {
        return this.moduleConfig.withI18nSupport;
    }

    get sidePanelWidth(): string {
        return this.moduleConfig.sidePanelAuthWidth;
    }
}
