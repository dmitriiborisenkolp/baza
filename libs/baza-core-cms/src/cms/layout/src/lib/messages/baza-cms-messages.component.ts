import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { BazaNgMessageLevel, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'baza-cms-messages',
    template: '',
})
export class BazaCmsMessagesComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private readonly nzMessages: NzMessageService,
        private readonly bazaNgMessages: BazaNgMessagesService,
        private readonly translate: TranslateService,
    ) {}

    ngOnInit(): void {
        this.bazaNgMessages.messages$.pipe(
            takeUntil(this.ngOnDestroy$),
        ).subscribe((message) => {
            const messageText = message.request.translate
                ? this.translate.instant(message.request.message, message.request.translateParams)
                : message.request.message;

            switch (message.level) {
                case BazaNgMessageLevel.Error: {
                    return this.nzMessages.error(messageText);
                }

                case BazaNgMessageLevel.Verbose:
                case BazaNgMessageLevel.Info: {
                    return this.nzMessages.info(messageText);
                }

                case BazaNgMessageLevel.Warning: {
                    return this.nzMessages.warning(messageText);
                }

                case BazaNgMessageLevel.Success: {
                    return this.nzMessages.success(messageText);
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }
}
