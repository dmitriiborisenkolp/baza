import { Routes } from '@angular/router';
import { BazaWhitelistAccountCmsComponent } from './components/baza-whitelist-account-cms/baza-whitelist-account-cms.component';

export const bazaWhitelistAccountCmsRoutes: Routes = [
    {
        path: 'whitelisted-accounts',
        component: BazaWhitelistAccountCmsComponent,
    },
];
