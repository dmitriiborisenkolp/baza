import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaWhitelistAccountCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaCmsLayoutModule } from '../../../../cms/layout/src';
import { BazaFormBuilderModule } from '../../../baza-form-builder/src';
import { BazaCrudCmsModule } from '../../../baza-crud/src';
import { BazaWhitelistAccountCmsComponent } from './components/baza-whitelist-account-cms/baza-whitelist-account-cms.component';

@NgModule({
    imports: [CommonModule, BazaWhitelistAccountCmsDataAccessModule, BazaCrudCmsModule, BazaFormBuilderModule, BazaCmsLayoutModule],
    declarations: [BazaWhitelistAccountCmsComponent],
})
export class BazaWhitelistAccountCmsModule {}
