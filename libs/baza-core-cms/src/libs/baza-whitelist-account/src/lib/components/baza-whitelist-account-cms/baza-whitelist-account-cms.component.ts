import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudItem, BazaTableFieldType } from '../../../../../baza-crud/src';
import {
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutModalService,
} from '../../../../../baza-form-builder/src';
import { BazaNgConfirmService, BazaNzButtonStyle, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { TranslateService } from '@ngx-translate/core';
import { Subject, throwError } from 'rxjs';
import { catchError, takeUntil, tap } from 'rxjs/operators';
import { WhitelistAccountDto } from '@scaliolabs/baza-core-shared';
import { BazaWhitelistAccountCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export const BAZA_CORE_WHITELIST_CMS_ID = 'baza-core-whitelist-cms-id';
export const BAZA_CORE_WHITELIST_FORM_ID = 'baza-core-whitelist-form-id';

interface State {
    config: BazaCrudConfig<WhitelistAccountDto>;
}

interface FormValue {
    id?: string;
    email: string;
}

@Component({
    templateUrl: './baza-whitelist-account-cms.component.html',
    styleUrls: ['./baza-whitelist-account-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaWhitelistAccountCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<WhitelistAccountDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly dataAccess: BazaWhitelistAccountCmsDataAccess,
        private readonly bazaConfirm: BazaNgConfirmService,
        private readonly translate: TranslateService,
        private readonly messages: BazaNgMessagesService,
        private readonly formLayout: BazaFormLayoutModalService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza.whitelistAccount.components.bazaCoreWhitelistAccountCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<WhitelistAccountDto> {
        const config: BazaCrudConfig<WhitelistAccountDto> = {
            id: BAZA_CORE_WHITELIST_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.ImportCsv,
                        options: {
                            title: this.i18n('actions.importCsv.title'),
                            hint: this.i18n('actions.importCsv.hint'),
                            icon: 'import',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (file) =>
                                this.dataAccess.importCsv(file).pipe(
                                    catchError((err) => {
                                        this.messages.bazaError(err);

                                        return throwError(err);
                                    }),
                                ),
                            callback: () => {
                                this.crud.refresh();
                                this.messages.success({
                                    translate: true,
                                    message: this.i18n('actions.importCsv.success'),
                                });
                            },
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            icon: 'plus',
                            type: BazaNzButtonStyle.Default,
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<WhitelistAccountDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (_, formValue) => this.dataAccess.add(formValue).pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request),
                },
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.id'),
                        width: 70,
                    },
                    {
                        field: 'email',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.email'),
                        width: 280,
                    },
                    {
                        field: 'dateCreated',
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        sortable: true,
                        title: this.i18n('fields.dateCreated'),
                        width: 280,
                    },
                ],
                actions: [
                    {
                        icon: 'delete',
                        title: this.i18n('actions.remove.title'),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.bazaConfirm.open({
                                message: this.translate.instant(this.i18n('actions.remove.confirm'), {
                                    email: entity.email,
                                }),
                                withoutMessageTranslate: true,
                                confirm: () =>
                                    this.dataAccess
                                        .remove({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() => this.crud.refresh()),
                                            takeUntil(this.ngOnDestroy$),
                                        )
                                        .toPromise(),
                            });
                        },
                    },
                ],
            },
        };

        return config;
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            email: [undefined, [Validators.required, Validators.email]],
        });
    }

    private formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_CORE_WHITELIST_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'email',
                        label: this.i18n('fields.email'),
                        required: true,
                    },
                ],
            },
        };
    }
}
