import { Injectable } from '@angular/core';
import { BazaCrudConfig } from '../../baza-crud-cms.model';
import { BazaCrudDataComponent } from '../baza-crud-data/baza-crud-data.component';
import { BazaCrudTreeComponent } from '../baza-crud-tree/baza-crud-tree.component';
import { BazaCrudInjectService } from './baza-crud-inject.service';

@Injectable({
    // By default BazaCrudService is injected in root space
    // You can provide on component level also if you have several CRUD's on same page
    providedIn: 'root',
})
export class BazaCrudService<T = any> {
    private _config: BazaCrudConfig<T>;
    private _dataComponent: BazaCrudDataComponent | BazaCrudTreeComponent;

    constructor(private readonly crudInjectionService: BazaCrudInjectService) {}

    get lastRequest() {
        return (this._dataComponent as BazaCrudDataComponent).lastRequest;
    }

    init(config: BazaCrudConfig<T>, dataComponent?: BazaCrudDataComponent | BazaCrudTreeComponent): void {
        this._config = config;
        this._dataComponent = dataComponent;
    }

    destroy(): void {
        this._config = undefined;
        this._dataComponent = undefined;
    }

    updateConfig(config: BazaCrudConfig<T>): void {
        if (config.id && this.crudInjectionService.has(config.id)) {
            const injectConfig = this.crudInjectionService.get(config.id);

            if (injectConfig.inject) {
                injectConfig.inject(config);
            }
        }

        this._config = config;
    }

    load(): void {
        if (this._dataComponent) {
            this._dataComponent.load();
        }
    }

    dateRange(dateFrom: Date, dateTo: Date): void {
        if (!this._dataComponent) {
            return;
        }

        this._dataComponent.load({
            dateTo: dateTo?.toISOString(),
            dateFrom: dateFrom?.toISOString(),
        });
    }

    cancelDateRange(): void {
        if (!this._dataComponent) {
            return;
        }

        this._dataComponent.load({
            dateTo: null,
            dateFrom: null,
        });
    }

    search(queryString: string): void {
        if (!this._dataComponent) {
            return;
        }

        this._dataComponent.load({
            index: 1,
            queryString,
        });
    }
}
