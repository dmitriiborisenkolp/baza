import { BazaTableFieldType } from './_.field-type';
import { Observable } from 'rxjs';
import { BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';

export interface BazaTableFieldTypeButton<T = any> {
    kind: BazaTableFieldType.Button;
    disabled$?: Observable<boolean>;
    onClick: (entity: T) => any;
    translate?: boolean;
    translateArgs?: (entity: T) => any;
    format?: (entity: T) => any | Observable<any>;
    icon?: string;
    isDanger?: boolean;
    buttonType: BazaNzButtonStyle;
}
