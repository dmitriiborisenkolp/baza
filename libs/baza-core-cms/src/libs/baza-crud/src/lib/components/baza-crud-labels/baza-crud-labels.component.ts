import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BazaCrudLabel } from '../../baza-crud-cms.model';
import { Observable, of } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { NzButtonType } from 'ng-zorro-antd/button/button.component';
import { map } from 'rxjs/operators';

@Component({
    selector: 'baza-crud-labels',
    templateUrl: './baza-crud-labels.component.html',
    styleUrls: ['./baza-crud-labels.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCrudLabelsComponent {
    @Input() labels: Array<BazaCrudLabel>;

    constructor(
        private readonly translate: TranslateService,
    ) {}

    title$(input: BazaCrudLabel): Observable<string> {
        if (input.translate) {
            return this.translate.get(
                input.title,
                input.translateArgs,
            );
        } else {
            return of(input.title);
        }
    }

    isActive$(input: BazaCrudLabel): Observable<boolean> {
        return input.isActive$
            ? input.isActive$
            : of(false);
    }

    nzType$(input: BazaCrudLabel): Observable<NzButtonType> {
        return this.isActive$(input).pipe(
            map((isActive) => isActive ? 'primary' : 'default')
        );
    }
}
