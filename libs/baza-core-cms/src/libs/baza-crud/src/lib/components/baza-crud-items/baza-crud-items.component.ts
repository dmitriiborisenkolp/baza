import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewContainerRef,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, finalize, takeUntil } from 'rxjs/operators';
import { BazaAclNgService } from '@scaliolabs/baza-core-ng';
import { NzTabPosition } from 'ng-zorro-antd/tabs';
import { BazaCrudService } from '../baza-crud/baza-crud.service';
import {
    AbstractBazaCrudItem,
    BazaCrudILegendItem,
    BazaCrudItem,
    BazaCrudItemButton,
    BazaCrudItems,
    BazaCrudItemsButton,
    BazaCrudItemsCheckbox,
    BazaCrudItemsContext,
    BazaCrudItemSearch,
    BazaCrudItemsExportToCsv,
    BazaCrudItemsSearch,
    BazaCrudItemsSearchField,
    BazaCrudItemsTabs,
    BazaCrudItemsLegend,
    BazaCrudItemsDateRange,
    BazaCrudTab,
} from './baza-crud-items.model';
import { BazaLoadingService } from '@scaliolabs/baza-core-ng';
import { BazaCrudItemsImportCsv } from '../baza-crud-items/baza-crud-items.model';
import { BazaCrudExportToCsvService } from '../../services/baza-crud-export-to-csv.service';

@Component({
    selector: 'baza-crud-items',
    templateUrl: './baza-crud-items.component.html',
    styleUrls: ['./baza-crud-items.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCrudItemsComponent implements OnChanges, OnInit, OnDestroy {
    @Input() items: Array<AbstractBazaCrudItem>;
    @Input() context: BazaCrudItemsContext = BazaCrudItemsContext.TopLeft;

    @Output('search') searchEvent: EventEmitter<string | undefined> = new EventEmitter<string | undefined>();

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public searchControl: FormControl = new FormControl();
    public dateRangeControl: FormControl = new FormControl();
    public csvExportLoading: BazaLoadingService;

    constructor(
        private readonly vcr: ViewContainerRef,
        private readonly acl: BazaAclNgService,
        private readonly loading: BazaLoadingService,
        private readonly service: BazaCrudService,
        private readonly exportToCsvService: BazaCrudExportToCsvService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['items'] && this.items) {
            for (const item of this.items) {
                if (!item.id) {
                    item.id = generateRandomHexString(8);
                }

                if (item.type === BazaCrudItem.Tabs) {
                    for (const tab of (item as BazaCrudItemsTabs).options.tabs) {
                        if (!tab.id) {
                            tab.id = generateRandomHexString(8);
                        }
                    }
                }

                if (item.type === BazaCrudItem.Search && !(item as BazaCrudItemsSearch).options.control) {
                    (item as BazaCrudItemsSearch).options.control = this.searchControl;
                }
            }
        }
    }

    ngOnInit(): void {
        this.csvExportLoading = this.loading.newInstance();

        this.searchControl.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            this.service.search(next);

            this.searchEvent.emit(next);
        });

        this.dateRangeControl.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            if (Array.isArray(next) && next.length === 2) {
                this.service.dateRange(next[0], next[1]);
            } else {
                this.service.cancelDateRange();
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.crud.components.bazaCrudItems.${input}`;
    }

    trackBySID(_: number, item: AbstractBazaCrudItem | BazaCrudTab): string {
        return item.id;
    }

    visible$(item: AbstractBazaCrudItem): Observable<boolean> {
        if (Array.isArray(item.acl) && item.acl.length > 0 && !this.acl.hasAccess(item.acl)) {
            return of(false);
        }

        return item.visible ? item.visible : of(true);
    }

    disabled$(options: BazaCrudItemButton): Observable<boolean> {
        return options.disabled ? options.disabled : of(false);
    }

    searchClear(options: BazaCrudItemSearch): void {
        options.control.setValue(undefined);

        if (options.onClear) {
            options.onClear();
        }
    }

    searchSubmit(options: BazaCrudItemSearch): void {
        if (options.onSearch) {
            options.onSearch(options.control.value);
        }
    }

    get nzTabPosition(): NzTabPosition {
        return [BazaCrudItemsContext.BottomLeft, BazaCrudItemsContext.BottomRight].includes(this.context) ? 'top' : 'bottom';
    }

    exportToCsv(crudItem: BazaCrudItemsExportToCsv<any>): void {
        this.exportToCsvService.exportToCSV(
            (request) => crudItem.options.endpoint(request),
            {
                lastRequest: this.service.lastRequest,
                fileName: crudItem.options.fileNameGenerator(crudItem.id),
                takeUntil$: this.ngOnDestroy$,
            },
            this.vcr,
        );
    }

    async onFileInputChange($event, crudItem: BazaCrudItemsImportCsv<any>): Promise<void> {
        const loading = this.loading.addLoading();
        const target = $event.target as HTMLInputElement;

        crudItem.options
            .endpoint(target.files[0])
            .pipe(
                takeUntil(this.ngOnDestroy$),
                finalize(() => loading.complete()),
            )
            .subscribe({
                next: (response) => crudItem.options.callback(response),
            });
    }

    legendItemBoxNgStyle(item: BazaCrudILegendItem): any {
        return {
            'background-color': item.color,
        };
    }

    isBazaCrudItemButton = (item: AbstractBazaCrudItem): item is BazaCrudItemsButton => item.type === BazaCrudItem.Button;

    isBazaCrudItemSearch = (item: AbstractBazaCrudItem): item is BazaCrudItemsSearch => item.type === BazaCrudItem.Search;

    isBazaCrudItemSearchField = (item: AbstractBazaCrudItem): item is BazaCrudItemsSearchField => item.type === BazaCrudItem.SearchField;

    isBazaCrudItemTabs = (item: AbstractBazaCrudItem): item is BazaCrudItemsTabs => item.type === BazaCrudItem.Tabs;

    isBazaCrudItemCheckbox = (item: AbstractBazaCrudItem): item is BazaCrudItemsCheckbox => item.type === BazaCrudItem.Checkbox;

    isBazaCrudItemExportToCsv = (item: AbstractBazaCrudItem): item is BazaCrudItemsExportToCsv<unknown> =>
        item.type === BazaCrudItem.ExportToCsv;

    isBazaCrudItemImportToCsv = (item: AbstractBazaCrudItem): item is BazaCrudItemsImportCsv<unknown> =>
        item.type === BazaCrudItem.ImportCsv;

    isBazaCrudItemLegend = (item: AbstractBazaCrudItem): item is BazaCrudItemsLegend => item.type === BazaCrudItem.Legend;

    isBazaCrudItemDateRange = (item: AbstractBazaCrudItem): item is BazaCrudItemsDateRange => item.type === BazaCrudItem.DateRange;
}
