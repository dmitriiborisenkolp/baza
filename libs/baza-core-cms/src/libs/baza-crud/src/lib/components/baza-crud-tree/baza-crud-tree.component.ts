import { FlatTreeControl } from '@angular/cdk/tree';
import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Inject,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
} from '@angular/core';
import { BazaAclNgService, BazaLoadingService, BazaNgMessagesService, genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaError, CrudListRequestDto, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NzTreeNodeOptions } from 'ng-zorro-antd/tree';
import { NzTreeFlatDataSource, NzTreeFlattener } from 'ng-zorro-antd/tree-view';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { debounceTime, finalize, retryWhen, takeUntil, tap } from 'rxjs/operators';
import { BazaCrudCmsConfig } from '../../baza-crud-cms.config';
import { BazaCrudQueryString } from '../baza-crud-data/baza-crud-data.model';
import { BazaCrudTreeDataConfig, BazaCrudTreeNode } from './baza-crud-tree.model';

const DEBOUNCE_LOAD_REQUEST_MS = 100;

interface LoadRequest {
    index?: number;
    queryString?: string;
}

interface State<T = any> {
    index: number;
    size: number;
    sizes: Array<number>;
    total: number;
    queryString?: BazaCrudQueryString;
    withPagination: boolean;

    lastRequest?: CrudListRequestDto;
    items?: Observable<NzTreeNodeOptions[]>;
}

interface FlatNode {
    expandable: boolean;
    item: NzTreeNodeOptions;
    level: number;
}

@Component({
    selector: 'baza-crud-tree',
    templateUrl: './baza-crud-tree.component.html',
    styleUrls: ['./baza-crud-tree.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [BazaLoadingService],
})
export class BazaCrudTreeComponent<T = any> implements OnChanges, OnInit, OnDestroy, AfterViewInit {
    @Input() config: BazaCrudTreeDataConfig<T>;

    public showLine = false;
    public expandAll = true;
    public selected: number[] | undefined;
    public readonly: boolean;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();
    private readonly nextLoad$: Subject<void> = new Subject<void>();

    private readonly nextLoadRequest$: Subject<LoadRequest> = new Subject<LoadRequest>();

    public current$: BehaviorSubject<Array<T>> = new BehaviorSubject<Array<T>>([]);

    public state: State<T> = {
        index: 1,
        size: this.moduleConfig.defaults.size,
        sizes: this.moduleConfig.defaults.sizes,
        total: 0,
        withPagination: true,
        items: of([]),
    };

    public treeControl = new FlatTreeControl<FlatNode>(
        (node) => node.level,
        (node) => node.expandable,
    );

    private treeFlattener = new NzTreeFlattener(
        this.transformer,
        (node) => node.level,
        (node) => node.expandable,
        (node) => node.children,
    );

    public dataSource = new NzTreeFlatDataSource(this.treeControl as any, this.treeFlattener);

    constructor(
        @Inject(BazaCrudCmsConfig) private readonly moduleConfig: BazaCrudCmsConfig,
        private readonly cdr: ChangeDetectorRef,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly loading: BazaLoadingService,
        private readonly acl: BazaAclNgService,
    ) {}

    private transformer(node: NzTreeNodeOptions, level: number) {
        return {
            expandable: !!node.children && node.children.length > 0,
            item: node,
            level: level,
        };
    }

    hasChild = (_: number, node: FlatNode) => node.expandable;

    ngAfterViewInit(): void {
        this.treeControl.expandAll();
    }

    ngOnChanges() {
        this.state = {
            ...this.state,
            total: 0,
            size: this.config.dataSource && this.config.dataSource.size ? this.config.dataSource.size : this.moduleConfig.defaults.size,
            sizes: this.config.dataSource && this.config.dataSource.sizes ? this.config.dataSource.sizes : this.moduleConfig.defaults.sizes,
        };
        this.cdr.markForCheck();

        this.load();
    }

    ngOnInit() {
        this.nextLoadRequest$
            .pipe(debounceTime(DEBOUNCE_LOAD_REQUEST_MS), takeUntil(this.ngOnDestroy$))
            .subscribe(() => this.performLoad());

        this.load();
    }

    ngOnDestroy() {
        this.nextLoad$.next();
        this.ngOnDestroy$.next();
    }

    performLoad(): void {
        this.nextLoad$.next();

        const loading = this.loading.addLoading();

        if (this.config.dataSource && this.config.dataSource.array) {
            this.config.dataSource
                .array({})
                .pipe(
                    retryWhen(genericRetryStrategy()),
                    tap(() => loading.complete()),
                    finalize(() => loading.complete()),
                    takeUntil(this.nextLoad$),
                )
                .subscribe(
                    (response) => {
                        this.current$.next(response.items);
                        this.dataSource.setData(this.filterItems(this.current$.value as unknown as BazaCrudTreeNode[]));
                        this.state = {
                            ...this.state,
                            index: 1,
                            size: response.items.length,
                            total: response.items.length,
                            withPagination: false,
                        };

                        this.cdr.markForCheck();
                    },
                    (err: BazaError) => {
                        isBazaErrorResponse(err) ? this.ngMessages.bazaError(err) : this.ngMessages.httpError();

                        this.cdr.markForCheck();
                    },
                );
        }
    }

    load(request: LoadRequest = {}): void {
        this.nextLoadRequest$.next(request);
    }

    filterItems(items: BazaCrudTreeNode[]): NzTreeNodeOptions[] {
        const nodeArray = [];
        const map = {};
        let node;

        items.forEach((el, i) => {
            map[el.id] = i;
            el['children'] = [];
        });

        items.forEach((el) => {
            node = el;
            if (node.parentId !== null) {
                items[map[node.parentId]]['children'].push(node);
            } else {
                nodeArray.push(node);
            }
        });

        return nodeArray as unknown as NzTreeNodeOptions[];
    }
}
