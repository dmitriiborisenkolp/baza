import { BazaTableFieldType } from './_.field-type';

export interface BazaTableFieldTypeUlid {
    kind: BazaTableFieldType.Ulid;
}
