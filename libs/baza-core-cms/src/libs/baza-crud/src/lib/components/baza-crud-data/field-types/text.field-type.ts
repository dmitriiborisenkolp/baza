import { BazaTableFieldType } from './_.field-type';
import { Observable } from 'rxjs';

export interface BazaTableFieldTypeText<T = any> {
    kind: BazaTableFieldType.Text;
    translate?: boolean;
    translateArgs?: (entity: T) => any;
    format?: (entity: T) => any | Observable<any>;
    hint?: (entity: T) => BazaTableFieldTypeHint | undefined;
}

export interface BazaTableFieldTypeHint {
    icon: string;
    text: string;
    translate?: boolean;
    translateArgs?: any;
}
