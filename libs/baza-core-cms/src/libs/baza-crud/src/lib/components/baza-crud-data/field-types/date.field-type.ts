import { BazaTableFieldType } from './_.field-type';

export interface BazaTableFieldTypeDate {
    kind: BazaTableFieldType.Date
}
