import { BazaTableFieldType } from './_.field-type';

export interface BazaTableFieldTypeDateUTC {
    kind: BazaTableFieldType.DateUTC;
}
