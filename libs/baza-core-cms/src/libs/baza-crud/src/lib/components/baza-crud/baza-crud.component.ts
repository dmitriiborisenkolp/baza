import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    Inject,
    Input,
    OnChanges,
    OnDestroy,
    PLATFORM_ID,
    SimpleChanges,
    ViewChild,
} from '@angular/core';
import { BazaCrudConfig } from '../../baza-crud-cms.model';
import { BazaCrudDataComponent } from '../baza-crud-data/baza-crud-data.component';
import { BazaCrudService } from './baza-crud.service';
import { isPlatformBrowser } from '@angular/common';

interface State {
    queryString?: string | undefined;
}

@Component({
    selector: 'baza-crud',
    templateUrl: './baza-crud.component.html',
    styleUrls: ['./baza-crud.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [BazaCrudService],
})
export class BazaCrudComponent<T = any> implements AfterViewInit, OnChanges, OnDestroy {
    @ViewChild(BazaCrudDataComponent) public data: BazaCrudDataComponent;

    @Input() config: BazaCrudConfig<T>;
    @Input() withCustomDataView = false;

    public state: State = {};

    constructor(
        // eslint-disable-next-line @typescript-eslint/ban-types
        @Inject(PLATFORM_ID) private platformId: Object,
        private readonly service: BazaCrudService,
    ) {}

    refresh(): void {
        if (this.data) {
            this.data.load();
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['config']) {
            this.service.updateConfig(this.config);

            if (isPlatformBrowser(this.platformId) && window) {
                window['bazaCrudId'] = () => this.config.id;
                window['bazaCrud'] = () => this;
            }
        }
    }

    ngAfterViewInit(): void {
        this.service.init(this.config, this.data);
    }

    ngOnDestroy(): void {
        this.service.destroy();

        if (isPlatformBrowser(this.platformId) && window) {
            window['bazaCrudId'] = () => undefined;
            window['bazaCrud'] = () => undefined;
        }
    }
}
