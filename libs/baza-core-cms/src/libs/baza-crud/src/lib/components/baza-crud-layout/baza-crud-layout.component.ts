import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BazaCrudLayoutConfig, BazaCrudLayoutTitleLevel } from '../../baza-crud-cms.model';
import { BazaCrudItemsContext } from '../baza-crud-items/baza-crud-items.model';

interface State {
    queryString?: string | undefined;
}

@Component({
    selector: 'baza-crud-layout',
    templateUrl: './baza-crud-layout.component.html',
    styleUrls: ['./baza-crud-layout.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCrudLayoutComponent {
    @Input() config: BazaCrudLayoutConfig;

    public state: State = {};

    public ITEMS_CONTEXT = BazaCrudItemsContext;

    get hasHeader(): boolean {
        return !!this.config && !!(this.config.title || (this.config.items && (this.config.items.topLeft || this.config.items.topRight)));
    }

    get headerTitleLevel(): BazaCrudLayoutTitleLevel {
        return this.config.titleLevel || 1;
    }

    get hasNavigation(): boolean {
        return !!this.config.navigation && this.config.navigation.nodes.length > 0;
    }

    get hasMessages(): boolean {
        return Array.isArray(this.config.messages) && this.config.messages.length > 0;
    }

    get hasLabels(): boolean {
        return Array.isArray(this.config.labels) && this.config.labels.length > 0;
    }

    get hasFooter(): boolean {
        return !!this.config && !!(this.config.items.bottomLeft || this.config.items.bottomRight);
    }

    onSearch(queryString: string | undefined): void {
        this.state = {
            ...this.state,
            queryString,
        };
    }
}
