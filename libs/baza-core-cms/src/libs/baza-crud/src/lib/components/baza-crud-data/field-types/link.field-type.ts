import { BazaTableFieldType } from './_.field-type';
import { Observable } from 'rxjs';

export interface BazaTableFieldTypeLink<T = any> {
    kind: BazaTableFieldType.Link;
    translate?: boolean;
    translateArgs?: (entity: T) => any;
    format?: (entity: T) => any | Observable<any>;
    icon?: string;
    link: (entity: T) => { routerLink: any; queryParams?: any; state?: any };
}
