import { BazaTableFieldType } from './_.field-type';

export interface BazaTableFieldTypeId {
    kind: BazaTableFieldType.Id;
}
