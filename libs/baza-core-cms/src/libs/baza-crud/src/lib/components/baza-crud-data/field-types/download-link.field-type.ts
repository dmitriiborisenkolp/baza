import { BazaTableFieldType } from './_.field-type';
import { Observable } from 'rxjs';

export interface BazaTableFieldTypeDownloadLink<T = any> {
    kind: BazaTableFieldType.DownloadLink,
    translate?: boolean;
    translateArgs?: (entity: T) => any;
    format?: (entity: T) => any | Observable<any>;
    icon?: string;
    link: (entity: T) => string;
}
