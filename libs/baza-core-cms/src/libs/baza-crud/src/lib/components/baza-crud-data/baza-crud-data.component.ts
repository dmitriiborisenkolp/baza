import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, of, Subject } from 'rxjs';
import { BazaError, generateRandomHexString, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { debounceTime, finalize, map, retryWhen, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BazaLoadingService, BazaNgMessagesService, genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaCrudCmsConfig } from '../../baza-crud-cms.config';
import * as moment from 'moment';
import { BazaAclNgService } from '@scaliolabs/baza-core-ng';
import { TranslateService } from '@ngx-translate/core';
import {
    BazaCrudAction,
    BazaCrudDataColumn,
    BazaCrudDataConfig,
    BazaCrudQueryString,
    BazaTableFieldTypeButton,
    BazaTableFieldTypeHint,
} from './baza-crud-data.model';
import { BazaTableFieldType } from './field-types/_.field-type';
import { BazaTableFieldTypeText } from './field-types/text.field-type';
import { BazaTableFieldTypeLink } from './field-types/link.field-type';
import { NzTableSortOrder } from 'ng-zorro-antd/table/src/table.types';
import { CrudListRequestDto, CrudListSort, CrudListSortOrder } from '@scaliolabs/baza-core-shared';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

const DEFAULT_COLUMN_WIDTH_ID = 80;
const DEFAULT_COLUMN_WIDTH_ULID = 200;
const DEFAULT_COLUMN_WIDTH_DATE = 190;

const DEBOUNCE_LOAD_REQUEST_MS = 100;

interface LoadRequest {
    index?: number;
    queryString?: string;
    dateFrom?: string;
    dateTo?: string;
}

interface State<T = any> {
    index: number;
    size: number;
    sizes: Array<number>;
    total: number;
    queryString?: BazaCrudQueryString;
    dateFrom?: string;
    dateTo?: string;
    withPagination: boolean;
    nzShowSizeChanger: boolean;
    nzShowTotal: boolean;
    sort: Array<{
        column: BazaCrudDataColumn<T>;
        order: NzTableSortOrder;
    }>;
    lastRequest?: CrudListRequestDto;
}

@Component({
    selector: 'baza-crud-data',
    templateUrl: './baza-crud-data.component.html',
    styleUrls: ['./baza-crud-data.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [BazaLoadingService],
})
export class BazaCrudDataComponent<T = any> implements OnChanges, OnInit, OnDestroy {
    @Input() config: BazaCrudDataConfig<T>;
    @Input() index: number;
    @Input() size: number;
    @Input() queryString: string;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();
    private readonly nextLoad$: Subject<void> = new Subject<void>();

    private readonly nextLoadRequest$: Subject<LoadRequest> = new Subject<LoadRequest>();

    public current$: BehaviorSubject<Array<T>> = new BehaviorSubject<Array<T>>([]);

    public state: State<T> = {
        index: 1,
        size: this.moduleConfig.defaults.size,
        sizes: this.moduleConfig.defaults.sizes,
        total: 0,
        withPagination: true,
        nzShowSizeChanger: true,
        nzShowTotal: true,
        sort: [],
    };

    constructor(
        private readonly domSanitizer: DomSanitizer,
        private readonly moduleConfig: BazaCrudCmsConfig,
        private readonly cdr: ChangeDetectorRef,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly loading: BazaLoadingService,
        private readonly acl: BazaAclNgService,
        private readonly translate: TranslateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['config'] && this.config) {
            for (const column of this.config.columns) {
                if (column && !column.id) {
                    column.id = generateRandomHexString(8);
                }
            }

            this.state = {
                ...this.state,
                total: 0,
                size: this.config.dataSource && this.config.dataSource.size ? this.config.dataSource.size : this.moduleConfig.defaults.size,
                sizes:
                    this.config.dataSource && this.config.dataSource.sizes
                        ? this.config.dataSource.sizes
                        : this.moduleConfig.defaults.sizes,
                nzShowSizeChanger:
                    this.config.dataSource && this.config.dataSource.nzShowSizeChanger !== undefined
                        ? this.config.dataSource.nzShowSizeChanger
                        : true,
                nzShowTotal:
                    this.config.dataSource && this.config.dataSource.nzShowTotal !== undefined ? this.config.dataSource.nzShowTotal : true,
            };

            this.cdr.markForCheck();

            this.load();
        }

        if (changes['index'] && this.index) {
            this.state = {
                ...this.state,
                index: this.index,
            };

            this.load();
        }

        if (changes['size'] && this.size) {
            this.state = {
                ...this.state,
                size: this.size,
                index: 1,
            };

            this.load();
        }

        if (changes['queryString']) {
            this.load({
                queryString: this.queryString,
            });
        }
    }

    ngOnInit(): void {
        this.nextLoadRequest$
            .pipe(debounceTime(DEBOUNCE_LOAD_REQUEST_MS), takeUntil(this.ngOnDestroy$))
            .subscribe((request) => this.performLoad(request));

        this.load();
    }

    ngOnDestroy(): void {
        this.nextLoad$.next();
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.crud.components.bazaCrudData.${input}`;
    }

    trustedUrl(url: string): SafeHtml {
        return this.domSanitizer.bypassSecurityTrustUrl(url);
    }

    get lastRequest(): CrudListRequestDto {
        return this.state.lastRequest;
    }

    get i18nTotalItems(): string {
        const key = (() => {
            if (!this.state.total) {
                return this.i18n('total.0');
            } else if (this.state.total === 1) {
                return this.i18n('total.1');
            } else {
                return this.i18n('total.many');
            }
        })();

        return this.translate.instant(key, {
            index: this.state.index,
            size: this.state.size,
            total: this.state.total,
        });
    }

    load(request: LoadRequest = {}): void {
        this.nextLoadRequest$.next(request);
    }

    loadFromRequest(
        request: Partial<{
            index: number;
            size: number;
            queryString: string;
            sort: Array<CrudListSort<T>>;
        }>,
    ): void {
        Object.assign(this.state, request);

        this.load();

        this.cdr.markForCheck();
    }

    loadNzPageSizeChanged(size: number): void {
        this.state = {
            ...this.state,
            index: 1,
            size,
        };

        this.load();
    }

    performLoad(request: LoadRequest): void {
        this.nextLoad$.next();

        if (request.index) {
            this.state.index = request.index;
        }

        if (request.dateFrom !== undefined) {
            this.state.dateFrom = request.dateFrom;
        }

        if (request.dateTo !== undefined) {
            this.state.dateTo = request.dateTo;
        }

        if (request.queryString !== undefined) {
            this.state.queryString = request.queryString;
        }

        const sortCommands: Array<CrudListSort<T>> = this.state.sort.map((s) => ({
            field: s.column.sortField ? s.column.sortField : (s.column.field as any),
            order: s.order === 'ascend' ? CrudListSortOrder.Asc : CrudListSortOrder.Desc,
        }));

        const loading = this.loading.addLoading();

        if (this.config.dataSource && this.config.dataSource.list) {
            const listRequest: CrudListRequestDto = {
                index: this.state.index,
                size: this.state.size,
                dateTo: this.state.dateTo || undefined,
                dateFrom: this.state.dateFrom || undefined,
                queryString: this.state.queryString || undefined,
                sort:
                    sortCommands.length > 0
                        ? {
                              commands: sortCommands,
                          }
                        : undefined,
            };

            this.config.dataSource
                .list(listRequest)
                .pipe(
                    retryWhen(genericRetryStrategy()),
                    tap(() => {
                        if (window && window.scrollTo) {
                            window.scrollTo({
                                top: 0,
                                behavior: 'auto',
                            });
                        }
                    }),
                    tap(() => loading.complete()),
                    finalize(() => loading.complete()),
                    takeUntil(this.nextLoad$),
                )
                .subscribe(
                    (response) => {
                        this.current$.next(response.items);

                        this.state = {
                            ...this.state,
                            total: response.pager.total,
                            index: response.pager.index,
                            size: response.pager.size,
                            withPagination: true,
                            lastRequest: listRequest,
                        };

                        this.cdr.markForCheck();
                    },
                    (err: BazaError) => {
                        isBazaErrorResponse(err) ? this.ngMessages.bazaError(err) : this.ngMessages.httpError();

                        this.cdr.markForCheck();
                    },
                );
        } else if (this.config.dataSource && this.config.dataSource.array) {
            this.config.dataSource
                .array({
                    queryString: request.queryString,
                    sort:
                        sortCommands.length > 0
                            ? {
                                  commands: sortCommands,
                              }
                            : undefined,
                })
                .pipe(
                    retryWhen(genericRetryStrategy()),
                    tap(() => {
                        if (window && window.scrollTo) {
                            window.scrollTo({
                                top: 0,
                                behavior: 'auto',
                            });
                        }
                    }),
                    finalize(() => loading.complete()),
                    takeUntil(this.nextLoad$),
                )
                .subscribe(
                    (response) => {
                        this.current$.next(response.items);

                        this.state = {
                            ...this.state,
                            index: 1,
                            size: response.items.length,
                            total: response.items.length,
                            withPagination: false,
                        };

                        this.cdr.markForCheck();
                    },
                    (err: BazaError) => {
                        isBazaErrorResponse(err) ? this.ngMessages.bazaError(err) : this.ngMessages.httpError();

                        this.cdr.markForCheck();
                    },
                );
        }
    }

    nzSortOrderChange(column: BazaCrudDataColumn<T>, order: NzTableSortOrder): void {
        if (!order) {
            this.state = {
                ...this.state,
                sort: this.state.sort.filter((c) => c.column.field !== column.field),
            };
        } else if (this.state.sort.find((c) => c.column.field === column.field)) {
            this.state = {
                ...this.state,
                sort: this.state.sort.map((c) => (c.column.field === column.field ? { ...c, order } : c)),
            };
        } else {
            this.state = {
                ...this.state,
                sort: [
                    ...this.state.sort,
                    {
                        column,
                        order,
                    },
                ],
            };
        }

        this.load({
            ...this.state.lastRequest,
        });
    }

    get activeColumns(): Array<BazaCrudDataColumn<T>> {
        return this.config.columns.filter((c) => !c.disabled);
    }

    get isLoading$(): Observable<boolean> {
        return this.loading.isLoading$;
    }

    get hasActions(): boolean {
        return (
            Array.isArray(this.config.actions) &&
            this.config.actions.length > 0 &&
            this.config.actions.some((action) => !action.acl || this.acl.hasAccess(action.acl))
        );
    }

    hasRowActions$(row: T): Observable<boolean> {
        if (!(Array.isArray(this.config.actions) && this.config.actions.length > 0)) {
            return of(false);
        }

        return combineLatest(this.config.actions.map((a) => this.isRowActionVisible$(a, row))).pipe(
            map((results) => results.includes(true)),
        );
    }

    trackByColumn(index: number, item: BazaCrudDataColumn<T>): string {
        return `${item.id}_${item.disabled}`;
    }

    columnNgStyle(column: BazaCrudDataColumn<T>): any {
        return {
            width: this.columnWidth(column),
            'text-align': this.columnAlign(column),
        };
    }

    rowNgStyle(column: BazaCrudDataColumn<T>, row: T): any {
        return {
            'text-align': this.columnAlign(column),
            ...(column.ngStyle ? column.ngStyle(row) : {}),
        };
    }

    rowNgClass(column: BazaCrudDataColumn<T>, row: T): any {
        const columnNgClass = column.ngClass ? column.ngClass(row) : [];

        if (column.level) {
            const level = column.level(row);

            if (Array.isArray(columnNgClass)) {
                columnNgClass.push(`indent-level-${level}`);
            } else {
                columnNgClass[`indent-level-${level}`] = true;
            }
        }

        if (this.config.dataSource.inactive && this.config.dataSource.inactive(row)) {
            if (Array.isArray(columnNgClass)) {
                columnNgClass.push(`inactive`);
            } else {
                columnNgClass[`inactive`] = true;
            }
        }

        if (this.config.dataSource.invalid && this.config.dataSource.invalid(row)) {
            if (Array.isArray(columnNgClass)) {
                columnNgClass.push(`invalid`);
            } else {
                columnNgClass[`invalid`] = true;
            }
        }

        return columnNgClass;
    }

    trRowNgClass(row: T): any {
        return {
            'is-row-clickable': this.config.dataSource.isRowClickable,
        };
    }

    onTrRowClick(row: T): any {
        if (this.config.dataSource.isRowClickable && this.config.dataSource.onRowClick) {
            this.config.dataSource.onRowClick(row);
        }
    }

    columnWidth(column: BazaCrudDataColumn<T>): any {
        if (column.type.kind === BazaTableFieldType.Id && !column.width) {
            return `${DEFAULT_COLUMN_WIDTH_ID}px`;
        }

        if (column.type.kind === BazaTableFieldType.Ulid && !column.width) {
            return `${DEFAULT_COLUMN_WIDTH_ULID}px`;
        }

        if (column.type.kind === BazaTableFieldType.Date && !column.width) {
            return `${DEFAULT_COLUMN_WIDTH_DATE}px`;
        }

        if (typeof column.width === 'number') {
            return `${column.width}px`;
        }

        if (typeof column.width === 'string') {
            return column.width;
        }

        return 'auto';
    }

    columnAlign(column: BazaCrudDataColumn<T>): any {
        if (
            [BazaTableFieldType.Id, BazaTableFieldType.Ulid, BazaTableFieldType.Date, BazaTableFieldType.Boolean].includes(
                column.type.kind,
            ) &&
            !column.textAlign
        ) {
            return 'center';
        }

        return column.textAlign || 'left';
    }

    date(input: string | Date): string {
        if (input instanceof Date) {
            const date = moment(input);

            return moment(date).format('LL');
        } else if (typeof input === 'string') {
            const date = moment(new Date(input));

            return moment(date).format('LL');
        } else {
            return 'N/A';
        }
    }

    dateUTC(input: string | Date): string {
        if (input instanceof Date) {
            const date = moment(input).utc();

            return moment(date).format('LL');
        } else if (typeof input === 'string') {
            const date = moment(new Date(input)).utc();

            return moment(date).format('LL');
        } else {
            return 'N/A';
        }
    }

    dateTime(input: string | Date): string {
        if (input instanceof Date) {
            const date = moment(input);

            return moment(date).format('LLL');
        } else if (typeof input === 'string') {
            const date = moment(new Date(input));

            return moment(date).format('LLL');
        } else {
            return 'N/A';
        }
    }

    isRowActionVisible$(action: BazaCrudAction<T>, row: T): Observable<boolean> {
        if (Array.isArray(action.acl) && action.acl.length > 0) {
            if (!this.acl.hasAccess(action.acl)) {
                return of(false);
            }
        }

        if (action.visible) {
            const result = action.visible(row);

            return result instanceof Observable ? result : of(result);
        } else {
            return of(true);
        }
    }

    actionTitle(action: BazaCrudAction<T>, row: T): string {
        return typeof action.title === 'function' ? action.title(row) : action.title;
    }

    text$(
        field: BazaTableFieldTypeText | BazaTableFieldTypeLink | BazaTableFieldTypeButton,
        column: BazaCrudDataColumn<T>,
        row: T,
    ): Observable<any> {
        if (field.format) {
            const result = field.format(row);

            return (result instanceof Observable ? result : of(result)).pipe(
                switchMap((next) => {
                    return field.translate ? this.translate.get(next, field.translateArgs ? field.translateArgs(row) : {}) : of(next);
                }),
            );
        } else {
            return of(this.source(column, row));
        }
    }

    hint$(hint: BazaTableFieldTypeHint): string {
        return hint.translate ? this.translate.instant(hint.text, hint.translateArgs) : hint.text;
    }

    source(column: BazaCrudDataColumn<T>, row: T): any {
        return column.source ? column.source(row) : row[column.field as keyof T];
    }

    float(column: BazaCrudDataColumn<T>, row: T): number {
        const source = parseFloat(this.source(column, row));

        if (isNaN(source)) {
            return 0;
        } else {
            return Math.floor(source) !== source ? parseFloat((source as number).toFixed(2)) : source;
        }
    }
}
