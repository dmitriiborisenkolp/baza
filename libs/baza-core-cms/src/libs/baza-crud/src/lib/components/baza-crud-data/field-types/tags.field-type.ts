import { BazaTableFieldType } from './_.field-type';

export interface BazaTableFieldTypeTags<T = any> {
    kind: BazaTableFieldType.Tags,
    translate?: boolean;
    translateArgs?: (entity: T) => any;
}
