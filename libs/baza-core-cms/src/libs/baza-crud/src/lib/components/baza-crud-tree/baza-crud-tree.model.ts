import { Observable } from 'rxjs';
import { CrudArrayRequestDto, CrudArrayResponseDto, CrudGetRequestDto, CrudGetResponseDto, CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { BazaCrudAction } from '../baza-crud-data/baza-crud-data.model';

export interface BazaCrudTreeNode<T = any> {
    readonly id: number;
    readonly parentId: number;
    readonly title: string;
    readonly meta: T;
}

export interface BazaCrudTreeDataConfig<T, R = T, PrimaryKey = number> {
    actions?: Array<BazaCrudAction<T>>;
    dataSource?: {
        nodes?: () => Observable<CrudListResponseDto<T>>;
        list?: (request: CrudListRequestDto<T>) => Observable<CrudListResponseDto<T>>;
        array?: (options: CrudArrayRequestDto<T>) => Observable<CrudArrayResponseDto<T>>;
        get?: (request: CrudGetRequestDto<PrimaryKey>, listEntity: T) => Observable<CrudGetResponseDto<R>>;
        sizes?: Array<number>;
        size?: number;
        nzShowSizeChanger?: boolean;
        nzShowTotal?: boolean;
        isItemClickable?: boolean;
        onItemClick?: (row: T) => void;
    };
}
