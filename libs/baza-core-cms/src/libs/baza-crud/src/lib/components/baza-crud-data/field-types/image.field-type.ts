import { BazaTableFieldType } from './_.field-type';

export interface BazaTableFieldTypeImage<T = any> {
    kind: BazaTableFieldType.Image,
    available: (entity: T) => boolean;
    nzSrc: (entity: T) => string,
}
