import { BazaTableFieldType } from './_.field-type';
import { NzProgressStatusType } from 'ng-zorro-antd/progress/typings';

export interface BazaTableFieldTypeProgress {
    kind: BazaTableFieldType.Progress;
    nzStatus: NzProgressStatusType;
}
