import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { BazaCrudNavigationConfig, BazaCrudNavigationNode } from '../../baza-crud-cms.model';

@Component({
    selector: 'baza-crud-navigation',
    templateUrl: './baza-crud-navigation.component.html',
    styleUrls: ['./baza-crud-navigation.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCrudNavigationComponent {
    @Input() config: BazaCrudNavigationConfig;

    constructor(
        private readonly translate: TranslateService,
    ) {}

    title$(node: BazaCrudNavigationNode): Observable<string> {
        return node.withoutTranslation
            ? of(node.title)
            : this.translate.get(node.title, node.translateArgs);
    }
}
