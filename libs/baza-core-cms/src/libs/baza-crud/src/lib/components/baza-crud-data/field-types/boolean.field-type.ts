import { BazaTableFieldType } from './_.field-type';

export interface BazaTableFieldTypeBoolean {
    kind: BazaTableFieldType.Boolean;
    withFalseIcon?: boolean
}
