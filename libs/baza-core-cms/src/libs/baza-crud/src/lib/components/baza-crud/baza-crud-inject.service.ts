import { Injectable } from '@angular/core';
import { BazaCrudConfig } from '../../baza-crud-cms.model';

/**
 * Inject Configuration
 */
interface BazaCrudInjectFullConfig<T = any> {
    inject(config: BazaCrudConfig<T>): void;
}

export type BazaCrudInjectConfig<T = any> = Partial<BazaCrudInjectFullConfig<T>>;

const INJECTION_CONFIGURATIONS: { [id: string]: BazaCrudInjectConfig } = {};

@Injectable({
    providedIn: 'root',
})
export class BazaCrudInjectService {
    /**
     * Register Inject Configuration for target CRUD
     * ID for target CRUD could be found in `data-baza-crud-id` attribute of `baza-crud-layout` component.
     * You also can call `bazaCrudId()` in console to get ID of CRUD which is displayed currently on page.
     */
    register<T = any>(id: string, config: BazaCrudInjectConfig<T>): void {
        INJECTION_CONFIGURATIONS[`${id}`] = config;
    }

    /**
     * Remove Inject Configuration of target CRUD
     * ID for target CRUD could be found in `data-baza-crud-id` attribute of `baza-crud-layout` component.
     */
    unregister(id: string): void {
        INJECTION_CONFIGURATIONS[`${id}`] = undefined;
    }

    /**
     * Returns true if there is Inject Configuration available (registered) for target CRUD
     */
    has(id: string): boolean {
        return !!INJECTION_CONFIGURATIONS[`${id}`];
    }

    /**
     * Returns Inject Configuration available (registered) for target CRUD
     */
    get(id: string): BazaCrudInjectConfig {
        if (!this.has(id)) {
            console.error(`No Baza CRUD injection configuration found for CMS "${id}"!`);
        }

        return INJECTION_CONFIGURATIONS[`${id}`] || {};
    }
}
