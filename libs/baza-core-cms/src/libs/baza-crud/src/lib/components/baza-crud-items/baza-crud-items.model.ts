import { Observable } from 'rxjs';
import { BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { FormControl } from '@angular/forms';
import { UrlCreationOptions } from '@angular/router';
import { CrudListRequestDto } from '@scaliolabs/baza-core-shared';
import { BazaCrudExportToCsvServiceRequest } from '../../services/baza-crud-export-to-csv.service';
import { NzDateMode } from 'ng-zorro-antd/date-picker/standard-types';

type BazaCrudItemId = string;

export enum BazaCrudItemsContext {
    TopLeft = 'topLeft',
    TopRight = 'topRight',
    BottomLeft = 'bottomLeft',
    BottomRight = 'bottomRight',
}

export enum BazaCrudItem {
    Button = 'button',
    Search = 'search',
    SearchField = 'searchField',
    Tabs = 'tabs',
    Checkbox = 'checkbox',
    ExportToCsv = 'exportToCsv',
    ImportCsv = 'importCsv',
    Legend = 'legend',
    DateRange = 'dateRange',
}

export interface AbstractBazaCrudItem {
    id?: BazaCrudItemId;
    type: BazaCrudItem;
    visible?: Observable<boolean>;
    acl?: Array<string>;
}

export interface BazaCrudItemsButton extends AbstractBazaCrudItem {
    type: BazaCrudItem.Button;
    options: BazaCrudItemButton;
}
export interface BazaCrudItemsSearch extends AbstractBazaCrudItem {
    type: BazaCrudItem.Search;
    options: BazaCrudItemSearch;
}
export interface BazaCrudItemsSearchField extends AbstractBazaCrudItem {
    type: BazaCrudItem.SearchField;
    options: BazaCrudItemSearchField;
}
export interface BazaCrudItemsTabs extends AbstractBazaCrudItem {
    type: BazaCrudItem.Tabs;
    options: BazaCrudTabs;
}
export interface BazaCrudItemsCheckbox extends AbstractBazaCrudItem {
    type: BazaCrudItem.Checkbox;
    options: BazaCrudItemCheckbox;
}
export interface BazaCrudItemsExportToCsv<DTO> extends AbstractBazaCrudItem {
    type: BazaCrudItem.ExportToCsv;
    options: BazaCrudExportToCsv<DTO>;
}
export interface BazaCrudItemsImportCsv<DTO> extends AbstractBazaCrudItem {
    type: BazaCrudItem.ImportCsv;
    options: BazaCrudImportCsv<DTO>;
}
export interface BazaCrudItemsLegend extends AbstractBazaCrudItem {
    type: BazaCrudItem.Legend;
    options: BazaCrudILegend;
}

export interface BazaCrudItemsDateRange extends AbstractBazaCrudItem {
    type: BazaCrudItem.DateRange;
    options: BazaDateRangeItem;
}

// tslint:disable-next-line:interface-over-type-literal
export type BazaCrudItems<DTO = any> =
    | BazaCrudItemsButton
    | BazaCrudItemsSearch
    | BazaCrudItemsSearchField
    | BazaCrudItemsTabs
    | BazaCrudItemsCheckbox
    | BazaCrudItemsExportToCsv<DTO>
    | BazaCrudItemsImportCsv<DTO>
    | BazaCrudItemsLegend
    | BazaCrudItemsDateRange;

export interface BazaCrudItemButton {
    title: string;
    callback: () => void;
    type?: BazaNzButtonStyle;
    icon?: string;
    withoutTranslation?: boolean;
    isDanger?: boolean;
    disabled?: Observable<boolean>;
}

export interface BazaCrudItemSearch {
    control?: FormControl;
    placeholder?: string;
    onSearch?: (queryString: string | undefined) => void;
    onClear?: () => void;
}

export interface BazaCrudItemSearchField {
    control: FormControl;
    onSearch?: (queryString: string | undefined) => void;
    onClear?: () => void;
    placeholder?: string;
}

export interface BazaCrudItemCheckbox {
    label: string;
    withoutTranslation?: boolean;
    translateParams?: any;
    control: FormControl;
}

export interface BazaCrudExportToCsv<DTO, LIST_REQUEST extends CrudListRequestDto = any> {
    title: string;
    withoutTranslation?: boolean;
    endpoint: (request: BazaCrudExportToCsvServiceRequest) => Observable<string>;
    fileNameGenerator: (crudItemId: string) => string;
    type?: BazaNzButtonStyle;
    icon?: string;
    isDanger?: boolean;
    disabled?: Observable<boolean>;
}

export interface BazaCrudImportCsv<DTO> {
    title: string;
    withoutTranslation?: boolean;
    endpoint: (file: File) => Observable<any>;
    callback: (response: any) => void;
    type?: BazaNzButtonStyle;
    icon?: string;
    isDanger?: boolean;
    disabled?: Observable<boolean>;
    hint?: string;
}

export interface BazaCrudILegend {
    items: Array<BazaCrudILegendItem>;
}

export interface BazaCrudILegendItem {
    label: string;
    color: string;
    withoutTranslation?: boolean;
    translateParams?: any;
}

export interface BazaDateRangeItem {
    nzMode: NzDateMode;
}

export interface BazaCrudTabs {
    tabs: Array<BazaCrudTab>;
}

export interface BazaCrudTab {
    id?: string;
    title: string;
    withoutTranslate?: boolean;
    routerLink?: {
        commands: any[];
        navigationExtras?: UrlCreationOptions;
        skipLocationChange?: boolean;
        replaceUrl?: boolean;
        acl?: Array<string>;
    };
    routerLinkExact?: boolean;
    onClick?: (id: string, tab: BazaCrudTab) => void;
    isActive$?: Observable<boolean>;
}
