import { BazaTableFieldType } from './_.field-type';

export interface BazaTableFieldTypeDateTime {
    kind: BazaTableFieldType.DateTime;
}
