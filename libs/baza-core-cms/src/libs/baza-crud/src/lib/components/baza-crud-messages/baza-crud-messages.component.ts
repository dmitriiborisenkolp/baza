import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BazaCrudMessage } from '../../baza-crud-cms.model';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';

@Component({
    selector: 'baza-crud-messages',
    templateUrl: './baza-crud-messages.component.html',
    styleUrls: ['./baza-crud-messages.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCrudMessagesComponent {
    @Input() messages: Array<BazaCrudMessage>;

    constructor(
        private readonly translate: TranslateService,
    ) {}

    message$(input: BazaCrudMessage): Observable<string> {
        if (input.message.translate) {
            return this.translate.get(
                input.message.text,
                input.message.translateArgs,
            );
        } else {
            return of(input.message.text);
        }
    }

    description$(input: BazaCrudMessage): Observable<string | undefined> {
        if (! input.description) {
            return undefined;
        }

        if (input.description.translate) {
            return this.translate.get(
                input.description.text,
                input.description.translateArgs,
            );
        } else {
            return of(input.description.text);
        }
    }
}
