import { CrudArrayRequestDto, CrudArrayResponseDto, CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaTableFieldTypeBoolean } from './field-types/boolean.field-type';
import { BazaTableFieldTypeDate } from './field-types/date.field-type';
import { BazaTableFieldTypeDateUTC } from './field-types/date-utc.field-type';
import { BazaTableFieldTypeId } from './field-types/id.field-type';
import { BazaTableFieldTypeText } from './field-types/text.field-type';
import { BazaTableFieldTypeLink } from './field-types/link.field-type';
import { BazaTableFieldTypeButton } from './field-types/button.field-type';
import { BazaTableFieldTypeDateTime } from './field-types/date-time.field-type';
import { BazaTableFieldTypeTags } from './field-types/tags.field-type';
import { BazaTableFieldTypeProgress } from './field-types/progress.field-type';
import { BazaTableFieldTypeImage } from './field-types/image.field-type';
import { BazaTableFieldTypeDownloadLink } from './field-types/download-link.field-type';
import { BazaTableFieldTypeUlid } from './field-types/ulid.field-type';

export type BazaCrudColumnId = string;
export type BazaCrudTitleFunc<T> = (input: T) => string;
export type BazaCrudQueryString = string | undefined;

export * from './field-types/_.field-type';
export * from './field-types/id.field-type';
export * from './field-types/ulid.field-type';
export * from './field-types/boolean.field-type';
export * from './field-types/button.field-type';
export * from './field-types/date.field-type';
export * from './field-types/date-utc.field-type';
export * from './field-types/link.field-type';
export * from './field-types/download-link.field-type';
export * from './field-types/id.field-type';
export * from './field-types/text.field-type';
export * from './field-types/date-time.field-type';
export * from './field-types/tags.field-type';
export * from './field-types/progress.field-type';
export * from './field-types/image.field-type';

export type BazaTableFieldTypes<T = any> =
    | BazaTableFieldTypeId
    | BazaTableFieldTypeUlid
    | BazaTableFieldTypeText<T>
    | BazaTableFieldTypeLink<T>
    | BazaTableFieldTypeDownloadLink<T>
    | BazaTableFieldTypeButton<T>
    | BazaTableFieldTypeBoolean
    | BazaTableFieldTypeDate
    | BazaTableFieldTypeDateUTC
    | BazaTableFieldTypeDateTime
    | BazaTableFieldTypeTags
    | BazaTableFieldTypeProgress
    | BazaTableFieldTypeImage;

export enum BazaTableFieldAlign {
    Left = 'left',
    Center = 'center',
    Right = 'right',
}

export interface BazaCrudDataConfig<T, R = T, PrimaryKey = number> {
    columns: Array<BazaCrudDataColumn<T>>;
    actions?: Array<BazaCrudAction<T>>;
    dataSource?: {
        list?: (request: CrudListRequestDto<T>) => Observable<CrudListResponseDto<T>>;
        array?: (options: CrudArrayRequestDto<T>) => Observable<CrudArrayResponseDto<T>>;
        sizes?: Array<number>;
        size?: number;
        nzShowSizeChanger?: boolean;
        nzShowTotal?: boolean;
        isRowClickable?: boolean;
        onRowClick?: (row: T) => void;
        inactive?: (row: T) => boolean;
        invalid?: (row: T) => boolean;
    };
}

export interface BazaCrudAction<T> {
    title?: string | BazaCrudTitleFunc<T>;
    withoutTranslate?: boolean;
    icon: string;
    action: (entity: T) => any;
    acl?: Array<string>;
    visible?: (entity: T) => Observable<boolean> | boolean;
}

export interface BazaCrudDataColumn<T> {
    id?: BazaCrudColumnId;
    title: string;
    type: BazaTableFieldTypes<T>;
    source?: (entity: T) => any;
    level?: (entity: T) => number;
    withoutTranslate?: boolean;
    field?: keyof T | string;
    value?: (entity: T) => any;
    width?: 'auto' | number | string;
    disabled?: boolean;
    sortable?: boolean;
    sortField?: string;
    textAlign?: BazaTableFieldAlign;
    ngStyle?: (entity: T) => any;
    ngClass?: (entity: T) => any;
}
