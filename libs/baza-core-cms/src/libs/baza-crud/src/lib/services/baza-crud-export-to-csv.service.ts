import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { BAZA_CRUD_CONSTANTS, CrudExportToCsvRequest, CrudListRequestDto } from '@scaliolabs/baza-core-shared';
import { BazaLoadingService } from '@scaliolabs/baza-core-ng';
import { finalize, takeUntil } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd/modal';
import {
    BazaCrudExportToCsvComponent,
    BazaCrudExportToCsvComponentResponse,
} from '../modals/baza-crud-export-to-csv/baza-crud-export-to-csv.component';

type Request<T = any, LIST_REQUEST extends CrudListRequestDto = CrudListRequestDto> = Omit<
    CrudExportToCsvRequest<T, LIST_REQUEST>,
    'fields'
>;

type ExportToCSVCallback<T = any, LIST_REQUEST extends CrudListRequestDto = any> = (
    request: Request<T, LIST_REQUEST>,
) => Observable<string>;

interface Options<LIST_REQUEST extends CrudListRequestDto> {
    fileName: string;
    takeUntil$: Subject<void>;
    lastRequest: LIST_REQUEST;
}

export { Request as BazaCrudExportToCsvServiceRequest };

@Injectable()
export class BazaCrudExportToCsvService {
    constructor(
        private readonly loading: BazaLoadingService,
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    exportToCSV<T = any, LIST_REQUEST extends CrudListRequestDto = any>(
        dataAccess: ExportToCSVCallback,
        options: Options<LIST_REQUEST>,
        vcr: ViewContainerRef,
    ): void {
        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaCrudExportToCsvComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], vcr.injector);

        const component = factory.create(injector);

        vcr.insert(component.hostView);

        setTimeout(() => {
            const afterClosed$ = new Subject<void>();

            component.instance.fileName = options.fileName;

            component.instance.state.nzModalRef = this.nzModal.create({
                nzTitle: null,
                nzOkText: null,
                nzContent: component.instance.nzModal,
                nzClosable: false,
                nzWidth: 520,
                nzFooter: null,
                nzCloseOnNavigation: true,
                nzMaskClosable: true,
            });

            component.instance.onSubmit.pipe(takeUntil(afterClosed$)).subscribe((modalResponse) => {
                this.performExportToCSV(dataAccess, options, modalResponse);
            });

            component.instance.onClose.pipe(takeUntil(afterClosed$)).subscribe(() => {
                afterClosed$.next();
            });
        });
    }

    private performExportToCSV<T = any, LIST_REQUEST extends CrudListRequestDto = any>(
        dataAccess: ExportToCSVCallback,
        options: Options<LIST_REQUEST>,
        modalResponse: BazaCrudExportToCsvComponentResponse,
    ): void {
        const loading = this.loading.addLoading();

        dataAccess({
            listRequest: {
                ...options.lastRequest,
                size: -1,
                index: 1,
            },
            delimiter: modalResponse.delimiter || BAZA_CRUD_CONSTANTS.csvDefaultDelimiter,
        })
            .pipe(
                finalize(() => loading.complete()),
                takeUntil(options.takeUntil$),
            )
            .subscribe((response) => {
                const pom = document.createElement('a');
                const blob = new Blob([response], { type: 'text/csv;charset=utf-8;' });

                pom.href = URL.createObjectURL(blob);
                pom.setAttribute('download', options.fileName);
                pom.click();
            });
    }
}
