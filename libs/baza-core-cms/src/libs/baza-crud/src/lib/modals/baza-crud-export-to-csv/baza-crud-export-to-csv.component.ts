import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BAZA_CRUD_CONSTANTS } from '@scaliolabs/baza-core-shared';
import { NavigationStart, Router } from '@angular/router';
import { filter, take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

interface State {
    nzModalRef?: NzModalRef<BazaCrudExportToCsvComponent>;
    form: FormGroup;
}

interface FormValue {
    delimiter: string;
}

export { FormValue as BazaCrudExportToCsvComponentResponse };

@Component({
    templateUrl: './baza-crud-export-to-csv.component.html',
    styleUrls: ['./baza-crud-export-to-csv.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaCrudExportToCsvComponent implements OnInit, OnDestroy {
    @Input() fileName: string;

    @Output() onClose = new EventEmitter<void>();
    @Output() onSubmit = new EventEmitter<FormValue>();

    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<any>;

    private readonly ngOnDestroy$ = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            delimiter: [BAZA_CRUD_CONSTANTS.csvDefaultDelimiter, [Validators.required]],
        }),
    };

    constructor(private readonly fb: FormBuilder, private readonly router: Router) {}

    ngOnInit(): void {
        this.router.events
            .pipe(
                filter((event) => event instanceof NavigationStart),
                take(1),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe(() => {
                if (this.state.nzModalRef) {
                    this.state.nzModalRef.close();
                }
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza.crud.modals.bazaCrudExportToCsv.${key}`;
    }

    submit(): void {
        if (this.state.form.valid) {
            this.onSubmit.next(this.state.form.value);

            this.state.nzModalRef.close();
        }
    }

    cancel(): void {
        this.onClose.next();

        this.state.nzModalRef.close();
    }
}
