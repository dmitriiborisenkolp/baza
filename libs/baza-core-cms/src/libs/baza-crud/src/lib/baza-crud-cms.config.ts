import { Injectable } from '@angular/core';

@Injectable()
export class BazaCrudCmsConfig {
    defaults: {
        size: number;
        sizes: Array<number>;
    };
}
