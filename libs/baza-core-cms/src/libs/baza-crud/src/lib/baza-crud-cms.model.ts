import { BazaCrudDataConfig } from './components/baza-crud-data/baza-crud-data.model';
import { BazaCrudItems } from './components/baza-crud-items/baza-crud-items.model';
import { Observable } from 'rxjs';
import { BazaCrudTreeDataConfig } from './components/baza-crud-tree/baza-crud-tree.model';

export type BazaCrudLayoutTitleLevel = 1 | 2 | 3 | 4 | 5 | 6;

export interface BazaCrudLayoutConfig {
    title?: string;
    titleLevel?: BazaCrudLayoutTitleLevel;
    titleArguments?: any;
    withoutTranslation?: boolean;
    backRouterLink?: {
        routerLink: any;
        queryParams?: any;
        state?: any;
    };
    navigation?: BazaCrudNavigationConfig;
    messages?: Array<BazaCrudMessage>;
    labels?: Array<BazaCrudLabel>;
    items?: {
        topLeft?: Array<BazaCrudItems>;
        topRight?: Array<BazaCrudItems>;
        toolbarLeft?: Array<BazaCrudItems>;
        toolbarRight?: Array<BazaCrudItems>;
        bottomLeft?: Array<BazaCrudItems>;
        bottomRight?: Array<BazaCrudItems>;
    };
}

export interface BazaCrudMessage {
    message: {
        text: string;
        translate?: boolean;
        translateArgs?: any;
    };
    description?: {
        text: string;
        translate?: boolean;
        translateArgs?: any;
    };
    level: BazaCrudMessageLevel;
    withIcon?: boolean;
}

export enum BazaCrudMessageLevel {
    Success = 'success',
    Info = 'info',
    Warning = 'warning',
    Error = 'error',
}

export interface BazaCrudLabel {
    id: string;
    title: string;
    translate?: boolean;
    translateArgs?: any;
    isActive$?: Observable<boolean>;
    callback: (label: BazaCrudLabel) => void;
}

export interface BazaCrudNavigationConfig {
    nodes: Array<BazaCrudNavigationNode>;
}

export interface BazaCrudNavigationNode {
    title?: string;
    withoutTranslation?: boolean;
    translateArgs?: any;
    icon?: string;
    routerLink?: {
        routerLink: any;
        queryParams?: any;
        state?: any;
    };
}

export interface BazaCrudConfig<T> extends BazaCrudLayoutConfig {
    // ID can be used to externally inject custom configuration
    id?: string;

    // Data definition (columns, endpoints, item actions, items)
    data?: BazaCrudDataConfig<T>;

    // Tree Data definition (can used instead of `data`)
    treeData?: BazaCrudTreeDataConfig<T>;
}
