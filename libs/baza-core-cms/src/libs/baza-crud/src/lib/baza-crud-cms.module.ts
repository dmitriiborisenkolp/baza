import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCrudComponent } from './components/baza-crud/baza-crud.component';
import { BazaCrudDataComponent } from './components/baza-crud-data/baza-crud-data.component';
import { BazaCrudItemsComponent } from './components/baza-crud-items/baza-crud-items.component';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { TranslateModule } from '@ngx-translate/core';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzInputModule } from 'ng-zorro-antd/input';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTableModule } from 'ng-zorro-antd/table';
import { BazaCrudCmsConfig } from './baza-crud-cms.config';
import { BazaAclNgModule } from '@scaliolabs/baza-core-ng';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { BazaCrudLayoutComponent } from './components/baza-crud-layout/baza-crud-layout.component';
import { BazaCrudNavigationComponent } from './components/baza-crud-navigation/baza-crud-navigation.component';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { BazaCrudMessagesComponent } from './components/baza-crud-messages/baza-crud-messages.component';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { BazaCrudLabelsComponent } from './components/baza-crud-labels/baza-crud-labels.component';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzTreeModule } from 'ng-zorro-antd/tree';
import { BazaCrudTreeComponent } from './components/baza-crud-tree/baza-crud-tree.component';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzTreeViewModule } from 'ng-zorro-antd/tree-view';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { BazaCrudExportToCsvComponent } from './modals/baza-crud-export-to-csv/baza-crud-export-to-csv.component';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { BazaCrudExportToCsvService } from './services/baza-crud-export-to-csv.service';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';

interface ForRootOptions {
    deps?: Array<any>;
    useFactory: (...deps) => BazaCrudCmsConfig;
}

export { ForRootOptions as BazaCrudCmsModuleForRootOptions };

@NgModule({
    imports: [
        CommonModule,
        NzTypographyModule,
        NzButtonModule,
        TranslateModule,
        NzMenuModule,
        NzInputModule,
        ReactiveFormsModule,
        RouterModule,
        NzToolTipModule,
        NzIconModule,
        NzTableModule,
        BazaAclNgModule,
        NzDropDownModule,
        NzTagModule,
        NzProgressModule,
        NzBreadCrumbModule,
        NzCheckboxModule,
        NzAlertModule,
        NzSpaceModule,
        NzTreeModule,
        NzTreeViewModule,
        NzImageModule,
        NzRadioModule,
        NzDatePickerModule,
    ],
    declarations: [
        BazaCrudComponent,
        BazaCrudDataComponent,
        BazaCrudItemsComponent,
        BazaCrudLayoutComponent,
        BazaCrudNavigationComponent,
        BazaCrudMessagesComponent,
        BazaCrudLabelsComponent,
        BazaCrudTreeComponent,
        BazaCrudExportToCsvComponent,
    ],
    exports: [BazaCrudComponent, BazaCrudDataComponent, BazaCrudItemsComponent, BazaCrudLayoutComponent, BazaCrudTreeComponent],
    providers: [BazaCrudExportToCsvService],
})
export class BazaCrudCmsModule {
    static forRoot(options: ForRootOptions): ModuleWithProviders<BazaCrudCmsModule> {
        return {
            ngModule: BazaCrudCmsModule,
            providers: [
                {
                    provide: BazaCrudCmsConfig,
                    deps: options.deps,
                    useFactory: options.useFactory,
                },
            ],
        };
    }
}
