import { Routes } from '@angular/router';
import { BazaAuthCmsMasterPasswordComponent } from './components/baza-auth-cms-master-password/baza-auth-cms-master-password.component';
import { BAZA_AUTH_CMS_MASTER_PASSWORD_RESOLVE_KEY, BazaAuthCmsMasterPasswordResolve } from './components/baza-auth-cms-master-password/baza-auth-cms-master-password.resolve';

export const bazaAuthCmsRoutes: Routes = [
    {
        path: 'auth-master-password',
        component: BazaAuthCmsMasterPasswordComponent,
        resolve: {
            [BAZA_AUTH_CMS_MASTER_PASSWORD_RESOLVE_KEY]: BazaAuthCmsMasterPasswordResolve,
        },
    },
];
