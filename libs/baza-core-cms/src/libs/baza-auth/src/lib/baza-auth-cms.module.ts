import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaAuthCmsMasterPasswordResolve } from './components/baza-auth-cms-master-password/baza-auth-cms-master-password.resolve';
import { BazaAuthCmsMasterPasswordComponent } from './components/baza-auth-cms-master-password/baza-auth-cms-master-password.component';
import { RouterModule } from '@angular/router';
import { BazaAuthDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { TranslateModule } from '@ngx-translate/core';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { ReactiveFormsModule } from '@angular/forms';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { BazaAuthCmsMasterPasswordService } from './services/baza-auth-cms-master-password.service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        BazaAuthDataAccessModule,
        NzTypographyModule,
        TranslateModule,
        NzSpaceModule,
        NzSwitchModule,
        ReactiveFormsModule,
        NzInputModule,
        NzButtonModule,
    ],
    providers: [BazaAuthCmsMasterPasswordResolve, BazaAuthCmsMasterPasswordService],
    declarations: [BazaAuthCmsMasterPasswordComponent],
})
export class BazaAuthCmsModule {}
