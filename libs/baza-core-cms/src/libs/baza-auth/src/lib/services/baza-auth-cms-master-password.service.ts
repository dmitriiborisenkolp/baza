import { Injectable } from '@angular/core';
import { BazaAuthMasterPasswordDataAccess } from '@scaliolabs/baza-core-data-access';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { AuthMasterPasswordDto } from '@scaliolabs/baza-core-shared';
import { BazaCmsBannerMessageService, BazaCmsBannerMessageType } from '../../../../../cms/layout/src';
import { BehaviorSubject, interval, Observable, retryWhen, Subject, takeUntil, tap } from 'rxjs';

const UPDATE_INTERVAL_MS: number = 15 /* sec */ * 1000; /* ms */

@Injectable({
    providedIn: 'root',
})
export class BazaAuthCmsMasterPasswordService {
    private readonly currentData: BehaviorSubject<AuthMasterPasswordDto> = new BehaviorSubject<AuthMasterPasswordDto>({ enabled: false });
    private readonly disabled$: Subject<void> = new Subject<void>();

    private isRefreshInProgress = false;

    current$ = this.currentData.asObservable();

    constructor(
        private readonly endpoint: BazaAuthMasterPasswordDataAccess,
        private readonly bazaCmsBannerMessageService: BazaCmsBannerMessageService,
    ) {
        this.fetchCurrent();
    }

    refresh(): void {
        if (!this.isRefreshInProgress) {
            interval(UPDATE_INTERVAL_MS)
                .pipe(takeUntil(this.disabled$))
                .subscribe(() => {
                    this.isRefreshInProgress = true;
                    this.fetchCurrent();
                });
        }
    }

    fetchCurrent(): void {
        this.endpoint
            .current()
            .pipe(
                retryWhen(genericRetryStrategy()),
                tap((current) => this.checkCurrent(current)),
            )
            .subscribe((current) => this.currentData.next(current));
    }

    checkCurrent(dto: AuthMasterPasswordDto): void {
        // if enabled, display banner if not already displayed and start refresh if not already started
        if (dto.enabled) {
            this.bazaCmsBannerMessageService.displayMessage(BazaCmsBannerMessageType.MasterPassword);
            this.refresh();
        } else {
            // if disabled, remove banner if visible, set disabled$ true
            this.bazaCmsBannerMessageService.hideMessage(BazaCmsBannerMessageType.MasterPassword);
            this.disabled$.next();
            this.isRefreshInProgress = false;
        }
    }

    enable(): Observable<void> {
        return this.endpoint.enable().pipe(
            retryWhen(genericRetryStrategy()),
            tap(() => this.fetchCurrent()),
        );
    }

    disable(): Observable<void> {
        return this.endpoint.disable().pipe(
            retryWhen(genericRetryStrategy()),
            tap(() => {
                this.disabled$.next();
                this.isRefreshInProgress = false;
            }),
            tap(() => this.fetchCurrent()),
        );
    }
}
