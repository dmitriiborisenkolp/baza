import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BAZA_AUTH_CMS_MASTER_PASSWORD_RESOLVE_KEY, BazaAuthCmsMasterPasswordResolveData } from './baza-auth-cms-master-password.resolve';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthMasterPasswordDto } from '@scaliolabs/baza-core-shared';
import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BazaLoadingService, LoadingHandler } from '@scaliolabs/baza-core-ng';
import { BazaAuthCmsMasterPasswordService } from '../../services/baza-auth-cms-master-password.service';

interface State {
    form: FormGroup;
    current: AuthMasterPasswordDto;
    show: boolean;
}

interface FormValue {
    enabled: boolean;
}

@Component({
    templateUrl: './baza-auth-cms-master-password.component.html',
    styleUrls: ['./baza-auth-cms-master-password.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaAuthCmsMasterPasswordComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            enabled: [false],
        }),
        current: this.resolvedData.dto,
        show: false,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly activatedRoute: ActivatedRoute,
        private readonly loading: BazaLoadingService,
        private readonly bazaAuthCmsMasterPasswordService: BazaAuthCmsMasterPasswordService,
    ) {}

    ngOnInit(): void {
        this.bazaAuthCmsMasterPasswordService.current$
            .pipe(
                takeUntil(this.ngOnDestroy$),
                tap((current) => this.update(current)),
                tap(() => this.cdr.markForCheck()),
            )
            .subscribe();

        const updateOnChange = () => {
            let loading: LoadingHandler;

            this.state.form
                .get('enabled')
                .valueChanges.pipe(
                    switchMap((next) => {
                        loading = this.loading.addLoading();
                        return next ? this.bazaAuthCmsMasterPasswordService.enable() : this.bazaAuthCmsMasterPasswordService.disable();
                    }),
                    tap(() => loading.complete()),
                    tap(() => this.cdr.markForCheck()),
                    takeUntil(this.ngOnDestroy$),
                )
                .subscribe();
        };

        this.update(this.state.current);

        updateOnChange();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get resolvedData(): BazaAuthCmsMasterPasswordResolveData {
        return this.activatedRoute.snapshot.data[`${BAZA_AUTH_CMS_MASTER_PASSWORD_RESOLVE_KEY}`];
    }

    get isLoading$(): Observable<boolean> {
        return this.loading.isLoading$;
    }

    i18n(input: string): string {
        return `baza.auth.masterPassword.components.bazaAuthCmsMasterPassword.${input}`;
    }

    update(withDTO: AuthMasterPasswordDto): void {
        this.state = {
            ...this.state,
            current: withDTO,
        };
        this.state.form.patchValue(
            {
                enabled: withDTO.enabled,
            } as Partial<FormValue>,
            {
                emitEvent: false,
            },
        );
        this.cdr.markForCheck();
    }

    get current(): string {
        return this.state.show ? this.state.current.current : '********************************';
    }

    toggle(): void {
        this.state = {
            ...this.state,
            show: !this.state.show,
        };
    }
}
