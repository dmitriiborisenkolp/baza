import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthMasterPasswordDto } from '@scaliolabs/baza-core-shared';
import { map, take } from 'rxjs/operators';
import { BazaAuthCmsMasterPasswordService } from '../../services/baza-auth-cms-master-password.service';

export const BAZA_AUTH_CMS_MASTER_PASSWORD_RESOLVE_KEY = 'authMasterPassword';

interface ResolveData {
    dto: AuthMasterPasswordDto;
}

export { ResolveData as BazaAuthCmsMasterPasswordResolveData };

@Injectable({
    providedIn: 'root',
})
export class BazaAuthCmsMasterPasswordResolve implements Resolve<ResolveData> {
    constructor(private readonly bazaAuthCmsMasterPasswordService: BazaAuthCmsMasterPasswordService) {}

    resolve(): Observable<ResolveData> {
        return this.bazaAuthCmsMasterPasswordService.current$.pipe(
            take(1),
            map((dto) => ({ dto })),
        );
    }
}
