import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BazaMailDataAccess } from '@scaliolabs/baza-core-data-access';
import { BazaCmsBannerMessageService, BazaCmsBannerMessageType } from '../../../../../cms/layout/src';
import { retryWhen, tap } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaMailIsReadyToUseResponse } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaMailCustomerioLinkBannerService {
    constructor(private readonly dataAccess: BazaMailDataAccess, private readonly ngBannerService: BazaCmsBannerMessageService) {}

    /**
     * Updates Banner about Customer.IO Ready Status
     */
    updateStatus(): Observable<unknown> {
        return this.dataAccess.isReadyToUse().pipe(
            retryWhen(genericRetryStrategy()),
            tap((response: BazaMailIsReadyToUseResponse) => {
                response.ready
                    ? this.ngBannerService.hideMessage(BazaCmsBannerMessageType.CustomerIoIsNotReady)
                    : this.ngBannerService.displayMessage(BazaCmsBannerMessageType.CustomerIoIsNotReady);
            }),
        );
    }
}
