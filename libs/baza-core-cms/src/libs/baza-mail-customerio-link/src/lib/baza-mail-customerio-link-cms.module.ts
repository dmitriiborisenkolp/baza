import { ModuleWithProviders, NgModule } from '@angular/core';
import { BazaMailCustomerioLinkCmsComponent } from './components/baza-mail-customerio-link-cms/baza-mail-customerio-link-cms.component';
import { BazaMailCustomerioLinkTagsCmsComponent } from './components/baza-mail-customerio-link-tags-cms/baza-mail-customerio-link-tags-cms.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaCmsLayoutModule } from '../../../../cms/layout/src';
import { BazaFormBuilderModule } from '../../../baza-form-builder/src';
import { BazaCrudCmsModule } from '../../../baza-crud/src';
import { BazaMailCustomerioLinksCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaMailDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaMailCustomerioLinkTagsCmsResolve } from './components/baza-mail-customerio-link-tags-cms/baza-mail-customerio-link-tags-cms.resolve';
import { BazaMailCustomerioLinkCmsResolve } from './components/baza-mail-customerio-link-cms/baza-mail-customerio-link-cms.resolve';
import { BazaMailCustomerioLinkCmsTemplateDocComponent } from './components/baza-mail-customerio-link-cms/template-doc/template-doc.component';
import { BazaMailCustomerioLinkCmsTemplateVariablesDocComponent } from './components/baza-mail-customerio-link-cms/template-variables-doc/template-variables-doc.component';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { BAZA_MAIL_CUSTOMERIO_LINK_CMS_CONFIG, BazaMailCustomerioLinkCmsConfig } from './baza-mail-customerio-link-cms.config';
import { BazaMailCustomerioLinkBannerService } from './services/baza-mail-customerio-link-banner.service';

interface ForRootOptions {
    deps?: Array<unknown>;
    useFactory: () => BazaMailCustomerioLinkCmsConfig;
}

export { ForRootOptions as BazaMailCustomerioLinkCmsModuleForRootOptions };

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BazaFormBuilderModule,
        BazaCrudCmsModule,
        BazaCmsLayoutModule,
        BazaMailCustomerioLinksCmsDataAccessModule,
        BazaMailDataAccessModule,
        NzTypographyModule,
        NzIconModule,
    ],
    declarations: [
        BazaMailCustomerioLinkCmsComponent,
        BazaMailCustomerioLinkTagsCmsComponent,
        BazaMailCustomerioLinkCmsTemplateDocComponent,
        BazaMailCustomerioLinkCmsTemplateVariablesDocComponent,
    ],
    providers: [BazaMailCustomerioLinkTagsCmsResolve, BazaMailCustomerioLinkCmsResolve, BazaMailCustomerioLinkBannerService],
})
export class BazaMailCustomerioLinkCmsModule {
    constructor(private readonly bannerService: BazaMailCustomerioLinkBannerService) {
        this.bannerService.updateStatus().subscribe();
    }

    static forRoot(options: ForRootOptions): ModuleWithProviders<BazaMailCustomerioLinkCmsModule> {
        return {
            ngModule: BazaMailCustomerioLinkCmsModule,
            providers: [
                {
                    provide: BAZA_MAIL_CUSTOMERIO_LINK_CMS_CONFIG,
                    useFactory: options.useFactory,
                    deps: options.deps,
                },
            ],
        };
    }
}
