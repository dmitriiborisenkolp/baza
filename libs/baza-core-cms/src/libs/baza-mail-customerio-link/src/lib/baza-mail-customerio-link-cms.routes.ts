import { Routes } from '@angular/router';
import { BazaMailCustomerioLinkTagsCmsComponent } from './components/baza-mail-customerio-link-tags-cms/baza-mail-customerio-link-tags-cms.component';
import { BazaMailCustomerioLinkCmsComponent } from './components/baza-mail-customerio-link-cms/baza-mail-customerio-link-cms.component';
import { BazaMailCustomerioLinkTagsCmsResolve } from './components/baza-mail-customerio-link-tags-cms/baza-mail-customerio-link-tags-cms.resolve';
import { BazaMailCustomerioLinkCmsResolve } from './components/baza-mail-customerio-link-cms/baza-mail-customerio-link-cms.resolve';

export const bazaMailCustomerioLinkCmsRoutes: Routes = [
    {
        path: 'mail/customerio-links',
        component: BazaMailCustomerioLinkTagsCmsComponent,
        resolve: {
            data: BazaMailCustomerioLinkTagsCmsResolve,
        },
    },
    {
        path: 'mail/customerio-links/:tag',
        component: BazaMailCustomerioLinkCmsComponent,
        resolve: {
            data: BazaMailCustomerioLinkCmsResolve,
        },
    },
];
