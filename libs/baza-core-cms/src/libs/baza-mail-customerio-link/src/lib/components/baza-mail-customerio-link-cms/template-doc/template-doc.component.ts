import { ChangeDetectionStrategy, Component, Input, TemplateRef, ViewChild } from '@angular/core';
import { BazaMailTemplate, BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';

@Component({
    templateUrl: './template-doc.component.html',
    styleUrls: ['./template-doc.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaMailCustomerioLinkCmsTemplateDocComponent {
    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<unknown>;

    @Input() mailTemplate: BazaMailTemplate;

    public nzModalRef: NzModalRef;

    get commonVariables(): Array<BazaMailTemplateVariable> {
        return (this.mailTemplate.variables || []).filter((next) => next.isDefault);
    }

    get templateVariables(): Array<BazaMailTemplateVariable> {
        return (this.mailTemplate.variables || []).filter((next) => !next.isDefault);
    }

    close(): void {
        this.nzModalRef.close();
    }
}
