import { ChangeDetectionStrategy, Component, ComponentFactoryResolver, Inject, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudItem, BazaTableFieldType } from '../../../../../baza-crud/src';
import { ActivatedRoute } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { BazaMailCustomerioLinkCmsResolveData } from './baza-mail-customerio-link-cms.resolve';
import { catchError, finalize, map, retryWhen, tap } from 'rxjs/operators';
import { BazaFormBuilderControlType, BazaFormBuilderLayout, BazaFormLayoutModalService } from '../../../../../baza-form-builder/src';
import { FormBuilder, Validators } from '@angular/forms';
import {
    BazaLoadingService,
    BazaNgConfirmService,
    BazaNgMessagesService,
    genericRetryStrategy,
    intValidator,
    moreThanZeroValidator,
} from '@scaliolabs/baza-core-ng';
import { BazaMailCustomerioLinksCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { BazaMailTemplate } from '@scaliolabs/baza-core-shared';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BazaMailCustomerioLinkCmsTemplateDocComponent } from './template-doc/template-doc.component';
import { BAZA_MAIL_CUSTOMERIO_LINK_CMS_CONFIG, BazaMailCustomerioLinkCmsConfig } from '../../baza-mail-customerio-link-cms.config';
import { BazaMailCustomerioLinkBannerService } from '../../services/baza-mail-customerio-link-banner.service';

const BAZA_CORE_MAIL_CUSTOMERIO_LINKS_CMS_LINK_FORM_ID = 'baza-core-mail-customerio-links-cms-link-form-id';

interface Row {
    name: string;
    title: string;
    description: string;
    customerioId: number | undefined;
    template: BazaMailTemplate;
}

interface LinkFormValue {
    customerIoId: number;
}

interface State {
    config: BazaCrudConfig<Row>;
}

@Component({
    templateUrl: './baza-mail-customerio-link-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaMailCustomerioLinkCmsComponent {
    public crud: BazaCrudComponent<Row>;

    constructor(
        @Inject(BAZA_MAIL_CUSTOMERIO_LINK_CMS_CONFIG) private readonly moduleConfig: BazaMailCustomerioLinkCmsConfig,
        private readonly fb: FormBuilder,
        private readonly vcr: ViewContainerRef,
        private readonly activatedRoute: ActivatedRoute,
        private readonly formModal: BazaFormLayoutModalService,
        private readonly dataAccess: BazaMailCustomerioLinksCmsDataAccess,
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly ngLoading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly customerIoBanner: BazaMailCustomerioLinkBannerService,
    ) {}

    public state: State = {
        config: this.config,
    };

    get resolvedData(): Observable<BazaMailCustomerioLinkCmsResolveData> {
        return this.activatedRoute.data.pipe(map((next) => next['data']));
    }

    get config(): BazaCrudConfig<Row> {
        return {
            title: this.i18n('title'),
            titleArguments: {
                tag: this.tag,
            },
            backRouterLink: {
                routerLink: ['/mail/customerio-links'],
            },
            items: {
                bottomLeft: [
                    {
                        type: BazaCrudItem.Legend,
                        options: {
                            items: [
                                {
                                    label: this.i18n('legend.noCustomerIo'),
                                    color: 'var(--error-color)',
                                },
                                {
                                    label: this.i18n('legend.hasCustomerIo'),
                                    color: 'var(--primary-color)',
                                },
                            ],
                        },
                    },
                ],
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('navigation.customerio'),
                    },
                    {
                        title: this.i18n('navigation.tags'),
                        routerLink: {
                            routerLink: ['/mail/customerio-links'],
                        },
                    },
                    {
                        title: this.tag,
                    },
                ],
            },
            data: {
                dataSource: {
                    array: () =>
                        this.dataAccess.listByTag(this.tag).pipe(
                            retryWhen(genericRetryStrategy()),
                            map((data) => ({
                                items: data.map((next) => ({
                                    name: next.template.name,
                                    title: next.template.title,
                                    description: next.template.description,
                                    customerioId: next.customerIoId,
                                    template: next.template,
                                })),
                            })),
                        ),
                },
                columns: [
                    {
                        field: 'title',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.title'),
                        ngStyle: (next) => ({
                            'font-weight': 500,
                            color: next.customerioId ? 'var(--primary-color)' : 'var(--error-color)',
                        }),
                    },
                    {
                        field: 'description',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.description'),
                    },
                    {
                        field: 'name',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.name'),
                        ngStyle: () => ({
                            'font-family': 'monospace',
                        }),
                    },
                    {
                        field: 'customerioId',
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => entity.customerioId || 'N/A',
                        },
                        width: 160,
                        title: this.i18n('columns.customerioId'),
                    },
                ],
                actions: [
                    {
                        title: this.i18n('actions.documentation'),
                        action: (row) => this.documentation(row.template),
                        icon: 'file',
                    },
                    {
                        title: this.i18n('actions.link'),
                        // eslint-disable-next-line security/detect-non-literal-fs-filename
                        action: (row) => this.link(row),
                        icon: 'edit',
                    },
                    {
                        title: this.i18n('actions.unlink.title'),
                        // eslint-disable-next-line security/detect-non-literal-fs-filename
                        action: (row) => this.unlink(row),
                        icon: 'delete',
                        visible: (row) => this.moduleConfig.withCustomerIoLinkCmsUnlinkFeature && !!row.customerioId,
                    },
                ],
            },
        };
    }

    get tag(): string {
        return this.activatedRoute.snapshot.params['tag'];
    }

    i18n(key: string): string {
        return `baza.mail.customerioLink.components.templates.${key}`;
    }

    link(row: Row): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<Row, LinkFormValue, unknown>({
            title: this.i18n('modals.link.title'),
            titleTransaleArgs: {
                name: row.title,
            },
            formGroup: this.fb.group({
                customerIoId: [row.customerioId, [Validators.required, moreThanZeroValidator, intValidator]],
            }),
            formBuilder: {
                id: BAZA_CORE_MAIL_CUSTOMERIO_LINKS_CMS_LINK_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Number,
                            formControlName: 'customerIoId',
                            label: this.i18n('modals.link.fields.customerIoId'),
                            required: true,
                        },
                    ],
                },
            },
            onSubmit: (skip, formValue) =>
                this.dataAccess
                    .set({
                        templateName: row.name,
                        customerTemplateId: formValue.customerIoId,
                    })
                    .pipe(
                        tap(() => this.crud.refresh()),
                        tap(() => this.customerIoBanner.updateStatus().subscribe()),
                    ),
        });
    }

    unlink(row: Row): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.unlink.message'),
            messageArgs: {
                name: row.name,
            },
            confirm: () => {
                const loading = this.ngLoading.addLoading();

                return this.dataAccess
                    .unset({
                        templateName: row.template.name,
                    })
                    .pipe(
                        catchError((err) => {
                            this.ngMessages.bazaError(err);

                            return throwError(err);
                        }),
                        tap(() =>
                            this.ngMessages.success({
                                message: this.i18n('actions.unlink.success'),
                                translateParams: {
                                    name: row.name,
                                },
                                translate: true,
                            }),
                        ),
                        finalize(() => loading.complete()),
                        tap(() => this.crud.refresh()),
                        tap(() => this.customerIoBanner.updateStatus().subscribe()),
                    )
                    .toPromise();
            },
        });
    }

    documentation(mailTemplate: BazaMailTemplate): void {
        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaMailCustomerioLinkCmsTemplateDocComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], this.vcr.injector);

        const component = factory.create(injector);

        component.instance.mailTemplate = mailTemplate;

        this.vcr.insert(component.hostView);

        setTimeout(() => {
            component.instance.nzModalRef = this.nzModal.create({
                nzContent: component.instance.nzModal,
                nzFooter: null,
                nzWidth: '80vw',
            });
        });
    }
}
