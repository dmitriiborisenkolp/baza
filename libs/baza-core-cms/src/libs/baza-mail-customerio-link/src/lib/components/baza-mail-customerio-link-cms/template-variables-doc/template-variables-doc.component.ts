import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';

@Component({
    selector: 'baza-mail-customerio-links-cms-template-variables-doc',
    templateUrl: './template-variables-doc.component.html',
    styleUrls: ['./template-variables-doc.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaMailCustomerioLinkCmsTemplateVariablesDocComponent {
    @Input() variables: Array<BazaMailTemplateVariable>;
}
