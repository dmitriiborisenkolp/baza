import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaTableFieldType } from '../../../../../baza-crud/src';
import { ActivatedRoute } from '@angular/router';
import { BazaMailCustomerioLinkTagsCmsResolveData } from './baza-mail-customerio-link-tags-cms.resolve';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

interface Row {
    name: string;
}

interface State {
    config: BazaCrudConfig<Row>;
}

@Component({
    templateUrl: './baza-mail-customerio-link-tags-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaMailCustomerioLinkTagsCmsComponent {
    public crud: BazaCrudComponent<Row>;

    public state: State = {
        config: this.config,
    };

    constructor(private readonly activatedRoute: ActivatedRoute) {}

    get config(): BazaCrudConfig<Row> {
        return {
            title: this.i18n('title'),
            navigation: {
                nodes: [
                    {
                        title: this.i18n('navigation.customerio'),
                    },
                    {
                        title: this.i18n('navigation.tags'),
                    },
                ],
            },
            data: {
                dataSource: {
                    array: () =>
                        this.resolvedData.pipe(
                            take(1),
                            map((data) => ({
                                items: data.tags.map((name) => ({
                                    name,
                                })),
                            })),
                        ),
                },
                columns: [
                    {
                        field: 'name',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.tag'),
                    },
                    {
                        field: 'templates',
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (next) => ({
                                routerLink: ['/mail/customerio-links', next.name],
                            }),
                            format: () => this.i18n('columns.templates'),
                            translate: true,
                            icon: 'right',
                        },
                        title: '',
                        width: 120,
                    },
                ],
            },
        };
    }

    get resolvedData(): Observable<BazaMailCustomerioLinkTagsCmsResolveData> {
        return this.activatedRoute.data.pipe(map((next) => next['data']));
    }

    i18n(key: string): string {
        return `baza.mail.customerioLink.components.tags.${key}`;
    }
}
