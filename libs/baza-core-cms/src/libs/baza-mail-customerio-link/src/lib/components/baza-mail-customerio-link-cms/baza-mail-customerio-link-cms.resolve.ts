import { Injectable } from '@angular/core';
import { BazaMailCustomerioLinkDto } from '@scaliolabs/baza-core-shared';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { BazaMailCustomerioLinksCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    links: Array<BazaMailCustomerioLinkDto>;
}

export { ResolveData as BazaMailCustomerioLinkCmsResolveData };

@Injectable()
export class BazaMailCustomerioLinkCmsResolve implements Resolve<ResolveData> {
    constructor(private readonly dataAccess: BazaMailCustomerioLinksCmsDataAccess) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> {
        const observables: [Observable<Array<BazaMailCustomerioLinkDto>>] = [this.dataAccess.listByTag(route.params['tag'])];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([links]) => ({
                links,
            })),
        );
    }
}
