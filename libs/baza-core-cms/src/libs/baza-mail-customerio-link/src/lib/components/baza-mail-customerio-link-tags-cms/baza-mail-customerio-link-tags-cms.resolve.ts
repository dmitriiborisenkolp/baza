import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { BazaMailDataAccess } from '@scaliolabs/baza-core-data-access';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    tags: Array<string>;
}

export { ResolveData as BazaMailCustomerioLinkTagsCmsResolveData };

@Injectable()
export class BazaMailCustomerioLinkTagsCmsResolve implements Resolve<ResolveData> {
    constructor(private readonly dataAccess: BazaMailDataAccess) {}

    resolve(): Observable<ResolveData> {
        const observables: [Observable<Array<string>>] = [this.dataAccess.mailTemplatesTags()];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([tags]) => ({
                tags,
            })),
        );
    }
}
