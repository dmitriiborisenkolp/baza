import { InjectionToken } from '@angular/core';

export interface BazaMailCustomerioLinkCmsConfig {
    withCustomerIoLinkCmsFeature: boolean;
    withCustomerIoLinkCmsUnlinkFeature: boolean;
}

export const BAZA_MAIL_CUSTOMERIO_LINK_CMS_CONFIG = new InjectionToken('BAZA_MAIL_CUSTOMERIO_LINK_CMS_CONFIG');
