import { Injectable } from '@angular/core';

@Injectable()
export class BazaAccountCmsConfig {
    withProfileImagesFeature: boolean;
    withResetAllPasswordFeature: boolean;
    withDeviceTokenFeature: boolean;
}
