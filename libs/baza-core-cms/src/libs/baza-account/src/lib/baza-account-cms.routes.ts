import { Routes } from '@angular/router';
import { BazaAccountComponent } from './components/baza-account/baza-account.component';

export const bazaAccountCmsRoutes: Routes = [
    {
        path: 'accounts',
        component: BazaAccountComponent,
    },
];
