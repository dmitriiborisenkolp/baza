import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaAccountComponent } from './components/baza-account/baza-account.component';
import { BazaPasswordDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaAccountCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaAclDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaAccountCmsConfig } from './baza-account-cms.config';
import { BazaAccountSearchModalComponent } from './components/baza-account-search-modal/baza-account-search-modal.component';
import { BazaAccountSearchModalService } from './components/baza-account-search-modal/baza-account-search-modal.service';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { BazaCmsLayoutModule } from '../../../../cms/layout/src';
import { BazaAuthNgModule } from '@scaliolabs/baza-core-ng';

interface ForRootOptions {
    deps?: Array<any>;
    useFactory: () => BazaAccountCmsConfig;
}

export { ForRootOptions as BazaAccountCmsModuleForRootOptions };

@NgModule({
    imports: [
        CommonModule,
        BazaAccountCmsDataAccessModule,
        BazaPasswordDataAccessModule,
        BazaAclDataAccessModule,
        BazaCmsLayoutModule,
        NzModalModule,
        BazaAuthNgModule,
    ],
    declarations: [BazaAccountComponent, BazaAccountSearchModalComponent],
    providers: [BazaAccountSearchModalService],
})
export class BazaAccountCmsModule {
    static forRoot(options: ForRootOptions): ModuleWithProviders<BazaAccountCmsModule> {
        return {
            ngModule: BazaAccountCmsModule,
            providers: [
                {
                    provide: BazaAccountCmsConfig,
                    deps: options.deps,
                    useFactory: options.useFactory,
                },
            ],
        };
    }
}
