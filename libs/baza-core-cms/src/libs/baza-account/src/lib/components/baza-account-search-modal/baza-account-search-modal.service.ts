import { ComponentFactoryResolver, Injectable, ReflectiveInjector, ViewContainerRef } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BazaAccountSearchModalComponent } from './baza-account-search-modal.component';
import { TranslateService } from '@ngx-translate/core';
import { AccountDto } from '@scaliolabs/baza-core-shared';
import { take } from 'rxjs/operators';

interface SearchModalRequest {
    vcr: ViewContainerRef,
    selected?: AccountDto;
}

interface SearchModalResponse {
    selected?: AccountDto;
}

@Injectable()
export class BazaAccountSearchModalService {
    constructor(
        private readonly translate: TranslateService,
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    private i18n(input: string): string {
        return this.translate.instant(`baza.formBuilder.components.bazaFieldAccount.searchModal.${input}`);
    }

    open(request: SearchModalRequest): Promise<SearchModalResponse> {
        return new Promise<SearchModalResponse>((resolve) => {
            const factory = this.componentFactoryResolver.resolveComponentFactory(BazaAccountSearchModalComponent);
            const injector = ReflectiveInjector.fromResolvedProviders([], request.vcr.injector);

            const component = factory.create(injector);

            request.vcr.insert(component.hostView);

            setTimeout(() => {
                component.instance.state.nzModalRef = this.nzModal.create({
                    nzTitle: null,
                    nzOkText: null,
                    nzCancelText: this.i18n('cancel'),
                    nzContent: component.instance.nzModal,
                    nzClosable: false,
                    nzOnOk: () => component.instance.submit(),
                    nzWidth: 960,
                });

                component.instance.state.nzModalRef.afterClose.pipe(
                    take(1),
                ).subscribe((response) => {
                    resolve(response?.selected);
                })
            });
        });
    }
}
