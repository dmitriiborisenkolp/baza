import { ChangeDetectionStrategy, Component, TemplateRef, ViewChild } from '@angular/core';
import { AccountDto } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { BazaCrudConfig, BazaCrudItem, BazaTableFieldType } from '../../../../../baza-crud/src';

export const BAZA_CORE_ACCOUNT_SEARCH_MODAL_CMS_ID = 'baza-core-account-search-modal-cms-id';

export const MAX_ACCOUNT_IN_OPTIONS = 10;

interface State {
    config: BazaCrudConfig<AccountDto>;
    nzModalRef?: NzModalRef;
}

@Component({
    templateUrl: './baza-account-search-modal.component.html',
    styleUrls: ['./baza-account-search-modal.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaAccountSearchModalComponent {
    @ViewChild('nzModal', { static: true }) nzModal: TemplateRef<any>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(private readonly accountCmsEndpoint: BazaAccountCmsDataAccess) {}

    submit(selected?: AccountDto): void {
        this.state.nzModalRef.close({
            selected,
        });
    }

    i18n(input: string): string {
        return `baza.account.components.bazaAccount.${input}`;
    }

    get crudConfig(): BazaCrudConfig<AccountDto> {
        return {
            id: BAZA_CORE_ACCOUNT_SEARCH_MODAL_CMS_ID,
            title: this.i18n('title'),
            titleLevel: 4,
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Search,
                        options: {},
                    },
                ],
            },
            data: {
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('fields.id'),
                        width: 'id',
                        sortable: true,
                    },
                    {
                        field: 'email',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.email'),
                        width: 250,
                        sortable: true,
                    },
                    {
                        field: 'fullName',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.fullName'),
                        sortable: true,
                        width: 'auto',
                    },
                    {
                        field: 'dateCreated',
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        title: this.i18n('fields.dateCreated'),
                        sortable: true,
                    },
                    {
                        field: 'isEmailConfirmed',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        title: this.i18n('fields.isEmailConfirmed'),
                        sortable: true,
                    },
                ],
                dataSource: {
                    list: (request) => this.accountCmsEndpoint.listAccounts(request),
                    size: MAX_ACCOUNT_IN_OPTIONS,
                    sizes: [MAX_ACCOUNT_IN_OPTIONS],
                    nzShowSizeChanger: false,
                    nzShowTotal: false,
                    isRowClickable: true,
                    onRowClick: (account) => {
                        this.submit(account);
                    },
                },
            },
        };
    }
}
