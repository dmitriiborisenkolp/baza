import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
    AccountAcl,
    AccountCmsDto,
    AccountRole,
    BazaAclGetAclResponse,
    BazaAclSetAclResponse,
    CoreAcl,
} from '@scaliolabs/baza-core-shared';
import { BazaAclDataAccess } from '@scaliolabs/baza-core-data-access';
import { BazaAccountCmsDataAccess, BazaAccountCmsResetPasswordDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { distinctUntilChanged, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { of, Subject } from 'rxjs';
import { AuthValidatorsService, BazaAclNgService, BazaNgConfirmService, BazaNzButtonStyle, JwtService } from '@scaliolabs/baza-core-ng';
import { BazaAccountCmsConfig } from '../../baza-account-cms.config';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import {
    BazaCoreFormBuilderComponents,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormControl,
    BazaFormLayoutModalService,
    BazaFormLayoutService,
} from '../../../../../baza-form-builder/src';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudItem, BazaTableFieldType } from '../../../../../baza-crud/src';
import { bazaAccountCmsDefineMetadata } from '../../inject-metadata/baza-cms-inject-metadata.util';

const DEFAULT_ACCOUNT_ROLE = AccountRole.User;

export const BAZA_CORE_ACCOUNT_CMS_ID = 'baza-core-account-cms-id';
export const BAZA_CORE_ACCOUNT_CMS_USER_FORM_ID = 'baza-core-account-cms-user-form-id';
export const BAZA_CORE_ACCOUNT_CMS_ADMIN_FORM_ID = 'baza-core-account-cms-admin-form-id';
export const BAZA_CORE_ACCOUNT_CMS_RESET_PASSWORD_FORM_ID = 'baza-core-account-cms-reset-password-form-id';
export const BAZA_CORE_ACCOUNT_CMS_CHANGE_EMAIL_FORM_ID = 'baza-core-account-cms-change-email-form-id';
export const BAZA_CORE_ACCOUNT_CMS_RESET_PASSWORD_ALL_USERS_FORM_ID = 'baza-core-account-cms-reset-password-all-users-form-id';
export const BAZA_CORE_ACCOUNT_CMS_SET_ACL_FORM_ID = 'baza-core-account-cms-set-acl-form-id';

interface AddAdministratorFormValue {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    profileImages: string;
    acl: Array<string>;
}

interface UpdateAccountFormValue {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    phone: string;
    profileImages: string;
}

interface AddUserFormValue {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    profileImages: string;
}

interface ResetPasswordFormValue {
    subject: string;
    message: string;
    attemptToSignInErrorMessage: string;
}

interface ChangeEmailFormValue {
    email: string;
}

interface SetAclFormValue {
    acl: Array<string>;
}

interface State {
    role: AccountRole;
    config: BazaCrudConfig<AccountCmsDto>;
}

@Component({
    templateUrl: './baza-account.component.html',
    styleUrls: ['./baza-account.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaAccountComponent<T = any> implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public search: FormControl = new FormControl();
    public crud: BazaCrudComponent<AccountCmsDto>;

    public state: State = {
        role: DEFAULT_ACCOUNT_ROLE,
        config: this.crudConfig,
    };

    constructor(
        private readonly moduleConfig: BazaAccountCmsConfig,
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly translate: TranslateService,
        private readonly accountCmsEndpoint: BazaAccountCmsDataAccess,
        private readonly accountCmsResetPasswordEndpoint: BazaAccountCmsResetPasswordDataAccess,
        private readonly jwtService: JwtService,
        private readonly formLayout: BazaFormLayoutService,
        private readonly formModal: BazaFormLayoutModalService,
        private readonly authValidators: AuthValidatorsService,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly aclNgEndpoint: BazaAclDataAccess,
        private readonly aclNgService: BazaAclNgService,
    ) {}

    ngOnInit(): void {
        this.activatedRoute.queryParams
            .pipe(
                map((qp) => qp['role'] || AccountRole.User),
                distinctUntilChanged(),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((role: AccountRole) => {
                this.state = {
                    ...this.state,
                    role,
                };

                if (this.crud) {
                    this.crud.refresh();
                }

                this.cdr.markForCheck();
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.account.components.bazaAccount.${input}`;
    }

    get crudConfig(): BazaCrudConfig<AccountCmsDto> {
        const config: BazaCrudConfig<AccountCmsDto> = {
            id: BAZA_CORE_ACCOUNT_CMS_ID,
            title: this.i18n('title'),
            items: {
                topLeft: [
                    {
                        type: BazaCrudItem.Tabs,
                        options: {
                            tabs: [
                                {
                                    title: this.i18n('users'),
                                    routerLink: {
                                        commands: ['.'],
                                        navigationExtras: {
                                            relativeTo: this.activatedRoute,
                                        },
                                    },
                                    routerLinkExact: true,
                                },
                                {
                                    title: this.i18n('admins'),
                                    routerLink: {
                                        commands: ['.'],
                                        navigationExtras: {
                                            relativeTo: this.activatedRoute,
                                            queryParams: {
                                                role: AccountRole.Admin,
                                            },
                                            queryParamsHandling: '',
                                        },
                                    },
                                },
                            ],
                        },
                    },
                ],
                topRight: [
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            control: this.search,
                        },
                    },
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('actions.exportToCsv'),
                            icon: 'download',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (request) =>
                                this.accountCmsEndpoint.exportToCsv({
                                    ...request,
                                    listRequest: { ...request.listRequest, roles: [this.state.role] },
                                    fields: [
                                        'id',
                                        'email',
                                        'fullName',
                                        'dateCreated',
                                        'lastSignIn',
                                        'signedUpWithClient',
                                        'isEmailConfirmed',
                                        'role',
                                    ].map((field) => ({
                                        field: field as any,
                                        title: this.translate.instant(this.i18n(`fields.${field}`)),
                                    })),
                                }),
                            fileNameGenerator: () => `${this.state.role}-accounts-${moment(new Date()).format('MM-DD-YYYY')}`,
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        visible: this.activatedRoute.queryParams.pipe(
                            map((qp) => (qp['role'] || '') === AccountRole.Admin),
                            takeUntil(this.ngOnDestroy$),
                        ),
                        acl: [AccountAcl.AccountsAdminCreate],
                        options: {
                            title: this.i18n('newAdministrator'),
                            type: BazaNzButtonStyle.Default,
                            callback: () => this.addAdministrator(),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        visible: this.activatedRoute.queryParams.pipe(
                            map((qp) => (qp['role'] || AccountRole.User) === AccountRole.User),
                            takeUntil(this.ngOnDestroy$),
                        ),
                        acl: [AccountAcl.AccountsCreate],
                        options: {
                            title: this.i18n('newUser'),
                            type: BazaNzButtonStyle.Default,
                            callback: () => this.addUser(),
                        },
                    },
                ],
                bottomLeft: [
                    {
                        type: BazaCrudItem.Button,
                        visible: this.moduleConfig.withResetAllPasswordFeature
                            ? this.activatedRoute.queryParams.pipe(
                                  map((qp) => (qp['role'] || AccountRole.User) === AccountRole.User),
                                  takeUntil(this.ngOnDestroy$),
                              )
                            : of(this.moduleConfig.withResetAllPasswordFeature),
                        acl: [AccountAcl.AccountsResetPassword],
                        options: {
                            title: this.i18n('resetPasswordForAllUsers'),
                            type: BazaNzButtonStyle.Link,
                            isDanger: true,
                            callback: () => this.resetPasswordForAllUsers(),
                        },
                    },
                ],
            },
            data: {
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('fields.id'),
                        width: 'id',
                        sortable: true,
                    },
                    {
                        field: 'email',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.email'),
                        width: 250,
                        sortable: true,
                    },
                    {
                        field: 'fullName',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.fullName'),
                        sortable: true,
                        width: 'auto',
                    },
                    {
                        field: 'dateCreated',
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        title: this.i18n('fields.dateCreated'),
                        sortable: true,
                    },
                    {
                        field: 'lastSignIn',
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        title: this.i18n('fields.lastSignIn'),
                        sortable: true,
                    },
                    {
                        field: 'signedUpWithClient',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.signedUpWithClient'),
                        sortable: true,
                    },
                    {
                        field: 'isEmailConfirmed',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        title: this.i18n('fields.isEmailConfirmed'),
                        sortable: true,
                    },
                ],
                actions: [
                    {
                        acl: [AccountAcl.AccountsUpdate],
                        icon: 'edit',
                        title: this.i18n('actions.updateAccount'),
                        action: (entity) => this.updateAccount(entity),
                    },
                    {
                        acl: [AccountAcl.AccountsUpdate],
                        icon: 'check',
                        title: this.i18n('actions.confirmAccount'),
                        visible: (entity) =>
                            !entity.isEmailConfirmed && !entity.isManagedUserAccount && entity.id !== this.jwtService.jwtPayload.accountId,
                        action: (entity) =>
                            this.accountCmsEndpoint
                                .confirmEmailAccountById({
                                    id: entity.id,
                                })
                                .pipe(tap(() => this.crud.refresh()))
                                .subscribe(),
                    },
                    {
                        acl: [AccountAcl.AccountsUpdate],
                        icon: 'check',
                        title: this.i18n('actions.unConfirmAccount'),
                        visible: (entity) =>
                            entity.isEmailConfirmed && !entity.isManagedUserAccount && entity.id !== this.jwtService.jwtPayload.accountId,
                        action: (entity) =>
                            this.accountCmsEndpoint
                                .confirmEmailAccountById({
                                    id: entity.id,
                                    confirmed: false,
                                })
                                .pipe(tap(() => this.crud.refresh()))
                                .subscribe(),
                    },
                    {
                        acl: [AccountAcl.AccountsDelete],
                        icon: 'delete',
                        title: this.i18n('actions.delete'),
                        visible: (entity) =>
                            of(
                                entity.id !== this.jwtService.jwtPayload.accountId &&
                                    !entity.isManagedUserAccount &&
                                    entity.role === AccountRole.Admin,
                            ),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.ngConfirm.open({
                                message: this.i18n('delete.message'),
                                confirm: () =>
                                    this.accountCmsEndpoint
                                        .deleteAccountById({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() => this.crud.refresh()),
                                            takeUntil(this.ngOnDestroy$),
                                        )
                                        .toPromise(),
                            });
                        },
                    },
                    {
                        acl: [AccountAcl.AccountsDeactivate],
                        icon: 'delete',
                        title: this.i18n('actions.deactivate'),
                        visible: (entity) =>
                            of(
                                entity.id !== this.jwtService.jwtPayload.accountId &&
                                    !entity.isManagedUserAccount &&
                                    entity.role !== AccountRole.Admin,
                            ),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.ngConfirm.open({
                                message: this.i18n('deactivate.message'),
                                confirm: () =>
                                    this.accountCmsEndpoint
                                        .deactivateAccount({
                                            email: entity.email,
                                        })
                                        .pipe(
                                            tap(() => this.crud.refresh()),
                                            takeUntil(this.ngOnDestroy$),
                                        )
                                        .toPromise(),
                            });
                        },
                    },
                    {
                        acl: [AccountAcl.AccountsAssign],
                        icon: 'plus-circle',
                        title: this.i18n('actions.assign'),
                        visible: (entity) =>
                            of(
                                !entity.isManagedUserAccount &&
                                    entity.id !== this.jwtService.jwtPayload.accountId &&
                                    entity.role !== AccountRole.Admin,
                            ),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.ngConfirm.open({
                                message: this.i18n('assign.message'),
                                confirm: () =>
                                    this.accountCmsEndpoint
                                        .assignAdminAccess({
                                            email: entity.email,
                                        })
                                        .pipe(
                                            tap(() => this.crud.refresh()),
                                            takeUntil(this.ngOnDestroy$),
                                        )
                                        .toPromise(),
                            });
                        },
                    },
                    {
                        acl: [AccountAcl.AccountsRevoke],
                        icon: 'minus-circle',
                        title: this.i18n('actions.revoke'),
                        visible: (entity) =>
                            of(
                                !entity.isManagedUserAccount &&
                                    entity.id !== this.jwtService.jwtPayload.accountId &&
                                    entity.role === AccountRole.Admin &&
                                    (this.jwtService.jwtPayload.accountAcl.includes(CoreAcl.SystemRoot) || !entity.isSystemRoot),
                            ),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.ngConfirm.open({
                                message: this.i18n('revoke.message'),
                                confirm: () =>
                                    this.accountCmsEndpoint
                                        .revokeAdminAccess({
                                            email: entity.email,
                                        })
                                        .pipe(
                                            tap(() => this.crud.refresh()),
                                            takeUntil(this.ngOnDestroy$),
                                        )
                                        .toPromise(),
                            });
                        },
                    },
                    {
                        acl: [CoreAcl.BazaAcl, CoreAcl.BazaSetAcl],
                        icon: 'unordered-list',
                        title: this.i18n('actions.setAcl'),
                        visible: (entity) => of(entity.id !== this.jwtService.jwtPayload.accountId && entity.role === AccountRole.Admin),
                        action: (entity) => this.setAcl(entity),
                    },
                    {
                        acl: [AccountAcl.AccountsResetPassword],
                        icon: 'exception',
                        title: this.i18n('actions.resetPassword'),
                        visible: (entity) => of(!entity.isManagedUserAccount && entity.role === AccountRole.User),
                        action: (entity) => this.resetPassword(entity),
                    },
                    {
                        acl: [AccountAcl.AccountsChangeEmail],
                        icon: 'mail',
                        title: this.i18n('actions.changeEmail'),
                        visible: (entity) => of(!entity.isManagedUserAccount),
                        action: (entity) => this.changeEmail(entity),
                    },
                    {
                        acl: [AccountAcl.AccountsAuthSession],
                        icon: 'user-switch',
                        title: this.i18n('actions.sessions'),
                        action: (entity) => this.router.navigate(['/auth-sessions', entity.id]),
                    },
                ],
                dataSource: {
                    list: (request) =>
                        this.accountCmsEndpoint.listAccounts({
                            ...request,
                            roles: [this.state.role],
                        }),
                },
            },
        };

        if (this.moduleConfig.withDeviceTokenFeature) {
            config.data.actions.push({
                acl: [AccountAcl.AccountsDeviceTokens],
                icon: 'mobile',
                title: this.i18n('actions.deviceTokens'),
                action: (entity) => this.router.navigate(['/device-tokens', entity.id]),
            });
        }

        return config;
    }

    addAdministrator(): void {
        const formGroup = this.fb.group({
            email: ['', [...this.authValidators.emailValidators()]],
            password: ['', [...this.authValidators.signUpValidators()]],
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            acl: [[], [Validators.required]],
            profileImages: [],
        });

        const metadataConfig = bazaAccountCmsDefineMetadata().callback({
            fb: this.fb,
            form: formGroup,
            activatedRoute: this.activatedRoute,
            accountRole: AccountRole.Admin,
        });

        for (const fieldName of Object.keys(metadataConfig.formGroup)) {
            formGroup.addControl(fieldName, new FormControl(...metadataConfig.formGroup[`${fieldName}`]));
        }

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<AccountCmsDto, AddAdministratorFormValue>({
            formGroup,
            title: this.i18n('forms.addAdministrator.title'),
            formBuilder: this.adminFormBuilder(metadataConfig.formBuilder),
            onSubmit: (entity, formValue) => {
                const metadata: Record<string, unknown> = {};

                const metadataFormFields = metadataConfig.formBuilder.filter(
                    (fieldConfig) => !!(fieldConfig as any as BazaFormControl).formControlName,
                ) as Array<BazaFormControl>;

                for (const fieldConfig of metadataFormFields) {
                    metadata[fieldConfig.formControlName] = formValue[fieldConfig.formControlName];
                }

                return this.accountCmsEndpoint
                    .registerAdminAccount({
                        email: formValue.email,
                        password: formValue?.password || undefined,
                        firstName: formValue.firstName,
                        lastName: formValue.lastName,
                        profileImages: [formValue.profileImages].filter((p) => !!p),
                        metadata,
                    })
                    .pipe(
                        switchMap((response) => {
                            if (this.aclNgService.hasAccess([CoreAcl.BazaAcl, CoreAcl.BazaSetAcl])) {
                                return this.aclNgEndpoint
                                    .setAcl({
                                        accountId: response.id,
                                        acl: formValue.acl,
                                    })
                                    .pipe(map(() => response));
                            } else {
                                return of(response);
                            }
                        }),
                    );
            },
            afterSubmit: () => {
                this.crud.refresh();
            },
        });
    }

    addUser(): void {
        const formGroup = this.fb.group({
            email: ['', [...this.authValidators.emailValidators()]],
            password: ['', [...this.authValidators.signUpValidators()]],
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            profileImages: [],
        });

        const metadataConfig = bazaAccountCmsDefineMetadata().callback({
            fb: this.fb,
            form: formGroup,
            activatedRoute: this.activatedRoute,
            accountRole: AccountRole.User,
        });

        for (const fieldName of Object.keys(metadataConfig.formGroup)) {
            formGroup.addControl(fieldName, new FormControl(...metadataConfig.formGroup[fieldName]));
        }

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<AccountCmsDto, AddUserFormValue>({
            formGroup,
            title: this.i18n('forms.addUser.title'),
            formBuilder: this.userFormBuilder(metadataConfig.formBuilder),
            onSubmit: (entity, formValue) => {
                const metadata: Record<string, unknown> = {};

                const metadataFormFields = metadataConfig.formBuilder.filter(
                    (fieldConfig) => !!(fieldConfig as any as BazaFormControl).formControlName,
                ) as Array<BazaFormControl>;

                for (const fieldConfig of metadataFormFields) {
                    metadata[fieldConfig.formControlName] = formValue[fieldConfig.formControlName];
                }

                return this.accountCmsEndpoint.registerUserAccount({
                    email: formValue.email,
                    password: formValue?.password || undefined,
                    firstName: formValue.firstName,
                    lastName: formValue.lastName,
                    profileImages: [formValue.profileImages].filter((p) => !!p),
                    metadata,
                });
            },
            afterSubmit: () => {
                this.crud.refresh();
            },
        });
    }

    setAcl(account: AccountCmsDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaAclGetAclResponse, SetAclFormValue, BazaAclSetAclResponse>({
            title: this.i18n('forms.setAcl.title'),
            formGroup: this.fb.group({
                acl: [[], [Validators.required]],
            }),
            formBuilder: this.setAclFormBuilder(),
            fetch: () =>
                this.aclNgEndpoint.getAcl({
                    accountId: account.id,
                }),
            onSubmit: (entity, formValue) =>
                this.aclNgEndpoint.setAcl({
                    accountId: account.id,
                    acl: formValue.acl,
                }),
            afterSubmit: () => {
                this.crud.refresh();
            },
        });
    }

    updateAccount(account: AccountCmsDto): void {
        const formGroup = this.fb.group({
            email: ['', [...this.authValidators.emailValidators()]],
            password: ['', [...this.authValidators.signUpValidators()]],
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            phone: ['', []],
            profileImages: [],
        });

        const metadataConfig = bazaAccountCmsDefineMetadata().callback({
            account,
            fb: this.fb,
            form: formGroup,
            activatedRoute: this.activatedRoute,
            accountRole: account.role,
        });

        const metadataFormFields = metadataConfig.formBuilder.filter(
            (fieldConfig) => !!(fieldConfig as any as BazaFormControl).formControlName,
        ) as Array<BazaFormControl>;

        for (const fieldName of Object.keys(metadataConfig.formGroup)) {
            formGroup.addControl(fieldName, new FormControl(...metadataConfig.formGroup[fieldName]));
        }

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<AccountCmsDto, UpdateAccountFormValue>({
            formGroup,
            title: this.i18n('forms.updateAccount.title'),
            formBuilder: this.updateAccountFormBuilder(account, metadataConfig.formBuilder),
            fetch: () =>
                this.accountCmsEndpoint.getAccountById({
                    id: account.id,
                }),
            populate: (input, formGroup) => {
                const formValue = {
                    ...input,
                    profileImages: input.profileImages ? input.profileImages[0] : undefined,
                };

                for (const fieldConfig of metadataFormFields) {
                    formValue[fieldConfig.formControlName] = account.metadata[fieldConfig.formControlName];
                }

                formGroup.patchValue(formValue);
            },
            onSubmit: (entity, formValue) => {
                const metadata: Record<string, unknown> = {};

                for (const fieldConfig of metadataFormFields) {
                    metadata[fieldConfig.formControlName] = formValue[fieldConfig.formControlName];
                }

                return this.accountCmsEndpoint.updateAccount({
                    id: entity.id,
                    password: formValue.password ? formValue.password : undefined, // https://github.com/typestack/class-validator/issues/347
                    firstName: formValue.firstName,
                    lastName: formValue.lastName,
                    phone: formValue.phone,
                    profileImages: [formValue.profileImages].filter((p) => !!p),
                    metadata: {
                        ...account.metadata,
                        ...metadata,
                    },
                });
            },
            afterSubmit: () => {
                this.crud.refresh();
            },
        });
    }

    resetPassword(account: AccountCmsDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<AccountCmsDto, ResetPasswordFormValue, void>({
            title: this.i18n('forms.resetPassword.title'),
            formGroup: this.fb.group({
                subject: ['', [Validators.required]],
                message: ['', [Validators.required]],
                attemptToSignInErrorMessage: ['', [Validators.required]],
            }),
            formBuilder: {
                id: BAZA_CORE_ACCOUNT_CMS_RESET_PASSWORD_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'subject',
                            label: this.i18n('forms.resetPassword.fields.subject'),
                            required: true,
                        },
                        {
                            type: BazaFormBuilderControlType.BazaHtml,
                            formControlName: 'message',
                            label: this.i18n('forms.resetPassword.fields.message'),
                            required: true,
                        },
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'attemptToSignInErrorMessage',
                            label: this.i18n('forms.resetPasswordForAllUsers.fields.attemptToSignInErrorMessage'),
                            required: true,
                        },
                    ],
                },
            },
            onSubmit: (entity, formValue) =>
                this.accountCmsResetPasswordEndpoint.requestResetPassword({
                    email: account.email,
                    subject: formValue.subject,
                    message: formValue.message,
                    attemptToSignInErrorMessage: formValue.attemptToSignInErrorMessage,
                }),
            afterSubmit: () => {
                this.crud.refresh();
            },
        });
    }

    changeEmail(account: AccountCmsDto): void {
        const formGroup = this.fb.group({
            fullName: [account.fullName],
            currentEmail: [account.email],
            email: ['', [Validators.required]],
        });

        formGroup.get('fullName').disable();
        formGroup.get('currentEmail').disable();

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<AccountCmsDto, ChangeEmailFormValue>({
            formGroup,
            title: this.i18n('forms.changeEmail.title'),
            formBuilder: {
                id: BAZA_CORE_ACCOUNT_CMS_CHANGE_EMAIL_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'fullName',
                            label: this.i18n('forms.changeEmail.fields.fullName'),
                            required: false,
                            disabled: true,
                        },
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'currentEmail',
                            label: this.i18n('forms.changeEmail.fields.currentEmail'),
                            required: false,
                            disabled: true,
                        },
                        {
                            type: BazaFormBuilderControlType.Email,
                            formControlName: 'email',
                            label: this.i18n('forms.changeEmail.fields.email'),
                            required: true,
                        },
                    ],
                },
            },
            onSubmit: (entity, formValue) =>
                this.accountCmsEndpoint.changeEmail({
                    id: account.id,
                    email: formValue.email,
                }),
            afterSubmit: () => {
                this.crud.refresh();
            },
        });
    }

    resetPasswordForAllUsers(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<AccountCmsDto, ResetPasswordFormValue, void>({
            title: this.i18n('forms.resetPasswordForAllUsers.title'),
            formGroup: this.fb.group({
                subject: ['', [Validators.required]],
                message: ['', [Validators.required]],
                attemptToSignInErrorMessage: ['', [Validators.required]],
            }),
            formBuilder: {
                id: BAZA_CORE_ACCOUNT_CMS_RESET_PASSWORD_ALL_USERS_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'subject',
                            label: this.i18n('forms.resetPasswordForAllUsers.fields.subject'),
                            required: true,
                        },
                        {
                            type: BazaFormBuilderControlType.BazaHtml,
                            formControlName: 'message',
                            label: this.i18n('forms.resetPasswordForAllUsers.fields.message'),
                            required: true,
                        },
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'attemptToSignInErrorMessage',
                            label: this.i18n('forms.resetPasswordForAllUsers.fields.attemptToSignInErrorMessage'),
                            required: true,
                        },
                    ],
                },
            },
            onSubmit: (entity, formValue) =>
                this.accountCmsResetPasswordEndpoint.requestResetPasswordAllAccounts({
                    subject: formValue.subject,
                    message: formValue.message,
                    attemptToSignInErrorMessage: formValue.attemptToSignInErrorMessage,
                }),
            afterSubmit: () => {
                this.crud.refresh();
            },
        });
    }

    private adminFormBuilder(metadataFormFields: Array<BazaCoreFormBuilderComponents | any>): BazaFormBuilder {
        return {
            id: BAZA_CORE_ACCOUNT_CMS_ADMIN_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('forms.addAdministrator.common'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.Text,
                                    formControlName: 'firstName',
                                    label: this.i18n('forms.fields.firstName'),
                                    required: true,
                                    autoFocus: true,
                                },
                                {
                                    type: BazaFormBuilderControlType.Text,
                                    formControlName: 'lastName',
                                    label: this.i18n('forms.fields.lastName'),
                                    required: true,
                                },
                                {
                                    type: BazaFormBuilderControlType.Email,
                                    formControlName: 'email',
                                    label: this.i18n('forms.fields.email'),
                                    required: true,
                                },
                                {
                                    type: BazaFormBuilderControlType.Password,
                                    formControlName: 'password',
                                    label: this.i18n('forms.fields.password'),
                                    placeholder: this.i18n('forms.addAdministrator.placeholders.password'),
                                    required: false,
                                },
                                {
                                    type: BazaFormBuilderControlType.BazaUpload,
                                    formControlName: 'profileImages',
                                    label: this.i18n('forms.fields.profileImages'),
                                    visible: of(this.moduleConfig.withProfileImagesFeature),
                                },
                                ...metadataFormFields,
                            ],
                        },
                    },
                    {
                        title: this.i18n('forms.addAdministrator.acl'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.BazaAcl,
                                    formControlName: 'acl',
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }

    private userFormBuilder(metadataFormFields: Array<BazaCoreFormBuilderComponents | any>): BazaFormBuilder {
        return {
            id: BAZA_CORE_ACCOUNT_CMS_USER_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('forms.addUser.common'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.Text,
                                    formControlName: 'firstName',
                                    label: this.i18n('forms.fields.firstName'),
                                    required: true,
                                    autoFocus: true,
                                },
                                {
                                    type: BazaFormBuilderControlType.Text,
                                    formControlName: 'lastName',
                                    label: this.i18n('forms.fields.lastName'),
                                    required: true,
                                },
                                {
                                    type: BazaFormBuilderControlType.Email,
                                    formControlName: 'email',
                                    label: this.i18n('forms.fields.email'),
                                    required: true,
                                },
                                {
                                    type: BazaFormBuilderControlType.Password,
                                    formControlName: 'password',
                                    label: this.i18n('forms.fields.password'),
                                    placeholder: this.i18n('forms.addUser.placeholders.password'),
                                    required: false,
                                },
                                {
                                    type: BazaFormBuilderControlType.BazaUpload,
                                    formControlName: 'profileImages',
                                    label: this.i18n('forms.fields.profileImages'),
                                    visible: of(this.moduleConfig.withProfileImagesFeature),
                                },
                                ...metadataFormFields,
                            ],
                        },
                    },
                ],
            },
        };
    }

    private setAclFormBuilder(): BazaFormBuilder {
        return {
            id: BAZA_CORE_ACCOUNT_CMS_SET_ACL_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.BazaAcl,
                        formControlName: 'acl',
                    },
                ],
            },
        };
    }

    private updateAccountFormBuilder(
        entity: AccountCmsDto,
        metadataFormFields: Array<BazaCoreFormBuilderComponents | any>,
    ): BazaFormBuilder {
        return {
            id: entity.role === AccountRole.Admin ? BAZA_CORE_ACCOUNT_CMS_ADMIN_FORM_ID : BAZA_CORE_ACCOUNT_CMS_USER_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'firstName',
                        label: this.i18n('forms.fields.firstName'),
                        required: true,
                        autoFocus: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'lastName',
                        label: this.i18n('forms.fields.lastName'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'phone',
                        label: this.i18n('forms.fields.phone'),
                    },
                    {
                        type: BazaFormBuilderControlType.Email,
                        formControlName: 'email',
                        label: this.i18n('forms.fields.email'),
                        required: true,
                        disabled: true,
                        readonly: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Password,
                        formControlName: 'password',
                        label: this.i18n('forms.fields.password'),
                        placeholder: this.i18n('forms.updateAccount.placeholders.password'),
                        required: false,
                        visible: !entity.isManagedUserAccount,
                    },
                    {
                        type: BazaFormBuilderControlType.BazaUpload,
                        formControlName: 'profileImages',
                        label: this.i18n('forms.fields.profileImages'),
                        visible: of(this.moduleConfig.withProfileImagesFeature),
                    },
                    ...metadataFormFields,
                ],
            },
        };
    }
}
