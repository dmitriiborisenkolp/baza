import { ActivatedRoute } from '@angular/router';
import { BazaCoreFormBuilderComponents } from '../../../../baza-form-builder/src';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccountCmsDto, AccountRole } from '@scaliolabs/baza-core-shared';

/**
 * Payload which may be useful to prepare additional metadata fields
 */
interface InjectFormBuilderPayload {
    fb: FormBuilder;
    form: FormGroup;
    activatedRoute: ActivatedRoute;
    account?: AccountCmsDto;
    accountRole: AccountRole;
}

/**
 * Form Builder & Form Group definitions generated with InjectFormBuilderPayload
 */
interface InjectFormBuilder<T = BazaCoreFormBuilderComponents> {
    formBuilder: Array<T>;
    formGroup: {
        [key: string]: any;
    };
}

/**
 * Callback
 */
export type BazaAccountCmsMetadataFunc<T = BazaCoreFormBuilderComponents> = (
    payload: InjectFormBuilderPayload,
) => InjectFormBuilder<T>;

/**
 * Configuration for Baza Account Metadata Field - it's not exported, but can be
 * fetched with bazaAccountCmsDefineMetadata helper
 */
const BAZA_CMS_INJECT_METADATA_CONFIG: {
    callback: BazaAccountCmsMetadataFunc;
} = {
    callback: () => ({
        formBuilder: [],
        formGroup: {},
    }),
};

/**
 * Set up Baza Account CMS Metadata Fields.
 * Returns current configuration.
 *
 * @param callback
 */
export function bazaAccountCmsDefineMetadata(callback?: BazaAccountCmsMetadataFunc) {
    if (callback) {
        BAZA_CMS_INJECT_METADATA_CONFIG.callback = callback;
    }

    return BAZA_CMS_INJECT_METADATA_CONFIG;
}
