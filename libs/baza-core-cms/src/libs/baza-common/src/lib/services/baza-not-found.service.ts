import { Injectable } from '@angular/core';
import { Router, UrlTree } from '@angular/router';

/**
 * Not Found Page service for Baza CMS
 */
@Injectable({
    providedIn: 'root',
})
export class BazaNotFoundService {
    constructor(private readonly router: Router) {}

    /**
     * Navigates to 404 Not Found Page
     */
    navigateToNotFound(): void {
        this.router.navigate(['/not-found'], {
            skipLocationChange: true,
        });
    }

    /**
     * Returns UrlTree for 404 Not Found Page
     * Use it for Can-Activate Guards
     */
    urlTreeToNotFound(): UrlTree {
        return this.router.createUrlTree([
            '/not-found',
            {
                skipLocationChange: true,
            },
        ]);
    }
}
