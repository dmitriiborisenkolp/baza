import { Routes } from '@angular/router';
import { BazaMaintenanceCmsComponent } from './components/baza-maintenance-cms/baza-maintenance-cms.component';
import { BazaMaintenanceCmsResolve } from './components/baza-maintenance-cms/baza-maintenance-cms.resolve';

export const bazaMaintenanceCmsRoutes: Routes = [
    {
        path: 'maintenance',
        component: BazaMaintenanceCmsComponent,
        resolve: {
            status: BazaMaintenanceCmsResolve,
        },
    },
];
