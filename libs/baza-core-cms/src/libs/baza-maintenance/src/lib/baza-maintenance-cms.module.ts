import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaMaintenanceCmsComponent } from './components/baza-maintenance-cms/baza-maintenance-cms.component';
import { BazaMaintenanceCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaMaintenanceCmsResolve } from './components/baza-maintenance-cms/baza-maintenance-cms.resolve';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { TranslateModule } from '@ngx-translate/core';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { ReactiveFormsModule } from '@angular/forms';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzButtonModule } from 'ng-zorro-antd/button';

@NgModule({
    imports: [
        CommonModule,
        BazaMaintenanceCmsDataAccessModule,
        NzTypographyModule,
        NzTypographyModule,
        TranslateModule,
        NzSpaceModule,
        ReactiveFormsModule,
        NzSwitchModule,
        NzButtonModule,
    ],
    declarations: [BazaMaintenanceCmsComponent],
    providers: [BazaMaintenanceCmsResolve],
    exports: [BazaMaintenanceCmsComponent],
})
export class BazaMaintenanceCmsModule {}
