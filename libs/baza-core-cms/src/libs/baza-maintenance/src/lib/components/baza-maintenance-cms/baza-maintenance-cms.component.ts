import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BAZA_MAINTENANCE_RESOLVE_KEY, BazaMaintenanceResolveData } from './baza-maintenance-cms.resolve';
import { BazaMaintenanceDto, BazaMaintenanceMessageDto } from '@scaliolabs/baza-core-shared';
import { Observable, Subject } from 'rxjs';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { distinctUntilChanged, finalize, map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BazaMaintenanceCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { Application } from '@scaliolabs/baza-core-shared';
import { BazaFormBuilderControlType, BazaFormBuilderLayout, BazaFormLayoutModalService } from '../../../../../baza-form-builder/src';

export const BAZA_CORE_MAINTENANCE_EDIT_MESSAGE_FORM_ID = 'baza-core-maintenance-edit-message-form-id';

interface State {
    form: FormGroup;
    message: BazaMaintenanceMessageDto;
    status: BazaMaintenanceDto;
}

interface MessageFormValue {
    message: string;
}

@Component({
    templateUrl: './baza-maintenance-cms.component.html',
    styleUrls: ['./baza-maintenance-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [BazaLoadingService],
})
export class BazaMaintenanceCmsComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        status: this.resolvedData.status,
        message: this.resolvedData.message,
        form: this.fb.group({}),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly activatedRoute: ActivatedRoute,
        private readonly loading: BazaLoadingService,
        private readonly endpoint: BazaMaintenanceCmsDataAccess,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly formLayout: BazaFormLayoutModalService,
    ) {}

    ngOnInit(): void {
        this.state.status.scope.forEach((application) => {
            const control = new FormControl(!this.state.status.enabled.includes(application));

            this.state.form.addControl(application, control);

            control.valueChanges
                .pipe(
                    distinctUntilChanged(),
                    switchMap((next) => {
                        this.loading.addLoading();

                        return next
                            ? this.endpoint.disable({ applications: [application] })
                            : this.endpoint.enable({ applications: [application] });
                    }),
                    tap(() => this.cdr.markForCheck()),
                    tap(() => this.loading.completeAll()),
                    takeUntil(this.ngOnDestroy$),
                )
                .subscribe(
                    (status) => {
                        this.state = {
                            ...this.state,
                            status,
                        };

                        this.ngMessages.success({
                            message: status.enabled.includes(application) ? this.i18n('disabled') : this.i18n('enabled'),
                            translate: true,
                            translateParams: {
                                application: application.toUpperCase(),
                            },
                        });
                    },
                    () =>
                        this.ngMessages.error({
                            message: this.i18n('failed'),
                            translate: true,
                            translateParams: {
                                application: application.toUpperCase(),
                            },
                        }),
                );
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.maintenance.components.bazaMaintenanceCms.${input}`;
    }

    i18nApplication(application: Application): string {
        return application.toUpperCase();
    }

    get isLoading$(): Observable<boolean> {
        return this.loading.isLoading$;
    }

    get resolvedData(): BazaMaintenanceResolveData {
        return this.activatedRoute.snapshot.data[`${BAZA_MAINTENANCE_RESOLVE_KEY}`];
    }

    get applications(): Array<Application> {
        return this.state.status.scope;
    }

    isEnabled(application: Application): boolean {
        return !this.state.status.enabled.includes(application);
    }

    editMessage(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<MessageFormValue>({
            formGroup: this.fb.group({
                message: [this.state.message.message, [Validators.required]],
            }),
            formBuilder: {
                id: BAZA_CORE_MAINTENANCE_EDIT_MESSAGE_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Text,
                            label: this.i18n('messageForm.fields.message'),
                            formControlName: 'message',
                        },
                    ],
                },
            },
            title: this.i18n('messageForm.title'),
            onSubmit: (_, formValue) => {
                const loading = this.loading.addLoading();

                return this.endpoint
                    .setMessage({
                        message: formValue.message,
                    })
                    .pipe(
                        tap((response) => {
                            this.state = {
                                ...this.state,
                                message: response,
                            };
                        }),
                        map(() => ({
                            message: formValue.message,
                        })),
                        finalize(() => loading.complete()),
                        finalize(() => this.cdr.markForCheck()),
                    );
            },
        });
    }
}
