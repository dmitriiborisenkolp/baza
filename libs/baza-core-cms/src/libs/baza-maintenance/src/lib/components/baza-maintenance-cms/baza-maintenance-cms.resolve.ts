import { Injectable } from '@angular/core';
import { BazaMaintenanceDto, BazaMaintenanceMessageDto } from '@scaliolabs/baza-core-shared';
import { Resolve } from '@angular/router';
import { BazaMaintenanceCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    status: BazaMaintenanceDto;
    message: BazaMaintenanceMessageDto;
}

export const BAZA_MAINTENANCE_RESOLVE_KEY = 'status';

export { ResolveData as BazaMaintenanceResolveData };

@Injectable()
export class BazaMaintenanceCmsResolve implements Resolve<ResolveData> {
    constructor(private readonly endpoint: BazaMaintenanceCmsDataAccess) {}

    resolve(): Observable<ResolveData> | Promise<ResolveData> | ResolveData {
        const observables: [Observable<BazaMaintenanceDto>, Observable<BazaMaintenanceMessageDto>] = [
            this.endpoint.status(),
            this.endpoint.getMessage(),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([status, message]) => ({
                status,
                message,
            })),
        );
    }
}
