import { Routes } from '@angular/router';
import { BazaDeviceTokensCmsComponent } from './components/baza-device-tokens-cms/baza-device-tokens-cms.component';
import { BazaDeviceTokensCmsGuard } from './components/baza-device-tokens-cms/baza-device-tokens-cms.guard';
import { BazaDeviceTokenCmsResolve } from './components/baza-device-tokens-cms/baza-device-tokens-cms.resolve';

export const bazaDeviceTokensCmsRoutes: Routes = [
    {
        path: 'device-tokens/:accountId',
        component: BazaDeviceTokensCmsComponent,
        resolve: {
            data: BazaDeviceTokenCmsResolve,
        },
        canActivate: [BazaDeviceTokensCmsGuard],
    },
];
