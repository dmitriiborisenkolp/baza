import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BazaDeviceTokenCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaCmsLayoutModule } from '../../../../cms/layout/src';
import { BazaDeviceTokensCmsComponent } from './components/baza-device-tokens-cms/baza-device-tokens-cms.component';
import { BazaDeviceTokensCmsGuard } from './components/baza-device-tokens-cms/baza-device-tokens-cms.guard';
import { BazaDeviceTokenCmsResolve } from './components/baza-device-tokens-cms/baza-device-tokens-cms.resolve';

@NgModule({
    imports: [CommonModule, RouterModule, BazaCmsLayoutModule, BazaDeviceTokenCmsDataAccessModule],
    declarations: [BazaDeviceTokensCmsComponent],
    providers: [BazaDeviceTokenCmsResolve, BazaDeviceTokensCmsGuard],
})
export class BazaDeviceTokensCmsModule {}
