import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BazaDeviceTokenCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { BazaDeviceTokenDto, BazaDeviceTokenType } from '@scaliolabs/baza-core-shared';
import { Subject, takeUntil, tap } from 'rxjs';
import {
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutModalService,
} from '../../../../../../libs/baza-form-builder/src';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudItem, BazaTableFieldAlign, BazaTableFieldType } from '../../../../../baza-crud/src';
import { BazaDeviceTokenCmsResolveData } from './baza-device-tokens-cms.resolve';

export const BAZA_CORE_DEVICE_TOKEN_CMS_ID = 'baza-core-device-token-cms-id';
export const BAZA_CORE_DEVICE_TOKEN_CMS_LINK_FORM_ID = 'baza-core-device-token-cms-link-form-id';
interface LinkDeviceTokenFormValue {
    type: BazaDeviceTokenType;
    token: string;
}

interface State {
    config: BazaCrudConfig<BazaDeviceTokenDto>;
}

@Component({
    templateUrl: './baza-device-tokens-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDeviceTokensCmsComponent implements OnDestroy {
    public crud: BazaCrudComponent<BazaDeviceTokenDto>;

    private readonly ngOnDestroy$ = new Subject<void>();

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly fb: FormBuilder,
        private readonly formLayout: BazaFormLayoutModalService,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly dataAccess: BazaDeviceTokenCmsDataAccess,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get resolvedData(): BazaDeviceTokenCmsResolveData {
        return this.activatedRoute.snapshot.data['data'];
    }

    i18n(key: string): string {
        return `baza.deviceTokens.components.tokens.${key}`;
    }

    get crudConfig(): BazaCrudConfig<BazaDeviceTokenDto> {
        return {
            id: BAZA_CORE_DEVICE_TOKEN_CMS_ID,
            title: this.i18n('title'),
            titleArguments: {
                id: this.resolvedData.account.id,
                fullName: this.resolvedData.account.fullName,
            },
            backRouterLink: {
                routerLink: ['/accounts'],
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('breadcrumbs.accounts'),
                        routerLink: {
                            routerLink: ['/accounts'],
                        },
                    },
                    {
                        title: this.resolvedData.account.fullName,
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('items.linkToken'),
                            type: BazaNzButtonStyle.Default,
                            callback: this.linkDeviceToken.bind(this),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: () => this.dataAccess.getAll({ accountId: this.resolvedData.account.id }),
                },
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                    },
                    {
                        field: 'type',
                        title: this.i18n('columns.type'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 100,
                    },
                    {
                        field: 'token',
                        title: this.i18n('columns.token'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                    {
                        field: 'createdAt',
                        title: this.i18n('columns.created'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 300,
                    },
                    {
                        field: 'updatedAt',
                        title: this.i18n('columns.updated'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 300,
                    },
                ],
                actions: [
                    {
                        icon: 'delete',
                        title: this.i18n('actions.unlink'),
                        action: this.unlinkDeviceToken.bind(this),
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            type: ['', [Validators.required]],
            token: ['', [Validators.required]],
        });
    }

    private formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_CORE_DEVICE_TOKEN_CMS_LINK_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Select,
                        formControlName: 'type',
                        label: this.i18n('forms.linkToken.fields.type'),
                        required: true,
                        autoFocus: true,
                        values: Object.values(BazaDeviceTokenType).map((type) => ({ value: type, label: type })),
                        props: {
                            nzAllowClear: true,
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'token',
                        label: this.i18n('forms.linkToken.fields.token'),
                        required: true,
                    },
                ],
            },
        };
    }

    private linkDeviceToken(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaDeviceTokenDto, LinkDeviceTokenFormValue>({
            formGroup: this.formGroup(),
            formBuilder: this.formBuilder(),
            title: this.i18n('forms.linkToken.title'),
            onSubmit: (_, formValue) => {
                // eslint-disable-next-line security/detect-non-literal-fs-filename
                return this.dataAccess
                    .link({
                        accountId: this.resolvedData.account.id,
                        type: formValue.type,
                        token: formValue.token,
                    })
                    .pipe(tap(() => this.crud.refresh()));
            },
        });
    }

    private unlinkDeviceToken({ id, accountId, type, token }: BazaDeviceTokenDto) {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('unlink.message'),
            messageArgs: { id },
            confirm: () => {
                // eslint-disable-next-line security/detect-non-literal-fs-filename
                this.dataAccess
                    .unlink({ accountId, type, token })
                    .pipe(
                        tap(() => this.crud.refresh()),
                        takeUntil(this.ngOnDestroy$),
                    )
                    .toPromise();
            },
        });
    }
}
