import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { BazaAccountCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { AccountCmsDto } from '@scaliolabs/baza-core-shared';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';

interface ResolveData {
    account: AccountCmsDto;
}

export { ResolveData as BazaDeviceTokenCmsResolveData };

@Injectable()
export class BazaDeviceTokenCmsResolve implements Resolve<ResolveData> {
    constructor(private readonly accountCmsDataAccess: BazaAccountCmsDataAccess) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> {
        const accountId = parseInt(route.params['accountId'], 10);

        const observables: [Observable<AccountCmsDto>] = [this.accountCmsDataAccess.getAccountById({ id: accountId })];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([account]) => ({
                account,
            })),
        );
    }
}
