import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { BazaNotFoundService } from '../../../../../baza-common/src';
import { BazaAccountCmsConfig } from '../../../../../baza-account/src';

@Injectable()
export class BazaDeviceTokensCmsGuard implements CanActivate {
    constructor(private readonly moduleConfig: BazaAccountCmsConfig, private readonly notFoundService: BazaNotFoundService) {}

    canActivate(): boolean {
        if (!this.moduleConfig.withDeviceTokenFeature) {
            this.notFoundService.navigateToNotFound();
            return false;
        }

        return true;
    }
}
