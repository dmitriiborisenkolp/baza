import { Injectable } from '@angular/core';
import { AccountCmsDto } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    account: AccountCmsDto;
}

export { ResolveData as BazaAuthSessionCmsResolveData };

@Injectable()
export class BazaAuthSessionCmsResolve implements Resolve<ResolveData> {
    constructor(private readonly accountDataAccess: BazaAccountCmsDataAccess) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> {
        const observables: [Observable<AccountCmsDto>] = [
            this.accountDataAccess.getAccountById({
                id: parseInt(route.params['accountId'], 10),
            }),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([account]) => ({
                account,
            })),
        );
    }
}
