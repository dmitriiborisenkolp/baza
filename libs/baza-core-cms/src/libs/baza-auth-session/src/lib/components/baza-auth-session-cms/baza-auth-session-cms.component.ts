import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaTableFieldAlign, BazaTableFieldType } from '../../../../../baza-crud/src';
import { BazaAuthSessionDto, BazaAuthSessionRecordType } from '@scaliolabs/baza-core-shared';
import { Subject } from 'rxjs';
import { BazaAuthSessionCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { BazaAuthSessionCmsResolveData } from './baza-auth-session-cms.resolve';
import { ActivatedRoute } from '@angular/router';

export const BAZA_CORE_AUTH_SESSIONS_CMS_ID = 'baza-core-auth-sessions-cms-id';

interface State {
    config: BazaCrudConfig<BazaAuthSessionDto>;
}

@Component({
    templateUrl: './baza-auth-session-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaAuthSessionCmsComponent implements OnDestroy {
    public crud: BazaCrudComponent<BazaAuthSessionDto>;

    private readonly ngOnDestroy$ = new Subject<void>();

    public state: State = {
        config: this.crudConfig,
    };

    constructor(private readonly activatedRoute: ActivatedRoute, private readonly dataAccess: BazaAuthSessionCmsDataAccess) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get resolvedData(): BazaAuthSessionCmsResolveData {
        return this.activatedRoute.snapshot.data['data'];
    }

    i18n(key: string): string {
        return `baza.authSessions.components.sessions.${key}`;
    }

    i18nType(type: BazaAuthSessionRecordType): string {
        return `baza.authSessions._.types.BazaAuthSessionRecordType.${type}`;
    }

    get crudConfig(): BazaCrudConfig<BazaAuthSessionDto> {
        return {
            id: BAZA_CORE_AUTH_SESSIONS_CMS_ID,
            title: this.i18n('title'),
            titleArguments: {
                id: this.resolvedData.account.id,
                fullName: this.resolvedData.account.fullName,
            },
            backRouterLink: {
                routerLink: ['/auth-sessions'],
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('sessions'),
                        routerLink: {
                            routerLink: ['/auth-sessions'],
                        },
                    },
                    {
                        title: this.resolvedData.account.fullName,
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccess.sessions({
                            ...request,
                            accountId: this.resolvedData.account.id,
                        }),
                },
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                    },
                    {
                        field: 'date',
                        title: this.i18n('columns.date'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        width: 130,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'ip',
                        title: this.i18n('columns.ip'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 130,
                    },
                    {
                        field: 'type',
                        title: this.i18n('columns.type'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            translate: true,
                            format: (entity) => this.i18nType(entity.type),
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 220,
                    },
                    {
                        field: 'os',
                        title: this.i18n('columns.os'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 200,
                    },
                    {
                        field: 'browser',
                        title: this.i18n('columns.browser'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 200,
                    },
                    {
                        field: 'userAgent',
                        title: this.i18n('columns.userAgent'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                ],
            },
        };
    }
}
