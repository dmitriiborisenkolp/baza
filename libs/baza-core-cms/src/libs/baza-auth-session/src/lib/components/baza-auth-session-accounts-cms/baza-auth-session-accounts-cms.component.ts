import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaTableFieldAlign, BazaTableFieldType } from '../../../../../baza-crud/src';
import { BazaAuthSessionAccountDto } from '@scaliolabs/baza-core-shared';
import { Subject } from 'rxjs';
import { BazaAuthSessionCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';

export const BAZA_CORE_AUTH_SESSION_ACCOUNTS_CMS_ID = 'baza-core-auth-session-accounts-cms-id';

interface State {
    config: BazaCrudConfig<BazaAuthSessionAccountDto>;
}

@Component({
    templateUrl: './baza-auth-session-accounts-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaAuthSessionAccountsCmsComponent implements OnDestroy {
    public crud: BazaCrudComponent<BazaAuthSessionAccountDto>;

    private readonly ngOnDestroy$ = new Subject<void>();

    public state: State = {
        config: this.crudConfig,
    };

    constructor(private readonly dataAccess: BazaAuthSessionCmsDataAccess) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza.authSessions.components.accounts.${key}`;
    }

    get crudConfig(): BazaCrudConfig<BazaAuthSessionAccountDto> {
        return {
            id: BAZA_CORE_AUTH_SESSION_ACCOUNTS_CMS_ID,
            title: this.i18n('title'),
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.accounts(request),
                },
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                    },
                    {
                        field: 'user',
                        title: this.i18n('columns.user'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        source: (entity) => entity.account.fullName,
                    },
                    {
                        field: 'lastActive',
                        title: this.i18n('columns.lastActive'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        source: (entity) => entity.lastSignIn,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 250,
                    },
                    {
                        field: 'createdAt',
                        title: this.i18n('columns.createdAt'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        source: (entity) => entity.createdAt,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 250,
                    },
                    {
                        field: 'authCountAttempts',
                        title: this.i18n('columns.authCountAttempts'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        source: (entity) => entity.authCountAttempts,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 120,
                    },
                    {
                        field: 'authCountSignIn',
                        title: this.i18n('columns.authCountSignIn'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        source: (entity) => entity.authCountSignIn,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 120,
                    },
                    {
                        field: 'details',
                        title: '',
                        type: {
                            kind: BazaTableFieldType.Link,
                            icon: 'right-square',
                            link: (entity) => ({
                                routerLink: ['/auth-sessions', entity.account.id],
                            }),
                            format: () => this.i18n('columns.details'),
                            translate: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 120,
                    },
                ],
            },
        };
    }
}
