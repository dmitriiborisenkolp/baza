import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BazaCmsLayoutModule } from '../../../../cms/layout/src';
import { BazaAuthSessionCmsComponent } from './components/baza-auth-session-cms/baza-auth-session-cms.component';
import { BazaAuthSessionAccountsCmsComponent } from './components/baza-auth-session-accounts-cms/baza-auth-session-accounts-cms.component';
import { BazaAuthSessionCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaAuthSessionCmsResolve } from './components/baza-auth-session-cms/baza-auth-session-cms.resolve';

@NgModule({
    imports: [CommonModule, RouterModule, BazaCmsLayoutModule, BazaAuthSessionCmsDataAccessModule],
    declarations: [BazaAuthSessionCmsComponent, BazaAuthSessionAccountsCmsComponent],
    providers: [BazaAuthSessionCmsResolve],
})
export class BazaAuthSessionCmsModule {}
