import { Routes } from '@angular/router';
import { BazaAuthSessionAccountsCmsComponent } from './components/baza-auth-session-accounts-cms/baza-auth-session-accounts-cms.component';
import { BazaAuthSessionCmsComponent } from './components/baza-auth-session-cms/baza-auth-session-cms.component';
import { BazaAuthSessionCmsResolve } from './components/baza-auth-session-cms/baza-auth-session-cms.resolve';

export const bazaAuthSessionCmsRoutes: Routes = [
    {
        path: 'auth-sessions',
        component: BazaAuthSessionAccountsCmsComponent,
    },
    {
        path: 'auth-sessions/:accountId',
        component: BazaAuthSessionCmsComponent,
        resolve: {
            data: BazaAuthSessionCmsResolve,
        },
    },
];
