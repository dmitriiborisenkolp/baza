import { Observable, of } from 'rxjs';

export const toObservable = (input: boolean | Observable<boolean>) => {
    return input instanceof Observable
        ? input
        : of(!! input);
}
