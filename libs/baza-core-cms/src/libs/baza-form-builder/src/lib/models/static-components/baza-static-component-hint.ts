import { BazaFormBuilderComponentHintComponent } from '../../form-builder/components/baza-form-builder-component-hint/baza-form-builder-component-hint.component';
import {
    BazaFormBuilderComponentStaticOptions,
    BazaFormBuilderStaticComponentType,
    registerStaticComponent,
} from '../baza-form-builder-component-static';

export interface BazaStaticComponentHint extends BazaFormBuilderComponentStaticOptions {
    type: BazaFormBuilderStaticComponentType.Hint;
    props?: Partial<BazaFormBuilderComponentHintComponent>;
}

registerStaticComponent(BazaFormBuilderStaticComponentType.Hint, {
    component: BazaFormBuilderComponentHintComponent,
});
