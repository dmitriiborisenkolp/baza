import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderUploadImageMultiComponent } from '../../form-builder/controls/baza-form-builder-upload';

export interface BazaFormControlFieldBazaImageMulti extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaImageMulti;
    props?: Partial<BazaFormBuilderUploadImageMultiComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaImageMulti, {
    component: BazaFormBuilderUploadImageMultiComponent,
    enableNzFormContainer: true,
});

