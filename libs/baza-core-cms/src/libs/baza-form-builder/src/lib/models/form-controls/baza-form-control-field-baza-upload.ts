import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderUploadComponent } from '../../form-builder/controls/baza-form-builder-upload';

export interface BazaFormControlFieldBazaUpload extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaUpload;
    props?: Partial<Exclude<BazaFormBuilderUploadComponent, 'enableMultiControls' | 'multiIsFirstControl' | 'multiIsLastControl'>>;
}

registerFormControl(BazaFormBuilderControlType.BazaUpload, {
    component: BazaFormBuilderUploadComponent,
    enableNzFormContainer: true,
});
