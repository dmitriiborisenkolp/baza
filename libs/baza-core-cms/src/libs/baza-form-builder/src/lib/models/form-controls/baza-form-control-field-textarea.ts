import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzInputDirective } from 'ng-zorro-antd/input';
import { BazaFormBuilderTextareaComponent } from '../../form-builder/controls/baza-form-builder-textarea/baza-form-builder-textarea.component';

export interface BazaFormControlFieldTextarea extends BazaFormControl {
    type: BazaFormBuilderControlType.TextArea;
    props?: Partial<BazaFormControl>;
    directiveProps?: Partial<NzInputDirective>;
    rows?: number;
}

registerFormControl(BazaFormBuilderControlType.TextArea, {
    component: BazaFormBuilderTextareaComponent,
    enableNzFormContainer: true,
});
