import { NzTabPosition } from 'ng-zorro-antd/tabs';
import { Observable } from 'rxjs';
import { BazaCoreFormBuilderComponents } from './baza-form-builder-fields';

export enum BazaFormBuilderLayout {
    FormOnly = 'FormOnly',
    WithTabs = 'WithTabs',
    WithAsyncTabs = 'WithAsyncTabs',
}

// Payload definition for each layout
export type BazaFormBuilder<T = BazaCoreFormBuilderComponents> =
    | BazaFormBuilderFormOnlyType<T>
    | BazaFormBuilderWithTabsType<T>
    | BazaFormBuilderWithAsyncTabsType<T>;

export type BazaFormBuilderFormOnlyType<T = BazaCoreFormBuilderComponents> = {
    id?: string;
    layout: BazaFormBuilderLayout.FormOnly;
    contents: BazaFormBuilderContents<T>;
};
export type BazaFormBuilderWithTabsType<T = BazaCoreFormBuilderComponents> = {
    id?: string;
    layout: BazaFormBuilderLayout.WithTabs;
    contents: BazaFormBuilderWithTabs<T>;
};
export type BazaFormBuilderWithAsyncTabsType<T = BazaCoreFormBuilderComponents> = {
    id?: string;
    layout: BazaFormBuilderLayout.WithAsyncTabs;
    contents: Observable<BazaFormBuilderWithTabs<T>>;
};

// Simple layout definition
export interface BazaFormBuilderContents<T = BazaCoreFormBuilderComponents> {
    // List of static-components
    fields: Array<T>;
}

// Tabs layout definition
export interface BazaFormBuilderWithTabs<T = BazaCoreFormBuilderComponents> {
    // Lists of tabs + fields definitions (@)
    tabs: Array<BazaFormBuilderTab<T>>;

    // Tabs position - top, bottom. right
    // @see https://ng.ant.design/components/tabs/en#when-to-use
    nzTabPosition?: NzTabPosition;
}

export interface BazaFormBuilderTab<T = BazaCoreFormBuilderComponents> {
    // Auto-Generated Tab Id
    id?: string;

    // Title of tab. By default translated via i18n
    title: string;

    // Enable it to disable title translation via i18n
    withoutTranslate?: boolean;

    // Hides tab if user has no specific ACL
    acl?: Array<string>;

    // Tab visibility. Tabs visibility depends on acl + given subject
    visible?: Observable<boolean>;

    // Tab disabled. Does not works with ACL.
    disabled?: Observable<boolean>;

    // Fields list
    contents: BazaFormBuilderContents<T>;
}
