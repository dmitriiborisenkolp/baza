import { BazaFormBuilderUploadAttachmentSingleComponent } from '../../form-builder/controls/baza-form-builder-upload';
import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';

export interface BazaFormControlFieldBazaUploadAttachment extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaUploadAttachment;
    props?: Partial<BazaFormBuilderUploadAttachmentSingleComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaUploadAttachment, {
    component: BazaFormBuilderUploadAttachmentSingleComponent,
    enableNzFormContainer: true,
});
