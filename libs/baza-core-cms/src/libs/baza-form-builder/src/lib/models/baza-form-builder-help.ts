import { NzFormTooltipIcon } from "ng-zorro-antd/form";
import { BazaFormFieldWithText } from "./baza-form-builder-with-text";

export interface BazaFormFieldHelp extends BazaFormFieldWithText{

    // Nz-Icon defaults to 'info-circle'
    icon?: NzFormTooltipIcon;

}
