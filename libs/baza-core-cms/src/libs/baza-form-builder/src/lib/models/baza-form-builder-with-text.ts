export interface BazaFormFieldWithText {
    // Text for field
    text: string;

    // Translate option?
    translate?: boolean;

    // Translate args for field
    translateArgs?: any;
}
