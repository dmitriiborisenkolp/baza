import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzInputDirective } from 'ng-zorro-antd/input';
import { BazaFormBuilderTextComponent } from '../../form-builder/controls/baza-form-builder-text/baza-form-builder-text.component';

export interface BazaFormControlFieldText extends BazaFormControl {
    type: BazaFormBuilderControlType.Text;
    props?: Partial<BazaFormBuilderTextComponent>;
    directiveProps?: Partial<NzInputDirective>;
}

registerFormControl(BazaFormBuilderControlType.Text, {
    component: BazaFormBuilderTextComponent,
    enableNzFormContainer: true,
});
