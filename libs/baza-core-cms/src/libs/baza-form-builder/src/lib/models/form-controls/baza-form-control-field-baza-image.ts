import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderUploadImageComponent } from '../../form-builder/controls/baza-form-builder-upload';

export interface BazaFormControlFieldBazaImage extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaImage;
    props?: Partial<BazaFormBuilderUploadImageComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaImage, {
    component: BazaFormBuilderUploadImageComponent,
    enableNzFormContainer: true,
});

