import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderAccountComponent } from '../../form-builder/controls/baza-form-builder-account';

export interface BazaFormControlFieldBazaAccount extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaAccount;
    props?: Partial<BazaFormBuilderAccountComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaAccount, {
    component: BazaFormBuilderAccountComponent,
    enableNzFormContainer: true,
});
