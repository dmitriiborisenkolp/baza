import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderMailRecipientComponent } from '../../form-builder/controls/baza-form-builder-mail-recipient';

export interface BazaFormControlFieldBazaMailRecipient extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaMailRecipient;
    props?: Partial<BazaFormBuilderMailRecipientComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaMailRecipient, {
    component: BazaFormBuilderMailRecipientComponent,
    enableNzFormContainer: true,
});
