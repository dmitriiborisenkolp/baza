import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderHtmlComponent } from '../../form-builder/controls/baza-form-builder-html';

export interface BazaFormControlFieldBazaHtml extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaHtml;
    props?: Partial<BazaFormBuilderHtmlComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaHtml, {
    component: BazaFormBuilderHtmlComponent,
    enableNzFormContainer: true,
});
