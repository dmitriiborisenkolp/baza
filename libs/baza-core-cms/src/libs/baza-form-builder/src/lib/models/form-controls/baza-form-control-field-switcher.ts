import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzSwitchComponent } from 'ng-zorro-antd/switch';

export interface BazaFormControlFieldSwitcher extends BazaFormControl {
    type: BazaFormBuilderControlType.Switcher;
    props?: Partial<NzSwitchComponent>;
}

registerFormControl(BazaFormBuilderControlType.Switcher, {
    component: NzSwitchComponent,
    enableNzFormContainer: true,
});
