import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderKeyValueComponent } from '../../form-builder/controls/baza-form-builder-key-value/baza-form-builder-key-value.component';

export interface BazaFormControlFieldBazaKeyValue extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaKeyValue;
    props?: Partial<BazaFormBuilderKeyValueComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaKeyValue, {
    component: BazaFormBuilderKeyValueComponent,
    enableNzFormContainer: true,
});
