import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzInputDirective } from 'ng-zorro-antd/input';
import { BazaFormBuilderPasswordComponent } from '../../form-builder/controls/baza-form-builder-password/baza-form-builder-password.component';

export interface BazaFormControlFieldPassword extends BazaFormControl {
    type: BazaFormBuilderControlType.Password;
    props?: Partial<BazaFormControl>;
    directiveProps?: Partial<NzInputDirective>;
}

registerFormControl(BazaFormBuilderControlType.Password, {
    component: BazaFormBuilderPasswordComponent,
    enableNzFormContainer: true,
});
