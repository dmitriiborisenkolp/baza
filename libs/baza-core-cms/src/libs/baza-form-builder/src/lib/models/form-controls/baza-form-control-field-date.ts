import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzDatePickerComponent } from 'ng-zorro-antd/date-picker';

export interface BazaFormControlFieldDate extends BazaFormControl {
    type: BazaFormBuilderControlType.Date;
    props?: Partial<NzDatePickerComponent>;
}

registerFormControl(BazaFormBuilderControlType.Date, {
    component: NzDatePickerComponent,
    enableNzFormContainer: true,
});
