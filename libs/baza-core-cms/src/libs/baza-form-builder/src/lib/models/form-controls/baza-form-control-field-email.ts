import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzInputDirective } from 'ng-zorro-antd/input';
import { BazaFormBuilderEmailComponent } from '../../form-builder/controls/baza-form-builder-email/baza-form-builder-email.component';

export interface BazaFormControlFieldEmail extends BazaFormControl {
    type: BazaFormBuilderControlType.Email;
    props?: Partial<BazaFormBuilderEmailComponent>;
    directiveProps?: Partial<NzInputDirective>;
}

registerFormControl(BazaFormBuilderControlType.Email, {
    component: BazaFormBuilderEmailComponent,
    enableNzFormContainer: true,
});
