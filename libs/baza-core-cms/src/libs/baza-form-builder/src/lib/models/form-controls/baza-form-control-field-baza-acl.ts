import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaCmsAclControlComponent } from '../../../../../baza-acl/src';

export interface BazaFormControlFieldBazaAcl extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaAcl;
    props?: Partial<BazaCmsAclControlComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaAcl, {
    component: BazaCmsAclControlComponent,
    enableNzFormContainer: false,
});
