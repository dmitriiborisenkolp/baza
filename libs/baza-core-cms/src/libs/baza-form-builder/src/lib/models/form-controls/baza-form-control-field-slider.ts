import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzSliderComponent } from 'ng-zorro-antd/slider';

export interface BazaFormControlFieldSlider extends BazaFormControl {
    type: BazaFormBuilderControlType.Slider;
    props?: Partial<NzSliderComponent>;
}

registerFormControl(BazaFormBuilderControlType.Slider, {
    component: NzSliderComponent,
    enableNzFormContainer: true,
});
