import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzTimePickerComponent } from 'ng-zorro-antd/time-picker';

export interface BazaFormControlFieldTime extends BazaFormControl {
    type: BazaFormBuilderControlType.Time;
    props?: Partial<NzTimePickerComponent>;
}

registerFormControl(BazaFormBuilderControlType.Time, {
    component: NzTimePickerComponent,
    enableNzFormContainer: true,
});
