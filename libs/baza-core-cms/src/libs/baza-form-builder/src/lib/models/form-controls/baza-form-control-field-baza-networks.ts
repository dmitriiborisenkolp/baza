import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderNetworksComponent } from '../../form-builder/controls/baza-form-builder-networks';

export interface BazaFormControlFieldBazaNetworks extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaNetworks;
    props?: Partial<BazaFormBuilderNetworksComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaNetworks, {
    component: BazaFormBuilderNetworksComponent,
    enableNzFormContainer: true,
});
