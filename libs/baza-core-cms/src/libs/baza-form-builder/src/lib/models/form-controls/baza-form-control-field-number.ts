import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzInputNumberComponent } from 'ng-zorro-antd/input-number';

export interface BazaFormControlFieldNumber extends BazaFormControl {
    type: BazaFormBuilderControlType.Number;
    props?: Partial<NzInputNumberComponent>;
}

registerFormControl(BazaFormBuilderControlType.Number, {
    component: NzInputNumberComponent,
    enableNzFormContainer: true,
    transformProps: (self: Partial<BazaFormControlFieldNumber>, services) => {
        if (! self.props) {
            self.props = {};
        }

        if (! self.props.nzPlaceHolder && self.placeholder) {
            self.props.nzPlaceHolder = self.placeholderWithoutTranslate
                ? self.placeholder
                : services.translate.instant(self.placeholder, self.placeholderTranslateArgs);
        } else if (! self.props.nzPlaceHolder && self.label) {
            self.props.nzPlaceHolder = self.labelWithoutTranslate
                ? self.label
                : services.translate.instant(self.label, self.labelTranslateArgs);
        }
    },
});
