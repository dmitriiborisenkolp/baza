import { BazaFormBuilderComponentStaticOptions, BazaFormBuilderStaticComponentType, registerStaticComponent } from '../baza-form-builder-component-static';
import { BazaFormBuilderComponentTimestampComponent } from '../../form-builder/components/baza-form-builder-component-timestamp/baza-form-builder-component-timestamp.component';

export interface BazaStaticComponentTimestamp extends BazaFormBuilderComponentStaticOptions {
    type: BazaFormBuilderStaticComponentType.TimeStamp;
    props?: Partial<BazaFormBuilderComponentTimestampComponent>;
}

registerStaticComponent(BazaFormBuilderStaticComponentType.TimeStamp, {
    component: BazaFormBuilderComponentTimestampComponent,
});
