import { BazaFormBuilderComponentStaticOptions, BazaFormBuilderStaticComponentType, registerStaticComponent } from '../baza-form-builder-component-static';
import { NzDividerComponent } from 'ng-zorro-antd/divider';

export interface BazaStaticComponentDivider extends BazaFormBuilderComponentStaticOptions {
    type: BazaFormBuilderStaticComponentType.Divider;
    props?: Partial<NzDividerComponent>;
    withoutTranslate?: boolean;
    translateArgs?: any;
}

registerStaticComponent(BazaFormBuilderStaticComponentType.Divider, {
    component: NzDividerComponent,
    transformProps: (self: Partial<BazaStaticComponentDivider>, services) => {
        if (! self.props) {
            self.props = {};
        }

        if (! self.withoutTranslate) {
            if (self.props.nzText && (typeof self.props.nzText === 'string')) {
                self.props.nzText = services.translate.instant(self.props.nzText, self.translateArgs);
            }
        }

        if (! self.props.nzText) {
            self.props.nzText = '';
        }
    },
});
