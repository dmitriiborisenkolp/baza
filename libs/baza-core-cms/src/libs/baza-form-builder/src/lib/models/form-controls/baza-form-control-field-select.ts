import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzSelectComponent } from 'ng-zorro-antd/select';

export interface BazaFormBuilderSelectOption<T = any> {
    // Value for select option
    value: T;

    // Label for select option. By default label option IS NOT translated via i18n
    label: string;

    // Enable it to enable label translation via i18n
    translate?: boolean;

    // Translation variables
    translateArgs?: any;

    // Disabled status for option
    disabled?: boolean;

    // Visible status for option
    hidden?: boolean;

    groupLabel?: string

    // Enable it to enable label translation via i18n
    groupLabelTranslate?: boolean;

    // Translation variables
    groupLabelTranslateArgs?: any;
}

export interface BazaFormControlFieldSelect extends BazaFormControl {
    type: BazaFormBuilderControlType.Select;
    props?: Partial<NzSelectComponent>;
    values?: Array<BazaFormBuilderSelectOption>;
}

registerFormControl(BazaFormBuilderControlType.Select, {
    component: NzSelectComponent,
    enableNzFormContainer: true,
    transformProps: (self: Partial<BazaFormControlFieldSelect>, services) => {
        if (Array.isArray(self.values)) {
            if (! self.props) {
                self.props = {};
            }

            self.props.nzOptions = self.values.map((option) => ({
                label: option.translate
                    ? services.translate.instant(option.label, option.translateArgs)
                    : option.label,
                value: option.value,
                disabled: option.disabled,
                hide: option.hidden,
                groupLabel: option.groupLabel && option.groupLabelTranslate
                    ? services.translate.instant(option.groupLabel, option.groupLabelTranslateArgs)
                    : option.groupLabel,
            }));
        }
    },
});
