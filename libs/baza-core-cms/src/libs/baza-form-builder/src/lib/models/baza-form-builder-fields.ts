import { BazaStaticComponentDivider } from './static-components/baza-static-component-divider';
import { BazaFormControlFieldText } from './form-controls/baza-form-control-field-text';
import { BazaFormControlFieldTextarea } from './form-controls/baza-form-control-field-textarea';
import { BazaFormControlFieldCheckbox } from './form-controls/baza-form-control-field-checkbox';
import { BazaFormControlFieldEmail } from './form-controls/baza-form-control-field-email';
import { BazaFormControlFieldPassword } from './form-controls/baza-form-control-field-password';
import { BazaFormControlFieldNumber } from './form-controls/baza-form-control-field-number';
import { BazaFormControlFieldSelect } from './form-controls/baza-form-control-field-select';
import { BazaFormControlFieldDate } from './form-controls/baza-form-control-field-date';
import { BazaFormControlFieldDateRange } from './form-controls/baza-form-control-field-date-range';
import { BazaFormControlFieldTime } from './form-controls/baza-form-control-field-time';
import { BazaFormControlFieldRate } from './form-controls/baza-form-control-field-rate';
import { BazaFormControlFieldSlider } from './form-controls/baza-form-control-field-slider';
import { BazaFormControlFieldSwitcher } from './form-controls/baza-form-control-field-switcher';
import { BazaStaticComponentTimestamp } from './static-components/baza-static-component-timestamp';
import { BazaFormControlFieldBazaSeo } from './form-controls/baza-form-control-field-baza-seo';
import { BazaFormControlFieldBazaNetworks } from './form-controls/baza-form-control-field-baza-networks';
import { BazaFormControlFieldBazaAcl } from './form-controls/baza-form-control-field-baza-acl';
import { BazaFormControlFieldBazaUpload } from './form-controls/baza-form-control-field-baza-upload';
import { BazaFormControlFieldBazaMailRecipient } from './form-controls/baza-form-control-field-baza-mail-recipient';
import { BazaFormControlFieldBazaAccount } from './form-controls/baza-form-control-field-baza-account';
import { BazaFormControlFieldBazaHtml } from './form-controls/baza-form-control-field-baza-html';
import { BazaFormControlFieldBazaUploadMulti } from './form-controls/baza-form-control-field-baza-upload-multi';
import { BazaFormControlFieldBazaUploadAttachment } from './form-controls/baza-form-control-field-baza-upload-attachment';
import { BazaFormControlFieldBazaUploadAttachmentMulti } from './form-controls/baza-form-control-field-baza-upload-attachment-multi';
import { BazaFormControlFieldBazaKeyValue } from './form-controls/baza-form-control-field-baza-key-value';
import { BazaFormControlFieldBazaImage } from './form-controls/baza-form-control-field-baza-image';
import { BazaFormControlFieldBazaImageMulti } from './form-controls/baza-form-control-field-baza-image-multi';
import { BazaStaticComponentHint } from './static-components/baza-static-component-hint';

/**
 * All BazaCore form builder static-components
 */

export type BazaFormBuilderStaticComponents = BazaStaticComponentDivider | BazaStaticComponentTimestamp | BazaStaticComponentHint;

/**
 * All Baza Core form builder controls definitions
 */
export type BazaFormBuilderFormControlComponents =
    | BazaFormControlFieldText
    | BazaFormControlFieldTextarea
    | BazaFormControlFieldCheckbox
    | BazaFormControlFieldEmail
    | BazaFormControlFieldPassword
    | BazaFormControlFieldNumber
    | BazaFormControlFieldSelect
    | BazaFormControlFieldDate
    | BazaFormControlFieldDateRange
    | BazaFormControlFieldTime
    | BazaFormControlFieldRate
    | BazaFormControlFieldSlider
    | BazaFormControlFieldSwitcher
    | BazaFormControlFieldBazaAccount
    | BazaFormControlFieldBazaAcl
    | BazaFormControlFieldBazaHtml
    | BazaFormControlFieldBazaMailRecipient
    | BazaFormControlFieldBazaNetworks
    | BazaFormControlFieldBazaSeo
    | BazaFormControlFieldBazaUpload
    | BazaFormControlFieldBazaUploadMulti
    | BazaFormControlFieldBazaUploadAttachment
    | BazaFormControlFieldBazaUploadAttachmentMulti
    | BazaFormControlFieldBazaKeyValue
    | BazaFormControlFieldBazaImage
    | BazaFormControlFieldBazaImageMulti;

/**
 * Merged definitions for static-components/form controls
 */
export type BazaCoreFormBuilderComponents = BazaFormBuilderStaticComponents | BazaFormBuilderFormControlComponents;
