import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { NzRateComponent } from 'ng-zorro-antd/rate';

export interface BazaFormControlFieldRate extends BazaFormControl {
    type: BazaFormBuilderControlType.Rate;
    props?: Partial<NzRateComponent>;
}

registerFormControl(BazaFormBuilderControlType.Rate, {
    component: NzRateComponent,
    enableNzFormContainer: true,
});
