import { BazaFormBuilderDateRangeComponent } from '../../form-builder/controls/baza-form-builder-date-range/baza-form-builder-date-range.component';
import { BazaFormBuilderControlType, BazaFormControl, registerFormControl } from '../baza-form-builder-control';

export interface BazaFormControlFieldDateRange extends BazaFormControl {
    type: BazaFormBuilderControlType.DateRange;
    props?: Partial<BazaFormBuilderDateRangeComponent>;
}

registerFormControl(BazaFormBuilderControlType.DateRange, {
    component: BazaFormBuilderDateRangeComponent,
    enableNzFormContainer: true,
});
