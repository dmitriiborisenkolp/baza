import { Observable } from 'rxjs';
import { BazaFormFieldHint } from './baza-form-builder-hint';
import { ComponentType } from '@scaliolabs/baza-core-shared';
import { BazaFormBuilderComponentType, bazaFormBuilderRegisterComponentType } from './baza-form-builder-component-types';
import { TranslateService } from '@ngx-translate/core';
import { ComponentRef } from '@angular/core';
import { BazaFormBuilderContentsFormControlPortalComponent } from '../components/baza-form-builder-contents/form-control-portal/form-control-portal.component';
import { BazaFormFieldHelp } from './baza-form-builder-help';

// All core fields definitions
// You are able to inject static-components directly via Inject
export enum BazaFormBuilderControlType {
    // Nz-Zorro static-components
    Text = 'baza-control-text', // https://ng.ant.design/components/input/en#when-to-use
    TextArea = 'baza-control-textarea', // https://ng.ant.design/components/input/en#when-to-use
    Email = 'baza-control-email', // https://ng.ant.design/components/input/en#when-to-use
    Password = 'baza-control-password', // https://ng.ant.design/components/input/en#when-to-use
    Select = 'baza-control-select', // https://ng.ant.design/components/select/en#when-to-use
    Checkbox = 'baza-control-checkbox', // https://ng.ant.design/components/checkbox/en#when-to-use
    Date = 'baza-control-date', // https://ng.ant.design/components/date-picker/en#when-to-use
    DateRange = 'baza-control-date-range', // https://ng.ant.design/components/date-picker/en#components-date-picker-demo-range-picker
    Time = 'baza-control-time', // https://ng.ant.design/components/time-picker/en#when-to-use
    Number = 'baza-control-number', // https://ng.ant.design/components/input-number/en#when-to-use
    Rate = 'baza-control-rate', // https://ng.ant.design/components/rate/en#when-to-use
    Slider = 'baza-control-slider', // https://ng.ant.design/components/slider/en#when-to-use
    Switcher = 'baza-control-switcher', // https://ng.ant.design/components/switch/en#when-to-use

    // Baza-Core static-components
    BazaAcl = 'baza-control-acl',
    BazaUpload = 'baza-control-upload',
    BazaUploadMulti = 'baza-control-upload-multi',
    BazaUploadAttachment = 'baza-control-upload-attachment',
    BazaUploadAttachmentMulti = 'baza-control-upload-attachment-multi',
    BazaImage = 'baza-control-image',
    BazaImageMulti = 'baza-control-image-multi',
    BazaHtml = 'baza-control-html',
    BazaSeo = 'baza-control-seo',
    BazaAccount = 'baza-control-account',
    BazaMailRecipient = 'baza-control-mail-recipient',
    BazaNetworks = 'baza-control-networks',
    BazaKeyValue = 'baza-key-value',
}

/**
 * FormControl-kind base interface for all form build fields.
 * Every new form controlled field should extends from BazaFormBuildComponentFormField interface
 */
export interface BazaFormControl<C = any, T = BazaFormBuilderControlType> {
    // ID for trackBy

    id?: string;

    // Component Type

    type: T;
    props?: Partial<C>;

    // Callback

    afterViewInit?: (componentRef: ComponentRef<C>, portal: BazaFormBuilderContentsFormControlPortalComponent) => any;

    // FormControl options

    formControlName: string;

    // Label options

    label?: string;
    labelTranslateArgs?: any;
    labelWithoutTranslate?: boolean;

    // Placeholder options

    placeholder?: string;
    placeholderTranslateArgs?: string;
    placeholderWithoutTranslate?: string;

    // UI options

    autoFocus?: boolean;
    hint?: BazaFormFieldHint;

    // Tool tip options
    help?: BazaFormFieldHelp;

    // Visibility / ACL options

    // Hides field if user has no specific ACL
    acl?: Array<string>;

    // Field visibility. Tabs visibility depends on acl + given subject
    visible?: boolean | Observable<boolean>;

    // Field visibility. Tabs visibility depends on acl + given subject
    readonly?: boolean | Observable<boolean>;

    // Field visibility. Tabs visibility depends on acl + given subject
    disabled?: boolean | Observable<boolean>;

    // Field required. Mostly it's used to display * mark for ng-zorro form
    required?: boolean | Observable<boolean>;
}

/**
 * Configuration for BazaFormBuilderComponentFormField component
 */
export interface BazaFormBuilderComponentFormControlConfig<T = any> {
    /**
     * Component to render
     */
    component: ComponentType<T>;

    /**
     * Transform props before ngOnInit/ngOnChanges lifecycles
     */
    transformProps?: (
        self: Partial<T>,
        services: {
            translate: TranslateService;
        },
    ) => void;

    /**
     * Enables nz-form-label/nz-form-control container for field
     */
    enableNzFormContainer: boolean;
}

/**
 * Configuration for BazaFormBuilderComponentFormField static-components stores here and being used in internal implementations
 * of baza-form-builder. Do not manually change it; use configureBazaFormComponent function to add new form control
 */
export const BAZA_FORM_BUILDER_COMPONENT_FORM_CONTROLS_CONFIGS: { [type: string]: BazaFormBuilderComponentFormControlConfig } = {};

/**
 * Configure baza-form-builder form control component
 *
 * @see BazaFormBuilderFieldType
 * @param type Unique Form Control identifier (usually BazaFormBuilderFieldType, but any other enums / strings can be used)
 * @param config Configuration for baza form component
 */
export function registerFormControl<T = BazaFormBuilderControlType>(type: T, config: BazaFormBuilderComponentFormControlConfig) {
    BAZA_FORM_BUILDER_COMPONENT_FORM_CONTROLS_CONFIGS[type as any] = config;

    bazaFormBuilderRegisterComponentType(type, BazaFormBuilderComponentType.FormControlComponent);
}

export function getBazaFormBuilderFormControlConfig<T = BazaFormBuilderControlType>(type: T) {
    const found = BAZA_FORM_BUILDER_COMPONENT_FORM_CONTROLS_CONFIGS[type as any];

    if (!found) {
        throw new Error(
            `[baza-form-builder] [getBazaFormBuilderFormControlConfig] Failed to get form control component configuration for "${type}"`,
        );
    }

    return found;
}
