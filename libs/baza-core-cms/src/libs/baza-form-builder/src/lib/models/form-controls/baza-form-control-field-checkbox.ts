import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderCheckboxComponent } from '../../form-builder/controls/baza-form-builder-checkbox/baza-form-builder-checkbox.component';

export interface BazaFormControlFieldCheckbox extends BazaFormControl {
    type: BazaFormBuilderControlType.Checkbox;
    props?: Partial<BazaFormBuilderCheckboxComponent>;
}

registerFormControl(BazaFormBuilderControlType.Checkbox, {
    component: BazaFormBuilderCheckboxComponent,
    enableNzFormContainer: false,
});
