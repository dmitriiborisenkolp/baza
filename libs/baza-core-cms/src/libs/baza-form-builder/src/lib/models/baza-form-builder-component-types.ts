import { BazaFormBuilderStaticComponentType } from './baza-form-builder-component-static';
import { BazaFormBuilderControlType } from './baza-form-builder-control';

type ComponentTypes = BazaFormBuilderStaticComponentType | BazaFormBuilderControlType;

export enum BazaFormBuilderComponentType {
    StaticComponent = 'StaticComponent',
    FormControlComponent = 'FormControlComponent',
}

const BAZA_FORM_BUILDER_COMPONENT_TYPES: { [type: string]: BazaFormBuilderComponentType } = {};

export function bazaFormBuilderRegisterComponentType<T = ComponentTypes>(type: T, componentType: BazaFormBuilderComponentType) {
    BAZA_FORM_BUILDER_COMPONENT_TYPES[type as any] = componentType;
}

export function bazaFormBuilderIsStaticComponent<T = ComponentTypes>(type: T): boolean {
    return BAZA_FORM_BUILDER_COMPONENT_TYPES[type as any] === BazaFormBuilderComponentType.StaticComponent;
}

export function bazaFormBuilderIsFormControlComponent<T = ComponentTypes>(type: T): boolean {
    return BAZA_FORM_BUILDER_COMPONENT_TYPES[type as any] === BazaFormBuilderComponentType.FormControlComponent;
}
