import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderUploadMultiComponent } from '../../form-builder/controls/baza-form-builder-upload';

export interface BazaFormControlFieldBazaUploadMulti extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaUploadMulti;
    props?: Partial<BazaFormBuilderUploadMultiComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaUploadMulti, {
    component: BazaFormBuilderUploadMultiComponent,
    enableNzFormContainer: true,
});
