import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';
import { BazaFormBuilderSeoComponent } from '../../form-builder/controls/baza-form-builder-seo';

export interface BazaFormControlFieldBazaSeo extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaSeo;
    props?: Partial<BazaFormBuilderSeoComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaSeo, {
    component: BazaFormBuilderSeoComponent,
    enableNzFormContainer: false,
});
