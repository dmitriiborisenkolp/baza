import { Observable } from 'rxjs';
import { ComponentType } from '@scaliolabs/baza-core-shared';
import { BazaFormBuilderComponentType, bazaFormBuilderRegisterComponentType } from './baza-form-builder-component-types';
import { TranslateService } from '@ngx-translate/core';

export enum BazaFormBuilderStaticComponentType {
    Divider = 'baza-component-divider',
    TimeStamp = 'baza-component-timestamp',
    Hint = 'baza-component-hint',
}

export interface BazaFormBuilderComponentStaticTransformServices {
    translate: TranslateService;
}

/**
 * FormControl-kind base interface for all form build fields.
 * Every new form controlled field should extends from BazaFormBuildComponentFormField interface
 */
export interface BazaFormBuilderComponentStaticOptions<C = any, T = BazaFormBuilderStaticComponentType> {
    // ID for trackBy

    id?: string;

    // Component Type

    type: T;
    props?: Partial<C>;

    // Visibility / ACL options

    // Hides field if user has no specific ACL
    acl?: Array<string>;

    // Field visibility. Tabs visibility depends on acl + given subject
    visible?: boolean | Observable<boolean>;
}

/**
 * Configuration for BazaFormBuilderComponentFormField component
 */
export interface BazaFormBuilderStaticComponentConfig<T = any> {
    /**
     * Component to render
     */
    component: ComponentType<T>;

    /**
     * Transform props before ngOnInit/ngOnChanges lifecycles
     */
    transformProps?: (
        self: Partial<T>,
        services: {
            translate: TranslateService;
        },
    ) => void;
}

/**
 * Configuration for BazaFormBuilderComponentFormField static-components stores here and being used in internal implementations
 * of baza-form-builder. Do not manually change it; use configureBazaFormComponent function to add new form control
 */
export const BAZA_FORM_BUILDER_STATIC_COMPONENT_CONFIGS: { [type: string]: BazaFormBuilderStaticComponentConfig } = {};

/**
 * Configure baza-form-builder form control component
 *
 * @see BazaFormBuilderFieldType
 * @param type Unique Form Control identifier (usually BazaFormBuilderFieldType, but any other enums / strings can be used)
 * @param config Configuration for baza form component
 */
export function registerStaticComponent<T = BazaFormBuilderStaticComponentType>(type: T, config: BazaFormBuilderStaticComponentConfig) {
    BAZA_FORM_BUILDER_STATIC_COMPONENT_CONFIGS[type as any] = config;

    bazaFormBuilderRegisterComponentType<any>(type, BazaFormBuilderComponentType.StaticComponent);
}

export function getBazaFormBuilderStaticComponentConfig<T = BazaFormBuilderStaticComponentType>(
    type: T,
): BazaFormBuilderStaticComponentConfig {
    const found = BAZA_FORM_BUILDER_STATIC_COMPONENT_CONFIGS[type as any];

    if (!found) {
        throw new Error(
            `[baza-form-builder] [getBazaFormBuilderStaticComponentConfig] Failed to get static component configuration for "${type}"`,
        );
    }

    return found;
}
