import { BazaFormBuilderUploadAttachmentMultiComponent } from '../../form-builder/controls/baza-form-builder-upload';
import { BazaFormControl, BazaFormBuilderControlType, registerFormControl } from '../baza-form-builder-control';

export interface BazaFormControlFieldBazaUploadAttachmentMulti extends BazaFormControl {
    type: BazaFormBuilderControlType.BazaUploadAttachmentMulti;
    props?: Partial<BazaFormBuilderUploadAttachmentMultiComponent>;
}

registerFormControl(BazaFormBuilderControlType.BazaUploadAttachmentMulti, {
    component: BazaFormBuilderUploadAttachmentMultiComponent,
    enableNzFormContainer: true,
});
