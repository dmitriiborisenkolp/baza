import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    forwardRef,
    Input,
    OnChanges,
    OnDestroy,
    Output,
    SimpleChanges,
    ViewChild,
    ViewContainerRef,
} from '@angular/core';
import {
    BazaFieldUploadPreset,
    bazaFieldUploadPresetExtensionsMap,
    BazaFileUploadOptions,
    BazaFileUploadTransport,
    defaultBazaFileUploadOptions,
} from './baza-form-builder-upload.model';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { Subject, throwError } from 'rxjs';
import { catchError, finalize, takeUntil } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AttachmentUploadResponse } from '@scaliolabs/baza-core-shared';
import { BazaAttachmentCacheService } from '../../../../../services/baza-attachment-cache.service';
import { validateUploadOptions } from '../../util/validate-upload-options';
import { BAZA_FORM_BUILDER_UPLOAD_SERVICES, BazaFormBuilderUploadService } from '../../services';

interface State {
    isDisabled: boolean;
    isDragOver: boolean;
}

@Component({
    selector: 'baza-field-upload',
    templateUrl: './baza-form-builder-upload.component.html',
    styleUrls: ['./baza-form-builder-upload.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderUploadComponent),
            multi: true,
        },
        ...BAZA_FORM_BUILDER_UPLOAD_SERVICES,
    ],
})
export class BazaFormBuilderUploadComponent implements ControlValueAccessor, OnChanges, OnDestroy {
    @ViewChild('fileInput', { read: ElementRef }) fileInput: ElementRef;

    @Input() withOptions: Partial<BazaFileUploadOptions>;

    @Input() enableMultiControls: boolean;
    @Input() multiIsFirstControl: boolean;
    @Input() multiIsLastControl: boolean;

    @Output('onAttachment') onAttachmentEvent: EventEmitter<AttachmentUploadResponse | undefined> = new EventEmitter<
        AttachmentUploadResponse | undefined
    >();

    @Output('onMultiRemove') onMultiRemoveEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('onMultiMoveUp') onMultiMoveUpEvent: EventEmitter<void> = new EventEmitter<void>();
    @Output('onMultiMoveDown') onMultiMoveDownEvent: EventEmitter<void> = new EventEmitter<void>();

    public state: State = {
        isDisabled: false,
        isDragOver: false,
    };
    public uploadLoading = this.loading.newInstance();
    public previewLoading = this.loading.newInstance();

    private _fileUrl: string;
    private _fileContentType: string;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    private nextWriteValue$: Subject<void> = new Subject<void>();
    private nextUpload$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly vcr: ViewContainerRef,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly httpClient: HttpClient,
        private readonly cache: BazaAttachmentCacheService,
        private readonly uploadService: BazaFormBuilderUploadService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['withOptions']) {
            validateUploadOptions(this.options);
        }
    }

    ngOnDestroy(): void {
        this.nextUpload$.next();
        this.nextWriteValue$.next();
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldUpload.${input}`;
    }

    get options(): BazaFileUploadOptions {
        return {
            ...defaultBazaFileUploadOptions(),
            ...this.withOptions,
        };
    }

    get title(): string {
        switch (this.options.preset) {
            default: {
                return this.i18n('dragFile');
            }

            case BazaFieldUploadPreset.Image:
            case BazaFieldUploadPreset.ImageAndWebp: {
                return this.i18n('dragImage');
            }

            case BazaFieldUploadPreset.Video: {
                return this.i18n('dragVideo');
            }

            case BazaFieldUploadPreset.Document: {
                return this.i18n('dragDocument');
            }
        }
    }

    get extensions(): Array<string> {
        if (this.options.extensions) {
            return this.options.extensions;
        } else if (this.options.preset) {
            const found = bazaFieldUploadPresetExtensionsMap.find((e) => e.preset === this.options.preset);

            return found ? found.extensions : [];
        } else {
            return [];
        }
    }

    get accepts(): string {
        const extensions = this.extensions;

        return Array.isArray(extensions) && extensions.length > 0 ? extensions.map((ext) => `.${ext}`).join(',') : undefined;
    }

    get previewUrl(): string {
        return this._fileUrl;
    }

    get previewContentType(): string {
        return this._fileContentType;
    }

    get hasUploadedFile(): boolean {
        return !!this._fileUrl;
    }

    onFileInputChange($event: Event): void {
        this.nextUpload$.next();

        const target = $event.target as HTMLInputElement;

        if (!target.files.length) {
            return;
        }

        this.upload(target.files);
    }

    upload(files: FileList): void {
        const localLoading = this.uploadLoading.addLoading();

        this.uploadService
            .upload({
                files,
                vcr: this.vcr,
                options: this.options,
                takeUntil$: this.ngOnDestroy$,
                extensions: this.extensions,
            })
            .pipe(
                catchError((error) => {
                    this.ngMessages.error({ message: error.message || JSON.stringify(error) });

                    return throwError(() => error);
                }),
                finalize(() => localLoading.complete()),
                finalize(() =>
                    setTimeout(() => {
                        this.cdr.detectChanges();
                    }),
                ),
            )
            .subscribe(
                (response) => {
                    response.forEach((next) => {
                        this._fileUrl = next.presignedTimeLimitedUrl;
                        this._fileContentType = next.contentType;

                        this.onChange(next.s3ObjectId);

                        if (next.attachment) {
                            this.onAttachmentEvent.next(next.attachment);
                        }
                    });
                },
                () => {
                    this.ngMessages.error({
                        message: this.i18n('failed'),
                        translate: true,
                    });
                },
            );

        setTimeout(() => {
            if (this.fileInput?.nativeElement) {
                this.fileInput.nativeElement.value = null;
            }
        });
    }

    deleteFile(): void {
        if (this.enableMultiControls) {
            this.onMultiRemoveEvent.emit(undefined);
        } else {
            this._fileUrl = undefined;
            this.onChange(undefined);

            if (this.options.transport.type === BazaFileUploadTransport.BazaAttachment) {
                this.onAttachmentEvent.emit(undefined);
            }
        }
    }

    onDragEnter($event: DragEvent): void {
        $event.preventDefault();

        this.state = {
            ...this.state,
            isDragOver: true,
        };

        this.cdr.markForCheck();
    }

    onDragOver($event: DragEvent): void {
        $event.preventDefault();

        this.state = {
            ...this.state,
            isDragOver: true,
        };

        this.cdr.markForCheck();
    }

    onDragLeave($event: DragEvent): void {
        $event.preventDefault();

        this.state = {
            ...this.state,
            isDragOver: false,
        };

        this.cdr.markForCheck();
    }

    onDrag($event: DragEvent): void {
        $event.preventDefault();

        this.state = {
            ...this.state,
            isDragOver: false,
        };

        const files = $event.dataTransfer.files;

        if (files.length) {
            this.upload(files);
        }

        this.cdr.markForCheck();
    }

    writeValue(value: string): void {
        this.nextWriteValue$.next();

        if (value) {
            const loading = this.previewLoading.addLoading();
            const cachedFile = this.cache.getCache(value);

            cachedFile
                .pipe(
                    finalize(() => loading.complete()),
                    finalize(() => this.cdr.markForCheck()),
                    takeUntil(this.ngOnDestroy$),
                    takeUntil(this.nextWriteValue$),
                )
                .subscribe((response) => {
                    this._fileUrl = response.url;
                    this._fileContentType = response.contentType;

                    this.cdr.markForCheck();
                });
        } else {
            this._fileUrl = undefined;
        }

        this.cdr.markForCheck();
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.state = {
            ...this.state,
            isDisabled,
        };

        this.cdr.markForCheck();
    }
}
