import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormArray, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { BazaSocialNetworks, BazaSocialNetworkDto, BazaSocialNetworksDto } from '@scaliolabs/baza-core-shared';

function isEmpty(input): boolean {
    return input === null || input === undefined || (input || '').toString().trim().length === 0;
}

interface FormValue {
    networks: BazaSocialNetworksDto;
}

interface State {
    form: FormGroup;
}

interface Value {
    value: string;
    label: BazaSocialNetworks;
}

export { FormValue as BazaFomBuilderNetworksDto };

@Component({
    selector: 'baza-field-networks',
    templateUrl: './baza-form-builder-networks.component.html',
    styleUrls: ['./baza-form-builder-networks.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderNetworksComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderNetworksComponent implements ControlValueAccessor, OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    public networkFields: Array<Value> = [
        {
            value: 'linkedin',
            label: BazaSocialNetworks.LinkedIn,
        },
        {
            value: 'twitter',
            label: BazaSocialNetworks.Twitter,
        },
        {
            value: 'github',
            label: BazaSocialNetworks.GitHub,
        },
        {
            value: 'other',
            label: BazaSocialNetworks.Other,
        },
    ];

    public state: State = {
        form: this.fb.group({
            networks: this.fb.array([]),
        }),
    };

    constructor(private readonly fb: FormBuilder, private readonly cdr: ChangeDetectorRef) {}

    ngOnInit(): void {
        this.state.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            if (this.onChange) {
                this.onChange(next);
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldNetworks.${input}`;
    }

    get networkForms(): FormArray {
        return this.state.form.get('networks') as FormArray;
    }

    castCurrentForm(form: AbstractControl): FormGroup {
        return form as FormGroup;
    }

    addNetwork(): void {
        this.cleanUpNetworks();

        this.networkForms.push(
            this.fb.group({
                name: [],
                url: [],
            }),
        );
    }

    deleteNetwork(index: number): void {
        this.cleanUpNetworks();
        this.networkForms.removeAt(index);
        this.cdr.markForCheck();
    }

    cleanUpNetworks(): void {
        for (let i = 0; i < this.networkForms.length; i++) {
            const formValue: BazaSocialNetworkDto = this.networkForms.controls[`${i}`].value;

            if (isEmpty(formValue.name) && isEmpty(formValue.url)) {
                this.networkForms.removeAt(i);

                return this.cleanUpNetworks();
            }
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(input: FormValue): void {
        const network = input ? input : this.networksDefault();

        while (this.networkForms.length) {
            this.networkForms.removeAt(0);
        }

        if (Array.isArray(network) && network.length > 0) {
            for (const item of network) {
                this.addNetwork();
                this.networkForms.at(this.networkForms.length - 1).patchValue(item);
            }
        }

        this.state.form.patchValue(network);

        this.cleanUpNetworks();

        if (this.networkForms.length === 0) {
            this.addNetwork();
        }

        this.cdr.markForCheck();
    }

    networksDefault() {
        return {
            networks: [],
        };
    }
}
