import { AttachmentUploadRequest } from '@scaliolabs/baza-core-shared';

export enum BazaFieldUploadPreset {
    Image = 'image',
    ImageAndWebp = 'image-and-webps',
    Video = 'video',
    Document = 'document',
}

export enum BazaFieldUploadImageRatioHandler {
    /**
     * Image will be cropped automatically by BE if Ratio option is passed
     */
    Backend = 'backend',

    /**
     * User will be able to crop image with CMS
     * If Ratio option is passed, image will fit with specific ratio
     */
    CropperJS = 'cropper-js',

    /**
     * Image will not be cropped by BE or CMS
     * Use this option to disable Cropper.JS dialog at all
     */
    Disabled = 'disabled',
}

export const BAZA_FIELD_UPLOAD_EXTS_IMAGE = ['png', 'jpg', 'jpeg', 'gif'];
export const BAZA_FIELD_UPLOAD_MIME_IMAGE = ['image/png', 'image/jpeg', 'image/gif'];

export const BAZA_FIELD_UPLOAD_EXTS_IMAGE_AND_WEBP = [...BAZA_FIELD_UPLOAD_EXTS_IMAGE, 'webp'];
export const BAZA_FIELD_UPLOAD_MIME_IMAGE_AND_WEBP = [...BAZA_FIELD_UPLOAD_MIME_IMAGE, 'image/webp'];

export const BAZA_FIELD_UPLOAD_EXTS_VIDEO = ['mp4'];
export const BAZA_FIELD_UPLOAD_MIME_VIDEO = ['video/mp4'];

export const BAZA_FIELD_UPLOAD_EXTS_DOCUMENT = ['pdf', 'doc', 'docx', 'xls', 'xlsx'];
export const BAZA_FIELD_UPLOAD_MIME_DOCUMENT = [
    'application/pdf',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.ms-excel',
    '\tapplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
];

export const BAZA_MULTI_FILE_UPLOAD_LIMIT = 100;

export const bazaFieldUploadPresetExtensionsMap: Array<{
    preset: BazaFieldUploadPreset;
    extensions: Array<string>;
    mime: Array<string>;
}> = [
    {
        preset: BazaFieldUploadPreset.Image,
        extensions: BAZA_FIELD_UPLOAD_EXTS_IMAGE,
        mime: BAZA_FIELD_UPLOAD_MIME_IMAGE,
    },
    {
        preset: BazaFieldUploadPreset.ImageAndWebp,
        extensions: BAZA_FIELD_UPLOAD_EXTS_IMAGE_AND_WEBP,
        mime: BAZA_FIELD_UPLOAD_MIME_IMAGE_AND_WEBP,
    },
    {
        preset: BazaFieldUploadPreset.Video,
        extensions: BAZA_FIELD_UPLOAD_EXTS_VIDEO,
        mime: BAZA_FIELD_UPLOAD_MIME_VIDEO,
    },
    {
        preset: BazaFieldUploadPreset.Document,
        extensions: BAZA_FIELD_UPLOAD_EXTS_DOCUMENT,
        mime: BAZA_FIELD_UPLOAD_MIME_DOCUMENT,
    },
];

export enum BazaFileUploadTransport {
    BazaAWS = 'BazaAWS',
    BazaAttachment = 'BazaAttachment',
}

export type BazaFileUploadTransportOptions =
    | { type: BazaFileUploadTransport.BazaAWS }
    | {
          type: BazaFileUploadTransport.BazaAttachment;
          options: (file: File, extension: string) => AttachmentUploadRequest;
      };

export interface BazaFileUploadOptions {
    /**
     * Transport - Baza AWS (direct upload) or Baza Attachment (upload & file pre-processing)
     * Baza AWS used as default
     */
    transport: BazaFileUploadTransportOptions;

    /**
     * Preset for extensions
     * By default all files allowed to upload
     */
    preset?: BazaFieldUploadPreset;

    /**
     * Custom extensions list
     * By default all files allowed to upload
     */
    extensions?: Array<string>;

    /**
     * Additional message for user
     */
    message?: {
        text: string;
        translate: boolean;
        translateArgs?: any;
    };

    /**
     * Maximum files size in bytes
     */
    maxFileSizeBytes?: number;

    /**
     * Maximum number of files uploaded at once
     * By default 100 are allowed
     */
    maxMultiFileUploadLimit?: number;

    /**
     * Enable / Disable multiselect files in file dialog
     * This option is not working with BazaFieldUploadImageRatioHandler.CropperJS option!
     */
    multiselect?: boolean;

    /**
     * Ratio for image to be cropped
     */
    ratio?: number;

    /**
     * Use API or Cropper.JS plugin to crop images?
     */
    ratioHandler?: BazaFieldUploadImageRatioHandler;
}

export function defaultBazaFileUploadOptions(): BazaFileUploadOptions {
    return {
        transport: {
            type: BazaFileUploadTransport.BazaAWS,
        },
        maxMultiFileUploadLimit: BAZA_MULTI_FILE_UPLOAD_LIMIT,
        ratioHandler: BazaFieldUploadImageRatioHandler.CropperJS,
        multiselect: false,
    };
}
