import { ChangeDetectionStrategy, Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { BazaCkEditorOptions } from '../../../../../../baza-ck-editor/src';

interface State {
    form: FormGroup;
}

interface FormValue {
    value: string;
}

@Component({
    selector: 'baza-field-html',
    templateUrl: './baza-form-builder-html.component.html',
    styleUrls: ['./baza-form-builder-html.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderHtmlComponent),
            multi: true
        }
    ],
})
export class BazaFormBuilderHtmlComponent implements ControlValueAccessor, OnInit, OnDestroy {
    @Input() options: Partial<BazaCkEditorOptions>;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            value: [''],
        }),
    };

    constructor(
        private readonly fb: FormBuilder,
    ) {}

    ngOnInit(): void {
        this.state.form.valueChanges.pipe(
            distinctUntilChanged(),
            takeUntil(this.ngOnDestroy$),
        ).subscribe((next: FormValue) => {
            if (this.onChange) {
                this.onChange(next.value);
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    writeValue(value: any): void {
        setTimeout(() => {
            this.state.form.patchValue({
                value,
            });
        });
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled
            ? this.state.form.disable()
            : this.state.form.enable();
    }
}
