import { ChangeDetectionStrategy, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BazaFormControlFieldCheckbox } from '../../../models/form-controls/baza-form-control-field-checkbox';

@Component({
    templateUrl: './baza-form-builder-checkbox.component.html',
    styleUrls: ['./baza-form-builder-checkbox.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderCheckboxComponent),
            multi: true
        }
    ],
})
export class BazaFormBuilderCheckboxComponent implements ControlValueAccessor {
    @Input() formGroup: FormGroup;
    @Input() formControlName: string;

    @Input() definition: BazaFormControlFieldCheckbox;
    @Input() label: string;
    @Input() placeholder: string;

    // TODO: There should be more clear way how to handle directives

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    writeValue(obj: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    registerOnChange(fn: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    registerOnTouched(fn: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setDisabledState(isDisabled: boolean): void {}
}
