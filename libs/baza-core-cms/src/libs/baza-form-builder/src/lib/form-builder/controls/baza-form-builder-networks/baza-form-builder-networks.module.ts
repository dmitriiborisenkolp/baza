import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { TranslateModule } from '@ngx-translate/core';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { CommonModule } from '@angular/common';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { BazaFormBuilderNetworksComponent } from './baza-form-builder-networks.component';
import { NzSelectModule } from 'ng-zorro-antd/select';

@NgModule({
    imports: [
        ReactiveFormsModule,
        NzFormModule,
        NzInputModule,
        TranslateModule,
        NzTypographyModule,
        NzDividerModule,
        NzSpaceModule,
        NzButtonModule,
        CommonModule,
        NzIconModule,
        NzSelectModule,
    ],
    declarations: [
        BazaFormBuilderNetworksComponent,
    ],
    exports: [
        BazaFormBuilderNetworksComponent,
    ],
})
export class BazaFormBuilderNetworksModule {}
