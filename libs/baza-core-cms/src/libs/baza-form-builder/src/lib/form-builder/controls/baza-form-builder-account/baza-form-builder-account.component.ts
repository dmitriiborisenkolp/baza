import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    forwardRef,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewContainerRef,
} from '@angular/core';
import { BazaAccountCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { AccountDto } from '@scaliolabs/baza-core-shared';
import { of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, finalize, map, switchMap, takeUntil } from 'rxjs/operators';
import { isEmpty } from '@scaliolabs/baza-core-shared';
import { BazaLoadingService } from '@scaliolabs/baza-core-ng';
import { BazaFieldAccountComponentOptions, BazaFieldAccountComponentSource } from './baza-form-builder-account.models';

interface FormValue {
    search: string;
    selected: AccountDto;
}

interface State {
    form: FormGroup;
    options: Array<AccountDto>;
}

const DEBOUNCE_TIME_MS = 100;

@Component({
    selector: 'baza-field-account',
    templateUrl: './baza-form-builder-account.component.html',
    styleUrls: ['./baza-form-builder-account.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderAccountComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderAccountComponent implements OnInit, OnDestroy, ControlValueAccessor {
    @Input() options: BazaFieldAccountComponentOptions;
    @Input() title: string;
    @Input() nzRequired: boolean;
    @Input() formControlName: string;
    @Input() placeholder: string;

    @Output() searchModal: EventEmitter<AccountDto> = new EventEmitter<AccountDto>();

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    private disableTriggerSearch = false;

    public state: State = {
        form: this.fb.group({
            search: [],
            selected: [undefined, [Validators.required]],
        }),
        options: [],
    };

    public compareFun = (o1: AccountDto | string, o2: AccountDto) => {
        if (o1) {
            return typeof o1 === 'string' ? o1 === o2.fullName : o1.fullName === o2.fullName;
        } else {
            return false;
        }
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly vcr: ViewContainerRef,
        private readonly accountEndpoint: BazaAccountCmsDataAccess,
        private readonly loading: BazaLoadingService,
    ) {}

    ngOnInit(): void {
        this.state.form
            .get('search')
            .valueChanges.pipe(
                distinctUntilChanged(),
                filter(() => !this.disableTriggerSearch),
                switchMap((queryString) => {
                    if (isEmpty(queryString)) {
                        return of([]);
                    } else {
                        const loading = this.loading.addLoading();

                        const source: BazaFieldAccountComponentSource =
                            this.options && this.options.source
                                ? this.options.source
                                : (request) => this.accountEndpoint.listAccounts(request);

                        return source({
                            queryString: typeof queryString === 'string' && !isEmpty(queryString) ? queryString : undefined,
                        }).pipe(
                            map((response) => response.items),
                            finalize(() => loading.complete()),
                        );
                    }
                }),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((options: Array<AccountDto>) => {
                this.state = {
                    ...this.state,
                    options,
                };

                this.cdr.markForCheck();
            });

        this.state.form
            .get('search')
            .valueChanges.pipe(distinctUntilChanged(), debounceTime(DEBOUNCE_TIME_MS), takeUntil(this.ngOnDestroy$))
            .subscribe((next) => {
                if (!!next && typeof next == 'object' && !!(next as AccountDto).id) {
                    this.patchFormValue = {
                        selected: next,
                    };

                    this.onChange(next);
                } else {
                    const isSameAccount =
                        typeof next === 'string' &&
                        !isEmpty(next) &&
                        !!this.formValue.selected &&
                        this.formValue.search === this.formValue.selected.fullName;

                    if (!isSameAccount) {
                        this.patchFormValue = {
                            selected: undefined,
                        };

                        this.onChange(undefined);
                    }
                }
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    set patchFormValue(patch: Partial<FormValue>) {
        this.disableTriggerSearch = true;

        this.state.form.patchValue(patch);

        this.disableTriggerSearch = false;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(obj: any): void {
        if (!!obj && typeof obj === 'object' && !!(obj as AccountDto).id) {
            this.patchFormValue = {
                selected: obj,
                search: (obj as AccountDto).fullName,
            };
        } else {
            this.patchFormValue = {
                selected: undefined,
                search: undefined,
            };
        }
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }

    search(): void {
        if (this.options.searchModal) {
            this.options.searchModal(this.formValue.selected).then((next) => {
                this.patchFormValue = {
                    selected: next,
                    search: next ? next.fullName : undefined,
                };

                if (this.onChange) {
                    this.onChange(this.formValue.selected);
                }
            });
        }

        this.searchModal.emit();
    }
}
