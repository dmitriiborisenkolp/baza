import { NgModule } from '@angular/core';
import { BazaFormBuilderMailRecipientComponent } from './baza-form-builder-mail-recipient.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { TranslateModule } from '@ngx-translate/core';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { BazaNgCoreModule } from '@scaliolabs/baza-core-ng';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NzFormModule,
        NzInputModule,
        TranslateModule,
        NzDividerModule,
        NzButtonModule,
        NzIconModule,
        BazaNgCoreModule
    ],
    declarations: [
        BazaFormBuilderMailRecipientComponent,
    ],
    exports: [
        BazaFormBuilderMailRecipientComponent,
    ],
})
export class BazaFormBuilderMailRecipientModule {}
