import { ComponentFactoryResolver, Injectable, ReflectiveInjector } from '@angular/core';
import { BazaFormBuilderUploadRequest, BazaFormBuilderUploadResponse } from './upload-file.service';
import { Observable, Subject } from 'rxjs';
import { BazaFormBuilderUploadQueueComponent } from '../components/upload-queue/upload-queue.component';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class BazaFormBuilderUploadQueueService {
    constructor(private readonly componentFactoryResolver: ComponentFactoryResolver) {}

    upload(request: BazaFormBuilderUploadRequest): Observable<Array<BazaFormBuilderUploadResponse>> {
        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaFormBuilderUploadQueueComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], request.vcr.injector);

        const component = factory.create(injector);
        const componentDestroy$ = new Subject<void>();

        component.instance.request = request;

        component.instance.onClose.pipe(takeUntil(componentDestroy$)).subscribe(() => {
            component.destroy();
            componentDestroy$.next();
        });

        request.vcr.insert(component.hostView);

        return component.instance.upload(request);
    }
}
