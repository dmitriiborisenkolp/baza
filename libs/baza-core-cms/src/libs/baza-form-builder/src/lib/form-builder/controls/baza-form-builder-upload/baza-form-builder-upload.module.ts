import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaFormBuilderUploadComponent } from './components/upload-single/baza-form-builder-upload.component';
import { BazaFieldUploadPreviewFileComponent } from './components/preview/preview-file/preview-file.component';
import { BazaFieldUploadPreviewImageComponent } from './components/preview/preview-image/preview-image.component';
import { BazaFieldUploadPreviewVideoComponent } from './components/preview/preview-video/preview-video.component';
import { BazaAwsDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaAttachmentDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { BazaFieldUploadPreviewComponent } from './components/preview/preview.component';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { BazaFormBuilderUploadMultiComponent } from './components/upload-multi/upload-multi.component';
import { BazaFormBuilderUploadAttachmentSingleComponent } from './components/upload-attachment-single/upload-attachment-single.component';
import { BazaFormBuilderUploadAttachmentMultiComponent } from './components/upload-attachment-multi/upload-attachment-multi.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaFormBuilderUploadImageComponent } from './components/upload-image/upload-image.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { BazaFormBuilderUploadImageMultiComponent } from './components/upload-image-multi/upload-image-multi.component';
import { BazaFormBuilderUploadCropperComponent } from './components/upload-cropper/upload-cropper.component';
import { BazaFormBuilderUploadQueueComponent } from './components/upload-queue/upload-queue.component';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BazaAwsDataAccessModule,
        BazaAttachmentDataAccessModule,
        NzButtonModule,
        NzDividerModule,
        NzIconModule,
        NzImageModule,
        NzModalModule,
        NzProgressModule,
        NzSpaceModule,
        TranslateModule,
        HttpClientModule,
        NzSpinModule,
        NzToolTipModule,
    ],
    declarations: [
        BazaFormBuilderUploadComponent,
        BazaFormBuilderUploadMultiComponent,
        BazaFormBuilderUploadAttachmentSingleComponent,
        BazaFormBuilderUploadAttachmentMultiComponent,
        BazaFieldUploadPreviewComponent,
        BazaFieldUploadPreviewFileComponent,
        BazaFieldUploadPreviewImageComponent,
        BazaFieldUploadPreviewVideoComponent,
        BazaFormBuilderUploadImageComponent,
        BazaFormBuilderUploadImageMultiComponent,
        BazaFormBuilderUploadCropperComponent,
        BazaFormBuilderUploadQueueComponent,
    ],
    exports: [
        BazaFormBuilderUploadComponent,
        BazaFormBuilderUploadMultiComponent,
        BazaFormBuilderUploadImageComponent,
        BazaFormBuilderUploadImageMultiComponent,
    ],
})
export class BazaFormBuilderUploadModule {}
