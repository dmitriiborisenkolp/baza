import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormArray, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

type FormControlValue = Record<string, string>;

interface FormEntryValue {
    key: string;
    value: string;
}

interface State {
    form: FormArray;
}

function formToFormControlValue(input: FormArray): FormControlValue {
    const result: FormControlValue = {};

    for (let i = 0; i < input.length; i++) {
        if (input.controls[`${i}`].valid) {
            const entry: FormEntryValue = input.controls[`${i}`].value;

            result[entry.key] = entry.value;
        }
    }

    return result;
}

@Component({
    templateUrl: './baza-form-builder-key-value.component.html',
    styleUrls: ['./baza-form-builder-key-value.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderKeyValueComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderKeyValueComponent implements ControlValueAccessor, OnInit, OnDestroy {
    @Input() keyPlaceholder: string = this.i18n('key');
    @Input() valuePlaceholder: string = this.i18n('value');

    @Input() maxKeyLength = 32;
    @Input() maxEntries = 20;

    public state: State = {
        form: this.fb.array([]),
    };

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    constructor(private readonly fb: FormBuilder, private readonly cdr: ChangeDetectorRef) {}

    ngOnInit(): void {
        this.addEntry();

        this.state.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe(() => {
            setTimeout(() => {
                this.onChange(formToFormControlValue(this.state.form));
            });
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldKeyValue.${input}`;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(obj: FormControlValue): void {
        while (this.state.form.length) {
            this.state.form.removeAt(0);
        }

        if (!!obj && typeof obj === 'object') {
            for (const key of Object.keys(obj)) {
                this.addEntry({
                    key,
                    value: obj[`${key}`],
                });
            }
        }

        if (!this.state.form.length) {
            this.addEntry();
        }

        this.cdr.markForCheck();
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();

        this.cdr.markForCheck();
    }

    addEntry(entryValue?: FormEntryValue): void {
        if (this.state.form.disabled) {
            return;
        }

        const entry = this.fb.group({
            key: ['', [Validators.required, Validators.maxLength(this.maxKeyLength)]],
            value: ['', [Validators.required]],
        });

        if (entryValue) {
            entry.patchValue(entryValue);
        }

        this.state.form.push(entry);
    }

    removeEntry(entryIndex: number): void {
        if (this.state.form.disabled) {
            return;
        }

        this.state.form.removeAt(entryIndex);

        if (!this.state.form.length) {
            this.addEntry();
        }
    }

    castCurrentForm(form: AbstractControl): FormGroup {
        return form as FormGroup;
    }

    get canAddEntry(): boolean {
        return this.state.form.valid && this.state.form.length < this.maxEntries;
    }

    get hasMaximumEntries(): boolean {
        return this.state.form.length >= this.maxEntries;
    }
}
