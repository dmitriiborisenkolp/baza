import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
    selector: 'baza-field-upload-preview-video',
    templateUrl: './preview-video.component.html',
    styleUrls: ['./preview-video.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFieldUploadPreviewVideoComponent {
    @Input() url: string;

    constructor(private readonly domSanitizer: DomSanitizer) {}

    get trustedUrl(): SafeUrl {
        return this.domSanitizer.bypassSecurityTrustUrl(this.url);
    }
}
