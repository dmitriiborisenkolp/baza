import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    templateUrl: './baza-form-builder-component-hint.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormBuilderComponentHintComponent {
    @Input() text: string;
}
