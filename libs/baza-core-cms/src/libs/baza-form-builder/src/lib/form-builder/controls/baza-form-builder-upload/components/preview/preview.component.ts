import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

export enum BazaFileUploadType {
    File = 'file',
    Video = 'video',
    Image = 'image',
}

const mapContentTypesToPreview: Array<{
    type: BazaFileUploadType;
    contentTypes: Array<string>;
}> = [
    {
        type: BazaFileUploadType.Image,
        contentTypes: ['image/jpeg', 'image/webp', 'image/png', 'image/gif', 'image/bmp'],
    },
    {
        type: BazaFileUploadType.Video,
        contentTypes: ['video/mp4', 'video/quicktime'],
    },
];

@Component({
    selector: 'baza-field-upload-preview',
    templateUrl: './preview.component.html',
    styleUrls: ['./preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFieldUploadPreviewComponent {
    @Input() url: string;
    @Input() contentType: string;
    @Input() disabled: boolean;

    get type(): BazaFileUploadType {
        const found = mapContentTypesToPreview.find((def) => def.contentTypes.includes(this.contentType));

        return found ? found.type : BazaFileUploadType.File;
    }
}
