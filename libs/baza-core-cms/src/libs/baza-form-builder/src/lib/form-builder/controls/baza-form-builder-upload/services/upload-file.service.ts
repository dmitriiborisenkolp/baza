import { Injectable, NgZone, ViewContainerRef } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { BazaFileUploadOptions, BazaFileUploadTransport } from '../components/upload-single/baza-form-builder-upload.model';
import { finalize, map, switchMap, takeUntil } from 'rxjs/operators';
import { BazaAttachmentDataAccess, BazaAwsDataAccess } from '@scaliolabs/baza-core-data-access';
import { BazaAttachmentCacheService } from '../../../../services/baza-attachment-cache.service';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { AttachmentDto } from '@scaliolabs/baza-core-shared';

export interface BazaFormBuilderUploadRequest {
    files: FileList;
    options: BazaFileUploadOptions;
    takeUntil$: Subject<void>;
    vcr: ViewContainerRef;
    extensions: Array<string>;
}

export interface BazaFormBuilderUploadResponse {
    s3ObjectId: string;
    presignedTimeLimitedUrl: string;
    contentType: string;
    attachment?: AttachmentDto;
}

@Injectable()
export class BazaFormBuilderUploadFileService {
    constructor(
        private readonly ngZone: NgZone,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly awsNgEndpoint: BazaAwsDataAccess,
        private readonly attachmentNgEndpoint: BazaAttachmentDataAccess,
        private readonly cacheService: BazaAttachmentCacheService,
    ) {}

    upload(
        file: File,
        request: BazaFormBuilderUploadRequest,
        callbacks = {
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            beforeStart: () => {},
        },
    ): Observable<Array<BazaFormBuilderUploadResponse>> {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return new Observable((observer) => {
            callbacks.beforeStart();

            const { options, extensions, takeUntil$ } = request;

            if (!file) {
                return of([]);
            }

            if (options.maxFileSizeBytes && file.size > options.maxFileSizeBytes) {
                this.ngMessages.error({
                    message: this.i18n('maxFileSizeError'),
                    translate: true,
                    translateParams: {
                        maxSizeMb: Math.ceil(options.maxFileSizeBytes / 1024 / 1024),
                    },
                });

                return of([]);
            }

            if (Array.isArray(extensions) && extensions.length > 0) {
                const split = file.name.split('.');
                const ext = split[split.length - 1];

                if (!extensions.some((compare) => compare.toLowerCase() === (ext || '').toLowerCase())) {
                    this.ngMessages.error({
                        message: this.i18n('fileTypeError'),
                        translate: true,
                        translateParams: {
                            maxSizeMb: Math.ceil(options.maxFileSizeBytes / 1024 / 1024),
                        },
                    });

                    return of([]);
                }
            }

            const type = options.transport.type;

            switch (options.transport.type) {
                default: {
                    throw new Error(`Invalid transport "${type}"`);
                }

                case BazaFileUploadTransport.BazaAWS: {
                    const loading = this.loading.addLoading();

                    return this.awsNgEndpoint
                        .upload(file)
                        .pipe(
                            switchMap((response) => {
                                return this.cacheService.getCache(response.s3ObjectId).pipe(
                                    map((cacheResponse) => ({
                                        response,
                                        cacheResponse,
                                    })),
                                );
                            }),
                            finalize(() =>
                                this.ngZone.run(() => {
                                    loading.complete();
                                }),
                            ),
                            takeUntil(takeUntil$),
                        )
                        .subscribe(
                            (result) => {
                                observer.next([
                                    {
                                        s3ObjectId: result.response.s3ObjectId,
                                        presignedTimeLimitedUrl: result.cacheResponse.url,
                                        contentType: result.cacheResponse.contentType,
                                    },
                                ]);

                                observer.complete();
                            },
                            (err) => observer.error(err),
                        );
                }

                case BazaFileUploadTransport.BazaAttachment: {
                    const loading = this.loading.addLoading();

                    const parts = (file.name || '').split('.');
                    const ext = parts[parts.length - 1];

                    return this.attachmentNgEndpoint
                        .upload(file, options.transport.options(file, ext))
                        .pipe(
                            switchMap((response) => {
                                return this.cacheService.getCache(response.s3ObjectId).pipe(
                                    map((cacheResponse) => ({
                                        response,
                                        cacheResponse,
                                    })),
                                );
                            }),
                            finalize(() =>
                                this.ngZone.run(() => {
                                    loading.complete();
                                }),
                            ),
                            takeUntil(takeUntil$),
                        )
                        .subscribe(
                            (result) => {
                                observer.next([
                                    {
                                        s3ObjectId: result.response.s3ObjectId,
                                        presignedTimeLimitedUrl: result.cacheResponse.url,
                                        contentType: result.cacheResponse.contentType,
                                        attachment: result.response,
                                    },
                                ]);

                                observer.complete();
                            },
                            (err) => {
                                observer.error(err);
                            },
                        );
                }
            }
        });
    }

    private i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldUploadMulti.${input}`;
    }
}
