import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaFormBuilderAccountComponent } from './baza-form-builder-account.component';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzFormModule } from 'ng-zorro-antd/form';
import { BazaNgCoreModule } from '@scaliolabs/baza-core-ng';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NzInputModule,
        NzAutocompleteModule,
        NzFormModule,
        BazaNgCoreModule,
        NzIconModule,
        BazaNgCoreModule,
    ],
    declarations: [
        BazaFormBuilderAccountComponent,
    ],
    exports: [
        BazaFormBuilderAccountComponent,
    ],
})
export class BazaFormBuilderAccountModule {
}
