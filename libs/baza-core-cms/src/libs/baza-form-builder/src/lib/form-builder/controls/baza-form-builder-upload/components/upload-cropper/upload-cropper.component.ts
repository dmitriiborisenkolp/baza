import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, Renderer2 } from '@angular/core';
import { BazaLoadingService } from '@scaliolabs/baza-core-ng';
import Cropper from 'cropperjs';
import { BazaFileUploadOptions } from '../upload-single/baza-form-builder-upload.model';

@Component({
    templateUrl: './upload-cropper.component.html',
    styleUrls: ['./upload-cropper.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormBuilderUploadCropperComponent {
    @Input() options: BazaFileUploadOptions;
    @Input() selectedFile: File;

    @Output() onUpload = new EventEmitter<File>();
    @Output() onClose = new EventEmitter<void>();

    public isNgModalVisible = true;

    private cropperInstance = null;
    private cropperLoading = this.loading.newInstance();

    constructor(private readonly loading: BazaLoadingService, private readonly renderer2: Renderer2) {}

    i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldUpload.cropper.${input}`;
    }

    cropModalOpened() {
        const cropLoading = this.cropperLoading.addLoading();

        this.convertFileToImage(this.selectedFile).then((result) => {
            // Load selected image file in the modal
            const imgEl = document.createElement('img');

            imgEl.style.maxWidth = '100%';
            imgEl.style.display = 'none';

            imgEl.setAttribute('class', 'selected-image');
            imgEl.setAttribute('src', result as string);

            this.renderer2.appendChild(document.querySelector('.img-container'), imgEl);

            this.cropperInstance = new Cropper(document.querySelector('.selected-image'), {
                viewMode: 1,
                autoCropArea: 1,
                ready: () => {
                    cropLoading.complete();
                    this.cropperInstance.setAspectRatio(this.options.ratio);
                },
            });
        });
    }

    private convertFileToImage(file: File): Promise<string | ArrayBuffer> {
        return new Promise((resolve) => {
            const reader = new FileReader();

            reader.addEventListener('load', () => {
                resolve(reader.result);
            });

            reader.readAsDataURL(file);
        });
    }

    handleCropOk(): void {
        if (this.cropperInstance) {
            const croppedCanvas = this.cropperInstance.getCroppedCanvas();

            croppedCanvas.toBlob((blob) => {
                const processedFile = new File([blob], this.selectedFile.name, { type: this.selectedFile.type });

                this.onUpload.emit(processedFile);
            });

            this.cropperInstance = null;
        }

        this.isNgModalVisible = false;
    }

    handleCropCancel(): void {
        if (this.cropperInstance) {
            this.cropperInstance = null;
        }

        this.onClose.emit();

        this.isNgModalVisible = false;
    }
}
