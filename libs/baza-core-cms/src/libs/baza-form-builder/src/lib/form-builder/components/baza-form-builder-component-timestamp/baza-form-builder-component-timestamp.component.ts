import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    templateUrl: './baza-form-builder-component-timestamp.component.html',
    styleUrls: ['./baza-form-builder-component-timestamp.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormBuilderComponentTimestampComponent {
    @Input() created: Date;
    @Input() updated: Date;
}
