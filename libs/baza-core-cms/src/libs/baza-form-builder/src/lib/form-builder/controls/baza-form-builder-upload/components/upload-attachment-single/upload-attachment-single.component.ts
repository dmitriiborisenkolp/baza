import { ChangeDetectionStrategy, Component, forwardRef, Input, OnChanges, OnDestroy, SimpleChanges, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AttachmentDto, AttachmentUploadRequest } from '@scaliolabs/baza-core-shared';
import { Subject } from 'rxjs';
import { map, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { BazaFileUploadOptions, BazaFileUploadTransport } from '../upload-single/baza-form-builder-upload.model';

type S3ObjectId = string;

interface State {
    form: FormGroup;
    withOptions?: BazaFileUploadOptions;
}

interface FormValue {
    value: S3ObjectId; // AWS S3 ObjectId
}

@Component({
    selector: 'baza-field-upload-attachment-single',
    templateUrl: './upload-attachment-single.component.html',
    styleUrls: ['./upload-attachment-single.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderUploadAttachmentSingleComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderUploadAttachmentSingleComponent implements ControlValueAccessor, OnInit, OnChanges, OnDestroy {
    @Input() withOptions: AttachmentUploadRequest | { (file: File, extension: string): AttachmentUploadRequest };
    @Input() fileUploadOptions: Partial<Exclude<BazaFileUploadOptions, 'transport' | 'message'>>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    public state: State = {
        form: this.fb.group({
            value: [],
        }),
    };

    constructor(private readonly fb: FormBuilder) {}

    ngOnInit(): void {
        this.state.form.valueChanges
            .pipe(
                map((fv: FormValue) => fv.value),
                distinctUntilChanged(),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((value) => {
                if (this.onChange) {
                    this.onChange(value);
                }
            });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['withOptions']) {
            this.state = {
                ...this.state,
                withOptions: {
                    transport: {
                        type: BazaFileUploadTransport.BazaAttachment,
                        options: (file, extension) =>
                            typeof this.withOptions === 'function' ? this.withOptions(file, extension) : this.withOptions,
                    },
                    ...this.fileUploadOptions,
                },
            };
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    onAttachment(attachment: AttachmentDto): void {
        if (this.onChange) {
            this.onChange(attachment);
        }
    }

    writeValue(attachment: AttachmentDto | S3ObjectId): void {
        if (typeof attachment === 'object' && attachment?.s3ObjectId) {
            this.state.form.patchValue({ value: attachment.s3ObjectId } as FormValue);
        } else {
            this.state.form.patchValue({ value: attachment } as FormValue);
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
