import { CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { AccountDto } from '@scaliolabs/baza-core-shared';

type FormControlValue = AccountDto;
type Source = (request: CrudListRequestDto<AccountDto>) => Observable<CrudListResponseDto<AccountDto>>;

interface Options {
    source?: Source;
    searchModal?: (selected: AccountDto | undefined) => Promise<AccountDto | undefined>;
}

export { FormControlValue as BazaFieldAccountComponentFormControlValue };
export { Source as BazaFieldAccountComponentSource };
export { Options as BazaFieldAccountComponentOptions };
