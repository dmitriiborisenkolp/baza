import { ChangeDetectionStrategy, Component, forwardRef, Input, OnDestroy } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AttachmentDto, AttachmentImageSetUpload, AttachmentType } from '@scaliolabs/baza-core-shared';
import { Subject } from 'rxjs';
import {
    BazaFieldUploadImageRatioHandler,
    BazaFileUploadOptions,
    BazaFileUploadTransport,
} from '../upload-single/baza-form-builder-upload.model';

interface State {
    form: FormGroup;
    withOptions?: BazaFileUploadOptions;
}

interface FormValue {
    value: string; // AWS S3 ObjectId
}

@Component({
    selector: 'baza-field-upload-image',
    templateUrl: './upload-image.component.html',
    styleUrls: ['./upload-image.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderUploadImageComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderUploadImageComponent implements ControlValueAccessor, OnDestroy {
    @Input() multiselect = false;
    @Input() withImageOptions: Partial<AttachmentImageSetUpload>;
    @Input() withRatioHandler: BazaFieldUploadImageRatioHandler = BazaFieldUploadImageRatioHandler.CropperJS;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    public state: State = {
        form: this.fb.group({
            value: [],
        }),
    };

    constructor(private readonly fb: FormBuilder) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get withOptions(): BazaFileUploadOptions {
        return {
            transport: {
                type: BazaFileUploadTransport.BazaAttachment,
                options: () => ({
                    type: AttachmentType.ImageSet,
                    payload: {
                        ...this.withImageOptions,
                    },
                }),
            },
            ratio: this.withImageOptions?.ratio,
            ratioHandler: this.withRatioHandler,
            multiselect: this.multiselect,
        };
    }

    onAttachment(attachment: AttachmentDto): void {
        if (this.onChange) {
            this.onChange(attachment);
        }
    }

    writeValue(obj: AttachmentDto): void {
        if (obj && obj.s3ObjectId) {
            this.state.form.patchValue({
                value: obj.s3ObjectId,
            } as FormValue);
        } else {
            this.state.form.reset();
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
