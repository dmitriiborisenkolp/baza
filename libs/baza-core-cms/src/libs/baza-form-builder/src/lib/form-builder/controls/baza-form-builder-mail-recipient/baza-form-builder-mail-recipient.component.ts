import { ChangeDetectionStrategy, Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import {
    AbstractControl,
    ControlValueAccessor,
    FormArray,
    FormBuilder,
    FormGroup,
    NG_VALUE_ACCESSOR,
    ValidationErrors,
    ValidatorFn,
    Validators,
} from '@angular/forms';
import { BazaFormMailRecipientDto } from '@scaliolabs/baza-core-shared';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

interface FormValue {
    name: string;
    email: string;
    cc: Array<CCFormValue>;
}

interface CCFormValue {
    name: string;
    email: string;
}

interface State {
    form: FormGroup;
}

@Component({
    selector: 'baza-field-mail-recipient',
    templateUrl: './baza-form-builder-mail-recipient.component.html',
    styleUrls: ['./baza-form-builder-mail-recipient.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderMailRecipientComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderMailRecipientComponent implements ControlValueAccessor, OnInit, OnDestroy {
    @Input() title: string;
    @Input() hint: string;
    @Input() nzRequired: boolean;
    @Input() formControlName: string;
    @Input() formGroup: FormGroup;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            name: [undefined, [Validators.required]],
            email: [undefined, [Validators.required, Validators.email]],
            cc: this.fb.array([]),
        }),
    };

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    constructor(private readonly fb: FormBuilder) {}

    ngOnInit(): void {
        this.state.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((formValue: FormValue) => {
            if (this.onChange) {
                this.onChange(formValue);
            }
        });

        this.formGroup.addValidators(this.statusValidator());
        this.formGroup.updateValueAndValidity();
    }

    ngOnDestroy(): void {
        this.formGroup.clearValidators();
        this.formGroup.updateValueAndValidity();
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldMailRecipient.${input}`;
    }

    get ccForm(): FormArray {
        return this.state.form.get('cc') as FormArray;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }

    writeValue(obj: BazaFormMailRecipientDto): void {
        while (this.ccForm.length) {
            this.ccForm.removeAt(0);
        }

        this.state.form.reset();

        if (!obj) {
            return;
        }

        this.state.form.patchValue({
            email: obj.email,
            name: obj.name,
        } as Partial<FormValue>);

        if (Array.isArray(obj.cc) && obj.cc.length > 0) {
            for (const cc of obj.cc) {
                if (cc.name && cc.email) {
                    const ccRecipientForm = this.fb.group({
                        name: [cc.name, [Validators.required]],
                        email: [cc.email, [Validators.required, Validators.email]],
                    });

                    this.ccForm.push(ccRecipientForm);
                }
            }
        }
    }

    get canAddRecipient(): boolean {
        if (!this.ccForm.length) {
            return true;
        }

        return this.ccForm.at(this.ccForm.length - 1).valid;
    }

    addCCRecipient(): void {
        if (!this.canAddRecipient) {
            return;
        }

        const ccRecipientForm = this.fb.group({
            name: [undefined, [Validators.required]],
            email: [undefined, [Validators.required, Validators.email]],
        });

        this.ccForm.push(ccRecipientForm);
    }

    removeCCRecipient(index: number): void {
        this.ccForm.removeAt(index);
    }

    /**
     * Validator method to pass the state of Form to parent FormGroup.
     * Parent FormGroup is not attached with `state.form` property.
     * @returns ValidatorFn
     */
    statusValidator(): ValidatorFn {
        return (): ValidationErrors | null => {
            if (this.state.form.invalid) {
                // Value is not used just to mark form with errors.
                return { invalid: true };
            }
            return null;
        };
    }

    castCurrentForm(form: AbstractControl): FormGroup {
        return form as FormGroup;
    }
}
