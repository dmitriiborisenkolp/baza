import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
    selector: 'baza-field-upload-preview-file',
    templateUrl: './preview-file.component.html',
    styleUrls: ['./preview-file.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFieldUploadPreviewFileComponent {
    @Input() url: string;

    constructor(private readonly domSanitizer: DomSanitizer) {}

    get trustedUrl(): SafeUrl {
        return this.domSanitizer.bypassSecurityTrustUrl(this.url);
    }
}
