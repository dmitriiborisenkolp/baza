import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NzDateMode } from 'ng-zorro-antd/date-picker';
import { BazaFormControlFieldDateRange } from '../../../models/form-controls/baza-form-control-field-date-range';

@Component({
    templateUrl: './baza-form-builder-date-range.component.html',
    styleUrls: ['./baza-form-builder-date-range.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderDateRangeComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderDateRangeComponent implements AfterViewInit, ControlValueAccessor {
    @Input() formGroup!: FormGroup;
    @Input() formControlName!: string;
    @Input() definition!: BazaFormControlFieldDateRange;
    @Input() mode!: NzDateMode;
    @Input() showTime!: boolean;
    @Input() showNow!: boolean;
    @Input() showToday!: boolean;

    constructor(private readonly cdr: ChangeDetectorRef) {}

    ngAfterViewInit(): void {
        this.cdr.detectChanges();
    }

    // TODO: There should be more clear way how to handle directives

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    writeValue(obj: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    registerOnChange(fn: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    registerOnTouched(fn: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setDisabledState(isDisabled: boolean): void {}
}
