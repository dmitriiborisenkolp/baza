import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { NzProgressStatusType } from 'ng-zorro-antd/progress';
import { AttachmentDto } from '@scaliolabs/baza-core-shared';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { concat, Observable, Subject, throwError } from 'rxjs';
import {
    BazaFormBuilderUploadCropperService,
    BazaFormBuilderUploadFileService,
    BazaFormBuilderUploadRequest,
    BazaFormBuilderUploadResponse,
} from '../../services';
import { catchError, filter, finalize, takeUntil, tap } from 'rxjs/operators';
import { isImageFile } from '../../util/is-image-file';

interface QueueFileInterface {
    file: File;
    isImage: boolean;
    isImageCropped: boolean;
    uploadState: QueueUploadState;
    s3ObjectId?: string;
    uploadedFile?: AttachmentDto;
    uploadResponse?: BazaFormBuilderUploadResponse;
}

interface ProgressStateInterface {
    nzPercent: number;
    nzStatus: NzProgressStatusType;
}

enum QueueUploadState {
    InQueue = 'InQueue',
    Uploading = 'Uploading',
    Failed = 'Failed',
    Successful = 'Successful',
}

@Component({
    templateUrl: './upload-queue.component.html',
    styleUrls: ['./upload-queue.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormBuilderUploadQueueComponent implements OnDestroy {
    @Input() request: BazaFormBuilderUploadRequest;

    @Output() onClose = new EventEmitter<void>();
    @Output() onCancel = new EventEmitter<void>();
    @Output() onSuccess = new EventEmitter<Array<BazaFormBuilderUploadResponse>>();

    public isUploading = false;

    private readonly ngOnDestroy$ = new Subject<void>();
    private readonly onNextUpload$ = new Subject<void>();
    private readonly onCancelledUpload$ = new Subject<File>();
    private readonly onCancelledAllUploads$ = new Subject<void>();

    public fileQueue: QueueFileInterface[] = [];
    public isNgModalVisible = true;
    public uploadLoading = this.loading.newInstance();

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly loading: BazaLoadingService,
        private readonly cropper: BazaFormBuilderUploadCropperService,
        private readonly uploadService: BazaFormBuilderUploadFileService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
        this.onNextUpload$.next();
    }

    i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldUpload.queue.${input}`;
    }

    get areAllFilesUploaded(): boolean {
        return this.fileQueue.every((next) => next.uploadState === QueueUploadState.Successful);
    }

    handleOk(): void {
        if (this.areAllFilesUploaded) {
            this.finish();
        } else {
            this.submit(this.fileQueue);
        }
    }

    handleCancel(): void {
        this.fileQueue.forEach((next) => {
            if (next.uploadState === QueueUploadState.Uploading) {
                next.uploadState = QueueUploadState.InQueue;
            }
        });

        if (this.isUploading) {
            this.onCancelledAllUploads$.next();
        } else {
            this.onCancel.emit();
            this.onClose.emit();
        }
    }

    upload(request: BazaFormBuilderUploadRequest): Observable<Array<BazaFormBuilderUploadResponse>> {
        return new Observable((observer) => {
            this.onCancel.pipe(takeUntil(this.ngOnDestroy$)).subscribe(() => {
                observer.next([]);
                observer.complete();
            });

            this.onSuccess.pipe(takeUntil(this.ngOnDestroy$)).subscribe((next) => {
                observer.next(next);
                observer.complete();
            });

            for (let i = 0; i < request.files.length; i++) {
                this.fileQueue.push({
                    file: request.files.item(i),
                    isImage: isImageFile(request.files.item(i).type),
                    isImageCropped: false,
                    uploadState: QueueUploadState.InQueue,
                });
            }
        });
    }

    submit(fileQueue: Array<QueueFileInterface>): void {
        fileQueue.forEach((next) => {
            next.uploadState = next.uploadState === QueueUploadState.Successful ? QueueUploadState.Successful : QueueUploadState.InQueue;
        });

        this.cdr.markForCheck();

        const observables = fileQueue
            .filter((next) => next.uploadState === QueueUploadState.InQueue)
            .map((next) =>
                this.uploadService
                    .upload(next.file, this.request, {
                        beforeStart: () => {
                            next.uploadState = QueueUploadState.Uploading;

                            this.cdr.markForCheck();
                        },
                    })
                    .pipe(
                        finalize(() => this.cdr.markForCheck()),
                        tap((response) => {
                            next.uploadState = QueueUploadState.Successful;
                            next.s3ObjectId = response[0].s3ObjectId;
                            next.uploadedFile = response[0].attachment;
                            next.uploadResponse = response[0];
                        }),
                        catchError((err) => {
                            next.uploadState = QueueUploadState.Failed;
                            next.s3ObjectId = undefined;
                            next.uploadedFile = undefined;
                            next.uploadResponse = undefined;

                            return throwError(err);
                        }),
                        takeUntil(this.onCancelledUpload$.pipe(filter((n) => n === next.file))),
                        takeUntil(this.ngOnDestroy$),
                    ),
            );

        if (observables.length) {
            this.isUploading = true;

            const loading = this.uploadLoading.addLoading();

            concat(...observables)
                .pipe(
                    finalize(() => (this.isUploading = false)),
                    finalize(() => loading.complete()),
                    takeUntil(this.onCancelledAllUploads$),
                    takeUntil(this.ngOnDestroy$),
                )
                .subscribe(() => {
                    const areAllFilesUploaded = this.fileQueue.every((next) => next.uploadState === QueueUploadState.Successful);

                    if (areAllFilesUploaded) {
                        this.finish();
                    }
                });
        }
    }

    finish(): void {
        const uploadedFiles = this.fileQueue.filter((next) => next.uploadState === QueueUploadState.Successful);

        this.onSuccess.emit(uploadedFiles.map((next) => next.uploadResponse));

        this.close();
    }

    close(): void {
        this.onClose.emit();
    }

    crop(queueFile: QueueFileInterface): void {
        if (!queueFile.isImage) {
            return;
        }

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const componentRef = this.cropper.open(queueFile.file, this.request.options, this.request.vcr);

        componentRef.instance.onUpload.pipe(takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            queueFile.file = next;
            queueFile.isImageCropped = true;

            this.cdr.markForCheck();
        });
    }

    exclude(queueFile: QueueFileInterface): void {
        this.fileQueue = this.fileQueue.filter((next) => next !== queueFile);

        this.onCancelledUpload$.next(queueFile.file);

        if (!this.fileQueue.length) {
            this.handleCancel();
        }
    }

    retry(queueFile: QueueFileInterface): void {
        if (queueFile.uploadState !== QueueUploadState.Failed) {
            return;
        }

        this.submit([queueFile]);
    }

    progressState(file: QueueFileInterface): ProgressStateInterface {
        if (file.uploadState === QueueUploadState.Successful) {
            return {
                nzPercent: 100,
                nzStatus: this.strokeStatus(file),
            };
        } else if (file.uploadState === QueueUploadState.Uploading) {
            return {
                nzPercent: 50,
                nzStatus: this.strokeStatus(file),
            };
        } else {
            return {
                nzPercent: 0,
                nzStatus: this.strokeStatus(file),
            };
        }
    }

    strokeStatus(item: QueueFileInterface): NzProgressStatusType {
        switch (item.uploadState) {
            case QueueUploadState.Uploading:
                return 'active';

            case QueueUploadState.Failed:
                return 'exception';

            case QueueUploadState.Successful:
                return 'success';

            default:
                return 'normal';
        }
    }
}
