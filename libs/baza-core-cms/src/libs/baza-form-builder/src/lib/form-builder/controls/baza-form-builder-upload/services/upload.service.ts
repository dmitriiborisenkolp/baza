import { Injectable } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { BazaFormBuilderUploadCropperService } from './upload-cropper.service';
import {
    BAZA_FIELD_UPLOAD_MIME_IMAGE,
    BAZA_MULTI_FILE_UPLOAD_LIMIT,
    BazaFieldUploadImageRatioHandler,
} from '../components/upload-single/baza-form-builder-upload.model';
import { BazaFormBuilderUploadFileService, BazaFormBuilderUploadRequest, BazaFormBuilderUploadResponse } from './upload-file.service';
import { BazaFormBuilderUploadQueueService } from './upload-queue.service';

@Injectable()
export class BazaFormBuilderUploadService {
    constructor(
        private readonly service: BazaFormBuilderUploadFileService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly cropper: BazaFormBuilderUploadCropperService,
        private readonly queue: BazaFormBuilderUploadQueueService,
    ) {}

    upload(request: BazaFormBuilderUploadRequest): Observable<Array<BazaFormBuilderUploadResponse>> {
        const { files, options, takeUntil$ } = request;

        if (files.length === 0) {
            return of([]);
        }

        const maxMultiFileUploadLimit = Math.min(
            BAZA_MULTI_FILE_UPLOAD_LIMIT,
            options.maxMultiFileUploadLimit || BAZA_MULTI_FILE_UPLOAD_LIMIT,
        );

        if (files.length > maxMultiFileUploadLimit) {
            this.ngMessages.error({
                message: this.i18n('maxMultiFileUploadLimitError'),
                translate: true,
                translateParams: {
                    maxMultiFileUploadLimit: options.maxMultiFileUploadLimit,
                },
            });

            return of([]);
        }

        const filesArr: Array<File> = [];

        for (let i = 0; i < files.length; i++) {
            filesArr.push(files.item(i));
        }

        if (filesArr.length === 0) {
            return of([]);
        }

        const isSingleFileSelectedOnly = filesArr.length === 1;
        const isRatioSpecified = !!options.ratio;
        const isCropperJsOptionEnabled =
            (isRatioSpecified && options.ratioHandler !== BazaFieldUploadImageRatioHandler.Backend) ||
            (!isRatioSpecified && options.ratioHandler !== BazaFieldUploadImageRatioHandler.Disabled);
        const areAllFilesAreImages = filesArr.every((fileItem) => BAZA_FIELD_UPLOAD_MIME_IMAGE.includes(fileItem.type));

        if (isSingleFileSelectedOnly) {
            const file = filesArr[0];

            if (areAllFilesAreImages && isCropperJsOptionEnabled) {
                return this.performUploadWithCropperJs(file, request);
            } else {
                return this.service.upload(file, request).pipe(takeUntil(takeUntil$));
            }
        } else {
            return this.queue.upload(request);
        }
    }

    private i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldUploadMulti.${input}`;
    }

    private performUploadWithCropperJs(
        file: File,
        request: BazaFormBuilderUploadRequest,
    ): Observable<Array<BazaFormBuilderUploadResponse>> {
        const { options, vcr, takeUntil$ } = request;

        return new Observable((observer) => {
            // eslint-disable-next-line security/detect-non-literal-fs-filename
            const componentRef = this.cropper.open(file, options, vcr);

            componentRef.instance.onClose.pipe(takeUntil(takeUntil$)).subscribe(
                () => {
                    observer.next([]);
                    observer.complete();
                },
                (err) => {
                    observer.error(err);
                },
            );

            componentRef.instance.onUpload.pipe(takeUntil(takeUntil$)).subscribe((croppedImageFile) => {
                this.service
                    .upload(croppedImageFile, request)
                    .pipe(takeUntil(takeUntil$))
                    .subscribe(
                        (next) => {
                            observer.next(next);
                            observer.complete();
                        },
                        (err) => {
                            observer.error(err);
                        },
                    );
            });
        });
    }
}
