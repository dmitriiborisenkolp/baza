import { BAZA_FIELD_UPLOAD_MIME_IMAGE } from '../components/upload-single/baza-form-builder-upload.model';

export function isImageFile(mimeType: string): boolean {
    return BAZA_FIELD_UPLOAD_MIME_IMAGE.includes(mimeType);
}
