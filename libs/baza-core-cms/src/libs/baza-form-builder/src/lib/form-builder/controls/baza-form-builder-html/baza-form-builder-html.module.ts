import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaFormBuilderHtmlComponent } from './baza-form-builder-html.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaCkEditorModule } from '../../../../../../baza-ck-editor/src';

@NgModule({
    imports: [
        CommonModule,
        BazaCkEditorModule,
        ReactiveFormsModule,
    ],
    declarations: [
        BazaFormBuilderHtmlComponent,
    ],
    exports: [
        BazaFormBuilderHtmlComponent,
    ],
})
export class BazaFormBuilderHtmlModule {}
