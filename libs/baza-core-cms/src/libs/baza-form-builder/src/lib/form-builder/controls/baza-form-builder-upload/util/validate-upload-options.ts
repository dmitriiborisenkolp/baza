import { BazaFieldUploadImageRatioHandler, BazaFileUploadOptions, BazaFileUploadTransport } from '../components/upload-single/baza-form-builder-upload.model';

export function validateUploadOptions(options: BazaFileUploadOptions) {
    if (
        options.ratio &&
        options.ratioHandler === BazaFieldUploadImageRatioHandler.Backend &&
        options.transport?.type !== BazaFileUploadTransport.BazaAttachment
    ) {
        throw new Error(
            'Ratio option with Backend ratioHandler and BazaAWS attachment strategy is unsupported',
        );
    }

    if (
        options.ratio &&
        options.ratioHandler === BazaFieldUploadImageRatioHandler.Disabled
    ) {
        throw new Error('Disabled Ratio Handler option is not supposed to work with enabled Ratio option');
    }
}
