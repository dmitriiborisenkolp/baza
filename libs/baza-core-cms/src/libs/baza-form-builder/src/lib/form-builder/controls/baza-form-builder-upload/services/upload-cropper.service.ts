import {
    ComponentFactoryResolver,
    ComponentRef,
    Injectable,
    ReflectiveInjector,
    ViewContainerRef,
} from '@angular/core';
import { BazaFormBuilderUploadCropperComponent } from './../components/upload-cropper/upload-cropper.component';
import { BazaFileUploadOptions } from '../components/upload-single/baza-form-builder-upload.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class BazaFormBuilderUploadCropperService {
    constructor(private readonly componentFactoryResolver: ComponentFactoryResolver) {}

    open(
        selectedFile: File,
        options: BazaFileUploadOptions,
        parentVcr: ViewContainerRef,
    ): ComponentRef<BazaFormBuilderUploadCropperComponent> {
        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaFormBuilderUploadCropperComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], parentVcr.injector);

        const component = factory.create(injector);
        const componentDestroy$ = new Subject<void>();

        component.instance.selectedFile = selectedFile;
        component.instance.options = options;

        component.instance.onClose.pipe(takeUntil(componentDestroy$)).subscribe(() => {
            component.destroy();
            componentDestroy$.next();
        });

        component.instance.onUpload.pipe(takeUntil(componentDestroy$)).subscribe(() => {
            component.destroy();
            componentDestroy$.next();
        });

        parentVcr.insert(component.hostView);

        return component;
    }
}
