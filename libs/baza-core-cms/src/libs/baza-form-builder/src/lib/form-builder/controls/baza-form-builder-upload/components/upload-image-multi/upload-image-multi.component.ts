import { ChangeDetectionStrategy, Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AttachmentDto, AttachmentImageSetUpload, AttachmentType } from '@scaliolabs/baza-core-shared';
import {
    BazaFieldUploadImageRatioHandler,
    BazaFileUploadOptions,
    BazaFileUploadTransport,
} from '../upload-single/baza-form-builder-upload.model';
import { Subject } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { BazaFormBuilderUploadMultiAttachmentEvent } from '../upload-multi/upload-multi.component';

type S3ObjectId = string;

interface State {
    form: FormGroup;
    attachments: Array<AttachmentDto | S3ObjectId>;
}

interface FormValue {
    value: Array<S3ObjectId>;
}

@Component({
    selector: 'baza-field-upload-image-multi',
    templateUrl: './upload-image-multi.component.html',
    styleUrls: ['./upload-image-multi.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderUploadImageMultiComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderUploadImageMultiComponent implements ControlValueAccessor, OnInit, OnDestroy {
    @Input() multiselect = true;
    @Input() withImageOptions: Partial<AttachmentImageSetUpload>;
    @Input() withRatioHandler: BazaFieldUploadImageRatioHandler = BazaFieldUploadImageRatioHandler.Backend;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            value: [[]],
        }),
        attachments: [],
    };

    constructor(private readonly fb: FormBuilder) {}

    ngOnInit(): void {
        this.state.form.valueChanges
            .pipe(
                map((fv: FormValue) => fv.value),
                distinctUntilChanged(),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((values) => {
                if (this.onChange) {
                    const newAttachmentsList = values
                        .map((s3ObjectId) =>
                            this.state.attachments.find((a) => (typeof a === 'object' ? a.s3ObjectId === s3ObjectId : a === s3ObjectId)),
                        )
                        .filter((attachment) => !!attachment);

                    this.onChange(newAttachmentsList);
                }
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get withOptions(): BazaFileUploadOptions {
        return {
            transport: {
                type: BazaFileUploadTransport.BazaAttachment,
                options: () => ({
                    type: AttachmentType.ImageSet,
                    payload: {
                        ...this.withImageOptions,
                    },
                }),
            },
            ratio: this.withImageOptions?.ratio,
            ratioHandler: this.withRatioHandler,
            multiselect: this.multiselect,
        };
    }

    onAttachment(event: BazaFormBuilderUploadMultiAttachmentEvent): void {
        this.state.attachments[event.index] = event.attachment;
        this.state.attachments = this.state.attachments.filter((attachment) => !!attachment);

        if (this.onChange) {
            this.onChange(this.state.attachments);
        }
    }

    writeValue(obj: Array<AttachmentDto>): void {
        if (Array.isArray(obj) && obj.length > 0) {
            this.state.form.patchValue({
                value: obj.map((attachment) => attachment.s3ObjectId),
            } as FormValue);

            this.state = {
                ...this.state,
                attachments: obj,
            };
        } else {
            this.state.form.patchValue({
                value: [],
            } as FormValue);
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
