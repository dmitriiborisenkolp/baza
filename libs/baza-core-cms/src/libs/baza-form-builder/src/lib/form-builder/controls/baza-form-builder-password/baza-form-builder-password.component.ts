import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    forwardRef,
    Input,
    OnChanges,
    ViewChild,
} from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NzInputDirective } from 'ng-zorro-antd/input';
import { BazaFormControlFieldPassword } from '../../../models/form-controls/baza-form-control-field-password';

@Component({
    templateUrl: './baza-form-builder-password.component.html',
    styleUrls: ['./baza-form-builder-password.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderPasswordComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderPasswordComponent implements OnChanges, AfterViewInit, ControlValueAccessor {
    @ViewChild('element', {
        read: NzInputDirective,
    })
    nzInputRef: NzInputDirective;

    @Input() formGroup: FormGroup;
    @Input() formControlName: string;
    @Input() definition: BazaFormControlFieldPassword;
    @Input() placeholder: string;

    constructor(private readonly cdr: ChangeDetectorRef) {}

    ngOnChanges(): void {
        this.updateDirectiveProps();
    }

    ngAfterViewInit(): void {
        this.updateDirectiveProps();

        this.cdr.detectChanges();
    }

    private updateDirectiveProps(): void {
        if (!this.nzInputRef) {
            return;
        }

        Object.assign(this.nzInputRef, this.definition.directiveProps);

        this.nzInputRef.ngOnChanges({});

        this.cdr.markForCheck();
    }

    // TODO: There should be more clear way how to handle directives

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    writeValue(obj: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    registerOnChange(fn: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    registerOnTouched(fn: any): void {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    setDisabledState(isDisabled: boolean): void {}
}
