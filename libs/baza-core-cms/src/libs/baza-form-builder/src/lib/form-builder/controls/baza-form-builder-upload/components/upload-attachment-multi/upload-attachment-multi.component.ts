import { ChangeDetectionStrategy, Component, forwardRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { AttachmentDto, AttachmentUploadRequest } from '@scaliolabs/baza-core-shared';
import { BazaFileUploadOptions, BazaFileUploadTransport } from '../upload-single/baza-form-builder-upload.model';
import { Subject } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { BazaFormBuilderUploadMultiAttachmentEvent } from '../upload-multi/upload-multi.component';

type S3ObjectId = string;

interface State {
    form: FormGroup;
    withOptions?: Partial<BazaFileUploadOptions>;
    attachments: Array<AttachmentDto | S3ObjectId>;
}

interface FormValue {
    value: Array<S3ObjectId>;
}

@Component({
    selector: 'baza-field-upload-attachment-multi',
    templateUrl: './upload-attachment-multi.component.html',
    styleUrls: ['./upload-attachment-multi.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderUploadAttachmentMultiComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderUploadAttachmentMultiComponent implements ControlValueAccessor, OnChanges, OnInit, OnDestroy {
    @Input() withOptions: AttachmentUploadRequest | { (file: File, extension: string): AttachmentUploadRequest };
    @Input() fileUploadOptions: Partial<Exclude<BazaFileUploadOptions, 'transport' | 'message'>>;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            value: [[]],
        }),
        attachments: [],
    };

    constructor(private readonly fb: FormBuilder) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['withOptions']) {
            this.state = {
                ...this.state,
                withOptions: {
                    transport: {
                        type: BazaFileUploadTransport.BazaAttachment,
                        options: (file, extension) =>
                            typeof this.withOptions === 'function' ? this.withOptions(file, extension) : this.withOptions,
                    },
                    ...this.fileUploadOptions,
                },
            };
        }
    }

    ngOnInit(): void {
        this.state.form.valueChanges
            .pipe(
                map((fv: FormValue) => fv.value),
                distinctUntilChanged(),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((values) => {
                if (this.onChange) {
                    const newAttachmentsList = values
                        .map((s3ObjectId) =>
                            this.state.attachments.find((a) => (typeof a === 'object' ? a.s3ObjectId === s3ObjectId : a === s3ObjectId)),
                        )
                        .filter((attachment) => !!attachment);

                    this.onChange(newAttachmentsList);
                }
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    onAttachment(event: BazaFormBuilderUploadMultiAttachmentEvent): void {
        this.state.attachments[event.index] = event.attachment;
        this.state.attachments = this.state.attachments.filter((attachment) => !!attachment);

        if (this.onChange) {
            this.onChange(this.state.attachments);
        }
    }

    writeValue(obj: Array<AttachmentDto | S3ObjectId>): void {
        if (Array.isArray(obj) && obj.length > 0) {
            this.state.form.patchValue({
                value: obj.map((attachment) => (typeof attachment === 'object' ? attachment.s3ObjectId : attachment)),
            } as FormValue);

            this.state = {
                ...this.state,
                attachments: obj,
            };
        } else {
            this.state.form.patchValue({
                value: [],
            } as FormValue);
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
