import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    forwardRef,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild,
    ViewContainerRef,
} from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormArray, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { BazaLoadingService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { AttachmentDto } from '@scaliolabs/baza-core-shared';
import { Subject, throwError } from 'rxjs';
import { catchError, distinctUntilChanged, finalize, takeUntil } from 'rxjs/operators';
import { BazaAttachmentCacheService } from '../../../../../services/baza-attachment-cache.service';
import { BAZA_FORM_BUILDER_UPLOAD_SERVICES, BazaFormBuilderUploadService } from '../../services';
import { validateUploadOptions } from '../../util/validate-upload-options';
import {
    BazaFieldUploadImageRatioHandler,
    bazaFieldUploadPresetExtensionsMap,
    BazaFileUploadOptions,
    defaultBazaFileUploadOptions,
} from '../upload-single/baza-form-builder-upload.model';

type S3ObjectId = string;

interface State {
    form: FormArray;
}

interface FormValue {
    value: S3ObjectId;
}

export interface BazaFormBuilderUploadMultiAttachmentEvent {
    index: number;
    attachment: AttachmentDto | S3ObjectId;
}

@Component({
    selector: 'baza-field-upload-multi',
    templateUrl: './upload-multi.component.html',
    styleUrls: ['./upload-multi.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderUploadMultiComponent),
            multi: true,
        },
        ...BAZA_FORM_BUILDER_UPLOAD_SERVICES,
    ],
})
export class BazaFormBuilderUploadMultiComponent implements ControlValueAccessor, OnChanges, OnInit, OnDestroy {
    @ViewChild('fileInput', { read: ElementRef }) fileInputEl: ElementRef;

    @Input() withOptions: Partial<BazaFileUploadOptions>;
    @Input() attachments: Array<AttachmentDto | S3ObjectId>;
    @Input() autoAddControlOnEmptyList = true;

    @Output('onAttachment') onAttachmentEvent = new EventEmitter<BazaFormBuilderUploadMultiAttachmentEvent>();

    public uploadLoading = this.loading.newInstance();
    public state: State = {
        form: this.fb.array([]),
    };

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly vcr: ViewContainerRef,
        private readonly loading: BazaLoadingService,
        private readonly fb: FormBuilder,
        private readonly cache: BazaAttachmentCacheService,
        private readonly uploadService: BazaFormBuilderUploadService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['withOptions']) {
            validateUploadOptions(this.options);
        }
    }

    ngOnInit(): void {
        this.state.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((formValue: Array<FormValue>) => {
            if (this.onChange) {
                this.onChange(formValue.filter((s) => !!s.value).map((s) => s.value));
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get options(): BazaFileUploadOptions {
        return {
            ...defaultBazaFileUploadOptions(),
            ...{
                ratioHandler: BazaFieldUploadImageRatioHandler.Backend,
                multiselect: true,
            },
            ...this.withOptions,
        };
    }

    get extensions(): Array<string> {
        if (this.options.extensions) {
            return this.options.extensions;
        } else if (this.options.preset) {
            const found = bazaFieldUploadPresetExtensionsMap.find((e) => e.preset === this.options.preset);

            return found ? found.extensions : [];
        } else {
            return [];
        }
    }

    get accepts(): string {
        const extensions = this.extensions;

        return Array.isArray(extensions) && extensions.length > 0 ? extensions.map((ext) => `.${ext}`).join(',') : undefined;
    }

    i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldUploadMulti.${input}`;
    }

    get canAddItem(): boolean {
        return true;
    }

    castCurrentForm(form: AbstractControl): FormGroup {
        return form as FormGroup;
    }

    addItem(s3ObjectId?: string): void {
        const emptyControl = this.state.form.controls[0];
        const form = this.fb.group({
            value: [s3ObjectId],
        });

        if (s3ObjectId && !emptyControl?.value?.value) {
            this.state = {
                ...this.state,
                form: this.fb.array([]),
            };
        }

        this.state.form.push(form);
    }

    onAttachment(index: number, attachment: AttachmentDto | S3ObjectId): void {
        this.onAttachmentEvent.emit({
            index,
            attachment,
        });
    }

    onRemove(index: number): void {
        this.state.form.removeAt(index);

        this.onAttachment(index, undefined);

        if (this.autoAddControlOnEmptyList && !this.state.form.length) {
            this.addItem();
        }
    }

    onMoveUp(index: number): void {
        this.shiftFormControls(-1, index);
    }

    onMoveDown(index: number): void {
        this.shiftFormControls(1, index);
    }

    shiftFormControls(shift: number, i: number): void {
        const controls = this.state.form.value;

        const swap = {
            control: controls[`${i}`],
            attachment: this.attachments[`${i}`],
        };

        controls[`${i}`] = controls[i + shift];
        this.attachments[`${i}`] = this.attachments[i + shift];

        controls[i + shift] = swap.control;
        this.attachments[i + shift] = swap.attachment;

        this.state.form.patchValue(controls);
    }

    writeValue(obj: Array<S3ObjectId>): void {
        while (this.state.form.controls.length) {
            this.state.form.removeAt(0);
        }

        if (Array.isArray(obj) && obj.length > 0) {
            for (const s3ObjectId of obj) {
                this.addItem(s3ObjectId);
            }
        }

        this.cdr.markForCheck();
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }

    onFileInputChange($event): void {
        const target = $event.target as HTMLInputElement;

        const localLoading = this.uploadLoading.addLoading();

        this.uploadService
            .upload({
                vcr: this.vcr,
                files: target.files,
                options: this.options,
                takeUntil$: this.ngOnDestroy$,
                extensions: this.extensions,
            })
            .pipe(
                catchError((error) => {
                    this.ngMessages.error({ message: error.message || JSON.stringify(error) });

                    return throwError(() => error);
                }),
                finalize(() => localLoading.complete()),
                finalize(() => this.cdr.markForCheck()),
            )
            .subscribe((response) => {
                response.forEach((next) => {
                    this.addItem(next.s3ObjectId);

                    this.onAttachment(this.state.form.controls.length - 1, next.attachment || next.s3ObjectId);
                });
            });

        setTimeout(() => {
            if (this.fileInputEl?.nativeElement) {
                this.fileInputEl.nativeElement.value = null;
            }
        });
    }
}
