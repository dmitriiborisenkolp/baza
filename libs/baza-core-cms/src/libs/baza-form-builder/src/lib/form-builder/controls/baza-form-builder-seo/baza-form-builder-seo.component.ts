import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormArray, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { bazaSeoDefault, BazaSeoDto, bazaSeoKnownMetaTags } from '@scaliolabs/baza-core-shared';
import { BazaFieldUploadPreset, BazaFileUploadOptions, BazaFileUploadTransport } from '../baza-form-builder-upload';
import { AttachmentType } from '@scaliolabs/baza-core-shared';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

function isEmpty(input): boolean {
    return input === null || input === undefined || (input || '').toString().trim().length === 0;
}

interface FormValue {
    title: string;
    urn: string;
    meta: Array<MetaFormValue>;
    card: CardFormValue;
}

interface MetaFormValue {
    name: string;
    contents: string;
}

interface CardFormValue {
    title: string;
    description: string;
    url: string;
    coverImageUrl: string;
}

interface State {
    form: FormGroup;
    uploadOptions: Partial<BazaFileUploadOptions>;
    knownMetatags: Array<string>;
}

@Component({
    selector: 'baza-field-seo',
    templateUrl: './baza-form-builder-seo.component.html',
    styleUrls: ['./baza-form-builder-seo.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderSeoComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderSeoComponent implements ControlValueAccessor, OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    public state: State = {
        form: this.fb.group({
            title: [],
            urn: [],
            meta: this.fb.array([]),
            card: this.fb.group({
                title: [],
                description: [],
                url: [],
                coverImageUrl: [],
            }),
        }),
        uploadOptions: {
            preset: BazaFieldUploadPreset.Image,
            transport: {
                type: BazaFileUploadTransport.BazaAttachment,
                options: () => ({
                    type: AttachmentType.Image,
                    payload: {},
                }),
            },
        },
        knownMetatags: [...bazaSeoKnownMetaTags],
    };

    constructor(private readonly fb: FormBuilder, private readonly cdr: ChangeDetectorRef) {}

    ngOnInit(): void {
        this.state.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            if (this.onChange) {
                this.onChange(next);
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(input: string): string {
        return `baza.formBuilder.components.bazaFieldSeo.${input}`;
    }

    get metaForms(): FormArray {
        return this.state.form.get('meta') as FormArray;
    }

    get cardForm(): FormGroup {
        return this.state.form.get('card') as FormGroup;
    }

    get canAddMeta(): boolean {
        if (this.metaForms.controls.length === 0) {
            return true;
        } else {
            const lastMetaForm = this.metaForms.controls[this.metaForms.controls.length - 1];

            return !isEmpty((lastMetaForm.value as MetaFormValue).name) && !isEmpty((lastMetaForm.value as MetaFormValue).contents);
        }
    }

    castCurrentForm(form: AbstractControl): FormGroup {
        return form as FormGroup;
    }

    addMeta(): void {
        this.cleanUpMeta();

        this.metaForms.push(
            this.fb.group({
                name: [],
                contents: [],
            }),
        );
    }

    deleteMeta(index: number): void {
        this.cleanUpMeta();

        this.metaForms.removeAt(index);
    }

    cleanUpMeta(): void {
        for (let i = 0; i < this.metaForms.length; i++) {
            const formValue: MetaFormValue = this.metaForms.controls[`${i}`].value;

            if (isEmpty(formValue.name) && isEmpty(formValue.contents)) {
                this.metaForms.removeAt(i);

                return this.cleanUpMeta();
            }
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(input: BazaSeoDto): void {
        const seo = input ? input : bazaSeoDefault();

        while (this.metaForms.length) {
            this.metaForms.removeAt(0);
        }

        if (Array.isArray(seo.meta) && seo.meta.length > 0) {
            for (const meta of seo.meta) {
                this.addMeta();
                this.metaForms.at(this.metaForms.length - 1).patchValue(meta);
            }
        }

        if (seo.card) {
            this.cardForm.patchValue(seo.card);
        }

        this.state.form.patchValue({
            title: seo.title,
            urn: seo.urn,
        } as FormValue);

        this.cleanUpMeta();

        if (this.metaForms.length === 0) {
            this.addMeta();
        }

        this.cdr.markForCheck();
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
