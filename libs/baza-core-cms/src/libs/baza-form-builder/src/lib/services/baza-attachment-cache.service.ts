import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { AttachmentDto } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { map, publishLast, refCount, retryWhen, shareReplay, switchMap } from 'rxjs/operators';
import { BazaAwsDataAccess } from '@scaliolabs/baza-core-data-access';

export interface ResponseWithContentType {
    url: string;
    contentType: string;
}

@Injectable()
export class BazaAttachmentCacheService {
    public CACHE_PRESIGNED_URLS: { [id: string]: Observable<ResponseWithContentType> } = {};

    constructor(private readonly httpClient: HttpClient, private readonly awsDataAccess: BazaAwsDataAccess) {}

    getCache(s3ObjectId: string): Observable<ResponseWithContentType> {
        if (!this.CACHE_PRESIGNED_URLS[`${s3ObjectId}`]) {
            this.CACHE_PRESIGNED_URLS[`${s3ObjectId}`] = this.awsDataAccess
                .presignedUrl({
                    s3ObjectId,
                })
                .pipe(
                    switchMap((presignedUrl) =>
                        this.httpClient
                            .get(presignedUrl, {
                                observe: 'response',
                                responseType: 'blob',
                                headers: {
                                    Range: 'bytes=0-0',
                                },
                            })
                            .pipe(
                                retryWhen(genericRetryStrategy()),
                                map(
                                    (response) =>
                                        ({
                                            url: response.url,
                                            contentType: response.headers.get('Content-Type'),
                                        } as ResponseWithContentType),
                                ),
                                shareReplay(),
                            ),
                    ),
                    publishLast(),
                    refCount(),
                );
        }

        return this.CACHE_PRESIGNED_URLS[`${s3ObjectId}`];
    }

    getPresignedUrlPreview(upload: AttachmentDto): Observable<ResponseWithContentType> {
        if (!this.CACHE_PRESIGNED_URLS[upload.s3ObjectId]) {
            this.cacheUpload(upload);
        }

        return this.CACHE_PRESIGNED_URLS[upload.s3ObjectId];
    }

    cacheUpload(upload: AttachmentDto): void {
        this.CACHE_PRESIGNED_URLS[upload.s3ObjectId] = this.httpClient
            .get(upload.presignedUrl, {
                observe: 'response',
                responseType: 'blob',
                headers: {
                    Range: 'bytes=0-0',
                },
            })
            .pipe(
                retryWhen(genericRetryStrategy()),
                map(
                    (response) =>
                        ({
                            url: response.url,
                            contentType: response.headers.get('Content-Type'),
                            response,
                        } as ResponseWithContentType),
                ),
                shareReplay(),
            );
    }

    getByS3ObjectId(s3ObjectId): Observable<ResponseWithContentType> {
        return this.CACHE_PRESIGNED_URLS[`${s3ObjectId}`];
    }
}
