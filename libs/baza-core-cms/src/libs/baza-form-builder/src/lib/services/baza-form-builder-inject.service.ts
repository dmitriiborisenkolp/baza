import { Injectable, ViewContainerRef } from '@angular/core';
import { BazaFormBuilder } from '../models/baza-form-builder-layouts';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { BazaFormLayoutModal } from '../components/baza-form-layout-modal/baza-form-layout-modal.model';
import { BazaFormLayoutConfig } from '../components/baza-form-layout-slider/baza-form-layout-slider.model';

/**
 * Form Inject Configuration
 */
interface BazaFormBuilderInjectFullConfig<T, FORM_VALUE = T, SUBMIT_RESPONSE = T> {
    /**
     * Mutate `config` or `formGroup` options to inject your custom configutation
     */
    inject(
        config: BazaFormBuilder,
        formGroup: FormGroup,
        resources: {
            /**
             * VCR used for Modal window
             */
            childVcr: ViewContainerRef;

            /**
             * ngOnDestroy callback
             */
            takeUntil$: Subject<void>;

            /**
             * Reference BazaFormLayoutModal (one of `modalConfig` or `sliderConfig` will be available)
             */
            modalConfig?: BazaFormLayoutModal<T, FORM_VALUE, SUBMIT_RESPONSE>;

            /**
             * Reference BazaFormLayoutConfig (one of `modalConfig` or `sliderConfig` will be available)
             */
            sliderConfig?: BazaFormLayoutConfig<T, FORM_VALUE, SUBMIT_RESPONSE>;
        },
    ): void;
}

export type BazaFormBuilderInjectConfig<T, FORM_VALUE = T, SUBMIT_RESPONSE = T> = Partial<
    BazaFormBuilderInjectFullConfig<T, FORM_VALUE, SUBMIT_RESPONSE>
>;

const INJECTION_CONFIGURATIONS: { [id: string]: BazaFormBuilderInjectConfig<any> } = {};

@Injectable({
    providedIn: 'root',
})
export class BazaFormBuilderInjectService {
    /**
     * Register Inject Configuration for target Baza Form
     * You can find ID of Form in `data-baza-form-id` attribute of `div.baza-form-builder` wrapper element
     * You can also call `bazaFormId()` in console to get ID of Form which is opened currently on page
     */
    register<T, FORM_VALUE = T, SUBMIT_RESPONSE = T>(
        id: string,
        config: BazaFormBuilderInjectConfig<T, FORM_VALUE, SUBMIT_RESPONSE>,
    ): void {
        INJECTION_CONFIGURATIONS[`${id}`] = config;
    }

    /**
     * Remove Inject Configuration for target Baza Form
     * @param id
     */
    unregister(id: string): void {
        INJECTION_CONFIGURATIONS[`${id}`] = undefined;
    }

    /**
     * Returns true if there is Inject Configuration available for target Baza Form
     */
    has(id: string): boolean {
        return !!INJECTION_CONFIGURATIONS[`${id}`];
    }

    /**
     * Returns Inject Configuration available for target Baza Form
     */
    get<T, FORM_VALUE = T, SUBMIT_RESPONSE = T>(id: string): BazaFormBuilderInjectConfig<T, FORM_VALUE, SUBMIT_RESPONSE> {
        if (!this.has(id)) {
            console.error(`No Baza Form Builder injection configuration found for Form "${id}"!`);
        }

        return INJECTION_CONFIGURATIONS[`${id}`] || {};
    }
}
