import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BazaAwsDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNgCoreModule } from '@scaliolabs/baza-core-ng';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzRateModule } from 'ng-zorro-antd/rate';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzTimePickerModule } from 'ng-zorro-antd/time-picker';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { BazaAclCmsModule } from '../../../baza-acl/src';
import { BazaFormBuilderContentsComponent } from './components/baza-form-builder-contents/baza-form-builder-contents.component';
import { BazaFormBuilderContentsFormControlPortalComponent } from './components/baza-form-builder-contents/form-control-portal/form-control-portal.component';
import { BazaFormBuilderContentsFormControlComponent } from './components/baza-form-builder-contents/form-control/form-control.component';
import { BazaFormBuilderContentsStaticComponent } from './components/baza-form-builder-contents/static-component/static-component.component';
import { BazaFormBuilderComponent } from './components/baza-form-builder/baza-form-builder.component';
import { BazaFormLayoutActionsComponent } from './components/baza-form-layout-actions/baza-form-layout-actions.component';
import { BazaFormLayoutModalComponent } from './components/baza-form-layout-modal/baza-form-layout-modal.component';
import { BazaFormLayoutComponent } from './components/baza-form-layout-slider/baza-form-layout-slider.component';
import { BazaFormBuilderComponentTimestampComponent } from './form-builder/components/baza-form-builder-component-timestamp/baza-form-builder-component-timestamp.component';
import { BazaFormBuilderAccountModule } from './form-builder/controls/baza-form-builder-account';
import { BazaFormBuilderCheckboxComponent } from './form-builder/controls/baza-form-builder-checkbox/baza-form-builder-checkbox.component';
import { BazaFormBuilderDateRangeComponent } from './form-builder/controls/baza-form-builder-date-range/baza-form-builder-date-range.component';
import { BazaFormBuilderEmailComponent } from './form-builder/controls/baza-form-builder-email/baza-form-builder-email.component';
import { BazaFormBuilderHtmlModule } from './form-builder/controls/baza-form-builder-html';
import { BazaFormBuilderKeyValueComponent } from './form-builder/controls/baza-form-builder-key-value/baza-form-builder-key-value.component';
import { BazaFormBuilderMailRecipientModule } from './form-builder/controls/baza-form-builder-mail-recipient';
import { BazaFormBuilderNetworksModule } from './form-builder/controls/baza-form-builder-networks';
import { BazaFormBuilderPasswordComponent } from './form-builder/controls/baza-form-builder-password/baza-form-builder-password.component';
import { BazaFormBuilderSeoModule } from './form-builder/controls/baza-form-builder-seo';
import { BazaFormBuilderTextComponent } from './form-builder/controls/baza-form-builder-text/baza-form-builder-text.component';
import { BazaFormBuilderTextareaComponent } from './form-builder/controls/baza-form-builder-textarea/baza-form-builder-textarea.component';
import { BazaFormBuilderUploadModule } from './form-builder/controls/baza-form-builder-upload';
import { BazaAttachmentCacheService } from './services/baza-attachment-cache.service';
import { BazaFormBuilderComponentHintComponent } from './form-builder/components/baza-form-builder-component-hint/baza-form-builder-component-hint.component';

const FORM_CONTROLS_MODULES = [
    BazaFormBuilderHtmlModule,
    BazaFormBuilderAccountModule,
    BazaFormBuilderMailRecipientModule,
    BazaFormBuilderNetworksModule,
    BazaFormBuilderSeoModule,
    BazaFormBuilderUploadModule,
    BazaAclCmsModule,
    NzInputModule,
    NzCheckboxModule,
    NzDividerModule,
    NzDatePickerModule,
    NzRateModule,
    NzSelectModule,
    NzToolTipModule,
    NzSliderModule,
    NzSwitchModule,
    NzTimePickerModule,
];

const FORM_CONTROLS_DECLARATIONS = [
    BazaFormBuilderPasswordComponent,
    BazaFormBuilderEmailComponent,
    BazaFormBuilderTextComponent,
    BazaFormBuilderTextareaComponent,
    BazaFormBuilderDateRangeComponent,
    BazaFormBuilderCheckboxComponent,
    BazaFormBuilderKeyValueComponent,
];

const FORM_STATIC_COMPONENTS_MODULES = [];

const FORM_STATIC_COMPONENTS_DECLARATIONS = [BazaFormBuilderComponentTimestampComponent, BazaFormBuilderComponentHintComponent];

// @dynamic
@NgModule({
    imports: [
        CommonModule,
        NzTabsModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule,
        NzFormModule,
        BazaNgCoreModule,
        NzDrawerModule,
        NzButtonModule,
        NzIconModule,
        NzSpaceModule,
        NzSpinModule,
        NzRateModule,
        BazaAwsDataAccessModule,
        NzTypographyModule,
        NzModalModule,
        NzInputModule,
        NzDividerModule,
        NzImageModule,
        NzGridModule,
        ...FORM_CONTROLS_MODULES,
        ...FORM_STATIC_COMPONENTS_MODULES,
    ],
    declarations: [
        BazaFormBuilderComponent,
        BazaFormBuilderContentsComponent,
        BazaFormBuilderContentsStaticComponent,
        BazaFormBuilderContentsFormControlComponent,
        BazaFormBuilderContentsFormControlPortalComponent,
        BazaFormLayoutComponent,
        BazaFormLayoutModalComponent,
        BazaFormLayoutActionsComponent,
        BazaFormBuilderTextComponent,
        ...FORM_CONTROLS_DECLARATIONS,
        ...FORM_STATIC_COMPONENTS_DECLARATIONS,
    ],
    providers: [BazaAttachmentCacheService],
    entryComponents: [BazaFormBuilderContentsFormControlPortalComponent],
    exports: [BazaFormBuilderComponent, BazaFormLayoutActionsComponent, BazaFormBuilderTextComponent, BazaFormBuilderContentsComponent],
})
export class BazaFormBuilderModule {}
