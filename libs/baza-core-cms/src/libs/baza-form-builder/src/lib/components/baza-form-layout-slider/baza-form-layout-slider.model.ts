import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { BazaFormLayoutAction } from '../baza-form-layout-actions/baza-form-layout-actions.model';
import { BazaFormBuilder } from '../../models/baza-form-builder-layouts';

export const FORM_LAYOUT_DEFAULT_WIDTH = 540;

type PopulateSync<T> = (input: T, formGroup: FormGroup) => void;
type PopulateAsync<T> = (input: T, formGroup: FormGroup) => Observable<void>;

type FetchSync<T> = () => T;
type FetchAsync<T> = () => Observable<T>;

export function defaultFormLayoutConfig<T = any, FORM_VALUE = T, SUBMIT_RESPONSE = T>(): Partial<
    BazaFormLayoutConfig<T, FORM_VALUE, SUBMIT_RESPONSE>
> {
    return {
        width: FORM_LAYOUT_DEFAULT_WIDTH,
    };
}

export interface BazaFormLayoutConfig<T, FORM_VALUE = T, SUBMIT_RESPONSE = T> {
    title: string;
    withoutTranslate?: boolean;
    width?: number;
    actions?: (defaults: { cancel: BazaFormLayoutAction<T>; submit: BazaFormLayoutAction<T> }) => Array<BazaFormLayoutAction<T>>;
    formGroup: FormGroup;
    formBuilder: BazaFormBuilder<any>;
    fetch?: FetchSync<T> | FetchAsync<T>;
    populate?: 'auto' | 'disabled' | PopulateSync<T> | PopulateAsync<T>;
    onSubmit?: (entity: T, formValue: FORM_VALUE) => Observable<SUBMIT_RESPONSE> | Promise<SUBMIT_RESPONSE> | void;
    afterSubmit?: (response: SUBMIT_RESPONSE) => Observable<any> | any;
    disableSubmit$?: Observable<boolean>;
    withoutSubmit?: boolean;
}
