import {
    ComponentFactoryResolver,
    ComponentRef,
    Inject,
    Injectable,
    PLATFORM_ID,
    ReflectiveInjector,
    SimpleChange,
    ViewContainerRef,
} from '@angular/core';
import { BazaFormLayoutComponent } from './baza-form-layout-slider.component';
import { take } from 'rxjs/operators';
import { BazaFormLayoutConfig, defaultFormLayoutConfig } from './baza-form-layout-slider.model';
import { isPlatformBrowser } from '@angular/common';

export interface BazaFormLayoutSliderOptions {
    childVcr?: ViewContainerRef;
}

@Injectable({
    providedIn: 'root',
})
export class BazaFormLayoutService {
    private _vcr: ViewContainerRef;

    constructor(
        // eslint-disable-next-line @typescript-eslint/ban-types
        @Inject(PLATFORM_ID) private platformId: Object,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    set vcr(vcr: ViewContainerRef) {
        this._vcr = vcr;
    }

    open<T, FORM_VALUE = T, SUBMIT_RESPONSE = T>(
        config: BazaFormLayoutConfig<T, FORM_VALUE, SUBMIT_RESPONSE>,
        options: BazaFormLayoutSliderOptions = {},
    ): ComponentRef<BazaFormLayoutComponent> {
        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaFormLayoutComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], options.childVcr?.injector || this._vcr.injector);

        const componentRef = factory.create(injector);

        componentRef.instance.config = { ...defaultFormLayoutConfig(), ...config } as BazaFormLayoutConfig<unknown, unknown, unknown>;
        componentRef.instance.childVcr = options.childVcr || this._vcr;
        componentRef.instance.ngOnChanges({
            config: new SimpleChange(undefined, componentRef.instance.config, true),
            childVcr: new SimpleChange(undefined, componentRef.instance.childVcr, true),
        });

        componentRef.changeDetectorRef.markForCheck();

        componentRef.instance.closeEvent.pipe(take(1)).subscribe(() => {
            componentRef.destroy();

            if (isPlatformBrowser(this.platformId) && window) {
                window['bazaFormSlider'] = () => undefined;
            }
        });

        this._vcr.insert(componentRef.hostView);

        if (isPlatformBrowser(this.platformId) && window) {
            // eslint-disable-next-line no-console
            window['bazaFormSlider'] = () => componentRef;
        }

        return componentRef;
    }
}
