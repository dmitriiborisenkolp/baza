import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewContainerRef,
} from '@angular/core';
import {
    BazaLoadingService,
    BazaNgMessagesService,
    BazaNzButtonStyle,
    genericRetryStrategy,
    ngTriggerFormValidations,
} from '@scaliolabs/baza-core-ng';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { distinctUntilChanged, finalize, map, retryWhen, switchMap, takeUntil, tap } from 'rxjs/operators';
import { BazaFormLayoutConfig, defaultFormLayoutConfig } from './baza-form-layout-slider.model';
import {
    BazaFormLayoutAction,
    BazaFormLayoutActionPlacement,
    BazaFormLayoutActionType,
} from '../baza-form-layout-actions/baza-form-layout-actions.model';

interface State<T> {
    entity?: T;
}

@Component({
    templateUrl: './baza-form-layout-slider.component.html',
    styleUrls: ['./baza-form-layout-slider.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormLayoutComponent<T = any, FORM_VALUE = any, SUBMIT_RESPONSE = T> implements OnChanges, OnInit, OnDestroy {
    @Input() config: BazaFormLayoutConfig<T, FORM_VALUE, SUBMIT_RESPONSE>;
    @Input() childVcr: ViewContainerRef;

    // TODO: Update output names here
    // eslint-disable-next-line @angular-eslint/no-output-native
    @Output('close') closeEvent: EventEmitter<void> = new EventEmitter<void>();

    // TODO: Update output names here
    // eslint-disable-next-line @angular-eslint/no-output-native
    @Output('submit') submitEvent: EventEmitter<SUBMIT_RESPONSE> = new EventEmitter<SUBMIT_RESPONSE>();

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();
    private readonly ngOnChanges$: Subject<void> = new Subject<void>();
    private readonly nextPopulate$: Subject<void> = new Subject<void>();
    private readonly nextFetch$: Subject<void> = new Subject<void>();
    private readonly nextSubmit$: Subject<void> = new Subject<void>();

    private readonly isFormValid$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    public state: State<T> = {};

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['config']) {
            this.ngOnChanges$.next();

            this.config = {
                ...defaultFormLayoutConfig<T, FORM_VALUE, SUBMIT_RESPONSE>(),
                ...this.config,
            };

            this.config.formGroup.statusChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnChanges$)).subscribe((next) => {
                this.isFormValid$.next(next === 'VALID');
            });

            if (this.config.fetch) {
                this.fetch()
                    .pipe(takeUntil(this.nextFetch$))
                    .subscribe((entity) => {
                        this.state = {
                            ...this.state,
                            entity,
                        };

                        this.populate();

                        this.cdr.markForCheck();
                    });
            }
        }
    }

    ngOnInit(): void {
        setTimeout(() => {
            this.config.formGroup.updateValueAndValidity();
        });

        this.loading.isLoading$.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe(() => {
            // TODO: Investigate issues with CDR & Loading/Disabled status
            setTimeout(() => this.cdr.detectChanges());
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
        this.ngOnChanges$.next();
        this.nextPopulate$.next();
        this.nextFetch$.next();
        this.nextSubmit$.next();
    }

    get isLoading$(): Observable<boolean> {
        return this.loading.isLoading$;
    }

    get isFormInvalid$(): Observable<boolean> {
        return this.isFormValid$.pipe(map((isValid) => !isValid));
    }

    get nzDrawerBodyStyle(): Record<string, unknown> {
        return {
            'overflow-x': 'hidden',
            'overflow-y': 'auto',
        };
    }

    i18n(key: string): string {
        return `baza.cms.layout.layouts.formLayout.${key}`;
    }

    fetch(): Observable<T> {
        this.nextFetch$.next();

        if (this.config.fetch) {
            const fetchResponse = this.config.fetch();

            if (fetchResponse instanceof Observable) {
                return fetchResponse.pipe(retryWhen(genericRetryStrategy()), takeUntil(this.nextFetch$));
            } else {
                return of(fetchResponse);
            }
        } else {
            return of(this.state.entity);
        }
    }

    populate(): void {
        this.nextPopulate$.next();

        if (this.config.populate === 'auto' || this.config.populate === undefined) {
            this.populateObject(this.state.entity);
        } else if (typeof this.config.populate === 'function') {
            const populateRes = this.config.populate(this.state.entity, this.config.formGroup);

            if (populateRes instanceof Observable) {
                populateRes.pipe(takeUntil(this.nextPopulate$)).subscribe();
            }
        }
    }

    populateObject(entity: T): void {
        if (entity) {
            const patch: { [key: string]: any } = {};

            Object.keys(entity).forEach((key) => {
                if (this.config.formGroup.controls[`${key}`]) {
                    patch[`${key}`] = entity[`${key}`];
                }
            });

            this.config.formGroup.patchValue(patch);
        } else {
            this.config.formGroup.reset();
        }

        this.cdr.markForCheck();
    }

    close(): void {
        this.closeEvent.emit();
    }

    get actions(): Array<BazaFormLayoutAction<T>> {
        if (this.config.actions) {
            return this.config.actions({
                submit: this.defaultActionSubmit,
                cancel: this.defaultActionCancel,
            });
        } else {
            return this.defaultActions;
        }
    }

    get defaultActions(): Array<BazaFormLayoutAction<T>> {
        return [this.defaultActionSubmit, this.defaultActionCancel];
    }

    get defaultActionSubmit(): BazaFormLayoutAction {
        return {
            type: BazaFormLayoutActionType.Submit,
            placement: BazaFormLayoutActionPlacement.Left,
            options: {
                title: this.i18n('actions.submit'),
                style: BazaNzButtonStyle.Primary,
                callback: () => this.onActionSubmit(),
                disabled$: this.isLoading$,
            },
        };
    }

    get defaultActionCancel(): BazaFormLayoutAction {
        return {
            type: BazaFormLayoutActionType.Button,
            placement: BazaFormLayoutActionPlacement.Left,
            options: {
                title: this.i18n('actions.cancel'),
                style: BazaNzButtonStyle.Default,
                callback: () => this.close(),
            },
        };
    }

    onActionClick(action: BazaFormLayoutAction<T>): void {
        switch (action.type) {
            case BazaFormLayoutActionType.Button:
            case BazaFormLayoutActionType.Submit: {
                action.options.callback(this.state.entity, {
                    close: () => this.close(),
                    submit: () => this.onActionSubmit(),
                });

                break;
            }
        }
    }

    onActionSubmit(): void {
        this.nextSubmit$.next();

        const form = this.config.formGroup;

        if (!form.valid) {
            ngTriggerFormValidations(form);
        }

        const loading = this.loading.addLoading();

        let response: SUBMIT_RESPONSE;

        const submitResult = this.config.onSubmit(this.state.entity, form.value);

        if (submitResult instanceof Promise) {
            submitResult
                .then((response) => {
                    if (this.config.afterSubmit) {
                        const afterSubmit = this.config.afterSubmit(response);

                        return afterSubmit instanceof Observable ? afterSubmit : of(undefined);
                    } else {
                        return of(undefined);
                    }
                })
                .catch((err) => {
                    this.ngMessages.bazaError(err);

                    form.enable();
                })
                .finally(() => {
                    this.cdr.markForCheck();

                    loading.complete();
                });
        } else if (submitResult instanceof Observable) {
            submitResult
                .pipe(
                    tap((result) => (response = result)),
                    switchMap((response) => {
                        if (this.config.afterSubmit) {
                            const afterSubmit = this.config.afterSubmit(response);

                            return afterSubmit instanceof Observable ? afterSubmit : of(undefined);
                        } else {
                            return of(undefined);
                        }
                    }),
                    finalize(() => loading.complete()),
                    tap(() => this.cdr.markForCheck()),
                    takeUntil(this.nextSubmit$),
                )
                .subscribe(
                    () => {
                        this.submitEvent.emit(response);

                        this.close();
                    },
                    (err) => {
                        this.ngMessages.bazaError(err);

                        form.enable();
                    },
                );
        } else {
            loading.complete();

            this.submitEvent.emit(response);

            this.close();
        }

        form.disable();
    }
}
