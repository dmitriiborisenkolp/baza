import { ChangeDetectionStrategy, Component, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { isNotNullOrUndefined } from '@scaliolabs/baza-core-shared';
import { TranslateService } from '@ngx-translate/core';
import { BazaFormBuilderFormControlComponents } from '../../../models/baza-form-builder-fields';
import { BazaFormControl, getBazaFormBuilderFormControlConfig } from '../../../models/baza-form-builder-control';
import { BazaFormFieldWithText } from '../../../models/baza-form-builder-with-text';
import { NzTSType } from 'ng-zorro-antd/core/types';
import { ThemeType } from '@ant-design/icons-angular';

@Component({
    selector: 'baza-form-builder-contents-form-control',
    templateUrl: './form-control.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./form-control.component.less'],
})
export class BazaFormBuilderContentsFormControlComponent {
    @Input() childVcr: ViewContainerRef;

    @Input() formGroup: FormGroup;
    @Input() definition: BazaFormBuilderFormControlComponents;

    constructor(private readonly translate: TranslateService) {}

    get withNzFormContainer(): boolean {
        return getBazaFormBuilderFormControlConfig(this.definition.type).enableNzFormContainer;
    }

    label$(field: BazaFormControl): Observable<string> {
        return field.labelWithoutTranslate || !field.label ? of(field.label) : this.translate.get(field.label);
    }

    placeholder$(field: BazaFormControl): Observable<string> {
        if (field.placeholder) {
            return field.placeholderWithoutTranslate || !field.placeholder ? of(field.placeholder) : this.translate.get(field.placeholder);
        } else {
            return this.label$(field);
        }
    }

    required$(field: BazaFormControl): Observable<boolean> {
        if (isNotNullOrUndefined(field.required)) {
            if (field.required instanceof Observable) {
                return field.required;
            } else {
                return of(field.required);
            }
        } else {
            const control = this.formGroup.get(field.formControlName);

            if (control.validator) {
                const validator = control.validator('' as any);
                const hasRequiredValidator = !!validator && validator['required'];

                return of(hasRequiredValidator);
            } else {
                return of(false);
            }
        }
    }

    nzExtra(field: BazaFormControl): string | TemplateRef<void> {
        if (!field.hint) {
            return '';
        }

        return this.translateField(field.hint);
    }

    nzTooltipTitle(field: BazaFormControl): NzTSType {
        if (!field.help) {
            return '';
        }

        return this.translateField(field.help);
    }

    translateField(field: BazaFormFieldWithText): string | any {
        if (field.translate) {
            return this.translate.instant(field.text, field.translateArgs);
        } else {
            return field.text;
        }
    }

    nzToolTipIconTheme(): ThemeType {
        return 'outline';
    }

    nzToolTipIconType(field: BazaFormControl): string {
        if (field.help.icon) {
            return field.help.icon.type as string;
        }

        return 'info-circle';
    }
}
