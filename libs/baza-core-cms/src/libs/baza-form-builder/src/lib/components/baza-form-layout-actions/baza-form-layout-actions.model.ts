import { BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { Observable } from 'rxjs';

export enum BazaFormLayoutActionType {
    Button = 'Button',
    Submit = 'Submit',
}

export enum BazaFormLayoutActionPlacement {
    Left = 'left',
    Right = 'right',
}

export interface FormLayoutAction<T> {
    id?: string;
    type: BazaFormLayoutActionType;
    placement?: BazaFormLayoutActionPlacement;
}

export type BazaFormLayoutAction<T = any> =
    BazaFormLayoutButton |
    BazaFormLayoutSubmit
;

interface BazaFormLayoutButton<T = any> extends FormLayoutAction<T> {
    type: BazaFormLayoutActionType.Button;
    options: BazaFormLayoutButtonOptions<T>;
}

interface BazaFormLayoutSubmit<T = any> extends FormLayoutAction<T> {
    type: BazaFormLayoutActionType.Submit;
    options: BazaFormLayoutButtonOptions<T>;
}

export interface BazaFormLayoutButtonOptions<T = any> {
    title: string;
    withoutTranslate?: boolean;
    style: BazaNzButtonStyle;
    isDanger?: boolean;
    disabled$?: Observable<boolean>;
    callback: (entity: T, callbacks: {
        close: () => void;
        submit: () => void;
    }) => void;
}
