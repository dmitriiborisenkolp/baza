import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnChanges,
    OnDestroy,
    SimpleChanges,
    ViewContainerRef,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { BazaFormBuilderContents } from '../../models/baza-form-builder-layouts';
import {
    BazaCoreFormBuilderComponents,
    BazaFormBuilderFormControlComponents,
    BazaFormBuilderStaticComponents,
} from '../../models/baza-form-builder-fields';
import { BazaFormControl } from '../../models/baza-form-builder-control';
import { bazaFormBuilderIsFormControlComponent, bazaFormBuilderIsStaticComponent } from '../../models/baza-form-builder-component-types';
import { BazaFormBuilderComponentStaticOptions } from '../../models/baza-form-builder-component-static';
import { BazaAclNgService } from '@scaliolabs/baza-core-ng';
import { toObservable } from '../../util/to-observable.util';

@Component({
    selector: 'baza-form-builder-contents',
    templateUrl: './baza-form-builder-contents.component.html',
    styleUrls: ['./baza-form-builder-contents.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormBuilderContentsComponent implements OnChanges, OnDestroy {
    @Input() contents: BazaFormBuilderContents;
    @Input() formGroup: FormGroup;
    @Input() childVcr: ViewContainerRef;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly translate: TranslateService,
        private readonly acl: BazaAclNgService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['contents']) {
            for (const field of this.contents.fields) {
                if (field && !field.id) {
                    field.id = generateRandomHexString();
                }

                if ((field as BazaFormControl).formControlName) {
                    const formControl = field as BazaFormControl;

                    combineLatest([
                        toObservable(formControl.disabled),
                        this.acl$(field).pipe(map((enabled) => !enabled)),
                        this.visible$(field).pipe(map((enabled) => !enabled)),
                    ])
                        .pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$))
                        .subscribe((disabledByAnyReason: Array<boolean>) => {
                            disabledByAnyReason.includes(true)
                                ? this.formGroup.get(formControl.formControlName)?.disable()
                                : this.formGroup.get(formControl.formControlName)?.enable();
                        });
                }
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    isStaticComponent(component: BazaCoreFormBuilderComponents) {
        return bazaFormBuilderIsStaticComponent(component.type);
    }

    castStaticComponent(component: BazaCoreFormBuilderComponents): BazaFormBuilderStaticComponents {
        return component as BazaFormBuilderStaticComponents;
    }

    isFormControlComponent(component: BazaCoreFormBuilderComponents) {
        return bazaFormBuilderIsFormControlComponent(component.type);
    }

    castFormControlComponent(component: BazaCoreFormBuilderComponents): BazaFormBuilderFormControlComponents {
        return component as BazaFormBuilderFormControlComponents;
    }

    trackByFieldId(index: number, field: BazaCoreFormBuilderComponents): any {
        return field.id;
    }

    ngComponentClass(component: BazaFormControl | BazaFormBuilderComponentStaticOptions) {
        return [`component-${component.type}`];
    }

    acl$(component: BazaFormControl | BazaFormBuilderComponentStaticOptions): Observable<boolean> {
        if (component.acl !== undefined) {
            return of(this.acl.hasAccess(component.acl));
        } else {
            return of(true);
        }
    }

    visible$(component: BazaFormControl | BazaFormBuilderComponentStaticOptions): Observable<boolean> {
        if (component.visible !== undefined) {
            return component.visible instanceof Observable ? component.visible : of(component.visible);
        } else {
            return of(true);
        }
    }
}
