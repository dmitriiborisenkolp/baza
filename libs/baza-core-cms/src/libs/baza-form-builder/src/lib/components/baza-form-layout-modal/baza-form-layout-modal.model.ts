import { FormGroup } from '@angular/forms';
import { BazaFormLayoutAction } from '../baza-form-layout-actions/baza-form-layout-actions.model';
import { Observable } from 'rxjs';
import { BazaFormBuilder } from '../../models/baza-form-builder-layouts';

type PopulateSync<T> = (input: T, formGroup: FormGroup) => void;
type PopulateAsync<T> = (input: T, formGroup: FormGroup) => Observable<void>;

type FetchSync<T> = () => T;
type FetchAsync<T> = () => Observable<T>;

export interface BazaFormLayoutModal<T, FORM_VALUE = T, SUBMIT_RESPONSE = T> {
    title?: string;
    withoutTranslate?: boolean;
    titleTransaleArgs?: any;
    width?: number;
    formGroup: FormGroup;
    formBuilder: BazaFormBuilder<any>;
    actions?: (defaults: { cancel: BazaFormLayoutAction<T>; submit: BazaFormLayoutAction<T> }) => Array<BazaFormLayoutAction<T>>;
    fetch?: FetchSync<T> | FetchAsync<T>;
    populate?: 'auto' | 'disabled' | PopulateSync<T> | PopulateAsync<T>;
    onSubmit?: (entity: T, formValue: FORM_VALUE) => Observable<SUBMIT_RESPONSE>;
    afterSubmit?: (response: SUBMIT_RESPONSE) => Observable<any> | any;
    disableSubmit$?: Observable<boolean>;
}

export function defaultFormLayoutModalConfig<T, FORM_VALUE = T, SUBMIT_RESPONSE = T>(): Partial<BazaFormLayoutModal<any>> {
    return {
        width: 520,
    };
}
