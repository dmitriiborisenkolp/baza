import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ComponentFactoryResolver,
    ComponentRef,
    Input,
    OnChanges,
    SimpleChange,
    SimpleChanges,
    ViewContainerRef,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { getBazaFormBuilderStaticComponentConfig } from '../../../models/baza-form-builder-component-static';
import { BazaFormBuilderStaticComponents } from '../../../models/baza-form-builder-fields';

@Component({
    selector: 'baza-form-builder-contents-static-component',
    templateUrl: './static-component.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormBuilderContentsStaticComponent implements OnChanges, AfterViewInit {
    @Input() childVcr: ViewContainerRef;
    @Input() definition: BazaFormBuilderStaticComponents;

    private componentRef: ComponentRef<any>;
    private isFirstChange = true;

    constructor(
        private readonly vcr: ViewContainerRef,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
        private readonly translateService: TranslateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['definition'] && this.componentRef) {
            const componentConfig = getBazaFormBuilderStaticComponentConfig(this.definition.type);

            if (componentConfig.transformProps) {
                componentConfig.transformProps(this.definition, {
                    translate: this.translateService,
                });
            }

            Object.assign(this.componentRef.instance, this.definition.props);

            this.componentRef.changeDetectorRef.markForCheck();
        }
    }

    ngAfterViewInit(): void {
        const componentConfig = getBazaFormBuilderStaticComponentConfig(this.definition.type);
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentConfig.component);
        const componentRef = this.vcr.createComponent(componentFactory, 0, this.childVcr?.injector || this.vcr.injector, undefined);

        if (componentConfig.transformProps) {
            componentConfig.transformProps(this.definition, {
                translate: this.translateService,
            });
        }

        this.componentRef = componentRef;

        const ngOnChangesObject: SimpleChanges = {};

        if (this.definition.props && this.componentRef.instance.ngOnChanges) {
            for (const key of Object.keys(this.definition.props)) {
                ngOnChangesObject[`${key}`] = new SimpleChange(
                    this.componentRef.instance[`${key}`],
                    this.definition.props[`${key}`],
                    this.isFirstChange,
                );
            }
        }

        Object.assign(componentRef.instance, this.definition.props);

        if (this.componentRef.instance.ngOnChanges) {
            this.componentRef.instance.ngOnChanges(ngOnChangesObject);
        }

        componentRef.changeDetectorRef.detectChanges();
    }
}
