import {
    ChangeDetectionStrategy,
    Component,
    ComponentFactoryResolver,
    ComponentRef,
    forwardRef,
    Injector,
    Input,
    OnChanges,
    OnInit,
    SimpleChange,
    SimpleChanges,
    ViewContainerRef,
} from '@angular/core';
import { FormGroup, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ngIsControlValueAccessorComponent } from '@scaliolabs/baza-core-ng';
import { getBazaFormBuilderFormControlConfig } from '../../../models/baza-form-builder-control';
import { BazaFormBuilderFormControlComponents } from '../../../models/baza-form-builder-fields';

@Component({
    selector: 'baza-form-builder-contents-form-control-portal',
    templateUrl: './form-control-portal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaFormBuilderContentsFormControlPortalComponent),
            multi: true,
        },
    ],
})
export class BazaFormBuilderContentsFormControlPortalComponent<C = any> implements OnInit, OnChanges {
    @Input() childVcr: ViewContainerRef;

    @Input() formGroup: FormGroup;
    @Input() formControlName: string;
    @Input() definition: BazaFormBuilderFormControlComponents;
    @Input() placeholder: string;
    @Input() label: string;

    private componentRef: ComponentRef<C>;
    private isFirstChange = true;

    constructor(
        public injector: Injector,
        private readonly vcr: ViewContainerRef,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
        private readonly translateService: TranslateService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['definition'] && this.componentRef) {
            this.updateProps();

            this.componentRef.changeDetectorRef.markForCheck();
        }
    }

    ngOnInit(): void {
        const componentConfig = getBazaFormBuilderFormControlConfig(this.definition.type);
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentConfig.component);
        const componentRef = this.vcr.createComponent(componentFactory, 0, this.childVcr?.injector || this.vcr.injector);

        const ngControl = this.injector.get(NgControl);
        ngControl.valueAccessor = componentRef.instance;

        this.componentRef = componentRef;

        this.updateProps();

        if (this.definition.afterViewInit) {
            this.definition.afterViewInit(this.componentRef, this);
        }

        componentRef.changeDetectorRef.detectChanges();
    }

    updateProps(): void {
        const componentConfig = getBazaFormBuilderFormControlConfig(this.definition.type);

        if (componentConfig.transformProps) {
            componentConfig.transformProps(this.definition, {
                translate: this.translateService,
            });
        }

        if (ngIsControlValueAccessorComponent(this.componentRef.instance)) {
            // const ngControl = this.componentRef.injector.get(NgControl);
        }

        this.componentRef.instance['formGroup'] = this.formGroup;
        this.componentRef.instance['formControlName'] = this.definition.formControlName;
        this.componentRef.instance['placeholder'] = this.placeholder;
        this.componentRef.instance['label'] = this.label;
        this.componentRef.instance['definition'] = this.definition;

        const ngOnChangesObject: SimpleChanges = {};

        if (this.definition.props && (this.componentRef.instance as any).ngOnChanges) {
            for (const key of Object.keys(this.definition.props)) {
                ngOnChangesObject[`${key}`] = new SimpleChange(
                    this.componentRef.instance[`${key}`],
                    this.definition.props[`${key}`],
                    this.isFirstChange,
                );
            }
        }

        Object.assign(this.componentRef.instance, this.definition.props);

        if ((this.componentRef.instance as any).ngOnChanges) {
            (this.componentRef.instance as any).ngOnChanges(ngOnChangesObject);
        }

        if (this.isFirstChange) {
            this.isFirstChange = false;
        }
    }
}
