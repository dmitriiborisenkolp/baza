import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges } from '@angular/core';
import { BazaFormLayoutAction, BazaFormLayoutActionPlacement, BazaFormLayoutActionType } from './baza-form-layout-actions.model';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';
import { Observable, of, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
    selector: 'baza-form-layout-actions',
    templateUrl: './baza-form-layout-actions.component.html',
    styleUrls: ['./baza-form-layout-actions.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormLayoutActionsComponent implements OnChanges, OnDestroy {
    @Input() actions: Array<BazaFormLayoutAction> = [];
    @Input() submitDisabled$: Observable<boolean> = of(false);
    @Input() isLoading = false;

    // TODO: Update output names here
    // eslint-disable-next-line @angular-eslint/no-output-native
    @Output('submit') submitEvent: EventEmitter<BazaFormLayoutAction> = new EventEmitter<BazaFormLayoutAction>();

    // TODO: Update output names here
    // eslint-disable-next-line @angular-eslint/no-output-native
    @Output('click') clickEvent: EventEmitter<BazaFormLayoutAction> = new EventEmitter<BazaFormLayoutAction>();

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['config']) {
            for (const action of this.actions) {
                if (! action.id) {
                    action.id = generateRandomHexString();
                }
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    trackBySID(index: number, item: BazaFormLayoutAction): string {
        return item.id;
    }

    actionsOfPlacements(placement: BazaFormLayoutActionPlacement): Array<BazaFormLayoutAction> {
        switch (placement) {
            case BazaFormLayoutActionPlacement.Left:
                return this.leftActions;

            case BazaFormLayoutActionPlacement.Right:
                return this.rightActions;
        }
    }

    get leftActions(): Array<BazaFormLayoutAction> {
        return this.actions.filter((action) => ! action.placement || action.placement === BazaFormLayoutActionPlacement.Left);
    }

    get rightActions(): Array<BazaFormLayoutAction> {
        return this.actions.filter((action) => action.placement === BazaFormLayoutActionPlacement.Right);
    }

    isButtonDisabled$(action: BazaFormLayoutAction): Observable<boolean> {
        if (action.type === BazaFormLayoutActionType.Submit) {
            return this.submitDisabled$.pipe(
                switchMap((isDisabled) => {
                    if (isDisabled) {
                        return of(true);
                    } else {
                        return action.options.disabled$
                            ? action.options.disabled$
                            : of(false);
                    }
                }),
            )
        } else if (action.type === BazaFormLayoutActionType.Button) {
            if (! action.options.disabled$) {
                return of(false);
            } else {
                return action.options.disabled$;
            }
        } else {
            return of(false);
        }
    }

    onButtonClick(action: BazaFormLayoutAction): void {
        this.clickEvent.emit(action);
    }
}
