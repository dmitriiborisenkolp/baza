import {
    ComponentFactoryResolver,
    ComponentRef,
    Inject,
    Injectable,
    PLATFORM_ID,
    ReflectiveInjector,
    SimpleChange,
    ViewContainerRef,
} from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BazaFormLayoutModalComponent } from './baza-form-layout-modal.component';
import { BazaFormLayoutModal, defaultFormLayoutModalConfig } from './baza-form-layout-modal.model';
import { take, takeUntil } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd/modal/modal-ref';
import { isPlatformBrowser } from '@angular/common';

export interface BazaFormLayoutModalOptions {
    childVcr?: ViewContainerRef;
    nzClosable?: boolean;
}

@Injectable({
    providedIn: 'root',
})
export class BazaFormLayoutModalService {
    private _vcr: ViewContainerRef;

    constructor(
        // eslint-disable-next-line @typescript-eslint/ban-types
        @Inject(PLATFORM_ID) private platformId: Object,
        private readonly nzModal: NzModalService,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
    ) {}

    set vcr(vcr: ViewContainerRef) {
        this._vcr = vcr;
    }

    open<T, FORM_VALUE = T, SUBMIT_RESPONSE = T>(
        config: BazaFormLayoutModal<T, FORM_VALUE, SUBMIT_RESPONSE>,
        options: BazaFormLayoutModalOptions = {},
    ): ComponentRef<BazaFormLayoutModalComponent> {
        let nzModalRef: NzModalRef;

        const factory = this.componentFactoryResolver.resolveComponentFactory(BazaFormLayoutModalComponent);
        const injector = ReflectiveInjector.fromResolvedProviders([], this._vcr.injector);

        const componentRef = factory.create(injector);

        componentRef.instance.childVcr = options.childVcr || this._vcr;
        componentRef.instance.config = { ...defaultFormLayoutModalConfig(), ...config } as BazaFormLayoutModal<unknown, unknown, unknown>;

        componentRef.instance.ngOnChanges({
            config: new SimpleChange(undefined, componentRef.instance.config, true),
            childVcr: new SimpleChange(undefined, componentRef.instance.childVcr, true),
        });

        this._vcr.insert(componentRef.hostView);

        setTimeout(() => {
            nzModalRef = this.nzModal.create({
                nzContent: componentRef.instance.nzModal,
                nzFooter: componentRef.instance.nzModalFooter,
                nzClosable: options.nzClosable,
                nzWidth: componentRef.instance.config.width,
            });

            componentRef.instance.closeEvent.pipe(take(1), takeUntil(nzModalRef.afterClose)).subscribe(() => nzModalRef.close());

            nzModalRef.afterClose.pipe(take(1)).subscribe(() => {
                if (isPlatformBrowser(this.platformId) && window) {
                    window['bazaFormModal'] = () => undefined;
                }
            });

            config.formGroup.updateValueAndValidity();
        });

        if (isPlatformBrowser(this.platformId) && window) {
            // eslint-disable-next-line no-console
            window['bazaFormModal'] = () => componentRef;
        }

        return componentRef;
    }
}
