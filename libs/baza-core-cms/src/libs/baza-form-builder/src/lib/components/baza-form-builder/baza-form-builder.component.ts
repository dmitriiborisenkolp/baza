import {
    ChangeDetectionStrategy,
    Component,
    Inject,
    Input,
    OnChanges,
    OnDestroy,
    PLATFORM_ID,
    SimpleChanges,
    ViewContainerRef,
} from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { BazaAclNgService } from '@scaliolabs/baza-core-ng';
import { FormGroup } from '@angular/forms';
import { BazaFormBuilder, BazaFormBuilderLayout, BazaFormBuilderTab } from '../../models/baza-form-builder-layouts';
import { BazaFormBuilderInjectService } from '../../services/baza-form-builder-inject.service';
import { isPlatformBrowser } from '@angular/common';
import { BazaFormLayoutModal } from '../baza-form-layout-modal/baza-form-layout-modal.model';
import { BazaFormLayoutConfig } from '../baza-form-layout-slider/baza-form-layout-slider.model';
import { map } from 'rxjs/operators';
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';

@Component({
    selector: 'baza-form-builder',
    templateUrl: './baza-form-builder.component.html',
    styleUrls: ['./baza-form-builder.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaFormBuilderComponent<T = any, FORM_VALUE = T, SUBMIT_RESPONSE = T> implements OnChanges, OnDestroy {
    @Input() config: BazaFormBuilder;
    @Input() formGroup: FormGroup;
    @Input() childVcr: ViewContainerRef;

    @Input() modalConfig?: BazaFormLayoutModal<T, FORM_VALUE, SUBMIT_RESPONSE>;
    @Input() sliderConfig?: BazaFormLayoutConfig<T, FORM_VALUE, SUBMIT_RESPONSE>;

    private readonly ngOnDestroy$ = new Subject<void>();

    private _asyncTabs$: Observable<Array<BazaFormBuilderTab>>;

    constructor(
        private readonly acl: BazaAclNgService,
        private readonly injectService: BazaFormBuilderInjectService,
        // eslint-disable-next-line @typescript-eslint/ban-types
        @Inject(PLATFORM_ID) private platformId: Object,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['config']) {
            if (this.config.id && this.injectService.has(this.config.id)) {
                const injectConfig = this.injectService.get<T, FORM_VALUE, SUBMIT_RESPONSE>(this.config.id);

                if (injectConfig.inject) {
                    injectConfig.inject(this.config, this.formGroup, {
                        childVcr: this.childVcr,
                        takeUntil$: this.ngOnDestroy$,
                        modalConfig: this.modalConfig,
                        sliderConfig: this.sliderConfig,
                    });
                }
            }

            if (isPlatformBrowser(this.platformId) && window) {
                window['bazaFormId'] = () => this.config.id;
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    tabVisible$(tab: BazaFormBuilderTab): Observable<boolean> {
        if (Array.isArray(tab.acl) && tab.acl.length > 0 && !this.acl.hasAccess(tab.acl)) {
            return of(false);
        }

        return tab.visible ? tab.visible : of(true);
    }

    tabDisabled$(tab: BazaFormBuilderTab): Observable<boolean> {
        if (Array.isArray(tab.acl) && tab.acl.length > 0 && !this.acl.hasAccess(tab.acl)) {
            return of(false);
        }

        return tab.disabled ? tab.disabled : of(false);
    }

    get tabs(): Array<BazaFormBuilderTab> {
        return this.config.layout === BazaFormBuilderLayout.WithTabs ? this.config.contents.tabs : [];
    }

    get asyncTabs$(): Observable<Array<BazaFormBuilderTab>> {
        if (!this._asyncTabs$) {
            this._asyncTabs$ =
                this.config.layout === BazaFormBuilderLayout.WithAsyncTabs ? this.config.contents.pipe(map((next) => next.tabs)) : of([]);
        }

        return this._asyncTabs$;
    }

    trackByTabId(index: number, tab: BazaFormBuilderTab): string {
        if (!tab.id) {
            tab.id = generateRandomHexString();
        }

        return tab.id;
    }
}
