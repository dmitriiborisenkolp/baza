import { ChangeDetectionStrategy, Component, forwardRef, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { CoreAcl } from '@scaliolabs/baza-core-shared';
import { BAZA_ACL_CONFIG } from '@scaliolabs/baza-core-shared';
import { JwtService } from '@scaliolabs/baza-core-ng';

export interface AclTab {
    title: string;
    nodes: Array<string>;
}

interface State {
    form: FormGroup;
    tabs: Array<AclTab>;
}

interface FormValue {
    [item: string]: boolean;
}

@Component({
    selector: 'baza-cms-acl-control',
    templateUrl: './baza-cms-acl-control.component.html',
    styleUrls: ['./baza-cms-acl-control.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaCmsAclControlComponent),
            multi: true,
        },
    ],
})
export class BazaCmsAclControlComponent implements ControlValueAccessor, OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    public state: State = {
        form: (() => {
            const formGroupOptions = {};

            BAZA_ACL_CONFIG.groups.forEach((aclGroup) => {
                aclGroup.nodes.forEach((node) => {
                    formGroupOptions[`${node}`] = [false];
                });
            });

            return this.fb.group(formGroupOptions);
        })(),
        tabs: BAZA_ACL_CONFIG.groups.map((aclGroup) => ({
            title: aclGroup.group,
            nodes: aclGroup.nodes,
        })),
    };

    constructor(private readonly fb: FormBuilder, private readonly jwt: JwtService) {}

    ngOnInit(): void {
        this.state.form.valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            if (this.onChange) {
                if (!next) {
                    this.onChange([]);
                } else {
                    const enabledValues = Object.keys(next).filter((key) => !!next[`${key}`]);

                    if (enabledValues.length > 0) {
                        this.onChange(enabledValues);
                    } else {
                        this.onChange([]);
                    }
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get formValue(): FormValue {
        return this.state.form.value;
    }

    get allAclGroup(): Array<string> {
        return BAZA_ACL_CONFIG.groups
            .filter((group) => group.nodes.some((node) => this.hasAccessToSetNode(node)))
            .map((group) => group.group);
    }

    aclNodesOfGroup(group: string): Array<string> {
        return BAZA_ACL_CONFIG.groups.find((a) => a.group === group).nodes.filter((node) => this.hasAccessToSetNode(node));
    }

    hasAccessToSetNode(node: string): boolean {
        return this.jwt.jwtPayload.accountAcl.includes(CoreAcl.SystemRoot) || this.jwt.jwtPayload.accountAcl.includes(node);
    }

    trackByGroup(index: number, group: string): string {
        return group;
    }

    trackByNode(index: number, node: string): string {
        return node;
    }

    i18nGroup(group: string): string {
        return `__acl.${group}.group`;
    }

    i18nNode(group: string, node: string): string {
        return `__acl.${group}.nodes.${node}`;
    }

    get isSystemRootEnabled(): boolean {
        return this.formValue[CoreAcl.SystemRoot];
    }

    isSystemRootNode(input: string): boolean {
        return input === CoreAcl.SystemRoot;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }

    writeValue(obj: Array<string>): void {
        const result: FormValue = {};

        const allAclNodes = (() => {
            const allAclNodes: Array<string> = [];

            for (const group of BAZA_ACL_CONFIG.groups) {
                for (const node of group.nodes) {
                    allAclNodes.push(node);
                }
            }

            return allAclNodes;
        })();

        for (const node of allAclNodes) {
            if (Array.isArray(obj)) {
                result[`${node}`] = obj.includes(node);
            } else {
                result[`${node}`] = false;
            }
        }

        this.state.form.patchValue(result);
    }
}
