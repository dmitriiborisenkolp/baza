import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCmsAclControlComponent } from './components/baza-cms-acl-control/baza-cms-acl-control.component';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { TranslateModule } from '@ngx-translate/core';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaAuthNgModule } from '@scaliolabs/baza-core-ng';

@NgModule({
    imports: [
        CommonModule,
        NzTabsModule,
        TranslateModule,
        NzCheckboxModule,
        ReactiveFormsModule,
        BazaAuthNgModule,
    ],
    declarations: [
        BazaCmsAclControlComponent,
    ],
    exports: [
        BazaCmsAclControlComponent,
    ],
})
export class BazaAclCmsModule {
}
