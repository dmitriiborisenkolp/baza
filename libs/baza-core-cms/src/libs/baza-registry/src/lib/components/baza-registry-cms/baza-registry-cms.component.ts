import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BazaNzButtonStyle, floatValidator, httpsLinkValidator, intValidator } from '@scaliolabs/baza-core-ng';
import { map, tap } from 'rxjs/operators';
import { BazaRegistryDataAccess } from '@scaliolabs/baza-core-data-access';
import { bazaMailRecipientsToTOField, CoreAcl, RegistryNode, RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';
import { of } from 'rxjs';
import {
    BazaFieldUploadPreset,
    BazaFileUploadTransport,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderFormControlComponents,
    BazaFormBuilderLayout,
    BazaFormFieldHint,
    BazaFormLayoutActionPlacement,
    BazaFormLayoutActionType,
    BazaFormLayoutModalService,
} from '../../../../../baza-form-builder/src';
import { BazaCrudComponent, BazaCrudConfig, BazaTableFieldType } from '../../../../../baza-crud/src';
import { TranslateService } from '@ngx-translate/core';

interface State {
    config: BazaCrudConfig<Entry>;
    currentSchema?: RegistrySchema;
    entries: Array<Entry>;
}

interface FormValue {
    label?: string;
    value: string;
}

interface Entry {
    path: string;
    label?: string;
    node: RegistryNode;
}

export const BAZA_CORE_REGISTRY_CMS_ID = 'baza-core-registry-cms-id';
export const BAZA_CORE_REGISTRY_FORM_ID = 'baza-core-registry-form-id';

@Component({
    templateUrl: './baza-registry-cms.component.html',
    styleUrls: ['./baza-registry-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaRegistryCmsComponent {
    public crud: BazaCrudComponent<RegistryNode>;

    public state: State = {
        config: this.crudConfig(),
        entries: [],
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly endpoint: BazaRegistryDataAccess,
        private readonly formLayout: BazaFormLayoutModalService,
        private readonly translate: TranslateService,
    ) {}

    i18n(key: string): string {
        return `baza.registry.components.bazaRegistryCms.${key}`;
    }

    private crudConfig(): BazaCrudConfig<Entry> {
        return {
            id: BAZA_CORE_REGISTRY_CMS_ID,
            title: this.i18n('title'),
            data: {
                dataSource: {
                    array: () =>
                        this.endpoint.currentSchema().pipe(
                            map((currentSchema) => {
                                this.state = {
                                    ...this.state,
                                    currentSchema,
                                };

                                this.rebuildEntries();

                                return {
                                    items: this.state.entries,
                                };
                            }),
                        ),
                },
                actions: [
                    {
                        title: this.i18n('actions.edit'),
                        acl: [CoreAcl.BazaRegistryManagement],
                        icon: 'edit',
                        action: (entity) => {
                            const formGroup = this.formGroup(entity);

                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            return this.formLayout.open<Entry, FormValue, RegistryNode>({
                                formGroup,
                                formBuilder: this.formBuilder(entity),
                                populate: (input, formGroup) =>
                                    formGroup.patchValue({
                                        label: input.node.label || input.node.name,
                                        value: input.node.value,
                                    } as FormValue),
                                title: entity.node.label || entity.node.name,
                                fetch: () => of(entity),
                                actions: (defaults) => [
                                    defaults.cancel,
                                    defaults.submit,
                                    {
                                        type: BazaFormLayoutActionType.Button,
                                        placement: BazaFormLayoutActionPlacement.Left,
                                        options: {
                                            title: this.i18n('forms.actions.setDefaults'),
                                            style: BazaNzButtonStyle.Default,
                                            disabled$: formGroup.valueChanges.pipe(
                                                map((next: FormValue) => next.value === entity.node.defaults),
                                            ),
                                            callback: () => {
                                                formGroup.patchValue({
                                                    value: entity.node.defaults,
                                                    label: entity.node.name,
                                                } as FormValue);
                                            },
                                        },
                                    },
                                ],
                                onSubmit: (entity, formValue) =>
                                    this.endpoint
                                        .updateSchemaRecord({
                                            ...formValue,
                                            path: entity.path,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            });
                        },
                    },
                ],
                columns: [
                    {
                        field: 'label',
                        title: this.i18n('columns.name'),
                        source: (entity) => entity.node.label || entity.node.name,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                    {
                        field: 'value',
                        title: this.i18n('columns.value'),
                        source: (entity) => {
                            switch (entity.node.type) {
                                default: {
                                    return entity.node.value;
                                }

                                case RegistryType.JSON: {
                                    return '<JSON>';
                                }

                                case RegistryType.Boolean: {
                                    return this.translate.instant(
                                        entity.node.value ? this.i18n('forms.boolean.yes') : this.i18n('forms.boolean.no'),
                                    );
                                }

                                case RegistryType.EmailRecipient: {
                                    let result: string = bazaMailRecipientsToTOField([entity.node.value]);

                                    if (Array.isArray(entity.node.value.cc) && entity.node.value.cc.length > 0) {
                                        result += ', CC: ';
                                        result += bazaMailRecipientsToTOField(entity.node.value.cc);
                                    }

                                    return result;
                                }
                            }
                        },
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        ngStyle: () => ({
                            'word-break': 'break-all',
                        }),
                    },
                    {
                        field: 'public',
                        title: this.i18n('columns.public'),
                        source: (entity) => entity.node.public,
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: false,
                        },
                        width: 0,
                    },
                ],
            },
        };
    }

    rebuildEntries(): void {
        const entries: Array<Entry> = [];

        const deep = (node: RegistrySchema | RegistryNode, label: string, path: Array<string>) => {
            if ('name' in node && 'type' in node) {
                if (!(node as RegistryNode).hiddenFromList) {
                    entries.push({
                        node: node as RegistryNode,
                        label: label,
                        path: path.join('.'),
                    });
                }
            } else {
                Object.keys(node).forEach((key) => {
                    if (!key.startsWith('_')) {
                        deep(node[`${key}`], label, [...path, key]);
                    }
                });
            }
        };

        deep(this.state.currentSchema, '', []);

        this.state = {
            ...this.state,
            entries: entries,
        };
    }

    private formGroup(entity: Entry): FormGroup {
        return this.fb.group({
            label: this.factoryFormGroupField({ node: { ...entity.node }, isLabel: true }),
            value: this.factoryFormGroupField({ node: entity.node }),
        });
    }

    private formBuilder(entity: Entry): BazaFormBuilder {
        return {
            id: BAZA_CORE_REGISTRY_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'label',
                        label: 'Name',
                        hint: this.buildHint({ isLabel: true, ...entity.node }),
                    },
                    this.factoryValueField(entity.node),
                ],
            },
        };
    }

    private buildHint(node): BazaFormFieldHint {
        return {
            text: (() => {
                switch (node.type) {
                    default: {
                        return node.hint ? this.i18n('forms.fields.hint_with_node_hint') : this.i18n('forms.fields.hint');
                    }
                    case RegistryType.JSON:
                    case RegistryType.EmailRecipient: {
                        return node.hint ? this.i18n('forms.fields.hint_with_node_hint_no_defaults') : undefined;
                    }
                }
            })(),
            translate: true,
            translateArgs: {
                defaults: node.isLabel ? node.name : node.defaults,
                hint: node.hint,
            },
        };
    }

    private factoryFormGroupField(options: { node: RegistryNode; isLabel?: boolean }): any {
        const defaultValidators: Array<ValidatorFn> = [];

        options.node.type = options.isLabel ? RegistryType.Text : options.node.type;

        if (!options.node.optional) {
            defaultValidators.push(Validators.required);
        }

        switch (options.node.type) {
            default: {
                return [undefined, [...defaultValidators]];
            }

            case RegistryType.Integer: {
                return [0, [...defaultValidators, intValidator]];
            }

            case RegistryType.Double: {
                return [0, [...defaultValidators, floatValidator]];
            }

            case RegistryType.HttpsLink: {
                return [0, [...defaultValidators, httpsLinkValidator]];
            }
        }
    }

    private factoryValueField(node): BazaFormBuilderFormControlComponents {
        const defaults: {
            formControlName: string;
            label: string;
            hint: BazaFormFieldHint;
        } = {
            formControlName: 'value',
            label: this.i18n('forms.fields.value'),
            hint: this.buildHint(node),
        };

        switch (node.type) {
            default: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.Text,
                };
            }

            case RegistryType.Email: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.Email,
                };
            }

            case RegistryType.Double:
            case RegistryType.Integer: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.Number,
                };
            }

            case RegistryType.Boolean: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.Select,
                    values: [
                        {
                            label: this.i18n('forms.boolean.yes'),
                            translate: true,
                            value: true,
                        },
                        {
                            label: this.i18n('forms.boolean.no'),
                            translate: true,
                            value: false,
                        },
                    ],
                };
            }

            case RegistryType.Image: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.BazaUpload,
                    props: {
                        options: {
                            preset: BazaFieldUploadPreset.Image,
                            transport: {
                                type: BazaFileUploadTransport.BazaAWS,
                            },
                        },
                    },
                };
            }

            case RegistryType.File: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.BazaUpload,
                    props: {
                        options: {
                            transport: {
                                type: BazaFileUploadTransport.BazaAWS,
                            },
                        },
                    },
                };
            }

            case RegistryType.Text: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.TextArea,
                };
            }

            case RegistryType.Html: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.BazaHtml,
                };
            }

            case RegistryType.HttpsLink: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.Text,
                };
            }

            case RegistryType.EmailRecipient: {
                return {
                    ...defaults,
                    type: BazaFormBuilderControlType.BazaMailRecipient,
                };
            }
        }
    }
}
