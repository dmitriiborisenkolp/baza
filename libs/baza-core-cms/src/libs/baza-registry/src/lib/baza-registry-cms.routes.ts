import { Routes } from '@angular/router';
import { BazaRegistryCmsComponent } from './components/baza-registry-cms/baza-registry-cms.component';

export const bazaRegistryCmsRoutes: Routes = [
    {
        path: 'registry',
        component: BazaRegistryCmsComponent,
    },
];
