import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaRegistryCmsComponent } from './components/baza-registry-cms/baza-registry-cms.component';
import { BazaRegistryDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaCmsLayoutModule } from '../../../../cms/layout/src';
import { BazaFormBuilderModule } from '../../../baza-form-builder/src';
import { BazaCrudCmsModule } from '../../../baza-crud/src';

@NgModule({
    imports: [
        CommonModule,
        BazaRegistryDataAccessModule,
        BazaCrudCmsModule,
        BazaFormBuilderModule,
        BazaCmsLayoutModule,
    ],
    declarations: [
        BazaRegistryCmsComponent,
    ],
})
export class BazaRegistryCmsModule {
}
