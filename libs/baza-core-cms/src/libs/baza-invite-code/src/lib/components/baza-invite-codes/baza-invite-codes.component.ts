import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudItem, BazaTableFieldType } from '../../../../../baza-crud/src';
import { tap } from 'rxjs/operators';
import {
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutService,
} from '../../../../../baza-form-builder/src';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaNgConfirmService } from '@scaliolabs/baza-core-ng';
import { BazaInviteCodeAcl, BazaInviteCodeCmsDto } from '@scaliolabs/baza-core-shared';
import { BazaInviteCodesCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';

export const BAZA_CORE_INVITE_CODES_CMS_ID = 'baza-core-invite-codes-cms-id';
export const BAZA_CORE_INVITE_CODES_FORM_ID = 'baza-core-invite-codes-form-id';

interface State {
    config: BazaCrudConfig<BazaInviteCodeCmsDto>;
}

interface FormValue {
    code: string;
}

@Component({
    templateUrl: './baza-invite-codes.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaInviteCodesComponent {
    public crud: BazaCrudComponent<BazaInviteCodeCmsDto>;

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly endpoint: BazaInviteCodesCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
    ) {}

    i18n(key: string): string {
        return `baza.inviteCodes.components.bazaInviteCodes.${key}`;
    }

    private crudConfig(): BazaCrudConfig<BazaInviteCodeCmsDto> {
        return {
            id: BAZA_CORE_INVITE_CODES_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<BazaInviteCodeCmsDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (entity, formValue) =>
                                        this.endpoint
                                            .createInviteCode({
                                                code: formValue.code,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                        acl: [BazaInviteCodeAcl.BazaInviteCodesManagement],
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.endpoint.listOfSharedInviteCodes(request),
                },
                actions: [
                    {
                        title: this.i18n('actions.edit'),
                        icon: 'edit',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<BazaInviteCodeCmsDto, FormValue>({
                                formGroup: this.formGroup(),
                                formBuilder: this.formBuilder(),
                                title: this.i18n('actions.add'),
                                fetch: () =>
                                    this.endpoint.getById({
                                        id: entity.id,
                                    }),
                                populate: (entity, formGroup) =>
                                    formGroup.patchValue({
                                        code: entity.codeDisplay,
                                    } as FormValue),
                                onSubmit: (entity, formValue) =>
                                    this.endpoint
                                        .updateInviteCode({
                                            id: entity.id,
                                            code: formValue.code,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            }),
                        acl: [BazaInviteCodeAcl.BazaInviteCodesManagement],
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                confirm: () =>
                                    this.endpoint
                                        .deleteInviteCode({
                                            id: entity.id,
                                        })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            }),
                        acl: [BazaInviteCodeAcl.BazaInviteCodesManagement],
                    },
                ],
                columns: [
                    {
                        field: 'codeDisplay',
                        title: this.i18n('columns.code'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'isConsumed',
                        title: this.i18n('columns.isConsumed'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: true,
                        },
                        width: 260,
                        sortable: true,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            code: [undefined, [Validators.required]],
        });
    }

    private formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_CORE_INVITE_CODES_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'code',
                        label: this.i18n('form.fields.code'),
                        placeholder: this.i18n('form.placeholders.code'),
                        required: true,
                    },
                ],
            },
        };
    }
}
