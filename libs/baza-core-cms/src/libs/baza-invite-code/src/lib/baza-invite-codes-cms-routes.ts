import { Routes } from '@angular/router';
import { BazaInviteCodesComponent } from './components/baza-invite-codes/baza-invite-codes.component';

export const bazaInviteCodesCmsRoutes: Routes = [
    {
        path: 'baza-core/invite-codes',
        component: BazaInviteCodesComponent,
    },
];
