import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCmsLayoutModule } from '../../../../cms/layout/src';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaFormBuilderModule } from '../../../baza-form-builder/src';
import { BazaCrudCmsModule } from '../../../baza-crud/src';
import { BazaInviteCodesComponent } from './components/baza-invite-codes/baza-invite-codes.component';
import { BazaInviteCodesCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaInviteCodesDataAccessModule } from '@scaliolabs/baza-core-data-access';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BazaFormBuilderModule,
        BazaCrudCmsModule,
        BazaCmsLayoutModule,
        BazaInviteCodesDataAccessModule,
        BazaInviteCodesCmsDataAccessModule,
    ],
    declarations: [BazaInviteCodesComponent],
})
export class BazaInviteCodesCmsModule {}
