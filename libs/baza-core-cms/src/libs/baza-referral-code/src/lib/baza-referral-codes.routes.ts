import { Routes } from '@angular/router';
import { BazaReferralComponent } from './baza-referral/baza-referral.component';
import { BazaReferralCodesComponent } from './baza-referral-codes/baza-referral-codes.component';
import { BazaReferralCodesResolve } from './baza-referral-codes/baza-referral-codes.resolve';
import { BazaReferralCodesUsageComponent } from './baza-referral-codes-usage/baza-referral-codes-usage.component';
import { BazaReferralCodesUsageResolve } from './baza-referral-codes-usage/baza-referral-codes-usage.resolve';

export const bazaReferralCodesRoutes: Routes = [
    {
        path: 'baza-core/referral-codes',
        component: BazaReferralComponent,
    },
    {
        path: 'baza-core/referral-codes/campaign/:campaignId',
        component: BazaReferralCodesComponent,
        resolve: {
            crud: BazaReferralCodesResolve,
        },
    },
    {
        path: 'baza-core/referral-codes/account/:accountId',
        component: BazaReferralCodesComponent,
        resolve: {
            crud: BazaReferralCodesResolve,
        },
    },
    {
        path: 'baza-core/referral-codes/campaign/:campaignId/usage',
        component: BazaReferralCodesUsageComponent,
        resolve: {
            crud: BazaReferralCodesUsageResolve,
        },
    },
    {
        path: 'baza-core/referral-codes/campaign/:campaignId/usage/:referralCodeId',
        component: BazaReferralCodesUsageComponent,
        resolve: {
            crud: BazaReferralCodesUsageResolve,
        },
    },
    {
        path: 'baza-core/referral-codes/account/:accountId/usage',
        component: BazaReferralCodesUsageComponent,
        resolve: {
            crud: BazaReferralCodesUsageResolve,
        },
    },
    {
        path: 'baza-core/referral-codes/account/:accountId/usage/:referralCodeId',
        component: BazaReferralCodesUsageComponent,
        resolve: {
            crud: BazaReferralCodesUsageResolve,
        },
    },
];
