import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { AccountDto, GetAccountByIdCmsResponse } from '@scaliolabs/baza-core-shared';
import { ReferralCampaignCmsGetByIdResponse, BazaReferralCampaignDto } from '@scaliolabs/baza-core-shared';
import { BazaAccountCmsDataAccess, BazaReferralCampaignCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

export enum BazaReferralCodeContext {
    None = 'None',
    Campaign = 'Campaign',
    Account = 'Account',
}

export interface BazaReferralCodeResolveData {
    context: BazaReferralCodeContext;
    account?: AccountDto;
    campaign?: BazaReferralCampaignDto;
}

@Injectable()
export class BazaReferralCodesResolve implements Resolve<BazaReferralCodeResolveData> {
    constructor(
        private readonly router: Router,
        private readonly campaignsEndpoint: BazaReferralCampaignCmsDataAccess,
        private readonly accountsEndpoint: BazaAccountCmsDataAccess,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<BazaReferralCodeResolveData> | BazaReferralCodeResolveData {
        const campaignId: number = route.params['campaignId'] ? parseInt(route.params['campaignId'], 10) : undefined;

        const accountId: number = route.params['accountId'] ? parseInt(route.params['accountId'], 10) : undefined;

        if (campaignId) {
            return this.resolveCampaign(campaignId);
        } else if (accountId) {
            return this.resolveAccount(accountId);
        } else {
            this.router.navigate(['/baza-core/referral-codes']);

            return {
                context: BazaReferralCodeContext.None,
            };
        }
    }

    private resolveCampaign(campaignId: number): Observable<BazaReferralCodeResolveData> {
        const observables: [Observable<ReferralCampaignCmsGetByIdResponse>] = [
            this.campaignsEndpoint.getById({
                id: campaignId,
            }),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([response]) => ({
                context: BazaReferralCodeContext.Campaign,
                campaign: response.campaign,
            })),
        );
    }

    private resolveAccount(accountId: number): Observable<BazaReferralCodeResolveData> {
        const observables: [Observable<GetAccountByIdCmsResponse>] = [
            this.accountsEndpoint.getAccountById({
                id: accountId,
            }),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([response]) => ({
                context: BazaReferralCodeContext.Account,
                account: response,
            })),
        );
    }
}
