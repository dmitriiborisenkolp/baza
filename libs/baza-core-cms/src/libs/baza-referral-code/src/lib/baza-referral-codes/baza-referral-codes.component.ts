import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudDataColumn,
    BazaCrudItem,
    BazaCrudMessage,
    BazaCrudMessageLevel,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '../../../../baza-crud/src';
import { BazaReferralCodeDto, BazaReferralCodesAcl } from '@scaliolabs/baza-core-shared';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutService,
} from '../../../../baza-form-builder/src';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { map, tap } from 'rxjs/operators';
import { BazaReferralCodeContext, BazaReferralCodeResolveData } from './baza-referral-codes.resolve';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { referralCodeIsValid } from './validators/baza-referral-code-is-valid.validator';
import {
    BazaReferralCampaignCmsDataAccess,
    BazaReferralCodeCmsDataAccess,
    BazaReferralCodeUsageCmsDataAccess,
} from '@scaliolabs/baza-core-cms-data-access';

export const BAZA_CORE_REFERRAL_CODES_CMS_ID = 'baza-core-referral-codes-cms-id';
export const BAZA_CORE_REFERRAL_CODES_FORM_ID = 'baza-core-referral-codes-form-id';

interface State {
    config: BazaCrudConfig<BazaReferralCodeDto>;
}

interface FormValue {
    codeDisplay: string;
    isActive: boolean;
    isDefault: boolean;
}

@Component({
    templateUrl: './baza-referral-codes.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaReferralCodesComponent {
    public crud: BazaCrudComponent<BazaReferralCodeDto>;

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly translate: TranslateService,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly dataAccessCampaigns: BazaReferralCampaignCmsDataAccess,
        private readonly dataAccessReferralCodes: BazaReferralCodeCmsDataAccess,
        private readonly dataAccessReferralUsage: BazaReferralCodeUsageCmsDataAccess,
    ) {}

    i18n(key: string): string {
        return `baza.referralCodes.components.referralCodes.${key}`;
    }

    get resolvedData(): BazaReferralCodeResolveData {
        return this.activatedRoute.snapshot.data['crud'];
    }

    private crudConfig(): BazaCrudConfig<BazaReferralCodeDto> {
        switch (this.resolvedData.context) {
            default: {
                throw new Error('Unknown context');
            }

            case BazaReferralCodeContext.Campaign: {
                return this.crudConfigCampaigns();
            }

            case BazaReferralCodeContext.Account: {
                return this.crudConfigAccount();
            }
        }
    }

    private crudConfigCampaigns(): BazaCrudConfig<BazaReferralCodeDto> {
        const messages: Array<BazaCrudMessage> = this.resolvedData.campaign.isActive
            ? [
                  {
                      level: BazaCrudMessageLevel.Success,
                      withIcon: true,
                      message: {
                          text: this.i18n('campaigns.messages.isActive.title'),
                          translate: true,
                      },
                      description: {
                          text: this.i18n('campaigns.messages.isActive.description'),
                          translate: true,
                      },
                  },
              ]
            : [
                  {
                      level: BazaCrudMessageLevel.Warning,
                      withIcon: true,
                      message: {
                          text: this.i18n('campaigns.messages.isNotActive.title'),
                          translate: true,
                      },
                      description: {
                          text: this.i18n('campaigns.messages.isNotActive.description'),
                          translate: true,
                      },
                  },
              ];

        return {
            id: BAZA_CORE_REFERRAL_CODES_CMS_ID,
            title: this.i18n('campaigns.title'),
            titleArguments: {
                campaign: this.resolvedData.campaign,
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('navigation.referral'),
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes'],
                        },
                    },
                    {
                        title: this.resolvedData.campaign.name,
                        withoutTranslation: true,
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes/campaign', this.resolvedData.campaign.id],
                        },
                    },
                ],
            },
            messages,
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('campaigns.usage'),
                            type: BazaNzButtonStyle.Link,
                            icon: 'stock',
                            callback: () => {
                                this.router.navigate(['/baza-core/referral-codes/campaign', this.resolvedData.campaign.id, 'usage']);
                            },
                        },
                    },
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('exportToCsv'),
                            type: BazaNzButtonStyle.Link,
                            icon: 'download',
                            fileNameGenerator: (id) => `referral-campaign-${id}-${moment(new Date()).format('mm-dd-yyyy')}`,
                            endpoint: (request) =>
                                this.dataAccessReferralUsage.exportListByReferralCampaignToCSV({
                                    ...request,
                                    listRequest: {
                                        campaignId: this.resolvedData.campaign.id,
                                    },
                                    fields: [
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.referralCode')),
                                            field: 'referralCode',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.joinedAccount')),
                                            field: 'joinedAccount',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.joinedWithClient')),
                                            field: 'joinedWithClient',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.dateJoinedAt')),
                                            field: 'dateJoinedAt',
                                        },
                                    ],
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () => {
                                const form = this.formGroup();

                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                return this.formLayout.open<BazaReferralCodeDto, FormValue>({
                                    formGroup: form,
                                    formBuilder: this.formBuilder(form),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (entity, formValue) =>
                                        this.dataAccessReferralCodes
                                            .create({
                                                codeDisplay: formValue.codeDisplay,
                                                isActive: formValue.isActive,
                                                isDefault: formValue.isDefault,
                                                associatedWithCampaignId: this.resolvedData.campaign.id,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                });
                            },
                        },
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccessReferralCodes.list({
                            ...request,
                            campaignId: this.resolvedData.campaign.id,
                        }),
                },
                actions: [
                    {
                        title: this.i18n('actions.edit'),
                        icon: 'edit',
                        action: (entity) => {
                            const form = this.formGroup();

                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            return this.formLayout.open<BazaReferralCodeDto, FormValue>({
                                formGroup: form,
                                formBuilder: this.formBuilder(form),
                                title: this.i18n('actions.edit'),
                                fetch: () =>
                                    this.dataAccessReferralCodes
                                        .getById({
                                            id: entity.id,
                                        })
                                        .pipe(map((response) => response.referralCode)),
                                populate: (entity, formGroup) =>
                                    formGroup.patchValue({
                                        codeDisplay: entity.codeDisplay,
                                        isActive: entity.isActive,
                                    } as FormValue),
                                onSubmit: (entity, formValue) =>
                                    this.dataAccessReferralCodes
                                        .edit({
                                            id: entity.id,
                                            codeDisplay: formValue.codeDisplay,
                                            isActive: formValue.isActive,
                                            isDefault: formValue.isDefault,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            });
                        },
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                confirm: () =>
                                    this.dataAccessReferralCodes
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            }),
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: false,
                    },
                    {
                        field: 'codeDisplay',
                        title: this.i18n('columns.codeDisplay'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'usage',
                        title: '',
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (entity) => ({
                                routerLink: ['/baza-core/referral-codes/campaign', this.resolvedData.campaign.id, 'usage', entity.id],
                            }),
                            format: () => this.i18n('columns.usage'),
                            translate: true,
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'usedCount',
                        title: this.i18n('columns.usedCount'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 160,
                    },
                    {
                        field: 'isActive',
                        title: this.i18n('columns.isActive'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: true,
                        },
                        width: 160,
                        sortable: false,
                    },
                ],
            },
        };
    }

    private crudConfigAccount(): BazaCrudConfig<BazaReferralCodeDto> {
        const messages: Array<BazaCrudMessage> = [];

        if (this.resolvedData.account.isDeactivated) {
            messages.push({
                level: BazaCrudMessageLevel.Error,
                withIcon: true,
                message: {
                    text: this.i18n('accounts.messages.isDeactivated.title'),
                    translate: true,
                },
                description: {
                    text: this.i18n('accounts.messages.isDeactivated.description'),
                    translate: true,
                },
            });
        } else if (!this.resolvedData.account.isEmailConfirmed) {
            messages.push({
                level: BazaCrudMessageLevel.Warning,
                withIcon: true,
                message: {
                    text: this.i18n('accounts.messages.isNotActive.title'),
                    translate: true,
                },
                description: {
                    text: this.i18n('accounts.messages.isNotActive.description'),
                    translate: true,
                },
            });
        } else {
            messages.push({
                level: BazaCrudMessageLevel.Success,
                withIcon: true,
                message: {
                    text: this.i18n('accounts.messages.isActive.title'),
                    translate: true,
                },
                description: {
                    text: this.i18n('accounts.messages.isActive.description'),
                    translate: true,
                },
            });
        }

        const columns: Array<BazaCrudDataColumn<BazaReferralCodeDto>> = [
            {
                field: 'id',
                title: this.i18n('columns.id'),
                type: {
                    kind: BazaTableFieldType.Id,
                },
                sortable: false,
            },
            {
                field: 'codeDisplay',
                title: this.i18n('columns.codeDisplay'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                sortable: false,
                ngStyle: () => ({
                    'font-weight': '500',
                }),
            },
            {
                field: 'usage',
                title: '',
                type: {
                    kind: BazaTableFieldType.Link,
                    link: (entity) => ({
                        routerLink: ['/baza-core/referral-codes/account', this.resolvedData.account.id, 'usage', entity.id],
                    }),
                    format: () => this.i18n('columns.usage'),
                    translate: true,
                },
                width: 160,
                textAlign: BazaTableFieldAlign.Center,
            },
            {
                field: 'usedCount',
                title: this.i18n('columns.usedCount'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                sortable: false,
                textAlign: BazaTableFieldAlign.Center,
                width: 160,
            },
            {
                field: 'isActive',
                title: this.i18n('columns.isActive'),
                type: {
                    kind: BazaTableFieldType.Boolean,
                    withFalseIcon: true,
                },
                width: 160,
                sortable: false,
            },
        ];

        if (this.resolvedData.context === BazaReferralCodeContext.Account) {
            columns.push({
                field: 'isDefault',
                title: this.i18n('columns.isDefault'),
                type: {
                    kind: BazaTableFieldType.Boolean,
                    withFalseIcon: true,
                },
                width: 160,
                sortable: false,
            });
        }

        return {
            id: BAZA_CORE_REFERRAL_CODES_CMS_ID,
            title: this.i18n('accounts.title'),
            titleArguments: {
                account: this.resolvedData.account,
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('navigation.referral'),
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes'],
                        },
                    },
                    {
                        title: this.resolvedData.account.fullName,
                        withoutTranslation: true,
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes/account', this.resolvedData.account.id],
                        },
                    },
                ],
            },
            messages,
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('accounts.usage'),
                            type: BazaNzButtonStyle.Link,
                            icon: 'stock',
                            callback: () => {
                                this.router.navigate(['/baza-core/referral-codes/account', this.resolvedData.account.id, 'usage']);
                            },
                        },
                    },
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('exportToCsv'),
                            type: BazaNzButtonStyle.Link,
                            icon: 'download',
                            fileNameGenerator: (id) => `referral-account-${id}-${moment(new Date()).format('mm-dd-yyyy')}`,
                            endpoint: (request) =>
                                this.dataAccessReferralUsage.exportListByReferralAccountToCSV({
                                    ...request,
                                    listRequest: {
                                        accountId: this.resolvedData.account.id,
                                    },
                                    fields: [
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.referralCode')),
                                            field: 'referralCode',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.joinedAccount')),
                                            field: 'joinedAccount',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.joinedWithClient')),
                                            field: 'joinedWithClient',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.dateJoinedAt')),
                                            field: 'dateJoinedAt',
                                        },
                                    ],
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () => {
                                const form = this.formGroup();

                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                return this.formLayout.open<BazaReferralCodeDto, FormValue>({
                                    formGroup: form,
                                    formBuilder: this.formBuilder(form),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (entity, formValue) =>
                                        this.dataAccessReferralCodes
                                            .create({
                                                codeDisplay: formValue.codeDisplay,
                                                isActive: formValue.isActive,
                                                isDefault: formValue.isDefault,
                                                associatedWithAccountId: this.resolvedData.account.id,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                });
                            },
                        },
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                ],
            },
            data: {
                columns,
                dataSource: {
                    list: (request) =>
                        this.dataAccessReferralCodes.list({
                            ...request,
                            accountId: this.resolvedData.account.id,
                        }),
                },
                actions: [
                    {
                        title: this.i18n('actions.edit'),
                        icon: 'edit',
                        visible: () => !this.resolvedData.account.isDeactivated,
                        action: (entity) => {
                            const form = this.formGroup();

                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            return this.formLayout.open<BazaReferralCodeDto, FormValue>({
                                formGroup: form,
                                formBuilder: this.formBuilder(form),
                                title: this.i18n('actions.edit'),
                                fetch: () =>
                                    this.dataAccessReferralCodes
                                        .getById({
                                            id: entity.id,
                                        })
                                        .pipe(map((response) => response.referralCode)),
                                populate: (entity, formGroup) =>
                                    formGroup.patchValue({
                                        codeDisplay: entity.codeDisplay,
                                        isActive: entity.isActive,
                                    } as FormValue),
                                onSubmit: (entity, formValue) =>
                                    this.dataAccessReferralCodes
                                        .edit({
                                            id: entity.id,
                                            codeDisplay: formValue.codeDisplay,
                                            isActive: formValue.isActive,
                                            isDefault: formValue.isDefault,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            });
                        },
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        visible: () => !this.resolvedData.account.isDeactivated,
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                confirm: () =>
                                    this.dataAccessReferralCodes
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            }),
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            codeDisplay: [undefined, [Validators.required, referralCodeIsValid()]],
            isActive: [true, [Validators.required]],
            isDefault: [false, [Validators.required]],
        });
    }

    private formBuilder(form: FormGroup): BazaFormBuilder {
        return {
            id: BAZA_CORE_REFERRAL_CODES_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'codeDisplay',
                        label: this.i18n('forms.fields.codeDisplay'),
                        placeholder: this.i18n('forms.placeholders.codeDisplay'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Checkbox,
                        formControlName: 'isActive',
                        label: this.i18n('forms.fields.isActive'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Checkbox,
                        formControlName: 'isDefault',
                        label: this.i18n('forms.fields.isDefault'),
                        required: false,
                        visible:
                            this.resolvedData.context === BazaReferralCodeContext.Account
                                ? form.valueChanges.pipe(map((fv: FormValue) => fv.isActive))
                                : false,
                    },
                ],
            },
        };
    }
}
