import { AbstractControl, ValidatorFn } from '@angular/forms';
import { bazaIsReferralCodeValid } from '@scaliolabs/baza-core-shared';

export const BAZA_REFERRAL_CODE_VALIDATOR_MESSAGE_DEFAULT = 'baza.referralCodes.validators.bazaReferralCodeValidator';

export function referralCodeIsValid(errorMessage = BAZA_REFERRAL_CODE_VALIDATOR_MESSAGE_DEFAULT): ValidatorFn {
    return (control: AbstractControl) => {
        if (
            !control.disabled &&
            !!control.value &&
            (control.value || '').toString().length > 0 &&
            !bazaIsReferralCodeValid(control.value)
        ) {
            return {
                [errorMessage]: true,
            };
        } else {
            return null;
        }
    };
}
