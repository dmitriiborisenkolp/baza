import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudItem, BazaTableFieldAlign, BazaTableFieldType } from '../../../../baza-crud/src';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutService,
} from '../../../../baza-form-builder/src';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { map, tap } from 'rxjs/operators';
import {
    BazaReferralAccountCsvDto,
    BazaReferralAccountDto,
    BazaReferralCampaignDto,
    BazaReferralCodesAcl,
} from '@scaliolabs/baza-core-shared';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { BazaReferralAccountCmsDataAccess, BazaReferralCampaignCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';

export const BAZA_CORE_REFERRAL_CMS_ID = 'baza-core-referral-cms-id';
export const BAZA_CORE_REFERRAL_FORM_ID = 'baza-core-referral-form-id';

enum Context {
    Accounts = 'Accounts',
    Campaigns = 'Campaigns',
}

interface State {
    context: Context;
    config: BazaCrudConfig<BazaReferralCampaignDto> | BazaCrudConfig<BazaReferralAccountDto>;
}

interface FormValue {
    name: string;
    isActive: boolean;
}

@Component({
    templateUrl: './baza-referral.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaReferralComponent {
    public crud: BazaCrudComponent<BazaReferralCampaignDto>;

    public state: State = {
        context: Context.Campaigns,
        config: this.crudConfig(Context.Campaigns),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly cdr: ChangeDetectorRef,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly dataAccessCampaigns: BazaReferralCampaignCmsDataAccess,
        private readonly dataAccessReferralAccounts: BazaReferralAccountCmsDataAccess,
        private readonly translate: TranslateService,
    ) {}

    i18n(key: string): string {
        return `baza.referralCodes.components.referral.${key}`;
    }

    private crudConfig(context: Context): BazaCrudConfig<BazaReferralCampaignDto> | BazaCrudConfig<BazaReferralAccountDto> {
        switch (context) {
            default: {
                throw new Error('Unknown context');
            }

            case Context.Campaigns: {
                return this.crudConfigCampaigns();
            }

            case Context.Accounts: {
                return this.crudConfigAccounts();
            }
        }
    }

    private crudConfigAccounts(): BazaCrudConfig<BazaReferralAccountDto> {
        return {
            id: BAZA_CORE_REFERRAL_CMS_ID,
            title: this.i18n('title'),
            items: {
                topLeft: [
                    {
                        type: BazaCrudItem.Tabs,
                        options: {
                            tabs: [
                                {
                                    id: 'campaigns',
                                    title: this.i18n('tabs.campaigns'),
                                    isActive$: of(false),
                                    onClick: () => {
                                        this.state = {
                                            ...this.state,
                                            context: Context.Campaigns,
                                            config: this.crudConfig(Context.Campaigns),
                                        };

                                        this.crud.data.current$.next([]);

                                        this.cdr.markForCheck();
                                    },
                                },
                                {
                                    id: 'accounts',
                                    title: this.i18n('tabs.accounts'),
                                    isActive$: of(true),
                                    // eslint-disable-next-line @typescript-eslint/no-empty-function
                                    onClick: () => {},
                                },
                            ],
                        },
                    },
                ],
                topRight: [
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('accounts.exportToCsv'),
                            type: BazaNzButtonStyle.Link,
                            icon: 'download',
                            fileNameGenerator: (id) => `referral-accounts-${id}-${moment(new Date()).format('mm-dd-yyyy')}`,
                            endpoint: (request) =>
                                this.dataAccessReferralAccounts.exportListToCSV({
                                    ...request,
                                    listRequest: {},
                                    fields: [
                                        'id',
                                        'email',
                                        'dateCreated',
                                        'lastSignIn',
                                        'fullName',
                                        'firstName',
                                        'lastName',
                                        'isEmailConfirmed',
                                        'signedUpWithClient',
                                        'signedUpWithReferralCode',
                                        'referralCode',
                                    ].map((field) => ({
                                        field: field as keyof BazaReferralAccountCsvDto,
                                        title: this.translate.instant(this.i18n(`accounts.columns.${field}`)),
                                    })),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccessReferralAccounts.list(request),
                },
                actions: [],
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('accounts.columns.id'),
                        width: 'id',
                        sortable: true,
                    },
                    {
                        field: 'email',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('accounts.columns.email'),
                        width: 250,
                        sortable: true,
                    },
                    {
                        field: 'fullName',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('accounts.columns.fullName'),
                        sortable: true,
                        width: 'auto',
                    },
                    {
                        field: 'dateCreated',
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        title: this.i18n('accounts.columns.dateCreated'),
                        sortable: true,
                    },
                    {
                        field: 'signedUpWithClient',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        source: (entity) => entity.signedUpWithClient,
                        title: this.i18n('accounts.columns.signedUpWithClient'),
                        textAlign: BazaTableFieldAlign.Center,
                        sortable: false,
                        width: 160,
                    },
                    {
                        field: 'signedUpWithReferralCode',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        source: (entity) => entity.signedUpWithReferralCode,
                        title: this.i18n('accounts.columns.signedUpWithReferralCode'),
                        textAlign: BazaTableFieldAlign.Center,
                        sortable: false,
                        width: 160,
                    },
                    {
                        field: 'referralCode',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        source: (entity) => (entity.referralCode ? entity.referralCode.codeDisplay : ''),
                        title: this.i18n('accounts.columns.referralCode'),
                        textAlign: BazaTableFieldAlign.Center,
                        sortable: false,
                        width: 160,
                    },
                    {
                        field: 'referralCodes',
                        title: '',
                        withoutTranslate: true,
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (entity) => ({
                                routerLink: ['/baza-core/referral-codes/account', entity.id],
                            }),
                            format: () => this.i18n('accounts.columns.referralCodes'),
                            translate: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 160,
                        sortable: false,
                    },
                ],
            },
        };
    }

    private crudConfigCampaigns(): BazaCrudConfig<BazaReferralCampaignDto> {
        return {
            id: BAZA_CORE_REFERRAL_CMS_ID,
            title: this.i18n('title'),
            items: {
                topLeft: [
                    {
                        type: BazaCrudItem.Tabs,
                        options: {
                            tabs: [
                                {
                                    id: 'campaigns',
                                    title: this.i18n('tabs.campaigns'),
                                    isActive$: of(true),
                                    // eslint-disable-next-line @typescript-eslint/no-empty-function
                                    onClick: () => {},
                                },
                                {
                                    title: this.i18n('tabs.accounts'),
                                    isActive$: of(false),
                                    onClick: () => {
                                        this.state = {
                                            ...this.state,
                                            context: Context.Accounts,
                                            config: this.crudConfig(Context.Accounts),
                                        };

                                        this.crud.data.current$.next([]);

                                        this.cdr.markForCheck();
                                    },
                                },
                            ],
                        },
                    },
                ],
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('campaigns.actions.add'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<BazaReferralCampaignDto, FormValue>({
                                    formGroup: this.formGroupCampaign(),
                                    formBuilder: this.formBuilderCampaign(),
                                    title: this.i18n('campaigns.actions.add'),
                                    onSubmit: (entity, formValue) =>
                                        this.dataAccessCampaigns
                                            .create({
                                                name: formValue.name,
                                                isActive: formValue.isActive,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccessCampaigns.list(request),
                },
                actions: [
                    {
                        title: this.i18n('campaigns.actions.referralCodes'),
                        icon: 'unordered-list',
                        action: (entity) => {
                            this.router.navigate(['/baza-core/referral-codes/campaign', entity.id]);
                        },
                    },
                    {
                        title: this.i18n('campaigns.actions.edit'),
                        icon: 'edit',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<BazaReferralCampaignDto, FormValue>({
                                formGroup: this.formGroupCampaign(),
                                formBuilder: this.formBuilderCampaign(),
                                title: this.i18n('campaigns.actions.edit'),
                                fetch: () =>
                                    this.dataAccessCampaigns
                                        .getById({
                                            id: entity.id,
                                        })
                                        .pipe(map((response) => response.campaign)),
                                populate: (entity, formGroup) =>
                                    formGroup.patchValue({
                                        name: entity.name,
                                        isActive: entity.isActive,
                                    } as FormValue),
                                onSubmit: (entity, formValue) =>
                                    this.dataAccessCampaigns
                                        .edit({
                                            id: entity.id,
                                            name: formValue.name,
                                            isActive: formValue.isActive,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            }),
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                    {
                        title: this.i18n('campaigns.actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('campaigns.actions.delete.confirm'),
                                confirm: () =>
                                    this.dataAccessCampaigns
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            }),
                        acl: [BazaReferralCodesAcl.BazaReferralCodesManagement],
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('campaigns.columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: false,
                    },
                    {
                        field: 'name',
                        title: this.i18n('campaigns.columns.name'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'referralCodes',
                        title: '',
                        withoutTranslate: true,
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (entity) => ({
                                routerLink: ['/baza-core/referral-codes/campaign', entity.id],
                            }),
                            format: () => this.i18n('campaigns.columns.referralCodes'),
                            translate: true,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 160,
                        sortable: false,
                    },
                    {
                        field: 'usedCount',
                        title: this.i18n('campaigns.columns.usedCount'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 160,
                    },
                    {
                        field: 'isActive',
                        title: this.i18n('campaigns.columns.isActive'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: true,
                        },
                        width: 160,
                        sortable: false,
                    },
                ],
            },
        };
    }

    private formGroupCampaign(): FormGroup {
        return this.fb.group({
            name: [undefined, [Validators.required]],
            isActive: [false, [Validators.required]],
        });
    }

    private formBuilderCampaign(): BazaFormBuilder {
        return {
            id: BAZA_CORE_REFERRAL_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'name',
                        label: this.i18n('campaigns.forms.fields.name'),
                        placeholder: this.i18n('campaigns.forms.placeholders.name'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Checkbox,
                        formControlName: 'isActive',
                        label: this.i18n('campaigns.forms.fields.isActive'),
                        required: true,
                    },
                ],
            },
        };
    }
}
