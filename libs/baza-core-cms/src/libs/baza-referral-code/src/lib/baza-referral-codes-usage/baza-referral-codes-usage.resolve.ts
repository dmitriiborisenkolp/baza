import { Injectable } from '@angular/core';
import {
    ReferralCampaignCmsGetByIdResponse,
    BazaReferralCampaignDto,
    BazaReferralCodeCmsGetByIdResponse,
    BazaReferralCodeCmsListResponse,
    BazaReferralCodeDto,
} from '@scaliolabs/baza-core-shared';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { combineLatest, Observable, of } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { AccountDto, GetAccountByIdCmsResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaAccountCmsDataAccess,
    BazaReferralCampaignCmsDataAccess,
    BazaReferralCodeCmsDataAccess,
} from '@scaliolabs/baza-core-cms-data-access';

export enum BazaReferralCodesUsageContext {
    None = 'None',
    Account = 'Account',
    Campaign = 'campaign',
}

export interface BazaReferralCodesUsageResolveData {
    context: BazaReferralCodesUsageContext;
    referralCode?: BazaReferralCodeDto;
    account?: AccountDto;
    campaign?: BazaReferralCampaignDto;
    referralCodesInCampaign?: Array<BazaReferralCodeDto>;
    referralCodesInAccount?: Array<BazaReferralCodeDto>;
}

@Injectable()
export class BazaReferralCodesUsageResolve implements Resolve<BazaReferralCodesUsageResolveData> {
    constructor(
        private readonly router: Router,
        private readonly accountsEndpoint: BazaAccountCmsDataAccess,
        private readonly referralCodesEndpoint: BazaReferralCodeCmsDataAccess,
        private readonly campaignsEndpoint: BazaReferralCampaignCmsDataAccess,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<BazaReferralCodesUsageResolveData> | BazaReferralCodesUsageResolveData {
        const campaignId: number = route.params['campaignId'] ? parseInt(route.params['campaignId'], 10) : undefined;

        const accountId: number = route.params['accountId'] ? parseInt(route.params['accountId'], 10) : undefined;

        const referralCodeId: number = route.params['referralCodeId'] ? parseInt(route.params['referralCodeId'], 10) : undefined;

        if (campaignId) {
            return this.resolveCampaign(campaignId, referralCodeId);
        } else if (accountId) {
            return this.resolveAccount(accountId, referralCodeId);
        } else {
            this.router.navigate(['/baza-core/referral-codes']);

            return {
                context: BazaReferralCodesUsageContext.None,
            };
        }
    }

    private resolveAccount(accountId: number, referralCodeId?: number): Observable<BazaReferralCodesUsageResolveData> {
        const observables: [
            Observable<GetAccountByIdCmsResponse>,
            Observable<BazaReferralCodeCmsListResponse>,
            Observable<BazaReferralCodeCmsGetByIdResponse>,
        ] = [
            this.accountsEndpoint.getAccountById({
                id: accountId,
            }),
            this.referralCodesEndpoint.list({
                index: 1,
                size: 999,
                accountId: accountId,
            }),
            referralCodeId
                ? this.referralCodesEndpoint.getById({
                      id: referralCodeId,
                  })
                : of(undefined),
        ];

        if (referralCodeId) {
            observables.push(
                this.referralCodesEndpoint.getById({
                    id: referralCodeId,
                }),
            );
        }

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([account, referralAccountCodes, referralCode]) => ({
                context: BazaReferralCodesUsageContext.Account,
                account,
                referralCode: referralCode ? referralCode.referralCode : undefined,
                referralCodesInAccount: referralAccountCodes.items,
            })),
        );
    }

    private resolveCampaign(campaignId: number, referralCodeId?: number): Observable<BazaReferralCodesUsageResolveData> {
        const observables: [Observable<ReferralCampaignCmsGetByIdResponse>, Observable<BazaReferralCodeCmsGetByIdResponse>] = [
            this.campaignsEndpoint.getById({
                id: campaignId,
                withFullListOfReferralCodes: true,
            }),
            referralCodeId
                ? this.referralCodesEndpoint.getById({
                      id: referralCodeId,
                  })
                : of(undefined),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([response, referralCode]) => ({
                context: BazaReferralCodesUsageContext.Campaign,
                campaign: response.campaign,
                referralCode: referralCode ? referralCode.referralCode : undefined,
                referralCodesInCampaign: response.referralCodes || [],
            })),
        );
    }
}
