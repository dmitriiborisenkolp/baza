import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaCrudItem, BazaTableFieldAlign, BazaTableFieldType } from '../../../../baza-crud/src';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BazaFormLayoutService } from '../../../../baza-form-builder/src';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import * as moment from 'moment';
import { BazaReferralCodesUsageContext, BazaReferralCodesUsageResolveData } from './baza-referral-codes-usage.resolve';
import { BazaReferralCodeUsageDto } from '@scaliolabs/baza-core-shared';
import { BehaviorSubject, Subject } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';
import { BazaReferralCodeUsageCmsDataAccess } from '@scaliolabs/baza-core-cms-data-access';

export const BAZA_CORE_REFERRAL_CODE_USAGES_CMS_ID = 'baza-core-referral-code-usages-cms-id';

interface State {
    config: BazaCrudConfig<BazaReferralCodeUsageDto>;
}

@Component({
    templateUrl: './baza-referral-codes-usage.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaReferralCodesUsageComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    private filterReferralCodeIds$: BehaviorSubject<Array<number>> = new BehaviorSubject<Array<number>>([]);

    public crud: BazaCrudComponent<BazaReferralCodeUsageDto>;

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly translate: TranslateService,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly dataAccessReferralUsage: BazaReferralCodeUsageCmsDataAccess,
    ) {}

    ngOnInit(): void {
        if (this.resolvedData.referralCode) {
            this.filterReferralCodeIds$.next([this.resolvedData.referralCode.id]);
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza.referralCodes.components.referralCodesUsage.${key}`;
    }

    get resolvedData(): BazaReferralCodesUsageResolveData {
        return this.activatedRoute.snapshot.data['crud'];
    }

    private crudConfig(): BazaCrudConfig<BazaReferralCodeUsageDto> {
        switch (this.resolvedData.context) {
            default: {
                throw new Error('Unknown context');
            }

            case BazaReferralCodesUsageContext.Campaign: {
                return this.crudConfigCampaigns();
            }

            case BazaReferralCodesUsageContext.Account: {
                return this.crudConfigAccounts();
            }
        }
    }

    private crudConfigCampaigns(): BazaCrudConfig<BazaReferralCodeUsageDto> {
        return {
            id: BAZA_CORE_REFERRAL_CODE_USAGES_CMS_ID,
            title: this.i18n('campaigns.title'),
            titleArguments: {
                campaign: this.resolvedData.campaign,
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('navigation.referral'),
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes'],
                        },
                    },
                    {
                        title: this.resolvedData.campaign.name,
                        withoutTranslation: true,
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes/campaign', this.resolvedData.campaign.id],
                        },
                    },
                    {
                        title: this.i18n('navigation.usage'),
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes', this.resolvedData.campaign.id, 'usage'],
                        },
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('exportToCsv'),
                            type: BazaNzButtonStyle.Link,
                            icon: 'download',
                            fileNameGenerator: (id) => `referral-campaign-${id}-${moment(new Date()).format('mm-dd-yyyy')}`,
                            endpoint: (request) =>
                                this.dataAccessReferralUsage.exportListByReferralCampaignToCSV({
                                    ...request,
                                    listRequest: {
                                        campaignId: this.resolvedData.campaign.id,
                                        filterByReferralCodeIds:
                                            this.filterReferralCodeIds$.getValue().length > 0
                                                ? this.filterReferralCodeIds$.getValue()
                                                : undefined,
                                    },
                                    fields: [
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.referralCode')),
                                            field: 'referralCode',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.joinedAccount')),
                                            field: 'joinedAccount',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.joinedWithClient')),
                                            field: 'joinedWithClient',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.dateJoinedAt')),
                                            field: 'dateJoinedAt',
                                        },
                                    ],
                                }),
                        },
                    },
                ],
            },
            labels: this.resolvedData.referralCodesInCampaign.map((referralCode) => ({
                id: referralCode.code,
                title: referralCode.codeDisplay,
                isActive$: this.filterReferralCodeIds$.pipe(
                    distinctUntilChanged(),
                    takeUntil(this.ngOnDestroy$),
                    map((activated) => activated.includes(referralCode.id)),
                ),
                callback: () => {
                    const current = this.filterReferralCodeIds$.getValue();

                    if (current.includes(referralCode.id)) {
                        this.filterReferralCodeIds$.next(current.filter((id) => id !== referralCode.id));
                    } else {
                        this.filterReferralCodeIds$.next([...current, referralCode.id]);
                    }

                    this.crud.refresh();
                },
            })),
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccessReferralUsage.listByReferralCampaign({
                            ...request,
                            campaignId: this.resolvedData.campaign.id,
                            filterByReferralCodeIds:
                                this.filterReferralCodeIds$.getValue().length > 0 ? this.filterReferralCodeIds$.getValue() : undefined,
                        }),
                },
                columns: [
                    {
                        field: 'referralCode',
                        title: this.i18n('columns.referralCode'),
                        source: (entity) => entity.referralCode.codeDisplay,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 250,
                        sortable: false,
                    },
                    {
                        field: 'joinedAccount',
                        title: this.i18n('columns.joinedAccount'),
                        source: (entity) => `${entity.joinedAccount.email} (${entity.joinedAccount.fullName})`,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'joinedWithClient',
                        title: this.i18n('columns.joinedWithClient'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 160,
                    },
                    {
                        field: 'dateJoinedAt',
                        title: this.i18n('columns.dateJoinedAt'),
                        source: (entity) => moment(new Date(entity.dateJoinedAt)).format('LLL'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 200,
                        sortable: false,
                    },
                ],
            },
        };
    }

    private crudConfigAccounts(): BazaCrudConfig<BazaReferralCodeUsageDto> {
        return {
            id: BAZA_CORE_REFERRAL_CODE_USAGES_CMS_ID,
            title: this.i18n('accounts.title'),
            titleArguments: {
                account: this.resolvedData.account,
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('navigation.referral'),
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes'],
                        },
                    },
                    {
                        title: this.resolvedData.account.fullName,
                        withoutTranslation: true,
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes/account', this.resolvedData.account.id],
                        },
                    },
                    {
                        title: this.i18n('navigation.usage'),
                        routerLink: {
                            routerLink: ['/baza-core/referral-codes', this.resolvedData.account.id, 'usage'],
                        },
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('exportToCsv'),
                            type: BazaNzButtonStyle.Link,
                            icon: 'download',
                            fileNameGenerator: (id) => `referral-account-${id}-${moment(new Date()).format('mm-dd-yyyy')}`,
                            endpoint: (request) =>
                                this.dataAccessReferralUsage.exportListByReferralAccountToCSV({
                                    ...request,
                                    listRequest: {
                                        accountId: this.resolvedData.account.id,
                                        filterByReferralCodeIds:
                                            this.filterReferralCodeIds$.getValue().length > 0
                                                ? this.filterReferralCodeIds$.getValue()
                                                : undefined,
                                    },
                                    fields: [
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.referralCode')),
                                            field: 'referralCode',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.joinedAccount')),
                                            field: 'joinedAccount',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.joinedWithClient')),
                                            field: 'joinedWithClient',
                                        },
                                        {
                                            title: this.translate.instant(this.i18n('usage.csv.dateJoinedAt')),
                                            field: 'dateJoinedAt',
                                        },
                                    ],
                                }),
                        },
                    },
                ],
            },
            labels: this.resolvedData.referralCodesInAccount.map((referralCode) => ({
                id: referralCode.code,
                title: referralCode.codeDisplay,
                isActive$: this.filterReferralCodeIds$.pipe(
                    distinctUntilChanged(),
                    takeUntil(this.ngOnDestroy$),
                    map((activated) => activated.includes(referralCode.id)),
                ),
                callback: () => {
                    const current = this.filterReferralCodeIds$.getValue();

                    if (current.includes(referralCode.id)) {
                        this.filterReferralCodeIds$.next(current.filter((id) => id !== referralCode.id));
                    } else {
                        this.filterReferralCodeIds$.next([...current, referralCode.id]);
                    }

                    this.crud.refresh();
                },
            })),
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccessReferralUsage.listByReferralAccount({
                            ...request,
                            accountId: this.resolvedData.account.id,
                            filterByReferralCodeIds:
                                this.filterReferralCodeIds$.getValue().length > 0 ? this.filterReferralCodeIds$.getValue() : undefined,
                        }),
                },
                columns: [
                    {
                        field: 'referralCode',
                        title: this.i18n('columns.referralCode'),
                        source: (entity) => entity.referralCode.codeDisplay,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 250,
                        sortable: false,
                    },
                    {
                        field: 'joinedAccount',
                        title: this.i18n('columns.joinedAccount'),
                        source: (entity) => `${entity.joinedAccount.email} (${entity.joinedAccount.fullName})`,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'joinedWithClient',
                        title: this.i18n('columns.joinedWithClient'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        textAlign: BazaTableFieldAlign.Center,
                        width: 160,
                    },
                    {
                        field: 'dateJoinedAt',
                        title: this.i18n('columns.dateJoinedAt'),
                        source: (entity) => moment(new Date(entity.dateJoinedAt)).format('LLL'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 200,
                        sortable: false,
                    },
                ],
            },
        };
    }
}
