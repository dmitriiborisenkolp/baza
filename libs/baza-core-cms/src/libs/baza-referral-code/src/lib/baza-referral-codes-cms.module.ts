import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCrudCmsModule } from '../../../baza-crud/src';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaFormBuilderModule } from '../../../baza-form-builder/src';
import { BazaCmsLayoutModule } from '../../../../cms/layout/src';
import { BazaReferralCodesCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaReferralCodesComponent } from './baza-referral-codes/baza-referral-codes.component';
import { BazaReferralCodesUsageComponent } from './baza-referral-codes-usage/baza-referral-codes-usage.component';
import { BazaReferralComponent } from './baza-referral/baza-referral.component';
import { BazaReferralCodesResolve } from './baza-referral-codes/baza-referral-codes.resolve';
import { BazaReferralCodesUsageResolve } from './baza-referral-codes-usage/baza-referral-codes-usage.resolve';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BazaFormBuilderModule,
        BazaCmsLayoutModule,
        BazaCrudCmsModule,
        BazaReferralCodesCmsDataAccessModule,
    ],
    declarations: [BazaReferralComponent, BazaReferralCodesComponent, BazaReferralCodesUsageComponent],
    providers: [BazaReferralCodesResolve, BazaReferralCodesUsageResolve],
})
export class BazaReferralCodesCmsModule {}
