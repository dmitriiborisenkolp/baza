import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BazaNgCurrentApplicationService } from '@scaliolabs/baza-core-ng';
import { ActivatedRoute } from '@angular/router';
import { BazaDashboardResolveData } from './baza-dashboard.resolve';

@Component({
    selector: 'baza-dashboard',
    templateUrl: './baza-dashboard.component.html',
    styleUrls: ['./baza-dashboard.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDashboardComponent {
    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly currentApp: BazaNgCurrentApplicationService,
    ) {}

    get resolvedData(): BazaDashboardResolveData {
        return this.activatedRoute.snapshot.data['dashboard'] as BazaDashboardResolveData;
    }

    i18n(input: string): string {
        return `baza.dashboard.components.bazaDashboard.${input}`;
    }

    get translateParams(): any {
        return {
            project: this.currentApp.project.name,
            projectCodeName: this.currentApp.project.codeName,
            version: this.resolvedData.version.current,
        };
    }
}
