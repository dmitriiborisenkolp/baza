import { Injectable } from '@angular/core';
import { VersionDto } from '@scaliolabs/baza-core-shared';
import { Resolve } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { BazaVersionDataAccess } from '@scaliolabs/baza-core-data-access';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

export interface BazaDashboardResolveData {
    version: VersionDto;
}

@Injectable()
export class BazaDashboardResolve implements Resolve<BazaDashboardResolveData> {
    constructor(
        private readonly version: BazaVersionDataAccess,
    ) {}

    resolve(): Observable<BazaDashboardResolveData> | Promise<BazaDashboardResolveData> | BazaDashboardResolveData {
        const observables = [
            this.version.version(),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([version]) => ({
                version,
            })),
        );
    }
}
