import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaDashboardComponent } from './components/baza-dashboard/baza-dashboard.component';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { TranslateModule } from '@ngx-translate/core';
import { BazaVersionDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaDashboardResolve } from './components/baza-dashboard/baza-dashboard.resolve';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        NzLayoutModule,
        NzTypographyModule,
        BazaVersionDataAccessModule,
    ],
    providers: [
        BazaDashboardResolve,
    ],
    declarations: [
        BazaDashboardComponent,
    ],
    exports: [
        BazaDashboardComponent,
    ],
})
export class BazaDashboardCmsModule {
}
