import { Routes } from '@angular/router';
import { BazaDashboardComponent } from './components/baza-dashboard/baza-dashboard.component';
import { BazaDashboardResolve } from './components/baza-dashboard/baza-dashboard.resolve';

export const bazaDashboardCmsRoutes: Routes = [
    {
        path: 'dashboard',
        component: BazaDashboardComponent,
        resolve: {
            dashboard: BazaDashboardResolve,
        },
    },
];
