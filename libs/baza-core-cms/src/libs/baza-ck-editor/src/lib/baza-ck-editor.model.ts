import { AttachmentUploadRequest } from '@scaliolabs/baza-core-shared';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular';

export enum BazaCkEditorUploadTransport {
    BazaAWS = 'BazaAWS',
    BazaAttachment = 'BazaAttachment',
}

export type BazaCkEditorUploadTransportOptions =
    { type: BazaCkEditorUploadTransport.BazaAWS } |
    { type: BazaCkEditorUploadTransport.BazaAttachment; options: (file: File, extension: string) => AttachmentUploadRequest }
;

export interface BazaCkEditorOptions {
    transport: BazaCkEditorUploadTransportOptions;
    withCkEditorOptions?: CKEditor5.Config;
}

export function defaultBazaCkEditorOptions(): BazaCkEditorOptions {
    return {
        transport: {
            type: BazaCkEditorUploadTransport.BazaAWS,
        },
    };
}
