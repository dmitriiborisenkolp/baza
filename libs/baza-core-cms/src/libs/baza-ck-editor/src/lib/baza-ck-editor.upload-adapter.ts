import { Injectable } from '@angular/core';
import { BazaAwsDataAccess } from '@scaliolabs/baza-core-data-access';
import { Subject } from 'rxjs';
import { finalize, map, takeUntil } from 'rxjs/operators';
import { BazaAttachmentDataAccess } from '@scaliolabs/baza-core-data-access';
import { AttachmentUploadRequest, BAZA_AWS_FIELD_PREFIX } from '@scaliolabs/baza-core-shared';
import { BazaCkEditorOptions, BazaCkEditorUploadTransport } from './baza-ck-editor.model';
import { BazaLoadingService } from '@scaliolabs/baza-core-ng';

@Injectable()
export class BazaCkEditorUploadAdapter {
    constructor(
        private readonly bazaAws: BazaAwsDataAccess,
        private readonly bazaAttachment: BazaAttachmentDataAccess,
        private readonly loading: BazaLoadingService,
    ) {}

    factory(options: BazaCkEditorOptions): any {
        return (loader, url) => {
            switch (options.transport.type) {
                default: {
                    throw new Error('Unknown transport');
                }

                case BazaCkEditorUploadTransport.BazaAWS: {
                    const adapter = new BazaAwsUploadAdapter(loader, url);

                    adapter.apiAws = this.bazaAws;
                    adapter.loading = this.loading;

                    return adapter;
                }

                case BazaCkEditorUploadTransport.BazaAttachment: {
                    const adapter = new BazaAttachmentUploadAdapter(loader, url);

                    adapter.apiAttachment = this.bazaAttachment;
                    adapter.apiAttachmentRequest = options.transport.options;
                    adapter.loading = this.loading;

                    return adapter;
                }
            }
        };
    }
}

class BazaAwsUploadAdapter {
    loader;
    url;

    loading: BazaLoadingService;
    apiAws: BazaAwsDataAccess;

    abort$: Subject<void> = new Subject<void>();

    constructor(lLoader, url) {
        this.loader = lLoader;
        this.url = url;
    }

    upload() {
        return (this.loader.file as Promise<File>).then((file) => {
            const loading = this.loading.addLoading();

            return this.apiAws
                .upload(file)
                .pipe(
                    map((response) => ({
                        default: `${response.presignedTimeLimitedUrl}#${BAZA_AWS_FIELD_PREFIX}:${response.s3ObjectId}`,
                    })),
                    finalize(() => loading.complete()),
                    takeUntil(this.abort$),
                )
                .toPromise();
        });
    }

    abort() {
        this.abort$.next();
    }
}

class BazaAttachmentUploadAdapter {
    loader;
    url;

    abort$: Subject<void> = new Subject<void>();

    loading: BazaLoadingService;
    apiAttachment: BazaAttachmentDataAccess;
    apiAttachmentRequest: (file: File, extension: string) => AttachmentUploadRequest;

    constructor(lLoader, url) {
        this.loader = lLoader;
        this.url = url;
    }

    upload() {
        return (this.loader.file as Promise<File>).then((file) => {
            const split = (file.name || '').split('.');
            const ext = split[split.length - 1];

            const loading = this.loading.addLoading();

            return this.apiAttachment
                .upload(file, this.apiAttachmentRequest(file, ext))
                .pipe(
                    map((response) => ({
                        default: `${response.presignedUrl}#${BAZA_AWS_FIELD_PREFIX}:${response.s3ObjectId}`,
                    })),
                    finalize(() => loading.complete()),
                    takeUntil(this.abort$),
                )
                .toPromise();
        });
    }

    abort() {
        this.abort$.next();
    }
}
