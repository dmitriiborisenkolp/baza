import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCkEditorComponent } from './baza-ck-editor.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { BazaAwsDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaAttachmentDataAccessModule } from '@scaliolabs/baza-core-data-access';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        CKEditorModule,
        BazaAwsDataAccessModule,
        BazaAttachmentDataAccessModule,
    ],
    declarations: [
        BazaCkEditorComponent,
    ],
    exports: [
        BazaCkEditorComponent,
    ],
})
export class BazaCkEditorModule {
}
