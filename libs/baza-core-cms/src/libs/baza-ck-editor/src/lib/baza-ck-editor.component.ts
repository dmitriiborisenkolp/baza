import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Inject,
    Input,
    OnChanges,
    OnDestroy,
    OnInit, PLATFORM_ID,
    SimpleChanges
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NgControl } from '@angular/forms';
import { CKEditor5 } from '@ckeditor/ckeditor5-angular';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { BazaCkEditorOptions, defaultBazaCkEditorOptions } from './baza-ck-editor.model';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import { BazaCkEditorUploadAdapter } from './baza-ck-editor.upload-adapter';
import { isPlatformBrowser } from '@angular/common';

interface State {
    form: FormGroup;
    ckEditor5config: CKEditor5.Config;
}

interface FormValue {
    value: string;
}

@Component({
    selector: 'baza-ck-editor',
    templateUrl: './baza-ck-editor.component.html',
    styleUrls: ['./baza-ck-editor.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        BazaCkEditorUploadAdapter,
    ],
})
export class BazaCkEditorComponent implements ControlValueAccessor, OnChanges, OnInit, OnDestroy {
    @Input() options: Partial<BazaCkEditorOptions>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();
    private readonly ngOnChanges$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.group({
            value: [''],
        }),
        ckEditor5config: {},
    };

    editor: CKEditor5.EditorConstructor;
    isEditorDisplayed = false;

    private disableValueChanges = false;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;
    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    constructor(
        public ngControl: NgControl,
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly ngEndpoint: BazaDataAccessService,
        private readonly uploadAdapter: BazaCkEditorUploadAdapter,
        @Inject(PLATFORM_ID) private platformId: Record<string, unknown>,
    ) {
        ngControl.valueAccessor = this;
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['options'] && ! changes['options'].firstChange) {
            this.updateCK5EditorConfig(this.options);
        }
    }

    ngOnInit(): void {
        this.updateCK5EditorConfig(this.options);

        this.state.form.valueChanges.pipe(
            filter(() => ! this.disableValueChanges),
            takeUntil(this.ngOnDestroy$),
        ).subscribe((v) => {
            this.onChange((v as FormValue).value);
        });

        if (isPlatformBrowser(this.platformId)) {
            import('@ckeditor/ckeditor5-build-classic')
                .then(instance => {
                    this.editor = instance.default;
                    this.isEditorDisplayed = true;

                    this.cdr.markForCheck();
                });
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
        this.ngOnChanges$.next();
    }

    updateCK5EditorConfig(withOptions: Partial<BazaCkEditorOptions>): void {
        const options: BazaCkEditorOptions = {
            ...defaultBazaCkEditorOptions(),
            ...withOptions,
        };

        this.state = {
            ...this.state,
            ckEditor5config: {
                ...options.withCkEditorOptions,
            },
        };

        this.cdr.markForCheck();
    }

    onCkEditorReady(editor: CKEditor5.Editor): void {
        const options: BazaCkEditorOptions = {
            ...defaultBazaCkEditorOptions(),
            ...this.options,
        };

        editor.plugins.get('FileRepository').createUploadAdapter = this.uploadAdapter.factory(options);
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled
            ? this.state.form.disable()
            : this.state.form.enable();
    }

    writeValue(obj: any): void {
        this.disableValueChanges = true;

        this.state.form.patchValue({
            value: obj,
        });

        this.disableValueChanges = false;
    }
}
