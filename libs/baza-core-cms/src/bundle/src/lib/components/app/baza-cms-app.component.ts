import { Component, OnInit } from '@angular/core';
import { BazaAuthCmsMasterPasswordService } from '../../../../../libs/baza-auth/src';

@Component({
    selector: 'baza-cms-app',
    templateUrl: './baza-cms-app.component.html',
    styleUrls: ['./baza-cms-app.component.less'],
})
export class BazaCmsAppComponent implements OnInit {
    constructor(private readonly bazaAuthCmsMasterPasswordService: BazaAuthCmsMasterPasswordService) {}

    ngOnInit() {
        this.bazaAuthCmsMasterPasswordService.fetchCurrent();
    }
}
