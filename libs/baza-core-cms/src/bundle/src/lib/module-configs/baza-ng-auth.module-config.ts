import {
    BazaAuthNgModuleForRootOptions,
    BazaJwtHttpInterceptorConfig,
    BazaJwtRequireAclGuardConfig,
    BazaJwtRequiredAuthGuardConfig,
    BazaJwtRequiredNoAuthGuardConfig,
    BazaJwtRequireRoleGuardConfig,
    BazaJwtVerifyGuardConfig,
    ngProdEnvironments,
} from '@scaliolabs/baza-core-ng';
import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';

export const BAZA_CMS_BUNDLE_GUARD_CONFIGS: {
    verifyJwtGuardConfig: BazaJwtVerifyGuardConfig;
    jwtHttpInterceptorConfig: BazaJwtHttpInterceptorConfig;
    requireRoleGuardConfig: BazaJwtRequireRoleGuardConfig;
    requireAclGuardConfig: BazaJwtRequireAclGuardConfig;
    requireAuthGuardConfig: BazaJwtRequiredAuthGuardConfig;
    requireNoAuthGuardConfig: BazaJwtRequiredNoAuthGuardConfig;
} = {
    verifyJwtGuardConfig: {
        verifySameJwtThrottle: 10 /* seconds */ * 1000,
    },
    jwtHttpInterceptorConfig: {
        redirect: () => ({
            commands: ['/auth'],
        }),
    },
    requireRoleGuardConfig: {
        redirect: () => ({
            commands: ['/auth'],
        }),
    },
    requireAclGuardConfig: {
        redirect: () => ({
            commands: ['/auth'],
        }),
    },
    requireAuthGuardConfig: {
        redirect: () => ({
            commands: ['/auth'],
        }),
    },
    requireNoAuthGuardConfig: {
        redirect: () => ({
            commands: ['/'],
        }),
    },
};

export const BAZA_NG_AUTH_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaAuthNgModuleForRootOptions = (config) => ({
    useFactory: () => ({
        autoRefreshToken: true,
        jwtStorages: {
            sessionStorage: ngProdEnvironments.includes(config.environment)
                ? {
                      enabled: true,
                      jwtKey: 'jwt',
                      jwtPayloadKey: 'jwtPayload',
                  }
                : {
                      enabled: false,
                  },
            localStorage: ngProdEnvironments.includes(config.environment)
                ? {
                      enabled: false,
                  }
                : {
                      enabled: true,
                      jwtKey: 'jwt',
                      jwtPayloadKey: 'jwtPayload',
                  },
            documentCookieStorage: {
                enabled: false,
            },
        },
        verifyJwtGuardConfig: BAZA_CMS_BUNDLE_GUARD_CONFIGS.verifyJwtGuardConfig,
        jwtHttpInterceptorConfig: BAZA_CMS_BUNDLE_GUARD_CONFIGS.jwtHttpInterceptorConfig,
        requireRoleGuardConfig: BAZA_CMS_BUNDLE_GUARD_CONFIGS.requireRoleGuardConfig,
        requireAclGuardConfig: BAZA_CMS_BUNDLE_GUARD_CONFIGS.requireAclGuardConfig,
        requireAuthGuardConfig: BAZA_CMS_BUNDLE_GUARD_CONFIGS.requireAuthGuardConfig,
        requireNoAuthGuardConfig: BAZA_CMS_BUNDLE_GUARD_CONFIGS.requireNoAuthGuardConfig,
    }),
});
