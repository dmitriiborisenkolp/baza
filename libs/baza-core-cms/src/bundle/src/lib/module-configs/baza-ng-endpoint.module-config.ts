import { BazaNgEndpointModuleForRootOptions } from '@scaliolabs/baza-core-data-access';
import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';

export const BAZA_NG_ENDPOINT_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaNgEndpointModuleForRootOptions = (config) => ({
    injects: [],
    useFactory: () => ({
        apiEndpointUrl: config.apiEndpoint(),
    }),
});

