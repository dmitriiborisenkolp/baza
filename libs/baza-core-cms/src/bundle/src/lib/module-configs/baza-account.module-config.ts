import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';
import { BazaAccountCmsModuleForRootOptions } from '../../../../libs/baza-account/src';

export const BAZA_ACCOUNT_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaAccountCmsModuleForRootOptions = (config) => ({
    useFactory: () => ({
        withProfileImagesFeature: false,
        withResetAllPasswordFeature: false,
        withDeviceTokenFeature: config.withDeviceTokenFeature,
    }),
});
