import { BazaPhoneCountryCodesNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';

export const BAZA_PHONE_COUNTRY_CODES_NG_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaPhoneCountryCodesNgModuleForRootOptions = () => ({
    useFactory: () => ({
        ttlMilliseconds: 60 /* minutes */ * 60 /* seconds */ * 1000 /* ms */,
    }),
});
