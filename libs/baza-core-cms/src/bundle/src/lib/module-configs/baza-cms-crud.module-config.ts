import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';
import { BazaCrudCmsModuleForRootOptions } from '../../../../libs/baza-crud/src';

export const BAZA_CMS_CRUD_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaCrudCmsModuleForRootOptions = () => ({
    deps: [],
    useFactory: () => ({
        defaults: {
            size: 25,
            sizes: [25, 50, 100],
        },
    }),
});
