import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';
import { BazaMailCustomerioLinkCmsModuleForRootOptions } from '../../../../libs/baza-mail-customerio-link/src';

export const BAZA_CUSTOMERIO_LINK_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaMailCustomerioLinkCmsModuleForRootOptions = (
    config,
) => ({
    deps: [],
    useFactory: () => ({
        withCustomerIoLinkCmsFeature: config.withCustomerIoLinkCms,
        withCustomerIoLinkCmsUnlinkFeature: config.withCustomerIoLinkCmsUnlinkFeature,
    }),
});
