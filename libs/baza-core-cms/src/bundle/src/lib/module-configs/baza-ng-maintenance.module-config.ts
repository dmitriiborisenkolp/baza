import { BazaMaintenanceNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';

export const BAZA_NG_MAINTENANCE_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaMaintenanceNgModuleForRootOptions = () => ({
    deps: [],
    useFactory: () => ({
        skip: true,
    }),
});
