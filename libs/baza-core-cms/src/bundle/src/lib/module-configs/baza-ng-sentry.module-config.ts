import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';
import { BazaSentryNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';

export const BAZA_NG_SENTRY_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaSentryNgModuleForRootOptions = (config: BazaCmsBundleConfig) => ({
    injects: [],
    useFactory: () => ({
        sentry: {
            dsn: (config.sentry || {}).dsn,
            browserOptions: {
                ...(config.sentry || {}).browserOptions,
                environment: config.environment,
            },
        },
    }),
});
