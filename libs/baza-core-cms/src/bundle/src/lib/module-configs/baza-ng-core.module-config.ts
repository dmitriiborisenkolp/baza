import { BazaNgCoreModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';
import { Application, getBazaProjectCodeName, getBazaProjectName } from '@scaliolabs/baza-core-shared';

export const BAZA_NG_CORE_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaNgCoreModuleForRootOptions = (config) => ({
    deps: [],
    useFactory: () => ({
        application: Application.CMS,
        project: {
            name: getBazaProjectName(),
            codeName: getBazaProjectCodeName(),
        },
        ngEnv: config.environment,
        defaultMessageDurationMilliseconds: 5000,
        localStorageKeyPrefix: `${getBazaProjectCodeName().toLowerCase()}Cms`,
        sessionStorageKeyPrefix: `${getBazaProjectCodeName().toLowerCase()}Cms`,
    }),
});
