import { bazaCmsBundleSideFactory } from '../baza-cms-bundle.sidebar-factory';
import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';
import { BazaCmsLayoutModuleForRootOptions } from '../../../../cms/layout/src';

export const BAZA_LAYOUT_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaCmsLayoutModuleForRootOptions = (config) => ({
    deps: [],
    useFactory: () => ({
        withI18nSupport: config.withI18n,
        sidePanelAuthWidth: config.sidePanelAuthWidth,
        sidePanel: (() => {
            const defaults = bazaCmsBundleSideFactory(config);
            const additionalGroups = config.sidePanelGroups();

            return config.sidePanelFactory(defaults, additionalGroups);
        })(),
        signOutRedirectUrlTree: {
            commands: ['/auth'],
            navigationExtras: {},
        },
    }),
});
