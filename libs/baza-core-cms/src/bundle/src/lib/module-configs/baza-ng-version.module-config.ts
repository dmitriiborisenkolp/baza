import { BazaVersionNgModuleForRootOptions, BazaVersionNgStrategy } from '@scaliolabs/baza-core-ng';
import { BazaCmsBundleConfig } from '../baza-cms-bundle.config';
import { BazaHttpHeaders } from '@scaliolabs/baza-core-shared';

export const BAZA_NG_VERSION_MODULE_CONFIG: (config: BazaCmsBundleConfig) => BazaVersionNgModuleForRootOptions = () => ({
    deps: [],
    useFactory: () => ({
        strategy: {
            type: BazaVersionNgStrategy.SkipVersion,
        },
        xHttpHeaderVersion: BazaHttpHeaders.HTTP_HEADER_VERSION,
        xHttpHeaderSkipVersion: BazaHttpHeaders.HTTP_HEADER_VERSION_SKIP,
    }),
});

