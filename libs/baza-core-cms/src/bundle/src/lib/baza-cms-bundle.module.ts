import { isPlatformBrowser, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import en from '@angular/common/locales/en';
import { Inject, ModuleWithProviders, NgModule, PLATFORM_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationEnd, Router, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { BazaCmsDataAccessModule } from '@scaliolabs/baza-core-cms-data-access';
import { BazaDataAccessModule, BazaI18nDataAccessModule } from '@scaliolabs/baza-core-data-access';
import {
    BazaAuthNgModule,
    BazaI18nNgModule,
    BazaMaintenanceNgModule,
    BazaNgCoreModule,
    BazaPhoneCountryCodesNgModule,
    BazaSentryNgModule,
    BazaVersionNgModule,
} from '@scaliolabs/baza-core-ng';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { filter, take } from 'rxjs/operators';
import { BazaCmsLayoutModule } from '../../../cms/layout/src';
import { bazaAccountCmsDefineMetadata, BazaAccountCmsModule } from '../../../libs/baza-account/src';
import { BazaAuthSessionCmsModule } from '../../../libs/baza-auth-session/src';
import { BazaAuthCmsModule } from '../../../libs/baza-auth/src';
import { BazaCrudCmsModule } from '../../../libs/baza-crud/src';
import { BazaDashboardCmsModule } from '../../../libs/baza-dashboard/src';
import { BazaDeviceTokensCmsModule } from '../../../libs/baza-device-token/src';
import { BazaInviteCodesCmsModule } from '../../../libs/baza-invite-code/src';
import { BazaMaintenanceCmsModule } from '../../../libs/baza-maintenance/src';
import { BazaReferralCodesCmsModule } from '../../../libs/baza-referral-code/src';
import { BazaRegistryCmsModule } from '../../../libs/baza-registry/src';
import { BazaWhitelistAccountCmsModule } from '../../../libs/baza-whitelist-account/src';
import { BazaCmsBundleConfig, BAZA_CMS_BUNDLE_CONFIG } from './baza-cms-bundle.config';
import { BazaCmsBundleModuleConfigs, defaultBazaCmsBundleModuleConfigs } from './baza-cms-bundle.module-configs';
import { BazaCmsAppComponent } from './components/app/baza-cms-app.component';
import { BAZA_ACCOUNT_PROVIDER } from './providers/baza-account.provider';
import { BAZA_NG_AUTH_JWT_INTERCEPTOR_PROVIDER } from './providers/baza-ng-auth-jwt-interceptor.provider';
import { BAZA_NG_I18N_INTERCEPTOR_PROVIDER } from './providers/baza-ng-i18n-interceptor.provider';
import { BazaNgCmsEnvironment } from './services/baza-ng-cms.environment';
import { BazaMailCustomerioLinkCmsModule } from '../../../libs/baza-mail-customerio-link/src';

registerLocaleData(en);

// @dynamic
@NgModule({
    declarations: [BazaCmsAppComponent],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        BazaI18nDataAccessModule,
        RouterModule,
        BazaNgCoreModule,
        BazaAccountCmsModule,
        BazaCrudCmsModule,
        BazaCmsLayoutModule,
        BazaAuthNgModule,
        BazaDataAccessModule,
        BazaI18nNgModule,
        TranslateModule,
        BazaMaintenanceNgModule,
        BazaVersionNgModule,
        BazaPhoneCountryCodesNgModule,
        BazaDashboardCmsModule,
        BazaRegistryCmsModule,
        BazaWhitelistAccountCmsModule,
        BazaMaintenanceCmsModule,
        BazaAuthSessionCmsModule,
        BazaInviteCodesCmsModule,
        BazaReferralCodesCmsModule,
        BazaDeviceTokensCmsModule,
        BazaMailCustomerioLinkCmsModule,
    ],
    exports: [BazaCmsAppComponent],
    providers: [BAZA_NG_AUTH_JWT_INTERCEPTOR_PROVIDER, BAZA_NG_I18N_INTERCEPTOR_PROVIDER, BAZA_ACCOUNT_PROVIDER],
    bootstrap: [BazaCmsAppComponent],
})
export class BazaCmsBundleModule {
    static forRoot(config: BazaCmsBundleConfig): ModuleWithProviders<BazaCmsBundleModule> {
        const moduleConfigs: BazaCmsBundleModuleConfigs = {
            ...defaultBazaCmsBundleModuleConfigs,
            ...config.moduleConfigs,
        };

        return {
            ngModule: BazaCmsBundleModule,
            providers: [
                {
                    provide: BAZA_CMS_BUNDLE_CONFIG,
                    useValue: config,
                },
                {
                    provide: NZ_I18N,
                    useValue: config.nzI18n,
                },
                ...BazaNgCoreModule.forRoot(moduleConfigs.BazaNgCoreModule(config)).providers,
                ...BazaAccountCmsModule.forRoot(moduleConfigs.BazaAccountCmsModule(config)).providers,
                ...BazaCrudCmsModule.forRoot(moduleConfigs.BazaCrudCmsModule(config)).providers,
                ...BazaCmsLayoutModule.forRoot(moduleConfigs.BazaCmsLayoutModule(config)).providers,
                ...BazaAuthNgModule.forRoot(moduleConfigs.BazaAuthNgModule(config)).providers,
                ...BazaDataAccessModule.forRoot(moduleConfigs.BazaNgEndpointModule(config)).providers,
                ...BazaCmsDataAccessModule.forRoot(moduleConfigs.BazaNgEndpointModule(config)).providers,
                ...BazaI18nNgModule.forRoot(moduleConfigs.BazaI18nNgModule(config)).providers,
                ...TranslateModule.forRoot(moduleConfigs.TranslateModule(config)).providers,
                ...BazaMaintenanceNgModule.forRoot(moduleConfigs.BazaMaintenanceNgModule(config)).providers,
                ...BazaVersionNgModule.forRoot(moduleConfigs.BazaVersionNgModule(config)).providers,
                ...BazaPhoneCountryCodesNgModule.forRoot(moduleConfigs.BazaPhoneCountryCodesNgModule(config)).providers,
                ...BazaSentryNgModule.forRoot(moduleConfigs.BazaSentryNgModule(config)).providers,
                ...BazaMailCustomerioLinkCmsModule.forRoot(moduleConfigs.BazaCustomerIoLinkCmsModule(config)).providers,
                BazaAuthCmsModule,
            ],
        };
    }

    constructor(
        // eslint-disable-next-line @typescript-eslint/ban-types
        @Inject(PLATFORM_ID) private platformId: Object,
        @Inject(BAZA_CMS_BUNDLE_CONFIG) private readonly moduleConfig: BazaCmsBundleConfig,
        private readonly router: Router,
        private readonly ngEnvironmentService: BazaNgCmsEnvironment,
    ) {
        this.initNgEnvironment();
        this.initAccountCms();
        this.destroyInitialLoader();
    }

    private initNgEnvironment(): void {
        this.ngEnvironmentService.current = this.moduleConfig.environmentConfig;
    }

    private initAccountCms(): void {
        if (this.moduleConfig.accountCmsMetadata) {
            bazaAccountCmsDefineMetadata(this.moduleConfig.accountCmsMetadata);
        }
    }

    private destroyInitialLoader(): void {
        if (isPlatformBrowser(this.platformId)) {
            this.router.events
                .pipe(
                    filter((e) => e instanceof NavigationEnd),
                    take(1),
                )
                .subscribe(() => {
                    if (window && window.document && window.document.getElementById) {
                        const initialLoader = window.document.getElementById('bazaInitialLoader');

                        if (initialLoader && initialLoader.remove) {
                            initialLoader.remove();
                        }
                    }
                });
        }
    }
}
