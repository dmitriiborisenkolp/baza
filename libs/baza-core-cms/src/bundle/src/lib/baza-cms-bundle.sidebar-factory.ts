import {
    AccountAcl,
    BazaInviteCodeAcl,
    BazaMailACL,
    BazaMaintenanceAcl,
    BazaReferralCodesAcl,
    CoreAcl,
} from '@scaliolabs/baza-core-shared';
import { CmsSidePanelConfig, CmsSidePanelMenuItemConfig } from '../../../cms/layout/src';
import { BazaCmsBundleMenu } from './models/baza-cms-bundle-menu';
import { BazaCmsBundleConfig } from './baza-cms-bundle.config';

export function bazaCmsBundleSideFactory(config: BazaCmsBundleConfig): CmsSidePanelConfig {
    const items: Array<{
        menuItem: BazaCmsBundleMenu;
        menuConfig: CmsSidePanelMenuItemConfig;
    }> = [
        {
            menuItem: BazaCmsBundleMenu.Dashboard,
            menuConfig: {
                title: 'baza.dashboard.title',
                translate: true,
                icon: 'dashboard',
                routerLink: ['/dashboard'],
            },
        },
        {
            menuItem: BazaCmsBundleMenu.Accounts,
            menuConfig: {
                title: 'baza.account.title',
                translate: true,
                icon: 'user',
                routerLink: ['/accounts'],
                requiresACL: [AccountAcl.Accounts],
            },
        },
        {
            menuItem: BazaCmsBundleMenu.AuthSessions,
            menuConfig: {
                title: 'baza.authSessions.title',
                translate: true,
                icon: 'user-switch',
                routerLink: ['/auth-sessions'],
                requiresACL: [AccountAcl.AccountsAuthSession],
            },
        },
        {
            menuItem: BazaCmsBundleMenu.WhitelistAccounts,
            menuConfig: {
                title: 'baza.whitelistAccount.title',
                translate: true,
                icon: 'lock',
                routerLink: ['/whitelisted-accounts'],
                requiresACL: [AccountAcl.Accounts],
            },
        },
        {
            menuItem: BazaCmsBundleMenu.MasterPassword,
            menuConfig: {
                title: 'baza.auth.masterPassword.title',
                translate: true,
                icon: 'key',
                routerLink: ['/auth-master-password'],
                requiresACL: [CoreAcl.BazaAuthMasterPassword],
            },
        },
        {
            menuItem: BazaCmsBundleMenu.Registry,
            menuConfig: {
                title: 'baza.registry.title',
                translate: true,
                icon: 'setting',
                routerLink: ['/registry'],
                requiresACL: [CoreAcl.BazaRegistry],
            },
        },
        {
            menuItem: BazaCmsBundleMenu.Maintenance,
            menuConfig: {
                title: 'baza.maintenance.title',
                translate: true,
                icon: 'api',
                routerLink: ['/maintenance'],
                requiresACL: [BazaMaintenanceAcl.BazaMaintenance],
            },
        },
        {
            menuItem: BazaCmsBundleMenu.MailCustomerIoLink,
            menuConfig: {
                title: 'baza.mail.customerioLink.title',
                translate: true,
                icon: 'file-text',
                routerLink: ['/mail/customerio-links'],
                requiresACL: [BazaMailACL.BazaMailCustomerIoLink],
                disabled: !config.withCustomerIoLinkCms,
            },
        },
        {
            menuItem: BazaCmsBundleMenu.InviteCode,
            menuConfig: {
                title: 'baza.inviteCodes.title',
                translate: true,
                icon: 'barcode',
                routerLink: ['/baza-core/invite-codes'],
                requiresACL: [BazaInviteCodeAcl.BazaInviteCodes],
            },
        },
        {
            menuItem: BazaCmsBundleMenu.ReferralCode,
            menuConfig: {
                title: 'baza.referralCodes.title',
                translate: true,
                icon: 'shake',
                routerLink: ['/baza-core/referral-codes'],
                requiresACL: [BazaReferralCodesAcl.BazaReferralCodes],
            },
        },
    ];

    return {
        baseWidth: config.sidePanelWidth,
        groups: [
            {
                title: 'baza.title',
                items: items.filter((item) => config.enabledCoreMenuItems.includes(item.menuItem)).map((item) => item.menuConfig),
            },
        ],
    };
}
