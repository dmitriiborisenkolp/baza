import { InjectionToken } from '@angular/core';
import { BazaCmsBundleModuleConfigs } from './baza-cms-bundle.module-configs';
import { NzI18nInterface } from 'ng-zorro-antd/i18n/nz-i18n.interface';
import { en_US } from 'ng-zorro-antd/i18n';
import { BrowserOptions } from '@sentry/browser/dist/backend';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { CmsSidePanelConfig, CmsSidePanelMenuGroupConfig } from '../../../cms/layout/src';
import { BazaCmsBundleEnvironment } from './models/baza-cms-bundle-environment';
import { BazaAccountCmsMetadataFunc } from '../../../libs/baza-account/src';
import { BazaCmsBundleMenu } from './models/baza-cms-bundle-menu';

export interface BazaCmsBundleConfig<ENV extends BazaCmsBundleEnvironment = BazaCmsBundleEnvironment> {
    apiEndpoint: () => string;
    environment: NgEnvironment;
    environmentConfig: ENV;
    withI18n: boolean;
    enabledCoreMenuItems: Array<BazaCmsBundleMenu>;
    sidePanelWidth: string;
    sidePanelAuthWidth: string;
    sidePanelFactory: (defaults: CmsSidePanelConfig, additionalGroups: Array<CmsSidePanelMenuGroupConfig>) => CmsSidePanelConfig;
    sidePanelGroups: () => Array<CmsSidePanelMenuGroupConfig>;
    moduleConfigs: Partial<BazaCmsBundleModuleConfigs>;
    nzI18n: NzI18nInterface;
    accountCmsMetadata?: BazaAccountCmsMetadataFunc;
    sentry?: {
        dsn: string;
        browserOptions?: BrowserOptions;
    };
    withDeviceTokenFeature: boolean;
    withCustomerIoLinkCms: boolean;
    withCustomerIoLinkCmsUnlinkFeature: boolean;
}

let INSTANCE: BazaCmsBundleConfigBuilder;

export class BazaCmsBundleConfigBuilder<ENV extends BazaCmsBundleEnvironment = BazaCmsBundleEnvironment> {
    private _config: BazaCmsBundleConfig = {
        apiEndpoint: () => {
            throw new Error('No API endpoint set');
        },
        environment: NgEnvironment.Local,
        environmentConfig: {
            ngEnv: NgEnvironment.Local,
            apiEndpoint: undefined,
            enableAngularProduction: false,
        },
        withI18n: false,
        enabledCoreMenuItems: Object.values(BazaCmsBundleMenu),
        sidePanelWidth: '280px',
        sidePanelAuthWidth: '500px',
        sidePanelFactory: (defaults, additionalGroups) => ({
            ...defaults,
            groups: [...defaults.groups, ...additionalGroups],
        }),
        sidePanelGroups: () => [],
        moduleConfigs: {},
        nzI18n: en_US,
        withDeviceTokenFeature: false,
        withCustomerIoLinkCms: false,
        withCustomerIoLinkCmsUnlinkFeature: false,
    };

    constructor() {
        INSTANCE = this;
    }

    get config(): BazaCmsBundleConfig {
        return this._config;
    }

    withApiEndpoint(apiEndpoint: string): BazaCmsBundleConfigBuilder {
        this.config.apiEndpoint = () => apiEndpoint;

        return this;
    }

    withEnvironment(env: ENV): BazaCmsBundleConfigBuilder {
        this._config.environmentConfig = env;
        this._config.environment = env.ngEnv;
        this._config.apiEndpoint = () => env.apiEndpoint;

        return this;
    }

    withNgEnvironment(ngEnv: NgEnvironment): BazaCmsBundleConfigBuilder {
        this.config.environment = ngEnv;

        return this;
    }

    withI18nSupport(): BazaCmsBundleConfigBuilder {
        this.config.withI18n = true;

        return this;
    }

    withoutI18nSupport(): BazaCmsBundleConfigBuilder {
        this.config.withI18n = false;

        return this;
    }

    withCoreMenuItems(menuItems: Array<BazaCmsBundleMenu>): BazaCmsBundleConfigBuilder {
        this.config.enabledCoreMenuItems = menuItems;

        return this;
    }

    withSentry(dsn: string, browserOptions?: BrowserOptions): BazaCmsBundleConfigBuilder {
        this.config.sentry = {
            ...this.config.sentry,
            dsn,
            browserOptions: {
                ...browserOptions,
                environment: this.config.environment,
            },
        };

        return this;
    }

    withNzI18n(nzI18n: NzI18nInterface): BazaCmsBundleConfigBuilder {
        this._config.nzI18n = nzI18n;

        return this;
    }

    withSidePanelGroups(sidePanelGroupsFactory: () => Array<CmsSidePanelMenuGroupConfig>): BazaCmsBundleConfigBuilder {
        this._config.sidePanelGroups = sidePanelGroupsFactory;

        return this;
    }

    withAccountCmsMetadata(callback: BazaAccountCmsMetadataFunc): BazaCmsBundleConfigBuilder {
        this._config.accountCmsMetadata = callback;

        return this;
    }

    withSidePanelWidth(cssWidth: string): BazaCmsBundleConfigBuilder {
        this._config.sidePanelWidth = cssWidth;

        return this;
    }

    withAuthSidePanelWidth(cssWidth: string): BazaCmsBundleConfigBuilder {
        this._config.sidePanelAuthWidth = cssWidth;

        return this;
    }

    withModuleConfigs(moduleConfigs: Partial<BazaCmsBundleModuleConfigs>): BazaCmsBundleConfigBuilder {
        this._config.moduleConfigs = {
            ...this._config.moduleConfigs,
            ...moduleConfigs,
        };

        return this;
    }

    withDeviceTokenFeature(): BazaCmsBundleConfigBuilder {
        this.config.withDeviceTokenFeature = true;

        return this;
    }

    withoutDeviceTokenFeature(): BazaCmsBundleConfigBuilder {
        this.config.withDeviceTokenFeature = false;

        return this;
    }

    /**
     * Enables Customer.IO Link CMS
     * This should be enabled if you're using Customer.IO Templates
     * @param options
     */
    withCustomerIoLinkCms(
        options = {
            withCustomerIoLinkCmsUnlinkFeature: false,
        },
    ): BazaCmsBundleConfigBuilder {
        this.config.withCustomerIoLinkCms = true;
        this.config.withCustomerIoLinkCmsUnlinkFeature = options.withCustomerIoLinkCmsUnlinkFeature;

        return this;
    }

    /**
     * Disables Customer.IO Link CMS
     * This should be enabled if you're using Customer.IO Templates
     */
    withoutCustomerIoLinkCms(): BazaCmsBundleConfigBuilder {
        this.config.withCustomerIoLinkCms = false;

        return this;
    }
}

const configBuilder = new BazaCmsBundleConfigBuilder();

export function bazaCmsBundleConfigBuilder(): BazaCmsBundleConfigBuilder {
    return configBuilder;
}

export const BAZA_CMS_BUNDLE_CONFIG = new InjectionToken('BAZA_CMS_BUNDLE_CONFIG');
