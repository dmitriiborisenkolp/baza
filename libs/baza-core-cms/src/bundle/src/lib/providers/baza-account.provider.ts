import { Provider } from '@angular/core';
import { BazaAccountCmsConfig } from '../../../../libs/baza-account/src';

const defaultConfig: BazaAccountCmsConfig = {
    withProfileImagesFeature: false,
    withResetAllPasswordFeature: false,
    withDeviceTokenFeature: false,
};

export const BAZA_ACCOUNT_PROVIDER: Provider = {
    provide: BazaAccountCmsConfig,
    useValue: defaultConfig,
};
