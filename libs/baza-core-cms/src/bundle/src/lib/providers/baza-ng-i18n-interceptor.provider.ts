import { Provider } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BazaI18nNgHttpInterceptor } from '@scaliolabs/baza-core-ng';

export const BAZA_NG_I18N_INTERCEPTOR_PROVIDER: Provider = {
    provide: HTTP_INTERCEPTORS,
    useClass: BazaI18nNgHttpInterceptor,
    multi: true,
};
