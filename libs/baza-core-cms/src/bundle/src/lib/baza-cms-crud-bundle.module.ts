import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BazaFormBuilderModule } from '../../../libs/baza-form-builder/src';
import { BazaCmsLayoutModule } from '../../../cms/layout/src';
import { BazaCrudCmsModule } from '../../../libs/baza-crud/src';

const SHARED_MODULES = [
    CommonModule,
    ReactiveFormsModule,
    BazaFormBuilderModule,
    BazaCrudCmsModule,
    BazaCmsLayoutModule,
];

@NgModule({
    imports: [
        ...SHARED_MODULES,
    ],
    exports: [
        ...SHARED_MODULES,
    ],
})
export class BazaCmsCrudBundleModule {}
