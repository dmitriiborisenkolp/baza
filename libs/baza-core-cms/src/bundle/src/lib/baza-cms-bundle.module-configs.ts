import { BazaAuthNgModuleForRootOptions, BazaPhoneCountryCodesNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BazaNgCoreModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BazaNgEndpointModuleForRootOptions } from '@scaliolabs/baza-core-data-access';
import { BazaI18nNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { TranslateModuleConfig } from '@ngx-translate/core/public_api';
import { BazaMaintenanceNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BazaVersionNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BAZA_NG_AUTH_MODULE_CONFIG } from './module-configs/baza-ng-auth.module-config';
import { BAZA_NG_ENDPOINT_MODULE_CONFIG } from './module-configs/baza-ng-endpoint.module-config';
import { BAZA_NG_VERSION_MODULE_CONFIG } from './module-configs/baza-ng-version.module-config';
import { BAZA_NG_MAINTENANCE_MODULE_CONFIG } from './module-configs/baza-ng-maintenance.module-config';
import { BAZA_LAYOUT_MODULE_CONFIG } from './module-configs/baza-layout.module-config';
import { BAZA_NG_I18_MODULE_CONFIG, NX_I18N_MODULE_CONFIG } from './module-configs/baza-ng-i18n.module-config';
import { BAZA_PHONE_COUNTRY_CODES_NG_MODULE_CONFIG } from './module-configs/baza-phone-country-codes.module-config';
import { BAZA_CMS_CRUD_MODULE_CONFIG } from './module-configs/baza-cms-crud.module-config';
import { BAZA_NG_CORE_MODULE_CONFIG } from './module-configs/baza-ng-core.module-config';
import { BAZA_ACCOUNT_MODULE_CONFIG } from './module-configs/baza-account.module-config';
import { BazaSentryNgModuleForRootOptions } from '@scaliolabs/baza-core-ng';
import { BAZA_NG_SENTRY_MODULE_CONFIG } from './module-configs/baza-ng-sentry.module-config';
import { BazaCmsLayoutModuleForRootOptions } from '../../../cms/layout/src';
import { BazaAccountCmsModuleForRootOptions } from '../../../libs/baza-account/src';
import { BazaCmsBundleConfig } from './baza-cms-bundle.config';
import { BazaCrudCmsModuleForRootOptions } from '../../../libs/baza-crud/src';
import { BazaMailCustomerioLinkCmsModuleForRootOptions } from '../../../libs/baza-mail-customerio-link/src';
import { BAZA_CUSTOMERIO_LINK_MODULE_CONFIG } from './module-configs/baza-customerio-link.module-config';

export interface BazaCmsBundleModuleConfigs {
    BazaAccountCmsModule: (config: BazaCmsBundleConfig) => BazaAccountCmsModuleForRootOptions;
    BazaCrudCmsModule: (config: BazaCmsBundleConfig) => BazaCrudCmsModuleForRootOptions;
    BazaCmsLayoutModule: (config: BazaCmsBundleConfig) => BazaCmsLayoutModuleForRootOptions;
    BazaAuthNgModule: (config: BazaCmsBundleConfig) => BazaAuthNgModuleForRootOptions;
    BazaNgCoreModule: (config: BazaCmsBundleConfig) => BazaNgCoreModuleForRootOptions;
    BazaNgEndpointModule: (config: BazaCmsBundleConfig) => BazaNgEndpointModuleForRootOptions;
    BazaI18nNgModule: (config: BazaCmsBundleConfig) => BazaI18nNgModuleForRootOptions;
    TranslateModule: (config: BazaCmsBundleConfig) => TranslateModuleConfig;
    BazaMaintenanceNgModule: (config: BazaCmsBundleConfig) => BazaMaintenanceNgModuleForRootOptions;
    BazaVersionNgModule: (config: BazaCmsBundleConfig) => BazaVersionNgModuleForRootOptions;
    BazaPhoneCountryCodesNgModule: (config: BazaCmsBundleConfig) => BazaPhoneCountryCodesNgModuleForRootOptions;
    BazaSentryNgModule: (config: BazaCmsBundleConfig) => BazaSentryNgModuleForRootOptions;
    BazaCustomerIoLinkCmsModule: (config: BazaCmsBundleConfig) => BazaMailCustomerioLinkCmsModuleForRootOptions;
}

export const defaultBazaCmsBundleModuleConfigs: BazaCmsBundleModuleConfigs = {
    BazaAccountCmsModule: BAZA_ACCOUNT_MODULE_CONFIG,
    BazaCrudCmsModule: BAZA_CMS_CRUD_MODULE_CONFIG,
    BazaCmsLayoutModule: BAZA_LAYOUT_MODULE_CONFIG,
    BazaAuthNgModule: BAZA_NG_AUTH_MODULE_CONFIG,
    BazaNgCoreModule: BAZA_NG_CORE_MODULE_CONFIG,
    BazaNgEndpointModule: BAZA_NG_ENDPOINT_MODULE_CONFIG,
    BazaI18nNgModule: BAZA_NG_I18_MODULE_CONFIG,
    TranslateModule: NX_I18N_MODULE_CONFIG,
    BazaMaintenanceNgModule: BAZA_NG_MAINTENANCE_MODULE_CONFIG,
    BazaVersionNgModule: BAZA_NG_VERSION_MODULE_CONFIG,
    BazaPhoneCountryCodesNgModule: BAZA_PHONE_COUNTRY_CODES_NG_MODULE_CONFIG,
    BazaSentryNgModule: BAZA_NG_SENTRY_MODULE_CONFIG,
    BazaCustomerIoLinkCmsModule: BAZA_CUSTOMERIO_LINK_MODULE_CONFIG,
};
