import { NgEnvironment } from '@scaliolabs/baza-core-ng';
import { BrowserOptions } from '@sentry/browser/dist/backend';

export interface BazaCmsBundleEnvironment {
    ngEnv: NgEnvironment;
    enableAngularProduction: boolean;
    apiEndpoint: string;
    sentry?: {
        dsn: string;
        browserOptions?: BrowserOptions;
    }
}
