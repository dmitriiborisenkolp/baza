export enum BazaCmsBundleMenu {
    Dashboard = 'Dashboard',
    Accounts = 'Accounts',
    AuthSessions = 'AuthSessions',
    WhitelistAccounts = 'WhitelistAccounts',
    MasterPassword = 'MasterPassword',
    Registry = 'Registry',
    Maintenance = 'Maintenance',
    MailCustomerIoLink = 'MailCustomerIoLink',
    InviteCode = 'InviteCode',
    ReferralCode = 'ReferralCode',
}
