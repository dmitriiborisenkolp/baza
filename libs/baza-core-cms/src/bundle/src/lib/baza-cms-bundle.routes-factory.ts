import { Routes } from '@angular/router';
import { BazaI18nPrefetchResolver, BazaJwtRequiredNoAuthGuardRouteConfig, JwtRequireNoAuthGuard } from '@scaliolabs/baza-core-ng';
import { JwtRequireAdminGuard, JwtVerifyGuard } from '@scaliolabs/baza-core-ng';
import { BazaPhoneCountryCodesResolver } from '@scaliolabs/baza-core-ng';
import {
    BazaCmsAuthLayoutComponent,
    BazaCmsLayoutComponent,
    BazaCmsLayoutResolve,
    BazaEntryLayoutComponent,
} from '../../../cms/layout/src';
import { BazaCmsForgotPasswordComponent, BazaCmsResetPasswordComponent } from '../../../cms/account/src';
import {
    BazaCmsAuthComponent,
    BazaCmsAuthMethodComponent,
    BazaCmsAuthMethodGoogleAuthenticatorComponent,
    BazaCmsAuthMethodMicrosoftAuthenticatorComponent,
    BazaCmsAuthVerifyComponent,
} from '../../../cms/auth/src';
import { bazaMaintenanceCmsRoutes } from '../../../libs/baza-maintenance/src';
import { bazaDashboardCmsRoutes } from '../../../libs/baza-dashboard/src';
import { bazaAccountCmsRoutes } from '../../../libs/baza-account/src';
import { bazaRegistryCmsRoutes } from '../../../libs/baza-registry/src';
import { bazaWhitelistAccountCmsRoutes } from '../../../libs/baza-whitelist-account/src';
import { bazaAuthCmsRoutes } from '../../../libs/baza-auth/src';
import { bazaNotFoundCmsRoutes } from '../../../cms/not-found/src';
import { bazaAuthSessionCmsRoutes } from '../../../libs/baza-auth-session/src';
import { bazaInviteCodesCmsRoutes } from '../../../libs/baza-invite-code/src';
import { bazaReferralCodesRoutes } from '../../../libs/baza-referral-code/src';
import { bazaDeviceTokensCmsRoutes } from '../../../libs/baza-device-token/src';
import { bazaMailCustomerioLinkCmsRoutes } from '../../../libs/baza-mail-customerio-link/src';

export function bazaCmsBundleRoutesFactory(additionalRoutes: Routes): Routes {
    return [
        {
            path: '',
            component: BazaEntryLayoutComponent,
            resolve: [BazaI18nPrefetchResolver, BazaPhoneCountryCodesResolver],
            children: [
                {
                    path: '',
                    component: BazaCmsLayoutComponent,
                    canActivateChild: [JwtVerifyGuard, JwtRequireAdminGuard],
                    resolve: [BazaCmsLayoutResolve],
                    children: [
                        {
                            path: '',
                            pathMatch: 'full',
                            redirectTo: 'dashboard',
                        },
                        ...bazaDashboardCmsRoutes,
                        ...bazaAccountCmsRoutes,
                        ...bazaRegistryCmsRoutes,
                        ...bazaWhitelistAccountCmsRoutes,
                        ...bazaMaintenanceCmsRoutes,
                        ...bazaAuthCmsRoutes,
                        ...bazaNotFoundCmsRoutes,
                        ...bazaAuthSessionCmsRoutes,
                        ...bazaInviteCodesCmsRoutes,
                        ...bazaReferralCodesRoutes,
                        ...bazaDeviceTokensCmsRoutes,
                        ...bazaMailCustomerioLinkCmsRoutes,
                        ...additionalRoutes,
                    ],
                },
                {
                    path: 'auth',
                    component: BazaCmsAuthLayoutComponent,
                    canActivateChild: [JwtRequireNoAuthGuard],
                    data: {
                        jwtRequireNoAuthGuard: {
                            redirect: () => ({
                                commands: ['/'],
                            }),
                        },
                    } as BazaJwtRequiredNoAuthGuardRouteConfig,
                    children: [
                        {
                            path: '',
                            pathMatch: 'full',
                            redirectTo: 'sign-in',
                        },
                        {
                            path: 'sign-in',
                            component: BazaCmsAuthComponent,
                        },
                        {
                            path: 'verify',
                            component: BazaCmsAuthVerifyComponent,
                        },
                        {
                            path: 'method',
                            component: BazaCmsAuthMethodComponent,
                        },
                        {
                            path: 'google-authenticator',
                            component: BazaCmsAuthMethodGoogleAuthenticatorComponent,
                        },
                        {
                            path: 'microsoft-authenticator',
                            component: BazaCmsAuthMethodMicrosoftAuthenticatorComponent,
                        },
                        {
                            path: 'forgot-password',
                            component: BazaCmsForgotPasswordComponent,
                        },
                        {
                            path: 'reset-password',
                            component: BazaCmsResetPasswordComponent,
                        },
                    ],
                },
            ],
        },
        {
            path: '**',
            redirectTo: 'not-found',
        },
    ];
}
