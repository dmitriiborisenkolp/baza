import { Injectable } from '@angular/core';
import { BazaCmsBundleEnvironment } from '../models/baza-cms-bundle-environment';

@Injectable({
    providedIn: 'root',
})
export class BazaNgCmsEnvironment<T extends BazaCmsBundleEnvironment = BazaCmsBundleEnvironment> {
    private _current: BazaCmsBundleEnvironment;

    set current(env: BazaCmsBundleEnvironment) {
        this._current = env;
    }

    get current(): BazaCmsBundleEnvironment {
        return JSON.parse(JSON.stringify(this._current));
    }
}
