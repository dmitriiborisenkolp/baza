import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess, BazaMailNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcMail } from '../../../constants/baza-nc.mail-templates';

describe('@baza-nc-api/bundle/integration-tests/001-baza-nc-mail-templates-defined.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessMail = new BazaMailNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will fetch mail templates and it will contains all mail templates defined for baza-nc-api package', async () => {
        const response = await dataAccessMail.mailTemplates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(Array.isArray(response)).toBeTruthy();

        for (const name of Object.values(BazaNcMail)) {
            expect(response.find((next) => next.name === name)).toBeDefined();
        }
    });
});
