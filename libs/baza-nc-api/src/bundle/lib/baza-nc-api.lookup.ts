import { BazaNcReportApiLookup } from '../../lib/report';

/**
 * Lookup for Baza NC API Resources
 */
export const BazaNcApiLookup = {
    libs: {
        report: BazaNcReportApiLookup,
    },
};
