import { BAZA_NORTH_CAPITAL_MODULE_CONFIG } from './module-configs/baza-north-capital.module-config';
import { BAZA_NORTH_CAPITAL_WEBHOOKS_MODULE_CONFIG } from './module-configs/baza-north-capital-webhooks.module-config';
import { BAZA_NORTH_CAPITAL_PURCHASE_FLOW_MODULE_CONFIG } from './module-configs/baza-north-capital-purchase-flow.module-config';
import { BazaNcPurchaseFlowModuleAsyncOptions } from '../../lib/pucrhase-flow';
import { NorthCapitalWebhooksModuleAsyncOptions } from '../../lib/webhook';
import { NorthCapitalApiNestjsModuleAsyncOptions } from '../../lib/transact-api';
import { BAZA_NORTH_CAPITAL_ISSUER_MODULE_CONFIG } from './module-configs/baza-north-capital-issuer.module-config';
import { BazaNcIssuerApiModuleAsyncOptions } from '../../lib/issuer';

export interface BazaNcApiBundleConfig {
    overrideModuleConfigs?: Partial<BazaNcApiBundleOverrideModuleConfig>;
}

export interface BazaNcApiBundleOverrideModuleConfig {
    transactApi: (config: BazaNcApiBundleConfig) => NorthCapitalApiNestjsModuleAsyncOptions;
    webhooks: (config: BazaNcApiBundleConfig) => NorthCapitalWebhooksModuleAsyncOptions;
    purchaseFlow: (config: BazaNcApiBundleConfig) => BazaNcPurchaseFlowModuleAsyncOptions;
    issuer: (config: BazaNcApiBundleConfig) => BazaNcIssuerApiModuleAsyncOptions;
}

export const defaultBazaNcApiBundleModuleConfigs: BazaNcApiBundleOverrideModuleConfig = {
    transactApi: BAZA_NORTH_CAPITAL_MODULE_CONFIG,
    webhooks: BAZA_NORTH_CAPITAL_WEBHOOKS_MODULE_CONFIG,
    purchaseFlow: BAZA_NORTH_CAPITAL_PURCHASE_FLOW_MODULE_CONFIG,
    issuer: BAZA_NORTH_CAPITAL_ISSUER_MODULE_CONFIG,
};

export const BAZA_NC_API_BUNDLE_CONFIG = Symbol();
