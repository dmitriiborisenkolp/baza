import { BazaNcIssuerApiModuleAsyncOptions } from '../../../lib/issuer';
import { EnvService } from '@scaliolabs/baza-core-api';
import { NorthCapitalApiEnvironments } from '@scaliolabs/baza-nc-shared';

export const BAZA_NORTH_CAPITAL_ISSUER_MODULE_CONFIG: () => BazaNcIssuerApiModuleAsyncOptions = () => ({
    injects: [EnvService],
    useFactory: async (env: EnvService) => {
        const issuerId = env.getAsString<NorthCapitalApiEnvironments.IssuerEnvironment>('BAZA_NORTH_CAPITAL_ISSUER_ID', {
            defaultValue: undefined,
        });

        return {
            useStaticIssuerId: issuerId,
        };
    },
});
