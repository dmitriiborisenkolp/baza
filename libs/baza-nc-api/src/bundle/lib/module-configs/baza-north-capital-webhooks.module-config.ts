import { EnvService } from '@scaliolabs/baza-core-api';
import { BazaNcApiBundleConfig } from '../baza-nc-api-bundle.config';
import { NorthCapitalWebhooksModuleAsyncOptions } from '../../../lib/webhook';
import { NorthCapitalApiEnvironments } from '@scaliolabs/baza-nc-shared';

export const BAZA_NORTH_CAPITAL_WEBHOOKS_MODULE_CONFIG: (config: BazaNcApiBundleConfig) => NorthCapitalWebhooksModuleAsyncOptions = () => ({
    injects: [EnvService],
    useFactory: async (env: EnvService<NorthCapitalApiEnvironments.WebhookEnvironment>) => ({
        webhookCatcherKey: env.getAsString('BAZA_NORTH_CAPITAL_WEBHOOK_KEY'),
        debug: env.getAsBoolean('BAZA_NORTH_CAPITAL_WEBHOOK_DEBUG', {
            defaultValue: false,
        }),
        kafkaPipe: {
            read: env.getAsBoolean('BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_READ', { defaultValue: false }),
            send: env.getAsBoolean('BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND', { defaultValue: false }),
        },
        encryptionKey: env.getAsString('BAZA_NORTH_CAPITAL_WEBHOOK_ENCRYPTION_KEY', { defaultValue: undefined }),
    }),
});
