import { EnvService } from '@scaliolabs/baza-core-api';
import { BazaNcApiBundleConfig } from '../baza-nc-api-bundle.config';
import { NorthCapitalApiNestjsModuleAsyncOptions } from '../../../lib/transact-api';

export const BAZA_NORTH_CAPITAL_MODULE_CONFIG: (config: BazaNcApiBundleConfig) => NorthCapitalApiNestjsModuleAsyncOptions = () => ({
    injects: [EnvService],
    useFactory: async(env: EnvService) => ({
        baseUrl: env.getAsString('BAZA_NORTH_CAPITAL_BASE_URL'),
        credentials: {
            clientID: env.getAsString('BAZA_NORTH_CAPITAL_CLIENT_ID'),
            developerAPIKey: env.getAsString('BAZA_NORTH_CAPITAL_DEVELOPER_API_KEY'),
        },
        debug: env.getAsBoolean('BAZA_NORTH_CAPITAL_DEBUG', {
            defaultValue: false,
        }),
        enableNestjsLog: env.getAsBoolean('BAZA_NORTH_CAPITAL_DEBUG_NESTJS_LOG', {
            defaultValue: false,
        }),
    }),
});
