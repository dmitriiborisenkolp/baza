import { EnvService } from '@scaliolabs/baza-core-api';
import { NorthCapitalApiEnvironments } from '@scaliolabs/baza-nc-shared';

import { BazaNcApiBundleConfig } from '../baza-nc-api-bundle.config';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaNcPurchaseFlowModuleAsyncOptions } from '../../../lib/pucrhase-flow';

export const BAZA_NORTH_CAPITAL_PURCHASE_FLOW_MODULE_CONFIG: (config: BazaNcApiBundleConfig) => BazaNcPurchaseFlowModuleAsyncOptions = () => ({
    injects: [EnvService],
    useFactory: async(env: EnvService) => ({
        tradesTTLSec: env.getAsNumeric<NorthCapitalApiEnvironments.ApiEnvironment>('BAZA_NORTH_CAPITAL_PURCHASE_FLOW_TTL'),
        disableDocuSignForEnvironments: [
            BazaEnvironments.E2e,
        ],
    }),
});
