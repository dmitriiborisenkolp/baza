import { Global, Module } from '@nestjs/common';
import { BazaNcAccountVerificationE2eModule } from '../../lib/account-verification';
import { BazaNcOfferingE2eModule } from '../../lib/offering';
import { BazaNcTaxDocumentsE2eModule } from '../../lib/tax-document';
import { BazaNcDividendE2eModule } from '../../lib/dividend';
import { BazaNcBankAccountsE2eModule } from '../../lib/bank-accounts';

const E2E_MODULES = [
    BazaNcAccountVerificationE2eModule,
    BazaNcOfferingE2eModule,
    BazaNcTaxDocumentsE2eModule,
    BazaNcDividendE2eModule,
    BazaNcBankAccountsE2eModule,
];

@Global()
@Module({
    imports: E2E_MODULES,
    exports: E2E_MODULES,
})
export class BazaNcApiE2eModule {}
