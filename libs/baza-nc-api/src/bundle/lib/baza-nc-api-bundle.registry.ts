import { BazaEnvironments, RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';
import { bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';

/**
 * TODO: Please move resources to libs/ of baza-nc-api
 */
export const bazaNcApiBundleRegistry: RegistrySchema = {
    bazaNc: {
        disableDocuSign: {
            name: 'Disable DocuSign integration',
            type: RegistryType.Boolean,
            hiddenFromList: false,
            public: true,
            defaults: false,
            forEnvironments: [BazaEnvironments.Stage, BazaEnvironments.Test, BazaEnvironments.Local],
        },
        maxACHTransferAmount: {
            name: 'Max ACH Transfer Amount (North Capital)',
            type: RegistryType.Double,
            hiddenFromList: false,
            public: true,
            defaults: 100000,
        },
        maxCreditCardTransferAmount: {
            name: 'Max Credit Card Amount (North Capital)',
            type: RegistryType.Double,
            hiddenFromList: false,
            public: true,
            defaults: 5000,
        },
        maxDwollaTransferAmount: {
            name: 'Max Account Balance Transfer Amount (Dwolla)',
            type: RegistryType.Double,
            hiddenFromList: !bazaNcApiConfig().withDwollaFeatures,
            public: true,
            defaults: 1000000,
        },
        creditCardFee: {
            name: 'Credit Card Payment Fee (North Capital)',
            type: RegistryType.Double,
            hiddenFromList: false,
            public: true,
            defaults: 3.15,
        },
        enableDividendsSuccessfulEmails: {
            name: 'Enable Email Notifications to Users for Successful Dividend payments',
            type: RegistryType.Boolean,
            hiddenFromList: false,
            public: false,
            defaults: true,
        },
        dividendsSuccessfulSubject: {
            name: 'Subject for Email Notifications about Successful Dividend Payments',
            type: RegistryType.String,
            hiddenFromList: false,
            public: false,
            defaults: 'You have successfully received {{ amount }} USD as dividends from your investment!',
        },
        dividendBatchAdminEmailNotifications: {
            name: 'Daily Notifications on Transfer Initiated, Successful Payment and Failed Dividend Payments',
            type: RegistryType.Boolean,
            hiddenFromList: false,
            public: false,
            defaults: true,
        },
        daysToReleaseFailedTrades: {
            name: 'Days to wait before releasing a trade with failed transaction',
            type: RegistryType.Integer,
            hiddenFromList: !bazaNcApiConfig().enableAutoCancellingFailedTrades,
            public: false,
            defaults: 1,
        },
        individualModeDividendNotifications: {
            name: 'Send individual notifications about dividends',
            type: RegistryType.Boolean,
            hiddenFromList: false,
            public: false,
            defaults: true,
        },
        clientReportDividendNotifications: {
            name: 'Send summary reports about successful dividends published',
            type: RegistryType.Boolean,
            hiddenFromList: false,
            public: false,
            defaults: true,
        },
        offeringStatusSubject: {
            name: 'Subject for Email Notifications about Offering Status',
            type: RegistryType.String,
            hiddenFromList: !bazaNcApiConfig().enableEmailNotificationForOfferingStatus,
            public: false,
            defaults: 'The Offering {{ offeringName }} is now open',
        },
        escrowOpsEmail: {
            name: 'North Capital Escrow Ops – Email',
            type: RegistryType.Email,
            hiddenFromList: false,
            public: false,
            defaults: 'escrow-ops@northcapital.com',
        },
        escrowOpsEmailSubject: {
            name: 'North Capital Escrow Ops Email – Subject',
            type: RegistryType.String,
            hiddenFromList: false,
            public: false,
            defaults: '{{ clientName }} – Dwolla Daily Payment transactions – {{ date }}',
        },
        escrowCsvDelimiter: {
            name: 'Delimiter for CSV filters sent to North Capital Escrow Ops',
            type: RegistryType.String,
            hiddenFromList: false,
            public: false,
            defaults: ',',
        },
    },
};
