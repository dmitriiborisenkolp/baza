import { DynamicModule, Global, Module, OnModuleInit } from '@nestjs/common';
import { BazaNcApiBundleConfig, BAZA_NC_API_BUNDLE_CONFIG, defaultBazaNcApiBundleModuleConfigs } from './baza-nc-api-bundle.config';
import { BazaNcTypeormApiModule } from '../../lib/typeorm';
import { BazaNcWebhookApiModule } from '../../lib/webhook';
import { BazaNcAccountVerificationApiModule } from '../../lib/account-verification';
import { BazaNcTransactionApiModule } from '../../lib/transaction';
import { BazaNcTransactApiModule } from '../../lib/transact-api';
import { BazaNcPurchaseFlowApiModule } from '../../lib/pucrhase-flow';
import { BazaNcOfferingApiModule } from '../../lib/offering';
import { BazaNcSyncApiModule } from '../../lib/sync';
import { BazaNcInvestorAccountApiModule } from '../../lib/investor-account';
import { BazaNcTaxDocumentsApiModule } from '../../lib/tax-document';
import { BazaNcDocusignApiModule } from '../../lib/docusign';
import { BazaNcE2eApiModule } from '../../lib/e2e';

import { BazaNcDividendApiModule } from '../../lib/dividend';
import { BazaNcDwollaApiModule } from '../../lib/dwolla';
import { BazaNcBankAccountsApiModule } from '../../lib/bank-accounts';
import { BazaNcWithdrawApiModule } from '../../lib/withdraw';
import { BazaNcCreditCardApiModule } from '../../lib/credit-card';
import { BazaNcAchApiModule } from '../../lib/ach';
import { BazaNcBootstrapApiModule } from '../../lib/bootstrap';
import { BazaNcTransferApiModule } from '../../lib/transfer';
import { BazaNcReportApiModule } from '../../lib/report';
import { BazaNcOperationApiModule } from '../../lib/operation';
import { BazaNcIssuerApiModule } from '../../lib/issuer';

@Module({})
export class BazaNcApiBundleModule {
    static forRootAsync(config: BazaNcApiBundleConfig): DynamicModule {
        const moduleConfigs = {
            ...defaultBazaNcApiBundleModuleConfigs,
            ...config.overrideModuleConfigs,
        };

        const NC_MODULES = [
            BazaNcTypeormApiModule,
            BazaNcInvestorAccountApiModule,
            BazaNcTransactApiModule.forRootAsync(moduleConfigs.transactApi(config)),
            BazaNcPurchaseFlowApiModule.forRootAsync(moduleConfigs.purchaseFlow(config)),
            BazaNcWebhookApiModule.forRootAsync(moduleConfigs.webhooks(config)),
            BazaNcIssuerApiModule.forRootAsync(moduleConfigs.issuer(config)),
            BazaNcOfferingApiModule,
            BazaNcAccountVerificationApiModule,
            BazaNcTransactionApiModule,
            BazaNcSyncApiModule,
            BazaNcTaxDocumentsApiModule,
            BazaNcDocusignApiModule,
            BazaNcE2eApiModule,
            BazaNcDividendApiModule,
            BazaNcDwollaApiModule,
            BazaNcBankAccountsApiModule,
            BazaNcWithdrawApiModule,
            BazaNcTransferApiModule,
            BazaNcAchApiModule,
            BazaNcCreditCardApiModule,
            BazaNcBootstrapApiModule,
            BazaNcReportApiModule,
            BazaNcOperationApiModule,
        ];

        return {
            module: BazaNcApiBundleGlobalModule,
            imports: [...NC_MODULES],
            exports: [BazaNcTypeormApiModule, BazaNcDividendApiModule],
            providers: [
                {
                    provide: BAZA_NC_API_BUNDLE_CONFIG,
                    useValue: config,
                },
            ],
        };
    }
}

@Global()
@Module({})
class BazaNcApiBundleGlobalModule {}
