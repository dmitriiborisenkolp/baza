import { Injectable, PipeTransform } from '@nestjs/common';
import { isUSACountry, PersonalInformationApplyRequest } from '@scaliolabs/baza-nc-shared';
import { isValidUsZipCodeUtil } from '@scaliolabs/baza-core-shared';
import { BazaNcZipcodeDoesNotHave5NumericDigitsException } from '../exceptions/baza-nc-zipcode-does-not-have-5-numeric-digits.exception';

@Injectable()
export class ZipCodeValidationForUsCitizenPipe implements PipeTransform {
    async transform(value: PersonalInformationApplyRequest) {
        if (value) {
            const { residentialCountry, residentialZipCode } = value;
            if (residentialZipCode && isUSACountry(residentialCountry) && !isValidUsZipCodeUtil(residentialZipCode)) {
                throw new BazaNcZipcodeDoesNotHave5NumericDigitsException();
            }
        }
        return value;
    }
}
