import { forwardRef, Global, Module } from '@nestjs/common';
import { BazaNcAccountVerificationService } from './baza-nc-account-verification.service';
import { BazaNcAccountVerificationStatusService } from './services/baza-nc-account-verification-status.service';
import { BazaNcAccountVerificationInvestorProfileService } from './services/baza-nc-account-verification-investor-profile.service';
import { BazaNcAccountVerificationPersonalInformationService } from './services/baza-nc-account-verification-personal-information.service';
import { BazaNcAccountVerificationRequestDocumentsService } from './services/baza-nc-account-verification-request-documents.service';
import { BazaNcAccountVerificationFormResourcesService } from './services/baza-nc-account-verification-form-resources.service';
import { BazaNcAccountVerificationAccountSetupService } from './services/baza-nc-account-verification-account-setup.service';
import { BazaNcAccountVerificationRequestSsnDocumentsService } from './services/baza-nc-account-verification-request-ssn-documents.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaNcAccountVerificationKycAmlService } from './services/baza-nc-account-verification-kyc-aml.service';
import { BazaNcAccountVerificationSessionService } from './services/baza-nc-account-verification-session.service';
import { BazaNcAccountVerificationKycLogService } from './services/baza-nc-account-verification-kyc-log.service';
import { BazaNcAccountVerificationController } from './controllers/baza-nc-account-verification.controller';
import { BazaNcAccountVerificationCmsController } from './controllers/baza-nc-account-verification-cms.controller';
import { BazaAwsModule } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationKycLogMapper } from './mappers/baza-nc-account-verification-kyc-log.mapper';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaE2eFixturesApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { KycStatusEventHandler } from './events-handlers/kyc-status.event-handler';
import { AccountVerificationKycStatusUpdateEventHandler } from './events-handlers/account-verification-kyc-status-update.event-handler';
import { AccountEmailChangedEventHandler } from './events-handlers/account-email-changed.event-handler';
import { BazaDwollaApiModule } from '@scaliolabs/baza-dwolla-api';
import { BazaNcDwollaApiModule } from '../../dwolla';
import { BazaNcInvestorAccountApiModule } from '../../investor-account';

const services = [
    BazaNcAccountVerificationService,
    BazaNcAccountVerificationAccountSetupService,
    BazaNcAccountVerificationKycAmlService,
    BazaNcAccountVerificationFormResourcesService,
    BazaNcAccountVerificationStatusService,
    BazaNcAccountVerificationPersonalInformationService,
    BazaNcAccountVerificationRequestSsnDocumentsService,
    BazaNcAccountVerificationRequestDocumentsService,
    BazaNcAccountVerificationInvestorProfileService,
    BazaNcAccountVerificationSessionService,
    BazaNcAccountVerificationKycLogService,
    BazaNcAccountVerificationKycLogMapper,
];

const eventHandlers = [KycStatusEventHandler, AccountVerificationKycStatusUpdateEventHandler, AccountEmailChangedEventHandler];

@Global()
@Module({
    controllers: [BazaNcAccountVerificationController, BazaNcAccountVerificationCmsController],
    imports: [
        CqrsModule,
        TypeOrmModule,
        BazaAwsModule,
        BazaNcTypeormApiModule,
        BazaCrudApiModule,
        forwardRef(() => BazaNcInvestorAccountApiModule),
        BazaE2eFixturesApiModule,
        BazaDwollaApiModule,
        BazaNcDwollaApiModule,
    ],
    providers: [...services, ...eventHandlers],
    exports: [...services],
})
export class BazaNcAccountVerificationApiModule {}
