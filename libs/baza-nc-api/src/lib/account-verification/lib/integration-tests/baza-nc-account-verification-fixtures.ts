export enum BazaNcAccountVerificationFixtures {
    BazaNcAccountVerificationE2eUserFixture = 'BazaNcAccountVerificationE2eUserFixture',
    BazaNcAccountVerificationE2eUser2Fixture = 'BazaNcAccountVerificationE2e2UserFixture',
    BazaNcAccountVerificationE2eUserFullFixture = 'BazaNcAccountVerificationE2eUserFullFixture',
    BazaNcAccountVerificationNoSsnE2eUserFixture = 'BazaNcAccountVerificationNoSsnE2eUserFixture',
    BazaNcAccountVerificationE2ePuertoRicoUserFixture = 'BazaNcAccountVerificationE2ePuertoRicoUserFixture',
}
