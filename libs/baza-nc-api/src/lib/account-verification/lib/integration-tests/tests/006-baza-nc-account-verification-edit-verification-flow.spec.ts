import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';
import * as fs from 'fs';
import * as path from 'path';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/006-baza-nc-account-verification-edit-verification-flow.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationNoSsnE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will finish account verification', async () => {
        const status = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.currentStep).toBe(AccountVerificationStep.RequestSSNDocument);

        const uploadSsnDocumentResponse = await dataAccessAccountVerification.uploadPersonalInformationSSNDocument(
            fs.createReadStream(path.join(__dirname, '../files/ssn.jpg')),
        );

        expect(isBazaErrorResponse(uploadSsnDocumentResponse)).toBeFalsy();
        expect(uploadSsnDocumentResponse.personalInformation.ssnDocumentFileName).not.toBeUndefined();
        expect(uploadSsnDocumentResponse.personalInformation.ssnDocumentId).not.toBeUndefined();

        const investorProfileResponse = await dataAccessAccountVerification.applyInvestorProfile({
            currentAnnualHouseholdIncome: 10000,
            netWorth: 150000,
            isAccreditedInvestor: true,
            isAssociatedWithFINRA: true,
        });

        expect(isBazaErrorResponse(investorProfileResponse)).toBeFalsy();

        const statusResponse = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(statusResponse)).toBeFalsy();
        expect(statusResponse.currentStep).toBe(AccountVerificationStep.Completed);
    });

    it('will still require SSN document to upload', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: false,
        });

        expect(response.currentStep).toBe(AccountVerificationStep.Completed);

        const statusResponse = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(statusResponse)).toBeFalsy();
        expect(statusResponse.currentStep).toBe(AccountVerificationStep.Completed);
    });

    it('will still required SSN document to upload after 3 seconds', async () => {
        await asyncExpect(
            async () => {
                const statusResponse = await dataAccessAccountVerification.index();

                expect(isBazaErrorResponse(statusResponse)).toBeFalsy();
                expect(statusResponse.currentStep).toBe(AccountVerificationStep.Completed);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will allow to bypass account verification with previously uploaded ssn document', async () => {
        const useResponse = await dataAccessAccountVerification.usePreviouslyUploadedDocument();

        expect(isBazaErrorResponse(useResponse)).toBeFalsy();
        expect(useResponse.currentStep).toBe(AccountVerificationStep.Completed);

        const statusResponse = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(statusResponse)).toBeFalsy();
        expect(statusResponse.currentStep).toBe(AccountVerificationStep.Completed);
    });
});
