import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';
import * as fs from 'fs';
import * as path from 'path';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

xdescribe('@scaliolabs/baza-nc-api/account-verification/008-baza-nc-account-verification-cmnw-1696.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will set phone number and move to LegalName step', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2282201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.LegalName);
    });

    it('will set Legal Name and move to DOB step', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            firstName: 'WILLIE',
            lastName: 'SMITH',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.DateOfBirth);
    });

    it('will set DOB and move to SSN step', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: '02-28-1975',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.SSN);
    });

    it('will set NO SSN and move to RequestSSNDocument step', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.RequestSSNDocument);
    });

    it('will move to Citizenship step after uploading document', async () => {
        const uploadSsnDocumentResponse = await dataAccessAccountVerification.uploadPersonalInformationSSNDocument(
            fs.createReadStream(path.join(__dirname, '../files/ssn.jpg')),
        );

        expect(isBazaErrorResponse(uploadSsnDocumentResponse)).toBeFalsy();
        expect(uploadSsnDocumentResponse.currentStep).toBe(AccountVerificationStep.Citizenship);
        expect(uploadSsnDocumentResponse.personalInformation.ssnDocumentFileName).not.toBeUndefined();
        expect(uploadSsnDocumentResponse.personalInformation.ssnDocumentId).not.toBeUndefined();
    });

    it('will still be on Citizenship step', async () => {
        const status = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.currentStep).toBe(AccountVerificationStep.Citizenship);
    });

    it('will still be on Citizenship step after 3 sec timeout', async () => {
        await asyncExpect(
            async () => {
                const status = await dataAccessAccountVerification.index();

                expect(isBazaErrorResponse(status)).toBeFalsy();
                expect(status.currentStep).toBe(AccountVerificationStep.Citizenship);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will set Citizenship and move to RedisentialAddress step', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            citizenship: Domicile.USCitizen,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.ResidentialAddress);
    });

    it('will set ResidentialAddress and move to InvestorProfile step', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            residentialStreetAddress1: '145588 LENINA STREET',
            residentialStreetAddress2: '12',
            residentialCity: 'MOSCOW',
            residentialState: 'MOSCOW',
            residentialZipCode: '129000',
            residentialCountry: 'Russia',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.kycStatus).toBe(KYCStatus.Disapproved);
        expect(response.currentStep).toBe(AccountVerificationStep.InvestorProfile);
    });

    it('will be on InvestorProfile step', async () => {
        const status = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.currentStep).toBe(AccountVerificationStep.InvestorProfile);
    });

    it('will be on InvestorProfile step after 3 sec timeout', async () => {
        await asyncExpect(
            async () => {
                const status = await dataAccessAccountVerification.index();

                expect(isBazaErrorResponse(status)).toBeFalsy();
                expect(status.currentStep).toBe(AccountVerificationStep.InvestorProfile);
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
