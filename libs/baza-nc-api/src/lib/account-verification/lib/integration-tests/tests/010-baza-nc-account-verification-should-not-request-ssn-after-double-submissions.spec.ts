import 'reflect-metadata';
import * as fs from 'fs';
import * as path from 'path';
import { Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaE2eFixturesNodeAccess, BazaE2eNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/010-baza-nc-account-verification-should-not-request-ssn-after-double-submissions.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('correctly setup personal information', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'Foo',
            lastName: 'Bar',
            dateOfBirth: '02-28-1975',
            hasSsn: false,
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: 'Street Address 1',
            residentialStreetAddress2: 'Street Address 2',
            residentialCity: 'ATLANTA',
            residentialState: 'TX',
            residentialZipCode: '40044',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.RequestSSNDocument);
        expect(response.kycStatus).toBe(KYCStatus.Disapproved);
        expect(response.personalInformation.hasSsn).toBe(false);
    });

    it('will successfully upload SSN document', async () => {
        const uploadSsnDocumentResponse = await dataAccessAccountVerification.uploadPersonalInformationSSNDocument(
            fs.createReadStream(path.join(__dirname, '../files/ssn.jpg')),
        );

        expect(isBazaErrorResponse(uploadSsnDocumentResponse)).toBeFalsy();
        expect(uploadSsnDocumentResponse.currentStep).toBe(AccountVerificationStep.InvestorProfile);
        expect(uploadSsnDocumentResponse.personalInformation.ssnDocumentFileName).not.toBeUndefined();
        expect(uploadSsnDocumentResponse.personalInformation.ssnDocumentId).not.toBeUndefined();
    });

    it('will not request SSN document again after full personal information submission', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'Foo',
            lastName: 'Bar',
            dateOfBirth: '02-28-1975',
            hasSsn: false,
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: 'Street Address 1',
            residentialStreetAddress2: 'Street Address 2',
            residentialCity: 'ATLANTA',
            residentialState: 'TX',
            residentialZipCode: '40044',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.InvestorProfile);
        expect(response.personalInformation.ssnDocumentFileName).not.toBeUndefined();
        expect(response.personalInformation.ssnDocumentId).not.toBeUndefined();
    });

    it('will still have InvestorProfile step in AccountVerificationDTO later', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessAccountVerification.index();

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.currentStep).toBe(AccountVerificationStep.InvestorProfile);
                expect(response.personalInformation.ssnDocumentFileName).not.toBeUndefined();
                expect(response.personalInformation.ssnDocumentId).not.toBeUndefined();
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
