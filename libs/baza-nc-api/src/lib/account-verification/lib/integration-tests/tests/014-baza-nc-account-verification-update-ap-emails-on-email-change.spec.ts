import 'reflect-metadata';
import { BazaAccountCmsNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/014-baza-nc-account-verification-update-ap-emails-on-email-change.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let USER_ACCOUNT_ID: number;

    const NEW_ACCOUNT_EMAIL = 'new-user-email@scal.io';

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eUser();

        USER_ACCOUNT_ID = http.authResponse.jwtPayload.accountId;
    });

    it('will successfully change email of account', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.changeEmail({
            id: USER_ACCOUNT_ID,
            email: NEW_ACCOUNT_EMAIL,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will update email in NC account and NC party', async () => {
        await http.auth({
            email: NEW_ACCOUNT_EMAIL,
            password: 'e2e-user-password',
        });

        await asyncExpect(async () => {
            const response = await dataAccessAccountVerification.index();

            expect(response._debugParty.emailAddress).toBe(NEW_ACCOUNT_EMAIL);
            expect(response._debugAccount.accountDetails.email).toBe(NEW_ACCOUNT_EMAIL);
        });
    });
});
