import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { Domicile } from '@scaliolabs/baza-nc-shared';
import * as fs from 'fs';
import * as path from 'path';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

// TODO: Looks like NC auto-approve is not working anymore
xdescribe('@scaliolabs/baza-nc-api/account-verification/005-baza-nc-account-verification-cmnw-1583-password-isnt-saving-at-first-time.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will correctly upload image as SSN', async () => {
        const response = await dataAccessAccountVerification.uploadPersonalInformationSSNDocument(
            fs.createReadStream(path.join(__dirname, '../files/ssn.jpg')),
        );

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.personalInformation.ssnDocumentFileName).not.toBeUndefined();
        expect(response.personalInformation.ssnDocumentId).not.toBeUndefined();
    });

    it('will correctly create NC account/party', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JHIN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: false,
            citizenship: Domicile.NonResident,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will have uploaded document', async () => {
        const response = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully re-upload SSN file', async () => {
        const response = await dataAccessAccountVerification.uploadPersonalInformationSSNDocument(
            fs.createReadStream(path.join(__dirname, '../files/ssn2.jpg')),
        );

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.personalInformation.ssnDocumentFileName).not.toBeUndefined();
        expect(response.personalInformation.ssnDocumentId).not.toBeUndefined();
    });

    it('will have new uploaded document', async () => {
        const response = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
