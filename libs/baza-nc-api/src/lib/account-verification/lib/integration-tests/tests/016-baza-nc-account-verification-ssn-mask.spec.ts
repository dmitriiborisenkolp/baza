import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcE2eNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/016-baza-nc-account-verification-ssn-mask.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await dataAccessE2eNc.enableMaskConfig();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    afterAll(async () => {
        await dataAccessE2eNc.disableMaskConfig();
    });

    it('will correctly returns SSN after setting it up', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            ssn: '111-22-3333',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('***-**-3333');
    });

    it('will correctly returns SSN later', async () => {
        const response = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('***-**-3333');
    });

    it('will allow to set SSN with numbers only', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            ssn: '111224444',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('***-**-4444');
    });

    it('will correctly returns new SSN later', async () => {
        const response = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('***-**-4444');
    });
});
