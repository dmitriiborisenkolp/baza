import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/026-baza-nc-account-verification-flags.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display that InProgress and IsCompleted flags both are false on initial call (1st call)', async () => {
        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeFalsy();
        expect(status.isAccountVerificationCompleted).toBeFalsy();
    });

    it('will display that InProgress and IsCompleted flags both are false on initial call (2nd call)', async () => {
        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeFalsy();
        expect(status.isAccountVerificationCompleted).toBeFalsy();
    });

    it('will set First and Last name (Case: Tmp Account Verification Session)', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            firstName: 'TOM',
            lastName: 'DOE',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeTruthy();
        expect(status.isAccountVerificationCompleted).toBeFalsy();
    });

    it('will set Phone  (Case: Tmp Account Verification Session, 2nd call)', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '9959018846',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeTruthy();
        expect(status.isAccountVerificationCompleted).toBeFalsy();
    });

    it('will fill Personal Information step fully (Case: Creating NC Account and Party)', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeTruthy();
        expect(status.isAccountVerificationCompleted).toBeFalsy();
    });

    it('will fill Personal Information step fully (Case: should not be completed when Personal Information is updating after creating NC Account and Party)', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1818',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeTruthy();
        expect(status.isAccountVerificationCompleted).toBeFalsy();
    });

    it('will fill Investor Account profile (Case: Setting up Account Verification as Completed)', async () => {
        const response = await dataAccessAccountVerification.applyInvestorProfile({
            isAccreditedInvestor: false,
            isAssociatedWithFINRA: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeFalsy();
        expect(status.isAccountVerificationCompleted).toBeTruthy();
    });

    it('will update Investor Account profile once more (Case: AV is completed and flags should remain unchanged)', async () => {
        const response = await dataAccessAccountVerification.applyInvestorProfile({
            isAccreditedInvestor: true,
            isAssociatedWithFINRA: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeFalsy();
        expect(status.isAccountVerificationCompleted).toBeTruthy();
    });

    it('will update Personal Information profile once more (Case: AV is completed and flags should remain unchanged)', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            firstName: 'TOM',
            lastName: 'DOE',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const status = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(status)).toBeFalsy();

        expect(status.isAccountVerificationInProgress).toBeFalsy();
        expect(status.isAccountVerificationCompleted).toBeTruthy();
    });
});
