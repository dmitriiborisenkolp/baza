import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/002-baza-nc-account-verification-common-case-mobile-flow.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    const expectInvestorAccountNotLinkedWithNc = async () => {
        await asyncExpect(
            async () => {
                const investorAccount = await dataAccessInvestorAccount.current();

                expect(investorAccount.northCapitalAccountId).toBeNull();
                expect(investorAccount.northCapitalPartyId).toBeNull();
            },
            null,
            { intervalMillis: 1000 },
        );
    };

    const expectInvestorAccountLinkedWithNc = async () => {
        await asyncExpect(
            async () => {
                const investorAccount = await dataAccessInvestorAccount.current();

                expect(investorAccount.northCapitalAccountId).not.toBeNull();
                expect(investorAccount.northCapitalPartyId).not.toBeNull();
            },
            null,
            { intervalMillis: 1000 },
        );
    };

    const expectAccountVerificationIsNotFinished = async () => {
        await asyncExpect(
            async () => {
                const investorAccount = await dataAccessInvestorAccount.current();

                expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
                expect(investorAccount.isAccountVerificationCompleted).toBeFalsy();

                const isCompleted = await dataAccessAccountVerification.isCompleted();

                expect(isBazaErrorResponse(isCompleted)).toBeFalsy();
                expect(isCompleted.isCompleted).toBeFalsy();
            },
            null,
            { intervalMillis: 1000 },
        );
    };

    const expectAccountVerificationIsFinished = async () => {
        await asyncExpect(
            async () => {
                const investorAccount = await dataAccessInvestorAccount.current();

                expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
                expect(investorAccount.isAccountVerificationCompleted).toBeTruthy();

                const isCompleted = await dataAccessAccountVerification.isCompleted();

                expect(isBazaErrorResponse(isCompleted)).toBeFalsy();
                expect(isCompleted.isCompleted).toBeTruthy();
            },
            null,
            { intervalMillis: 1000 },
        );
    };

    const expectAccountVerificationStatus = async (status: AccountVerificationStep) => {
        await asyncExpect(
            async () => {
                const response = await dataAccessAccountVerification.index();

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.currentStep).toBe(status);
            },
            null,
            { intervalMillis: 1000 },
        );
    };

    it('correctly says that user is not finished account verification before', async () => {
        await expectAccountVerificationIsNotFinished();
        await expectInvestorAccountNotLinkedWithNc();
        await expectAccountVerificationStatus(AccountVerificationStep.PhoneNumber);
    });

    it('[MOBILE FLOW] correctly setup personal information – PhoneNumber', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.LegalName);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('1');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('14');
    });

    it('[MOBILE FLOW] correctly setup personal information – LegalName', async () => {
        await expectAccountVerificationIsNotFinished();
        await expectInvestorAccountNotLinkedWithNc();

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            firstName: 'JOHN',
            lastName: 'SMITH',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.DateOfBirth);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('1');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('14');
        expect(response.personalInformation.firstName).toBe('JOHN');
        expect(response.personalInformation.lastName).toBe('SMITH');
    });

    it('[MOBILE FLOW] correctly setup personal information – DateOfBirth', async () => {
        await expectAccountVerificationIsNotFinished();
        await expectInvestorAccountNotLinkedWithNc();

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: '02-28-1975',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.SSN);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('1');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('14');
        expect(response.personalInformation.firstName).toBe('JOHN');
        expect(response.personalInformation.lastName).toBe('SMITH');
        expect(response.personalInformation.dateOfBirth).toBe('02-28-1975');
    });

    it('[MOBILE FLOW] correctly setup personal information – SSN', async () => {
        await expectAccountVerificationIsNotFinished();
        await expectInvestorAccountNotLinkedWithNc();

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: true,
            ssn: '112-22-3333',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.Citizenship);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('1');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('14');
        expect(response.personalInformation.firstName).toBe('JOHN');
        expect(response.personalInformation.lastName).toBe('SMITH');
        expect(response.personalInformation.dateOfBirth).toBe('02-28-1975');
        expect(response.personalInformation.hasSsn).toBe(true);
        expect(response.personalInformation.ssn).toBe('112-22-3333');
    });

    it('[MOBILE FLOW] correctly setup personal information – Citizenship', async () => {
        await expectAccountVerificationIsNotFinished();
        await expectInvestorAccountNotLinkedWithNc();

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            citizenship: Domicile.USCitizen,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.ResidentialAddress);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('1');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('14');
        expect(response.personalInformation.firstName).toBe('JOHN');
        expect(response.personalInformation.lastName).toBe('SMITH');
        expect(response.personalInformation.dateOfBirth).toBe('02-28-1975');
        expect(response.personalInformation.hasSsn).toBe(true);
        expect(response.personalInformation.ssn).toBe('112-22-3333');
        expect(response.personalInformation.citizenship).toBe(Domicile.USCitizen);
    });

    it('[MOBILE FLOW] correctly setup personal information – ResidentialAddress', async () => {
        await expectAccountVerificationIsNotFinished();
        await expectInvestorAccountNotLinkedWithNc();

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.InvestorProfile);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('1');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('14');
        expect(response.personalInformation.firstName).toBe('JOHN');
        expect(response.personalInformation.lastName).toBe('SMITH');
        expect(response.personalInformation.dateOfBirth).toBe('02-28-1975');
        expect(response.personalInformation.hasSsn).toBe(true);
        expect(response.personalInformation.ssn).toBe('112-22-3333');
        expect(response.personalInformation.citizenship).toBe(Domicile.USCitizen);
        expect(response.personalInformation.residentialStreetAddress1).toBe('222333 PEACHTREE PLACE');
        expect(response.personalInformation.residentialStreetAddress2).toBe('1414');
        expect(response.personalInformation.residentialCity).toBe('ATLANTA');
        expect(response.personalInformation.residentialState).toBe('GA');
        expect(response.personalInformation.residentialZipCode).toBe('30033');
        expect(response.personalInformation.residentialCountry).toBe('USA');
    });

    it('[MOBILE FLOW] correctly setup personal information – will have InvestorProfile status after applying personal information and there will be NcAccountId/NcPartyId set', async () => {
        await expectAccountVerificationIsNotFinished();
        await expectInvestorAccountLinkedWithNc();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
        expect(investorAccount.isAccountVerificationCompleted).toBeFalsy();
        expect(investorAccount.northCapitalAccountId).not.toBeNull();
        expect(investorAccount.northCapitalPartyId).not.toBeNull();
    });

    it('[MOBILE FLOW] correctly setup personal information – will have AutoApproved status', async () => {
        await expectAccountVerificationIsNotFinished();

        const response = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.kycStatus).toBe(KYCStatus.AutoApproved);
    });

    it('[MOBILE FLOW] correctly setup investor profile', async () => {
        const response = await dataAccessAccountVerification.applyInvestorProfile({
            netWorth: 250,
            currentAnnualHouseholdIncome: 100000,
            isAccreditedInvestor: true,
            isAssociatedWithFINRA: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.kycStatus).toBe(KYCStatus.AutoApproved);
        expect(response.currentStep).toBe(AccountVerificationStep.Completed);

        expect(response.investorProfile.netWorth).toBe(250);
        expect(response.investorProfile.currentAnnualHouseholdIncome).toBe(100000);
        expect(response.investorProfile.isAccreditedInvestor).toBe(true);
        expect(response.investorProfile.isAssociatedWithFINRA).toBe(true);
    });

    it('[MOBILE FLOW] Correctly says after that account verification is completed', async () => {
        await expectAccountVerificationIsFinished();
    });
});
