import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/007-baza-nc-account-verification-edit-verification-flow-had-ssn.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will finish account verification', async () => {
        const investorProfileResponse = await dataAccessAccountVerification.applyInvestorProfile({
            currentAnnualHouseholdIncome: 10000,
            netWorth: 150000,
            isAccreditedInvestor: true,
            isAssociatedWithFINRA: true,
        });

        expect(isBazaErrorResponse(investorProfileResponse)).toBeFalsy();

        const statusResponse = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(statusResponse)).toBeFalsy();
        expect(statusResponse.currentStep).toBe(AccountVerificationStep.Completed);
    });

    it('will force user to keen on request SSN document step after setting update as has no ssn', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: false,
        });

        expect(response.currentStep).toBe(AccountVerificationStep.RequestSSNDocument);

        const statusResponse = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(statusResponse)).toBeFalsy();
        expect(statusResponse.currentStep).toBe(AccountVerificationStep.RequestSSNDocument);
    });
});
