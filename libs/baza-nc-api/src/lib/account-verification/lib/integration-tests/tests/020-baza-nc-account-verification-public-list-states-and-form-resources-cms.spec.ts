import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationCmsNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/020-baza-nc-account-verification-public-list-states-and-form-resources-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will allow to use formResources as non-authenticated user', async () => {
        const response = await dataAccessAccountVerification.formResources();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.listCountries.length > 0).toBeTruthy();
        expect(response.primCountryStates.length > 0).toBeTruthy();
    });
});
