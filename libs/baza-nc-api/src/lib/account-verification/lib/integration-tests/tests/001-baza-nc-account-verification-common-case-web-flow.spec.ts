import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountVerificationStep, BazaNcApiErrorCodes } from '@scaliolabs/baza-nc-shared';
import { Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/001-baza-nc-account-verification-common-case-web-flow.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    const expectAccountVerificationIsNotFinished = async () => {
        await asyncExpect(
            async () => {
                const investorAccount = await dataAccessInvestorAccount.current();

                expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
                expect(investorAccount.isAccountVerificationCompleted).toBeFalsy();

                const isCompleted = await dataAccessAccountVerification.isCompleted();

                expect(isBazaErrorResponse(isCompleted)).toBeFalsy();
                expect(isCompleted.isCompleted).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    };

    const expectAccountVerificationIsFinished = async () => {
        await asyncExpect(
            async () => {
                const investorAccount = await dataAccessInvestorAccount.current();

                expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
                expect(investorAccount.isAccountVerificationCompleted).toBeTruthy();

                const isCompleted = await dataAccessAccountVerification.isCompleted();

                expect(isBazaErrorResponse(isCompleted)).toBeFalsy();
                expect(isCompleted.isCompleted).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    };

    const expectAccountVerificationStatus = async (status: AccountVerificationStep) => {
        await asyncExpect(
            async () => {
                const response = await dataAccessAccountVerification.index();

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.currentStep).toBe(status);
            },
            null,
            { intervalMillis: 2000 },
        );
    };

    it('correctly says that user is not finished account verification before', async () => {
        await asyncExpect(
            async () => {
                await expectAccountVerificationIsNotFinished();
                await expectAccountVerificationStatus(AccountVerificationStep.PhoneNumber);

                const investorAccount = await dataAccessInvestorAccount.current();

                expect(investorAccount.northCapitalAccountId).toBeNull();
                expect(investorAccount.northCapitalPartyId).toBeNull();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('[WEB FLOW] correctly setup personal information', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.InvestorProfile);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('1');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('14');
        expect(response.personalInformation.firstName).toBe('JOHN');
        expect(response.personalInformation.lastName).toBe('SMITH');
        expect(response.personalInformation.dateOfBirth).toBe('02-28-1975');
        expect(response.personalInformation.hasSsn).toBe(true);
        expect(response.personalInformation.ssn).toBe('112-22-3333');
        expect(response.personalInformation.citizenship).toBe(Domicile.USCitizen);
        expect(response.personalInformation.residentialStreetAddress1).toBe('222333 PEACHTREE PLACE');
        expect(response.personalInformation.residentialStreetAddress2).toBe('1414');
        expect(response.personalInformation.residentialCity).toBe('ATLANTA');
        expect(response.personalInformation.residentialState).toBe('GA');
        expect(response.personalInformation.residentialZipCode).toBe('30033');
        expect(response.personalInformation.residentialCountry).toBe('USA');
    });

    it('[WEB FLOW] will not allow to use special characters', async () => {
        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            firstName: 'JÖHN',
            lastName: 'SMîTH',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcApiErrorCodes.BazaNcCharacterIsNotAllowed);
    });

    it('[WEB FLOW] correctly setup personal information – will have InvestorProfile status after applying personal information and there will be NcAccountId/NcPartyId set', async () => {
        await expectAccountVerificationIsNotFinished();
        await expectAccountVerificationStatus(AccountVerificationStep.InvestorProfile);

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
        expect(investorAccount.isAccountVerificationCompleted).toBeFalsy();
        expect(investorAccount.northCapitalAccountId).not.toBeNull();
        expect(investorAccount.northCapitalPartyId).not.toBeNull();
    });

    it('[WEB FLOW] correctly setup personal information – will have AutoApproved status', async () => {
        await expectAccountVerificationIsNotFinished();

        const response = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.kycStatus).toBe(KYCStatus.AutoApproved);
    });

    it('[WEB FLOW] correctly setup investor profile', async () => {
        await expectAccountVerificationIsNotFinished();

        const response = await dataAccessAccountVerification.applyInvestorProfile({
            netWorth: 250,
            currentAnnualHouseholdIncome: 100000,
            isAccreditedInvestor: true,
            isAssociatedWithFINRA: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.kycStatus).toBe(KYCStatus.AutoApproved);
        expect(response.currentStep).toBe(AccountVerificationStep.Completed);

        expect(response.investorProfile.netWorth).toBe(250);
        expect(response.investorProfile.currentAnnualHouseholdIncome).toBe(100000);
        expect(response.investorProfile.isAccreditedInvestor).toBe(true);
        expect(response.investorProfile.isAssociatedWithFINRA).toBe(true);
    });

    it('[WEB FLOW] Correctly says after that account verification is completed', async () => {
        await expectAccountVerificationIsFinished();
    });
});
