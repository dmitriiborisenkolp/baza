import 'reflect-metadata';
import { BazaAccountCmsNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { AccountRole, BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';
import { BazaNcBankAccountFixtures } from '../../../../bank-accounts/lib/integration-tests/baza-nc-bank-account.fixtures';
import { Domicile } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

/**
 * NC somewhere near Mar 2023 introduced changes around `updateParty` which leads for us to possible losing SSN
 * numbers of investors. This test case simulates issues with losing SSN numbers
 * Do not disable this test case!
 */
describe('@scaliolabs/baza-nc-api/account-verification/025-baza-nc-account-verification-baza-1915.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let USER_EMAIL: string;

    const authUniqueUser = async () => {
        await http.auth({
            email: USER_EMAIL,
            password: 'e2e-user-password',
        });
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcBankAccountFixtures.BazaNcBankAccountsCaseB,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
        });

        USER_EMAIL = accounts.items[0].email;
    });

    beforeEach(async () => {
        await authUniqueUser();
    });

    it('will pass first step of account verification', async () => {
        const response = await dataAccess.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[1] will display that SSN number and Prim Address 2 are represented', async () => {
        const response = await dataAccess.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('112-22-3333');
        expect(response.personalInformation.residentialStreetAddress2).toBe('1414');
    });

    it('will change phone country code', async () => {
        const response = await dataAccess.applyPersonalInformation({
            phoneCountryCode: '7',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[2] will display that SSN number and Prim Address 2 are represented', async () => {
        const response = await dataAccess.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('112-22-3333');
        expect(response.personalInformation.residentialStreetAddress2).toBe('1414');
    });

    it('will removes Prim Address 2', async () => {
        const response = await dataAccess.applyPersonalInformation({
            residentialStreetAddress2: '',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[1] will display that SSN number is represented and Prim Address 2 is removed', async () => {
        const response = await dataAccess.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('112-22-3333');
        expect(response.personalInformation.residentialStreetAddress2).not.toBeDefined();
    });

    it('will attempts to remove SSN number', async () => {
        const response = await dataAccess.applyPersonalInformation({
            ssn: '',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[2] will display that SSN number is represented and Prim Address 2 is removed', async () => {
        const response = await dataAccess.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('112-22-3333');
        expect(response.personalInformation.residentialStreetAddress2).not.toBeDefined();
    });

    it('will change SSN number, updates custom field and set Prim Address 2', async () => {
        const response = await dataAccess.applyPersonalInformation({
            residentialStreetAddress2: 'L2 1515',
            phoneCountryCode: '9',
            ssn: '22-333-4444',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that all fields persists after changes', async () => {
        const response = await dataAccess.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.ssn).toBe('22-333-4444');
        expect(response.personalInformation.phoneCountryCode).toBe('9');
        expect(response.personalInformation.residentialStreetAddress2).toBe('L2 1515');
    });
});
