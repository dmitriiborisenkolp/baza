import 'reflect-metadata';
import * as fs from 'fs';
import * as path from 'path';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcE2eNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { AccountVerificationStep, Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);
const LATEST_ADDITIONAL_DOCUMENT = {
    additionalDocumentId: undefined,
    additionalDocumentFileName: undefined,
};

describe('@scaliolabs/baza-nc-api/account-verification/017-baza-nc-account-verification-additional-document.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('setup invalid personal information with KYC failure', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            citizenship: Domicile.NonResident,
            dateOfBirth: '07-22-1996',
            firstName: 'scalio',
            hasSsn: true,
            lastName: 'user',
            phone: '1111111',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '840',
            residentialCity: 'test',
            residentialCountry: 'United States',
            residentialState: 'test',
            residentialStreetAddress1: 'test',
            residentialStreetAddress2: 'test',
            residentialZipCode: '12345',
            ssn: '123-45-67899',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.RequestDocuments);
        expect(response.kycStatus).toBe(KYCStatus.Disapproved);
    });

    it('will successfully upload additional document', async () => {
        const uploadSsnDocumentResponse = await dataAccessAccountVerification.uploadPersonalInformationDocument(
            fs.createReadStream(path.join(__dirname, '../files/ssn.jpg')),
        );

        expect(isBazaErrorResponse(uploadSsnDocumentResponse)).toBeFalsy();
        expect(uploadSsnDocumentResponse.currentStep).toBe(AccountVerificationStep.InvestorProfile);
        expect(uploadSsnDocumentResponse.personalInformation.additionalDocumentFileName).not.toBeNull();
        expect(uploadSsnDocumentResponse.personalInformation.additionalDocumentFileName).toMatch('ssn');
        expect(uploadSsnDocumentResponse.personalInformation.additionalDocumentId).not.toBeNull();

        Object.assign(LATEST_ADDITIONAL_DOCUMENT, uploadSsnDocumentResponse.personalInformation);
    });

    it('verify additional document uploaded correctly', async () => {
        const response = await dataAccessAccountVerification.index();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.InvestorProfile);
        expect(response.personalInformation.additionalDocumentFileName).toEqual(LATEST_ADDITIONAL_DOCUMENT.additionalDocumentFileName);
        expect(response.personalInformation.additionalDocumentId).toEqual(LATEST_ADDITIONAL_DOCUMENT.additionalDocumentId);
    });
});
