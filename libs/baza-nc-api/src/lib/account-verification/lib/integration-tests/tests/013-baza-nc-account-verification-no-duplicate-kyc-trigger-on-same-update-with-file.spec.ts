import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountVerificationStep, Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';
import * as fs from 'fs';
import * as path from 'path';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/013-baza-nc-account-verification-no-duplicate-kyc-trigger-on-same-update-with-file.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationNoSsnE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will have InvestorProfile step after upload file', async () => {
        const uploadSsnDocumentResponse = await dataAccessAccountVerification.uploadPersonalInformationSSNDocument(
            fs.createReadStream(path.join(__dirname, '../files/ssn.jpg')),
        );

        expect(isBazaErrorResponse(uploadSsnDocumentResponse)).toBeFalsy();

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.currentStep).toBe(AccountVerificationStep.InvestorProfile);
    });

    it('will ave InvestorProfile step status after multiple updates', async () => {
        for (let i = 0; i < 3; i++) {
            const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
                phone: '2292201647',
                phoneCountryCode: '1',
                phoneCountryNumericCode: '14',
                firstName: 'JOHN',
                lastName: 'SMITH',
                dateOfBirth: '03-28-1975',
                hasSsn: false,
                citizenship: Domicile.USCitizen,
                residentialStreetAddress1: '222334 PEACHTREE PLACE',
                residentialStreetAddress2: '1414',
                residentialCity: 'ATLANTA',
                residentialState: 'GA',
                residentialZipCode: '30033',
                residentialCountry: 'USA',
            });

            expect(isBazaErrorResponse(updateResponse)).toBeFalsy();

            const indexResponse = await dataAccessAccountVerification.index();

            expect(isBazaErrorResponse(indexResponse)).toBeFalsy();

            expect(updateResponse.currentStep).toBe(AccountVerificationStep.InvestorProfile);
            expect(indexResponse.currentStep).toBe(AccountVerificationStep.InvestorProfile);
        }
    });
});
