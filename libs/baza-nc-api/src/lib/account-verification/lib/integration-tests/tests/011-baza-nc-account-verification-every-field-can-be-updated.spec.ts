import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { Domicile } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/011-baza-nc-account-verification-every-field-can-be-updated.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFullFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will update firstName field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            firstName: 'RYAN',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
    });

    it('will update lastName field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            lastName: 'DAVE',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
    });

    it('will update DOB field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: '01-01-1991',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
    });

    it('will update DOB field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: '01-01-1991',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
    });

    it('will update phone field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '9959018846',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
    });

    it('will update phoneCountryCode field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            phoneCountryCode: '7',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
    });

    it('will update phoneCountryNumericCode field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            phoneCountryNumericCode: '11',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
    });

    it('will update citizenship field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            citizenship: Domicile.NonResident,
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
    });

    it('will update residentialStreetAddress1 field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            residentialStreetAddress1: 'Lenina',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('1414');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('1414');
    });

    it('will update residentialStreetAddress2 field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            residentialStreetAddress2: 'ap. 13',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
    });

    it('will update residentialCity field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            residentialCity: 'Moscow',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
    });

    it('will update residentialState field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            residentialState: 'Oblast',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
    });

    it('will update residentialZipCode field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            residentialZipCode: '154000',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
    });

    it('will update residentialCountry field', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            residentialCountry: 'Russia',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(updateResponse.personalInformation.residentialCountry).toBe('Russia');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(indexResponse.personalInformation.residentialCountry).toBe('Russia');
    });

    it('will set as NO SSN', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: false,
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(updateResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(updateResponse.personalInformation.hasSsn).toBeFalsy();
        expect(updateResponse.personalInformation.ssn).toBeNull();

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(indexResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(indexResponse.personalInformation.hasSsn).toBeFalsy();
        expect(indexResponse.personalInformation.ssn).toBeNull();
    });

    it('will set new SSN', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: true,
            ssn: '111-222-333',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(updateResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(updateResponse.personalInformation.hasSsn).toBeTruthy();
        expect(updateResponse.personalInformation.ssn).toBe('111-222-333');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(indexResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(indexResponse.personalInformation.hasSsn).toBeTruthy();
        expect(indexResponse.personalInformation.ssn).toBe('111-222-333');
    });

    it('will set update SSN', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: true,
            ssn: '444-555-666',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(updateResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(updateResponse.personalInformation.hasSsn).toBeTruthy();
        expect(updateResponse.personalInformation.ssn).toBe('444-555-666');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(indexResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(indexResponse.personalInformation.hasSsn).toBeTruthy();
        expect(indexResponse.personalInformation.ssn).toBe('444-555-666');
    });

    it('will set update SSN once more', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: true,
            ssn: '444-555-999',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(updateResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(updateResponse.personalInformation.hasSsn).toBeTruthy();
        expect(updateResponse.personalInformation.ssn).toBe('444-555-999');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(indexResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(indexResponse.personalInformation.hasSsn).toBeTruthy();
        expect(indexResponse.personalInformation.ssn).toBe('444-555-999');
    });

    it('will set SSN once more w/o hasSsn flag', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            ssn: '444-555-888',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(updateResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(updateResponse.personalInformation.hasSsn).toBeTruthy();
        expect(updateResponse.personalInformation.ssn).toBe('444-555-888');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(indexResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(indexResponse.personalInformation.hasSsn).toBeTruthy();
        expect(indexResponse.personalInformation.ssn).toBe('444-555-888');
    });

    it('will clean up SSN', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: false,
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(updateResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(updateResponse.personalInformation.hasSsn).toBeFalsy();
        expect(updateResponse.personalInformation.ssn).toBeNull();

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(indexResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(indexResponse.personalInformation.hasSsn).toBeFalsy();
        expect(indexResponse.personalInformation.ssn).toBeNull();
    });

    it('will bring SSN back', async () => {
        const updateResponse = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: true,
            ssn: '555-666-777',
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.personalInformation.firstName).toBe('RYAN');
        expect(updateResponse.personalInformation.lastName).toBe('DAVE');
        expect(updateResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(updateResponse.personalInformation.phone).toBe('9959018846');
        expect(updateResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(updateResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(updateResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(updateResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(updateResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(updateResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(updateResponse.personalInformation.residentialState).toBe('Oblast');
        expect(updateResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(updateResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(updateResponse.personalInformation.hasSsn).toBeTruthy();
        expect(updateResponse.personalInformation.ssn).toBe('555-666-777');

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.personalInformation.firstName).toBe('RYAN');
        expect(indexResponse.personalInformation.lastName).toBe('DAVE');
        expect(indexResponse.personalInformation.dateOfBirth).toBe('01-01-1991');
        expect(indexResponse.personalInformation.phone).toBe('9959018846');
        expect(indexResponse.personalInformation.phoneCountryCode).toBe('7');
        expect(indexResponse.personalInformation.phoneCountryNumericCode).toBe('11');
        expect(indexResponse.personalInformation.citizenship).toBe(Domicile.NonResident);
        expect(indexResponse.personalInformation.residentialStreetAddress1).toBe('Lenina');
        expect(indexResponse.personalInformation.residentialStreetAddress2).toBe('ap. 13');
        expect(indexResponse.personalInformation.residentialCity).toBe('Moscow');
        expect(indexResponse.personalInformation.residentialState).toBe('Oblast');
        expect(indexResponse.personalInformation.residentialZipCode).toBe('154000');
        expect(indexResponse.personalInformation.residentialCountry).toBe('Russia');
        expect(indexResponse.personalInformation.hasSsn).toBeTruthy();
        expect(indexResponse.personalInformation.ssn).toBe('555-666-777');
    });

    it('will update investor profile', async () => {
        const updateResponse = await dataAccessAccountVerification.applyInvestorProfile({
            isAssociatedWithFINRA: true,
            isAccreditedInvestor: true,
            netWorth: 150000,
            currentAnnualHouseholdIncome: 250000,
        });

        expect(isBazaErrorResponse(updateResponse)).toBeFalsy();
        expect(updateResponse.investorProfile.isAssociatedWithFINRA).toBeTruthy();
        expect(updateResponse.investorProfile.isAccreditedInvestor).toBeTruthy();
        expect(updateResponse.investorProfile.netWorth).toBe(150000);
        expect(updateResponse.investorProfile.currentAnnualHouseholdIncome).toBe(250000);

        const indexResponse = await dataAccessAccountVerification.index();

        expect(indexResponse.investorProfile.isAssociatedWithFINRA).toBeTruthy();
        expect(indexResponse.investorProfile.isAccreditedInvestor).toBeTruthy();
        expect(indexResponse.investorProfile.netWorth).toBe(150000);
        expect(indexResponse.investorProfile.currentAnnualHouseholdIncome).toBe(250000);
    });
});
