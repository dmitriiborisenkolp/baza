import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { AccountVerificationStep, BazaNorthCapitalAccountVerificationErrorCodes, Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/019-baza-nc-account-verification-validate-zipcode-on-apply-personal-information.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    describe('zipcode validation for US addresses', () => {
        beforeEach(async () => {
            await http.authE2eUser();
        });

        it('should not apply personal information with invalid zipcode for US address (country: U.S.)', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                residentialCountry: 'U.S.' /* valid US address */,
                residentialZipCode: '1' /* invalid US ZipCode */,
            });

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect((response as any).code).toBe(
                BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits,
            );
            expect((response as any).statusCode).toBe(HttpStatus.BAD_REQUEST);
        });

        it('should not apply personal information with invalid zipcode for US address (country: U.S)', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                residentialCountry: 'U.S' /* valid US address */,
                residentialZipCode: '1' /* invalid US ZipCode */,
            });

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect((response as any).code).toBe(
                BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits,
            );
            expect((response as any).statusCode).toBe(HttpStatus.BAD_REQUEST);
        });

        it('should not apply personal information with invalid zipcode for US address (country: USA)', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                residentialCountry: 'USA' /* valid US address */,
                residentialZipCode: '1' /* invalid US ZipCode */,
            });

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect((response as any).code).toBe(
                BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits,
            );
            expect((response as any).statusCode).toBe(HttpStatus.BAD_REQUEST);
        });

        it('should not apply personal information with invalid zipcode for US address (country: United States)', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                residentialCountry: 'United States' /* valid US address */,
                residentialZipCode: '1' /* invalid US ZipCode */,
            });

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect((response as any).code).toBe(
                BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits,
            );
            expect((response as any).statusCode).toBe(HttpStatus.BAD_REQUEST);
        });

        it('should not apply personal information with invalid zipcode for US address (country: US)', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                residentialCountry: 'US' /* valid US address */,
                residentialZipCode: '1' /* invalid US ZipCode */,
            });

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect((response as any).code).toBe(
                BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits,
            );
            expect((response as any).statusCode).toBe(HttpStatus.BAD_REQUEST);
        });

        it('should not apply personal information with less than 5 digit zipcode for US addresses', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                residentialCountry: 'USA' /* valid US address */,
                residentialZipCode: '1234' /* less than 5 digits */,
            });

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect((response as any).code).toBe(
                BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits,
            );
            expect((response as any).statusCode).toBe(HttpStatus.BAD_REQUEST);
        });

        it('should not apply personal information with more than 5 digit zipcode for US addresses', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                residentialCountry: 'USA' /* valid US address */,
                residentialZipCode: '123456' /* less than 5 digits */,
            });

            expect(isBazaErrorResponse(response)).toBeTruthy();
            expect((response as any).code).toBe(
                BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits,
            );
            expect((response as any).statusCode).toBe(HttpStatus.BAD_REQUEST);
        });

        it('should apply personal information with valid zipcode for US addresses with full detail', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                citizenship: Domicile.USCitizen,
                dateOfBirth: '07-22-1996',
                firstName: 'scalio',
                hasSsn: true,
                lastName: 'user',
                phone: '1111111',
                phoneCountryCode: '1',
                phoneCountryNumericCode: '840',
                residentialCity: 'test',
                residentialCountry: 'USA' /* valid US address */,
                residentialState: 'test',
                residentialStreetAddress1: 'test',
                residentialStreetAddress2: 'test',
                residentialZipCode: '12345' /* 5 digit zipcode */,
                ssn: '516-48-9277' /* valid ssn */,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
            expect(response.currentStep).toBe(AccountVerificationStep.RequestDocuments);
            expect(response.kycStatus).toBe(KYCStatus.Disapproved);
        });
    });

    describe('zipcode validation for NONE-US addresses', () => {
        beforeEach(async () => {
            await http.authE2eUser2();
        });
        it('should apply personal information with less than 5 digit zipcode for NONE-US addresses', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                citizenship: Domicile.NonResident,
                dateOfBirth: '07-22-1996',
                firstName: 'scalio',
                hasSsn: true,
                lastName: 'user',
                phone: '1111111',
                phoneCountryCode: '2',
                phoneCountryNumericCode: '840',
                residentialCity: 'test',
                residentialCountry: 'RUSSIA' /* NONE-US Country */,
                residentialState: 'test',
                residentialStreetAddress1: 'test',
                residentialStreetAddress2: 'test',
                residentialZipCode: '1' /* less than 5 digit zipcode */,
                ssn: '123-45-67899',
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
            expect(response.currentStep).toBe(AccountVerificationStep.RequestDocuments);
            expect(response.kycStatus).toBe(KYCStatus.Disapproved);
        });

        it('should apply personal information with more than 5 digit zipcode for NONE-US addresses', async () => {
            const response = await dataAccessAccountVerification.applyPersonalInformation({
                citizenship: Domicile.NonResident,
                dateOfBirth: '07-22-1996',
                firstName: 'scalio',
                hasSsn: true,
                lastName: 'user',
                phone: '1111111',
                phoneCountryCode: '2',
                phoneCountryNumericCode: '840',
                residentialCity: 'test',
                residentialCountry: 'RUSSIA' /* NONE-US Country */,
                residentialState: 'test',
                residentialStreetAddress1: 'test',
                residentialStreetAddress2: 'test',
                residentialZipCode: '123456' /* more than 5 digit zipcode */,
                ssn: '123-45-67899',
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
            expect(response.currentStep).toBe(AccountVerificationStep.RequestDocuments);
            expect(response.kycStatus).toBe(KYCStatus.Disapproved);
        });
    });
});
