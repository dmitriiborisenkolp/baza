import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/024-baza-nc-account-verification-baza-1904.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will call formResources endpoint without errors', async () => {
        const response = await dataAccessAccountVerification.formResources();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that no investor account has been created', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id > 0).toBeFalsy();
    });

    it('will try to run both account verification (current) and formResources endpoint at same time', async () => {
        const responseA = dataAccessAccountVerification.formResources();
        const responseB = dataAccessAccountVerification.index();

        await responseA;
        await responseB;

        expect(isBazaErrorResponse(responseA)).toBeFalsy();
        expect(isBazaErrorResponse(responseB)).toBeFalsy();
    });
});
