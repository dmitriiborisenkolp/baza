import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcE2eNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { AccountVerificationStep, Domicile, NorthCapitalAccountId, PartyId } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/022-baza-nc-account-verification-fields-updates.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessNcE2e = new BazaNcE2eNodeAccess(http);

    let ncAccountId: NorthCapitalAccountId;
    let ncPartyId: PartyId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will perform account verification', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.InvestorProfile);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('1');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('14');
        expect(response.personalInformation.firstName).toBe('JOHN');
        expect(response.personalInformation.lastName).toBe('SMITH');
        expect(response.personalInformation.dateOfBirth).toBe('02-28-1975');
        expect(response.personalInformation.hasSsn).toBe(true);
        expect(response.personalInformation.ssn).toBe('112-22-3333');
        expect(response.personalInformation.citizenship).toBe(Domicile.USCitizen);
        expect(response.personalInformation.residentialStreetAddress1).toBe('222333 PEACHTREE PLACE');
        expect(response.personalInformation.residentialStreetAddress2).toBe('1414');
        expect(response.personalInformation.residentialCity).toBe('ATLANTA');
        expect(response.personalInformation.residentialState).toBe('GA');
        expect(response.personalInformation.residentialZipCode).toBe('30033');
        expect(response.personalInformation.residentialCountry).toBe('USA');

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(investorAccount.northCapitalAccountId).toBeDefined();
        expect(investorAccount.northCapitalPartyId).toBeDefined();

        ncAccountId = investorAccount.northCapitalAccountId;
        ncPartyId = investorAccount.northCapitalPartyId;
    });

    it('[1] will update field1/field2/field3 fields of NC Party', async () => {
        const account = await dataAccessNcE2e.getNcAccount({ ncAccountId });

        expect(isBazaErrorResponse(account)).toBeFalsy();

        expect(account.accountDetails.accountName).toBe('JOHN SMITH');
        expect(account.accountDetails.field1).toBe('14');

        const party = await dataAccessNcE2e.getNcParty({ ncPartyId });

        expect(isBazaErrorResponse(party)).toBeFalsy();

        expect(party.partyDetails[0].firstName).toBe('JOHN');
        expect(party.partyDetails[0].lastName).toBe('SMITH');
        expect(party.partyDetails[0].field1).toBe('112-22-3333');
        expect(party.partyDetails[0].field2).toBe('ATLANTA GA 30033');
        expect(party.partyDetails[0].field3).toBe('1');
    });

    it('will update account verification details', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '7',
            phoneCountryNumericCode: '15',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '666-77-8888',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'TEXAS',
            residentialState: 'TX',
            residentialZipCode: '30034',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.currentStep).toBe(AccountVerificationStep.RequestDocuments);
        expect(response.personalInformation.phone).toBe('2292201647');
        expect(response.personalInformation.phoneCountryCode).toBe('7');
        expect(response.personalInformation.phoneCountryNumericCode).toBe('15');
        expect(response.personalInformation.firstName).toBe('JOHN');
        expect(response.personalInformation.lastName).toBe('SMITH');
        expect(response.personalInformation.dateOfBirth).toBe('02-28-1975');
        expect(response.personalInformation.hasSsn).toBe(true);
        expect(response.personalInformation.ssn).toBe('666-77-8888');
        expect(response.personalInformation.citizenship).toBe(Domicile.USCitizen);
        expect(response.personalInformation.residentialStreetAddress1).toBe('222333 PEACHTREE PLACE');
        expect(response.personalInformation.residentialStreetAddress2).toBe('1414');
        expect(response.personalInformation.residentialCity).toBe('TEXAS');
        expect(response.personalInformation.residentialState).toBe('TX');
        expect(response.personalInformation.residentialZipCode).toBe('30034');
        expect(response.personalInformation.residentialCountry).toBe('USA');

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(investorAccount.northCapitalAccountId).toBe(ncAccountId);
        expect(investorAccount.northCapitalPartyId).toBe(ncPartyId);
    });

    it('[2] will update field1/field2/field3 fields of NC Party', async () => {
        const account = await dataAccessNcE2e.getNcAccount({ ncAccountId });

        expect(isBazaErrorResponse(account)).toBeFalsy();

        expect(account.accountDetails.accountName).toBe('JOHN SMITH');
        expect(account.accountDetails.field1).toBe('15');

        const party = await dataAccessNcE2e.getNcParty({ ncPartyId });

        expect(isBazaErrorResponse(party)).toBeFalsy();

        expect(party.partyDetails[0].firstName).toBe('JOHN');
        expect(party.partyDetails[0].lastName).toBe('SMITH');
        expect(party.partyDetails[0].field1).toBe('666-77-8888');
        expect(party.partyDetails[0].field2).toBe('TEXAS TX 30034');
        expect(party.partyDetails[0].field3).toBe('7');
    });
});
