// https://scalio.atlassian.net/browse/CMNW-1698

import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';
import * as fs from 'fs';
import * as path from 'path';
import { BazaNcAccountVerificationNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(100000);

describe('@scaliolabs/baza-nc-api/account-verification/008-baza-nc-account-verification-cmnw-1698.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    const expectAccountVerificationIsFinished = async () => {
        await asyncExpect(
            async () => {
                const investorAccount = await dataAccessInvestorAccount.current();

                expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
                expect(investorAccount.isAccountVerificationCompleted).toBeTruthy();

                const isCompleted = await dataAccessAccountVerification.isCompleted();

                expect(isBazaErrorResponse(isCompleted)).toBeFalsy();
                expect(isCompleted.isCompleted).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    };

    it('will setup account verification with failed KYC/AML', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'FOO',
            lastName: 'BAR',
            dateOfBirth: '02-28-1974',
            hasSsn: false,
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '844454 SOME PLACE',
            residentialStreetAddress2: '18',
            residentialCity: 'MOSCOW',
            residentialState: 'MOSCOW',
            residentialZipCode: '12900',
            residentialCountry: 'Russia',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.kycStatus).toBe(KYCStatus.Disapproved);
        expect(response.currentStep).toBe(AccountVerificationStep.RequestSSNDocument);

        const uploadSsnDocumentResponse = await dataAccessAccountVerification.uploadPersonalInformationSSNDocument(
            fs.createReadStream(path.join(__dirname, '../files/ssn.jpg')),
        );

        expect(isBazaErrorResponse(uploadSsnDocumentResponse)).toBeFalsy();
        expect(uploadSsnDocumentResponse.currentStep).toBe(AccountVerificationStep.InvestorProfile);

        const investorProfileResponse = await dataAccessAccountVerification.applyInvestorProfile({
            netWorth: 250,
            currentAnnualHouseholdIncome: 100000,
            isAccreditedInvestor: true,
            isAssociatedWithFINRA: true,
        });

        expect(isBazaErrorResponse(investorProfileResponse)).toBeFalsy();
        expect(investorProfileResponse.kycStatus).toBe(KYCStatus.Disapproved);
        expect(investorProfileResponse.currentStep).toBe(AccountVerificationStep.Completed);

        await expectAccountVerificationIsFinished();
    });

    // TODO: Looks like NC auto-approve is not working anymore
    xit('will setup account verification with valid data and will not ask for additional documents', async () => {
        const responsePhone = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
        });

        expect(isBazaErrorResponse(responsePhone)).toBeFalsy();
        expect(responsePhone.kycStatus).toBe(KYCStatus.Disapproved);
        expect(responsePhone.currentStep).toBe(AccountVerificationStep.Completed);

        const responseLegalName = await dataAccessAccountVerification.applyPersonalInformation({
            firstName: 'JHIN',
            lastName: 'SMITH',
        });

        expect(isBazaErrorResponse(responseLegalName)).toBeFalsy();
        expect(responseLegalName.kycStatus).toBe(KYCStatus.Disapproved);
        expect(responseLegalName.currentStep).toBe(AccountVerificationStep.Completed);

        const responseDOB = await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: '02-28-1975',
        });

        expect(isBazaErrorResponse(responseDOB)).toBeFalsy();
        expect(responseDOB.kycStatus).toBe(KYCStatus.Disapproved);
        expect(responseDOB.currentStep).toBe(AccountVerificationStep.Completed);

        const responseSSN = await dataAccessAccountVerification.applyPersonalInformation({
            hasSsn: true,
            ssn: '112-22-3333',
        });

        expect(isBazaErrorResponse(responseSSN)).toBeFalsy();
        expect(responseSSN.kycStatus).toBe(KYCStatus.AutoApproved);
        expect(responseSSN.currentStep).toBe(AccountVerificationStep.Completed);

        const responseCitizenship = await dataAccessAccountVerification.applyPersonalInformation({
            citizenship: Domicile.USCitizen,
        });

        expect(isBazaErrorResponse(responseCitizenship)).toBeFalsy();
        expect(responseCitizenship.kycStatus).toBe(KYCStatus.AutoApproved);
        expect(responseCitizenship.currentStep).toBe(AccountVerificationStep.Completed);

        const responseResidential = await dataAccessAccountVerification.applyPersonalInformation({
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(responseResidential)).toBeFalsy();
        expect(responseResidential.kycStatus).toBe(KYCStatus.AutoApproved);
        expect(responseResidential.currentStep).toBe(AccountVerificationStep.Completed);
    });
});
