import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';
import {
    BazaNcKycStatus,
    Domicile,
    KYCStatus,
    US_STATES_NON_US_COUNTRIES,
    US_STATES_PRIMARY,
    US_STATES_SECONDARY,
    UsState,
} from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/027-baza-nc-account-verification-us-pr-states.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will returns additional states mapping for US', async () => {
        const response = await dataAccessAccountVerification.formResources();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.statesMapping).toBeDefined();
        expect(Array.isArray(response.statesMapping) && response.statesMapping.length > 0);
        expect(US_STATES_PRIMARY.every((next) => response.primCountryStates.map((def) => def.value).includes(next))).toBeTruthy();
        expect(US_STATES_SECONDARY.every((next) => response.primCountryStates.map((def) => def.value).includes(next))).toBeTruthy();

        const def = response.statesMapping.find((next) => next.state === UsState.PR);

        expect(def).toBeDefined();
        expect(def).toEqual({ state: UsState.PR, country: 'Puerto Rico' });

        const usa = response.countries.find((next) => next.isUSA);

        expect(usa).toBeDefined();

        expect(US_STATES_PRIMARY.every((next) => usa.states.map((def) => def.value).includes(next))).toBeTruthy();
        expect(US_STATES_SECONDARY.every((next) => usa.states.map((def) => def.value).includes(next))).toBeTruthy();

        expect(US_STATES_NON_US_COUNTRIES.every((next) => !response.listCountries.includes(next))).toBeTruthy();
    });

    it('will fill Account Verification for Puerto Rico citizen', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'PR',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.residentialCountry).toBe('USA');
        expect(response.personalInformation.residentialState).toBe('PR');
        expect(response.kycStatus).toBe(KYCStatus.AutoApproved);
    });

    it('will returns that investor is not considered as Foreign Investor', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isInvestorVerified).toBeTruthy();
        expect(response.status.isForeign).toBeFalsy();
        expect(response.status.nc).toBe(BazaNcKycStatus.Approved);
    });
});
