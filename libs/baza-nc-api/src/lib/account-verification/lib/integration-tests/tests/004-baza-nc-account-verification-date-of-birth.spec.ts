import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/account-verification/004-baza-nc-account-verification-date-of-birth.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will not allow to set DOB with incorrect format', async () => {
        const dob = moment(new Date()).add(1, 'years').format('YYYY-MM-DD');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBIsInvalid);
    });

    it('will not allow to set DOB as 1 year old', async () => {
        const dob = moment(new Date()).add(-17, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBShouldBeAbove18);
    });

    it('will not allow to set DOB as 17 year old', async () => {
        const dob = moment(new Date()).add(-1, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBShouldBeAbove18);
    });

    it('will allow to set DOB as 18 year old', async () => {
        const dob = moment(new Date()).add(-18, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to set DOB as 19 year old', async () => {
        const dob = moment(new Date()).add(-19, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to set DOB as 24 year old', async () => {
        const dob = moment(new Date()).add(-24, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to set DOB in +1 years in future', async () => {
        const dob = moment(new Date()).add(1, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBShouldBeAbove18);
    });

    it('will not allow to set DOB in +17 years in future', async () => {
        const dob = moment(new Date()).add(17, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBShouldBeAbove18);
    });

    it('will not allow to set DOB in +18 years in future', async () => {
        const dob = moment(new Date()).add(18, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBShouldBeAbove18);
    });

    it('will not allow to set DOB in +19 years in future', async () => {
        const dob = moment(new Date()).add(19, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBShouldBeAbove18);
    });

    it('will not allow to set DOB in +24 years in future', async () => {
        const dob = moment(new Date()).add(24, 'years').format('MM-DD-YYYY');

        const response: BazaError = (await dataAccessAccountVerification.applyPersonalInformation({
            dateOfBirth: dob,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBShouldBeAbove18);
    });
});
