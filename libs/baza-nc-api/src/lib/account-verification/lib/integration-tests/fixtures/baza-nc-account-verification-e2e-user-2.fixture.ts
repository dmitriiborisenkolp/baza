import { Injectable } from '@nestjs/common';
import { BazaCoreAuthUniqueUsersFixture, BazaCoreAuthUsersFixture, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';
import { BazaNcAccountVerificationPersonalInformationService } from '../../services/baza-nc-account-verification-personal-information.service';
import { Domicile } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../../typeorm';
import { BazaNcInvestorAccountService } from '../../../../investor-account';

@Injectable()
export class BazaNcAccountVerificationE2eUser2Fixture implements BazaE2eFixture {
    public static _e2eUserInvestorAccount: BazaNcInvestorAccountEntity;

    constructor(
        private readonly e2eUserFixture: BazaCoreAuthUsersFixture,
        private readonly accountVerification: BazaNcAccountVerificationPersonalInformationService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly e2eUniqueUserFixture: BazaCoreAuthUniqueUsersFixture,
    ) {}

    async up<E2eNcAccountVerificationFixtures>(): Promise<void> {
        const account = this.e2eUserFixture.e2eUser2 || this.e2eUniqueUserFixture.e2eUser;
        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(account);

        if (!investorAccount.northCapitalAccountId) {
            await this.accountVerification.applyPersonalInformation(account, {
                phone: '2292201647',
                phoneCountryCode: '1',
                phoneCountryNumericCode: '14',
                firstName: 'JOHN',
                lastName: 'SMITH',
                dateOfBirth: '02-28-1975',
                hasSsn: true,
                ssn: '112-22-3333',
                citizenship: Domicile.USCitizen,
                residentialStreetAddress1: '222333 PEACHTREE PLACE',
                residentialStreetAddress2: '1414',
                residentialCity: 'ATLANTA',
                residentialState: 'GA',
                residentialZipCode: '30033',
                residentialCountry: 'USA',
            });
        }

        BazaNcAccountVerificationE2eUser2Fixture._e2eUserInvestorAccount = investorAccount;
    }

    get investorAccount(): BazaNcInvestorAccountEntity {
        return BazaNcAccountVerificationE2eUser2Fixture._e2eUserInvestorAccount;
    }
}

defineE2eFixtures<BazaNcAccountVerificationFixtures>([
    {
        fixture: BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUser2Fixture,
        provider: BazaNcAccountVerificationE2eUser2Fixture,
    },
]);
