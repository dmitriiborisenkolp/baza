import { Injectable } from '@nestjs/common';
import { BazaCoreAuthUniqueUsersFixture, BazaCoreAuthUsersFixture, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationFixtures } from '../baza-nc-account-verification-fixtures';
import { BazaNcAccountVerificationPersonalInformationService } from '../../services/baza-nc-account-verification-personal-information.service';
import { Domicile } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../../typeorm';
import { BazaNcInvestorAccountService } from '../../../../investor-account';
import { BazaNcAccountVerificationInvestorProfileService } from '../../services/baza-nc-account-verification-investor-profile.service';

@Injectable()
export class BazaNcAccountVerificationE2eUserFullFixture implements BazaE2eFixture {
    private static _e2eUserInvestorAccount: BazaNcInvestorAccountEntity;

    constructor(
        private readonly e2eUserFixture: BazaCoreAuthUsersFixture,
        private readonly accountVerificationPersonalInfo: BazaNcAccountVerificationPersonalInformationService,
        private readonly accountVerificationInvestorProfile: BazaNcAccountVerificationInvestorProfileService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly e2eUniqueUserFixture: BazaCoreAuthUniqueUsersFixture,
    ) {}

    async up<E2eNcAccountVerificationFixtures>(): Promise<void> {
        const account = this.e2eUserFixture.e2eUser || this.e2eUniqueUserFixture.e2eUser;
        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(account);

        await this.accountVerificationPersonalInfo.applyPersonalInformation(account, {
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        await this.accountVerificationInvestorProfile.applyInvestorProfile(account, {
            isAssociatedWithFINRA: false,
            isAccreditedInvestor: false,
            netWorth: 100000,
            currentAnnualHouseholdIncome: 200000,
        });

        BazaNcAccountVerificationE2eUserFullFixture._e2eUserInvestorAccount = investorAccount;
    }

    get investorAccount(): BazaNcInvestorAccountEntity {
        return BazaNcAccountVerificationE2eUserFullFixture._e2eUserInvestorAccount;
    }
}

defineE2eFixtures<BazaNcAccountVerificationFixtures>([
    {
        fixture: BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFullFixture,
        provider: BazaNcAccountVerificationE2eUserFullFixture,
    },
]);
