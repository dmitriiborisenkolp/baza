import { Injectable } from '@nestjs/common';
import {
    AccountVerificationDto,
    AccountVerificationStep,
    AccreditedStatus,
    BAZA_NC_ACCOUNT_VERIFICATION_PRIM_ADDRESS_PLACEHOLDER,
    BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER,
    BAZA_NC_ACCOUNT_VERIFICATION_STATE_PLACEHOLDER,
    NorthCapitalErrorCodes,
    NorthCapitalYesNoBoolean,
} from '@scaliolabs/baza-nc-shared';
import { AccountEntity, AwsService, EnvService } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationSessionService } from './baza-nc-account-verification-session.service';
import {
    BazaNcConfigurationNestjsService,
    isNorthCapitalException,
    ncOutput,
    NcOutputContext,
    NorthCapitalAccountsApiNestjsService,
    NorthCapitalPartiesApiNestjsService,
} from '../../../transact-api';
import { BazaNcInvestorAccountService } from '../../../investor-account';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { EventBus } from '@nestjs/cqrs';

/**
 * Account Verification Status Service
 * This service is responsible for fetching information about current Status of Account Verification and updating
 * related `isAccountVerificationInProgress` and `isAccountVerificationCompleted` flags
 * This service is also returns an Account Verification DTO which is a primary source of all information about
 * Account Verification
 */
@Injectable()
export class BazaNcAccountVerificationStatusService {
    constructor(
        private readonly env: EnvService,
        private readonly eventBus: EventBus,
        private readonly ncConfig: BazaNcConfigurationNestjsService,
        private readonly session: BazaNcAccountVerificationSessionService,
        private readonly accountsApi: NorthCapitalAccountsApiNestjsService,
        private readonly partiesApi: NorthCapitalPartiesApiNestjsService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly awsService: AwsService,
    ) {}

    /**
     * Returns true if Account Verification is completed for current user
     * @param user
     */
    async isCompleted(user: AccountEntity): Promise<boolean> {
        return (await this.getCurrentStep(user)) === AccountVerificationStep.Completed;
    }

    /**
     * Returns recommended step for Account Verification
     * @param user
     */
    async getCurrentStep(user: AccountEntity): Promise<AccountVerificationStep> {
        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(user);

        const hasSavedAccountAtNC = !!investorAccount && !!investorAccount.northCapitalAccountId && !!investorAccount.northCapitalPartyId;

        if (hasSavedAccountAtNC) {
            const session = await this.session.getSession(user.id);

            const account = ncOutput(
                (
                    await this.accountsApi.getAccount({
                        accountId: investorAccount.northCapitalAccountId,
                    })
                ).accountDetails,
                NcOutputContext.GetAccount,
            );

            const suitability = await (async () => {
                try {
                    return (
                        await this.accountsApi.getSuitability({
                            accountId: investorAccount.northCapitalAccountId,
                        })
                    ).accountDetails;
                } catch (err) {
                    if (isNorthCapitalException(err, NorthCapitalErrorCodes.SuitabilityIsNotExistActive)) {
                        return null;
                    } else {
                        throw err;
                    }
                }
            });

            if (suitability) {
                delete suitability['id'];
                delete suitability['developerAPIKey'];
            }

            const hasInvestorProfileData = account.accreditedStatus !== AccreditedStatus.Pending;

            const conditions = [
                { step: AccountVerificationStep.Completed, meetConditions: hasInvestorProfileData },
                { step: AccountVerificationStep.InvestorProfile, meetConditions: true },
            ];

            if (session.hasRequestForSSNDocument) {
                return AccountVerificationStep.RequestSSNDocument;
            } else if (session.hasRequestForAdditionalDocuments) {
                return AccountVerificationStep.RequestDocuments;
            } else {
                return conditions.find((c) => c.meetConditions).step;
            }
        } else {
            const session = await this.session.getSession(user.id);

            const hasPhone = !!session.phone;
            const hasLegalName = !!session.firstName && !!session.lastName;
            const hasDateOfBirth = !!session.dateOfBirth;
            const hasSSN = session.hasSsn !== undefined && session.hasSsn !== null;
            const hasSsnDocument = (session.hasSsn === true && !!session.ssn) || (!session.hasSsn && session.hasSsnDocument);
            const hasCitizenship = !!session.citizenship;
            const hasResidential =
                !!session.residentialCountry &&
                !!session.residentialCity &&
                !!session.residentialZipCode &&
                !!session.residentialStreetAddress1;

            const conditions = [
                {
                    step: AccountVerificationStep.ResidentialAddress,
                    meetConditions:
                        hasPhone && hasLegalName && hasDateOfBirth && hasSSN && hasSsnDocument && hasCitizenship && hasResidential,
                }, // Specific case, usually shouldn't match
                {
                    step: AccountVerificationStep.ResidentialAddress,
                    meetConditions: hasPhone && hasLegalName && hasDateOfBirth && hasSSN && hasSsnDocument && hasCitizenship,
                },
                {
                    step: AccountVerificationStep.Citizenship,
                    meetConditions: hasPhone && hasLegalName && hasDateOfBirth && hasSSN && hasSsnDocument,
                },
                { step: AccountVerificationStep.RequestSSNDocument, meetConditions: hasPhone && hasLegalName && hasDateOfBirth && hasSSN },
                { step: AccountVerificationStep.SSN, meetConditions: hasPhone && hasLegalName && hasDateOfBirth },
                { step: AccountVerificationStep.DateOfBirth, meetConditions: hasPhone && hasLegalName },
                { step: AccountVerificationStep.LegalName, meetConditions: hasPhone },
                { step: AccountVerificationStep.PhoneNumber, meetConditions: true },
            ];

            return conditions.find((c) => c.meetConditions).step;
        }
    }

    /**
     * Returns Account Verification DTO
     * Account Verification DTO is primary source of everything about Account Verification
     * @param user
     */
    async fetchAccountVerification(user: AccountEntity): Promise<AccountVerificationDto> {
        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(user);

        const isSavedToNC = !!investorAccount && !!investorAccount.northCapitalPartyId;

        if (isSavedToNC) {
            const account = ncOutput(
                (
                    await this.accountsApi.getAccount({
                        accountId: investorAccount.northCapitalAccountId,
                    })
                ).accountDetails,
                NcOutputContext.GetAccount,
            );

            const party = ncOutput(
                (
                    await this.partiesApi.getParty({
                        partyId: investorAccount.northCapitalPartyId,
                    })
                ).partyDetails[0],
                NcOutputContext.GetParty,
            );

            const currentStep = await this.getCurrentStep(user);

            const hasSsn =
                (party.socialSecurityNumber || '').toString().trim().length > 0 &&
                party.socialSecurityNumber !== BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER;
            const hasSsnDocument = !hasSsn && !!investorAccount.ssnDocumentFileName;

            const ncDocuments = await (async () => {
                return await (async () => {
                    try {
                        return await this.partiesApi.getPartyDocument({
                            partyId: party.partyId,
                        });
                    } catch (err) {
                        if (
                            isNorthCapitalException(err, NorthCapitalErrorCodes.NoDocumentFoundForThisParty) ||
                            isNorthCapitalException(err, NorthCapitalErrorCodes.AccountCreditCardDetailsDoesnTExistActive)
                        ) {
                            return {
                                partyDocumentDetails: [],
                            };
                        } else {
                            throw err;
                        }
                    }
                })();
            })();

            const ssnDocument = (() => {
                if (hasSsnDocument && ncDocuments?.partyDocumentDetails.length) {
                    return ncDocuments.partyDocumentDetails.find((d) => d.documentTitle === investorAccount.ssnDocumentFileName);
                }
            })();

            const additionalDocument = (() => {
                if (ncDocuments?.partyDocumentDetails.length) {
                    const docs = ncDocuments.partyDocumentDetails.filter((d) => d.documentTitle !== investorAccount.ssnDocumentFileName);

                    if (docs.length) {
                        return docs[docs.length - 1];
                    }
                }
            })();

            const suitability = await (async () => {
                try {
                    return (
                        await this.accountsApi.getSuitability({
                            accountId: investorAccount.northCapitalAccountId,
                        })
                    ).accountDetails;
                } catch (err) {
                    if (isNorthCapitalException(err, NorthCapitalErrorCodes.SuitabilityIsNotExistActive)) {
                        return null;
                    } else {
                        throw err;
                    }
                }
            })();

            if (suitability) {
                delete suitability['id'];
                delete suitability['developerAPIKey'];
            }

            const residentialState = party.primState === BAZA_NC_ACCOUNT_VERIFICATION_STATE_PLACEHOLDER ? undefined : party.primState;

            const enableDebugLog = this.env.isTestEnvironment || this.ncConfig.configuration.debug;

            return {
                currentStep,
                kycStatus: party.kycStatus,
                _debugAccount: enableDebugLog
                    ? await this.accountsApi.getAccount({
                          accountId: investorAccount.northCapitalAccountId,
                      })
                    : undefined,
                _debugParty: enableDebugLog ? party : undefined,
                personalInformation: {
                    firstName: party.firstName,
                    lastName: party.lastName,
                    dateOfBirth: party.dob,
                    hasSsn,
                    ssn: hasSsn ? party.socialSecurityNumber : null,
                    phone: party.phone,
                    phoneCountryNumericCode: account.field1,
                    phoneCountryCode: party.field3,
                    citizenship: party.domicile,
                    residentialCountry: party.primCountry,
                    residentialState,
                    residentialCity: party.primCity,
                    residentialZipCode: party.primZip,
                    residentialStreetAddress1: party.primAddress1,
                    residentialStreetAddress2:
                        party.primAddress2 && party.primAddress2 !== BAZA_NC_ACCOUNT_VERIFICATION_PRIM_ADDRESS_PLACEHOLDER
                            ? party.primAddress2
                            : undefined,
                    hasSsnDocument: !!investorAccount.ssnDocumentFileName,
                    ssnDocumentFileName: ssnDocument ? ssnDocument.documentTitle : null,
                    ssnDocumentUrl: ssnDocument ? ssnDocument.documentUrl : null,
                    ssnDocumentId: ssnDocument ? ssnDocument.documentid : null,
                    additionalDocumentFileName: additionalDocument ? additionalDocument.documentTitle : null,
                    additionalDocumentUrl: additionalDocument ? additionalDocument.documentUrl : null,
                    additionalDocumentId: additionalDocument ? additionalDocument.documentid : null,
                },
                investorProfile: {
                    isAccreditedInvestor:
                        account.accreditedStatus !== AccreditedStatus.Pending
                            ? account.accreditedStatus !== AccreditedStatus.NotAccredited
                            : null,
                    isAssociatedWithFINRA: party.associatedPerson ? party.associatedPerson === NorthCapitalYesNoBoolean.Yes : null,
                    currentAnnualHouseholdIncome: parseInt(party.currentHouseholdIncome, 10),
                    netWorth: parseInt(party.householdNetworth, 10),
                },
            };
        } else {
            const currentStep = await this.getCurrentStep(user);
            const session = await this.session.getSession(user.id);
            const ssn = session.hasSsn ? session.ssn : null;

            const residentialState =
                session.residentialState === BAZA_NC_ACCOUNT_VERIFICATION_STATE_PLACEHOLDER ? undefined : session.residentialState;

            const ssnDocumentUrl = session.ssnDocumentAwsKey
                ? await this.awsService.presignedUrl({
                      s3ObjectId: session.ssnDocumentAwsKey,
                      ttlSeconds: 10 /* minutes */ * 60 /* seconds */,
                  })
                : null;

            return {
                currentStep: currentStep,
                personalInformation: {
                    ...session,
                    ssnDocumentUrl,
                    residentialState,
                    ssn,
                },
            };
        }
    }

    /**
     * Returns true if NC Party is created for Account
     * @param user
     */
    async hasParty(user: AccountEntity): Promise<boolean> {
        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(user);

        return !!investorAccount && !!investorAccount.northCapitalPartyId;
    }
}
