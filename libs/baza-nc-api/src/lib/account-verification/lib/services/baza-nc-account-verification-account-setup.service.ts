import { Injectable } from '@nestjs/common';
import {
    AccountDetails,
    AccountType,
    AccountVerificationCompletedEvent,
    AccountVerificationPersonalInformationDto,
    AccountVerificationStep,
    AccreditedStatus,
    AmlStatus,
    ApprovalStatus,
    BAZA_NC_ACCOUNT_VERIFICATION_PRIM_ADDRESS_PLACEHOLDER,
    BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER,
    BAZA_NC_ACCOUNT_VERIFICATION_STATE_PLACEHOLDER,
    DomesticYN,
    Domicile,
    FirstEntryType,
    KYCStatus,
    LinkType,
    NorthCapitalAccountId,
    NorthCapitalErrorCodes,
    PartyDetails,
    PartyId,
    RelatedEntryType,
    UpdatePartyRequest,
} from '@scaliolabs/baza-nc-shared';
import { default as fetch } from 'node-fetch';
import { AccountVerificationSession, BazaNcAccountVerificationSessionService } from './baza-nc-account-verification-session.service';
import { BazaNcAccountVerificationKycAmlService } from './baza-nc-account-verification-kyc-aml.service';
import { AccountEntity, AwsService } from '@scaliolabs/baza-core-api';
import { isEmpty, isNotNullOrUndefined } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';
import { BazaNcInvestorDobIsInvalidException } from '../exceptions/baza-nc-investor-dob-is-invalid.exception';
import { BazaNcInvestorDobShouldBeAbove18Exception } from '../exceptions/baza-nc-investor-dob-should-be-above-18.exception';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import {
    isNorthCapitalException,
    NcInputContext,
    ncInputObjectPipe,
    ncOutput,
    NcOutputContext,
    NorthCapitalAccountsApiNestjsService,
    NorthCapitalDocumentsApiNestjsService,
    NorthCapitalLinksApiNestjsService,
    NorthCapitalPartiesApiNestjsService,
} from '../../../transact-api';
import { BazaNcInvestorAccountService } from '../../../investor-account';
import { BazaNcInvestorDobYearIsInvalidException } from '../exceptions/baza-nc-investor-dob-year-is-invalid.exception';
import { BazaNcDwollaCustomerService } from '../../../dwolla';
import { BazaNcAccountVerificationStatusService } from './baza-nc-account-verification-status.service';
import { EventBus } from '@nestjs/cqrs';

export interface SetupResponseNotSavedToNC {
    isSavedToNC: false;
    shouldRequestSSNDocument: boolean;
    shouldRequestDocuments: boolean;
}

export interface SetupResponseSavedToNC {
    isSavedToNC: true;
    wasCreated: boolean;
    accountId: NorthCapitalAccountId;
    partyId: PartyId;
    kycStatus: KYCStatus;
    shouldRequestDocuments: boolean;
    shouldRequestSSNDocument: boolean;
}

export type SetupResponse = SetupResponseNotSavedToNC | SetupResponseSavedToNC;

/**
 * Account Verification NC Account & Party service
 * This is an internal service which is used for saving information to NC or to temporary Session during Account
 * Verification
 */
@Injectable()
export class BazaNcAccountVerificationAccountSetupService {
    constructor(
        private readonly eventBus: EventBus,
        private readonly status: BazaNcAccountVerificationStatusService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly session: BazaNcAccountVerificationSessionService,
        private readonly accountsApi: NorthCapitalAccountsApiNestjsService,
        private readonly partiesApi: NorthCapitalPartiesApiNestjsService,
        private readonly linksApi: NorthCapitalLinksApiNestjsService,
        private readonly kyc: BazaNcAccountVerificationKycAmlService,
        private readonly documentsApi: NorthCapitalDocumentsApiNestjsService,
        private readonly awsS3Service: AwsService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly dwollaService: BazaNcDwollaCustomerService,
    ) {}

    /**
     * Saves Personal Information to NC or Account Verification Session
     * @param user
     * @param request
     */
    async save(user: AccountEntity, request: AccountVerificationPersonalInformationDto): Promise<SetupResponse> {
        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(user);

        const session: AccountVerificationSession = {
            ...(await this.session.getSession(user.id)),
            ...request,
        };

        const isSavedToNC = investorAccount && investorAccount.northCapitalAccountId && investorAccount.northCapitalPartyId;

        if (request.dateOfBirth) {
            const parsed = moment(request.dateOfBirth, 'MM-DD-YYYY', true);

            if (!parsed.isValid()) {
                throw new BazaNcInvestorDobIsInvalidException();
            }

            if (parsed.year() < 1900) {
                throw new BazaNcInvestorDobYearIsInvalidException();
            }

            if (moment(new Date()).diff(parsed, 'years') < 18) {
                throw new BazaNcInvestorDobShouldBeAbove18Exception();
            }
        }

        if (isSavedToNC) {
            const response = await this.updateNCAccount(user, request);

            const setupResponse: SetupResponse = {
                isSavedToNC: true,
                wasCreated: false,
                kycStatus: response.partyDetails.kycStatus,
                accountId: response.accountDetails.accountId,
                partyId: response.partyDetails.partyId,
                shouldRequestDocuments: false,
                shouldRequestSSNDocument: false,
            };

            await this.updateDocumentsAndKyc(user, session, response.diffRequest, setupResponse);
            await this.updateAccountVerificationInProgressAndCompletedFlags(user);

            return setupResponse;
        } else {
            if (this.session.isSessionReadyToBeSentToNorthCapital(session)) {
                const response = await this.createNCAccount(user, session);

                session.ncPartyId = response.partyDetails.partyId;
                session.ncAccountId = response.accountDetails.accountId;

                await this.session.save(user.id, session);

                const setupResponse: SetupResponse = {
                    isSavedToNC: true,
                    wasCreated: true,
                    accountId: response.accountDetails.accountId,
                    partyId: response.partyDetails.partyId,
                    kycStatus: response.partyDetails.kycStatus,
                    shouldRequestDocuments: false,
                    shouldRequestSSNDocument: false,
                };

                await this.updateDocumentsAndKyc(user, session, request, setupResponse);
                await this.updateAccountVerificationInProgressAndCompletedFlags(user);

                return setupResponse;
            } else {
                await this.session.save(user.id, session);

                const setupResponse: SetupResponse = {
                    isSavedToNC: false,
                    shouldRequestSSNDocument: false,
                    shouldRequestDocuments: false,
                };

                await this.updateDocumentsAndKyc(user, session, request, setupResponse);
                await this.updateAccountVerificationInProgressAndCompletedFlags(user);

                return setupResponse;
            }
        }
    }

    /**
     * Creates NC Account for Investor Account
     * @param user
     * @param request
     * @private
     */
    private async createNCAccount(
        user: AccountEntity,
        request: AccountVerificationSession,
    ): Promise<{ accountDetails: AccountDetails; partyDetails: PartyDetails }> {
        const session = await this.session.getSession(user.id);
        const shouldUploadSSN = !session.hasSsn && session.ssnDocumentUrl;

        const state =
            isNotNullOrUndefined(request.residentialState) && !isEmpty(request.residentialState)
                ? request.residentialState
                : BAZA_NC_ACCOUNT_VERIFICATION_STATE_PLACEHOLDER;

        const accountDetails = ncOutput(
            (
                await this.accountsApi.createAccount(
                    ncInputObjectPipe(
                        {
                            accountRegistration: `${request.firstName} ${request.lastName}`,
                            type: AccountType.Individual,
                            domesticYN: [Domicile.USCitizen, Domicile.USResident].includes(request.citizenship)
                                ? DomesticYN.DomesticAccount
                                : DomesticYN.InternationalAccount,
                            streetAddress1: request.residentialStreetAddress1,
                            streetAddress2: request.residentialStreetAddress2,
                            city: request.residentialCity,
                            state: state,
                            zip: request.residentialZipCode,
                            country: request.residentialCountry,
                            KYCstatus: KYCStatus.Pending,
                            AMLstatus: AmlStatus.Pending,
                            AccreditedStatus: AccreditedStatus.Pending,
                            approvalStatus: ApprovalStatus.Approved,
                            email: user.email,
                            phone: request.phoneCountryCode
                                ? `+${request.phoneCountryCode || 1} ${request.phone.toString()}`
                                : request.phone.toString(),
                        },
                        NcInputContext.GetAccount,
                    ),
                )
            ).accountDetails[0],
            NcOutputContext.GetAccount,
        );

        const partyDetails = ncOutput(
            (
                await this.partiesApi.createParty(
                    ncInputObjectPipe(
                        {
                            emailAddress: user.email,
                            firstName: request.firstName,
                            lastName: request.lastName,
                            dob: request.dateOfBirth,
                            socialSecurityNumber: request.ssn || '',
                            phone: request.phone,
                            domicile: request.citizenship,
                            primCountry: request.residentialCountry,
                            primState: state,
                            primCity: request.residentialCity,
                            primZip: request.residentialZipCode,
                            primAddress1: request.residentialStreetAddress1,
                            primAddress2: request.residentialStreetAddress2,
                        },
                        NcInputContext.GetParty,
                    ),
                )
            ).partyDetails[1][0],
            NcOutputContext.GetParty,
        );

        await this.updateAccountCustomFields(accountDetails.accountId, {
            phoneCountryNumericCode: request.phoneCountryNumericCode,
        });

        await this.updatePartyCustomFields(partyDetails.partyId, {
            phoneCountryCode: request.phoneCountryCode,
        });

        if (shouldUploadSSN) {
            const ssnDocumentUrl = await this.awsS3Service.presignedUrl({
                s3ObjectId: session.ssnDocumentAwsKey,
                ttlSeconds: 10 /* minutes */ * 60 /* seconds */,
            });

            const awsDocument = await fetch(ssnDocumentUrl);
            const awsDocumentBlob = await awsDocument.buffer();
            const awsDocumentFileName = session.ssnDocumentFileName;

            await this.documentsApi.uploadPartyDocument(
                {
                    partyId: partyDetails.partyId,
                    file_name: awsDocumentFileName,
                    documentTitle: awsDocumentFileName,
                },
                awsDocumentBlob,
            );

            if (session.ssnDocumentAwsKey) {
                await this.awsS3Service.deleteWithAwsKey(session.ssnDocumentAwsKey);

                session.ssnDocumentAwsKey = undefined;

                await this.session.save(user.id, session);
            }
        }

        await this.linksApi.createLink({
            firstEntry: accountDetails.accountId,
            firstEntryType: FirstEntryType.Account,
            relatedEntry: partyDetails.partyId,
            relatedEntryType: RelatedEntryType.IndivACParty,
            linkType: LinkType.Owner,
            primary_value: 1,
        });

        const createInvestorAccount = async (ncAccountId: string, ncPartyId: string) => {
            const entity = await this.investorAccountService.upsertInvestorAccountFor(user);

            entity.northCapitalAccountId = ncAccountId;
            entity.northCapitalPartyId = ncPartyId;
            entity.isBankAccountLinked = false;
            entity.isInvestorVerified = false;
            entity.isAccreditedInvestor = false;

            if (shouldUploadSSN) {
                const partyDocuments = await (async () => {
                    try {
                        return await this.partiesApi.getPartyDocument({
                            partyId: partyDetails.partyId,
                        });
                    } catch (err) {
                        if (isNorthCapitalException(err, NorthCapitalErrorCodes.AccountCreditCardDetailsDoesnTExistActive)) {
                            return {
                                partyDocumentDetails: [],
                            };
                        } else {
                            throw err;
                        }
                    }
                })();

                if (partyDocuments && partyDocuments.partyDocumentDetails.length) {
                    const latestDocument = partyDocuments.partyDocumentDetails[partyDocuments.partyDocumentDetails.length - 1];

                    if (latestDocument) {
                        entity.ssnDocumentId = latestDocument.documentid;
                        entity.ssnDocumentFileName = latestDocument.documentTitle;
                    }
                }
            }

            entity.isForeignInvestor = !(await this.dwollaService.canHaveDwollaCustomer(user.id, undefined, partyDetails.partyId));

            await this.investorAccountRepository.save(entity);
        };

        await createInvestorAccount(accountDetails.accountId, partyDetails.partyId);

        return {
            accountDetails,
            partyDetails,
        };
    }

    /**
     * Updates NC Account with updated Personal Information details
     * @param user
     * @param request
     */
    async updateNCAccount(
        user: AccountEntity,
        request: AccountVerificationSession,
    ): Promise<{
        accountDetails: AccountDetails;
        partyDetails: PartyDetails;
        diffRequest: Partial<AccountVerificationPersonalInformationDto>;
    }> {
        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(user.id);

        const account = ncOutput(
            (
                await this.accountsApi.getAccount({
                    accountId: investorAccount.northCapitalAccountId,
                })
            ).accountDetails,
            NcOutputContext.GetAccount,
        );

        const party = ncOutput(
            (
                await this.partiesApi.getParty({
                    partyId: investorAccount.northCapitalPartyId,
                })
            ).partyDetails[0],
            NcOutputContext.GetParty,
        );

        const diffRequest = this.diffRequestWithActualData(request, account, party);

        if (Object.values(diffRequest).length > 0) {
            if (diffRequest.hasSsn === true) {
                investorAccount.ssnDocumentFileName = null;
                investorAccount.ssnDocumentId = null;
            }

            if (
                diffRequest.hasSsn === false &&
                party.socialSecurityNumber &&
                party.socialSecurityNumber !== BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER
            ) {
                diffRequest.ssn = BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER;
            }

            if (!diffRequest.residentialStreetAddress2 && diffRequest.residentialStreetAddress2 !== undefined) {
                diffRequest.residentialStreetAddress2 = BAZA_NC_ACCOUNT_VERIFICATION_PRIM_ADDRESS_PLACEHOLDER;
            }

            const state = isNotNullOrUndefined(diffRequest.residentialState)
                ? isEmpty(diffRequest.residentialState)
                    ? BAZA_NC_ACCOUNT_VERIFICATION_STATE_PLACEHOLDER
                    : diffRequest.residentialState
                : undefined;

            const accountDetails = (
                await this.accountsApi.updateAccount(
                    ncInputObjectPipe(
                        {
                            accountId: investorAccount.northCapitalAccountId,
                            accountRegistration:
                                diffRequest.firstName && diffRequest.lastName
                                    ? `${diffRequest.firstName} ${diffRequest.lastName}`
                                    : account.accountName,
                            streetAddress1: diffRequest.residentialStreetAddress1,
                            streetAddress2: diffRequest.residentialStreetAddress2,
                            city: diffRequest.residentialCity,
                            state,
                            zip: diffRequest.residentialZipCode,
                            domesticYN: (() => {
                                if (diffRequest.citizenship) {
                                    return [Domicile.USCitizen, Domicile.USResident].includes(diffRequest.citizenship)
                                        ? DomesticYN.DomesticAccount
                                        : DomesticYN.InternationalAccount;
                                } else {
                                    return undefined;
                                }
                            })(),
                            country: diffRequest.residentialCountry,
                            email: user.email,
                            phone: (() => {
                                if (diffRequest.phone && diffRequest.phoneCountryCode) {
                                    return `+${diffRequest.phoneCountryCode || 1} ${diffRequest.phone.toString()}`;
                                } else if (diffRequest.phone) {
                                    return diffRequest.phone.toString();
                                } else {
                                    return undefined;
                                }
                            })(),
                        },
                        NcInputContext.GetAccount,
                    ),
                )
            ).accountDetails;

            const updatePartyRequest: UpdatePartyRequest = ncInputObjectPipe(
                {
                    emailAddress: user.email !== party.emailAddress ? user.email : undefined,
                    partyId: investorAccount.northCapitalPartyId,
                    firstName: diffRequest.firstName,
                    lastName: diffRequest.lastName,
                    dob: diffRequest.dateOfBirth,
                    socialSecurityNumber: diffRequest.ssn,
                    phone: diffRequest.phone,
                    domicile: diffRequest.citizenship,
                    primCountry: diffRequest.residentialCountry,
                    primState: diffRequest.residentialState,
                    primCity: diffRequest.residentialCity,
                    primZip: diffRequest.residentialZipCode,
                    primAddress1: diffRequest.residentialStreetAddress1,
                    primAddress2: diffRequest.residentialStreetAddress2,
                },
                NcInputContext.GetParty,
            );

            for (const key of Object.keys(updatePartyRequest)) {
                if (key === 'primAddress2') {
                    continue;
                }

                if (isEmpty(updatePartyRequest[key])) {
                    delete updatePartyRequest[key];
                }
            }

            const partyDetails = (await this.partiesApi.updateParty(updatePartyRequest)).partyDetails[1][0];

            const updateInvestorAccount = async (ncAccountId: string, ncPartyId: string) => {
                investorAccount.northCapitalAccountId = ncAccountId;
                investorAccount.northCapitalPartyId = ncPartyId;

                if (diffRequest.hasSsn === true) {
                    investorAccount.ssnDocumentId = null;
                    investorAccount.ssnDocumentFileName = null;
                }

                investorAccount.isForeignInvestor = !(await this.dwollaService.canHaveDwollaCustomer(
                    investorAccount.user.id,
                    undefined,
                    partyDetails.partyId,
                ));

                await this.investorAccountRepository.save(investorAccount);
            };

            await this.updateAccountCustomFields(investorAccount.northCapitalAccountId, {
                phoneCountryNumericCode: diffRequest.phoneCountryNumericCode,
            });

            await this.updatePartyCustomFields(investorAccount.northCapitalPartyId, {
                phoneCountryCode: diffRequest.phoneCountryCode,
            });

            await updateInvestorAccount(accountDetails.accountId, partyDetails.partyId);

            return {
                accountDetails,
                partyDetails,
                diffRequest,
            };
        } else {
            return {
                diffRequest,
                partyDetails: party,
                accountDetails: account,
            };
        }
    }

    /**
     * Updates SSN Document, Additional Document and KYC flags / statuses
     * @param user
     * @param session
     * @param request
     * @param setupResponse
     */
    async updateDocumentsAndKyc(
        user: AccountEntity,
        session: AccountVerificationSession,
        request: AccountVerificationPersonalInformationDto,
        setupResponse: SetupResponse,
    ): Promise<void> {
        if (request.hasSsn === false || request.ssn === '' || request.ssn === BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER) {
            const hasUploadedSSNDocument = !!session.ssnDocumentUrl;

            if (!hasUploadedSSNDocument) {
                setupResponse.shouldRequestSSNDocument = true;
                session.hasRequestForSSNDocument = true;

                await this.session.save(user.id, session);
            }
        }

        if (!request.hasSsn && request.hasSsnDocument && session.hasRequestForSSNDocument) {
            setupResponse.shouldRequestSSNDocument = false;
            session.hasRequestForSSNDocument = false;

            await this.session.save(user.id, session);
        }

        if (session.hasSsn && session.hasRequestForSSNDocument) {
            session.hasRequestForSSNDocument = false;

            await this.session.save(user.id, session);
        }

        if (setupResponse.isSavedToNC) {
            const updateAdditionalDocumentFlags = async (kycStatus: KYCStatus) => {
                const kycStatusRequiredToUploadAdditionalDocuments = [KYCStatus.Disapproved].includes(kycStatus);

                const isApproved = [KYCStatus.AutoApproved, KYCStatus.Approved, KYCStatus.ManuallyApproved].includes(kycStatus);

                if (isApproved) {
                    session.hasRequestForAdditionalDocuments = false;
                    setupResponse.shouldRequestDocuments = false;

                    await this.session.save(user.id, session);
                } else if (kycStatusRequiredToUploadAdditionalDocuments) {
                    if (!session.hasSsnDocument && !session.hasAdditionalDocument) {
                        if (
                            session.hasSsn === false ||
                            isEmpty(session.ssn) ||
                            session.ssn === BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER
                        ) {
                            session.hasRequestForSSNDocument = true;
                            setupResponse.shouldRequestSSNDocument = true;

                            await this.session.save(user.id, session);
                        } else {
                            session.hasRequestForAdditionalDocuments = true;
                            setupResponse.shouldRequestDocuments = true;

                            await this.session.save(user.id, session);
                        }
                    }
                }
            };

            const currentKyc = await this.kyc.getKyc(user);

            await updateAdditionalDocumentFlags(currentKyc);
            await this.updateIsInvestorVerifiedFlag(user, currentKyc);

            const shouldUpdateKycAml =
                setupResponse.wasCreated ||
                (
                    [
                        'firstName',
                        'lastName',
                        'dateOfBirth',
                        'ssn',
                        'hasSsn',
                        'residentialCity',
                        'residentialState',
                        'residentialCountry',
                        'residentialStreetAddress1',
                        'residentialStreetAddress2',
                        'residentialZipCode',
                    ] as Array<keyof AccountVerificationPersonalInformationDto>
                )
                    // eslint-disable-next-line no-prototype-builtins
                    .some((field) => request.hasOwnProperty(field));

            if (shouldUpdateKycAml) {
                const newKyc = await this.kyc.updateKycAml(user, {
                    forceKYCAml: true,
                });

                await updateAdditionalDocumentFlags(newKyc);
                await this.updateIsInvestorVerifiedFlag(user, newKyc);
            }
        }
    }

    /**
     * Compares current NC Account and NC Party details with Request and returns a Diff Request which will contains
     * only fields which should be updated on NC side
     * @param request
     * @param accountDetails
     * @param partyDetails
     * @private
     */
    private diffRequestWithActualData(
        request: AccountVerificationPersonalInformationDto,
        accountDetails: AccountDetails,
        partyDetails: PartyDetails,
    ): Partial<AccountVerificationPersonalInformationDto> {
        const detectors: Array<{
            field: keyof AccountVerificationPersonalInformationDto;
            detect: () => boolean;
        }> = [
            {
                field: 'firstName',
                detect: () => partyDetails.firstName !== request.firstName,
            },
            {
                field: 'lastName',
                detect: () => partyDetails.lastName !== request.lastName,
            },
            {
                field: 'firstName',
                detect: () => partyDetails.dob !== request.dateOfBirth,
            },
            {
                field: 'hasSsn',
                detect: () => {
                    const hasSsn =
                        !!partyDetails.socialSecurityNumber &&
                        partyDetails.socialSecurityNumber !== BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER;

                    return hasSsn !== request.hasSsn;
                },
            },
            {
                field: 'ssn',
                detect: () => {
                    const hasSsnInRequest = request.ssn && request.ssn !== BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER;
                    const hasSsnInParty =
                        partyDetails.socialSecurityNumber &&
                        partyDetails.socialSecurityNumber !== BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER;

                    if (hasSsnInRequest) {
                        return !hasSsnInParty || partyDetails.socialSecurityNumber !== request.ssn;
                    } else {
                        return request.ssn !== undefined ? hasSsnInParty : false;
                    }
                },
            },
            {
                field: 'phone',
                detect: () => {
                    if (!request.phoneCountryCode && !partyDetails.field3) {
                        return false;
                    }

                    const phoneToSave = `+${request.phoneCountryCode} ${request.phone.toString()}`;

                    return `+${partyDetails.field3} ${partyDetails.phone}` !== phoneToSave;
                },
            },
            {
                field: 'phoneCountryCode',
                detect: () => {
                    if (!request.phone && !partyDetails.phone) {
                        return false;
                    }

                    return partyDetails.field3 !== request.phoneCountryCode;
                },
            },
            {
                field: 'phoneCountryNumericCode',
                detect: () => accountDetails.field1 !== request.phoneCountryNumericCode,
            },
            {
                field: 'dateOfBirth',
                detect: () => partyDetails.dob !== request.dateOfBirth,
            },
            {
                field: 'citizenship',
                detect: () => partyDetails.domicile !== request.citizenship,
            },
            {
                field: 'residentialStreetAddress1',
                detect: () => partyDetails.primAddress1 !== request.residentialStreetAddress1,
            },
            {
                field: 'residentialStreetAddress2',
                detect: () => {
                    const hasRequestStreetAddress2 =
                        !!request.residentialStreetAddress2 &&
                        request.residentialStreetAddress2 !== BAZA_NC_ACCOUNT_VERIFICATION_PRIM_ADDRESS_PLACEHOLDER;
                    const hasPartyStreetAddress2 =
                        !!partyDetails.primAddress2 && partyDetails.primAddress2 !== BAZA_NC_ACCOUNT_VERIFICATION_PRIM_ADDRESS_PLACEHOLDER;

                    if (hasRequestStreetAddress2) {
                        return !hasPartyStreetAddress2 || partyDetails.primAddress2 !== request.residentialStreetAddress2;
                    } else {
                        return request.residentialStreetAddress2 !== undefined ? hasPartyStreetAddress2 : false;
                    }
                },
            },
            {
                field: 'residentialCity',
                detect: () => partyDetails.primCity !== request.residentialCity,
            },
            {
                field: 'residentialState',
                detect: () => partyDetails.primState !== request.residentialState,
            },
            {
                field: 'residentialZipCode',
                detect: () => partyDetails.primZip !== request.residentialZipCode,
            },
            {
                field: 'residentialCountry',
                detect: () => partyDetails.primCountry !== request.residentialCountry,
            },
        ];

        const result: Partial<AccountVerificationPersonalInformationDto> = {};

        for (const def of detectors) {
            // eslint-disable-next-line no-prototype-builtins
            if (request.hasOwnProperty(def.field) && def.detect()) {
                result[def.field as any] = request[def.field];
            }
        }

        return result;
    }

    /**
     * Updates custom field1/field2/field3 of NC Account
     * @param ncAccountId
     * @param patch
     */
    async updateAccountCustomFields(
        ncAccountId: NorthCapitalAccountId,
        patch: {
            phoneCountryNumericCode?: string;
        } = {},
    ): Promise<void> {
        const account = (await this.accountsApi.getAccount({ accountId: ncAccountId })).accountDetails;

        await this.accountsApi.updateAccount({
            accountId: ncAccountId,
            accountRegistration: account.accountName,
            field1: patch.phoneCountryNumericCode ? patch.phoneCountryNumericCode : account.field1,
        });
    }

    /**
     * Updates custom field1/field2/field3 of NC Party
     * It's required for DocuSign templates
     * @param ncPartyId
     * @param patch
     */
    async updatePartyCustomFields(
        ncPartyId: PartyId,
        patch: {
            phoneCountryCode?: string;
        } = {},
    ): Promise<void> {
        const party = (
            await this.partiesApi.getParty({
                partyId: ncPartyId,
            })
        ).partyDetails[0];

        await this.partiesApi.updateParty({
            partyId: ncPartyId,
            field1:
                party.socialSecurityNumber && party.socialSecurityNumber !== BAZA_NC_ACCOUNT_VERIFICATION_SSN_PLACEHOLDER
                    ? party.socialSecurityNumber
                    : ' ',
            field2: `${party.primCity} ${party.primState} ${party.primZip}`,
            field3: patch.phoneCountryCode ? patch.phoneCountryCode.toString() : undefined,
        });
    }

    /**
     * Updates `isAccountVerificationCompleted` and `isAccountVerificationInProgress` flags for Investor Account
     * @param user
     */
    async updateAccountVerificationInProgressAndCompletedFlags(user: AccountEntity): Promise<void> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(user.id);

        if (!investorAccount) {
            return;
        }

        if (investorAccount.isAccountVerificationCompleted && investorAccount.isAccountVerificationInProgress) {
            investorAccount.isAccountVerificationInProgress = false;

            await this.investorAccountRepository.save(investorAccount);
        } else if (!investorAccount.isAccountVerificationCompleted) {
            const currentStep = await this.status.getCurrentStep(user);
            const isInProgress = currentStep !== AccountVerificationStep.LegalName || (await this.session.hasSession(user.id));

            if (currentStep === AccountVerificationStep.Completed) {
                investorAccount.isAccountVerificationCompleted = true;
                investorAccount.isAccountVerificationInProgress = false;

                await this.investorAccountRepository.save(investorAccount);

                this.eventBus.publish(
                    new AccountVerificationCompletedEvent({
                        userId: user.id,
                        ncAccountId: investorAccount.northCapitalAccountId,
                        ncPartyId: investorAccount.northCapitalPartyId,
                    }),
                );
            } else if (isInProgress) {
                investorAccount.isAccountVerificationCompleted = false;
                investorAccount.isAccountVerificationInProgress = true;

                await this.investorAccountRepository.save(investorAccount);
            } else {
                investorAccount.isAccountVerificationCompleted = false;
                investorAccount.isAccountVerificationInProgress = false;
            }
        }
    }

    /**
     * Updates `isInvestorVerified` flag for Investor
     * @param user
     * @param kycStatus
     */
    async updateIsInvestorVerifiedFlag(user: AccountEntity, kycStatus: KYCStatus): Promise<void> {
        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(user.id);

        investorAccount.isInvestorVerified = [KYCStatus.AutoApproved, KYCStatus.Approved, KYCStatus.ManuallyApproved].includes(kycStatus);

        await this.investorAccountRepository.save(investorAccount);
    }
}
