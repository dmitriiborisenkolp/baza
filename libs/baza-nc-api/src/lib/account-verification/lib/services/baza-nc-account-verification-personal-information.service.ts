import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { BazaNcAccountVerificationAccountSetupService } from './baza-nc-account-verification-account-setup.service';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationSessionService } from './baza-nc-account-verification-session.service';
import { AccountVerificationPersonalInformationDto } from '@scaliolabs/baza-nc-shared';

export interface ApplyPersonalInformationResponse {
    shouldRequestDocuments: boolean;
    shouldRequestSSNDocument: boolean;
}

/**
 * Account Verification Service - Personal Information step
 */
@Injectable()
export class BazaNcAccountVerificationPersonalInformationService {
    constructor(
        private readonly connection: Connection,
        private readonly session: BazaNcAccountVerificationSessionService,
        private readonly accountSetup: BazaNcAccountVerificationAccountSetupService,
    ) {}

    /**
     * Updates Personal Information data for Account Verification
     * @param user
     * @param request
     */
    public async applyPersonalInformation(
        user: AccountEntity,
        request: AccountVerificationPersonalInformationDto,
    ): Promise<ApplyPersonalInformationResponse> {
        const setupResponse = await this.accountSetup.save(user, request);

        if (setupResponse.isSavedToNC) {
            return {
                shouldRequestDocuments: setupResponse.shouldRequestDocuments,
                shouldRequestSSNDocument: setupResponse.shouldRequestSSNDocument,
            };
        } else {
            return {
                shouldRequestDocuments: false,
                shouldRequestSSNDocument: setupResponse.shouldRequestSSNDocument,
            };
        }
    }
}
