import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { PartyId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationSessionEntity } from '../../../typeorm';

export type AccountVerificationSession = Partial<BazaNcAccountVerificationSessionEntity>;

/**
 * Account Verification Session Service
 * Account Verification Session is responsible to temporary store Account Verification data during Personal Information
 * screens. As far as it's no possible to create NC Account / NC Party with only part of all required fields, we store
 * this information in Redis. Once there is enough data to create NC Account / NC Party, we remove this temporary
 * session
 */
@Injectable()
export class BazaNcAccountVerificationSessionService {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeORM Repository for BazaNcAccountVerificationSessionEntity
     */
    get repository(): Repository<BazaNcAccountVerificationSessionEntity> {
        return this.connection.getRepository(BazaNcAccountVerificationSessionEntity);
    }

    /**
     * Returns Account Verification Session
     * Account Verification session temporarily stores information filled within Personal Information screen(s) in
     * DB. Once there is enough data to save it on NC side, API will create NC Account and NC Party and destroys
     * this temporary session
     * @param userId
     */
    async getSession(userId: number): Promise<AccountVerificationSession> {
        const session = await this.repository.findOne({
            where: [
                {
                    userId,
                },
            ],
        });

        if (session) {
            await this.process(session);

            return session;
        } else {
            const newEntity = new BazaNcAccountVerificationSessionEntity();

            newEntity.userId = userId;

            await this.repository.save(newEntity);

            return newEntity;
        }
    }

    /**
     * Returns true if there is an Account Verification Session saved previously for Account
     * @param userId
     */
    async hasSession(userId: number): Promise<boolean> {
        const session = await this.repository.findOne({
            where: [
                {
                    userId,
                },
            ],
        });

        return !!session;
    }

    /**
     * Returns Account Verification Session or `undefined` by NC Party ID
     * @param ncPartyId
     */
    async findSessionByPartyId(ncPartyId: PartyId): Promise<AccountVerificationSession | undefined> {
        const session = await this.repository.findOne({
            where: [
                {
                    ncPartyId,
                },
            ],
        });

        if (session) {
            await this.process(session);

            return session;
        } else {
            return undefined;
        }
    }

    /**
     * Update Account Verification Session
     * @param userId
     * @param session
     */
    async save(userId: number, session: AccountVerificationSession): Promise<AccountVerificationSession> {
        const existing = await this.getSession(userId);

        Object.assign(existing, session);

        await this.repository.save(existing);

        return existing;
    }

    /**
     * Returns true if Account Verification Session has enough data for NC Account and NC Party creation
     * @param session
     */
    isSessionReadyToBeSentToNorthCapital(session: AccountVerificationSession): boolean {
        return (
            !!session.firstName &&
            !!session.lastName &&
            !!session.residentialStreetAddress1 &&
            !!session.residentialCity &&
            !!session.residentialZipCode &&
            !!session.residentialCountry &&
            !!session.dateOfBirth
        );
    }

    /**
     * Updates Account Verification Session (SSN Flags)
     * @param session
     */
    private async process(session: AccountVerificationSession): Promise<void> {
        if (session.hasSsn && !session.ssn) {
            session.hasSsn = false;
        }

        if (!session.hasSsn && session.ssn) {
            session.ssn = '';
        }

        if (session.hasSsn === true) {
            session.ssnDocumentAwsKey = null;
            session.ssnDocumentUrl = null;
            session.ssnDocumentFileName = null;
            session.hasSsnDocument = false;
        }

        await this.repository.save(session);
    }
}
