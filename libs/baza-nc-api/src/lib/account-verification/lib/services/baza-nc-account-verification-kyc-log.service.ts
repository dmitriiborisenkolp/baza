import { Injectable } from '@nestjs/common';
import { KYCStatus, NorthCapitalAccountId, PartyId } from '@scaliolabs/baza-nc-shared';
import { Connection, Repository } from 'typeorm';
import { KycLogDto, NcAccountVerificationKycLogsRequest } from '@scaliolabs/baza-nc-shared';
import { CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { CrudService } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationKycLogMapper } from '../mappers/baza-nc-account-verification-kyc-log.mapper';
import { BazaNcAccountVerificationKycLogEntity } from '../../../typeorm';

interface LogRequest {
    userId: number;
    userEmail: string;
    userFullName: string;
    ncAccountId: NorthCapitalAccountId;
    ncPartyId: PartyId;
    previousKycStatus: KYCStatus;
    newKycStatus: KYCStatus;
    kycDetails: any;
}

@Injectable()
export class BazaNcAccountVerificationKycLogService {
    constructor(
        private readonly connection: Connection,
        private readonly crud: CrudService,
        private readonly kycLogMapper: BazaNcAccountVerificationKycLogMapper,
    ) {}

    get repository(): Repository<BazaNcAccountVerificationKycLogEntity> {
        return this.connection.getRepository(BazaNcAccountVerificationKycLogEntity);
    }

    async log(request: LogRequest): Promise<void> {
        const newEntity = new BazaNcAccountVerificationKycLogEntity();

        newEntity.createdAt = new Date();
        newEntity.userId = request.userId;
        newEntity.userEmail = request.userEmail;
        newEntity.userFullName = request.userFullName;
        newEntity.ncAccountId = request.ncAccountId;
        newEntity.ncPartyId = request.ncPartyId;
        newEntity.previousKycStatus = request.previousKycStatus;
        newEntity.newKycStatus = request.newKycStatus;
        newEntity.kycDetails = request.kycDetails;

        await this.repository.save(newEntity);
    }

    async list(request: NcAccountVerificationKycLogsRequest): Promise<CrudListResponseDto<KycLogDto>> {
        return this.crud.find<BazaNcAccountVerificationKycLogEntity, KycLogDto>({
            request,
            entity: BazaNcAccountVerificationKycLogEntity,
            mapper: async (items) => this.kycLogMapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
            },
        });
    }
}
