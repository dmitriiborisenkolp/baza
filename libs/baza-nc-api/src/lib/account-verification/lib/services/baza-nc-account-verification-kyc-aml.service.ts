import { Injectable } from '@nestjs/common';
import { KYCStatus } from '@scaliolabs/baza-nc-shared';
import { map } from 'rxjs/operators';
import { BazaNcAccountVerificationKycLogService } from './baza-nc-account-verification-kyc-log.service';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcNoPartyIdAvailableYetException } from '../exceptions/baza-nc-no-party-id-available-yet.exception';
import { NorthCapitalKycAmlNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountService } from '../../../investor-account';

@Injectable()
export class BazaNcAccountVerificationKycAmlService {
    constructor(
        private readonly kycLog: BazaNcAccountVerificationKycLogService,
        private readonly ncKycAmlApi: NorthCapitalKycAmlNestjsService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
    ) {}

    async getKyc(user: AccountEntity): Promise<KYCStatus> {
        if (!(await this.investorAccountService.isUserHasInvestorAccount(user.id))) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(user.id);

        if (!investorAccount.northCapitalPartyId) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        return (
            await this.ncKycAmlApi.getKycAml({
                partyId: investorAccount.northCapitalPartyId,
            })
        )['KYC Status'];
    }

    async updateKycAml(
        user: AccountEntity,
        options: {
            forceKYCAml: boolean;
        } = {
            forceKYCAml: false,
        },
    ): Promise<KYCStatus> {
        if (!(await this.investorAccountService.isUserHasInvestorAccount(user.id))) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(user.id);

        if (!investorAccount.northCapitalPartyId) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        const partyId = investorAccount.northCapitalPartyId;

        const currentKycAmlStatus = (
            await this.ncKycAmlApi.getKycAml({
                partyId,
            })
        )['KYC Status'];

        const shouldPerform =
            options.forceKYCAml ||
            (currentKycAmlStatus !== KYCStatus.Pending &&
                ![KYCStatus.Approved, KYCStatus.AutoApproved, KYCStatus.ManuallyApproved].includes(currentKycAmlStatus));

        if (shouldPerform) {
            const kycResponse = await this.ncKycAmlApi.performKycAmlBasic({
                partyId,
            });

            await this.kycLog.log({
                userId: user.id,
                userEmail: user.email,
                userFullName: `${user.firstName} ${user.lastName}`,
                ncAccountId: investorAccount && investorAccount.northCapitalAccountId ? investorAccount.northCapitalAccountId : 'UNKNOWN',
                ncPartyId: partyId,
                previousKycStatus: currentKycAmlStatus,
                newKycStatus: kycResponse.kyc.kycstatus,
                kycDetails: kycResponse.kyc,
            });

            return kycResponse.kyc.kycstatus;
        } else {
            return currentKycAmlStatus;
        }
    }
}
