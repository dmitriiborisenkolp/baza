import { Injectable } from '@nestjs/common';
import { AccountVerificationInvestorProfileDto, AccreditedStatus, NorthCapitalYesNoBoolean } from '@scaliolabs/baza-nc-shared';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcNoPartyIdAvailableYetException } from '../exceptions/baza-nc-no-party-id-available-yet.exception';
import {
    ncOutput,
    NcOutputContext,
    NorthCapitalAccountsApiNestjsService,
    NorthCapitalPartiesApiNestjsService,
} from '../../../transact-api';
import { BazaNcInvestorAccountService } from '../../../investor-account';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { BazaNcAccountVerificationAccountSetupService } from './baza-nc-account-verification-account-setup.service';

/**
 * Account Verification Investor Profile Service
 * This service is responsible for Suitability questions, i.e. 2nd step of Account Verification
 */
@Injectable()
export class BazaNcAccountVerificationInvestorProfileService {
    constructor(
        private readonly accountSetup: BazaNcAccountVerificationAccountSetupService,
        private readonly partiesApi: NorthCapitalPartiesApiNestjsService,
        private readonly accountsApi: NorthCapitalAccountsApiNestjsService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    /**
     * Applies Investor Profile to Account Verification
     * @param user
     * @param request
     */
    public async applyInvestorProfile(user: AccountEntity, request: AccountVerificationInvestorProfileDto): Promise<void> {
        if (!(await this.investorAccountService.isUserHasInvestorAccount(user.id))) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(user.id);

        if (!investorAccount.northCapitalPartyId) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        const account = ncOutput(
            (
                await this.accountsApi.getAccount({
                    accountId: investorAccount.northCapitalAccountId,
                })
            ).accountDetails,
            NcOutputContext.GetAccount,
        );

        await this.partiesApi.updateParty({
            partyId: investorAccount.northCapitalPartyId,
            associatedPerson: request.isAssociatedWithFINRA ? NorthCapitalYesNoBoolean.Yes : NorthCapitalYesNoBoolean.No,
            currentHouseholdIncome: request.currentAnnualHouseholdIncome,
            householdNetworth: request.netWorth,
        });

        await this.accountsApi.updateAccount({
            accountId: investorAccount.northCapitalAccountId,
            accountRegistration: account.accountName,
            AccreditedStatus: request.isAccreditedInvestor ? AccreditedStatus.SelfAccredited : AccreditedStatus.NotAccredited,
        });

        investorAccount.isAccreditedInvestor = request.isAccreditedInvestor;

        await this.investorAccountRepository.save(investorAccount);
        await this.accountSetup.updateAccountVerificationInProgressAndCompletedFlags(user);
    }
}
