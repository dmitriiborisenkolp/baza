import { Injectable } from '@nestjs/common';
import { AccountVerificationInvalidDocumentExtensionException } from '../exceptions/baza-nc-invalid-document-extension.exception';
import * as path from 'path';
import { ExpressMulterFile } from '../models';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationSessionService } from './baza-nc-account-verification-session.service';
import { BazaNcNoPartyIdAvailableYetException } from '../exceptions/baza-nc-no-party-id-available-yet.exception';
import { isDocumentValidForUploadToNorthCapital } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalDocumentsApiNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountService } from '../../../investor-account';
import { BazaNcAccountVerificationAccountSetupService } from './baza-nc-account-verification-account-setup.service';

/**
 * Account Verification Additional Documents Service
 * This service is used for uploading Additional documents during Account Verification
 */
@Injectable()
export class BazaNcAccountVerificationRequestDocumentsService {
    constructor(
        private readonly session: BazaNcAccountVerificationSessionService,
        private readonly documentsApi: NorthCapitalDocumentsApiNestjsService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly accountVerificationSetup: BazaNcAccountVerificationAccountSetupService,
    ) {}

    /**
     * Uploads additional document to NC
     * @param user
     * @param file
     */
    public async uploadPersonalInformationDocument(user: AccountEntity, file: ExpressMulterFile): Promise<void> {
        if (!(await this.investorAccountService.isUserHasInvestorAccount(user.id))) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(user.id);

        if (!investorAccount.northCapitalPartyId) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        if (!isDocumentValidForUploadToNorthCapital(file.originalname)) {
            throw new AccountVerificationInvalidDocumentExtensionException();
        }

        const fileName = path.basename(file.originalname);
        const documentTitle = path.basename(fileName, path.extname(fileName));

        await this.documentsApi.uploadPartyDocument(
            {
                partyId: investorAccount.northCapitalPartyId,
                file_name: fileName,
                documentTitle,
            },
            file.buffer,
        );

        const session = await this.session.getSession(user.id);

        session.hasSsnDocument = true;
        session.hasAdditionalDocument = true;
        session.hasRequestForSSNDocument = false;
        session.hasRequestForAdditionalDocuments = false;

        await this.session.save(user.id, session);
        await this.accountVerificationSetup.updateAccountVerificationInProgressAndCompletedFlags(user);
    }
}
