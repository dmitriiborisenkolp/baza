import { Injectable } from '@nestjs/common';
import { BazaNcAccountVerificationAccountSetupService } from './baza-nc-account-verification-account-setup.service';
import { BazaNcAccountVerificationRequestDocumentsService } from './baza-nc-account-verification-request-documents.service';
import { AccountVerificationInvalidDocumentExtensionException } from '../exceptions/baza-nc-invalid-document-extension.exception';
import * as path from 'path';
import { ExpressMulterFile } from '../models';
import { AccountEntity, AwsService } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationSessionService } from './baza-nc-account-verification-session.service';
import { isDocumentValidForUploadToNorthCapital, NorthCapitalErrorCodes } from '@scaliolabs/baza-nc-shared';
import { BazaNcNoPartyIdAvailableYetException } from '../exceptions/baza-nc-no-party-id-available-yet.exception';
import { BazaNcNoSsnDocumentWasUploadedBeforeException } from '../exceptions/baza-nc-no-ssn-document-was-uploaded-before.exception';
import { isNorthCapitalException, NorthCapitalPartiesApiNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountService } from '../../../investor-account';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';

/**
 * Account Verification - SSN Document Service
 * The service is responsible for Uploading SSN Document or force-requesting user to upload SSN Document
 */
@Injectable()
export class BazaNcAccountVerificationRequestSsnDocumentsService {
    constructor(
        private readonly accountSetup: BazaNcAccountVerificationAccountSetupService,
        private readonly requestDocumentsService: BazaNcAccountVerificationRequestDocumentsService,
        private readonly partiesApi: NorthCapitalPartiesApiNestjsService,
        private readonly awsS3Service: AwsService,
        private readonly session: BazaNcAccountVerificationSessionService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    /**
     * Uploads SSN Document for Account Verification
     * @param user
     * @param file
     */
    public async uploadPersonalInformationSSNDocument(user: AccountEntity, file: ExpressMulterFile): Promise<void> {
        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(user);

        const session = await this.session.getSession(user.id);

        if (investorAccount && investorAccount.northCapitalAccountId && investorAccount.northCapitalPartyId) {
            await this.requestDocumentsService.uploadPersonalInformationDocument(user, file);

            await this.investorAccountRepository.save(investorAccount);

            const partyDocuments = await (async () => {
                try {
                    return await this.partiesApi.getPartyDocument({
                        partyId: investorAccount.northCapitalPartyId,
                    });
                } catch (err) {
                    if (isNorthCapitalException(err, NorthCapitalErrorCodes.AccountCreditCardDetailsDoesnTExistActive)) {
                        return {
                            partyDocumentDetails: [],
                        };
                    } else {
                        throw err;
                    }
                }
            })();

            if (partyDocuments && partyDocuments.partyDocumentDetails.length) {
                const latestDocument = partyDocuments.partyDocumentDetails[partyDocuments.partyDocumentDetails.length - 1];

                if (latestDocument) {
                    investorAccount.ssnDocumentId = latestDocument.documentid;
                    investorAccount.ssnDocumentFileName = latestDocument.documentTitle;

                    await this.investorAccountRepository.save(investorAccount);

                    await this.session.save(user.id, {
                        ...session,
                        hasSsnDocument: true,
                        ssnDocumentFileName: latestDocument.documentTitle,
                        ssnDocumentId: latestDocument.documentid,
                        hasRequestForSSNDocument: false,
                        hasRequestForAdditionalDocuments: false,
                    });
                }
            }
        } else {
            await this.tmpUploadSSNDocument(user, file);
        }

        await this.accountSetup.updateAccountVerificationInProgressAndCompletedFlags(user);
    }

    /**
     * Bypass SSN Document Request for Account Verification
     * Usually it's used by IOS application
     * @param user
     */
    public async usePreviouslyUploadedSSNDocument(user: AccountEntity): Promise<void> {
        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(user);

        if (!investorAccount.northCapitalPartyId) {
            throw new BazaNcNoPartyIdAvailableYetException();
        }

        if (!investorAccount.ssnDocumentFileName) {
            throw new BazaNcNoSsnDocumentWasUploadedBeforeException();
        }

        const session = await this.session.getSession(user.id);

        await this.session.save(user.id, {
            ...session,
            hasRequestForSSNDocument: false,
            hasRequestForAdditionalDocuments: false,
        });

        await this.accountSetup.updateAccountVerificationInProgressAndCompletedFlags(user);
    }

    /**
     * Temporary uploads SSN Document to AWS S3
     * We're uploading document to AWS for cases when it's not possible to create NC Account / Party yet, but we would
     * like to accept users SSN Document and store it somewhere
     * Once NC Account and Party created, we re-upload tmp file from AWS to NC and remove it from S3
     * @param user
     * @param file
     * @private
     */
    private async tmpUploadSSNDocument(user: AccountEntity, file: ExpressMulterFile): Promise<string> {
        if (!isDocumentValidForUploadToNorthCapital(file.originalname)) {
            throw new AccountVerificationInvalidDocumentExtensionException();
        }

        const session = await this.session.getSession(user.id);

        const document = await this.awsS3Service.uploadFromBuffer({
            fileBuffer: file.buffer,
            contentType: file.mimetype,
            fileName: file.originalname,
        });

        await this.accountSetup.save(user, {
            ...session,
            hasSsnDocument: true,
            ssnDocumentUrl: document.presignedTimeLimitedUrl,
            ssnDocumentFileName: path.basename(file.originalname),
            ssnDocumentAwsKey: document.s3ObjectId,
        });

        const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(user);

        investorAccount.ssnDocumentFileName = path.basename(file.originalname);

        await this.investorAccountRepository.save(investorAccount);

        return investorAccount.ssnDocumentFileName;
    }
}
