import { Injectable } from '@nestjs/common';
import { Country, Domicile, UsState } from '@scaliolabs/baza-nc-shared';
import { ncCountries, ncCountryStates, NcStateCountryMapDefinition } from '../constants/baza-nc-states-and-contries-list';
import {
    AccountVerificationFormResourcesDto,
    AccountVerificationStep,
    getCountryStatesList,
    isUSACountry,
} from '@scaliolabs/baza-nc-shared';

/**
 * Account Verification Form Resources Services
 * Returns resources which are used for FE implementation of Account Verification
 */
@Injectable()
export class BazaNcAccountVerificationFormResourcesService {
    /**
     * Returns resources which are used for FE implementation of Account Verification
     */
    async formResources(
        request: {
            primCountry?: string;
        } = {
            primCountry: Country.USA,
        },
    ): Promise<AccountVerificationFormResourcesDto> {
        return {
            stepsOrder: [
                AccountVerificationStep.PhoneNumber,
                AccountVerificationStep.LegalName,
                AccountVerificationStep.DateOfBirth,
                AccountVerificationStep.SSN,
                AccountVerificationStep.Citizenship,
                AccountVerificationStep.ResidentialAddress,
                AccountVerificationStep.InvestorProfile,
                AccountVerificationStep.Completed,
            ],
            countries: [Country.USA].map((country) => {
                return {
                    country,
                    isUSA: isUSACountry(country),
                    states: getCountryStatesList(country).map((state) => ({
                        title: state,
                        value: state,
                    })),
                };
            }),
            listCountries: ncCountries,
            primCountryStates: this.countriesList(request.primCountry).map((state) => ({
                title: state,
                value: state,
            })),
            statesMapping: this.statesCountryMapping(request.primCountry),
            citizenship: [
                {
                    citizenship: Domicile.USCitizen,
                    label: 'U.S. Citizen',
                },
                {
                    citizenship: Domicile.USResident,
                    label: 'U.S. Resident',
                },
                {
                    citizenship: Domicile.NonResident,
                    label: 'Non-U.S. Resident',
                },
            ],
        };
    }

    /**
     * Returns list of countries
     */
    countriesList(country: string): Array<string> {
        const find = isUSACountry(country) ? 'United States' : country;

        const definition = ncCountryStates.find((def) => def.country === find);

        return definition ? definition.states : [];
    }

    /**
     * Returns additional states -> country mapping (returns non-empty result for US only)
     */
    statesCountryMapping(country: string): Array<NcStateCountryMapDefinition> {
        const find = isUSACountry(country) ? 'United States' : country;

        const definition = ncCountryStates.find((def) => def.country === find);

        return definition ? definition.statesCountryMapping : [];
    }
}
