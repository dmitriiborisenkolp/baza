import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class AccountVerificationInvalidDocumentExtensionException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvalidDocumentExtension,
            'Invalid document extension (JPG, JPEG, PNG and PDF allowed)',
            HttpStatus.BAD_REQUEST,
        );
    }
}
