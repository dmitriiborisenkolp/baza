import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcDocumentWithGivenSidNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationDocumentWithGivenSIDNotFound,
            'Document with given sid was not found',
            HttpStatus.NOT_FOUND,
        );
    }
}
