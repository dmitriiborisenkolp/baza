import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcNoPartyIdAvailableYetException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationNoPartyIdAvailable,
            'No partyId available yet, please go to previous steps of account verification process',
            HttpStatus.BAD_REQUEST,
        );
    }
}
