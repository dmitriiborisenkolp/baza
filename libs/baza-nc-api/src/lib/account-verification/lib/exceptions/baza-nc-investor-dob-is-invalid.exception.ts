import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcInvestorDobIsInvalidException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBIsInvalid,
            'Investor DateOfBirth has invalid value (should be MM-DD-YYYY, Example: "01-31-2016")',
            HttpStatus.BAD_REQUEST,
        );
    }
}
