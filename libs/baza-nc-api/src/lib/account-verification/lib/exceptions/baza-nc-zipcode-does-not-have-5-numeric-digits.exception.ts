import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcZipcodeDoesNotHave5NumericDigitsException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits,
            'Your ZIP code does not have 5 numeric digits',
            HttpStatus.BAD_REQUEST,
        );
    }
}
