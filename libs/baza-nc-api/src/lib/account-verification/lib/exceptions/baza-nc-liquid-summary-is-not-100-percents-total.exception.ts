import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcAccountVerificationLiquidSummaryIsNot100PercentsTotalException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationLiquidSummaryIsNot100PercentsTotal,
            'Fields liquidPercents + illiquidPercents should be equal to 100',
            HttpStatus.BAD_REQUEST,
        );
    }
}
