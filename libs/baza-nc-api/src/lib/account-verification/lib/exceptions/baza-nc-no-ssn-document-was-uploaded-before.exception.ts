import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcNoSsnDocumentWasUploadedBeforeException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationNoSsnDocumentWasUploadedBefore,
            'No SSN document was uploaded before',
            HttpStatus.BAD_REQUEST,
        );
    }
}
