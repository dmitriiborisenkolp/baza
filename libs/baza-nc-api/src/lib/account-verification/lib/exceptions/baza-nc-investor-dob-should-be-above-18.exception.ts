import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcInvestorDobShouldBeAbove18Exception extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBShouldBeAbove18,
            'Investor should be 18 or older',
            HttpStatus.BAD_REQUEST,
        );
    }
}
