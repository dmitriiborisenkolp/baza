import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcInvestorDobYearIsInvalidException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationInvestorDOBYearShouldBeAfter1900,
            'Investor DateOfBirth year has an invalid value (should be greater than 1900, Example: "02-28-1975")',
            HttpStatus.BAD_REQUEST,
        );
    }
}
