import { forwardRef, Module } from '@nestjs/common';
import { BazaNcAccountVerificationApiModule } from './baza-nc-account-verification-api.module';
import { BazaNcAccountVerificationE2eUserFixture } from './integration-tests/fixtures/baza-nc-account-verification-e2e-user.fixture';
import { BazaNcAccountVerificationE2eUserFullFixture } from './integration-tests/fixtures/baza-nc-account-verification-e2e-user-full-fixture.service';
import { BazaNcAccountVerificationNoSsnE2eUserFixture } from './integration-tests/fixtures/baza-nc-account-verification-no-ssn-e2e-user-fixture.service';
import { BazaApiBundleModule, BazaE2eFixturesApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationE2eUser2Fixture } from './integration-tests/fixtures/baza-nc-account-verification-e2e-user-2.fixture';

const E2E_FIXTURES = [
    BazaNcAccountVerificationE2eUserFixture,
    BazaNcAccountVerificationE2eUser2Fixture,
    BazaNcAccountVerificationE2eUserFullFixture,
    BazaNcAccountVerificationNoSsnE2eUserFixture,
];

@Module({
    imports: [
        // TODO: Remove it after baza-core e2e modules migration
        forwardRef(() => BazaApiBundleModule),
        forwardRef(() => BazaE2eFixturesApiModule),
        forwardRef(() => BazaNcAccountVerificationApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcAccountVerificationE2eModule {}
