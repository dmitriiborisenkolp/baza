import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountVerificationKycStatusUpdateEvent, BazaNcWebhookEvent, NorthCapitalTopic } from '@scaliolabs/baza-nc-shared';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcWebhookEvent)
export class KycStatusEventHandler implements IEventHandler<BazaNcWebhookEvent> {
    constructor(private readonly eventBus: EventBus) {}

    handle(event: BazaNcWebhookEvent): void {
        cqrs(KycStatusEventHandler.name, async () => {
            const message = event.message;

            switch (message.topic) {
                case NorthCapitalTopic.updateParty: {
                    const partyId = message.responseBody.partyId;
                    const kycStatus = message.responseBody.kycStatus || message.responseBody.KYCstatus;

                    if (partyId && kycStatus) {
                        this.eventBus.publish(
                            new AccountVerificationKycStatusUpdateEvent({
                                partyId,
                                kycStatus,
                            }),
                        );
                    }

                    break;
                }

                case NorthCapitalTopic.updateKycAml: {
                    const partyId = message.responseBody['Financial Party details'].partyId;
                    const kycStatus =
                        message.responseBody['Financial Party details'].kycStatus ||
                        message.responseBody['Financial Party details'].KYCStatus;

                    if (partyId && kycStatus) {
                        this.eventBus.publish(
                            new AccountVerificationKycStatusUpdateEvent({
                                partyId,
                                kycStatus,
                            }),
                        );
                    }
                }
            }
        });
    }
}
