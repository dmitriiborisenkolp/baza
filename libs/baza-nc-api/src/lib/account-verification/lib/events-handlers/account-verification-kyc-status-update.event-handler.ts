import { KYCStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationSessionService } from '../services/baza-nc-account-verification-session.service';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountVerificationKycStatusUpdateEvent } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { cqrs } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationAccountSetupService } from '../services/baza-nc-account-verification-account-setup.service';

/**
 * Actions on KYC/AML Status Update (usually from NC Webhook)
 */
@EventsHandler(AccountVerificationKycStatusUpdateEvent)
export class AccountVerificationKycStatusUpdateEventHandler implements IEventHandler<AccountVerificationKycStatusUpdateEvent> {
    constructor(
        private readonly session: BazaNcAccountVerificationSessionService,
        private readonly setup: BazaNcAccountVerificationAccountSetupService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    handle(command: AccountVerificationKycStatusUpdateEvent): void {
        cqrs(AccountVerificationKycStatusUpdateEventHandler.name, async () => {
            await this.markInvestorAccountAsVerified(command);
            await this.requestAdditionalDocumentsOnDisapproved(command);
        });
    }

    /**
     * We mark Investor as Verified if KYC/AML is passed
     */
    private async markInvestorAccountAsVerified(command: AccountVerificationKycStatusUpdateEvent): Promise<void> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByNcPartyId(command.request.partyId);

        if (investorAccount) {
            await this.setup.updateIsInvestorVerifiedFlag(investorAccount.user, command.request.kycStatus);
        }
    }

    /**
     * When we're receiving failed KYC event, there could be old documents uploaded before by User
     * We mark Investor Account as has no additional documents uploaded to be sure that next next User will be asked
     * to upload more documents
     */
    private async requestAdditionalDocumentsOnDisapproved(command: AccountVerificationKycStatusUpdateEvent): Promise<void> {
        if ([KYCStatus.Disapproved, KYCStatus.NeedMoreInfo].includes(command.request.kycStatus)) {
            const session = await this.session.findSessionByPartyId(command.request.partyId);

            if (session) {
                session.hasAdditionalDocument = false;

                await this.session.save(session.userId, session);
            }
        }
    }
}
