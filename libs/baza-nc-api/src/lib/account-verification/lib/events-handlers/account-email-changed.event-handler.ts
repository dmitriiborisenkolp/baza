import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountApiEvent, AccountApiEvents, ApiCqrsEvent } from '@scaliolabs/baza-core-shared';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { NorthCapitalAccountsApiNestjsService, NorthCapitalPartiesApiNestjsService } from '../../../transact-api';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(ApiCqrsEvent)
export class AccountEmailChangedEventHandler implements IEventHandler<ApiCqrsEvent<AccountApiEvent, AccountApiEvents>> {
    constructor(
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly partyApi: NorthCapitalPartiesApiNestjsService,
        private readonly accountApi: NorthCapitalAccountsApiNestjsService,
    ) {}

    handle(event: ApiCqrsEvent<AccountApiEvent, AccountApiEvents>): void {
        cqrs(AccountEmailChangedEventHandler.name, async () => {
            if (event.apiEvent.event.topic === AccountApiEvent.BazaAccountEmailChanged) {
                const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(
                    event.apiEvent.event.payload.accountId,
                );

                if (!investorAccount) {
                    return;
                }

                if (investorAccount.northCapitalPartyId) {
                    await this.partyApi.updateParty({
                        partyId: investorAccount.northCapitalPartyId,
                        emailAddress: event.apiEvent.event.payload.accountEmail,
                    });
                }

                if (investorAccount.northCapitalAccountId) {
                    await this.accountApi.updateAccount({
                        accountId: investorAccount.northCapitalAccountId,
                        email: event.apiEvent.event.payload.accountEmail,
                        accountRegistration: investorAccount.user.fullName,
                    });
                }
            }
        });
    }
}
