import { Body, Controller, Get, Post, UploadedFile, UseGuards, UseInterceptors, UsePipes } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { ExpressMulterFile } from '../models';
import { BazaNcAccountVerificationService } from '../baza-nc-account-verification.service';
import { AccountEntity, AuthGuard, AuthOptionalGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import {
    AccountVerificationDto,
    AccountVerificationFormResourcesDto,
    AccountVerificationInvestorProfileDto,
    AccountVerificationIsCompletedDto,
    AccountVerificationStep,
    BazaNcAccountVerificationEndpoint,
    BazaNcAccountVerificationEndpointPaths,
    ListStatesRequestDto,
    ListStatesResponseDto,
    PersonalInformationApplyRequest,
} from '@scaliolabs/baza-nc-shared';
import { BazaNorthCapitalOpenApi } from '@scaliolabs/baza-nc-shared';
import { bazaNcTransactApiValidateRequest } from '../../../transact-api/lib/utils/baza-nc-transact-api-validate-request';
import { bazaNcAccountVerificationHelper } from '../helpers/baza-nc-account-verification.helper';
import { ZipCodeValidationForUsCitizenPipe } from '../pipes/zipcode-validatoin-for-us-addresses.pipe';

@Controller()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalAccountVerification)
export class BazaNcAccountVerificationController implements BazaNcAccountVerificationEndpoint {
    constructor(private readonly service: BazaNcAccountVerificationService) {}

    @Get(BazaNcAccountVerificationEndpointPaths.index)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'current',
        description: `
            Returns account verification DTO

            DEVELOPMENT: _debug field contains source PartyDetails if exists. It's available for Development and Stage only environments.
        `,
    })
    @ApiOkResponse({
        type: AccountVerificationDto,
    })
    public async index(@ReqAccount() account: AccountEntity): Promise<AccountVerificationDto> {
        return bazaNcAccountVerificationHelper(await this.service.fetchAccountVerification(account));
    }

    @Get(BazaNcAccountVerificationEndpointPaths.formResources)
    @UseGuards(AuthOptionalGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'formResources',
        description: 'Returns form resources',
    })
    @ApiOkResponse({
        type: AccountVerificationFormResourcesDto,
    })
    public async formResources(@ReqAccount() account: AccountEntity): Promise<AccountVerificationFormResourcesDto> {
        return this.service.formResources(account);
    }

    @Post(BazaNcAccountVerificationEndpointPaths.listStates)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'listStates',
        description: 'Returns list of the country states',
    })
    @ApiOkResponse({
        type: ListStatesResponseDto,
    })
    public async listStates(@Body() request: ListStatesRequestDto): Promise<ListStatesResponseDto> {
        return this.service.listState(request);
    }

    @Get(BazaNcAccountVerificationEndpointPaths.isCompleted)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'isCompleted',
        description: 'Is account verification completed?',
    })
    @ApiOkResponse({
        type: AccountVerificationIsCompletedDto,
    })
    public async isCompleted(@ReqAccount() account: AccountEntity): Promise<AccountVerificationIsCompletedDto> {
        return {
            isCompleted: await this.service.isCompleted(account),
        };
    }

    @Post(BazaNcAccountVerificationEndpointPaths.uploadPersonalInformationSSNDocument)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'uploadPersonalInformationSSNDocument',
        description: `
            Upload document for personal information data (SSN)

            PNG, PDF and JPG files supported. Do not append "filename0=" for filename as described at https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation?mid=MjI3`,
    })
    @ApiOkResponse()
    @UseInterceptors(FileInterceptor('file'))
    public async uploadPersonalInformationSSNDocument(
        @UploadedFile() file: ExpressMulterFile,
        @ReqAccount() account: AccountEntity,
    ): Promise<AccountVerificationDto> {
        await this.service.uploadPersonalInformationSSNDocument(account, file);

        return bazaNcAccountVerificationHelper(await this.service.fetchAccountVerification(account));
    }

    @Post(BazaNcAccountVerificationEndpointPaths.usePreviouslyUploadedDocument)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'usePreviouslyUploadedDocument',
        description: `
            Special Case for Edit Verification Flow.

            If we are requesting to upload SSN document, user already uploaded SSN document and he want to use same document,
            please send the request to stop asking user to upload new SSN document.
        `,
    })
    async usePreviouslyUploadedDocument(@ReqAccount() account: AccountEntity): Promise<AccountVerificationDto> {
        await this.service.usePreviouslyUploadedSSNDocument(account);

        return bazaNcAccountVerificationHelper(await this.service.fetchAccountVerification(account));
    }

    @Post(BazaNcAccountVerificationEndpointPaths.uploadPersonalInformationDocument)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'uploadPersonalInformationDocument',
        description: `
            Upload document for personal information data.

            PNG, PDF and JPG files supported. Do not append "filename0=" for filename as described at https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation?mid=MjI3`,
    })
    @ApiOkResponse()
    @UseInterceptors(FileInterceptor('file'))
    public async uploadPersonalInformationDocument(
        @UploadedFile() file: ExpressMulterFile,
        @ReqAccount() account: AccountEntity,
    ): Promise<AccountVerificationDto> {
        await this.service.uploadPersonalInformationDocument(account, file);

        return bazaNcAccountVerificationHelper(await this.service.fetchAccountVerification(account));
    }

    @Post(BazaNcAccountVerificationEndpointPaths.applyPersonalInformation)
    @UsePipes(new ZipCodeValidationForUsCitizenPipe())
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'applyPersonalInformation',
        description: `
            Send & Apply personal information data for account verification process

            Warming: it takes long due of slow response from North Capital Transact API. It can take ~1min to finish request.

            For more details, please visit North Capital API documentation located at
            https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation?mid=MTMy
            and https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation?mid=MTMz
        `,
    })
    @ApiOkResponse({
        type: AccountVerificationDto,
    })
    public async applyPersonalInformation(
        @Body() request: PersonalInformationApplyRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<AccountVerificationDto> {
        bazaNcTransactApiValidateRequest(request);

        const response = await this.service.applyPersonalInformation(account, request);
        const status = bazaNcAccountVerificationHelper(await this.service.fetchAccountVerification(account));

        if (response.shouldRequestSSNDocument) {
            return {
                ...status,
                currentStep: AccountVerificationStep.RequestSSNDocument,
            };
        } else if (response.shouldRequestDocuments) {
            return {
                ...status,
                currentStep: AccountVerificationStep.RequestDocuments,
            };
        } else {
            return status;
        }
    }

    @Post(BazaNcAccountVerificationEndpointPaths.applyInvestorProfile)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'applyInvestorProfile',
        description: `
            Send & Apply investor profile data for account verification process

            Warming: it takes long due of slow response from North Capital Transact API. It can take up to ~1min to finish request.

            For more details, please visit North Capital API documentation located at
            https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation?mid=MTMy
            and https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation?mid=MTMz
        `,
    })
    @ApiOkResponse({
        type: AccountVerificationDto,
    })
    public async applyInvestorProfile(
        @Body() request: AccountVerificationInvestorProfileDto,
        @ReqAccount() account: AccountEntity,
    ): Promise<AccountVerificationDto> {
        bazaNcTransactApiValidateRequest(request);

        await this.service.applyInvestorProfile(account, request);

        return bazaNcAccountVerificationHelper(await this.service.fetchAccountVerification(account));
    }
}
