import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import {
    BazaNcAccountVerificationCmsEndpoint,
    BazaNcAccountVerificationCmsEndpointPaths,
    BazaNcCmsAccountVerificationFormResourcesDto,
    BazaNorthCapitalCMSOpenApi,
    ListStatesRequestCmsDto,
    ListStatesResponseCmsDto,
    NcAccountVerificationKycLogsRequest,
    NcAccountVerificationKycLogsResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationService } from '../baza-nc-account-verification.service';
import { BazaNcAccountVerificationKycLogService } from '../services/baza-nc-account-verification-kyc-log.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalAccountVerificationCMS)
export class BazaNcAccountVerificationCmsController implements BazaNcAccountVerificationCmsEndpoint {
    constructor(
        private readonly kycLogService: BazaNcAccountVerificationKycLogService,
        private readonly accountVerificationService: BazaNcAccountVerificationService,
    ) {}

    @Post(BazaNcAccountVerificationCmsEndpointPaths.kycLogs)
    @ApiOperation({
        summary: 'kycLogs',
        description: 'Returns KYC/AML logs',
    })
    @ApiOkResponse({
        type: NcAccountVerificationKycLogsResponse,
    })
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    async kycLogs(@Body() request: NcAccountVerificationKycLogsRequest): Promise<NcAccountVerificationKycLogsResponse> {
        return this.kycLogService.list(request);
    }

    @Get(BazaNcAccountVerificationCmsEndpointPaths.formResources)
    @ApiOperation({
        summary: 'formResources',
        description: 'Returns form resources ( countries list and states )',
    })
    @ApiOkResponse({
        type: BazaNcCmsAccountVerificationFormResourcesDto,
    })
    async formResources(): Promise<BazaNcCmsAccountVerificationFormResourcesDto> {
        return this.accountVerificationService.formResources();
    }

    @Post(BazaNcAccountVerificationCmsEndpointPaths.listStates)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'listStates',
        description: 'Returns list of the country states',
    })
    @ApiOkResponse({
        type: ListStatesResponseCmsDto,
    })
    listStates(@Body() request: ListStatesRequestCmsDto): Promise<ListStatesResponseCmsDto> {
        return this.accountVerificationService.listState(request);
    }
}
