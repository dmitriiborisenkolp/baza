import { forwardRef, Inject, Injectable } from '@nestjs/common';
import {
    BazaNcAccountVerificationPersonalInformationService,
    ApplyPersonalInformationResponse,
} from './services/baza-nc-account-verification-personal-information.service';
import { BazaNcAccountVerificationRequestDocumentsService } from './services/baza-nc-account-verification-request-documents.service';
import { BazaNcAccountVerificationInvestorProfileService } from './services/baza-nc-account-verification-investor-profile.service';
import { BazaNcAccountVerificationStatusService } from './services/baza-nc-account-verification-status.service';
import { BazaNcAccountVerificationFormResourcesService } from './services/baza-nc-account-verification-form-resources.service';
import { BazaNcAccountVerificationRequestSsnDocumentsService } from './services/baza-nc-account-verification-request-ssn-documents.service';
import { EventBus } from '@nestjs/cqrs';
import { ExpressMulterFile } from './models';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { Country } from '@scaliolabs/baza-nc-shared';
import {
    AccountVerificationDto,
    AccountVerificationFormResourcesDto,
    AccountVerificationInvestorProfileDto,
    AccountVerificationPersonalInformationDto,
    ListStatesRequestDto,
    ListStatesResponseDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountService } from '../../investor-account';
import { BazaNcInvestorAccountNotFoundException } from '../../typeorm';
import { ncOutput, NcOutputContext, NorthCapitalPartiesApiNestjsService } from '../../transact-api';

/**
 * Facade Service for Account Verification
 * Use it for your project as entry point for everything related to Account Verification
 */
@Injectable()
export class BazaNcAccountVerificationService {
    constructor(
        private readonly eventBus: EventBus,
        private readonly status: BazaNcAccountVerificationStatusService,
        private readonly partiesApi: NorthCapitalPartiesApiNestjsService,
        private readonly formResourcesService: BazaNcAccountVerificationFormResourcesService,
        @Inject(forwardRef(() => BazaNcInvestorAccountService))
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly stepPersonalInformation: BazaNcAccountVerificationPersonalInformationService,
        private readonly stepRequestSSNDocuments: BazaNcAccountVerificationRequestSsnDocumentsService,
        private readonly stepRequestDocuments: BazaNcAccountVerificationRequestDocumentsService,
        private readonly stepInvestorProfile: BazaNcAccountVerificationInvestorProfileService,
    ) {}

    /**
     * Returns true if Account Verification is completed
     * @param user
     */
    async isCompleted(user: AccountEntity): Promise<boolean> {
        return this.status.isCompleted(user);
    }

    /**
     * Returns Form Resources (i.e. resources used for FE implementation of Account Verification)
     * @param user
     */
    async formResources(user?: AccountEntity): Promise<AccountVerificationFormResourcesDto> {
        if (user && (await this.investorAccountService.isUserHasInvestorAccount(user.id)) && (await this.status.hasParty(user))) {
            const investorAccount = await this.investorAccountService.upsertInvestorAccountFor(user);

            if (!investorAccount.northCapitalPartyId) {
                throw new BazaNcInvestorAccountNotFoundException();
            }

            const party = ncOutput(
                (
                    await this.partiesApi.getParty({
                        partyId: investorAccount.northCapitalPartyId,
                    })
                ).partyDetails[0],
                NcOutputContext.GetParty,
            );

            return this.formResourcesService.formResources({
                primCountry: (party.primCountry || '').toString().trim() || Country.USA,
            });
        } else {
            return this.formResourcesService.formResources();
        }
    }

    /**
     * Returns States for given Country
     * @param request
     */
    async listState(request: ListStatesRequestDto): Promise<ListStatesResponseDto> {
        const states = this.formResourcesService.countriesList(request.country);

        return {
            country: request.country,
            states: states.map((state) => ({
                title: state,
                value: state,
            })),
        };
    }

    /**
     * Returns Account Verification DTO
     * @param user
     */
    async fetchAccountVerification(user: AccountEntity): Promise<AccountVerificationDto> {
        return this.status.fetchAccountVerification(user);
    }

    /**
     * Uploads SSN Document to NC
     * @param user
     * @param file
     */
    async uploadPersonalInformationSSNDocument(user: AccountEntity, file: ExpressMulterFile): Promise<void> {
        await this.stepRequestSSNDocuments.uploadPersonalInformationSSNDocument(user, file);
    }

    /**
     * Bypass Account Verification request to upload SSN Document
     * @param user
     */
    async usePreviouslyUploadedSSNDocument(user: AccountEntity): Promise<void> {
        await this.stepRequestSSNDocuments.usePreviouslyUploadedSSNDocument(user);
    }

    /**
     * Uploads additional document to NC
     * @param user
     * @param file
     */
    async uploadPersonalInformationDocument(user: AccountEntity, file: ExpressMulterFile): Promise<void> {
        await this.stepRequestDocuments.uploadPersonalInformationDocument(user, file);
    }

    /**
     * Applies Personal Information for Account Verification
     * @param user
     * @param request
     */
    async applyPersonalInformation(
        user: AccountEntity,
        request: AccountVerificationPersonalInformationDto,
    ): Promise<ApplyPersonalInformationResponse> {
        return this.stepPersonalInformation.applyPersonalInformation(user, request);
    }

    /**
     * Applies Investor Profile for Account Verification (Suitability questions)
     * @param user
     * @param request
     */
    async applyInvestorProfile(user: AccountEntity, request: AccountVerificationInvestorProfileDto): Promise<void> {
        await this.stepInvestorProfile.applyInvestorProfile(user, request);
    }
}
