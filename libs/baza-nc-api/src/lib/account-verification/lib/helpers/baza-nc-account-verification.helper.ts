import { maskString } from '@scaliolabs/baza-core-shared';
import { AccountVerificationDto, bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';

const MAX_CHARACTERS_SSN = 4;

export function bazaNcAccountVerificationHelper(dto: AccountVerificationDto): AccountVerificationDto {
    if (!dto) {
        return undefined;
    }

    if (bazaNcApiConfig().maskSSNFields && dto.personalInformation?.ssn) {
        dto.personalInformation.ssn = maskString(dto.personalInformation.ssn.replace(/[^\d]/g, ''), {
            regex: /\d/g,
            numCharacters: MAX_CHARACTERS_SSN,
        });

        if (dto.personalInformation.ssn.length === 9) {
            const part1 = dto.personalInformation.ssn.slice(0, 3);
            const part2 = dto.personalInformation.ssn.slice(3, 5);
            const part3 = dto.personalInformation.ssn.slice(5, 9);

            dto.personalInformation.ssn = `${part1}-${part2}-${part3}`;
        }
    }

    return dto;
}
