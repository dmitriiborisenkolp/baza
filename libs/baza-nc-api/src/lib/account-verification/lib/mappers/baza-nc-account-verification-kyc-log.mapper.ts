import { Injectable } from '@nestjs/common';
import { KycLogDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationKycLogEntity } from '../../../typeorm';

@Injectable()
export class BazaNcAccountVerificationKycLogMapper {
    entityToDTO(entity: BazaNcAccountVerificationKycLogEntity): KycLogDto {
        return {
            id: entity.id,
            createdAt: entity.createdAt.toISOString(),
            ncAccountId: entity.ncAccountId,
            ncPartyId: entity.ncPartyId,
            previousKycStatus: entity.previousKycStatus,
            newKycStatus: entity.newKycStatus,
            userEmail: entity.userEmail,
            userFullName: entity.userFullName,
            userId: entity.userId,
            kycDetails: entity.kycDetails,
        };
    }

    entitiesToDTOs(input: Array<BazaNcAccountVerificationKycLogEntity>): Array<KycLogDto> {
        return input.map((e) => this.entityToDTO(e));
    }
}
