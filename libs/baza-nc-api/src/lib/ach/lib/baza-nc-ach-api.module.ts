import { Module } from '@nestjs/common';
import { BazaNcTransactApiModule } from '../../transact-api';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { BazaNcAchService } from './services/baza-nc-ach.service';

@Module({
    imports: [
        BazaNcTransactApiModule,
        BazaNcTypeormApiModule,
    ],
    providers: [
        BazaNcAchService,
    ],
    exports: [
        BazaNcAchService,
    ],
})
export class BazaNcAchApiModule {}
