import { Injectable } from '@nestjs/common';
import {
    areBankAccountDetailsSpecified,
    ExternalAccountType,
    NorthCapitalErrorCodes,
    PlaidBankAccountDetails,
    PurchaseFlowBankAccountDto,
    PurchaseFlowSetBankAccountDetailsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountService } from '../../../investor-account';
import { isNorthCapitalException } from '../../../transact-api';
import { NorthCapitalAchTransfersApiNestjsService } from '../../../transact-api/lib/api/north-capital-ach-transfers-api.nestjs-service';
import { BazaNcInvestorAccountEntity, BazaNcInvestorAccountRepository } from '../../../typeorm';
import { BazaNcPurchaseFlowInvalidAccountDetailsException } from '../exceptions/baza-nc-purchase-flow-invalid-account-details.exception';

/**
 * The service is responsible to work with bank account which is stored with NC API
 * Also the service is handling `isBankAccountLinked` flag for Investor Account
 */
@Injectable()
export class BazaNcAchService {
    constructor(
        private readonly investorAccount: BazaNcInvestorAccountService,
        private readonly ncACHTransfersApi: NorthCapitalAchTransfersApiNestjsService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    /**
     * Returns information about Bank Account from NC API
     * If Bank Account is not set, method will return `{ isAvailable: false }`
     * @param investorAccount
     */
    async getBankAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<PurchaseFlowBankAccountDto> {
        if (!investorAccount.northCapitalAccountId) {
            return {
                isAvailable: false,
            };
        }

        try {
            const response = await this.ncACHTransfersApi.getExternalAccount({
                accountId: investorAccount.northCapitalAccountId,
                types: ExternalAccountType.Account,
            });

            const details = response.statusDesc as PlaidBankAccountDetails;

            const hasDetails = areBankAccountDetailsSpecified(details);

            if (hasDetails) {
                return {
                    isAvailable: true,
                    details: {
                        accountName: details.AccountName.toUpperCase(),
                        accountNickName: details.AccountNickName.toUpperCase(),
                        accountNumber: details.AccountNumber,
                        accountRoutingNumber: details.AccountRoutingNumber,
                        accountType: details.accountType,
                    },
                };
            } else {
                return {
                    isAvailable: false,
                };
            }
        } catch (err) {
            if (isNorthCapitalException(err, NorthCapitalErrorCodes.InvestorExternalAccountDoesNotExistNicknameMismatch)) {
                return {
                    isAvailable: false,
                };
            } else {
                throw err;
            }
        }
    }

    /**
     * Sets Bank Account for current Investor with NC API
     * @param investorAccount
     * @param request
     */
    async setBankAccount(
        investorAccount: BazaNcInvestorAccountEntity,
        request: PurchaseFlowSetBankAccountDetailsDto,
    ): Promise<PurchaseFlowBankAccountDto> {
        try {
            await this.ncACHTransfersApi.updateExternalAccount({
                accountId: investorAccount.northCapitalAccountId,
                types: ExternalAccountType.Account,
                ExtAccountfullname: request.accountName,
                Extnickname: request.accountNickName || request.accountName,
                ExtRoutingnumber: request.accountRoutingNumber,
                ExtAccountnumber: request.accountNumber,
                accountType: request.accountType,
            });
        } catch (err) {
            if (isNorthCapitalException(err, NorthCapitalErrorCodes.InvalidRoutingNumber)) {
                throw new BazaNcPurchaseFlowInvalidAccountDetailsException();
            } else {
                throw err;
            }
        }

        investorAccount.isBankAccountLinked = true;
        investorAccount.isBankAccountNcAchLinked = true;

        await this.investorAccountRepository.save(investorAccount);

        return {
            isAvailable: true,
            details: {
                accountName: request.accountName.toUpperCase(),
                accountNickName: (request.accountNickName || request.accountName).toUpperCase(),
                accountRoutingNumber: request.accountRoutingNumber,
                accountNumber: request.accountNumber,
                accountType: request.accountType,
            },
        };
    }

    /**
     * Removes Bank Account from NC API
     * Also sets `investorAccount.isBankAccountLinked` as false
     * @param investorAccount
     */
    async deleteBankAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<void> {
        const existing = await this.getBankAccount(investorAccount);

        if (existing.isAvailable) {
            await this.ncACHTransfersApi.deleteExternalAccount({
                types: ExternalAccountType.Account,
                accountId: investorAccount.northCapitalAccountId,
            });

            investorAccount.isBankAccountLinked = investorAccount.isBankAccountCashInLinked;
            investorAccount.isBankAccountNcAchLinked = false;

            await this.investorAccountRepository.save(investorAccount);
        }
    }
}
