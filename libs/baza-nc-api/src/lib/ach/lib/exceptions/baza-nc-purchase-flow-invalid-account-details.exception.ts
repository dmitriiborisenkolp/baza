import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowException } from '../../../typeorm';

export class BazaNcPurchaseFlowInvalidAccountDetailsException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.InvalidRoutingNumber,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.InvalidRoutingNumber],
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}
