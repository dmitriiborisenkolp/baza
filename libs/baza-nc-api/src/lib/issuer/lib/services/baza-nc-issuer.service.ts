import { Inject, Injectable } from '@nestjs/common';
import { IssuerDetails } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalIssuersApiNestjsService } from '../../../transact-api';
import { BAZA_NC_ISSUER_API_CONFIG, BazaNcIssuerApiConfig } from '../baza-nc-issuer-api.config';
import { BazaNcIssuerNotDefinedException } from '../exceptions/baza-nc-issuer-not-defined.exception';
import { AccountEntity } from '@scaliolabs/baza-core-api';

/**
 * Baza NC Issuer Service
 * Contains Issuer factory which is used for Creatng Offerings
 */
@Injectable()
export class BazaNcIssuerService {
    constructor(
        @Inject(BAZA_NC_ISSUER_API_CONFIG) private readonly moduleConfig: BazaNcIssuerApiConfig,
        private readonly ncIssuerApi: NorthCapitalIssuersApiNestjsService,
    ) {}

    /**
     * Returns Issuer for Offerings
     * Static Issuer ID will be when BAZA_NORTH_CAPITAL_ISSUER_ID environment is provided
     * A new Issuer will be generated in opposite case
     * @param accountIssuer
     */
    async factoryIssuer(accountIssuer: AccountEntity): Promise<IssuerDetails> {
        return this.moduleConfig.useStaticIssuerId ? this.factoryExistingIssuer() : this.factoryNewIssuer(accountIssuer);
    }

    /**
     * Creates a new Issuer for Offering
     * Issuer will be created based on First Name / Last Name / Full Name / Email
     * of given Account Issuer
     * @param accountIssuer
     * @private
     */
    private async factoryNewIssuer(accountIssuer: AccountEntity): Promise<IssuerDetails> {
        const ncIssuer = await this.ncIssuerApi.createIssuer({
            email: accountIssuer.email,
            firstName: accountIssuer.firstName,
            lastName: accountIssuer.lastName,
            issuerName: accountIssuer.fullName,
        });

        return ncIssuer.issuerDetails[1][0];
    }

    /**
     * Returns existing Issuer for Offering
     * @private
     */
    private async factoryExistingIssuer(): Promise<IssuerDetails> {
        if (!this.moduleConfig.useStaticIssuerId) {
            throw new BazaNcIssuerNotDefinedException();
        }

        const ncIssuer = await this.ncIssuerApi.getIssuer({
            issuerId: this.moduleConfig.useStaticIssuerId,
        });

        return ncIssuer.issuerDetails[0];
    }
}
