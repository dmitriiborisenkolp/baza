import { BAZA_NC_ISSUER_API_CONFIG, BazaNcIssuerApiConfig } from './baza-nc-issuer-api.config';
import { DynamicModule, Global, Module } from '@nestjs/common';
import { BazaNcTransactApiModule } from '../../transact-api';
import { BazaNcIssuerService } from './services/baza-nc-issuer.service';
import { InjectionToken } from '@nestjs/common/interfaces/modules/injection-token.interface';
import { OptionalFactoryDependency } from '@nestjs/common';

interface AsyncOptions {
    injects: Array<InjectionToken | OptionalFactoryDependency>;
    useFactory(...args: Array<unknown>): Promise<BazaNcIssuerApiConfig>;
}

export { AsyncOptions as BazaNcIssuerApiModuleAsyncOptions };

@Module({})
export class BazaNcIssuerApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaNcIssuerApiGlobalModule,
            providers: [
                {
                    provide: BAZA_NC_ISSUER_API_CONFIG,
                    useFactory: options.useFactory,
                    inject: options.injects,
                },
            ],
            exports: [BAZA_NC_ISSUER_API_CONFIG],
        };
    }
}

@Global()
@Module({
    imports: [BazaNcTransactApiModule],
    providers: [BazaNcIssuerService],
    exports: [BazaNcIssuerService],
})
class BazaNcIssuerApiGlobalModule {}
