import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIssuerErrorCodes, bazaNcIssuerErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIssuerNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcIssuerErrorCodes.BazaNcIssuerNotFound,
            bazaNcIssuerErrorCodesI18n[BazaNcIssuerErrorCodes.BazaNcIssuerNotFound],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
