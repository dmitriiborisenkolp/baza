import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIssuerErrorCodes, bazaNcIssuerErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIssuerNotDefinedException extends BazaAppException {
    constructor() {
        super(
            BazaNcIssuerErrorCodes.BazaNcIssuerNotDefined,
            bazaNcIssuerErrorCodesI18n[BazaNcIssuerErrorCodes.BazaNcIssuerNotDefined],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
