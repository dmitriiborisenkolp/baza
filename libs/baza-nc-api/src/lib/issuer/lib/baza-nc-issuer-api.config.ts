import { IssuerId } from '@scaliolabs/baza-nc-shared';

/**
 * Baza NC Issuer API Module Config Injection Token
 */
export const BAZA_NC_ISSUER_API_CONFIG = Symbol();

/**
 * Configuration for Baza NC Issuer API Module
 */
export interface BazaNcIssuerApiConfig {
    /**
     * Uses Static (predefined) Issuer Id
     * If enabled, API will user BAZA_NORTH_CAPITAL_ISSUER_ID environment value as Issuer Id
     * for Offerings. It's recommended to enable it because NC API has small
     * but noticeable delay between creating an issuer and availability of issuer
     * If disabled, API will create a new Issuer every time when new Offering
     * is created
     */
    useStaticIssuerId?: IssuerId;
}
