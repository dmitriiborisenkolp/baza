import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';

import { BazaNcBootstrapNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../account-verification';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/bootstrap/integration-tests/001-baza-nc-bootstrap-web.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessBootstrap = new BazaNcBootstrapNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('[WEB] will correctly works for non-auth requests', async () => {
        const response = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.account).not.toBeDefined();
        expect(response.investorAccount).not.toBeDefined();
        expect(response.signedUpWithClient).not.toBeDefined();
        expect(response.registry).toBeDefined();
        expect(Array.isArray(response.registry)).toBeTruthy();
    });

    it('[WEB] will correctly works for with-auth / without investor account requests', async () => {
        await http.authE2eUser();

        const response = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.account).toBeDefined();
        expect(response.investorAccount).toBeDefined();
        expect(response.investorAccount.northCapitalAccountId).not.toBeDefined();
        expect(response.investorAccount.northCapitalPartyId).not.toBeDefined();
        expect(response.investorAccount.isAccountVerificationCompleted).toBeFalsy();
        expect(response.investorAccount.isBankAccountLinked).toBeFalsy();
        expect(response.investorAccount.isInvestorVerified).toBeFalsy();
        expect(response.investorAccount.isCreditCardLinked).toBeFalsy();
        expect(response.investorAccount.isPaymentMethodLinked).toBeFalsy();
        expect(response.signedUpWithClient).toBeDefined();
        expect(response.registry).toBeDefined();
        expect(Array.isArray(response.registry)).toBeTruthy();
    });

    it('[WEB] will correctly works for with-auth / with investor account requests', async () => {
        await http.authE2eUser();

        await dataAccessE2eFixtures.up({
            fixtures: [BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture],
            specFile: __filename,
        });

        const response = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.account).toBeDefined();
        expect(response.investorAccount).toBeDefined();
        expect(response.investorAccount.northCapitalAccountId).not.toBeNull();
        expect(response.investorAccount.northCapitalPartyId).not.toBeNull();
        expect(response.investorAccount.isAccountVerificationCompleted).toBeFalsy();
        expect(response.investorAccount.isBankAccountLinked).toBeFalsy();
        expect(response.investorAccount.isInvestorVerified).toBeDefined();
        expect(response.investorAccount.isCreditCardLinked).toBeFalsy();
        expect(response.investorAccount.isPaymentMethodLinked).toBeFalsy();
        expect(response.signedUpWithClient).toBeDefined();
        expect(response.registry).toBeDefined();
        expect(Array.isArray(response.registry)).toBeTruthy();
    });

    it('[WEB] will correctly works for with-auth / with investor account and fully completed account verification requests', async () => {
        await http.authE2eUser();

        await dataAccessE2eFixtures.up({
            fixtures: [BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFullFixture],
            specFile: __filename,
        });

        const response = await dataAccessBootstrap.bootstrap();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.account).toBeDefined();
        expect(response.investorAccount).toBeDefined();
        expect(response.investorAccount.northCapitalAccountId).not.toBeNull();
        expect(response.investorAccount.northCapitalPartyId).not.toBeNull();
        expect(response.investorAccount.isAccountVerificationCompleted).toBeTruthy();
        expect(response.investorAccount.isBankAccountLinked).toBeFalsy();
        expect(response.investorAccount.isInvestorVerified).toBeDefined();
        expect(response.investorAccount.isCreditCardLinked).toBeFalsy();
        expect(response.investorAccount.isPaymentMethodLinked).toBeFalsy();
        expect(response.signedUpWithClient).toBeDefined();
        expect(response.registry).toBeDefined();
        expect(Array.isArray(response.registry)).toBeTruthy();
    });
});
