import { Injectable } from '@nestjs/common';
import { AccountEntity, BazaAccountMapper, BazaReferralCodeService } from '@scaliolabs/baza-core-api';
import { BazaRegistryService } from '@scaliolabs/baza-core-api';
import { BazaReferralCodeAccountMetadata, replaceTags } from '@scaliolabs/baza-core-shared';
import { BazaNcBootstrapDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountMapper, BazaNcInvestorAccountService } from '../../../investor-account';

@Injectable()
export class BazaNcBootstrapService {
    constructor(
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly investorAccountMapper: BazaNcInvestorAccountMapper,
        private readonly accountMapper: BazaAccountMapper,
        private readonly registry: BazaRegistryService,
        private readonly referralCodeService: BazaReferralCodeService,
    ) {}

    async bootstrap(account?: AccountEntity): Promise<BazaNcBootstrapDto> {
        if (account) {
            const investorAccount = await this.investorAccountService.findInvestorAccountByUserId(account.id);
            const copyPasteReferralCode = this.registry.getValue('bazaAccounts.referralCodes.copyText');
            const referralCode = await this.referralCodeService.findDefaultOrActiveReferralCodeOfAccount(account);

            const response: BazaNcBootstrapDto = {
                account: this.accountMapper.entityToDto(account),
                registry: await this.registry.publicSchema(),
                investorAccount: investorAccount
                    ? this.investorAccountMapper.entityToDTO(investorAccount)
                    : this.investorAccountMapper.virtualInvestorAccountDto(account),
                signedUpWithClient: account.signedUpWithClient,
            };

            if (referralCode) {
                response.referralCode = referralCode.codeDisplay;
                response.referralCodeCopyText = replaceTags(copyPasteReferralCode, {
                    code: referralCode.codeDisplay,
                });
            }

            const metadata: BazaReferralCodeAccountMetadata = account.metadata;

            if (metadata.signedUpWithReferralCode) {
                response.signedUpWithReferralCode = metadata.signedUpWithReferralCode;
            }

            return response;
        } else {
            return {
                registry: await this.registry.publicSchema(),
            };
        }
    }
}
