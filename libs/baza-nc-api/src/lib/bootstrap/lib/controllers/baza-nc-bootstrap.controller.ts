import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AccountEntity, AuthOptionalNoFailOnInvalidTokenGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import {
    BazaNcBootstrapEndpoint,
    BazaNcBootstrapEndpointPaths,
    BazaNcBootstrapDto,
    BazaNorthCapitalOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcBootstrapService } from '../services/baza-nc-bootstrap.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalBootstrap)
@UseGuards(AuthOptionalNoFailOnInvalidTokenGuard)
export class BazaNcBootstrapController implements BazaNcBootstrapEndpoint {
    constructor(private readonly service: BazaNcBootstrapService) {}

    @Get(BazaNcBootstrapEndpointPaths.Bootstrap)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'bootstrap',
        description: 'Bootstrap data for web clients. Should be called before / after sign in.',
    })
    @ApiOkResponse({
        type: BazaNcBootstrapDto,
    })
    async bootstrap(@ReqAccount() account: AccountEntity): Promise<BazaNcBootstrapDto> {
        return this.service.bootstrap(account);
    }
}
