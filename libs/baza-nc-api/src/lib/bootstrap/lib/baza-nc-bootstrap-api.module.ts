import { Module } from '@nestjs/common';
import { BazaAccountApiModule } from '@scaliolabs/baza-core-api';
import { BazaRegistryApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcInvestorAccountApiModule } from '../../investor-account';
import { BazaNcBootstrapController } from './controllers/baza-nc-bootstrap.controller';
import { BazaNcBootstrapService } from './services/baza-nc-bootstrap.service';

@Module({
    imports: [BazaAccountApiModule, BazaRegistryApiModule, BazaNcInvestorAccountApiModule],
    controllers: [BazaNcBootstrapController],
    providers: [BazaNcBootstrapService],
    exports: [BazaNcBootstrapService],
})
export class BazaNcBootstrapApiModule {}
