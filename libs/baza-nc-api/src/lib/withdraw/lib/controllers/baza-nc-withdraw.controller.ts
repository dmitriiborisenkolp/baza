import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcWithdrawEndpoint,
    BazaNcWithdrawEndpointPaths,
    BazaNcWithdrawListRequest,
    BazaNcWithdrawListResponse,
    BazaNcWithdrawReprocessRequest,
    BazaNcWithdrawRequest,
    BazaNorthCapitalOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaDwollaPaymentMapper } from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentDto } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcInvestorAccountService, InvestorAccountGuard, ReqInvestorAccount } from '../../../investor-account';
import { BazaNcWithdrawService } from '../services/baza-nc-withdraw.service';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalWithdraw)
@UseGuards(InvestorAccountGuard)
export class BazaNcWithdrawController implements BazaNcWithdrawEndpoint {
    constructor(
        private readonly service: BazaNcWithdrawService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly dwollaPaymentMapper: BazaDwollaPaymentMapper,
    ) {}

    @Get(BazaNcWithdrawEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Withdraw requests',
    })
    @ApiOkResponse({
        type: BazaNcWithdrawListResponse,
    })
    async list(
        @Query() request: BazaNcWithdrawListRequest,
        @ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<BazaNcWithdrawListResponse> {
        return this.service.list(investorAccount, request);
    }

    @Post(BazaNcWithdrawEndpointPaths.withdraw)
    @ApiOperation({
        summary: 'withdraw',
        description: 'Withdrawing available balance from Dwolla account to an external bank account',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentDto,
    })
    async withdraw(
        @Body() request: BazaNcWithdrawRequest,
        @ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<BazaDwollaPaymentDto> {
        const entity = await this.service.withdraw(investorAccount, request);

        return this.dwollaPaymentMapper.entityToDTO(entity);
    }

    @Post(BazaNcWithdrawEndpointPaths.reprocess)
    @ApiOperation({
        summary: 'reprocess',
        description: 'Will reprocess the failed withdrawal request',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentDto,
    })
    async reprocess(
        @Body() request: BazaNcWithdrawReprocessRequest,
        @ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<BazaDwollaPaymentDto> {
        const entity = await this.service.reprocess(investorAccount, request);

        return this.dwollaPaymentMapper.entityToDTO(entity);
    }
}
