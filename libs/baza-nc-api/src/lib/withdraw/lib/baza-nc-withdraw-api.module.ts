import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcWithdrawController } from './controllers/baza-nc-withdraw.controller';
import { BazaNcWithdrawService } from './services/baza-nc-withdraw.service';
import { BazaDwollaPaymentApiModule } from '@scaliolabs/baza-dwolla-api';

@Module({
    controllers: [BazaNcWithdrawController],
    imports: [BazaCrudApiModule, BazaDwollaPaymentApiModule],
    providers: [BazaNcWithdrawService],
    exports: [BazaNcWithdrawService],
})
export class BazaNcWithdrawApiModule {}
