import { Injectable } from '@nestjs/common';
import {
    BazaNcBankAccountExport,
    BazaNcBankAccountType,
    BazaNcWithdrawListRequest,
    BazaNcWithdrawListResponse,
    BazaNcWithdrawReprocessRequest,
    BazaNcWithdrawRequest,
    convertToCents,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountNoDefaultException, BazaNcBankAccountsService } from '../../../bank-accounts';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { BazaDwollaPaymentEntity, BazaDwollaPaymentService } from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';

@Injectable()
export class BazaNcWithdrawService {
    constructor(
        private readonly bankAccountService: BazaNcBankAccountsService,
        private readonly dwollaPaymentService: BazaDwollaPaymentService,
    ) {}

    /**
     * Returns list of Withdraw requests
     * @param investorAccount
     * @param request
     */
    async list(investorAccount: BazaNcInvestorAccountEntity, request: BazaNcWithdrawListRequest): Promise<BazaNcWithdrawListResponse> {
        const account = investorAccount.user;

        return this.dwollaPaymentService.list(account.id, {
            ...request,
            types: [BazaDwollaPaymentType.Withdrawal],
        });
    }

    /**
     * Withdraw Funds
     * @param investorAccount
     * @param request
     */
    async withdraw(investorAccount: BazaNcInvestorAccountEntity, request: BazaNcWithdrawRequest): Promise<BazaDwollaPaymentEntity> {
        const account = investorAccount.user;
        const cashOutFundingSourceId = await this.touchWithdrawDwollaWallet(investorAccount);

        return this.dwollaPaymentService.transfer({
            accountId: account.id,
            amountCents: request.amount ? convertToCents(parseFloat(request.amount)) : undefined,
            dwollaCustomerId: investorAccount.dwollaCustomerId,
            payload: {
                type: BazaDwollaPaymentType.Withdrawal,
                cashOutFundingSourceId,
                ...request,
            },
        });
    }

    /**
     * Reprocess Request
     * @param investorAccount
     * @param request
     */
    async reprocess(
        investorAccount: BazaNcInvestorAccountEntity,
        request: BazaNcWithdrawReprocessRequest,
    ): Promise<BazaDwollaPaymentEntity> {
        const account = investorAccount.user;
        const cashOutFundingSourceId = await this.touchWithdrawDwollaWallet(investorAccount);

        return this.dwollaPaymentService.reprocess({
            accountId: account.id,
            ulid: request.ulid,
            dwollaCustomerId: investorAccount.dwollaCustomerId,
            payload: {
                cashOutFundingSourceId,
            },
        });
    }

    /**
     * Returns the Funding Source Id for the selected cash-out bank account.
     * If no funding source id is not available then it will export the bank account to Dwolla
     * @param investorAccount
     * @returns dwollaFundingSourceId
     */
    async touchWithdrawDwollaWallet(investorAccount: BazaNcInvestorAccountEntity) {
        const bankAccount = await this.bankAccountService.default(investorAccount.id, BazaNcBankAccountType.CashOut);

        if (!bankAccount) {
            throw new BazaNcBankAccountNoDefaultException();
        }

        let { dwollaFundingSourceId } = bankAccount;

        if (!dwollaFundingSourceId) {
            dwollaFundingSourceId = (
                await this.bankAccountService.export(investorAccount.id, {
                    services: [BazaNcBankAccountExport.Dwolla],
                    ulid: bankAccount.ulid,
                    setAsDefault: true,
                })
            ).dwollaFundingSourceId;
        }

        return dwollaFundingSourceId;
    }
}
