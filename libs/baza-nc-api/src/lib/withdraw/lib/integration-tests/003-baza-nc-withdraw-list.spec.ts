import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../account-verification';
import { BazaNcWithdrawalNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaDwollaPaymentFixtures } from '@scaliolabs/baza-dwolla-api';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/withdraw/003-baza-nc-withdraw-list.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcWithdrawalNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaDwollaPaymentFixtures.BazaDwollaPaymentSampleFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch withdraw list (Page 1)', async () => {
        const response = await dataAccess.list({
            index: 1,
            size: 3,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.pager.size).toBe(3);
        expect(response.pager.total).toBe(6);

        expect(response.items[0].amount).toBe('159.00');
        expect(response.items[1].amount).toBe('258.00');
        expect(response.items[2].amount).toBe('156.00');
    });

    it('will fetch withdraw list (Page 2)', async () => {
        const response = await dataAccess.list({
            index: 2,
            size: 3,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.pager.size).toBe(3);
        expect(response.pager.total).toBe(6);

        expect(response.items[0].amount).toBe('53.00');
        expect(response.items[1].amount).toBe('59.00');
        expect(response.items[2].amount).toBe('353.00');
    });
});
