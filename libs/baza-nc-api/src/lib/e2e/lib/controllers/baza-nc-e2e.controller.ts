import { Body, Controller, Param, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcE2eEndpoint,
    BazaNcE2eEndpointPaths,
    BazaNcE2eUpdateTradeStatusRequest,
    BazaNorthCapitalCMSOpenApi,
    BazaNcOfferingDto,
    BazaNcE2eUpdateCCFundMoveStatusRequest,
    BazaNcE2eUpdateExternalFundMoveStatusRequest,
    BazaNcE2eSetTradeStatusRequest,
    BazaNcE2eSetFundingSourceNameRequest,
    NorthCapitalTopic,
    BazaNcE2eGetTradeStatusResponse,
    BazaNcE2eGetTradeStatusRequest,
    BazaNcE2eGetNcAccountRequest,
    GetAccountResponse,
    BazaNcE2eGetNcPartyRequest,
    GetPartyResponse,
} from '@scaliolabs/baza-nc-shared';
import { AccountEntity, AuthGuard, AuthRequireAdminRoleGuard, BazaE2eGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import { BazaNcE2eService } from '../services/baza-nc-e2e.service';
import { BazaNcOfferingMapper } from '../../../offering';

@Controller()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalE2e)
@UseGuards(BazaE2eGuard)
export class BazaNcE2eController implements BazaNcE2eEndpoint {
    constructor(private readonly service: BazaNcE2eService, private readonly ncOfferingMapper: BazaNcOfferingMapper) {}

    @Post(BazaNcE2eEndpointPaths.updateTradeStatus)
    @ApiOperation({
        summary: 'updateTradeStatus',
        description: 'E2E Helper - Update trade status',
    })
    @ApiOkResponse()
    async updateTradeStatus(@Body() request: BazaNcE2eUpdateTradeStatusRequest): Promise<void> {
        await this.service.updateTradeStatus(request);
    }

    @Post(BazaNcE2eEndpointPaths.getTradeStatus)
    @ApiOperation({
        summary: 'getTradeStatus',
        description: 'E2E Helper - Returns Trade Status',
    })
    @ApiOkResponse({
        type: BazaNcE2eGetTradeStatusResponse,
    })
    async getTradeStatus(@Body() request: BazaNcE2eGetTradeStatusRequest): Promise<BazaNcE2eGetTradeStatusResponse> {
        return this.service.getTradeStatus(request);
    }

    @Post(BazaNcE2eEndpointPaths.setTradeStatus)
    @ApiOperation({
        summary: 'setTradeStatus',
        description: 'E2E Helper - Set trade status (immediately, w/o webhooks)',
    })
    @ApiOkResponse()
    async setTradeStatus(@Body() request: BazaNcE2eSetTradeStatusRequest): Promise<void> {
        await this.service.setTradeStatus(request);
    }

    @Post(BazaNcE2eEndpointPaths.enableMaskConfig)
    @ApiOperation({
        summary: 'enableMaskConfig',
        description: 'E2E Helper - Enable Mask Configuration (SSN, Credit Card, Bank Account Details)',
    })
    @ApiOkResponse()
    async enableMaskConfig(): Promise<void> {
        await this.service.enableMaskConfig();
    }

    @Post(BazaNcE2eEndpointPaths.disableMaskConfig)
    @ApiOperation({
        summary: 'disableMaskConfig',
        description: 'E2E Helper - Disable Mask Configuration (SSN, Credit Card, Bank Account Details)',
    })
    @ApiOkResponse()
    async disableMaskConfig(): Promise<void> {
        await this.service.disableMaskConfig();
    }

    @Post(BazaNcE2eEndpointPaths.createExampleOffering)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiOperation({
        summary: 'createExampleOffering',
        description: 'E2E Helper - Create an example NC Offering',
    })
    @ApiOkResponse({
        type: BazaNcOfferingDto,
    })
    async createExampleOffering(@ReqAccount() account: AccountEntity): Promise<BazaNcOfferingDto> {
        const ncOffering = await this.service.createExampleOffering(account);

        return this.ncOfferingMapper.entityToDTO(ncOffering);
    }

    @Post(BazaNcE2eEndpointPaths.enableTransactionFee)
    @ApiOperation({
        summary: 'enableTransactionFeeFeature',
        description: 'E2E Helper - Enable Transaction Fee feature',
    })
    @ApiOkResponse()
    async enableTransactionFeeFeature(): Promise<void> {
        await this.service.enableTransactionFeeFeature();
    }

    @Post(BazaNcE2eEndpointPaths.disableTransactionFee)
    @ApiOperation({
        summary: 'disableTransactionFeeFeature',
        description: 'E2E Helper - Disable Transaction Fee feature',
    })
    @ApiOkResponse()
    async disableTransactionFeeFeature(): Promise<void> {
        await this.service.disableTransactionFeeFeature();
    }

    @Post(BazaNcE2eEndpointPaths.updateCCFundMoveStatus)
    @ApiOperation({
        summary: 'updateCCFundMoveStatus',
        description: 'E2E Helper - Update CC Transaction status',
    })
    @ApiOkResponse()
    async updateCCFundMoveStatus(@Body() request: BazaNcE2eUpdateCCFundMoveStatusRequest): Promise<void> {
        await this.service.updateCCFundMoveStatus(request);
    }

    @Post(BazaNcE2eEndpointPaths.updateExternalFundMoveStatus)
    @ApiOperation({
        summary: 'updateExternalFundMoveStatus',
        description: 'E2E Helper - Update ACH Transaction status',
    })
    @ApiOkResponse()
    async updateExternalFundMoveStatus(@Body() request: BazaNcE2eUpdateExternalFundMoveStatusRequest): Promise<void> {
        await this.service.updateExternalFundMoveStatus(request);
    }

    @Post(BazaNcE2eEndpointPaths.enableDwollaAutoTouch)
    @ApiOperation({
        summary: 'enableDwollaAutoTouch',
        description: 'E2E Helper - Enable Dwolla Customer Auto-Touch during Dwolla Dividends Processing',
    })
    @ApiOkResponse()
    async enableDwollaAutoTouch(): Promise<void> {
        await this.service.enableDwollaAutoTouch();
    }

    @Post(BazaNcE2eEndpointPaths.disableDwollaAutoTouch)
    @ApiOperation({
        summary: 'disableDwollaAutoTouch',
        description: 'E2E Helper - Disable Dwolla Customer Auto-Touch during Dwolla Dividends Processing',
    })
    @ApiOkResponse()
    async disableDwollaAutoTouch(): Promise<void> {
        await this.service.disableDwollaAutoTouch();
    }

    @Post(BazaNcE2eEndpointPaths.setFundingSourceName)
    @ApiOperation({
        summary: 'setFundingSourceName',
        description: 'E2E Helper - Set Funding Source Name',
    })
    @ApiOkResponse()
    async setFundingSourceName(@Body() request: BazaNcE2eSetFundingSourceNameRequest): Promise<void> {
        await this.service.setFundingSourceName(request);
    }

    @Post(BazaNcE2eEndpointPaths.simulateNcWebhook)
    @ApiOperation({
        summary: 'simulateNcWebhook',
        description: 'E2E Helper - Emulates NC Webhook',
    })
    @ApiOkResponse()
    async simulateNcWebhook(@Body() payload: unknown, @Param('topic') topic: NorthCapitalTopic): Promise<void> {
        await this.service.simulateNcWebhook(payload, topic);
    }

    @Post(BazaNcE2eEndpointPaths.enableLBASync)
    @ApiOperation({
        summary: 'enableLBASync',
        description: 'E2E Helper - Enables sync between NBA and LBA APIs',
    })
    @ApiOkResponse()
    async enableLBASync(): Promise<void> {
        await this.service.enableLBASync();
    }

    @Post(BazaNcE2eEndpointPaths.disableLBASync)
    @ApiOperation({
        summary: 'disableLBASync',
        description: 'E2E Helper - Disables sync between NBA and LBA APIs',
    })
    @ApiOkResponse()
    async disableLBASync(): Promise<void> {
        await this.service.disableBASync();
    }

    @Post(BazaNcE2eEndpointPaths.getNcAccount)
    @ApiOperation({
        summary: 'getNcAccount',
        description: 'E2E Helper - Returns NC Account',
    })
    @ApiOkResponse()
    async getNcAccount(@Body() request: BazaNcE2eGetNcAccountRequest): Promise<GetAccountResponse> {
        return this.service.getNcAccount(request);
    }

    @Post(BazaNcE2eEndpointPaths.getNcParty)
    @ApiOperation({
        summary: 'getNcParty',
        description: 'E2E Helper - Returns NC Party',
    })
    @ApiOkResponse()
    async getNcParty(@Body() request: BazaNcE2eGetNcPartyRequest): Promise<GetPartyResponse> {
        return this.service.getNcParty(request);
    }
}
