import { Module } from '@nestjs/common';
import { BazaNcE2eController } from './controllers/baza-nc-e2e.controller';
import { BazaNcE2eService } from './services/baza-nc-e2e.service';
import { BazaNcTransactApiModule } from '../../transact-api';
import { BazaNcWebhookApiModule } from '../../webhook';
import { BazaNcWithdrawApiModule } from '../../withdraw';
import { BazaNcDwollaApiModule } from '../../dwolla';

@Module({
    imports: [BazaNcTransactApiModule, BazaNcWebhookApiModule, BazaNcWithdrawApiModule, BazaNcDwollaApiModule],
    controllers: [BazaNcE2eController],
    providers: [BazaNcE2eService],
})
export class BazaNcE2eApiModule {}
