import { Injectable } from '@nestjs/common';
import {
    BazaNcE2eSetTradeStatusRequest,
    BazaNcE2eUpdateCCFundMoveStatusRequest,
    BazaNcE2eUpdateTradeStatusRequest,
    bazaNcApiConfig,
    NC_OFFERING_E2E,
    NorthCapitalTopic,
    BazaNcE2eSetFundingSourceNameRequest,
    BazaNcE2eEndpointPaths,
    BazaNcE2eGetTradeStatusRequest,
    BazaNcE2eGetTradeStatusResponse,
    BazaNcE2eGetNcAccountRequest,
    GetAccountResponse,
    BazaNcE2eGetNcPartyRequest,
    GetPartyResponse,
} from '@scaliolabs/baza-nc-shared';
import {
    NorthCapitalAccountsApiNestjsService,
    NorthCapitalPartiesApiNestjsService,
    NorthCapitalTradesApiNestjsService,
} from '../../../transact-api';
import { BazaNcWebhookService } from '../../../webhook/lib/services/baza-nc-webhook.service';
import { BazaNcInvestorAccountRepository, BazaNcOfferingEntity, BazaNcTransactionRepository } from '../../../typeorm';
import { BazaNcOfferingsService } from '../../../offering';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { DwollaFundingSourcesApiNestjsService } from '@scaliolabs/baza-dwolla-api';
import { BazaNcDwollaCustomerService } from '../../../dwolla';

/**
 * E2E Helpers for Testing NC API
 */
@Injectable()
export class BazaNcE2eService {
    constructor(
        private readonly ncWebhooks: BazaNcWebhookService,
        private readonly ncAccountApi: NorthCapitalAccountsApiNestjsService,
        private readonly ncPartyApi: NorthCapitalPartiesApiNestjsService,
        private readonly ncTradeApi: NorthCapitalTradesApiNestjsService,
        private readonly ncOfferingService: BazaNcOfferingsService,
        private readonly transactionRepository: BazaNcTransactionRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly ncDwollaCustomerService: BazaNcDwollaCustomerService,
        private readonly dwollaFundingSourcesApi: DwollaFundingSourcesApiNestjsService,
    ) {}

    /**
     * E2E Helper - Update trade status
     * @param request
     */
    async updateTradeStatus(request: BazaNcE2eUpdateTradeStatusRequest): Promise<void> {
        const tradeDetails = await this.ncTradeApi.getTrade({
            tradeId: request.tradeId,
            accountId: request.accountId,
        });

        await this.ncTradeApi.updateTradeStatus({
            tradeId: request.tradeId,
            accountId: request.accountId,
            orderStatus: request.orderStatus,
        });

        await this.ncWebhooks.publish({
            event: {
                topic: NorthCapitalTopic.updateTradeStatus,
                responseBody: {
                    ...tradeDetails.partyDetails[0],
                    tradeId: request.tradeId,
                    accountId: request.accountId,
                    orderStatus: request.orderStatus,
                } as any,
            },
        });
    }

    async getTradeStatus(request: BazaNcE2eGetTradeStatusRequest): Promise<BazaNcE2eGetTradeStatusResponse> {
        const trade = await this.transactionRepository.getByNcTradeId(request.tradeId);

        const ncResponse = await this.ncTradeApi.getTrade({
            tradeId: request.tradeId,
            accountId: trade.ncAccountId,
        });

        return {
            tradeId: request.tradeId,
            ncOrderStatus: ncResponse.partyDetails[ncResponse.partyDetails.length - 1].orderStatus,
        };
    }

    /**
     * E2E Helper - Set trade status (immediately, w/o webhooks)
     * @param request
     */
    async setTradeStatus(request: BazaNcE2eSetTradeStatusRequest): Promise<void> {
        const transaction = await this.transactionRepository.getByNcTradeId(request.tradeId);

        transaction.state = request.transactionState;
        transaction.ncOrderStatus = request.orderStatus;

        await this.transactionRepository.save(transaction);
    }

    /**
     * E2E Helper - Enable Mask Configuration (SSN, Credit Card, Bank Account Details)
     */
    async enableMaskConfig(): Promise<void> {
        bazaNcApiConfig({
            maskSSNFields: true,
            maskBankDetailsFields: true,
            maskCreditCardFields: true,
        });
    }

    /**
     * E2E Helper - Disable Mask Configuration (SSN, Credit Card, Bank Account Details)
     */
    async disableMaskConfig(): Promise<void> {
        bazaNcApiConfig({
            maskSSNFields: false,
            maskBankDetailsFields: false,
            maskCreditCardFields: false,
        });
    }

    /**
     * E2E Helper - Create an example NC Offering
     * @param issuer
     */
    async createExampleOffering(issuer: AccountEntity): Promise<BazaNcOfferingEntity> {
        return this.ncOfferingService.createOffering(issuer, {
            request: {
                offeringName: 'E2E Example Offering',
                targetAmount: 10000,
                maxAmount: 10000,
                currentValueCents: 1000000,
                unitPrice: 10,
                minAmount: 10,
                maxSharesPerAccount: 1000,
                startDate: '01-01-2021',
                endDate: '01-01-2030',
                offeringText: 'E2E Example Offering Descriptionm',
                stampingText: 'E2E',
                updatedIPaddress: '0.0.0.0',
                subscriptionOfferingIds: NC_OFFERING_E2E.docuSignExampleTemplateIds,
            },
        });
    }

    /**
     * E2E Helper - Enable Transaction Fee feature
     */
    async enableTransactionFeeFeature(): Promise<void> {
        bazaNcApiConfig({
            withTransactionFeeFeature: true,
        });
    }

    /**
     * E2E Helper - Disable Transaction Fee feature
     */
    async disableTransactionFeeFeature(): Promise<void> {
        bazaNcApiConfig({
            withTransactionFeeFeature: false,
        });
    }

    /**
     * E2E Helper - Update CC Transaction status
     * @param request
     */
    async updateCCFundMoveStatus(request: BazaNcE2eUpdateCCFundMoveStatusRequest): Promise<void> {
        const tradeDetails = await this.ncTradeApi.getTrade({
            tradeId: request.tradeId,
            accountId: request.accountId,
        });

        await this.ncWebhooks.publish({
            event: {
                topic: NorthCapitalTopic.updateCCFundMoveStatus,
                responseBody: {
                    ...tradeDetails.partyDetails[0],
                    tradeId: request.tradeId,
                    accountId: request.accountId,
                    fundStatus: request.fundMoveStatus,
                } as any,
            },
        });
    }

    /**
     * E2E Helper - Update ACH Transaction status
     * @param request
     */
    async updateExternalFundMoveStatus(request: BazaNcE2eUpdateCCFundMoveStatusRequest): Promise<void> {
        const tradeDetails = await this.ncTradeApi.getTrade({
            tradeId: request.tradeId,
            accountId: request.accountId,
        });

        await this.ncWebhooks.publish({
            event: {
                topic: NorthCapitalTopic.updateExternalFundMoveStatus,
                responseBody: {
                    ...tradeDetails.partyDetails[0],
                    tradeId: request.tradeId,
                    accountId: request.accountId,
                    fundStatus: request.fundMoveStatus,
                } as any,
            },
        });
    }

    /**
     * E2E Helper - Enable Dwolla Customer Auto-Touch during Dwolla Dividends Processing
     */
    async enableDwollaAutoTouch(): Promise<void> {
        bazaNcApiConfig({
            dwollaAutoTouchCustomers: true,
        });
    }

    /**
     * E2E Helper - Disable Dwolla Customer Auto-Touch during Dwolla Dividends Processing
     */
    async disableDwollaAutoTouch(): Promise<void> {
        bazaNcApiConfig({
            dwollaAutoTouchCustomers: false,
        });
    }

    /**
     * E2E Helper - Set Funding Source Name for Investor's Dwolla Wallet
     * Use DwollaFailureCodes to simulate different issues
     * @see DwollaFailureCodes
     * @param request
     */
    async setFundingSourceName(request: BazaNcE2eSetFundingSourceNameRequest): Promise<void> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(request.investorAccountId);
        const dwollaCustomerBalanceFundingSource = await this.ncDwollaCustomerService.getDwollaCustomerBalanceFundingSource(
            investorAccount.id,
        );

        await this.dwollaFundingSourcesApi.updateFundingSource(dwollaCustomerBalanceFundingSource.id, {
            name: request.name,
        });
    }

    /**
     * E2E Helper - Emulates NC Webhook
     * @param payload
     * @param topic
     */
    async simulateNcWebhook(payload: any, topic: NorthCapitalTopic): Promise<void> {
        await this.ncWebhooks.publish({
            event: {
                topic,
                responseBody: payload,
            } as any,
        });
    }

    /**
     * E2E Helper - Enables sync between NBA and LBA APIs
     */
    async enableLBASync(): Promise<void> {
        bazaNcApiConfig({
            enableLegacyBankSyncWithNBAS: true,
        });
    }

    /**
     * E2E Helper - Disables sync between NBA and LBA APIs
     */
    async disableBASync(): Promise<void> {
        bazaNcApiConfig({
            enableLegacyBankSyncWithNBAS: false,
        });
    }

    /**
     * E2E Helper - Returns NC Account
     * @param request
     */
    async getNcAccount(request: BazaNcE2eGetNcAccountRequest): Promise<GetAccountResponse> {
        return this.ncAccountApi.getAccount({
            accountId: request.ncAccountId,
        });
    }

    /**
     * E2E Helper - Returns NC Party
     * @param request
     */
    async getNcParty(request: BazaNcE2eGetNcPartyRequest): Promise<GetPartyResponse> {
        return this.ncPartyApi.getParty({
            partyId: request.ncPartyId,
        });
    }
}
