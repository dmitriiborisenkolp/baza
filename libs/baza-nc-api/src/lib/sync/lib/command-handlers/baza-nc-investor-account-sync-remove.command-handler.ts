import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaNcInvestorAccountSyncRemoveCommand, NcInvestorAccountSyncRemoveCommandResponse } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { InvestorAccountSyncService } from '../services/investor-account-sync.service';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcInvestorAccountSyncRemoveCommand)
export class BazaNcInvestorAccountSyncRemoveCommandHandler implements ICommandHandler<BazaNcInvestorAccountSyncRemoveCommand> {
    constructor(private readonly repository: BazaNcInvestorAccountRepository, private readonly syncService: InvestorAccountSyncService) {}

    async execute(command: BazaNcInvestorAccountSyncRemoveCommand): Promise<NcInvestorAccountSyncRemoveCommandResponse> {
        return cqrsCommand<NcInvestorAccountSyncRemoveCommandResponse>(BazaNcInvestorAccountSyncRemoveCommandHandler.name, async () => {
            const investorAccount = await this.repository.findInvestorAccountById(command.payload.investorAccountId);

            if (investorAccount) {
                return this.syncService.syncRemove({
                    investorAccountId: investorAccount.id,
                });
            } else {
                return {
                    affectedOfferingIds: [],
                };
            }
        });
    }
}
