import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import {
    BazaNcInvestorAccountSyncPaymentMethodsCommand,
    NcInvestorAccountSyncPaymentMethodsCommandResponse,
} from '@scaliolabs/baza-nc-shared';
import { InvestorAccountSyncService } from '../services/investor-account-sync.service';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcInvestorAccountSyncPaymentMethodsCommand)
export class BazaNcInvestorAccountSyncPaymentMethodsCommandHandler
    implements ICommandHandler<BazaNcInvestorAccountSyncPaymentMethodsCommand, NcInvestorAccountSyncPaymentMethodsCommandResponse>
{
    constructor(private readonly repository: BazaNcInvestorAccountRepository, private readonly syncService: InvestorAccountSyncService) {}

    async execute(command: BazaNcInvestorAccountSyncPaymentMethodsCommand): Promise<NcInvestorAccountSyncPaymentMethodsCommandResponse> {
        return cqrsCommand<NcInvestorAccountSyncPaymentMethodsCommandResponse>(
            BazaNcInvestorAccountSyncPaymentMethodsCommandHandler.name,
            async () => {
                const investorAccount = await this.repository.getInvestorAccountById(command.payload.investorAccountId);

                await this.syncService.syncPaymentMethods({
                    userId: investorAccount.user.id,
                });

                const updated = await this.repository.getInvestorAccountById(command.payload.investorAccountId);

                return {
                    isBankAccountLinked: updated.isBankAccountLinked,
                    isCreditCardLinked: updated.isCreditCardLinked,
                };
            },
        );
    }
}
