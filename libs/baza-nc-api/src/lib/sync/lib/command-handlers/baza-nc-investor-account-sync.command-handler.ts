import { CommandBus, CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaNcInvestorAccountSyncCommand, NcInvestorAccountSyncCommandResponse } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { InvestorAccountSyncService } from '../services/investor-account-sync.service';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcInvestorAccountSyncCommand)
export class BazaNcInvestorAccountSyncCommandHandler implements ICommandHandler<BazaNcInvestorAccountSyncCommand> {
    constructor(
        private readonly commandBus: CommandBus,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly syncService: InvestorAccountSyncService,
    ) {}

    async execute(command: BazaNcInvestorAccountSyncCommand): Promise<NcInvestorAccountSyncCommandResponse> {
        return cqrsCommand<NcInvestorAccountSyncCommandResponse>(BazaNcInvestorAccountSyncCommandHandler.name, async () => {
            const investorAccount = await this.investorAccountRepository.getInvestorAccountById(command.payload.investorAccountId);

            return this.syncService.sync({
                userId: investorAccount.user.id,
                ncAccountId: investorAccount.northCapitalAccountId,
                ncPartyId: investorAccount.northCapitalPartyId,
            });
        });
    }
}
