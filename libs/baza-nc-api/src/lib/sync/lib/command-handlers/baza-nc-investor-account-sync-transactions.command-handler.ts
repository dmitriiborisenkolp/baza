import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaNcInvestorAccountSyncTransactionsCommand, NcInvestorAccountSyncTransactionsCommandResponse } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { InvestorAccountSyncTransactionsService } from '../services/investor-account-sync-transactions.service';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcInvestorAccountSyncTransactionsCommand)
export class BazaNcInvestorAccountSyncTransactionsCommandHandler
    implements ICommandHandler<BazaNcInvestorAccountSyncTransactionsCommand, NcInvestorAccountSyncTransactionsCommandResponse>
{
    constructor(
        private readonly repository: BazaNcInvestorAccountRepository,
        private readonly transactionRepository: BazaNcTransactionRepository,
        private readonly syncTransactionsService: InvestorAccountSyncTransactionsService,
    ) {}

    async execute(command: BazaNcInvestorAccountSyncTransactionsCommand): Promise<NcInvestorAccountSyncTransactionsCommandResponse> {
        return cqrsCommand<NcInvestorAccountSyncTransactionsCommandResponse>(
            BazaNcInvestorAccountSyncTransactionsCommandHandler.name,
            async () => {
                const investorAccount = await this.repository.getInvestorAccountById(command.payload.investorAccountId);

                await this.syncTransactionsService.sync(investorAccount.user.id);

                return {
                    countTransactions: await this.transactionRepository.countTransactionsOfInvestorAccount(investorAccount),
                };
            },
        );
    }
}
