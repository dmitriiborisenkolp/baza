import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import {
    BazaNcInvestorAccountSyncAffectedOfferingsCommand,
    NcInvestorAccountSyncAffectedOfferingsCommandResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { BazaNcOfferingsPercentsFundedService } from '../../../offering';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcInvestorAccountSyncAffectedOfferingsCommand)
export class BazaNcInvestorAccountSyncAffectedOfferingsCommandHandler
    implements ICommandHandler<BazaNcInvestorAccountSyncAffectedOfferingsCommand, NcInvestorAccountSyncAffectedOfferingsCommandResponse>
{
    constructor(
        private readonly repository: BazaNcInvestorAccountRepository,
        private readonly transactionRepository: BazaNcTransactionRepository,
        private readonly ncOfferingPercentsFundedService: BazaNcOfferingsPercentsFundedService,
    ) {}

    async execute(
        command: BazaNcInvestorAccountSyncAffectedOfferingsCommand,
    ): Promise<NcInvestorAccountSyncAffectedOfferingsCommandResponse> {
        return cqrsCommand<NcInvestorAccountSyncAffectedOfferingsCommandResponse>(
            BazaNcInvestorAccountSyncAffectedOfferingsCommandHandler.name,
            async () => {
                const investorAccount = await this.repository.getInvestorAccountById(command.payload.investorAccountId);
                const affectedOfferingIds = await this.transactionRepository.findAffectedOfferingsOfInvestorAccount(investorAccount);

                for (const offeringId of affectedOfferingIds) {
                    await this.ncOfferingPercentsFundedService.updatePercentsFunded(offeringId);
                }

                return {
                    affectedOfferingIds,
                };
            },
        );
    }
}
