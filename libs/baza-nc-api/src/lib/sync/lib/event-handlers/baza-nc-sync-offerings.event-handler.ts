import {
    BazaNcPurchaseFlowTransactionType,
    BazaNcTransactionReturnedEvent,
    BazaNcWebhookEvent,
    mapNcTransactionStatusToTransactionState,
    NorthCapitalErrorCodes,
    NorthCapitalTopic,
    TransactionState,
    TransactionStatus,
} from '@scaliolabs/baza-nc-shared';
import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { isNorthCapitalException, NorthCapitalTradesApiNestjsService } from '../../../transact-api';
import { BazaNcOfferingRepository, BazaNcTransactionEntity, BazaNcTransactionRepository } from '../../../typeorm';
import { BazaNcOfferingsService } from '../../../offering';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcWebhookEvent)
export class BazaNcSyncOfferingsEventHandler implements IEventHandler<BazaNcWebhookEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly ncOfferingRepository: BazaNcOfferingRepository,
        private readonly ncOfferingsService: BazaNcOfferingsService,
        private readonly ncTradesApi: NorthCapitalTradesApiNestjsService,
        private readonly transactionsRepository: BazaNcTransactionRepository,
    ) {}

    handle(event: BazaNcWebhookEvent): void {
        cqrs(BazaNcSyncOfferingsEventHandler.name, async () => {
            const message = event.message;

            switch (message.topic) {
                case NorthCapitalTopic.createTrade: {
                    try {
                        const trade = await this.ncTradesApi.getTradeStatus({
                            tradeId: message.responseBody.tradeId,
                        });

                        const offeringId = trade.tradeDetails[0].offeringId;

                        if (await this.ncOfferingsService.hasOffering(offeringId)) {
                            await this.ncOfferingsService.syncWithNcOffering(offeringId);
                        }
                    } catch (err) {
                        if (!isNorthCapitalException(err, NorthCapitalErrorCodes.TradeIdDoesNotExits)) {
                            throw err;
                        }
                    }

                    break;
                }

                case NorthCapitalTopic.deleteTrade: {
                    try {
                        const trade = await this.ncTradesApi.getTradeStatus({
                            tradeId: message.responseBody.tradeId,
                        });

                        const offeringId = trade.tradeDetails[0].offeringId;

                        if (await this.ncOfferingsService.hasOffering(offeringId)) {
                            await this.ncOfferingsService.syncWithNcOffering(offeringId);
                        }
                    } catch (err) {
                        if (!isNorthCapitalException(err, NorthCapitalErrorCodes.TradeIdDoesNotExits)) {
                            throw err;
                        }
                    }

                    break;
                }

                case NorthCapitalTopic.updateTradeStatus: {
                    const offeringId = message.responseBody.offeringId;

                    if (await this.ncOfferingsService.hasOffering(offeringId)) {
                        await this.ncOfferingsService.syncWithNcOffering(message.responseBody.offeringId);
                    }

                    break;
                }

                case NorthCapitalTopic.updateCCFundMoveStatus: {
                    const trade = await this.transactionsRepository.findByNcTradeId(message.responseBody.tradeId);
                    await this.updateTransactionStatus(trade, message.responseBody?.fundStatus);
                    break;
                }

                case NorthCapitalTopic.updateExternalFundMoveStatus: {
                    const trade = await this.transactionsRepository.findByNcTradeId(message.responseBody.tradeId);
                    await this.updateTransactionStatus(trade, message.responseBody?.fundStatus);
                    break;
                }

                case NorthCapitalTopic.updateTradeTransactionType: {
                    const { tradeId, transactionType } = message.responseBody;
                    const trade = await this.transactionsRepository.findByNcTradeId(tradeId);
                    trade.transactionType = BazaNcPurchaseFlowTransactionType[transactionType];
                    await this.transactionsRepository.save(trade);
                    break;
                }
            }
        });
    }

    private async updateTransactionStatus(trade: BazaNcTransactionEntity | undefined, fundMoveStatus: TransactionStatus | undefined) {
        if (!trade || !fundMoveStatus) return;
        const status = mapNcTransactionStatusToTransactionState.find((c) =>
            c.transactionStatus.includes(fundMoveStatus.toUpperCase()),
        )?.transactionState;

        if (!status) return;

        trade.state = status;
        await this.transactionsRepository.save(trade);

        switch (status) {
            case TransactionState.PaymentReturned:
                await this.eventBus.publish(
                    new BazaNcTransactionReturnedEvent({
                        id: trade.id,
                        ncOfferingId: trade.ncOfferingId,
                        ncTradeId: trade.ncTradeId,
                    }),
                );

                break;
        }
    }
}
