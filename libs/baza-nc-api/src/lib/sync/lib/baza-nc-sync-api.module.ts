import { Module } from '@nestjs/common';
import { BazaNcSyncController } from './controllers/baza-nc-sync.controller';
import { InvestorAccountSyncService } from './services/investor-account-sync.service';
import { InvestorAccountSyncTransactionsService } from './services/investor-account-sync-transactions.service';
import { BazaNcInvestorAccountApiModule } from '../../investor-account';
import { BazaNcTransactionApiModule } from '../../transaction';
import { BazaNcOfferingApiModule } from '../../offering';
import { BazaNcPurchaseFlowApiModule } from '../../pucrhase-flow';
import { BazaNcAccountVerificationApiModule } from '../../account-verification';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaNcInvestorAccountSyncPaymentMethodsCommandHandler } from './command-handlers/baza-nc-investor-account-sync-payment-methods.command-handler';
import { BazaNcInvestorAccountSyncTransactionsCommandHandler } from './command-handlers/baza-nc-investor-account-sync-transactions.command-handler';
import { BazaNcInvestorAccountSyncAffectedOfferingsCommandHandler } from './command-handlers/baza-nc-investor-account-sync-affected-offerings.command-handler';
import { BazaNcInvestorAccountSyncCommandHandler } from './command-handlers/baza-nc-investor-account-sync.command-handler';
import { BazaNcInvestorAccountSyncRemoveCommandHandler } from './command-handlers/baza-nc-investor-account-sync-remove.command-handler';
import { BazaNcReportApiModule } from '../../report';
import { BazaNcSyncOfferingsEventHandler } from './event-handlers/baza-nc-sync-offerings.event-handler';

const eventHandlers = [BazaNcSyncOfferingsEventHandler];

const commandHandlers = [
    BazaNcInvestorAccountSyncPaymentMethodsCommandHandler,
    BazaNcInvestorAccountSyncTransactionsCommandHandler,
    BazaNcInvestorAccountSyncAffectedOfferingsCommandHandler,
    BazaNcInvestorAccountSyncCommandHandler,
    BazaNcInvestorAccountSyncRemoveCommandHandler,
];

@Module({
    imports: [
        CqrsModule,
        BazaNcInvestorAccountApiModule,
        BazaNcAccountVerificationApiModule,
        BazaNcPurchaseFlowApiModule,
        BazaNcTransactionApiModule,
        BazaNcOfferingApiModule,
        BazaNcReportApiModule,
    ],
    controllers: [BazaNcSyncController],
    providers: [InvestorAccountSyncService, InvestorAccountSyncTransactionsService, ...eventHandlers, ...commandHandlers],
    exports: [InvestorAccountSyncService, InvestorAccountSyncTransactionsService],
})
export class BazaNcSyncApiModule {}
