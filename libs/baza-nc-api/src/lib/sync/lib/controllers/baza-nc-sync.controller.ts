import {
    BazaNcSyncEndpoint,
    NcSyncEndpointPaths,
    NcSyncInvestorAccountPaymentMethodsRequest,
    NcSyncInvestorAccountRequest,
    NcSyncInvestorAccountResponse,
    NcSyncPercentsFundedForOfferingRequest,
    NcSyncRemoveInvestorAccountRequest,
    NcSyncRemoveInvestorAccountResponse,
    NcSyncTransactionsRequest,
} from '@scaliolabs/baza-nc-shared';
import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaNorthCapitalAcl, BazaNorthCapitalCMSOpenApi } from '@scaliolabs/baza-nc-shared';
import { InvestorAccountSyncService } from '../services/investor-account-sync.service';
import { AuthGuard, AuthRequireAdminRoleGuard, BazaLogger } from '@scaliolabs/baza-core-api';
import { WithAccessGuard } from '@scaliolabs/baza-core-api';
import { AclNodes } from '@scaliolabs/baza-core-api';
import { InvestorAccountSyncTransactionsService } from '../services/investor-account-sync-transactions.service';
import { BazaNcOfferingsPercentsFundedService } from '../../../offering';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalSyncCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcSync])
export class BazaNcSyncController implements BazaNcSyncEndpoint {
    constructor(
        private readonly logger: BazaLogger,
        private readonly syncService: InvestorAccountSyncService,
        private readonly syncTransactionsService: InvestorAccountSyncTransactionsService,
        private readonly syncPercentsFundedService: BazaNcOfferingsPercentsFundedService,
    ) {}

    @Post(NcSyncEndpointPaths.syncAllInvestorAccounts)
    @ApiOperation({
        summary: 'syncAllInvestorAccounts',
        description: 'Sync all investor accounts and investor account transactions',
    })
    @ApiOkResponse()
    async syncAllInvestorAccounts(): Promise<void> {
        this.syncService.syncAllInvestorAccounts(); // await is missed here for reason
    }

    @Post(NcSyncEndpointPaths.syncInvestorAccount)
    @ApiOperation({
        summary: 'syncInvestorAccount',
        description: 'Sync investor account and transactions',
    })
    @ApiOkResponse({
        type: NcSyncInvestorAccountResponse,
    })
    async syncInvestorAccount(@Body() request: NcSyncInvestorAccountRequest): Promise<NcSyncInvestorAccountResponse> {
        return this.syncService.sync(request);
    }

    @Post(NcSyncEndpointPaths.syncInvestorAccountPaymentMethods)
    @ApiOperation({
        summary: 'syncInvestorAccountPaymentMethods',
        description: 'Sync investor account payment methods',
    })
    @ApiOkResponse()
    async syncInvestorAccountPaymentMethods(@Body() request: NcSyncInvestorAccountPaymentMethodsRequest): Promise<void> {
        await this.syncService.syncPaymentMethods(request);
    }

    @Post(NcSyncEndpointPaths.syncAllInvestorAccountsPaymentMethods)
    @ApiOperation({
        summary: 'syncAllInvestorAccountsPaymentMethods',
        description: 'Sync payment methods for every investor account',
    })
    @ApiOkResponse()
    async syncAllInvestorAccountsPaymentMethods(): Promise<void> {
        this.syncService.syncAllInvestorAccountsPaymentMethods().catch((err) => this.logger.error(err, BazaNcSyncController.name)); // await is missed here for reason
    }

    @Post(NcSyncEndpointPaths.syncTransactions)
    @ApiOperation({
        summary: 'syncTransactions',
        description: 'Sync transactions only for investor account',
    })
    @ApiOkResponse()
    async syncTransactions(@Body() request: NcSyncTransactionsRequest): Promise<void> {
        const promise = this.syncTransactionsService.sync(request.userId, {
            syncOfferingPercentsFunded: true,
        });

        if (!request.async) {
            await promise;
        }
    }

    @Post(NcSyncEndpointPaths.syncPercentsFunded)
    @ApiOperation({
        summary: 'syncPercentsFunded',
        description: 'Sync percents funded for all offerings',
    })
    @ApiOkResponse()
    async syncPercentsFunded(): Promise<void> {
        await this.syncPercentsFundedService.updateAllOfferings();
    }

    @Post(NcSyncEndpointPaths.syncPercentsFundedOfOffering)
    @ApiOperation({
        summary: 'syncPercentsFundedOfOffering',
        description: 'Sync percents funded for offering',
    })
    @ApiOkResponse()
    async syncPercentsFundedOfOffering(@Body() request: NcSyncPercentsFundedForOfferingRequest): Promise<void> {
        await this.syncPercentsFundedService.updatePercentsFunded(request.offeringId);
    }

    @Post(NcSyncEndpointPaths.syncPercentsFundedOfOffering)
    @ApiOperation({
        summary: 'syncRemove',
        description: 'Remove (exclude) previously synced data',
    })
    @ApiOkResponse({
        type: NcSyncRemoveInvestorAccountResponse,
    })
    async syncRemove(@Body() request: NcSyncRemoveInvestorAccountRequest): Promise<NcSyncRemoveInvestorAccountResponse> {
        return this.syncService.syncRemove(request);
    }
}
