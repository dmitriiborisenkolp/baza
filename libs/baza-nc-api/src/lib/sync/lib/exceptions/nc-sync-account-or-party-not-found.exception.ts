import { BazaAppException } from '@scaliolabs/baza-core-api';
import { NorthCapitalAccountId, PartyId } from '@scaliolabs/baza-nc-shared';
import { BazaNcSyncErrorCodes } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class NcSyncAccountOrPartyNotFoundException extends BazaAppException {
    constructor(args: { ncAccountId: NorthCapitalAccountId; ncPartyId: PartyId }) {
        super(
            BazaNcSyncErrorCodes.BazaNcSyncAccountOrPartyNotFound,
            'Account "{{ ncAccountId }}" or party "{{ ncPartyId }}" was not found',
            HttpStatus.NOT_FOUND,
            args,
        );
    }
}
