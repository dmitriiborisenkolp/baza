import { BazaAppException } from '@scaliolabs/baza-core-api';
import { NorthCapitalAccountId, PartyId } from '@scaliolabs/baza-nc-shared';
import { BazaNcSyncErrorCodes } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class NcSyncPartyIsNotOwnedByAccountException extends BazaAppException {
    constructor(args: { ncAccountId: NorthCapitalAccountId; ncPartyId: PartyId }) {
        super(
            BazaNcSyncErrorCodes.BazaNcSyncAccountOrPartyNotFound,
            'Party "{{ ncPartyId }}" is not part of "{{ ncAccountId }}" account',
            HttpStatus.CONFLICT,
            args,
        );
    }
}
