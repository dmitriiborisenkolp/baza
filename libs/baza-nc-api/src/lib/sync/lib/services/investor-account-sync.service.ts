import { Injectable, Logger } from '@nestjs/common';
import { AccountEntity, BazaAccountRepository } from '@scaliolabs/baza-core-api';
import {
    AccountDetails,
    AccreditedStatus,
    NcSyncInvestorAccountPaymentMethodsRequest,
    NcSyncInvestorAccountResponse,
    NcSyncRemoveInvestorAccountRequest,
    NcSyncRemoveInvestorAccountResponse,
    NorthCapitalAccountId,
    PartyDetails,
    PartyId,
} from '@scaliolabs/baza-nc-shared';
import { InvestorAccountSyncTransactionsService } from './investor-account-sync-transactions.service';
import { NcSyncAccountOrPartyNotFoundException } from '../exceptions/nc-sync-account-or-party-not-found.exception';
import { NcSyncInvestorAccountRequest } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationAccountSetupService } from '../../../account-verification';
import { BazaNcInvestorAccountService } from '../../../investor-account';
import { INVESTOR_ACCOUNT_RELATIONS, BazaNcInvestorAccountRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { NorthCapitalAccountsApiNestjsService, NorthCapitalPartiesApiNestjsService } from '../../../transact-api';
import { BazaNcPurchaseFlowBankAccountService, BazaNcPurchaseFlowCreditCardService } from '../../../pucrhase-flow';
import { BazaNcOfferingsPercentsFundedService } from '../../../offering';

/**
 * Sync Tools for Baza NC Investor Account
 */
@Injectable()
export class InvestorAccountSyncService {
    constructor(
        private readonly accountRepository: BazaAccountRepository,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly ncAccountsApi: NorthCapitalAccountsApiNestjsService,
        private readonly ncPartyApi: NorthCapitalPartiesApiNestjsService,
        private readonly accountVerificationAccountSetup: BazaNcAccountVerificationAccountSetupService,
        private readonly syncTransactionsService: InvestorAccountSyncTransactionsService,
        private readonly purchaseFlowBankService: BazaNcPurchaseFlowBankAccountService,
        private readonly purchaseFlowCreditCardService: BazaNcPurchaseFlowCreditCardService,
        private readonly transactionRepository: BazaNcTransactionRepository,
        private readonly ncOfferingPercentsFundedService: BazaNcOfferingsPercentsFundedService,
    ) {}

    /**
     * Syncronize Bank Account, Credit Card and Transactions of Investor Account
     * When finished will update Percents Funded of affected Offerings
     * @param request
     */
    async sync(request: NcSyncInvestorAccountRequest): Promise<NcSyncInvestorAccountResponse> {
        const account = await this.accountRepository.getActiveAccountWithId(request.userId);

        const updateResponse = await this.updateInvestorAccount(account, request);

        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(account.id);

        await this.accountVerificationAccountSetup.updateAccountVerificationInProgressAndCompletedFlags(account);

        await this.purchaseFlowBankService.updateBankAccountStatus(investorAccount);
        await this.purchaseFlowCreditCardService.updateCreditCardStatus(investorAccount);
        await this.syncTransactionsService.sync(account.id);

        const affectedOfferingIds = await this.transactionRepository.findAffectedOfferingsOfInvestorAccount(investorAccount);

        for (const offeringId of affectedOfferingIds) {
            await this.ncOfferingPercentsFundedService.updatePercentsFunded(offeringId);
        }

        return {
            email: updateResponse.ncParty.emailAddress,
            countTransactions: await this.transactionRepository.countTransactionsOfInvestorAccount(investorAccount),
            isBankAccountLinked: investorAccount.isBankAccountLinked,
            isCreditCardLinked: investorAccount.isCreditCardLinked,
            affectedOfferingIds,
        };
    }

    /**
     * Updates Offering Status when Investor Account is Removed
     * @param request
     */
    async syncRemove(request: NcSyncRemoveInvestorAccountRequest): Promise<NcSyncRemoveInvestorAccountResponse> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountById(request.investorAccountId);

        if (investorAccount) {
            const affectedOfferingIds = await this.transactionRepository.findAffectedOfferingsOfInvestorAccount(investorAccount);

            for (const offeringId of affectedOfferingIds) {
                await this.ncOfferingPercentsFundedService.updatePercentsFunded(offeringId);
            }

            return {
                affectedOfferingIds,
            };
        } else {
            return {
                affectedOfferingIds: [],
            };
        }
    }

    /**
     * Sync Payment Methods (Bank Account, Credit Card Status) of Investor Account
     * @param request
     */
    async syncPaymentMethods(request: NcSyncInvestorAccountPaymentMethodsRequest): Promise<void> {
        const account = await this.accountRepository.getActiveAccountWithId(request.userId);
        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(account.id);

        if (investorAccount) {
            await this.purchaseFlowBankService.updateBankAccountStatus(investorAccount);
            await this.purchaseFlowCreditCardService.updateCreditCardStatus(investorAccount);
        }
    }

    /**
     * Sync Payment Methods for all Investor Accounts
     */
    async syncAllInvestorAccountsPaymentMethods(): Promise<void> {
        const investorAccounts = await this.investorAccountRepository.repository.find({
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });

        for (const investorAccount of investorAccounts) {
            if (investorAccount.northCapitalAccountId && investorAccount.northCapitalPartyId) {
                await this.syncPaymentMethods({
                    userId: investorAccount.user.id,
                }).catch((err) => {
                    Logger.error(`[InvestorAccountSyncService] [syncAllInvestorAccountsPaymentMethods] Error:`);
                    Logger.error(err);
                });
            }
        }
    }

    /**
     * Sync all Investor Accounts about everything
     */
    async syncAllInvestorAccounts(): Promise<void> {
        const investorAccounts = await this.investorAccountRepository.repository.find({
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });

        for (const investorAccount of investorAccounts) {
            if (investorAccount.northCapitalAccountId && investorAccount.northCapitalPartyId) {
                await this.sync({
                    userId: investorAccount.user.id,
                    ncPartyId: investorAccount.northCapitalPartyId,
                    ncAccountId: investorAccount.northCapitalAccountId,
                }).catch((err) => {
                    Logger.error(`[InvestorAccountSyncService] [syncAllInvestorAccounts] Error:`);
                    Logger.error(err);
                });
            }
        }
    }

    /**
     * Updates NC Account Id, NC Party ID and KYC Status of Investor Account
     * Used for assigning NC Account and NC Party to Investor Account
     * @param account
     * @param request
     */
    async updateInvestorAccount(
        account: AccountEntity,
        request: NcSyncInvestorAccountRequest,
    ): Promise<{
        ncParty: PartyDetails;
        ncAccount: AccountDetails;
    }> {
        const investorAccount = await this.investorAccountService.getInvestorAccountByUserId(account.id);

        const ncEntities = await this.updateEmails(account, request);

        investorAccount.northCapitalAccountId = request.ncAccountId;
        investorAccount.northCapitalPartyId = request.ncPartyId;
        investorAccount.isAccreditedInvestor = [AccreditedStatus.SelfAccredited, AccreditedStatus.VerifiedAccredited]
            .map((next) => next.toLowerCase())
            .includes(ncEntities.ncAccount.accreditedStatus.toLowerCase());

        await this.investorAccountRepository.save(investorAccount);
        await this.accountVerificationAccountSetup.updateIsInvestorVerifiedFlag(account, ncEntities.ncParty.kycStatus);

        return ncEntities;
    }

    /**
     * Update Emails both for NC Account and NC Party from Baza Account
     * @param account
     * @param request
     */
    async updateEmails(
        account: AccountEntity,
        request: {
            ncAccountId: NorthCapitalAccountId;
            ncPartyId: PartyId;
        },
    ): Promise<{
        ncParty: PartyDetails;
        ncAccount: AccountDetails;
    }> {
        const ncEntities = await (async () => {
            try {
                const ncAccount = await this.ncAccountsApi.getAccount({
                    accountId: request.ncAccountId,
                });

                const ncParty = (
                    await this.ncPartyApi.getParty({
                        partyId: request.ncPartyId,
                    })
                ).partyDetails[0];

                return {
                    ncAccount,
                    ncParty,
                };
            } catch (err) {
                throw new NcSyncAccountOrPartyNotFoundException({
                    ncAccountId: request.ncAccountId,
                    ncPartyId: request.ncPartyId,
                });
            }
        })();

        if (ncEntities.ncAccount.accountDetails.email !== account.email) {
            await this.ncAccountsApi.updateAccount({
                accountId: ncEntities.ncAccount.accountDetails.accountId,
                accountRegistration: account.fullName,
                email: account.email,
            });

            ncEntities.ncAccount.accountDetails.email = account.email;
        }

        if (ncEntities.ncParty.emailAddress !== account.email) {
            await this.ncPartyApi.updateParty({
                partyId: ncEntities.ncParty.partyId,
                emailAddress: account.email,
            });

            ncEntities.ncParty.emailAddress = account.email;
        }

        return {
            ncAccount: ncEntities.ncAccount.accountDetails,
            ncParty: ncEntities.ncParty,
        };
    }
}
