import { Injectable } from '@nestjs/common';
import {
    bazaNcOfferingName,
    convertToCents,
    FundStatus,
    GetAccountTradeHistoryResponseEntry,
    mapNcTransactionStatusToTransactionState,
    ncOrderStatusToTransactionState,
    NorthCapitalAccountId,
    OfferingId,
    OrderStatus,
    purchaseFlowTransactionTypeToNcTransactionTypeMap,
    TradeId,
    TradeStatusMap,
    TransactionState,
    TransactionStatus,
    TransactionType,
} from '@scaliolabs/baza-nc-shared';
import { BazaSentryApiService } from '@scaliolabs/baza-core-api';
import * as _ from 'underscore';
import * as moment from 'moment';
import {
    BazaNcInvestorAccountEntity,
    BazaNcInvestorAccountRepository,
    BazaNcOfferingEntity,
    BazaNcOfferingRepository,
    BazaNcTransactionEntity,
    BazaNcTransactionRepository,
} from '../../../typeorm';
import {
    NorthCapitalAchTransfersApiNestjsService,
    NorthCapitalCreditCardTransactionApiNestjsService,
    NorthCapitalTradesApiNestjsService,
} from '../../../transact-api';
import { BazaNcPurchaseFlowLimitsService } from '../../../pucrhase-flow';
import { BazaNcOfferingsPercentsFundedService, BazaNcOfferingTransactionFeesService } from '../../../offering';
import { BazaDwollaPaymentEntity, BazaDwollaPaymentService } from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentStatus, BazaDwollaPaymentType, DwollaTransferId } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcReportService } from '../../../report';

type AchTradeStatuses = { [tradeId: string]: any };
type CreditCardTradeStatuses = { [tradeId: string]: any };

/**
 * Synchronise transactions tool
 * @see InvestorAccountSyncTransactionsService.sync
 */
@Injectable()
export class InvestorAccountSyncTransactionsService {
    constructor(
        private readonly investorAccount: BazaNcInvestorAccountRepository,
        private readonly ncTradesApi: NorthCapitalTradesApiNestjsService,
        private readonly ncACHApi: NorthCapitalAchTransfersApiNestjsService,
        private readonly ncCreditCardApi: NorthCapitalCreditCardTransactionApiNestjsService,
        private readonly bazaDwollaPaymentService: BazaDwollaPaymentService,
        private readonly transactionsRepository: BazaNcTransactionRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
        private readonly bazaSentryService: BazaSentryApiService,
        private readonly limitsService: BazaNcPurchaseFlowLimitsService,
        private readonly transactionFeeService: BazaNcOfferingTransactionFeesService,
        private readonly ncReportService: BazaNcReportService,
        private readonly ncOfferingPercentsFundedService: BazaNcOfferingsPercentsFundedService,
    ) {}

    /**
     * Synchronize Transactions from NC API and Dwolla Payment API
     * Synchronisations works in 2 steps:
     *  1. Deletes all known transactions of Investor from local DB
     *  2. Reads trade related information from NC API and Dwolla Payment API & recreate transactions
     * @param accountId
     * @param options
     */
    async sync(
        accountId: number,
        options = {
            syncOfferingPercentsFunded: false,
        },
    ): Promise<void> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(accountId);

        await this.transactionsRepository.deleteAllTransactionsOfAccount(investorAccount.user);

        if (!investorAccount.northCapitalAccountId) {
            return;
        }

        const tradesHistory = (
            await this.ncTradesApi.getAccountTradeHistory({
                accountId: investorAccount.northCapitalAccountId,
            })
        ).accountDetails;

        const dwollaReportsSync: Array<[TradeId, DwollaTransferId]> = [];
        const [achTradesStatuses, creditCardTradesStatuses] = await this.getLatestTradeStatuses(investorAccount.northCapitalAccountId);
        const offeringIds: Record<OfferingId, true> = {};

        for (const ncTrade of tradesHistory) {
            try {
                const offering = await this.offeringRepository.findOfferingWithId(ncTrade.offeringId);

                offeringIds[offering.ncOfferingId] = true;

                if (!offering) {
                    continue;
                }

                if (ncTrade.transactionType === TransactionType.TBD) {
                    const syncTBDTradeResponse = await this.syncTBDTrade(accountId, ncTrade);

                    if (syncTBDTradeResponse) {
                        const [transactionStatus, dwollaPayment] = syncTBDTradeResponse;

                        if (![OrderStatus.Created, OrderStatus.Canceled].includes(transactionStatus)) {
                            await this.createTransactionEntity(ncTrade, investorAccount, offering, transactionStatus);
                        }

                        dwollaReportsSync.push([ncTrade.orderId, dwollaPayment.dwollaTransferId]);
                    }
                } else if (this.isFailedTrade(ncTrade, { ...achTradesStatuses, ...creditCardTradesStatuses })) {
                    await this.handleFailedTrade(ncTrade, investorAccount, offering, { ...achTradesStatuses, ...creditCardTradesStatuses });
                } else {
                    const transactionStatus = await this.syncNcAchOrCreditCardTrade(
                        accountId,
                        ncTrade.orderId,
                        achTradesStatuses,
                        creditCardTradesStatuses,
                    );

                    if (![OrderStatus.Created, OrderStatus.Canceled].includes(transactionStatus)) {
                        await this.createTransactionEntity(ncTrade, investorAccount, offering, transactionStatus);
                    }
                }
            } catch (err) {
                this.bazaSentryService.captureException(err);
            }
        }

        for (const pair of dwollaReportsSync) {
            await this.ncReportService.syncByDwollaPayment(pair[0], pair[1]);
        }

        if (options.syncOfferingPercentsFunded) {
            for (const offeringId of Object.keys(offeringIds)) {
                await this.ncOfferingPercentsFundedService.updatePercentsFunded(offeringId);
            }
        }
    }

    /**
     * Returns latest trade statuses from ExternalFundMove and CCFundMove historiees
     * @param ncAccountId
     * @private
     */
    private async getLatestTradeStatuses(ncAccountId: NorthCapitalAccountId): Promise<[AchTradeStatuses, CreditCardTradeStatuses]> {
        const externalFundMoveHistory = (
            await (async () => {
                try {
                    return await this.ncACHApi.getExternalFundMoveHistory({
                        accountId: ncAccountId,
                    });
                } catch (err) {
                    return {
                        accountDetails: [],
                    };
                }
            })()
        ).accountDetails;

        const creditCardHistory = (
            await (async () => {
                try {
                    return await this.ncCreditCardApi.getCCFundMoveHistory({
                        accountId: ncAccountId,
                    });
                } catch (err) {
                    return {
                        creditcardDetails: [],
                    };
                }
            })()
        ).creditcardDetails;

        const achTradesStatuses: AchTradeStatuses = {};
        const creditCardTradesStatuses: CreditCardTradeStatuses = {};

        for (const tradeId of _.uniq(externalFundMoveHistory.map((e) => e.tradeId))) {
            const sorted = externalFundMoveHistory
                .filter((e) => e.tradeId === tradeId)
                .sort(
                    (a, b) =>
                        moment(b.createdDate, 'YYYY-MM-DD HH:mm:ss').toDate().getTime() -
                        moment(b.createdDate, 'YYYY-MM-DD HH:mm:ss').toDate().getTime(),
                );

            if (sorted.length) {
                achTradesStatuses[tradeId] = sorted[0];
            }
        }

        for (const tradeId of _.uniq(creditCardHistory.map((e) => e.tradeId))) {
            const sorted = creditCardHistory
                .filter((e) => e.tradeId === tradeId)
                .sort(
                    (a, b) =>
                        moment(b.createdDate, 'YYYY-MM-DD HH:mm:ss').toDate().getTime() -
                        moment(b.createdDate, 'YYYY-MM-DD HH:mm:ss').toDate().getTime(),
                );

            if (sorted.length) {
                creditCardTradesStatuses[tradeId] = sorted[0];
            }
        }

        return [achTradesStatuses, creditCardTradesStatuses];
    }

    /**
     * Returns OrderStatus  which should be set for NC Trade with ACH or CreditCard Transaction Type
     * If method returns `undefined`, in this case NC Trade should be ignored
     * @param accountId
     * @param ncTradeId
     * @param achTradesStatuses
     * @param creditCardTradesStatuses
     * @private
     */
    private async syncNcAchOrCreditCardTrade(
        accountId: number,
        ncTradeId: TradeId,
        achTradesStatuses: TradeStatusMap,
        creditCardTradesStatuses: TradeStatusMap,
    ): Promise<OrderStatus | undefined> {
        const tradeStatus = await this.ncTradesApi.getTradeStatus({
            tradeId: ncTradeId,
        });

        if ([OrderStatus.Canceled].includes(tradeStatus.tradeDetails[0].orderStatus)) {
            return undefined;
        }

        const tradeId = tradeStatus.tradeDetails[0].orderId;
        const orderStatus = tradeStatus.tradeDetails[0].orderStatus;

        // TODO: We should find a way to simplify it and get idea how Order Status, Transaction Status
        // TODO: and Fund Status should works together
        if (orderStatus === OrderStatus.Created) {
            const { transactionstatus, fundStatus } = achTradesStatuses[tradeId] || creditCardTradesStatuses[tradeId] || {};

            const foundTransactionStatus = this.getTradeStatus(transactionstatus);

            switch (foundTransactionStatus) {
                case 'PENDING':
                    return OrderStatus.Pending;
                case 'DECLINED':
                    return OrderStatus.Rejected;
                case 'VOID':
                case 'VOIDED':
                    return OrderStatus.Canceled;
                case 'APPROVED': {
                    const foundFundStatus = this.getTradeStatus(fundStatus);
                    const fundedFundStatuses = [FundStatus.Settled];
                    const returnedFundStatuses = [FundStatus.Returned, FundStatus.Voided];

                    if (returnedFundStatuses.includes(foundFundStatus)) {
                        return OrderStatus.UnwindSettled;
                    } else if (fundedFundStatuses.includes(foundFundStatus)) {
                        return OrderStatus.Funded;
                    } else {
                        return OrderStatus.Pending;
                    }
                }
            }
        }

        return orderStatus;
    }

    /**
     * Returns OrderStatus which should be set for synced NC Trade with TBD Transaction Type
     * Additionally it updates NC Order Status in case if current NC Order Status does not match current Dwolla Payment Status
     * If method returns `undefined`, in this case NC Trade should be ignored
     * @param accountId
     * @param ncTrade
     * @private
     */
    private async syncTBDTrade(
        accountId: number,
        ncTrade: GetAccountTradeHistoryResponseEntry,
    ): Promise<[OrderStatus, BazaDwollaPaymentEntity]> {
        const dwollaPayments = (
            await this.bazaDwollaPaymentService.list(accountId, {
                size: 1,
                ncTradeId: ncTrade.orderId,
                types: [BazaDwollaPaymentType.PurchaseShares],
            })
        ).items;

        if (!dwollaPayments.length) {
            return undefined;
        }

        const dwollaPayment = await this.bazaDwollaPaymentService.sync(dwollaPayments[0].ulid);
        const dwollaPaymentStatus = dwollaPayment.status;

        const mapDwollaPaymentStatusToOrderStatus: Record<BazaDwollaPaymentStatus, OrderStatus> = {
            [BazaDwollaPaymentStatus.Pending]: OrderStatus.Pending,
            [BazaDwollaPaymentStatus.Cancelled]: OrderStatus.Canceled,
            [BazaDwollaPaymentStatus.Failed]: OrderStatus.Rejected,
            [BazaDwollaPaymentStatus.Processed]: OrderStatus.Funded,
        };

        const transactionStatus = mapDwollaPaymentStatusToOrderStatus[dwollaPaymentStatus];

        if (dwollaPaymentStatus === BazaDwollaPaymentStatus.Failed && ncTrade.orderStatus !== OrderStatus.Rejected) {
            await this.updateNcTradeStatus(ncTrade.orderId, ncTrade.accountId, OrderStatus.Rejected);

            return [OrderStatus.Rejected, dwollaPayment];
        } else if (dwollaPaymentStatus === BazaDwollaPaymentStatus.Cancelled && ncTrade.orderStatus !== OrderStatus.Canceled) {
            await this.updateNcTradeStatus(ncTrade.orderId, ncTrade.accountId, OrderStatus.Canceled);

            return undefined;
        } else if (dwollaPaymentStatus === BazaDwollaPaymentStatus.Processed && ncTrade.orderStatus === OrderStatus.Settled) {
            return [OrderStatus.Settled, dwollaPayment];
        } else if (
            dwollaPaymentStatus === BazaDwollaPaymentStatus.Processed &&
            ![OrderStatus.Funded, OrderStatus.Settled].includes(ncTrade.orderStatus)
        ) {
            await this.updateNcTradeStatus(ncTrade.orderId, ncTrade.accountId, OrderStatus.Funded);

            return [OrderStatus.Funded, dwollaPayment];
        } else {
            return [transactionStatus, dwollaPayment];
        }
    }

    /**
     * Updates Order Status
     * Used for TBS Trade synchronisation. As far as we're storing Dwolla Payments separately from NC Trades / Transactions,
     * it may be possible that we will process Dwolla Payment while NC Trade will not be stored in our database. In this
     * case we're updating NC Trade Statuses during transactions sync, if it's required.
     * @param ncTradeId
     * @param setOrderStatus
     * @private
     */
    private async updateNcTradeStatus(ncTradeId: TradeId, ncAccountId: NorthCapitalAccountId, setOrderStatus: OrderStatus): Promise<void> {
        await this.ncTradesApi.updateTradeStatus({
            tradeId: ncTradeId,
            accountId: ncAccountId,
            orderStatus: setOrderStatus,
        });
    }

    /**
     * Creates TransactionEntity to local DB
     * @param ncTrade
     * @param investorAccount
     * @param offering
     * @param orderStatus
     * @param transactionStatus
     * @private
     */
    private async createTransactionEntity(
        ncTrade: GetAccountTradeHistoryResponseEntry,
        investorAccount: BazaNcInvestorAccountEntity,
        offering: BazaNcOfferingEntity,
        orderStatus: OrderStatus,
        transactionStatus?: TransactionState,
    ): Promise<BazaNcTransactionEntity> {
        const entity = new BazaNcTransactionEntity();

        entity.createdAt = new Date(ncTrade.trade_createdDate);
        entity.account = investorAccount.user;
        entity.investorAccount = investorAccount;
        entity.ncOrderStatus = orderStatus;
        entity.state = transactionStatus ?? ncOrderStatusToTransactionState(orderStatus);
        entity.ncOfferingId = ncTrade.offeringId;
        entity.ncOfferingName = bazaNcOfferingName(offering);
        entity.ncTradeId = ncTrade.orderId;
        entity.ncAccountId = investorAccount.northCapitalAccountId;
        entity.pricePerShareCents = convertToCents(offering.ncUnitPrice);
        entity.amountCents = convertToCents(parseFloat(ncTrade.totalAmount));
        entity.feeCents = 0;
        entity.totalCents = entity.amountCents;
        entity.shares = Math.ceil(entity.amountCents / entity.pricePerShareCents);
        entity.transactionFees = offering.ncOfferingFees || 0;
        entity.transactionFeesCents = 0;
        entity.submitted = true;

        const transactionTypeDef = purchaseFlowTransactionTypeToNcTransactionTypeMap.find(
            (def) => def.ncTransactionType === ncTrade.transactionType,
        );

        if (this.transactionFeeService.shouldChargeTransactionFees && entity.transactionFees) {
            entity.amountCents = this.transactionFeeService.percentageDecrementedFees(entity.amountCents, offering.ncOfferingFees);
            entity.transactionFeesCents = entity.totalCents - entity.amountCents;
            entity.feeCents += entity.transactionFeesCents;
            entity.shares = Math.ceil(entity.amountCents / entity.pricePerShareCents);
        }

        if (transactionTypeDef) {
            entity.transactionType = transactionTypeDef.purchaseFlowTransactionType;

            const limits = await this.limitsService.limitsForPurchase(entity.amountCents);
            const limitsDef = limits.find((l) => l.transactionType === transactionTypeDef.purchaseFlowTransactionType);

            if (limitsDef) {
                entity.totalCents = limitsDef.amountWithFeesCents + entity.transactionFeesCents;
                entity.feeCents += limitsDef.feeCents;
            }
        }

        await this.transactionsRepository.save(entity);

        return entity;
    }

    /**
     *
     * @param ncTrade
     * @param tradeStatuses
     * @returns
     */
    private isFailedTrade(ncTrade: GetAccountTradeHistoryResponseEntry, tradeStatuses: { [tradeId: string]: any }): boolean {
        const foundFundStatus = tradeStatuses[ncTrade.orderId]?.fundStatus as FundStatus;

        return foundFundStatus && foundFundStatus === FundStatus.Returned;
    }

    /**
     *
     * @param ncTrade
     * @param investorAccount
     * @param offering
     * @param tradeStatuses
     * @returns
     */
    private async handleFailedTrade(
        ncTrade: GetAccountTradeHistoryResponseEntry,
        investorAccount: BazaNcInvestorAccountEntity,
        offering: BazaNcOfferingEntity,
        tradeStatuses: { [tradeId: string]: any },
    ): Promise<BazaNcTransactionEntity> {
        const tradeStatus = await this.ncTradesApi.getTradeStatus({
            tradeId: ncTrade.orderId,
        });

        const tradeId = tradeStatus.tradeDetails[0].orderId;
        const orderStatus = tradeStatus.tradeDetails[0].orderStatus;

        const foundFundStatus = tradeStatuses[tradeId].fundStatus.toUpperCase() as FundStatus;

        const transactionStatus = mapNcTransactionStatusToTransactionState.find((c) =>
            c.transactionStatus.includes(foundFundStatus),
        )?.transactionState;

        return this.createTransactionEntity(ncTrade, investorAccount, offering, orderStatus, transactionStatus);
    }

    /**
     *
     * @param status
     * @returns
     */
    private getTradeStatus(status: TransactionStatus): TransactionStatus;
    private getTradeStatus(status: FundStatus): FundStatus;
    private getTradeStatus<T extends string>(status: T): T {
        if (!status) return undefined;

        return status.toUpperCase() as T;
    }
}
