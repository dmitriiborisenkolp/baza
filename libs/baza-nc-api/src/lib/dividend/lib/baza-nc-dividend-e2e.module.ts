import { forwardRef, Module } from '@nestjs/common';
import { BazaNcDividendApiModule } from './baza-nc-dividend-api.module';
import { BazaApiBundleModule, BazaE2eFixturesApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationApiModule } from '../../account-verification';
import { BazaNcDwollaApiModule } from '../../dwolla';
import { BazaNcDividendTransactionFixture } from './integration-tests/fixtures/baza-nc-dividend-transaction.fixture';
import { BazaNcDividendProcessedTransactionFixture } from './integration-tests/fixtures/baza-nc-dividend-processed-transaction.fixture';
import { BazaNcDividendProcessedTransactionMultipleUsersFixture } from './integration-tests/fixtures/baza-nc-dividend-processed-transaction-multiple-users.fixture';
import { BazaNcDividendProcessedTransactionFilterAndCsvFixture } from './integration-tests/fixtures/baza-nc-dividend-processed-transaction-filter-and-csv.fixture';
import { BazaNcDividendDwollaTransactionFixture } from './integration-tests/fixtures/baza-nc-dividend-dwolla-transaction.fixture';

const E2E_FIXTURES = [
    BazaNcDividendTransactionFixture,
    BazaNcDividendProcessedTransactionFixture,
    BazaNcDividendProcessedTransactionMultipleUsersFixture,
    BazaNcDividendProcessedTransactionFilterAndCsvFixture,
    BazaNcDividendDwollaTransactionFixture,
];

@Module({
    imports: [
        // TODO: Remove it after baza-core e2e modules migration
        forwardRef(() => BazaApiBundleModule),
        forwardRef(() => BazaE2eFixturesApiModule),
        forwardRef(() => BazaNcAccountVerificationApiModule),
        forwardRef(() => BazaNcDividendApiModule),
        forwardRef(() => BazaNcDwollaApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcDividendE2eModule {}
