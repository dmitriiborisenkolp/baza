import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import {
    BazaNcDividendCmsDeleteRequest,
    BazaNcDividendCmsDto,
    BazaNcDividendCmsEndpoint,
    BazaNcDividendCmsEndpointPaths,
    BazaNcDividendCmsExportToCsvRequest,
    BazaNcDividendCmsGetByUlidRequest,
    BazaNcDividendCmsListRequest,
    BazaNcDividendCmsListResponse,
    BazaNorthCapitalAcl,
    BazaNorthCapitalCMSOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendCmsService } from '../../services/baza-nc-dividend-cms.service';
import { BazaNcDividendCmsMapper } from '../../mappers/baza-nc-dividend-cms.mapper';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalDividendCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcDividend])
export class BazaNcDividendCmsController implements BazaNcDividendCmsEndpoint {
    constructor(private readonly service: BazaNcDividendCmsService, private readonly mapper: BazaNcDividendCmsMapper) {}

    @Post(BazaNcDividendCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of dividends',
    })
    @ApiOkResponse({
        type: BazaNcDividendCmsListResponse,
    })
    async list(@Body() request: BazaNcDividendCmsListRequest): Promise<BazaNcDividendCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcDividendCmsEndpointPaths.getByUlid)
    @ApiOperation({
        summary: 'getByUlid',
        description: 'Returns Dividend by ULID',
    })
    @ApiOkResponse({
        type: BazaNcDividendCmsDto,
    })
    async getByUlid(@Body() request: BazaNcDividendCmsGetByUlidRequest): Promise<BazaNcDividendCmsDto> {
        const entity = await this.service.getByUlid(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcDividendCmsEndpointPaths.delete)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendManagement])
    @ApiOperation({
        summary: 'delete',
        description: 'Deletes existing dividend',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaNcDividendCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(BazaNcDividendCmsEndpointPaths.exportToCsv)
    @ApiOperation({
        summary: 'exportToCsv',
        description: 'Exports dividends to CSV',
    })
    @ApiOkResponse({
        content: {
            'text/csv': {},
        },
    })
    async exportToCsv(@Body() request: BazaNcDividendCmsExportToCsvRequest): Promise<string> {
        return this.service.exportToCsv(request);
    }
}
