import { Body, Controller, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcDividendTransactionEntryCmsCreateRequest,
    BazaNcDividendTransactionEntryCmsDeleteRequest,
    BazaNcDividendTransactionEntryCmsEndpoint,
    BazaNcDividendTransactionEntryCmsEndpointPaths,
    BazaNcDividendTransactionEntryCmsGetByUlidRequest,
    BazaNcDividendTransactionEntryCmsListRequest,
    BazaNcDividendTransactionEntryCmsListResponse,
    BazaNcDividendTransactionEntryCmsUpdateRequest,
    BazaNcDividendTransactionEntryDto,
    BazaNcDividendTransactionEntryExportCsvRequest,
    BazaNcDividendTransactionEntryImportCsvRequest,
    BazaNcDividendTransactionEntryImportCsvResponse,
    BazaNorthCapitalAcl,
    BazaNorthCapitalCMSOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, CsvService, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaNcDividendTransactionEntryMapper } from '../../mappers/baza-nc-dividend-transaction-entry.mapper';
import { BazaNcDividendTransactionEntryCmsService } from '../../services/baza-nc-dividend-transaction-entry-cms.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { BazaNcDividendImportCsvService } from '../../services/baza-nc-dividend-import-csv.service';
import { BazaNcDividendImportCsvFailedToParseException } from '../../exceptions/baza-nc-dividend-import-csv-failed-to-parse.exception';
import {} from 'multer';
import { BAZA_CRUD_CONSTANTS } from '@scaliolabs/baza-core-shared';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalDividendTransactionEntriesCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransaction])
export class BazaNcDividendTransactionEntryCmsController implements BazaNcDividendTransactionEntryCmsEndpoint {
    constructor(
        private readonly csv: CsvService,
        private readonly mapper: BazaNcDividendTransactionEntryMapper,
        private readonly service: BazaNcDividendTransactionEntryCmsService,
        private readonly importCsvService: BazaNcDividendImportCsvService,
    ) {}

    @Post(BazaNcDividendTransactionEntryCmsEndpointPaths.create)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransactionManagement])
    @ApiOperation({
        summary: 'create',
        description: 'Creates new Dividend Transaction Entry',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionEntryDto,
    })
    async create(@Body() request: BazaNcDividendTransactionEntryCmsCreateRequest): Promise<BazaNcDividendTransactionEntryDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcDividendTransactionEntryCmsEndpointPaths.update)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransactionManagement])
    @ApiOperation({
        summary: 'update',
        description: 'Update existing non-Pending Dividend Transaction Entry',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionEntryDto,
    })
    async update(@Body() request: BazaNcDividendTransactionEntryCmsUpdateRequest): Promise<BazaNcDividendTransactionEntryDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcDividendTransactionEntryCmsEndpointPaths.delete)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransactionManagement])
    @ApiOperation({
        summary: 'delete',
        description: 'Deletes existing non-Pending Dividend Transaction Entry',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaNcDividendTransactionEntryCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(BazaNcDividendTransactionEntryCmsEndpointPaths.getByUlid)
    @ApiOperation({
        summary: 'getByUlid',
        description: 'Returns Dividends Transaction Entry by ULID',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionEntryDto,
    })
    async getByUlid(@Body() request: BazaNcDividendTransactionEntryCmsGetByUlidRequest): Promise<BazaNcDividendTransactionEntryDto> {
        const entity = await this.service.getByUlid(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcDividendTransactionEntryCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Dividend Transaction Entries',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionEntryCmsListResponse,
    })
    async list(@Body() request: BazaNcDividendTransactionEntryCmsListRequest): Promise<BazaNcDividendTransactionEntryCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcDividendTransactionEntryCmsEndpointPaths.importCsv)
    @UseInterceptors(FileInterceptor('file'))
    @ApiOperation({
        summary: 'importCsv',
        description: 'Import Dividend Transaction Entries from CSV file',
    })
    @ApiOkResponse()
    async importCsv(
        @UploadedFile() file: Express.Multer.File,
        @Body() request: BazaNcDividendTransactionEntryImportCsvRequest,
    ): Promise<BazaNcDividendTransactionEntryImportCsvResponse> {
        let csv: Array<Array<string>>;

        try {
            csv = await this.csv.parseCsvFile(file, request.csvDelimiter || BAZA_CRUD_CONSTANTS.csvDefaultDelimiter);
        } catch (err) {
            throw new BazaNcDividendImportCsvFailedToParseException();
        }

        return this.importCsvService.importCsv({
            ...request,
            csv,
        });
    }

    @Post(BazaNcDividendTransactionEntryCmsEndpointPaths.exportToCsv)
    @ApiOperation({
        summary: 'exportToCsv',
        description: 'Exports list of Dividend Transaction Entries to CSV',
    })
    @ApiOkResponse({
        content: {
            'text/csv': {},
        },
    })
    exportToCsv(@Body() request: BazaNcDividendTransactionEntryExportCsvRequest): Promise<string> {
        return this.service.exportToCsv(request);
    }
}
