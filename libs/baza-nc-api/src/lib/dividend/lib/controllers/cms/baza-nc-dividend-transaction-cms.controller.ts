import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcDividendTransactionCmsCreateRequest,
    BazaNcDividendTransactionCmsDeleteRequest,
    BazaNcDividendTransactionCmsEndpoint,
    BazaNcDividendTransactionCmsEndpointPaths,
    BazaNcDividendTransactionCmsExportToCsvRequest,
    BazaNcDividendTransactionCmsGetByUlidRequest,
    BazaNcDividendTransactionCmsListRequest,
    BazaNcDividendTransactionCmsListResponse,
    BazaNcDividendTransactionCmsProcessRequest,
    BazaNcDividendTransactionCmsProcessResponse,
    BazaNcDividendTransactionCmsReprocessRequest,
    BazaNcDividendTransactionCmsReprocessResponse,
    BazaNcDividendTransactionCmsSendProcessConfirmationRequest,
    BazaNcDividendTransactionCmsSyncRequest,
    BazaNcDividendTransactionDto,
    BazaNorthCapitalAcl,
    BazaNorthCapitalCMSOpenApi,
} from '@scaliolabs/baza-nc-shared';
import {
    AccountEntity,
    AclNodes,
    AuthGuard,
    AuthRequireAdminRoleGuard,
    BazaLogger,
    CorrelationId,
    ReqAccount,
    WithAccessGuard,
} from '@scaliolabs/baza-core-api';
import { BazaNcDividendTransactionMapper } from '../../mappers/baza-nc-dividend-transaction.mapper';
import { BazaNcDividendTransactionCmsService } from '../../services/baza-nc-dividend-transaction-cms.service';
import { BazaNcDividendTransactionEntryRepository } from '../../../../typeorm';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalDividendTransactionCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransaction])
export class BazaNcDividendTransactionCmsController implements BazaNcDividendTransactionCmsEndpoint {
    constructor(
        private readonly mapper: BazaNcDividendTransactionMapper,
        private readonly service: BazaNcDividendTransactionCmsService,
        private readonly entriesRepository: BazaNcDividendTransactionEntryRepository,
        private readonly logger: BazaLogger,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
        description: 'Creates new Dividend Transaction',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionDto,
    })
    async create(@Body() request: BazaNcDividendTransactionCmsCreateRequest): Promise<BazaNcDividendTransactionDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.delete)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransactionManagement])
    @ApiOperation({
        summary: 'delete',
        description:
            'Deletes existing Dividend Transaction. Dividend Transaction which is processing or has successful entries cannot be deleted',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaNcDividendTransactionCmsDeleteRequest): Promise<void> {
        const entity = await this.service.getByUlid(request);

        await this.service.delete(entity);
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Dividend Transactions',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionDto,
    })
    async list(@Body() request: BazaNcDividendTransactionCmsListRequest): Promise<BazaNcDividendTransactionCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.getByUlid)
    @ApiOperation({
        summary: 'getByUlid',
        description: 'Returns Dividend Transaction by ULID',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionDto,
    })
    async getByUlid(@Body() request: BazaNcDividendTransactionCmsGetByUlidRequest): Promise<BazaNcDividendTransactionDto> {
        const entity = await this.service.getByUlid(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.process)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransactionManagement])
    @ApiOperation({
        summary: 'process',
        description: 'Process Dividend Transaction',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionCmsProcessResponse,
    })
    async process(
        @Body() request: BazaNcDividendTransactionCmsProcessRequest,
        @ReqAccount() account: AccountEntity,
        @CorrelationId() correlationId: string,
    ): Promise<BazaNcDividendTransactionCmsProcessResponse> {
        this.logger.info(`${this.constructor.name}.${this.process.name}`, {
            correlationId,
            account,
            request,
        });

        return this.service.process(request, account, correlationId);
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.reprocess)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransactionManagement])
    @ApiOperation({
        summary: 'reprocess',
        description: 'Re-Process Dividend Transaction Entries',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionCmsReprocessResponse,
    })
    async reprocess(@Body() request: BazaNcDividendTransactionCmsReprocessRequest): Promise<BazaNcDividendTransactionCmsReprocessResponse> {
        await this.service.reprocess(request);

        const dividendTransaction = await this.service.getByUlid({
            ulid: request.ulid,
        });

        if (Array.isArray(request.entryULIDs) && request.entryULIDs.length > 0) {
            const entries = await this.entriesRepository.findByUlids(request.entryULIDs);

            return {
                newStatus: dividendTransaction.status,
                entries: entries.map((next) => ({
                    status: next.status,
                    failureReason: next.failureReason,
                })),
            };
        } else {
            return {
                newStatus: dividendTransaction.status,
            };
        }
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.sync)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransactionManagement])
    @ApiOperation({
        summary: 'sync',
        description: 'Sync dividend transaction statuses with external sources',
    })
    @ApiOkResponse()
    async sync(@Body() request: BazaNcDividendTransactionCmsSyncRequest): Promise<void> {
        await this.service.sync(request);
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.sendProcessConfirmation)
    @AclNodes([BazaNorthCapitalAcl.BazaNcDividendTransactionManagement])
    @ApiOperation({
        summary: 'sendProcessConfirmation',
        description: 'Sends email to confirm start processing on Dividend Transaction',
    })
    @ApiOkResponse({
        type: BazaNcDividendTransactionDto,
    })
    async sendProcessConfirmation(
        @Body() request: BazaNcDividendTransactionCmsSendProcessConfirmationRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<void> {
        await this.service.sendProcessConfirmation(request, account);
    }

    @Post(BazaNcDividendTransactionCmsEndpointPaths.exportToCsv)
    @ApiOperation({
        summary: 'exportToCsv',
        description: 'Exports dividend transactions to CSV',
    })
    @ApiOkResponse({
        content: {
            'text/csv': {},
        },
    })
    async exportToCsv(@Body() request: BazaNcDividendTransactionCmsExportToCsvRequest): Promise<string> {
        return this.service.exportToCsv(request);
    }
}
