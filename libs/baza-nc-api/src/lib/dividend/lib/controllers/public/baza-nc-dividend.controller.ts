import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiExtraModels, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcDividendDto,
    BazaNcDividendEndpoint,
    BazaNcDividendEndpointPaths,
    BazaNcDividendListRequest,
    BazaNcDividendListResponse,
    bazaNcDividendPaymentHistorySwaggerExportedDtos,
    BazaNcDividendReprocessRequest,
    BazaNcDividendTotalAmountResponse,
    BazaNorthCapitalOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendService } from '../../services/baza-nc-dividend.service';
import { BazaNcDividendMapper } from '../../mappers/baza-nc-dividend.mapper';
import { InvestorAccountOptionalGuard, ReqInvestorAccountId } from '../../../../investor-account';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalDividend)
@ApiExtraModels(...bazaNcDividendPaymentHistorySwaggerExportedDtos)
@UseGuards(InvestorAccountOptionalGuard)
export class BazaNcDividendController implements BazaNcDividendEndpoint {
    constructor(private readonly service: BazaNcDividendService, private readonly mapper: BazaNcDividendMapper) {}

    @Post(BazaNcDividendEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List dividends of current account',
    })
    @ApiOkResponse({
        type: BazaNcDividendListResponse,
    })
    async list(
        @Body() request: BazaNcDividendListRequest,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<BazaNcDividendListResponse> {
        return this.service.list(request, investorAccountId);
    }

    @Get(BazaNcDividendEndpointPaths.getByUlid)
    @ApiOperation({
        summary: 'getByUlid',
        description: 'Returns dividend by ULID',
    })
    @ApiOkResponse({
        type: BazaNcDividendDto,
    })
    async getByUlid(@Param('ulid') ulid: string, @ReqInvestorAccountId() investorAccountId: number): Promise<BazaNcDividendDto> {
        const dividend = await this.service.getByUlid(ulid, investorAccountId);

        return this.mapper.entityToDTO(dividend);
    }

    @Get(BazaNcDividendEndpointPaths.totalAmount)
    @ApiOperation({
        summary: 'totalAmount',
        description: 'Returns total amount dividends of current account',
    })
    @ApiOkResponse({
        type: BazaNcDividendTotalAmountResponse,
    })
    async totalAmount(@ReqInvestorAccountId() investorAccountId: number): Promise<BazaNcDividendTotalAmountResponse> {
        return this.service.totalAmount(investorAccountId);
    }

    @Post(BazaNcDividendEndpointPaths.reprocess)
    @ApiOperation({
        summary: 'reprocess',
        description: 'Reprocess Failed or Cancelled dividends',
    })
    @ApiOkResponse({
        type: BazaNcDividendDto,
    })
    async reprocess(
        @Body() request: BazaNcDividendReprocessRequest,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<BazaNcDividendDto> {
        const entity = await this.service.reprocess(request, investorAccountId);

        return this.mapper.entityToDTO(entity);
    }
}
