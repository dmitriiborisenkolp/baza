import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendDwollaNoCustomerBalanceAvailableException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendDwollaNoCustomerBalanceAvailable,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendDwollaNoCustomerBalanceAvailable],
            HttpStatus.BAD_REQUEST,
        );
    }
}
