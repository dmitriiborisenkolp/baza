import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BAZA_NC_DIVIDEND_CONSTANTS, BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendImportCsvTooManyRecordsException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendImportCsvTooManyRecords,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendImportCsvTooManyRecords],
            HttpStatus.BAD_REQUEST,
            { max: BAZA_NC_DIVIDEND_CONSTANTS.maxCsvFileLines },
        );
    }
}
