import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendImportCsvUnknownColumnException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendImportCsvDuplicateColumns,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendImportCsvDuplicateColumns],
            HttpStatus.BAD_REQUEST,
        );
    }
}
