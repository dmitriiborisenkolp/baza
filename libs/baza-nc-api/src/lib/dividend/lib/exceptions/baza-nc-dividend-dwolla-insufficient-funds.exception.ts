import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendDwollaInsufficientFundsException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendDwollaInsufficientFunds,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendDwollaInsufficientFunds],
            HttpStatus.BAD_REQUEST,
        );
    }
}
