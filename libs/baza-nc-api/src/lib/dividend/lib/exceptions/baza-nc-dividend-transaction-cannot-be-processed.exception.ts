import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendTransactionCannotBeProcessedException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeProcessed,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeProcessed],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
