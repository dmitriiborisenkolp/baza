import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendTransactionCannotBeDeletedException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeDeleted,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeDeleted],
            HttpStatus.BAD_REQUEST,
        );
    }
}
