import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendImportCsvNoEntriesException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendImportCsvNoEntries,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendImportCsvNoEntries],
            HttpStatus.BAD_REQUEST,
        );
    }
}
