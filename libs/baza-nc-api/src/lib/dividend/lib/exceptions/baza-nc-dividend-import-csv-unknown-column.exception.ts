import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendImportCsvUnknownColumnException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendImportCsvUnknownColumn,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendImportCsvUnknownColumn],
            HttpStatus.BAD_REQUEST,
        );
    }
}
