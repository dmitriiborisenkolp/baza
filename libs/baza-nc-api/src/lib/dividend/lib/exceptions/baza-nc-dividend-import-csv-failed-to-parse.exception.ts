import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendImportCsvFailedToParseException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendImportCsvFailedToParse,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendImportCsvFailedToParse],
            HttpStatus.BAD_REQUEST,
        );
    }
}
