import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendTransactionIsEmptyException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendTransactionIsEmpty,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendTransactionIsEmpty],
            HttpStatus.BAD_REQUEST,
        );
    }
}
