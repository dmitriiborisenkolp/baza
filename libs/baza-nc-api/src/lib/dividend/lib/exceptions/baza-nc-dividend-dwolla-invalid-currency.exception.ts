import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendDwollaInvalidCurrencyException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendDwollaInvalidCurrency,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendDwollaInvalidCurrency],
            HttpStatus.BAD_REQUEST,
        );
    }
}
