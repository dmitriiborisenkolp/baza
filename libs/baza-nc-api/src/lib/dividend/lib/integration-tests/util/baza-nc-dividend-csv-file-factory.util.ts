import * as fs from 'fs';
import * as process from 'process';
import * as path from 'path';
import { BAZA_CRUD_CONSTANTS } from '@scaliolabs/baza-core-shared';

export function bazaNcDividendCsvFileFactory(csvEntries: Array<Array<string>>, delimiter = BAZA_CRUD_CONSTANTS.csvDefaultDelimiter) {
    const csvPath = path.join(process.cwd(), 'tmp', 'tmp.csv');
    const lines: Array<string> = csvEntries.map((line) => line.join(delimiter));

    fs.writeFileSync(csvPath, lines.join('\n'));

    return fs.createReadStream(csvPath, {
        autoClose: true,
    });
}
