export function bazaNcDividendExtractConfirmToken(mail: string): string {
    const regex = /confirm=(.*)/;

    return (mail.match(regex) || [])[1];
}
