import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import {
    BazaNcDividendPaymentSource,
    BazaNcDividendTransactionCmsCreateRequest,
    BazaNcDividendTransactionEntryCmsCreateRequest,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendTransactionCmsService } from '../../services/baza-nc-dividend-transaction-cms.service';
import { BazaNcDividendTransactionEntryCmsService } from '../../services/baza-nc-dividend-transaction-entry-cms.service';
import { BazaNcDividendFixtures } from '../baza-nc-dividend.fixtures';
import { BazaNcAccountVerificationE2eUser2Fixture, BazaNcAccountVerificationE2eUserFixture } from '../../../../account-verification';
import { BazaNcDividendProcessingService } from '../../services/baza-nc-dividend-processing.service';
import { BazaNcOfferingMultipleFixture } from '../../../../offering';

const date = '2022-06-18T14:28:17.727Z';
const date2 = '2022-06-10T14:28:17.727Z';
const date3 = '2022-06-13T14:28:17.727Z';
const date4 = '2022-07-01T14:28:17.727Z';

interface FixtureRequest {
    transaction: BazaNcDividendTransactionCmsCreateRequest;
    entries: Array<{
        email: 'e2e@scal.io' | 'e2e-2@scal.io';
        request: Omit<BazaNcDividendTransactionEntryCmsCreateRequest, 'dividendTransactionUlid' | 'investorAccountId'>;
    }>;
}

const FIXTURE: Array<FixtureRequest> = [
    {
        transaction: {
            title: 'DT 1',
        },
        entries: [
            {
                email: 'e2e@scal.io',
                request: {
                    date,
                    amountCents: 1000,
                    source: BazaNcDividendPaymentSource.NC,
                    offeringId: 'alpha',
                },
            },
            {
                email: 'e2e@scal.io',
                request: {
                    date: date2,
                    amountCents: 1250,
                    source: BazaNcDividendPaymentSource.NC,
                    offeringId: 'beta',
                },
            },
            {
                email: 'e2e-2@scal.io',
                request: {
                    date: date3,
                    amountCents: 1497,
                    source: BazaNcDividendPaymentSource.NC,
                    offeringId: 'alpha',
                },
            },
        ],
    },
    {
        transaction: {
            title: 'DT 2',
        },
        entries: [
            {
                email: 'e2e@scal.io',
                request: {
                    date: date4,
                    amountCents: 1200,
                    source: BazaNcDividendPaymentSource.NC,
                    offeringId: 'alpha',
                },
            },
        ],
    },
];

@Injectable()
export class BazaNcDividendProcessedTransactionFilterAndCsvFixture implements BazaE2eFixture {
    constructor(
        private readonly dividendTransactionService: BazaNcDividendTransactionCmsService,
        private readonly dividendTransactionEntriesService: BazaNcDividendTransactionEntryCmsService,
        private readonly dividendTransactionProcessService: BazaNcDividendProcessingService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of FIXTURE) {
            const dividendTransaction = await this.dividendTransactionService.create(fixtureRequest.transaction);

            for (const entryFixtureRequest of fixtureRequest.entries) {
                const investorAccount =
                    entryFixtureRequest.email === 'e2e@scal.io'
                        ? BazaNcAccountVerificationE2eUserFixture._e2eUserInvestorAccount
                        : BazaNcAccountVerificationE2eUser2Fixture._e2eUserInvestorAccount;

                await this.dividendTransactionEntriesService.create({
                    ...entryFixtureRequest.request,
                    investorAccountId: investorAccount.id,
                    dividendTransactionUlid: dividendTransaction.ulid,
                    offeringId:
                        entryFixtureRequest.request.offeringId === 'alpha'
                            ? BazaNcOfferingMultipleFixture.NC_OFFERING_ID_1
                            : BazaNcOfferingMultipleFixture.NC_OFFERING_ID_2,
                });
            }

            await this.dividendTransactionProcessService.process({
                dividendTransactionUlid: dividendTransaction.ulid,
                async: false,
            });
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaNcDividendFixtures.BazaNcDividendProcessedTransactionFilterAndCsvFixture,
        provider: BazaNcDividendProcessedTransactionFilterAndCsvFixture,
    },
]);
