import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import {
    BazaNcDividendPaymentSource,
    BazaNcDividendTransactionCmsCreateRequest,
    BazaNcDividendTransactionEntryCmsCreateRequest,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendTransactionCmsService } from '../../services/baza-nc-dividend-transaction-cms.service';
import { BazaNcDividendTransactionEntryCmsService } from '../../services/baza-nc-dividend-transaction-entry-cms.service';
import { BazaNcDividendFixtures } from '../baza-nc-dividend.fixtures';
import { BazaNcAccountVerificationE2eUserFixture } from '../../../../account-verification';
import { BazaNcOfferingFixture } from '../../../../offering';

const date = '2022-06-18T14:28:17.727Z';
const date2 = '2022-06-10T14:28:17.727Z';
const date3 = '2022-06-13T14:28:17.727Z';
const date4 = '2022-07-01T14:28:17.727Z';

interface FixtureRequest {
    transaction: BazaNcDividendTransactionCmsCreateRequest;
    entries: Array<Omit<BazaNcDividendTransactionEntryCmsCreateRequest, 'dividendTransactionUlid' | 'investorAccountId' | 'offeringId'>>;
}

const FIXTURE: Array<FixtureRequest> = [
    {
        transaction: {
            title: 'DT 1',
        },
        entries: [
            {
                date,
                amountCents: 1000,
                source: BazaNcDividendPaymentSource.NC,
            },
            {
                date: date2,
                amountCents: 1250,
                source: BazaNcDividendPaymentSource.NC,
            },
            {
                date: date3,
                amountCents: 1497,
                source: BazaNcDividendPaymentSource.NC,
            },
        ],
    },
    {
        transaction: {
            title: 'DT 2',
        },
        entries: [
            {
                date: date4,
                amountCents: 1200,
                source: BazaNcDividendPaymentSource.NC,
            },
        ],
    },
];

@Injectable()
export class BazaNcDividendTransactionFixture implements BazaE2eFixture {
    constructor(
        private readonly dividendTransactionService: BazaNcDividendTransactionCmsService,
        private readonly dividendTransactionEntriesService: BazaNcDividendTransactionEntryCmsService,
    ) {}

    async up(): Promise<void> {
        const investorAccount = BazaNcAccountVerificationE2eUserFixture._e2eUserInvestorAccount;

        for (const fixtureRequest of FIXTURE) {
            const dividendTransaction = await this.dividendTransactionService.create(fixtureRequest.transaction);

            for (const entryFixtureRequest of fixtureRequest.entries) {
                await this.dividendTransactionEntriesService.create({
                    ...entryFixtureRequest,
                    investorAccountId: investorAccount.id,
                    dividendTransactionUlid: dividendTransaction.ulid,
                    offeringId: BazaNcOfferingFixture.NC_OFFERING_ID,
                });
            }
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaNcDividendFixtures.BazaNcDividendTransactionFixture,
        provider: BazaNcDividendTransactionFixture,
    },
]);
