export enum BazaNcDividendFixtures {
    BazaNcDividendTransactionFixture = 'BazaNcDividendTransactionFixture',
    BazaNcDividendProcessedTransactionFixture = 'BazaNcDividendProcessedTransactionFixture',
    BazaNcDividendProcessedTransactionMultipleUsersFixture = 'BazaNcDividendProcessedTransactionMultipleUsersFixture',
    BazaNcDividendProcessedTransactionFilterAndCsvFixture = 'BazaNcDividendProcessedTransactionFilterAndCsvFixture',
    BazaNcDividendDwollaTransactionFixture = 'BazaNcDividendDwollaTransactionFixture',
}
