import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { BazaNcDividendPaymentSource, NorthCapitalAccountId, OfferingId } from '@scaliolabs/baza-nc-shared';
import { bazaNcDividendCsvFileFactory } from '../util/baza-nc-dividend-csv-file-factory.util';
import * as moment from 'moment';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/006-baza-nc-dividend-import-csv-different-delimiter.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessDividends = new BazaNcDividendNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);

    let dividendTransactionUlid: string;
    let ncOfferingId: OfferingId;
    let ncAccountId: NorthCapitalAccountId;

    const newDividendTransaction = async () => {
        const dividendTransactionResponse = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(dividendTransactionResponse));

        dividendTransactionUlid = dividendTransactionResponse.ulid;

        return dividendTransactionResponse;
    };

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = false;
    });

    it('will fetch all required resources', async () => {
        const offering = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(offering)).toBeFalsy();
        expect(offering.length).toBe(1);

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        ncAccountId = investorAccount.northCapitalAccountId;
        ncOfferingId = offering[0].ncOfferingId;
    });

    it('will successfully imports dividends from CSV file with ";" (CSV Excel) delimiter', async () => {
        const createDividendTransactionEntryDate = new Date();

        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory(
            [
                ['North Capital ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
                [ncAccountId, '$50.00', '4/15/2022', ncOfferingId],
                [ncAccountId, '$52.00', '4/12/2022', ncOfferingId],
                [ncAccountId, '$12.00', '3/10/2022', ncOfferingId],
            ],
            ';',
        );

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
            csvDelimiter: ';',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeTruthy();
        expect(response.errors).not.toBeDefined();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will process dividends transaction', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(dividendTransaction.dateProcessedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
        expect(moment(dividendTransaction.dateProcessedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will display dividends for user', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividends.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(3);

        expect(response.items[0].amount).toBe('50.00');
        expect(response.items[1].amount).toBe('52.00');
        expect(response.items[2].amount).toBe('12.00');
    });
});
