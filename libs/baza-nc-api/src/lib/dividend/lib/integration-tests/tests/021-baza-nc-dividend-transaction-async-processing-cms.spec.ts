import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountCmsNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { awaitTimeout, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendPaymentSource, BazaNcDividendTransactionStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcDividendFixtures } from '../baza-nc-dividend.fixtures';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import * as moment from 'moment/moment';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

let token: string;

describe('@scaliolabs/baza-nc-api/dividend/021-baza-nc-dividend-transaction-async-processing-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccessDividendTransactionEntriesCms = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessInvestorAccountCms = new BazaNcInvestorAccountCmsNodeAccess(http);

    const date = '2022-06-18T14:28:17.727Z';

    let dividendTransactionUlid: string;
    let investorAccountId: number;
    let offeringId: OfferingId;
    let ulid: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcDividendFixtures.BazaNcDividendTransactionFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will fetch offering id', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBeGreaterThan(0);

        offeringId = response[0].ncOfferingId;
    });

    it('will create a dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT async',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('DT async');

        dividendTransactionUlid = response.ulid;
    });

    it('will fetch investor account', async () => {
        const response = await dataAccessInvestorAccountCms.listInvestorAccounts({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        investorAccountId = response.items[0].id;
    });

    it('will display empty list of entries for the new dividends', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will create an entry for the dividends', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.create({
            date,
            dividendTransactionUlid,
            investorAccountId,
            offeringId,
            source: BazaNcDividendPaymentSource.NC,
            amountCents: 1000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        ulid = response.ulid;
    });

    it('will display created entry in entries list for the new dividend', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].ulid).toBe(ulid);
    });

    it('will send process confirmation', async () => {
        const response = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will contains a confirmation email and token in mailbox', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);
    });

    it('will process dividends with in async manner', async () => {
        const response = await dataAccessDividendTransactionCms.process({
            token,
            async: true,
            password: 'e2e-admin-password',
        });
        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.newStatus).toBe(BazaNcDividendTransactionStatus.InProgress);
    });

    it('will process transaction after a bit', async () => {
        await asyncExpect(
            async () => {
                const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });
                expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

                expect(dividendTransaction.status).toBe(BazaNcDividendTransactionStatus.Successful);
                expect(dividendTransaction.dateUpdatedAt).toBeDefined();
                expect(dividendTransaction.dateProcessedAt).toBeDefined();
            },
            null,
            { intervalMillis: 5000 },
        );
    });
});
