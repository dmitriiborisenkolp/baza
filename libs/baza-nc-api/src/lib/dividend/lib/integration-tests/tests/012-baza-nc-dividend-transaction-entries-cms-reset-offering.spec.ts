import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountCmsNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import * as moment from 'moment/moment';

jest.setTimeout(240000);

const date = '2022-06-18T14:28:17.727Z';
const date2 = '2022-06-17T14:28:17.727Z';

describe('@scaliolabs/baza-nc-api/dividend/012-baza-nc-dividend-transaction-entries-cms-reset-offering.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividend = new BazaNcDividendNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccessDividendTransactionEntriesCms = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessInvestorAccountCms = new BazaNcInvestorAccountCmsNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);

    let ulid: string;
    let offeringId: OfferingId;
    let investorAccountId: number;
    let dividendTransactionUlid: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will fetch offering id', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBeGreaterThan(0);

        offeringId = response[0].ncOfferingId;
    });

    it('will fetch investor account', async () => {
        const response = await dataAccessInvestorAccountCms.listInvestorAccounts({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        investorAccountId = response.items[0].id;
    });

    it('will create a new dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('DT 1');

        dividendTransactionUlid = response.ulid;
    });

    it('will create an entry for DT1 with Offering', async () => {
        const createDividendTransactionEntryDate = new Date();

        const response = await dataAccessDividendTransactionEntriesCms.create({
            date,
            dividendTransactionUlid,
            investorAccountId,
            offeringId,
            source: BazaNcDividendPaymentSource.NC,
            amountCents: 1000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.date).toBe(date);
        expect(response.dateCreatedAt).not.toBe(date);
        expect(response.dateUpdatedAt).toBeUndefined();
        expect(response.dateProcessedAt).toBeUndefined();

        expect(response.status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.investorAccount.id).toBe(investorAccountId);
        expect(response.offeringId).toBe(offeringId);

        expect(response.amount).toBe('10.00');
        expect(response.amountCents).toBe(1000);

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);

        ulid = response.ulid;
    });

    it('will display selected offering in get response', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.getByUlid({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.ulid).toBe(ulid);
        expect(response.offeringId).toBe(offeringId);
        expect(response.offeringTitle).toBe('E2E NC Offering Fixture');
    });

    it('will allow to clear offering ID for dividend transaction entry', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.update({
            date,
            offeringId: undefined,
            amountCents: 1000,
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display anymore selected offering in get response', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.getByUlid({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.ulid).toBe(ulid);
        expect(response.offeringId).not.toBeDefined();
        expect(response.offeringTitle).toBe('N/A');
    });

    it('will process dividends transaction', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(dividendTransaction.dateProcessedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
        expect(moment(dividendTransaction.dateProcessedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will not display offering for dividend', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].amountCents).toBe(1000);
        expect(response.items[0].offeringId).not.toBeDefined();
        expect(response.items[0].offeringTitle).toBe('N/A');
    });
});
