import 'reflect-metadata';
import * as moment from 'moment';
import {
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
    BazaRegistryNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { awaitTimeout, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcDividendFixtures } from '../baza-nc-dividend.fixtures';
import {
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionDto,
    BazaNcDividendTransactionEntryDto,
    BazaNcDividendTransactionStatus,
} from '@scaliolabs/baza-nc-shared';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcDwollaNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { BazaDwollaE2eNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/020-baza-nc-dividend-report.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessDwollaE2e = new BazaDwollaE2eNodeAccess(http);
    const dataAccessRegistry = new BazaRegistryNodeAccess(http);
    const dataAccessDividend = new BazaNcDividendNodeAccess(http);
    const dataAccessDividendTransaction = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccessDividendTransactionEntries = new BazaNcDividendTransactionEntryCmsNodeAccess(http);

    let DIVIDEND_TRANSACTIONS: Array<BazaNcDividendTransactionDto> = [];
    let DIVIDEND_TRANSACTION_ENTRIES: Array<BazaNcDividendTransactionEntryDto> = [];

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcDividendFixtures.BazaNcDividendDwollaTransactionFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(async () => {
        await http.authE2eAdmin();

        await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaNc.individualModeDividendNotifications',
            value: true,
        });

        await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaNc.clientReportDividendNotifications',
            value: true,
        });

        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = false;
    });

    it('will successfully touch Dwolla customer', async () => {
        await http.authE2eUser();

        const response = await dataAccessDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });

    it('will fetch dividend transactions', async () => {
        const response = await dataAccessDividendTransaction.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(2);
        expect(response.items[0].dateCreatedAt).toBeDefined();
        expect(response.items[1].dateCreatedAt).toBeDefined();

        DIVIDEND_TRANSACTIONS = response.items;
    });

    it('will successfully process dividend transaction', async () => {
        const sendNotificationResponse = await dataAccessDividendTransaction.sendProcessConfirmation({
            ulid: DIVIDEND_TRANSACTIONS[1].ulid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        await dataAccessE2e.flushMailbox();

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionDate = new Date();

        const confirmResponse = await dataAccessDividendTransaction.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransaction.getByUlid({ ulid: DIVIDEND_TRANSACTIONS[1].ulid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will display that all dividend entries are in Pending state', async () => {
        const response = await dataAccessDividendTransactionEntries.list({
            dividendTransactionUlid: DIVIDEND_TRANSACTIONS[1].ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);

        expect(response.items.every((next) => next.status === BazaNcDividendPaymentStatus.Pending)).toBeTruthy();

        expect(response.items[0].dateCreatedAt).toBeDefined();
        expect(response.items[1].dateCreatedAt).toBeDefined();
        expect(response.items[2].dateCreatedAt).toBeDefined();

        expect(response.items[0].dateUpdatedAt).toBeDefined();
        expect(response.items[1].dateUpdatedAt).toBeDefined();
        expect(response.items[2].dateUpdatedAt).toBeDefined();

        DIVIDEND_TRANSACTION_ENTRIES = response.items;
    });

    it('will send webhooks to process dividend transaction', async () => {
        for (const entry of DIVIDEND_TRANSACTION_ENTRIES) {
            expect(entry.dwollaTransferId).toBeDefined();

            const response = await dataAccessDwollaE2e.simulateWebhookEvent({
                _links: {},
                created: new Date().toISOString(),
                timestamp: new Date().toISOString(),
                resourceId: entry.dwollaTransferId,
                id: entry.dwollaTransferId,
                topic: DwollaEventTopic.transfer_completed,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            await awaitTimeout(1000);
        }
    });

    it('will display that all dividend entries are in Processed state', async () => {
        const response = await dataAccessDividendTransactionEntries.list({
            dividendTransactionUlid: DIVIDEND_TRANSACTIONS[1].ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);

        expect(response.items.every((next) => next.status === BazaNcDividendPaymentStatus.Processed)).toBeTruthy();

        expect(response.items[0].dateCreatedAt).toBeDefined();
        expect(response.items[1].dateCreatedAt).toBeDefined();
        expect(response.items[2].dateCreatedAt).toBeDefined();

        expect(response.items[0].dateUpdatedAt).toBeDefined();
        expect(response.items[1].dateUpdatedAt).toBeDefined();
        expect(response.items[2].dateUpdatedAt).toBeDefined();

        DIVIDEND_TRANSACTION_ENTRIES = response.items;
    });

    it('will display that Dividend Transaction is marked as Successful', async () => {
        const response = await dataAccessDividendTransaction.getByUlid({
            ulid: DIVIDEND_TRANSACTIONS[1].ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.status).toBe(BazaNcDividendTransactionStatus.Successful);
        expect(response.dateProcessedAt).toBeDefined();
        expect(response.dateUpdatedAt).toBeDefined();
    });

    it('will contain emails in mailbox', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();

        expect(mailbox.length).toBe(7);

        expect(mailbox[0].subject).toContain('Dividends Confirmed');

        expect(mailbox[1].subject).toContain('transfer');
        expect(mailbox[1].subject).toContain('10.00');
        expect(mailbox[2].subject).toContain('received');
        expect(mailbox[2].subject).toContain('10.00');

        expect(mailbox[3].subject).toContain('transfer');
        expect(mailbox[3].subject).toContain('12.50');
        expect(mailbox[4].subject).toContain('received');
        expect(mailbox[4].subject).toContain('12.50');

        expect(mailbox[5].subject).toContain('transfer');
        expect(mailbox[5].subject).toContain('14.97');
        expect(mailbox[6].subject).toContain('received');
        expect(mailbox[6].subject).toContain('14.97');
    });

    it('will contains all dividend entries in report email', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();

        const report = mailbox[0].messageBodyText;

        expect(report).toContain('You have successfully published dividends to your investors');
        expect(report).toContain('$10.00 N/A Processed');
        expect(report).toContain('$12.50 E2E NC Offering Fixture Processed –');
        expect(report).toContain('$14.97 E2E NC Offering Fixture Processed');
    });

    it('will display all dividends for user as processed', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items[0].amount).toBe('10.00');
        expect(response.items[1].amount).toBe('14.97');
        expect(response.items[2].amount).toBe('12.50');
    });

    it('will disable individual client notifications', async () => {
        await dataAccessE2e.flushMailbox();

        const response = await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaNc.individualModeDividendNotifications',
            value: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will process dividend transaction #2', async () => {
        const sendNotificationResponse = await dataAccessDividendTransaction.sendProcessConfirmation({
            ulid: DIVIDEND_TRANSACTIONS[0].ulid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        await dataAccessE2e.flushMailbox();

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const confirmResponse = await dataAccessDividendTransaction.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransaction.getByUlid({ ulid: DIVIDEND_TRANSACTIONS[0].ulid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
    });

    it('will send webhooks to process dividend transaction (2)', async () => {
        const response = await dataAccessDividendTransactionEntries.list({
            dividendTransactionUlid: DIVIDEND_TRANSACTIONS[0].ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        DIVIDEND_TRANSACTION_ENTRIES = response.items;

        for (const entry of DIVIDEND_TRANSACTION_ENTRIES) {
            expect(entry.dwollaTransferId).toBeDefined();

            const response = await dataAccessDwollaE2e.simulateWebhookEvent({
                _links: {},
                created: new Date().toISOString(),
                timestamp: new Date().toISOString(),
                resourceId: entry.dwollaTransferId,
                id: entry.dwollaTransferId,
                topic: DwollaEventTopic.transfer_failed,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
        }
    });

    it('will have only report email in mailbox', async () => {
        await asyncExpect(
            async () => {
                const mailbox = await dataAccessE2e.mailbox();

                expect(isBazaErrorResponse(mailbox)).toBeFalsy();
                expect(mailbox.length).toBe(1);

                expect(mailbox[0].messageBodyText).toContain('You have published dividends to your investors, but some of payments failed');
                expect(mailbox[0].messageBodyText).toContain('$12.00');
            },
            null,
            { intervalMillis: 1000 },
        );
    });
});
