import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcE2eNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus, BazaNcDwollaErrorCodes } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { BazaDwollaErrorCodes } from '@scaliolabs/baza-dwolla-shared';
import * as moment from 'moment/moment';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/019-baza-nc-dividend-populate-failure-reason.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccessDividendTransactionEntriesCms = new BazaNcDividendTransactionEntryCmsNodeAccess(http);

    let investorAccountId: number;
    let dividendTransactionUlid: string;
    let ulid: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await dataAccessE2eNc.disableDwollaAutoTouch();
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(async () => {
        await dataAccessE2eNc.enableDwollaAutoTouch();
    });

    it('will fetch investor account id', async () => {
        await http.authE2eUser();

        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.dwollaCustomerId).toBeNull();

        investorAccountId = response.id;
    });

    it('will successfully create dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        dividendTransactionUlid = response.ulid;
    });

    it('will successfully add dividend transaction entries', async () => {
        const createDividendTransactionEntryDate = new Date();

        const responseDTE1 = await dataAccessDividendTransactionEntriesCms.create({
            date: new Date().toISOString(),
            amountCents: 100,
            source: BazaNcDividendPaymentSource.Dwolla,
            dividendTransactionUlid,
            investorAccountId,
        });

        expect(isBazaErrorResponse(responseDTE1)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will display that no failure reasons set yet', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].failureReason).toBeNull();
    });

    it('will attempt to process dividend transaction (No Dwolla Customer case)', async () => {
        const sendProcessTokenResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendProcessTokenResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionDate = new Date();

        const response = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will check that investor is still dont have dwollaCustomerId', async () => {
        await http.authE2eUser();

        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.dwollaCustomerId).toBeNull();
    });

    it('will display that transaction is failed and failure reasons are set', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Failed);
        expect(response.items[0].failureReason).not.toBeNull();

        expect(response.items[0].failureReason.code);

        ulid = response.items[0].ulid;
    });

    it('will attempt to reprocess failed dividend', async () => {
        const response = await dataAccessDividendTransactionCms.reprocess({
            ulid: dividendTransactionUlid,
            entryULIDs: [ulid],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that transaction is failed and failure reasons are set', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Failed);
        expect(response.items[0].failureReason).not.toBeNull();
        expect(response.items[0].dateUpdatedAt).toBeDefined();
        expect(response.items[0].failureReason.code).toBe(BazaNcDwollaErrorCodes.BazaNcDwollaCustomerIsNotAvailable);

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();

        ulid = response.items[0].ulid;
    });

    it('will successfully touch Dwolla Customer', async () => {
        await http.authE2eUser();

        const response = await dataAccessDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });

    it('will reprocess failed dividend', async () => {
        const reProcessDividendTransactionDate = new Date();
        const response = await dataAccessDividendTransactionCms.reprocess({
            ulid: dividendTransactionUlid,
            entryULIDs: [ulid],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(reProcessDividendTransactionDate)).toBe(true);
    });

    it('will display that transaction is succeed', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[0].failureReason).toBeNull();
        expect(response.items[0].dateUpdatedAt).toBeDefined();

        ulid = response.items[0].ulid;
    });
});
