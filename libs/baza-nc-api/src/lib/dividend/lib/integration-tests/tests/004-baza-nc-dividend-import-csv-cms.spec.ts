import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BAZA_NC_DIVIDEND_CONSTANTS,
    BazaNcDividendErrorCodes,
    BazaNcDividendImportCsvEntryError,
    BazaNcDividendImportCsvEntryErrorDto,
    BazaNcDividendPaymentSource,
    NorthCapitalAccountId,
    OfferingId,
} from '@scaliolabs/baza-nc-shared';
import { bazaNcDividendCsvFileFactory } from '../util/baza-nc-dividend-csv-file-factory.util';
import * as moment from 'moment';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { DwollaCustomerId } from '@scaliolabs/baza-dwolla-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/004-baza-nc-dividend-import-csv-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessDividends = new BazaNcDividendNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessDwolla = new BazaNcDwollaNodeAccess(http);

    let dividendTransactionUlid: string;
    let ncOfferingId: OfferingId;
    let accountId: number;
    let ncAccountId: NorthCapitalAccountId;
    let dwollaCustomerId: DwollaCustomerId;

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = false;
    });

    const expectDate = (
        date: string,
        equalTo: {
            day: number;
            month: number;
            year: number;
        },
    ) => {
        const parsed = new Date(date);

        expect(parsed.getDate()).toBe(equalTo.day);
        expect(parsed.getMonth() + 1).toBe(equalTo.month);
        expect(parsed.getFullYear()).toBe(equalTo.year);
    };

    const newDividendTransaction = async () => {
        const dividendTransactionResponse = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(dividendTransactionResponse));

        dividendTransactionUlid = dividendTransactionResponse.ulid;

        return dividendTransactionResponse;
    };

    it('will correctly detects date with moment.js', () => {
        const testCases: Array<{
            input: string;
            expected: string;
            day: number;
            month: number;
            year: number;
        }> = [
            {
                input: '4/15/2022',
                expected: '04/15/2022',
                day: 15,
                month: 4,
                year: 2022,
            },
            {
                input: '4/12/2022',
                expected: '04/12/2022',
                day: 12,
                month: 4,
                year: 2022,
            },
            {
                input: '3/10/2022',
                expected: '03/10/2022',
                day: 10,
                month: 3,
                year: 2022,
            },
            {
                input: '2/1/2022',
                expected: '02/01/2022',
                day: 1,
                month: 2,
                year: 2022,
            },
            {
                input: '1/1/2022',
                expected: '01/01/2022',
                day: 1,
                month: 1,
                year: 2022,
            },
        ];

        for (const testCase of testCases) {
            const date = moment(testCase.input);

            expect(date.format('MM/DD/YYYY')).toBe(testCase.expected);

            expect(date.toDate().getDate()).toBe(testCase.day);
            expect(date.toDate().getMonth() + 1).toBe(testCase.month);
            expect(date.toDate().getFullYear()).toBe(testCase.year);
        }
    });

    it('will fetch all required resources', async () => {
        const offering = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(offering)).toBeFalsy();
        expect(offering.length).toBe(1);

        await http.authE2eUser();
        await dataAccessDwolla.touch();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        accountId = investorAccount.userId;
        ncAccountId = investorAccount.northCapitalAccountId;
        dwollaCustomerId = investorAccount.dwollaCustomerId;
        ncOfferingId = offering[0].ncOfferingId;
    });

    it('will not apply dividends with empty csv file', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([]);

        const response: BazaError = (await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendImportCsvNoEntries);
    });

    it('will not apply dividends with csv file without entries', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([['North Capital ID Number', 'Date', 'North Capital Offering ID']]);

        const response: BazaError = (await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendImportCsvNoEntries);
    });

    it('will not apply dividends with csv file without header', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            [ncAccountId, '4/15/2022', ncOfferingId],
            [ncAccountId, '4/12/2022', ncOfferingId],
            [ncAccountId, '3/10/2022', ncOfferingId],
        ]);

        const response: BazaError = (await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendImportCsvUnknownColumn);
    });

    it('will not apply dividends from csv file with missing column (Amount)', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Date', 'North Capital Offering ID'],
            [ncAccountId, '4/15/2022', ncOfferingId],
            [ncAccountId, '4/12/2022', ncOfferingId],
            [ncAccountId, '3/10/2022', ncOfferingId],
        ]);

        const response: BazaError = (await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendImportCsvFailedToParse);
    });

    it('will not apply dividends from csv file with missing column (Date)', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Amount', 'North Capital Offering ID'],
            [ncAccountId, '$50.00', ncOfferingId],
            [ncAccountId, '$52.00', ncOfferingId],
            [ncAccountId, '$12.00', ncOfferingId],
        ]);

        const response: BazaError = (await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendImportCsvFailedToParse);
    });

    it('will not apply dividends with invalid amount', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [ncAccountId, '$50.00', '4/15/2022', ncOfferingId],
            [ncAccountId, '-$52.00', '4/12/2022', ncOfferingId],
            [ncAccountId, '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeFalsy();
        expect(response.errors).toEqual([
            {
                row: 3,
                reason: BazaNcDividendImportCsvEntryError.AmountIsLessThanZero,
            },
        ] as Array<BazaNcDividendImportCsvEntryErrorDto>);

        const entriesResponse = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entriesResponse)).toBeFalsy();

        expect(entriesResponse.items.length).toBe(0);
    });

    it('will not apply dividends with invalid date', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [ncAccountId, '$50.00', '4/15/2022', ncOfferingId],
            [ncAccountId, '$52.00', '4/99/2022', ncOfferingId],
            [ncAccountId, '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeFalsy();
        expect(response.errors).toEqual([
            {
                row: 3,
                reason: BazaNcDividendImportCsvEntryError.FailedToParseDate,
            },
        ] as Array<BazaNcDividendImportCsvEntryErrorDto>);

        const entriesResponse = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entriesResponse)).toBeFalsy();

        expect(entriesResponse.items.length).toBe(0);
    });

    it('will not apply dividends with date in future', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [ncAccountId, '$50.00', '4/15/2022', ncOfferingId],
            [ncAccountId, '$52.00', '4/12/2099', ncOfferingId],
            [ncAccountId, '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeFalsy();
        expect(response.errors).toEqual([
            {
                row: 3,
                reason: BazaNcDividendImportCsvEntryError.DateIsInFuture,
            },
        ] as Array<BazaNcDividendImportCsvEntryErrorDto>);

        const entriesResponse = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entriesResponse)).toBeFalsy();

        expect(entriesResponse.items.length).toBe(0);
    });

    it('will not apply dividends with unknown NC Account Id', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [ncAccountId, '$50.00', '4/15/2022', ncOfferingId],
            ['A000000', '$52.00', '4/12/2022', ncOfferingId],
            [ncAccountId, '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeFalsy();
        expect(response.errors).toEqual([
            {
                row: 3,
                reason: BazaNcDividendImportCsvEntryError.InvestorAccountNotFound,
            },
        ] as Array<BazaNcDividendImportCsvEntryErrorDto>);

        const entriesResponse = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entriesResponse)).toBeFalsy();

        expect(entriesResponse.items.length).toBe(0);
    });

    it('will not apply dividends with unknown Baza Account Id', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['Baza Account ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [accountId.toString(), '$50.00', '4/15/2022', ncOfferingId],
            ['9999999', '$52.00', '4/12/2022', ncOfferingId],
            [accountId.toString(), '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeFalsy();
        expect(response.errors).toEqual([
            {
                row: 3,
                reason: BazaNcDividendImportCsvEntryError.InvestorAccountNotFound,
            },
        ] as Array<BazaNcDividendImportCsvEntryErrorDto>);

        const entriesResponse = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entriesResponse)).toBeFalsy();

        expect(entriesResponse.items.length).toBe(0);
    });

    it('will not apply dividends with unknown offering', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [ncAccountId, '$50.00', '4/15/2022', ncOfferingId],
            [ncAccountId, '$52.00', '4/12/2022', '999999'],
            [ncAccountId, '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeFalsy();
        expect(response.errors).toEqual([
            {
                row: 3,
                reason: BazaNcDividendImportCsvEntryError.OfferingNotFound,
            },
        ] as Array<BazaNcDividendImportCsvEntryErrorDto>);

        const entriesResponse = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entriesResponse)).toBeFalsy();

        expect(entriesResponse.items.length).toBe(0);
    });

    it('will not apply dividends for invalid csv file and it will returns multiple errors in response', async () => {
        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['Baza Account ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [accountId.toString(), '$50.00', '4/15/2099', ncOfferingId],
            [accountId.toString(), '-$52.00', '4/12/2022', ncOfferingId],
            [accountId.toString(), '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeFalsy();
        expect(response.errors).toEqual([
            {
                row: 2,
                reason: BazaNcDividendImportCsvEntryError.DateIsInFuture,
            },
            {
                row: 3,
                reason: BazaNcDividendImportCsvEntryError.AmountIsLessThanZero,
            },
        ] as Array<BazaNcDividendImportCsvEntryErrorDto>);

        const entriesResponse = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entriesResponse)).toBeFalsy();

        expect(entriesResponse.items.length).toBe(0);
    });

    it('will successfully apply dividends from csv file (case 1: NC Account Id)', async () => {
        const createDividendTransactionEntryDate = new Date();

        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [ncAccountId, '$50.00', '4/15/2022', ncOfferingId],
            [ncAccountId, '$52.00', '4/12/2022', ncOfferingId],
            [ncAccountId, '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeTruthy();
        expect(response.errors).not.toBeDefined();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will not allow to apply too much dividends from csv file', async () => {
        await newDividendTransaction();

        const csvSource: Array<Array<string>> = [['Baza Account ID Number', 'Amount', 'Date', 'North Capital Offering ID']];

        for (let i = 0; i < BAZA_NC_DIVIDEND_CONSTANTS.maxCsvFileLines + 1; i++) {
            csvSource.push([accountId.toString(), '$50.00', '4/15/2022', ncOfferingId]);
        }

        const response: BazaError = (await dataAccess.importCsv(bazaNcDividendCsvFileFactory(csvSource), {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendImportCsvTooManyRecords);
    });

    it('will successfully apply dividends from csv file (case 2: Baza Account Id)', async () => {
        const createDividendTransactionEntryDate = new Date();

        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['Baza Account ID Number', 'Amount', 'Date', 'North Capital Offering ID'],
            [accountId.toString(), '$50.00', '4/15/2022', ncOfferingId],
            [accountId.toString(), '$52.00', '4/12/2022', ncOfferingId],
            [accountId.toString(), '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeTruthy();
        expect(response.errors).not.toBeDefined();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will display new entries in dividends transaction', async () => {
        const response = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);

        expect(response.items[0].investorAccount.northCapitalAccountId).toBe(ncAccountId);
        expect(response.items[0].offeringId).toBe(ncOfferingId);
        expect(response.items[0].amount).toBe('12.00');

        expect(response.items[1].investorAccount.northCapitalAccountId).toBe(ncAccountId);
        expect(response.items[1].offeringId).toBe(ncOfferingId);
        expect(response.items[1].amount).toBe('52.00');

        expect(response.items[2].investorAccount.northCapitalAccountId).toBe(ncAccountId);
        expect(response.items[2].offeringId).toBe(ncOfferingId);
        expect(response.items[2].amount).toBe('50.00');

        expectDate(response.items[0].date, { day: 10, month: 3, year: 2022 });
        expectDate(response.items[1].date, { day: 12, month: 4, year: 2022 });
        expectDate(response.items[2].date, { day: 15, month: 4, year: 2022 });
    });

    it('will process dividends transaction', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(dividendTransaction.dateProcessedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
        expect(moment(dividendTransaction.dateProcessedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will display dividends for user', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividends.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(3);

        expect(response.items[0].amount).toBe('50.00');
        expect(response.items[1].amount).toBe('52.00');
        expect(response.items[2].amount).toBe('12.00');
    });

    it('will apply dividends from csv file (case 3: Dwolla Customer Id)', async () => {
        const createDividendTransactionEntryDate = new Date();

        await newDividendTransaction();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['Dwolla ID', 'Amount', 'Date', 'North Capital Offering ID'],
            [dwollaCustomerId, '$50.00', '4/15/2022', ncOfferingId],
            [dwollaCustomerId, '$52.00', '4/12/2022', ncOfferingId],
            [dwollaCustomerId, '$12.00', '3/10/2022', ncOfferingId],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.success).toBeTruthy();
        expect(response.errors).not.toBeDefined();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });
});
