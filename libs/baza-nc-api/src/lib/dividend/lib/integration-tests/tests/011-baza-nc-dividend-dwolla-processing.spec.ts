import 'reflect-metadata';
import * as moment from 'moment';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    BazaNcDividendPaymentSource,
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionStatus,
    NorthCapitalAccountId,
    OfferingId,
} from '@scaliolabs/baza-nc-shared';
import { DwollaCustomerId, DwollaEventTopic, DwollaTransferId } from '@scaliolabs/baza-dwolla-shared';
import { bazaNcDividendCsvFileFactory } from '../util/baza-nc-dividend-csv-file-factory.util';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { BazaDwollaWebhookNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/011-baza-nc-dividend-dwolla-processing.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccessDividendTransactionEntriesCms = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessDividends = new BazaNcDividendNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessDwollaWebhooks = new BazaDwollaWebhookNodeAccess(http);

    let dividendTransactionUlid: string;
    let ncOfferingId: OfferingId;
    let accountId: number;
    let ncAccountId: NorthCapitalAccountId;
    let dwollaCustomerId: DwollaCustomerId;
    let dwollaTransferId: DwollaTransferId;
    let dwollaTransferId2: DwollaTransferId;

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = false;
    });

    it('will fetch all required resources', async () => {
        const offering = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(offering)).toBeFalsy();
        expect(offering.length).toBe(1);

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        accountId = investorAccount.userId;
        ncAccountId = investorAccount.northCapitalAccountId;
        dwollaCustomerId = investorAccount.dwollaCustomerId;
        ncOfferingId = offering[0].ncOfferingId;
    });

    it('will create a dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        dividendTransactionUlid = response.ulid;
    });

    it('will import dividends from CSV', async () => {
        const createDividendTransactionEntryDate = new Date();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Date', 'Amount', 'North Capital Offering ID'],
            [ncAccountId, '4/15/2022', '0.02', ncOfferingId],
            [ncAccountId, '4/12/2022', '0.01', ncOfferingId],
        ]);

        const response = await dataAccessDividendTransactionEntriesCms.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.Dwolla,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will display that dividends created for Dwolla source', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].source).toBe(BazaNcDividendPaymentSource.Dwolla);
        expect(response.items[1].source).toBe(BazaNcDividendPaymentSource.Dwolla);

        expect(response.items[0].dwollaTransferId).toBeNull();
        expect(response.items[1].dwollaTransferId).toBeNull();
    });

    it('will process dividends', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const updateDividendTransactionEntryDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(updateDividendTransactionEntryDate)).toBe(true);
    });

    it('will set dwollaTransferId for entries', async () => {
        const response = await asyncExpect(
            async () => {
                const response = await dataAccessDividendTransactionEntriesCms.list({
                    dividendTransactionUlid,
                });

                expect(response.items.length).toBe(2);

                expect(response.items[0].source).toBe(BazaNcDividendPaymentSource.Dwolla);
                expect(response.items[1].source).toBe(BazaNcDividendPaymentSource.Dwolla);

                expect(response.items[0].dwollaTransferId).toBeDefined();
                expect(response.items[1].dwollaTransferId).toBeDefined();

                expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Pending);
                expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Pending);

                return response;
            },
            null,
            { intervalMillis: 3000 },
        );

        dwollaTransferId = response.items[0].dwollaTransferId;
        dwollaTransferId2 = response.items[1].dwollaTransferId;
    });

    it('will create dividends for user', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividends.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].source).toBe(BazaNcDividendPaymentSource.Dwolla);
        expect(response.items[1].source).toBe(BazaNcDividendPaymentSource.Dwolla);

        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Pending);
    });

    it('will send Dwolla webhook with transfer_completed topic', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_completed,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: dwollaTransferId,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will correctly updates dividends and dividend entries (1)', async () => {
        await asyncExpect(
            async () => {
                const responseDividendTransaction = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(responseDividendTransaction)).toBeFalsy();

                expect(responseDividendTransaction.status).toBe(BazaNcDividendTransactionStatus.InProgress);
                expect(responseDividendTransaction.dateUpdatedAt).toBeDefined();

                const responseDividendEntries = await dataAccessDividendTransactionEntriesCms.list({
                    dividendTransactionUlid,
                });

                expect(responseDividendEntries.items.length).toBe(2);

                expect(responseDividendEntries.items[0].dwollaTransferId).toBeDefined();
                expect(responseDividendEntries.items[1].dwollaTransferId).toBeDefined();

                expect(responseDividendEntries.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(responseDividendEntries.items[1].status).toBe(BazaNcDividendPaymentStatus.Pending);

                expect(responseDividendEntries.items[0].dateProcessedAt).toBeDefined();

                expect(responseDividendEntries.items[0].dateUpdatedAt).toBeDefined();
                expect(responseDividendEntries.items[1].dateUpdatedAt).toBeDefined();

                await http.authE2eUser();

                const responseDividends = await dataAccessDividends.list({});

                expect(isBazaErrorResponse(responseDividends)).toBeFalsy();

                expect(responseDividends.items.length).toBe(2);

                expect(responseDividends.items[1].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(responseDividends.items[0].status).toBe(BazaNcDividendPaymentStatus.Pending);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will send Dwolla webhook with transfer_failed topic', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_failed,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: dwollaTransferId,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will correctly updates dividends and dividend entries (2)', async () => {
        await asyncExpect(
            async () => {
                const responseDividendTransaction = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(responseDividendTransaction)).toBeFalsy();

                expect(responseDividendTransaction.status).toBe(BazaNcDividendTransactionStatus.InProgress);
                expect(responseDividendTransaction.dateUpdatedAt).toBeDefined();

                const responseDividendEntries = await dataAccessDividendTransactionEntriesCms.list({
                    dividendTransactionUlid,
                });

                expect(responseDividendEntries.items.length).toBe(2);

                expect(responseDividendEntries.items[0].dwollaTransferId).toBeDefined();
                expect(responseDividendEntries.items[1].dwollaTransferId).toBeDefined();

                expect(responseDividendEntries.items[0].status).toBe(BazaNcDividendPaymentStatus.Failed);
                expect(responseDividendEntries.items[1].status).toBe(BazaNcDividendPaymentStatus.Pending);

                expect(responseDividendEntries.items[0].dateUpdatedAt).toBeDefined();
                expect(responseDividendEntries.items[1].dateUpdatedAt).toBeDefined();

                await http.authE2eUser();

                const responseDividends = await dataAccessDividends.list({});

                expect(isBazaErrorResponse(responseDividends)).toBeFalsy();

                expect(responseDividends.items.length).toBe(2);

                expect(responseDividends.items[1].status).toBe(BazaNcDividendPaymentStatus.Failed);
                expect(responseDividends.items[0].status).toBe(BazaNcDividendPaymentStatus.Pending);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will send Dwolla webhook with transfer_cancelled topic', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_cancelled,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: dwollaTransferId,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will correctly updates dividends and dividend entries (3)', async () => {
        await asyncExpect(
            async () => {
                const responseDividendTransaction = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(responseDividendTransaction)).toBeFalsy();

                expect(responseDividendTransaction.status).toBe(BazaNcDividendTransactionStatus.InProgress);
                expect(responseDividendTransaction.dateUpdatedAt).toBeDefined();

                const responseDividendEntries = await dataAccessDividendTransactionEntriesCms.list({
                    dividendTransactionUlid,
                });

                expect(responseDividendEntries.items.length).toBe(2);

                expect(responseDividendEntries.items[0].dwollaTransferId).toBeDefined();
                expect(responseDividendEntries.items[1].dwollaTransferId).toBeDefined();

                expect(responseDividendEntries.items[0].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
                expect(responseDividendEntries.items[1].status).toBe(BazaNcDividendPaymentStatus.Pending);

                expect(responseDividendEntries.items[0].dateUpdatedAt).toBeDefined();
                expect(responseDividendEntries.items[1].dateUpdatedAt).toBeDefined();

                await http.authE2eUser();

                const responseDividends = await dataAccessDividends.list({});

                expect(isBazaErrorResponse(responseDividends)).toBeFalsy();

                expect(responseDividends.items.length).toBe(2);

                expect(responseDividends.items[1].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
                expect(responseDividends.items[0].status).toBe(BazaNcDividendPaymentStatus.Pending);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will send Dwolla webhook with transfer_completed topic for Dividend 2', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_completed,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: dwollaTransferId2,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will correctly updates dividends and dividend entries and closes dividend transaction', async () => {
        await asyncExpect(
            async () => {
                const responseDividendTransaction = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(responseDividendTransaction)).toBeFalsy();

                expect(responseDividendTransaction.status).toBe(BazaNcDividendTransactionStatus.Cancelled);
                expect(responseDividendTransaction.dateUpdatedAt).toBeDefined();

                const responseDividendEntries = await dataAccessDividendTransactionEntriesCms.list({
                    dividendTransactionUlid,
                });

                expect(responseDividendEntries.items.length).toBe(2);

                expect(responseDividendEntries.items[0].dwollaTransferId).toBeDefined();
                expect(responseDividendEntries.items[1].dwollaTransferId).toBeDefined();

                expect(responseDividendEntries.items[0].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
                expect(responseDividendEntries.items[1].status).toBe(BazaNcDividendPaymentStatus.Processed);

                expect(responseDividendEntries.items[1].dateProcessedAt).toBeDefined();

                expect(responseDividendEntries.items[0].dateUpdatedAt).toBeDefined();
                expect(responseDividendEntries.items[1].dateUpdatedAt).toBeDefined();

                await http.authE2eUser();

                const responseDividends = await dataAccessDividends.list({});

                expect(isBazaErrorResponse(responseDividends)).toBeFalsy();

                expect(responseDividends.items.length).toBe(2);

                expect(responseDividends.items[1].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
                expect(responseDividends.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will send Dwolla webhook with transfer_completed topic for Dividend 1', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_completed,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: dwollaTransferId,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will correctly updates dividends and dividend entries and mark dividend transaction as Processed', async () => {
        await asyncExpect(
            async () => {
                const responseDividendTransaction = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(responseDividendTransaction)).toBeFalsy();

                expect(responseDividendTransaction.status).toBe(BazaNcDividendTransactionStatus.Successful);
                expect(responseDividendTransaction.dateUpdatedAt).toBeDefined();
                expect(responseDividendTransaction.dateProcessedAt).toBeDefined();

                const responseDividendEntries = await dataAccessDividendTransactionEntriesCms.list({
                    dividendTransactionUlid,
                });

                expect(responseDividendEntries.items.length).toBe(2);

                expect(responseDividendEntries.items[0].dwollaTransferId).toBeDefined();
                expect(responseDividendEntries.items[1].dwollaTransferId).toBeDefined();

                expect(responseDividendEntries.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(responseDividendEntries.items[1].status).toBe(BazaNcDividendPaymentStatus.Processed);

                expect(responseDividendEntries.items[0].dateUpdatedAt).toBeDefined();
                expect(responseDividendEntries.items[1].dateUpdatedAt).toBeDefined();

                expect(responseDividendEntries.items[0].dateProcessedAt).toBeDefined();
                expect(responseDividendEntries.items[1].dateProcessedAt).toBeDefined();

                await http.authE2eUser();

                const responseDividends = await dataAccessDividends.list({});

                expect(isBazaErrorResponse(responseDividends)).toBeFalsy();

                expect(responseDividends.items.length).toBe(2);

                expect(responseDividends.items[1].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(responseDividends.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
