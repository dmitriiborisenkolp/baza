import 'reflect-metadata';
import * as moment from 'moment';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    BazaNcDividendErrorCodes,
    BazaNcDividendPaymentSource,
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionEntryDto,
    BazaNcDividendTransactionStatus,
    convertFromCents,
    NorthCapitalAccountId,
    OfferingId,
} from '@scaliolabs/baza-nc-shared';
import { DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { bazaNcDividendCsvFileFactory } from '../util/baza-nc-dividend-csv-file-factory.util';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { BazaDwollaWebhookNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import {
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_CANCEL_TRANSACTION,
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_FAIL_TRANSACTION,
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_PROCESS_TRANSACTION,
} from '../constants/baza-nc-dividend-e2e.constants';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/014-baza-nc-dividend-dwolla-reprocess-entries.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessDwollaWebhooks = new BazaDwollaWebhookNodeAccess(http);

    let dividendTransactionUlid: string;
    let ncOfferingId: OfferingId;
    let ncAccountId: NorthCapitalAccountId;

    const entries: Partial<{
        pending: BazaNcDividendTransactionEntryDto;
        cancelled: BazaNcDividendTransactionEntryDto;
        failed: BazaNcDividendTransactionEntryDto;
        processed: BazaNcDividendTransactionEntryDto;
    }> = {};

    const updateEntries = async () => {
        const response = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);

        entries.processed = response.items[0];
        entries.cancelled = response.items[1];
        entries.failed = response.items[2];
        entries.pending = response.items[3];
    };

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = false;
    });

    it('will fetch all required resources', async () => {
        const offering = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(offering)).toBeFalsy();
        expect(offering.length).toBe(1);

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        ncAccountId = investorAccount.northCapitalAccountId;
        ncOfferingId = offering[0].ncOfferingId;
    });

    it('will create a dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        dividendTransactionUlid = response.ulid;
    });

    it('will import dividends from CSV', async () => {
        const createDividendTransactionEntryDate = new Date();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Date', 'Amount', 'North Capital Offering ID'],
            [ncAccountId, '4/15/2022', '0.02', ncOfferingId],
            [
                ncAccountId,
                '4/12/2022',
                convertFromCents(BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_FAIL_TRANSACTION).toFixed(2),
                ncOfferingId,
            ],
            [
                ncAccountId,
                '4/12/2022',
                convertFromCents(BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_CANCEL_TRANSACTION).toFixed(2),
                ncOfferingId,
            ],
            [
                ncAccountId,
                '4/12/2022',
                convertFromCents(BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_PROCESS_TRANSACTION).toFixed(2),
                ncOfferingId,
            ],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.Dwolla,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will display all dividends as pending', async () => {
        const response = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);
    });

    it('will process dividends', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        await dataAccessE2e.flushMailbox();

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will display that dividend transaction is still pending', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.status).toBe(BazaNcDividendTransactionStatus.InProgress);
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will display all dividends with correct statuses', async () => {
        const response = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
        expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Failed);
        expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

        expect(response.items[0].canBeReprocessed).toBeFalsy();
        expect(response.items[1].canBeReprocessed).toBeTruthy();
        expect(response.items[2].canBeReprocessed).toBeTruthy();
        expect(response.items[3].canBeReprocessed).toBeFalsy();

        expect(response.items[0].dateProcessedAt).toBeDefined();

        expect(response.items[0].dateUpdatedAt).toBeDefined();
        expect(response.items[1].dateUpdatedAt).toBeDefined();
        expect(response.items[2].dateUpdatedAt).toBeDefined();
        expect(response.items[3].dateUpdatedAt).toBeDefined();

        entries.processed = response.items[0];
        entries.cancelled = response.items[1];
        entries.failed = response.items[2];
        entries.pending = response.items[3];

        const transaction = await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(transaction)).toBeFalsy();

        expect(transaction.status).toBe(BazaNcDividendTransactionStatus.InProgress);
    });

    it('will not allow to reprocess processed entity', async () => {
        const response: BazaError = (await dataAccessDividendTransactionCms.reprocess({
            ulid: dividendTransactionUlid,
            entryULIDs: [entries.processed.ulid],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeProcessed);

        const transaction = await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(transaction)).toBeFalsy();

        expect(transaction.status).toBe(BazaNcDividendTransactionStatus.InProgress);
    });

    it('will not allow to reprocess pending entity', async () => {
        const response: BazaError = (await dataAccessDividendTransactionCms.reprocess({
            ulid: dividendTransactionUlid,
            entryULIDs: [entries.pending.ulid],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeProcessed);

        const transaction = await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(transaction)).toBeFalsy();

        expect(transaction.status).toBe(BazaNcDividendTransactionStatus.InProgress);
    });

    it('will allow to reprocess cancelled entity', async () => {
        const response = await dataAccessDividendTransactionCms.reprocess({
            ulid: dividendTransactionUlid,
            entryULIDs: [entries.cancelled.ulid],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.newStatus).toBe(BazaNcDividendTransactionStatus.InProgress);

        const transaction = await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(transaction)).toBeFalsy();

        expect(transaction.status).toBe(BazaNcDividendTransactionStatus.InProgress);

        await updateEntries();
    });

    it('will send Dwolla webhook with transfer_completed for cancelled dividend', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_completed,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: entries.cancelled.dwollaTransferId,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will show updates in dividend transaction after reprocessing cancelled dividend', async () => {
        const response = await asyncExpect(
            async () => {
                const response = await dataAccess.list({
                    dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(4);
                expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Failed);
                expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

                expect(response.items[0].canBeReprocessed).toBeFalsy();
                expect(response.items[1].canBeReprocessed).toBeFalsy();
                expect(response.items[2].canBeReprocessed).toBeTruthy();
                expect(response.items[3].canBeReprocessed).toBeFalsy();

                expect(response.items[0].dateProcessedAt).toBeDefined();
                expect(response.items[1].dateProcessedAt).toBeDefined();

                expect(response.items[0].dateUpdatedAt).toBeDefined();
                expect(response.items[1].dateUpdatedAt).toBeDefined();
                expect(response.items[2].dateUpdatedAt).toBeDefined();
                expect(response.items[3].dateUpdatedAt).toBeDefined();

                return response;
            },
            null,
            { intervalMillis: 1000 },
        );

        entries.processed = response.items[0];
        entries.cancelled = response.items[1];
        entries.failed = response.items[2];
        entries.pending = response.items[3];

        await asyncExpect(
            async () => {
                const transaction = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(transaction)).toBeFalsy();

                expect(transaction.status).toBe(BazaNcDividendTransactionStatus.InProgress);
            },
            null,
            { intervalMillis: 1000 },
        );

        await updateEntries();
    });

    it('will send Dwolla webhook with transfer_completed for pending dividend', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_completed,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: entries.pending.dwollaTransferId,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will show updates in dividend transaction after processing pending dividend', async () => {
        const response = await asyncExpect(
            async () => {
                const response = await dataAccess.list({
                    dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(4);
                expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Failed);
                expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Processed);

                expect(response.items[0].canBeReprocessed).toBeFalsy();
                expect(response.items[1].canBeReprocessed).toBeFalsy();
                expect(response.items[2].canBeReprocessed).toBeTruthy();
                expect(response.items[3].canBeReprocessed).toBeFalsy();

                expect(response.items[0].dateProcessedAt).toBeDefined();
                expect(response.items[1].dateProcessedAt).toBeDefined();
                expect(response.items[3].dateProcessedAt).toBeDefined();

                expect(response.items[0].dateUpdatedAt).toBeDefined();
                expect(response.items[1].dateUpdatedAt).toBeDefined();
                expect(response.items[2].dateUpdatedAt).toBeDefined();
                expect(response.items[3].dateUpdatedAt).toBeDefined();

                return response;
            },
            null,
            { intervalMillis: 1000 },
        );

        entries.processed = response.items[0];
        entries.cancelled = response.items[1];
        entries.failed = response.items[2];
        entries.pending = response.items[3];

        await asyncExpect(
            async () => {
                const transaction = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(transaction)).toBeFalsy();

                expect(transaction.status).toBe(BazaNcDividendTransactionStatus.Failed);
            },
            null,
            { intervalMillis: 1000 },
        );

        await updateEntries();
    });

    it('will send Dwolla webhook with transfer_completed for failed dividend', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_completed,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: entries.failed.dwollaTransferId,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will show updates in dividend transaction after reprocessing failed dividend', async () => {
        const response = await asyncExpect(
            async () => {
                const response = await dataAccess.list({
                    dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(4);
                expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Processed);

                expect(response.items[0].canBeReprocessed).toBeFalsy();
                expect(response.items[1].canBeReprocessed).toBeFalsy();
                expect(response.items[2].canBeReprocessed).toBeFalsy();
                expect(response.items[3].canBeReprocessed).toBeFalsy();

                expect(response.items[0].dateProcessedAt).toBeDefined();
                expect(response.items[1].dateProcessedAt).toBeDefined();
                expect(response.items[2].dateProcessedAt).toBeDefined();
                expect(response.items[3].dateProcessedAt).toBeDefined();

                expect(response.items[0].dateUpdatedAt).toBeDefined();
                expect(response.items[1].dateUpdatedAt).toBeDefined();
                expect(response.items[2].dateUpdatedAt).toBeDefined();
                expect(response.items[3].dateUpdatedAt).toBeDefined();

                return response;
            },
            null,
            { intervalMillis: 1000 },
        );

        entries.processed = response.items[0];
        entries.cancelled = response.items[1];
        entries.failed = response.items[2];
        entries.pending = response.items[3];

        await asyncExpect(
            async () => {
                const transaction = await dataAccessDividendTransactionCms.getByUlid({
                    ulid: dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(transaction)).toBeFalsy();

                expect(transaction.status).toBe(BazaNcDividendTransactionStatus.Successful);
            },
            null,
            { intervalMillis: 1000 },
        );
    });
});
