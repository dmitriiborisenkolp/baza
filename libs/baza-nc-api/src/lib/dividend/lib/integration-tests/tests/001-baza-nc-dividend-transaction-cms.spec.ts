import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcDividendTransactionCmsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendErrorCodes } from '@scaliolabs/baza-nc-shared';

describe('@scaliolabs/baza-nc-api/dividend/001-baza-nc-dividend-transaction-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);

    let ulid: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will have empty list of dividend transactions before tests', async () => {
        const response = await dataAccessDividendTransactionCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will create a new dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('DT 1');
        expect(response.canBeDeleted).toBeTruthy();

        ulid = response.ulid;
    });

    it('will returns dividend transaction by ulid', async () => {
        const response = await dataAccessDividendTransactionCms.getByUlid({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('DT 1');
    });

    it('will displays dividend transaction in list', async () => {
        const response = await dataAccessDividendTransactionCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);

        expect(response.items[0].ulid).toBe(ulid);
        expect(response.items[0].title).toBe('DT 1');
    });

    it('will create one more dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 2',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('DT 2');
    });

    it('will displays more dividend transactions in list', async () => {
        const response = await dataAccessDividendTransactionCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].ulid).not.toBe(ulid);
        expect(response.items[0].title).toBe('DT 2');
        expect(response.items[1].ulid).toBe(ulid);
        expect(response.items[1].title).toBe('DT 1');
    });

    it('will not allow to send process request for dividend transaction with no entries', async () => {
        const response: BazaError = (await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionIsEmpty);
    });

    it('will allow to delete dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.delete({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display anymore deleted dividend transaction in list', async () => {
        const response = await dataAccessDividendTransactionCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);

        expect(response.items[0].ulid).not.toBe(ulid);
        expect(response.items[0].title).toBe('DT 2');
    });
});
