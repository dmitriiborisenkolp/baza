import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendCmsNodeAccess,
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { BazaNcDividendPaymentSource, NorthCapitalAccountId } from '@scaliolabs/baza-nc-shared';
import * as moment from 'moment';
import { bazaNcDividendCsvFileFactory } from '../util/baza-nc-dividend-csv-file-factory.util';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/005-baza-nc-dividend-without-offering.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccessDividendTransactionEntriesCms = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessDividends = new BazaNcDividendNodeAccess(http);
    const dataAccessDividendsCms = new BazaNcDividendCmsNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);

    let dividendTransactionUlid: string;
    let investorAccountId: number;
    let ncAccountId: NorthCapitalAccountId;

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will fetch all required resources', async () => {
        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        investorAccountId = investorAccount.id;
        ncAccountId = investorAccount.northCapitalAccountId;
    });

    it('will create a new dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        dividendTransactionUlid = response.ulid;
    });

    it('will add a new dividend transaction entry w/o offering', async () => {
        const createDividendTransactionEntryDate = new Date();

        const response = await dataAccessDividendTransactionEntriesCms.create({
            dividendTransactionUlid,
            date: new Date().toISOString(),
            investorAccountId,
            amountCents: 1000,
            source: BazaNcDividendPaymentSource.NC,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will import dividend transaction entries w/o offering', async () => {
        const createDividendTransactionEntryDate = new Date();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Amount', 'Date'],
            [ncAccountId, '$50.00', '4/15/2022'],
            [ncAccountId, '$52.00', '4/12/2022'],
            [ncAccountId, '$12.00', '3/10/2022'],
        ]);

        const response = await dataAccessDividendTransactionEntriesCms.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.NC,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will display entries in list', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(4);

        for (const item of response.items) {
            expect(item.offeringId).not.toBeDefined();
            expect(item.offeringTitle).toBe('N/A');
        }
    });

    it('will apply dividends', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(dividendTransaction.dateProcessedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
        expect(moment(dividendTransaction.dateProcessedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will display dividends in CMS correctly', async () => {
        const response = await dataAccessDividendsCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(4);

        for (const item of response.items) {
            expect(item.offeringId).not.toBeDefined();
            expect(item.offeringTitle).toBe('N/A');
        }
    });

    it('will display dividends in portfolio correctly', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividends.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(4);

        for (const item of response.items) {
            expect(item.offeringId).not.toBeDefined();
            expect(item.offeringTitle).toBe('N/A');
        }
    });
});
