import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcDividendTransactionCmsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendFixtures } from '../baza-nc-dividend.fixtures';
import { BazaNcDividendTransactionCsvDto } from '@scaliolabs/baza-nc-shared';

describe('@scaliolabs/baza-nc-api/dividend/008-baza-nc-dividend-transaction-filter-and-export-csv.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionCmsNodeAccess(http);

    const fields = (['status', 'title'] as Array<keyof BazaNcDividendTransactionCsvDto>).map((field) => ({
        field: field as any,
        title: field,
    }));

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcDividendFixtures.BazaNcDividendTransactionFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will filter by title', async () => {
        const response = await dataAccess.list({
            queryString: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);
        expect(response.items[0].title).toBe('DT 1');

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                queryString: 'DT 1',
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual(['status;title', 'Draft;DT 1']);
    });
});
