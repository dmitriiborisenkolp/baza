import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { BazaNcDividendFixtures } from '../baza-nc-dividend.fixtures';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/007-baza-nc-dividend-mail-notifications-user-successful-payment.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUser2Fixture,
                BazaNcDividendFixtures.BazaNcDividendProcessedTransactionMultipleUsersFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will contains 3 emails about successful dividends', async () => {
        await asyncExpect(
            async () => {
                const mailbox = await dataAccessE2e.mailbox();

                expect(isBazaErrorResponse(mailbox)).toBeFalsy();

                /**
                 * Dividend Transaction 1: successful payment notifications for 2 users
                 * Dividend Transaction 2: successful payment notification for 1 user
                 */
                expect(mailbox.length).toBe(3);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will send correct email for e2e@scal.io user', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();

        expect(mailbox[0].to[0]).toBe('e2e@scal.io');
        expect(mailbox[0].subject.includes('12.00 USD')).toBeTruthy();
        expect(mailbox[0].messageBodyText.includes('12.00 USD')).toBeTruthy();
        expect(mailbox[0].messageBodyHtml.includes('12.00 USD')).toBeTruthy();

        expect(mailbox[2].to[0]).toBe('e2e@scal.io');
        expect(mailbox[2].subject.includes('22.50 USD')).toBeTruthy();
        expect(mailbox[2].messageBodyText.includes('22.50 USD')).toBeTruthy();
        expect(mailbox[2].messageBodyHtml.includes('22.50 USD')).toBeTruthy();
    });

    it('will send correct email for e2e-2@scal.io user', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();

        expect(mailbox[1].to[0]).toBe('e2e-2@scal.io');
        expect(mailbox[1].subject.includes('14.97 USD')).toBeTruthy();
        expect(mailbox[1].messageBodyText.includes('14.97 USD')).toBeTruthy();
        expect(mailbox[1].messageBodyHtml.includes('14.97 USD')).toBeTruthy();
    });
});
