import 'reflect-metadata';
import * as moment from 'moment';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    BazaNcDividendDto,
    BazaNcDividendErrorCodes,
    BazaNcDividendPaymentSource,
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionEntryDto,
    convertFromCents,
    NorthCapitalAccountId,
    OfferingId,
} from '@scaliolabs/baza-nc-shared';
import { DwollaCustomerId, DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { bazaNcDividendCsvFileFactory } from '../util/baza-nc-dividend-csv-file-factory.util';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import {
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_CANCEL_TRANSACTION,
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_FAIL_TRANSACTION,
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_PROCESS_TRANSACTION,
} from '../constants/baza-nc-dividend-e2e.constants';
import { BazaDwollaWebhookNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/017-baza-nc-dividend-dwolla-reprocess-public-api', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividend = new BazaNcDividendNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessDwollaWebhooks = new BazaDwollaWebhookNodeAccess(http);

    let dividendTransactionUlid: string;
    let ncOfferingId: OfferingId;
    let accountId: number;
    let ncAccountId: NorthCapitalAccountId;
    let dwollaCustomerId: DwollaCustomerId;

    const dividends: Partial<{
        pending: BazaNcDividendDto;
        cancelled: BazaNcDividendDto;
        failed: BazaNcDividendDto;
        processed: BazaNcDividendDto;
    }> = {};

    const entries: Partial<{
        pending: BazaNcDividendTransactionEntryDto;
        cancelled: BazaNcDividendTransactionEntryDto;
        failed: BazaNcDividendTransactionEntryDto;
        processed: BazaNcDividendTransactionEntryDto;
    }> = {};

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUser2Fixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = false;
    });

    it('will fetch all required resources', async () => {
        const offering = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(offering)).toBeFalsy();
        expect(offering.length).toBe(1);

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        accountId = investorAccount.userId;
        ncAccountId = investorAccount.northCapitalAccountId;
        dwollaCustomerId = investorAccount.dwollaCustomerId;
        ncOfferingId = offering[0].ncOfferingId;
    });

    it('will create a dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        dividendTransactionUlid = response.ulid;
    });

    it('will import dividends from CSV', async () => {
        const createDividendTransactionEntryDate = new Date();

        const csvFile = bazaNcDividendCsvFileFactory([
            ['North Capital ID Number', 'Date', 'Amount', 'North Capital Offering ID'],
            [ncAccountId, '4/01/2022', '0.02', ncOfferingId],
            [
                ncAccountId,
                '4/02/2022',
                convertFromCents(BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_FAIL_TRANSACTION).toFixed(2),
                ncOfferingId,
            ],
            [
                ncAccountId,
                '4/03/2022',
                convertFromCents(BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_CANCEL_TRANSACTION).toFixed(2),
                ncOfferingId,
            ],
            [
                ncAccountId,
                '4/04/2022',
                convertFromCents(BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_PROCESS_TRANSACTION).toFixed(2),
                ncOfferingId,
            ],
        ]);

        const response = await dataAccess.importCsv(csvFile, {
            source: BazaNcDividendPaymentSource.Dwolla,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);
    });

    it('will process dividends', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        await dataAccessE2e.flushMailbox();

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will display all dividend entries with correct statuses', async () => {
        await http.authE2eAdmin();

        const response = await asyncExpect(
            async () => {
                const response = await dataAccess.list({
                    dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(4);
                expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
                expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Failed);
                expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

                expect(response.items[0].canBeReprocessed).toBeFalsy();
                expect(response.items[1].canBeReprocessed).toBeTruthy();
                expect(response.items[2].canBeReprocessed).toBeTruthy();
                expect(response.items[3].canBeReprocessed).toBeFalsy();

                expect(response.items[0].dateProcessedAt).toBeDefined();

                expect(response.items[0].dateUpdatedAt).toBeDefined();
                expect(response.items[1].dateUpdatedAt).toBeDefined();
                expect(response.items[2].dateUpdatedAt).toBeDefined();
                expect(response.items[3].dateUpdatedAt).toBeDefined();

                return response;
            },
            null,
            { intervalMillis: 2000 },
        );

        entries.processed = response.items[0];
        entries.cancelled = response.items[1];
        entries.failed = response.items[2];
        entries.pending = response.items[3];
    });

    it('will display all dividends with correct statuses', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
        expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Failed);
        expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

        expect(response.items[0].canBeReprocessed).toBeFalsy();
        expect(response.items[1].canBeReprocessed).toBeTruthy();
        expect(response.items[2].canBeReprocessed).toBeTruthy();
        expect(response.items[3].canBeReprocessed).toBeFalsy();

        dividends.processed = response.items[0];
        dividends.cancelled = response.items[1];
        dividends.failed = response.items[2];
        dividends.pending = response.items[3];
    });

    it('will not allow to reprocess processed dividend', async () => {
        await http.authE2eUser();

        const response: BazaError = (await dataAccessDividend.reprocess({
            ulid: dividends.processed.ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeProcessed);
    });

    it('will display all dividends with correct statuses (2)', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
        expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Failed);
        expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

        expect(response.items[0].canBeReprocessed).toBeFalsy();
        expect(response.items[1].canBeReprocessed).toBeTruthy();
        expect(response.items[2].canBeReprocessed).toBeTruthy();
        expect(response.items[3].canBeReprocessed).toBeFalsy();
    });

    it('will not allow to reprocess pending dividend', async () => {
        await http.authE2eUser();

        const response: BazaError = (await dataAccessDividend.reprocess({
            ulid: dividends.pending.ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeProcessed);
    });

    it('will display all dividends with correct statuses (3)', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
        expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Failed);
        expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

        expect(response.items[0].canBeReprocessed).toBeFalsy();
        expect(response.items[1].canBeReprocessed).toBeTruthy();
        expect(response.items[2].canBeReprocessed).toBeTruthy();
        expect(response.items[3].canBeReprocessed).toBeFalsy();
    });

    it('will not allow to reprocess failed dividend of another user', async () => {
        await http.authE2eUser2();

        const response: BazaError = (await dataAccessDividend.reprocess({
            ulid: dividends.pending.ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendNotFound);
    });

    it('will allow to reprocess failed dividend', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.reprocess({
            ulid: dividends.failed.ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.status).toBe(BazaNcDividendPaymentStatus.Pending);
    });

    it('will display all dividends with correct statuses (4)', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Cancelled);
        expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

        expect(response.items[0].canBeReprocessed).toBeFalsy();
        expect(response.items[1].canBeReprocessed).toBeTruthy();
        expect(response.items[2].canBeReprocessed).toBeFalsy();
        expect(response.items[3].canBeReprocessed).toBeFalsy();
    });

    it('will not allow to reprocess cancelled dividend of another user', async () => {
        await http.authE2eUser2();

        const response: BazaError = (await dataAccessDividend.reprocess({
            ulid: dividends.cancelled.ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendNotFound);
    });

    it('will allow to reprocess cancelled dividend', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.reprocess({
            ulid: dividends.cancelled.ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.status).toBe(BazaNcDividendPaymentStatus.Pending);
    });

    it('will display all dividends with correct statuses (5)', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

        expect(response.items[0].canBeReprocessed).toBeFalsy();
        expect(response.items[1].canBeReprocessed).toBeFalsy();
        expect(response.items[2].canBeReprocessed).toBeFalsy();
        expect(response.items[3].canBeReprocessed).toBeFalsy();
    });

    it('will display all dividend entries with correct statuses (after reprocessing)', async () => {
        await http.authE2eAdmin();

        const response = await dataAccess.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
        expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

        expect(response.items[0].dwollaTransferId).toBe(entries.processed.dwollaTransferId);
        expect(response.items[1].dwollaTransferId).not.toBe(entries.cancelled.dwollaTransferId);
        expect(response.items[2].dwollaTransferId).not.toBe(entries.failed.dwollaTransferId);
        expect(response.items[3].dwollaTransferId).toBe(entries.pending.dwollaTransferId);

        expect(response.items[0].canBeReprocessed).toBeFalsy();
        expect(response.items[1].canBeReprocessed).toBeFalsy();
        expect(response.items[2].canBeReprocessed).toBeFalsy();
        expect(response.items[3].canBeReprocessed).toBeFalsy();

        expect(response.items[0].dateProcessedAt).toBeDefined();

        expect(response.items[0].dateUpdatedAt).toBeDefined();
        expect(response.items[1].dateUpdatedAt).toBeDefined();
        expect(response.items[2].dateUpdatedAt).toBeDefined();
        expect(response.items[3].dateUpdatedAt).toBeDefined();

        entries.processed = response.items[0];
        entries.cancelled = response.items[1];
        entries.failed = response.items[2];
        entries.pending = response.items[3];
    });

    it('will send Dwolla webhook with transfer_completed for cancelled dividend', async () => {
        const response = await dataAccessDwollaWebhooks.accept({
            topic: DwollaEventTopic.transfer_completed,
            id: '72127b77-3f2c-4ccf-b3ed-4076b6e91c75',
            resourceId: entries.cancelled.dwollaTransferId,
            timestamp: '2022-07-11T19:14:18.776Z',
            created: '2022-07-11T19:14:18.776Z',
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display all dividends with correct statuses (6)', async () => {
        await http.authE2eUser();

        await asyncExpect(
            async () => {
                const response = await dataAccessDividend.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(4);
                expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[1].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[2].status).toBe(BazaNcDividendPaymentStatus.Pending);
                expect(response.items[3].status).toBe(BazaNcDividendPaymentStatus.Pending);

                expect(response.items[0].canBeReprocessed).toBeFalsy();
                expect(response.items[1].canBeReprocessed).toBeFalsy();
                expect(response.items[2].canBeReprocessed).toBeFalsy();
                expect(response.items[3].canBeReprocessed).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
