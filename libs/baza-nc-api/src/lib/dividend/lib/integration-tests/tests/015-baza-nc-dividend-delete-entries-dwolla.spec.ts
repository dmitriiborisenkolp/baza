import 'reflect-metadata';
import * as moment from 'moment';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcDividendErrorCodes, BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_FAIL_TRANSACTION } from '../constants/baza-nc-dividend-e2e.constants';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

const date = '2022-06-18T14:28:17.727Z';

describe('@scaliolabs/baza-nc-api/dividend/015-baza-nc-dividend-delete-entries-dwolla.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessDividends = new BazaNcDividendNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);

    let dividendTransactionUlid: string;
    let ncOfferingId: OfferingId;
    let investorAccountId: number;
    let ulid: string;

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = false;
    });

    it('will fetch all required resources', async () => {
        const offering = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(offering)).toBeFalsy();
        expect(offering.length).toBe(1);

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        investorAccountId = investorAccount.id;
        ncOfferingId = offering[0].ncOfferingId;
    });

    it('will create a dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        dividendTransactionUlid = response.ulid;
    });

    it('will create an entry', async () => {
        const createDividendTransactionEntryDate = new Date();

        const response = await dataAccess.create({
            date,
            dividendTransactionUlid,
            investorAccountId,
            offeringId: ncOfferingId,
            source: BazaNcDividendPaymentSource.Dwolla,
            amountCents: 1000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canBeDeleted).toBeTruthy();
        expect(response.canBeUpdated).toBeTruthy();
        expect(response.canBeReprocessed).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);

        ulid = response.ulid;
    });

    it('will successfully delete entry', async () => {
        const deleteDividendTransactionEntryDate = new Date();
        const response = await dataAccess.delete({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(deleteDividendTransactionEntryDate)).toBe(true);
    });

    it('will not delete dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will create an entry (1)', async () => {
        const createDividendTransactionEntryDate = new Date();

        const response = await dataAccess.create({
            date,
            dividendTransactionUlid,
            investorAccountId,
            offeringId: ncOfferingId,
            source: BazaNcDividendPaymentSource.Dwolla,
            amountCents: BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_FAIL_TRANSACTION,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canBeDeleted).toBeTruthy();
        expect(response.canBeUpdated).toBeTruthy();
        expect(response.canBeReprocessed).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);

        ulid = response.ulid;
    });

    it('will process dividends', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        await dataAccessE2e.flushMailbox();

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionEntryDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionEntryDate)).toBe(true);
    });

    it('will shows that dividend transaction is failed', async () => {
        await asyncExpect(
            async () => {
                const list = await dataAccess.list({
                    dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(list)).toBeFalsy();
                expect(list.items.length).toBe(1);

                expect(list.items[0].status).toBe(BazaNcDividendPaymentStatus.Failed);
                expect(list.items[0].canBeUpdated).toBeTruthy();
                expect(list.items[0].canBeDeleted).toBeTruthy();
                expect(list.items[0].canBeReprocessed).toBeTruthy();
                expect(list.items[0].dateUpdatedAt).toBeDefined();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully delete failed entry', async () => {
        const response = await dataAccess.delete({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will delete dividend transaction', async () => {
        const response: BazaError = (await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionNotFound);
    });

    it('will shows correct amount of paid dividends for user', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividends.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(0);
    });
});
