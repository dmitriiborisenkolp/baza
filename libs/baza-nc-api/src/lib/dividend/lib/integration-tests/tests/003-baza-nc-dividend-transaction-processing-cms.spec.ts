import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcDividendErrorCodes,
    BazaNcDividendPaymentSource,
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionStatus,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcDividendFixtures } from '../baza-nc-dividend.fixtures';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import * as moment from 'moment/moment';

jest.setTimeout(240000);

let token: string;

describe('@scaliolabs/baza-nc-api/dividend/003-baza-nc-dividend-transaction-processing-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessDividend = new BazaNcDividendNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccessDividendTransactionEntriesCms = new BazaNcDividendTransactionEntryCmsNodeAccess(http);

    let dividendTransactionUlid: string;
    let dividendTransactionUlid2: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcDividendFixtures.BazaNcDividendTransactionFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns list of dividend transactions', async () => {
        const response = await dataAccessDividendTransactionCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('DT 2');
        expect(response.items[0].status).toBe(BazaNcDividendTransactionStatus.Draft);
        expect(response.items[1].title).toBe('DT 1');
        expect(response.items[1].status).toBe(BazaNcDividendTransactionStatus.Draft);

        dividendTransactionUlid = response.items[1].ulid;
        dividendTransactionUlid2 = response.items[0].ulid;
    });

    it('will display no dividends for current user', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will display 0 total amount for current user', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.totalAmount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalAmount).toBe('0.00');
        expect(response.totalAmountCents).toBe(0);
    });

    it('will display correct flags for dividend transaction entries before processing', async () => {
        const entries = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entries)).toBeFalsy();
        expect(entries.items.length > 0).toBeTruthy();

        for (const entry of entries.items) {
            expect(entry.canBeUpdated).toBeTruthy();
            expect(entry.canBeReprocessed).toBeFalsy();
            expect(entry.canBeDeleted).toBeTruthy();
        }
    });

    it('will not allow to process dividends with random token', async () => {
        const response: BazaError = (await dataAccessDividendTransactionCms.process({
            token: 'd6880177',
            async: false,
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendConfirmationNotFound);
    });

    it('will send process confirmation', async () => {
        const response = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to process dividends with random token (2)', async () => {
        const response: BazaError = (await dataAccessDividendTransactionCms.process({
            token: 'd6880177',
            async: false,
            password: 'e2e-admin-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendConfirmationNotFound);
    });

    it('will contains a confirmation email and token in mailbox', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);
    });

    it('will not allow to process dividends with correct token and invalid password', async () => {
        const response: BazaError = (await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-invalid-password',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendConfirmationInvalidPassword);
    });

    it('will allow to process dividends with correct token and password', async () => {
        const processDividendTransactionDate = new Date();

        const response = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(dividendTransaction.dateProcessedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
        expect(moment(dividendTransaction.dateProcessedAt).isSameOrAfter(processDividendTransactionDate)).toBe(true);
    });

    it('will update status for dividends transaction', async () => {
        const response = await dataAccessDividendTransactionCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items[0].status).toBe(BazaNcDividendTransactionStatus.Draft);
        expect(response.items[1].status).toBe(BazaNcDividendTransactionStatus.Successful);

        expect(response.items[0].isLocked).toBeFalsy();
        expect(response.items[1].isLocked).toBeTruthy();

        expect(response.items[0].canBeDeleted).toBeTruthy();
        expect(response.items[1].canBeDeleted).toBeFalsy();

        expect(response.items[0].isProcessing).toBeFalsy();
        expect(response.items[1].isProcessing).toBeFalsy();
    });

    it('will update status for dividend transaction entries', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);

        response.items.forEach((next) => {
            expect(next.status).toBe(BazaNcDividendPaymentStatus.Processed);
        });
    });

    it('will display dividends for user 1', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);

        response.items.forEach((next) => {
            expect(next.status).toBe(BazaNcDividendPaymentStatus.Processed);
        });

        expect(response.items[0].amountCents).toBe(1000);
        expect(response.items[1].amountCents).toBe(1497);
        expect(response.items[2].amountCents).toBe(1250);

        expect(response.items[0].amount).toBe('10.00');
        expect(response.items[1].amount).toBe('14.97');
        expect(response.items[2].amount).toBe('12.50');
    });

    it('will display no dividends for user 2', async () => {
        await http.authE2eUser2();

        const response = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will display that processed dividends transaction cannot be deleted anymore', async () => {
        const response = await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canBeDeleted).toBeFalsy();
    });

    it('will display that un-processed dividends transaction can be deleted', async () => {
        const response = await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canBeDeleted).toBeTruthy();
    });

    it('will fail to delete processed dividends transaction', async () => {
        const response: BazaError = (await dataAccessDividendTransactionCms.delete({
            ulid: dividendTransactionUlid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionCannotBeDeleted);
    });

    it('will successfully delete un-processed dividends transaction', async () => {
        const response = await dataAccessDividendTransactionCms.delete({
            ulid: dividendTransactionUlid2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display total amount of new dividends for current user', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividend.totalAmount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalAmount).toBe('37.47');
        expect(response.totalAmountCents).toBe(3747);
    });

    it('will display correct flags for dividend transaction entries after processing', async () => {
        const entries = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entries)).toBeFalsy();
        expect(entries.items.length > 0).toBeTruthy();

        for (const entry of entries.items) {
            expect(entry.canBeUpdated).toBeFalsy();
            expect(entry.canBeReprocessed).toBeFalsy();
            expect(entry.canBeDeleted).toBeFalsy();
        }
    });

    it('will not allow to add more dividend entries when dividends transaction is processed', async () => {
        await http.authE2eUser();

        const listResponse = await dataAccessDividend.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length > 0).toBeTruthy();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessDividendTransactionEntriesCms.create({
            dividendTransactionUlid,
            amountCents: 1000,
            investorAccountId: investorAccount.id,
            source: BazaNcDividendPaymentSource.NC,
            offeringId: listResponse.items[0].offeringId,
            date: new Date().toISOString(),
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionIsLocked);
    });

    it('will not allow update dividend transaction entry when dividends transaction is processed', async () => {
        const entries = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entries)).toBeFalsy();
        expect(entries.items.length > 0).toBeTruthy();

        const response: BazaError = (await dataAccessDividendTransactionEntriesCms.update({
            ulid: entries.items[0].ulid,
            offeringId: entries.items[0].offeringId,
            date: entries.items[0].date,
            amountCents: 1000,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionIsLocked);
    });

    it('will not allow delete dividend transaction entry when dividends transaction is processed', async () => {
        const entries = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(entries)).toBeFalsy();
        expect(entries.items.length > 0).toBeTruthy();

        const response: BazaError = (await dataAccessDividendTransactionEntriesCms.delete({
            ulid: entries.items[0].ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcDividendErrorCodes.BazaNcDividendTransactionIsLocked);
    });
});
