import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcDividendTransactionCmsNodeAccess, BazaNcDividendTransactionEntryCmsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendFixtures } from '../baza-nc-dividend.fixtures';
import { BazaNcDividendTransactionEntryCsvDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dividend/009-baza-nc-dividend-transaction-entries-filter-and-export-csv.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessDividendTransaction = new BazaNcDividendTransactionCmsNodeAccess(http);

    const fields = (['date', 'investorAccount', 'amount', 'status', 'title'] as Array<keyof BazaNcDividendTransactionEntryCsvDto>).map(
        (field) => ({
            field: field as any,
            title: field,
        }),
    );

    let dividendTransactionUlid: string;
    let ncAccountId: string;
    let ncPartyId: string;
    let ncOfferingId: string;
    let ncOfferingName: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingMultipleFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUser2Fixture,
                BazaNcDividendFixtures.BazaNcDividendProcessedTransactionFilterAndCsvFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns dividend transactions', async () => {
        const response = await dataAccessDividendTransaction.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(2);

        dividendTransactionUlid = response.items[1].ulid;
    });

    it('will filter by investor account name (E2E User 2)', async () => {
        const response = await dataAccess.list({
            queryString: 'E2E User 2',
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        expect(response.items[0].amount).toBe('14.97');

        ncAccountId = response.items[0].investorAccount.northCapitalAccountId;
        ncPartyId = response.items[0].investorAccount.northCapitalAccountId;
        ncOfferingId = response.items[0].offeringId;
        ncOfferingName = response.items[0].offeringTitle;

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                dividendTransactionUlid,
                queryString: 'E2E User 2',
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual(['date;investorAccount;amount;status;title', '06/13/2022;E2E User 2;14.97;Processed;']);
    });

    it('will filter by investor NC Account Id (E2E User 2)', async () => {
        const response = await dataAccess.list({
            queryString: ncAccountId,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        expect(response.items[0].amount).toBe('14.97');

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                dividendTransactionUlid,
                queryString: ncAccountId,
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual(['date;investorAccount;amount;status;title', '06/13/2022;E2E User 2;14.97;Processed;']);
    });

    it('will filter by investor NC Party Id (E2E User 2)', async () => {
        const response = await dataAccess.list({
            queryString: ncPartyId,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        expect(response.items[0].amount).toBe('14.97');

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                dividendTransactionUlid,
                queryString: ncPartyId,
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual(['date;investorAccount;amount;status;title', '06/13/2022;E2E User 2;14.97;Processed;']);
    });

    it('will filter by investor account name (E2E User)', async () => {
        const response = await dataAccess.list({
            queryString: 'E2E User',
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(3);

        expect(response.items[0].amount).toBe('14.97');
        expect(response.items[1].amount).toBe('12.50');
        expect(response.items[2].amount).toBe('10.00');

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                dividendTransactionUlid,
                queryString: 'E2E User',
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual([
            'date;investorAccount;amount;status;title',
            '06/13/2022;E2E User 2;14.97;Processed;',
            '06/10/2022;E2E User;12.50;Processed;',
            '06/18/2022;E2E User;10.00;Processed;',
        ]);
    });

    it('will filter by investor account and date', async () => {
        const dateFrom = new Date(2022, 5, 10);
        const dateTo = new Date(2022, 5, 13);

        const response = await dataAccess.list({
            queryString: 'E2E User',
            dividendTransactionUlid,
            dateFrom: dateFrom.toISOString(),
            dateTo: dateTo.toISOString(),
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(2);

        expect(response.items[0].amount).toBe('14.97');
        expect(response.items[1].amount).toBe('12.50');

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                dividendTransactionUlid,
                queryString: 'E2E User',
                dateFrom: dateFrom.toISOString(),
                dateTo: dateTo.toISOString(),
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual([
            'date;investorAccount;amount;status;title',
            '06/13/2022;E2E User 2;14.97;Processed;',
            '06/10/2022;E2E User;12.50;Processed;',
        ]);
    });

    it('will filter by offering id', async () => {
        const response = await dataAccess.list({
            queryString: ncOfferingId,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(2);

        expect(response.items[0].amount).toBe('14.97');
        expect(response.items[1].amount).toBe('10.00');

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                dividendTransactionUlid,
                queryString: ncOfferingId,
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual([
            'date;investorAccount;amount;status;title',
            '06/13/2022;E2E User 2;14.97;Processed;',
            '06/18/2022;E2E User;10.00;Processed;',
        ]);
    });

    it('will filter by offering name', async () => {
        const response = await dataAccess.list({
            queryString: ncOfferingName,
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(2);

        expect(response.items[0].amount).toBe('14.97');
        expect(response.items[1].amount).toBe('10.00');

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                dividendTransactionUlid,
                queryString: ncOfferingName,
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual([
            'date;investorAccount;amount;status;title',
            '06/13/2022;E2E User 2;14.97;Processed;',
            '06/18/2022;E2E User;10.00;Processed;',
        ]);
    });

    it('will filter by amount', async () => {
        const response = await dataAccess.list({
            queryString: '10.00',
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        expect(response.items[0].amount).toBe('10.00');

        const csvResponse = await dataAccess.exportToCsv({
            listRequest: {
                queryString: '10.00',
                dividendTransactionUlid,
            },
            fields,
        });

        expect(isBazaErrorResponse(csvResponse)).toBeFalsy();
        expect(typeof csvResponse === 'string').toBeTruthy();

        const csv = csvResponse.split('\n');

        expect(csv).toEqual(['date;investorAccount;amount;status;title', '06/18/2022;E2E User;10.00;Processed;']);
    });
});
