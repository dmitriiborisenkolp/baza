import 'reflect-metadata';
import * as moment from 'moment';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BazaNcDividendNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { bazaNcDividendExtractConfirmToken } from '../util/baza-nc-dividend-extract-confirm-token.util';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

const date = '2022-06-18T14:28:17.727Z';

describe('@scaliolabs/baza-nc-api/dividend/016-baza-nc-dividend-dwolla-sync.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessDividends = new BazaNcDividendNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);

    let dividendTransactionUlid: string;
    let ncOfferingId: OfferingId;
    let investorAccountId: number;
    let ulid: string;

    beforeAll(async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = true;

        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    afterAll(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        moment.suppressDeprecationWarnings = false;
    });

    it('will fetch all required resources', async () => {
        const offering = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(offering)).toBeFalsy();
        expect(offering.length).toBe(1);

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        investorAccountId = investorAccount.id;
        ncOfferingId = offering[0].ncOfferingId;
    });

    it('will create a dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        dividendTransactionUlid = response.ulid;
    });

    it('will create an entry', async () => {
        const createDividendTransactionEntryDate = new Date();

        const response = await dataAccess.create({
            date,
            dividendTransactionUlid,
            investorAccountId,
            offeringId: ncOfferingId,
            source: BazaNcDividendPaymentSource.Dwolla,
            amountCents: 1000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canBeDeleted).toBeTruthy();
        expect(response.canBeUpdated).toBeTruthy();
        expect(response.canBeReprocessed).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);

        ulid = response.ulid;
    });

    it('will process dividends', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        await dataAccessE2e.flushMailbox();

        const token = bazaNcDividendExtractConfirmToken(mailbox[0].messageBodyText);

        const processDividendTransactionEntryDate = new Date();

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(processDividendTransactionEntryDate)).toBe(true);

        await dataAccessE2e.flushMailbox();
    });

    it('will shows that all dividend entries are on Pending state', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccess.list({
                    dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Pending);
                expect(response.items[0].dateUpdatedAt).toBeDefined();
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will sync with dwolla', async () => {
        const response = await dataAccessDividendTransactionCms.sync({
            ulid: dividendTransactionUlid,
            async: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will mark dividend entries as processed', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccess.list({
                    dividendTransactionUlid,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
                expect(response.items[0].dateUpdatedAt).toBeDefined();
                expect(response.items[0].dateProcessedAt).toBeDefined();
            },
            null,
            { intervalMillis: 1000 },
        );
    });

    it('will display dividends for users as processed', async () => {
        await http.authE2eUser();

        const response = await dataAccessDividends.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaNcDividendPaymentStatus.Processed);
    });

    it('will send a email notification', async () => {
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();

        expect(mailbox.length).toBe(1);
        expect(mailbox[0].messageBodyText.includes('10.00')).toBeTruthy();
    });
});
