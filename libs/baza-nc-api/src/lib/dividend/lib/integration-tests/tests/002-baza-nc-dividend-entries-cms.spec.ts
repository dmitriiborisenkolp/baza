import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcInvestorAccountCmsNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import * as moment from 'moment';

jest.setTimeout(240000);

const date = '2022-06-18T14:28:17.727Z';
const date2 = '2022-06-17T14:28:17.727Z';

describe('@scaliolabs/baza-nc-api/dividend/002-baza-nc-dividend-entries-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccessDividendTransactionEntriesCms = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessInvestorAccountCms = new BazaNcInvestorAccountCmsNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);

    /* according to test : 'will create an entry for DT1' :
     * <dividendTransactionUlid> is an ulid of dividend-transaction that is parent of dividend-transaction-entry with <ulid>
     */
    let ulid: string;
    let offeringId: OfferingId;
    let investorAccountId: number;
    let dividendTransactionUlid: string;
    let dividendTransactionUlid2: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will fetch offering id', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBeGreaterThan(0);

        offeringId = response[0].ncOfferingId;
    });

    it('will fetch investor account', async () => {
        const response = await dataAccessInvestorAccountCms.listInvestorAccounts({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        investorAccountId = response.items[0].id;
    });

    it('will create a new dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('DT 1');

        dividendTransactionUlid = response.ulid;
    });

    it('will create one more dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 2',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('DT 2');

        dividendTransactionUlid2 = response.ulid;
    });

    it('will display empty list of entries for DT1 (1)', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will display that dividend transaction still can be deleted', async () => {
        const response = await dataAccessDividendTransactionCms.getByUlid({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canBeDeleted).toBeTruthy();
    });

    it('will display empty list of entries for DT2 (1)', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid: dividendTransactionUlid2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will create an entry for DT1', async () => {
        const createDividendTransactionEntryDate = new Date();

        const response = await dataAccessDividendTransactionEntriesCms.create({
            date,
            dividendTransactionUlid,
            investorAccountId,
            offeringId,
            source: BazaNcDividendPaymentSource.NC,
            amountCents: 1000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.date).toBe(date);
        expect(response.dateCreatedAt).not.toBe(date);
        expect(response.dateUpdatedAt).toBeUndefined();
        expect(response.dateProcessedAt).toBeUndefined();

        expect(response.status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.investorAccount.id).toBe(investorAccountId);
        expect(response.offeringId).toBe(offeringId);

        expect(response.amount).toBe('10.00');
        expect(response.amountCents).toBe(1000);

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(createDividendTransactionEntryDate)).toBe(true);

        ulid = response.ulid;
    });

    it('will display created entry in entries list for DT1 (2)', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].ulid).toBe(ulid);
    });

    it('will display empty list of entries for DT2 (2)', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid: dividendTransactionUlid2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will update entry', async () => {
        const updateDividendTransactionEntryDate = new Date();

        const response = await dataAccessDividendTransactionEntriesCms.update({
            ulid,
            offeringId,
            date: date2,
            amountCents: 5055,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.date).toBe(date2);
        expect(response.dateCreatedAt).not.toBe(date);
        expect(response.dateCreatedAt).not.toBe(date2);
        expect(response.dateUpdatedAt).toBeDefined();
        expect(response.dateProcessedAt).toBeUndefined();

        expect(response.status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.investorAccount.id).toBe(investorAccountId);
        expect(response.offeringId).toBe(offeringId);

        expect(response.amount).toBe('50.55');
        expect(response.amountCents).toBe(5055);

        expect(response.ulid).toBe(ulid);

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(updateDividendTransactionEntryDate)).toBe(true);
    });

    it('will returns entry by id', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.getByUlid({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.date).toBe(date2);
        expect(response.dateCreatedAt).not.toBe(date);
        expect(response.dateCreatedAt).not.toBe(date2);
        expect(response.dateUpdatedAt).toBeDefined();
        expect(response.dateProcessedAt).toBeUndefined();

        expect(response.status).toBe(BazaNcDividendPaymentStatus.Pending);
        expect(response.investorAccount.id).toBe(investorAccountId);
        expect(response.offeringId).toBe(offeringId);

        expect(response.amount).toBe('50.55');
        expect(response.amountCents).toBe(5055);

        expect(response.ulid).toBe(ulid);
    });

    it('will reflect updates in DT1 entries list', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].ulid).toBe(ulid);
        expect(response.items[0].amount).toBe('50.55');
    });

    it('will delete entry', async () => {
        const deleteDividendTransactionEntryDate = new Date();

        const response = await dataAccessDividendTransactionEntriesCms.delete({
            ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const dividendTransaction = await dataAccessDividendTransactionCms.getByUlid({ ulid: dividendTransactionUlid });

        expect(isBazaErrorResponse(dividendTransaction)).toBeFalsy();

        expect(dividendTransaction.dateUpdatedAt).toBeDefined();
        expect(moment(dividendTransaction.dateUpdatedAt).isSameOrAfter(deleteDividendTransactionEntryDate)).toBe(true);
    });

    it('will display empty list of entries for DT1 (3)', async () => {
        const response = await dataAccessDividendTransactionEntriesCms.list({
            dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });
});
