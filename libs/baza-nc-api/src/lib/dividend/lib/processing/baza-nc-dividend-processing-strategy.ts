import { BazaNcDividendEntity, BazaNcDividendTransactionEntity, BazaNcDividendTransactionEntryEntity } from '../../../typeorm';
import { BazaNcDividendPaymentSource } from '@scaliolabs/baza-nc-shared';

/**
 * (Internal interfaces for processing services)
 * @private
 */
export interface BazaNcDividendProcessApiRequest {
    dividendEntries: Array<BazaNcDividendTransactionEntryEntity>;
    dividendTransaction: BazaNcDividendTransactionEntity;
}

/**
 * (Internal interfaces for processing services)
 * @private
 */
export interface BazaNcDividendProcessApiResponse {
    successfulPayments: Array<BazaNcDividendEntity>;
    failedPayments: Array<BazaNcDividendTransactionEntryEntity>;
}

/**
 * (Internal interfaces for processing services)
 * @private
 */
export interface BazaNcDividendReprocessApiRequest {
    dividendEntries: Array<BazaNcDividendTransactionEntryEntity>;
    dividendTransaction: BazaNcDividendTransactionEntity;
}

/**
 * (Internal interfaces for processing services)
 * @private
 */
export interface BazaNcDividendReprocessApiResponse {
    successfulPayments: Array<BazaNcDividendEntity>;
    failedPayments: Array<BazaNcDividendTransactionEntryEntity>;
}

/**
 * (Internal interfaces for processing services)
 * @private
 */
export interface BazaNcDividendSyncApiRequest {
    dividendEntries: Array<BazaNcDividendTransactionEntryEntity>;
    dividendTransaction: BazaNcDividendTransactionEntity;
}

/**
 * (Internal interfaces for processing services)
 * @private
 */
export interface BazaNcDividendSyncApiResponse {
    unknown: Array<BazaNcDividendTransactionEntryEntity>;
    missing: Array<BazaNcDividendTransactionEntryEntity>;
    pending: Array<BazaNcDividendTransactionEntryEntity>;
    processed: Array<BazaNcDividendTransactionEntryEntity>;
    failed: Array<BazaNcDividendTransactionEntryEntity>;
    cancelled: Array<BazaNcDividendTransactionEntryEntity>;
}

/**
 * (Internal interfaces for processing services)
 * @private
 */
export interface BazaNcDividendProcessApiCallbacks {
    /**
     * Callback to abort Dividend Transaction
     */
    abortDividendTransaction: () => Promise<void>;

    /**
     * Callback to mark Dividend Transaction as Failed
     */
    failDividendTransaction: () => Promise<void>;

    /**
     * Callback to update Dividend Transaction status. Should be executed after each Dividend Transaction Entry (re-)processing
     * and status updates
     */
    updateDividendTransaction: (options?: { shouldBeMarkedAsStarted?: boolean }) => Promise<void>;
}

/**
 * Processing Strategy interface
 * For each available BazaNcDividendPaymentSource Baza provides processing strategy
 * @see BazaNcDividendPaymentSource
 */
export interface BazaNcDividendProcessingStrategy {
    /**
     * Source with which strategy is working
     */
    source: BazaNcDividendPaymentSource;

    /**
     * Starts processing for Dividend Transaction + Dividend Transaction Entries with same BazaNcDividendPaymentSource
     * @param request
     * @param callbacks
     */
    process(
        request: BazaNcDividendProcessApiRequest,
        callbacks: BazaNcDividendProcessApiCallbacks,
    ): Promise<BazaNcDividendProcessApiResponse>;

    /**
     * Starts re-processing for Dividend Transaction + Dividend Transaction Entries with same BazaNcDividendPaymentSource
     * @param request
     * @param callbacks
     */
    reprocess?(
        request: BazaNcDividendReprocessApiRequest,
        callbacks: BazaNcDividendProcessApiCallbacks,
    ): Promise<BazaNcDividendReprocessApiResponse>;

    /**
     * Refreshes statuses (retrieves from external source) for Dividend Transaction + Dividend Transaction Entries with same BazaNcDividendPaymentSource
     * @param request
     * @param callbacks
     */
    sync?(request: BazaNcDividendSyncApiRequest, callbacks: BazaNcDividendProcessApiCallbacks): Promise<BazaNcDividendSyncApiResponse>;
}
