import { Injectable, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { from, of, Subject } from 'rxjs';
import {
    BazaNcDividendDwollaClientNotificationsService,
    BazaNcDividendDwollaClientNotificationsServiceRequest as Request,
} from './baza-nc-dividend-dwolla-client-notifications.service';
import { catchError, concatMap } from 'rxjs/operators';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import { BazaNcDividendTransactionEntryEntity, BazaNcDividendTransactionEntryRepository } from '../../../../../typeorm';

enum RequestType {
    TransferInitiated,
    TransferCancelled,
    TransferCompleted,
    TransferFailed,
}

type QueueRequest = { type: RequestType; request: Request };

/**
 * Email notifications queue for Dividends sent with Dwolla
 * @see BazaNcDividendDwollaClientNotificationsService
 */
@Injectable()
export class BazaNcDividendDwollaClientNotificationsQueueService implements OnApplicationBootstrap, OnApplicationShutdown {
    private readonly queue$ = new Subject<QueueRequest>();
    private readonly onApplicationShutdown$ = new Subject<void>();

    constructor(
        private readonly logger: BazaLogger,
        private readonly service: BazaNcDividendDwollaClientNotificationsService,
        private readonly dividendEntryRepository: BazaNcDividendTransactionEntryRepository,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Set up Email Queue on Nest Application start
     */
    onApplicationBootstrap(): void {
        this.queue$
            .pipe(
                concatMap((next) =>
                    from(this.processQueueItem(next)).pipe(
                        catchError((err) => {
                            this.logger.error(err);

                            return of(undefined);
                        }),
                    ),
                ),
            )
            .subscribe();
    }

    /**
     * Shutdowns Email Queue on Nest Application shutdown
     */
    onApplicationShutdown(): void {
        this.onApplicationShutdown$.next();
    }

    async touchDividendTransactionReport(dividendEntry: BazaNcDividendTransactionEntryEntity): Promise<void> {
        dividendEntry.processedByDwolla = true;

        await this.dividendEntryRepository.save([dividendEntry]);

        if (!(await this.dividendEntryRepository.hasUnprocessedByDwollaEntities(dividendEntry.dividendTransaction))) {
            if (this.service.isClientReportEnabled) {
                await this.service.sendClientReportAboutDividendTransaction({
                    dividendTransaction: dividendEntry.dividendTransaction,
                });
            }
        }
    }

    /**
     * Sends "Transfer Initiated" notification to CMS Admin ("Admin Email" in Registry)
     * @param request
     */
    async transferInitiatedNotification(request: Request): Promise<void> {
        this.queue$.next({
            type: RequestType.TransferInitiated,
            request,
        });
    }

    /**
     * Sends "Transfer Cancelled" notification to CMS Admin ("Admin Email" in Registry)
     * @param request
     */
    async transferCancelledNotification(request: Request): Promise<void> {
        this.queue$.next({
            type: RequestType.TransferCancelled,
            request,
        });
    }

    /**
     * Sends "Transfer Completed" notification to CMS Admin ("Admin Email" in Registry)
     * @param request
     */
    async transferCompletedNotification(request: Request): Promise<void> {
        this.queue$.next({
            type: RequestType.TransferCompleted,
            request,
        });
    }

    /**
     * Sends "Transfer Failed" notification to CMS Admin ("Admin Email" in Registry)
     * @param request
     */
    async transferFailedNotification(request: Request): Promise<void> {
        this.queue$.next({
            type: RequestType.TransferFailed,
            request,
        });
    }

    /**
     * Adds Email Notification to Queue
     * @param queueRequest
     * @private
     */
    private async processQueueItem(queueRequest: QueueRequest): Promise<void> {
        switch (queueRequest.type) {
            default: {
                throw new Error(`Unknown QueueItem type "${queueRequest.type}"`);
            }

            case RequestType.TransferInitiated: {
                return this.service.transferInitiatedNotification(queueRequest.request);
            }

            case RequestType.TransferCancelled: {
                return this.service.transferCancelledNotification(queueRequest.request);
            }

            case RequestType.TransferCompleted: {
                return this.service.transferCompletedNotification(queueRequest.request);
            }

            case RequestType.TransferFailed: {
                return this.service.transferFailedNotification(queueRequest.request);
            }
        }
    }
}
