import { Injectable } from '@nestjs/common';
import { BazaRegistryService, I18nApiService, MailService } from '@scaliolabs/baza-core-api';
import {
    BazaNcDividendRepository,
    BazaNcDividendTransactionEntity,
    BazaNcDividendTransactionEntryRepository,
    BazaNcInvestorAccountRepository,
} from '../../../../../typeorm';
import { BazaNcDividendPaymentStatus, convertFromCents } from '@scaliolabs/baza-nc-shared';
import { BazaRegistryNodeEmailRecipientValue, getBazaClientName, replaceTags } from '@scaliolabs/baza-core-shared';
import { DwollaTransferId } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcDividendTransactionEntryMapper } from '../../../mappers/baza-nc-dividend-transaction-entry.mapper';
import { BazaNcDividendTransactionMapper } from '../../../mappers/baza-nc-dividend-transaction.mapper';
import { BazaNcMail } from '../../../../../../constants/baza-nc.mail-templates';

interface Request {
    amountCents: number;
    investorAccountId: number;
    dwollaTransferId: DwollaTransferId;
    reason?: string;
}

export { Request as BazaNcDividendDwollaClientNotificationsServiceRequest };

/**
 * Notification service for Dividends sent by Dwolla
 * Do not use this service directly - use BazaNcDividendDwollaClientNotificationsQueueService instead
 * @see BazaNcDividendDwollaClientNotificationsQueueService
 */
@Injectable()
export class BazaNcDividendDwollaClientNotificationsService {
    constructor(
        private readonly i18n: I18nApiService,
        private readonly mail: MailService,
        private readonly registry: BazaRegistryService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly dividendRepository: BazaNcDividendRepository,
        private readonly dividendTransactionMapper: BazaNcDividendTransactionMapper,
        private readonly dividendTransactionEntryRepository: BazaNcDividendTransactionEntryRepository,
        private readonly dividendTransactionEntryMapper: BazaNcDividendTransactionEntryMapper,
    ) {}

    /**
     * Returns true if Individual (per-dividend) notifications enabled
     * @see BazaNcDividendDwollaClientNotificationsService.transferInitiatedNotification
     * @see BazaNcDividendDwollaClientNotificationsService.transferCancelledNotification
     * @see BazaNcDividendDwollaClientNotificationsService.transferCompletedNotification
     * @see BazaNcDividendDwollaClientNotificationsService.transferFailedNotification
     */
    get areIndividualNotificationsEnabled(): boolean {
        return this.registry.getValue('bazaNc.individualModeDividendNotifications');
    }

    /**
     * Returns true if Bulk notifications enabled
     */
    get isClientReportEnabled(): boolean {
        return this.registry.getValue('bazaNc.clientReportDividendNotifications');
    }

    /**
     * Sends Bulk Report to Client about Dividend Transaction results
     * @param request
     */
    async sendClientReportAboutDividendTransaction(request: { dividendTransaction: BazaNcDividendTransactionEntity }): Promise<void> {
        const transaction = await this.dividendTransactionMapper.entityToDTO(request.dividendTransaction);
        const entries = (
            await this.dividendTransactionEntryMapper.entitiesToDTOs(
                await this.dividendTransactionEntryRepository.findByDividendTransaction(request.dividendTransaction),
            )
        ).map((next) => ({
            ...next,
            offeringTitle: next.offeringTitle || 'N/A',
            failureReason: next.failureReason?.reason || '–',
        }));

        const isSuccess = entries.every((next) => next.status === BazaNcDividendPaymentStatus.Processed);

        await this.mail.send({
            name: BazaNcMail.BazaNcDividendDwollaReport,
            subject: replaceTags('baza-nc.mail.clientNotifications.dwolla.report.subject', {
                clientName: getBazaClientName(),
            }),
            to: [this.registry.getValue('adminEmail') as BazaRegistryNodeEmailRecipientValue],
            variables: () => ({
                transaction,
                entries,
                message: this.i18n.get(
                    isSuccess
                        ? 'baza-nc.mail.clientNotifications.dwolla.report.success'
                        : 'baza-nc.mail.clientNotifications.dwolla.report.failed',
                ),
            }),
        });
    }

    /**
     * Sends "Transfer Initiated" notification to CMS Admin ("Admin Email" in Registry)
     * @param request
     */
    async transferInitiatedNotification(request: Request): Promise<void> {
        const amount = convertFromCents(request.amountCents).toFixed(2);
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(request.investorAccountId);

        await this.mail.send({
            name: BazaNcMail.BazaNcDividendDwollaTransferInitiated,
            uniqueId: `baza_nc_dividend_dwolla_client_notifications_initiated_${request.dwollaTransferId}`,
            subject: 'baza-nc.mail.clientNotifications.dwolla.individual.transferInitiated.subject',
            to: [this.registry.getValue('adminEmail') as BazaRegistryNodeEmailRecipientValue],
            variables: () => ({
                amount,
                customer: investorAccount.user.fullName,
                customerDwollaId: investorAccount.dwollaCustomerId,
                dwollaTransferId: request.dwollaTransferId,
            }),
        });
    }

    /**
     * Sends "Transfer Cancelled" notification to CMS Admin ("Admin Email" in Registry)
     * @param request
     */
    async transferCancelledNotification(request: Request): Promise<void> {
        const amount = convertFromCents(request.amountCents).toFixed(2);
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(request.investorAccountId);

        await this.mail.send({
            name: BazaNcMail.BazaNcDividendDwollaTransferCanceled,
            subject: 'baza-nc.mail.clientNotifications.dwolla.individual.transferCancelled.subject',
            to: [this.registry.getValue('adminEmail') as BazaRegistryNodeEmailRecipientValue],
            variables: () => ({
                amount,
                customer: investorAccount.user.fullName,
                customerDwollaId: investorAccount.dwollaCustomerId,
                dwollaTransferId: request.dwollaTransferId,
            }),
        });
    }

    /**
     * Sends "Transfer Completed" notification to CMS Admin ("Admin Email" in Registry)
     * @param request
     */
    async transferCompletedNotification(request: Request): Promise<void> {
        const amount = convertFromCents(request.amountCents).toFixed(2);
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(request.investorAccountId);

        await this.mail.send({
            name: BazaNcMail.BazaNcDividendDwollaTransferCompleted,
            uniqueId: `baza_nc_dividend_dwolla_client_notifications_completed_${request.dwollaTransferId}`,
            subject: 'baza-nc.mail.clientNotifications.dwolla.individual.transferCompleted.subject',
            to: [this.registry.getValue('adminEmail') as BazaRegistryNodeEmailRecipientValue],
            variables: () => ({
                amount,
                customer: investorAccount.user.fullName,
                customerDwollaId: investorAccount.dwollaCustomerId,
                dwollaTransferId: request.dwollaTransferId,
            }),
        });
    }

    /**
     * Sends "Transfer Failed" notification to CMS Admin ("Admin Email" in Registry)
     * @param request
     */
    async transferFailedNotification(request: Request): Promise<void> {
        const amount = convertFromCents(request.amountCents).toFixed(2);
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(request.investorAccountId);

        await this.mail.send({
            name: BazaNcMail.BazaNcDividendDwollaTransferFailed,
            subject: 'baza-nc.mail.clientNotifications.dwolla.individual.transferFailed.subject',
            to: [this.registry.getValue('adminEmail') as BazaRegistryNodeEmailRecipientValue],
            variables: () => ({
                amount,
                customer: investorAccount.user.fullName,
                customerDwollaId: investorAccount.dwollaCustomerId,
                dwollaTransferId: request.dwollaTransferId,
                reason: request.reason,
            }),
        });
    }
}
