import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '@scaliolabs/baza-dwolla-api';
import { DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcDividendRepository, BazaNcDividendTransactionEntryRepository } from '../../../../../../typeorm';
import { BazaNcDividendPaymentHistoryAction, BazaNcDividendPaymentStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendProcessingService } from '../../../../services/baza-nc-dividend-processing.service';
import { BazaNcDividendDwollaClientNotificationsQueueService } from '../baza-nc-dividend-dwolla-client-notifications-queue.service';
import { BazaNcDividendDwollaClientNotificationsService } from '../baza-nc-dividend-dwolla-client-notifications.service';
import { BazaNcDividendDwollaProcessingStrategy } from '../baza-nc-dividend-dwolla-processing.strategy';
import { BazaRegistryService, cqrs } from '@scaliolabs/baza-core-api';
import { BazaNcDividendSuccessfulPaymentEvent } from '../../../../events/baza-nc-dividend-successful-payment.event';

/**
 * Event handler for Dwolla transfer_completed, customer_bank_transfer_completed events
 * The event handler affects Dividend Transaction Entries created with Dwolla type
 */
@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class TransferCompletedEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly registry: BazaRegistryService,
        private readonly service: BazaNcDividendProcessingService,
        private readonly dividendRepository: BazaNcDividendRepository,
        private readonly dividendEntriesRepository: BazaNcDividendTransactionEntryRepository,
        private readonly dwollaStrategy: BazaNcDividendDwollaProcessingStrategy,
        private readonly clientNotificationsQueue: BazaNcDividendDwollaClientNotificationsQueueService,
        private readonly clientNotifications: BazaNcDividendDwollaClientNotificationsService,
    ) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(TransferCompletedEventHandler.name, async () => {
            if (
                [DwollaEventTopic.transfer_completed, DwollaEventTopic.customer_bank_transfer_completed].includes(
                    event.payload.responseBody.topic,
                )
            ) {
                const transferId = event.payload.responseBody.resourceId;

                const dividend = await this.dividendRepository.findByDwollaTransferId(transferId);
                const dividendEntry = await this.dividendEntriesRepository.findByDwollaTransferId(transferId);

                if (dividend) {
                    const previousStatus = dividend.status;
                    const newStatus = BazaNcDividendPaymentStatus.Processed;

                    dividend.status = newStatus;
                    dividend.history.push({
                        date: event.payload.responseBody.created,
                        status: newStatus,
                        action: {
                            type: BazaNcDividendPaymentHistoryAction.DwollaTransferStatusUpdated,
                            dwollaTransferId: dividend.dwollaTransferId,
                            previousStatus,
                            newStatus,
                        },
                    });

                    await this.dividendRepository.save([dividend]);

                    if (this.registry.getValue('bazaNc.enableDividendsSuccessfulEmails')) {
                        this.eventBus.publish(
                            new BazaNcDividendSuccessfulPaymentEvent({
                                dividends: [dividend],
                            }),
                        );
                    }
                }

                if (dividendEntry) {
                    dividendEntry.status = BazaNcDividendPaymentStatus.Processed;
                    dividendEntry.dateUpdatedAt = new Date();
                    dividendEntry.dateProcessedAt = new Date();

                    await this.dwollaStrategy.resetFailureReason(dividendEntry);
                    await this.dividendEntriesRepository.save([dividendEntry]);

                    await this.service.updateDividendTransaction({
                        dividendTransactionUlid: dividendEntry.dividendTransaction.ulid,
                        shouldBeMarkedAsStarted: false,
                    });

                    if (this.clientNotifications.areIndividualNotificationsEnabled) {
                        await this.clientNotificationsQueue.transferCompletedNotification({
                            investorAccountId: dividendEntry.investorAccount.id,
                            amountCents: dividendEntry.amountCents,
                            dwollaTransferId: dividendEntry.dwollaTransferId,
                        });
                    }

                    await this.clientNotificationsQueue.touchDividendTransactionReport(dividendEntry);
                }
            }
        });
    }
}
