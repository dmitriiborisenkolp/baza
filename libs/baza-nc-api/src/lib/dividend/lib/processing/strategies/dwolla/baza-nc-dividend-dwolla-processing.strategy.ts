import { Injectable } from '@nestjs/common';
import {
    BazaNcDividendProcessApiCallbacks,
    BazaNcDividendProcessApiRequest,
    BazaNcDividendProcessApiResponse,
    BazaNcDividendProcessingStrategy,
    BazaNcDividendReprocessApiRequest,
    BazaNcDividendReprocessApiResponse,
    BazaNcDividendSyncApiRequest,
    BazaNcDividendSyncApiResponse,
} from '../../baza-nc-dividend-processing-strategy';
import {
    bazaNcDividendMapDwollaTransferToPaymentStatuses,
    BazaNcDividendPaymentSource,
    BazaNcDividendPaymentStatus,
    convertFromCents,
    convertToCents,
} from '@scaliolabs/baza-nc-shared';
import {
    BazaDwollaMasterAccountService,
    DwollaConfigurationNestjsService,
    DwollaCustomersApiNestjsService,
    DwollaTransfersApiNestjsService,
    DwollaWebhookSandboxSimulationApiNestjsService,
} from '@scaliolabs/baza-dwolla-api';
import * as _ from 'lodash';
import {
    BazaDwollaErrorCodes,
    bazaDwollaErrorCodesEnI18n,
    DwollaCurrency,
    DwollaCustomerId,
    DwollaFailureCodes,
    dwollaFailureCodesI18n,
    DwollaFundingSource,
    DwollaTransferId,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaNcDividendDwollaInvalidCurrencyException } from '../../../exceptions/baza-nc-dividend-dwolla-invalid-currency.exception';
import { BazaNcDividendDwollaInsufficientFundsException } from '../../../exceptions/baza-nc-dividend-dwolla-insufficient-funds.exception';
import {
    BazaNcDividendEntity,
    BazaNcDividendRepository,
    BazaNcDividendTransactionEntryEntity,
    BazaNcDividendTransactionEntryRepository,
    BazaNcDividendTransactionRepository,
    BazaNcInvestorAccountRepository,
} from '../../../../../typeorm';
import { BazaNcDwollaCustomerService } from '../../../../../dwolla';
import { BazaAccountRepository, BazaAppException, BazaLogger, BazaRegistryService, EnvService } from '@scaliolabs/baza-core-api';
import {
    BAZA_NC_DIVIDEND_E2E_CONSTANT_AMOUNTS,
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_CANCEL_TRANSACTION,
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_FAIL_TRANSACTION,
    BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_PROCESS_TRANSACTION,
} from '../../../integration-tests/constants/baza-nc-dividend-e2e.constants';
import { BazaNcDividendDwollaClientNotificationsQueueService } from './baza-nc-dividend-dwolla-client-notifications-queue.service';

/**
 * Dividend Transaction Processing strategy for Dwolla
 * The service is used for all Dividend Transaction Entries with BazaNcDividendPaymentSource.Dwolla type
 * @see BazaNcDividendPaymentSource
 * @see BazaNcDividendPaymentSource.Dwolla
 * @see BazaNcDividendProcessingStrategy
 */
@Injectable()
export class BazaNcDividendDwollaProcessingStrategy implements BazaNcDividendProcessingStrategy {
    constructor(
        private readonly env: EnvService,
        private readonly logger: BazaLogger,
        private readonly registry: BazaRegistryService,
        private readonly ncDwollaCustomerService: BazaNcDwollaCustomerService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly dividendsRepository: BazaNcDividendRepository,
        private readonly dividendTransactionRepository: BazaNcDividendTransactionRepository,
        private readonly dividendTransactionEntryRepository: BazaNcDividendTransactionEntryRepository,
        private readonly dwollaConfig: DwollaConfigurationNestjsService,
        private readonly dwollaMasterAccount: BazaDwollaMasterAccountService,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
        private readonly dwollaCustomerService: DwollaCustomersApiNestjsService,
        private readonly dwollaWebhookSandboxSimulation: DwollaWebhookSandboxSimulationApiNestjsService,
        private readonly clientNotifications: BazaNcDividendDwollaClientNotificationsQueueService,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    get source(): BazaNcDividendPaymentSource {
        return BazaNcDividendPaymentSource.Dwolla;
    }

    /**
     * Process Dwolla Dividends
     * @param request
     * @param callbacks
     */
    async process(
        request: BazaNcDividendProcessApiRequest,
        callbacks: BazaNcDividendProcessApiCallbacks,
        correlationId?: string,
    ): Promise<BazaNcDividendProcessApiResponse> {
        this.logger.info(`${this.constructor.name}.${this.process.name}`, {
            correlationId,
            request,
        });

        if (request.dividendEntries.length === 0) {
            return {
                failedPayments: [],
                successfulPayments: [],
            };
        }

        const failedPayments: Array<BazaNcDividendTransactionEntryEntity> = [];

        let processed = 0;

        for (const target of request.dividendEntries) {
            try {
                const dwollaCustomerId = await this.ncDwollaCustomerService.getDwollaCustomerId(target.investorAccount.id, correlationId);
                const dwollaCustomerBalanceFundingSource = await this.ncDwollaCustomerService.getDwollaCustomerBalanceFundingSource(
                    target.investorAccount.id,
                    correlationId,
                );

                const masterAccountBalanceFundingSource = await this.getMasterAccountBalanceFundingSource(request, callbacks);

                const { dwollaTransferId } = await this.dwollaTransferApi.initiateTransfer({
                    _links: {
                        source: {
                            href: masterAccountBalanceFundingSource._links.self.href,
                        },
                        destination: {
                            href: dwollaCustomerBalanceFundingSource._links.self.href,
                        },
                    },
                    amount: {
                        currency: DwollaCurrency.USD,
                        value: convertFromCents(target.amountCents).toFixed(2),
                    },
                });

                if (this.env.isTestEnvironment && BAZA_NC_DIVIDEND_E2E_CONSTANT_AMOUNTS.includes(target.amountCents)) {
                    await this.e2eFactoryDividend(target, dwollaCustomerId, dwollaTransferId);
                } else {
                    target.status = BazaNcDividendPaymentStatus.Pending;
                    target.dwollaTransferId = dwollaTransferId;
                    target.failureReason = null;
                    target.dateUpdatedAt = new Date();
                    target.result = await this.factoryDividend(target, dwollaCustomerId, dwollaTransferId);

                    await this.dividendTransactionEntryRepository.save([target]);

                    await callbacks.updateDividendTransaction({ shouldBeMarkedAsStarted: false });
                }

                processed++;
            } catch (error) {
                this.logger.error(error);

                if (!!error && typeof error === 'object' && error instanceof BazaAppException) {
                    target.status = BazaNcDividendPaymentStatus.Failed;
                    target.dwollaTransferId = null;
                    target.dateUpdatedAt = new Date();
                    target.failureReason = {
                        code: error.errorCode,
                        reason: error.message,
                    };

                    await this.dividendTransactionEntryRepository.save([target]);

                    await callbacks.updateDividendTransaction();

                    failedPayments.push(target);
                } else {
                    await callbacks.failDividendTransaction();

                    throw error;
                }
            }
        }

        if (processed > 0 && this.dwollaConfig.configuration.isSandbox && this.registry.getValue('bazaDwolla.enableWebhookSimulations')) {
            await this.dwollaWebhookSandboxSimulation.sandboxWebhookSimulations();
        }

        await callbacks.updateDividendTransaction({ shouldBeMarkedAsStarted: false });

        return {
            failedPayments,
            successfulPayments: [],
        };
    }

    /**
     * Re-process Dwolla Dividends which are failed or cancelled
     * @param request
     * @param callbacks
     */
    async reprocess(
        request: BazaNcDividendReprocessApiRequest,
        callbacks: BazaNcDividendProcessApiCallbacks,
    ): Promise<BazaNcDividendReprocessApiResponse> {
        if (request.dividendEntries.length === 0) {
            return {
                failedPayments: [],
                successfulPayments: [],
            };
        }

        const failedPayments: Array<BazaNcDividendTransactionEntryEntity> = [];

        let processed = 0;

        for (const target of request.dividendEntries) {
            try {
                const dwollaCustomerId = await this.ncDwollaCustomerService.getDwollaCustomerId(target.investorAccount.id);
                const dwollaCustomerBalanceFundingSource = await this.ncDwollaCustomerService.getDwollaCustomerBalanceFundingSource(
                    target.investorAccount.id,
                );

                const masterAccountBalanceFundingSource = await this.getMasterAccountBalanceFundingSource(request, callbacks);

                const oldDwollaTransferId = target.dwollaTransferId;
                const response = await this.dwollaTransferApi.initiateTransfer({
                    _links: {
                        source: {
                            href: masterAccountBalanceFundingSource._links.self.href,
                        },
                        destination: {
                            href: dwollaCustomerBalanceFundingSource._links.self.href,
                        },
                    },
                    amount: {
                        currency: DwollaCurrency.USD,
                        value: convertFromCents(target.amountCents).toFixed(2),
                    },
                });

                const newDwollaTransferId = response.dwollaTransferId;

                target.status = BazaNcDividendPaymentStatus.Pending;
                target.dwollaTransferId = newDwollaTransferId;
                target.dateUpdatedAt = new Date();
                target.failureReason = null;

                await this.dividendTransactionEntryRepository.save([target]);

                await this.factoryDividend(target, dwollaCustomerId, oldDwollaTransferId, newDwollaTransferId);

                await callbacks.updateDividendTransaction();

                processed++;
            } catch (error) {
                this.logger.error(error);

                if (!!error && typeof error === 'object' && error instanceof BazaAppException) {
                    target.status = BazaNcDividendPaymentStatus.Failed;
                    target.dwollaTransferId = null;
                    target.dateUpdatedAt = new Date();
                    target.failureReason = {
                        code: error.errorCode,
                        reason: error.message,
                    };

                    await this.dividendTransactionEntryRepository.save([target]);

                    await callbacks.updateDividendTransaction();

                    failedPayments.push(target);
                } else {
                    await callbacks.failDividendTransaction();

                    throw error;
                }
            }
        }

        if (processed > 0 && this.dwollaConfig.configuration.isSandbox && this.registry.getValue('bazaDwolla.enableWebhookSimulations')) {
            await this.dwollaWebhookSandboxSimulation.sandboxWebhookSimulations();
        }

        return {
            failedPayments,
            successfulPayments: [],
        };
    }

    /**
     * Synchronize statuses for all dividend entries & dividends
     * @param request
     */
    async sync(request: BazaNcDividendSyncApiRequest): Promise<BazaNcDividendSyncApiResponse> {
        const result: BazaNcDividendSyncApiResponse = {
            pending: [],
            processed: [],
            failed: [],
            cancelled: [],
            unknown: [],
            missing: [],
        };

        for (const entry of request.dividendEntries) {
            if (!entry.dwollaTransferId) {
                result.unknown.push(entry);

                continue;
            }

            try {
                const response = await this.dwollaTransferApi.getTransfer(entry.dwollaTransferId);
                const status = bazaNcDividendMapDwollaTransferToPaymentStatuses.find((next) => next.dwolla === response.status)?.status;

                if (!status) {
                    result.unknown.push(entry);

                    continue;
                }

                if (entry.status !== status) {
                    switch (status) {
                        default: {
                            result.unknown.push(entry);

                            break;
                        }

                        case BazaNcDividendPaymentStatus.Pending: {
                            result.pending.push(entry);

                            break;
                        }
                        case BazaNcDividendPaymentStatus.Processed: {
                            result.processed.push(entry);

                            break;
                        }

                        case BazaNcDividendPaymentStatus.Failed: {
                            result.failed.push(entry);

                            break;
                        }

                        case BazaNcDividendPaymentStatus.Cancelled: {
                            result.cancelled.push(entry);

                            break;
                        }
                    }
                }
            } catch (err) {
                const isMissing =
                    err instanceof BazaAppException &&
                    err.errorCode === BazaDwollaErrorCodes.BazaDwollaApiError &&
                    err.message === 'The requested resource was not found.';

                if (isMissing) {
                    result.missing.push(entry);
                } else {
                    result.unknown.push(entry);
                }
            }
        }

        return result;
    }

    /**
     * Retrieves Failure reason from Dwolla API
     * @param target
     * @private
     */
    async retrieveFailureReason(target: BazaNcDividendTransactionEntryEntity): Promise<void> {
        if (!target.dwollaTransferId) {
            this.logger.warn(`No transfer Id available for Dividend Transaction "${target.ulid}"`);

            target.failureReason = {
                code: BazaDwollaErrorCodes.BazaDwollaNoCustomerAccount,
                reason: bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaNoCustomerAccount],
            };
            target.dateUpdatedAt = new Date();

            await this.dividendTransactionEntryRepository.save([target]);

            return;
        }

        try {
            const response = await this.dwollaTransferApi.getFailureReasonForTransfer(target.dwollaTransferId);
            const found = dwollaFailureCodesI18n.find((next) => next.code === response.code);

            target.failureReason = {
                code: response.code,
                reason: found?.reason,
                details: found?.details,
            };
            target.dateUpdatedAt = new Date();

            await this.dividendTransactionEntryRepository.save([target]);
        } catch (err) {
            this.logger.warn(`Failed to fetch failure reason for Dividend Transaction "${target.ulid}"`);

            if (!target.failureReason) {
                target.failureReason = {
                    code: BazaDwollaErrorCodes.BazaDwollaUnknownPaymentError,
                    reason: bazaDwollaErrorCodesEnI18n[BazaDwollaErrorCodes.BazaDwollaUnknownPaymentError],
                };
            }
        }
    }

    /**
     * Clean Failure Reason from Dividend Transaction Entry
     * @param target
     * @private
     */
    async resetFailureReason(target: BazaNcDividendTransactionEntryEntity): Promise<void> {
        target.failureReason = null;
        target.dateUpdatedAt = new Date();

        await this.dividendTransactionEntryRepository.save([target]);
    }

    /**
     * Returns Master Account Balance Funding Source
     * @param request
     * @param callbacks
     * @private
     */
    private async getMasterAccountBalanceFundingSource(
        request: BazaNcDividendProcessApiRequest | BazaNcDividendReprocessApiRequest,
        callbacks: BazaNcDividendProcessApiCallbacks,
    ): Promise<DwollaFundingSource> {
        const masterAccountBalance = await this.dwollaMasterAccount.balance();
        const masterAccountBalanceFundingSource = await this.dwollaMasterAccount.balanceFundingSource();

        const totalSumOfDividends = _.sum(request.dividendEntries.map((next) => next.amountCents));

        if (masterAccountBalance.currency !== DwollaCurrency.USD) {
            await callbacks.abortDividendTransaction();

            throw new BazaNcDividendDwollaInvalidCurrencyException();
        }

        if (totalSumOfDividends > convertToCents(parseFloat(masterAccountBalance.value))) {
            await callbacks.abortDividendTransaction();

            await Promise.all(
                request.dividendEntries.map((dividendEntry) => {
                    this.clientNotifications.transferFailedNotification({
                        investorAccountId: dividendEntry.investorAccount.id,
                        amountCents: dividendEntry.amountCents,
                        dwollaTransferId: dividendEntry.dwollaTransferId,
                        reason: 'Insufficient Funds',
                    });
                }),
            );

            throw new BazaNcDividendDwollaInsufficientFundsException();
        }

        return masterAccountBalanceFundingSource;
    }

    /**
     * Creates and returns a Dividend entry for DividendTransactionEntry
     * @param entry
     * @param dwollaCustomerId
     * @param dwollaTransferId
     * @param newDwollaTransferId
     * @private
     */
    private async factoryDividend(
        entry: BazaNcDividendTransactionEntryEntity,
        dwollaCustomerId: DwollaCustomerId,
        dwollaTransferId: DwollaTransferId,
        newDwollaTransferId?: DwollaTransferId,
    ): Promise<BazaNcDividendEntity> {
        const existing = await this.dividendsRepository.findByDwollaTransferId(dwollaTransferId);
        const entity = existing || new BazaNcDividendEntity();

        if (!entity.history) {
            entity.history = [];
        }

        entity.offering = entry.offering;
        entity.investorAccount = entry.investorAccount;
        entity.source = entry.source;
        entity.date = entry.date;
        entity.amountCents = entry.amountCents;
        entity.status = entry.status;
        entity.dwollaTransferId = newDwollaTransferId || entry.dwollaTransferId;

        await this.dividendsRepository.save([entity]);

        return entity;
    }

    /**
     * Creates and returns a Dividend entry for DividendTransactionEntry (e2e)
     * @param target
     * @param dwollaCustomerId
     * @param dwollaTransferId
     * @param newDwollaTransferId
     * @private
     */
    private async e2eFactoryDividend(
        target: BazaNcDividendTransactionEntryEntity,
        dwollaCustomerId: DwollaCustomerId,
        dwollaTransferId: DwollaTransferId,
        newDwollaTransferId?: DwollaTransferId,
    ): Promise<void> {
        if (target.amountCents === BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_FAIL_TRANSACTION) {
            target.status = BazaNcDividendPaymentStatus.Failed;
            target.dateUpdatedAt = new Date();
            target.dwollaTransferId = dwollaTransferId;
            target.failureReason = { code: DwollaFailureCodes.R01, reason: 'E2E Failed', details: 'E2E Helpers' };
            target.result = await this.factoryDividend(target, dwollaTransferId, dwollaCustomerId, newDwollaTransferId);

            await this.dividendTransactionEntryRepository.save([target]);
        } else if (target.amountCents === BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_CANCEL_TRANSACTION) {
            target.status = BazaNcDividendPaymentStatus.Cancelled;
            target.dateUpdatedAt = new Date();
            target.dwollaTransferId = dwollaTransferId;
            target.failureReason = null;
            target.result = await this.factoryDividend(target, dwollaTransferId, dwollaCustomerId, newDwollaTransferId);

            await this.dividendTransactionEntryRepository.save([target]);
        } else if (target.amountCents === BAZA_NC_DIVIDEND_E2E_CONSTANTS_AMOUNT_TO_PROCESS_TRANSACTION) {
            target.status = BazaNcDividendPaymentStatus.Processed;
            target.failureReason = null;
            target.dateUpdatedAt = new Date();
            target.dwollaTransferId = dwollaTransferId;
            target.dateProcessedAt = new Date();
            target.result = await this.factoryDividend(target, dwollaTransferId, dwollaCustomerId, newDwollaTransferId);

            await this.dividendTransactionEntryRepository.save([target]);
        }
    }
}
