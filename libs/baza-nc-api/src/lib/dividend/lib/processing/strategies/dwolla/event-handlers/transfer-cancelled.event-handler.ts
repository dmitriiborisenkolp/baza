import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '@scaliolabs/baza-dwolla-api';
import { DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcDividendRepository, BazaNcDividendTransactionEntryRepository } from '../../../../../../typeorm';
import { BazaNcDividendPaymentHistoryAction, BazaNcDividendPaymentStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendProcessingService } from '../../../../services/baza-nc-dividend-processing.service';
import { BazaNcDividendDwollaClientNotificationsQueueService } from '../baza-nc-dividend-dwolla-client-notifications-queue.service';
import { BazaNcDividendDwollaClientNotificationsService } from '../baza-nc-dividend-dwolla-client-notifications.service';
import { cqrs } from '@scaliolabs/baza-core-api';

/**
 * Event handler for Dwolla transfer_cancelled, customer_bank_transfer_cancelled events
 * The event handler affects Dividend Transaction Entries created with Dwolla type
 */
@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class TransferCancelledEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(
        private readonly service: BazaNcDividendProcessingService,
        private readonly dividendRepository: BazaNcDividendRepository,
        private readonly dividendEntriesRepository: BazaNcDividendTransactionEntryRepository,
        private readonly clientNotificationsQueue: BazaNcDividendDwollaClientNotificationsQueueService,
        private readonly clientNotifications: BazaNcDividendDwollaClientNotificationsService,
    ) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(TransferCancelledEventHandler.name, async () => {
            if (
                [DwollaEventTopic.transfer_cancelled, DwollaEventTopic.customer_bank_transfer_cancelled].includes(
                    event.payload.responseBody.topic,
                )
            ) {
                const transferId = event.payload.responseBody.resourceId;

                const dividend = await this.dividendRepository.findByDwollaTransferId(transferId);
                const dividendEntry = await this.dividendEntriesRepository.findByDwollaTransferId(transferId);

                if (dividend) {
                    const previousStatus = dividend.status;
                    const newStatus = BazaNcDividendPaymentStatus.Cancelled;

                    dividend.status = newStatus;
                    dividend.history.push({
                        date: event.payload.responseBody.created,
                        status: newStatus,
                        action: {
                            type: BazaNcDividendPaymentHistoryAction.DwollaTransferStatusUpdated,
                            dwollaTransferId: dividend.dwollaTransferId,
                            previousStatus,
                            newStatus,
                        },
                    });

                    await this.dividendRepository.save([dividend]);
                }

                if (dividendEntry) {
                    dividendEntry.status = BazaNcDividendPaymentStatus.Cancelled;
                    dividendEntry.failureReason = null;
                    dividendEntry.dateUpdatedAt = new Date();

                    await this.dividendEntriesRepository.save([dividendEntry]);

                    await this.service.updateDividendTransaction({
                        dividendTransactionUlid: dividendEntry.dividendTransaction.ulid,
                        shouldBeMarkedAsStarted: false,
                    });

                    if (this.clientNotifications.areIndividualNotificationsEnabled) {
                        await this.clientNotificationsQueue.transferCancelledNotification({
                            investorAccountId: dividendEntry.investorAccount.id,
                            amountCents: dividendEntry.amountCents,
                            dwollaTransferId: dividendEntry.dwollaTransferId,
                        });
                    }

                    await this.clientNotificationsQueue.touchDividendTransactionReport(dividendEntry);
                }
            }
        });
    }
}
