import { Injectable } from '@nestjs/common';
import {
    BazaNcDividendProcessApiCallbacks,
    BazaNcDividendProcessApiRequest,
    BazaNcDividendProcessApiResponse,
    BazaNcDividendProcessingStrategy,
} from '../../baza-nc-dividend-processing-strategy';
import { BazaNcDividendPaymentHistoryAction, BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus } from '@scaliolabs/baza-nc-shared';
import {
    BazaNcDividendEntity,
    BazaNcDividendRepository,
    BazaNcDividendTransactionEntryEntity,
    BazaNcDividendTransactionEntryRepository,
} from '../../../../../typeorm';

/**
 * Dividend Transaction Processing strategy for NC
 * The service is used for all Dividend Transaction Entries with BazaNcDividendPaymentSource.NC type
 * The service is not actually doing any external calls - it just sets Dividend Entries / Dividends as "Processed" immediately
 * @see BazaNcDividendPaymentSource
 * @see BazaNcDividendPaymentSource.NC
 * @see BazaNcDividendProcessingStrategy
 */
@Injectable()
export class BazaNcDividendNcProcessingStrategy implements BazaNcDividendProcessingStrategy {
    constructor(
        private readonly dividendsRepository: BazaNcDividendRepository,
        private readonly entriesRepository: BazaNcDividendTransactionEntryRepository,
    ) {}

    get source(): BazaNcDividendPaymentSource {
        return BazaNcDividendPaymentSource.NC;
    }

    /**
     * Process Dividend Transaction Entries with BazaNcDividendPaymentSource.NC type
     * @param request
     * @param callbacks
     */
    async process(
        request: BazaNcDividendProcessApiRequest,
        callbacks: BazaNcDividendProcessApiCallbacks,
    ): Promise<BazaNcDividendProcessApiResponse> {
        const successfulPayments: Array<BazaNcDividendEntity> = [];
        const failedPayments: Array<BazaNcDividendTransactionEntryEntity> = [];

        for (const target of request.dividendEntries) {
            const result = await this.processEntry(target, callbacks);

            if (result && result.status === BazaNcDividendPaymentStatus.Processed) {
                successfulPayments.push(result);
            }
        }

        return {
            successfulPayments,
            failedPayments,
        };
    }

    /**
     * Process Dividend Transaction Entry with BazaNcDividendPaymentSource.NC type
     * @param target
     * @param callbacks
     * @private
     */
    private async processEntry(
        target: BazaNcDividendTransactionEntryEntity,
        callbacks: BazaNcDividendProcessApiCallbacks,
    ): Promise<BazaNcDividendEntity> {
        const dividend = await this.factoryDividend(target);

        await this.markEntryAsProcessed(target, dividend);

        await callbacks.updateDividendTransaction();

        return dividend;
    }

    /**
     * Creates a Dividend Entry
     * @param entry
     * @private
     */
    private async factoryDividend(entry: BazaNcDividendTransactionEntryEntity): Promise<BazaNcDividendEntity> {
        const entity = new BazaNcDividendEntity();

        entity.offering = entry.offering;
        entity.investorAccount = entry.investorAccount;
        entity.source = entry.source;
        entity.date = entry.date;
        entity.amountCents = entry.amountCents;
        entity.status = BazaNcDividendPaymentStatus.Processed;
        entity.history = [
            {
                date: entity.date.toISOString(),
                status: BazaNcDividendPaymentStatus.Processed,
                action: {
                    type: BazaNcDividendPaymentHistoryAction.PaidWithNC,
                },
            },
        ];

        await this.dividendsRepository.save([entity]);

        return entity;
    }

    /**
     * Marks Dividend Transaction Entry and Dividend as processed
     * @param entry
     * @param dividend
     * @private
     */
    private async markEntryAsProcessed(entry: BazaNcDividendTransactionEntryEntity, dividend: BazaNcDividendEntity): Promise<void> {
        entry.dateUpdatedAt = new Date();
        entry.dateProcessedAt = entry.dateUpdatedAt;
        entry.status = BazaNcDividendPaymentStatus.Processed;
        entry.result = dividend;

        await this.entriesRepository.save([entry]);
    }
}
