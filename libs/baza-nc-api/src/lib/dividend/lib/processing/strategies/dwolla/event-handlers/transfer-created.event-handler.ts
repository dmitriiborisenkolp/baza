import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '@scaliolabs/baza-dwolla-api';
import { DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcDividendTransactionEntryRepository } from '../../../../../../typeorm';
import { BazaNcDividendProcessingService } from '../../../../services/baza-nc-dividend-processing.service';
import { BazaNcDividendDwollaClientNotificationsQueueService } from '../baza-nc-dividend-dwolla-client-notifications-queue.service';
import { BazaNcDividendDwollaClientNotificationsService } from '../baza-nc-dividend-dwolla-client-notifications.service';
import { cqrs } from '@scaliolabs/baza-core-api';

/**
 * Event handler for Dwolla transfer_created, customer_bank_transfer_created events
 * The event handler affects Dividend Transaction Entries created with Dwolla type
 */
@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class TransferCreatedEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(
        private readonly service: BazaNcDividendProcessingService,
        private readonly dividendEntriesRepository: BazaNcDividendTransactionEntryRepository,
        private readonly clientNotificationsQueue: BazaNcDividendDwollaClientNotificationsQueueService,
        private readonly clientNotifications: BazaNcDividendDwollaClientNotificationsService,
    ) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(TransferCreatedEventHandler.name, async () => {
            if (
                [DwollaEventTopic.transfer_created, DwollaEventTopic.customer_bank_transfer_created].includes(
                    event.payload.responseBody.topic,
                )
            ) {
                // Waring: DO NOT set dividend transaction status or dividend status as Pending!
                // Reason: Dwolla may sends series of events like this: transfer_created, transfer_completed, transfer_created, transfer_created (final result should be "completed")

                const transferId = event.payload.responseBody.resourceId;
                const dividendEntry = await this.dividendEntriesRepository.findByDwollaTransferId(transferId);

                if (dividendEntry) {
                    if (this.clientNotifications.areIndividualNotificationsEnabled) {
                        await this.clientNotificationsQueue.transferInitiatedNotification({
                            investorAccountId: dividendEntry.investorAccount.id,
                            amountCents: dividendEntry.amountCents,
                            dwollaTransferId: dividendEntry.dwollaTransferId,
                        });
                    }
                }
            }
        });
    }
}
