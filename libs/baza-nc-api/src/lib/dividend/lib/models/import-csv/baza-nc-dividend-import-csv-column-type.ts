/**
 * List of available column types for CSV import Dividend Transaction Entries to Dividend Transaction
 * Column types are being detected with bazaNcDividendImportCsvColumnTypeDetector helper
 * @see bazaNcDividendImportCsvColumnTypeDetector
 */
export enum BazaNcDividendImportCsvColumnType {
    NcAccountId = 'NcAccountId',
    BazaAccountId = 'BazaAccountId',
    DwollaCustomerId = 'DwollaCustomerId',
    Amount = 'Amount',
    Date = 'Date',
    NcOfferingId = 'NcOfferingId',
}

/**
 * List of required columns for CSV import Dividend Transaction Entries to Dividend Transaction
 * @see BazaNcDividendImportCsvColumnType
 */
export const bazaNcDividendImportCsvRequiredColumns: Array<BazaNcDividendImportCsvColumnType> = [
    BazaNcDividendImportCsvColumnType.Amount,
    BazaNcDividendImportCsvColumnType.Date,
];
