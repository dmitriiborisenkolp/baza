import { BazaNcInvestorAccountEntity } from '../../../../typeorm';
import { OfferingId } from '@scaliolabs/baza-nc-shared';

/**
 * (CSV import Dividend Transaction Entries to Dividend Transaction)
 * Interface used for CSV entries
 */
export interface BazaNcDividendImportCsvEntry {
    amountCents: number;
    date: Date;
    ncOfferingId: OfferingId;
    investorAccount: BazaNcInvestorAccountEntity;
}
