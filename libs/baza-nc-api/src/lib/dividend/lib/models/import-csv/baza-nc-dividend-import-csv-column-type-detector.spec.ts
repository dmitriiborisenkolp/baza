import { BazaNcDividendImportCsvColumnType } from './baza-nc-dividend-import-csv-column-type';
import { bazaNcDividendImportCsvColumnTypeDetector } from './baza-nc-dividend-import-csv-column-type-detector';

interface TestCase {
    column: BazaNcDividendImportCsvColumnType;
    input: Array<string>;
}

describe('@scaliolabs/baza-nc-api/dividend/models/import-csv/baza-nc-dividend-import-csv-column-type-detector.spec.ts', () => {
    it(`will correctly detect column ${BazaNcDividendImportCsvColumnType.NcAccountId}`, () => {
        const testCases: Array<TestCase> = [
            {
                column: BazaNcDividendImportCsvColumnType.NcAccountId,
                input: ['NC Account', 'nc account', 'north capital account', 'north account', 'North Capital ID Number'],
            },
        ];

        for (const testCase of testCases) {
            for (const inputString of testCase.input) {
                expect(bazaNcDividendImportCsvColumnTypeDetector(inputString)).toBe(testCase.column);
            }
        }
    });

    it(`will correctly detect column ${BazaNcDividendImportCsvColumnType.BazaAccountId}`, () => {
        const testCases: Array<TestCase> = [
            {
                column: BazaNcDividendImportCsvColumnType.BazaAccountId,
                input: ['account', 'Account', 'baza Account', 'Baza Account', 'BAZA ACCOUNT', 'Baza Account ID Number'],
            },
        ];

        for (const testCase of testCases) {
            for (const inputString of testCase.input) {
                expect(bazaNcDividendImportCsvColumnTypeDetector(inputString)).toBe(testCase.column);
            }
        }
    });

    it(`will correctly detect column ${BazaNcDividendImportCsvColumnType.DwollaCustomerId}`, () => {
        const testCases: Array<TestCase> = [
            {
                column: BazaNcDividendImportCsvColumnType.DwollaCustomerId,
                input: ['dwolla account', 'Dwolla Customer ID', 'dwolla customer'],
            },
        ];

        for (const testCase of testCases) {
            for (const inputString of testCase.input) {
                expect(bazaNcDividendImportCsvColumnTypeDetector(inputString)).toBe(testCase.column);
            }
        }
    });

    it(`will correctly detect column ${BazaNcDividendImportCsvColumnType.Amount}`, () => {
        const testCases: Array<TestCase> = [
            {
                column: BazaNcDividendImportCsvColumnType.Amount,
                input: ['amount', 'Amount', 'AMOUNT'],
            },
        ];

        for (const testCase of testCases) {
            for (const inputString of testCase.input) {
                expect(bazaNcDividendImportCsvColumnTypeDetector(inputString)).toBe(testCase.column);
            }
        }
    });

    it(`will correctly detect column ${BazaNcDividendImportCsvColumnType.Date}`, () => {
        const testCases: Array<TestCase> = [
            {
                column: BazaNcDividendImportCsvColumnType.Date,
                input: ['date', 'date paid', 'Date', 'Date At'],
            },
        ];

        for (const testCase of testCases) {
            for (const inputString of testCase.input) {
                expect(bazaNcDividendImportCsvColumnTypeDetector(inputString)).toBe(testCase.column);
            }
        }
    });

    it(`will correctly detect column ${BazaNcDividendImportCsvColumnType.NcOfferingId}`, () => {
        const testCases: Array<TestCase> = [
            {
                column: BazaNcDividendImportCsvColumnType.NcOfferingId,
                input: ['North Capital Offering ID', 'NC Offering ID', 'nc Offering', 'north capital offering', 'offering', 'Offering'],
            },
        ];

        for (const testCase of testCases) {
            for (const inputString of testCase.input) {
                expect(bazaNcDividendImportCsvColumnTypeDetector(inputString)).toBe(testCase.column);
            }
        }
    });
});
