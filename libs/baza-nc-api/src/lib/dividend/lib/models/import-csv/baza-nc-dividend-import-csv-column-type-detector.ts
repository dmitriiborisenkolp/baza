import { BazaNcDividendImportCsvColumnType } from './baza-nc-dividend-import-csv-column-type';
import { BazaNcDividendImportCsvUnknownColumnException } from '../../exceptions/baza-nc-dividend-import-csv-unknown-column.exception';

/**
 * Column detectors for CSV import Dividend Transaction Entries to Dividend Transaction
 * Detectors should detect column type based on contents of CSV cell
 * CSV service will use first truthy match from this list
 * @see BazaNcDividendImportCsvColumnType
 */
export const BAZA_NC_DIVIDEND_IMPORT_CSV_COLUMN_DETECTORS: Array<{
    column: BazaNcDividendImportCsvColumnType;
    match: (header: string) => boolean;
}> = [
    {
        column: BazaNcDividendImportCsvColumnType.DwollaCustomerId,
        match: (header) => {
            const containsNc = header.includes('nc') || header.includes('north');
            const containsDwolla = header.includes('dwolla');
            const containsId = header.includes('id');
            const containsAccount = header.includes('account');
            const containsCustomer = header.includes('customer');

            return (containsAccount || containsCustomer || containsId) && containsDwolla && !containsNc;
        },
    },
    {
        column: BazaNcDividendImportCsvColumnType.Amount,
        match: (header) => {
            return header.includes('amount');
        },
    },
    {
        column: BazaNcDividendImportCsvColumnType.Date,
        match: (header) => {
            return header.includes('date');
        },
    },
    {
        column: BazaNcDividendImportCsvColumnType.NcOfferingId,
        match: (header) => {
            return header.includes('offering');
        },
    },
    {
        column: BazaNcDividendImportCsvColumnType.NcAccountId,
        match: (header) => {
            const containsNc = header.includes('nc') || header.includes('north');
            const containsId = header.includes('id');
            const containsDwolla = header.includes('dwolla');
            const containsAccount = header.includes('account');
            const containsCustomer = header.includes('customer');

            return (containsAccount || containsId) && !containsCustomer && !containsDwolla && containsNc;
        },
    },
    {
        column: BazaNcDividendImportCsvColumnType.BazaAccountId,
        match: (header) => {
            const containsNc = header.includes('nc') || header.includes('north');
            const containsId = header.includes('id');
            const containsDwolla = header.includes('dwolla');
            const containsAccount = header.includes('account');
            const containsCustomer = header.includes('customer');

            return (containsAccount || containsId) && !containsCustomer && !(containsNc || containsDwolla);
        },
    },
];

/**
 * Column detector for CSV import Dividend Transaction Entries to Dividend Transaction
 * The helper is using detectors collection BAZA_NC_DIVIDEND_IMPORT_CSV_COLUMN_DETECTORS
 * @see BazaNcDividendImportCsvColumnType
 * @see BAZA_NC_DIVIDEND_IMPORT_CSV_COLUMN_DETECTORS
 * @param header
 */
export function bazaNcDividendImportCsvColumnTypeDetector(header: string): BazaNcDividendImportCsvColumnType {
    for (const detector of BAZA_NC_DIVIDEND_IMPORT_CSV_COLUMN_DETECTORS) {
        if (detector.match(header.toLowerCase())) {
            return detector.column;
        }
    }

    throw new BazaNcDividendImportCsvUnknownColumnException();
}
