import { Module, OnModuleInit } from '@nestjs/common';
import { BazaNcDividendController } from './controllers/public/baza-nc-dividend.controller';
import { BazaNcDividendService } from './services/baza-nc-dividend.service';
import { BazaNcDividendCmsController } from './controllers/cms/baza-nc-dividend-cms.controller';
import { BazaNcDividendMapper } from './mappers/baza-nc-dividend.mapper';
import { BazaNcDividendCmsMapper } from './mappers/baza-nc-dividend-cms.mapper';
import { BazaNcDividendCmsService } from './services/baza-nc-dividend-cms.service';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcDividendTransactionCmsController } from './controllers/cms/baza-nc-dividend-transaction-cms.controller';
import { BazaNcDividendTransactionMapper } from './mappers/baza-nc-dividend-transaction.mapper';
import { BazaNcDividendTransactionCmsService } from './services/baza-nc-dividend-transaction-cms.service';
import { BazaNcDividendTransactionConfirmationService } from './services/baza-nc-dividend-transaction-confirmation.service';
import { BazaNcDividendTransactionEntryMapper } from './mappers/baza-nc-dividend-transaction-entry.mapper';
import { BazaNcDividendTransactionEntryCmsService } from './services/baza-nc-dividend-transaction-entry-cms.service';
import { BazaNcDividendTransactionEntryCmsController } from './controllers/cms/baza-nc-dividend-transaction-entry-cms.controller';
import { BazaNcDividendNcProcessingStrategy } from './processing/strategies/nc/baza-nc-dividend-nc-processing.strategy';
import { BazaNcDividendDwollaProcessingStrategy } from './processing/strategies/dwolla/baza-nc-dividend-dwolla-processing.strategy';
import { BazaNcDividendProcessingService } from './services/baza-nc-dividend-processing.service';
import { BazaNcDividendImportCsvService } from './services/baza-nc-dividend-import-csv.service';
import { BazaNcDividendMailNotificationsService } from './services/baza-nc-dividend-mail-notifications.service';
import { SendUserMailNotificationAboutSuccessfulPaymentsEventHandler } from './event-handlers/send-user-mail-notification-about-successful-payments.event-handler';
import { BazaNcDividendCmsCsvMapper } from './mappers/baza-nc-dividend-cms-csv.mapper';
import { BazaNcDividendTransactionCsvMapper } from './mappers/baza-nc-dividend-transaction-csv.mapper';
import { BazaNcDividendTransactionEntryCsvMapper } from './mappers/baza-nc-dividend-transaction-entry-csv.mapper';
import { BazaDwollaApiModule, BazaDwollaMasterAccountApiModule } from '@scaliolabs/baza-dwolla-api';
import { TransferCompletedEventHandler } from './processing/strategies/dwolla/event-handlers/transfer-completed.event-handler';
import { TransferCancelledEventHandler } from './processing/strategies/dwolla/event-handlers/transfer-cancelled.event-handler';
import { TransferFailedEventHandler } from './processing/strategies/dwolla/event-handlers/transfer-failed.event-handler';
import { TransferCreatedEventHandler } from './processing/strategies/dwolla/event-handlers/transfer-created.event-handler';
import { BazaNcDividendDwollaClientNotificationsService } from './processing/strategies/dwolla/baza-nc-dividend-dwolla-client-notifications.service';
import { BazaNcDividendDwollaClientNotificationsQueueService } from './processing/strategies/dwolla/baza-nc-dividend-dwolla-client-notifications-queue.service';
import { BazaNcDwollaApiModule } from '../../dwolla';
import { BazaNcDividendActionsAccessService } from './services/baza-nc-dividend-actions-access.service';

const services = [
    BazaNcDividendService,
    BazaNcDividendMapper,
    BazaNcDividendCmsMapper,
    BazaNcDividendCmsService,
    BazaNcDividendTransactionMapper,
    BazaNcDividendTransactionCmsService,
    BazaNcDividendTransactionConfirmationService,
    BazaNcDividendTransactionEntryMapper,
    BazaNcDividendTransactionEntryCmsService,
    BazaNcDividendProcessingService,
    BazaNcDividendImportCsvService,
    BazaNcDividendMailNotificationsService,
    BazaNcDividendCmsCsvMapper,
    BazaNcDividendTransactionCsvMapper,
    BazaNcDividendTransactionEntryCsvMapper,
    BazaNcDividendDwollaClientNotificationsService,
    BazaNcDividendDwollaClientNotificationsQueueService,
    BazaNcDividendActionsAccessService,
];

const dwollaEventHandlers = [
    TransferCreatedEventHandler,
    TransferCompletedEventHandler,
    TransferCancelledEventHandler,
    TransferFailedEventHandler,
];

const eventHandlers = [SendUserMailNotificationAboutSuccessfulPaymentsEventHandler, ...dwollaEventHandlers];

const processingStrategies = [BazaNcDividendNcProcessingStrategy, BazaNcDividendDwollaProcessingStrategy];

@Module({
    providers: [...services, ...eventHandlers, ...processingStrategies],
    controllers: [
        BazaNcDividendController,
        BazaNcDividendCmsController,
        BazaNcDividendTransactionCmsController,
        BazaNcDividendTransactionEntryCmsController,
    ],
    imports: [BazaCrudApiModule, BazaDwollaApiModule, BazaNcDwollaApiModule, BazaDwollaMasterAccountApiModule],
    exports: services,
})
export class BazaNcDividendApiModule implements OnModuleInit {
    constructor(
        private readonly processingService: BazaNcDividendProcessingService,
        private readonly strategyNc: BazaNcDividendNcProcessingStrategy,
        private readonly strategyDwolla: BazaNcDividendDwollaProcessingStrategy,
    ) {}

    onModuleInit(): void {
        this.processingService.setProcessingStrategies([this.strategyNc, this.strategyDwolla]);
    }
}
