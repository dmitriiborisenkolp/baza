import { Injectable } from '@nestjs/common';
import {
    BazaNcDividendCmsCsvDto,
    BazaNcDividendCmsDeleteRequest,
    BazaNcDividendCmsDto,
    BazaNcDividendCmsExportToCsvRequest,
    BazaNcDividendCmsGetByUlidRequest,
    BazaNcDividendCmsListRequest,
    BazaNcDividendCmsListResponse,
    convertToCents,
} from '@scaliolabs/baza-nc-shared';
import { CrudCsvService, CrudService } from '@scaliolabs/baza-core-api';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import {
    BAZA_NC_DIVIDEND_RELATIONS,
    BazaNcDividendEntity,
    BazaNcDividendRepository,
    BazaNcInvestorAccountEntity,
    BazaNcInvestorAccountRepository,
    BazaNcOfferingRepository,
} from '../../../typeorm';
import { BazaNcDividendCmsMapper } from '../mappers/baza-nc-dividend-cms.mapper';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { Between, In } from 'typeorm';
import { bazaEmptyCrudListResponse, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';
import { BazaNcDividendCmsCsvMapper } from '../mappers/baza-nc-dividend-cms-csv.mapper';

/**
 * Dividend CMS Service
 * Used mostly for Dividend CMS endpoints
 */
@Injectable()
export class BazaNcDividendCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudCsv: CrudCsvService,
        private readonly mapper: BazaNcDividendCmsMapper,
        private readonly csvMapper: BazaNcDividendCmsCsvMapper,
        private readonly repository: BazaNcDividendRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
    ) {}

    /**
     * Returns list of Dividends
     * @param request
     */
    async list(request: BazaNcDividendCmsListRequest): Promise<BazaNcDividendCmsListResponse> {
        const findOptions = await this.findOptionsFactory(request);

        if (findOptions) {
            return this.crud.find<BazaNcDividendEntity, BazaNcDividendCmsDto>({
                request,
                findOptions,
                mapper: async (items) => this.mapper.entitiesToDTOs(items),
                entity: BazaNcDividendEntity,
            });
        } else {
            return bazaEmptyCrudListResponse();
        }
    }

    /**
     * Returns list of Dividends for Export To CSV
     * @param request
     */
    async listCsv(request: BazaNcDividendCmsListRequest): Promise<CrudListResponseDto<BazaNcDividendCmsCsvDto>> {
        const findOptions = await this.findOptionsFactory(request);

        if (findOptions) {
            return this.crud.find<BazaNcDividendEntity, BazaNcDividendCmsCsvDto>({
                request: request as any,
                findOptions,
                mapper: async (items) => this.csvMapper.entitiesToDTOs(items),
                entity: BazaNcDividendEntity,
            });
        } else {
            return bazaEmptyCrudListResponse();
        }
    }

    /**
     * Exports Dividends to CSV
     * @param request
     */
    async exportToCsv(request: BazaNcDividendCmsExportToCsvRequest): Promise<string> {
        const findOptions = await this.findOptionsFactory(request.listRequest);

        return this.crudCsv.exportToCsv<BazaNcDividendEntity, BazaNcDividendCmsCsvDto>({
            ...request,
            findOptions,
            entity: BazaNcDividendEntity,
            mapper: async (items) => this.csvMapper.entitiesToDTOs(items),
            delimiter: request.delimiter,
        });
    }

    /**
     * Returns Dividend by ULID
     * @param request
     */
    async getByUlid(request: BazaNcDividendCmsGetByUlidRequest): Promise<BazaNcDividendEntity> {
        return this.repository.getByUlid(request.ulid);
    }

    /**
     * Deletes Dividend
     * @param request
     */
    async delete(request: BazaNcDividendCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getByUlid(request.ulid);

        await this.repository.remove([entity]);
    }

    /**
     * Factory for TypeORM where options which are used for lists
     * @see BazaNcDividendCmsService.list
     * @see BazaNcDividendCmsService.listCsv
     * @param request
     * @private
     */
    private async findOptionsFactory(request: BazaNcDividendCmsListRequest): Promise<FindManyOptions<BazaNcDividendEntity> | false> {
        const where: Array<FindConditions<BazaNcDividendEntity>> = [];

        const findOptions: FindManyOptions<BazaNcDividendEntity> = {
            order: {
                ulid: 'DESC',
            },
            relations: BAZA_NC_DIVIDEND_RELATIONS,
            where,
        };

        if (request.investorAccountId) {
            const investorAccount = await this.investorAccountRepository.findInvestorAccountById(request.investorAccountId);

            if (investorAccount) {
                where.push({
                    investorAccount,
                });
            } else {
                return false;
            }
        }

        if (request.queryString) {
            if (request.queryString.match(/^\$?(\d+)\.$/)) {
                request.queryString = request.queryString.replace('.', '');
            }

            const hasInvestorAccountSearch = !!request.investorAccountId;
            const isNcAccountIdSearch = request.queryString.match(/^A(\d+)$/);
            const isNcPartyIdSearch = request.queryString.match(/^A(\d+)$/);
            const isNcOfferingIdSearch = request.queryString.match(/^(\d{5,})$/);
            const isAmountSearch = !isNcOfferingIdSearch && request.queryString.match(/^\$?\d+$|(?=^\$?[.\d]+$)^\$?\d+\.\d+$/);

            if (isAmountSearch) {
                if (!where.length) {
                    where.push({});
                }

                for (const whereConditions of where) {
                    whereConditions.amountCents = convertToCents(parseFloat(request.queryString.replace('$', '')));
                }
            } else if (isNcOfferingIdSearch) {
                const offerings = await this.offeringRepository.searchByOfferingId(request.queryString);

                if (offerings.length > 0) {
                    if (!where.length) {
                        where.push({});
                    }

                    for (const whereConditions of where) {
                        whereConditions.offering = In(offerings.map((next) => next.id)) as any;
                    }
                } else {
                    return false;
                }
            } else {
                if (hasInvestorAccountSearch) {
                    const investorAccounts: Array<BazaNcInvestorAccountEntity> =
                        await this.investorAccountRepository.searchByFullnameOrEmail(request.queryString);
                    const offerings = await this.offeringRepository.searchByOfferingIdOrName(request.queryString);

                    if (investorAccounts.length > 0 || offerings.length > 0) {
                        if (investorAccounts.length) {
                            where.push({
                                investorAccount: In(investorAccounts.map((next) => next.id)) as any,
                            });
                        }

                        if (offerings.length) {
                            where.push({
                                offering: In(offerings.map((next) => next.id)) as any,
                            });
                        }
                    } else {
                        return false;
                    }
                } else {
                    const investorAccounts: Array<BazaNcInvestorAccountEntity> =
                        await this.investorAccountRepository.searchByFullnameOrEmail(request.queryString);
                    const offerings = await this.offeringRepository.searchByOfferingIdOrName(request.queryString);

                    if (isNcAccountIdSearch) {
                        investorAccounts.push(...(await this.investorAccountRepository.searchByNcAccountId(request.queryString)));
                    } else if (isNcPartyIdSearch) {
                        investorAccounts.push(...(await this.investorAccountRepository.searchByNcAccountId(request.queryString)));
                    }

                    if (investorAccounts.length > 0 || offerings.length > 0) {
                        if (investorAccounts.length) {
                            where.push({
                                investorAccount: In(investorAccounts.map((next) => next.id)) as any,
                            });
                        }

                        if (offerings.length) {
                            where.push({
                                offering: In(offerings.map((next) => next.id)) as any,
                            });
                        }
                    } else {
                        return false;
                    }
                }
            }
        }

        if (request.dateFrom && request.dateTo) {
            if (!where.length) {
                where.push({});
            }

            for (const whereConditions of where) {
                whereConditions.date = Between(
                    moment(request.dateFrom)
                        .set({
                            hours: 0,
                            minutes: 0,
                            seconds: 0,
                            milliseconds: 0,
                        })
                        .toDate(),
                    moment(request.dateTo)
                        .set({
                            hours: 23,
                            minutes: 59,
                            seconds: 59,
                            milliseconds: 999,
                        })
                        .toDate(),
                );
            }
        }

        return findOptions;
    }
}
