import { Injectable } from '@nestjs/common';
import { BazaNcDividendProcessApiCallbacks, BazaNcDividendProcessingStrategy } from '../processing/baza-nc-dividend-processing-strategy';
import {
    BazaNcDividendEntity,
    BazaNcDividendRepository,
    BazaNcDividendTransactionEntity,
    BazaNcDividendTransactionEntryEntity,
    BazaNcDividendTransactionEntryRepository,
    BazaNcDividendTransactionRepository,
} from '../../../typeorm';
import {
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionCmsReprocessRequest,
    BazaNcDividendTransactionCmsSyncRequest,
    BazaNcDividendTransactionStatus,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendTransactionCannotBeProcessedException } from '../exceptions/baza-nc-dividend-transaction-cannot-be-processed.exception';
import { BazaNcDividendTransactionIsEmptyException } from '../exceptions/baza-nc-dividend-transaction-is-empty.exception';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcDividendSuccessfulPaymentEvent } from '../events/baza-nc-dividend-successful-payment.event';
import { BazaNcDividendFailedPaymentEvent } from '../events/baza-nc-dividend-failed-payment.event';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import { BazaNcDividendTransactionEntryIsLockedException } from '../exceptions/baza-nc-dividend-transaction-is-locked.exception';
import { BazaNcDividendActionsAccessService } from './baza-nc-dividend-actions-access.service';

/**
 * Dividend Transaction Processing Service
 * The service is responsible to process Dividend Transaction & Execute all necessary external APIs to process dividends
 */
@Injectable()
export class BazaNcDividendProcessingService {
    private readonly strategies: Array<BazaNcDividendProcessingStrategy> = [];

    constructor(
        private readonly eventBus: EventBus,
        private readonly logger: BazaLogger,
        private readonly access: BazaNcDividendActionsAccessService,
        private readonly dividendRepository: BazaNcDividendRepository,
        private readonly dividendTransactionRepository: BazaNcDividendTransactionRepository,
        private readonly dividendTransactionEntryRepository: BazaNcDividendTransactionEntryRepository,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Setup Processing strategies for Dividends
     * By default Baza supports NC and Dwolla strategies for dividends
     * @param strategies
     */
    setProcessingStrategies(strategies: Array<BazaNcDividendProcessingStrategy>): void {
        while (this.strategies.length) {
            this.strategies.pop();
        }

        this.strategies.push(...strategies);
    }

    /**
     * Starts Dividend Transaction and process all entities in the transaction
     * Dividend Transaction will be locked until processing finish
     * @param request
     */
    async process(request: { dividendTransactionUlid: string; async: boolean }, correlationId?: string): Promise<void> {
        this.logger.info(`${this.constructor.name}.${this.process.name}`, {
            correlationId,
            request,
        });

        await this.validateCanDividendTransactionBeStarted(request, correlationId);

        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(request.dividendTransactionUlid, correlationId);
        const dividendEntries = await this.dividendTransactionEntryRepository.findByDividendTransaction(dividendTransaction, correlationId);

        if (dividendTransaction.status === BazaNcDividendTransactionStatus.InProgress) {
            throw new BazaNcDividendTransactionEntryIsLockedException();
        }

        const callbacks = await this.factoryProcessCallback(dividendTransaction, correlationId);

        await callbacks.updateDividendTransaction();

        for (const dividend of dividendEntries) {
            const strategy = this.strategies.find((next) => next.source === dividend.source);

            if (!strategy) {
                throw new BazaNcDividendTransactionCannotBeProcessedException();
            }
        }

        const successfulPayments: Array<BazaNcDividendEntity> = [];
        const failedPayments: Array<BazaNcDividendTransactionEntryEntity> = [];

        try {
            for (const strategy of this.strategies) {
                const entries = dividendEntries.filter((next) => next.source === strategy.source);

                if (entries.length === 0) {
                    continue;
                }

                const serviceResponse = await strategy.process(
                    {
                        dividendTransaction,
                        dividendEntries: entries,
                    },
                    callbacks,
                );

                successfulPayments.push(...serviceResponse.successfulPayments);
                failedPayments.push(...serviceResponse.failedPayments);
            }
        } catch (err) {
            await callbacks.failDividendTransaction();

            throw err;
        }

        if (successfulPayments.filter((next) => !!next).length > 0) {
            this.eventBus.publish(
                new BazaNcDividendSuccessfulPaymentEvent({
                    dividends: successfulPayments,
                    dividendTransaction,
                }),
            );
        }

        if (failedPayments.filter((next) => !!next).length > 0) {
            this.eventBus.publish(
                new BazaNcDividendFailedPaymentEvent({
                    entries: failedPayments,
                    dividendTransaction,
                }),
            );
        }
    }

    /**
     * Re-process specific entries of Dividend Transaction
     * Reprocessing could be not executed while Dividend Transaction is in InProgress status
     * @param request
     */
    async reprocess(request: BazaNcDividendTransactionCmsReprocessRequest): Promise<void> {
        await this.validateCanDividendTransactionBeStarted({
            dividendTransactionUlid: request.ulid,
        });

        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(request.ulid);
        const dividendEntries = (await this.dividendTransactionEntryRepository.findByDividendTransaction(dividendTransaction)).filter(
            (next) => request.entryULIDs.includes(next.ulid),
        );

        if (dividendEntries.length === 0) {
            throw new BazaNcDividendTransactionIsEmptyException();
        }

        const callbacks = this.factoryProcessCallback(dividendTransaction);

        await callbacks.updateDividendTransaction();

        for (const dividend of dividendEntries) {
            const strategy = this.strategies.find((next) => next.source === dividend.source);

            if (!strategy || !strategy.reprocess || !this.access.canReprocessDividendEntry(dividend, dividend.dividendTransaction)) {
                throw new BazaNcDividendTransactionCannotBeProcessedException();
            }
        }

        const successfulPayments: Array<BazaNcDividendEntity> = [];
        const failedPayments: Array<BazaNcDividendTransactionEntryEntity> = [];

        try {
            for (const strategy of this.strategies) {
                const entries = dividendEntries.filter((next) => next.source === strategy.source);

                if (entries.length === 0) {
                    continue;
                }

                const serviceResponse = await strategy.reprocess(
                    {
                        dividendTransaction,
                        dividendEntries: entries,
                    },
                    callbacks,
                );

                successfulPayments.push(...serviceResponse.successfulPayments);
                failedPayments.push(...serviceResponse.failedPayments);
            }
        } catch (err) {
            await callbacks.failDividendTransaction();

            throw err;
        }

        if (successfulPayments.filter((next) => !!next).length > 0) {
            this.eventBus.publish(
                new BazaNcDividendSuccessfulPaymentEvent({
                    dividends: successfulPayments,
                    dividendTransaction,
                }),
            );
        }

        if (failedPayments.filter((next) => !!next).length > 0) {
            this.eventBus.publish(
                new BazaNcDividendFailedPaymentEvent({
                    entries: failedPayments,
                    dividendTransaction,
                }),
            );
        }

        await callbacks.updateDividendTransaction();
    }

    /**
     * Synchronize all entries in DividendTransaction (updates statuses from external sources)
     * The operation cannot be performed if transaction is in InProgress status
     * @param request
     */
    async sync(request: Omit<BazaNcDividendTransactionCmsSyncRequest, 'async'>): Promise<void> {
        await this.validateCanDividendTransactionBeStarted({
            dividendTransactionUlid: request.ulid,
        });

        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(request.ulid);
        const dividendEntries = await this.dividendTransactionEntryRepository.findByDividendTransaction(dividendTransaction);

        const callbacks = this.factoryProcessCallback(dividendTransaction);

        this.logger.debug(`Started synchronization of payment statuses for Dividend Transaction ${request.ulid}`);

        for (const dividend of dividendEntries) {
            const strategy = this.strategies.find((next) => next.source === dividend.source);

            if (!strategy) {
                throw new BazaNcDividendTransactionCannotBeProcessedException();
            }
        }

        const setStatus = async (entry: BazaNcDividendTransactionEntryEntity, newStatus: BazaNcDividendPaymentStatus) => {
            entry.status = newStatus;
            entry.dateUpdatedAt = new Date();

            if (newStatus === BazaNcDividendPaymentStatus.Processed) {
                entry.dateProcessedAt = new Date();
            }

            await this.dividendRepository.setStatusByDwollaTransferId(entry.dwollaTransferId, newStatus);
            await this.dividendTransactionEntryRepository.save([entry]);

            this.logger.debug(`Transfer dividend entry ${entry.ulid} (status: ${entry.status}) updated to ${newStatus} status`);
        };

        try {
            for (const strategy of this.strategies) {
                if (!strategy.sync) {
                    continue;
                }

                const entries = dividendEntries.filter((next) => next.source === strategy.source);

                if (entries.length === 0) {
                    continue;
                }

                const serviceResponse = await strategy.sync(
                    {
                        dividendTransaction,
                        dividendEntries: entries,
                    },
                    callbacks,
                );

                for (const entry of serviceResponse.unknown) {
                    this.logger.warn(`Failed to sync transfer status for dividend entry ${entry.ulid} (status: ${entry.status})`);
                }

                for (const entry of serviceResponse.missing) {
                    await this.dividendRepository.findByDwollaTransferId(entry.dwollaTransferId);
                    await this.dividendTransactionEntryRepository.remove([entry]);

                    this.logger.warn(`Transfer dividend entry ${entry.ulid} (status: ${entry.status}) deleted`);
                }

                for (const entry of serviceResponse.pending) {
                    await setStatus(entry, BazaNcDividendPaymentStatus.Pending);
                }

                for (const entry of serviceResponse.processed) {
                    await setStatus(entry, BazaNcDividendPaymentStatus.Processed);
                }

                for (const entry of serviceResponse.failed) {
                    await setStatus(entry, BazaNcDividendPaymentStatus.Failed);
                }

                for (const entry of serviceResponse.cancelled) {
                    await setStatus(entry, BazaNcDividendPaymentStatus.Cancelled);
                }

                if (serviceResponse.processed.length > 0) {
                    const dividends = await this.dividendRepository.findByDwollaTransferIds(
                        serviceResponse.processed.map((next) => next.dwollaTransferId).filter((next) => !!next),
                    );

                    if (dividends.length > 0) {
                        this.eventBus.publish(
                            new BazaNcDividendSuccessfulPaymentEvent({
                                dividends,
                                dividendTransaction,
                            }),
                        );
                    }
                }

                if (serviceResponse.failed.length > 0) {
                    this.eventBus.publish(
                        new BazaNcDividendFailedPaymentEvent({
                            entries: serviceResponse.failed,
                            dividendTransaction,
                        }),
                    );
                }
            }
        } catch (err) {
            await callbacks.failDividendTransaction();

            throw err;
        }

        await callbacks.updateDividendTransaction();
    }

    /**
     * Updates Dividend Transaction status
     * @param request
     */
    async updateDividendTransaction(request: { dividendTransactionUlid: string; shouldBeMarkedAsStarted: boolean }): Promise<void> {
        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(request.dividendTransactionUlid);
        const dividendEntries = await this.dividendTransactionEntryRepository.findByDividendTransaction(dividendTransaction);

        if (request.shouldBeMarkedAsStarted) {
            dividendEntries.forEach((next) => (next.processedByDwolla = false));

            await this.dividendTransactionEntryRepository.save(dividendEntries);
        }

        const areAllDividendsProcessed = dividendEntries.every((next) => this.access.isDividendEntryProcessed(next));
        const areAllTransactionsSuccessful = dividendEntries.every((next) => next.status === BazaNcDividendPaymentStatus.Processed);
        const hasCancelledTransactions = dividendEntries.some((next) => next.status === BazaNcDividendPaymentStatus.Cancelled);
        const hasFailedTransactions = dividendEntries.some((next) => next.status === BazaNcDividendPaymentStatus.Failed);

        if (areAllDividendsProcessed) {
            if (hasCancelledTransactions) {
                dividendTransaction.status = BazaNcDividendTransactionStatus.Cancelled;
            } else if (hasFailedTransactions) {
                dividendTransaction.status = BazaNcDividendTransactionStatus.Failed;
            } else if (areAllTransactionsSuccessful) {
                dividendTransaction.status = BazaNcDividendTransactionStatus.Successful;
                dividendTransaction.dateProcessedAt = new Date();
            } else {
                dividendTransaction.status = BazaNcDividendTransactionStatus.Unknown;
            }

            dividendTransaction.dateUpdatedAt = new Date();

            await this.dividendTransactionRepository.save([dividendTransaction]);
        } else if (request.shouldBeMarkedAsStarted) {
            if (dividendTransaction.status !== BazaNcDividendTransactionStatus.InProgress) {
                dividendTransaction.status = BazaNcDividendTransactionStatus.InProgress;
                dividendTransaction.dateUpdatedAt = new Date();

                await this.dividendTransactionRepository.save([dividendTransaction]);
            }
        } else {
            dividendTransaction.dateUpdatedAt = new Date();

            await this.dividendTransactionRepository.save([dividendTransaction]);
        }
    }

    /**
     * Returns true if Dividend Transaction is not locked.
     * Lock status is counting for process and sync requests.
     * @param request
     */
    async validateCanDividendTransactionBeStarted(request: { dividendTransactionUlid }, correlationId?: string): Promise<void> {
        this.logger.info(`${this.constructor.name}.${this.validateCanDividendTransactionBeStarted.name}`, {
            correlationId,
            request,
        });

        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(request.dividendTransactionUlid);
        const dividendEntries = await this.dividendTransactionEntryRepository.findByDividendTransaction(dividendTransaction);

        if (dividendEntries.length === 0) {
            throw new BazaNcDividendTransactionIsEmptyException();
        }
    }

    /**
     * Returns callbacks for different processing operations
     * Could be used for additional operations outside Baza packages
     * @param dividendTransaction
     */
    factoryProcessCallback(
        dividendTransaction: BazaNcDividendTransactionEntity,
        correlationId?: string,
    ): BazaNcDividendProcessApiCallbacks {
        const updateDividendTransaction = async (
            options: {
                shouldBeMarkedAsStarted?: boolean;
            } = {
                shouldBeMarkedAsStarted: true,
            },
        ) => {
            await this.updateDividendTransaction({
                dividendTransactionUlid: dividendTransaction.ulid,
                shouldBeMarkedAsStarted: options.shouldBeMarkedAsStarted,
            });
        };

        const abortDividendTransaction = async () => {
            dividendTransaction.status = BazaNcDividendTransactionStatus.Draft;

            await this.dividendTransactionRepository.save([dividendTransaction]);
        };

        const failDividendTransaction = async () => {
            dividendTransaction.status = BazaNcDividendTransactionStatus.Failed;

            await this.dividendTransactionRepository.save([dividendTransaction]);
        };

        return {
            updateDividendTransaction,
            abortDividendTransaction,
            failDividendTransaction,
        };
    }
}
