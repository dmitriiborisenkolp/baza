import { Injectable } from '@nestjs/common';
import { BazaRegistryService, MailService } from '@scaliolabs/baza-core-api';
import {
    BazaNcDividendEntity,
    BazaNcDividendTransactionEntity,
    BazaNcInvestorAccountRepository,
    BazaNcOfferingEntity,
} from '../../../typeorm';
import { getBazaClientName, getBazaProjectCodeName, getBazaProjectName, replaceTags } from '@scaliolabs/baza-core-shared';
import * as _ from 'lodash';
import { convertFromCents } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountMapper } from '../../../investor-account';
import { BazaNcMail } from '../../../../constants/baza-nc.mail-templates';

/**
 * The service shares methods for email notifications
 */
@Injectable()
export class BazaNcDividendMailNotificationsService {
    constructor(
        private readonly mail: MailService,
        private readonly registry: BazaRegistryService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly investorAccountMapper: BazaNcInvestorAccountMapper,
    ) {}

    /**
     * Sends notification about successful dividend payment to Investor
     * @param dividends
     * @param dividendTransaction
     */
    async sendNotificationAboutSuccessfulPaymentsToInvestor(
        dividends: Array<BazaNcDividendEntity>,
        dividendTransaction?: BazaNcDividendTransactionEntity,
    ): Promise<void> {
        const sendEmailNotification = async (
            investorId: number,
            variables: Record<string, unknown> = {},
            offering: BazaNcOfferingEntity = null,
        ) => {
            let name: BazaNcMail = BazaNcMail.BazaNcDividendSuccessfulPaymentWithoutOffering;

            const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorId);

            variables.investorAccount = this.investorAccountMapper.entityToDTO(investorAccount);

            if (offering) {
                variables.offeringName = offering.ncOfferingName;

                name = BazaNcMail.BazaNcDividendSuccessfulPaymentWithOffering;
            }

            await this.mail.send({
                name,
                uniqueId: dividendTransaction
                    ? `baza_nc_dividend_successful_payments_${dividendTransaction.ulid}_${investorAccount.id}`
                    : undefined,
                subject: replaceTags(this.registry.getValue('bazaNc.dividendsSuccessfulSubject'), {
                    amount: variables.amount,
                    clientName: getBazaClientName(),
                    projectName: getBazaProjectName(),
                    projectCodeName: getBazaProjectCodeName(),
                }),
                to: [
                    {
                        email: investorAccount.user.email,
                        name: investorAccount.user.fullName,
                    },
                ],
                variables: () => variables,
            });
        };

        const dividendsWithOffering = dividends.filter((next) => next.offering);
        const dividendsWithoutOffering = dividends.filter((next) => !next.offering);

        for (const dividendSet of [dividendsWithOffering, dividendsWithoutOffering]) {
            const investorAccountIds = _.uniq(dividendSet.map((next) => next.investorAccount.id));
            const dividendSetWithOffering = !!dividendSet[0]?.offering?.ncOfferingName;

            for (const investorAccountId of investorAccountIds) {
                const filtered = dividendSet.filter((next) => next.investorAccount.id === investorAccountId);
                if (dividendSetWithOffering) {
                    for (const filteredItem of _.uniqBy(filtered, 'offering.ncOfferingName')) {
                        const offering = filteredItem.offering;
                        const amount = convertFromCents(
                            _.sum(
                                filtered
                                    .filter((next) => next.offering.ncOfferingName === filteredItem.offering.ncOfferingName)
                                    .map((next) => next.amountCents),
                            ),
                        ).toFixed(2);

                        await sendEmailNotification(investorAccountId, { amount }, offering);
                    }
                } else {
                    const amount = convertFromCents(_.sum(filtered.map((next) => next.amountCents))).toFixed(2);

                    await sendEmailNotification(investorAccountId, { amount });
                }
            }
        }
    }
}
