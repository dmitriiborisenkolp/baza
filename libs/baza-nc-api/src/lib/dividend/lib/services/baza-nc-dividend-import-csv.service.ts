import { Injectable } from '@nestjs/common';
import {
    BAZA_NC_DIVIDEND_CONSTANTS,
    BazaNcDividendImportCsvEntryError,
    BazaNcDividendImportCsvEntryErrorDto,
    BazaNcDividendPaymentSource,
    BazaNcDividendTransactionEntryImportCsvResponse,
    convertToCents,
    OfferingId,
} from '@scaliolabs/baza-nc-shared';
import {
    BazaNcDividendTransactionRepository,
    BazaNcInvestorAccountEntity,
    BazaNcInvestorAccountRepository,
    BazaNcOfferingRepository,
} from '../../../typeorm';
import { BazaNcDividendTransactionEntryIsLockedException } from '../exceptions/baza-nc-dividend-transaction-entry-is-locked.exception';
import { BazaNcDividendImportCsvNoEntriesException } from '../exceptions/baza-nc-dividend-import-csv-no-entries.exception';
import {
    BazaNcDividendImportCsvColumnType,
    bazaNcDividendImportCsvRequiredColumns,
} from '../models/import-csv/baza-nc-dividend-import-csv-column-type';
import { bazaNcDividendImportCsvColumnTypeDetector } from '../models/import-csv/baza-nc-dividend-import-csv-column-type-detector';
import { BazaNcDividendImportCsvFailedToParseException } from '../exceptions/baza-nc-dividend-import-csv-failed-to-parse.exception';
import { BazaNcDividendImportCsvDuplicateAccountColumnsException } from '../exceptions/baza-nc-dividend-import-csv-duplicate-account-columns.exception';
import { BazaNcDividendImportCsvUnknownColumnException } from '../exceptions/baza-nc-dividend-import-csv-duplicate-columns.exception';
import { BazaNcDividendImportCsvEntry } from '../models/import-csv/baza-nc-dividend-import-csv-entry';
import { BazaNcDividendTransactionEntryCmsService } from './baza-nc-dividend-transaction-entry-cms.service';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import * as moment from 'moment';
import { BazaNcDividendImportCsvTooManyRecordsException } from '../exceptions/baza-nc-dividend-import-csv-too-many-records.exception';
import { BazaNcDividendActionsAccessService } from './baza-nc-dividend-actions-access.service';

interface ImportCsvRequest {
    csv: Array<Array<string>>;
    source: BazaNcDividendPaymentSource;
    dividendTransactionUlid: string;
}

type SuccessEntry = {
    row: number;
    result: BazaNcDividendImportCsvEntry;
    success: true;
};

type ErrorEntry = {
    row: number;
    result: BazaNcDividendImportCsvEntryErrorDto;
    success: false;
};

type Entry = SuccessEntry | ErrorEntry;

/**
 * Import Dividend via CSV Service
 * The service parses CSV File and creates Dividend Transaction Entries for Dividend Transaction
 */
@Injectable()
export class BazaNcDividendImportCsvService {
    constructor(
        private readonly access: BazaNcDividendActionsAccessService,
        private readonly service: BazaNcDividendTransactionEntryCmsService,
        private readonly dividendTransactionRepository: BazaNcDividendTransactionRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
    ) {}

    /**
     * Imports Dividend Transaction Entries to Dividend Transaction from CSV File
     * Import via CSV supports multiple combinations of columns
     * @see BazaNcDividendImportCsvColumnType
     * @see BazaNcDividendImportCsvEntry
     * @see BazaNcDividendImportCsvEntryErrorDto
     * @param request
     */
    async importCsv(request: ImportCsvRequest): Promise<BazaNcDividendTransactionEntryImportCsvResponse> {
        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(request.dividendTransactionUlid);

        if (!this.access.canProcessDividendTransaction(dividendTransaction)) {
            throw new BazaNcDividendTransactionEntryIsLockedException();
        }

        if (request.csv.length < 2) {
            throw new BazaNcDividendImportCsvNoEntriesException();
        }

        if (request.csv.length > BAZA_NC_DIVIDEND_CONSTANTS.maxCsvFileLines) {
            throw new BazaNcDividendImportCsvTooManyRecordsException();
        }

        const columns = this.csvParseHeader(request.csv[0]);
        const entries: Array<Entry> = [];

        for (let row = 1; row < request.csv.length; row++) {
            entries.push({
                ...(await this.csvParseEntry(row + 1, columns, request.csv[row])),
                row,
            } as Entry);
        }

        const errorEntries = entries.filter((next) => !next.success);
        const successEntries: Array<SuccessEntry> = entries.filter((next) => next.success) as Array<SuccessEntry>;

        if (errorEntries.length) {
            return {
                success: false,
                errors: errorEntries.map((next) => next.result as BazaNcDividendImportCsvEntryErrorDto),
            };
        } else {
            for (const entry of successEntries) {
                await this.service.create({
                    date: entry.result.date.toISOString(),
                    offeringId: entry.result.ncOfferingId,
                    amountCents: entry.result.amountCents,
                    source: request.source,
                    dividendTransactionUlid: dividendTransaction.ulid,
                    investorAccountId: entry.result.investorAccount.id,
                });
            }

            return {
                success: true,
            };
        }
    }

    /**
     * CSV Parser - Header Parser
     * Import via CSV supports multiple combinations of columns
     * @see BazaNcDividendImportCsvColumnType
     * @param header
     * @private
     */
    private csvParseHeader(header: Array<string>): Array<BazaNcDividendImportCsvColumnType> {
        const detectedColumns: Array<BazaNcDividendImportCsvColumnType> = [];

        for (const row of header) {
            detectedColumns.push(bazaNcDividendImportCsvColumnTypeDetector(row));
        }

        for (const columnType of Object.values(BazaNcDividendImportCsvColumnType)) {
            const found = detectedColumns.filter((next) => next === columnType);

            if (found.length > 1) {
                throw new BazaNcDividendImportCsvUnknownColumnException();
            }
        }

        const accountColumns = detectedColumns.filter((next) =>
            [
                BazaNcDividendImportCsvColumnType.BazaAccountId,
                BazaNcDividendImportCsvColumnType.NcAccountId,
                BazaNcDividendImportCsvColumnType.DwollaCustomerId,
            ].includes(next),
        );

        if (accountColumns.length > 1) {
            throw new BazaNcDividendImportCsvDuplicateAccountColumnsException();
        } else if (accountColumns.length === 0) {
            throw new BazaNcDividendImportCsvFailedToParseException();
        }

        for (const columnType of bazaNcDividendImportCsvRequiredColumns) {
            if (!detectedColumns.includes(columnType)) {
                throw new BazaNcDividendImportCsvFailedToParseException();
            }
        }

        return detectedColumns;
    }

    /**
     * CSV Parser - Entry Parser
     * @see BazaNcDividendImportCsvEntry
     * @see BazaNcDividendImportCsvEntryErrorDto
     * @param row
     * @param columns
     * @param values
     * @private
     */
    private async csvParseEntry(row: number, columns: Array<BazaNcDividendImportCsvColumnType>, values: Array<string>): Promise<Entry> {
        const errorEntry = (reason: BazaNcDividendImportCsvEntryError) =>
            ({
                row,
                success: false,
                result: {
                    row,
                    reason,
                },
            } as ErrorEntry);

        try {
            let date: Date;
            let amountCents: number;
            let investorAccount: BazaNcInvestorAccountEntity;
            let ncOfferingId: OfferingId;

            for (let columnN = 0; columnN < columns.length; columnN++) {
                const column = columns[columnN];
                const value = (values[columnN] || '').trim();

                switch (column) {
                    default: {
                        throw new BazaNcDividendImportCsvUnknownColumnException();
                    }

                    case BazaNcDividendImportCsvColumnType.NcAccountId: {
                        investorAccount = await this.investorAccountRepository.findInvestorAccountByNcAccountId(value);

                        if (!investorAccount) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.InvestorAccountNotFound);
                        }

                        break;
                    }

                    case BazaNcDividendImportCsvColumnType.BazaAccountId: {
                        const id = parseInt(value, 10);

                        if (isNaN(id)) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.InvestorAccountNotFound);
                        }

                        investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(parseInt(value, 10));

                        if (!investorAccount) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.InvestorAccountNotFound);
                        }

                        break;
                    }

                    case BazaNcDividendImportCsvColumnType.DwollaCustomerId: {
                        investorAccount = await this.investorAccountRepository.findInvestorAccountByDwollaId(value);

                        if (!investorAccount) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.InvestorAccountNotFound);
                        }

                        break;
                    }

                    case BazaNcDividendImportCsvColumnType.Amount: {
                        const normalizedValue = value.replace('$', '').replace(/,/g, '').replace(/ /g, '');
                        const amount = parseFloat(normalizedValue);

                        if (isNaN(amount)) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.FailedToParseAmount);
                        }

                        if (amount < 0) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.AmountIsLessThanZero);
                        }

                        amountCents = convertToCents(amount);

                        break;
                    }

                    case BazaNcDividendImportCsvColumnType.Date: {
                        const momentDate = moment(value);

                        if (!momentDate.isValid()) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.FailedToParseDate);
                        }

                        date = momentDate.toDate();

                        if (date.getTime() > new Date().getTime()) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.DateIsInFuture);
                        }

                        break;
                    }

                    case BazaNcDividendImportCsvColumnType.NcOfferingId: {
                        const offering = await this.offeringRepository.findOfferingWithId(value);

                        if (!offering) {
                            return errorEntry(BazaNcDividendImportCsvEntryError.OfferingNotFound);
                        }

                        ncOfferingId = offering.ncOfferingId;

                        break;
                    }
                }
            }

            if (!investorAccount || !date || !amountCents) {
                return errorEntry(BazaNcDividendImportCsvEntryError.NotEnoughData);
            }

            return {
                row,
                success: true,
                result: {
                    date,
                    amountCents,
                    ncOfferingId,
                    investorAccount,
                },
            } as SuccessEntry;
        } catch (error) {
            if (error instanceof BazaAppException) {
                return {
                    row,
                    success: false,
                    result: {
                        row,
                        reason: BazaNcDividendImportCsvEntryError.Exception,
                        exception: error.message,
                    },
                } as ErrorEntry;
            } else {
                return {
                    row,
                    success: false,
                    result: {
                        row,
                        reason: BazaNcDividendImportCsvEntryError.Exception,
                        exception: 'INTERNAL_SERVER_ERROR',
                    },
                } as ErrorEntry;
            }
        }
    }
}
