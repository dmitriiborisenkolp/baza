import { Injectable } from '@nestjs/common';
import {
    BazaNcDividendDto,
    BazaNcDividendListRequest,
    BazaNcDividendListResponse,
    BazaNcDividendReprocessRequest,
    BazaNcDividendTotalAmountResponse,
    convertFromCents,
} from '@scaliolabs/baza-nc-shared';
import {
    BAZA_NC_DIVIDEND_RELATIONS,
    BazaNcDividendEntity,
    BazaNcDividendRepository,
    BazaNcDividendTransactionEntryRepository,
    BazaNcInvestorAccountRepository,
} from '../../../typeorm';
import { CrudService, EnvService } from '@scaliolabs/baza-core-api';
import { BazaNcDividendMapper } from '../mappers/baza-nc-dividend.mapper';
import { BazaNcDividendNotFoundException } from '../exceptions/baza-nc-dividend-not-found.exception';
import { bazaEmptyCrudListResponse } from '@scaliolabs/baza-core-shared';
import { In } from 'typeorm';
import { BazaNcDividendProcessingService } from './baza-nc-dividend-processing.service';
import { BazaNcDividendCannotBeReprocessedException } from '../exceptions/baza-nc-dividend-cannot-be-reprocessed.exception';
import { BazaNcDividendActionsAccessService } from './baza-nc-dividend-actions-access.service';

@Injectable()
export class BazaNcDividendService {
    constructor(
        private readonly env: EnvService,
        private readonly crud: CrudService,
        private readonly mapper: BazaNcDividendMapper,
        private readonly access: BazaNcDividendActionsAccessService,
        private readonly repository: BazaNcDividendRepository,
        private readonly entriesRepository: BazaNcDividendTransactionEntryRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly processing: BazaNcDividendProcessingService,
    ) {}

    /**
     * Returns list of dividends (Public API)
     * @param request
     * @param investorAccountId
     */
    async list(request: BazaNcDividendListRequest, investorAccountId: number): Promise<BazaNcDividendListResponse> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountById(investorAccountId);
        const conditions = [
            {
                investorAccount,
            },
        ] as any;

        if (request.status) {
            for (const condition of conditions) {
                condition.status = In(request.status);
            }
        }

        if (!investorAccount) {
            return bazaEmptyCrudListResponse();
        } else {
            return this.crud.find<BazaNcDividendEntity, BazaNcDividendDto>({
                request,
                entity: BazaNcDividendEntity,
                mapper: async (items) => this.mapper.entitiesToDTOs(items),
                findOptions: {
                    where: conditions,
                    order: {
                        date: 'DESC',
                    },
                    relations: BAZA_NC_DIVIDEND_RELATIONS,
                },
            });
        }
    }

    /**
     * Returns Dividend by ULID (Public API)
     * @param ulid
     * @param investorAccountId
     */
    async getByUlid(ulid: string, investorAccountId: number): Promise<BazaNcDividendEntity> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);
        const dividend = await this.repository.getByUlid(ulid);

        if (dividend.investorAccount.id !== investorAccount.id) {
            throw new BazaNcDividendNotFoundException();
        }

        return dividend;
    }

    /**
     * Returns Total Amount (Sum) of all dividends for current account (Public API)
     * @param investorAccountId
     */
    async totalAmount(investorAccountId: number): Promise<BazaNcDividendTotalAmountResponse> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountById(investorAccountId);

        if (!investorAccount) {
            return {
                totalAmount: '0.00',
                totalAmountCents: 0,
            };
        }

        const totalAmountCents = await this.repository.totalAmount({
            investorAccount,
        });

        return {
            totalAmount: convertFromCents(totalAmountCents).toFixed(2),
            totalAmountCents,
        };
    }

    /**
     * Reprocess Dividend. It will work only for Cancelled or Failed payments.
     * @param request
     * @param investorAccountId
     */
    async reprocess(request: BazaNcDividendReprocessRequest, investorAccountId: number): Promise<BazaNcDividendEntity> {
        const dividend = await this.repository.getByUlid(request.ulid);
        const dividendEntry = await this.entriesRepository.getByDividend(dividend);
        const dividendTransaction = dividendEntry.dividendTransaction;

        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);

        if (dividend.investorAccount.id !== investorAccount.id) {
            throw new BazaNcDividendNotFoundException();
        }

        if (!this.access.canReprocessDividendEntry(dividendEntry, dividendTransaction)) {
            throw new BazaNcDividendCannotBeReprocessedException();
        }

        await this.processing.reprocess({
            ulid: dividendTransaction.ulid,
            entryULIDs: [dividendEntry.ulid],
        });

        return this.repository.getByUlid(request.ulid);
    }
}
