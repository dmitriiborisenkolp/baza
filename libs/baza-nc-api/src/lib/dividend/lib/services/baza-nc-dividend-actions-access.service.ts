import { Injectable } from '@nestjs/common';
import {
    bazaNcDividendNonDraftTransactionStatuses,
    bazaNcDividendProcessedPaymentStatuses,
    bazaNcDividendReprocessablePaymentSources,
    bazaNcDividendReprocessablePaymentStatuses,
    bazaNcDividendTransactionStatusesLocked,
    bazaNcDividendTransactionStatusesToProcess,
    bazaNcDividendUpdatablePaymentStatuses,
} from '@scaliolabs/baza-nc-shared';
import {
    BazaNcDividendEntity,
    BazaNcDividendTransactionEntity,
    BazaNcDividendTransactionEntryEntity,
    BazaNcDividendTransactionEntryRepository,
} from '../../../typeorm';

/**
 * The service returns information about Locked / Draft statuses and information about possibility to update/delete/reprocess entries
 */
@Injectable()
export class BazaNcDividendActionsAccessService {
    constructor(private readonly entriesRepository: BazaNcDividendTransactionEntryRepository) {}

    /**
     * Returns true if Dividend Transaction is Locked
     * @param dividendTransaction
     */
    isDividendTransactionLocked(dividendTransaction: BazaNcDividendTransactionEntity): boolean {
        return (
            !this.canProcessDividendTransaction(dividendTransaction) ||
            bazaNcDividendTransactionStatusesLocked.includes(dividendTransaction.status)
        );
    }

    /**
     * Returns true if Dividend Transaction is not in Draft state (i.e. it's processing or it was processed / failed / cancelled before)
     * @param dividendTransaction
     */
    isDividendTransactionDraft(dividendTransaction: BazaNcDividendTransactionEntity): boolean {
        return !bazaNcDividendNonDraftTransactionStatuses.includes(dividendTransaction.status);
    }

    /**
     * Returns true if Dividend Transaction Entry is Locked
     * @param entry
     * @param dividendTransaction
     */
    isDividendEntryLocked(entry: BazaNcDividendTransactionEntryEntity, dividendTransaction: BazaNcDividendTransactionEntity): boolean {
        return this.isDividendTransactionLocked(dividendTransaction) || !bazaNcDividendUpdatablePaymentStatuses.includes(entry.status);
    }

    /**
     * Returns true if Dividend Transaction Entry was processed (with Success, Failed or Cancelled results)
     * @param entry
     */
    isDividendEntryProcessed(entry: BazaNcDividendTransactionEntryEntity): boolean {
        return bazaNcDividendProcessedPaymentStatuses.includes(entry.status);
    }

    /**
     * Returns true if Dividend Transaction can be started (processed)
     * @param dividendTransaction
     */
    canProcessDividendTransaction(dividendTransaction: BazaNcDividendTransactionEntity): boolean {
        return bazaNcDividendTransactionStatusesToProcess.includes(dividendTransaction.status);
    }

    /**
     * Returns true if Dividend Transaction can be deleted
     * @param dividendTransaction
     */
    async canDeleteDividendTransaction(dividendTransaction: BazaNcDividendTransactionEntity): Promise<boolean> {
        if (bazaNcDividendNonDraftTransactionStatuses.includes(dividendTransaction.status)) {
            return false;
        } else {
            const nonDraftEntries = await this.entriesRepository.findNonDraftEntriesForDividendTransaction(dividendTransaction);

            return nonDraftEntries.length === 0;
        }
    }

    /**
     * Returns true if Dividend Transaction Entry can be updated
     * @param entry
     * @param dividendTransaction
     */
    canUpdateDividendEntry(entry: BazaNcDividendTransactionEntryEntity, dividendTransaction: BazaNcDividendTransactionEntity): boolean {
        return !this.isDividendTransactionLocked(dividendTransaction) && !this.isDividendEntryLocked(entry, dividendTransaction);
    }

    /**
     * Returns true if Dividend Transaction Entry can be deleted
     * @param entry
     * @param dividendTransaction
     */
    canDeleteDividendEntry(entry: BazaNcDividendTransactionEntryEntity, dividendTransaction: BazaNcDividendTransactionEntity): boolean {
        return !this.isDividendTransactionLocked(dividendTransaction) && !this.isDividendEntryLocked(entry, dividendTransaction);
    }

    /**
     * Returns true if Dividend Transaction Entry can be re-processed
     * @param entry
     * @param dividendTransaction
     */
    canReprocessDividendEntry(entry: BazaNcDividendTransactionEntryEntity, dividendTransaction: BazaNcDividendTransactionEntity): boolean {
        return (
            bazaNcDividendReprocessablePaymentSources.includes(entry.source) &&
            bazaNcDividendReprocessablePaymentStatuses.includes(entry.status)
        );
    }

    /**
     * Returns true if Dividend can be re-processed
     * @param dividend
     */
    canReprocessDividend(dividend: BazaNcDividendEntity): boolean {
        return (
            bazaNcDividendReprocessablePaymentSources.includes(dividend.source) &&
            bazaNcDividendReprocessablePaymentStatuses.includes(dividend.status)
        );
    }
}
