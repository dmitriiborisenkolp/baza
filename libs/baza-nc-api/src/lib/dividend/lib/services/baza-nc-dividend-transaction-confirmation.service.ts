import { Injectable } from '@nestjs/common';
import {
    BAZA_NC_DIVIDEND_CONFIRMATION_TOKEN_LENGTH,
    BazaNcDividendConfirmationEntity,
    BazaNcDividendConfirmationRepository,
    BazaNcDividendTransactionEntity,
} from '../../../typeorm';
import { BazaNcDividendTransactionCmsProcessRequest, BazaNorthCapitalAcl } from '@scaliolabs/baza-nc-shared';
import { AccountEntity, AclService, BazaLogger, BcryptService, EnvService, MailService } from '@scaliolabs/baza-core-api';
import { BazaNcDividendConfirmationNotFoundException } from '../exceptions/baza-nc-dividend-confirmation-not-found.exception';
import { BazaNcDividendConfirmationInvalidPasswordException } from '../exceptions/baza-nc-dividend-confirmation-invalid-password.exception';
import { AccountRole, generateRandomHexString, replaceTags, withoutEndingSlash } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendConfirmationUnauthorizedException } from '../exceptions/baza-nc-dividend-confirmation-unauthorized.exception';
import { BazaNcMail } from '../../../../constants/baza-nc.mail-templates';

/**
 * 2FA Service to confirm processing of Dividend Transaction
 */
@Injectable()
export class BazaNcDividendTransactionConfirmationService {
    constructor(
        private readonly env: EnvService,
        private readonly mail: MailService,
        private readonly bcryptService: BcryptService,
        private readonly tokenRepository: BazaNcDividendConfirmationRepository,
        private readonly acl: AclService,
        private readonly logger: BazaLogger,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Sends a 2FA Email which contains link + secret token to CMS. The link should lead to Dividend Transaction Entries
     * CMS and displays modal window to confirm Dividend Transaction
     * @param dividendTransaction
     * @param currentAccount
     */
    async sendProcessConfirmationEmail(dividendTransaction: BazaNcDividendTransactionEntity, currentAccount: AccountEntity): Promise<void> {
        if (!(await this.hasAccess(currentAccount))) {
            throw new BazaNcDividendConfirmationUnauthorizedException();
        }

        const existing = await this.tokenRepository.findByDividendTransaction(dividendTransaction);

        if (existing.length) {
            await this.tokenRepository.remove(existing);
        }

        const token = new BazaNcDividendConfirmationEntity();

        token.token = generateRandomHexString(BAZA_NC_DIVIDEND_CONFIRMATION_TOKEN_LENGTH);
        token.author = currentAccount;
        token.transaction = dividendTransaction;

        await this.tokenRepository.save([token]);

        await this.mail.send({
            name: BazaNcMail.BazaNcDividendConfirmProcessTransaction,
            uniqueId: `baza_nc_dividend_transaction_confirm_${token.ulid}`,
            subject: replaceTags('baza-nc.mail.confirmDividendTransaction.subject', {
                dividendTransactionTitle: dividendTransaction.title,
            }),
            to: [
                {
                    email: currentAccount.email,
                    name: currentAccount.fullName,
                },
            ],
            variables: () => ({
                dividendConfirmationToken: token.token,
                dividendConfirmationLink: replaceTags(
                    '{{ cmsUrl }}/baza-nc/dividends/transactions/{{ dividendTransactionUlid }}/?confirm={{ dividendConfirmationToken }}',
                    {
                        cmsUrl: withoutEndingSlash(this.env.getAsString('BAZA_APP_CMS_URL')),
                        dividendTransactionUlid: dividendTransaction.ulid,
                        dividendConfirmationToken: token.token,
                    },
                ),
                dividendTransactionUlid: dividendTransaction.ulid,
                dividendTransactionTitle: dividendTransaction.title,
            }),
        });
    }

    /**
     * Authorize user to process Dividend Transaction
     * After sending 2FA email confirmation & following link, user should type his password. If password is incorrect,
     * user will not be able to start Dividend Transaction. When password is correct, Dividend Transaction will be immediately started
     * @param request
     * @param currentAccount
     * @param correlationId
     */
    async authoriseProcess(
        request: BazaNcDividendTransactionCmsProcessRequest,
        currentAccount: AccountEntity,
        correlationId?: string,
    ): Promise<BazaNcDividendTransactionEntity> {
        this.logger.info(`${this.constructor.name}.${this.authoriseProcess.name}`, {
            correlationId,
            currentAccount,
            request,
        });

        const token = await this.tokenRepository.getByToken(request.token);

        if (token.author.id !== currentAccount.id) {
            throw new BazaNcDividendConfirmationNotFoundException();
        }

        if (!(await this.hasAccess(token.author))) {
            throw new BazaNcDividendConfirmationUnauthorizedException();
        }

        const isPasswordValid = await this.bcryptService.compare(request.password, currentAccount.password);

        if (!isPasswordValid) {
            throw new BazaNcDividendConfirmationInvalidPasswordException();
        }

        await this.tokenRepository.remove([token]);

        return token.transaction;
    }

    /**
     * Returns true if current Admin account has access to process Dividend Transaction
     * @param account
     * @private
     */
    private async hasAccess(account: AccountEntity): Promise<boolean> {
        return (
            account.role === AccountRole.Admin &&
            (await this.acl.hasAccess(account, [
                BazaNorthCapitalAcl.BazaNcDividend,
                BazaNorthCapitalAcl.BazaNcDividendTransaction,
                BazaNorthCapitalAcl.BazaNcDividendTransactionConfirm,
            ]))
        );
    }
}
