import { Injectable } from '@nestjs/common';
import {
    BAZA_NC_DIVIDEND_TRANSACTION_RELATIONS,
    BazaNcDividendTransactionEntity,
    BazaNcDividendTransactionEntryRepository,
    BazaNcDividendTransactionRepository,
} from '../../../typeorm';
import {
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionCmsCreateRequest,
    BazaNcDividendTransactionCmsExportToCsvRequest,
    BazaNcDividendTransactionCmsGetByUlidRequest,
    BazaNcDividendTransactionCmsListRequest,
    BazaNcDividendTransactionCmsListResponse,
    BazaNcDividendTransactionCmsProcessRequest,
    BazaNcDividendTransactionCmsProcessResponse,
    BazaNcDividendTransactionCmsReprocessRequest,
    BazaNcDividendTransactionCmsSendProcessConfirmationRequest,
    BazaNcDividendTransactionCmsSyncRequest,
    BazaNcDividendTransactionCsvDto,
    BazaNcDividendTransactionDto,
    BazaNcDividendTransactionStatus,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendTransactionMapper } from '../mappers/baza-nc-dividend-transaction.mapper';
import { AccountEntity, BazaLogger, CrudCsvService, CrudService } from '@scaliolabs/baza-core-api';
import { BazaNcDividendTransactionCannotBeDeletedException } from '../exceptions/baza-nc-dividend-transaction-cannot-be-deleted.exception';
import { BazaNcDividendTransactionConfirmationService } from './baza-nc-dividend-transaction-confirmation.service';
import { BazaNcDividendProcessingService } from './baza-nc-dividend-processing.service';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { Between, ILike } from 'typeorm';
import { postgresLikeEscape } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';
import { BazaNcDividendTransactionCsvMapper } from '../mappers/baza-nc-dividend-transaction-csv.mapper';
import { BazaNcDividendActionsAccessService } from './baza-nc-dividend-actions-access.service';

/**
 * Dividend Transaction CMS Service
 * The service is mostly used for Dividend Transaction CMS API
 */
@Injectable()
export class BazaNcDividendTransactionCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudCsv: CrudCsvService,
        private readonly access: BazaNcDividendActionsAccessService,
        private readonly repository: BazaNcDividendTransactionRepository,
        private readonly entriesRepository: BazaNcDividendTransactionEntryRepository,
        private readonly mapper: BazaNcDividendTransactionMapper,
        private readonly csvMapper: BazaNcDividendTransactionCsvMapper,
        private readonly confirm: BazaNcDividendTransactionConfirmationService,
        private readonly processing: BazaNcDividendProcessingService,
        private readonly logger: BazaLogger,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Creates a new Dividend Transaction
     * @param request
     */
    async create(request: BazaNcDividendTransactionCmsCreateRequest): Promise<BazaNcDividendTransactionEntity> {
        const entity = new BazaNcDividendTransactionEntity();

        entity.title = request.title;

        await this.repository.save([entity]);

        return entity;
    }

    /**
     * Deletes existing Dividend Transaction
     * @param entity
     */
    async delete(entity: BazaNcDividendTransactionEntity): Promise<void> {
        if (!(await this.access.canDeleteDividendTransaction(entity))) {
            throw new BazaNcDividendTransactionCannotBeDeletedException();
        }

        const dividendEntries = await this.entriesRepository.findByDividendTransaction(entity);

        const hasProcessedTransactions = dividendEntries.some((next) =>
            [BazaNcDividendPaymentStatus.Processed, BazaNcDividendPaymentStatus.Failed, BazaNcDividendPaymentStatus.Cancelled].includes(
                next.status,
            ),
        );

        if (hasProcessedTransactions) {
            throw new BazaNcDividendTransactionCannotBeDeletedException();
        }

        await this.repository.remove([entity]);
    }

    /**
     * Returns list of Dividend Transactions
     * @param request
     */
    async list(request: BazaNcDividendTransactionCmsListRequest): Promise<BazaNcDividendTransactionCmsListResponse> {
        return this.crud.find<BazaNcDividendTransactionEntity, BazaNcDividendTransactionDto>({
            request,
            findOptions: await this.findOptionsFactory(request),
            entity: BazaNcDividendTransactionEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });
    }

    /**
     * Exports list of Dividend Transactions to CSV
     * @param request
     */
    async exportToCsv(request: BazaNcDividendTransactionCmsExportToCsvRequest): Promise<string> {
        return this.crudCsv.exportToCsv<BazaNcDividendTransactionEntity, BazaNcDividendTransactionCsvDto>({
            ...request,
            findOptions: await this.findOptionsFactory(request.listRequest),
            entity: BazaNcDividendTransactionEntity,
            mapper: async (items) => this.csvMapper.entitiesToDTOs(items),
        });
    }

    /**
     * Returns Dividend Transaction by ULID
     * @param request
     */
    async getByUlid(request: BazaNcDividendTransactionCmsGetByUlidRequest): Promise<BazaNcDividendTransactionEntity> {
        return this.repository.getByUlid(request.ulid);
    }

    /**
     * Process Dividend Transaction
     * @see BazaNcDividendProcessingService
     * @param request
     * @param currentAccount
     */
    async process(
        request: BazaNcDividendTransactionCmsProcessRequest,
        currentAccount: AccountEntity,
        correlationId?: string,
    ): Promise<BazaNcDividendTransactionCmsProcessResponse> {
        this.logger.info(`${this.constructor.name}.${this.process.name}`, {
            correlationId,
            currentAccount,
            request,
        });

        const dividendTransaction = await this.confirm.authoriseProcess(request, currentAccount, correlationId);

        const promise = this.processing.process(
            {
                dividendTransactionUlid: dividendTransaction.ulid,
                async: request.async,
            },
            correlationId,
        );

        if (!request.async) {
            await promise;
        }

        return {
            newStatus: request.async
                ? BazaNcDividendTransactionStatus.InProgress
                : (await this.repository.getByUlid(dividendTransaction.ulid)).status,
        };
    }

    /**
     * Re-Process Dividend Transaction
     * @see BazaNcDividendProcessingService
     * @param request
     */
    async reprocess(request: BazaNcDividendTransactionCmsReprocessRequest): Promise<BazaNcDividendTransactionEntity> {
        await this.processing.reprocess(request);

        return this.repository.getByUlid(request.ulid);
    }

    /**
     * Sync statuses for Dividend and Dividend Transaction Entries affected by Dividend Transaction
     * @param request
     */
    async sync(request: BazaNcDividendTransactionCmsSyncRequest): Promise<void> {
        const promise = this.processing.sync(request);

        if (request.async === false) {
            await promise;
        }
    }

    /**
     * Sends a 2FA email to confirm processing of Dividend Transaction
     * @param request
     * @param currentAccount
     */
    async sendProcessConfirmation(
        request: BazaNcDividendTransactionCmsSendProcessConfirmationRequest,
        currentAccount: AccountEntity,
    ): Promise<void> {
        await this.processing.validateCanDividendTransactionBeStarted({
            dividendTransactionUlid: request.ulid,
        });

        const entity = await this.repository.getByUlid(request.ulid);

        await this.confirm.sendProcessConfirmationEmail(entity, currentAccount);
    }

    /**
     * TypeORM Find Options factory which is used for list methods
     * @see BazaNcDividendTransactionCmsService.list
     * @see BazaNcDividendTransactionCmsService.exportToCsv
     * @param request
     * @private
     */
    private async findOptionsFactory(
        request: BazaNcDividendTransactionCmsListRequest,
    ): Promise<FindManyOptions<BazaNcDividendTransactionEntity>> {
        const where: Array<FindConditions<BazaNcDividendTransactionEntity>> = [];

        const findOptions: FindManyOptions<BazaNcDividendTransactionEntity> = {
            relations: BAZA_NC_DIVIDEND_TRANSACTION_RELATIONS,
            order: {
                ulid: 'DESC',
            },
            where,
        };

        if (request.queryString) {
            where.push({
                title: ILike(`%${postgresLikeEscape(request.queryString.trim())}%`),
            });
        }

        if (request.dateFrom && request.dateTo) {
            if (!where.length) {
                where.push({});
            }

            for (const whereConditions of where) {
                whereConditions.dateCreatedAt = Between(
                    moment(request.dateFrom)
                        .set({
                            hours: 0,
                            minutes: 0,
                            seconds: 0,
                            milliseconds: 0,
                        })
                        .toDate(),
                    moment(request.dateTo)
                        .set({
                            hours: 23,
                            minutes: 59,
                            seconds: 59,
                            milliseconds: 999,
                        })
                        .toDate(),
                );
            }
        }

        return findOptions;
    }
}
