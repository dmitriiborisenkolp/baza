import { Injectable } from '@nestjs/common';
import {
    BazaNcDividendPaymentStatus,
    BazaNcDividendTransactionEntryCmsCreateRequest,
    BazaNcDividendTransactionEntryCmsDeleteRequest,
    BazaNcDividendTransactionEntryCmsGetByUlidRequest,
    BazaNcDividendTransactionEntryCmsListRequest,
    BazaNcDividendTransactionEntryCmsListResponse,
    BazaNcDividendTransactionEntryCmsUpdateRequest,
    BazaNcDividendTransactionEntryCsvDto,
    BazaNcDividendTransactionEntryDto,
    BazaNcDividendTransactionEntryExportCsvRequest,
    convertToCents,
} from '@scaliolabs/baza-nc-shared';
import {
    BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
    BazaNcDividendRepository,
    BazaNcDividendTransactionEntryEntity,
    BazaNcDividendTransactionEntryRepository,
    BazaNcDividendTransactionRepository,
    BazaNcInvestorAccountEntity,
    BazaNcInvestorAccountRepository,
    BazaNcOfferingRepository,
} from '../../../typeorm';
import { CrudCsvService, CrudService, CsvService } from '@scaliolabs/baza-core-api';
import { BazaNcDividendTransactionEntryMapper } from '../mappers/baza-nc-dividend-transaction-entry.mapper';
import { BazaNcDividendTransactionEntryIsLockedException } from '../exceptions/baza-nc-dividend-transaction-entry-is-locked.exception';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { Between, In } from 'typeorm';
import { bazaEmptyCrudListResponse } from '@scaliolabs/baza-core-shared';
import * as moment from 'moment';
import { BazaNcDividendTransactionEntryCsvMapper } from '../mappers/baza-nc-dividend-transaction-entry-csv.mapper';
import { BazaNcDividendProcessingService } from './baza-nc-dividend-processing.service';
import { BazaNcDividendActionsAccessService } from './baza-nc-dividend-actions-access.service';

/**
 * Dividend Transaction Entry CMS Service
 * The service is used mostly for Dividend Transaction Entry CMS API
 */
@Injectable()
export class BazaNcDividendTransactionEntryCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudCsv: CrudCsvService,
        private readonly csv: CsvService,
        private readonly access: BazaNcDividendActionsAccessService,
        private readonly mapper: BazaNcDividendTransactionEntryMapper,
        private readonly csvMapper: BazaNcDividendTransactionEntryCsvMapper,
        private readonly repository: BazaNcDividendTransactionEntryRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly dividendRepository: BazaNcDividendRepository,
        private readonly dividendTransactionRepository: BazaNcDividendTransactionRepository,
        private readonly dividendTransactionProcessing: BazaNcDividendProcessingService,
    ) {}

    /**
     * Creates a new Dividend Transaction Entry
     * @param request
     */
    async create(request: BazaNcDividendTransactionEntryCmsCreateRequest): Promise<BazaNcDividendTransactionEntryEntity> {
        const offering = request.offeringId ? await this.offeringRepository.getOfferingWithId(request.offeringId) : undefined;

        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(request.investorAccountId);
        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(request.dividendTransactionUlid);

        if (this.access.isDividendTransactionLocked(dividendTransaction)) {
            throw new BazaNcDividendTransactionEntryIsLockedException();
        }

        const entity = new BazaNcDividendTransactionEntryEntity();

        entity.dateCreatedAt = new Date();
        entity.date = new Date(request.date);
        entity.offering = offering;
        entity.investorAccount = investorAccount;
        entity.dividendTransaction = dividendTransaction;
        entity.source = request.source;
        entity.amountCents = request.amountCents;
        entity.status = BazaNcDividendPaymentStatus.Pending;

        await this.repository.save([entity]);

        dividendTransaction.dateUpdatedAt = new Date();
        await this.dividendTransactionRepository.save([dividendTransaction]);

        return entity;
    }

    /**
     * Updates existing Dividend Transaction Entry
     * @param request
     */
    async update(request: BazaNcDividendTransactionEntryCmsUpdateRequest): Promise<BazaNcDividendTransactionEntryEntity> {
        const entity = await this.repository.getByUlid(request.ulid);
        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(entity.dividendTransaction.ulid);

        if (this.access.isDividendTransactionLocked(dividendTransaction)) {
            throw new BazaNcDividendTransactionEntryIsLockedException();
        }

        if (!this.access.canUpdateDividendEntry(entity, entity.dividendTransaction)) {
            throw new BazaNcDividendTransactionEntryIsLockedException();
        }

        const offering = request.offeringId ? await this.offeringRepository.getOfferingWithId(request.offeringId) : null;

        entity.date = new Date(request.date);
        entity.dateUpdatedAt = new Date();
        entity.amountCents = request.amountCents;
        entity.offering = offering;

        await this.repository.save([entity]);

        dividendTransaction.dateUpdatedAt = new Date();
        await this.dividendTransactionRepository.save([dividendTransaction]);

        return entity;
    }

    /**
     * Deletes Dividend Transaction Entry
     * If Dividend Transaction is not Draft and all entries was removed from Dividend Transaction, Dividend Transactions
     * will be removed
     * @param request
     */
    async delete(request: BazaNcDividendTransactionEntryCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getByUlid(request.ulid);
        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(entity.dividendTransaction.ulid);

        if (this.access.isDividendTransactionLocked(entity.dividendTransaction)) {
            throw new BazaNcDividendTransactionEntryIsLockedException();
        }

        if (!this.access.canDeleteDividendEntry(entity, entity.dividendTransaction)) {
            throw new BazaNcDividendTransactionEntryIsLockedException();
        }

        await this.repository.remove([entity]);

        const dividendEntry = await this.dividendRepository.findByDwollaTransferId(entity.dwollaTransferId);

        if (dividendEntry) {
            await this.dividendRepository.remove([dividendEntry]);
        }

        const entries = await this.repository.findByDividendTransaction(dividendTransaction);

        if (entries.length > 0) {
            await this.dividendTransactionProcessing.updateDividendTransaction({
                dividendTransactionUlid: dividendTransaction.ulid,
                shouldBeMarkedAsStarted: false,
            });
        } else if (!this.access.isDividendTransactionDraft(dividendTransaction)) {
            await this.dividendTransactionRepository.remove([dividendTransaction]);
        } else {
            dividendTransaction.dateUpdatedAt = new Date();
            await this.dividendTransactionRepository.save([dividendTransaction]);
        }
    }

    /**
     * Returns Dividend Transaction Entry by ULID
     * @param request
     */
    async getByUlid(request: BazaNcDividendTransactionEntryCmsGetByUlidRequest): Promise<BazaNcDividendTransactionEntryEntity> {
        return this.repository.getByUlid(request.ulid);
    }

    /**
     * Returns list of Dividend Transaction Entries
     * @param request
     */
    async list(request: BazaNcDividendTransactionEntryCmsListRequest): Promise<BazaNcDividendTransactionEntryCmsListResponse> {
        const findOptions = await this.findOptionsFactory(request);

        if (findOptions) {
            return this.crud.find<BazaNcDividendTransactionEntryEntity, BazaNcDividendTransactionEntryDto>({
                request,
                findOptions,
                entity: BazaNcDividendTransactionEntryEntity,
                mapper: async (items) => this.mapper.entitiesToDTOs(items),
            });
        } else {
            return bazaEmptyCrudListResponse();
        }
    }

    /**
     * Exports list of Dividend Transaction Entry to CSV
     * @param request
     */
    async exportToCsv(request: BazaNcDividendTransactionEntryExportCsvRequest): Promise<string> {
        const findOptions = await this.findOptionsFactory(request.listRequest);

        return this.crudCsv.exportToCsv<BazaNcDividendTransactionEntryEntity, BazaNcDividendTransactionEntryCsvDto>({
            ...request,
            findOptions,
            entity: BazaNcDividendTransactionEntryEntity,
            mapper: async (items) => this.csvMapper.entitiesToDTOs(items),
        });
    }

    /**
     * TypeORM factory for Where conditions used by list methods
     * BazaNcDividendTransactionEntryCmsService.list
     * BazaNcDividendTransactionEntryCmsService.exportToCsv
     * @param request
     * @private
     */
    private async findOptionsFactory(
        request: BazaNcDividendTransactionEntryCmsListRequest,
    ): Promise<FindManyOptions<BazaNcDividendTransactionEntryEntity> | false> {
        const dividendTransaction = await this.dividendTransactionRepository.getByUlid(request.dividendTransactionUlid);

        const where: Array<FindConditions<BazaNcDividendTransactionEntryEntity>> = [];

        const findOptions: FindManyOptions<BazaNcDividendTransactionEntryEntity> = {
            order: {
                ulid: 'DESC',
            },
            relations: BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
            where,
        };

        if (request.queryString) {
            if (request.queryString.match(/^\$?(\d+)\.$/)) {
                request.queryString = request.queryString.replace('.', '');
            }

            const isNcAccountIdSearch = request.queryString.match(/^A(\d+)$/);
            const isNcPartyIdSearch = request.queryString.match(/^A(\d+)$/);
            const isNcOfferingIdSearch = request.queryString.match(/^(\d{5,})$/);
            const isAmountSearch = !isNcOfferingIdSearch && request.queryString.match(/^\$?\d+$|(?=^\$?[.\d]+$)^\$?\d+\.\d+$/);

            if (isAmountSearch) {
                if (!where.length) {
                    where.push({});
                }

                for (const whereConditions of where) {
                    whereConditions.amountCents = convertToCents(parseFloat(request.queryString.replace('$', '')));
                }
            } else if (isNcOfferingIdSearch) {
                const offerings = await this.offeringRepository.searchByOfferingId(request.queryString);

                if (offerings.length > 0) {
                    if (!where.length) {
                        where.push({});
                    }

                    for (const whereConditions of where) {
                        whereConditions.offering = In(offerings.map((next) => next.id)) as any;
                    }
                } else {
                    return false;
                }
            } else {
                const investorAccounts: Array<BazaNcInvestorAccountEntity> = await this.investorAccountRepository.searchByFullnameOrEmail(
                    request.queryString,
                );
                const offerings = await this.offeringRepository.searchByOfferingIdOrName(request.queryString);

                if (isNcAccountIdSearch) {
                    investorAccounts.push(...(await this.investorAccountRepository.searchByNcAccountId(request.queryString)));
                } else if (isNcPartyIdSearch) {
                    investorAccounts.push(...(await this.investorAccountRepository.searchByNcAccountId(request.queryString)));
                }

                if (investorAccounts.length > 0 || offerings.length > 0) {
                    if (investorAccounts.length > 0) {
                        where.push({
                            investorAccount: In(investorAccounts.map((next) => next.id)) as any,
                        });
                    }

                    if (offerings.length > 0) {
                        where.push({
                            offering: In(offerings.map((next) => next.id)) as any,
                        });
                    }
                } else {
                    return false;
                }
            }
        }

        if (request.dateFrom && request.dateTo) {
            if (!where.length) {
                where.push({});
            }

            for (const whereConditions of where) {
                whereConditions.date = Between(
                    moment(request.dateFrom)
                        .set({
                            hours: 0,
                            minutes: 0,
                            seconds: 0,
                            milliseconds: 0,
                        })
                        .toDate(),
                    moment(request.dateTo)
                        .set({
                            hours: 23,
                            minutes: 59,
                            seconds: 59,
                            milliseconds: 999,
                        })
                        .toDate(),
                );
            }
        }

        if (!where.length) {
            where.push({});
        }

        for (const whereConditions of where) {
            whereConditions.dividendTransaction = dividendTransaction;
        }

        return findOptions;
    }
}
