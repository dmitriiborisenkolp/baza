import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcDividendSuccessfulPaymentEvent } from '../events/baza-nc-dividend-successful-payment.event';
import { BazaNcDividendMailNotificationsService } from '../services/baza-nc-dividend-mail-notifications.service';
import { BazaRegistryService, cqrs } from '@scaliolabs/baza-core-api';

/**
 * The Event Handlers sends email notifications to Investors about successful dividend payments
 */
@EventsHandler(BazaNcDividendSuccessfulPaymentEvent)
export class SendUserMailNotificationAboutSuccessfulPaymentsEventHandler implements IEventHandler<BazaNcDividendSuccessfulPaymentEvent> {
    constructor(
        private readonly registry: BazaRegistryService,
        private readonly notificationService: BazaNcDividendMailNotificationsService,
    ) {}

    handle(event: BazaNcDividendSuccessfulPaymentEvent): void {
        cqrs(SendUserMailNotificationAboutSuccessfulPaymentsEventHandler.name, async () => {
            if (this.registry.getValue('bazaNc.enableDividendsSuccessfulEmails')) {
                await this.notificationService.sendNotificationAboutSuccessfulPaymentsToInvestor(event.payload.dividends);
            }
        });
    }
}
