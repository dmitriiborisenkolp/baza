import { BazaNcDividendEntity, BazaNcDividendTransactionEntity } from '../../../typeorm';

/**
 * Successful Dividend Payments
 * The event executes when Dividend Transaction Entry successfully processes
 * It's used for email notifications and some other effects
 */
export class BazaNcDividendSuccessfulPaymentEvent {
    constructor(
        public readonly payload: {
            dividends: Array<BazaNcDividendEntity>;
            dividendTransaction?: BazaNcDividendTransactionEntity;
        },
    ) {}
}
