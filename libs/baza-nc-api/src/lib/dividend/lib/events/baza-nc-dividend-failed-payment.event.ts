import { BazaNcDividendTransactionEntity, BazaNcDividendTransactionEntryEntity } from '../../../typeorm';

/**
 * Failed to Pay Dividends
 * The event executes when Dividend Transaction Entry was failed to process
 * It's used for email notifications and some other effects
 */
export class BazaNcDividendFailedPaymentEvent {
    constructor(
        public readonly payload: {
            entries: Array<BazaNcDividendTransactionEntryEntity>;
            dividendTransaction?: BazaNcDividendTransactionEntity;
        },
    ) {}
}
