import { Injectable } from '@nestjs/common';
import { BazaNcDividendTransactionEntryEntity, BazaNcInvestorAccountRepository } from '../../../typeorm';
import { BazaNcDividendTransactionEntryCsvDto, convertFromCents } from '@scaliolabs/baza-nc-shared';
import * as moment from 'moment';

/**
 * BazaNcDividendTransactionEntryEntity to BazaNcDividendTransactionEntryCsvDto Mapper
 */
@Injectable()
export class BazaNcDividendTransactionEntryCsvMapper {
    constructor(private readonly investorAccountRepository: BazaNcInvestorAccountRepository) {}

    async entityToDTO(input: BazaNcDividendTransactionEntryEntity): Promise<BazaNcDividendTransactionEntryCsvDto> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(input.investorAccount.id);

        return {
            ulid: input.ulid,
            date: moment(input.date).format('MM/DD/YYYY'),
            dateCreatedAt: moment(input.dateCreatedAt).format('MM/DD/YYYY'),
            dateUpdatedAt: input.dateUpdatedAt ? moment(input.dateUpdatedAt).format('MM/DD/YYYY') : 'N/A',
            dateProcessedAt: input.dateProcessedAt ? moment(input.dateProcessedAt).format('MM/DD/YYYY') : 'N/A',
            source: input.source,
            amount: convertFromCents(input.amountCents).toFixed(2),
            status: input.status,
            offering: input.offering?.ncOfferingName || 'N/A',
            ncOfferingId: input.offering?.ncOfferingId || 'N/A',
            ncAccountId: investorAccount.northCapitalAccountId || 'N/A',
            ncPartyId: investorAccount.northCapitalPartyId || 'N/A',
            investorAccount: investorAccount.user.fullName,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcDividendTransactionEntryEntity>): Promise<Array<BazaNcDividendTransactionEntryCsvDto>> {
        return Promise.all(input.map((next) => this.entityToDTO(next)));
    }
}
