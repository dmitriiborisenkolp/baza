import { Injectable } from '@nestjs/common';
import { BazaNcDividendTransactionEntity } from '../../../typeorm';
import { BazaNcDividendTransactionDto, BazaNcDividendTransactionStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendActionsAccessService } from '../services/baza-nc-dividend-actions-access.service';

/**
 * BazaNcDividendTransactionEntity to BazaNcDividendTransactionDto Mapper
 */
@Injectable()
export class BazaNcDividendTransactionMapper {
    constructor(private readonly access: BazaNcDividendActionsAccessService) {}

    async entityToDTO(input: BazaNcDividendTransactionEntity): Promise<BazaNcDividendTransactionDto> {
        return {
            ulid: input.ulid,
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            dateUpdatedAt: input.dateUpdatedAt?.toISOString(),
            dateProcessedAt: input.dateProcessedAt?.toISOString(),
            status: input.status,
            title: input.title,
            canBeDeleted: await this.access.canDeleteDividendTransaction(input),
            isLocked: this.access.isDividendTransactionLocked(input),
            isProcessing: input.status === BazaNcDividendTransactionStatus.InProgress,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcDividendTransactionEntity>): Promise<Array<BazaNcDividendTransactionDto>> {
        return Promise.all(input.map((next) => this.entityToDTO(next)));
    }
}
