import { Injectable } from '@nestjs/common';
import { BazaNcDividendEntity, BazaNcInvestorAccountRepository } from '../../../typeorm';
import { BazaNcDividendCmsCsvDto, convertFromCents } from '@scaliolabs/baza-nc-shared';
import * as moment from 'moment';

/**
 * BazaNcDividendEntity to BazaNcDividendCmsCsvDto Mapper
 */
@Injectable()
export class BazaNcDividendCmsCsvMapper {
    constructor(private readonly investorAccountRepository: BazaNcInvestorAccountRepository) {}

    async entityToDTO(input: BazaNcDividendEntity): Promise<BazaNcDividendCmsCsvDto> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(input.investorAccount.id);

        return {
            ulid: input.ulid,
            source: input.source,
            date: moment(input.date).format('MM/DD/YYYY'),
            investorAccount: investorAccount.user.fullName,
            ncAccountId: investorAccount.northCapitalAccountId || 'N/A',
            ncPartyId: investorAccount.northCapitalPartyId || 'N/A',
            amount: `${convertFromCents(input.amountCents).toFixed(2)}`,
            status: input.status,
            offering: input.offering?.ncOfferingName || 'N/A',
            ncOfferingId: input.offering?.ncOfferingId || 'N/A',
        };
    }

    async entitiesToDTOs(input: Array<BazaNcDividendEntity>): Promise<Array<BazaNcDividendCmsCsvDto>> {
        return Promise.all(input.map((next) => this.entityToDTO(next)));
    }
}
