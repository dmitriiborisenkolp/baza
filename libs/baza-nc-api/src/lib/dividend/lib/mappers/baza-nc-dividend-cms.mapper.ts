import { Injectable } from '@nestjs/common';
import { BazaNcDividendMapper } from './baza-nc-dividend.mapper';
import { BazaNcDividendCmsDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountMapper } from '../../../investor-account/lib/mappers/baza-nc-investor-account.mapper';
import { BazaNcDividendEntity, BazaNcInvestorAccountRepository } from '../../../typeorm';

/**
 * BazaNcDividendEntity to BazaNcDividendCmsDto mapper
 * @see BazaNcDividendMapper
 */
@Injectable()
export class BazaNcDividendCmsMapper {
    constructor(
        private readonly baseMapper: BazaNcDividendMapper,
        private readonly investorAccountMapper: BazaNcInvestorAccountMapper,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    async entityToDTO(input: BazaNcDividendEntity): Promise<BazaNcDividendCmsDto> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(input.investorAccount.id);

        return {
            ...(await this.baseMapper.entityToDTO(input)),
            investorAccount: await this.investorAccountMapper.entityToDTO(investorAccount),
        };
    }

    async entitiesToDTOs(input: Array<BazaNcDividendEntity>): Promise<Array<BazaNcDividendCmsDto>> {
        return Promise.all(input.map((next) => this.entityToDTO(next)));
    }
}
