import { Injectable } from '@nestjs/common';
import { BazaNcDividendTransactionEntity } from '../../../typeorm';
import { BazaNcDividendTransactionCsvDto } from '@scaliolabs/baza-nc-shared';
import * as moment from 'moment';

/**
 * BazaNcDividendTransactionEntity to BazaNcDividendTransactionCsvDto Mapper
 */
@Injectable()
export class BazaNcDividendTransactionCsvMapper {
    entityToDTO(input: BazaNcDividendTransactionEntity): BazaNcDividendTransactionCsvDto {
        return {
            ulid: input.ulid,
            dateCreatedAt: moment(input.dateCreatedAt).format('MM/DD/YYYY'),
            dateUpdatedAt: input.dateUpdatedAt ? moment(input.dateUpdatedAt).format('MM/DD/YYYY') : 'N/A',
            dateProcessedAt: input.dateProcessedAt ? moment(input.dateProcessedAt).format('MM/DD/YYYY') : 'N/A',
            status: input.status,
            title: input.title,
        };
    }

    entitiesToDTOs(input: Array<BazaNcDividendTransactionEntity>): Array<BazaNcDividendTransactionCsvDto> {
        return input.map((next) => this.entityToDTO(next));
    }
}
