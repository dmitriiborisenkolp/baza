import { Injectable } from '@nestjs/common';
import { BazaNcDividendDto, bazaNcOfferingName, convertFromCents } from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendEntity } from '../../../typeorm';
import { BazaNcDividendActionsAccessService } from '../services/baza-nc-dividend-actions-access.service';

/**
 * BazaNcDividendEntity to BazaNcDividendDto Mapper
 */
@Injectable()
export class BazaNcDividendMapper {
    constructor(private readonly access: BazaNcDividendActionsAccessService) {}

    async entityToDTO(input: BazaNcDividendEntity): Promise<BazaNcDividendDto> {
        return {
            ulid: input.ulid,
            source: input.source,
            offeringId: input.offering ? input.offering.ncOfferingId : undefined,
            offeringTitle: bazaNcOfferingName(input.offering),
            date: input.date.toISOString(),
            amount: convertFromCents(input.amountCents).toFixed(2),
            amountCents: input.amountCents,
            status: input.status,
            canBeReprocessed: this.access.canReprocessDividend(input),
            history: input.history,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcDividendEntity>): Promise<Array<BazaNcDividendDto>> {
        return Promise.all(input.map((next) => this.entityToDTO(next)));
    }
}
