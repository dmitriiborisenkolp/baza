import { Injectable } from '@nestjs/common';
import { BazaNcDividendTransactionEntryEntity, BazaNcInvestorAccountRepository } from '../../../typeorm';
import { BazaNcInvestorAccountMapper } from '../../../investor-account';
import { BazaNcDividendTransactionEntryDto, bazaNcOfferingName, convertFromCents } from '@scaliolabs/baza-nc-shared';
import { BazaNcDividendActionsAccessService } from '../services/baza-nc-dividend-actions-access.service';

/**
 * BazaNcDividendTransactionEntryEntity to BazaNcDividendTransactionEntryDto Mapper
 */
@Injectable()
export class BazaNcDividendTransactionEntryMapper {
    constructor(
        private readonly access: BazaNcDividendActionsAccessService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly investorAccountMapper: BazaNcInvestorAccountMapper,
    ) {}

    async entityToDTO(input: BazaNcDividendTransactionEntryEntity): Promise<BazaNcDividendTransactionEntryDto> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(input.investorAccount.id);

        return {
            ulid: input.ulid,
            date: input.date.toISOString(),
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            dateUpdatedAt: input.dateUpdatedAt?.toISOString(),
            dateProcessedAt: input.dateProcessedAt?.toISOString(),
            offeringId: input.offering?.ncOfferingId,
            offeringTitle: bazaNcOfferingName(input.offering),
            source: input.source,
            investorAccount: this.investorAccountMapper.entityToDTO(investorAccount),
            amount: convertFromCents(input.amountCents).toFixed(2),
            amountCents: input.amountCents,
            status: input.status,
            canBeReprocessed: this.access.canReprocessDividendEntry(input, input.dividendTransaction),
            canBeUpdated: this.access.canUpdateDividendEntry(input, input.dividendTransaction),
            canBeDeleted: this.access.canDeleteDividendEntry(input, input.dividendTransaction),
            failureReason: input.failureReason,
            dwollaTransferId: input.dwollaTransferId,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcDividendTransactionEntryEntity>): Promise<Array<BazaNcDividendTransactionEntryDto>> {
        return Promise.all(input.map((next) => this.entityToDTO(next)));
    }
}
