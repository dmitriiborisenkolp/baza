import { Injectable } from '@nestjs/common';
import { convertFromCents, NcTransactionCsvDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcTransactionMapper } from './baza-nc-transaction.mapper';
import { BazaNcTransactionEntity } from '../../../typeorm';

@Injectable()
export class BazaNcTransactionCsvMapper {
    constructor(private readonly baseMapper: BazaNcTransactionMapper) {}

    async entityToCSV(input: BazaNcTransactionEntity): Promise<NcTransactionCsvDto> {
        return {
            ...(await this.baseMapper.entityToDTO(input)),
            pricePerShare: `$${convertFromCents(input.pricePerShareCents).toFixed(2)}`,
            amount: `$${convertFromCents(input.amountCents).toFixed(2)}`,
            fee: `$${convertFromCents(input.feeCents).toFixed(2)}`,
            total: `$${convertFromCents(input.totalCents).toFixed(2)}`,
        };
    }

    async entitiesToCSVs(input: Array<BazaNcTransactionEntity>): Promise<Array<NcTransactionCsvDto>> {
        const result: Array<NcTransactionCsvDto> = [];

        for (const entity of input) {
            result.push(await this.entityToCSV(entity));
        }

        return result;
    }
}
