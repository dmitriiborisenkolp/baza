import { Injectable } from '@nestjs/common';
import { NcTransactionDto } from '@scaliolabs/baza-nc-shared';
import { Connection } from 'typeorm';
import { OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcTransactionEntity } from '../../../typeorm';

export type PurchaseNameFactory = (connection: Connection, ncOfferingId: OfferingId, ncOfferingName: string) => Promise<string>;

@Injectable()
export class BazaNcTransactionMapper {
    private _purchaseNameFactory: PurchaseNameFactory = async (connection, ncOfferingId, ncOfferingName) => ncOfferingName || 'Unknown';

    constructor(private readonly connection: Connection) {}

    set purchaseNameFactory(callback: PurchaseNameFactory) {
        this._purchaseNameFactory = callback;
    }

    async entityToDTO(input: BazaNcTransactionEntity): Promise<NcTransactionDto> {
        const ncOfferingName = input.ncOfferingName?.replace('u2013', '–');

        return {
            id: input.id,
            createdAt: input.createdAt?.toISOString(),
            accountId: input.account?.id,
            accountFullName: input.account?.fullName,
            investorAccountId: input.investorAccount?.id,
            ncOfferingId: input.ncOfferingId,
            ncOfferingName,
            ncTradeId: input.ncTradeId,
            ncOrderStatus: input.ncOrderStatus,
            ncAccountId: input.ncAccountId,
            amountCents: input.amountCents,
            feeCents: input.feeCents,
            transactionFeesCents: input.transactionFeesCents,
            totalCents: input.totalCents,
            pricePerShareCents: input.pricePerShareCents,
            shares: input.shares,
            state: input.state,
            name: await this._purchaseNameFactory(this.connection, input.ncOfferingId, ncOfferingName),
        };
    }

    async entitiesToDTOs(input: Array<BazaNcTransactionEntity>): Promise<Array<NcTransactionDto>> {
        const results: Array<NcTransactionDto> = [];

        for (const entity of input) {
            results.push(await this.entityToDTO(entity));
        }

        return results;
    }
}
