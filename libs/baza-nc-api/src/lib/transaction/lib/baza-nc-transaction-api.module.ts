import { Global, Module } from '@nestjs/common';
import { BazaNcTransactionsService } from './services/baza-nc-transactions.service';
import { BazaNcTransactionMapper } from './mappers/baza-nc-transaction.mapper';
import { BazaNcTransactionsController } from './controllers/baza-nc-transactions.controller';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { CqrsModule } from '@nestjs/cqrs';
import { TransactionCmsController } from './controllers/baza-nc-transactions-cms.controller';
import { PurchaseFlowSessionSubmittedEventHandler } from './event-handlers/purchase-flow-session-submitted-event.handler';
import { TradeStatusUpdatedEventHandler } from './event-handlers/trade-status-updated.event-handler';
import { OfferingUpdatedEventHandler } from './event-handlers/offering-updated.event-handler';
import { PurchaseFlowSessionCreatedEventHandler } from './event-handlers/purchase-flow-session-created-event.handler';
import { BazaNcTransactionInvestmentsService } from './services/baza-nc-transaction-investments.service';
import { BazaNcTransactionCsvMapper } from './mappers/baza-nc-transaction-csv.mapper';
import { PurchaseFlowSessionUpdatedEventHandler } from './event-handlers/purchase-flow-session-updated.event-handler';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { BazaNcWebhookApiModule } from '../../webhook';
import { BazaNcInvestorAccountApiModule } from '../../investor-account';
import { TradeCancelledEventHandler } from './event-handlers/trade-cancelled.event-handler';
import { TradeStatusUpdateService } from './services/trade-status-update.service';
import { TransactionReturnedEventService } from './event-handlers/purchase-flow-transaction-returned.event.service';

const eventHandlers = [
    PurchaseFlowSessionCreatedEventHandler,
    PurchaseFlowSessionUpdatedEventHandler,
    PurchaseFlowSessionSubmittedEventHandler,
    TradeStatusUpdatedEventHandler,
    TradeCancelledEventHandler,
    TransactionReturnedEventService,
    OfferingUpdatedEventHandler,
];

@Global()
@Module({
    imports: [CqrsModule, BazaNcTypeormApiModule, BazaCrudApiModule, BazaNcWebhookApiModule, BazaNcInvestorAccountApiModule],
    controllers: [BazaNcTransactionsController, TransactionCmsController],
    providers: [
        BazaNcTransactionsService,
        BazaNcTransactionMapper,
        BazaNcTransactionInvestmentsService,
        BazaNcTransactionCsvMapper,
        TradeStatusUpdateService,
        ...eventHandlers,
    ],
    exports: [BazaNcTransactionsService, BazaNcTransactionMapper, BazaNcTransactionCsvMapper, BazaNcTransactionInvestmentsService],
})
export class BazaNcTransactionApiModule {}
