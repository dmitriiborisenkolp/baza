import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcTransactionCmsEndpoint,
    BazaNcTransactionCmsEndpointPaths,
    ExportToCsvTransactionCmsRequest,
    GetByIdTransactionCmsRequest,
    GetByIdTransactionCmsResponse,
    ListTransactionsCmsRequest,
    ListTransactionsCmsResponse,
    NcTransactionDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaNorthCapitalAcl, BazaNorthCapitalCMSOpenApi } from '@scaliolabs/baza-nc-shared';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import { WithAccessGuard } from '@scaliolabs/baza-core-api';
import { AclNodes } from '@scaliolabs/baza-core-api';
import { BazaNcTransactionMapper } from '../mappers/baza-nc-transaction.mapper';
import { BazaNcTransactionsService } from '../services/baza-nc-transactions.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalTransactionsCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcTransactions])
export class TransactionCmsController implements BazaNcTransactionCmsEndpoint {
    constructor(private readonly service: BazaNcTransactionsService, private readonly mapper: BazaNcTransactionMapper) {}

    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Transactions',
    })
    @ApiOkResponse({
        type: ListTransactionsCmsResponse,
    })
    @Post(BazaNcTransactionCmsEndpointPaths.list)
    async list(@Body() request: ListTransactionsCmsRequest): Promise<ListTransactionsCmsResponse> {
        return this.service.listEntities(request);
    }

    @ApiOperation({
        summary: 'getById',
        description: 'Returns Transaction by ID',
    })
    @ApiOkResponse({
        type: NcTransactionDto,
    })
    @Post(BazaNcTransactionCmsEndpointPaths.getById)
    async getById(@Body() request: GetByIdTransactionCmsRequest): Promise<GetByIdTransactionCmsResponse> {
        const entity = await this.service.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @ApiOperation({
        summary: 'exportToCsv',
        description: 'Exports list of Transactions to CSV',
    })
    @ApiOkResponse({
        type: String,
    })
    @Post(BazaNcTransactionCmsEndpointPaths.exportToCsv)
    async exportToCsv(@Body() request: ExportToCsvTransactionCmsRequest): Promise<string> {
        return this.service.exportToCsv(request);
    }
}
