import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaNorthCapitalOpenApi } from '@scaliolabs/baza-nc-shared';
import { AccountEntity, AuthGuard, ReqAccount, ReqAccountId } from '@scaliolabs/baza-core-api';
import {
    NcTransactionDto,
    BazaNcTransactionEndpoint,
    BazaNcTransactionEndpointPaths,
    BazaNcTransactionGetByIdRequest,
    BazaNcTransactionGetByIdResponse,
    BazaNcTransactionListRequest,
    BazaNcTransactionListResponse,
    BazaNcTransactionInvestmentsListRequest,
    BazaNcTransactionInvestmentsResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcTransactionsService } from '../services/baza-nc-transactions.service';
import { BazaNcTransactionMapper } from '../mappers/baza-nc-transaction.mapper';
import { CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { BazaNcTransactionInvestmentsService } from '../services/baza-nc-transaction-investments.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalTransactions)
@UseGuards(AuthGuard)
export class BazaNcTransactionsController implements BazaNcTransactionEndpoint {
    constructor(
        private readonly service: BazaNcTransactionsService,
        private readonly investmentsService: BazaNcTransactionInvestmentsService,
        private readonly mapper: BazaNcTransactionMapper,
    ) {}

    @Post(BazaNcTransactionEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List of user\'s purchases. Check "getById" endpoint to review DTO',
    })
    @ApiOkResponse({
        type: CrudListResponseDto,
    })
    async list(@Body() request: BazaNcTransactionListRequest, @ReqAccountId() accountId: number): Promise<BazaNcTransactionListResponse> {
        return await this.service.listEntities({
            ...request,
            accountId: accountId,
        });
    }

    @Post(BazaNcTransactionEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns transaction by id',
    })
    @ApiOkResponse({
        type: NcTransactionDto,
    })
    async getById(@Body() request: BazaNcTransactionGetByIdRequest): Promise<BazaNcTransactionGetByIdResponse> {
        const entity = await this.service.getById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcTransactionEndpointPaths.investments)
    @ApiOperation({
        summary: 'investments',
        description: 'Returns investment details',
    })
    @ApiOkResponse({
        type: BazaNcTransactionInvestmentsResponse,
    })
    async investments(
        @Body() request: BazaNcTransactionInvestmentsListRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcTransactionInvestmentsResponse> {
        return this.investmentsService.listInvestmentsOfAccount(account, request);
    }
}
