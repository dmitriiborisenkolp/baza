import { Injectable } from '@nestjs/common';
import { BazaNcTransactionEntity } from '../../../typeorm';
import { OrderStatus, TransactionState, mapTransactionState } from '@scaliolabs/baza-nc-shared';

@Injectable()
export class TradeStatusUpdateService {
    private readonly transactionStatePriorities = [
        TransactionState.PendingPayment,
        TransactionState.Created,
        TransactionState.PaymentReturned,
        // TODO: need to discuss the priority of the rest
    ];

    getTransactionStateByOrderStatus(orderStatus: OrderStatus, transaction: BazaNcTransactionEntity): TransactionState {
        const state = mapTransactionState.find((c) => c.match.includes(orderStatus)).transactionState;

        const state_order = this.transactionStatePriorities.indexOf(state);

        if (state_order === -1 || state_order > this.transactionStatePriorities.indexOf(transaction.state)){
            return state;
        }
        else{
            return transaction.state
        }
    }
}
