import { Injectable } from '@nestjs/common';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { paginate } from '@scaliolabs/baza-core-shared';
import {
    convertFromCents,
    NcOfferingInvestmentDto,
    TRANSACTION_INVESTMENTS_COUNTABLE_STATES,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { BazaNcOfferingListItemMapper } from '../../../offering';

@Injectable()
export class BazaNcTransactionInvestmentsService {
    constructor(
        private readonly offerings: BazaNcOfferingRepository,
        private readonly transactions: BazaNcTransactionRepository,
        private readonly offeringMapper: BazaNcOfferingListItemMapper,
    ) {}

    async listInvestmentsOfAccount(
        account: AccountEntity,
        request: CrudListRequestDto<NcOfferingInvestmentDto>,
    ): Promise<CrudListResponseDto<NcOfferingInvestmentDto>> {
        const allInvestments = await this.allInvestmentsOfAccount(account);

        const offset = Math.max(0, request.index - 1) * request.size;

        return {
            items: paginate<NcOfferingInvestmentDto>(allInvestments, offset, request.size),
            pager: {
                total: allInvestments.length,
                size: request.size,
                index: request.index,
            },
        };
    }

    private async allInvestmentsOfAccount(account: AccountEntity): Promise<Array<NcOfferingInvestmentDto>> {
        const offerings = await this.offerings.findAll();
        const investments: Array<NcOfferingInvestmentDto> = [];

        for (const offering of offerings) {
            const transactions = await this.transactions.repository.find({
                relations: this.transactions.relations,
                where: [
                    {
                        account,
                        ncOfferingId: offering.ncOfferingId,
                    },
                ],
                order: {
                    id: 'DESC',
                },
            });

            let purchasedShares = 0;
            let purchasedAmountTotal = 0;
            let purchasedAmountTotalCents = 0;
            let lastTransactionState: TransactionState = undefined;

            for (const transaction of transactions) {
                if (TRANSACTION_INVESTMENTS_COUNTABLE_STATES.includes(transaction.state)) {
                    if (!lastTransactionState) {
                        lastTransactionState = transaction.state;
                    }

                    purchasedShares += transaction.shares;
                    purchasedAmountTotal += convertFromCents(transaction.amountCents);
                    purchasedAmountTotalCents += transaction.amountCents;
                }
            }

            investments.push({
                ncOffering: this.offeringMapper.entityToDTO(offering),
                lastTransactionState,
                purchasedShares,
                purchasedAmountTotal: purchasedAmountTotal.toFixed(2),
                purchasedAmountTotalCents,
            });
        }

        return investments.filter((dto) => !!dto.lastTransactionState);
    }
}
