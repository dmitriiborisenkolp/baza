import { Injectable } from '@nestjs/common';
import { CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { CrudCsvService, CrudService } from '@scaliolabs/baza-core-api';
import {
    ExportToCsvTransactionCmsRequest,
    ListTransactionsCmsRequest,
    NcTransactionCsvDto,
    NcTransactionDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaAccountRepository } from '@scaliolabs/baza-core-api';
import { ILike, In } from 'typeorm';
import { postgresLikeEscape } from '@scaliolabs/baza-core-shared';
import { BazaNcTransactionMapper } from '../mappers/baza-nc-transaction.mapper';
import { BazaNcTransactionCsvMapper } from '../mappers/baza-nc-transaction-csv.mapper';
import { BazaNcOfferingRepository, BazaNcTransactionEntity, BazaNcTransactionRepository } from '../../../typeorm';
import { FindConditions } from 'typeorm/browser/find-options/FindConditions';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';

const NC_TRANSACTION_RELATIONS = ['account', 'investorAccount'];

@Injectable()
export class BazaNcTransactionsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudCsvService: CrudCsvService,
        private readonly repository: BazaNcTransactionRepository,
        private readonly accountRepository: BazaAccountRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
        private readonly mapper: BazaNcTransactionMapper,
        private readonly csvMapper: BazaNcTransactionCsvMapper,
    ) {}

    async getById(id: number): Promise<BazaNcTransactionEntity> {
        return this.repository.getById(id);
    }

    async listEntities(request: ListTransactionsCmsRequest, options = { reverse: false }): Promise<CrudListResponseDto<NcTransactionDto>> {
        const findOptions = await this.prepareOptions(request, options);

        return this.crud.find({
            request,
            entity: BazaNcTransactionEntity,
            findOptions,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });
    }

    async exportToCsv(request: ExportToCsvTransactionCmsRequest): Promise<string> {
        const findOptions = await this.prepareOptions(request.listRequest);

        return this.crudCsvService.exportToCsv<BazaNcTransactionEntity, NcTransactionCsvDto>({
            entity: BazaNcTransactionEntity,
            request: request.listRequest,
            fields: request.fields,
            delimiter: request.delimiter,
            mapper: async (items) => this.csvMapper.entitiesToCSVs(items),
            findOptions,
        });
    }

    /**
     * Utility method to filter the list. Used by `listEntities` & `exportToCsv`
     * @param request Request object.
     * @returns FindManyOptions ref
     */
    async prepareOptions(request: ListTransactionsCmsRequest, options?): Promise<FindManyOptions> {
        const where: Array<FindConditions<BazaNcTransactionEntity>> = [];
        const order: { createdAt?: 'ASC' | 'DESC'; id?: 'ASC' | 'DESC' } = {};

        if (options) {
            order.createdAt = options.reverse ? 'ASC' : 'DESC';
        } else {
            order.id = 'DESC';
        }

        const findOptions = await this.crud.findOptions<BazaNcTransactionEntity>(
            {
                relations: NC_TRANSACTION_RELATIONS,
                order,
                where,
            },
            request,
        );

        if (request.queryString) {
            const accounts = await this.accountRepository.repository.find({
                select: ['id'],
                where: [
                    {
                        fullName: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                    },
                ],
            });

            const offerings = await this.offeringRepository.repository.find({
                select: ['ncOfferingId'],
                where: [
                    {
                        ncIssueName: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                    },
                ],
            });

            where.push(
                ...([
                    { account: In(accounts.map((account) => account.id)) },
                    { ncOfferingId: In(offerings.map((offering) => offering.ncOfferingId)) },
                ] as any),
            );
        } else {
            if (request.accountId) {
                where.push({
                    account: request.accountId as any,
                });
            }

            if (request.ncOfferingId) {
                where.push({
                    ncOfferingId: request.ncOfferingId,
                });
            }
        }

        if (request.isSubmitted === true || request.isSubmitted === false) {
            if (!where.length) {
                where.push({});
            }

            for (const conditions of where) {
                conditions.submitted = request.isSubmitted;

                if (request.status) {
                    conditions.state = In(request.status) as any;
                }
            }
        }

        return findOptions;
    }
}
