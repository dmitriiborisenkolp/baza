import { BazaLogger, BazaRegistryService, cqrs, MailService } from '@scaliolabs/baza-core-api';
import { BazaRegistryNodeEmailRecipientValue, replaceTags } from '@scaliolabs/baza-core-shared';
import { BazaNcTransactionReturnedEvent } from '@scaliolabs/baza-nc-shared';
import { BazaNcMail } from '../../../../constants/baza-nc.mail-templates';
import { BazaNcInvestorAccountRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';

@EventsHandler(BazaNcTransactionReturnedEvent)
export class TransactionReturnedEventService implements IEventHandler<BazaNcTransactionReturnedEvent> {
    constructor(
        private readonly logger: BazaLogger,
        private readonly transactions: BazaNcTransactionRepository,
        private readonly mail: MailService,
        private readonly registry: BazaRegistryService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    handle(event: BazaNcTransactionReturnedEvent): void {
        cqrs(TransactionReturnedEventService.name, async () => {
            const tradeId = event.request.ncTradeId;
            const transaction = await this.transactions.findByNcTradeId(tradeId);

            if (!transaction) {
                return;
            }

            if (!transaction) {
                this.logger.error(`[OnPurchaseFlowTradeSubmittedEventHandler] User purchase was not found for tradeId ${tradeId}`);
                return;
            }

            const investorAccount = await this.investorAccountRepository.findInvestorAccountByNcAccountId(transaction.ncAccountId);

            const admin: BazaRegistryNodeEmailRecipientValue = this.registry.getValue('adminEmail');

            const variables = {
                investorName: investorAccount.user.fullName,
                clientName: this.registry.getValue('bazaCommon.clientName'),
            };

            await this.mail.send({
                name: BazaNcMail.BazaNcPaymentReturned,
                uniqueId: `baza_nc_integrations_payment_returned_${event.request.ncTradeId}`,
                to: [
                    {
                        name: admin.name,
                        email: admin.email,
                        cc: [
                            ...admin.cc,
                            {
                                name: investorAccount.user.fullName,
                                email: investorAccount.user.email,
                            },
                        ],
                    },
                    {
                        name: investorAccount.user.fullName,
                        email: investorAccount.user.email,
                    },
                ],
                subject: replaceTags('baza-nc.mail.paymentFailed.subject', {
                    clientName: this.registry.getValue('bazaCommon.clientName'),
                }),
                variables: () => variables,
            });
        });
    }
}
