import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import {
    BazaNcPurchaseFlowSessionUpdated,
    BazaNcPurchaseFlowTransactionType,
    purchaseFlowTransactionTypeToNcTransactionTypeMap,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcTransactionRepository } from '../../../typeorm';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcPurchaseFlowSessionUpdated)
export class PurchaseFlowSessionUpdatedEventHandler implements IEventHandler<BazaNcPurchaseFlowSessionUpdated> {
    constructor(private readonly repository: BazaNcTransactionRepository) {}

    handle(event: BazaNcPurchaseFlowSessionUpdated): void {
        cqrs(PurchaseFlowSessionUpdatedEventHandler.name, async () => {
            const entity = await this.repository.findByNcTradeId(event.request.ncTradeId);

            if (!entity) {
                return;
            }

            if (event.request.transactionType) {
                const transactionTypeDef = purchaseFlowTransactionTypeToNcTransactionTypeMap.find(
                    (def) => def.purchaseFlowTransactionType === (event.request.transactionType || BazaNcPurchaseFlowTransactionType.ACH),
                );

                entity.transactionType = transactionTypeDef.purchaseFlowTransactionType;
            }

            entity.amountCents = event.request.amountAsCents;
            entity.feeCents = event.request.feeAsCents;
            entity.totalCents = event.request.totalAsCents;
            entity.shares = event.request.numberOfShares;
            entity.pricePerShareCents = event.request.pricePerSharesAsCents;

            await this.repository.save(entity);
        });
    }
}
