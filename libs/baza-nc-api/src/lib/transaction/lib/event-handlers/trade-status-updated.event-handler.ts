import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcTradeStatusUpdatedEvent, BazaNcWebhookEvent, NorthCapitalTopic, TradeDetails } from '@scaliolabs/baza-nc-shared';
import { BazaNcTransactionEntity, BazaNcTransactionRepository } from '../../../typeorm';
import { TradeStatusUpdateService } from '../services/trade-status-update.service';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcWebhookEvent)
export class TradeStatusUpdatedEventHandler implements IEventHandler<BazaNcWebhookEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly transactionsRepository: BazaNcTransactionRepository,
        private readonly tradeStatusUpdateService: TradeStatusUpdateService,
    ) {}

    handle(event: BazaNcWebhookEvent): void {
        cqrs(TradeStatusUpdatedEventHandler.name, async () => {
            const message = event.message;

            switch (message.topic) {
                case NorthCapitalTopic.updateTradeStatus: {
                    const tradeId = message.responseBody.tradeId;
                    const trade = await this.transactionsRepository.findByNcTradeId(tradeId);

                    if (trade) {
                        await this.updateTransactionState(trade, message.responseBody);
                    }
                }
            }
        });
    }

    private async updateTransactionState(trade: BazaNcTransactionEntity, responseBody: TradeDetails): Promise<void> {
        const previousOrderStatus = trade.ncOrderStatus;
        const previousTransactionState = trade.state;

        trade.ncOrderStatus = responseBody.orderStatus;
        trade.state = this.tradeStatusUpdateService.getTransactionStateByOrderStatus(responseBody.orderStatus, trade);

        await this.transactionsRepository.save(trade);

        this.eventBus.publish(
            new BazaNcTradeStatusUpdatedEvent({
                id: trade.id,
                ncTradeId: trade.ncTradeId,
                ncOfferingId: trade.ncOfferingId,
                previousOrderStatus,
                previousTransactionState,
                newOrderStatus: trade.ncOrderStatus,
                newTransactionState: trade.state,
            }),
        );
    }
}
