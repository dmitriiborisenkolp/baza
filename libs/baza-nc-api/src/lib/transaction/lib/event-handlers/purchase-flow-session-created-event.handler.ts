import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaAccountRepository, cqrs } from '@scaliolabs/baza-core-api';
import { bazaNcOfferingName, BazaNcPurchaseFlowSessionCreated } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountRepository, BazaNcTransactionEntity, BazaNcTransactionRepository } from '../../../typeorm';

@EventsHandler(BazaNcPurchaseFlowSessionCreated)
export class PurchaseFlowSessionCreatedEventHandler implements IEventHandler<BazaNcPurchaseFlowSessionCreated> {
    constructor(
        private readonly repository: BazaNcTransactionRepository,
        private readonly accountRepository: BazaAccountRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    handle(event: BazaNcPurchaseFlowSessionCreated): void {
        cqrs(PurchaseFlowSessionCreatedEventHandler.name, async () => {
            const entity = new BazaNcTransactionEntity();

            const account = await this.accountRepository.getActiveAccountWithId(event.request.userId);
            const investorAccount = await this.investorAccountRepository.getInvestorAccountByUserId(event.request.userId);

            entity.account = account;
            entity.investorAccount = investorAccount;
            entity.ncOfferingId = event.request.ncOfferingId;
            entity.ncOfferingName = bazaNcOfferingName(event.request);
            entity.ncTradeId = event.request.ncTradeId;
            entity.ncOrderStatus = event.request.ncOrderStatus;
            entity.state = event.request.state;
            entity.ncAccountId = investorAccount.northCapitalAccountId;
            entity.amountCents = event.request.amountAsCents;
            entity.transactionFeesCents = event.request.transactionFeesCents;
            entity.transactionFees = event.request.transactionFees;
            entity.feeCents = event.request.feeAsCents;
            entity.totalCents = event.request.totalAsCents;
            entity.shares = event.request.numberOfShares;
            entity.pricePerShareCents = event.request.pricePerSharesAsCents;
            entity.submitted = false;
            entity.transactionType = event.request.transactionType;

            await this.repository.save(entity);
        });
    }
}
