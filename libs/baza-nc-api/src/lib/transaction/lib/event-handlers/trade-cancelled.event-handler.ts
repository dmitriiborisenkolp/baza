import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import {
    BazaNcTradeStatusUpdatedEvent,
    BazaNcWebhookEvent,
    mapTransactionState,
    NorthCapitalErrorCodes,
    NorthCapitalTopic,
} from '@scaliolabs/baza-nc-shared';
import { isNorthCapitalException, NorthCapitalTradesApiNestjsService } from '../../../transact-api';
import { BazaNcTransactionRepository } from '../../../typeorm';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcWebhookEvent)
export class TradeCancelledEventHandler implements IEventHandler<BazaNcWebhookEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly ncTradesApi: NorthCapitalTradesApiNestjsService,
        private readonly transactionRepository: BazaNcTransactionRepository,
    ) {}

    handle(event: BazaNcWebhookEvent): void {
        cqrs(TradeCancelledEventHandler.name, async () => {
            const message = event.message;

            switch (message.topic) {
                case NorthCapitalTopic.deleteTrade: {
                    try {
                        const trade = await this.transactionRepository.findByNcTradeId(message.responseBody.tradeId);

                        if (trade) {
                            const tradeStatus = await this.ncTradesApi.getTradeStatus({
                                tradeId: message.responseBody.tradeId,
                            });

                            const previousTransactionState = trade.state;
                            const previousOrderStatus = trade.ncOrderStatus;

                            trade.ncOrderStatus = tradeStatus.tradeDetails[0].orderStatus;
                            trade.state = mapTransactionState.find((c) =>
                                c.match.includes(tradeStatus.tradeDetails[0].orderStatus),
                            ).transactionState;

                            await this.transactionRepository.save(trade);

                            await this.eventBus.publish(
                                new BazaNcTradeStatusUpdatedEvent({
                                    id: trade.id,
                                    ncOfferingId: trade.ncOfferingId,
                                    ncTradeId: trade.ncTradeId,
                                    newOrderStatus: trade.ncOrderStatus,
                                    newTransactionState: trade.state,
                                    previousOrderStatus,
                                    previousTransactionState,
                                }),
                            );
                        }
                    } catch (err) {
                        if (!isNorthCapitalException(err, NorthCapitalErrorCodes.TradeIdDoesNotExits)) {
                            throw err;
                        }
                    }
                }
            }
        });
    }
}
