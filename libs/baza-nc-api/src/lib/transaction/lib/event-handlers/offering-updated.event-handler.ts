import { bazaNcOfferingName, BazaNcWebhookEvent, NorthCapitalTopic } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcWebhookEvent)
export class OfferingUpdatedEventHandler implements IEventHandler<BazaNcWebhookEvent> {
    constructor(
        private readonly ncTransactionRepository: BazaNcTransactionRepository,
        private readonly ncOfferingRepository: BazaNcOfferingRepository,
    ) {}

    handle(event: BazaNcWebhookEvent): void {
        cqrs(OfferingUpdatedEventHandler.name, async () => {
            const message = event.message;

            switch (message.topic) {
                case NorthCapitalTopic.updateOffering: {
                    const offering = await this.ncOfferingRepository.findOfferingWithId(message.responseBody.offeringId);

                    if (offering) {
                        const transactionsAffected = await this.ncTransactionRepository.findByNcOfferingId(message.responseBody.offeringId);

                        for (const transaction of transactionsAffected) {
                            transaction.ncOfferingName = bazaNcOfferingName(offering);

                            await this.ncTransactionRepository.save(transaction);
                        }
                    }
                }
            }
        });
    }
}
