import { Injectable, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { BazaNcWebhookEvent, NcApiEvent, NcApiEvents, NorthCapitalPayload } from '@scaliolabs/baza-nc-shared';
import { EventBus } from '@nestjs/cqrs';
import { ApiEventBus, BazaLogger } from '@scaliolabs/baza-core-api';
import { Subject } from 'rxjs';
import { ApiEventSource } from '@scaliolabs/baza-core-shared';
import { BazaNcWebhookConfigurationService } from '../baza-nc-webhook-api.config';

interface PublishWebhookRequest {
    event: NorthCapitalPayload;
}

@Injectable()
export class BazaNcWebhookService implements OnApplicationBootstrap, OnApplicationShutdown {
    private onApplicationShutdown$: Subject<void> = new Subject<void>();

    constructor(
        private readonly moduleConfig: BazaNcWebhookConfigurationService,
        private readonly eventBus: EventBus,
        private readonly logger: BazaLogger,
        private readonly bazaApiBus: ApiEventBus<NcApiEvent, NcApiEvents>,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    get config() {
        return this.moduleConfig.configuration;
    }

    onApplicationShutdown(): void {
        this.onApplicationShutdown$.next();
    }

    onApplicationBootstrap(): void {
        this.listen();
    }

    async publish(request: PublishWebhookRequest, correlationId?: string): Promise<void> {
        this.logger.info(`${this.constructor.name}.${this.publish.name}`, {
            correlationId,
            request,
        });

        const topic = request.event.topic;
        const responseBody = {
            ...request.event.responseBody,
        };

        this.logger.log('Nc Webhook received:');
        this.logger.log(request);

        this.eventBus.publish(
            new BazaNcWebhookEvent({
                topic,
                responseBody,
            } as any),
        );

        if (this.config.kafkaPipe.send) {
            await this.bazaApiBus.publish(
                {
                    topic: NcApiEvent.BazaNcWebhook,
                    payload: {
                        topic,
                        ...request.event,
                    },
                },
                {
                    target: ApiEventSource.EveryNode,
                },
            );
        }
    }

    listen(correlationId?: string): void {
        this.logger.info(`${this.constructor.name}.${this.listen.name}`, {
            correlationId,
        });

        if (!this.config.kafkaPipe.read) {
            return;
        }

        this.bazaApiBus.subscribe({
            source: ApiEventSource.EveryNode,
            takeUntil$: this.onApplicationShutdown$,
            callback: async (e) => {
                const topic = e.event.payload.topic;
                const responseBody = e.event.payload.responseBody;

                this.eventBus.publish(
                    new BazaNcWebhookEvent({
                        topic,
                        responseBody,
                    } as any),
                );

                if (this.config.debug) {
                    this.logger.log('Webhook received from Kafka:');
                    this.logger.log(e.event);
                }
            },
        });
    }
}
