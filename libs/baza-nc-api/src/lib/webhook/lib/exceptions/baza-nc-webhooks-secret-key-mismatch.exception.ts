import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcWebhooksErrorCodes } from '@scaliolabs/baza-nc-shared';

const ERROR_MESSAGE = 'Secret key mismatch';

export class BazaNcWebhooksSecretKeyMismatchException extends BazaAppException {
    constructor() {
        super(BazaNcWebhooksErrorCodes.NcWebhookSecretKeyMismatch, ERROR_MESSAGE, HttpStatus.FORBIDDEN);
    }
}
