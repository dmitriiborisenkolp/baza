import { DynamicModule, Global, Module } from '@nestjs/common';
import { BazaNcWebhookCatcherController } from './controllers/baza-nc-webhook-catcher.controller';
import type { BazaNcWebhookApiConfig } from './baza-nc-webhook-api.config';
import { BazaNcWebhookService } from './services/baza-nc-webhook.service';
import { BazaNcTransactApiModule } from '../../transact-api';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaNcWebhookConfigurationService } from './baza-nc-webhook-api.config';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BazaNcWebhookApiConfig>;
}

export { AsyncOptions as NorthCapitalWebhooksModuleAsyncOptions };

@Module({})
export class BazaNcWebhookApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BazaNcWebhookApiGlobalModule,
            providers: [
                {
                    provide: BazaNcWebhookConfigurationService,
                    inject: options.injects,
                    useFactory: async (...injects) => {
                        return new BazaNcWebhookConfigurationService(await options.useFactory(...injects));
                    },
                },
            ],
            exports: [BazaNcWebhookConfigurationService],
        };
    }
}

@Global()
@Module({
    imports: [CqrsModule, BazaNcTransactApiModule],
    controllers: [BazaNcWebhookCatcherController],
    providers: [BazaNcWebhookService],
    exports: [BazaNcWebhookService],
})
class BazaNcWebhookApiGlobalModule {}
