import { Injectable } from '@nestjs/common';

export interface BazaNcWebhookApiConfig {
    webhookCatcherKey: string;
    debug: boolean;
    encryptionKey: string; // aes-256-cbc
    kafkaPipe: {
        read: boolean;
        send: boolean;
    };
}

@Injectable()
export class BazaNcWebhookConfigurationService {
    constructor(private readonly _configuration: BazaNcWebhookApiConfig) {}

    get configuration(): BazaNcWebhookApiConfig {
        return {
            ...this._configuration,
        };
    }
}
