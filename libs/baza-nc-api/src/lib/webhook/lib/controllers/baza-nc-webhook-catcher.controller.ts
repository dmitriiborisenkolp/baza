import { Body, Controller, HttpStatus, Param, Post, UseInterceptors } from '@nestjs/common';
import { BazaNcWebhooksSecretKeyMismatchException } from '../exceptions/baza-nc-webhooks-secret-key-mismatch.exception';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiImplicitParam } from '@nestjs/swagger/dist/decorators/api-implicit-param.decorator';
import { BazaNorthCapitalOpenApi } from '@scaliolabs/baza-nc-shared';
import { BazaNcWebhookService } from '../services/baza-nc-webhook.service';
import { BazaNcWebhooksEndpoint, BazaNcWebhooksEndpointPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalTopic } from '@scaliolabs/baza-nc-shared';
import { BazaNcWebhookConfigurationService } from '../baza-nc-webhook-api.config';
import { NcWebhookDecryptionInterceptor } from '../../interceptors/nc-webhook-decryption.interceptor';
import { CorrelationId } from '@scaliolabs/baza-core-api';

@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalWebhooks)
@Controller()
@UseInterceptors(NcWebhookDecryptionInterceptor)
export class BazaNcWebhookCatcherController implements BazaNcWebhooksEndpoint {
    constructor(private readonly moduleConfig: BazaNcWebhookConfigurationService, private readonly service: BazaNcWebhookService) {}

    @ApiOperation({
        summary: 'Endpoint for north capital webhook',
    })
    @ApiOkResponse({
        status: HttpStatus.CREATED,
        description: 'WebHook payload was caught and sent to Kafka',
    })
    @ApiImplicitParam({
        name: 'secret',
        description:
            'Secret key which set at .env file. Prevents unauthorized access from unknown sources by keeping this route param value in secret',
    })
    @ApiImplicitParam({
        name: 'topic',
        description: 'Topic to sent. Please check KafkaTopic enum for details',
        enum: Object.values(NorthCapitalTopic),
    })
    @Post(BazaNcWebhooksEndpointPaths.publish)
    async publish(
        @Body() payload: unknown,
        @Param('secret') secretKey: string,
        @Param('topic') topic: NorthCapitalTopic,
        @CorrelationId() correlationId: string,
    ): Promise<void> {
        if (secretKey !== this.moduleConfig.configuration.webhookCatcherKey) {
            throw new BazaNcWebhooksSecretKeyMismatchException();
        }

        await this.service.publish(
            {
                event: {
                    topic,
                    responseBody: payload,
                } as any,
            },
            correlationId,
        );
    }
}
