import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Request } from 'express';
import * as crypto from 'crypto';
import { BazaNcWebhookConfigurationService } from '../lib/baza-nc-webhook-api.config';
import * as rawBody from 'raw-body';

@Injectable()
export class NcWebhookDecryptionInterceptor implements NestInterceptor {
    private readonly encryptionKey: string;
    private iv: Buffer;

    constructor(private readonly moduleConfig: BazaNcWebhookConfigurationService) {
        this.encryptionKey = this.moduleConfig.configuration.encryptionKey;
    }

    async intercept(context: ExecutionContext, next: CallHandler) {
        const request: Request = context.switchToHttp().getRequest();

        if (this.encryptionKey) {
            const encryptedBody: any = (await rawBody(request)).toString('utf8').trim();

            const decryptedPayload = this.decryptPayload(encryptedBody);

            request.body = JSON.parse(this.convertPayloadToJson(decryptedPayload));
        }

        return next.handle();
    }

    private decryptPayload(encryptedPayload): string {
        const algorithm = 'aes-256-cbc';
        const iv_length = 16;
        const secret_key = this.encryptionKey.padEnd(32, '*');
        const iv_with_ciphertext = Buffer.from(encryptedPayload.replace(/plusencr/g, '+'), 'base64');
        this.iv = iv_with_ciphertext.slice(0, iv_length);
        const ciphertext = iv_with_ciphertext.slice(iv_length);
        const decipher = crypto.createDecipheriv(algorithm, secret_key, this.iv);
        const decrypted = Buffer.concat([decipher.update(ciphertext), decipher.final()]);
        return decrypted.toString();
    }

    private convertPayloadToJson(payload: string) {
        const payloadItems: Array<string> = payload.split(',');
        const finalObj: Record<string, string> = {};

        payloadItems.forEach((item) => {
            const itemSplitted = item.split('=');

            finalObj[itemSplitted[0].trim()] = itemSplitted[1].trim();
        });

        return JSON.stringify(finalObj);
    }
}
