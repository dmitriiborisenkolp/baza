import { BazaNcBankAccountE2eConstants } from '@scaliolabs/baza-nc-shared';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace BazaNcDwollaPlaidLinkE2eConstants {
    export function getMock(publicToken: string) {
        if (publicToken === BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_CHECKING) {
            return E2E_ACCOUNT_CHECKING;
        } else if (publicToken === BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_SAVINGS) {
            return E2E_ACCOUNT_SAVINGS;
        } else {
            throw new Error('Unknown E2E Mock');
        }
    }

    export function getIdentity() {
        return {
            accounts: [
                {
                    name: 'JOHN DOE',
                    owners: [
                        {
                            names: ['JOHN DOE'],
                        },
                    ],
                },
            ],
        };
    }

    export const E2E_ACCOUNT_CHECKING = {
        accounts: [
            {
                account_id: '8drqdjDXQ4UPlk4XXB7nhko3BlLPQXfrMJJAl',
                balances: {
                    available: 100,
                    current: 110,
                    iso_currency_code: 'USD',
                    limit: null,
                    unofficial_currency_code: null,
                },
                mask: '0000',
                name: 'Plaid Checking',
                official_name: 'Plaid Gold Standard 0% Interest Checking',
                subtype: 'checking',
                type: 'depository',
            },
        ],
        item: {
            available_products: ['assets', 'balance', 'credit_details', 'income', 'investments', 'liabilities', 'transactions'],
            billed_products: ['auth', 'identity'],
            consent_expiration_time: null,
            error: null,
            institution_id: 'ins_56',
            item_id: 'j9WP9mJza3CobxLqqrQPfMKdwxD8X1T1v8vWG',
            optional_products: null,
            products: ['auth', 'identity'],
            update_type: 'background',
            webhook: '',
        },
        numbers: {
            ach: [
                {
                    account: '1111222233330000',
                    account_id: '8drqdjDXQ4UPlk4XXB7nhko3BlLPQXfrMJJAl',
                    routing: '011401533',
                    wire_routing: '021000021',
                },
            ],
            bacs: [],
            eft: [],
            international: [],
        },
        request_id: 'gFBO75YbM4au5wK',
    };

    export const E2E_ACCOUNT_SAVINGS = {
        accounts: [
            {
                account_id: 'Xvpg7NjyzZF7E4lJE4P9uP1oZy151eixe5NxN',
                balances: {
                    available: 200,
                    current: 210,
                    iso_currency_code: 'USD',
                    limit: null,
                    unofficial_currency_code: null,
                },
                mask: '1111',
                name: 'Plaid Saving',
                official_name: 'Plaid Silver Standard 0.1% Interest Saving',
                subtype: 'savings',
                type: 'depository',
            },
        ],
        item: {
            available_products: ['assets', 'balance', 'credit_details', 'identity', 'income', 'investments', 'liabilities', 'transactions'],
            billed_products: ['auth'],
            consent_expiration_time: null,
            error: null,
            institution_id: 'ins_56',
            item_id: 'x1Lg4dQAEeczBpPMBp5ds7Z6M5P8rDfnyRya5',
            optional_products: null,
            products: ['auth'],
            update_type: 'background',
            webhook: '',
        },
        numbers: {
            ach: [
                {
                    account: '1111222233331111',
                    account_id: 'Xvpg7NjyzZF7E4lJE4P9uP1oZy151eixe5NxN',
                    routing: '011401533',
                    wire_routing: '021000021',
                },
            ],
            bacs: [],
            eft: [],
            international: [],
        },
        request_id: '88NmLpdhVwe2Kgr',
    };
}
