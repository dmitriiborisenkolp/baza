import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationE2eUserFixture } from '../../../../account-verification';
import { AccountTypeCheckingSaving, BazaNcBankAccountAddRequest, BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountsService } from '../../services/baza-nc-bank-accounts.service';
import { BazaNcBankAccountFixtures } from '../baza-nc-bank-account.fixtures';

const BAZA_NC_BANK_ACCOUNTS_FIXTURE: Array<BazaNcBankAccountAddRequest> = [
    {
        type: BazaNcBankAccountType.CashIn,
        accountNumber: '1111222233330000',
        accountRoutingNumber: '011401533',
        accountName: 'John Doe',
        accountNickName: 'JD',
        accountType: AccountTypeCheckingSaving.Checking,
    },
    {
        type: BazaNcBankAccountType.CashIn,
        accountNumber: '1111222233330000',
        accountRoutingNumber: '021000021',
        accountName: 'John Doe',
        accountNickName: 'JD',
        accountType: AccountTypeCheckingSaving.Checking,
    },
    {
        type: BazaNcBankAccountType.CashOut,
        accountNumber: '1111222233330000',
        accountRoutingNumber: '011401533',
        accountName: 'John Doe',
        accountNickName: 'JD',
        accountType: AccountTypeCheckingSaving.Checking,
    },
    {
        type: BazaNcBankAccountType.CashOut,
        accountNumber: '1111222233330000',
        accountRoutingNumber: '021000021',
        accountName: 'John Doe',
        accountNickName: 'JD',
        accountType: AccountTypeCheckingSaving.Checking,
    },
];

@Injectable()
export class BazaNcBankAccountsCaseBFixture implements BazaE2eFixture {
    constructor(private readonly service: BazaNcBankAccountsService) {}

    async up(): Promise<void> {
        const investorAccount = BazaNcAccountVerificationE2eUserFixture._e2eUserInvestorAccount;

        for (const request of BAZA_NC_BANK_ACCOUNTS_FIXTURE) {
            await this.service.add(investorAccount.id, request);
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaNcBankAccountFixtures.BazaNcBankAccountsCaseB,
        provider: BazaNcBankAccountsCaseBFixture,
    },
]);
