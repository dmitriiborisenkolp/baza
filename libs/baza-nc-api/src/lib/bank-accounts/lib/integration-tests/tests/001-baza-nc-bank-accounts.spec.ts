import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcBankAccountsNodeAccess, BazaNcE2eNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { AccountTypeCheckingSaving, BazaNcBankAccountErrorCodes, BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/bank-account/001-baza-nc-bank-accounts.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessNcE2e = new BazaNcE2eNodeAccess(http);
    const dataAccess = new BazaNcBankAccountsNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);

    let ulidCashIn1: string;
    let ulidCashIn2: string;
    let ulidCashIn3: string;
    let ulidCashInImportedFromCashOut1: string;
    let ulidCashOut1: string;
    let ulidCashOut2SimilarToCashIn1: string;
    let ulidCashInAnotherUser: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await dataAccessNcE2e.enableLBASync();
    });

    afterAll(async () => {
        await dataAccessNcE2e.disableLBASync();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display that investor account has no linked bank accounts', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isBankAccountLinked).toBeFalsy();
        expect(response.isBankAccountCashOutLinked).toBeFalsy();
    });

    it('will have no bank accounts before', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(0);
    });

    it('will display no default accounts for cash-in available yet (with throwError: 0)', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn, {
            withOptions: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).not.toBeDefined();
        expect(Array.isArray(response.options)).toBeTruthy();
        expect(response.options.length).toBe(0);
        expect(response.isAvailable).toBeFalsy();
    });

    it('will not display options array in defaults (withOptions: 0)', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn, {
            withOptions: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).not.toBeDefined();
        expect(response.options).not.toBeDefined();
        expect(response.isAvailable).toBeFalsy();
    });

    it('will fail with error code in attempt to fetch cash-in bank account (with throwError: 1)', async () => {
        const response: BazaError = (await dataAccess.default(BazaNcBankAccountType.CashIn, {
            throwError: true,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcBankAccountErrorCodes.BazaNcBankAccountNoDefault);
    });

    it('will successfully add a new bank account', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '123456789',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Checking,
            setAsDefault: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Checking");
        expect(response.isDefault).toBeTruthy();
        expect(response.accountNumber).toBe('*****6789');
        expect(response.accountRoutingNumber).toBe('**0000');

        ulidCashIn1 = response.ulid;
    });

    it('will set that investor account has linked cash-in bank account', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isBankAccountLinked).toBeTruthy();
        expect(response.isBankAccountCashOutLinked).toBeFalsy();
    });

    it('will display added cash-in bank account as default', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn, {
            withOptions: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.options.length).toBe(1);
        expect(response.options[0].ulid).toBe(ulidCashIn1);
        expect(response.result).toBeDefined();
        expect(response.result.ulid).toBe(ulidCashIn1);
    });

    it('will display added account in defaults', async () => {
        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).toBe(ulidCashIn1);
    });

    it('will create a new non-default cash-in bank account', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '223456781',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Checking,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Checking");
        expect(response.isDefault).toBeFalsy();

        ulidCashIn2 = response.ulid;
    });

    it('will still display same cash-in bank account as default (1)', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn, {
            withOptions: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.options.length).toBe(1);
        expect(response.options[0].ulid).toBe(ulidCashIn1);
        expect(response.result).toBeDefined();
        expect(response.result.ulid).toBe(ulidCashIn1);
    });

    it('will add cash-out bank account as non-default (but it will be auto-selected as default anyway)', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashOut,
            accountNumber: '283456781',
            accountRoutingNumber: '000001',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Savings");
        expect(response.isDefault).toBeTruthy();

        ulidCashOut1 = response.ulid;
    });

    it('will set that investor account has linked cash-out bank account', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isBankAccountLinked).toBeTruthy();
        expect(response.isBankAccountCashOutLinked).toBeTruthy();
    });

    it('will still display same cash-in bank account as default (2)', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn, {
            withOptions: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.options.length).toBe(1);
        expect(response.options[0].ulid).toBe(ulidCashIn1);
        expect(response.result).toBeDefined();
        expect(response.result.ulid).toBe(ulidCashIn1);
    });

    it('will display newly added cash-out account as default', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashOut, {
            withOptions: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.options.length).toBe(1);
        expect(response.options[0].ulid).toBe(ulidCashOut1);
        expect(response.result).toBeDefined();
        expect(response.result.ulid).toBe(ulidCashOut1);
    });

    it('will display both cash-in and cash-out accounts in defaults', async () => {
        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).toBe(ulidCashIn1);

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].ulid).toBe(ulidCashOut1);
    });

    it('will create a new cash-in bank account and set it as default with setAsDefault: true', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '623456781',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Checking,
            setAsDefault: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Checking");
        expect(response.isDefault).toBeTruthy();

        ulidCashIn3 = response.ulid;
    });

    it('will reflects that new cash-in account is selected as default', async () => {
        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).toBe(ulidCashIn3);

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].ulid).toBe(ulidCashOut1);
    });

    it('will set another cash-in account as default', async () => {
        const response = await dataAccess.set({
            ulid: ulidCashIn2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].name).toBe("John Smith's Checking");
        expect(response[0].isDefault).toBeTruthy();
    });

    it('will reflect changes in defaults', async () => {
        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).toBe(ulidCashIn2);
        expect(response[0].name).toBe("John Smith's Checking");

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].ulid).toBe(ulidCashOut1);
    });

    it('will create one more cash-out account as default', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashOut,
            accountNumber: '345678912',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Savings,
            setAsDefault: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Savings");
        expect(response.isDefault).toBeTruthy();

        ulidCashOut2SimilarToCashIn1 = response.ulid;
    });

    it('will displays that cash-out bank account #1 is not default anymore', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashOut);

        expect(response.isAvailable).toBeTruthy();
        expect(response.result.ulid).not.toBe(ulidCashIn1);
        expect(response.result.ulid).toBe(ulidCashOut2SimilarToCashIn1);
    });

    it('will display that 5 bank accounts are created at this moment', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(5);
    });

    it('will set cash-out account as default cash-in account, but it will import bank account', async () => {
        const response = await dataAccess.set({
            ulid: ulidCashOut1,
            types: [BazaNcBankAccountType.CashIn],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].name).toBe("John Smith's Savings");
        expect(response[0].isDefault).toBeTruthy();
        expect(response[0].ulid).not.toBe(ulidCashOut1);
    });

    it('will display that a new bank account added', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(6);
    });

    it('will display that newly created cash out accounts from cash-in template was created', async () => {
        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).not.toBe(ulidCashIn2);
        expect(response[0].name).toBe("John Smith's Savings");

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].ulid).not.toBe(ulidCashOut1);

        expect([ulidCashIn1, ulidCashIn2, ulidCashIn3, ulidCashOut1, ulidCashOut2SimilarToCashIn1].includes(response[0].ulid)).toBeFalsy();

        ulidCashInImportedFromCashOut1 = response[0].ulid;
    });

    it('will try to set as default once more same cash-in bank account as cash-out', async () => {
        const response = await dataAccess.set({
            ulid: ulidCashOut1,
            types: [BazaNcBankAccountType.CashIn],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].name).toBe("John Smith's Savings");
        expect(response[0].isDefault).toBeTruthy();
        expect(response[0].ulid).not.toBe(ulidCashOut1);
    });

    it('will still displays that there are 6 accounts available', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(6);
    });

    it('will still display same default bank accounts as it was', async () => {
        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).toBe(ulidCashInImportedFromCashOut1);
        expect(response[0].name).toBe("John Smith's Savings");

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].ulid).not.toBe(ulidCashOut1);

        expect([ulidCashIn1, ulidCashIn2, ulidCashIn3, ulidCashOut1, ulidCashOut2SimilarToCashIn1].includes(response[0].ulid)).toBeFalsy();
    });

    it('will successfully remove default cash-in bank account', async () => {
        const response = await dataAccess.remove(ulidCashInImportedFromCashOut1);

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.defaultBankAccountUlid).toBe(ulidCashIn1);
    });

    it('will set that investor account still has linked cash-in and cash-out bank accounts', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isBankAccountLinked).toBeTruthy();
        expect(response.isBankAccountCashOutLinked).toBeTruthy();
    });

    it('will display that bank account is removed', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(5);
        expect(response.find((next) => next.ulid === ulidCashInImportedFromCashOut1)).toBeUndefined();
    });

    it('will select first available bank account as default', async () => {
        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).toBe(ulidCashIn1);
        expect(response[0].name).toBe("John Smith's Checking");

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].ulid).not.toBe(ulidCashOut1);
    });

    it('will display no bank accounts available for another user', async () => {
        await http.authE2eUser2();

        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(0);
    });

    it('will not allow to get bank account of another user (without account verification)', async () => {
        await http.authE2eUser2();

        const response: BazaError = (await dataAccess.get(ulidCashIn1)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcBankAccountErrorCodes.BazaNcBankAccountNotFound);
    });

    it('will not allow to get bank account of another user (with account verification)', async () => {
        await dataAccessE2eFixtures.up({
            fixtures: [BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUser2Fixture],
            specFile: __filename,
        });

        await http.authE2eUser2();

        const response: BazaError = (await dataAccess.get(ulidCashIn1)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcBankAccountErrorCodes.BazaNcBankAccountNotFound);
    });

    it('will allow create same bank account for another user', async () => {
        await http.authE2eUser2();

        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '345678912',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Savings,
            setAsDefault: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Savings");
        expect(response.isDefault).toBeTruthy();

        ulidCashInAnotherUser = response.ulid;
    });

    it('will display added bank account of another user as default', async () => {
        await http.authE2eUser2();

        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].ulid).toBe(ulidCashInAnotherUser);
    });

    it('will remove all cash-in bank accounts', async () => {
        for (const ulid of [ulidCashIn1, ulidCashIn2, ulidCashIn3]) {
            const response = await dataAccess.remove(ulid);

            expect(isBazaErrorResponse(response)).toBeFalsy();
        }
    });

    it('will display that investor account has no more cash-in bank accounts', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isBankAccountLinked).toBeFalsy();
        expect(response.isBankAccountCashOutLinked).toBeTruthy();
    });

    it('will display that no default cash-in bank available anymore', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn, {
            withOptions: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
        expect(response.options.length).toBe(0);
    });

    it('will remove all cash-out bank accounts', async () => {
        for (const ulid of [ulidCashOut1, ulidCashOut2SimilarToCashIn1]) {
            const response = await dataAccess.remove(ulid);

            expect(isBazaErrorResponse(response)).toBeFalsy();
        }
    });

    it('will display that investor account has no more cash-out bank accounts', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isBankAccountLinked).toBeFalsy();
        expect(response.isBankAccountCashOutLinked).toBeFalsy();
    });

    it('will display that no default cash-out bank available anymore', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashOut, {
            withOptions: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
        expect(response.options.length).toBe(0);
    });
});
