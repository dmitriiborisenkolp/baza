import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcBankAccountsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import {
    AccountTypeCheckingSaving,
    BazaNcBankAccountDto,
    BazaNcBankAccountErrorCodes,
    BazaNcBankAccountExport,
    BazaNcBankAccountType,
    BazaNcPurchaseFlowErrorCodes,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountFixtures } from '../baza-nc-bank-account.fixtures';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/bank-account/002-baza-nc-bank-account-export-nc.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcBankAccountsNodeAccess(http);

    let BANK_ACCOUNTS: Array<BazaNcBankAccountDto> = [];

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcBankAccountFixtures.BazaNcBankAccountsCaseA,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch list of bank accounts', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);
        expect(response[0].isDefault).toBeTruthy();
        expect(response[1].isDefault).toBeFalsy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();

        BANK_ACCOUNTS = response;
    });

    it('will successfully exports NC account', async () => {
        const response = await dataAccess.export({
            ulid: BANK_ACCOUNTS[0].ulid,
            services: [BazaNcBankAccountExport.NC],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.exported).toEqual([BazaNcBankAccountExport.NC]);
    });

    it('will display that account was exported to NC', async () => {
        const response = await dataAccess.ncAchBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
    });

    it('will displays that account was added to list', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([BazaNcBankAccountExport.NC]);

        expect(response[0].isDefault).toBeTruthy();
        expect(response[1].isDefault).toBeFalsy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will successfully re-export once more same account to NC', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([BazaNcBankAccountExport.NC]);

        expect(response[0].isDefault).toBeTruthy();
        expect(response[1].isDefault).toBeFalsy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will export another account to NC (w/o mark it as default)', async () => {
        const response = await dataAccess.export({
            ulid: BANK_ACCOUNTS[1].ulid,
            services: [BazaNcBankAccountExport.NC],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.exported).toEqual([BazaNcBankAccountExport.NC]);
    });

    it('will display that bank account is set on NC ACH side', async () => {
        const response = await dataAccess.ncAchBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('************0001');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
    });

    it('will display that previous account is not marked as exported to NC anymore', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([]);
        expect(response[1].exported).toEqual([BazaNcBankAccountExport.NC]);

        expect(response[0].isDefault).toBeTruthy();
        expect(response[1].isDefault).toBeFalsy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will exports account to NC and mark it as default', async () => {
        const response = await dataAccess.export({
            ulid: BANK_ACCOUNTS[1].ulid,
            services: [BazaNcBankAccountExport.NC],
            setAsDefault: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.exported).toEqual([BazaNcBankAccountExport.NC]);
        expect(response.isDefault).toBeTruthy();
    });

    it('will display that account  marked as exported to NC and as default', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([]);
        expect(response[1].exported).toEqual([BazaNcBankAccountExport.NC]);

        expect(response[0].isDefault).toBeFalsy();
        expect(response[1].isDefault).toBeTruthy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will not allow to export cash-out bank account to NC', async () => {
        const response: BazaError = (await dataAccess.export({
            ulid: BANK_ACCOUNTS[4].ulid,
            services: [BazaNcBankAccountExport.NC],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcBankAccountErrorCodes.BazaNcBankAccountCannotBeExportedToNc);
    });

    it('will display that account  marked as exported to NC and as default (2)', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([]);
        expect(response[1].exported).toEqual([BazaNcBankAccountExport.NC]);

        expect(response[0].isDefault).toBeFalsy();
        expect(response[1].isDefault).toBeTruthy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will correctly fails with invalid routing number when exporting invalid account to NC', async () => {
        const response: BazaError = (await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '1111222233330000',
            accountRoutingNumber: '123',
            accountName: 'John Doe',
            accountNickName: 'JD',
            accountType: AccountTypeCheckingSaving.Checking,
            export: [BazaNcBankAccountExport.NC],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.InvalidRoutingNumber);
    });

    it('will still display that only 5 bank accounts available in list', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([]);
        expect(response[1].exported).toEqual([BazaNcBankAccountExport.NC]);

        expect(response[0].isDefault).toBeFalsy();
        expect(response[1].isDefault).toBeTruthy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });
});
