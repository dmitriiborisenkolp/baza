import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaNcBankAccountsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { AccountRole, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcBankAccountDto, BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountFixtures } from '../baza-nc-bank-account.fixtures';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/bank-account/006-baza-nc-bank-account-baza-1283.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcBankAccountsNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let USER_EMAIL: string;
    let BANK_ACCOUNTS: Array<BazaNcBankAccountDto> = [];

    let ULID_CASH_IN_1: string;
    let ULID_CASH_IN_2: string;
    let ULID_CASH_OUT_1: string;
    let ULID_CASH_OUT_2: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcBankAccountFixtures.BazaNcBankAccountsCaseB,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
        });

        USER_EMAIL = accounts.items[0].email;
    });

    beforeEach(async () => {
        await http.auth({
            email: USER_EMAIL,
            password: 'e2e-user-password',
        });
    });

    it('will fetch list of bank accounts', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(4);
        expect(response[0].isDefault).toBeTruthy();
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[1].isDefault).toBeFalsy();
        expect(response[1].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[2].isDefault).toBeTruthy();
        expect(response[2].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[3].isDefault).toBeFalsy();
        expect(response[3].type).toBe(BazaNcBankAccountType.CashOut);

        ULID_CASH_IN_1 = response[0].ulid;
        ULID_CASH_IN_2 = response[1].ulid;
        ULID_CASH_OUT_1 = response[2].ulid;
        ULID_CASH_OUT_2 = response[3].ulid;

        BANK_ACCOUNTS = response;
    });

    it('will set cash-out similar bank account as cash-in', async () => {
        const response = await dataAccess.set({
            ulid: ULID_CASH_OUT_1,
            types: [BazaNcBankAccountType.CashIn],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).not.toBe(ULID_CASH_OUT_1);
        expect(response[0].ulid).toBe(ULID_CASH_IN_1);
    });

    it('will not make any actual changes in list', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(4);
        expect(response[0].isDefault).toBeTruthy();
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[1].isDefault).toBeFalsy();
        expect(response[1].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[2].isDefault).toBeTruthy();
        expect(response[2].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[3].isDefault).toBeFalsy();
        expect(response[3].type).toBe(BazaNcBankAccountType.CashOut);
    });

    it('will set both cash-in and cash-out bank accounts (both already defaults)', async () => {
        const response = await dataAccess.set({
            ulid: ULID_CASH_OUT_1,
            types: [BazaNcBankAccountType.CashIn, BazaNcBankAccountType.CashOut],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).not.toBe(ULID_CASH_OUT_1);
        expect(response[0].ulid).toBe(ULID_CASH_IN_1);

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].ulid).not.toBe(ULID_CASH_IN_1);
        expect(response[1].ulid).toBe(ULID_CASH_OUT_1);
    });

    it('will not make any actual changes in list (2)', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(4);
        expect(response[0].isDefault).toBeTruthy();
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[1].isDefault).toBeFalsy();
        expect(response[1].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[2].isDefault).toBeTruthy();
        expect(response[2].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[3].isDefault).toBeFalsy();
        expect(response[3].type).toBe(BazaNcBankAccountType.CashOut);
    });

    it('will set both cash-in and cash-out bank accounts (both are not defaults)', async () => {
        const response = await dataAccess.set({
            ulid: ULID_CASH_OUT_2,
            types: [BazaNcBankAccountType.CashIn, BazaNcBankAccountType.CashOut],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).toBe(ULID_CASH_IN_2);

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].ulid).toBe(ULID_CASH_OUT_2);
    });

    it('will reflect changes in list', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(4);
        expect(response[0].isDefault).toBeFalsy();
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[1].isDefault).toBeTruthy();
        expect(response[1].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[2].isDefault).toBeFalsy();
        expect(response[2].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[3].isDefault).toBeTruthy();
        expect(response[3].type).toBe(BazaNcBankAccountType.CashOut);
    });

    it('will set cash-out bank account as default cash-in account only', async () => {
        const response = await dataAccess.set({
            ulid: ULID_CASH_OUT_1,
            types: [BazaNcBankAccountType.CashIn],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);

        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].ulid).toBe(ULID_CASH_IN_1);
    });

    it('will correctly reflect changes in list', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(4);
        expect(response[0].isDefault).toBeTruthy();
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[1].isDefault).toBeFalsy();
        expect(response[1].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[2].isDefault).toBeFalsy();
        expect(response[2].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[3].isDefault).toBeTruthy();
        expect(response[3].type).toBe(BazaNcBankAccountType.CashOut);
    });
});
