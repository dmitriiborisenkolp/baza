import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import {
    BazaNcBankAccountsNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcSyncCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { AccountRole, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import {
    AccountTypeCheckingSaving,
    BazaNcBankAccountE2eConstants,
    BazaNcBankAccountExport,
    BazaNcBankAccountType,
} from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/bank-account/010-baza-nc-bank-account-nc-ach-nbas-plaid-flow.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessNBA = new BazaNcBankAccountsNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessNcSyncCms = new BazaNcSyncCmsNodeAccess(http);

    let USER_ID: number;
    let USER_EMAIL: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
        });

        USER_ID = accounts.items[0].id;
        USER_EMAIL = accounts.items[0].email;
    });

    beforeEach(async () => {
        await http.auth({
            email: USER_EMAIL,
            password: 'e2e-user-password',
        });
    });

    it('will successfully create dwolla customer for investor', async () => {
        const response = await dataAccessDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBeTruthy();
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });

    it('will link NC ACH bank account using Plaid', async () => {
        const response = await dataAccessNBA.linkOnSuccess({
            publicToken: BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_CHECKING,
            type: BazaNcBankAccountType.CashIn,
            setAsDefault: false,
            export: [BazaNcBankAccountExport.NC],
            secure: true,
            static: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.bankAccount.accountNumber).toBe('************0000');
        expect(response.bankAccount.accountRoutingNumber).toBe('*****1533');
        expect(response.bankAccount.accountType).toBe(AccountTypeCheckingSaving.Checking);
    });

    it('will display that NC ACH account is set on NC side', async () => {
        const response = await dataAccessNBA.ncAchBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountType).toBe(AccountTypeCheckingSaving.Checking);
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
    });

    it('will display that cash-in bank account is not set (1)', async () => {
        const response = await dataAccessNBA.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
    });

    it('will display correct flags for investor account (1)', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: false,
            isBankAccountCashOutLinked: false,
        });
    });

    it('will link another NC ACH bank account using Plaid', async () => {
        const response = await dataAccessNBA.linkOnSuccess({
            publicToken: BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_SAVINGS,
            type: BazaNcBankAccountType.CashIn,
            setAsDefault: false,
            export: [BazaNcBankAccountExport.NC],
            secure: true,
            static: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.bankAccount.accountNumber).toBe('************1111');
        expect(response.bankAccount.accountRoutingNumber).toBe('*****1533');
        expect(response.bankAccount.accountType).toBe(AccountTypeCheckingSaving.Savings);
    });

    it('will display that another NC ACH account is set on NC side', async () => {
        const response = await dataAccessNBA.ncAchBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountType).toBe(AccountTypeCheckingSaving.Savings);
        expect(response.details.accountNumber).toBe('************1111');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
    });

    it('will display that cash-in bank account is not set (2)', async () => {
        const response = await dataAccessNBA.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
    });

    it('will display correct flags for investor account (2)', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: false,
            isBankAccountCashOutLinked: false,
        });
    });

    it('will sync payment methods (1)', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNcSyncCms.syncInvestorAccountPaymentMethods({
            userId: USER_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will keep flags for investor account unchanged (1)', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: false,
            isBankAccountCashOutLinked: false,
        });
    });

    it('will link Cash-In bank account using Plaid', async () => {
        const response = await dataAccessNBA.linkOnSuccess({
            publicToken: BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_SAVINGS,
            type: BazaNcBankAccountType.CashIn,
            setAsDefault: true,
            export: [BazaNcBankAccountExport.Dwolla],
            secure: true,
            static: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.bankAccount.accountNumber).toBe('************1111');
        expect(response.bankAccount.accountRoutingNumber).toBe('*****1533');
        expect(response.bankAccount.accountType).toBe(AccountTypeCheckingSaving.Savings);
    });

    it('will correctly updates flags for investor account (2)', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: true,
            isBankAccountCashOutLinked: false,
        });
    });

    it('will display that Cash-In bank account as available', async () => {
        const response = await dataAccessNBA.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.result).toBeDefined();
        expect(response.result.accountNumber).toBe('************1111');
        expect(response.result.accountRoutingNumber).toBe('*****1533');
    });

    it('will link another NC ACH bank account using Plaid', async () => {
        const response = await dataAccessNBA.linkOnSuccess({
            publicToken: BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_CHECKING,
            type: BazaNcBankAccountType.CashIn,
            setAsDefault: false,
            export: [BazaNcBankAccountExport.NC],
            secure: true,
            static: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.bankAccount.accountNumber).toBe('************0000');
        expect(response.bankAccount.accountRoutingNumber).toBe('*****1533');
        expect(response.bankAccount.accountType).toBe(AccountTypeCheckingSaving.Checking);
    });

    it('will display that previous NC ACH account is set on NC side', async () => {
        const response = await dataAccessNBA.ncAchBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountType).toBe(AccountTypeCheckingSaving.Checking);
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
    });

    it('will display that Cash-In bank account as available and same as before', async () => {
        const response = await dataAccessNBA.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.result).toBeDefined();
        expect(response.result.accountType).toBe(AccountTypeCheckingSaving.Savings);
        expect(response.result.accountNumber).toBe('************1111');
        expect(response.result.accountRoutingNumber).toBe('*****1533');
    });

    it('will link same Cash-In bank account as for NC ACH', async () => {
        const response = await dataAccessNBA.linkOnSuccess({
            publicToken: BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_CHECKING,
            type: BazaNcBankAccountType.CashIn,
            setAsDefault: true,
            export: [BazaNcBankAccountExport.NC],
            secure: true,
            static: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.bankAccount.accountNumber).toBe('************0000');
        expect(response.bankAccount.accountRoutingNumber).toBe('*****1533');
        expect(response.bankAccount.accountType).toBe(AccountTypeCheckingSaving.Checking);
    });

    it('will link Cash-Out bank account using Plaid', async () => {
        const response = await dataAccessNBA.linkOnSuccess({
            publicToken: BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_CHECKING,
            type: BazaNcBankAccountType.CashOut,
            setAsDefault: true,
            export: [BazaNcBankAccountExport.Dwolla],
            secure: true,
            static: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.bankAccount.type).toBe(BazaNcBankAccountType.CashOut);
        expect(response.bankAccount.accountNumber).toBe('************0000');
        expect(response.bankAccount.accountRoutingNumber).toBe('*****1533');
        expect(response.bankAccount.accountType).toBe(AccountTypeCheckingSaving.Checking);
    });

    it('will correctly updates investor account bank flags', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: true,
            isBankAccountCashOutLinked: true,
        });
    });

    it('will sync payment methods (2)', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNcSyncCms.syncInvestorAccountPaymentMethods({
            userId: USER_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will keep flags same as before', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: true,
            isBankAccountCashOutLinked: true,
        });
    });

    it('will display Cash-Out bank account as available (2)', async () => {
        const response = await dataAccessNBA.default(BazaNcBankAccountType.CashOut);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.result).toBeDefined();
        expect(response.result.accountNumber).toBe('************0000');
    });

    it('will display correct bank account for NC ACH', async () => {
        const response = await dataAccessNBA.ncAchBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountType).toBe(AccountTypeCheckingSaving.Checking);
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
    });
});
