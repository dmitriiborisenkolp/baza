import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcBankAccountsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { AccountTypeCheckingSaving, BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/bank-account/007-baza-nc-bank-account-static.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcBankAccountsNodeAccess(http);

    let ulid: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will add bank account with static flag enabled', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '123456789',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Checking,
            setAsDefault: true,
            static: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Checking");
        expect(response.isDefault).toBeTruthy();
        expect(response.accountNumber).toBe('*****6789');
        expect(response.accountRoutingNumber).toBe('**0000');
    });

    it('will add a new bank account with static flag enabled (should replace previous one)', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '283456781',
            accountRoutingNumber: '000001',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Savings,
            setAsDefault: true,
            static: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Savings");
        expect(response.isDefault).toBeTruthy();
    });

    it('will display that only 1 account available', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].accountNumber).toBe('*****6781');
        expect(response[0].isDefault).toBeTruthy();
    });

    it('will re-add first bank account with static flag enabled', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '123456789',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Checking,
            setAsDefault: true,
            static: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Checking");
        expect(response.isDefault).toBeTruthy();
        expect(response.accountNumber).toBe('*****6789');
        expect(response.accountRoutingNumber).toBe('**0000');
    });

    it('will display its still only 1 account available', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].accountNumber).toBe('*****6789');
        expect(response[0].isDefault).toBeTruthy();

        ulid = response[0].ulid;
    });

    it('will add bank account as Cash-Out with static flag enabled', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashOut,
            accountNumber: '283456781',
            accountRoutingNumber: '000001',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Savings,
            setAsDefault: true,
            static: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.name).toBe("John Smith's Savings");
        expect(response.isDefault).toBeTruthy();
    });

    it('will set Cash-In bank account as Cash-Out with static flag enabled', async () => {
        const response = await dataAccess.set({
            ulid,
            types: [BazaNcBankAccountType.CashOut],
            static: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display its still only 2 account available', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].accountNumber).toBe('*****6789');
        expect(response[0].isDefault).toBeTruthy();

        expect(response[1].type).toBe(BazaNcBankAccountType.CashOut);
        expect(response[1].accountNumber).toBe('*****6789');
        expect(response[1].isDefault).toBeTruthy();
    });
});
