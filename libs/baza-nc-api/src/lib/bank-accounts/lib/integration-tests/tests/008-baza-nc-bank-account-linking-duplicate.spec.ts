import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcBankAccountsNodeAccess, BazaNcInvestorAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { AccountTypeCheckingSaving, BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/bank-account/001-baza-nc-bank-accounts.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcBankAccountsNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);

    let ulidChecking: string;
    let ulidSavings: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display that investor account has no linked bank accounts', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isBankAccountLinked).toBeFalsy();
        expect(response.isBankAccountCashOutLinked).toBeFalsy();
    });

    it('will have no bank accounts before', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(0);
    });

    it('will add a Checking bank account', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '123456789',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Checking,
            setAsDefault: true,
            static: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ulidChecking = response.ulid;
    });

    it('will display that Checking bank account is selected as default', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.result).toBeDefined();
        expect(response.result.ulid).toBe(ulidChecking);
        expect(response.result.accountType).toBe(AccountTypeCheckingSaving.Checking);
    });

    it('will add a Savings bank account', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '283456781',
            accountRoutingNumber: '000001',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Savings,
            setAsDefault: true,
            static: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ulidSavings = response.ulid;

        expect(ulidChecking).not.toBe(ulidSavings);
    });

    it('will display that Saving bank account is selected as default', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.result).toBeDefined();
        expect(response.result.ulid).toBe(ulidSavings);
        expect(response.result.accountType).toBe(AccountTypeCheckingSaving.Savings);
    });

    it('will re-add Checking duplicate bank account', async () => {
        const response = await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '123456789',
            accountRoutingNumber: '000000',
            accountNickName: 'JN',
            accountName: 'John Smith',
            accountType: AccountTypeCheckingSaving.Checking,
            setAsDefault: true,
            static: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.ulid).not.toBe(ulidChecking);

        ulidChecking = response.ulid;
    });

    it('will display that Checking bank account is selected as default', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.result).toBeDefined();
        expect(response.result.ulid).toBe(ulidChecking);
        expect(response.result.accountType).toBe(AccountTypeCheckingSaving.Checking);
    });

    it('will display that there is only 1 bank account available total in list due of using "static" flag', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);
    });
});
