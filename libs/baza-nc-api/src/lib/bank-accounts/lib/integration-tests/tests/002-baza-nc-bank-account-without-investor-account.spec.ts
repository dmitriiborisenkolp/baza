import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcBankAccountsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcBankAccountErrorCodes, BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/bank-account/002-baza-nc-bank-account-without-investor-account.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcBankAccountsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will not fail with list endpoint', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(0);
    });

    it('will not fail with defaults endpoint', async () => {
        const response = await dataAccess.defaults();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(0);
    });

    it('will not fail with default endpoint (with throwError: 0)', async () => {
        const response = await dataAccess.default(BazaNcBankAccountType.CashIn, {
            throwError: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
    });

    it('will fail with default endpoint (with throwError: 1)', async () => {
        const response: BazaError = (await dataAccess.default(BazaNcBankAccountType.CashIn, {
            throwError: true,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcBankAccountErrorCodes.BazaNcBankAccountNoDefault);
    });
});
