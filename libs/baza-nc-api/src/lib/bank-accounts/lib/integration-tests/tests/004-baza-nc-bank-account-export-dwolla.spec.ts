import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaNcBankAccountsNodeAccess, BazaNcDwollaNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { AccountRole, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import {
    AccountTypeCheckingSaving,
    BazaNcBankAccountDto,
    BazaNcBankAccountErrorCodes,
    BazaNcBankAccountExport,
    BazaNcBankAccountType,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountFixtures } from '../baza-nc-bank-account.fixtures';
import { BazaDwollaErrorCodes } from '@scaliolabs/baza-dwolla-shared';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/bank-account/004-baza-nc-bank-account-export-dwolla.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcBankAccountsNodeAccess(http);
    const dataAccessDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let USER_EMAIL: string;
    let BANK_ACCOUNTS: Array<BazaNcBankAccountDto> = [];

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcBankAccountFixtures.BazaNcBankAccountsCaseA,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
        });

        USER_EMAIL = accounts.items[0].email;
    });

    beforeEach(async () => {
        await http.auth({
            email: USER_EMAIL,
            password: 'e2e-user-password',
        });
    });

    it('will fetch list of bank accounts', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);
        expect(response[0].isDefault).toBeTruthy();
        expect(response[1].isDefault).toBeFalsy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();

        BANK_ACCOUNTS = response;
    });

    it('will fail to export cash-in account w/o dwolla customer enabled for investor', async () => {
        const response: BazaError = (await dataAccess.export({
            ulid: BANK_ACCOUNTS[0].ulid,
            services: [BazaNcBankAccountExport.Dwolla],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcBankAccountErrorCodes.BazaNcBankAccountNoDwollaCustomerAvailable);
    });

    it('will successfully create dwolla customer for investor', async () => {
        const response = await dataAccessDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBeTruthy();
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });

    it('will successfully exports cash-in account to Dwolla', async () => {
        const response = await dataAccess.export({
            ulid: BANK_ACCOUNTS[0].ulid,
            services: [BazaNcBankAccountExport.Dwolla],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.exported).toEqual([BazaNcBankAccountExport.Dwolla]);
    });

    it('will displays that cash-in account was exported to Dwolla', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([BazaNcBankAccountExport.Dwolla]);

        expect(response[0].isDefault).toBeTruthy();
        expect(response[1].isDefault).toBeFalsy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will also exports cash-in account to NC', async () => {
        const response = await dataAccess.export({
            ulid: BANK_ACCOUNTS[0].ulid,
            services: [BazaNcBankAccountExport.NC],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.exported).toEqual([BazaNcBankAccountExport.Dwolla, BazaNcBankAccountExport.NC]);
    });

    it('will displays that cash-in account is export both Dwolla and NC', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([BazaNcBankAccountExport.Dwolla, BazaNcBankAccountExport.NC]);

        expect(response[0].isDefault).toBeTruthy();
        expect(response[1].isDefault).toBeFalsy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will export another account to NC (w/o mark it as default)', async () => {
        const response = await dataAccess.export({
            ulid: BANK_ACCOUNTS[1].ulid,
            services: [BazaNcBankAccountExport.NC],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.exported).toEqual([BazaNcBankAccountExport.NC]);
    });

    it('will display that previous account is not marked as exported to NC anymore', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([BazaNcBankAccountExport.Dwolla]);
        expect(response[1].exported).toEqual([BazaNcBankAccountExport.NC]);

        expect(response[0].isDefault).toBeTruthy();
        expect(response[1].isDefault).toBeFalsy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will exports account to Dwolla and mark it as default', async () => {
        const response = await dataAccess.export({
            ulid: BANK_ACCOUNTS[1].ulid,
            services: [BazaNcBankAccountExport.Dwolla],
            setAsDefault: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.exported).toEqual([BazaNcBankAccountExport.NC, BazaNcBankAccountExport.Dwolla]);
        expect(response.isDefault).toBeTruthy();
    });

    it('will display that account  marked as exported to Dwolla and as default', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([BazaNcBankAccountExport.Dwolla]);
        expect(response[1].exported).toEqual([BazaNcBankAccountExport.NC, BazaNcBankAccountExport.Dwolla]);

        expect(response[0].isDefault).toBeFalsy();
        expect(response[1].isDefault).toBeTruthy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will allow to export cash-out bank account to NC', async () => {
        const response = await dataAccess.export({
            ulid: BANK_ACCOUNTS[4].ulid,
            services: [BazaNcBankAccountExport.Dwolla],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.exported).toEqual([BazaNcBankAccountExport.Dwolla]);
        expect(response.isDefault).toBeFalsy();
    });

    it('will display that account marked as exported to NC and as default (2)', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([BazaNcBankAccountExport.Dwolla]);
        expect(response[1].exported).toEqual([BazaNcBankAccountExport.NC, BazaNcBankAccountExport.Dwolla]);
        expect(response[4].exported).toEqual([BazaNcBankAccountExport.Dwolla]);

        expect(response[0].isDefault).toBeFalsy();
        expect(response[1].isDefault).toBeTruthy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });

    it('will correctly fails with invalid routing number when exporting invalid account to Dwolla', async () => {
        const response: BazaError = (await dataAccess.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '1111222233330000',
            accountRoutingNumber: '123',
            accountName: 'John Doe',
            accountNickName: 'JD',
            accountType: AccountTypeCheckingSaving.Checking,
            export: [BazaNcBankAccountExport.Dwolla],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaDwollaErrorCodes.BazaDwollaApiError);
        expect(response.message).toBe('Routing number must be exactly 9 characters.');
    });

    it('will still display that only 5 bank accounts available in list', async () => {
        const response = await dataAccess.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(5);

        expect(response[0].exported).toEqual([BazaNcBankAccountExport.Dwolla]);
        expect(response[1].exported).toEqual([BazaNcBankAccountExport.NC, BazaNcBankAccountExport.Dwolla]);

        expect(response[0].isDefault).toBeFalsy();
        expect(response[1].isDefault).toBeTruthy();
        expect(response[2].isDefault).toBeFalsy();
        expect(response[3].isDefault).toBeTruthy();
        expect(response[4].isDefault).toBeFalsy();
    });
});
