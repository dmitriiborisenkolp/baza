import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import {
    BazaNcBankAccountsNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcSyncCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { AccountRole, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import {
    AccountTypeCheckingSaving,
    BazaNcBankAccountErrorCodes,
    BazaNcBankAccountExport,
    BazaNcBankAccountType,
} from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/bank-account/009-baza-nc-bank-account-nc-ach-legacy-flow.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessNBA = new BazaNcBankAccountsNodeAccess(http);
    const dataAccessLBA = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessNcSyncCms = new BazaNcSyncCmsNodeAccess(http);

    let USER_ID: number;
    let USER_EMAIL: string;
    let ulidCashIn1: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
        });

        USER_ID = accounts.items[0].id;
        USER_EMAIL = accounts.items[0].email;
    });

    beforeEach(async () => {
        await http.auth({
            email: USER_EMAIL,
            password: 'e2e-user-password',
        });
    });

    it('[NBA] will display that no bank account is set yet', async () => {
        const response = await dataAccessNBA.ncAchBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
        expect(response.details).not.toBeDefined();
    });

    it('[LBA] will display that no bank account is set yet', async () => {
        const response = await dataAccessLBA.getBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
        expect(response.details).not.toBeDefined();
    });

    it('will successfully create dwolla customer for investor', async () => {
        const response = await dataAccessDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBeTruthy();
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });

    it('will link NC ACH using LBA', async () => {
        const response = await dataAccessLBA.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('1111222233330000');
    });

    it('will display that account is set on NC side', async () => {
        const response = await dataAccessLBA.getBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('1111222233330000');
    });

    it('will display that no bank accounts set in NBA', async () => {
        const response = await dataAccessNBA.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(0);
    });

    it('will display that NC ACH Account is linked in Investor Account DTO', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: false,
            isBankAccountCashOutLinked: false,
        });
    });

    it('[NBA] will not allow to set up secured cash-in account without exports', async () => {
        const response: BazaError = (await dataAccessNBA.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '1111222233330001',
            accountRoutingNumber: '011401533',
            accountName: 'John Doe',
            accountNickName: 'JD',
            accountType: AccountTypeCheckingSaving.Savings,
            setAsDefault: true,
            secure: true,
            static: false,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcBankAccountErrorCodes.BazaNcBankAccountExportsAreRequired);
    });

    it('[NBA] will set up cash-in only account', async () => {
        const response = await dataAccessNBA.add({
            type: BazaNcBankAccountType.CashIn,
            accountNumber: '1111222233330001',
            accountRoutingNumber: '011401533',
            accountName: 'John Doe',
            accountNickName: 'JD',
            accountType: AccountTypeCheckingSaving.Savings,
            setAsDefault: true,
            secure: true,
            static: false,
            export: [BazaNcBankAccountExport.Dwolla],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ulidCashIn1 = response.ulid;
    });

    it('will display that both NC ACH Account and Cash-In Account are linked in Investor Account DTO (1)', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: true,
            isBankAccountCashOutLinked: false,
        });
    });

    it('will display that same account as before is set on NC side', async () => {
        const response = await dataAccessLBA.getBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('1111222233330000');
    });

    it('will display that Cash-In account is set', async () => {
        const response = await dataAccessNBA.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.result).toBeDefined();
        expect(response.result.accountNumber).toBe('************0001');
    });

    it("will not allow to re-export Cash-In Account to NC (because it's secured)", async () => {
        const response: BazaError = (await dataAccessNBA.export({
            ulid: ulidCashIn1,
            setAsDefault: true,
            services: [BazaNcBankAccountExport.NC],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcBankAccountErrorCodes.BazaNcBankAccountIsSecured);
    });

    it('[LBA] will set NC ACH account same as Cash In', async () => {
        const response = await dataAccessLBA.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330001',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('1111222233330001');
    });

    it('will display that new account is set is set on NC side', async () => {
        const response = await dataAccessLBA.getBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('1111222233330001');
    });

    it('will display same cash in account as it was before', async () => {
        const response = await dataAccessNBA.default(BazaNcBankAccountType.CashIn);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.result).toBeDefined();
        expect(response.result.accountNumber).toBe('************0001');
    });

    it('will display that flags remains unchanged (1)', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: true,
            isBankAccountCashOutLinked: false,
        });
    });

    it('will sync payment methods (1)', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNcSyncCms.syncInvestorAccountPaymentMethods({
            userId: USER_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that flags remains unchanged (2)', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: true,
            isBankAccountCashInLinked: true,
            isBankAccountCashOutLinked: false,
        });
    });

    it('will delete NC ACH account', async () => {
        const response = await dataAccessLBA.deleteBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will correctly updates flags', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: false,
            isBankAccountCashInLinked: true,
            isBankAccountCashOutLinked: false,
        });
    });

    it('will sync payment methods (1)', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNcSyncCms.syncInvestorAccountPaymentMethods({
            userId: USER_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that flags remains unchanged (3)', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response).toMatchObject({
            isBankAccountLinked: true,
            isBankAccountNcAchLinked: false,
            isBankAccountCashInLinked: true,
            isBankAccountCashOutLinked: false,
        });
    });
});
