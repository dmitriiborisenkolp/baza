import { CqrsModule } from '@nestjs/cqrs';
import { Global, Module } from '@nestjs/common';
import { BazaDwollaApiModule } from '@scaliolabs/baza-dwolla-api';
import { BazaPlaidApiModule } from '@scaliolabs/baza-plaid-api';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { BazaNcInvestorAccountApiModule } from '../../investor-account';
import { BazaNcBankAccountsService } from './services/baza-nc-bank-accounts.service';
import { BazaNcBankAccountsController } from './controllers/baza-nc-bank-accounts.controller';
import { BazaNcBankAccountMapper } from './mappers/baza-nc-bank-account.mapper';
import { BazaNcBankAccountsNcService } from './services/baza-nc-bank-accounts-nc.service';
import { BazaNcBankAccountsDwollaService } from './services/baza-nc-bank-accounts-dwolla.service';
import { BazaNcAchApiModule } from '../../ach';

@Global()
@Module({
    controllers: [BazaNcBankAccountsController],
    imports: [
        CqrsModule,
        BazaNcTypeormApiModule,
        BazaNcInvestorAccountApiModule,
        BazaDwollaApiModule,
        BazaNcAchApiModule,
        BazaPlaidApiModule,
    ],
    providers: [BazaNcBankAccountsService, BazaNcBankAccountMapper, BazaNcBankAccountsNcService, BazaNcBankAccountsDwollaService],
    exports: [BazaNcBankAccountsService, BazaNcBankAccountMapper],
})
export class BazaNcBankAccountsApiModule {}
