import { Injectable } from '@nestjs/common';
import { BazaNcBankAccountEntity } from '../../../typeorm';
import { BazaNcBankAccountDto } from '@scaliolabs/baza-nc-shared';
import { maskString } from '@scaliolabs/baza-core-shared';

/**
 * BazaNcBankAccountEntity to BazaNcBankAccountDto Mapper
 */
@Injectable()
export class BazaNcBankAccountMapper {
    entityToDTO(entity: BazaNcBankAccountEntity): BazaNcBankAccountDto {
        return {
            ulid: entity.ulid,
            type: entity.type,
            exported: entity.exported,
            source: entity.source,
            isDefault: entity.isDefault,
            name: entity.name,
            accountName: entity.accountName,
            accountNickName: entity.accountNickName,
            accountRoutingNumber: maskString(entity.accountRoutingNumber, { numCharacters: 4 }),
            accountNumber: maskString(entity.accountNumber, { numCharacters: 4 }),
            accountType: entity.accountType,
        };
    }

    entitiesToDTOs(entities: Array<BazaNcBankAccountEntity>): Array<BazaNcBankAccountDto> {
        return entities.map((next) => this.entityToDTO(next));
    }
}
