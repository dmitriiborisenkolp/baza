import { HttpStatus, Injectable } from '@nestjs/common';
import {
    BazaNcBankAccountEntity,
    BazaNcBankAccountRepository,
    BazaNcInvestorAccountEntity,
    BazaNcInvestorAccountRepository,
} from '../../../typeorm';
import { BazaNcBankAccountNotFoundException } from '../exceptions/baza-nc-bank-account-not-found.exception';
import {
    AccountTypeCheckingSaving,
    bazaNcApiConfig,
    BazaNcBankAccountAchDto,
    BazaNcBankAccountAddRequest,
    BazaNcBankAccountDto,
    BazaNcBankAccountE2eConstants,
    BazaNcBankAccountExport,
    BazaNcBankAccountExportRequest,
    BazaNcBankAccountLinkOnSuccessRequest,
    BazaNcBankAccountSetRequest,
    BazaNcBankAccountSource,
    BazaNcBankAccountType,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountNoDefaultException } from '../exceptions/baza-nc-bank-account-no-default.exception';
import { BazaNcBankAccountDuplicateException } from '../exceptions/baza-nc-bank-account-duplicate.exception';
import { BazaNcBankAccountsNcService } from './baza-nc-bank-accounts-nc.service';
import { BazaNcBankAccountsDwollaService } from './baza-nc-bank-accounts-dwolla.service';
import { BazaNcBankAccountUnknownExportServiceException } from '../exceptions/baza-nc-bank-account-unknown-export-service.exception';
import { BazaPlaidService } from '@scaliolabs/baza-plaid-api';
import {
    BazaPlaidErrorCodes,
    bazaPlaidErrorCodesI18n,
    BazaPlaidLinkRequestDto,
    BazaPlaidLinkResponseDto,
} from '@scaliolabs/baza-plaid-shared';
import { BazaAppException, EnvService } from '@scaliolabs/baza-core-api';
import { AccountSubtype as PlaidAccountSubtype } from 'plaid';
import { BazaNcAchService } from '../../../ach';
import { maskString } from '@scaliolabs/baza-core-shared';
import { BazaNcBankAccountExportsAreRequiredException } from '../exceptions/baza-nc-bank-account-exports-are-required.exception';
import { BazaNcBankAccountIsSecuredException } from '../exceptions/baza-nc-bank-account-is-secured.exception';
import { BazaNcDwollaPlaidLinkE2eConstants } from '../constants/baza-nc-dwolla-plaid-link-e2e.constants';
import { BazaNcBankAccountNoIdentityException } from '../exceptions/baza-nc-bank-account-no-identity.exception';
import { DwollaFundingSourcesApiNestjsService } from '@scaliolabs/baza-dwolla-api';

interface BazaNcBankAccountsExportService {
    export(
        target: Omit<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
        investorAccount: BazaNcInvestorAccountEntity,
        bankAccounts: Array<BazaNcBankAccountEntity>,
        accountNumbers: Pick<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
    ): Promise<void>;
}

@Injectable()
export class BazaNcBankAccountsService {
    constructor(
        private readonly env: EnvService,
        private readonly repository: BazaNcBankAccountRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly ncAchService: BazaNcAchService,
        private readonly bankAccountNc: BazaNcBankAccountsNcService,
        private readonly bankAccountDwolla: BazaNcBankAccountsDwollaService,
        private readonly bazaPlaidService: BazaPlaidService,
        private readonly dwollaFundingSourcesApi: DwollaFundingSourcesApiNestjsService,
    ) {}

    private readonly exportServicesMap: Array<{
        export: BazaNcBankAccountExport;
        service: BazaNcBankAccountsExportService;
    }> = [
        { export: BazaNcBankAccountExport.NC, service: this.bankAccountNc },
        { export: BazaNcBankAccountExport.Dwolla, service: this.bankAccountDwolla },
    ];

    /**
     * Returns map of supported Plaid <> NC, Dwolla account types
     * Baza NC libraries supports only Checking or Savings accounts
     */
    get mapPlaidToNcAccountType(): Array<{
        plaid: PlaidAccountSubtype;
        nc: AccountTypeCheckingSaving;
    }> {
        return [
            { plaid: PlaidAccountSubtype.Checking, nc: AccountTypeCheckingSaving.Checking },
            { plaid: PlaidAccountSubtype.Savings, nc: AccountTypeCheckingSaving.Savings },
        ];
    }

    /**
     * Returns list of Bank Accounts for given Investor Account
     * @param investorAccountId
     */
    async list(investorAccountId: number): Promise<Array<BazaNcBankAccountEntity>> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountById(investorAccountId);

        if (investorAccount) {
            return this.repository.findByInvestorAccount(investorAccount);
        } else {
            return [];
        }
    }

    /**
     * Returns list of Default Bank Accounts (1 per Bank Account Type) for given Investor Account
     * @param investorAccountId
     * @param type
     */
    async defaults(investorAccountId: number, type?: BazaNcBankAccountType): Promise<Array<BazaNcBankAccountEntity>> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountById(investorAccountId);

        if (investorAccount) {
            const types = type ? [type] : Object.values(BazaNcBankAccountType);
            const defaults = await this.repository.findDefaultsByInvestorAccount(investorAccount);

            return types
                .map((next) => defaults.find((bankAccount) => bankAccount.isDefault && bankAccount.type === next))
                .filter((next) => !!next);
        } else {
            return [];
        }
    }

    /**
     * Returns Default Bank Account for given Investor Account and Bank Account Type
     * If not found, the method will throw an BazaNcBankAccountNoDefaultException
     * @param investorAccountId
     * @param type
     */
    async default(investorAccountId: number, type: BazaNcBankAccountType): Promise<BazaNcBankAccountEntity> {
        const result = await this.findDefault(investorAccountId, type);

        if (!result) {
            throw new BazaNcBankAccountNoDefaultException();
        }

        return result;
    }

    /**
     * Returns Default Bank Account for given Investor Account and Bank Account Type
     * If not found, the method will return `undefined`
     * @param investorAccountId
     * @param type
     */
    async findDefault(investorAccountId: number, type: BazaNcBankAccountType): Promise<BazaNcBankAccountEntity | undefined> {
        return (await this.defaults(investorAccountId)).find((next) => next.type === type);
    }

    /**
     * Returns Bank Account by ULID
     * If Bank Account was not found or Bank Account is not attached to given Investor Account, the method
     * will throw BazaNcBankAccountNotFoundException
     * @param investorAccountId
     * @param ulid
     */
    async getByUlid(investorAccountId: number, ulid: string) {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountById(investorAccountId);

        if (!investorAccount) {
            throw new BazaNcBankAccountNotFoundException();
        }

        const bankAccount = await this.repository.getByUlid(ulid);

        if (bankAccount.investorAccount.id !== investorAccount.id) {
            throw new BazaNcBankAccountNotFoundException();
        }

        return bankAccount;
    }

    /**
     * Returns true if there is a similar bank account exists with same Type, Account Name, Account Number and Account Routing
     * Number
     * @param investorAccountId
     * @param bankAccount
     */
    async findBankAccount(
        investorAccountId: number,
        bankAccount: Pick<BazaNcBankAccountDto, 'type' | 'accountNumber' | 'accountRoutingNumber'>,
    ): Promise<BazaNcBankAccountEntity | undefined> {
        const bankAccounts = await this.list(investorAccountId);

        return bankAccounts.find(
            (next) =>
                next.type === bankAccount.type &&
                next.accountNumber === bankAccount.accountNumber &&
                next.accountRoutingNumber === bankAccount.accountRoutingNumber &&
                next.secure === false,
        );
    }

    /**
     * Creates new Bank Account
     * @param investorAccountId
     * @param request
     * @param additionalOptions
     */
    async add(
        investorAccountId: number,
        request: BazaNcBankAccountAddRequest,
        additionalOptions: {
            plaidAccountId?: string;
            dwollaProcessorToken?: string;
            returnDuplicate?: boolean;
            markAsLinkedWithLegacy?: boolean;
        } = {},
    ): Promise<BazaNcBankAccountEntity> {
        if (bazaNcApiConfig().forceSecureBankAccounts) {
            request.secure = true;
        }

        const isSecured = !!request.secure;
        const hasExports = Array.isArray(request.export) && request.export.length > 0;

        if (isSecured && !hasExports) {
            throw new BazaNcBankAccountExportsAreRequiredException();
        }

        const entity = await (async () => {
            const existing = await this.findBankAccount(investorAccountId, request);

            if (existing && !additionalOptions.returnDuplicate) {
                throw new BazaNcBankAccountDuplicateException();
            } else if (existing) {
                return existing;
            } else {
                const entity = new BazaNcBankAccountEntity();
                const accounts = (await this.list(investorAccountId)).filter((next) => next.type === request.type);

                entity.type = request.type;
                entity.investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);
                entity.source = BazaNcBankAccountSource.Manual;
                entity.isDefault = request.setAsDefault !== false && accounts.length === 0;
                entity.name =
                    request.name ||
                    bazaNcApiConfig().bankAccountNameFactory({
                        type: request.type,
                        accountType: request.accountType,
                        accountName: request.accountName,
                        accountNickName: request.accountNickName,
                    });
                entity.accountName = request.accountName;
                entity.accountNickName = request.accountNickName;
                entity.accountNumber = request.accountNumber;
                entity.accountRoutingNumber = request.accountRoutingNumber;
                entity.accountType = request.accountType;
                entity.plaidAccountId = additionalOptions.plaidAccountId;
                entity.dwollaProcessorToken = additionalOptions.dwollaProcessorToken;
                entity.secure = isSecured;

                if (isSecured) {
                    entity.accountNumber = maskString(entity.accountNumber, { numCharacters: 4 });
                    entity.accountRoutingNumber = maskString(entity.accountRoutingNumber, { numCharacters: 4 });
                }

                await this.repository.save([entity]);

                return entity;
            }
        })();

        if (additionalOptions.markAsLinkedWithLegacy && !entity.isLinkedWithLegacy) {
            entity.isLinkedWithLegacy = true;

            await this.repository.save([entity]);
        }

        if (request.export) {
            try {
                if (request.static) {
                    const removedBankAccounts = await this.repository.removeAllBankAccountsExcepts(entity);

                    const userCurrentBankAccounts = await this.repository.findByInvestorAccount(
                        await this.investorAccountRepository.findInvestorAccountById(investorAccountId),
                    );

                    for (const bankAccount of removedBankAccounts) {
                        const existingDuplicateBankAccount = userCurrentBankAccounts.find(
                            (acc) => acc.dwollaFundingSourceId === bankAccount.dwollaFundingSourceId,
                        );

                        if (!existingDuplicateBankAccount && bankAccount.dwollaFundingSourceId) {
                            await this.dwollaFundingSourcesApi.removeFundingSource(bankAccount.dwollaFundingSourceId, {
                                removed: true,
                            });
                        }
                    }
                }

                await this.export(
                    investorAccountId,
                    {
                        ulid: entity.ulid,
                        services: request.export,
                    },
                    {
                        accountNumber: request.accountNumber,
                        accountRoutingNumber: request.accountRoutingNumber,
                    },
                );
            } catch (exception) {
                await this.repository.remove([entity]);

                throw exception;
            }
        }

        if (request.setAsDefault) {
            await this.set(investorAccountId, {
                ulid: entity.ulid,
            });
        }

        if (request.static) {
            await this.repository.removeAllBankAccountsExcepts(entity);
        }

        await this.updateInvestorBankAccountStatuses(investorAccountId);

        return this.repository.getByUlid(entity.ulid);
    }

    /**
     * Returns Plaid Link Token
     * You should use the Link Token for displaying Plaid Link widget. Use `linkOnSuccess` endpoint for handling
     * onSuccess callback
     * @param investorAccountId
     * @param options
     */
    async link(investorAccountId: number, options: BazaPlaidLinkRequestDto): Promise<BazaPlaidLinkResponseDto> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);

        return this.bazaPlaidService.getPlaidLink(investorAccount.user, options);
    }

    /**
     * Plaid Link On-Success Handler
     * The method returns Bank Account. Additionally, it warns if there is a duplicate account already exists
     * @param investorAccountId
     * @param request
     */
    async linkOnSuccess(
        investorAccountId: number,
        request: BazaNcBankAccountLinkOnSuccessRequest,
    ): Promise<{
        duplicate: boolean;
        bankAccount: BazaNcBankAccountEntity;
    }> {
        if (bazaNcApiConfig().forceSecureBankAccounts) {
            request.secure = true;
        }

        const isE2E =
            this.env.isTestEnvironment &&
            [BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_CHECKING, BazaNcBankAccountE2eConstants.E2E_PLAID_TOKEN_SAVINGS].includes(
                request.publicToken,
            );

        const accessToken = !isE2E ? await this.bazaPlaidService.getExchangeToken(request.publicToken) : request.publicToken;

        const bankDetails = !isE2E
            ? await this.bazaPlaidService.getBankDetails(accessToken)
            : BazaNcDwollaPlaidLinkE2eConstants.getMock(accessToken);

        if (bankDetails.accounts.length !== 1) {
            throw new BazaAppException(
                BazaPlaidErrorCodes.BazaPlaidInvalidAccountsNumber,
                bazaPlaidErrorCodesI18n[BazaPlaidErrorCodes.BazaPlaidInvalidAccountsNumber],
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }

        const bankAccount = bankDetails.accounts[0];
        const bankAccountId = bankAccount.account_id;
        const bankAccountType = this.mapPlaidToNcAccountType.find((next) => next.plaid === bankAccount.subtype)?.nc;
        const achNumbers = bankDetails.numbers.ach.find((next) => next.account_id === bankAccountId);
        const accountInstitutionName = request.identity?.institution?.name;

        let bankAccountName;
        let bakAccountHolderName;

        if (!bankAccountType) {
            throw new BazaAppException(
                BazaPlaidErrorCodes.BazaPlaidUnsupporterAccountType,
                bazaPlaidErrorCodesI18n[BazaPlaidErrorCodes.BazaPlaidUnsupporterAccountType],
                HttpStatus.CONFLICT,
            );
        }

        if (!achNumbers) {
            throw new BazaAppException(
                BazaPlaidErrorCodes.BazaPlaidACHNumbersNotFound,
                bazaPlaidErrorCodesI18n[BazaPlaidErrorCodes.BazaPlaidACHNumbersNotFound],
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }

        if (request.identity) {
            bankAccountName = request.identity.account.name;
            bakAccountHolderName = request.identity.account.name;
        } else {
            const identity = !isE2E
                ? await this.bazaPlaidService.getIdentity(accessToken, [bankAccountId])
                : BazaNcDwollaPlaidLinkE2eConstants.getIdentity();

            bankAccountName = identity.accounts[0].name;
            bakAccountHolderName = identity.accounts[0].owners[0]?.names[0] || identity.accounts[0].name;
        }

        if (!bankAccountName || !bakAccountHolderName) {
            throw new BazaNcBankAccountNoIdentityException();
        }

        const existing = await this.findBankAccount(investorAccountId, {
            type: request.type,
            accountNumber: achNumbers.account,
            accountRoutingNumber: achNumbers.routing,
        });

        const dwollaProcessorToken = !isE2E ? await this.bazaPlaidService.getDwollaProcessorToken(accessToken, bankAccountId) : undefined;

        if (existing && !(existing.plaidAccountId && existing.dwollaProcessorToken)) {
            existing.plaidAccountId = bankAccountId;
            existing.dwollaProcessorToken = dwollaProcessorToken;

            await this.repository.save([existing]);
        }

        // TODO: Later wrap it with request.static and check out does it works properly
        if (!isE2E) {
            await this.cleanUpStaticAccounts(investorAccountId);
        }

        const addedBankAccount =
            existing ||
            (await this.add(
                investorAccountId,
                {
                    accountType: bankAccountType,
                    export: request.export,
                    name: bankAccountName,
                    accountName: accountInstitutionName || bakAccountHolderName,
                    accountNickName: bakAccountHolderName,
                    type: request.type,
                    accountNumber: achNumbers.account,
                    accountRoutingNumber: achNumbers.routing,
                    setAsDefault: request.setAsDefault,
                    secure: request.secure,
                    static: !isE2E, // TODO: Later replace it with request.static and check out does it works properly
                },
                {
                    plaidAccountId: bankAccountId,
                    dwollaProcessorToken,
                },
            ));

        if (request.static) {
            await this.cleanUpStaticAccounts(investorAccountId);
        }

        if (request.setAsDefault && existing && !existing.isDefault) {
            existing.isDefault = true;

            await this.set(investorAccountId, {
                ulid: existing.ulid,
                types: [request.type],
                static: request.static,
            });
        }

        return {
            duplicate: !!existing,
            bankAccount: addedBankAccount,
        };
    }

    /**
     * Returns similar bank account with specific type. If there are no similar bank accounts for given type available,
     * the method will automatically create duplicate
     * @param investorAccountId
     * @param bankAccount
     * @param type
     */
    async similar(
        investorAccountId: number,
        bankAccount: BazaNcBankAccountEntity,
        type: BazaNcBankAccountType,
    ): Promise<BazaNcBankAccountEntity> {
        const similar = await this.findBankAccount(investorAccountId, { ...bankAccount, type });
        const result = similar ? similar : this.add(investorAccountId, { ...bankAccount, type });

        await this.updateInvestorBankAccountStatuses(investorAccountId);

        return result;
    }

    /**
     * Removes bank account. If bank account is selected as default, then first available account
     * will be selected as default
     * @param investorAccountId
     * @param ulid
     */
    async remove(investorAccountId: number, ulid: string): Promise<void> {
        const accounts = await this.list(investorAccountId);
        const target = accounts.find((next) => next.ulid === ulid);

        if (!target) {
            throw new BazaNcBankAccountNotFoundException();
        }

        const accountsGroup = accounts.filter((next) => next.ulid !== target.ulid && next.type === target.type);
        const dwollaFundingSourceId = target.dwollaFundingSourceId;

        if (target.isDefault && accountsGroup.length > 0) {
            const first = accountsGroup[0];

            await this.set(investorAccountId, {
                ulid: first.ulid,
            });
        }

        await this.repository.remove([target]);

        if (dwollaFundingSourceId) {
            await this.dwollaFundingSourcesApi.removeFundingSource(dwollaFundingSourceId, {
                removed: true,
            });
        }

        await this.updateInvestorBankAccountStatuses(investorAccountId);
    }

    /**
     * Sets bank account as default
     * If request.types is provided, the method may create duplicate bank accounts for types which does now have
     * similar bank accounts
     * Note: The returned bank account may not be set as default in case when it sets up as default for different
     * bank accounts types. Instead similar bank accont or newly created bank account will be set as default!
     * @see BazaNcBankAccountsService.add
     * @see BazaNcBankAccountsService.findBankAccount
     * @param investorAccountId
     * @param request
     */
    async set(investorAccountId: number, request: BazaNcBankAccountSetRequest): Promise<Array<BazaNcBankAccountEntity>> {
        const bankAccounts = await this.list(investorAccountId);
        const bankAccount = bankAccounts.find((next) => next.ulid === request.ulid);
        const types = request.types || [bankAccount.type];

        if (!bankAccount) {
            throw new BazaNcBankAccountNotFoundException();
        }

        const result: Array<BazaNcBankAccountEntity> = [];

        for (const type of types) {
            const bankAccountsOfType = bankAccounts.filter((next) => next.type === type);

            if (bankAccount.type === type) {
                bankAccountsOfType.forEach((next) => (next.isDefault = next.ulid === request.ulid));

                const newDefaultBankAccount = bankAccountsOfType.find((next) => next.isDefault);

                if (!newDefaultBankAccount) {
                    throw new BazaNcBankAccountNoDefaultException();
                }

                await this.repository.save(bankAccountsOfType);

                if (request.static) {
                    await this.repository.removeAllBankAccountsExcepts(bankAccount);
                }

                result.push(newDefaultBankAccount);
            } else {
                const similar = await this.similar(investorAccountId, bankAccount, type);

                bankAccountsOfType.forEach((next) => (next.isDefault = false));
                similar.isDefault = true;

                await this.repository.save(bankAccountsOfType);
                await this.repository.save([similar]);

                if (request.static) {
                    await this.repository.removeAllBankAccountsExcepts(similar);
                }

                result.push(similar);
            }
        }

        await this.updateInvestorBankAccountStatuses(investorAccountId);

        return result;
    }

    /**
     * Updates `isBankAccountLinked` and `isBankAccountCashOutLinked` flags of Investor Accounts
     * @see BazaNcInvestorAccountEntity.isBankAccountLinked
     * @see BazaNcInvestorAccountEntity.isBankAccountCashOutLinked
     * @param investorAccountId
     */
    async updateInvestorBankAccountStatuses(investorAccountId: number): Promise<void> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);
        const bankAccounts = await this.defaults(investorAccountId);

        const ncAchAccount = await this.ncAchService.getBankAccount(investorAccount);

        const cashInAccount = bankAccounts.find((next) => next.type === BazaNcBankAccountType.CashIn && next.isDefault);
        const cashOutAccount = bankAccounts.find((next) => next.type === BazaNcBankAccountType.CashOut && next.isDefault);

        investorAccount.isBankAccountLinked = ncAchAccount.isAvailable || !!cashInAccount;
        investorAccount.isBankAccountNcAchLinked = ncAchAccount.isAvailable;
        investorAccount.isBankAccountCashInLinked = !!cashInAccount;
        investorAccount.isBankAccountCashOutLinked = !!cashOutAccount;

        await this.investorAccountRepository.save(investorAccount);
    }

    /**
     * Exports Bank Account to external services (North Capital or Dwolla)
     * Only one Bank Account could be exported to NC at once. Additional export calls will mark previosly exported
     * bank accounts as not exported to NC. Also, you can export only Cash-In Bank Accounts which will be used
     * for Cash-In operations or for purchasing shares using legacy API
     * Any number of Bank Accounts of any type could be exported to Dwolla.
     * @param investorAccountId
     * @param request
     * @param withRealNumbers
     */
    async export(
        investorAccountId: number,
        request: BazaNcBankAccountExportRequest,
        withRealNumbers?: {
            accountNumber: string;
            accountRoutingNumber: string;
        },
    ): Promise<BazaNcBankAccountEntity> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);
        const bankAccounts = await this.list(investorAccountId);
        const target = bankAccounts.find((next) => next.ulid === request.ulid);

        if (!target) {
            throw new BazaNcBankAccountNotFoundException();
        }

        if (target.secure && !withRealNumbers) {
            throw new BazaNcBankAccountIsSecuredException();
        }

        for (const service of request.services) {
            const def = this.exportServicesMap.find((next) => next.export === service);

            if (!def) {
                throw new BazaNcBankAccountUnknownExportServiceException(service);
            }

            await def.service.export(
                target,
                investorAccount,
                bankAccounts,
                withRealNumbers || {
                    accountNumber: target.accountNumber,
                    accountRoutingNumber: target.accountRoutingNumber,
                },
            );
        }

        return request.setAsDefault ? (await this.set(investorAccountId, { ulid: target.ulid }))[0] : target;
    }

    /**
     * Removes Bank Accounts which are marked with Legacy flag
     * @param investorAccountId
     */
    async deleteLegacyBankAccounts(investorAccountId: number): Promise<void> {
        const bankAccounts = (await this.list(investorAccountId)).filter((next) => next.isLinkedWithLegacy);

        if (bankAccounts.length > 0) {
            for (const bankAccount of bankAccounts) {
                await this.remove(investorAccountId, bankAccount.ulid);
            }
        }
    }

    /**
     * Returns Bank Account Details for ACH transactions which are currently set on NC
     * These details will be immediately used for next purchasing shares session
     * It's recommended to use this endpoint instead of `default` for displaying actual data on NC side instead of
     * possible data on Baza side
     * @param investorAccountId
     */
    async ncAchBankAccount(investorAccountId: number): Promise<BazaNcBankAccountAchDto> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountById(investorAccountId);

        if (!investorAccount) {
            return {
                isAvailable: false,
            };
        }

        const result = await this.ncAchService.getBankAccount(investorAccount);

        return {
            ...result,
            details: result.isAvailable
                ? {
                      ...result.details,
                      accountNumber: maskString(result.details.accountNumber, { numCharacters: 4 }),
                      accountRoutingNumber: maskString(result.details.accountRoutingNumber, { numCharacters: 4 }),
                  }
                : undefined,
        };
    }

    /**
     * Clean up list of Bank Accounts
     * The method will also remove Funding Source from Dwolla
     * @param investorAccountId
     */
    async cleanUpStaticAccounts(investorAccountId: number): Promise<void> {
        const accounts = await this.list(investorAccountId);
        const excludeTypes = Object.values(BazaNcBankAccountType).filter(
            (next) => accounts.filter((account) => account.type == next && !account.isDefault).length === 0,
        );

        const accountsToRemove = accounts.filter((next) => !next.isDefault && !excludeTypes.includes(next.type));

        for (const account of accountsToRemove) {
            await this.remove(investorAccountId, account.ulid);
        }
    }
}
