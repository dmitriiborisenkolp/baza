import { Injectable } from '@nestjs/common';
import { BazaNcBankAccountExport, BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';
import { BazaNcAchService } from '../../../ach';
import { BazaNcBankAccountEntity, BazaNcBankAccountRepository, BazaNcInvestorAccountEntity } from '../../../typeorm';
import { BazaNcBankAccountCannotBeExportedToNcException } from '../exceptions/baza-nc-bank-account-cannot-be-exported-to-nc.exception';

/**
 * Internal service which is responsible to work with Bank Accounts + NC API
 */
@Injectable()
export class BazaNcBankAccountsNcService {
    constructor(private readonly achService: BazaNcAchService, private readonly repository: BazaNcBankAccountRepository) {}

    /**
     * Exports Bank Account to North Capital
     * @see BazaNcAchService.setBankAccount
     * @param target
     * @param investorAccount
     * @param bankAccounts
     * @param accountNumbers
     */
    async export(
        target: Omit<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
        investorAccount: BazaNcInvestorAccountEntity,
        bankAccounts: Array<BazaNcBankAccountEntity>,
        accountNumbers: Pick<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
    ): Promise<void> {
        if (target.type !== BazaNcBankAccountType.CashIn) {
            throw new BazaNcBankAccountCannotBeExportedToNcException();
        }

        bankAccounts.forEach((next) => (next.exported = next.exported.filter((service) => service !== BazaNcBankAccountExport.NC)));

        await this.achService.setBankAccount(investorAccount, {
            accountName: target.accountName.toUpperCase(),
            accountNumber: accountNumbers.accountNumber,
            accountRoutingNumber: accountNumbers.accountRoutingNumber,
            accountType: target.accountType,
        });

        if (!target.exported.includes(BazaNcBankAccountExport.NC)) {
            target.exported.push(BazaNcBankAccountExport.NC);
        }

        await this.repository.save(bankAccounts);
    }
}
