import { Injectable } from '@nestjs/common';
import { BazaNcBankAccountEntity, BazaNcBankAccountRepository, BazaNcInvestorAccountEntity } from '../../../typeorm';
import { DwollaFundingSourcesApiNestjsService } from '@scaliolabs/baza-dwolla-api';
import { BazaNcBankAccountNoDwollaCustomerAvailableException } from '../exceptions/baza-nc-bank-account-no-dwolla-customer-available.exception';
import { bazaDwollaExtractId, DwollaBankAccountType, DwollaFundingSourceId } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcBankAccountCannotBeExportedToDwollaException } from '../exceptions/baza-nc-bank-account-cannot-be-exported-to-dwolla.exception';
import { AccountTypeCheckingSaving, bazaNcApiConfig, BazaNcBankAccountExport } from '@scaliolabs/baza-nc-shared';
import { BazaDwollaApiErrorException } from '@scaliolabs/baza-dwolla-api';

/**
 * Internal service which is responsible to work with Bank Accounts + Dwolla API
 */
@Injectable()
export class BazaNcBankAccountsDwollaService {
    constructor(
        private readonly repository: BazaNcBankAccountRepository,
        private readonly dwollaFundingSourcesApi: DwollaFundingSourcesApiNestjsService,
    ) {}

    /**
     * NC Bank Account Type -> Dwolla Bank Account Type map
     * @private
     */
    private mapDwollaToNcTypes: Array<{
        nc: AccountTypeCheckingSaving;
        dwolla: DwollaBankAccountType;
    }> = [
        { nc: AccountTypeCheckingSaving.Checking, dwolla: DwollaBankAccountType.Checking },
        { nc: AccountTypeCheckingSaving.Savings, dwolla: DwollaBankAccountType.Savings },
    ];

    /**
     * Exports Bank Account to Dwolla
     * @param target
     * @param investorAccount
     * @param bankAccounts
     * @param accountNumbers
     */
    async export(
        target: Omit<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
        investorAccount: BazaNcInvestorAccountEntity,
        bankAccounts: Array<BazaNcBankAccountEntity>,
        accountNumbers: Pick<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
    ): Promise<void> {
        if (!investorAccount.dwollaCustomerId) {
            throw new BazaNcBankAccountNoDwollaCustomerAvailableException();
        }

        target.dwollaFundingSourceId = target.dwollaProcessorToken
            ? await this.createBankAccountFundingSourceWithPlaidToken(target, investorAccount)
            : await this.createBankAccountFundingSource(target, investorAccount, accountNumbers);

        if (!target.exported.includes(BazaNcBankAccountExport.Dwolla)) {
            target.exported.push(BazaNcBankAccountExport.Dwolla);
        }

        await this.repository.save([target as BazaNcBankAccountEntity]);
    }

    /**
     * Creates Dwolla Funding Source.
     * If there is duplicate exists at Dwolla side, an ID of existing Dwolla Funding Source will be returned
     * @param target
     * @param investorAccount
     * @param accountNumbers
     * @private
     */
    private async createBankAccountFundingSource(
        target: Omit<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
        investorAccount: BazaNcInvestorAccountEntity,
        accountNumbers: Pick<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
    ): Promise<DwollaFundingSourceId> {
        const typeDef = this.mapDwollaToNcTypes.find((next) => next.nc === target.accountType);

        if (!typeDef) {
            throw new BazaNcBankAccountCannotBeExportedToDwollaException();
        }

        try {
            return await this.dwollaFundingSourcesApi.createBankAccountFundingSource(investorAccount.dwollaCustomerId, {
                bankAccountType: typeDef.dwolla,
                accountNumber: accountNumbers.accountNumber,
                routingNumber: accountNumbers.accountRoutingNumber,
                name: target.name || bazaNcApiConfig().bankAccountNameFactory(target),
            });
        } catch (error) {
            if (error instanceof BazaDwollaApiErrorException && error?.dwollaErrorCode === 'DuplicateResource') {
                const id = bazaDwollaExtractId((error._embedded?.about?.href || '').toString());

                if (id) {
                    return id;
                }
            }

            throw error;
        }
    }

    /**
     * Creates Dwolla Funding Source using Plaid Processor Token.
     * If there is duplicate exists at Dwolla side, an ID of existing Dwolla Funding Source will be returned
     * @param target
     * @param investorAccount
     * @private
     */
    private async createBankAccountFundingSourceWithPlaidToken(
        target: Omit<BazaNcBankAccountEntity, 'accountNumber' | 'accountRoutingNumber'>,
        investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<DwollaFundingSourceId> {
        const typeDef = this.mapDwollaToNcTypes.find((next) => next.nc === target.accountType);

        if (!typeDef) {
            throw new BazaNcBankAccountCannotBeExportedToDwollaException();
        }

        try {
            return await this.dwollaFundingSourcesApi.createFundingSourceWithPlaidToken(investorAccount.dwollaCustomerId, {
                name: target.name || bazaNcApiConfig().bankAccountNameFactory(target),
                plaidToken: target.dwollaProcessorToken,
            });
        } catch (error) {
            if (error instanceof BazaDwollaApiErrorException && error?.dwollaErrorCode === 'DuplicateResource') {
                const id = bazaDwollaExtractId((error._embedded?.about?.href || '').toString());

                if (id) {
                    return id;
                }
            }

            throw error;
        }
    }
}
