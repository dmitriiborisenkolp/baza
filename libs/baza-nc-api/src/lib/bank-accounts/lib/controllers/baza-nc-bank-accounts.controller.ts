import { Body, Controller, Delete, Get, Param, Post, Query, UseGuards, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiConflictResponse, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import {
    BazaNcBankAccountAddRequest,
    BazaNcBankAccountDto,
    BazaNcBankAccountEndpoint,
    BazaNcBankAccountEndpointPaths,
    BazaNcBankAccountErrorCodes,
    BazaNcBankAccountExportRequest,
    BazaNcBankAccountGetDefaultByTypeRequest,
    BazaNcBankAccountGetDefaultByTypeResponse,
    BazaNcBankAccountLinkOnSuccessRequest,
    BazaNcBankAccountLinkOnSuccessResponse,
    BazaNcBankAccountRemoveResponse,
    BazaNcBankAccountSetRequest,
    BazaNcBankAccountType,
    BazaNorthCapitalOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountMapper } from '../mappers/baza-nc-bank-account.mapper';
import { BazaNcBankAccountsService } from '../services/baza-nc-bank-accounts.service';
import { BazaPlaidLinkRequestDto, BazaPlaidLinkResponseDto } from '@scaliolabs/baza-plaid-shared';
import { BazaNcBankAccountAchDto } from '@scaliolabs/baza-nc-shared';
import { InvestorAccountOptionalGuard, ReqInvestorAccountId } from '../../../investor-account';
import { ulid } from 'ulid';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalBankAccounts)
@UseGuards(InvestorAccountOptionalGuard)
export class BazaNcBankAccountsController implements BazaNcBankAccountEndpoint {
    constructor(private readonly service: BazaNcBankAccountsService, private readonly mapper: BazaNcBankAccountMapper) {}

    @Get(BazaNcBankAccountEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of all available bank accounts',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountDto,
        isArray: true,
    })
    async list(@ReqInvestorAccountId() investorAccountId: number): Promise<Array<BazaNcBankAccountDto>> {
        const entities = await this.service.list(investorAccountId);

        return this.mapper.entitiesToDTOs(entities);
    }

    @Get(BazaNcBankAccountEndpointPaths.defaults)
    @ApiOperation({
        summary: 'defaults',
        description:
            'Returns list of bank accounts for each BankNcAccountType which are selected as default\n' +
            "Please note that user may didn't select bank account for some types yet - in this case there will be no" +
            'bank account with this type in response available',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountDto,
        isArray: true,
    })
    async defaults(@ReqInvestorAccountId() investorAccountId: number): Promise<Array<BazaNcBankAccountDto>> {
        const entities = await this.service.defaults(investorAccountId);

        return this.mapper.entitiesToDTOs(entities);
    }

    @Get(BazaNcBankAccountEndpointPaths.default)
    @ApiOperation({
        summary: 'default',
        description: 'Returns default bank account of given type',
    })
    @ApiParam({
        name: 'type',
        type: 'string',
        enum: Object.values(BazaNcBankAccountType),
        example: BazaNcBankAccountType.CashIn,
        description: 'Bank Account Type',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountGetDefaultByTypeResponse,
    })
    async default(
        @Param('type') type: BazaNcBankAccountType,
        @Query(
            new ValidationPipe({
                transform: true,
                forbidNonWhitelisted: true,
            }),
        )
        request: BazaNcBankAccountGetDefaultByTypeRequest,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<BazaNcBankAccountGetDefaultByTypeResponse> {
        const result = request.throwError
            ? await this.service.default(investorAccountId, type)
            : await this.service.findDefault(investorAccountId, type);

        const response: BazaNcBankAccountGetDefaultByTypeResponse = {
            isAvailable: false,
        };

        if (result) {
            response.isAvailable = true;
            response.result = result;
        }

        if (request.withOptions) {
            response.options = await this.service.defaults(investorAccountId, type);
        }

        return response;
    }

    @Get(BazaNcBankAccountEndpointPaths.get)
    @ApiOperation({
        summary: 'get',
        description: 'Returns Bank Account by ULID',
    })
    @ApiParam({
        name: 'ulid',
        type: 'string',
        example: ulid(),
        description: 'Bank Account Type',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountDto,
    })
    async get(@Param('ulid') ulid: string, @ReqInvestorAccountId() investorAccountId: number): Promise<BazaNcBankAccountDto> {
        const entity = await this.service.getByUlid(investorAccountId, ulid);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcBankAccountEndpointPaths.add)
    @ApiOperation({
        summary: 'add',
        description:
            'Adds a Bank Account (MANUAL)\n' +
            'Use `setAsDefault: true` to automatically set Bank Account as Default, i.e. use this account instantly for Purchase Flow / Future Payments',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountDto,
    })
    @ApiConflictResponse({
        description: `The method will throw an error with ${BazaNcBankAccountErrorCodes.BazaNcBankAccountDuplicate}
            error code if there is bank account with same Type, Account Name, Account Number and Account Routing Number
            exists`,
    })
    async add(
        @Body() request: BazaNcBankAccountAddRequest,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<BazaNcBankAccountDto> {
        const entity = await this.service.add(investorAccountId, request);

        return this.mapper.entityToDTO(entity);
    }

    @Get(BazaNcBankAccountEndpointPaths.link)
    @ApiOperation({
        summary: 'link',
        description:
            'Returns Plaid Link Token. \n' +
            'You should use the Link Token for displaying Plaid Link widget. Use `linkOnSuccess` endpoint for handling' +
            'onSuccess callback',
    })
    @ApiOkResponse({
        type: BazaPlaidLinkResponseDto,
    })
    async link(
        @Query() request: BazaPlaidLinkRequestDto,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<BazaPlaidLinkResponseDto> {
        return this.service.link(investorAccountId, request);
    }

    @Post(BazaNcBankAccountEndpointPaths.linkOnSuccess)
    @ApiOperation({
        summary: 'linkOnSuccess',
        description:
            'Plaid Link On-Success Handler. \n' +
            'The method returns Bank Account. Additionally it warns if there is a duplicate account already exists',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountLinkOnSuccessResponse,
    })
    async linkOnSuccess(
        @Body() request: BazaNcBankAccountLinkOnSuccessRequest,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<BazaNcBankAccountLinkOnSuccessResponse> {
        const response = await this.service.linkOnSuccess(investorAccountId, request);

        return {
            duplicate: response.duplicate,
            bankAccount: this.mapper.entityToDTO(response.bankAccount),
        };
    }

    @Delete(BazaNcBankAccountEndpointPaths.remove)
    @ApiParam({
        name: 'ulid',
        type: 'string',
        example: ulid(),
        description: 'Bank Account Type',
    })
    @ApiOperation({
        summary: 'remove',
        description:
            'Removes Bank Account by ULID\n' +
            "If it's possible, a first Bank Account in list of Bank Accounts of same type will be selected as default",
    })
    @ApiOkResponse()
    async remove(@Param('ulid') ulid: string, @ReqInvestorAccountId() investorAccountId: number): Promise<BazaNcBankAccountRemoveResponse> {
        const bankAccount = await this.service.getByUlid(investorAccountId, ulid);
        const bankAccountType = bankAccount.type;

        await this.service.remove(investorAccountId, ulid);

        const defaults = await this.service.defaults(investorAccountId);
        const defaultBankAccount = defaults.find((next) => next.type === bankAccountType);

        return {
            defaultBankAccountUlid: defaultBankAccount?.ulid,
        };
    }

    @Post(BazaNcBankAccountEndpointPaths.set)
    @ApiOperation({
        summary: 'set',
        description: 'Set Bank Account as default for given Bank Account Type',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountDto,
        isArray: true,
    })
    async set(
        @Body() request: BazaNcBankAccountSetRequest,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<Array<BazaNcBankAccountDto>> {
        const entities = await this.service.set(investorAccountId, request);

        return this.mapper.entitiesToDTOs(entities);
    }

    @Post(BazaNcBankAccountEndpointPaths.export)
    @ApiOperation({
        summary: 'export',
        description:
            'Exports Bank Account to external services (North Capital or Dwolla). \n' +
            'Only one Bank Account could be exported to NC at once. Additional export calls will mark previously exported ' +
            'bank accounts as not exported to NC. Also, you can export only Cash-In Bank Accounts which will be used ' +
            'for Cash-In operations or for purchasing shares using legacy API. Any number of Bank Accounts of any ' +
            'type could be exported to Dwolla.',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountDto,
    })
    async export(
        @Body() request: BazaNcBankAccountExportRequest,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<BazaNcBankAccountDto> {
        const entity = await this.service.export(investorAccountId, request);

        return this.mapper.entityToDTO(entity);
    }

    @Get(BazaNcBankAccountEndpointPaths.ncAchBankAccount)
    @ApiOperation({
        summary: 'ncAchBankAccount',
        description:
            'Returns Bank Account Details for ACH transactions which are currently set on NC. ' +
            'These details will be immediately used for next purchasing shares session. ' +
            "It's recommended to use this endpoint instead of `default` for displaying actual data on NC side instead of" +
            'possible data on Baza side',
    })
    @ApiOkResponse({
        type: BazaNcBankAccountAchDto,
    })
    async ncAchBankAccount(@ReqInvestorAccountId() investorAccountId: number): Promise<BazaNcBankAccountAchDto> {
        return this.service.ncAchBankAccount(investorAccountId);
    }
}
