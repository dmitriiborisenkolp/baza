import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcBankAccountErrorCodes, bazaNcBankAccountErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcBankAccountUnknownExportServiceException extends BazaAppException {
    constructor(service: string) {
        super(
            BazaNcBankAccountErrorCodes.BazaNcBankAccountUnknownExportService,
            bazaNcBankAccountErrorCodesI18n[BazaNcBankAccountErrorCodes.BazaNcBankAccountUnknownExportService],
            HttpStatus.BAD_REQUEST,
            { service },
        );
    }
}
