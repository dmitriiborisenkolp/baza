import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcBankAccountErrorCodes, bazaNcBankAccountErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcBankAccountNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcBankAccountErrorCodes.BazaNcBankAccountNotFound,
            bazaNcBankAccountErrorCodesI18n[BazaNcBankAccountErrorCodes.BazaNcBankAccountNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
