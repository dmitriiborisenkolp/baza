import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcBankAccountErrorCodes, bazaNcBankAccountErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcBankAccountCannotBeExportedToNcException extends BazaAppException {
    constructor() {
        super(
            BazaNcBankAccountErrorCodes.BazaNcBankAccountCannotBeExportedToNc,
            bazaNcBankAccountErrorCodesI18n[BazaNcBankAccountErrorCodes.BazaNcBankAccountCannotBeExportedToNc],
            HttpStatus.BAD_REQUEST,
        );
    }
}
