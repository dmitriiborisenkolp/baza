import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcBankAccountErrorCodes, bazaNcBankAccountErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcBankAccountNoIdentityException extends BazaAppException {
    constructor() {
        super(
            BazaNcBankAccountErrorCodes.BazaNcBankAccountNoIdentity,
            bazaNcBankAccountErrorCodesI18n[BazaNcBankAccountErrorCodes.BazaNcBankAccountNoIdentity],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
