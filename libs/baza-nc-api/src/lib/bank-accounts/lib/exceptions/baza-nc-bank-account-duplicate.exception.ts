import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcBankAccountErrorCodes, bazaNcBankAccountErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcBankAccountDuplicateException extends BazaAppException {
    constructor() {
        super(
            BazaNcBankAccountErrorCodes.BazaNcBankAccountDuplicate,
            bazaNcBankAccountErrorCodesI18n[BazaNcBankAccountErrorCodes.BazaNcBankAccountDuplicate],
            HttpStatus.CONFLICT,
        );
    }
}
