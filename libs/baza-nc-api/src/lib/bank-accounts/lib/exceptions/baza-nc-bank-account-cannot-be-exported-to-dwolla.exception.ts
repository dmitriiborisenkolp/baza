import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcBankAccountErrorCodes, bazaNcBankAccountErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcBankAccountCannotBeExportedToDwollaException extends BazaAppException {
    constructor() {
        super(
            BazaNcBankAccountErrorCodes.BazaNcBankAccountCannotBeExportedToDwolla,
            bazaNcBankAccountErrorCodesI18n[BazaNcBankAccountErrorCodes.BazaNcBankAccountCannotBeExportedToDwolla],
            HttpStatus.BAD_REQUEST,
        );
    }
}
