import { forwardRef, Module } from '@nestjs/common';
import { BazaApiBundleModule, BazaE2eFixturesApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationApiModule } from '../../account-verification';
import { BazaNcBankAccountsApiModule } from './baza-nc-bank-accounts-api.module';
import { BazaNcBankAccountsCaseAFixture } from './integration-tests/fixtures/baza-nc-bank-accounts-case-a.fixture';
import { BazaNcBankAccountsCaseBFixture } from './integration-tests/fixtures/baza-nc-bank-accounts-case-b.fixture';

const E2E_FIXTURES = [BazaNcBankAccountsCaseAFixture, BazaNcBankAccountsCaseBFixture];

@Module({
    imports: [
        // TODO: Remove it after baza-core e2e modules migration
        forwardRef(() => BazaApiBundleModule),
        forwardRef(() => BazaE2eFixturesApiModule),
        forwardRef(() => BazaNcAccountVerificationApiModule),
        forwardRef(() => BazaNcBankAccountsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcBankAccountsE2eModule {}
