import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';

import { AccountRole, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcApiOfferingFixtures } from '../../../offering';
import { BazaNcAccountVerificationFixtures } from '../../../account-verification';
import {
    BazaNcBankAccountsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcDividendTransactionEntryCmsNodeAccess,
    BazaNcDividendTransactionCmsNodeAccess,
    BazaNcOfferingCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { AccountTypeCheckingSaving, BazaNcBankAccountType, BazaNcDividendPaymentSource, OfferingId } from '@scaliolabs/baza-nc-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/transfer/001-baza-nc-transfer.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDividendTransactionCms = new BazaNcDividendTransactionCmsNodeAccess(http);
    const dataAccess = new BazaNcDividendTransactionEntryCmsNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessBankAccount = new BazaNcBankAccountsNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let dividendTransactionUlid: string;
    let ncOfferingId: OfferingId;
    let investorAccountId: number;

    let USER_EMAIL: string;

    const authenticateUniqueUser = async () => {
        await http.auth({
            email: USER_EMAIL,
            password: 'e2e-user-password',
        });
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
        });

        USER_EMAIL = accounts.items[0].email;
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will fetch all required resources', async () => {
        const offering = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(offering)).toBeFalsy();
        expect(offering.length).toBe(1);

        await authenticateUniqueUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();

        investorAccountId = investorAccount.id;
        ncOfferingId = offering[0].ncOfferingId;
    });

    it('will create a dividend transaction', async () => {
        const response = await dataAccessDividendTransactionCms.create({
            title: 'DT 1',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        dividendTransactionUlid = response.ulid;
    });

    it('will create an entry', async () => {
        const date = '2022-06-18T14:28:17.727Z';

        const response = await dataAccess.create({
            date,
            dividendTransactionUlid,
            investorAccountId,
            offeringId: ncOfferingId,
            source: BazaNcDividendPaymentSource.Dwolla,
            amountCents: 3000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canBeDeleted).toBeTruthy();
        expect(response.canBeUpdated).toBeTruthy();
        expect(response.canBeReprocessed).toBeFalsy();
    });

    it('will process dividends', async () => {
        const sendNotificationResponse = await dataAccessDividendTransactionCms.sendProcessConfirmation({
            ulid: dividendTransactionUlid,
        });

        expect(isBazaErrorResponse(sendNotificationResponse)).toBeFalsy();

        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();
        expect(mailbox.length).toBe(1);

        await dataAccessE2e.flushMailbox();

        const match = mailbox[0].messageBodyText.match(/confirm=(.*)/);

        const token = match[1];

        const confirmResponse = await dataAccessDividendTransactionCms.process({
            token,
            async: false,
            password: 'e2e-admin-password',
        });

        expect(isBazaErrorResponse(confirmResponse)).toBeFalsy();

        await dataAccessE2e.flushMailbox();
    });

    it('will add valid cash out bank accout for the user', async () => {
        await authenticateUniqueUser();

        await asyncExpect(
            async () => {
                const response = await dataAccessBankAccount.add({
                    type: BazaNcBankAccountType.CashIn,
                    accountNumber: '26207729',
                    accountRoutingNumber: '021000021',
                    accountNickName: 'JN',
                    accountName: 'James Smith',
                    accountType: AccountTypeCheckingSaving.Checking,
                    setAsDefault: true,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.name).toBe("James Smith's Checking");
                expect(response.isDefault).toBeTruthy();
                expect(response.accountNumber).toBe('****7729');
                expect(response.accountRoutingNumber).toBe('*****0021');
            },
            null,
            { intervalMillis: 5000 },
        );
    });
});
