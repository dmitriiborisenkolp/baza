import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';

import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../account-verification';
import { BazaDwollaPaymentFixtures } from '@scaliolabs/baza-dwolla-api';
import { BazaNcTransferNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/transfer/002-baza-nc-transfer=list.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcTransferNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaDwollaPaymentFixtures.BazaDwollaPaymentSampleFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch transfer list (Page 1)', async () => {
        const response = await dataAccess.list({
            index: 1,
            size: 3,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.pager.size).toBe(3);
        expect(response.pager.total).toBe(6);

        expect(response.items[0].amount).toBe('257.00');
        expect(response.items[1].amount).toBe('54.00');
        expect(response.items[2].amount).toBe('152.00');
    });

    it('will fetch transfer list (Page 1)', async () => {
        const response = await dataAccess.list({
            index: 2,
            size: 3,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.pager.size).toBe(3);
        expect(response.pager.total).toBe(6);

        expect(response.items[0].amount).toBe('351.00');
        expect(response.items[1].amount).toBe('158.00');
        expect(response.items[2].amount).toBe('55.00');
    });
});
