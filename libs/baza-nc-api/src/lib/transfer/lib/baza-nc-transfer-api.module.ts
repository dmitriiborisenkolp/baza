import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcTransferController } from './controllers/baza-nc-transfer.controller';
import { BazaNcTransferService } from './services/baza-nc-transfer-service';
import { BazaDwollaApiModule } from '@scaliolabs/baza-dwolla-api';

@Module({
    imports: [BazaCrudApiModule, BazaDwollaApiModule],
    controllers: [BazaNcTransferController],
    providers: [BazaNcTransferService],
    exports: [BazaNcTransferService],
})
export class BazaNcTransferApiModule {}
