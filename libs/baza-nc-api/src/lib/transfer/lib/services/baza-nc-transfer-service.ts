import { Injectable } from '@nestjs/common';
import {
    BazaNcBankAccountExport,
    BazaNcBankAccountType,
    BazaNcTransferListRequest,
    BazaNcTransferListResponse,
    BazaNcTransferReprocessRequest,
    BazaNcTransferRequest,
    convertToCents,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountNoDefaultException, BazaNcBankAccountsService } from '../../../bank-accounts';
import { BazaDwollaPaymentEntity, BazaDwollaPaymentService } from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';

@Injectable()
export class BazaNcTransferService {
    constructor(
        private readonly bankAccountService: BazaNcBankAccountsService,
        private readonly dwollaPaymentService: BazaDwollaPaymentService,
    ) {}

    /**
     * Returns list of Transfers
     * @param investorAccount
     * @param request
     */
    async list(investorAccount: BazaNcInvestorAccountEntity, request: BazaNcTransferListRequest): Promise<BazaNcTransferListResponse> {
        return this.dwollaPaymentService.list(investorAccount.userId, {
            ...request,
            types: [BazaDwollaPaymentType.CashIn],
        });
    }

    /**
     * Transfers Funds to Dwolla Customer Balance
     * @param investorAccount
     * @param request
     */
    async transfer(investorAccount: BazaNcInvestorAccountEntity, request: BazaNcTransferRequest): Promise<BazaDwollaPaymentEntity> {
        const account = investorAccount.user;
        const cashInFundingSourceId = await this.touchTransferWallet(investorAccount);

        return this.dwollaPaymentService.transfer({
            accountId: account.id,
            amountCents: convertToCents(parseFloat(request.amount)),
            dwollaCustomerId: investorAccount.dwollaCustomerId,
            payload: {
                type: BazaDwollaPaymentType.CashIn,
                cashInFundingSourceId,
                ...request,
            },
        });
    }

    /**
     * Creates or returns Dwolla Funding Source Id for Transfer operations
     * @param investorAccount
     * @returns dwollaFundingSourceId
     */
    async touchTransferWallet(investorAccount: BazaNcInvestorAccountEntity) {
        const bankAccount = await this.bankAccountService.default(investorAccount.id, BazaNcBankAccountType.CashIn);

        if (!bankAccount) {
            throw new BazaNcBankAccountNoDefaultException();
        }

        let { dwollaFundingSourceId } = bankAccount;

        if (!dwollaFundingSourceId) {
            dwollaFundingSourceId = (
                await this.bankAccountService.export(investorAccount.id, {
                    services: [BazaNcBankAccountExport.Dwolla],
                    ulid: bankAccount.ulid,
                    setAsDefault: true,
                })
            ).dwollaFundingSourceId;
        }

        return dwollaFundingSourceId;
    }

    /**
     * Reprocess Transfer Request
     * @param investorAccount
     * @param request
     */
    async reprocess(
        investorAccount: BazaNcInvestorAccountEntity,
        request: BazaNcTransferReprocessRequest,
    ): Promise<BazaDwollaPaymentEntity> {
        const account = investorAccount.user;
        const cashInfundingSourceId = await this.touchTransferWallet(investorAccount);

        return this.dwollaPaymentService.reprocess({
            accountId: account.id,
            ulid: request.ulid,
            dwollaCustomerId: investorAccount.dwollaCustomerId,
            payload: {
                cashInfundingSourceId,
            },
        });
    }
}
