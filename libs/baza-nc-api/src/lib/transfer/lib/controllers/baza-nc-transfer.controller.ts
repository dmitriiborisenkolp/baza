import { Body, Controller, UseGuards, Post, Get, Query } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import {
    BazaNcTransferEndpoint,
    BazaNcTransferEndpointPaths,
    BazaNcTransferListRequest,
    BazaNcTransferListResponse,
    BazaNcTransferReprocessRequest,
    BazaNcTransferRequest,
    BazaNorthCapitalOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { ApiOperation } from '@nestjs/swagger';
import { BazaDwollaPaymentDto } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcInvestorAccountService, InvestorAccountGuard, ReqInvestorAccount } from '../../../investor-account';
import { BazaDwollaPaymentMapper } from '@scaliolabs/baza-dwolla-api';
import { BazaNcTransferService } from '../services/baza-nc-transfer-service';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalTransfer)
@UseGuards(InvestorAccountGuard)
export class BazaNcTransferController implements BazaNcTransferEndpoint {
    constructor(
        private readonly service: BazaNcTransferService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
        private readonly dwollaPaymentMapper: BazaDwollaPaymentMapper,
    ) {}

    @Get(BazaNcTransferEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of transfers',
    })
    @ApiOkResponse({
        type: BazaNcTransferListResponse,
    })
    async list(
        @Query() request: BazaNcTransferListRequest,
        @ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<BazaNcTransferListResponse> {
        return this.service.list(investorAccount, request);
    }

    @Post(BazaNcTransferEndpointPaths.transfer)
    @ApiOperation({
        summary: 'transfer',
        description: 'Transfers from bank account into Dwolla Customer Balance',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentDto,
    })
    async transfer(
        @Body() request: BazaNcTransferRequest,
        @ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<BazaDwollaPaymentDto> {
        const entity = await this.service.transfer(investorAccount, request);

        return this.dwollaPaymentMapper.entityToDTO(entity);
    }

    @Post(BazaNcTransferEndpointPaths.reprocess)
    @ApiOperation({
        summary: 'Reprocess Failed Transfers',
        description: 'Will reproces the failed Transfer(cash-in) request',
    })
    @ApiOkResponse({
        type: BazaDwollaPaymentDto,
    })
    async reprocess(
        @Body() request: BazaNcTransferReprocessRequest,
        @ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<BazaDwollaPaymentDto> {
        const entity = await this.service.reprocess(investorAccount, request);

        return this.dwollaPaymentMapper.entityToDTO(entity);
    }
}
