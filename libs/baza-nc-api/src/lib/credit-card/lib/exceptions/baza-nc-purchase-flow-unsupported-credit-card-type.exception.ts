import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { BazaCreditCardType } from '@scaliolabs/baza-core-shared';

export class BazaNcPurchaseFlowUnsupportedCreditCardTypeException extends BazaNcPurchaseFlowException {
    constructor(args: { creditCard: BazaCreditCardType }) {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.CreditCardIsNotSupported,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.CreditCardIsNotSupported],
            },
            HttpStatus.BAD_REQUEST,
            args,
        );
    }
}
