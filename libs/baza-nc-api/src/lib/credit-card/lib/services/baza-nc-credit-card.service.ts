import { Injectable } from '@nestjs/common';
import {
    BazaNcCreditCardDto,
    BazaNcInvestorCreditCardLinkedEvent,
    BazaNcInvestorCreditCardUnlinkedEvent,
    BazaNcPurchaseFlowSetCreditCardRequest,
    bazaNcPurchaseFlowCreditCardTypes,
} from '@scaliolabs/baza-nc-shared';
import { BazaCreditCardService } from '@scaliolabs/baza-core-api';
import { bazaCreditCardDetect, BazaCreditCardExpiredResponse, BazaCreditCardType } from '@scaliolabs/baza-core-shared';
import { NorthCapitalErrorCodes } from '@scaliolabs/baza-nc-shared';
import { throwError } from 'rxjs';
import { isNorthCapitalException, NorthCapitalCreditCardTransactionApiNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountEntity, BazaNcInvestorAccountRepository } from '../../../typeorm';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcPurchaseFlowCreditCardExpiredException } from '../exceptions/baza-nc-purchase-flow-credit-card-expired.exception';
import { BazaNcPurchaseFlowCreditCardOutOfRangeException } from '../exceptions/baza-nc-purchase-flow-credit-card-out-of-range.exception';
import { BazaNcPurchaseFlowInvalidCardDetailsException } from '../exceptions/baza-nc-purchase-flow-invalid-card-details.exception';
import { BazaNcPurchaseFlowInvalidCreditCardNumberException } from '../exceptions/baza-nc-purchase-flow-invalid-credit-card-number.exception';
import { BazaNcPurchaseFlowUnsupportedCreditCardTypeException } from '../exceptions/baza-nc-purchase-flow-unsupported-credit-card-type.exception';

/**
 * Baza NC Credit Card Service
 * Allows to get or set Credit Card for current Investor Account
 * Investor Account can have only 1 Credit Card attached per Investor Account
 */
@Injectable()
export class BazaNcCreditCardService {
    constructor(
        private readonly eventBus: EventBus,
        private readonly ncCreditCardApi: NorthCapitalCreditCardTransactionApiNestjsService,
        private readonly bazaCreditCard: BazaCreditCardService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    /**
     * Validates Credit Card Number
     * @param creditCardNumber
     */
    async validateCreditCardNumber(creditCardNumber: string): Promise<boolean> {
        const response = this.bazaCreditCard.validate({
            creditCardNumber,
            accept: bazaNcPurchaseFlowCreditCardTypes().map((d) => d.baza),
        });

        return response.isValid;
    }

    /**
     * Set or replace Credit Card for Investor Account
     * @param clientIp
     * @param investorAccount
     * @param request
     */
    async setCreditCard(
        clientIp: string,
        investorAccount: BazaNcInvestorAccountEntity,
        request: BazaNcPurchaseFlowSetCreditCardRequest,
    ): Promise<BazaNcCreditCardDto> {
        if (request.creditCardExpireYear > 2000) {
            request.creditCardExpireYear = request.creditCardExpireYear - 2000;
        }

        const expireStatus = this.bazaCreditCard.isExpired({
            creditCardExpireYear: request.creditCardExpireYear,
            creditCardExpireMonth: request.creditCardExpireMonth,
        });

        if (expireStatus.result === BazaCreditCardExpiredResponse.Expired) {
            throw new BazaNcPurchaseFlowCreditCardExpiredException();
        } else if (expireStatus.result !== BazaCreditCardExpiredResponse.Valid) {
            throw new BazaNcPurchaseFlowCreditCardOutOfRangeException();
        }

        const response = this.bazaCreditCard.validate({
            creditCardNumber: request.creditCardNumber,
            accept: bazaNcPurchaseFlowCreditCardTypes().map((d) => d.baza),
        });

        if (!response.isValid) {
            if (response.creditCardType) {
                throw new BazaNcPurchaseFlowUnsupportedCreditCardTypeException({ creditCard: response.creditCardType });
            } else {
                throw new BazaNcPurchaseFlowInvalidCreditCardNumberException();
            }
        }

        const creditCardType = bazaNcPurchaseFlowCreditCardTypes().find((d) => d.baza === response.creditCardType);

        const ncRequest = {
            accountId: investorAccount.northCapitalAccountId,
            cardType: creditCardType.nc,
            creditCardName: request.creditCardholderName,
            creditCardNumber: request.creditCardNumber.toString().split(' ').join(''),
            cvvNumber: request.creditCardCvv,
            expirationDate: `${(request.creditCardExpireMonth as number).toString().padStart(2, '0')}${request.creditCardExpireYear
                .toString()
                .padStart(2, '0')}`,
        };

        const ncResponse = await (async () => {
            try {
                const hasCreditCard = await this.hasCreditCard(investorAccount);

                return hasCreditCard
                    ? await this.ncCreditCardApi.updateCreditCard({
                          ...ncRequest,
                          updatedIpAddress: clientIp,
                      })
                    : await this.ncCreditCardApi.addCreditCard({
                          ...ncRequest,
                          createdIpAddress: clientIp,
                      });
            } catch (err) {
                if (isNorthCapitalException(err, NorthCapitalErrorCodes.DataParameterMissing)) {
                    return false;
                } else {
                    return throwError(err);
                }
            }
        })();

        if (ncResponse === false) {
            throw new BazaNcPurchaseFlowInvalidCardDetailsException();
        }

        investorAccount.isCreditCardLinked = true;

        await this.investorAccountRepository.save(investorAccount);

        this.eventBus.publish(
            new BazaNcInvestorCreditCardLinkedEvent({
                bazaAccountId: investorAccount.user.id,
                ncAccountId: investorAccount.northCapitalAccountId,
                bazaNcInvestorAccountId: investorAccount.id,
            }),
        );

        return {
            creditCardType: creditCardType.baza,
            creditCardExpireYear: request.creditCardExpireYear,
            creditCardExpireMonth: request.creditCardExpireMonth,
            creditCardholderName: ncRequest.creditCardName.toUpperCase(),
            creditCardNumber: ncRequest.creditCardNumber,
        };
    }

    /**
     * Returns true if current Investor Account has Credit Card
     * @param investorAccount
     */
    async hasCreditCard(investorAccount: BazaNcInvestorAccountEntity): Promise<boolean> {
        if (!investorAccount.northCapitalAccountId) {
            return false;
        }

        const ncCreditCard = await (async () => {
            try {
                return await this.ncCreditCardApi.getCreditCard({
                    accountId: investorAccount.northCapitalAccountId,
                });
            } catch (err) {
                if (isNorthCapitalException(err, NorthCapitalErrorCodes.AccountCreditCardDetailsDoesnTExistActive)) {
                    return false;
                } else {
                    throw err;
                }
            }
        })();

        return !!ncCreditCard;
    }

    /**
     * Returns Credit Card for current Investor Account
     * @param investorAccount
     */
    async getCreditCard(investorAccount: BazaNcInvestorAccountEntity): Promise<BazaNcCreditCardDto> {
        if (await this.hasCreditCard(investorAccount)) {
            const ncCreditCard = await this.ncCreditCardApi.getCreditCard({
                accountId: investorAccount.northCapitalAccountId,
            });

            const expire = ncCreditCard.creditcardDetails.expirationDate;
            const detected = bazaCreditCardDetect(ncCreditCard.creditcardDetails.creditCardNumber);

            return {
                creditCardType: detected ? detected.type : BazaCreditCardType.Visa,
                creditCardNumber: ncCreditCard.creditcardDetails.creditCardNumber,
                creditCardholderName: ncCreditCard.creditcardDetails.creditCardName.toUpperCase(),
                creditCardExpireMonth: parseInt(expire.slice(0, 2), 10),
                creditCardExpireYear: parseInt(expire.slice(2, 4), 10),
            };
        } else {
            return undefined;
        }
    }

    /**
     * Deletes Credit Card Details of Investor Account from NC API
     * @param clientIp
     * @param investorAccount
     */
    async deleteCreditCard(clientIp: string, investorAccount: BazaNcInvestorAccountEntity): Promise<boolean> {
        const existing = await this.hasCreditCard(investorAccount);

        if (existing) {
            await this.ncCreditCardApi.deleteCreditCard({
                accountId: investorAccount.northCapitalAccountId,
                updatedIpAddress: clientIp,
            });

            investorAccount.isCreditCardLinked = false;

            await this.investorAccountRepository.save(investorAccount);

            this.eventBus.publish(
                new BazaNcInvestorCreditCardUnlinkedEvent({
                    bazaAccountId: investorAccount.user.id,
                    ncAccountId: investorAccount.northCapitalAccountId,
                    bazaNcInvestorAccountId: investorAccount.id,
                }),
            );

            return true;
        } else {
            return false;
        }
    }

    /**
     * Updates `isCreditCardLinked` flag of Investor Account
     * @param investorAccount
     */
    async updateCreditCardStatus(investorAccountId: number): Promise<void> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);

        investorAccount.isCreditCardLinked = await this.hasCreditCard(investorAccount);

        await this.investorAccountRepository.save(investorAccount);
    }
}
