import { Module } from '@nestjs/common';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { BazaNcCreditCardService } from './services/baza-nc-credit-card.service';
import { BazaCreditCardApiModule } from '@scaliolabs/baza-core-api';

@Module({
    imports: [
        BazaCreditCardApiModule,
        BazaNcTypeormApiModule,
    ],
    providers: [
        BazaNcCreditCardService,
    ],
    exports: [
        BazaNcCreditCardService,
    ],
})
export class BazaNcCreditCardApiModule {}
