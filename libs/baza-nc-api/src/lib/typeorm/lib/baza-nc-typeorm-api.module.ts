import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BazaNcOfferingRepository } from './repositories/baza-nc-offering.repository';
import { BazaNcTransactionRepository } from './repositories/baza-nc-transaction.repository';
import { BazaNcTradesSessionRepository } from './repositories/baza-nc-trades-session.repository';
import { BazaNcInvestorAccountRepository } from './repositories/baza-nc-investor-account.repository';
import { BazaNcTaxDocumentRepository } from './repositories/baza-nc-tax-document.repository';
import { BazaNcDividendRepository } from './repositories/baza-nc-dividend.repository';
import { BazaNcDividendTransactionRepository } from './repositories/baza-nc-dividend-transaction.repository';
import { BazaNcDividendConfirmationRepository } from './repositories/baza-nc-dividend-confirmation.repository';
import { BazaNcDividendTransactionEntryRepository } from './repositories/baza-nc-dividend-transaction-entry.repository';
import { BazaNcBankAccountRepository } from './repositories/baza-nc-bank-account.repository';
import { BazaNcFailedRequestLogRepository } from './repositories/baza-nc-failed-request-log.repository';
import { BazaNcReportRepository } from './repositories/baza-nc-report.repository';

const REPOSITORIES = [
    BazaNcOfferingRepository,
    BazaNcTransactionRepository,
    BazaNcTradesSessionRepository,
    BazaNcInvestorAccountRepository,
    BazaNcTaxDocumentRepository,
    BazaNcDividendRepository,
    BazaNcDividendTransactionRepository,
    BazaNcDividendConfirmationRepository,
    BazaNcDividendTransactionEntryRepository,
    BazaNcBankAccountRepository,
    BazaNcFailedRequestLogRepository,
    BazaNcReportRepository,
];

@Module({
    imports: [TypeOrmModule],
    providers: [...REPOSITORIES],
    exports: [...REPOSITORIES],
})
export class BazaNcTypeormApiModule {}
