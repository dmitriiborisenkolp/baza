import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcReportErrorCodes, bazaNcReportErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcReportNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcReportErrorCodes.BazaNcReportNotFound,
            bazaNcReportErrorCodesI18n[BazaNcReportErrorCodes.BazaNcReportNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
