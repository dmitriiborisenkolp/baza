import { BazaAppException } from '@scaliolabs/baza-core-api';
import { TransactionsErrorCodes } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcTransactionNotFoundException extends BazaAppException {
    constructor(args: { id: number | string }) {
        super(TransactionsErrorCodes.BazaNcTransactionNotFound, 'Transaction with ID {{ id }} not found', HttpStatus.NOT_FOUND, args);
    }
}
