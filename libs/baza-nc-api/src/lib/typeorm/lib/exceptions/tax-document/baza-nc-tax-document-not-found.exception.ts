import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaNcTaxDocumentErrorCodes, bazaNcTaxDocumentsErrorCodesI18n } from '@scaliolabs/baza-nc-shared';

export class BazaNcTaxDocumentNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcTaxDocumentErrorCodes.BazaNcTaxDocumentsNotFound,
            bazaNcTaxDocumentsErrorCodesI18n[BazaNcTaxDocumentErrorCodes.BazaNcTaxDocumentsNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
