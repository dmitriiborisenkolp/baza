import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcOfferingsErrorCodes } from '@scaliolabs/baza-nc-shared';

const ERR_MESSAGE = 'Offering "{{ offeringId }}" not found';

export class BazaNcOfferingNotFoundException extends BazaAppException {
    constructor(args: { offeringId: string | number }) {
        super(BazaNcOfferingsErrorCodes.NcOfferingNotFound, ERR_MESSAGE, HttpStatus.NOT_FOUND, args);
    }
}
