import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from './baza-nc-purchase-flow.exception';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';

export class BazaNcPurchaseFlowTradeSessionNotFoundException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.TradeSessionNotFound,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.TradeSessionNotFound],
            },
            HttpStatus.NOT_FOUND,
        );
    }
}
