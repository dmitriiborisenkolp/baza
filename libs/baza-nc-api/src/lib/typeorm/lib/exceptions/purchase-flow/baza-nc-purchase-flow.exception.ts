import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { PurchaseFlowErrorDto } from '@scaliolabs/baza-nc-shared';

export class BazaNcPurchaseFlowException extends BazaAppException {
    constructor(public readonly dto: PurchaseFlowErrorDto, statusCode: HttpStatus, args?: any) {
        super(dto.errorCode, dto.message as any, statusCode, args, {
            message: dto.message,
            mayPurchase: dto.mayPurchase,
        });
    }
}
