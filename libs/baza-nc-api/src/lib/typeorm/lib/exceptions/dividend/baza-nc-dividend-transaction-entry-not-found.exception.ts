import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendTransactionEntryNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendTransactionEntryNotFound,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendTransactionEntryNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
