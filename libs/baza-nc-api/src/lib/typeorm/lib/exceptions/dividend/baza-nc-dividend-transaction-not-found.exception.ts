import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendTransactionNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendErrorCodes.BazaNcDividendTransactionNotFound,
            bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendTransactionNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
