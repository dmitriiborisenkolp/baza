import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDividendErrorCodes, bazaNcDividendErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDividendNotFoundException extends BazaAppException {
    constructor() {
        super(BazaNcDividendErrorCodes.BazaNcDividendNotFound, bazaNcDividendErrorCodesI18n[BazaNcDividendErrorCodes.BazaNcDividendNotFound], HttpStatus.NOT_FOUND);
    }
}
