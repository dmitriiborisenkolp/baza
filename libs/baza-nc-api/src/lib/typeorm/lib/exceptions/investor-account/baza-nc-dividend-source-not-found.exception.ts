import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { bazaDividendSourceErrorCodesEnI18n, BazaNcDividendSourceErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcDividendSourceNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcDividendSourceErrorCodes.DividendSourceNotFound,
            bazaDividendSourceErrorCodesEnI18n[BazaNcDividendSourceErrorCodes.DividendSourceNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
