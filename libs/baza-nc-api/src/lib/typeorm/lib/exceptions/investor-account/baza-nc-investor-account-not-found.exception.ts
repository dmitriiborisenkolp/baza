import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcInvestorAccountErrorCodes, bazaNcInvestorAccountErrorCodesEnI18n } from '@scaliolabs/baza-nc-shared';

export class BazaNcInvestorAccountNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcInvestorAccountErrorCodes.NcInvestorAccountNotFound,
            bazaNcInvestorAccountErrorCodesEnI18n[BazaNcInvestorAccountErrorCodes.NcInvestorAccountNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
