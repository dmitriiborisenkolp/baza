import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import {
    BAZA_NC_DIVIDEND_CONFIRMATION_RELATIONS,
    BazaNcDividendConfirmationEntity,
} from '../entities/baza-nc-dividend-confirmation.entity';
import { BazaNcDividendConfirmationNotFoundException } from '../exceptions/dividend/baza-nc-dividend-confirmation-not-found.exception';
import { BazaNcDividendTransactionEntity } from '../entities/baza-nc-dividend-transaction.entity';

@Injectable()
export class BazaNcDividendConfirmationRepository {
    constructor(private readonly connection: Connection) {}

    get repository(): Repository<BazaNcDividendConfirmationEntity> {
        return this.connection.getRepository(BazaNcDividendConfirmationEntity);
    }

    async save(entities: Array<BazaNcDividendConfirmationEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaNcDividendConfirmationEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findByToken(token: string): Promise<BazaNcDividendConfirmationEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    token,
                },
            ],
            relations: BAZA_NC_DIVIDEND_CONFIRMATION_RELATIONS,
        });
    }

    async getByToken(token: string): Promise<BazaNcDividendConfirmationEntity> {
        const entity = await this.findByToken(token);

        if (!entity) {
            throw new BazaNcDividendConfirmationNotFoundException();
        }

        return entity;
    }

    async findByDividendTransaction(transaction: BazaNcDividendTransactionEntity): Promise<Array<BazaNcDividendConfirmationEntity>> {
        return this.repository.find({
            where: [
                {
                    transaction,
                },
            ],
            relations: BAZA_NC_DIVIDEND_CONFIRMATION_RELATIONS,
        });
    }
}
