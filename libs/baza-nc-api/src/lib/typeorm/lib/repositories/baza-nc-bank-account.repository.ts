import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BAZA_NC_BANK_ACCOUNT_ENTITY_RELATIONS, BazaNcBankAccountEntity } from '../entities/baza-nc-bank-account.entity';
import { BazaNcBankAccountNotFoundException } from '../exceptions/bank-account/baza-nc-bank-account-not-found.exception';
import { BazaNcInvestorAccountEntity } from '../entities/baza-nc-investor-account.entity';
import { BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';
import { BazaNcBankAccountNoDefaultException } from '../exceptions/bank-account/baza-nc-bank-account-no-default.exception';

/**
 * Repository service for BazaNcBankAccountEntity
 */
@Injectable()
export class BazaNcBankAccountRepository {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeORM repository for BazaNcBankAccountEntity
     */
    get repository(): Repository<BazaNcBankAccountEntity> {
        return this.connection.getRepository(BazaNcBankAccountEntity);
    }

    /**
     * Saves BazaNcBankAccountEntity entities to DB
     * @param entities
     */
    async save(entities: Array<BazaNcBankAccountEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    /**
     * Removes BazaNcBankAccountEntity entities to DB
     * @param entities
     */
    async remove(entities: Array<BazaNcBankAccountEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    /**
     * Returns BazaNcBankAccountEntity entity by ULID
     * Method will returns `undefined` in case if there is no BazaNcBankAccountEntity entity with given ULID
     * @param ulid
     */
    async findByUlid(ulid: string): Promise<BazaNcBankAccountEntity | undefined> {
        return this.repository.findOne({
            where: [{ ulid }],
            relations: BAZA_NC_BANK_ACCOUNT_ENTITY_RELATIONS,
        });
    }

    /**
     * Returns BazaNcBankAccountEntity entity by ULID
     * Method will throw an aerror in case if there is no BazaNcBankAccountEntity entity with given ULID
     * @param ulid
     */
    async getByUlid(ulid: string): Promise<BazaNcBankAccountEntity> {
        const entity = await this.findByUlid(ulid);

        if (!entity) {
            throw new BazaNcBankAccountNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcBankAccountEntity entities of Investor Account
     * @param investorAccount
     */
    async findByInvestorAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<Array<BazaNcBankAccountEntity>> {
        return this.repository.find({
            where: [{ investorAccount }],
            order: { ulid: 'ASC' },
            relations: BAZA_NC_BANK_ACCOUNT_ENTITY_RELATIONS,
        });
    }

    /**
     * Returns BazaNcBankAccountEntity of Investor Account (Default bank accounts only)
     * @param investorAccount
     */
    async findDefaultsByInvestorAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<Array<BazaNcBankAccountEntity>> {
        return this.repository.find({
            where: [{ investorAccount, isDefault: true }],
            order: { ulid: 'ASC' },
            relations: BAZA_NC_BANK_ACCOUNT_ENTITY_RELATIONS,
        });
    }

    /**
     * Returns BazaNcBankAccountEntity entities of Investor Account and Type (Default bank accounts only)
     * @param investorAccount
     * @param type
     */
    async findDefaultsByInvestorAccountAndType(
        investorAccount: BazaNcInvestorAccountEntity,
        type: BazaNcBankAccountType,
    ): Promise<BazaNcBankAccountEntity> {
        const response = await this.repository.find({
            where: [{ investorAccount, type, isDefault: true }],
            order: { ulid: 'ASC' },
            relations: BAZA_NC_BANK_ACCOUNT_ENTITY_RELATIONS,
        });

        if (!response.length) {
            throw new BazaNcBankAccountNoDefaultException();
        }

        return response[0];
    }

    /**
     * Removes all bank accounts with same type except given one and will return list of removed bank accounts
     * @param bankAccount
     */
    async removeAllBankAccountsExcepts(bankAccount: BazaNcBankAccountEntity): Promise<BazaNcBankAccountEntity[]> {
        const bankAccounts = (await this.findByInvestorAccount(bankAccount.investorAccount)).filter(
            (next) => next.type === bankAccount.type && next.ulid !== bankAccount.ulid,
        );

        await this.remove(bankAccounts);

        return bankAccounts;
    }
}
