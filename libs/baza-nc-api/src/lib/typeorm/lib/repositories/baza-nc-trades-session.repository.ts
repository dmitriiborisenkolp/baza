import { Injectable, Logger } from '@nestjs/common';
import { Connection, LessThan, Repository } from 'typeorm';
import { BazaNcTradesSessionEntity } from '../entities/baza-nc-trades-session.entity';
import { NorthCapitalAccountId, OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowTradeSessionNotFoundException } from '../exceptions/purchase-flow/baza-nc-purchase-flow-trade-session-not-found.exception';

@Injectable()
export class BazaNcTradesSessionRepository {
    constructor(private readonly connection: Connection) {}

    get repository(): Repository<BazaNcTradesSessionEntity> {
        return this.connection.getRepository(BazaNcTradesSessionEntity);
    }

    async save(entity: BazaNcTradesSessionEntity): Promise<void> {
        const newEntity: BazaNcTradesSessionEntity = {
            ...entity,
            totalCents: Math.floor(entity.totalCents),
            transactionFeesCents: Math.floor(entity.transactionFeesCents),
        };

        await this.repository.save(newEntity);
    }

    async remove(entity: BazaNcTradesSessionEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async findActiveTradesByOffering(offeringId: OfferingId): Promise<Array<BazaNcTradesSessionEntity>> {
        return this.repository.find({
            where: [
                {
                    offeringId,
                    isOutdated: false,
                },
            ],
        });
    }

    async findActiveTradeByTradeId(tradeId: TradeId): Promise<BazaNcTradesSessionEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    tradeId,
                    isOutdated: false,
                },
            ],
        });
    }

    async findTradeByTradeId(tradeId: TradeId): Promise<BazaNcTradesSessionEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    tradeId,
                },
            ],
        });
    }

    async getActiveTradeByTradeId(tradeId: TradeId): Promise<BazaNcTradesSessionEntity> {
        const entity = await this.findActiveTradeByTradeId(tradeId);

        if (!entity) {
            throw new BazaNcPurchaseFlowTradeSessionNotFoundException();
        }

        return entity;
    }

    async getActiveTradeSessionByTradeId(request: { tradeId: TradeId }): Promise<BazaNcTradesSessionEntity> {
        const entity = await this.repository.findOne({
            where: [
                {
                    tradeId: request.tradeId,
                    isOutdated: false,
                },
            ],
            order: {
                updatedAt: 'DESC',
            },
        });

        if (!entity) {
            throw new BazaNcPurchaseFlowTradeSessionNotFoundException();
        }

        return entity;
    }

    async getActiveTradeByAccountAndOfferingId(request: {
        accountId: NorthCapitalAccountId;
        offeringId: OfferingId;
    }): Promise<BazaNcTradesSessionEntity> {
        const entity = await this.repository.findOne({
            where: [
                {
                    accountId: request.accountId,
                    offeringId: request.offeringId,
                    isOutdated: false,
                },
            ],
            order: {
                updatedAt: 'DESC',
            },
        });

        if (!entity) {
            throw new BazaNcPurchaseFlowTradeSessionNotFoundException();
        }

        return entity;
    }

    async hasActiveTradeByAccountAndOfferingId(request: { accountId: NorthCapitalAccountId; offeringId: OfferingId }): Promise<boolean> {
        const entity = await this.repository.findOne({
            where: [
                {
                    accountId: request.accountId,
                    offeringId: request.offeringId,
                    isOutdated: false,
                },
            ],
        });

        return !!entity;
    }

    async findActiveTradeByAccountAndOfferingId(request: {
        accountId: NorthCapitalAccountId;
        offeringId: OfferingId;
        numberOfShares: number;
        amount: number;
    }): Promise<BazaNcTradesSessionEntity> {
        return this.repository.findOne({
            where: [
                {
                    accountId: request.accountId,
                    offeringId: request.offeringId,
                    amountCents: request.amount,
                    numberOfShares: request.numberOfShares,
                    isOutdated: false,
                },
            ],
        });
    }

    async markTradesAsOutdatedByAccountAndOfferingId(request: { accountId: NorthCapitalAccountId; offeringId: OfferingId }): Promise<void> {
        const entities = await this.repository.find({
            where: [
                {
                    accountId: request.accountId,
                    offeringId: request.offeringId,
                    isOutdated: false,
                },
            ],
        });

        for (const entity of entities) {
            Logger.log(`[NcTradesSessionRepository] [markTradesAsOutdatedByAccountAndOfferingId] [${entity.tradeId}]`);

            entity.isOutdated = true;
            entity.shouldBeDeletedFromNC = true;

            await this.repository.save(entity);
        }
    }

    async destroyTradeSession(tradeId: TradeId): Promise<void> {
        const entity = await this.findTradeByTradeId(tradeId);

        if (entity) {
            await this.repository.remove(entity);
        }
    }

    async fetchAllOutdatedActiveTradeSessions(tradesTTLSec: number): Promise<Array<BazaNcTradesSessionEntity>> {
        const lessThan = new Date();

        lessThan.setTime(lessThan.getTime() - tradesTTLSec * 1000);

        return this.repository.find({
            where: [
                {
                    updatedAt: LessThan(lessThan),
                    isOutdated: false,
                },
            ],
        });
    }

    async fetchAllMarkToNCDeleteTradeSessions(): Promise<Array<BazaNcTradesSessionEntity>> {
        return this.repository.find({
            where: [
                {
                    shouldBeDeletedFromNC: true,
                    tradeSessionWasDeletedFromNC: false,
                },
            ],
        });
    }
}
