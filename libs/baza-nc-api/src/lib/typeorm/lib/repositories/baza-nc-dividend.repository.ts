import { Injectable } from '@nestjs/common';
import { Connection, In, Repository } from 'typeorm';
import { BAZA_NC_DIVIDEND_RELATIONS, BazaNcDividendEntity } from '../entities/baza-nc-dividend.entity';
import { BazaNcDividendNotFoundException } from '../exceptions/dividend/baza-nc-dividend-not-found.exception';
import { BazaNcInvestorAccountEntity } from '../entities/baza-nc-investor-account.entity';
import { BazaNcOfferingEntity } from '../entities/baza-nc-offering.entity';
import { DwollaTransferId } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcDividendPaymentStatus } from '@scaliolabs/baza-nc-shared';

@Injectable()
export class BazaNcDividendRepository {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeORM Repository for BazaNcDividendEntity
     */
    get repository(): Repository<BazaNcDividendEntity> {
        return this.connection.getRepository(BazaNcDividendEntity);
    }

    /**
     * Saves BazaNcDividendEntity entities to DB
     * @param entities
     */
    async save(entities: Array<BazaNcDividendEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    /**
     * Removes BazaNcDividendEntity entities from DB
     * @param entities
     */
    async remove(entities: Array<BazaNcDividendEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    /**
     * Returns BazaNcDividendEntity by ULID
     * @param ulid
     */
    async findByUlid(ulid: string): Promise<BazaNcDividendEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    ulid,
                },
            ],
            relations: BAZA_NC_DIVIDEND_RELATIONS,
        });
    }

    /**
     * Returns BazaNcDividendEntity by ULID. Will throw an BazaNcDividendNotFoundException error if not found
     * @param ulid
     */
    async getByUlid(ulid: string): Promise<BazaNcDividendEntity> {
        const entity = await this.findByUlid(ulid);

        if (!entity) {
            throw new BazaNcDividendNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcDividendEntity entity by dwollaTransferId
     * @param dwollaTransferId
     */
    async findByDwollaTransferId(dwollaTransferId: DwollaTransferId): Promise<BazaNcDividendEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    dwollaTransferId,
                },
            ],
            relations: BAZA_NC_DIVIDEND_RELATIONS,
        });
    }

    /**
     * Returns BazaNcDividendEntity entities by Dwolla Transfer Ids
     * @param dwollaTransferIds
     */
    async findByDwollaTransferIds(dwollaTransferIds: Array<DwollaTransferId>): Promise<Array<BazaNcDividendEntity>> {
        return this.repository.find({
            where: [
                {
                    dwollaTransferId: In(dwollaTransferIds),
                },
            ],
            relations: BAZA_NC_DIVIDEND_RELATIONS,
        });
    }

    /**
     * Updates Status for BazaNcDividendEntity entities
     * @param dwollaTransferId
     * @param newStatus
     */
    async setStatusByDwollaTransferId(dwollaTransferId: DwollaTransferId, newStatus: BazaNcDividendPaymentStatus) {
        await this.repository.update(
            {
                dwollaTransferId,
            },
            {
                status: newStatus,
            },
        );
    }

    /**
     * Removes BazaNcDividendEntity entity by Dwolla Transfer Id
     * @param dwollaTransferId
     */
    async removeByDwollaTransferId(dwollaTransferId: DwollaTransferId): Promise<void> {
        await this.repository.delete({
            dwollaTransferId,
        });
    }

    /**
     * Returns total dividends amount of Dividend Transaction
     * @param request
     */
    async totalAmount(request: { investorAccount?: BazaNcInvestorAccountEntity; offering?: BazaNcOfferingEntity }): Promise<number> {
        const qb = this.repository.manager
            .createQueryBuilder()
            .from(BazaNcDividendEntity, 'dividend')
            .select('SUM(dividend.amountCents)', 'sum')
            .innerJoin('dividend.investorAccount', 'investorAccount')
            .leftJoin('dividend.offering', 'offering')
            .where('dividend.investorAccount.id = :investorAccountId', {
                investorAccountId: request.investorAccount.id,
            });

        if (request.investorAccount && request.offering) {
            qb.where('dividend.investorAccount.id = :investorAccountId', {
                investorAccountId: request.investorAccount.id,
            }).andWhere('dividend.offering.id = :offeringId', {
                offeringId: request.offering.id,
            });
        } else if (request.investorAccount) {
            qb.where('dividend.investorAccount.id = :investorAccountId', {
                investorAccountId: request.investorAccount.id,
            });
        } else if (request.offering) {
            qb.where('dividend.offering.id = :offeringId', {
                offeringId: request.offering.id,
            });
        }

        const result = await qb.getRawOne();

        return parseFloat(result?.sum) || 0;
    }
}
