import { Injectable } from '@nestjs/common';
import { BazaNcOfferingEntity } from '../entities/baza-nc-offering.entity';
import { Connection, ILike, IsNull, Not, Repository } from 'typeorm';
import { BazaNcOfferingNotFoundException } from '../exceptions/offering/baza-nc-offering-not-found.exception';
import {
    BazaNcOfferingCreatedEvent,
    BazaNcOfferingDeletedEvent,
    BazaNcOfferingStatus,
    BazaNcOfferingUpdatedEvent,
    OfferingId,
} from '@scaliolabs/baza-nc-shared';
import { EventBus } from '@nestjs/cqrs';
import { postgresLikeEscape } from '@scaliolabs/baza-core-shared';

/**
 * Repository Service for BazaNcOfferingEntity
 */
@Injectable()
export class BazaNcOfferingRepository {
    constructor(private readonly eventBus: EventBus, private readonly connection: Connection) {}

    /**
     * Returns TypeORM repository for BazaNcOfferingEntity
     */
    get repository(): Repository<BazaNcOfferingEntity> {
        return this.connection.getRepository<BazaNcOfferingEntity>(BazaNcOfferingEntity);
    }

    /**
     * Saves BazaNcOfferingEntity to DB
     * Also publish BazaNcOfferingCreatedEvent or BazaNcOfferingUpdatedEvent events if `disableEventPublish` is true
     * @param offering
     * @param options
     */
    async save(
        offering: BazaNcOfferingEntity,
        options: {
            disableEventPublish?: boolean;
        } = {},
    ): Promise<void> {
        const isNew = !offering.id;

        await this.repository.save(offering);

        if (!options.disableEventPublish) {
            if (isNew) {
                this.eventBus.publish(
                    new BazaNcOfferingCreatedEvent({
                        id: offering.id,
                        ncOfferingId: offering.ncOfferingId,
                    }),
                );
            } else {
                this.eventBus.publish(
                    new BazaNcOfferingUpdatedEvent({
                        id: offering.id,
                        ncOfferingId: offering.ncOfferingId,
                    }),
                );
            }
        }
    }

    /**
     * Removes BazaNcOfferingEntity from DB
     * @param offering
     */
    async remove(offering: BazaNcOfferingEntity): Promise<void> {
        const id = offering.id;
        const ncOfferingId = offering.ncOfferingId;

        await this.repository.remove(offering);

        this.eventBus.publish(
            new BazaNcOfferingDeletedEvent({
                id,
                ncOfferingId,
            }),
        );
    }

    /**
     * Returns true if BazaNcOfferingEntity with given ID exists
     * @param offeringId
     */
    async hasOfferingWithId(offeringId: OfferingId): Promise<boolean> {
        return !!(await this.repository.findOne({
            where: [
                {
                    ncOfferingId: offeringId,
                },
            ],
        }));
    }

    /**
     * Returns all BazaNcOfferingEntity entities
     * Returns only with `id`, `ncOfferingId`, `ncOfferingName` and `status` fields
     */
    async findAll(): Promise<Array<Pick<BazaNcOfferingEntity, 'id' | 'ncOfferingId' | 'ncOfferingName' | 'status'>>> {
        return this.repository.find({
            select: ['id', 'ncOfferingId', 'ncOfferingName', 'status'],
            order: {
                id: 'DESC',
            },
        });
    }

    /**
     * Returns BazaNcOfferingEntity by NC Offering ID
     * Returns undefined if none found
     * @param offeringId
     */
    async findOfferingWithId(offeringId: OfferingId): Promise<BazaNcOfferingEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    ncOfferingId: offeringId,
                },
            ],
        });
    }

    /**
     * Returns BazaNcOfferingEntity by NC Offering ID
     * Throws BazaNcOfferingNotFoundException if entity was not found
     * @param offeringId
     * @param options
     */
    async getOfferingWithId(offeringId: OfferingId, options = { sync: false }): Promise<BazaNcOfferingEntity> {
        const entity = await this.repository.findOne({
            where: [
                {
                    ncOfferingId: offeringId,
                },
            ],
        });

        if (!entity) {
            throw new BazaNcOfferingNotFoundException({
                offeringId,
            });
        }

        return entity;
    }

    /**
     * Search for BazaNcOfferingEntity entities with partial in NC Offering ID
     * @param partial
     */
    async searchByOfferingId(partial: string): Promise<Array<BazaNcOfferingEntity>> {
        return this.repository.find({
            where: [
                {
                    ncOfferingId: ILike(`%${postgresLikeEscape(partial)}%`),
                },
            ],
        });
    }

    /**
     * Search for BazaNcOfferingEntity entities with partial in NC Offering ID or Offering Name
     * @param partial
     */
    async searchByOfferingIdOrName(partial: string): Promise<Array<BazaNcOfferingEntity>> {
        return this.repository.find({
            where: [
                {
                    ncOfferingId: ILike(`%${postgresLikeEscape(partial)}%`),
                },
                {
                    ncOfferingName: ILike(`%${postgresLikeEscape(partial)}%`),
                },
            ],
        });
    }

    /**
     * Returns Offerings which has trades to release
     */
    async getOpenOfferingsHavingDaysToRelease(): Promise<Array<BazaNcOfferingEntity>> {
        return this.repository.find({
            where: [
                {
                    status: BazaNcOfferingStatus.Open,
                    daysToReleaseFailedTrades: Not(IsNull()),
                },
            ],
        });
    }

    /**
     * Returns all Open offerings
     */
    async getAllOpenOfferings(): Promise<Array<BazaNcOfferingEntity>> {
        return this.repository.find({
            where: [
                {
                    status: BazaNcOfferingStatus.Open,
                },
            ],
        });
    }
}
