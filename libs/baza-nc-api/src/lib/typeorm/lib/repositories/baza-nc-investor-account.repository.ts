import { Injectable } from '@nestjs/common';
import { Connection, ILike, In, Repository } from 'typeorm';
import { BazaNcInvestorAccountEntity } from '../entities/baza-nc-investor-account.entity';
import { BazaNcInvestorAccountNotFoundException } from '../exceptions/investor-account/baza-nc-investor-account-not-found.exception';
import { AccountEntity, BazaAccountRepository } from '@scaliolabs/baza-core-api';
import { NorthCapitalAccountId, PartyId } from '@scaliolabs/baza-nc-shared';
import { postgresLikeEscape } from '@scaliolabs/baza-core-shared';
import { DwollaCustomerId } from '@scaliolabs/baza-dwolla-shared';

export const INVESTOR_ACCOUNT_RELATIONS = ['user'];

/**
 * Repository Service for BazaNcInvestorAccountEntity
 */
@Injectable()
export class BazaNcInvestorAccountRepository {
    constructor(private readonly connection: Connection, private readonly accountRepository: BazaAccountRepository) {}

    /**
     * Returns TypeORM repository for BazaNcInvestorAccountEntity
     */
    get repository(): Repository<BazaNcInvestorAccountEntity> {
        return this.connection.getRepository(BazaNcInvestorAccountEntity);
    }

    /**
     * Saves BazaNcInvestorAccountEntity to DB
     * @param entity
     */
    async save(entity: BazaNcInvestorAccountEntity): Promise<BazaNcInvestorAccountEntity | void> {
        return await this.repository.save(entity);
    }

    /**
     * Removes BazaNcInvestorAccountEntity from DB
     * @param entity
     */
    async remove(entity: BazaNcInvestorAccountEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    /**
     * Returns BazaNcInvestorAccountEntity by Id
     * Returns undefined if Entity with given ID was not found
     * @param id
     */
    async findInvestorAccountById(id: number): Promise<BazaNcInvestorAccountEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    id,
                },
            ],
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });
    }

    /**
     * Returns BazaNcInvestorAccountEntity by Id
     * Throws BazaNcInvestorAccountNotFoundException if Entity with given Id was not found
     * @param id
     */
    async getInvestorAccountById(id: number): Promise<BazaNcInvestorAccountEntity> {
        const entity = await this.findInvestorAccountById(id);

        if (!entity) {
            throw new BazaNcInvestorAccountNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcInvestorAccountEntity by Account ("User") Id
     * Returns undefined if Entity with given Account Id was not found
     * @param userId
     */
    async findInvestorAccountByUserId(userId: number | AccountEntity): Promise<BazaNcInvestorAccountEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    user: userId,
                },
            ],
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });
    }

    /**
     * Returns BazaNcInvestorAccountEntity by Account ("User") Id
     * Throws BazaNcInvestorAccountNotFoundException if Entity with given Account Id was not found
     * @param userId
     */
    async getInvestorAccountByUserId(userId: number): Promise<BazaNcInvestorAccountEntity> {
        const entity = await this.findInvestorAccountByUserId(userId);

        if (!entity) {
            throw new BazaNcInvestorAccountNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcInvestorAccountEntity by NC Account Id
     * Returns undefined if Entity with given NC Account Id was not found
     * @param northCapitalAccountId
     */
    async findInvestorAccountByNcAccountId(northCapitalAccountId: NorthCapitalAccountId): Promise<BazaNcInvestorAccountEntity | undefined> {
        return await this.repository.findOne({
            where: [
                {
                    northCapitalAccountId,
                },
            ],
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });
    }

    /**
     * Returns BazaNcInvestorAccountEntity by NC Account Id
     * Throws BazaNcInvestorAccountNotFoundException if Entity with given NC Account Id was not found
     * @param northCapitalAccountId
     */
    async getInvestorAccountByNcAccountId(northCapitalAccountId: NorthCapitalAccountId): Promise<BazaNcInvestorAccountEntity> {
        const entity = await this.findInvestorAccountByNcAccountId(northCapitalAccountId);

        if (!entity) {
            throw new BazaNcInvestorAccountNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcInvestorAccountEntity by NC Party Id
     * Returns undefined if Entity with given NC Party Id was not found
     * @param northCapitalPartyId
     */
    async getInvestorAccountByNcPartyId(northCapitalPartyId: PartyId): Promise<BazaNcInvestorAccountEntity> {
        const entity = await this.repository.findOne({
            where: [
                {
                    northCapitalPartyId,
                },
            ],
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });

        if (!entity) {
            throw new BazaNcInvestorAccountNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcInvestorAccountEntity by NC Party Id
     * Throws BazaNcInvestorAccountNotFoundException if Entity with given NC Party Id was not found
     * @param northCapitalPartyId
     */
    async findInvestorAccountByNcPartyId(northCapitalPartyId: PartyId): Promise<BazaNcInvestorAccountEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    northCapitalPartyId,
                },
            ],
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });
    }

    /**
     * Returns BazaNcInvestorAccountEntity with given Dwolla Customer Id
     * Returns undefined if Entity with given Dwolla Customer Id was not found
     * @param dwollaCustomerId
     */
    async findInvestorAccountByDwollaId(dwollaCustomerId: DwollaCustomerId): Promise<BazaNcInvestorAccountEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    dwollaCustomerId,
                },
            ],
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });
    }

    /**
     * Returns BazaNcInvestorAccountEntity with given Dwolla Customer Id
     * Throws BazaNcInvestorAccountNotFoundException if Entity with given Dwolla Customer Id was not found
     * @param dwollaCustomerId
     */
    async getInvestorAccountByDwollaId(dwollaCustomerId: DwollaCustomerId): Promise<BazaNcInvestorAccountEntity> {
        const entity = await this.findInvestorAccountByDwollaId(dwollaCustomerId);

        if (!entity) {
            throw new BazaNcInvestorAccountNotFoundException();
        }

        return entity;
    }

    /**
     * Search BazaNcInvestorAccountEntity entities by Full Name or Email of associated Baza Account
     * @param partial
     */
    async searchByFullnameOrEmail(partial: string): Promise<Array<BazaNcInvestorAccountEntity>> {
        const accounts = await this.accountRepository.searchActiveAccountsByFullNameOrEmail(partial);

        if (accounts.length > 0) {
            return this.repository.find({
                where: [
                    {
                        user: In(accounts.map((next) => next.id)),
                    },
                ],
                relations: INVESTOR_ACCOUNT_RELATIONS,
            });
        } else {
            return [];
        }
    }

    /**
     * Search BazaNcInvestorAccountEntity entities with NC Account ID similar to input
     * @param partial
     */
    async searchByNcAccountId(partial: string): Promise<Array<BazaNcInvestorAccountEntity>> {
        return this.repository.find({
            where: [
                {
                    northCapitalAccountId: ILike(`%${postgresLikeEscape(partial)}%`),
                },
            ],
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });
    }

    /**
     * Search BazaNcInvestorAccountEntity entities with NC Party ID similar to input
     * @param partial
     */
    async searchByNcPartyId(partial: string): Promise<Array<BazaNcInvestorAccountEntity>> {
        return this.repository.find({
            where: [
                {
                    northCapitalPartyId: ILike(`%${postgresLikeEscape(partial)}%`),
                },
            ],
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });
    }

    /**
     * Returns IDs of all existing Investor Accounts
     */
    async findAllInvestorAccountIds(): Promise<Array<number>> {
        const qb = await this.repository.createQueryBuilder('entity').select(['id']).getRawMany();

        return (qb || []).map((next) => next.id);
    }
}
