import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaNcTaxDocumentEntity } from '../entities/baza-nc-tax-document.entity';
import { BazaNcTaxDocumentNotFoundException } from '../exceptions/tax-document/baza-nc-tax-document-not-found.exception';

export const BAZA_NC_TAX_DOCUMENT_RELATIONS = ['investorAccount', 'investorAccount.user'];

@Injectable()
export class BazaNcTaxDocumentRepository {
    constructor(private readonly connection: Connection) {}

    get repository(): Repository<BazaNcTaxDocumentEntity> {
        return this.connection.getRepository(BazaNcTaxDocumentEntity);
    }

    async save(entities: Array<BazaNcTaxDocumentEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaNcTaxDocumentEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<BazaNcTaxDocumentEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    id,
                },
            ],
            relations: BAZA_NC_TAX_DOCUMENT_RELATIONS,
        });
    }

    async findByUrl(url: string): Promise<BazaNcTaxDocumentEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    url,
                },
            ],
            relations: BAZA_NC_TAX_DOCUMENT_RELATIONS,
        });
    }

    async getById(id: number): Promise<BazaNcTaxDocumentEntity> {
        const entity = await this.findById(id);

        if (!entity) {
            throw new BazaNcTaxDocumentNotFoundException();
        }

        return entity;
    }

    async getByUrl(url: string): Promise<BazaNcTaxDocumentEntity> {
        const entity = await this.findByUrl(url);

        if (!entity) {
            throw new BazaNcTaxDocumentNotFoundException();
        }

        return entity;
    }

    async findAll(): Promise<Array<BazaNcTaxDocumentEntity>> {
        return this.repository.find({
            order: {
                id: 'DESC',
            },
        });
    }

    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository.createQueryBuilder('e').select('MAX(e.sortOrder)', 'max').getRawOne();

        return result.max || 0;
    }
}
