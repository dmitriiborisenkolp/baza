import { Injectable } from '@nestjs/common';
import { Connection, In, LessThan, Not, Repository } from 'typeorm';
import { BazaNcTransactionNotFoundException } from '../exceptions/transaction/baza-nc-transaction-not-found.exception';
import { BAZA_NC_TRANSACTION_ENTITY_RELATIONS, BazaNcTransactionEntity } from '../entities/baza-nc-transaction.entity';
import {
    BazaNcTradeCreatedEvent,
    BazaNcTradeDeletedEvent,
    BazaNcTradeUpdatedEvent,
    OfferingId,
    OrderStatus,
    TradeId,
} from '@scaliolabs/baza-nc-shared';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { TransactionState } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../entities/baza-nc-investor-account.entity';
import { EventBus } from '@nestjs/cqrs';
import * as _ from 'lodash';
import { BazaNcOfferingEntity } from '../entities/baza-nc-offering.entity';
import * as moment from 'moment';

/**
 * Repository Service for BazaNcTransactionEntity
 */
@Injectable()
export class BazaNcTransactionRepository {
    constructor(private readonly eventBus: EventBus, private readonly connection: Connection) {}

    /**
     * Returns TypeORM repository for BazaNcTransactionEntity
     */
    get repository(): Repository<BazaNcTransactionEntity> {
        return this.connection.getRepository<BazaNcTransactionEntity>(BazaNcTransactionEntity);
    }

    /**
     * Returns relations for BazaNcTransactionEntity
     */
    get relations(): Array<keyof BazaNcTransactionEntity> {
        return BAZA_NC_TRANSACTION_ENTITY_RELATIONS;
    }

    /**
     * Saves BazaNcTransactionEntity entity to DB
     * The method will trigger BazaNcTradeCreatedEvent or BazaNcTradeUpdatedEvent event
     * @param entity
     */
    async save(entity: BazaNcTransactionEntity): Promise<void> {
        const isNew = !entity.id;

        await this.repository.save(entity);

        if (isNew) {
            this.eventBus.publish(
                new BazaNcTradeCreatedEvent({
                    id: entity.id,
                    ncTradeId: entity.ncTradeId,
                    ncOfferingId: entity.ncOfferingId,
                }),
            );
        } else {
            this.eventBus.publish(
                new BazaNcTradeUpdatedEvent({
                    id: entity.id,
                    ncTradeId: entity.ncTradeId,
                    ncOfferingId: entity.ncOfferingId,
                }),
            );
        }
    }

    /**
     * Removes BazaNcTransactionEntity from DB
     * The method will trigger BazaNcTradeDeletedEvent event
     * @param entity
     */
    async remove(entity: BazaNcTransactionEntity): Promise<void> {
        await this.repository.remove(entity);

        this.eventBus.publish(
            new BazaNcTradeDeletedEvent({
                ncOfferingId: entity.ncOfferingId,
                ncTradeId: entity.ncTradeId,
            }),
        );
    }

    /**
     * Returns BazaNcTransactionEntity by ID
     * The method will throw BazaNcTransactionNotFoundException if Entity was not found
     * @param id
     */
    async getById(id: number): Promise<BazaNcTransactionEntity> {
        const entity = await this.repository.findOne({
            where: [
                {
                    id,
                },
            ],
            relations: this.relations,
        });

        if (!entity) {
            throw new BazaNcTransactionNotFoundException({ id });
        }

        return entity;
    }

    /**
     * Returns BazaNcTransactionEntity by NC Trade Id
     * The method will returns undefined if Entity was not found
     * @param ncTradeId
     */
    async findByNcTradeId(ncTradeId: TradeId): Promise<BazaNcTransactionEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    ncTradeId: ncTradeId.toString(),
                },
            ],
            relations: this.relations,
        });
    }

    /**
     * Returns BazaNcTransactionEntity by NC Trade Ids (Many)
     * @param ncTradeIds
     */
    async findByNcTradeIds(ncTradeIds: Array<TradeId>): Promise<Array<BazaNcTransactionEntity>> {
        return this.repository.find({
            where: [
                {
                    ncTradeId: In(ncTradeIds.map((ncTradeId) => ncTradeId.toString())),
                },
            ],
            relations: this.relations,
        });
    }

    /**
     * Returns BazaNcTransactionEntity by NC Trade Id
     * The method will throw BazaNcTransactionNotFoundException if Entity was not found
     * @param ncTradeId
     */
    async getByNcTradeId(ncTradeId: TradeId): Promise<BazaNcTransactionEntity> {
        const entity = await this.findByNcTradeId(ncTradeId);

        if (!entity) {
            throw new BazaNcTransactionNotFoundException({ id: ncTradeId });
        }

        return entity;
    }

    /**
     * Returns BazaNcTransactionEntity entities by Offering ID
     * @param ncOfferingId
     */
    async findByNcOfferingId(ncOfferingId: OfferingId): Promise<Array<BazaNcTransactionEntity>> {
        return this.repository.find({
            where: [
                {
                    ncOfferingId,
                },
            ],
        });
    }

    /**
     * Returns BazaNcTransactionEntity entities by Offering ID and with given TransactionStates
     * @param ncOfferingId
     * @param states
     */
    async findByNcOfferingIdWithStates(ncOfferingId: OfferingId, states: Array<TransactionState>): Promise<Array<BazaNcTransactionEntity>> {
        return this.repository.find({
            where: [
                {
                    ncOfferingId,
                    state: In(states),
                },
            ],
            relations: this.relations,
        });
    }

    /**
     * Returns all Transactions of Investor Account
     * @param ncOfferingId
     * @param investorAccount
     */
    async findAllTransactionsOfInvestorAccount(
        ncOfferingId: OfferingId,
        investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<Array<BazaNcTransactionEntity>> {
        return this.repository.find({
            where: [
                {
                    ncOfferingId,
                    investorAccount,
                },
            ],
        });
    }

    /**
     * Returns all Transactions of Offering
     * @param ncOfferingId
     */
    async findAllTransactionsOfOffering(ncOfferingId: OfferingId): Promise<Array<BazaNcTransactionEntity>> {
        return this.repository.find({
            where: [
                {
                    ncOfferingId,
                },
            ],
            relations: this.relations,
        });
    }

    /**
     * Deletes all Transactions of given Account
     * @param account
     */
    async deleteAllTransactionsOfAccount(account: AccountEntity): Promise<void> {
        await this.repository.delete({
            account,
        });
    }

    /**
     * Returns total number of Transactions of Investor Account
     * @param investorAccount
     */
    async countTransactionsOfInvestorAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<number> {
        return this.repository.count({
            where: [
                {
                    investorAccount,
                },
            ],
        });
    }

    /**
     * Returns list of Offerings (Offering ID) which was somehow affected (by purchase) by given Investor Account
     * @param investorAccount
     */
    async findAffectedOfferingsOfInvestorAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<Array<OfferingId>> {
        const allTransactions = await this.repository.find({
            where: [
                {
                    investorAccount,
                },
            ],
        });

        return _.uniq(allTransactions.map((transaction) => transaction.ncOfferingId));
    }

    /**
     * Returns list of all Failed Transactions for given Offering
     * Details: It looks for transaction with TransactionState.PaymentReturned state, but excludes Cancelled transactions
     * (i.e. with ncOrderStatus as OrderStatus.Canceled)
     * @param offering
     */
    async findFailedTransactionsForOffering(offering: BazaNcOfferingEntity): Promise<Array<BazaNcTransactionEntity>> {
        return this.repository.find({
            where: [
                {
                    ncOfferingId: offering.ncOfferingId,
                    ncOrderStatus: Not(OrderStatus.Canceled),
                    state: TransactionState.PaymentReturned,
                    createdAt: LessThan(moment().subtract({ days: offering.daysToReleaseFailedTrades })),
                },
            ],
        });
    }
}
