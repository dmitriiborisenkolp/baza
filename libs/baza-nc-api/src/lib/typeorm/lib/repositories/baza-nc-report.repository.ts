import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BAZA_NC_REPORT_ENTITY_RELATIONS, BazaNcReportEntity } from '../entities/baza-nc-report.entity';
import { BazaNcReportNotFoundException } from '../exceptions/report/baza-nc-report-not-found.exception';
import { TradeId } from '@scaliolabs/baza-nc-shared';

/**
 * Repository Service for BazaNcReportEntity
 */
@Injectable()
export class BazaNcReportRepository {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeOrm repository BazaNcReportEntity
     */
    get repository(): Repository<BazaNcReportEntity> {
        return this.connection.getRepository(BazaNcReportEntity);
    }

    /**
     * Returns relations for BazaNcReportEntity
     */
    get relations(): Array<keyof BazaNcReportEntity> {
        return BAZA_NC_REPORT_ENTITY_RELATIONS;
    }

    /**
     * Saves BazaNcReportEntity entities to DB
     * @param entities
     */
    async save(entities: Array<BazaNcReportEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    /**
     * Removes BazaNcReportEntity entities from DB
     * @param entities
     */
    async remove(entities: Array<BazaNcReportEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    /**
     * Returns BazaNcReportEntity by ULID
     * If entity with given will not be found, the method will returns undefined
     * @param ulid
     */
    async findByUlid(ulid: string): Promise<BazaNcReportEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    ulid,
                },
            ],
            relations: this.relations,
        });
    }

    /**
     * Returns BazaNcReportEntity by ULID
     * If entity with given will not be found, the method will
     * throw BazaNcReportNotFoundException
     * @param ulid
     */
    async getByUlid(ulid: string): Promise<BazaNcReportEntity> {
        const entity = await this.findByUlid(ulid);

        if (!entity) {
            throw new BazaNcReportNotFoundException();
        }

        return entity;
    }

    /**
     * Deletes BazaNcReportEntity entities with given TradeId
     * @param tradeId
     */
    async deleteByTradeId(tradeId: TradeId): Promise<void> {
        await this.repository.delete({
            tradeId,
        });
    }

    /**
     * Returns BazaNcReportEntity by NC Trade Id
     * The method will returns undefined if Entity was not found
     * @param tradeId
     */
    async findByNcTradeId(tradeId: TradeId): Promise<BazaNcReportEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    tradeId,
                },
            ],
            relations: this.relations,
        });
    }

    /**
     * Returns BazaNcReportEntity by NC Trade Id
     * The method will throw BazaNcReportNotFoundException if Entity was not found
     * @param tradeId
     */
    async getByNcTradeId(tradeId: TradeId): Promise<BazaNcReportEntity> {
        const entity = await this.findByNcTradeId(tradeId);

        if (!entity) {
            throw new BazaNcReportNotFoundException();
        }

        return entity;
    }
}
