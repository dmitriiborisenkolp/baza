import { Injectable } from '@nestjs/common';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import { Connection, Repository } from 'typeorm';
import { BAZA_NC_DIVIDEND_TRANSACTION_RELATIONS, BazaNcDividendTransactionEntity } from '../entities/baza-nc-dividend-transaction.entity';
import { BazaNcDividendTransactionNotFoundException } from '../exceptions/dividend/baza-nc-dividend-transaction-not-found.exception';

@Injectable()
export class BazaNcDividendTransactionRepository {
    constructor(private readonly connection: Connection, private readonly logger: BazaLogger) {
        this.logger.setContext(this.constructor.name);
    }

    get repository(): Repository<BazaNcDividendTransactionEntity> {
        return this.connection.getRepository(BazaNcDividendTransactionEntity);
    }

    async save(entities: Array<BazaNcDividendTransactionEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaNcDividendTransactionEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findByUlid(ulid: string, correlationId?: string): Promise<BazaNcDividendTransactionEntity | undefined> {
        this.logger.info(`${this.constructor.name}.${this.findByUlid.name}`, {
            correlationId,
            ulid,
        });

        return this.repository.findOne({
            where: [
                {
                    ulid,
                },
            ],
            relations: BAZA_NC_DIVIDEND_TRANSACTION_RELATIONS,
        });
    }

    async getByUlid(ulid: string, correlationId?: string): Promise<BazaNcDividendTransactionEntity> {
        this.logger.info(`${this.constructor.name}.${this.getByUlid.name}`, {
            correlationId,
            ulid,
        });

        const entity = await this.findByUlid(ulid);

        if (!entity) {
            throw new BazaNcDividendTransactionNotFoundException();
        }

        return entity;
    }
}
