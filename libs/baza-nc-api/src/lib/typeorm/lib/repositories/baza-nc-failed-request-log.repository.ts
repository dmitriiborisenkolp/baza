import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaNcFailedRequestLogEntity } from '../entities/baza-nc-failed-request-log.entity';

/**
 * Repository for BazaNcFailedRequestLogEntity
 */
@Injectable()
export class BazaNcFailedRequestLogRepository {
    constructor(private readonly connection: Connection) {}

    /**
     * Returns TypeORM repository for BazaNcFailedRequestLogEntity
     */
    get repository(): Repository<BazaNcFailedRequestLogEntity> {
        return this.connection.getRepository(BazaNcFailedRequestLogEntity);
    }

    /**
     * Saves BazaNcFailedRequestLogEntity to DB
     * @param entity
     */
    async save(entity: BazaNcFailedRequestLogEntity): Promise<void> {
        await this.repository.save(entity);
    }
}
