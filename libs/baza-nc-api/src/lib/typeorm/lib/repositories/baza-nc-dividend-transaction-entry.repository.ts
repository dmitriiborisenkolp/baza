import { Injectable } from '@nestjs/common';
import { Connection, In, Repository } from 'typeorm';
import {
    BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
    BazaNcDividendTransactionEntryEntity,
} from '../entities/baza-nc-dividend-transaction-entry.entity';
import { BazaNcDividendTransactionEntryNotFoundException } from '../exceptions/dividend/baza-nc-dividend-transaction-entry-not-found.exception';
import { BazaNcDividendTransactionEntity } from '../entities/baza-nc-dividend-transaction.entity';
import { bazaNcDividendNonDraftPaymentStatuses, BazaNcDividendPaymentSource } from '@scaliolabs/baza-nc-shared';
import { DwollaTransferId } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcDividendEntity } from '../entities/baza-nc-dividend.entity';
import { BazaLogger } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcDividendTransactionEntryRepository {
    constructor(private readonly connection: Connection, private readonly logger: BazaLogger) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Returns BazaNcDividendTransactionEntryEntity TypeORM repository
     */
    get repository(): Repository<BazaNcDividendTransactionEntryEntity> {
        return this.connection.getRepository(BazaNcDividendTransactionEntryEntity);
    }

    /**
     * Saves BazaNcDividendTransactionEntryEntity entities to DB
     * @param entities
     */
    async save(entities: Array<BazaNcDividendTransactionEntryEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    /**
     * Removes BazaNcDividendTransactionEntryEntity entities from DB
     * @param entities
     */
    async remove(entities: Array<BazaNcDividendTransactionEntryEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    /**
     * Returns BazaNcDividendTransactionEntryEntity entity by ULID
     * @param ulid
     */
    async findByUlid(ulid: string): Promise<BazaNcDividendTransactionEntryEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    ulid,
                },
            ],
            relations: BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
        });
    }

    /**
     * Returns BazaNcDividendTransactionEntryEntity entities by ULIDs
     * @param ulids
     */
    async findByUlids(ulids: Array<string>): Promise<Array<BazaNcDividendTransactionEntryEntity>> {
        return this.repository.find({
            where: [
                {
                    ulid: In(ulids),
                },
            ],
            relations: BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
        });
    }

    /**
     * Returns BazaNcDividendTransactionEntryEntity by ULID
     * Throw an BazaNcDividendTransactionEntryNotFoundException error if not found
     * @param ulid
     */
    async getByUlid(ulid: string): Promise<BazaNcDividendTransactionEntryEntity> {
        const entity = await this.findByUlid(ulid);

        if (!entity) {
            throw new BazaNcDividendTransactionEntryNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcDividendTransactionEntity entity by Dwolla Transfer Id.
     * @param dwollaTransferId
     */
    async findByDwollaTransferId(dwollaTransferId: DwollaTransferId): Promise<BazaNcDividendTransactionEntryEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    dwollaTransferId,
                },
            ],
            relations: BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
        });
    }

    /**
     * Returns BazaNcDividendTransactionEntity entity by Dwolla Transfer Id. Throw an
     * BazaNcDividendTransactionEntryNotFoundException error if not found
     * @param dwollaTransferId
     */
    async getByDwollaTransferId(dwollaTransferId: DwollaTransferId): Promise<BazaNcDividendTransactionEntryEntity> {
        const entity = await this.findByDwollaTransferId(dwollaTransferId);

        if (!entity) {
            throw new BazaNcDividendTransactionEntryNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcDividendTransaction entities by Dividend Transaction
     * @param dividendTransaction
     */
    async findByDividendTransaction(
        dividendTransaction: BazaNcDividendTransactionEntity,
        correlationId?: string,
    ): Promise<Array<BazaNcDividendTransactionEntryEntity>> {
        this.logger.info(`${this.constructor.name}.${this.findByDividendTransaction.name}`, {
            correlationId,
            dividendTransaction,
        });

        return this.repository.find({
            where: [
                {
                    dividendTransaction,
                },
            ],
            relations: BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
            order: {
                ulid: 'ASC',
            },
        });
    }

    /**
     * Returns BazaNcDividendTransactionEntity entity by Dividend Transaction
     * @param dividend
     */
    async findByDividend(dividend: BazaNcDividendEntity): Promise<BazaNcDividendTransactionEntryEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    result: dividend,
                },
            ],
            relations: BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
            order: {
                ulid: 'ASC',
            },
        });
    }

    /**
     * Returns BazaNcDividendTransactionEntity of Dividend Transaction. Throws an
     * BazaNcDividendTransactionEntryNotFoundException error if not found
     * @param dividend
     */
    async getByDividend(dividend: BazaNcDividendEntity): Promise<BazaNcDividendTransactionEntryEntity> {
        const entity = await this.findByDividend(dividend);

        if (!entity) {
            throw new BazaNcDividendTransactionEntryNotFoundException();
        }

        return entity;
    }

    /**
     * Returns non-draft BazaNcDividendTransactionEntity entities of Dividend Transaction
     * @param dividendTransaction
     */
    async findNonDraftEntriesForDividendTransaction(
        dividendTransaction: BazaNcDividendTransactionEntity,
    ): Promise<Array<BazaNcDividendTransactionEntryEntity>> {
        return this.repository.find({
            where: [
                {
                    dividendTransaction,
                    status: In(bazaNcDividendNonDraftPaymentStatuses),
                },
            ],
            relations: BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS,
            order: {
                ulid: 'ASC',
            },
        });
    }

    /**
     * Returns true if Dividend Transaction still has entities which are not processed by Dwolla
     * @param dividendTransaction
     */
    async hasUnprocessedByDwollaEntities(dividendTransaction: BazaNcDividendTransactionEntity): Promise<boolean> {
        const count = await this.repository.count({
            where: [
                {
                    dividendTransaction,
                    source: BazaNcDividendPaymentSource.Dwolla,
                    processedByDwolla: false,
                },
            ],
        });

        return count > 0;
    }
}
