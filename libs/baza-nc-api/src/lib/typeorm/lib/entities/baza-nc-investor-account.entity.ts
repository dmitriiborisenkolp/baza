import { Column, Entity, PrimaryGeneratedColumn, OneToOne, JoinColumn, Unique, RelationId } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcPurchaseFlowTransactionType, DocumentId } from '@scaliolabs/baza-nc-shared';
import { DwollaCustomerStatus, DwollaDocumentFailureReason, DwollaFailureReasonDetail } from '@scaliolabs/baza-dwolla-shared';

@Unique(['northCapitalAccountId'])
@Entity({
    name: 'nc_investor_account',
})
export class BazaNcInvestorAccountEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @OneToOne(() => AccountEntity, {
        cascade: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    user: AccountEntity;

    @RelationId((entity: BazaNcInvestorAccountEntity) => entity.user)
    userId: number;

    @Column({
        nullable: true,
    })
    northCapitalAccountId: string;

    @Column({
        nullable: true,
    })
    northCapitalPartyId: string;

    @Column({
        default: false,
    })
    isAccountVerificationInProgress: boolean;

    @Column({
        default: false,
    })
    isAccountVerificationCompleted: boolean;

    @Column({
        default: false,
    })
    isInvestorVerified: boolean;

    @Column({
        default: false,
    })
    isAccreditedInvestor: boolean;

    @Column({
        default: false,
    })
    isBankAccountLinked: boolean;

    @Column({
        default: false,
    })
    isBankAccountNcAchLinked: boolean;

    @Column({
        default: false,
    })
    isBankAccountCashInLinked: boolean;

    @Column({
        default: false,
    })
    isBankAccountCashOutLinked: boolean;

    @Column({
        default: false,
    })
    isCreditCardLinked: boolean;

    get isPaymentMethodLinked(): boolean {
        return this.isCreditCardLinked || this.isBankAccountLinked;
    }

    @Column({
        nullable: true,
    })
    ssnDocumentFileName: string;

    @Column({
        nullable: true,
        type: 'varchar',
    })
    ssnDocumentId: DocumentId;

    @Column({
        nullable: true,
    })
    dwollaCustomerId: string;

    @Column({
        nullable: true,
    })
    dwollaCustomerError: string;

    @Column({
        default: true,
    })
    isForeignInvestor: boolean;

    @Column({
        type: 'varchar',
        default: BazaNcPurchaseFlowTransactionType.ACH,
    })
    defaultPaymentMethod: BazaNcPurchaseFlowTransactionType;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    dwollaCustomerVerificationStatus: DwollaCustomerStatus;

    @Column({
        type: 'json',
        nullable: true,
        default: [],
    })
    dwollaCustomerVerificationFailureReasons: Array<DwollaFailureReasonDetail>;
}
