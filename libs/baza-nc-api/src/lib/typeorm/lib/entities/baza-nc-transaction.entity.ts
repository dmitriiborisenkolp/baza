import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { NorthCapitalAccountId, OfferingId, OrderStatus, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { TransactionState } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from './baza-nc-investor-account.entity';

export const BAZA_NC_TRANSACTION_ENTITY_RELATIONS: Array<keyof BazaNcTransactionEntity> = ['account', 'investorAccount'];

@Index(['ncOfferingId'])
@Entity({
    name: 'transaction_entity',
})
export class BazaNcTransactionEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    createdAt: Date = new Date();

    @ManyToOne(() => AccountEntity, {
        cascade: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity;

    @ManyToOne(() => BazaNcInvestorAccountEntity, {
        cascade: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    investorAccount: BazaNcInvestorAccountEntity;

    @Column({
        default: true,
    })
    submitted: boolean;

    @Column({
        type: 'varchar',
    })
    ncOrderStatus: OrderStatus;

    @Column({
        type: 'varchar',
        default: TransactionState.PendingPayment,
    })
    state: TransactionState;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    ncAccountId: NorthCapitalAccountId;

    @Column({
        type: 'varchar',
    })
    ncOfferingId: OfferingId;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    ncOfferingName?: string;

    @Column({
        type: 'varchar',
    })
    ncTradeId: string;

    @Column()
    pricePerShareCents: number;

    @Column()
    shares: number;

    @Column()
    amountCents: number;

    @Column({
        nullable: false,
        default: 0,
    })
    feeCents: number;

    @Column({
        nullable: false,
        default: 0,
    })
    totalCents: number;

    @Column({
        nullable: false,
        default: 0,
    })
    transactionFeesCents: number;

    @Column({
        nullable: false,
        type: 'float',
        default: 0.0,
    })
    transactionFees: number;

    @Column({
        nullable: true,
        type: 'varchar',
    })
    transactionType: BazaNcPurchaseFlowTransactionType;
}
