import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { BazaNcDividendHistoryDto, BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus } from '@scaliolabs/baza-nc-shared';
import { ulid } from 'ulid';
import { BazaNcInvestorAccountEntity } from './baza-nc-investor-account.entity';
import { BazaNcOfferingEntity } from './baza-nc-offering.entity';
import { PrimaryUlidColumn } from '@scaliolabs/baza-core-api';

export const BAZA_NC_DIVIDEND_RELATIONS = ['investorAccount', 'offering'];

@Entity()
export class BazaNcDividendEntity {
    @PrimaryUlidColumn()
    ulid: string = ulid();

    @ManyToOne(() => BazaNcInvestorAccountEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    investorAccount: BazaNcInvestorAccountEntity;

    @ManyToOne(() => BazaNcOfferingEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    offering?: BazaNcOfferingEntity;

    @Column({
        type: 'varchar',
    })
    source: BazaNcDividendPaymentSource;

    @Column()
    date: Date;

    @Column({
        type: 'int',
    })
    amountCents: number;

    @Column({
        type: 'varchar',
    })
    status: BazaNcDividendPaymentStatus;

    @Column({
        type: 'json',
    })
    history: Array<BazaNcDividendHistoryDto>;

    @Column({
        nullable: true,
        unique: true,
    })
    dwollaTransferId?: string;
}
