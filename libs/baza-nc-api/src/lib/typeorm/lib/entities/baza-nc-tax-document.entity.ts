import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BazaNcInvestorAccountEntity } from './baza-nc-investor-account.entity';

@Entity({
    name: 'nc_tax_document',
})
export class BazaNcTaxDocumentEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    isPublished: boolean;

    @Column()
    dateCreatedAt: Date = new Date();

    @Column({
        nullable: true,
    })
    dateUpdatedAt?: Date;

    @ManyToOne(() => BazaNcInvestorAccountEntity, {
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    investorAccount: BazaNcInvestorAccountEntity;

    @Column()
    title: string;

    @Column({
        nullable: true,
    })
    description?: string;

    @Column()
    fileS3Key: string;
}
