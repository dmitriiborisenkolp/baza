import { Column, Entity, Index, PrimaryColumn } from 'typeorm';
import { Domicile, NorthCapitalAccountId, NorthCapitalDate, PartyId } from '@scaliolabs/baza-nc-shared';

@Index(['ncPartyId'])
@Entity({
    name: 'account_verification_session_entity',
})
export class BazaNcAccountVerificationSessionEntity {
    @PrimaryColumn()
    userId: number;

    @Column({
        nullable: true,
        type: 'varchar',
    })
    ncAccountId: NorthCapitalAccountId;

    @Column({
        nullable: true,
        type: 'varchar',
    })
    ncPartyId: PartyId;

    @Column({
        default: false,
        nullable: true,
    })
    hasRequestForAdditionalDocuments: boolean;

    @Column({
        default: false,
        nullable: true,
    })
    hasRequestForSSNDocument: boolean;

    @Column({
        default: false,
        nullable: true,
    })
    hasSsnDocument: boolean;

    @Column({
        default: false,
        nullable: true,
    })
    hasAdditionalDocument: boolean;

    @Column({
        default: false,
    })
    wasCompleted: boolean;

    @Column({
        nullable: true,
    })
    phone: string;

    @Column({
        nullable: true,
    })
    phoneCountryCode: string;

    @Column({
        nullable: true,
    })
    phoneCountryNumericCode: string;

    @Column({
        nullable: true,
    })
    firstName: string;

    @Column({
        nullable: true,
    })
    lastName: string;

    @Column({
        nullable: true,
    })
    residentialStreetAddress1: string;

    @Column({
        nullable: true,
    })
    residentialStreetAddress2: string;

    @Column({
        nullable: true,
    })
    residentialCity: string;

    @Column({
        nullable: true,
    })
    residentialState: string;

    @Column({
        nullable: true,
    })
    residentialZipCode: string;

    @Column({
        nullable: true,
    })
    residentialCountry: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    dateOfBirth: NorthCapitalDate;

    @Column({
        nullable: true,
    })
    ssn: string | undefined;

    @Column({
        nullable: true,
    })
    hasSsn: boolean;

    @Column({
        nullable: true,
    })
    ssnDocumentFileName: string;

    @Column({
        nullable: true,
    })
    ssnDocumentUrl: string;

    @Column({
        nullable: true,
    })
    ssnDocumentAwsKey: string;

    @Column({
        nullable: true,
    })
    ssnDocumentId: string;

    @Column({
        nullable: true,
        type: 'varchar',
    })
    citizenship: Domicile;
}
