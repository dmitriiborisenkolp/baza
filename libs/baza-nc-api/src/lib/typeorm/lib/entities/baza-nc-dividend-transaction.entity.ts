import { Column, CreateDateColumn, Entity } from 'typeorm';
import { ulid } from 'ulid';
import { BazaNcDividendTransactionStatus } from '@scaliolabs/baza-nc-shared';
import { PrimaryUlidColumn } from '@scaliolabs/baza-core-api';

export const BAZA_NC_DIVIDEND_TRANSACTION_RELATIONS = [];

@Entity()
export class BazaNcDividendTransactionEntity {
    @PrimaryUlidColumn()
    ulid: string = ulid();

    @CreateDateColumn()
    dateCreatedAt: Date;

    @Column({
        nullable: true,
    })
    dateUpdatedAt?: Date;

    @Column({
        nullable: true,
    })
    dateProcessedAt?: Date;

    @Column({
        type: 'varchar',
        default: BazaNcDividendTransactionStatus.Draft,
    })
    status: BazaNcDividendTransactionStatus;

    @Column({
        nullable: true,
    })
    title?: string;
}
