import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';
import { ulid } from 'ulid';
import { BazaNcDividendFailureReasonDto, BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from './baza-nc-investor-account.entity';
import { BazaNcDividendEntity } from './baza-nc-dividend.entity';
import { BazaNcDividendTransactionEntity } from './baza-nc-dividend-transaction.entity';
import { BazaNcOfferingEntity } from './baza-nc-offering.entity';
import { PrimaryUlidColumn } from '@scaliolabs/baza-core-api';

export const BAZA_NC_DIVIDEND_TRANSACTION_ENTRY_RELATIONS = ['investorAccount', 'result', 'dividendTransaction', 'offering'];

@Entity()
export class BazaNcDividendTransactionEntryEntity {
    @PrimaryUlidColumn()
    ulid: string = ulid();

    @CreateDateColumn()
    dateCreatedAt: Date;

    @Column({
        nullable: true,
    })
    dateUpdatedAt?: Date;

    @Column({
        nullable: true,
    })
    dateProcessedAt?: Date;

    @OneToOne(() => BazaNcDividendEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    result?: BazaNcDividendEntity;

    @Column()
    date: Date;

    @ManyToOne(() => BazaNcInvestorAccountEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    investorAccount: BazaNcInvestorAccountEntity;

    @ManyToOne(() => BazaNcOfferingEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    offering: BazaNcOfferingEntity;

    @ManyToOne(() => BazaNcDividendTransactionEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    dividendTransaction: BazaNcDividendTransactionEntity;

    @Column({
        type: 'varchar',
    })
    source: BazaNcDividendPaymentSource;

    @Column({
        type: 'int',
    })
    amountCents: number;

    @Column({
        type: 'varchar',
    })
    status: BazaNcDividendPaymentStatus;

    @Column({
        nullable: true,
    })
    dwollaTransferId?: string;

    @Column({
        nullable: true,
        type: 'jsonb',
    })
    failureReason?: BazaNcDividendFailureReasonDto;

    @Column({
        nullable: false,
        default: false,
    })
    processedByDwolla?: boolean;
}
