import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { ulid } from 'ulid';
import {
    AccountTypeCheckingSaving,
    BazaNcBankAccountExport,
    BazaNcBankAccountSource,
    BazaNcBankAccountType,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from './baza-nc-investor-account.entity';
import { PrimaryUlidColumn } from '@scaliolabs/baza-core-api';

export const BAZA_NC_BANK_ACCOUNT_ENTITY_RELATIONS = ['investorAccount'];

@Entity()
export class BazaNcBankAccountEntity {
    @PrimaryUlidColumn()
    ulid: string = ulid();

    @ManyToOne(() => BazaNcInvestorAccountEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    investorAccount: BazaNcInvestorAccountEntity;

    @Column({
        type: 'varchar',
    })
    type: BazaNcBankAccountType;

    @Column({
        type: 'jsonb',
        default: [],
    })
    exported: Array<BazaNcBankAccountExport>;

    @Column({
        type: 'varchar',
    })
    source: BazaNcBankAccountSource;

    @Column({
        default: false,
    })
    isDefault: boolean;

    @Column({
        nullable: true,
    })
    dwollaFundingSourceId?: string;

    @Column()
    name: string;

    @Column()
    accountName: string;

    @Column()
    accountNickName: string;

    @Column()
    accountNumber: string;

    @Column()
    accountRoutingNumber: string;

    @Column({
        type: 'varchar',
    })
    accountType: AccountTypeCheckingSaving;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    plaidAccountId?: string;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    dwollaProcessorToken?: string;

    @Column({
        default: false,
    })
    isLinkedWithLegacy?: boolean;

    @Column({
        default: false,
    })
    secure?: boolean;
}
