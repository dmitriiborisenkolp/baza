import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { IssuerId, IssuerType, BazaNcOfferingStatus, OfferingId, OfferingStatus } from '@scaliolabs/baza-nc-shared';

@Unique(['ncOfferingId'])
@Entity({
    name: 'nc_offering_entity',
})
export class BazaNcOfferingEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column({
        type: 'varchar',
        nullable: false,
        default: BazaNcOfferingStatus.ComingSoon,
    })
    status: BazaNcOfferingStatus;

    @Column({
        default: 0,
        type: 'float',
    })
    percentsFunded: number;

    @Column({
        nullable: true,
    })
    ncOfferingName: string;

    @Column({
        default: 1,
        type: 'int',
    })
    ncCurrentValueCents: number;

    @Column({
        default: false,
        type: 'boolean',
    })
    ncIsTargetAmountHidden = false;

    @Column({
        nullable: true,
    })
    ncTargetAmountPlaceholder?: string;

    @Column({
        type: 'varchar',
    })
    ncOfferingId: OfferingId;

    @Column({
        type: 'varchar',
    })
    ncIssuerId: IssuerId;

    @Column()
    ncIssueName: string;

    @Column({
        type: 'varchar',
    })
    ncIssueType: IssuerType;

    @Column({
        default: 0,
        type: 'float',
    })
    ncTargetAmount: number;

    @Column({
        default: 0,
        type: 'float',
    })
    ncMinAmount: number;

    @Column({
        default: 0,
        type: 'float',
    })
    ncMaxAmount: number;

    @Column({
        default: 1,
    })
    ncMaxSharesPerAccount: number;

    @Column({
        type: 'float',
    })
    ncUnitPrice: number;

    @Column({
        default: 0.0,
        type: 'float',
    })
    ncOfferingFees: number;

    @Column({
        default: 0,
        type: 'integer',
    })
    ncTotalShares: number;

    @Column({
        default: 0,
        type: 'integer',
    })
    ncRemainingShares: number;

    @Column()
    ncStartDate: string;

    @Column()
    ncEndDate: string;

    @Column({
        type: 'varchar',
    })
    ncOfferingStatus: OfferingStatus;

    @Column()
    ncOfferingText: string;

    @Column()
    ncStampingText: string;

    @Column({
        nullable: true,
    })
    ncUpdatedIPAddress: string;

    @Column({
        nullable: true, // TODO: Should be removed later
        type: 'json',
    })
    ncSubscriptionOfferingIds: Array<string>;

    @Column({
        type: 'integer',
        nullable: true,
    })
    daysToReleaseFailedTrades: number;
}
