import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { KYCStatus, NorthCapitalAccountId, PartyId } from '@scaliolabs/baza-nc-shared';

@Entity({
    name: 'account_verification_kyc_log_entity',
})
export class BazaNcAccountVerificationKycLogEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    createdAt: Date;

    @Column()
    userId: number;

    @Column({
        type: 'varchar',
    })
    userEmail: string;

    @Column({
        type: 'varchar',
    })
    userFullName: string;

    @Column({
        type: 'varchar',
    })
    ncAccountId: NorthCapitalAccountId;

    @Column({
        type: 'varchar',
    })
    ncPartyId: PartyId;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    previousKycStatus: KYCStatus;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    newKycStatus: KYCStatus;

    @Column({
        type: 'json',
    })
    kycDetails: any;
}
