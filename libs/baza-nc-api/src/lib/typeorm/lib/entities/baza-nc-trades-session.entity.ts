import { Column, Entity, PrimaryColumn } from 'typeorm';
import { NorthCapitalAccountId, OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';

@Entity({
    name: 'nc_trades_session_entity',
})
export class BazaNcTradesSessionEntity {
    @Column({
        nullable: false,
        default: BazaNcPurchaseFlowTransactionType.ACH,
        type: 'varchar',
    })
    transactionType: BazaNcPurchaseFlowTransactionType;

    @Column({
        nullable: false,
        type: 'varchar',
    })
    accountId: NorthCapitalAccountId;

    @PrimaryColumn({
        type: 'varchar',
    })
    tradeId: TradeId;

    @Column({
        nullable: false,
        type: 'varchar',
    })
    offeringId: OfferingId;

    @Column({
        nullable: false,
    })
    createdAt: Date;

    @Column({
        nullable: false,
    })
    updatedAt: Date;

    @Column({
        nullable: true,
    })
    activeTo: Date;

    @Column({
        nullable: false,
    })
    isOutdated: boolean;

    @Column({
        nullable: false,
    })
    shouldBeDeletedFromNC: boolean;

    @Column({
        nullable: false,
        default: false,
    })
    tradeSessionWasDeletedFromNC: boolean;

    @Column({
        nullable: false,
    })
    numberOfShares: number;

    @Column({
        nullable: false,
    })
    amountCents: number;

    @Column({
        nullable: false,
        default: 0,
    })
    feeCents: number;

    @Column({
        nullable: false,
        default: 0,
    })
    totalCents: number;

    @Column({
        nullable: false,
    })
    pricePerShareCents: number;

    @Column({
        nullable: true,
    })
    docuSignUrl: string;

    @Column({
        nullable: false,
        default: 0,
    })
    transactionFeesCents: number;

    @Column({
        nullable: false,
        type: 'float',
        default: 0.0,
    })
    transactionFees: number;
}
