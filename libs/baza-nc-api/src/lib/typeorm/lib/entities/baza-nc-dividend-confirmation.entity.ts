import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcDividendTransactionEntity } from './baza-nc-dividend-transaction.entity';
import { ulid } from 'ulid';
import { PrimaryUlidColumn } from '@scaliolabs/baza-core-api';

export const BAZA_NC_DIVIDEND_CONFIRMATION_TOKEN_LENGTH = 8;

export const BAZA_NC_DIVIDEND_CONFIRMATION_RELATIONS = ['author', 'transaction'];

@Entity()
export class BazaNcDividendConfirmationEntity {
    @PrimaryUlidColumn()
    ulid: string = ulid();

    @Column({
        type: 'varchar',
        length: BAZA_NC_DIVIDEND_CONFIRMATION_TOKEN_LENGTH,
    })
    token: string;

    @ManyToOne(() => AccountEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    author: AccountEntity;

    @ManyToOne(() => BazaNcDividendTransactionEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    transaction: BazaNcDividendTransactionEntity;
}
