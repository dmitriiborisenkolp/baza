import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

/**
 * Log Entity for Failed Requests to NC API
 * May be used for statistics & reports to NC
 */
@Entity()
export class BazaNcFailedRequestLogEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    @Column()
    uri: string;

    @Column()
    date: Date;

    @Column()
    requestId: string;
}
