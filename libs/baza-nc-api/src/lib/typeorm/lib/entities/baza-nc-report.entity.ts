import { Column, CreateDateColumn, Entity } from 'typeorm';
import { PrimaryUlidColumn } from '@scaliolabs/baza-core-api';
import {
    BAZA_NC_TRADE_ID_LENGTH,
    BazaNcReportStatus,
    bazaNcReportStatusesToResend,
    bazaNcReportStatusesToSend,
    bazaNcReportStatusesToSync,
    TradeId,
} from '@scaliolabs/baza-nc-shared';
import { enumLength } from '@scaliolabs/baza-core-shared';
import { BAZA_DWOLLA_ACH_ID_MAX_LENGTH, BAZA_DWOLLA_TRANSFER_ID_LENGTH } from '@scaliolabs/baza-dwolla-api';
import { ulid } from 'ulid';

export const BAZA_NC_REPORT_ENTITY_RELATIONS: Array<keyof BazaNcReportEntity> = [];

@Entity()
export class BazaNcReportEntity {
    @PrimaryUlidColumn()
    ulid = ulid();

    @CreateDateColumn()
    dateCreatedAt: Date;

    @Column({
        nullable: true,
    })
    dateFundedAt?: Date;

    @Column({
        nullable: true,
    })
    dateReportedAt?: Date;

    @Column({
        type: 'varchar',
        length: BAZA_NC_TRADE_ID_LENGTH,
        unique: true,
    })
    // Warning: Do not make relations here. Trade entities could be removed when using sync tools!
    tradeId: TradeId;

    @Column({
        type: 'char',
        length: BAZA_DWOLLA_TRANSFER_ID_LENGTH,
    })
    dwollaTransferId: string;

    @Column({
        type: 'varchar',
        length: BAZA_DWOLLA_ACH_ID_MAX_LENGTH,
        unique: true,
        nullable: true,
    })
    achId?: string;

    @Column({
        type: 'varchar',
        length: enumLength(BazaNcReportStatus),
    })
    status: BazaNcReportStatus;

    /**
     * Returns True if Report is already sent to NC
     */
    get isSent(): boolean {
        return !!this.dateReportedAt;
    }

    /**
     * Returns true if Report can be sent
     */
    get canBeSent(): boolean {
        return bazaNcReportStatusesToSend.includes(this.status);
    }

    /**
     * Returns true if Report can be re-sent
     */
    get canBeResent(): boolean {
        return bazaNcReportStatusesToResend.includes(this.status);
    }

    /**
     * Returns true if Record can be synced
     */
    get canBySynced(): boolean {
        return bazaNcReportStatusesToSync.includes(this.status);
    }
}
