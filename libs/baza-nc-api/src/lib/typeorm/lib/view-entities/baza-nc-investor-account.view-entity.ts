import { Connection, ViewColumn, ViewEntity } from 'typeorm';
import { BazaNcInvestorAccountEntity } from '../entities/baza-nc-investor-account.entity';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { DocumentId } from '@scaliolabs/baza-nc-shared';

@ViewEntity({
    expression: (connection: Connection) =>
        connection
            .createQueryBuilder()
            .select('investorAccount.id', 'id')
            .addSelect('investorAccount.userId', 'userId')
            .addSelect('investorAccount.northCapitalAccountId', 'northCapitalAccountId')
            .addSelect('investorAccount.northCapitalPartyId', 'northCapitalPartyId')
            .addSelect('investorAccount.dwollaCustomerId', 'dwollaCustomerId')
            .addSelect('investorAccount.dwollaCustomerError', 'dwollaCustomerError')
            .addSelect('investorAccount.dwollaCustomerVerificationFailureReasons', 'dwollaCustomerVerificationFailureReasons')
            .addSelect('investorAccount.dwollaCustomerVerificationStatus', 'dwollaCustomerVerificationStatus')
            .addSelect('investorAccount.isAccountVerificationCompleted', 'isAccountVerificationCompleted')
            .addSelect('investorAccount.isInvestorVerified', 'isInvestorVerified')
            .addSelect('investorAccount.isBankAccountLinked', 'isBankAccountLinked')
            .addSelect('investorAccount.isCreditCardLinked', 'isCreditCardLinked')
            .addSelect('investorAccount.ssnDocumentFileName', 'ssnDocumentFileName')
            .addSelect('investorAccount.ssnDocumentId', 'ssnDocumentId')
            .addSelect('user.fullName', 'fullName')
            .from(BazaNcInvestorAccountEntity, 'investorAccount')
            .leftJoin(AccountEntity, 'user', 'user.id = investorAccount.userId'),
})
export class BazaNcInvestorAccountViewEntity {
    @ViewColumn()
    readonly id: number;

    user: AccountEntity;

    @ViewColumn()
    userId: number;

    @ViewColumn()
    fullName: string;

    @ViewColumn()
    readonly northCapitalAccountId: string;

    @ViewColumn()
    readonly northCapitalPartyId: string;

    @ViewColumn()
    readonly dwollaCustomerId: string;

    @ViewColumn()
    readonly dwollaCustomerError: string;

    @ViewColumn()
    readonly dwollaCustomerVerificationStatus: string;

    @ViewColumn()
    readonly dwollaCustomerVerificationFailureReasons: string;

    @ViewColumn()
    readonly isAccountVerificationCompleted: boolean;

    @ViewColumn()
    readonly isInvestorVerified: boolean;

    @ViewColumn()
    readonly isBankAccountLinked: boolean;

    @ViewColumn()
    readonly isCreditCardLinked: boolean;

    @ViewColumn()
    readonly ssnDocumentFileName: string;

    @ViewColumn()
    readonly ssnDocumentId: DocumentId;
}
