import { Inject, Injectable, Logger } from '@nestjs/common';
import { interval, Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { BazaNcPurchaseFlowTradesSessionService } from '../services/baza-nc-purchase-flow-trades-session.service';
import { PURCHASE_FLOW_API_MODULE_CONFIG, PurchaseFlowApiModuleConfiguration } from '../baza-nc-purchase-flow-api.config';
import { EnvService } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcPurchaseFlowTradesSessionWatcher {
    private readonly stop$: Subject<void> = new Subject<void>();

    constructor(
        @Inject(PURCHASE_FLOW_API_MODULE_CONFIG) private readonly moduleConfig: PurchaseFlowApiModuleConfiguration,
        private readonly env: EnvService,
        private readonly service: BazaNcPurchaseFlowTradesSessionService,
    ) {}

    async start(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            interval(1000 * this.moduleConfig.tradesTTLSec)
                .pipe(startWith(0))
                .subscribe(() => {
                    Promise.all([this.service.markAllOutdatedTradeSessions(), this.service.deleteAllTradeSessionsMarkedAsNCDelete()]).catch(
                        (err) => Logger.error(`[PurchaseFlowTradesSessionService] [startWatchForOutdatedTrades] ${err}`),
                    );
                });

            Logger.log(`[PurchaseFlowTradesSessionService] start watch for outdated trades`);
        }
    }

    async stop(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            Logger.log(`[PurchaseFlowTradesSessionService] stop watch for outdated trades`);

            this.stop$.next();
        }
    }
}
