import { Injectable, Logger } from '@nestjs/common';
import { interval, Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { BazaNcPurchaseFlowFailedTradesService } from '../services/baza-nc-purchase-flow-failed-trades.service';
import { EnvService } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcPurchaseFlowFailedTradesWatcher {
    private readonly stop$: Subject<void> = new Subject<void>();
    private readonly intervalDelaySec: number = 3600; // 1 hour

    constructor(private readonly env: EnvService, private readonly service: BazaNcPurchaseFlowFailedTradesService) {}

    async start(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            interval(1000 * this.intervalDelaySec)
                .pipe(startWith(0))
                .subscribe(() => {
                    this.service
                        .releaseSharesForOutDatedFailedTrades()
                        .catch((err) => Logger.error(`[PurchaseFlowTradesSessionService] [startWatchForFailedTrades] ${err}`));
                });

            Logger.log(`[BazaNcPurchaseFlowFailedTradesService] start watch for failed trades`);
        }
    }

    async stop(): Promise<void> {
        if (this.env.areWatchersAllowed) {
            Logger.log(`[BazaNcPurchaseFlowFailedTradesService] stop watch for failed trades`);

            this.stop$.next();
        }
    }
}
