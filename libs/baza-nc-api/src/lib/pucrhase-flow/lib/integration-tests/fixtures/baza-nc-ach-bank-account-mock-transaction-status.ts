export enum BazaNcAchBankAccountMockTranasctionStatus {
    Settled = 'Test ACH Settled',
    ReturnedInsufficientFunds = 'Test ACH R01',
    ReturnedBankAccountClosed = 'Test ACH R02',
    ReturnedUnableToLocateAccount = 'Test ACH R03',
    ReturnedInvalidBankAccountNumber = 'Test ACH R04',
    ReturnedUnauthorizedDebitEntry = 'Test ACH R05',
    // TODO: add the rest of status mocks could be found here https://app.hubspot.com/documents/4042879/view/272478681?accessId=8277f4
}
