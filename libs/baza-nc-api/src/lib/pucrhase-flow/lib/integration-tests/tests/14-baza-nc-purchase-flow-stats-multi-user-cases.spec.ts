import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { AccountTypeCheckingSaving, OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

const http = new BazaDataAccessNode();

const dataAccessE2e = new BazaE2eNodeAccess(http);
const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

let NC_OFFERING_ID: OfferingId;
let TRANSACTION_ID_1: TradeId;

describe('@scaliolabs/baza-nc-api/purchase-flow/14-baza-nc-purchase-flow-stats-multi-user-cases.spec.ts', () => {
    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUser2Fixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingStatsFixture,
            ],
            specFile: __filename,
        });
    });

    it('will successfully set up bank account (user 1)', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully set up bank account (user 2)', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will return offering by id', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;
    });

    it('will display 7 available shares for user 1 initially', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(7);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
    });

    it('will display 7 available shares for user 2 initially', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(7);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
    });

    it('will reserve 6 shares for user 1', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 6,
            amount: 6000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRANSACTION_ID_1 = response.id;
    });

    it('will display 7 available shares for user 1 (✅ withReservedShares ✅ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(7);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(10);
        expect(response.numSharesAvailableForUser).toBe(7);
    });

    it('will display 4 available shares for user 2 (✅ withReservedShares ✅ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(4);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(4);
        expect(response.numSharesAvailableForUser).toBe(4);
    });

    it('will display 7 available shares for user 1 (❌ withReservedShares ✅ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            withReservedShares: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(7);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(10);
        expect(response.numSharesAvailableForUser).toBe(7);
    });

    it('will display 4 available shares for user 2 (❌ withReservedShares ✅ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            withReservedShares: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(4);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(4);
        expect(response.numSharesAvailableForUser).toBe(4);
    });

    it('will display 7 available shares for user 1 (✅ withReservedShares ❌ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            withReservedByOtherUsersShares: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(7);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(10);
        expect(response.numSharesAvailableForUser).toBe(7);
    });

    it('will display 4 available shares for user 2 (✅ withReservedShares ❌ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            withReservedByOtherUsersShares: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(7);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(10);
        expect(response.numSharesAvailableForUser).toBe(7);
    });

    it('will display 7 available shares for user 1 (❌ withReservedShares ❌ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            withReservedShares: false,
            withReservedByOtherUsersShares: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(7);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(10);
        expect(response.numSharesAvailableForUser).toBe(7);
    });

    it('will display 7 available shares for user 2 (❌ withReservedShares ❌ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            withReservedShares: false,
            withReservedByOtherUsersShares: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(7);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(10);
        expect(response.numSharesAvailableForUser).toBe(7);
    });

    it('will submit payment', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID_1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display 1 available shares for user 1 (✅ withReservedShares ✅ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(1);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(6);
        expect(response.totalOfferingSharesRemaining).toBe(4);
        expect(response.numSharesAvailableForUser).toBe(1);
    });

    it('will display 4 available shares for user 2 (✅ withReservedShares ✅ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(4);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(4);
        expect(response.numSharesAvailableForUser).toBe(4);
    });

    it('will display 1 available shares for user 1 (❌ withReservedShares ❌ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            withReservedShares: false,
            withReservedByOtherUsersShares: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(1);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(6);
        expect(response.totalOfferingSharesRemaining).toBe(4);
        expect(response.numSharesAvailableForUser).toBe(1);
    });

    it('will display 4 available shares for user 2 (❌ withReservedShares ❌ withReservedByOtherUsersShares)', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            withReservedShares: false,
            withReservedByOtherUsersShares: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(4);
        expect(response.numSharesAlreadyPurchasedByUser).toBe(0);
        expect(response.totalOfferingSharesRemaining).toBe(4);
        expect(response.numSharesAvailableForUser).toBe(4);
    });
});
