import 'reflect-metadata';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcE2eNodeAccess, BazaNcPurchaseFlowBankAccountNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { AccountTypeCheckingSaving } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/purchase-flow/07-baza-nc-purchase-flow-bank-account-mask.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await dataAccessE2eNc.enableMaskConfig();

        await http.authE2eUser();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    afterAll(async () => {
        await dataAccessE2eNc.disableMaskConfig();
    });

    it('will correctly set up bank account and returns masked account/routing numbers', async () => {
        const response = await dataAccessBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
    });

    it('will correctly returns bank account with masked account/routing numbers', async () => {
        const response = await dataAccessBankAccount.getBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
    });
});
