import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { AccountTypeCheckingSaving, convertToCents, OfferingId } from '@scaliolabs/baza-nc-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../../account-verification';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcApiOfferingFixtures } from '../../../../../offering';

jest.setTimeout(240000);

let NC_OFFERING_ID: OfferingId;

describe('@scaliolabs/baza-nc-api/purchase-flow/without-transaction-fee/02-baza-nc-purchase-flow-cmnw-1599.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;

        await http.authE2eUser();

        await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will have 100 shares available to purchase', async () => {
        const stats = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(stats)).toBeFalsy();

        expect(stats.canPurchase).toBe(100);
        expect(stats.numSharesAlreadyPurchasedByUser).toBe(0);
    });

    it('will allow to create session with close to limit', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: convertToCents(80 /* shares */ * 10 /* unit price */),
            numberOfShares: 80,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const stats = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(stats)).toBeFalsy();

        expect(stats.canPurchase).toBe(100);
        expect(stats.numSharesAlreadyPurchasedByUser).toBe(0);
    });

    it('will not failed to update session with max limit', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: convertToCents(100 /* shares */ * 10 /* unit price */),
            numberOfShares: 100,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const stats = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(stats)).toBeFalsy();

        expect(stats.canPurchase).toBe(100);
        expect(stats.numSharesAlreadyPurchasedByUser).toBe(0);
    });
});
