import 'reflect-metadata';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaCommonErrorCodes, BazaCoreE2eFixtures, shiftMonth } from '@scaliolabs/baza-core-shared';
import { BazaCreditCardMonth, BazaCreditCardType } from '@scaliolabs/baza-core-shared';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowCreditCardTypes } from '@scaliolabs/baza-nc-shared';
import { CardType } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowCreditCardNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/purchase-flow/01-baza-nc-purchase-flow-credit-card.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCreditCard = new BazaNcPurchaseFlowCreditCardNodeAccess(http);

    const NOW = new Date();
    const CURRENT_YEAR = NOW.getFullYear() - 2000;
    const NEXT_YEAR = NOW.getFullYear() - 2000 + 1;
    const CURRENT_MONTH = shiftMonth(NOW.getMonth(), 1);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eUser();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('correctly displays that no credit card set', async () => {
        const response = await dataAccessCreditCard.getCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isAvailable).toBeFalsy();
        expect(response.creditCard).toBeUndefined();
    });

    it('correctly setup credit card', async () => {
        const response = await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH as any,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.creditCardType).toBe(BazaCreditCardType.Visa);
        expect(response.creditCardholderName).toBe('JOHN DOE');
        expect(response.creditCardNumber).toBe('4916338506082832');
        expect(response.creditCardExpireMonth).toBe(CURRENT_MONTH);
        expect(response.creditCardExpireYear).toBe(NEXT_YEAR);
    });

    it('correctly displays that credit card set', async () => {
        const response = await dataAccessCreditCard.getCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isAvailable).toBeTruthy();
        expect(response.creditCard).toBeDefined();
        expect(response.creditCard.creditCardType).toBe(BazaCreditCardType.Visa);
        expect(response.creditCard.creditCardholderName).toBe('JOHN DOE');
        expect(response.creditCard.creditCardNumber).toBe('4916338506082832');
        expect(response.creditCard.creditCardExpireMonth).toBe(CURRENT_MONTH);
        expect(response.creditCard.creditCardExpireYear).toBe(NEXT_YEAR);
    });

    it('correctly updates credit card with uppercase holder name', async () => {
        const response = await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '5280 9342 8317 1080',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: BazaCreditCardMonth.Aug,
            creditCardCvv: '456',
            creditCardholderName: 'FOO BAR',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.creditCardType).toBe(BazaCreditCardType.MasterCard);
        expect(response.creditCardholderName).toBe('FOO BAR');
        expect(response.creditCardNumber).toBe('5280934283171080');
        expect(response.creditCardExpireMonth).toBe(8);
        expect(response.creditCardExpireYear).toBe(NEXT_YEAR);
    });

    it('correctly displays that credit card set (#2)', async () => {
        const response = await dataAccessCreditCard.getCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isAvailable).toBeTruthy();
        expect(response.creditCard).toBeDefined();
        expect(response.creditCard.creditCardType).toBe(BazaCreditCardType.MasterCard);
        expect(response.creditCard.creditCardholderName).toBe('FOO BAR');
        expect(response.creditCard.creditCardNumber).toBe('5280934283171080');
        expect(response.creditCard.creditCardExpireMonth).toBe(8);
        expect(response.creditCard.creditCardExpireYear).toBe(NEXT_YEAR);
    });

    it('correctly updates credit card with lowercase holder name', async () => {
        const response = await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '5280 9342 8317 1080',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: BazaCreditCardMonth.Aug,
            creditCardCvv: '456',
            creditCardholderName: 'Foo Bar',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.creditCardType).toBe(BazaCreditCardType.MasterCard);
        expect(response.creditCardholderName).toBe('FOO BAR');
        expect(response.creditCardNumber).toBe('5280934283171080');
        expect(response.creditCardExpireMonth).toBe(8);
        expect(response.creditCardExpireYear).toBe(NEXT_YEAR);
    });

    it('correctly displays that credit card set (#2)', async () => {
        const response = await dataAccessCreditCard.getCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isAvailable).toBeTruthy();
        expect(response.creditCard).toBeDefined();
        expect(response.creditCard.creditCardType).toBe(BazaCreditCardType.MasterCard);
        expect(response.creditCard.creditCardholderName).toBe('FOO BAR');
        expect(response.creditCard.creditCardNumber).toBe('5280934283171080');
        expect(response.creditCard.creditCardExpireMonth).toBe(8);
        expect(response.creditCard.creditCardExpireYear).toBe(NEXT_YEAR);
    });

    it('correctly delete credit card', async () => {
        const response = await dataAccessCreditCard.deleteCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isDeleted).toBeTruthy();
    });

    it('correctly displays that no credit card set (#2)', async () => {
        const response = await dataAccessCreditCard.getCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isAvailable).toBeFalsy();
        expect(response.creditCard).toBeUndefined();
    });

    it('correctly handle duplicate delete credit card', async () => {
        const response = await dataAccessCreditCard.deleteCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isDeleted).toBeFalsy();
    });

    it('correctly displays that no credit card set (#3)', async () => {
        const response = await dataAccessCreditCard.getCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isAvailable).toBeFalsy();
        expect(response.creditCard).toBeUndefined();
    });

    it('correctly validated valid card (Visa)', async () => {
        const response = await dataAccessCreditCard.validateCreditCard({
            creditCardNumber: '4916338506082832',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeTruthy();
        expect(response.creditCardType).toBeDefined();
        expect(response.creditCardType).toBe(BazaCreditCardType.Visa);

        const ncType = bazaNcPurchaseFlowCreditCardTypes().find((d) => d.baza === response.creditCardType);

        expect(ncType).toBeDefined();
        expect(ncType.nc).toBe(CardType.Visa);
    });

    it('correctly validated valid card (MasterCard)', async () => {
        const response = await dataAccessCreditCard.validateCreditCard({
            creditCardNumber: '5456060454627409',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeTruthy();
        expect(response.creditCardType).toBeDefined();
        expect(response.creditCardType).toBe(BazaCreditCardType.MasterCard);

        const ncType = bazaNcPurchaseFlowCreditCardTypes().find((d) => d.baza === response.creditCardType);

        expect(ncType).toBeDefined();
        expect(ncType.nc).toBe(CardType.MasterCard);
    });

    it.skip('correctly invalidate American Express card', async () => {
        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '3459 3634 6788 903',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH as any,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.CreditCardIsNotSupported);
    });

    it('correctly invalidate invalid card number', async () => {
        const response = await dataAccessCreditCard.validateCreditCard({
            creditCardNumber: '4916338506082833',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.isValid).toBeFalsy();
        expect(response.creditCardType).not.toBeDefined();
    });

    it('correctly response if user provides invalid card credentials (CVV code)', async () => {
        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH as any,
            creditCardCvv: '1234',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.InvalidCardDetails);
    });

    it('correctly response if user provides invalid card credentials (Invalid card length)', async () => {
        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832 56',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH as any,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.InvalidCardDetails);
    });

    it('will correctly fails with proper error code for invalid year (YEAR - 1)', async () => {
        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: CURRENT_YEAR - 1,
            creditCardExpireMonth: CURRENT_MONTH as any,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.CreditCardIsExpired);
    });

    it('will correctly fails with proper error code for invalid year (YEAR - 1)', async () => {
        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: CURRENT_YEAR - 1,
            creditCardExpireMonth: CURRENT_MONTH as any,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.CreditCardIsExpired);
    });

    it('will correctly fails with proper error code for invalid year (MONTH - 1)', async () => {
        let currentMonth = CURRENT_MONTH - 1;
        let currentYear = CURRENT_YEAR;

        if (currentMonth === 0) {
            currentMonth = 12;
            currentYear = currentYear - 1;
        }

        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: currentYear,
            creditCardExpireMonth: currentMonth,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.CreditCardIsExpired);
    });

    it('will correctly fails with proper error code for invalid year (OutOfRange MONTH)', async () => {
        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: CURRENT_YEAR - 1,
            creditCardExpireMonth: 13 as any,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.CreditCardIsOutOfRange);
    });

    it('will correctly fails with proper error code for invalid year (OutOfRange MONTH #2)', async () => {
        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: CURRENT_YEAR - 1,
            creditCardExpireMonth: 0, // invalid month range (js -> getMonth)
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaCommonErrorCodes.BazaValidationError);
    });

    it('will correctly fails with proper error code for invalid year (OutOfRange YEAR)', async () => {
        const response: BazaError = (await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: 100,
            creditCardExpireMonth: CURRENT_MONTH,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.CreditCardIsOutOfRange);
    });
});
