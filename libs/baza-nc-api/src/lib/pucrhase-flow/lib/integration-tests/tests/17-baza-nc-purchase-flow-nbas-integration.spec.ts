import 'reflect-metadata';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcBankAccountsNodeAccess,
    BazaNcE2eNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { AccountTypeCheckingSaving, BazaNcBankAccountType } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/purchase-flow/17-baza-nc-purchase-flow-nbas-integration.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessNBAS = new BazaNcBankAccountsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await dataAccessE2eNc.enableMaskConfig();
        await dataAccessE2eNc.enableLBASync();
    });

    afterAll(async () => {
        await dataAccessE2eNc.disableMaskConfig();
        await dataAccessE2eNc.disableLBASync();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display that no bank account is set', async () => {
        const response = await dataAccessBankAccount.getBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
        expect(response.details).not.toBeDefined();

        const investorResponse = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorResponse));

        expect(investorResponse.isBankAccountLinked).toBeFalsy();
        expect(investorResponse.isBankAccountCashOutLinked).toBeFalsy();
    });

    it('will not fail with attempt to delete bank account', async () => {
        const response = await dataAccessBankAccount.deleteBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully set bank account', async () => {
        const response = await dataAccessBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
        expect(response.details.accountName).toBe('FOO BAR');
        expect(response.details.accountNickName).toBe('FOO BAR');
    });

    it('will display that bank account is set', async () => {
        const response = await dataAccessBankAccount.getBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
        expect(response.details.accountName).toBe('FOO BAR');
        expect(response.details.accountNickName).toBe('FOO BAR');
    });

    it('will display that investor account flags are correctly set', async () => {
        const investorResponse = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorResponse));

        expect(investorResponse.isBankAccountLinked).toBeTruthy();
        expect(investorResponse.isBankAccountCashOutLinked).toBeFalsy();
    });

    it('will display that cash-in bank account is added to NBAS', async () => {
        const response = await dataAccessNBAS.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].accountName).toBe('Foo Bar');
        expect(response[0].accountNickName).toBe('Foo Bar');
        expect(response[0].accountNumber).toBe('************0000');
        expect(response[0].accountRoutingNumber).toBe('*****1533');
    });

    it('will allow to re-set same bank account once more', async () => {
        const response = await dataAccessBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****1533');
        expect(response.details.accountName).toBe('FOO BAR');
        expect(response.details.accountNickName).toBe('FOO BAR');
    });

    it('will not create duplicate in NBAS', async () => {
        const response = await dataAccessNBAS.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].accountName).toBe('Foo Bar');
        expect(response[0].accountNickName).toBe('Foo Bar');
        expect(response[0].accountNumber).toBe('************0000');
        expect(response[0].accountRoutingNumber).toBe('*****1533');
    });

    it('will allow to set different bank account', async () => {
        const response = await dataAccessBankAccount.setBankAccount({
            accountName: 'John Smith',
            accountRoutingNumber: '021000021',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.details).toBeDefined();
        expect(response.details.accountNumber).toBe('************0000');
        expect(response.details.accountRoutingNumber).toBe('*****0021');
        expect(response.details.accountName).toBe('JOHN SMITH');
        expect(response.details.accountNickName).toBe('JOHN SMITH');
    });

    it('will display that there is still only 1 bank account is NBAS', async () => {
        const response = await dataAccessNBAS.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].type).toBe(BazaNcBankAccountType.CashIn);
        expect(response[0].accountName).toBe('John Smith');
        expect(response[0].accountNickName).toBe('John Smith');
        expect(response[0].accountNumber).toBe('************0000');
        expect(response[0].accountRoutingNumber).toBe('*****0021');
    });

    it('will add bank account using NBAS', async () => {
        const response = await dataAccessNBAS.add({
            accountName: 'Foo Bar',
            accountNickName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Checking,
            type: BazaNcBankAccountType.CashIn,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const listResponse = await dataAccessNBAS.list();

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.length).toBe(2);
    });

    it('will allow to delete bank account', async () => {
        const response = await dataAccessBankAccount.deleteBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that bank account is also removed from NBAS (but will not touch other accounts)', async () => {
        const listResponse = await dataAccessNBAS.list();

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.length).toBe(1);
        expect(listResponse[0].accountRoutingNumber).toBe('*****1533');
    });
});
