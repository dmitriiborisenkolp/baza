import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { TransactionState } from '@scaliolabs/baza-nc-shared';
import { AccountTypeCheckingSaving, OfferingId } from '@scaliolabs/baza-nc-shared';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowCreditCardNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcTransactionsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../../offering';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

let NC_OFFERING_ID: OfferingId;
let PURCHASE_FLOW_ID: string;

describe('@scaliolabs/baza-nc-api/purchase-flow/without-transaction-fee/01-baza-nc-purchase-flow-no-double-reserve-shares.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessPurchaseFlowCreditCard = new BazaNcPurchaseFlowCreditCardNodeAccess(http);
    const dataAccessTransactions = new BazaNcTransactionsNodeAccess(http);
    const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eUser();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('has created fixture offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;

        // Sign to E2E user back
        await http.authE2eUser();

        // Set up bank account and credit card
        await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        await dataAccessPurchaseFlowCreditCard.setCreditCard({
            creditCardExpireYear: 40,
            creditCardNumber: '5280934283171080',
            creditCardExpireMonth: 12,
            creditCardholderName: 'JOHN DOE',
            creditCardCvv: '123',
        });
    });

    it('has 0 shares in investments', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(Array.isArray(response.items) && response.items.length === 0).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(async () => {
            const investments = await dataAccessTransactions.investments({
                index: 1,
                size: 10,
            });

            expect(isBazaErrorResponse(investments)).toBeFalsy();
            expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
        });
    });

    it('will have 5 shares and CREATED status after first session call', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();
        expect(session.amount).toBe(5000);
        expect(session.numberOfShares).toBe(5);
        expect(session.transactionType).toBe(BazaNcPurchaseFlowTransactionType.ACH);

        PURCHASE_FLOW_ID = session.id;

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 1).toBeTruthy();
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.Created);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will still have 5 shares and CREATED status after same session call', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
        });

        expect(session.id).toBe(PURCHASE_FLOW_ID);

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 1).toBeTruthy();
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.Created);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will still have 5 shares and CREATED status after changing transactions type', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
        });

        expect(session.id).toBe(PURCHASE_FLOW_ID);

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 1).toBeTruthy();
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.Created);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will still have CreditType transaction type if no transaction type here provided', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
        });

        expect(session.id).toBe(PURCHASE_FLOW_ID);
        expect(session.transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);
    });

    it('will still have 5 shares and PENDING_PAYMENT status after changing transactions type back to ACH', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(session.id).toBe(PURCHASE_FLOW_ID);
        expect(session.transactionType).toBe(BazaNcPurchaseFlowTransactionType.ACH);
    });

    it('will still have 5 shares and PENDING_PAYMENT status after submitting', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: PURCHASE_FLOW_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 1).toBeTruthy();
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.PendingPayment);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
                expect(investments.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
                expect(investments.items[0].purchasedShares).toBe(5);
                expect(investments.items[0].purchasedAmountTotal).toBe('50.00');
                expect(investments.items[0].purchasedAmountTotalCents).toBe(5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will still have 5 shares in investments after starting one more session', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 3,
            amount: 3000,
        });

        expect(session.id).not.toBe(PURCHASE_FLOW_ID);

        PURCHASE_FLOW_ID = session.id;

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 2).toBeTruthy();
                expect(transactions.items[1].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[1].amountCents).toBe(5000);
                expect(transactions.items[1].state).toBe(TransactionState.PendingPayment);
                expect(transactions.items[1].shares).toBe(5);
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(3000);
                expect(transactions.items[0].state).toBe(TransactionState.Created);
                expect(transactions.items[0].shares).toBe(3);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
                expect(investments.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
                expect(investments.items[0].purchasedShares).toBe(5);
                expect(investments.items[0].purchasedAmountTotal).toBe('50.00');
                expect(investments.items[0].purchasedAmountTotalCents).toBe(5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will be 10 shares after changing number of shares', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
        });

        expect(session.id).not.toBe(PURCHASE_FLOW_ID);

        PURCHASE_FLOW_ID = session.id;

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 2).toBeTruthy();
                expect(transactions.items[1].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[1].amountCents).toBe(5000);
                expect(transactions.items[1].state).toBe(TransactionState.PendingPayment);
                expect(transactions.items[1].shares).toBe(5);
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.Created);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
                expect(investments.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
                expect(investments.items[0].purchasedShares).toBe(5);
                expect(investments.items[0].purchasedAmountTotal).toBe('50.00');
                expect(investments.items[0].purchasedAmountTotalCents).toBe(5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will be 10 shares after specifying transaction type as ACH', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(session.id).toBe(PURCHASE_FLOW_ID);

        PURCHASE_FLOW_ID = session.id;

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 2).toBeTruthy();
                expect(transactions.items[1].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[1].amountCents).toBe(5000);
                expect(transactions.items[1].state).toBe(TransactionState.PendingPayment);
                expect(transactions.items[1].shares).toBe(5);
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.Created);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
                expect(investments.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
                expect(investments.items[0].purchasedShares).toBe(5);
                expect(investments.items[0].purchasedAmountTotal).toBe('50.00');
                expect(investments.items[0].purchasedAmountTotalCents).toBe(5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will be 10 shares after specifying transaction type as CreditCard', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
        });

        expect(session.id).toBe(PURCHASE_FLOW_ID);

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 2).toBeTruthy();
                expect(transactions.items[1].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[1].amountCents).toBe(5000);
                expect(transactions.items[1].state).toBe(TransactionState.PendingPayment);
                expect(transactions.items[1].shares).toBe(5);
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.Created);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
                expect(investments.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
                expect(investments.items[0].purchasedShares).toBe(5);
                expect(investments.items[0].purchasedAmountTotal).toBe('50.00');
                expect(investments.items[0].purchasedAmountTotalCents).toBe(5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will be 10 shares after not specifying transaction type (should be still CreditCard)', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
        });

        expect(session.id).toBe(PURCHASE_FLOW_ID);

        PURCHASE_FLOW_ID = session.id;

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 2).toBeTruthy();
                expect(transactions.items[1].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[1].amountCents).toBe(5000);
                expect(transactions.items[1].state).toBe(TransactionState.PendingPayment);
                expect(transactions.items[1].shares).toBe(5);
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.Created);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
                expect(investments.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
                expect(investments.items[0].purchasedShares).toBe(5);
                expect(investments.items[0].purchasedAmountTotal).toBe('50.00');
                expect(investments.items[0].purchasedAmountTotalCents).toBe(5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will be 10 shares after submitting second purchase flow', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: PURCHASE_FLOW_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(Array.isArray(transactions.items) && transactions.items.length === 2).toBeTruthy();
                expect(transactions.items[1].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[1].amountCents).toBe(5000);
                expect(transactions.items[1].state).toBe(TransactionState.PendingPayment);
                expect(transactions.items[1].shares).toBe(5);
                expect(transactions.items[0].ncOfferingId).toBe(NC_OFFERING_ID);
                expect(transactions.items[0].amountCents).toBe(5000);
                expect(transactions.items[0].state).toBe(TransactionState.PendingPayment);
                expect(transactions.items[0].shares).toBe(5);
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
                expect(investments.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
                expect(investments.items[0].purchasedShares).toBe(10);
                expect(investments.items[0].purchasedAmountTotal).toBe('100.00');
                expect(investments.items[0].purchasedAmountTotalCents).toBe(10000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
