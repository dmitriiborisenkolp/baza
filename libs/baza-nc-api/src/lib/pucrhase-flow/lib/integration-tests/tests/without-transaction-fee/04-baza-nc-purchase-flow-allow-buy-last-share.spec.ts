import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { awaitTimeout, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountTypeCheckingSaving, OfferingId, BazaNcPurchaseFlowTransactionType, TradeId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../../account-verification';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcApiOfferingFixtures } from '../../../../../offering';

jest.setTimeout(240000);

let NC_OFFERING_ID: OfferingId;
let TRANSACTION_ID: TradeId;

describe('@scaliolabs/baza-nc-api/purchase-flow/without-transaction-fee/04-baza-nc-purchase-flow-allow-buy-last-share.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingAllowBuyAllSharesFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    afterAll(async () => {
        // TODO: Remove it after reworking purchase flow event handlers
        await awaitTimeout(5000);
    });

    it('will fetch available offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;
    });

    it('will allow to set bank account', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display with /stats endpoint that 10 out of 10 shares are available to purchase', async () => {
        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 90000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(10);
        expect(response.isValidPurchase).toBeTruthy();
    });

    it('will allow to start session with 9 out of 10 shares', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 90000,
            numberOfShares: 9,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRANSACTION_ID = response.id;
    });

    it('[1] will still display with /stats endpoint that 10 out of 10 shares are available to purchase', async () => {
        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 90000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(10);
        expect(response.isValidPurchase).toBeTruthy();
    });

    it('will not fail with updating trade session with same 9 out of 10 shares', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 90000,
            numberOfShares: 9,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(TRANSACTION_ID);
    });

    it('[1] will still display with /stats endpoint that 10 out of 10 shares are available to purchase', async () => {
        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 90000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(10);
        expect(response.isValidPurchase).toBeTruthy();
    });

    it('will allow to submit purchase 9 out of 10 shares', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that only 1 out of 10 shares are available to purchase', async () => {
        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 10000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(1);
        expect(response.isValidPurchase).toBeTruthy();
    });

    it('will allow to start session with last share', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 10000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).not.toBe(TRANSACTION_ID);

        TRANSACTION_ID = response.id;
    });

    it('[1] will still display that only 1 out of 10 shares are available to purchase', async () => {
        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 10000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(1);
        expect(response.isValidPurchase).toBeTruthy();
    });

    it('will display in stats that its not allowed to reserve more than 1 share', async () => {
        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 20000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(1);
        expect(response.isValidPurchase).toBeFalsy();
    });

    it('will allow to update trade session with same last share', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 10000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[2] will still display that only 1 out of 10 shares are available to purchase', async () => {
        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 10000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(1);
        expect(response.isValidPurchase).toBeTruthy();
    });

    it('will allow to submit purchase for last share', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that no shares available anymore', async () => {
        const response = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 10000,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.canPurchase).toBe(0);
        expect(response.isValidPurchase).toBeFalsy();
    });

    it('will not allow to start session with one more share anymore', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 10000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(response)).toBeTruthy();
    });
});
