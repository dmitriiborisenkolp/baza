import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcE2eNodeAccess, BazaNcOfferingCmsNodeAccess, BazaNcPurchaseFlowLimitsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcPurchaseFlowTransactionType, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';

jest.setTimeout(240000);

let NC_OFFERING_ID: OfferingId;

describe('@scaliolabs/baza-nc-api/purchase-flow/02-baza-nc-purchase-flow-limits.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowLimitsNodeAccess(http);
    const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingAllowBuyAllSharesWithTransactionFeeFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });

        await dataAccessE2eNc.enableTransactionFeeFeature();
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length > 0).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;

        await http.authE2eUser();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    afterAll(async () => {
        await dataAccessE2eNc.disableTransactionFeeFeature();
    });

    it('correctly returns limits data for ACH/CreditCard transfers', async () => {
        const response = await dataAccessPurchaseFlow.limits();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.creditCardPaymentsFee >= 0).toBeTruthy();
        expect(response.maxACHTransferAmountCents > 0).toBeTruthy();
        expect(response.maxCreditCardTransferAmountCents > 0).toBeTruthy();
    });

    it('correctly returns purchase limits data with TransactionFees for ACH/CreditCard transfers', async () => {
        const limits = await dataAccessPurchaseFlow.limits();
        const ccMaxAmountCents = limits.maxCreditCardTransferAmountCents;

        // (transactionFee => 1%). So, 10000 + 100 = 10100 (With fees)
        const requestedAmountCents = 10000;
        const requestedAmountCentsWithTransactionFees = requestedAmountCents + 100;

        const response = await dataAccessPurchaseFlow.limitsForPurchase({
            requestedAmountCents,
            offeringId: NC_OFFERING_ID,
        });

        // Verify CCFees are applied
        expect(isBazaErrorResponse(limits)).toBeFalsy();
        expect(ccMaxAmountCents > 0).toBeTruthy();
        expect(ccMaxAmountCents > requestedAmountCents).toBeTruthy();

        // Verify API successfully implemented
        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(3);

        // ACH Transaction
        expect(response[0].isAvailable).toBeTruthy();
        expect(response[0].transactionFeesCents).toBe(100);
        expect(response[0].feeCents).toBe(0);
        expect(response[0].transactionType).toBe(BazaNcPurchaseFlowTransactionType.ACH);

        // CC Transaction
        const feeCents = Math.ceil((requestedAmountCentsWithTransactionFees / 100) * limits.creditCardPaymentsFee);
        expect(response[1].isAvailable).toBeTruthy();
        expect(response[1].transactionFeesCents).toBe(100);
        expect(response[1].feeCents).toBe(feeCents);
        expect(response[1].transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);

        // Account Balance
        expect(response[2].isAvailable).toBeTruthy();
        expect(response[2].transactionFeesCents).toBe(100);
        expect(response[2].feeCents).toBe(0);
        expect(response[2].transactionType).toBe(BazaNcPurchaseFlowTransactionType.AccountBalance);
    });

    it('should restrict CreditCard transaction with TransactionFees', async () => {
        const limits = await dataAccessPurchaseFlow.limits();
        const ccMaxAmountCents = limits.maxCreditCardTransferAmountCents;

        // Setting RequestedAmount same as MaxLimitAmount
        const response = await dataAccessPurchaseFlow.limitsForPurchase({
            requestedAmountCents: ccMaxAmountCents,
            offeringId: NC_OFFERING_ID,
        });

        // Verify CCFees are applied
        expect(isBazaErrorResponse(limits)).toBeFalsy();
        expect(ccMaxAmountCents > 0).toBeTruthy();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(3);

        // CC Transaction
        expect(response[1].transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);
        expect(response[1].isAvailable).toBeFalsy();

        // Account Balance Transaction
        expect(response[2].transactionType).toBe(BazaNcPurchaseFlowTransactionType.AccountBalance);
        expect(response[2].isAvailable).toBeTruthy();
    });
});
