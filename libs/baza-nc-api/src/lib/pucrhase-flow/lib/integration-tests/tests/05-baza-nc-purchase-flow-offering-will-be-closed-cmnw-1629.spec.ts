import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { AccountTypeCheckingSaving, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcPurchaseFlowErrorCodes } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

let NC_OFFERING_ID: OfferingId;
let NC_TRANSACTION_ID: string;

describe('@scaliolabs/baza-nc-api/purchase-flow/05-baza-nc-purchase-flow-offering-will-be-closed-cmnw-1629.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingNoAccountLimitFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;

        await http.authE2eUser();

        await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will successfully reserve all available shares', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            amount: 1000000,
            numberOfShares: 1000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        NC_TRANSACTION_ID = session.id;

        const stats = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(stats)).toBeFalsy();

        expect(stats.canPurchase).toBe(1000);
        expect(stats.numSharesMax).toBe(1000);
        expect(stats.totalPurchasedAmount).toBe(0);
        expect(stats.numSharesAvailableForUser).toBe(1000);
        expect(stats.numSharesAlreadyPurchasedByUser).toBe(0);

        const statsWithRequestedAmountCents = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 1000000,
        });

        expect(isBazaErrorResponse(statsWithRequestedAmountCents)).toBeFalsy();

        expect(statsWithRequestedAmountCents.canPurchase).toBe(1000);
        expect(statsWithRequestedAmountCents.numSharesMax).toBe(1000);
        expect(statsWithRequestedAmountCents.totalPurchasedAmount).toBe(0);
        expect(statsWithRequestedAmountCents.numSharesAvailableForUser).toBe(1000);
        expect(statsWithRequestedAmountCents.numSharesAlreadyPurchasedByUser).toBe(0);
    });

    it('will successfully submit payment', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: NC_TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const stats = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(stats)).toBeFalsy();

        expect(stats.canPurchase).toBe(0);
        expect(stats.numSharesMax).toBe(1000);
        expect(stats.totalPurchasedAmount).toBe(10000);
        expect(stats.numSharesAvailableForUser).toBe(0);
        expect(stats.numSharesAlreadyPurchasedByUser).toBe(1000);

        const statsWithRequestedAmountCents = await dataAccessPurchaseFlow.stats({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 1000000,
        });

        expect(isBazaErrorResponse(statsWithRequestedAmountCents)).toBeFalsy();

        expect(statsWithRequestedAmountCents.canPurchase).toBe(0);
        expect(statsWithRequestedAmountCents.numSharesMax).toBe(1000);
        expect(statsWithRequestedAmountCents.totalPurchasedAmount).toBe(10000);
        expect(statsWithRequestedAmountCents.numSharesAvailableForUser).toBe(0);
        expect(statsWithRequestedAmountCents.numSharesAlreadyPurchasedByUser).toBe(1000);
    });

    it('will close offering', async () => {
        await http.authE2eAdmin();

        await asyncExpect(async () => {
            const response = await dataAccessOfferingCMS.list({
                index: 1,
                size: 10,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
            expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

            expect(response.items[0].status).toBe(BazaNcOfferingStatus.Closed);
        });
    });

    it('will not allow to reserve more shares and displays that offering is closed', async () => {
        const session: BazaError = (await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            amount: 1000000,
            numberOfShares: 1000,
        })) as any;

        expect(isBazaErrorResponse(session)).toBeTruthy();
        expect(session.code).toBe(BazaNcPurchaseFlowErrorCodes.OfferingIsClosed);
    });
});
