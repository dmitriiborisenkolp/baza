import 'reflect-metadata';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BazaDwollaE2eNodeAccess,
    BazaDwollaEscrowCmsNodeAccess,
    BazaDwollaMasterAccountNodeAccess,
    BazaDwollaPaymentCmsNodeAccess,
} from '@scaliolabs/baza-dwolla-node-access';
import {
    BazaNcDwollaNodeAccess,
    BazaNcE2eNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowLimitsNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcTransactionsCmsNodeAccess,
    BazaNcTransactionsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    BazaDwollaEscrowFundingSourceDto,
    BazaDwollaPaymentStatus,
    BazaDwollaPaymentType,
    DwollaCustomerId,
    DwollaEventTopic,
    DwollaFundingSourceType,
    DwollaTransferId,
} from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcInvestorAccountDto,
    BazaNcPurchaseFlowTransactionType,
    OfferingId,
    OrderStatus,
    TradeId,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/purchase-flow/21-baza-nc-purchase-flow-dwolla-account-balance-escrow.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessE2eDwolla = new BazaDwollaE2eNodeAccess(http);
    const dataAccessNcDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessDwollaMasterAccount = new BazaDwollaMasterAccountNodeAccess(http);
    const dataAccessNcInvestor = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessNcLimits = new BazaNcPurchaseFlowLimitsNodeAccess(http);
    const dataAccessNcOfferings = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessNcPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessNcTransactions = new BazaNcTransactionsNodeAccess(http);
    const dataAccessDwollaPayment = new BazaDwollaPaymentCmsNodeAccess(http);
    const dataAccessTransactionsCms = new BazaNcTransactionsCmsNodeAccess(http);
    const dataAccessEscrow = new BazaDwollaEscrowCmsNodeAccess(http);

    let TRADE_ID: TradeId;
    let DWOLLA_CUSTOMER_ID: DwollaCustomerId;
    let NC_OFFERING_ID: OfferingId;
    let DWOLLA_TRANSFER_ID: DwollaTransferId;
    let DWOLLA_ACH_TRANSFER_ID: DwollaTransferId;
    let DWOLLA_MASTER_BALANCE: number;
    let INVESTOR_ACCOUNT: BazaNcInvestorAccountDto;
    let bankFundingSource: BazaDwollaEscrowFundingSourceDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display that at least 1 Bank Account FS is available to use', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessEscrow.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const fundSource = response.find((next) => next.type === DwollaFundingSourceType.Bank);

        expect(fundSource).toBeDefined();

        bankFundingSource = fundSource;
    });

    it('will select bank account as escrow', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessEscrow.set({
            dwollaFundingSourceId: bankFundingSource.dwollaFundingSourceId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.type).toBe(DwollaFundingSourceType.Bank);
        expect(response.isSelectedAsEscrow).toBeTruthy();
    });

    it('will successfully touches dwolla customer for account', async () => {
        const response = await dataAccessNcDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investor = await dataAccessNcInvestor.current();

        expect(isBazaErrorResponse(investor)).toBeFalsy();
        expect(investor.dwollaCustomerId).toBeDefined();

        DWOLLA_CUSTOMER_ID = investor.dwollaCustomerId;
        INVESTOR_ACCOUNT = investor;
    });

    it('will add funds to investor', async () => {
        const response = await dataAccessE2eDwolla.transferFundsFromMasterAccount({
            dwollaCustomerId: DWOLLA_CUSTOMER_ID,
            amount: '100.00',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fetch current dwolla master account balance', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaMasterAccount.balance();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        DWOLLA_MASTER_BALANCE = parseFloat(response.value);
    });

    it('will fetch offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNcOfferings.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);

        NC_OFFERING_ID = response[0].ncOfferingId;
    });

    it('will display limits for AccountBalance payment methods', async () => {
        const limits = await dataAccessNcLimits.limitsForPurchase({
            offeringId: NC_OFFERING_ID,
            requestedAmountCents: 2000,
        });

        expect(isBazaErrorResponse(limits)).toBeFalsy();

        const accountBalanceLimits = limits.find((next) => next.transactionType === BazaNcPurchaseFlowTransactionType.AccountBalance);

        expect(accountBalanceLimits).toBeDefined();
        expect(accountBalanceLimits.transactionType).toBe(BazaNcPurchaseFlowTransactionType.AccountBalance);
        expect(accountBalanceLimits.isAvailable).toBeTruthy();
        expect(accountBalanceLimits.feeCents).toBe(0);
        expect(accountBalanceLimits.transactionFeesCents).toBe(0);
        expect(accountBalanceLimits.amountWithFeesCents).toBe(2000);
        expect(accountBalanceLimits.maximumAmountCents).toBe(1000000);
    });

    it('will start new purchase flow session (purchase 2 shares)', async () => {
        const response = await dataAccessNcPurchaseFlow.session({
            amount: 2000,
            numberOfShares: 2,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;
    });

    it('will submit purchase flow session', async () => {
        const response = await dataAccessNcPurchaseFlow.submit({
            id: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new investment', async () => {
        const response = await dataAccessNcTransactions.investments({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
        expect(response.items[0].lastTransactionState).toBe(TransactionState.PendingPayment);
        expect(response.items[0].purchasedAmountTotalCents).toBe(2000);
        expect(response.items[0].purchasedShares).toBe(2);
    });

    it('will display that new Dwolla Payment is created', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaPayment.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Pending);
        expect(response.items[0].amount).toBe('20.00');
        expect(response.items[0].amountCents).toBe(2000);
        expect(response.items[0].type).toBe(BazaDwollaPaymentType.PurchaseShares);

        DWOLLA_TRANSFER_ID = response.items[0].dwollaTransferId;

        const fetchAchResponse = await asyncExpect(
            async () => {
                const fetchAchResponse = await dataAccessDwollaPayment.fetchAchDetails({
                    dwollaTransferId: DWOLLA_TRANSFER_ID,
                });

                expect(isBazaErrorResponse(fetchAchResponse)).toBeFalsy();

                return fetchAchResponse;
            },
            null,
            { intervalMillis: 3000 },
        );

        DWOLLA_ACH_TRANSFER_ID = fetchAchResponse.achDwollaTransferId;
    });

    it('will display that valid amounts actually sent to Dwolla API', async () => {
        const response = await dataAccessE2eDwolla.getTransferValue({
            dwollaTransferId: DWOLLA_TRANSFER_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.amount).toBe('20.00');
        expect(response.amountCents).toBe(2000);
    });

    it('will NOT add funds to master account balance', async () => {
        await http.authE2eAdmin();

        await dataAccessE2eDwolla.runSandboxSimulations();

        const response = await asyncExpect(
            async () => {
                const response = await dataAccessDwollaMasterAccount.balance();

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(parseFloat(response.value)).toBe(DWOLLA_MASTER_BALANCE);

                return response;
            },
            null,
            { intervalMillis: 3000 },
        );

        DWOLLA_MASTER_BALANCE = parseFloat(response.value);
    });

    it('will display that correct NC Trade was created', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessTransactionsCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        expect(response.items[0].ncTradeId).toBe(TRADE_ID);
        expect(response.items[0].amountCents).toBe(2000);
    });

    it('will simulate dwolla completed webhook', async () => {
        const response = await dataAccessE2eDwolla.simulateWebhookEvent({
            _links: {},
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            resourceId: DWOLLA_ACH_TRANSFER_ID,
            id: DWOLLA_ACH_TRANSFER_ID,
            topic: DwollaEventTopic.bank_transfer_completed,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that Dwolla Payment is processed', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessDwollaPayment.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.items[0].amountCents).toBe(2000);
                expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Processed);
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will simulates updateTradeStatus webhook', async () => {
        const response = await dataAccessE2eNc.updateTradeStatus({
            tradeId: TRADE_ID,
            accountId: INVESTOR_ACCOUNT.northCapitalAccountId,
            orderStatus: OrderStatus.Funded,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that correct NC Trade was updated', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessTransactionsCms.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(1);

                expect(response.items[0].ncTradeId).toBe(TRADE_ID);
                expect(response.items[0].amountCents).toBe(2000);
                expect(response.items[0].ncOrderStatus).toBe(OrderStatus.Funded);
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will display that investment is successful', async () => {
        const response = await dataAccessNcTransactions.investments({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
        expect(response.items[0].lastTransactionState).toBe(TransactionState.PaymentFunded);
        expect(response.items[0].purchasedAmountTotalCents).toBe(2000);
        expect(response.items[0].purchasedShares).toBe(2);
    });
});
