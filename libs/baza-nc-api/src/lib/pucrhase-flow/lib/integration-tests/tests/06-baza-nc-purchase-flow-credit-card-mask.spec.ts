import 'reflect-metadata';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse, shiftMonth } from '@scaliolabs/baza-core-shared';
import { BazaNcE2eNodeAccess, BazaNcPurchaseFlowCreditCardNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';

jest.setTimeout(240000);

const NOW = new Date('2021-03-01');

xdescribe('@scaliolabs/baza-nc-api/purchase-flow/06-baza-nc-purchase-flow-credit-card-mask.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessCreditCard = new BazaNcPurchaseFlowCreditCardNodeAccess(http);

    const NEXT_YEAR = NOW.getFullYear() - 2000 + 2;
    const CURRENT_MONTH = shiftMonth(NOW.getMonth(), 1);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await dataAccessE2eNc.enableMaskConfig();

        await http.authE2eUser();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    afterAll(async () => {
        await dataAccessE2eNc.disableMaskConfig();
    });

    it('will correctly set credit card with delimeters', async () => {
        const response = await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.creditCardNumber).toBe('************2832');
    });

    it('will correctly returns credit card number as masked', async () => {
        const response = await dataAccessCreditCard.getCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.creditCard.creditCardNumber).toBe('************2832');
    });

    it('will correctly set credit card without delimeters', async () => {
        const response = await dataAccessCreditCard.setCreditCard({
            creditCardNumber: '4916338506082832',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.creditCardNumber).toBe('************2832');
    });

    it('will correctly returns new credit card number as masked', async () => {
        const response = await dataAccessCreditCard.getCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.creditCard.creditCardNumber).toBe('************2832');
    });
});
