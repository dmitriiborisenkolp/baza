import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, shiftMonth } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    AccountTypeCheckingSaving,
    OfferingId,
    NorthCapitalAccountId,
    TradeId,
    TransactionState,
    BazaNcPurchaseFlowTransactionType,
} from '@scaliolabs/baza-nc-shared';
import {
    BazaNcE2eNodeAccess,
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowCreditCardNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcTransactionsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { BazaNcAchBankAccountMockTranasctionStatus, BazaNcCreditCardMockTranasctionStatus } from '../fixtures';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

let NC_OFFERING_ID: OfferingId;

xdescribe('@scaliolabs/baza-nc-api/integration-tests/tests/16-baza-nc-purchase-flow-refund-and-partial-refund.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessPurchaseFlowCreditCard = new BazaNcPurchaseFlowCreditCardNodeAccess(http);
    const dataAccessTransactions = new BazaNcTransactionsNodeAccess(http);
    const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessNcE2e = new BazaNcE2eNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);

    const NEXT_YEAR = new Date().getFullYear() - 2000 + 2;
    const CURRENT_MONTH = shiftMonth(new Date().getMonth(), 1);

    let ACCOUNT_ID: NorthCapitalAccountId;
    let TRADE_ID: TradeId;
    let TRADE_ID_2: TradeId;
    let TRANSACTION_ID: number;
    let TRANSACTION_ID_2: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcApiOfferingWithFeeFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        ACCOUNT_ID = investorAccount.northCapitalAccountId;
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('has created fixture offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;

        // Sign to E2E user back
        await http.authE2eUser();
    });

    it('has 0 shares in investments', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(Array.isArray(response.items) && response.items.length === 0).toBeTruthy();
            },
            null,
            { intervalMillis: 3000 },
        );

        await asyncExpect(
            async () => {
                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will successfully purchase 1 share with bank account', async () => {
        const session = await dataAccessPurchaseFlow.session({
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            amount: 1000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const bankAccount = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: BazaNcAchBankAccountMockTranasctionStatus.ReturnedBankAccountClosed,
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();

        const submit = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();

        TRADE_ID = session.id;
    });

    it('will update transaction status to RETURNED', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessNcE2e.updateExternalFundMoveStatus({
                    accountId: ACCOUNT_ID,
                    fundMoveStatus: 'RETURNED',
                    tradeId: TRADE_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will show Returned status in transaction list', async () => {
        const transactions = await dataAccessTransactions.list({
            index: 1,
            size: 10,
        });
        expect(isBazaErrorResponse(transactions)).toBeFalsy();

        expect(Array.isArray(transactions.items) && transactions.items.length === 1).toBeTruthy();

        TRANSACTION_ID = transactions.items[0].id;
    });

    it('will request to reprocess the payment', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessPurchaseFlow.reprocessPayment({
                    id: TRANSACTION_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will show PENDING_PAYMENT status for the transaction we requested to reprocess its payment', async () => {
        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();

                expect(transactions.items[0].state).toBe(TransactionState.PendingPayment);
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will add credit card', async () => {
        const response = await dataAccessPurchaseFlowCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH,
            creditCardCvv: '123',
            creditCardholderName: BazaNcCreditCardMockTranasctionStatus.Returned,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully purchase 1 share with credit card', async () => {
        const session = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        await dataAccessPurchaseFlow.session({
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
        });

        const submit = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();

        TRADE_ID_2 = session.id;
    });

    it('will update transaction status to RETURNED', async () => {
        await asyncExpect(async () => {
            const response = await dataAccessNcE2e.updateExternalFundMoveStatus({
                accountId: ACCOUNT_ID,
                fundMoveStatus: 'RETURNED',
                tradeId: TRADE_ID_2,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
        });
    });

    it('will show Returned status in transaction list', async () => {
        const transactions = await dataAccessTransactions.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(transactions)).toBeFalsy();

        expect(Array.isArray(transactions.items) && transactions.items.length === 2).toBeTruthy();

        TRANSACTION_ID_2 = transactions.items[0].id;
    });

    it('will request to reprocess the payment', async () => {
        const response = await dataAccessPurchaseFlow.reprocessPayment({
            id: TRANSACTION_ID_2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will throw error if requested for the same reprocessed payment', async () => {
        await asyncExpect(async () => {
            const response = await dataAccessPurchaseFlow.reprocessPayment({
                id: TRANSACTION_ID_2,
            });

            expect(isBazaErrorResponse(response)).toBeTruthy();
        });
    });

    it('will successfully purchase 2 more share with credit card', async () => {
        const session = await asyncExpect(
            async () => {
                const session = await dataAccessPurchaseFlow.session({
                    numberOfShares: 2,
                    offeringId: NC_OFFERING_ID,
                    requestDocuSignUrl: false,
                    amount: 2000,
                });

                await dataAccessPurchaseFlow.session({
                    numberOfShares: 2,
                    offeringId: NC_OFFERING_ID,
                    requestDocuSignUrl: false,
                    amount: 2000,
                    transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
                });

                expect(isBazaErrorResponse(session)).toBeFalsy();

                return session;
            },
            null,
            { intervalMillis: 3000 },
        );

        await asyncExpect(
            async () => {
                const submit = await dataAccessPurchaseFlow.submit({
                    id: session.id,
                });

                expect(isBazaErrorResponse(submit)).toBeFalsy();
            },
            null,
            { intervalMillis: 3000 },
        );

        TRADE_ID = session.id;
    });

    it('will update transaction status to RETURNED', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessNcE2e.updateExternalFundMoveStatus({
                    accountId: ACCOUNT_ID,
                    fundMoveStatus: 'RETURNED',
                    tradeId: TRADE_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will show Returned status in transaction list', async () => {
        const transactions = await dataAccessTransactions.list({
            index: 1,
            size: 10,
        });
        expect(isBazaErrorResponse(transactions)).toBeFalsy();

        expect(Array.isArray(transactions.items) && transactions.items.length === 3).toBeTruthy();

        TRANSACTION_ID = transactions.items[0].id;
    });

    it('will set a new bank account', async () => {
        const bankAccount = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: BazaNcAchBankAccountMockTranasctionStatus.ReturnedInsufficientFunds,
            accountRoutingNumber: '011401533',
            accountNumber: '11115552233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();
    });

    it('will request to reprocess the payment with a new bank account and payment method', async () => {
        const response = await dataAccessPurchaseFlow.reprocessPayment({
            id: TRANSACTION_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will show PENDING_PAYMENT status for the transaction we requested to reprocess its payment', async () => {
        await asyncExpect(
            async () => {
                const transactions = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(transactions)).toBeFalsy();
                expect(transactions.items.length).toBe(3);
                expect(transactions.items[2].state).toBe(TransactionState.PendingPayment);
            },
            null,
            { intervalMillis: 3000 },
        );
    });
});
