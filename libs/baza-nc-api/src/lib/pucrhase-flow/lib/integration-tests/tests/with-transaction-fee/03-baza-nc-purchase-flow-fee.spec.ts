import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { AccountTypeCheckingSaving, convertFromCents, OfferingId } from '@scaliolabs/baza-nc-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowCreditCardNodeAccess,
    BazaNcPurchaseFlowLimitsNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcTransactionsCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../../offering';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

let NC_OFFERING_ID: OfferingId;
let NC_TRANSACTION_ID: string;
let CREDIT_CARD_FEE: number;

describe('@scaliolabs/baza-nc-api/purchase-flow/with-transaction-fee/03-baza-nc-purchase-flow-fee.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessPurchaseFlowCreditCard = new BazaNcPurchaseFlowCreditCardNodeAccess(http);
    const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessTransactionCMS = new BazaNcTransactionsCmsNodeAccess(http);
    const dataAccessLimits = new BazaNcPurchaseFlowLimitsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcApiOfferingWithFeeFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;

        expect(NC_OFFERING_ID).toBeDefined();

        await http.authE2eUser();

        await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        await dataAccessPurchaseFlowCreditCard.setCreditCard({
            creditCardExpireYear: 40,
            creditCardNumber: '5280934283171080',
            creditCardExpireMonth: 12,
            creditCardholderName: 'JOHN DOE',
            creditCardCvv: '123',
        });

        const limits = await dataAccessLimits.limits();

        CREDIT_CARD_FEE = limits.creditCardPaymentsFee;
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will have 0 fee for ACH transfers after creating trade', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();
        expect(session.fee).toBe(0);

        NC_TRANSACTION_ID = session.id;

        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const tradesResponse = await dataAccessTransactionCMS.list({
                    index: 1,
                });

                expect(isBazaErrorResponse(tradesResponse)).toBeFalsy();
                expect(tradesResponse.items[0]).toBeDefined();
                expect(tradesResponse.items[0].amountCents).toBe(5000);
                expect(tradesResponse.items[0].feeCents).toBe(0);
                expect(tradesResponse.items[0].totalCents).toBe(5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will still be 0 fee for ACH transfers after submitting trade', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: NC_TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const tradesResponse = await dataAccessTransactionCMS.list({
                    index: 1,
                });

                expect(isBazaErrorResponse(tradesResponse)).toBeFalsy();
                expect(tradesResponse.items[0]).toBeDefined();
                expect(tradesResponse.items[0].amountCents).toBe(5000);
                expect(tradesResponse.items[0].feeCents).toBe(0);
                expect(tradesResponse.items[0].totalCents).toBe(5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will have fee for CreditCard transfers after creating trade', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
        });

        const expectedFee = Math.round(convertFromCents(5000) * CREDIT_CARD_FEE);

        expect(isBazaErrorResponse(session)).toBeFalsy();
        expect(session.fee).toBe(expectedFee);

        NC_TRANSACTION_ID = session.id;

        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const tradesResponse = await dataAccessTransactionCMS.list({
                    index: 1,
                });

                expect(isBazaErrorResponse(tradesResponse)).toBeFalsy();
                expect(tradesResponse.items[0]).toBeDefined();
                expect(tradesResponse.items[0].amountCents).toBe(5000);
                expect(tradesResponse.items[0].feeCents).toBe(expectedFee);
                expect(tradesResponse.items[0].totalCents).toBe(expectedFee + 5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will still have fee after submitting CreditCard trade', async () => {
        const expectedFee = Math.round(convertFromCents(5000) * CREDIT_CARD_FEE);

        const response = await dataAccessPurchaseFlow.submit({
            id: NC_TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const tradesResponse = await dataAccessTransactionCMS.list({
                    index: 1,
                });

                expect(isBazaErrorResponse(tradesResponse)).toBeFalsy();
                expect(tradesResponse.items[0]).toBeDefined();
                expect(tradesResponse.items[0].amountCents).toBe(5000);
                expect(tradesResponse.items[0].feeCents).toBe(expectedFee);
                expect(tradesResponse.items[0].totalCents).toBe(expectedFee + 5000);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will update fee after changing transaction type from ACH to CreditCard', async () => {
        const expectedFee = Math.round(convertFromCents(5000) * CREDIT_CARD_FEE);

        const session = await dataAccessPurchaseFlow.session({
            offeringId: NC_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();
        expect(session.fee).toBe(0);

        await asyncExpect(
            async () => {
                const sessionUpdated = await dataAccessPurchaseFlow.session({
                    offeringId: NC_OFFERING_ID,
                    numberOfShares: 5,
                    amount: 5000,
                    transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
                });

                expect(isBazaErrorResponse(sessionUpdated)).toBeFalsy();
                expect(sessionUpdated.fee).toBe(expectedFee);
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
