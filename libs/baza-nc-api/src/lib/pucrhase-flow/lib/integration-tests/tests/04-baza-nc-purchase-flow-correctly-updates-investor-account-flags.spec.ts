import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, shiftMonth } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountTypeCheckingSaving } from '@scaliolabs/baza-nc-shared';
import {
    BazaNcInvestorAccountCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowCreditCardNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/purchase-flow/04-baza-nc-purchase-flow-correctly-updates-investor-account-flags.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountCmsNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessPurchaseFlowCreditCard = new BazaNcPurchaseFlowCreditCardNodeAccess(http);

    const NEXT_YEAR = new Date().getFullYear() - 2000 + 1;
    const CURRENT_MONTH = shiftMonth(new Date().getMonth(), 1);

    const getE2eUserFlags: () => Promise<{
        isPaymentMethodLinked: boolean;
        isBankAccountLinked: boolean;
        isCreditCardLinked: boolean;
    }> = async () => {
        await http.authE2eAdmin();

        const response = await dataAccessInvestorAccount.getInvestorAccount({
            id: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await http.authE2eUser();

        return {
            isPaymentMethodLinked: response.isPaymentMethodLinked,
            isBankAccountLinked: response.isBankAccountLinked,
            isCreditCardLinked: response.isCreditCardLinked,
        };
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eUser();
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display "false" from start', async () => {
        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeFalsy();
        expect(flags.isBankAccountLinked).toBeFalsy();
        expect(flags.isCreditCardLinked).toBeFalsy();
    });

    it('will display isBankAccountLinked as true, isCreditCardLinked as false and isPaymentMethodLinked as true after linking bank account', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeTruthy();
        expect(flags.isBankAccountLinked).toBeTruthy();
        expect(flags.isCreditCardLinked).toBeFalsy();

        const getResponse = await dataAccessPurchaseFlowBankAccount.getBankAccount();

        expect(isBazaErrorResponse(getResponse)).toBeFalsy();
        expect(getResponse.isAvailable).toBeTruthy();
        expect(getResponse.details.accountName).toBe('FOO BAR');
        expect(getResponse.details.accountRoutingNumber).toBe('011401533');
        expect(getResponse.details.accountNumber).toBe('1111222233330000');
        expect(getResponse.details.accountType).toBe(AccountTypeCheckingSaving.Savings);
    });

    it('will display isBankAccountLinked as false, isCreditCardLinked as false and isPaymentMethodLinked as false after unlinking bank account', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.deleteBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeFalsy();
        expect(flags.isBankAccountLinked).toBeFalsy();
        expect(flags.isCreditCardLinked).toBeFalsy();

        const getResponse = await dataAccessPurchaseFlowBankAccount.getBankAccount();

        expect(isBazaErrorResponse(getResponse)).toBeFalsy();
        expect(getResponse.isAvailable).toBeFalsy();
        expect(getResponse.details).toBeUndefined();
    });

    it('will display isBankAccountLinked as false, isCreditCardLinked as true and isPaymentMethodLinked as true after linking credit card', async () => {
        const response = await dataAccessPurchaseFlowCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeTruthy();
        expect(flags.isBankAccountLinked).toBeFalsy();
        expect(flags.isCreditCardLinked).toBeTruthy();
    });

    it('will display isBankAccountLinked as true, isCreditCardLinked as true and isPaymentMethodLinked as true after linking bank account additionally to credit card (BankAccount then Credit Card)', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeTruthy();
        expect(flags.isBankAccountLinked).toBeTruthy();
        expect(flags.isCreditCardLinked).toBeTruthy();
    });

    it('will display isBankAccountLinked as true, isCreditCardLinked as false and isPaymentMethodLinked as true after unlinking bank account (BankAccount then Credit Card)', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.deleteBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeTruthy();
        expect(flags.isBankAccountLinked).toBeFalsy();
        expect(flags.isCreditCardLinked).toBeTruthy();
    });

    it('will display isBankAccountLinked as false, isCreditCardLinked as false and isPaymentMethodLinked as false after unlinking credit card (BankAccount then Credit Card)', async () => {
        const response = await dataAccessPurchaseFlowCreditCard.deleteCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeFalsy();
        expect(flags.isBankAccountLinked).toBeFalsy();
        expect(flags.isCreditCardLinked).toBeFalsy();
    });

    it('will display isBankAccountLinked as true, isCreditCardLinked as true and isPaymentMethodLinked as true after linking bank account additionally to credit card (Credit Card then BankAccount)', async () => {
        await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        await dataAccessPurchaseFlowCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        });

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeTruthy();
        expect(flags.isBankAccountLinked).toBeTruthy();
        expect(flags.isCreditCardLinked).toBeTruthy();
    });

    it('will display isBankAccountLinked as false, isCreditCardLinked as true and isPaymentMethodLinked as true after unlinking bank account (Credit Card then BankAccount)', async () => {
        const response = await dataAccessPurchaseFlowCreditCard.deleteCreditCard();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeTruthy();
        expect(flags.isBankAccountLinked).toBeTruthy();
        expect(flags.isCreditCardLinked).toBeFalsy();
    });

    it('will display isBankAccountLinked as false, isCreditCardLinked as false and isPaymentMethodLinked as false after unlinking credit card (Credit Card then BankAccount)', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.deleteBankAccount();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const flags = await getE2eUserFlags();

        expect(flags.isPaymentMethodLinked).toBeFalsy();
        expect(flags.isBankAccountLinked).toBeFalsy();
        expect(flags.isCreditCardLinked).toBeFalsy();
    });
});
