import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { AccountTypeCheckingSaving, OfferingId, NorthCapitalAccountId, OrderStatus, TradeId } from '@scaliolabs/baza-nc-shared';
import {
    BazaNcE2eNodeAccess,
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowCreditCardNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcTransactionsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

let NC_OFFERING_ID: OfferingId;

describe('@scaliolabs/baza-nc-api/purchase-flow/15-baza-nc-purchase-flow-refund-and-partial-refund.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessPurchaseFlowCreditCard = new BazaNcPurchaseFlowCreditCardNodeAccess(http);
    const dataAccessTransactions = new BazaNcTransactionsNodeAccess(http);
    const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessNcE2e = new BazaNcE2eNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);

    let ACCOUNT_ID: NorthCapitalAccountId;
    let TRADE_ID: TradeId;
    let TRADE_ID_2: TradeId;
    let TRADE_ID_3: TradeId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcApiOfferingWithFeeFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        ACCOUNT_ID = investorAccount.northCapitalAccountId;
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('has created fixture offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;

        // Sign to E2E user back
        await http.authE2eUser();

        // Set up bank account and credit card
        await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        await dataAccessPurchaseFlowCreditCard.setCreditCard({
            creditCardExpireYear: 40,
            creditCardNumber: '5280934283171080',
            creditCardExpireMonth: 12,
            creditCardholderName: 'JOHN DOE',
            creditCardCvv: '123',
        });
    });

    it('has 0 shares in investments', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessTransactions.list({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(Array.isArray(response.items) && response.items.length === 0).toBeTruthy();

                const investments = await dataAccessTransactions.investments({
                    index: 1,
                    size: 10,
                });

                expect(isBazaErrorResponse(investments)).toBeFalsy();
                expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully purchase 1 share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            amount: 1000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const bankAccount = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();

        const submit = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();

        TRADE_ID = session.id;
    });

    it('will update trade status to UNWIND SETTLED', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessNcE2e.updateTradeStatus({
                    accountId: ACCOUNT_ID,
                    orderStatus: OrderStatus.UnwindSettled,
                    tradeId: TRADE_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will return no shares in the investments list', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
    });

    it('will change trade status to funded', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.Funded,
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will return the share with funded status in investment list with one share', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
        expect(investments.items[0].purchasedShares === 1).toBeTruthy();
    });

    it('will successfully purchase another share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            amount: 1000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const bankAccount = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();

        const submit = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();

        TRADE_ID_2 = session.id;
    });

    it('will change trade status to funded', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessNcE2e.updateTradeStatus({
                    accountId: ACCOUNT_ID,
                    orderStatus: OrderStatus.Funded,
                    tradeId: TRADE_ID_2,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will return 2 shares with funded status in investment list', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
        expect(investments.items[0].purchasedShares === 2).toBeTruthy();
    });

    it('will change the first trade status to UNWIND SETTLED', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.UnwindSettled,
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will return the 1 share with funded status in investment list', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
        expect(investments.items[0].purchasedShares === 1).toBeTruthy();
    });

    it('will change the second trade status to UNWIND SETTLED', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.UnwindSettled,
            tradeId: TRADE_ID_2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will return empty investment list', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
    });

    it('will successfully purchase 5 new shares', async () => {
        const session = await dataAccessPurchaseFlow.session({
            numberOfShares: 5,
            offeringId: NC_OFFERING_ID,
            requestDocuSignUrl: false,
            amount: 5000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const bankAccount = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();

        const submit = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();

        TRADE_ID_3 = session.id;
    });

    it('will change trade status to UNWIND SETTLED', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessNcE2e.updateTradeStatus({
                    accountId: ACCOUNT_ID,
                    orderStatus: OrderStatus.UnwindSettled,
                    tradeId: TRADE_ID_3,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will still return an empty list', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
    });

    it('will change new trade status to funded', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.Funded,
            tradeId: TRADE_ID_3,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will also change first trade status to funded', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.Funded,
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will still return investment array with 6 shares', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
        expect(investments.items[0].purchasedShares === 6).toBeTruthy();
    });

    it('will also change second trade status to funded', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.Funded,
            tradeId: TRADE_ID_2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('this will still return investment array with 7 shares', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 1).toBeTruthy();
        expect(investments.items[0].purchasedShares === 7).toBeTruthy();
    });

    it('will update all trades and refund them all', async () => {
        const response = await Promise.all([
            dataAccessNcE2e.updateTradeStatus({
                accountId: ACCOUNT_ID,
                orderStatus: OrderStatus.UnwindSettled,
                tradeId: TRADE_ID,
            }),
            dataAccessNcE2e.updateTradeStatus({
                accountId: ACCOUNT_ID,
                orderStatus: OrderStatus.UnwindSettled,
                tradeId: TRADE_ID_2,
            }),
            dataAccessNcE2e.updateTradeStatus({
                accountId: ACCOUNT_ID,
                orderStatus: OrderStatus.UnwindSettled,
                tradeId: TRADE_ID_3,
            }),
        ]);

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('this time we will get no items in investment list since all trades been refunded', async () => {
        const investments = await dataAccessTransactions.investments({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(investments)).toBeFalsy();
        expect(Array.isArray(investments.items) && investments.items.length === 0).toBeTruthy();
    });
});
