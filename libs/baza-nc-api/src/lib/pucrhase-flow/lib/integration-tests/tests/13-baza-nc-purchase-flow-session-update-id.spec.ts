import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    BazaCommonErrorCodes,
    BazaCoreE2eFixtures,
    BazaError,
    generateRandomHexString,
    isBazaErrorResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { AccountTypeCheckingSaving, OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

const http = new BazaDataAccessNode();

const dataAccessE2e = new BazaE2eNodeAccess(http);
const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

let NC_OFFERING_ID: OfferingId;
let TRANSACTION_ID: TradeId;
let UPDATE_ID_1: string;
let UPDATE_ID_2: string;

describe('@scaliolabs/baza-nc-api/purchase-flow/13-baza-nc-purchase-flow-session-update-id.spec.ts', () => {
    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch available offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;
    });

    it('will set up a bank account', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to start new session', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        UPDATE_ID_1 = response.updateId;
        TRANSACTION_ID = response.id;
    });

    it('will not allow to update trade session with invalid update id', async () => {
        const response: BazaError = (await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            updateId: generateRandomHexString(),
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaCommonErrorCodes.BazaSessionUpdateIdMismatch);
    });

    it('will allow update trade session with valid update id', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            updateId: UPDATE_ID_1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        UPDATE_ID_2 = response.updateId;
    });

    it('will now allow again to update trade session with previous update id', async () => {
        const response: BazaError = (await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            updateId: UPDATE_ID_1,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaCommonErrorCodes.BazaSessionUpdateIdMismatch);
    });

    it('will allow update trade session without update ids', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        UPDATE_ID_1 = response.updateId;
    });

    it('will not allow to submit session with invalid update id', async () => {
        const response: BazaError = (await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
            updateId: generateRandomHexString(),
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaCommonErrorCodes.BazaSessionUpdateIdMismatch);
    });

    it('will not allow to submit session with outdated update id', async () => {
        const response: BazaError = (await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
            updateId: UPDATE_ID_2,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaCommonErrorCodes.BazaSessionUpdateIdMismatch);
    });

    it('will allow to submit session with valid update id', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
            updateId: UPDATE_ID_1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
