import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDwollaE2eNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { DwollaCustomerId } from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { AccountTypeCheckingSaving, BazaNcPurchaseFlowTransactionType, OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { BazaNcAchBankAccountMockTranasctionStatus } from '../fixtures';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/purchase-flow/20-baza-nc-purchase-flow-dwolla-switch-payment-methods.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessNcDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessE2eDwolla = new BazaDwollaE2eNodeAccess(http);
    // const dataAccessDwollaCmsPayment = new BazaDwollaPaymentCmsNodeAccess(http);
    const dataAccessNcOfferings = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessNcInvestor = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessNcPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);

    let NC_OFFERING_ID: OfferingId;
    let DWOLLA_CUSTOMER_ID: DwollaCustomerId;
    // let DWOLLA_FAILED_TRANSFER_ID: DwollaTransferId;
    let TRADE_ID: TradeId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNcOfferings.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);

        NC_OFFERING_ID = response[0].ncOfferingId;
    });

    it('will touch dwolla customer for account', async () => {
        const response = await dataAccessNcDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investor = await dataAccessNcInvestor.current();

        expect(isBazaErrorResponse(investor)).toBeFalsy();
        expect(investor.dwollaCustomerId).toBeDefined();

        DWOLLA_CUSTOMER_ID = investor.dwollaCustomerId;
    });

    it('will add funds to investor', async () => {
        const response = await dataAccessE2eDwolla.transferFundsFromMasterAccount({
            dwollaCustomerId: DWOLLA_CUSTOMER_ID,
            amount: '100.00',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will set a new bank account for investor', async () => {
        const bankAccount = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: BazaNcAchBankAccountMockTranasctionStatus.ReturnedInsufficientFunds,
            accountRoutingNumber: '011401533',
            accountNumber: '11115552233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();
    });

    it('will display that no purchase flow session available yet', async () => {
        const response = await dataAccessNcPurchaseFlow.current(NC_OFFERING_ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeFalsy();
        expect(response.session).not.toBeDefined();
    });

    it('will start new purchase flow session (purchase 2 shares)', async () => {
        const response = await dataAccessNcPurchaseFlow.session({
            amount: 2000,
            numberOfShares: 2,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;
    });

    it('will successfully switch payment method to Credit Card', async () => {
        const response = await dataAccessNcPurchaseFlow.session({
            amount: 2000,
            numberOfShares: 2,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);
    });

    it('will display that payment is actually switched to credit card', async () => {
        const response = await dataAccessNcPurchaseFlow.current(NC_OFFERING_ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.session).toBeDefined();
        expect(response.session.transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);
    });

    it('will switch payment method back to Account Balance', async () => {
        const response = await dataAccessNcPurchaseFlow.session({
            amount: 2000,
            numberOfShares: 2,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.AccountBalance);
    });

    it('will display that payment is switched back to Account Balance', async () => {
        const response = await dataAccessNcPurchaseFlow.current(NC_OFFERING_ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isAvailable).toBeTruthy();
        expect(response.session).toBeDefined();
        expect(response.session.transactionType).toBe(BazaNcPurchaseFlowTransactionType.AccountBalance);
    });
});
