import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { awaitTimeout, BazaCommonErrorCodes, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    AccountTypeCheckingSaving,
    BazaNcPurchaseFlowErrorCodes,
    OfferingId,
    BazaNcPurchaseFlowTransactionType,
    TradeId,
} from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

const http = new BazaDataAccessNode();

const dataAccessE2e = new BazaE2eNodeAccess(http);
const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

let NC_OFFERING_ID: OfferingId;
let TRANSACTION_ID: TradeId;

describe('@scaliolabs/baza-nc-api/purchase-flow/12-baza-nc-purchase-flow-session-lock.spec.ts', () => {
    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch available offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;
    });

    it('will set up a bank account', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to have two session requests at once', async () => {
        const requestA = dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        await awaitTimeout(250);

        const requestB = dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        const responseA = await requestA;
        const responseB: BazaError = (await requestB) as any;

        expect(isBazaErrorResponse(responseA)).toBeFalsy();
        expect(isBazaErrorResponse(responseB)).toBeTruthy();

        expect(responseB.code).toBe(BazaCommonErrorCodes.BazaSessionIsLocked);

        TRANSACTION_ID = responseA.id;
    });

    it('will not allow to send 2 destroy requests at once', async () => {
        const requestA = dataAccessPurchaseFlow.destroy({
            offeringId: NC_OFFERING_ID,
        });

        await awaitTimeout(250);

        const requestB = dataAccessPurchaseFlow.destroy({
            offeringId: NC_OFFERING_ID,
        });

        const responseA = await requestA;
        const responseB: BazaError = (await requestB) as any;

        expect(isBazaErrorResponse(responseA)).toBeFalsy();
        expect(isBazaErrorResponse(responseB)).toBeTruthy();

        expect(responseB.code).toBe(BazaCommonErrorCodes.BazaSessionIsLocked);
    });

    it('will allow to start new session', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).not.toBe(TRANSACTION_ID);
        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.ACH);

        TRANSACTION_ID = response.id;
    });

    it('will now allow to submit while session update is in progress', async () => {
        const requestA = dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        await awaitTimeout(250);

        const requestB = dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        const responseA = await requestA;
        const responseB: BazaError = (await requestB) as any;

        expect(isBazaErrorResponse(responseA)).toBeFalsy();
        expect(isBazaErrorResponse(responseB)).toBeTruthy();

        expect(responseB.code).toBe(BazaCommonErrorCodes.BazaSessionIsLocked);
    });

    it('will not allow to submit session twice (but first attempt will be successfull)', async () => {
        const requestA = dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        await awaitTimeout(250);

        const requestB = dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        const responseA = await requestA;
        const responseB: BazaError = (await requestB) as any;

        expect(isBazaErrorResponse(responseA)).toBeFalsy();
        expect(isBazaErrorResponse(responseB)).toBeTruthy();

        expect(responseB.code).toBe(BazaCommonErrorCodes.BazaSessionIsLocked);
    });

    it('will not allow to submit same session again', async () => {
        const response: BazaError = (await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.TradeSessionNotFound);
    });
});
