import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    AccountTypeCheckingSaving,
    BazaNcPurchaseFlowErrorCodes,
    OfferingId,
    BazaNcPurchaseFlowTransactionType,
    TradeId,
} from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

const http = new BazaDataAccessNode();

const dataAccessE2e = new BazaE2eNodeAccess(http);
const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

let NC_OFFERING_ID: OfferingId;
let TRANSACTION_ID: TradeId;

describe('@scaliolabs/baza-nc-api/purchase-flow/08-baza-nc-purchase-flow-will-start-session-with-valid-type-case-a.spec.ts (Case A: ❌ Bank Account ❌ Credit Card)', () => {
    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch available offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;
    });

    it('will start session with ACH type for user with no bank account and no credit card available', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.ACH);

        TRANSACTION_ID = response.id;
    });

    it('will display that session has ACH transaction type after updating session', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(TRANSACTION_ID);
        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.ACH);
    });

    it('will successfully update session to Credit Card type for user with no bank account and no credit card available', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(TRANSACTION_ID);
        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);
    });

    it('will not reset transaction type back to ACH after updating session', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(TRANSACTION_ID);
        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);
    });

    it('will successfully update session back to ACH', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(TRANSACTION_ID);
        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.ACH);
    });

    it('will not allow to submit session because of missing bank account', async () => {
        const response: BazaError = (await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.NoBankAccount);
    });

    it('will set up a bank account', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to submit session', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to submit same session again', async () => {
        const response: BazaError = (await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.TradeSessionNotFound);
    });
});
