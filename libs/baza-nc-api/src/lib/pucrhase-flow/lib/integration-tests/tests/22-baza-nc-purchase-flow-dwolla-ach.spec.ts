import 'reflect-metadata';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
    BazaRegistryNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BazaDwollaE2eNodeAccess,
    BazaDwollaEscrowCmsNodeAccess,
    BazaDwollaMasterAccountNodeAccess,
    BazaDwollaPaymentCmsNodeAccess,
} from '@scaliolabs/baza-dwolla-node-access';
import {
    BazaNcDwollaNodeAccess,
    BazaNcE2eNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcReportCmsNodeAccess,
    BazaNcTransactionsCmsNodeAccess,
    BazaNcTransactionsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    BazaDwollaEscrowFundingSourceDto,
    BazaDwollaPaymentStatus,
    BazaDwollaPaymentType,
    DwollaCustomerId,
    DwollaEventTopic,
    DwollaFundingSourceType,
    DwollaTransferId,
} from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcInvestorAccountDto,
    BazaNcPurchaseFlowTransactionType,
    BazaNcReportStatus,
    OfferingId,
    OrderStatus,
    TradeId,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/purchase-flow/22-baza-nc-purchase-flow-dwolla-ach.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessRegistry = new BazaRegistryNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessE2eDwolla = new BazaDwollaE2eNodeAccess(http);
    const dataAccessNcDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessDwollaMasterAccount = new BazaDwollaMasterAccountNodeAccess(http);
    const dataAccessNcInvestor = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessNcOfferings = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessNcPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessNcTransactions = new BazaNcTransactionsNodeAccess(http);
    const dataAccessDwollaPayment = new BazaDwollaPaymentCmsNodeAccess(http);
    const dataAccessTransactionsCms = new BazaNcTransactionsCmsNodeAccess(http);
    const dataAccessEscrow = new BazaDwollaEscrowCmsNodeAccess(http);
    const dataAccessNcReport = new BazaNcReportCmsNodeAccess(http);

    let ULID: string;
    let TRADE_ID: TradeId;
    let DWOLLA_CUSTOMER_ID: DwollaCustomerId;
    let NC_OFFERING_ID: OfferingId;
    let DWOLLA_TRANSFER_ID: DwollaTransferId;
    let DWOLLA_TRANSFER_ACH_ID: DwollaTransferId;
    let DWOLLA_MASTER_BALANCE: number;
    let INVESTOR_ACCOUNT: BazaNcInvestorAccountDto;

    let bankFundingSource: BazaDwollaEscrowFundingSourceDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaDwolla.enableWebhookSimulations',
            value: false,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    afterAll(async () => {
        await http.authE2eAdmin();

        await dataAccessRegistry.updateSchemaRecord({
            path: 'bazaDwolla.enableWebhookSimulations',
            value: true,
        });
    });

    it('will display that at least 1 Bank Account FS is available to use', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessEscrow.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const fundSource = response.find((next) => next.type === DwollaFundingSourceType.Bank);

        expect(fundSource).toBeDefined();

        bankFundingSource = fundSource;
    });

    it('will select bank account as escrow', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessEscrow.set({
            dwollaFundingSourceId: bankFundingSource.dwollaFundingSourceId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.type).toBe(DwollaFundingSourceType.Bank);
        expect(response.isSelectedAsEscrow).toBeTruthy();
    });

    it('will successfully touches dwolla customer for account', async () => {
        const response = await dataAccessNcDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investor = await dataAccessNcInvestor.current();

        expect(isBazaErrorResponse(investor)).toBeFalsy();
        expect(investor.dwollaCustomerId).toBeDefined();

        DWOLLA_CUSTOMER_ID = investor.dwollaCustomerId;
        INVESTOR_ACCOUNT = investor;
    });

    it('will add funds to investor', async () => {
        const response = await dataAccessE2eDwolla.transferFundsFromMasterAccount({
            dwollaCustomerId: DWOLLA_CUSTOMER_ID,
            amount: '100.00',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fetch current dwolla master account balance', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaMasterAccount.balance();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        DWOLLA_MASTER_BALANCE = parseFloat(response.value);
    });

    it('will fetch offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNcOfferings.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);

        NC_OFFERING_ID = response[0].ncOfferingId;
    });

    it('will start new purchase flow session (purchase 2 shares)', async () => {
        const response = await dataAccessNcPurchaseFlow.session({
            amount: 2000,
            numberOfShares: 2,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;
    });

    it('will submit purchase flow session', async () => {
        const response = await dataAccessNcPurchaseFlow.submit({
            id: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new investment', async () => {
        const response = await dataAccessNcTransactions.investments({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].ncOffering.ncOfferingId).toBe(NC_OFFERING_ID);
        expect(response.items[0].lastTransactionState).toBe(TransactionState.PendingPayment);
        expect(response.items[0].purchasedAmountTotalCents).toBe(2000);
        expect(response.items[0].purchasedShares).toBe(2);
    });

    it('will display that trade is on CREATED status', async () => {
        const response = await dataAccessE2eNc.getTradeStatus({
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.ncOrderStatus).toBe(OrderStatus.Created);
    });

    it('will display that new Transaction is on Pending status', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessTransactionsCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].amountCents).toBe(2000);
        expect(response.items[0].ncOrderStatus).toBe(OrderStatus.Created);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        TRADE_ID = response.items[0].ncTradeId;
    });

    it('will display that new Dwolla Payment is created and it has achDwollaTransferId', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaPayment.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Pending);
        expect(response.items[0].amount).toBe('20.00');
        expect(response.items[0].amountCents).toBe(2000);
        expect(response.items[0].type).toBe(BazaDwollaPaymentType.PurchaseShares);
        expect(response.items[0].achDwollaTransferId).toBeDefined();

        DWOLLA_TRANSFER_ID = response.items[0].dwollaTransferId;
        DWOLLA_TRANSFER_ACH_ID = response.items[0].achDwollaTransferId;
    });

    it('will simulate transfer_completed webhook (both dwollaTransferId + achDwollaTransferId)', async () => {
        await dataAccessE2eDwolla.simulateWebhookEvent({
            _links: {},
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            resourceId: DWOLLA_TRANSFER_ID,
            id: DWOLLA_TRANSFER_ID,
            topic: DwollaEventTopic.transfer_completed,
        });

        await dataAccessE2eDwolla.simulateWebhookEvent({
            _links: {},
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            resourceId: DWOLLA_TRANSFER_ACH_ID,
            id: DWOLLA_TRANSFER_ACH_ID,
            topic: DwollaEventTopic.transfer_completed,
        });
    });

    it('will not perform any changes on dwolla payment status with transfer_* dwolla webhooks family', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessDwollaPayment.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(1);
                expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Pending);
                expect(response.items[0].amount).toBe('20.00');
                expect(response.items[0].amountCents).toBe(2000);
                expect(response.items[0].type).toBe(BazaDwollaPaymentType.PurchaseShares);
                expect(response.items[0].achDwollaTransferId).toBeDefined();
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will simulate bank_transfer_completed webhook (but with dwollaTransferId instead of)', async () => {
        await dataAccessE2eDwolla.simulateWebhookEvent({
            _links: {},
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            resourceId: DWOLLA_TRANSFER_ID,
            id: DWOLLA_TRANSFER_ID,
            topic: DwollaEventTopic.bank_transfer_completed,
        });
    });

    it('will not perform any changes on dwolla payment status because of using invalid dwollaTransferId', async () => {
        await http.authE2eAdmin();

        const response = await asyncExpect(
            async () => {
                const response = await dataAccessDwollaPayment.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(1);
                expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Pending);
                expect(response.items[0].amount).toBe('20.00');
                expect(response.items[0].amountCents).toBe(2000);
                expect(response.items[0].type).toBe(BazaDwollaPaymentType.PurchaseShares);
                expect(response.items[0].achDwollaTransferId).toBeDefined();

                return response;
            },
            null,
            { intervalMillis: 3000 },
        );

        ULID = response.items[0].ulid;
    });

    it('will display that Transaction is still on Pending status', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessTransactionsCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].amountCents).toBe(2000);
        expect(response.items[0].ncOrderStatus).toBe(OrderStatus.Created);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);
    });

    it('will display that trade is sill on CREATED status', async () => {
        const response = await dataAccessE2eNc.getTradeStatus({
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.ncOrderStatus).toBe(OrderStatus.Created);
    });

    it('will sync payment status using Dwolla Payments Sync tool', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaPayment.sync({
            ulid: ULID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.achDwollaTransferId).toBeDefined();
        expect(response.status).toBe(BazaDwollaPaymentStatus.Pending);

        DWOLLA_TRANSFER_ACH_ID = response.achDwollaTransferId;
    });

    it('will display that there is a Report created for Dwolla Payment with Pending status', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessNcReport.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will simulate bank_transfer_completed webhook (with correct achDwollaId)', async () => {
        await dataAccessE2eDwolla.simulateWebhookEvent({
            _links: {},
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            resourceId: DWOLLA_TRANSFER_ACH_ID,
            id: DWOLLA_TRANSFER_ACH_ID,
            topic: DwollaEventTopic.bank_transfer_completed,
        });
    });

    it('will display that Dwolla Payment is on Processed status', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessDwollaPayment.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(1);
                expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Processed);
                expect(response.items[0].amount).toBe('20.00');
                expect(response.items[0].amountCents).toBe(2000);
                expect(response.items[0].type).toBe(BazaDwollaPaymentType.PurchaseShares);
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will display that trade is updated to FUNDED status', async () => {
        const response = await dataAccessE2eNc.getTradeStatus({
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.ncOrderStatus).toBe(OrderStatus.Funded);
    });

    it('will remain current dwolla master account balance same as before', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaMasterAccount.balance();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(parseFloat(response.value)).toBe(DWOLLA_MASTER_BALANCE);
    });
});
