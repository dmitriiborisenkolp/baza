import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDwollaE2eNodeAccess, BazaDwollaPaymentCmsNodeAccess } from '@scaliolabs/baza-dwolla-node-access';
import { BazaDwollaPaymentStatus, DwollaCustomerId, DwollaEventTopic, DwollaTransferId } from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { AccountTypeCheckingSaving, BazaNcPurchaseFlowTransactionType, OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { BazaNcAchBankAccountMockTranasctionStatus } from '../fixtures';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/purchase-flow/19-baza-nc-purchase-flow-dwolla-transaction-notification.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessNcDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessE2eDwolla = new BazaDwollaE2eNodeAccess(http);
    const dataAccessDwollaCmsPayment = new BazaDwollaPaymentCmsNodeAccess(http);
    const dataAccessNcOfferings = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessNcInvestor = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessNcPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);

    let NC_OFFERING_ID: OfferingId;
    let DWOLLA_CUSTOMER_ID: DwollaCustomerId;
    let DWOLLA_FAILED_TRANSFER_ID: DwollaTransferId;
    let TRADE_ID: TradeId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessNcOfferings.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);

        NC_OFFERING_ID = response[0].ncOfferingId;
    });

    it('will touch dwolla customer for account', async () => {
        const response = await dataAccessNcDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investor = await dataAccessNcInvestor.current();

        expect(isBazaErrorResponse(investor)).toBeFalsy();
        expect(investor.dwollaCustomerId).toBeDefined();

        DWOLLA_CUSTOMER_ID = investor.dwollaCustomerId;
    });

    it('will add funds to investor', async () => {
        const response = await dataAccessE2eDwolla.transferFundsFromMasterAccount({
            dwollaCustomerId: DWOLLA_CUSTOMER_ID,
            amount: '100.00',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will set a new bank account for investor', async () => {
        const bankAccount = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: BazaNcAchBankAccountMockTranasctionStatus.ReturnedInsufficientFunds,
            accountRoutingNumber: '011401533',
            accountNumber: '11115552233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();
    });

    it('will start new purchase flow session (purchase 2 shares)', async () => {
        const response = await dataAccessNcPurchaseFlow.session({
            amount: 2000,
            numberOfShares: 2,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;
    });

    it('will submit purchase flow session', async () => {
        const response = await dataAccessNcPurchaseFlow.submit({
            id: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fetch user payment', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaCmsPayment.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);

        expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Pending);

        DWOLLA_FAILED_TRANSFER_ID = response.items[0].dwollaTransferId;
    });

    it('will simulate dwolla transfer_failed webhook', async () => {
        const response = await dataAccessE2eDwolla.simulateWebhookEvent({
            _links: {},
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            resourceId: DWOLLA_FAILED_TRANSFER_ID,
            id: DWOLLA_FAILED_TRANSFER_ID,
            topic: DwollaEventTopic.transfer_failed,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fetch user payment and the status will be "Failed"', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessDwollaCmsPayment.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);

                expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Failed);
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will notify investor and client about failed purchase', async () => {
        await asyncExpect(async () => {
            const response = await dataAccessE2e.mailbox();

            expect(response.length).toBe(1);

            expect(response[0].to).toEqual(['scalio-admin@scal.io', 'e2e@scal.io']);
            expect(response[0].subject).toContain('Payment Failed');
            expect(response[0].messageBodyText).toContain('payment was not processed successfully');
            expect(response[0].messageBodyHtml).toContain('payment was not processed successfully');
        });
    });
});
