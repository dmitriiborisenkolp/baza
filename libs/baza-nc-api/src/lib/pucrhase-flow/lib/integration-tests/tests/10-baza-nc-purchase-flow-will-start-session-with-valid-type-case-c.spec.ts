import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowCreditCardNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse, shiftMonth } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import { BazaNcPurchaseFlowErrorCodes, OfferingId, BazaNcPurchaseFlowTransactionType, TradeId } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

const http = new BazaDataAccessNode();

const dataAccessE2e = new BazaE2eNodeAccess(http);
const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
const dataAccessPurchaseFlowCreditCard = new BazaNcPurchaseFlowCreditCardNodeAccess(http);
const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

let NC_OFFERING_ID: OfferingId;
let TRANSACTION_ID: TradeId;

describe('@scaliolabs/baza-nc-api/purchase-flow/10-baza-nc-purchase-flow-will-start-session-with-valid-type-case-c.spec.ts (Case A: ❌ Bank Account ✅ Credit Card)', () => {
    const NEXT_YEAR = new Date().getFullYear() - 2000 + 2;
    const CURRENT_MONTH = shiftMonth(new Date().getMonth(), 1);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will successfully set up credit card', async () => {
        const response = await dataAccessPurchaseFlowCreditCard.setCreditCard({
            creditCardNumber: '4916 3385 0608 2832',
            creditCardExpireYear: NEXT_YEAR,
            creditCardExpireMonth: CURRENT_MONTH,
            creditCardCvv: '123',
            creditCardholderName: 'John Doe',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fetch available offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;
    });

    it('will start session with Credit Card type for user with no bank account and no credit card available', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);

        TRANSACTION_ID = response.id;
    });

    it('will display that session has Credit Card transaction type after updating session', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(TRANSACTION_ID);
        expect(response.transactionType).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);
    });

    it('will allow to submit session', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to submit same session again', async () => {
        const response: BazaError = (await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcPurchaseFlowErrorCodes.TradeSessionNotFound);
    });
});
