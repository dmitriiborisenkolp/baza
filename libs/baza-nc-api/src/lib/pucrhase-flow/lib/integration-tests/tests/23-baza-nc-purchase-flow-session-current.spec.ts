import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { awaitTimeout, BazaCommonErrorCodes, BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    AccountTypeCheckingSaving,
    BazaNcPurchaseFlowErrorCodes,
    OfferingId,
    BazaNcPurchaseFlowTransactionType,
    TradeId,
} from '@scaliolabs/baza-nc-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

const http = new BazaDataAccessNode();

const dataAccessE2e = new BazaE2eNodeAccess(http);
const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
const dataAccessOfferingCMS = new BazaNcOfferingCmsNodeAccess(http);

let NC_OFFERING_ID: OfferingId;
let TRANSACTION_ID: TradeId;
let TTL: string;

describe('@scaliolabs/baza-nc-api/purchase-flow/23-baza-nc-purchase-flow-session-current.spec.ts', () => {
    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch available offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCMS.list({
            index: 1,
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response.items) && response.items.length === 1).toBeTruthy();

        NC_OFFERING_ID = response.items[0].ncOfferingId;
    });

    it('will set up a bank account', async () => {
        const response = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will returns that session is not available yet', async () => {
        const response = await dataAccessPurchaseFlow.current(NC_OFFERING_ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.session).not.toBeDefined();
        expect(response.isAvailable).toBeFalsy();
    });

    it('will start purchase flow session', async () => {
        const response = await dataAccessPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TTL = response.willBeActiveUpTo;
        TRANSACTION_ID = response.id;
    });

    it('will run current endpoint and it will update trade session TTL', async () => {
        await asyncExpect(async () => {
            const response = await dataAccessPurchaseFlow.current(NC_OFFERING_ID);

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect(response.session).toBeDefined();
            expect(response.isAvailable).toBeTruthy();
            expect(new Date(response.session.willBeActiveUpTo).getTime()).toBeGreaterThan(new Date(TTL).getTime());
        });
    });

    it('will finish purchase', async () => {
        const response = await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will returns that session is not available anymore', async () => {
        const response = await dataAccessPurchaseFlow.current(NC_OFFERING_ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.session).not.toBeDefined();
        expect(response.isAvailable).toBeFalsy();
    });
});
