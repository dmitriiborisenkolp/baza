import { BazaNcCreditCardDto, bazaNcApiConfig, PurchaseFlowBankAccountDto } from '@scaliolabs/baza-nc-shared';
import { maskString } from '@scaliolabs/baza-core-shared';

const MAX_CHARACTERS_ACCOUNT_NUMBER = 4;
const MAX_CHARACTERS_ROUTING_NUMBER = 4;
const MAX_CHARACTERS_CREDIT_CARD_NUMBER = 4;

export function bazaNcPurchaseFlowHelperBankDetails(dto: PurchaseFlowBankAccountDto): PurchaseFlowBankAccountDto {
    if (!dto) {
        return undefined;
    }

    if (bazaNcApiConfig().maskBankDetailsFields && dto.details) {
        dto.details.accountNumber = maskString(dto.details.accountNumber, {
            regex: /\d/g,
            numCharacters: MAX_CHARACTERS_ACCOUNT_NUMBER,
        });

        dto.details.accountRoutingNumber = maskString(dto.details.accountRoutingNumber, {
            regex: /\d/g,
            numCharacters: MAX_CHARACTERS_ROUTING_NUMBER,
        });
    }

    if (dto.details) {
        dto.details.accountName = dto.details.accountName.toUpperCase();
        dto.details.accountNickName = dto.details.accountNickName.toUpperCase();
    }

    return dto;
}

export function bazaNcPurchaseFlowHelperCreditCard(dto: BazaNcCreditCardDto): BazaNcCreditCardDto {
    if (!dto) {
        return undefined;
    }

    if (bazaNcApiConfig().maskCreditCardFields) {
        dto.creditCardNumber = maskString(dto.creditCardNumber, {
            regex: /\d/g,
            numCharacters: MAX_CHARACTERS_CREDIT_CARD_NUMBER,
        });
    }

    return dto;
}
