import { BazaEnvironments } from '@scaliolabs/baza-core-shared';

export class PurchaseFlowApiModuleConfiguration {
    tradesTTLSec: number;
    disableDocuSignForEnvironments: Array<BazaEnvironments>;
}

export const PURCHASE_FLOW_API_MODULE_CONFIG = Symbol();
