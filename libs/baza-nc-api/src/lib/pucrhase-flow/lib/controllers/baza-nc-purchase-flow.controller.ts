import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { AccountEntity, CorrelationId, IpAddress, ReqAccount, BazaLogger } from '@scaliolabs/baza-core-api';
import { AuthGuard } from '@scaliolabs/baza-core-api';
import {
    BazaNcPurchaseFlowEndpoint,
    BazaNcPurchaseFlowEndpointPaths,
    DestroySessionDto,
    OfferingId,
    PurchaseFlowCurrentDto,
    PurchaseFlowDestroyResponse,
    PurchaseFlowDto,
    PurchaseFlowPersonalInfoDto,
    PurchaseFlowSessionDto,
    PurchaseFlowSubmitResponse,
    ReProcessPaymentDto,
    ReProcessPaymentResponse,
    StatsDto,
    StatsRequestDto,
    SubmitPurchaseFlowDto,
    ValidateResponseDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaNorthCapitalOpenApi } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowSessionService } from '../services/baza-nc-purchase-flow-session.service';
import { BazaNcPurchaseFlowPersonalInformationService } from '../services/baza-nc-purchase-flow-personal-information.service';
import { BazaNcPurchaseFlowStatsService } from '../services/baza-nc-purchase-flow-stats.service';
import { BazaNcHasInvestorAccountGuard, BazaNcInvestorAccountService } from '../../../investor-account';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowSessionValidatorService } from '../services/baza-nc-purchase-flow-session-validator.service';
import { BazaNcPurchaseFlowReprocessService } from '../services/baza-nc-purchase-flow-reprocess.service';
import { BazaNcPurchaseFlowTradesSessionService } from '../services/baza-nc-purchase-flow-trades-session.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalPurchaseFlow)
@UseGuards(AuthGuard, BazaNcHasInvestorAccountGuard)
@ApiBearerAuth()
export class BazaNcPurchaseFlowController implements BazaNcPurchaseFlowEndpoint {
    constructor(
        private readonly investorAccount: BazaNcInvestorAccountService,
        private readonly purchaseFlowSession: BazaNcPurchaseFlowSessionService,
        private readonly purchaseFlowReprocess: BazaNcPurchaseFlowReprocessService,
        private readonly purchaseFlowStats: BazaNcPurchaseFlowStatsService,
        private readonly purchaseFloPersonalInformation: BazaNcPurchaseFlowPersonalInformationService,
        private readonly purchaseFlowSessionValidator: BazaNcPurchaseFlowSessionValidatorService,
        private readonly purchaseFlowTradeSession: BazaNcPurchaseFlowTradesSessionService,
        private readonly logger: BazaLogger,
    ) {}

    @Get(BazaNcPurchaseFlowEndpointPaths.current)
    @ApiOperation({
        summary: 'current',
        description:
            'Returns current status of Purchase Flow and Purchase Flow DTO, if session is started. Also updates TTL of session, if available.',
    })
    @ApiParam({
        name: 'offeringId',
        description:
            'Offering ID. Purchase Flow Sessions could be created for as many offerings as possible,' +
            'but for each Offering only 1 Purchase Flow session will be allowed',
    })
    @ApiOkResponse({
        type: PurchaseFlowCurrentDto,
    })
    async current(@Param('offeringId') offeringId: OfferingId, @ReqAccount() account: AccountEntity): Promise<PurchaseFlowCurrentDto> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        const response = await this.purchaseFlowSession.current(investorAccount, offeringId);

        if (response.isAvailable) {
            await this.purchaseFlowTradeSession.touchSessionForTrade(response.session.id);

            return this.purchaseFlowSession.current(investorAccount, offeringId);
        } else {
            return {
                isAvailable: false,
            };
        }
    }

    @Post(BazaNcPurchaseFlowEndpointPaths.stats)
    @ApiOperation({
        summary: 'stats',
        description: 'Stats for Purchase Flow',
    })
    @ApiOkResponse({
        type: StatsDto,
    })
    async stats(@Body() request: StatsRequestDto, @ReqAccount() account: AccountEntity): Promise<StatsDto> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        return this.purchaseFlowStats.stats(investorAccount, request);
    }

    @Post(BazaNcPurchaseFlowEndpointPaths.validate)
    @ApiOperation({
        summary: 'validate',
        description: 'Validates session request before submitting it to North Capital',
    })
    @ApiOkResponse({
        type: PurchaseFlowDto,
    })
    async validate(@Body() request: PurchaseFlowSessionDto, @ReqAccount() account: AccountEntity): Promise<ValidateResponseDto> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        try {
            const stats = await this.purchaseFlowSessionValidator.validate(investorAccount, request);

            return {
                success: true,
                errorCode: undefined,
                mayPurchase: stats.canPurchase,
            };
        } catch (err) {
            if (err instanceof BazaNcPurchaseFlowException) {
                return {
                    success: false,
                    errorCode: err.dto.errorCode as any,
                    message: err.dto.message,
                    mayPurchase: err.dto.mayPurchase,
                };
            } else {
                throw err;
            }
        }
    }

    @Post(BazaNcPurchaseFlowEndpointPaths.session)
    @ApiOperation({
        summary: 'session',
        description: 'Returns existing session or creates a new purchase flow session',
    })
    @ApiOkResponse({
        type: PurchaseFlowDto,
    })
    async session(
        @Body() request: PurchaseFlowSessionDto,
        @IpAddress() ip: string,
        @ReqAccount() account: AccountEntity,
        @CorrelationId() correlationId: string,
    ): Promise<PurchaseFlowDto> {
        this.logger.info(`[BazaNcPurchaseFlowController.session]`, {
            user: account.id,
            correlationId,
            request,
        });

        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        return this.purchaseFlowSession.session(ip, investorAccount, request, correlationId);
    }

    @Post(BazaNcPurchaseFlowEndpointPaths.submit)
    @ApiOperation({
        summary: 'submit',
        description: 'Submits (Finishes) Purchase. Current Purchase Flow session will be destroyed on successful Submit.',
    })
    @ApiOkResponse()
    async submit(
        @Body() request: SubmitPurchaseFlowDto,
        @IpAddress() ip: string,
        @ReqAccount() account: AccountEntity,
    ): Promise<PurchaseFlowSubmitResponse> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        await this.purchaseFlowSession.submit(ip, investorAccount, request);

        return {
            success: true,
        };
    }

    @Post(BazaNcPurchaseFlowEndpointPaths.reprocessPayment)
    @ApiOperation({
        summary: 'reprocessPayment',
        description: 'Reprocesses Failed Payment',
    })
    @ApiOkResponse()
    async reprocessPayment(
        @Body() request: ReProcessPaymentDto,
        @IpAddress() ip: string,
        @ReqAccount() account: AccountEntity,
    ): Promise<ReProcessPaymentResponse> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        await this.purchaseFlowReprocess.reprocess(ip, investorAccount, request);

        return {
            success: true,
        };
    }

    @Post(BazaNcPurchaseFlowEndpointPaths.destroy)
    @ApiOperation({
        summary: 'destroy',
        description: 'Destroys all existing Purchase Flow Sessions with given parameters',
    })
    @ApiOkResponse({
        type: PurchaseFlowDto,
    })
    async destroy(@Body() request: DestroySessionDto, @ReqAccount() account: AccountEntity): Promise<PurchaseFlowDestroyResponse> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        await this.purchaseFlowSession.destroy(investorAccount, request);

        return {
            success: true,
        };
    }

    @Get(BazaNcPurchaseFlowEndpointPaths.getPersonalInfo)
    @ApiOperation({
        summary: 'getPersonalInfo',
        description: 'Helper method which returns data for Personal Information block',
    })
    @ApiOkResponse({
        type: PurchaseFlowPersonalInfoDto,
    })
    async getPersonalInfo(@ReqAccount() account: AccountEntity): Promise<PurchaseFlowPersonalInfoDto> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        return this.purchaseFloPersonalInformation.getPersonalInfo(investorAccount);
    }
}
