import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaNorthCapitalOpenApi } from '@scaliolabs/baza-nc-shared';
import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
    BazaNcPurchaseFlowBankAccountEndpointPaths,
    PurchaseFlowBankAccountDto,
    PurchaseFlowPlaidLinkDto,
    PurchaseFlowSetBankAccountDetailsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowNoNcAccountException } from '../exceptions/baza-nc-purchase-flow-no-nc-account.exception';
import { BazaNcPurchaseFlowBankAccountEndpoint } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowBankAccountService } from '../services/baza-nc-purchase-flow-bank-account.service';
import { AccountEntity, AuthGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import { BazaNcPurchaseFlowPlaidService } from '../services/baza-nc-purchase-flow-plaid.service';
import { BazaNcHasInvestorAccountGuard, BazaNcInvestorAccountService } from '../../../investor-account';
import { bazaNcPurchaseFlowHelperBankDetails } from '../helpers/baza-nc-purchase-flow.helper';

/**
 * Usage of the controller is deprecated. We're keeping it for backward compatibility reasons only
 */
@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalPurchaseFlowBankAccount)
@UseGuards(AuthGuard)
export class BazaNcPurchaseFlowBankAccountController implements BazaNcPurchaseFlowBankAccountEndpoint {
    constructor(
        private readonly investorAccount: BazaNcInvestorAccountService,
        private readonly bankAccountService: BazaNcPurchaseFlowBankAccountService,
        private readonly plaidService: BazaNcPurchaseFlowPlaidService,
    ) {}

    @Get(BazaNcPurchaseFlowBankAccountEndpointPaths.getPlaidLink)
    @UseGuards(BazaNcHasInvestorAccountGuard)
    @ApiOperation({
        summary: 'getPlaidLink [DEPRECATED]',
        description: 'Returns link to register bank account with Plaid',
        deprecated: true,
    })
    @ApiOkResponse({
        type: PurchaseFlowPlaidLinkDto,
    })
    /** @deprecated */
    async getPlaidLink(@ReqAccount() account: AccountEntity): Promise<PurchaseFlowPlaidLinkDto> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        if (!investorAccount.northCapitalAccountId) {
            throw new BazaNcPurchaseFlowNoNcAccountException();
        }

        return this.plaidService.getPlaidLink(investorAccount);
    }

    @Get(BazaNcPurchaseFlowBankAccountEndpointPaths.getBankAccount)
    @ApiOperation({
        summary: 'getBankAccount [DEPRECATED]',
        description: 'Returns bank account details fetched with Plaid',
        deprecated: true,
    })
    @ApiOkResponse({
        type: PurchaseFlowBankAccountDto,
    })
    /** @deprecated */
    async getBankAccount(@ReqAccount() account: AccountEntity): Promise<PurchaseFlowBankAccountDto> {
        const investorAccount = await this.investorAccount.findInvestorAccountByUserId(account.id);

        if (!investorAccount) {
            return {
                isAvailable: false,
            };
        }

        if (!investorAccount.northCapitalAccountId) {
            return {
                isAvailable: false,
            };
        }

        const bankAccount = await this.bankAccountService.getBankAccount(investorAccount);

        return bazaNcPurchaseFlowHelperBankDetails(bankAccount);
    }

    @Post(BazaNcPurchaseFlowBankAccountEndpointPaths.setBankAccount)
    @UseGuards(BazaNcHasInvestorAccountGuard)
    @ApiOperation({
        summary: 'setBankAccount [DEPRECATED]',
        description: 'Manual setup bank account details',
        deprecated: true,
    })
    @ApiOkResponse({
        type: PurchaseFlowBankAccountDto,
    })
    /** @deprecated */
    async setBankAccount(
        @Body() request: PurchaseFlowSetBankAccountDetailsDto,
        @ReqAccount() account: AccountEntity,
    ): Promise<PurchaseFlowBankAccountDto> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        if (!investorAccount.northCapitalAccountId) {
            throw new BazaNcPurchaseFlowNoNcAccountException();
        }

        const bankAccount = await this.bankAccountService.setBankAccount(investorAccount, request);

        return bazaNcPurchaseFlowHelperBankDetails(bankAccount);
    }

    @Post(BazaNcPurchaseFlowBankAccountEndpointPaths.deleteBankAccount)
    @UseGuards(BazaNcHasInvestorAccountGuard)
    @ApiOperation({
        summary: 'deleteBankAccount [DEPRECATED]',
        description: 'Delete linked bank account',
        deprecated: true,
    })
    @ApiOkResponse()
    /** @deprecated */
    async deleteBankAccount(@ReqAccount() account: AccountEntity): Promise<void> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        await this.bankAccountService.deleteBankAccount(investorAccount);
    }
}
