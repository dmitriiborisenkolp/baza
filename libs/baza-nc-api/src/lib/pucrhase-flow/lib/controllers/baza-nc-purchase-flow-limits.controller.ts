import { Body, Controller, Post } from '@nestjs/common';
import {
    BazaNcLimitsDto,
    BazaNcPurchaseFlowLimitsEndpoint,
    BazaNcPurchaseFlowLimitsEndpointPaths,
    BazaNcPurchaseFlowLimitsForPurchaseRequest,
    BazaNcPurchaseFlowLimitsForPurchaseResponse,
    PurchaseFlowLimitsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowLimitsService } from '../services/baza-nc-purchase-flow-limits.service';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaNorthCapitalOpenApi } from '@scaliolabs/baza-nc-shared';

@Controller()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalPurchaseFlowCreditCard)
export class BazaNcPurchaseFlowLimitsController implements BazaNcPurchaseFlowLimitsEndpoint {
    constructor(private readonly service: BazaNcPurchaseFlowLimitsService) {}

    @Post(BazaNcPurchaseFlowLimitsEndpointPaths.limits)
    @ApiOperation({
        summary: 'limits',
        description: 'Returns current limits for transactions',
    })
    @ApiOkResponse({
        type: BazaNcLimitsDto,
    })
    async limits(): Promise<BazaNcLimitsDto> {
        return this.service.limits();
    }

    @Post(BazaNcPurchaseFlowLimitsEndpointPaths.limitsForPurchase)
    @ApiOperation({
        summary: 'limitsForPurchase',
        description: 'Returns lists of transaction types with fees calculation and flag which indicated can transaction can be performed',
    })
    @ApiOkResponse({
        type: PurchaseFlowLimitsDto,
        isArray: true,
    })
    async limitsForPurchase(
        @Body() request: BazaNcPurchaseFlowLimitsForPurchaseRequest,
    ): Promise<BazaNcPurchaseFlowLimitsForPurchaseResponse> {
        return this.service.limitsForPurchaseByOfferingId(request.requestedAmountCents, request.offeringId);
    }
}
