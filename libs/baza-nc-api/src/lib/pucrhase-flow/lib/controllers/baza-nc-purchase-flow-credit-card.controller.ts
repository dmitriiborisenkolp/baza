import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaNorthCapitalOpenApi } from '@scaliolabs/baza-nc-shared';
import { AccountEntity, AuthGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import {
    BazaNcCreditCardDto,
    BazaNcPurchaseFlowCreditCardEndpoint,
    BazaNcPurchaseFlowCreditCardEndpointPaths,
    BazaNcPurchaseFlowDeleteCreditCardResponse,
    BazaNcPurchaseFlowGetCreditCardResponse,
    BazaNcPurchaseFlowSetCreditCardRequest,
    BazaNcPurchaseFlowValidateCreditCardRequest,
    BazaNcPurchaseFlowValidateCreditCardResponse,
    bazaNcPurchaseFlowCreditCardTypes,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowCreditCardService } from '../services/baza-nc-purchase-flow-credit-card.service';
import { bazaCreditCardDetect } from '@scaliolabs/baza-core-shared';
import { IpAddress } from '@scaliolabs/baza-core-api';
import { BazaNcHasInvestorAccountGuard, BazaNcInvestorAccountService } from '../../../investor-account';
import { bazaNcPurchaseFlowHelperCreditCard } from '../helpers/baza-nc-purchase-flow.helper';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalPurchaseFlowCreditCard)
@UseGuards(AuthGuard)
export class BazaNcPurchaseFlowCreditCardController implements BazaNcPurchaseFlowCreditCardEndpoint {
    constructor(
        private readonly investorAccount: BazaNcInvestorAccountService,
        private readonly service: BazaNcPurchaseFlowCreditCardService,
    ) {}

    @Post(BazaNcPurchaseFlowCreditCardEndpointPaths.setCreditCard)
    @UseGuards(BazaNcHasInvestorAccountGuard)
    @ApiOperation({
        summary: 'setCreditCard',
        description: 'Set or update credit card',
        deprecated: true,
    })
    @ApiOkResponse({
        type: BazaNcCreditCardDto,
    })
    async setCreditCard(
        @Body() request: BazaNcPurchaseFlowSetCreditCardRequest,
        @IpAddress() ip: string,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcCreditCardDto> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        return bazaNcPurchaseFlowHelperCreditCard(await this.service.setCreditCard(ip, investorAccount, request));
    }

    @Post(BazaNcPurchaseFlowCreditCardEndpointPaths.getCreditCard)
    @ApiOperation({
        summary: 'getCreditCard',
        description: 'Return credit card details (if exists)',
        deprecated: true,
    })
    @ApiOkResponse({
        type: BazaNcPurchaseFlowGetCreditCardResponse,
    })
    async getCreditCard(@ReqAccount() account: AccountEntity): Promise<BazaNcPurchaseFlowGetCreditCardResponse> {
        const investorAccount = await this.investorAccount.findInvestorAccountByUserId(account.id);

        if (!investorAccount) {
            return {
                isAvailable: false,
            };
        }

        const creditCard = bazaNcPurchaseFlowHelperCreditCard(await this.service.getCreditCard(investorAccount));

        if (creditCard) {
            return {
                isAvailable: true,
                creditCard,
            };
        } else {
            return {
                isAvailable: false,
            };
        }
    }

    @Post(BazaNcPurchaseFlowCreditCardEndpointPaths.deleteCreditCard)
    @UseGuards(BazaNcHasInvestorAccountGuard)
    @ApiOperation({
        summary: 'deleteCreditCard',
        description: 'Delete credit card (if exists)',
        deprecated: true,
    })
    @ApiOkResponse({
        type: BazaNcPurchaseFlowDeleteCreditCardResponse,
    })
    async deleteCreditCard(
        @IpAddress() ip: string,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcPurchaseFlowDeleteCreditCardResponse> {
        const investorAccount = await this.investorAccount.getInvestorAccountByUserId(account.id);

        const isDeleted = await this.service.deleteCreditCard(ip, investorAccount);

        return {
            isDeleted,
        };
    }

    @Post(BazaNcPurchaseFlowCreditCardEndpointPaths.validateCreditCard)
    @ApiOperation({
        summary: 'validateCreditCard',
        description: 'Validate credit card details',
        deprecated: true,
    })
    @ApiOkResponse({
        type: BazaNcPurchaseFlowValidateCreditCardResponse,
    })
    async validateCreditCard(
        @Body() request: BazaNcPurchaseFlowValidateCreditCardRequest,
    ): Promise<BazaNcPurchaseFlowValidateCreditCardResponse> {
        const creditCard = bazaCreditCardDetect(request.creditCardNumber);

        if (creditCard && creditCard.isCreditCardNumberValid) {
            const def = bazaNcPurchaseFlowCreditCardTypes().find((d) => d.baza === creditCard.type);

            return {
                isValid: !!def,
                creditCardType: creditCard.type,
            };
        } else {
            return {
                isValid: false,
            };
        }
    }
}
