import { BazaNcPurchaseFlowController } from './controllers/baza-nc-purchase-flow.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaNcPurchaseFlowTradesSessionService } from './services/baza-nc-purchase-flow-trades-session.service';
import { BazaNcPurchaseFlowStatsService } from './services/baza-nc-purchase-flow-stats.service';
import { BazaNcPurchaseFlowTradesSessionWatcher } from './watchers/baza-nc-purchase-flow-trades-session.watcher';
import { BazaNcPurchaseFlowFailedTradesWatcher } from './watchers/baza-nc-purchase-flow-failed-trades.watcher';
import { DynamicModule, Global, Module, OnApplicationBootstrap, OnApplicationShutdown } from '@nestjs/common';
import { PURCHASE_FLOW_API_MODULE_CONFIG, PurchaseFlowApiModuleConfiguration } from './baza-nc-purchase-flow-api.config';
import { BazaNcPurchaseFlowTradeSubmittedEventHandler } from './event-handlers/baza-nc-purchase-flow-trade-submitted.event-handler';
import { BazaNcPurchaseFlowSessionService } from './services/baza-nc-purchase-flow-session.service';
import { BazaNcPurchaseFlowPlaidService } from './services/baza-nc-purchase-flow-plaid.service';
import { BazaNcPurchaseFlowPersonalInformationService } from './services/baza-nc-purchase-flow-personal-information.service';
import { BazaNcPurchaseFlowBankAccountService } from './services/baza-nc-purchase-flow-bank-account.service';
import { BazaNcPurchaseFlowDocusignService } from './services/baza-nc-purchase-flow-docusign.service';
import { BazaNcPurchaseFlowBankAccountController } from './controllers/baza-nc-purchase-flow-bank-account.controller';
import { BazaCoreSessionLockApiModule, BazaCreditCardApiModule, EnvService } from '@scaliolabs/baza-core-api';
import { BazaNcPurchaseFlowCreditCardController } from './controllers/baza-nc-purchase-flow-credit-card.controller';
import { BazaNcPurchaseFlowCreditCardService } from './services/baza-nc-purchase-flow-credit-card.service';
import { BazaNcPurchaseFlowLimitsService } from './services/baza-nc-purchase-flow-limits.service';
import { BazaNcPurchaseFlowLimitsController } from './controllers/baza-nc-purchase-flow-limits.controller';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { BazaNcOfferingApiModule } from '../../offering';
import { BazaNcInvestorAccountApiModule } from '../../investor-account';
import { BazaNcPurchaseFlowSessionValidatorService } from './services/baza-nc-purchase-flow-session-validator.service';
import { BazaNcPurchaseFlowFailedTradesService } from './services/baza-nc-purchase-flow-failed-trades.service';
import { BazaNcCreditCardApiModule } from '../../credit-card';
import { BazaNcAchApiModule } from '../../ach';
import { BazaNcPurchaseFlowReprocessService } from './services/baza-nc-purchase-flow-reprocess.service';
import { BAZA_NC_PURCHASE_FLOW_STRATEGIES_MAP } from './strategies';
import { BazaDwollaPaymentApiModule } from '@scaliolabs/baza-dwolla-api';
import { BazaNcDwollaApiModule } from '../../dwolla';
import { TransferCancelledEventHandler } from './strategies/dwolla-account-balance/event-handlers/transfer-cancelled.event-handler';
import { TransferCompletedEventHandler } from './strategies/dwolla-account-balance/event-handlers/transfer-completed.event-handler';
import { TransferFailedEventHandler } from './strategies/dwolla-account-balance/event-handlers/transfer-failed.event-handler';
import { bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<PurchaseFlowApiModuleConfiguration>;
}

export { AsyncOptions as BazaNcPurchaseFlowModuleAsyncOptions };

const watchers = [BazaNcPurchaseFlowTradesSessionWatcher, BazaNcPurchaseFlowFailedTradesWatcher];

const eventHandlers = [
    BazaNcPurchaseFlowTradeSubmittedEventHandler,
    TransferCancelledEventHandler,
    TransferCompletedEventHandler,
    TransferFailedEventHandler,
];

const services = [
    BazaNcPurchaseFlowTradesSessionService,
    BazaNcPurchaseFlowStatsService,
    BazaNcPurchaseFlowSessionService,
    BazaNcPurchaseFlowPlaidService,
    BazaNcPurchaseFlowPersonalInformationService,
    BazaNcPurchaseFlowBankAccountService,
    BazaNcPurchaseFlowDocusignService,
    BazaNcPurchaseFlowCreditCardService,
    BazaNcPurchaseFlowLimitsService,
    BazaNcPurchaseFlowSessionValidatorService,
    BazaNcPurchaseFlowFailedTradesService,
    BazaNcPurchaseFlowReprocessService,
    ...BAZA_NC_PURCHASE_FLOW_STRATEGIES_MAP.map((next) => next.strategy),
];

@Module({})
export class BazaNcPurchaseFlowApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: PurchaseFlowGlobalModule,
            providers: [
                {
                    provide: PURCHASE_FLOW_API_MODULE_CONFIG,
                    inject: options.injects,
                    useFactory: options.useFactory,
                },
            ],
            exports: [PURCHASE_FLOW_API_MODULE_CONFIG],
        };
    }
}

@Global()
@Module({
    imports: [
        CqrsModule,
        BazaCoreSessionLockApiModule,
        BazaNcTypeormApiModule,
        BazaNcInvestorAccountApiModule,
        BazaNcOfferingApiModule,
        BazaNcCreditCardApiModule,
        BazaNcAchApiModule,
        BazaCreditCardApiModule,
        BazaDwollaPaymentApiModule,
        BazaNcDwollaApiModule,
    ],
    controllers: [
        BazaNcPurchaseFlowController,
        BazaNcPurchaseFlowBankAccountController,
        BazaNcPurchaseFlowCreditCardController,
        BazaNcPurchaseFlowLimitsController,
    ],
    providers: [...services, ...watchers, ...eventHandlers],
    exports: [...services],
})
class PurchaseFlowGlobalModule implements OnApplicationBootstrap, OnApplicationShutdown {
    constructor(
        private readonly envService: EnvService,
        private readonly watcherTradesSessions: BazaNcPurchaseFlowTradesSessionWatcher,
        private readonly watcherFailedTradesSessions: BazaNcPurchaseFlowFailedTradesWatcher,
    ) {}

    async onApplicationBootstrap(): Promise<void> {
        if (this.envService.isMasterNode) {
            await this.watcherTradesSessions.start();

            if (bazaNcApiConfig().enableAutoCancellingFailedTrades) {
                await this.watcherFailedTradesSessions.start();
            }
        }
    }

    async onApplicationShutdown(): Promise<void> {
        if (this.envService.isMasterNode) {
            await this.watcherTradesSessions.stop();

            if (bazaNcApiConfig().enableAutoCancellingFailedTrades) {
                await this.watcherFailedTradesSessions.stop();
            }
        }
    }
}
