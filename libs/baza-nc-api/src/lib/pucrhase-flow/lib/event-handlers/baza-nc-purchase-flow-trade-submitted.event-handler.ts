import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcPurchaseFlowSessionSubmittedEvent } from '@scaliolabs/baza-nc-shared';
import { BazaLogger, cqrs } from '@scaliolabs/baza-core-api';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingsPercentsFundedService, BazaNcOfferingsService, BazaNcOfferingStatusService } from '../../../offering';
import { BazaNcOfferingRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { awaitTimeout } from '@scaliolabs/baza-core-shared';

@EventsHandler(BazaNcPurchaseFlowSessionSubmittedEvent)
export class BazaNcPurchaseFlowTradeSubmittedEventHandler implements IEventHandler<BazaNcPurchaseFlowSessionSubmittedEvent> {
    constructor(
        private readonly logger: BazaLogger,
        private readonly transactionsRepository: BazaNcTransactionRepository,
        private readonly ncOfferingService: BazaNcOfferingsService,
        private readonly ncOfferingStatus: BazaNcOfferingStatusService,
        private readonly ncOfferingRepository: BazaNcOfferingRepository,
        private readonly percentsFunded: BazaNcOfferingsPercentsFundedService,
    ) {}

    handle(command: BazaNcPurchaseFlowSessionSubmittedEvent): void {
        cqrs(BazaNcPurchaseFlowTradeSubmittedEventHandler.name, async () => {
            /**
             * TODO: We should investigate why we're waiting for 1 second here and resolve it
             */
            await awaitTimeout(3000);
            await this.perform(command);
        });
    }

    async perform(command: BazaNcPurchaseFlowSessionSubmittedEvent): Promise<void> {
        await this.updateNcOffering(command);
        await this.updatePercentsFunded(command);
        await this.closeOfferingIfItsOutOfShares(command);
    }

    async updateNcOffering(command: BazaNcPurchaseFlowSessionSubmittedEvent): Promise<void> {
        const tradeId = command.request.ncTradeId;

        const transaction = await this.transactionsRepository.findByNcTradeId(tradeId);

        if (!transaction) {
            this.logger.error(`[OnUpdateTradeStatusEventHandler] Transaction was not found for tradeId ${tradeId}`);

            return;
        }

        await this.ncOfferingService.syncWithNcOffering(transaction.ncOfferingId);
    }

    async updatePercentsFunded(command: BazaNcPurchaseFlowSessionSubmittedEvent): Promise<void> {
        await this.percentsFunded.updatePercentsFunded(command.request.ncOfferingId);
    }

    private async closeOfferingIfItsOutOfShares(command: BazaNcPurchaseFlowSessionSubmittedEvent): Promise<void> {
        const tradeId = command.request.ncTradeId;

        const transaction = await this.transactionsRepository.findByNcTradeId(tradeId);

        if (!transaction) {
            this.logger.error(`[OnPurchaseFlowTradeSubmittedEventHandler] User purchase was not found for tradeId ${tradeId}`);
            return;
        }

        const offering = await this.ncOfferingRepository.findOfferingWithId(transaction.ncOfferingId);

        if (!offering) {
            this.logger.error(`[OnPurchaseFlowTradeSubmittedEventHandler] Offering was not found for tradeId ${tradeId}`);
            return;
        }

        if (offering.percentsFunded >= 100) {
            await this.ncOfferingStatus.updateStatus(offering.ncOfferingId, BazaNcOfferingStatus.Closed);
        }
    }
}
