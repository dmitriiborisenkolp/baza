import { Inject, Injectable } from '@nestjs/common';
import { Connection, In, Not, Repository } from 'typeorm';
import { PURCHASE_FLOW_API_MODULE_CONFIG, PurchaseFlowApiModuleConfiguration } from '../baza-nc-purchase-flow-api.config';
import { convertFromCents, convertToCents, TransactionState } from '@scaliolabs/baza-nc-shared';
import { StatsDto, StatsRequestDto } from '@scaliolabs/baza-nc-shared';
import { I18nApiService } from '@scaliolabs/baza-core-api';
import { BazaNcPurchaseFlowLimitsService } from './baza-nc-purchase-flow-limits.service';
import { BazaNcInvestorAccountEntity, BazaNcOfferingEntity, BazaNcOfferingRepository, BazaNcTransactionEntity } from '../../../typeorm';
import { BazaNcPurchaseFlowTradesSessionService } from './baza-nc-purchase-flow-trades-session.service';

function safeNaN(input: number): number {
    return isNaN(input) ? 0 : input;
}

@Injectable()
export class BazaNcPurchaseFlowStatsService {
    constructor(
        @Inject(PURCHASE_FLOW_API_MODULE_CONFIG) private readonly moduleConfig: PurchaseFlowApiModuleConfiguration,
        private readonly connection: Connection,
        private readonly offeringRepository: BazaNcOfferingRepository,
        private readonly translate: I18nApiService,
        private readonly limits: BazaNcPurchaseFlowLimitsService,
        private readonly tradeSession: BazaNcPurchaseFlowTradesSessionService,
    ) {}

    get transactionsRepository(): Repository<BazaNcTransactionEntity> {
        return this.connection.getRepository(BazaNcTransactionEntity);
    }

    async stats(investorAccount: BazaNcInvestorAccountEntity, request: StatsRequestDto): Promise<StatsDto> {
        const offering = await this.offeringRepository.getOfferingWithId(request.offeringId);

        const i18n = (input: string, replaces?: any) =>
            this.translate.get(`baza-nc.purchaseFlow.purchaseFlowStats.${input}`, {
                replaces,
            });

        const numSharesReservedByCurrentUser = await (async () => {
            if (
                !investorAccount.northCapitalAccountId &&
                (await this.tradeSession.hasActiveTradeByAccountAndOfferingId({
                    accountId: investorAccount.northCapitalAccountId,
                    offeringId: request.offeringId,
                }))
            ) {
                const session = await this.tradeSession.getActiveTradeByAccountAndOfferingId({
                    accountId: investorAccount.northCapitalAccountId,
                    offeringId: request.offeringId,
                });

                return session.numberOfShares;
            } else {
                return 0;
            }
        })();

        const numSharesReservedByOtherUsers = Math.max(
            0,
            (await (async () => {
                let result = 0;

                const activeTrades = await this.tradeSession.findActiveTradeSessionsOfOffering({
                    offeringId: request.offeringId,
                });

                for (const trade of activeTrades) {
                    if (trade.accountId !== investorAccount.northCapitalAccountId) {
                        result += trade.numberOfShares;
                    }
                }

                return result;
            })()) - numSharesReservedByCurrentUser,
        );

        let totalOfferingSharesRemaining =
            Math.max(0, await this.totalOfferingSharesRemaining(offering, investorAccount)) -
            numSharesReservedByOtherUsers -
            numSharesReservedByCurrentUser;

        if (request.withReservedShares === false) {
            totalOfferingSharesRemaining += numSharesReservedByCurrentUser;
        }

        if (request.withReservedByOtherUsersShares === false) {
            totalOfferingSharesRemaining += numSharesReservedByOtherUsers;
        }

        const numSharesMax = Math.max(0, this.ncMaximumShares(offering));
        const totalPurchasedAmount = Math.max(0, convertFromCents(await this.totalPurchasedAmount(offering)));
        const totalPurchasedAmountCents = convertFromCents(totalPurchasedAmount);
        const numSharesAvailableForUser = Math.max(
            0,
            Math.min(totalOfferingSharesRemaining, await this.numSharesAvailableForUser(offering, investorAccount)) -
                numSharesReservedByCurrentUser,
        );
        const numMinPurchaseAbleShares = Math.max(await this.ncMinSharesPerAccount(offering));
        const numSharesAlreadyPurchasedByUser = Math.max(0, await this.numSharesPurchasedByUser(offering, investorAccount));
        const numSharesPurchasedByOtherUsers = Math.max(0, await this.numSharesPurchasedByOtherUsers(offering, investorAccount));

        const canPurchase = Math.max(0, Math.max(0, Math.min(...[totalOfferingSharesRemaining, numSharesMax, numSharesAvailableForUser])));

        const statsDTO: StatsDto = {
            canPurchase,
            numSharesMax,
            totalPurchasedAmount,
            totalPurchasedAmountCents,
            totalOfferingSharesRemaining,
            numSharesAvailableForUser,
            numSharesAlreadyPurchasedByUser,
            numSharesPurchasedByOtherUsers,
            numSharesReservedByOtherUsers,
            numMinPurchaseAbleShares,
            isValidPurchase: true,
        };

        const truthyStrategies: Array<{
            match: () => boolean;
            execute: () => void;
        }> = [
            {
                match: () => canPurchase > 0 && totalOfferingSharesRemaining < numSharesAvailableForUser,
                execute: () => {
                    statsDTO.isValidPurchase = true;
                    statsDTO.message = i18n('onlyNumberOfSharesRemaining', {
                        canPurchase,
                    });
                },
            },
            {
                match: () => canPurchase > 0 && numSharesAlreadyPurchasedByUser === 0,
                execute: () => {
                    statsDTO.isValidPurchase = true;
                    statsDTO.message = i18n('youCanPurchaseUpTo', {
                        canPurchase,
                    });
                },
            },
            {
                match: () => canPurchase > 0 && numSharesAlreadyPurchasedByUser > 0,
                execute: () => {
                    statsDTO.isValidPurchase = true;
                    statsDTO.message = i18n('youHaveMadePreviousPurchase', {
                        canPurchase,
                    });
                },
            },
        ];

        const falsyStrategies: Array<{
            match: () => boolean;
            execute: () => void;
        }> = [
            {
                match: () => numSharesAlreadyPurchasedByUser === this.ncMaxSharesPerAccount(offering),
                execute: () => {
                    statsDTO.isValidPurchase = false;
                    statsDTO.message = i18n('youReachedMaximumAmount');
                },
            },
            {
                match: () => numMinPurchaseAbleShares > numSharesAvailableForUser,
                execute: () => {
                    statsDTO.isValidPurchase = false;
                    statsDTO.message = i18n('youReachedMaximumAmount');
                },
            },
            {
                match: () =>
                    !!request.requestedAmountCents && canPurchase < request.requestedAmountCents / convertToCents(offering.ncUnitPrice),
                execute: () => {
                    statsDTO.isValidPurchase = false;
                    statsDTO.message = i18n('youCanPurchaseUpTo', {
                        canPurchase,
                    });
                },
            },
            {
                match: () => canPurchase === 0,
                execute: () => {
                    statsDTO.isValidPurchase = false;
                    statsDTO.message = i18n('noSharesAvailable');
                },
            },
        ];

        for (const strategy of truthyStrategies) {
            if (strategy.match()) {
                strategy.execute();

                break;
            }
        }

        for (const strategy of falsyStrategies) {
            if (strategy.match()) {
                strategy.execute();

                break;
            }
        }

        if (request.requestedAmountCents) {
            statsDTO.availableTransactionTypes = await this.limits.limitsForPurchase(request.requestedAmountCents, offering.ncOfferingFees);
        }

        return statsDTO;
    }

    ncMaximumShares(offering: BazaNcOfferingEntity): number {
        return offering.ncTotalShares;
    }

    ncMaxSharesPerAccount(offering: BazaNcOfferingEntity): number {
        return offering.ncMaxSharesPerAccount || Math.floor(offering.ncMaxAmount / offering.ncUnitPrice);
    }

    ncMinSharesPerAccount(offering: BazaNcOfferingEntity): number {
        return Math.floor(offering.ncMinAmount / offering.ncUnitPrice);
    }

    async totalOfferingSharesRemaining(offering: BazaNcOfferingEntity, investorAccount: BazaNcInvestorAccountEntity): Promise<number> {
        const purchasedShares =
            (await this.numSharesPurchasedByOtherUsers(offering, investorAccount)) +
            (await this.numSharesPurchasedByUser(offering, investorAccount));

        return Math.max(0, safeNaN(Math.max(0, this.ncMaximumShares(offering) - purchasedShares)));
    }

    async totalPurchasedAmount(offering: BazaNcOfferingEntity): Promise<number> {
        let totalPurchasedAmount = 0;

        const purchases = await this.transactionsRepository.find({
            where: [
                {
                    ncOfferingId: offering.ncOfferingId,
                    state: In([
                        TransactionState.PendingPayment,
                        TransactionState.PaymentFunded,
                        TransactionState.PaymentConfirmed,
                        TransactionState.UnwindPending,
                    ]),
                },
            ],
        });

        for (const purchase of purchases) {
            totalPurchasedAmount += purchase.amountCents;
        }

        return totalPurchasedAmount;
    }

    async numSharesAvailableForUser(offering: BazaNcOfferingEntity, investorAccount: BazaNcInvestorAccountEntity): Promise<number> {
        return safeNaN(
            Math.max(0, this.ncMaxSharesPerAccount(offering) - (await this.numSharesPurchasedByUser(offering, investorAccount))),
        );
    }

    async numSharesPurchasedByUser(offeringEntity: BazaNcOfferingEntity, investorAccount: BazaNcInvestorAccountEntity): Promise<number> {
        let totalAmount = 0;

        const purchases = await this.transactionsRepository.find({
            where: [
                {
                    account: investorAccount.user,
                    ncOfferingId: offeringEntity.ncOfferingId,
                    state: In([
                        TransactionState.PendingPayment,
                        TransactionState.PaymentFunded,
                        TransactionState.PaymentConfirmed,
                        TransactionState.UnwindPending,
                    ]),
                },
            ],
        });

        for (const purchase of purchases) {
            totalAmount += purchase.amountCents;
        }

        return safeNaN(Math.max(0, convertFromCents(totalAmount / offeringEntity.ncUnitPrice)));
    }

    async numSharesPurchasedByOtherUsers(
        offeringEntity: BazaNcOfferingEntity,
        investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<number> {
        let totalAmount = 0;

        const purchases = await this.transactionsRepository.find({
            where: [
                {
                    account: Not(investorAccount.user.id),
                    ncOfferingId: offeringEntity.ncOfferingId,
                    state: In([
                        TransactionState.PendingPayment,
                        TransactionState.PaymentFunded,
                        TransactionState.PaymentConfirmed,
                        TransactionState.UnwindPending,
                    ]),
                },
            ],
        });

        for (const purchase of purchases) {
            totalAmount += purchase.amountCents;
        }

        return safeNaN(Math.max(0, convertFromCents(totalAmount / offeringEntity.ncUnitPrice)));
    }
}
