import { Injectable } from '@nestjs/common';
import { BazaNcCreditCardDto, BazaNcPurchaseFlowSetCreditCardRequest, bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException } from '../exceptions/baza-nc-purchase-flow-legacy-bank-account-system-disabled.exception';
import { BazaNcCreditCardService } from '../../../credit-card';

/**
 * Credit Card Service for Purchase Flow
 * The service will work only if Legacy BAS is enabled
 * Allows to get or set Credit Card for current Investor Account
 * Investor Account can have only 1 Credit Card attached per Investor Account
 */
@Injectable()
export class BazaNcPurchaseFlowCreditCardService {
    constructor(private readonly service: BazaNcCreditCardService) {}

    /**
     * Set or replace Credit Card for Investor Account
     * @param clientIp
     * @param investorAccount
     * @param request
     */
    async setCreditCard(
        clientIp: string,
        investorAccount: BazaNcInvestorAccountEntity,
        request: BazaNcPurchaseFlowSetCreditCardRequest,
    ): Promise<BazaNcCreditCardDto> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        return this.service.setCreditCard(clientIp, investorAccount, request);
    }

    /**
     * Returns true if current Investor Account has Credit Card
     * @param investorAccount
     */
    async hasCreditCard(investorAccount: BazaNcInvestorAccountEntity): Promise<boolean> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        return this.service.hasCreditCard(investorAccount);
    }

    /**
     * Returns Credit Card for current Investor Account
     * @param investorAccount
     */
    async getCreditCard(investorAccount: BazaNcInvestorAccountEntity): Promise<BazaNcCreditCardDto | undefined> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        return this.service.getCreditCard(investorAccount);
    }

    /**
     * Deletes Credit Card Details of Investor Account from NC API
     * @param clientIp
     * @param investorAccount
     */
    async deleteCreditCard(clientIp: string, investorAccount: BazaNcInvestorAccountEntity): Promise<boolean> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        return this.service.deleteCreditCard(clientIp, investorAccount);
    }

    /**
     * Updates `isCreditCardLinked` flag of Investor Account
     * @param investorAccount
     */
    async updateCreditCardStatus(investorAccount: BazaNcInvestorAccountEntity): Promise<void> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        return this.service.updateCreditCardStatus(investorAccount.id);
    }
}
