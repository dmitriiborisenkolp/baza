import { Inject, Injectable } from '@nestjs/common';
import { SessionDocusignDto, SessionDocusignRequestDto } from '@scaliolabs/baza-nc-shared';
import { EsignStatus, NorthCapitalErrorCodes } from '@scaliolabs/baza-nc-shared';
import { BazaLogger, BazaRegistryService } from '@scaliolabs/baza-core-api';
import { EnvService } from '@scaliolabs/baza-core-api';
import { PURCHASE_FLOW_API_MODULE_CONFIG, PurchaseFlowApiModuleConfiguration } from '../baza-nc-purchase-flow-api.config';
import { BazaNcPurchaseFlowDocusignUrlsAreDisabledForCurrentEnvironmentException } from '../exceptions/baza-nc-purchase-flow-docusign-urls-are-disabled-for-current-environment.exception';
import { isNorthCapitalException, NorthCapitalDocumentsApiNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountEntity, BazaNcTradesSessionEntity, BazaNcTradesSessionRepository } from '../../../typeorm';

@Injectable()
export class BazaNcPurchaseFlowDocusignService {
    constructor(
        @Inject(PURCHASE_FLOW_API_MODULE_CONFIG) private readonly moduleConfig: PurchaseFlowApiModuleConfiguration,
        private readonly env: EnvService,
        private readonly registry: BazaRegistryService,
        private readonly tradeSessionRepository: BazaNcTradesSessionRepository,
        private readonly ncDocumentsApi: NorthCapitalDocumentsApiNestjsService,
        private readonly logger: BazaLogger,
    ) {}

    async sessionDocuSign(
        investorAccount: BazaNcInvestorAccountEntity,
        tradeSession: BazaNcTradesSessionEntity,
        request: SessionDocusignRequestDto,
        correlationId?: string,
    ): Promise<SessionDocusignDto> {
        this.logger.info(`[BazaNcPurchaseFlowDocusignService.sessionDocuSign]`, {
            user: investorAccount.userId,
            investorAccountId: investorAccount.id,
            request,
            correlationId,
        });

        if (request.requestDocuSignUrl && this.moduleConfig.disableDocuSignForEnvironments.includes(this.env.current)) {
            throw new BazaNcPurchaseFlowDocusignUrlsAreDisabledForCurrentEnvironmentException();
        }

        const isDocuSignDisabled = await this.registry.getValue('bazaNc.disableDocuSign');

        if (isDocuSignDisabled) {
            return {
                areDocumentsSigned: true,
            };
        } else {
            const documents = await (async () => {
                try {
                    return await this.ncDocumentsApi.getAllSignedDocument({
                        tradeId: tradeSession.tradeId,
                    });
                } catch (err) {
                    if (isNorthCapitalException(err, NorthCapitalErrorCodes.SubscriptionDocumentNotSent)) {
                        return {
                            SignedDocumentDetails: [],
                        };
                    } else {
                        throw err;
                    }
                }
            })();

            const areDocumentsSigned: boolean =
                documents.SignedDocumentDetails.length > 0 &&
                documents.SignedDocumentDetails.some((d) => d.esignstatus === EsignStatus.Signed);

            const docuSignUrl = await (async () => {
                if (request.requestDocuSignUrl && !areDocumentsSigned) {
                    const ncDocuments = await this.ncDocumentsApi.sendSubscriptionDocumentClient({
                        tradeId: tradeSession.tradeId,
                        accountId: investorAccount.northCapitalAccountId,
                        offeringId: tradeSession.offeringId,
                    });
                    tradeSession.docuSignUrl = ncDocuments.document_details.url;

                    await this.tradeSessionRepository.save(tradeSession);

                    return tradeSession.docuSignUrl;
                } else {
                    return undefined;
                }
            })();

            return {
                areDocumentsSigned,
                docuSignUrl,
            };
        }
    }
}
