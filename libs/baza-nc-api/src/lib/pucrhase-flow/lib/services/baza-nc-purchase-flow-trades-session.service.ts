import { Inject, Injectable, Logger } from '@nestjs/common';
import { NorthCapitalAccountId, NorthCapitalErrorCodes, OfferingId, TradeId, TransactionState } from '@scaliolabs/baza-nc-shared';
import { PURCHASE_FLOW_API_MODULE_CONFIG, PurchaseFlowApiModuleConfiguration } from '../baza-nc-purchase-flow-api.config';
import { BazaNcPurchaseFlowSessionDeleted, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcTradesSessionEntity, BazaNcTradesSessionRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { isNorthCapitalException, NorthCapitalTradesApiNestjsService } from '../../../transact-api';
import { BazaLogger } from '@scaliolabs/baza-core-api';

/**
 * Add some threshold which will allows to perform session for edge cases.
 */
export const THRESHOLD_FOR_INCOMING_REQUESTS_SECONDS = 30;

@Injectable()
export class BazaNcPurchaseFlowTradesSessionService {
    constructor(
        @Inject(PURCHASE_FLOW_API_MODULE_CONFIG) private readonly moduleConfig: PurchaseFlowApiModuleConfiguration,
        private readonly eventBus: EventBus,
        private readonly repository: BazaNcTradesSessionRepository,
        private readonly transactionRepository: BazaNcTransactionRepository,
        private readonly ncTradesApi: NorthCapitalTradesApiNestjsService,
        private readonly logger: BazaLogger,
    ) {}

    async createSessionForTrade(
        request: {
            tradeId: TradeId;
            offeringId: OfferingId;
            accountId: NorthCapitalAccountId;
            amountCents: number;
            feeCents: number;
            totalCents: number;
            pricePerSharesCents: number;
            numberOfShares: number;
            transactionType: BazaNcPurchaseFlowTransactionType;
            transactionFees?: number;
            transactionFeesCents?: number;
        },
        correlationId?: string,
    ): Promise<BazaNcTradesSessionEntity> {
        this.logger.info(`[BazaNcPurchaseFlowTradesSessionService.createSessionForTrade]`, {
            correlationId,
            request,
        });

        const existing = await this.repository.findActiveTradeByTradeId(request.tradeId);

        if (existing) {
            await this.destroyTrade(request.tradeId);
        }

        const newEntity = new BazaNcTradesSessionEntity();

        newEntity.createdAt = new Date();
        newEntity.updatedAt = new Date();
        newEntity.tradeId = request.tradeId;
        newEntity.offeringId = request.offeringId;
        newEntity.accountId = request.accountId;
        newEntity.amountCents = request.amountCents;
        newEntity.feeCents = request.feeCents;
        newEntity.totalCents = request.totalCents;
        newEntity.pricePerShareCents = request.pricePerSharesCents;
        newEntity.numberOfShares = request.numberOfShares;
        newEntity.isOutdated = false;
        newEntity.shouldBeDeletedFromNC = false;
        newEntity.activeTo = new Date();
        newEntity.transactionType = request.transactionType;
        newEntity.transactionFees = request.transactionFees || 0;
        newEntity.transactionFeesCents = request.transactionFeesCents || 0;
        newEntity.activeTo.setTime(
            newEntity.activeTo.getTime() + 1000 * (this.moduleConfig.tradesTTLSec + THRESHOLD_FOR_INCOMING_REQUESTS_SECONDS),
        );

        await this.repository.save(newEntity);

        return newEntity;
    }

    async touchSessionForTrade(tradeId: TradeId): Promise<BazaNcTradesSessionEntity> {
        const entity = await this.repository.getActiveTradeByTradeId(tradeId);

        entity.updatedAt = new Date();
        entity.activeTo = new Date();
        entity.isOutdated = false;

        entity.activeTo.setTime(entity.activeTo.getTime() + 1000 * this.moduleConfig.tradesTTLSec);

        await this.repository.save(entity);

        return entity;
    }

    async destroyTrade(tradeId: TradeId): Promise<void> {
        const session = await this.repository.findTradeByTradeId(tradeId);

        if (!session) {
            return;
        }

        session.isOutdated = true;
        session.shouldBeDeletedFromNC = true;

        await this.repository.save(session);
    }

    async completeTrade(tradeId: TradeId): Promise<void> {
        await this.repository.destroyTradeSession(tradeId);
    }

    async markTradesSessionAsOutdated(
        request: { accountId: NorthCapitalAccountId; offeringId: OfferingId },
        correlationId?: string,
    ): Promise<void> {
        this.logger.info(`[BazaNcPurchaseFlowTradesSessionService.markTradesSessionAsOutdated]`, {
            correlationId,
            request,
        });
        await this.repository.markTradesAsOutdatedByAccountAndOfferingId(request);
    }

    async getActiveTradeSessionByTradeId(request: { tradeId: TradeId }): Promise<BazaNcTradesSessionEntity> {
        return this.repository.getActiveTradeByTradeId(request.tradeId);
    }

    async getActiveTradeByAccountAndOfferingId(
        request: {
            accountId: NorthCapitalAccountId;
            offeringId: OfferingId;
        },
        correlationId?: string,
    ): Promise<BazaNcTradesSessionEntity> {
        this.logger.info(`[BazaNcPurchaseFlowTradesSessionService.getActiveTradeByAccountAndOfferingId]`, {
            correlationId,
            request,
        });
        return this.repository.getActiveTradeByAccountAndOfferingId(request);
    }

    async hasActiveTradeByAccountAndOfferingId(
        request: { accountId: NorthCapitalAccountId; offeringId: OfferingId },
        correlationId?: string,
    ): Promise<boolean> {
        this.logger.info(`[BazaNcPurchaseFlowTradesSessionService.hasActiveTradeByAccountAndOfferingId]`, {
            correlationId,
            request,
        });

        return this.repository.hasActiveTradeByAccountAndOfferingId(request);
    }

    async findActiveTradeSessionsOfOffering(request: { offeringId: OfferingId }): Promise<Array<BazaNcTradesSessionEntity>> {
        return this.repository.findActiveTradesByOffering(request.offeringId);
    }

    async markAllOutdatedTradeSessions(): Promise<void> {
        const outdated = await this.repository.fetchAllOutdatedActiveTradeSessions(this.moduleConfig.tradesTTLSec);

        for (const entity of outdated) {
            entity.isOutdated = true;
            entity.shouldBeDeletedFromNC = true;

            await this.repository.save(entity);

            Logger.log(`[PurchaseFlowTradesSessionService] Trade "${entity.tradeId}" marked as to be deleted from NC`);
        }
    }

    async deleteAllTradeSessionsMarkedAsNCDelete(correlationId?: string): Promise<void> {
        this.logger.info(`[BazaNcPurchaseFlowTradesSessionService.deleteAllTradeSessionsMarkedAsNCDelete]`, {
            correlationId,
        });

        const entities = await this.repository.fetchAllMarkToNCDeleteTradeSessions();

        for (const entity of entities) {
            Logger.log(`[PurchaseFlowTradesSessionService] [DELETE OUTDATED] [ENTITY] [${entity.tradeId}]`);

            const transaction = await this.transactionRepository.findByNcTradeId(entity.tradeId);

            if (transaction) {
                await this.transactionRepository.remove(transaction);
            }

            this.eventBus.publish(
                new BazaNcPurchaseFlowSessionDeleted({
                    ncOfferingId: entity.offeringId,
                    ncTradeId: entity.tradeId,
                }),
            );

            entity.tradeSessionWasDeletedFromNC = true;

            await this.repository.save(entity);
        }

        for (const entity of entities) {
            Logger.log(`[PurchaseFlowTradesSessionService] [DELETE OUTDATED] [NC] [${entity.tradeId}]`);

            try {
                await this.ncTradesApi.deleteTrade({
                    accountId: entity.accountId,
                    tradeId: entity.tradeId,
                });
            } catch (err) {
                if (!isNorthCapitalException(err, NorthCapitalErrorCodes.TradeStatusShouldBeInCreated)) {
                    throw err;
                }
            }

            Logger.log(`[PurchaseFlowTradesSessionService] Trade "${entity.tradeId}" was deleted from NC`);
        }
    }
}
