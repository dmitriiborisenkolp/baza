import { Injectable } from '@nestjs/common';
import {
    bazaNcApiConfig,
    BazaNcBankAccountExport,
    BazaNcBankAccountType,
    PurchaseFlowBankAccountDto,
    PurchaseFlowSetBankAccountDetailsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException } from '../exceptions/baza-nc-purchase-flow-legacy-bank-account-system-disabled.exception';
import { BazaNcAchService } from '../../../ach';
import { BazaNcBankAccountsService } from '../../../bank-accounts';

@Injectable()
export class BazaNcPurchaseFlowBankAccountService {
    constructor(private readonly achService: BazaNcAchService, private readonly bankAccountService: BazaNcBankAccountsService) {}

    /**
     * Returns true if LBA <> NBA sync is enabled
     * Mostly it should be disabled
     */
    get isLegacyNBASSyncEnabled(): boolean {
        return bazaNcApiConfig().enableLegacyBankSyncWithNBAS;
    }

    /**
     * Returns information about Bank Account from NC API
     * If Bank Account is not set, method will return `{ isAvailable: false }`
     * @deprecated Please migrate to new Bank Account system
     * @param investorAccount
     */
    async getBankAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<PurchaseFlowBankAccountDto> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        return this.achService.getBankAccount(investorAccount);
    }

    /**
     * Sets Bank Account for current Investor with NC API
     * @deprecated Please migrate to new Bank Account system
     * @param investorAccount
     * @param request
     */
    async setBankAccount(
        investorAccount: BazaNcInvestorAccountEntity,
        request: PurchaseFlowSetBankAccountDetailsDto,
    ): Promise<PurchaseFlowBankAccountDto> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        if (this.isLegacyNBASSyncEnabled) {
            await this.bankAccountService.deleteLegacyBankAccounts(investorAccount.id);

            const bankAccount = await this.bankAccountService.add(
                investorAccount.id,
                {
                    ...request,
                    type: BazaNcBankAccountType.CashIn,
                    accountNickName: request.accountNickName || request.accountName,
                    export: [BazaNcBankAccountExport.NC],
                },
                { returnDuplicate: true, markAsLinkedWithLegacy: true },
            );

            return {
                isAvailable: true,
                details: {
                    accountName: bankAccount.accountName,
                    accountNickName: bankAccount.accountNickName,
                    accountNumber: bankAccount.accountNumber,
                    accountRoutingNumber: bankAccount.accountRoutingNumber,
                    accountType: bankAccount.accountType,
                },
            };
        } else {
            return this.achService.setBankAccount(investorAccount, request);
        }
    }

    /**
     * Update `investorAccount.isBankAccountLinked` status
     * The method is also checking for existing bank account on NC API side and automatically imports it
     * @deprecated Please migrate to new Bank Account system
     * @param investorAccount
     */
    async updateBankAccountStatus(investorAccount: BazaNcInvestorAccountEntity): Promise<void> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        const currentBankAccount = await this.getBankAccount(investorAccount);

        if (this.isLegacyNBASSyncEnabled) {
            if (currentBankAccount.isAvailable) {
                const similar = await this.bankAccountService.findBankAccount(investorAccount.id, {
                    type: BazaNcBankAccountType.CashIn,
                    accountNumber: currentBankAccount.details.accountNumber,
                    accountRoutingNumber: currentBankAccount.details.accountRoutingNumber,
                });

                if (!similar) {
                    await this.bankAccountService.add(investorAccount.id, {
                        ...currentBankAccount.details,
                        type: BazaNcBankAccountType.CashIn,
                        accountNickName: currentBankAccount.details.accountNickName || currentBankAccount.details.accountName,
                    });
                }
            }
        }

        await this.bankAccountService.updateInvestorBankAccountStatuses(investorAccount.id);
    }

    /**
     * Removes Bank Account from NC API
     * Also sets `investorAccount.isBankAccountLinked` as false
     * @deprecated Please migrate to new Bank Account system
     * @param investorAccount
     */
    async deleteBankAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<void> {
        if (!bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        const currentBankAccount = await this.getBankAccount(investorAccount);

        if (currentBankAccount.isAvailable) {
            if (this.isLegacyNBASSyncEnabled) {
                await this.bankAccountService.deleteLegacyBankAccounts(investorAccount.id);

                const similar = await this.bankAccountService.findBankAccount(investorAccount.id, {
                    type: BazaNcBankAccountType.CashIn,
                    accountNumber: currentBankAccount.details.accountNumber,
                    accountRoutingNumber: currentBankAccount.details.accountRoutingNumber,
                });

                if (similar) {
                    await this.bankAccountService.remove(investorAccount.id, similar.ulid);
                }
            }

            await this.achService.deleteBankAccount(investorAccount);
        }
    }
}
