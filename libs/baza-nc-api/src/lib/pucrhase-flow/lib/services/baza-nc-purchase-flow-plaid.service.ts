import { Injectable } from '@nestjs/common';
import { bazaNcApiConfig, PurchaseFlowPlaidLinkDto } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalErrorCodes } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowBankAccountService } from './baza-nc-purchase-flow-bank-account.service';
import { isNorthCapitalException, NorthCapitalAchTransfersApiNestjsService, NorthCapitalException } from '../../../transact-api';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException } from '../exceptions/baza-nc-purchase-flow-legacy-bank-account-system-disabled.exception';

@Injectable()
export class BazaNcPurchaseFlowPlaidService {
    constructor(
        private readonly bank: BazaNcPurchaseFlowBankAccountService,
        private readonly ncACHTransfersApi: NorthCapitalAchTransfersApiNestjsService,
    ) {}

    async getPlaidLink(investorAccount: BazaNcInvestorAccountEntity): Promise<PurchaseFlowPlaidLinkDto> {
        if (! bazaNcApiConfig().enableLegacyBankAccountsSystem) {
            throw new BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException();
        }

        const plaidLink = (
            await (async () => {
                try {
                    return await this.ncACHTransfersApi.linkExternalAccount({
                        accountId: investorAccount.northCapitalAccountId,
                    });
                } catch (err) {
                    if (isNorthCapitalException(err, NorthCapitalErrorCodes.ExternalAccountAlreadyExists)) {
                        return this.ncACHTransfersApi.updateLinkExternalAccount({
                            accountId: investorAccount.northCapitalAccountId,
                        });
                    } else {
                        throw err;
                    }
                }
            })()
        ).accountDetails;

        return {
            link: plaidLink,
        };
    }
}
