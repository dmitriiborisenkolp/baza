import { Injectable } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { BazaNcInvestorAccountEntity, BazaNcTransactionRepository } from '../../../typeorm';
import { ReProcessPaymentDto, TRANSACTION_REPROCESS_AVAILABLE_STATES, TransactionState } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowInvalidPaymentReprocessException } from '../exceptions/baza-nc-purchase-flow-invalid-payment-reprocess.exception';
import { BazaNcPurchaseFlowBankAccountService } from './baza-nc-purchase-flow-bank-account.service';
import { NorthCapitalAchTransfersApiNestjsService, NorthCapitalTradesApiNestjsService } from '../../../transact-api';
import { BazaNcPurchaseFlowCreditCardService } from './baza-nc-purchase-flow-credit-card.service';
import { BazaNcPurchaseFlowLimitsService } from './baza-nc-purchase-flow-limits.service';
import { BazaNcPurchaseFlowTradesSessionService } from './baza-nc-purchase-flow-trades-session.service';
import { BazaNcPurchaseFlowSessionService } from './baza-nc-purchase-flow-session.service';
import { BAZA_NC_PURCHASE_FLOW_STRATEGIES_MAP, BazaNcPurchaseFlowStrategy } from '../strategies';
import { BazaNcPurchaseFlowUnknownStrategyException } from '../exceptions/baza-nc-purchase-flow-unknown-strategy.exception';

/**
 * Reprocess for Purchase Flow
 */
@Injectable()
export class BazaNcPurchaseFlowReprocessService {
    constructor(
        private readonly moduleRef: ModuleRef,
        private readonly bank: BazaNcPurchaseFlowBankAccountService,
        private readonly creditCard: BazaNcPurchaseFlowCreditCardService,
        private readonly transactionRepository: BazaNcTransactionRepository,
        private readonly tradesSessions: BazaNcPurchaseFlowTradesSessionService,
        private readonly limits: BazaNcPurchaseFlowLimitsService,
        private readonly sessions: BazaNcPurchaseFlowSessionService,
        private readonly ncACHTransfersApi: NorthCapitalAchTransfersApiNestjsService,
        private readonly ncTradesApi: NorthCapitalTradesApiNestjsService,
    ) {}

    /**
     * Reprocess Payment
     * @param ip
     * @param investorAccount
     * @param request
     */
    async reprocess(ip: string, investorAccount: BazaNcInvestorAccountEntity, request: ReProcessPaymentDto): Promise<void> {
        request.transactionType
            ? await this.requestReProcessPaymentWithUpdatedPaymentMethod(ip, investorAccount, request)
            : await this.requestReprocessPaymentForSamePaymentMethod(ip, investorAccount, request);
    }

    /**
     * Reprocess Paymment using same payment method
     * @param ip
     * @param investorAccount
     * @param request
     * @private
     */
    private async requestReprocessPaymentForSamePaymentMethod(
        ip: string,
        investorAccount: BazaNcInvestorAccountEntity,
        request: ReProcessPaymentDto,
    ): Promise<void> {
        const { id } = request;
        const trade = await this.transactionRepository.getById(id);

        if (!TRANSACTION_REPROCESS_AVAILABLE_STATES.includes(trade.state)) {
            throw new BazaNcPurchaseFlowInvalidPaymentReprocessException();
        }

        const strategyDef = BAZA_NC_PURCHASE_FLOW_STRATEGIES_MAP.find((next) => next.type === trade.transactionType);

        if (!strategyDef) {
            throw new BazaNcPurchaseFlowUnknownStrategyException();
        }

        await (this.moduleRef.get(strategyDef.strategy) as BazaNcPurchaseFlowStrategy).reprocess({
            ip,
            tradeId: trade.ncTradeId,
            transactionFeesCents: trade.transactionFeesCents,
            transactionFees: trade.transactionFees,
            amountCents: trade.amountCents,
            offeringId: trade.ncOfferingId,
            investorAccount,
        });

        trade.state = TransactionState.PendingPayment;

        await this.transactionRepository.save(trade);
    }

    /**
     * Reprocess Payment with Updated Payment Method
     * @param ip
     * @param investorAccount
     * @param request
     * @private
     */
    private async requestReProcessPaymentWithUpdatedPaymentMethod(
        ip: string,
        investorAccount: BazaNcInvestorAccountEntity,
        request: ReProcessPaymentDto,
    ): Promise<void> {
        const { id } = request;
        const trade = await this.transactionRepository.getById(id);
        if (trade.state !== TransactionState.PaymentReturned) {
            throw new BazaNcPurchaseFlowInvalidPaymentReprocessException();
        }

        await this.ncTradesApi.deleteTrade({
            accountId: trade.ncAccountId,
            tradeId: trade.ncTradeId,
        });

        const session = await this.sessions.sessionStart(ip, investorAccount, {
            amount: trade.amountCents,
            numberOfShares: trade.shares,
            offeringId: trade.ncOfferingId,
            transactionType: request.transactionType,
        });

        const tradeSession = await this.tradesSessions.getActiveTradeByAccountAndOfferingId({
            accountId: trade.ncAccountId,
            offeringId: trade.ncOfferingId,
        });

        await this.sessions.submit(ip, investorAccount, {
            id: session.id,
        });

        const newlySubmittedTrade = await this.transactionRepository.findByNcTradeId(tradeSession.tradeId);

        trade.ncTradeId = newlySubmittedTrade.ncTradeId;
        trade.transactionType = newlySubmittedTrade.transactionType;
        trade.transactionFees = newlySubmittedTrade.transactionFees;
        trade.transactionFeesCents = newlySubmittedTrade.transactionFeesCents;
        trade.totalCents = newlySubmittedTrade.totalCents;
        trade.feeCents = newlySubmittedTrade.feeCents;
        trade.state = TransactionState.PendingPayment;
        trade.ncOrderStatus = newlySubmittedTrade.ncOrderStatus;

        await this.transactionRepository.save(trade);

        await this.transactionRepository.remove(newlySubmittedTrade);
    }
}
