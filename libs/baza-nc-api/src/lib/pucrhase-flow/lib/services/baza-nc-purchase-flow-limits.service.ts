import { Injectable } from '@nestjs/common';
import {
    BazaNcLimitsDto,
    BazaNcPurchaseFlowTransactionType,
    PurchaseFlowLimitsDto,
    convertFromCents,
    convertToCents,
} from '@scaliolabs/baza-nc-shared';
import { BazaLogger, BazaRegistryService } from '@scaliolabs/baza-core-api';
import { BazaNcOfferingsService, BazaNcOfferingTransactionFeesService } from '../../../offering';

@Injectable()
export class BazaNcPurchaseFlowLimitsService {
    constructor(
        private readonly registry: BazaRegistryService,
        private readonly transactionFees: BazaNcOfferingTransactionFeesService,
        private readonly ncOffering: BazaNcOfferingsService,
        private readonly logger: BazaLogger,
    ) {}

    async limits(): Promise<BazaNcLimitsDto> {
        const maxACHTransferAmountCents = convertToCents(await this.registry.getValue('bazaNc.maxACHTransferAmount'));
        const maxCreditCardTransferAmountCents = convertToCents(await this.registry.getValue('bazaNc.maxCreditCardTransferAmount'));
        const creditCardPaymentsFee = await this.registry.getValue('bazaNc.creditCardFee');
        const maxDwollaTransferAmount = await this.registry.getValue('bazaNc.maxDwollaTransferAmount');

        return {
            maxACHTransferAmountCents,
            maxCreditCardTransferAmountCents,
            creditCardPaymentsFee,
            maxDwollaTransferAmount,
        };
    }

    async limitsForPurchase(requestedAmountCents: number, transactionFees = 0, correlationId?: string): Promise<Array<PurchaseFlowLimitsDto>> {
        this.logger.info(`[BazaNcPurchaseFlowLimitsService.limitsForPurchase]`, {
            requestedAmountCents,
            transactionFees,
            correlationId,
        });

        const limits = await this.limits();

        let transactionFeesCents = 0;

        if (this.transactionFees.shouldChargeTransactionFees && transactionFees) {
            transactionFeesCents = Math.ceil(
                this.transactionFees.percentageIncrementedFees(requestedAmountCents, transactionFees) - requestedAmountCents,
            );

            requestedAmountCents += transactionFeesCents;
        }

        return [
            {
                transactionType: BazaNcPurchaseFlowTransactionType.ACH,
                isAvailable: requestedAmountCents <= limits.maxACHTransferAmountCents,
                amountWithFeesCents: requestedAmountCents,
                feeCents: 0,
                transactionFeesCents,
                transactionFees,
                maximumAmountCents: limits.maxACHTransferAmountCents,
            },
            {
                transactionType: BazaNcPurchaseFlowTransactionType.CreditCard,
                isAvailable:
                    requestedAmountCents + convertFromCents(requestedAmountCents) * limits.creditCardPaymentsFee <=
                    limits.maxCreditCardTransferAmountCents,
                amountWithFeesCents: Math.ceil(
                    requestedAmountCents + convertFromCents(requestedAmountCents) * limits.creditCardPaymentsFee,
                ),
                feeCents: Math.ceil(convertFromCents(requestedAmountCents) * limits.creditCardPaymentsFee),
                transactionFeesCents,
                transactionFees,
                maximumAmountCents: limits.maxCreditCardTransferAmountCents,
            },
            {
                transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
                isAvailable: requestedAmountCents <= limits.maxDwollaTransferAmount,
                amountWithFeesCents: requestedAmountCents,
                feeCents: 0,
                transactionFeesCents,
                transactionFees,
                maximumAmountCents: limits.maxDwollaTransferAmount,
            },
        ];
    }

    async limitsForPurchaseByOfferingId(requestedAmountCents: number, offeringId?: string): Promise<Array<PurchaseFlowLimitsDto>> {
        if (offeringId) {
            const offering = await this.ncOffering.getOffering(offeringId);

            return await this.limitsForPurchase(requestedAmountCents, offering.ncOfferingFees);
        } else {
            return this.limitsForPurchase(requestedAmountCents);
        }
    }
}
