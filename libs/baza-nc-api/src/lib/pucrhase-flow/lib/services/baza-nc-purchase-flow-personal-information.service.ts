import { Injectable } from '@nestjs/common';
import { PurchaseFlowPersonalInfoDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { ncOutput, NcOutputContext, NorthCapitalPartiesApiNestjsService } from '../../../transact-api';

@Injectable()
export class BazaNcPurchaseFlowPersonalInformationService {
    constructor(private readonly ncPartiesApi: NorthCapitalPartiesApiNestjsService) {}

    async getPersonalInfo(investorAccount: BazaNcInvestorAccountEntity): Promise<PurchaseFlowPersonalInfoDto> {
        const party = ncOutput(
            (
                await this.ncPartiesApi.getParty({
                    partyId: investorAccount.northCapitalPartyId,
                })
            ).partyDetails[0],
            NcOutputContext.GetParty,
        );

        const hasSsn = (party.socialSecurityNumber || '').trim().length > 0;

        return {
            name: `${party.firstName} ${party.lastName}`,
            address: {
                residentialCountry: party.primCountry,
                residentialState: party.primState,
                residentialCity: party.primCity,
                residentialStreetAddress1: party.primAddress1,
                residentialStreetAddress2: party.primAddress2,
                residentialZipCode: party.primZip,
            },
            dateOfBirth: party.dob,
            hasSsn,
            ssn: hasSsn ? party.socialSecurityNumber : undefined,
        };
    }
}
