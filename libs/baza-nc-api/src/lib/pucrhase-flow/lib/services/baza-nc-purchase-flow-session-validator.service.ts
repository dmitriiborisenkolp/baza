import { Injectable } from '@nestjs/common';
import { BazaNcInvestorAccountEntity, BazaNcPurchaseFlowException } from '../../../typeorm';
import {
    BazaNcPurchaseFlowErrorCodes,
    BazaNcOfferingStatus,
    PurchaseFlowSessionDto,
    BazaNcPurchaseFlowTransactionType,
    StatsDto,
} from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowInvestorPurchaseDeniedException } from '../exceptions/baza-nc-purchase-flow-investor-purchase-denied.exception';
import { BazaNcPurchaseFlowOfferingIsComingSoonException } from '../exceptions/baza-nc-purchase-flow-offering-is-coming-soon.exception';
import { BazaNcPurchaseFlowOfferingIsClosedException } from '../exceptions/baza-nc-purchase-flow-offering-is-closed.exception';
import { BazaNcPurchaseFlowLimitsViolationException } from '../exceptions/baza-nc-purchase-flow-limits-violation.exception';
import { BazaNcPurchaseFlowStatsService } from './baza-nc-purchase-flow-stats.service';
import { BazaNcOfferingsService } from '../../../offering';
import { BazaNcPurchaseFlowLimitsService } from './baza-nc-purchase-flow-limits.service';
import { BazaLogger } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcPurchaseFlowSessionValidatorService {
    constructor(
        private readonly stats: BazaNcPurchaseFlowStatsService,
        private readonly ncOffering: BazaNcOfferingsService,
        private readonly limits: BazaNcPurchaseFlowLimitsService,
        private readonly logger: BazaLogger,
    ) {}

    async validate(
        investorAccount: BazaNcInvestorAccountEntity,
        request: PurchaseFlowSessionDto,
        correlationId?: string,
    ): Promise<StatsDto> {
        this.logger.info(`[BazaNcPurchaseFlowSessionValidatorService.validate]`, {
            investorAccountId: investorAccount.id,
            userId: investorAccount.userId,
            request,
            correlationId,
        });

        const stats = await this.stats.stats(investorAccount, {
            offeringId: request.offeringId,
        });

        if (stats.canPurchase < request.numberOfShares) {
            throw new BazaNcPurchaseFlowException(
                {
                    errorCode: BazaNcPurchaseFlowErrorCodes.SessionCanNotBeStarted,
                    message: stats.message,
                    mayPurchase: stats.canPurchase,
                },
                HttpStatus.BAD_REQUEST,
            );
        }

        if (!stats.isValidPurchase) {
            throw new BazaNcPurchaseFlowInvestorPurchaseDeniedException(stats.message);
        }

        return stats;
    }

    async validateInvestorPurchase(investorAccount: BazaNcInvestorAccountEntity, offeringId: string): Promise<void> {
        const stats = await this.stats.stats(investorAccount, {
            offeringId,
        });

        if (!stats.isValidPurchase) {
            throw new BazaNcPurchaseFlowInvestorPurchaseDeniedException(stats.message);
        }
    }

    async validateOffering(offeringId: string, correlationId?: string): Promise<void> {
        this.logger.info(`[BazaNcPurchaseFlowSessionValidatorService.validateOffering]`, {
            offeringId,
            correlationId,
        });

        const offering = await this.ncOffering.getOffering(offeringId);

        if (offering.status === BazaNcOfferingStatus.ComingSoon) {
            this.logger.error(
                `[BazaNcPurchaseFlowSessionValidatorService.validateOffering] BazaNcPurchaseFlowOfferingIsComingSoonException offeringId: ${offeringId}, correlationID: ${correlationId}`,
            );
            throw new BazaNcPurchaseFlowOfferingIsComingSoonException();
        }

        if (offering.status === BazaNcOfferingStatus.Closed) {
            this.logger.error(
                `[BazaNcPurchaseFlowSessionValidatorService.validateOffering] BazaNcPurchaseFlowOfferingIsClosedException offeringId: ${offeringId}, correlationID: ${correlationId}`,
            );
            throw new BazaNcPurchaseFlowOfferingIsClosedException();
        }
    }

    async validatePurchaseAmount(
        type: BazaNcPurchaseFlowTransactionType,
        amountCents: number,
        transactionFee: number,
        correlationId?: string,
    ): Promise<void> {
        this.logger.info(`[BazaNcPurchaseFlowSessionValidatorService.validatePurchaseAmount]`, {
            type, 
            amountCents,
            transactionFee,
            correlationId,
        });

        const limits = await this.limits.limitsForPurchase(amountCents, transactionFee);

        const limit = limits.find((l) => l.transactionType === (type || BazaNcPurchaseFlowTransactionType.ACH));

        if (!limit || !limit.isAvailable) {
            throw new BazaNcPurchaseFlowLimitsViolationException();
        }
    }
}
