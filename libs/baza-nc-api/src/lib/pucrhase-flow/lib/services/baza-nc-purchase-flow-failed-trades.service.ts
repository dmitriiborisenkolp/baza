import { BazaNcTransactionRepository, BazaNcOfferingRepository, BazaNcOfferingEntity } from '../../../typeorm';
import { Injectable, Logger } from '@nestjs/common';
import { NorthCapitalTradesApiNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountRepository, BazaNcTransactionEntity } from '../../../typeorm';
import { BazaRegistryService } from '@scaliolabs/baza-core-api';
import { BazaNcTradeDeletedEvent, mapTransactionState, OrderStatus } from '@scaliolabs/baza-nc-shared';
import { MailService } from '@scaliolabs/baza-core-api';
import { BazaRegistryNodeEmailRecipientValue, replaceTags } from '@scaliolabs/baza-core-shared';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcMail } from '../../../../constants/baza-nc.mail-templates';

@Injectable()
export class BazaNcPurchaseFlowFailedTradesService {
    constructor(
        private readonly offeringRepository: BazaNcOfferingRepository,
        private readonly transactionRepository: BazaNcTransactionRepository,
        private readonly ncTradesApi: NorthCapitalTradesApiNestjsService,
        private readonly registry: BazaRegistryService,
        private readonly mail: MailService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly eventBus: EventBus,
    ) {}

    async releaseSharesForOutDatedFailedTrades(): Promise<void> {
        const daysToReleaseRegistry: number = this.registry.getValue('bazaNc.daysToReleaseFailedTrades');
        const openOfferingsArr = [];

        if (daysToReleaseRegistry) {
            openOfferingsArr.push(...(await this.offeringRepository.getAllOpenOfferings()));
        }

        openOfferingsArr.push(...(await this.offeringRepository.getOpenOfferingsHavingDaysToRelease()));

        for (const offering of openOfferingsArr) {
            await this.handleReleasingFailedTransactions({
                ...offering,
                daysToReleaseFailedTrades: offering.daysToReleaseFailedTrades ?? daysToReleaseRegistry,
            });
        }
    }

    private async handleReleasingFailedTransactions(offering: BazaNcOfferingEntity): Promise<void> {
        const transactions = await this.transactionRepository.findFailedTransactionsForOffering(offering);

        await Promise.all(
            transactions.map(async (transaction) => {
                await this.ncTradesApi.deleteTrade({
                    accountId: transaction.ncAccountId,
                    tradeId: transaction.ncTradeId,
                    errDesc: `releasing shares after ${offering.daysToReleaseFailedTrades} days for failed payment`,
                });

                transaction.ncOrderStatus = OrderStatus.Canceled;
                transaction.state = mapTransactionState.find((c) => c.match.includes(OrderStatus.Canceled)).transactionState;

                await this.transactionRepository.save(transaction);

                await this.sendEmailNotification(transaction);

                await this.eventBus.publish(
                    new BazaNcTradeDeletedEvent({
                        ncOfferingId: transaction.ncOfferingId,
                        ncTradeId: transaction.ncTradeId,
                    }),
                );

                Logger.log(`[PurchaseFlowTradesSessionService] Trade "${transaction.ncTradeId}" was deleted from NC`);
            }),
        );
    }

    private async sendEmailNotification(transaction: BazaNcTransactionEntity) {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByNcAccountId(transaction.ncAccountId);

        const admin: BazaRegistryNodeEmailRecipientValue = this.registry.getValue('adminEmail');

        const variables = {
            investorName: investorAccount.user.fullName,
            email: admin.email,
        };

        await this.mail.send({
            name: BazaNcMail.BazaNcTradeCanceled,
            uniqueId: `baza_nc_integrations_trade_cancelled_${transaction.ncTradeId}`,
            to: [
                admin,
                {
                    name: investorAccount.user.fullName,
                    email: investorAccount.user.email,
                },
            ],
            subject: replaceTags('baza-nc.mail.tradeCanceled.subject', {
                clientName: this.registry.getValue('bazaCommon.clientName'),
            }),
            variables: () => variables,
        });
    }
}
