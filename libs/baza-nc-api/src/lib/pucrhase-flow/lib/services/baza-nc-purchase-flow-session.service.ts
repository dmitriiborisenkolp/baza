import { Inject, Injectable } from '@nestjs/common';
import {
    BazaNcPurchaseFlowSessionCreated,
    BazaNcPurchaseFlowSessionSubmittedEvent,
    BazaNcPurchaseFlowSessionUpdated,
    convertToCents,
    DestroySessionDto,
    isTransactionStatusCancelled,
    mapTransactionState,
    OrderStatus,
    PurchaseFlowDto,
    PurchaseFlowSessionDto,
    BazaNcPurchaseFlowTransactionType,
    purchaseFlowTransactionTypeToNcTransactionTypeMap,
    SubmitPurchaseFlowDto,
    bazaNcOfferingName,
    PurchaseFlowCurrentDto,
    OfferingId,
    NorthCapitalErrorCodes,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowAmountMismatchException } from '../exceptions/baza-nc-purchase-flow-amount-mismatch.exception';
import { BazaNcPurchaseFlowNoNcAccountException } from '../exceptions/baza-nc-purchase-flow-no-nc-account.exception';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcPurchaseFlowStatsService } from './baza-nc-purchase-flow-stats.service';
import { BazaNcPurchaseFlowTradesSessionService } from './baza-nc-purchase-flow-trades-session.service';
import { BazaNcPurchaseFlowDocusignService } from './baza-nc-purchase-flow-docusign.service';
import { BazaNcPurchaseFlowLimitsService } from './baza-nc-purchase-flow-limits.service';
import { BazaNcPurchaseFlowLimitsViolationException } from '../exceptions/baza-nc-purchase-flow-limits-violation.exception';
import { BazaNcPurchaseFlowUnknownTransactionTypeException } from '../exceptions/baza-nc-purchase-flow-unknown-transaction-type.exception';
import { NorthCapitalException, NorthCapitalPartiesApiNestjsService, NorthCapitalTradesApiNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountEntity, BazaNcTradesSessionEntity, BazaNcTradesSessionRepository } from '../../../typeorm';
import { BazaNcOfferingsPercentsFundedService, BazaNcOfferingsService, BazaNcOfferingTransactionFeesService } from '../../../offering';
import { BazaNcPurchaseFlowSessionValidatorService } from './baza-nc-purchase-flow-session-validator.service';
import { PURCHASE_FLOW_API_MODULE_CONFIG, PurchaseFlowApiModuleConfiguration } from '../baza-nc-purchase-flow-api.config';
import { BazaLogger, BazaSessionLockService } from '@scaliolabs/baza-core-api';
import { ModuleRef } from '@nestjs/core';
import { BAZA_NC_PURCHASE_FLOW_STRATEGIES_MAP, BazaNcPurchaseFlowStrategy } from '../strategies';
import { BazaNcPurchaseFlowUnknownStrategyException } from '../exceptions/baza-nc-purchase-flow-unknown-strategy.exception';
import { BazaNcAccountVerificationAccountSetupService } from '../../../account-verification';
import { BazaNcPurchaseFlowNotEnoughSharesException } from '../exceptions/baza-nc-purchase-flow-not-enough-shares.exception';

/**
 * Purchase Flow Session Service
 * Main Service for Purchasing Shares using different methods
 * The service has 2 core methods:
 *  - `session` - will start Purchase Flow Session. Purchase Flow will automatically expires after 30 minutes inactivity
 *  - `submit` - will submit Purchase Flow Session. All actual payments will be performed here.
 */
@Injectable()
export class BazaNcPurchaseFlowSessionService {
    constructor(
        @Inject(PURCHASE_FLOW_API_MODULE_CONFIG) private readonly moduleConfig: PurchaseFlowApiModuleConfiguration,
        private readonly logger: BazaLogger,
        private readonly eventBus: EventBus,
        private readonly moduleRef: ModuleRef,
        private readonly stats: BazaNcPurchaseFlowStatsService,
        private readonly docuSign: BazaNcPurchaseFlowDocusignService,
        private readonly limits: BazaNcPurchaseFlowLimitsService,
        private readonly ncOffering: BazaNcOfferingsService,
        private readonly ncTradesRepository: BazaNcTradesSessionRepository,
        private readonly tradesSessions: BazaNcPurchaseFlowTradesSessionService,
        private readonly transactionFees: BazaNcOfferingTransactionFeesService,
        private readonly sessionValidator: BazaNcPurchaseFlowSessionValidatorService,
        private readonly sessionLock: BazaSessionLockService,
        private readonly ncPartiesApi: NorthCapitalPartiesApiNestjsService,
        private readonly ncTradesApi: NorthCapitalTradesApiNestjsService,
        private readonly ncAccountSetup: BazaNcAccountVerificationAccountSetupService,
        private readonly percentsFundedService: BazaNcOfferingsPercentsFundedService,
    ) {}

    /**
     * Returns current Purchase Flow Session for given Offering Id
     * @param investorAccount
     * @param offeringId
     */
    async current(investorAccount: BazaNcInvestorAccountEntity, offeringId: OfferingId): Promise<PurchaseFlowCurrentDto> {
        const isAvailable = await this.tradesSessions.hasActiveTradeByAccountAndOfferingId({
            accountId: investorAccount.northCapitalAccountId,
            offeringId,
        });

        if (isAvailable) {
            const tradeSession = await this.tradesSessions.getActiveTradeByAccountAndOfferingId({
                accountId: investorAccount.northCapitalAccountId,
                offeringId,
            });

            const docuSign = await this.docuSign.sessionDocuSign(investorAccount, tradeSession, {
                requestDocuSignUrl: false,
            });

            return {
                isAvailable,
                session: {
                    id: tradeSession.tradeId,
                    transactionType: tradeSession.transactionType,
                    amount: tradeSession.amountCents,
                    transactionFeesCents: tradeSession.transactionFeesCents,
                    transactionFees: tradeSession.transactionFees,
                    fee: tradeSession.feeCents,
                    total: tradeSession.totalCents,
                    numberOfShares: tradeSession.numberOfShares,
                    willBeActiveUpTo: tradeSession.activeTo.toISOString(),
                    areDocumentsSigned: docuSign.areDocumentsSigned,
                    updateId: '',
                },
            };
        } else {
            return {
                isAvailable,
            };
        }
    }

    /**
     * Starts Purchase Flow Session
     * @param ip
     * @param investorAccount
     * @param request
     */
    async session(
        ip: string,
        investorAccount: BazaNcInvestorAccountEntity,
        request: PurchaseFlowSessionDto,
        correlationId?: string,
    ): Promise<PurchaseFlowDto> {
        this.logger.info(`[BazaNcPurchaseFlowSessionService.session]`, {
            user: investorAccount.userId,
            correlationId,
            request,
        });

        const response = await this.sessionLock.run(
            async () => {
                await this.sessionValidator.validateOffering(request.offeringId, correlationId);

                const ncAccountId = investorAccount.northCapitalAccountId;

                let transactionFee = 0;
                let tradeSession: BazaNcTradesSessionEntity;

                let hasActiveTradeSession = await this.tradesSessions.hasActiveTradeByAccountAndOfferingId(
                    {
                        accountId: ncAccountId,
                        offeringId: request.offeringId,
                    },
                    correlationId,
                );

                if (hasActiveTradeSession) {
                    tradeSession = await this.tradesSessions.getActiveTradeByAccountAndOfferingId(
                        {
                            accountId: investorAccount.northCapitalAccountId,
                            offeringId: request.offeringId,
                        },
                        correlationId,
                    );

                    const tradeStatusResponse = await this.ncTradesApi.getTradeStatus(
                        {
                            tradeId: tradeSession.tradeId,
                        },
                        correlationId,
                    );

                    const isTradeSessionAmountMatches = tradeSession.amountCents === request.amount;
                    const isTradeSessionIsInCreatedStatus = tradeStatusResponse.tradeDetails[0].orderStatus === OrderStatus.Created;

                    if (!(isTradeSessionAmountMatches && isTradeSessionIsInCreatedStatus)) {
                        await this.tradesSessions.markTradesSessionAsOutdated(
                            {
                                accountId: ncAccountId,
                                offeringId: request.offeringId,
                            },
                            correlationId,
                        );

                        await this.tradesSessions.deleteAllTradeSessionsMarkedAsNCDelete();

                        hasActiveTradeSession = false;
                    }
                }

                await this.sessionValidator.validate(investorAccount, request, correlationId);

                if (this.transactionFees.shouldChargeTransactionFees) {
                    const offering = await this.ncOffering.getOffering(request.offeringId);

                    transactionFee = offering.ncOfferingFees || 0;
                }

                await this.sessionValidator.validatePurchaseAmount(request.transactionType, request.amount, transactionFee, correlationId);

                return hasActiveTradeSession
                    ? await this.sessionUpdate(investorAccount, request, correlationId)
                    : await this.sessionStart(ip, investorAccount, request, correlationId);
            },
            {
                prefix: 'BAZA_NC_PURCHASE_FLOW',
                sessionTtlSeconds: this.moduleConfig.tradesTTLSec,
                accountId: investorAccount.user.id,
                sessionUpdateId: request.updateId,
            },
        );

        return {
            ...response.result,
            updateId: response.sessionUpdateId,
        };
    }

    /**
     * Submits existing Purchase Flow Session
     * @param ip
     * @param investorAccount
     * @param request
     */
    async submit(ip: string, investorAccount: BazaNcInvestorAccountEntity, request: SubmitPurchaseFlowDto): Promise<void> {
        const response = await this.sessionLock.run(
            async () => {
                const session = await this.tradesSessions.getActiveTradeSessionByTradeId({
                    tradeId: request.id,
                });

                await this.sessionValidator.validateOffering(session.offeringId);
                await this.sessionValidator.validatePurchaseAmount(session.transactionType, session.amountCents, session.transactionFees);
                await this.sessionValidator.validateInvestorPurchase(investorAccount, session.offeringId);

                const offering = await this.ncOffering.getOffering(session.offeringId);

                const ncAccountId = investorAccount.northCapitalAccountId;
                const ncOfferingId = session.offeringId;

                const tradeSession = await this.tradesSessions.getActiveTradeByAccountAndOfferingId({
                    accountId: ncAccountId,
                    offeringId: ncOfferingId,
                });

                const tradeStatus = (
                    await this.ncTradesApi.getTradeStatus({
                        tradeId: tradeSession.tradeId,
                    })
                ).tradeDetails[0].transactionstatus;

                if (isTransactionStatusCancelled(tradeStatus)) {
                    await this.tradesSessions.markTradesSessionAsOutdated({
                        accountId: ncAccountId,
                        offeringId: ncOfferingId,
                    });

                    await this.tradesSessions.deleteAllTradeSessionsMarkedAsNCDelete();
                }

                const strategyDef = BAZA_NC_PURCHASE_FLOW_STRATEGIES_MAP.find((next) => next.type === tradeSession.transactionType);

                if (!strategyDef) {
                    throw new BazaNcPurchaseFlowUnknownStrategyException();
                }

                if (tradeSession.transactionFees) {
                    tradeSession.feeCents += tradeSession.transactionFeesCents;
                }

                await (this.moduleRef.get(strategyDef.strategy) as BazaNcPurchaseFlowStrategy).purchase({
                    ip,
                    investorAccount,
                    offeringId: offering.ncOfferingId,
                    tradeId: tradeSession.tradeId,
                    transactionFees: tradeSession.transactionFees,
                    transactionFeesCents: tradeSession.transactionFeesCents,
                    amountCents: tradeSession.amountCents,
                });

                await this.eventBus.publish(
                    new BazaNcPurchaseFlowSessionSubmittedEvent({
                        userId: investorAccount.user.id,
                        ncAccountId: investorAccount.northCapitalAccountId,
                        ncPartyId: investorAccount.northCapitalPartyId,
                        ncTradeId: tradeSession.tradeId,
                        ncOfferingId: offering.ncOfferingId,
                        ncOfferingName: bazaNcOfferingName(offering),
                        amountAsCents: tradeSession.amountCents,
                        transactionFees: tradeSession.transactionFees,
                        transactionFeesCents: tradeSession.transactionFeesCents,
                        feeAsCents: tradeSession.feeCents,
                        totalAsCents: tradeSession.totalCents,
                        numberOfShares: Math.ceil(tradeSession.numberOfShares),
                        pricePerSharesAsCents: tradeSession.pricePerShareCents,
                        ncOrderStatus: OrderStatus.Created,
                        state: mapTransactionState.find((c) => c.match.includes(OrderStatus.Pending)).transactionState,
                    }),
                );

                await this.tradesSessions.completeTrade(tradeSession.tradeId);
            },
            {
                prefix: 'BAZA_NC_PURCHASE_FLOW',
                sessionTtlSeconds: this.moduleConfig.tradesTTLSec,
                accountId: investorAccount.user.id,
                sessionUpdateId: request.updateId,
            },
        );

        return response.result;
    }

    /**
     * Destroys Purchase Flow Session (With Session Lock wrapper)
     * @param investorAccount
     * @param request
     */
    async destroy(investorAccount: BazaNcInvestorAccountEntity, request: DestroySessionDto): Promise<void> {
        await this.sessionLock.run(
            async () => {
                await this.destroySession(investorAccount, request);
            },
            {
                prefix: 'BAZA_NC_PURCHASE_FLOW',
                sessionTtlSeconds: this.moduleConfig.tradesTTLSec,
                accountId: investorAccount.user.id,
                sessionUpdateId: request.updateId,
            },
        );
    }

    /**
     * Destroys Purchase Flow Session
     * @param investorAccount
     * @param request
     */
    private async destroySession(
        investorAccount: BazaNcInvestorAccountEntity,
        request: DestroySessionDto,
        correlationId?: string,
    ): Promise<void> {
        this.logger.info(`[BazaNcPurchaseFlowSessionService.destroySession]`, {
            user: investorAccount.userId,
            investorAccountId: investorAccount.id,
            request,
            correlationId,
        });

        if (!investorAccount.northCapitalAccountId) {
            throw new BazaNcPurchaseFlowNoNcAccountException();
        }

        const ncAccountId = investorAccount.northCapitalAccountId;

        const hasTradeSession = await this.tradesSessions.hasActiveTradeByAccountAndOfferingId({
            accountId: ncAccountId,
            offeringId: request.offeringId,
        });

        if (hasTradeSession) {
            await this.tradesSessions.markTradesSessionAsOutdated({
                accountId: ncAccountId,
                offeringId: request.offeringId,
            });

            await this.tradesSessions.deleteAllTradeSessionsMarkedAsNCDelete();
        }
    }

    /**
     * Starts new Purchase Flow Session
     * @param ip
     * @param investorAccount
     * @param request
     * @param correlationId
     */
    async sessionStart(
        ip: string,
        investorAccount: BazaNcInvestorAccountEntity,
        request: PurchaseFlowSessionDto,
        correlationId?: string,
    ): Promise<PurchaseFlowDto> {
        this.logger.info(`[BazaNcPurchaseFlowSessionService.sessionStart]`, {
            user: investorAccount.userId,
            investorAccountId: investorAccount.id,
            request,
            correlationId,
        });

        if (!request.transactionType) {
            const hasOnlyLinkedCreditCard = investorAccount.isCreditCardLinked && !investorAccount.isBankAccountLinked;

            request.transactionType = hasOnlyLinkedCreditCard
                ? BazaNcPurchaseFlowTransactionType.CreditCard
                : BazaNcPurchaseFlowTransactionType.ACH;
        }

        await this.destroySession(
            investorAccount,
            {
                offeringId: request.offeringId,
            },
            correlationId,
        );

        const offering = await this.ncOffering.syncWithNcOffering(request.offeringId, correlationId);

        if (convertToCents(offering.ncUnitPrice * request.numberOfShares) !== request.amount) {
            throw new BazaNcPurchaseFlowAmountMismatchException();
        }

        const transactionTypeDef = purchaseFlowTransactionTypeToNcTransactionTypeMap.find(
            (def) => def.purchaseFlowTransactionType === request.transactionType,
        );

        if (!transactionTypeDef) {
            throw new BazaNcPurchaseFlowUnknownTransactionTypeException();
        }

        const ncAccountId = investorAccount.northCapitalAccountId;
        const ncPartyId = investorAccount.northCapitalPartyId;

        await this.tradesSessions.markTradesSessionAsOutdated(
            {
                accountId: ncAccountId,
                offeringId: request.offeringId,
            },
            correlationId,
        );

        await this.ncAccountSetup.updateAccountCustomFields(ncAccountId);
        await this.ncAccountSetup.updatePartyCustomFields(ncPartyId);

        const trade = await (async () => {
            try {
                return this.ncTradesApi.createTrade(
                    {
                        accountId: ncAccountId,
                        offeringId: request.offeringId,
                        transactionType: transactionTypeDef.ncTransactionType,
                        transactionUnits: request.numberOfShares.toString(),
                        createdIpAddress: ip,
                    },
                    correlationId,
                );
            } catch (error) {
                if (error instanceof NorthCapitalException && error.ncStatusCode === NorthCapitalErrorCodes.OfferDoesNotHaveEnoughShares) {
                    this.percentsFundedService.updatePercentsFunded(request.offeringId).catch((err) => {
                        this.logger.error(`Failed to sync offering details for Offering ${request.offeringId}:`);
                        this.logger.error(err);
                    });

                    throw new BazaNcPurchaseFlowNotEnoughSharesException();
                } else {
                    throw error;
                }
            }
        })();

        const tradeId = trade.purchaseDetails[1][0].tradeId;

        const transactionFeesCents = this.getTransactionFeesCents(request.amount, offering.ncOfferingFees, correlationId);
        const limits = await this.limits.limitsForPurchase(request.amount, offering.ncOfferingFees, correlationId);
        const feeCents = limits.find((l) => l.transactionType === transactionTypeDef.purchaseFlowTransactionType).feeCents;

        const tradeSession = await this.tradesSessions.createSessionForTrade(
            {
                tradeId,
                accountId: ncAccountId,
                offeringId: request.offeringId,
                amountCents: request.amount,
                feeCents: feeCents,
                totalCents: request.amount + feeCents + transactionFeesCents,
                transactionFees: offering.ncOfferingFees,
                transactionFeesCents,
                numberOfShares: request.numberOfShares,
                pricePerSharesCents: convertToCents(offering.ncUnitPrice),
                transactionType: request.transactionType,
            },
            correlationId,
        );

        const docuSign = await this.docuSign.sessionDocuSign(
            investorAccount,
            tradeSession,
            {
                requestDocuSignUrl: request.requestDocuSignUrl,
            },
            correlationId,
        );

        await this.eventBus.publish(
            new BazaNcPurchaseFlowSessionCreated({
                userId: investorAccount.user.id,
                ncAccountId: investorAccount.northCapitalAccountId,
                ncPartyId: investorAccount.northCapitalPartyId,
                ncTradeId: tradeSession.tradeId,
                ncOfferingId: offering.ncOfferingId,
                ncOfferingName: bazaNcOfferingName(offering),
                amountAsCents: tradeSession.amountCents,
                transactionFeesCents: tradeSession.transactionFeesCents,
                transactionFees: tradeSession.transactionFees,
                feeAsCents: tradeSession.feeCents,
                totalAsCents: tradeSession.totalCents,
                numberOfShares: Math.ceil(tradeSession.numberOfShares),
                pricePerSharesAsCents: tradeSession.pricePerShareCents,
                ncOrderStatus: OrderStatus.Created,
                state: mapTransactionState.find((c) => c.match.includes(OrderStatus.Created)).transactionState,
                transactionType: request.transactionType,
            }),
        );

        return {
            id: tradeSession.tradeId,
            transactionType: tradeSession.transactionType,
            amount: tradeSession.amountCents,
            transactionFeesCents: tradeSession.transactionFeesCents,
            transactionFees: tradeSession.transactionFees,
            fee: tradeSession.feeCents,
            total: tradeSession.totalCents,
            numberOfShares: tradeSession.numberOfShares,
            willBeActiveUpTo: tradeSession.activeTo.toISOString(),
            areDocumentsSigned: docuSign.areDocumentsSigned,
            docuSignUrl: docuSign.docuSignUrl,
            updateId: '',
        };
    }

    /**
     * Updates existing Purchase Flow Session
     * @param investorAccount
     * @param request
     * @private
     */
    private async sessionUpdate(
        investorAccount: BazaNcInvestorAccountEntity,
        request: PurchaseFlowSessionDto,
        correlationId?: string,
    ): Promise<PurchaseFlowDto> {
        this.logger.info(`[BazaNcPurchaseFlowSessionService.sessionUpdate]`, {
            user: investorAccount.userId,
            investorAccountId: investorAccount.id,
            request,
            correlationId,
        });

        const tradeSession = await this.tradesSessions.getActiveTradeByAccountAndOfferingId({
            accountId: investorAccount.northCapitalAccountId,
            offeringId: request.offeringId,
        });

        await this.tradesSessions.touchSessionForTrade(tradeSession.tradeId);

        const tradeStatus = (
            await this.ncTradesApi.getTradeStatus(
                {
                    tradeId: tradeSession.tradeId,
                },
                correlationId,
            )
        ).tradeDetails[0].transactionstatus;

        if (isTransactionStatusCancelled(tradeStatus)) {
            await this.tradesSessions.markTradesSessionAsOutdated(
                {
                    accountId: investorAccount.northCapitalAccountId,
                    offeringId: request.offeringId,
                },
                correlationId,
            );

            await this.tradesSessions.deleteAllTradeSessionsMarkedAsNCDelete(correlationId);
        }

        tradeSession.transactionFeesCents = this.getTransactionFeesCents(
            tradeSession.amountCents,
            tradeSession.transactionFees,
            correlationId,
        );

        if (request.transactionType) {
            if (request.transactionType !== tradeSession.transactionType) {
                const transactionTypeDef = purchaseFlowTransactionTypeToNcTransactionTypeMap.find(
                    (def) => def.purchaseFlowTransactionType === (request.transactionType || BazaNcPurchaseFlowTransactionType.ACH),
                );

                if (!transactionTypeDef) {
                    throw new BazaNcPurchaseFlowUnknownTransactionTypeException();
                }

                const limits = (await this.limits.limitsForPurchase(request.amount, tradeSession.transactionFees, correlationId)).find(
                    (l) => l.transactionType === transactionTypeDef.purchaseFlowTransactionType,
                );

                if (!limits || !limits.isAvailable) {
                    throw new BazaNcPurchaseFlowLimitsViolationException();
                }

                await this.ncTradesApi.updateTradeTransactionType(
                    {
                        tradeId: tradeSession.tradeId,
                        transactionType: transactionTypeDef.ncTransactionType,
                    },
                    correlationId,
                );

                tradeSession.transactionType = transactionTypeDef.purchaseFlowTransactionType;
                tradeSession.feeCents = limits.feeCents;
                tradeSession.totalCents = limits.feeCents + tradeSession.amountCents + tradeSession.transactionFeesCents;

                await this.ncTradesRepository.save(tradeSession);
            }
        }

        const docuSign = await this.docuSign.sessionDocuSign(
            investorAccount,
            tradeSession,
            {
                requestDocuSignUrl: request.requestDocuSignUrl,
            },
            correlationId,
        );

        await this.eventBus.publish(
            new BazaNcPurchaseFlowSessionUpdated({
                ncTradeId: tradeSession.tradeId,
                amountAsCents: tradeSession.amountCents,
                feeAsCents: tradeSession.feeCents,
                totalAsCents: tradeSession.totalCents,
                numberOfShares: Math.ceil(tradeSession.numberOfShares),
                pricePerSharesAsCents: tradeSession.pricePerShareCents,
                transactionType: request.transactionType,
            }),
        );

        return {
            id: tradeSession.tradeId,
            transactionType: tradeSession.transactionType,
            amount: tradeSession.amountCents,
            transactionFeesCents: tradeSession.transactionFeesCents,
            transactionFees: tradeSession.transactionFees,
            fee: tradeSession.feeCents,
            total: tradeSession.feeCents + tradeSession.amountCents + tradeSession.transactionFeesCents,
            numberOfShares: tradeSession.numberOfShares,
            willBeActiveUpTo: tradeSession.activeTo.toISOString(),
            areDocumentsSigned: docuSign.areDocumentsSigned,
            docuSignUrl: docuSign.docuSignUrl,
            updateId: '',
        };
    }

    /**
     * Calculates Transaction Fees
     * @param amount
     * @param fees
     * @param correlationId
     * @private
     */
    private getTransactionFeesCents(amount: number, fees: number, correlationId?: string): number {
        this.logger.info(`[BazaNcPurchaseFlowSessionService.getTransactionFeesCents]`, {
            amount,
            fees,
            correlationId,
        });

        let transactionFeesCents = 0;

        if (this.transactionFees.shouldChargeTransactionFees) {
            transactionFeesCents = this.transactionFees.percentageIncrementedFees(amount, fees) - amount;
        }

        return transactionFeesCents;
    }
}
