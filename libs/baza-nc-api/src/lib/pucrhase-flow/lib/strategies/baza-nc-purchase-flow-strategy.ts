import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';

export interface BazaNcPurchaseFlowServiceRequest {
    ip: string;
    investorAccount: BazaNcInvestorAccountEntity;
    offeringId: OfferingId;
    tradeId: TradeId;
    amountCents: number;
    transactionFees: number;
    transactionFeesCents: number;
}

export interface BazaNcPurchaseFlowStrategy {
    /**
     * Purchase Shares (for /baza-nc/purchase-flow/submit)
     */
    purchase(request: BazaNcPurchaseFlowServiceRequest): Promise<void>;

    /**
     * Purchase Shares (for /baza-nc/purchase-flow/reprocessPayment)
     */
    reprocess(request: BazaNcPurchaseFlowServiceRequest): Promise<void>;
}
