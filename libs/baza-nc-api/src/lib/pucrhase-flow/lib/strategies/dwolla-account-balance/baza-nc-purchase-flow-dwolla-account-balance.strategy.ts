import { BazaNcPurchaseFlowServiceRequest, BazaNcPurchaseFlowStrategy } from '../baza-nc-purchase-flow-strategy';
import { Injectable } from '@nestjs/common';
import { BazaNcPurchaseFlowNoDwollaCustomerException } from '../../exceptions/baza-nc-purchase-flow-no-dwolla-customer.exception';
import { BazaNcDwollaCustomerService } from '../../../../dwolla';
import { BazaNcPurchaseFlowTransactionType, convertFromCents } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowDwollaInsufficientFundsException } from '../../exceptions/baza-nc-purchase-flow-dwolla-insufficient-funds.exception';
import { BazaNcPurchaseFlowLimitsService } from '../../services/baza-nc-purchase-flow-limits.service';
import {
    BazaDwollaPaymentService,
    DwollaConfigurationNestjsService,
    DwollaWebhookSandboxSimulationApiNestjsService,
} from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';
import { BazaRegistryService } from '@scaliolabs/baza-core-api';

/**
 * Purchase Flow Strategy - using Dwolla Account Balance
 */
@Injectable()
export class BazaNcPurchaseFlowDwollaAccountBalanceStrategy implements BazaNcPurchaseFlowStrategy {
    constructor(
        private readonly registry: BazaRegistryService,
        private readonly ncDwollaCustomerService: BazaNcDwollaCustomerService,
        private readonly dwollaPaymentService: BazaDwollaPaymentService,
        private readonly ncLimitsService: BazaNcPurchaseFlowLimitsService,
        private readonly dwollaConfig: DwollaConfigurationNestjsService,
        private readonly dwollaWebhookSandboxSimulation: DwollaWebhookSandboxSimulationApiNestjsService,
    ) {}

    /**
     * Purchase shares using Dwolla Account Balance
     * @param request
     */
    async purchase(request: BazaNcPurchaseFlowServiceRequest): Promise<void> {
        const { investorAccount, amountCents, transactionFees, tradeId } = request;

        if (!investorAccount.dwollaCustomerId) {
            throw new BazaNcPurchaseFlowNoDwollaCustomerException();
        }

        const balance = await this.ncDwollaCustomerService.getDwollaCustomerBalance(investorAccount.user.id);

        if (!balance.isDwollaAvailable || parseFloat(balance.balance.value) < convertFromCents(request.amountCents)) {
            throw new BazaNcPurchaseFlowDwollaInsufficientFundsException();
        }

        const limits = (await this.ncLimitsService.limitsForPurchase(amountCents, transactionFees)).find(
            (l) => l.transactionType === BazaNcPurchaseFlowTransactionType.AccountBalance,
        );

        await this.dwollaPaymentService.transfer({
            accountId: investorAccount.user.id,
            dwollaCustomerId: investorAccount.dwollaCustomerId,
            amountCents: amountCents + limits.transactionFeesCents + limits.feeCents,
            payload: {
                type: BazaDwollaPaymentType.PurchaseShares,
                ncTradeId: tradeId,
            },
        });

        if (this.dwollaConfig.configuration.isSandbox && this.registry.getValue('bazaDwolla.enableWebhookSimulations')) {
            await this.dwollaWebhookSandboxSimulation.sandboxWebhookSimulations();
        }
    }

    /**
     * Reprocess payment using Dwolla Account Balance
     * @param request
     */
    async reprocess(request: BazaNcPurchaseFlowServiceRequest): Promise<void> {
        await this.purchase(request);
    }
}
