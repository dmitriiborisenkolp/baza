import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DwollaPaymentCompletedEvent } from '@scaliolabs/baza-dwolla-api';
import { BazaNcTransactionRepository } from '../../../../../typeorm';
import { BazaDwollaPaymentPurchaseSharesPayload, BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';
import { OrderStatus } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalTradesApiNestjsService } from '../../../../../transact-api';
import { cqrs } from '@scaliolabs/baza-core-api';

/**
 * Transfer Completed - marks Trade as Funded if related Dwolla Payment is
 * completed
 */
@EventsHandler(DwollaPaymentCompletedEvent)
export class TransferCompletedEventHandler implements IEventHandler<DwollaPaymentCompletedEvent> {
    constructor(
        private readonly repository: BazaNcTransactionRepository,
        private readonly ncTradeApi: NorthCapitalTradesApiNestjsService,
    ) {}

    handle(event: DwollaPaymentCompletedEvent): void {
        cqrs(TransferCompletedEventHandler.name, async () => {
            if (event.payload.payment.type !== BazaDwollaPaymentType.PurchaseShares) {
                return;
            }

            const tradeId = (event.payload.payment.payload as BazaDwollaPaymentPurchaseSharesPayload).ncTradeId;
            const trade = await this.repository.findByNcTradeId(tradeId);

            if (!trade) {
                return;
            }

            await this.ncTradeApi.updateTradeStatus({
                tradeId,
                accountId: trade.ncAccountId,
                orderStatus: OrderStatus.Funded,
            });
        });
    }
}
