import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DwollaPaymentCancelledEvent } from '@scaliolabs/baza-dwolla-api';
import { BazaNcTransactionRepository } from '../../../../../typeorm';
import { BazaDwollaPaymentPurchaseSharesPayload, BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';
import { OrderStatus } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalTradesApiNestjsService } from '../../../../../transact-api';
import { cqrs } from '@scaliolabs/baza-core-api';

/**
 * Transfer Cancelled - marks Trade as Funded if related Dwolla Payment is
 * cancelled
 */
@EventsHandler(DwollaPaymentCancelledEvent)
export class TransferCancelledEventHandler implements IEventHandler<DwollaPaymentCancelledEvent> {
    constructor(
        private readonly repository: BazaNcTransactionRepository,
        private readonly ncTradeApi: NorthCapitalTradesApiNestjsService,
    ) {}

    handle(event: DwollaPaymentCancelledEvent): void {
        cqrs(TransferCancelledEventHandler.name, async () => {
            if (event.payload.payment.type === BazaDwollaPaymentType.PurchaseShares) {
                const tradeId = (event.payload.payment.payload as BazaDwollaPaymentPurchaseSharesPayload).ncTradeId;
                const trade = await this.repository.findByNcTradeId(tradeId);

                if (!trade) {
                    return;
                }

                await this.ncTradeApi.updateTradeStatus({
                    tradeId,
                    accountId: trade.ncAccountId,
                    orderStatus: OrderStatus.Canceled,
                });
            }
        });
    }
}
