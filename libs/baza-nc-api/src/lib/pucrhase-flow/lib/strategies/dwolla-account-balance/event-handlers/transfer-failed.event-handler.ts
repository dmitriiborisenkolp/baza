import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DwollaPaymentFailedEvent } from '@scaliolabs/baza-dwolla-api';
import { BazaNcTransactionRepository } from '../../../../../typeorm';
import { BazaDwollaPaymentPurchaseSharesPayload, BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcTransactionReturnedEvent, OrderStatus } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalTradesApiNestjsService } from '../../../../../transact-api';
import { cqrs } from '@scaliolabs/baza-core-api';

/**
 * Transfer Failed - marks Trade as Funded if related Dwolla Payment is
 * failed
 */
@EventsHandler(DwollaPaymentFailedEvent)
export class TransferFailedEventHandler implements IEventHandler<DwollaPaymentFailedEvent> {
    constructor(
        private readonly eventBus: EventBus,
        private readonly repository: BazaNcTransactionRepository,
        private readonly ncTradeApi: NorthCapitalTradesApiNestjsService,
    ) {}

    handle(event: DwollaPaymentFailedEvent): void {
        cqrs(TransferFailedEventHandler.name, async () => {
            if (event.payload.payment.type !== BazaDwollaPaymentType.PurchaseShares) {
                return;
            }

            const tradeId = (event.payload.payment.payload as BazaDwollaPaymentPurchaseSharesPayload).ncTradeId;
            const trade = await this.repository.findByNcTradeId(tradeId);

            if (!trade) {
                return;
            }

            await this.ncTradeApi.updateTradeStatus({
                tradeId,
                accountId: trade.ncAccountId,
                orderStatus: OrderStatus.Rejected,
            });

            await this.eventBus.publish(
                new BazaNcTransactionReturnedEvent({
                    id: trade.id,
                    ncOfferingId: trade.ncOfferingId,
                    ncTradeId: trade.ncTradeId,
                }),
            );
        });
    }
}
