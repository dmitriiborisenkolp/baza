import { ComponentType } from '@scaliolabs/baza-core-shared';
import { BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowStrategy } from './baza-nc-purchase-flow-strategy';
import { BazaNcPurchaseFlowNcAchStrategy } from './nc-ach/baza-nc-purchase-flow-nc-ach.strategy';
import { BazaNcPurchaseFlowNcCreditCardStrategy } from './nc-credit-card/baza-nc-purchase-flow-nc-credit-card.strategy';
import { BazaNcPurchaseFlowDwollaAccountBalanceStrategy } from './dwolla-account-balance/baza-nc-purchase-flow-dwolla-account-balance.strategy';

export * from './baza-nc-purchase-flow-strategy';

export * from './nc-ach/baza-nc-purchase-flow-nc-ach.strategy';
export * from './nc-credit-card/baza-nc-purchase-flow-nc-credit-card.strategy';
export * from './dwolla-account-balance/baza-nc-purchase-flow-dwolla-account-balance.strategy';

export const BAZA_NC_PURCHASE_FLOW_STRATEGIES_MAP: Array<{
    type: BazaNcPurchaseFlowTransactionType;
    strategy: ComponentType<BazaNcPurchaseFlowStrategy>;
}> = [
    { type: BazaNcPurchaseFlowTransactionType.ACH, strategy: BazaNcPurchaseFlowNcAchStrategy },
    { type: BazaNcPurchaseFlowTransactionType.CreditCard, strategy: BazaNcPurchaseFlowNcCreditCardStrategy },
    { type: BazaNcPurchaseFlowTransactionType.AccountBalance, strategy: BazaNcPurchaseFlowDwollaAccountBalanceStrategy },
];
