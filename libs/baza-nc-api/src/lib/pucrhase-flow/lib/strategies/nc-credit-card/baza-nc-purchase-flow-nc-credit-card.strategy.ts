import { Injectable } from '@nestjs/common';
import { BazaNcPurchaseFlowServiceRequest, BazaNcPurchaseFlowStrategy } from '../baza-nc-purchase-flow-strategy';
import { BazaNcPurchaseFlowNoCreditCardException } from '../../exceptions/baza-nc-purchase-flow-no-credit-card.exception';
import { BazaNcPurchaseFlowTransactionType, convertFromCents } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowCreditCardService } from '../../services/baza-nc-purchase-flow-credit-card.service';
import { NorthCapitalCreditCardTransactionApiNestjsService } from '../../../../transact-api';
import { BazaNcPurchaseFlowLimitsService } from '../../services/baza-nc-purchase-flow-limits.service';

/**
 * Purchase Flow Strategy - NC Credit Card
 */
@Injectable()
export class BazaNcPurchaseFlowNcCreditCardStrategy implements BazaNcPurchaseFlowStrategy {
    constructor(
        private readonly creditCard: BazaNcPurchaseFlowCreditCardService,
        private readonly ncCreditCardApi: NorthCapitalCreditCardTransactionApiNestjsService,
        private readonly limits: BazaNcPurchaseFlowLimitsService,
    ) {}

    /**
     * Purchase Shares using NC Credit Card Method
     * @param request
     */
    async purchase(request: BazaNcPurchaseFlowServiceRequest): Promise<void> {
        const { investorAccount, amountCents, transactionFees, tradeId, ip } = request;

        if (!(await this.creditCard.hasCreditCard(investorAccount))) {
            throw new BazaNcPurchaseFlowNoCreditCardException();
        }

        const limits = (await this.limits.limitsForPurchase(amountCents, transactionFees)).find(
            (l) => l.transactionType === BazaNcPurchaseFlowTransactionType.CreditCard,
        );

        await this.ncCreditCardApi.cCFundMovement({
            tradeId,
            accountId: investorAccount.northCapitalAccountId,
            createdIpAddress: ip,
            amount: convertFromCents(amountCents + limits.transactionFeesCents + limits.feeCents).toFixed(2),
        });
    }

    /**
     * Reprocess payment using NC Credit Card Method
     * @param request
     */
    async reprocess(request: BazaNcPurchaseFlowServiceRequest): Promise<void> {
        await this.purchase(request);
    }
}
