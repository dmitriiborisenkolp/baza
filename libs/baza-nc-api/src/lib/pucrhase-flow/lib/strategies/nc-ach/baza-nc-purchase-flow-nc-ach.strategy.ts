import { Injectable } from '@nestjs/common';
import { BazaNcPurchaseFlowServiceRequest, BazaNcPurchaseFlowStrategy } from '../baza-nc-purchase-flow-strategy';
import { BazaNcPurchaseFlowNoBankAccountException } from '../../exceptions/baza-nc-purchase-flow-no-bank-account.exception';
import { convertFromCents } from '@scaliolabs/baza-nc-shared';
import { getBazaProjectName } from '@scaliolabs/baza-core-shared';
import { BazaNcPurchaseFlowBankAccountService } from '../../services/baza-nc-purchase-flow-bank-account.service';
import { NorthCapitalAchTransfersApiNestjsService } from '../../../../transact-api';

/**
 * Purchase Flow Strategy - NC ACH
 */
@Injectable()
export class BazaNcPurchaseFlowNcAchStrategy implements BazaNcPurchaseFlowStrategy {
    constructor(
        private readonly bank: BazaNcPurchaseFlowBankAccountService,
        private readonly ncACHTransfersApi: NorthCapitalAchTransfersApiNestjsService,
    ) {}

    /**
     * Purchase Shares using NC ACH Method
     * @param request
     */
    async purchase(request: BazaNcPurchaseFlowServiceRequest): Promise<void> {
        const { investorAccount, amountCents, transactionFeesCents, tradeId, offeringId, ip } = request;

        const bankAccount = await this.bank.getBankAccount(investorAccount);

        if (!bankAccount.isAvailable) {
            throw new BazaNcPurchaseFlowNoBankAccountException();
        }

        await this.ncACHTransfersApi.externalFundMove({
            tradeId,
            offeringId,
            accountId: investorAccount.northCapitalAccountId,
            checkNumber: tradeId, // See https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation?mid=MjE5
            NickName: bankAccount.details.accountNickName,
            amount: convertFromCents(amountCents + transactionFeesCents).toString(),
            description: `${getBazaProjectName()} transaction`,
            createdIpAddress: ip,
        });
    }

    /**
     * Re-process Purchase Shares using NC ACH Method
     * @param request
     */
    async reprocess(request: BazaNcPurchaseFlowServiceRequest): Promise<void> {
        await this.purchase(request);
    }
}
