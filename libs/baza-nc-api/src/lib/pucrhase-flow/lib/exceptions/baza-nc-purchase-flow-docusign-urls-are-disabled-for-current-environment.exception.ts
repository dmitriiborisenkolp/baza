import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowException } from '../../../typeorm';

export class BazaNcPurchaseFlowDocusignUrlsAreDisabledForCurrentEnvironmentException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.DocuSignUrlsAreDisabledForCurrentEnvironment,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.DocuSignUrlsAreDisabledForCurrentEnvironment],
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}
