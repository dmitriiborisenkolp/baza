import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';

export class BazaNcPurchaseFlowDocumentsNotSignedException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.DocumentsAreNotSigned,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.DocumentsAreNotSigned],
            },
            HttpStatus.CONFLICT,
        );
    }
}
