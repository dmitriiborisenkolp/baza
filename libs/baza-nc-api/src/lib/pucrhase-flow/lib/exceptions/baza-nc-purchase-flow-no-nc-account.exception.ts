import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { getBazaProjectName } from '@scaliolabs/baza-core-shared';

export class BazaNcPurchaseFlowNoNcAccountException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.NorthCapitalAccountIsNotReady,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.NorthCapitalAccountIsNotReady],
            },
            HttpStatus.NOT_FOUND,
            {
                projectName: getBazaProjectName(),
            },
        );
    }
}
