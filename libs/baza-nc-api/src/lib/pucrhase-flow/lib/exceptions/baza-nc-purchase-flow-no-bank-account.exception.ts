import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';

export class BazaNcPurchaseFlowNoBankAccountException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.NoBankAccount,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.NoBankAccount],
            },
            HttpStatus.NOT_FOUND,
        );
    }
}
