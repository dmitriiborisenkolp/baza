import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';

export class BazaNcPurchaseFlowLegacyBankAccountSystemDisabledException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.LegacyBankAccountSystemDisabled,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.LegacyBankAccountSystemDisabled],
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}
