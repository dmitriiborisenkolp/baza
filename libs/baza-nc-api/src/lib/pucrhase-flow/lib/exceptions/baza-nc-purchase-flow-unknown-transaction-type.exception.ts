import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowException } from '../../../typeorm';

export class BazaNcPurchaseFlowUnknownTransactionTypeException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.UnknownTransactionType,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.UnknownTransactionType],
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}
