import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { BazaCreditCardType } from '@scaliolabs/baza-core-shared';

export class BazaNcPurchaseFlowUnknownStrategyException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.UnknownPurchaseFlowStrategy,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.UnknownPurchaseFlowStrategy],
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}
