import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';

export class BazaNcPurchaseFlowInvalidCreditCardNumberException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.CreditCardIsInvalid,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.CreditCardIsInvalid],
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}
