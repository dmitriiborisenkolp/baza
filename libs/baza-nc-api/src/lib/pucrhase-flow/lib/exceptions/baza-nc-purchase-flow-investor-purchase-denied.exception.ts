import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowException } from '../../../typeorm';
import { BazaNcPurchaseFlowErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcPurchaseFlowInvestorPurchaseDeniedException extends BazaNcPurchaseFlowException {
    constructor(message) {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.InvestorPurchaseDeniedException,
                message,
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}
