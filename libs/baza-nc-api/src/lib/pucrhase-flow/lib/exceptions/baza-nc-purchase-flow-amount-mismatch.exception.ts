import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowException } from '../../../typeorm';

export class BazaNcPurchaseFlowAmountMismatchException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.AmountMismatch,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.AmountMismatch],
            },
            HttpStatus.CONFLICT,
        );
    }
}
