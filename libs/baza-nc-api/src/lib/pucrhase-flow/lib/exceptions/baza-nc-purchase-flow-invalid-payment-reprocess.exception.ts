import { HttpStatus } from '@nestjs/common';
import { BazaNcPurchaseFlowErrorCodes, bazaNcPurchaseFlowErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowException } from '../../../typeorm';

export class BazaNcPurchaseFlowInvalidPaymentReprocessException extends BazaNcPurchaseFlowException {
    constructor() {
        super(
            {
                errorCode: BazaNcPurchaseFlowErrorCodes.InvalidPaymentReprocess,
                message: bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.InvalidPaymentReprocess],
            },
            HttpStatus.BAD_REQUEST,
        );
    }
}
