import { forwardRef, Module } from '@nestjs/common';
import { BazaNcTaxDocumentsApiModule } from './baza-nc-tax-documents-api.module';
import { BazaNcTaxDocumentsFixture } from './integration-tests/fixtures/baza-nc-tax-documents.fixture';

const E2E_FIXTURES = [
    BazaNcTaxDocumentsFixture,
];

@Module({
    imports: [
        forwardRef(() => BazaNcTaxDocumentsApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcTaxDocumentsE2eModule {}
