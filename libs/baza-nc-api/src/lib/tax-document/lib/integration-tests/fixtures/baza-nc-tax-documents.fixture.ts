import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcTaxDocumentCmsService } from '../../services/baza-nc-tax-document-cms.service';
import { BazaNcTaxDocumentCmsCreateRequest } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationE2eUserFixture } from '../../../../account-verification';
import { BazaNcTaxDocumentFixtures } from '../baza-nc-tax-document.fixtures';

const E2E_TAX_DOCUMENTS_FIXTURE: Array<{
    request: BazaNcTaxDocumentCmsCreateRequest;
}> = [
    {
        request: {
            investorAccountId: undefined,
            title: 'Tax Document 1',
            description: 'Tax Document 1 (Published)',
            isPublished: true,
            fileS3Key: 'tax-document-1',
        },
    },
    {
        request: {
            investorAccountId: undefined,
            title: 'Tax Document 2',
            description: 'Tax Document 2 (Not Published)',
            isPublished: false,
            fileS3Key: 'tax-document-2',
        },
    },
    {
        request: {
            investorAccountId: undefined,
            title: 'Tax Document 3',
            description: 'Tax Document 3 (Published)',
            isPublished: true,
            fileS3Key: 'tax-document-3',
        },
    },
];

@Injectable()
export class BazaNcTaxDocumentsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcTaxDocumentCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of E2E_TAX_DOCUMENTS_FIXTURE) {
            await this.service.create({
                ...fixtureRequest.request,
                investorAccountId: BazaNcAccountVerificationE2eUserFixture._e2eUserInvestorAccount.id,
            });
        }
    }
}

defineE2eFixtures<BazaNcTaxDocumentFixtures>([{
    fixture: BazaNcTaxDocumentFixtures.BazaNcTaxDocuments,
    provider: BazaNcTaxDocumentsFixture,
}]);
