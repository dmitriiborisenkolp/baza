import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcInvestorAccountCmsNodeAccess,
    BazaNcTaxDocumentCmsNodeAccess,
    BazaNcTaxDocumentNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcTaxDocumentFixtures } from '../baza-nc-tax-document.fixtures';
import { BazaNcTaxDocumentErrorCodes } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/tax-document/integration-tests/tests/002-baza-nc-tax-document.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcTaxDocumentNodeAccess(http);
    const dataAccessCms = new BazaNcTaxDocumentCmsNodeAccess(http);
    const dataAccessInvestorAccountCms = new BazaNcInvestorAccountCmsNodeAccess(http);

    let TAX_DOCUMENT_ID: number;
    let UNPUBLISHED_TAX_DOCUMENT_ID: number;
    let INVESTOR_ACCOUNT_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcTaxDocumentFixtures.BazaNcTaxDocuments,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will have investor account', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessInvestorAccountCms.listInvestorAccounts({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        INVESTOR_ACCOUNT_ID = response.items[0].id;
    });

    it('will display tax documents of investor accounts in CMS', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessCms.list({
            investorAccountId: INVESTOR_ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const unpublished = response.items.find((t) => !t.isPublished);

        expect(unpublished).toBeDefined();

        UNPUBLISHED_TAX_DOCUMENT_ID = unpublished.id;
    });

    it('will display list of published tax documents', async () => {
        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Tax Document 3');
        expect(response.items[1].title).toBe('Tax Document 1');

        TAX_DOCUMENT_ID = response.items[0].id;
    });

    it('will returns published tax document by id', async () => {
        const response = await dataAccess.get({
            id: TAX_DOCUMENT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not returns unpublished tax document by id', async () => {
        const response: BazaError = (await dataAccess.get({
            id: UNPUBLISHED_TAX_DOCUMENT_ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcTaxDocumentErrorCodes.BazaNcTaxDocumentsNotFound);
    });

    it('will not display tax documents of another account', async () => {
        await http.authE2eUser2();

        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(0);
    });

    it('will not returns published document of another account', async () => {
        await http.authE2eUser2();

        const response: BazaError = (await dataAccess.get({
            id: TAX_DOCUMENT_ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcTaxDocumentErrorCodes.BazaNcTaxDocumentsNotFound);
    });
});
