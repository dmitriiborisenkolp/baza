import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcInvestorAccountCmsNodeAccess, BazaNcTaxDocumentCmsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/tax-document/integration-tests/tests/001-baza-nc-tax-document-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessCms = new BazaNcTaxDocumentCmsNodeAccess(http);
    const dataAccessInvestorAccountCms = new BazaNcInvestorAccountCmsNodeAccess(http);

    let TAX_DOCUMENT_ID: number;
    let INVESTOR_ACCOUNT_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will have investor account', async () => {
        const response = await dataAccessInvestorAccountCms.listInvestorAccounts({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        INVESTOR_ACCOUNT_ID = response.items[0].id;
    });

    it('will successfully create tax document for investor account (with description)', async () => {
        const response = await dataAccessCms.create({
            investorAccountId: INVESTOR_ACCOUNT_ID,
            title: 'Example Tax Document 1',
            description: 'Description for Example Tax Document 1',
            fileS3Key: 'example-tax-document-1',
            isPublished: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.description).toBe('Description for Example Tax Document 1');

        TAX_DOCUMENT_ID = response.id;
    });

    it('will successfully create tax document for investor account (without description)', async () => {
        const response = await dataAccessCms.create({
            investorAccountId: INVESTOR_ACCOUNT_ID,
            title: 'Example Tax Document 2',
            fileS3Key: 'example-tax-document-2',
            isPublished: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.description).toBeNull();
    });

    it('will allow to get tax document by id', async () => {
        const response = await dataAccessCms.getById({
            id: TAX_DOCUMENT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.title).toBe('Example Tax Document 1');
    });

    it('will display uploaded tax document in list response', async () => {
        const response = await dataAccessCms.list({
            investorAccountId: INVESTOR_ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Example Tax Document 2');
        expect(response.items[1].title).toBe('Example Tax Document 1');
    });

    it('will successfully update tax document', async () => {
        const response = await dataAccessCms.update({
            id: TAX_DOCUMENT_ID,
            title: 'Example Tax Document 1 *',
            description: 'Description for Example Tax Document 1',
            fileS3Key: 'example-tax-document-1',
            isPublished: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.title).toBe('Example Tax Document 1 *');
    });

    it('will display updated tax document in list response', async () => {
        const response = await dataAccessCms.list({
            investorAccountId: INVESTOR_ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Example Tax Document 2');
        expect(response.items[1].title).toBe('Example Tax Document 1 *');
    });

    it('will successfully remove tax document', async () => {
        const response = await dataAccessCms.delete({
            id: TAX_DOCUMENT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted tax document in list response', async () => {
        const response = await dataAccessCms.list({
            investorAccountId: INVESTOR_ACCOUNT_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Tax Document 2');
    });
});
