import { Global, Module } from "@nestjs/common";
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcTaxDocumentController } from './controllers/baza-nc-tax-document.controller';
import { BazaNcTaxDocumentCmsController } from './controllers/baza-nc-tax-document-cms.controller';
import { BazaNcTaxDocumentCmsService } from './services/baza-nc-tax-document-cms.service';
import { BazaNcTaxDocumentMapper } from './mappers/baza-nc-tax-document.mapper';
import { BazaNcTaxDocumentCmsMapper } from './mappers/baza-nc-tax-document-cms.mapper';
import { BazaNcTaxDocumentService } from './services/baza-nc-tax-document.service';

@Global()
@Module({
    imports: [
        BazaCrudApiModule,
    ],
    controllers: [
        BazaNcTaxDocumentController,
        BazaNcTaxDocumentCmsController,
    ],
    providers: [
        BazaNcTaxDocumentMapper,
        BazaNcTaxDocumentCmsMapper,
        BazaNcTaxDocumentService,
        BazaNcTaxDocumentCmsService,
    ],
    exports: [
        BazaNcTaxDocumentMapper,
        BazaNcTaxDocumentCmsMapper,
        BazaNcTaxDocumentService,
        BazaNcTaxDocumentCmsService,
    ],
})
export class BazaNcTaxDocumentsApiModule {}
