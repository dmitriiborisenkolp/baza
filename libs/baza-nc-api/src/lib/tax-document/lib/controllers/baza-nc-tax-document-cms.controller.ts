import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaNcTaxDocumentCmsService } from '../services/baza-nc-tax-document-cms.service';
import {
    BazaNcTaxDocumentCmsCreateRequest,
    BazaNcTaxDocumentCmsDeleteRequest,
    BazaNcTaxDocumentCmsDto,
    BazaNcTaxDocumentCmsEndpoint,
    BazaNcTaxDocumentCmsEndpointPaths,
    BazaNcTaxDocumentCmsGetByIdRequest,
    BazaNcTaxDocumentCmsListRequest,
    BazaNcTaxDocumentCmsListResponse,
    BazaNcTaxDocumentCmsUpdateRequest,
    BazaNorthCapitalAcl,
    BazaNorthCapitalCMSOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcTaxDocumentCmsMapper } from '../mappers/baza-nc-tax-document-cms.mapper';
import { BazaNcTaxDocumentRepository } from '../../../typeorm';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalTaxDocumentCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcTaxDocument])
export class BazaNcTaxDocumentCmsController implements BazaNcTaxDocumentCmsEndpoint {
    constructor(
        private readonly service: BazaNcTaxDocumentCmsService,
        private readonly repository: BazaNcTaxDocumentRepository,
        private readonly mapper: BazaNcTaxDocumentCmsMapper,
    ) {}

    @Post(BazaNcTaxDocumentCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: BazaNcTaxDocumentCmsDto,
    })
    async create(@Body() request: BazaNcTaxDocumentCmsCreateRequest): Promise<BazaNcTaxDocumentCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcTaxDocumentCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: BazaNcTaxDocumentCmsDto,
    })
    async update(@Body() request: BazaNcTaxDocumentCmsUpdateRequest): Promise<BazaNcTaxDocumentCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcTaxDocumentCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaNcTaxDocumentCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(BazaNcTaxDocumentCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: BazaNcTaxDocumentCmsListResponse,
    })
    async list(@Body() request: BazaNcTaxDocumentCmsListRequest): Promise<BazaNcTaxDocumentCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcTaxDocumentCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: BazaNcTaxDocumentCmsDto,
    })
    async getById(@Body() request: BazaNcTaxDocumentCmsGetByIdRequest): Promise<BazaNcTaxDocumentCmsDto> {
        const entity = await this.repository.findById(request.id);

        return this.mapper.entityToDTO(entity);
    }
}
