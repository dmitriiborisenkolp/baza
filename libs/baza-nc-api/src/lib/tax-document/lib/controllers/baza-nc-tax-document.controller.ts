import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcTaxDocumentDto,
    BazaNcTaxDocumentEndpoint,
    BazaNcTaxDocumentEndpointPaths,
    BazaNcTaxDocumentGetRequest,
    BazaNcTaxDocumentListRequest,
    BazaNcTaxDocumentListResponse,
    BazaNorthCapitalOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcTaxDocumentService } from '../services/baza-nc-tax-document.service';
import { AccountEntity, ReqAccount } from '@scaliolabs/baza-core-api';
import { BazaNcTaxDocumentMapper } from '../mappers/baza-nc-tax-document.mapper';
import { InvestorAccountOptionalGuard, ReqInvestorAccountId } from '../../../investor-account';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalTaxDocument)
@UseGuards(InvestorAccountOptionalGuard)
export class BazaNcTaxDocumentController implements BazaNcTaxDocumentEndpoint {
    constructor(private readonly mapper: BazaNcTaxDocumentMapper, private readonly service: BazaNcTaxDocumentService) {}

    @Get(BazaNcTaxDocumentEndpointPaths.get)
    @ApiOperation({
        summary: 'get',
        description: 'Returns Tax Document by ID',
    })
    @ApiOkResponse({
        type: BazaNcTaxDocumentDto,
    })
    async get(@Param() request: BazaNcTaxDocumentGetRequest, @ReqAccount() account: AccountEntity): Promise<BazaNcTaxDocumentDto> {
        const entity = await this.service.get(request, account);

        return this.mapper.entityToDTO(entity);
    }

    @Get(BazaNcTaxDocumentEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns tax documents list',
    })
    @ApiOkResponse({
        type: BazaNcTaxDocumentListResponse,
    })
    async list(
        @Query() request: BazaNcTaxDocumentListRequest,
        @ReqInvestorAccountId() investorAccountId: number,
    ): Promise<BazaNcTaxDocumentListResponse> {
        return this.service.list(request, investorAccountId);
    }
}
