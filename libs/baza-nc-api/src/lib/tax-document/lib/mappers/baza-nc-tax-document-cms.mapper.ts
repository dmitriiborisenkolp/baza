import { Injectable } from "@nestjs/common";
import { BazaNcTaxDocumentMapper } from './baza-nc-tax-document.mapper';
import { BazaNcTaxDocumentCmsDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcTaxDocumentEntity } from "../../../typeorm";

@Injectable()
export class BazaNcTaxDocumentCmsMapper {
    constructor(
        private readonly baseMapper: BazaNcTaxDocumentMapper,
    ) {}

    async entityToDTO(input: BazaNcTaxDocumentEntity): Promise<BazaNcTaxDocumentCmsDto> {
        return {
            ...await this.baseMapper.entityToDTO(input),
            isPublished: input.isPublished,
            fileS3Key: input.fileS3Key,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcTaxDocumentEntity>): Promise<Array<BazaNcTaxDocumentCmsDto>> {
        const result: Array<BazaNcTaxDocumentCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
