import { Injectable } from "@nestjs/common";
import { AwsService } from '@scaliolabs/baza-core-api';
import { BazaNcTaxDocumentDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcTaxDocumentEntity } from "../../../typeorm";

@Injectable()
export class BazaNcTaxDocumentMapper {
    constructor(
        private readonly aws: AwsService,
    ) {
    }

    async entityToDTO(input: BazaNcTaxDocumentEntity): Promise<BazaNcTaxDocumentDto> {
        return {
            id: input.id,
            title: input.title,
            description: input.description,
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            dateUpdatedAt: input.dateUpdatedAt
                ? input.dateUpdatedAt.toISOString()
                : undefined,
            fileUrl: await this.aws.presignedUrl({
                s3ObjectId: input.fileS3Key,
            }),
        };
    }

    async entitiesToDTOs(input: Array<BazaNcTaxDocumentEntity>): Promise<Array<BazaNcTaxDocumentDto>> {
        const result: Array<BazaNcTaxDocumentDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
