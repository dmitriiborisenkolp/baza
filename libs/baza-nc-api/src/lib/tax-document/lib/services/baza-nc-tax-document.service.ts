import { Injectable } from '@nestjs/common';
import {
    BazaNcTaxDocumentDto,
    BazaNcTaxDocumentGetRequest,
    BazaNcTaxDocumentListRequest,
    BazaNcTaxDocumentListResponse,
} from '@scaliolabs/baza-nc-shared';
import { AccountEntity, CrudService } from '@scaliolabs/baza-core-api';
import { BazaNcTaxDocumentMapper } from '../mappers/baza-nc-tax-document.mapper';
import {
    BazaNcTaxDocumentEntity,
    BazaNcTaxDocumentNotFoundException,
    BAZA_NC_TAX_DOCUMENT_RELATIONS,
    BazaNcTaxDocumentRepository,
    BazaNcInvestorAccountRepository,
} from '../../../typeorm';
import { bazaEmptyCrudListResponse } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaNcTaxDocumentService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: BazaNcTaxDocumentMapper,
        private readonly repository: BazaNcTaxDocumentRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    async get(request: BazaNcTaxDocumentGetRequest, account: AccountEntity): Promise<BazaNcTaxDocumentEntity> {
        const entity = await this.repository.getById(request.id);

        if (!entity.investorAccount || entity.investorAccount.user.id !== account.id || !entity.isPublished) {
            throw new BazaNcTaxDocumentNotFoundException();
        }

        return entity;
    }

    async list(request: BazaNcTaxDocumentListRequest, investorAccountId: number): Promise<BazaNcTaxDocumentListResponse> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountById(investorAccountId);

        if (!investorAccount) {
            return bazaEmptyCrudListResponse();
        }

        return this.crud.find<BazaNcTaxDocumentEntity, BazaNcTaxDocumentDto>({
            request,
            entity: BazaNcTaxDocumentEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                where: [
                    {
                        investorAccount,
                        isPublished: true,
                    },
                ],
                relations: BAZA_NC_TAX_DOCUMENT_RELATIONS,
                order: {
                    id: 'DESC',
                },
            },
        });
    }
}
