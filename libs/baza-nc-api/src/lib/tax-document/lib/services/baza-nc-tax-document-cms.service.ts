import { Injectable } from '@nestjs/common';
import { CrudService } from '@scaliolabs/baza-core-api';
import { BazaNcTaxDocumentCmsMapper } from '../mappers/baza-nc-tax-document-cms.mapper';
import {
    BazaNcTaxDocumentCmsDto,
    BazaNcTaxDocumentCmsCreateRequest,
    BazaNcTaxDocumentCmsDeleteRequest,
    BazaNcTaxDocumentCmsListRequest,
    BazaNcTaxDocumentCmsListResponse,
    BazaNcTaxDocumentCmsUpdateRequest,
    BazaNcTaxDocumentEntityBody,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcTaxDocumentEntity, BazaNcInvestorAccountRepository, BazaNcTaxDocumentRepository } from '../../../typeorm';

@Injectable()
export class BazaNcTaxDocumentCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly repository: BazaNcTaxDocumentRepository,
        private readonly mapper: BazaNcTaxDocumentCmsMapper,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    async create(request: BazaNcTaxDocumentCmsCreateRequest): Promise<BazaNcTaxDocumentEntity> {
        const entity = new BazaNcTaxDocumentEntity();

        entity.investorAccount = await this.investorAccountRepository.findInvestorAccountById(request.investorAccountId);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: BazaNcTaxDocumentCmsUpdateRequest): Promise<BazaNcTaxDocumentEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: BazaNcTaxDocumentCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);
    }

    async list(request: BazaNcTaxDocumentCmsListRequest): Promise<BazaNcTaxDocumentCmsListResponse> {
        return this.crud.find<BazaNcTaxDocumentEntity, BazaNcTaxDocumentCmsDto>({
            entity: BazaNcTaxDocumentEntity,
            request,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    id: 'DESC',
                },
                where: [
                    {
                        investorAccount: request.investorAccountId,
                    },
                ],
            },
        });
    }

    async populate(target: BazaNcTaxDocumentEntity, entityBody: BazaNcTaxDocumentEntityBody): Promise<void> {
        if (target.id) {
            target.dateUpdatedAt = new Date();
        } else {
            target.dateCreatedAt = new Date();
            target.dateUpdatedAt = null;
        }

        target.isPublished = entityBody.isPublished;
        target.title = entityBody.title;
        target.description = entityBody.description;
        target.fileS3Key = entityBody.fileS3Key;
    }
}
