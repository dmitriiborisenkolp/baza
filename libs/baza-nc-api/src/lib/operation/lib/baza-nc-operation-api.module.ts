import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcOperationService } from './services/baza-nc-operation.service';
import { BazaNcOperationController } from './controllers/baza-nc-operation.controller';
import { BazaNcOperationDataSource } from './data-sources/baza-nc-operation.data-source';
import { BazaNcWithdrawApiModule } from '../../withdraw';
import { BazaNcDividendApiModule } from '../../dividend';
import { BazaNcTransferApiModule } from '../../transfer';

@Module({
    imports: [BazaCrudApiModule, BazaNcWithdrawApiModule, BazaNcDividendApiModule, BazaNcTransferApiModule],
    controllers: [BazaNcOperationController],
    providers: [BazaNcOperationService, BazaNcOperationDataSource],
    exports: [BazaNcOperationService, BazaNcOperationDataSource],
})
export class BazaNcOperationApiModule {}
