import { Injectable } from '@nestjs/common';
import { BazaNcOperationDataSource, BazaNcOperationDataSourceInterface } from '../data-sources/baza-nc-operation.data-source';
import {
    BazaNcOperationListRequest,
    BazaNcOperationListResponse,
    BazaNcOperationStatsResponse,
    BazaNcOperationType,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';

/**
 * Baza NC Operations Service
 * Works with Data Source to fetch operations list of Investor Account
 * Default implementation of Data Source can be considered as low-performance
 * solution. If you would like to optimize it, you can replace default Data Source
 * solution with your custom better performance one during onApplicationBootstrap
 * lifecycle hook
 * @see BazaNcOperationDataSource
 * @see BazaNcOperationDataSourceInterface
 * @see BazaNcOperationService.dataSource
 * @see BazaNcOperationTitleService
 */
@Injectable()
export class BazaNcOperationService {
    private _dataSource: BazaNcOperationDataSourceInterface;

    constructor(private readonly defaultDataSource: BazaNcOperationDataSource) {}

    /**
     * Returns Data Source which is used for Operations endpoint
     */
    get dataSource(): BazaNcOperationDataSourceInterface {
        return this._dataSource || this.defaultDataSource;
    }

    /**
     * Override Data Source
     * You may override default Data Source implementation with better performance solution
     * @see BazaNcOperationDataSource
     * @param overrideWithDataSource
     */
    set dataSource(overrideWithDataSource: BazaNcOperationDataSourceInterface) {
        this._dataSource = overrideWithDataSource;
    }

    /**
     * Returns operations list of Investor Account
     * @param investorAccount
     * @param request
     */
    async list(investorAccount: BazaNcInvestorAccountEntity, request: BazaNcOperationListRequest): Promise<BazaNcOperationListResponse> {
        return this.dataSource.fetch(investorAccount, request);
    }

    /**
     * Returns operation stats of Investor Account
     * @param investorAccount
     */
    async stats(investorAccount: BazaNcInvestorAccountEntity): Promise<BazaNcOperationStatsResponse> {
        return this.dataSource.stats(investorAccount);
    }
}
