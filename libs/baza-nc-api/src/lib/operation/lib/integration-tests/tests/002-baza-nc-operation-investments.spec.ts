import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcOperationNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import {
    AccountTypeCheckingSaving,
    BazaNcOperationDirection,
    BazaNcOperationPayloadInvestmentDto,
    BazaNcOperationType,
    OfferingId,
    TradeId,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcApiOfferingFixtures } from '../../../../offering';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/operation/002-baza-nc-operation-investments.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcOperationNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessPurchaseFlowSession = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);

    let tradeId: TradeId;
    let offeringId: OfferingId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);

        offeringId = response[0].ncOfferingId;
    });

    it('will display no entries in operations list', async () => {
        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
        expect(response.pager.size).toBe(10);
        expect(response.pager.index).toBe(1);
        expect(response.pager.total).toBe(0);
    });

    it('will purchase 5 shares', async () => {
        const session = await dataAccessPurchaseFlowSession.session({
            numberOfShares: 5,
            offeringId,
            requestDocuSignUrl: false,
            amount: 5000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const bankAccount = dataAccessBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();

        const submit = await dataAccessPurchaseFlowSession.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();

        tradeId = session.id;
    });

    it('will display a new item in operations', async () => {
        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.pager.size).toBe(10);
        expect(response.pager.index).toBe(1);
        expect(response.pager.total).toBe(1);

        expect(
            response.items.map((next) => ({
                amount: next.amount,
                amountCents: next.amountCents,
                direction: next.direction,
                payload: {
                    type: next.payload.type,
                    canBeReprocessed: next.payload.canBeReprocessed,
                    offeringTitle: (next.payload as BazaNcOperationPayloadInvestmentDto).offeringTitle,
                    shares: (next.payload as BazaNcOperationPayloadInvestmentDto).shares,
                    totalCents: (next.payload as BazaNcOperationPayloadInvestmentDto).totalCents,
                },
            })),
        ).toEqual([
            {
                amount: '50.00',
                amountCents: 5000,
                direction: BazaNcOperationDirection.Out,
                payload: {
                    type: BazaNcOperationType.Investment,
                    canBeReprocessed: false,
                    offeringTitle: 'E2E NC Offering Fixture',
                    shares: 5,
                    totalCents: 5000,
                },
            },
        ]);
    });
});
