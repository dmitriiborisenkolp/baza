import { filterDateFrom } from './filter-date-from';
import { filterDateTo } from './filter-date-to';
import { filterTypes } from './filter-types';
import { filterOfferingIds } from './filter-offering-ids';
import { filterTransactionStates } from './filter-transaction-states';
import { filterDividendStatuses } from './filter-dividend-statuses';
import { filterDwollaPaymentStatuses } from './filter-dwolla-payment-statuses';

export const filters = [
    filterDateFrom,
    filterDateTo,
    filterTypes,
    filterOfferingIds,
    filterTransactionStates,
    filterDividendStatuses,
    filterDwollaPaymentStatuses,
];
