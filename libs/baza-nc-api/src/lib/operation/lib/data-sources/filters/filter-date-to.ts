import { BazaNcOperationDto, BazaNcOperationListRequest } from '@scaliolabs/baza-nc-shared';

export const filterDateTo = (request: BazaNcOperationListRequest) => (next: BazaNcOperationDto) =>
    request.dateTo ? new Date(next.date).getTime() <= new Date(request.dateTo).getTime() : true;
