import { BazaNcOperationDto, BazaNcOperationListRequest } from '@scaliolabs/baza-nc-shared';

export const filterTypes = (request: BazaNcOperationListRequest) => (next: BazaNcOperationDto) =>
    Array.isArray(request.types) && request.types.length > 0 ? request.types.includes(next.payload.type) : true;
