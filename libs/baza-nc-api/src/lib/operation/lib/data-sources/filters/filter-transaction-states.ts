import {
    BazaNcOperationDto,
    BazaNcOperationListRequest,
    BazaNcOperationPayloadInvestmentDto,
    BazaNcOperationType,
} from '@scaliolabs/baza-nc-shared';

export const filterTransactionStates = (request: BazaNcOperationListRequest) => (next: BazaNcOperationDto) => {
    if (Array.isArray(request.transactionStates) && request.transactionStates.length > 0) {
        if (
            next.payload.type === BazaNcOperationType.Investment &&
            !request.transactionStates.includes((next.payload as BazaNcOperationPayloadInvestmentDto).state)
        ) {
            return false;
        }
    }

    return true;
};
