import {
    BazaNcOperationDto,
    BazaNcOperationListRequest,
    BazaNcOperationPayloadDividendDto,
    BazaNcOperationType,
} from '@scaliolabs/baza-nc-shared';

export const filterDividendStatuses = (request: BazaNcOperationListRequest) => (next: BazaNcOperationDto) => {
    if (Array.isArray(request.dividendStatuses) && request.transactionStates.length > 0) {
        if (
            next.payload.type === BazaNcOperationType.Dividend &&
            !request.dividendStatuses.includes((next.payload as BazaNcOperationPayloadDividendDto).status)
        ) {
            return false;
        }
    }

    return true;
};
