import {
    BazaNcOperationDto,
    BazaNcOperationListRequest,
    BazaNcOperationPayloadTransferDto,
    BazaNcOperationPayloadWithdrawDto,
    BazaNcOperationType,
} from '@scaliolabs/baza-nc-shared';

export const filterDwollaPaymentStatuses = (request: BazaNcOperationListRequest) => (next: BazaNcOperationDto) => {
    if (Array.isArray(request.dwollaPaymentStatuses) && request.transactionStates.length > 0) {
        if (
            [BazaNcOperationType.Withdraw, BazaNcOperationType.Transfer].includes(next.payload.type) &&
            !request.dwollaPaymentStatuses.includes(
                (next.payload as BazaNcOperationPayloadWithdrawDto | BazaNcOperationPayloadTransferDto).status,
            )
        ) {
            return false;
        }
    }

    return true;
};
