import {
    BazaNcOperationDto,
    BazaNcOperationListRequest,
    BazaNcOperationPayloadDividendDto,
    BazaNcOperationPayloadInvestmentDto,
    BazaNcOperationType,
} from '@scaliolabs/baza-nc-shared';

export const filterOfferingIds = (request: BazaNcOperationListRequest) => (next: BazaNcOperationDto) => {
    if (Array.isArray(request.offeringIds) && request.offeringIds.length > 0) {
        if (
            [BazaNcOperationType.Investment, BazaNcOperationType.Dividend].includes(next.payload.type) &&
            !request.offeringIds.includes(
                (next.payload as BazaNcOperationPayloadInvestmentDto | BazaNcOperationPayloadDividendDto).offeringId,
            )
        ) {
            return false;
        }
    }

    return true;
};
