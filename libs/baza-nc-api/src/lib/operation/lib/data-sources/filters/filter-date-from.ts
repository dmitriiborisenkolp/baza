import { BazaNcOperationDto, BazaNcOperationListRequest } from '@scaliolabs/baza-nc-shared';

export const filterDateFrom = (request: BazaNcOperationListRequest) => (next: BazaNcOperationDto) =>
    request.dateFrom ? new Date(next.date).getTime() >= new Date(request.dateFrom).getTime() : true;
