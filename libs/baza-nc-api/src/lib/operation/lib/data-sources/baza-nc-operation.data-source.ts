import { Injectable } from '@nestjs/common';
import {
    bazaNcDividendReprocessablePaymentSources,
    bazaNcDividendReprocessablePaymentStatuses,
    BazaNcOperationDto,
    BazaNcOperationListRequest,
    BazaNcOperationListResponse,
    BazaNcOperationPayloadDividendDto,
    BazaNcOperationPayloadInvestmentDto,
    BazaNcOperationStatsResponse,
    BazaNcOperationType,
    convertFromCents,
    mapBazaNcOperationTypeToDirection,
    TRANSACTION_REPROCESS_AVAILABLE_STATES,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { naturalCompare, paginate } from '@scaliolabs/baza-core-shared';
import { BAZA_CRUD_DEFAULT_SIZE } from '@scaliolabs/baza-core-api';
import * as _ from 'lodash';
import { BazaNcTransactionsService } from '../../../transaction';
import { BazaNcDividendService } from '../../../dividend';
import { BazaNcWithdrawService } from '../../../withdraw';
import { BazaNcTransferService } from '../../../transfer';
import { BazaDwollaPaymentStatus } from '@scaliolabs/baza-dwolla-shared';
import { filters } from './filters';

/**
 * Interface for Data-Source of Operations
 * You may replace it with different solution using BazaNcOperationService.setDataSource method
 * @see BazaNcOperationService.setDataSource
 */
export interface BazaNcOperationDataSourceInterface {
    stats(investorAccount: BazaNcInvestorAccountEntity): Promise<BazaNcOperationStatsResponse>;
    fetch(investorAccount: BazaNcInvestorAccountEntity, request: BazaNcOperationListRequest): Promise<BazaNcOperationListResponse>;
}

/**
 * Data-Source for Operations endpoint
 */
@Injectable()
export class BazaNcOperationDataSource implements BazaNcOperationDataSourceInterface {
    constructor(
        private readonly transactionsService: BazaNcTransactionsService,
        private readonly dividendsService: BazaNcDividendService,
        private readonly withdrawService: BazaNcWithdrawService,
        private readonly transferService: BazaNcTransferService,
    ) {}

    /**
     * Returns Stats of Investors Account
     */
    async stats(investorAccount: BazaNcInvestorAccountEntity): Promise<BazaNcOperationStatsResponse> {
        const all = await this.fetchAll(investorAccount);

        const offeringIdsInvestments = _.uniq(
            all
                .filter((next) => next.payload.type === BazaNcOperationType.Investment)
                .map((next) => (next.payload as BazaNcOperationPayloadInvestmentDto).offeringId),
        ).sort((a, b) => naturalCompare(a, b));

        const offeringIdsDividends = _.uniq(
            all
                .filter((next) => next.payload.type === BazaNcOperationType.Dividend)
                .map((next) => (next.payload as BazaNcOperationPayloadInvestmentDto | BazaNcOperationPayloadDividendDto).offeringId),
        ).sort((a, b) => naturalCompare(a, b));

        const offeringIdsMap: Record<BazaNcOperationType, Array<string> | undefined> = {
            [BazaNcOperationType.Investment]: offeringIdsInvestments,
            [BazaNcOperationType.Dividend]: offeringIdsInvestments,
            [BazaNcOperationType.Withdraw]: offeringIdsDividends,
            [BazaNcOperationType.Transfer]: undefined,
        };

        return {
            total: all.length,
            types: Object.values(BazaNcOperationType).map((type) => ({
                type,
                total: all.filter((next) => next.payload.type === type).length,
                // eslint-disable-next-line security/detect-object-injection
                offeringIds: offeringIdsMap[type],
            })),
            offeringIds: _.uniq([...offeringIdsInvestments, ...offeringIdsDividends]).sort((a, b) => naturalCompare(a, b)),
        };
    }

    /**
     * Returns all available operations of Investor Accounts
     * @param investorAccount
     * @param request
     */
    async fetch(investorAccount: BazaNcInvestorAccountEntity, request: BazaNcOperationListRequest): Promise<BazaNcOperationListResponse> {
        let all = await this.fetchAll(investorAccount);

        filters.forEach((next) => (all = all.filter(next(request))));

        request.index = Number(request.index) || 1;
        request.size = Number(request.size) || BAZA_CRUD_DEFAULT_SIZE;

        const offset = Math.max(0, request.index - 1) * request.size;

        return {
            pager: {
                index: request.index || 0,
                size: request.size || -1,
                total: all.length,
            },
            items: paginate(all, offset, request.size),
        };
    }

    /**
     * Fetches and combines all operations of Investor Account
     * @param investorAccount
     * @private
     */
    private async fetchAll(investorAccount: BazaNcInvestorAccountEntity): Promise<Array<BazaNcOperationDto>> {
        return _.flatten(
            await Promise.all([
                this.fetchAllInvestments(investorAccount),
                this.fetchAllDividends(investorAccount),
                this.fetchAllWithdraws(investorAccount),
                this.fetchAllTransfers(investorAccount),
            ]),
        ).sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
    }

    /**
     * Fetches all investments (purchasing shares) of Investor Account
     * @param investorAccount
     * @private
     */
    private async fetchAllInvestments(investorAccount: BazaNcInvestorAccountEntity): Promise<Array<BazaNcOperationDto>> {
        const transactions = await this.transactionsService.listEntities({
            isSubmitted: true,
            accountId: investorAccount.userId,
            size: -1,
            index: 1,
        });

        return transactions.items.map((next) => ({
            date: next.createdAt,
            amount: convertFromCents(next.amountCents).toFixed(2),
            amountCents: next.amountCents,
            direction: mapBazaNcOperationTypeToDirection[BazaNcOperationType.Investment],
            payload: {
                type: BazaNcOperationType.Investment,
                id: next.id,
                state: next.state,
                offeringId: next.ncOfferingId,
                offeringTitle: next.name,
                shares: next.shares,
                feeCents: next.feeCents,
                pricePerShareCents: next.pricePerShareCents,
                totalCents: next.totalCents,
                transactionFeesCents: next.transactionFeesCents,
                canBeReprocessed: TRANSACTION_REPROCESS_AVAILABLE_STATES.includes(next.state),
            },
        }));
    }

    /**
     * Fetches all Dividends of Investor Account
     * @param investorAccount
     * @private
     */
    private async fetchAllDividends(investorAccount: BazaNcInvestorAccountEntity): Promise<Array<BazaNcOperationDto>> {
        const dividends = await this.dividendsService.list(
            {
                index: -1,
                size: -1,
            },
            investorAccount.id,
        );

        return dividends.items.map((next) => ({
            date: next.date,
            direction: mapBazaNcOperationTypeToDirection[BazaNcOperationType.Dividend],
            amount: next.amount,
            amountCents: next.amountCents,
            payload: {
                type: BazaNcOperationType.Dividend,
                status: next.status,
                canBeReprocessed:
                    bazaNcDividendReprocessablePaymentSources.includes(next.source) &&
                    bazaNcDividendReprocessablePaymentStatuses.includes(next.status),
                ulid: next.ulid,
                offeringTitle: next.offeringTitle,
                amount: next.amount,
            },
        }));
    }

    /**
     * Fetches all withdraws of Investor Account
     * @param investorAccount
     * @private
     */
    private async fetchAllWithdraws(investorAccount: BazaNcInvestorAccountEntity): Promise<Array<BazaNcOperationDto>> {
        const withdraws = await this.withdrawService.list(investorAccount, {
            index: 1,
            size: -1,
        });

        return withdraws.items.map((next) => ({
            date: next.dateCreatedAt,
            amount: next.amount,
            amountCents: next.amountCents,
            direction: mapBazaNcOperationTypeToDirection[BazaNcOperationType.Withdraw],
            payload: {
                type: BazaNcOperationType.Withdraw,
                ulid: next.ulid,
                status: next.status,
                amount: next.amount,
                canBeReprocessed: next.status === BazaDwollaPaymentStatus.Failed,
            },
        }));
    }

    /**
     * Fetches all Transfers of Investor Account
     * @param investorAccount
     * @private
     */
    private async fetchAllTransfers(investorAccount: BazaNcInvestorAccountEntity): Promise<Array<BazaNcOperationDto>> {
        const transfers = await this.transferService.list(investorAccount, {
            index: 1,
            size: -1,
        });

        return transfers.items.map((next) => ({
            date: next.dateCreatedAt,
            amount: next.amount,
            amountCents: next.amountCents,
            direction: mapBazaNcOperationTypeToDirection[BazaNcOperationType.Transfer],
            payload: {
                type: BazaNcOperationType.Transfer,
                ulid: next.ulid,
                status: next.status,
                amount: next.amount,
                canBeReprocessed: next.status === BazaDwollaPaymentStatus.Failed,
            },
        }));
    }
}
