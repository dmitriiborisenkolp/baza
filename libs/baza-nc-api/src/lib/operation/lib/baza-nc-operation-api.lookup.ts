import { BazaNcOperationApiModule } from './baza-nc-operation-api.module';
import { BazaNcOperationService } from './services/baza-nc-operation.service';
import { BazaNcOperationDataSource } from './data-sources/baza-nc-operation.data-source';

export const bazaNcOperationApiLookup = {
    module: {
        BazaNcOperationApiModule: BazaNcOperationApiModule,
    },
    services: {
        BazaNcOperationService: BazaNcOperationService,
    },
    dataSource: {
        BazaNcOperationDataSource: BazaNcOperationDataSource,
    },
};
