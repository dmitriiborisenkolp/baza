import { Controller, Get, ParseArrayPipe, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiExtraModels, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcOperationEndpoint,
    BazaNcOperationEndpointPaths,
    BazaNcOperationListRequest,
    BazaNcOperationListResponse,
    bazaNcOperationPayloadSwaggerExports,
    BazaNcOperationStatsResponse,
    BazaNcOperationType,
    BazaNorthCapitalOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcOperationService } from '../services/baza-nc-operation.service';
import { InvestorAccountOptionalGuard, ReqInvestorAccount } from '../../../investor-account';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { bazaEmptyCrudListResponse } from '@scaliolabs/baza-core-shared';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalOperation)
@ApiExtraModels(...bazaNcOperationPayloadSwaggerExports)
@UseGuards(InvestorAccountOptionalGuard)
export class BazaNcOperationController implements BazaNcOperationEndpoint {
    constructor(private readonly service: BazaNcOperationService) {}

    @Get(BazaNcOperationEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description:
            'Returns list of operations. The endpoint returns combined list of ' +
            'purchases, withdraw and transfer operations in single list',
    })
    @ApiOkResponse({
        type: BazaNcOperationListResponse,
    })
    async list(
        @Query() request: BazaNcOperationListRequest,
        @ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity | undefined,
    ): Promise<BazaNcOperationListResponse> {
        if (!investorAccount) {
            return bazaEmptyCrudListResponse();
        }

        return this.service.list(investorAccount, request);
    }

    @Get(BazaNcOperationEndpointPaths.stats)
    @ApiOperation({
        summary: 'stats',
        description: 'Returns Operation Stats for Investor Account',
    })
    @ApiOkResponse({
        type: BazaNcOperationStatsResponse,
    })
    async stats(@ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity | undefined): Promise<BazaNcOperationStatsResponse> {
        if (!investorAccount) {
            return {
                total: 0,
                types: Object.values(BazaNcOperationType).map((type) => ({
                    type,
                    total: 0,
                    offeringIds: [],
                })),
                offeringIds: [],
            };
        }

        return this.service.stats(investorAccount);
    }
}
