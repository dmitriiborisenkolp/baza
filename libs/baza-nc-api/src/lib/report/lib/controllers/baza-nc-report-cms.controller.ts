import { Body, Controller, Header, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcReportCmsEndpoint,
    BazaNcReportCmsEndpointPaths,
    BazaNcReportCmsExportToCsvRequest,
    BazaNcReportCmsGetByIdRequest,
    BazaNcReportCmsListRequest,
    BazaNcReportCmsListResponse,
    BazaNcReportCmsResendOneRequest,
    BazaNcReportCmsSendManyRequest,
    BazaNcReportCmsSendManyResponse,
    BazaNcReportCmsSendOneRequest,
    BazaNcReportCmsSyncAllRequest,
    BazaNcReportCmsSyncRequest,
    BazaNcReportDto,
    BazaNorthCapitalAcl,
    BazaNorthCapitalCMSOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaNcReportMapper } from '../mappers/baza-nc-report.mapper';
import { BazaNcReportService } from '../services/baza-nc-report.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalReportCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcReport])
export class BazaNcReportCmsController implements BazaNcReportCmsEndpoint {
    constructor(private readonly mapper: BazaNcReportMapper, private readonly service: BazaNcReportService) {}

    @Post(BazaNcReportCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns List of Reports',
    })
    @ApiOkResponse({
        type: BazaNcReportCmsListResponse,
    })
    async list(@Body() request: BazaNcReportCmsListRequest): Promise<BazaNcReportCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcReportCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns Report by ULID',
    })
    @ApiOkResponse({
        type: BazaNcReportDto,
    })
    async getById(@Body() request: BazaNcReportCmsGetByIdRequest): Promise<BazaNcReportDto> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcReportCmsEndpointPaths.sendOne)
    @ApiOperation({
        summary: 'sendOne',
        description: 'Sends Report',
    })
    @ApiOkResponse({
        type: BazaNcReportDto,
    })
    async sendOne(@Body() request: BazaNcReportCmsSendOneRequest): Promise<BazaNcReportDto> {
        const entity = await this.service.sendOne(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcReportCmsEndpointPaths.sendMany)
    @ApiOperation({
        summary: 'sendMany',
        description: 'Sends all Reports. Could be filtered by Date From and Date To',
    })
    @ApiOkResponse()
    async sendMany(@Body() request: BazaNcReportCmsSendManyRequest): Promise<BazaNcReportCmsSendManyResponse> {
        return this.service.sendMany(request);
    }

    @Post(BazaNcReportCmsEndpointPaths.resendOne)
    @ApiOperation({
        summary: 'resendOne',
        description: 'Resends Report which was sent recently',
    })
    @ApiOkResponse({
        type: BazaNcReportDto,
    })
    async resendOne(@Body() request: BazaNcReportCmsResendOneRequest): Promise<BazaNcReportDto> {
        const entity = await this.service.resendOne(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcReportCmsEndpointPaths.exportToCsv)
    @Header('Content-Type', 'text/csv')
    @ApiOperation({
        summary: 'exportToCsv',
        description: 'Exports all Reports to CSV. Could be filtered by Date From and Date To',
    })
    @ApiOkResponse()
    async exportToCsv(@Body() request: BazaNcReportCmsExportToCsvRequest): Promise<string> {
        return this.service.exportToCsv(request);
    }

    @Post(BazaNcReportCmsEndpointPaths.sync)
    @ApiOperation({
        summary: 'sync',
        description: 'Sync Status for Report',
    })
    @ApiOkResponse()
    async sync(@Body() request: BazaNcReportCmsSyncRequest): Promise<BazaNcReportDto> {
        const entity = await this.service.sync(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcReportCmsEndpointPaths.syncAll)
    @ApiOperation({
        summary: 'syncAll',
        description: 'Sync Statuses for all unreported Reports. Works in asyc way.',
    })
    @ApiOkResponse()
    async syncAll(@Body() request: BazaNcReportCmsSyncAllRequest): Promise<void> {
        await this.service.syncAll(request);
    }
}
