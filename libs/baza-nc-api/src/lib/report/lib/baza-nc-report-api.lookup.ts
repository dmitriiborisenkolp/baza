import { BazaNcReportAchIdUpdatedEvent } from './events/baza-nc-report-ach-id-updated.event';
import { BazaNcReportRegisteredEvent } from './events/baza-nc-report-registered.event';
import { BazaNcReportStatusUpdatedEvent } from './events/baza-nc-report-status-updated.event';
import { BazaNcReportUnregisteredEvent } from './events/baza-nc-report-unregistered.event';
import { BazaNcReportApiModule } from './baza-nc-report-api.module';
import { BazaNcReportMapper } from './mappers/baza-nc-report.mapper';
import { BazaNcReportService } from './services/baza-nc-report.service';
import { BazaNcReportMailService } from './services/baza-nc-report-mail.service';
import { BazaNcReportExportToCsvService } from './services/baza-nc-report-export-to-csv.service';

/**
 * Lookup for Baza NC API - Report Resources
 */
export const BazaNcReportApiLookup = {
    modules: {
        BazaNcReportApiModule: BazaNcReportApiModule,
    },
    events: {
        BazaNcReportAchIdUpdatedEvent: BazaNcReportAchIdUpdatedEvent,
        BazaNcReportRegisteredEvent: BazaNcReportRegisteredEvent,
        BazaNcReportStatusUpdatedEvent: BazaNcReportStatusUpdatedEvent,
        BazaNcReportUnregisteredEvent: BazaNcReportUnregisteredEvent,
    },
    mappers: {
        BazaNcReportMapper: BazaNcReportMapper,
    },
    services: {
        BazaNcReportService: BazaNcReportService,
        BazaNcReportMailService: BazaNcReportMailService,
        BazaNcReportExportToCsvService: BazaNcReportExportToCsvService,
    },
};
