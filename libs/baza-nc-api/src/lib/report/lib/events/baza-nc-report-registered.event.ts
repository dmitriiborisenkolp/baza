import { BazaNcReportEntity } from '../../../typeorm';

/**
 * Event which will be published when Report is registered
 */
export class BazaNcReportRegisteredEvent {
    constructor(
        public readonly payload: {
            report: BazaNcReportEntity;
        },
    ) {}
}
