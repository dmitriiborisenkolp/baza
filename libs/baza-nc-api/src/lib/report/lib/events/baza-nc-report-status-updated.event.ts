import { BazaNcReportEntity } from '../../../typeorm';
import { BazaNcReportStatus } from '@scaliolabs/baza-nc-shared';

/**
 * Event which will be published when Status of Report is updated
 */
export class BazaNcReportStatusUpdatedEvent {
    constructor(
        public readonly payload: {
            report: BazaNcReportEntity;
            previousStatus: BazaNcReportStatus;
            newStatus: BazaNcReportStatus;
        },
    ) {}
}
