import { BazaNcReportEntity } from '../../../typeorm';

/**
 * Event which will be published when ACH ID of existing Report is updated
 */
export class BazaNcReportAchIdUpdatedEvent {
    constructor(
        public readonly payload: {
            report: BazaNcReportEntity;
            beforeAchId: string | undefined;
            afterAchId: string;
        },
    ) {}
}
