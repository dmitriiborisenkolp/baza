import { TradeId } from '@scaliolabs/baza-nc-shared';

/**
 * Event which will be published when Report is deleted (unregistered) and tracking for Trade is off
 */
export class BazaNcReportUnregisteredEvent {
    constructor(
        public readonly payload: {
            tradeId: TradeId;
            achId: string;
        },
    ) {}
}
