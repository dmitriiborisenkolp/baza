import { Module } from '@nestjs/common';
import { BazaNcReportCmsController } from './controllers/baza-nc-report-cms.controller';
import { BazaNcReportMapper } from './mappers/baza-nc-report.mapper';
import { BazaCrudApiModule, BazaMailModule } from '@scaliolabs/baza-core-api';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { BazaNcReportService } from './services/baza-nc-report.service';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaNcReportMailService } from './services/baza-nc-report-mail.service';
import { BazaNcReportExportToCsvService } from './services/baza-nc-report-export-to-csv.service';
import { PurchaseFlowCancelledEventHandler } from './event-handlers/purchase-flow-cancelled.event-handler';
import { PurchaseFlowFundedEventHandler } from './event-handlers/purchase-flow-funded.event-handler';
import { PurchaseFlowPendingPaymentEventHandler } from './event-handlers/purchase-flow-pending-payment.event-handler';
import { BazaDwollaApiModule, BazaDwollaEscrowApiModule } from '@scaliolabs/baza-dwolla-api';

const EVENT_HANDLERS = [PurchaseFlowPendingPaymentEventHandler, PurchaseFlowCancelledEventHandler, PurchaseFlowFundedEventHandler];

@Module({
    imports: [CqrsModule, BazaCrudApiModule, BazaNcTypeormApiModule, BazaMailModule, BazaDwollaApiModule, BazaDwollaEscrowApiModule],
    controllers: [BazaNcReportCmsController],
    providers: [...EVENT_HANDLERS, BazaNcReportMapper, BazaNcReportService, BazaNcReportMailService, BazaNcReportExportToCsvService],
    exports: [BazaNcReportMapper, BazaNcReportService, BazaNcReportMailService, BazaNcReportExportToCsvService],
})
export class BazaNcReportApiModule {}
