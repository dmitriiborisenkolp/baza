import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaDwollaE2eNodeAccess,
    BazaDwollaEscrowCmsNodeAccess,
    BazaDwollaPaymentCmsNodeAccess,
} from '@scaliolabs/baza-dwolla-node-access';
import {
    BazaDwollaEscrowFundingSourceDto,
    BazaDwollaPaymentStatus,
    BazaDwollaPaymentType,
    DwollaCustomerId,
    DwollaEventTopic,
    DwollaFundingSourceType,
    DwollaTransferId,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDwollaEscrowFixtures } from '@scaliolabs/baza-dwolla-api';
import {
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcReportCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '../../../../account-verification';
import { BazaNcApiOfferingFixtures } from '../../../../offering';
import {
    BazaNcPurchaseFlowTransactionType,
    BazaNcReportCmsListResponse,
    BazaNcReportErrorCodes,
    BazaNcReportStatus,
    OfferingId,
    TradeId,
} from '@scaliolabs/baza-nc-shared';
import * as moment from 'moment';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

xdescribe('@scaliolabs/baza-nc-api/report/integration-tests/001-baza-nc-report-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccess = new BazaNcReportCmsNodeAccess(http);
    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessEscrow = new BazaDwollaEscrowCmsNodeAccess(http);
    const dataAccessE2eDwolla = new BazaDwollaE2eNodeAccess(http);
    const dataAccessNcDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessNcInvestor = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessNcOfferings = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessNcPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessDwollaPayment = new BazaDwollaPaymentCmsNodeAccess(http);

    let balanceFundingSource: BazaDwollaEscrowFundingSourceDto;
    let bankFundingSource: BazaDwollaEscrowFundingSourceDto;

    let TRADE_ID: TradeId;
    let DWOLLA_CUSTOMER_ID: DwollaCustomerId;
    let NC_OFFERING_ID: OfferingId;
    let DWOLLA_TRANSFER_ID: DwollaTransferId;
    let DWOLLA_ACH_TRANSFER_ID: DwollaTransferId;
    let LIST_RESPONSE: BazaNcReportCmsListResponse;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaDwollaEscrowFixtures.BazaDwollaEscrowFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will display that at least 1 Bank Account FS is available to use', async () => {
        const response = await dataAccessEscrow.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        bankFundingSource = response.find((next) => next.type === DwollaFundingSourceType.Bank);
        balanceFundingSource = response.find((next) => next.type === DwollaFundingSourceType.Balance);

        expect(bankFundingSource).toBeDefined();
        expect(balanceFundingSource).toBeDefined();
    });

    it('will select bank account as escrow', async () => {
        const response = await dataAccessEscrow.set({
            dwollaFundingSourceId: bankFundingSource.dwollaFundingSourceId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.type).toBe(DwollaFundingSourceType.Bank);
        expect(response.isSelectedAsEscrow).toBeTruthy();
    });

    it('will display that no purchases reported yet', async () => {
        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will successfully touches dwolla customer for account', async () => {
        await http.authE2eUser();

        const response = await dataAccessNcDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investor = await dataAccessNcInvestor.current();

        expect(isBazaErrorResponse(investor)).toBeFalsy();
        expect(investor.dwollaCustomerId).toBeDefined();

        DWOLLA_CUSTOMER_ID = investor.dwollaCustomerId;
    });

    it('will add funds to investor', async () => {
        const response = await dataAccessE2eDwolla.transferFundsFromMasterAccount({
            dwollaCustomerId: DWOLLA_CUSTOMER_ID,
            amount: '100.00',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fetch offering', async () => {
        const response = await dataAccessNcOfferings.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);

        NC_OFFERING_ID = response[0].ncOfferingId;
    });

    it('will purchase 2 shares', async () => {
        await http.authE2eUser();

        const response = await dataAccessNcPurchaseFlow.session({
            amount: 2000,
            numberOfShares: 2,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;

        const submitResponse = await dataAccessNcPurchaseFlow.submit({
            id: TRADE_ID,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display purchase reported with PendingPayment status', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccess.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);

                expect(response.items[0].investorName).toBe('E2E USER');
                expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[0].amount).toBe('20.00');
                expect(response.items[0].dateReportedAt).not.toBeDefined();
                expect(response.items[0].dateFundedAt).not.toBeDefined();
                expect(response.items[0].achId).not.toBeDefined();
                expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.totalRecordsToSend).toBe(0);
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will display that new Dwolla Payment is created', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaPayment.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Pending);
        expect(response.items[0].amount).toBe('20.00');
        expect(response.items[0].amountCents).toBe(2000);
        expect(response.items[0].type).toBe(BazaDwollaPaymentType.PurchaseShares);

        DWOLLA_TRANSFER_ID = response.items[0].dwollaTransferId;

        const fetchAchResponse = await asyncExpect(
            async () => {
                const fetchAchResponse = await dataAccessDwollaPayment.fetchAchDetails({
                    dwollaTransferId: DWOLLA_TRANSFER_ID,
                });

                expect(isBazaErrorResponse(fetchAchResponse)).toBeFalsy();

                return fetchAchResponse;
            },
            null,
            { intervalMillis: 5000 },
        );

        DWOLLA_ACH_TRANSFER_ID = fetchAchResponse.achDwollaTransferId;
    });

    it('will simulate dwolla completed webhook', async () => {
        await dataAccessE2eDwolla.runSandboxSimulations();

        await asyncExpect(
            async () => {
                const response = await dataAccessE2eDwolla.simulateWebhookEvent({
                    _links: {},
                    created: new Date().toISOString(),
                    timestamp: new Date().toISOString(),
                    resourceId: DWOLLA_ACH_TRANSFER_ID,
                    id: DWOLLA_ACH_TRANSFER_ID,
                    topic: DwollaEventTopic.bank_transfer_completed,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will display that there is 1 report ready to be sent', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccess.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);

                expect(response.items[0].investorName).toBe('E2E USER');
                expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[0].amount).toBe('20.00');
                expect(response.items[0].dateReportedAt).not.toBeDefined();
                expect(response.items[0].dateFundedAt).toBeDefined();
                expect(response.items[0].achId).toBeDefined();
                expect(response.items[0].status).toBe(BazaNcReportStatus.ReadyToReport);

                expect(response.totalRecordsToSend).toBe(1);
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will purchase 3 more shares', async () => {
        await http.authE2eUser();

        const response = await dataAccessNcPurchaseFlow.session({
            amount: 3000,
            numberOfShares: 3,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;

        const submitResponse = await dataAccessNcPurchaseFlow.submit({
            id: TRADE_ID,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display that there is 1 report Ready to Report and 1 more report for Pending Payment', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccess.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(2);

                expect(response.items[0].investorName).toBe('E2E USER');
                expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[0].amount).toBe('30.00');
                expect(response.items[0].dateReportedAt).not.toBeDefined();
                expect(response.items[0].dateFundedAt).not.toBeDefined();
                expect(response.items[0].achId).not.toBeDefined();
                expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[1].investorName).toBe('E2E USER');
                expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[1].amount).toBe('20.00');
                expect(response.items[1].dateReportedAt).not.toBeDefined();
                expect(response.items[1].dateFundedAt).toBeDefined();
                expect(response.items[1].achId).toBeDefined();
                expect(response.items[1].status).toBe(BazaNcReportStatus.ReadyToReport);

                expect(response.totalRecordsToSend).toBe(1);
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will purchase 1 more share', async () => {
        await http.authE2eUser();

        const response = await dataAccessNcPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;

        const submitResponse = await dataAccessNcPurchaseFlow.submit({
            id: TRADE_ID,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display that there is 1 report Ready to Report and 2 more reports for Pending Payment', async () => {
        LIST_RESPONSE = await asyncExpect(
            async () => {
                const response = await dataAccess.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(3);

                expect(response.items[0].investorName).toBe('E2E USER');
                expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[0].amount).toBe('10.00');
                expect(response.items[0].dateReportedAt).not.toBeDefined();
                expect(response.items[0].dateFundedAt).not.toBeDefined();
                expect(response.items[0].achId).not.toBeDefined();
                expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[1].investorName).toBe('E2E USER');
                expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[1].amount).toBe('30.00');
                expect(response.items[1].dateReportedAt).not.toBeDefined();
                expect(response.items[1].dateFundedAt).not.toBeDefined();
                expect(response.items[1].achId).not.toBeDefined();
                expect(response.items[1].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[2].investorName).toBe('E2E USER');
                expect(response.items[2].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[2].amount).toBe('20.00');
                expect(response.items[2].dateReportedAt).not.toBeDefined();
                expect(response.items[2].dateFundedAt).toBeDefined();
                expect(response.items[2].achId).toBeDefined();
                expect(response.items[2].status).toBe(BazaNcReportStatus.ReadyToReport);

                expect(response.totalRecordsToSend).toBe(1);

                return response;
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will not allow to resend report which is not sent before', async () => {
        const response: BazaError = (await dataAccess.resendOne({
            ulid: LIST_RESPONSE.items[2].ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcReportErrorCodes.BazaNcReportCannotBeSent);
    });

    it('will send reports to NC Escrow Ops', async () => {
        await dataAccessE2e.flushMailbox();

        const response = await dataAccess.sendMany({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.affected).toBe(1);
    });

    it('will display that there is an email available', async () => {
        const now = moment(new Date()).format('YYYY-MM-DD');
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();

        expect(mailbox.length).toBe(1);

        const tradeId = LIST_RESPONSE.items[2].tradeId;
        const achId = LIST_RESPONSE.items[2].achId;

        expect(mailbox[0].subject).toBe(`Baza Client e2e – Dwolla Daily Payment transactions – ${now}`);
        expect(mailbox[0].to).toEqual(['escrow-ops@northcapital.com']);
        expect(mailbox[0].attachments).toEqual([
            {
                filename: `escrow-BAZA-${now}.csv`,
                content: `Individual ACH ID,Trade ID,Investor name,Amount,Offering name\n${achId},${tradeId},E2E USER,20.00,E2E NC Offering Fixture`,
                contentType: 'text/csv',
            },
        ]);
    });

    it('will display that report is marked as sent', async () => {
        LIST_RESPONSE = await asyncExpect(
            async () => {
                const response = await dataAccess.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(3);

                expect(response.items[0].investorName).toBe('E2E USER');
                expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[0].amount).toBe('10.00');
                expect(response.items[0].dateReportedAt).not.toBeDefined();
                expect(response.items[0].dateFundedAt).not.toBeDefined();
                expect(response.items[0].achId).not.toBeDefined();
                expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[1].investorName).toBe('E2E USER');
                expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[1].amount).toBe('30.00');
                expect(response.items[1].dateReportedAt).not.toBeDefined();
                expect(response.items[1].dateFundedAt).not.toBeDefined();
                expect(response.items[1].achId).not.toBeDefined();
                expect(response.items[1].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[2].investorName).toBe('E2E USER');
                expect(response.items[2].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[2].amount).toBe('20.00');
                expect(response.items[2].dateReportedAt).toBeDefined();
                expect(response.items[2].dateFundedAt).toBeDefined();
                expect(response.items[2].achId).toBeDefined();
                expect(response.items[2].status).toBe(BazaNcReportStatus.Reported);

                expect(response.totalRecordsToSend).toBe(0);

                return response;
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will not allow to send a report which is already sent', async () => {
        const response: BazaError = (await dataAccess.sendOne({
            ulid: LIST_RESPONSE.items[2].ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcReportErrorCodes.BazaNcReportAlreadySent);
    });

    it('will not allow to send a report of Pending Payment reports', async () => {
        const response: BazaError = (await dataAccess.sendOne({
            ulid: LIST_RESPONSE.items[1].ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcReportErrorCodes.BazaNcReportCannotBeSent);
    });

    it('will allow to resend a report which was sent before', async () => {
        await dataAccessE2e.flushMailbox();

        const response = await dataAccess.resendOne({
            ulid: LIST_RESPONSE.items[2].ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.status).toBe(BazaNcReportStatus.Reported);
    });

    it('will display that there is an email available (2)', async () => {
        const now = moment(new Date()).format('YYYY-MM-DD');
        const mailbox = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(mailbox)).toBeFalsy();

        expect(mailbox.length).toBe(1);

        const tradeId = LIST_RESPONSE.items[2].tradeId;
        const achId = LIST_RESPONSE.items[2].achId;

        expect(mailbox[0].subject).toBe(`Baza Client e2e – Dwolla Daily Payment transactions – ${now}`);
        expect(mailbox[0].to).toEqual(['escrow-ops@northcapital.com']);
        expect(mailbox[0].attachments).toEqual([
            {
                filename: `escrow-BAZA-${now}.csv`,
                content: `Individual ACH ID,Trade ID,Investor name,Amount,Offering name\n${achId},${tradeId},E2E USER,20.00,E2E NC Offering Fixture`,
                contentType: 'text/csv',
            },
        ]);
    });

    it('will not fail on sendMany request later (when there are no records to be sent)', async () => {
        const response = await dataAccess.sendMany({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.affected).toBe(0);
    });

    it('will display that there is 1 report Reported, 2 more reports for Pending Payment', async () => {
        LIST_RESPONSE = await asyncExpect(
            async () => {
                const response = await dataAccess.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(3);

                expect(response.items[0].investorName).toBe('E2E USER');
                expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[0].amount).toBe('10.00');
                expect(response.items[0].dateReportedAt).not.toBeDefined();
                expect(response.items[0].dateFundedAt).not.toBeDefined();
                expect(response.items[0].achId).not.toBeDefined();
                expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[1].investorName).toBe('E2E USER');
                expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[1].amount).toBe('30.00');
                expect(response.items[1].dateReportedAt).not.toBeDefined();
                expect(response.items[1].dateFundedAt).not.toBeDefined();
                expect(response.items[1].achId).not.toBeDefined();
                expect(response.items[1].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[2].investorName).toBe('E2E USER');
                expect(response.items[2].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[2].amount).toBe('20.00');
                expect(response.items[2].dateReportedAt).toBeDefined();
                expect(response.items[2].dateFundedAt).toBeDefined();
                expect(response.items[2].achId).toBeDefined();
                expect(response.items[2].status).toBe(BazaNcReportStatus.Reported);

                return response;
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will exports list to CSV', async () => {
        await dataAccessE2e.flushMailbox();

        const response = await dataAccess.exportToCsv({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = [
            'Individual ACH ID,Trade ID,Investor name,Amount,Offering name,Status',
            `N/A,${LIST_RESPONSE.items[0].tradeId},E2E USER,10.00,E2E NC Offering Fixture,Pending Payment`,
            `N/A,${LIST_RESPONSE.items[1].tradeId},E2E USER,30.00,E2E NC Offering Fixture,Pending Payment`,
            `${LIST_RESPONSE.items[2].achId},${LIST_RESPONSE.items[2].tradeId},E2E USER,20.00,E2E NC Offering Fixture,Reported`,
        ];

        expect(response).toBe(expected.join('\n'));
    });

    it('will not send any emails after exporting CSV', async () => {
        const response = await dataAccessE2e.mailbox();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(0);
    });

    it('will select balance as escrow', async () => {
        const response = await dataAccessEscrow.set({
            dwollaFundingSourceId: balanceFundingSource.dwollaFundingSourceId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.type).toBe(DwollaFundingSourceType.Balance);
        expect(response.isSelectedAsEscrow).toBeTruthy();
    });

    it('will purchase 4 shares with Balance Escrow', async () => {
        await http.authE2eUser();

        const response = await dataAccessNcPurchaseFlow.session({
            amount: 4000,
            numberOfShares: 4,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;

        const submitResponse = await dataAccessNcPurchaseFlow.submit({
            id: TRADE_ID,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display that there was no changes in list', async () => {
        LIST_RESPONSE = await asyncExpect(
            async () => {
                const response = await dataAccess.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(3);

                expect(response.items[0].investorName).toBe('E2E USER');
                expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[0].amount).toBe('10.00');
                expect(response.items[0].dateReportedAt).not.toBeDefined();
                expect(response.items[0].dateFundedAt).not.toBeDefined();
                expect(response.items[0].achId).not.toBeDefined();
                expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[1].investorName).toBe('E2E USER');
                expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[1].amount).toBe('30.00');
                expect(response.items[1].dateReportedAt).not.toBeDefined();
                expect(response.items[1].dateFundedAt).not.toBeDefined();
                expect(response.items[1].achId).not.toBeDefined();
                expect(response.items[1].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[2].investorName).toBe('E2E USER');
                expect(response.items[2].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[2].amount).toBe('20.00');
                expect(response.items[2].dateReportedAt).toBeDefined();
                expect(response.items[2].dateFundedAt).toBeDefined();
                expect(response.items[2].achId).toBeDefined();
                expect(response.items[2].status).toBe(BazaNcReportStatus.Reported);

                expect(response.totalRecordsToSend).toBe(0);

                return response;
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will switch balance to bank as escrow', async () => {
        const response = await dataAccessEscrow.set({
            dwollaFundingSourceId: bankFundingSource.dwollaFundingSourceId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.type).toBe(DwollaFundingSourceType.Bank);
        expect(response.isSelectedAsEscrow).toBeTruthy();
    });

    it('will display that new Dwolla Payment is created', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaPayment.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(4);
        expect(response.items[0].status).toBe(BazaDwollaPaymentStatus.Pending);
        expect(response.items[0].amount).toBe('40.00');
        expect(response.items[0].amountCents).toBe(4000);
        expect(response.items[0].type).toBe(BazaDwollaPaymentType.PurchaseShares);
    });

    it('will simulate dwolla completed webhook', async () => {
        await dataAccessE2eDwolla.runSandboxSimulations();

        const fetchAchResponse = await asyncExpect(
            async () => {
                const fetchAchResponse = await dataAccessDwollaPayment.fetchAchDetails({
                    dwollaTransferId: DWOLLA_TRANSFER_ID,
                });

                expect(isBazaErrorResponse(fetchAchResponse)).toBeFalsy();

                return fetchAchResponse;
            },
            null,
            { intervalMillis: 5000 },
        );

        DWOLLA_ACH_TRANSFER_ID = fetchAchResponse.achDwollaTransferId;

        const response = await dataAccessE2eDwolla.simulateWebhookEvent({
            _links: {},
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            resourceId: DWOLLA_ACH_TRANSFER_ID,
            id: DWOLLA_ACH_TRANSFER_ID,
            topic: DwollaEventTopic.bank_transfer_completed,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that there was no changes in list', async () => {
        LIST_RESPONSE = await asyncExpect(
            async () => {
                const response = await dataAccess.list({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(3);

                expect(response.items[0].investorName).toBe('E2E USER');
                expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[0].amount).toBe('10.00');
                expect(response.items[0].dateReportedAt).not.toBeDefined();
                expect(response.items[0].dateFundedAt).not.toBeDefined();
                expect(response.items[0].achId).not.toBeDefined();
                expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[1].investorName).toBe('E2E USER');
                expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[1].amount).toBe('30.00');
                expect(response.items[1].dateReportedAt).not.toBeDefined();
                expect(response.items[1].dateFundedAt).not.toBeDefined();
                expect(response.items[1].achId).not.toBeDefined();
                expect(response.items[1].status).toBe(BazaNcReportStatus.PendingPayment);

                expect(response.items[2].investorName).toBe('E2E USER');
                expect(response.items[2].offeringName).toBe('E2E NC Offering Fixture');
                expect(response.items[2].amount).toBe('20.00');
                expect(response.items[2].dateReportedAt).toBeDefined();
                expect(response.items[2].dateFundedAt).toBeDefined();
                expect(response.items[2].achId).toBeDefined();
                expect(response.items[2].status).toBe(BazaNcReportStatus.Reported);

                expect(response.totalRecordsToSend).toBe(0);

                return response;
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will filter list by statuses', async () => {
        const response = await dataAccess.list({
            statuses: [BazaNcReportStatus.PendingPayment],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].investorName).toBe('E2E USER');
        expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[0].amount).toBe('10.00');
        expect(response.items[0].dateReportedAt).not.toBeDefined();
        expect(response.items[0].dateFundedAt).not.toBeDefined();
        expect(response.items[0].achId).not.toBeDefined();
        expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

        expect(response.items[1].investorName).toBe('E2E USER');
        expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[1].amount).toBe('30.00');
        expect(response.items[1].dateReportedAt).not.toBeDefined();
        expect(response.items[1].dateFundedAt).not.toBeDefined();
        expect(response.items[1].achId).not.toBeDefined();
        expect(response.items[1].status).toBe(BazaNcReportStatus.PendingPayment);

        expect(response.totalRecordsToSend).toBe(0);
    });

    it('will filter export to csv by statuses', async () => {
        const response = await dataAccess.exportToCsv({
            statuses: [BazaNcReportStatus.PendingPayment],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = [
            'Individual ACH ID,Trade ID,Investor name,Amount,Offering name,Status',
            `N/A,${LIST_RESPONSE.items[0].tradeId},E2E USER,10.00,E2E NC Offering Fixture,Pending Payment`,
            `N/A,${LIST_RESPONSE.items[1].tradeId},E2E USER,30.00,E2E NC Offering Fixture,Pending Payment`,
        ];

        expect(response).toBe(expected.join('\n'));
    });

    it('will purchase 1 more share for sync tests', async () => {
        await http.authE2eUser();

        const response = await dataAccessNcPurchaseFlow.session({
            amount: 1000,
            numberOfShares: 1,
            offeringId: NC_OFFERING_ID,
            transactionType: BazaNcPurchaseFlowTransactionType.AccountBalance,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        TRADE_ID = response.id;

        const submitResponse = await dataAccessNcPurchaseFlow.submit({
            id: TRADE_ID,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display that a new report entity added with PendingPayment status', async () => {
        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);

        expect(response.items[0].investorName).toBe('E2E USER');
        expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[0].amount).toBe('10.00');
        expect(response.items[0].dateReportedAt).not.toBeDefined();
        expect(response.items[0].dateFundedAt).not.toBeDefined();
        expect(response.items[0].achId).not.toBeDefined();
        expect(response.items[0].status).toBe(BazaNcReportStatus.PendingPayment);

        expect(response.items[1].investorName).toBe('E2E USER');
        expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[1].amount).toBe('10.00');
        expect(response.items[1].dateReportedAt).not.toBeDefined();
        expect(response.items[1].dateFundedAt).not.toBeDefined();
        expect(response.items[1].achId).not.toBeDefined();
        expect(response.items[1].status).toBe(BazaNcReportStatus.PendingPayment);

        expect(response.items[2].investorName).toBe('E2E USER');
        expect(response.items[2].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[2].amount).toBe('30.00');
        expect(response.items[2].dateReportedAt).not.toBeDefined();
        expect(response.items[2].dateFundedAt).not.toBeDefined();
        expect(response.items[2].achId).not.toBeDefined();
        expect(response.items[2].status).toBe(BazaNcReportStatus.PendingPayment);

        expect(response.items[3].investorName).toBe('E2E USER');
        expect(response.items[3].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[3].amount).toBe('20.00');
        expect(response.items[3].dateReportedAt).toBeDefined();
        expect(response.items[3].dateFundedAt).toBeDefined();
        expect(response.items[3].achId).toBeDefined();
        expect(response.items[3].status).toBe(BazaNcReportStatus.Reported);

        expect(response.totalRecordsToSend).toBe(0);

        LIST_RESPONSE = response;
    });

    it('will successfully sync report with Dwolla API', async () => {
        const response = await dataAccess.sync({
            ulid: LIST_RESPONSE.items[0].ulid,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await asyncExpect(async () => {
            const response = await dataAccess.getById({
                ulid: LIST_RESPONSE.items[0].ulid,
            });

            expect(response.status).toBe(BazaNcReportStatus.ReadyToReport);
        });
    });

    it('will not allow to sync already reported item', async () => {
        const response: BazaError = (await dataAccess.sync({
            ulid: LIST_RESPONSE.items[3].ulid,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcReportErrorCodes.BazaNcReportCannotBeSynced);
    });

    it('will update synced report with correct status', async () => {
        const response = await dataAccess.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(4);

        expect(response.items[0].investorName).toBe('E2E USER');
        expect(response.items[0].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[0].amount).toBe('10.00');
        expect(response.items[0].dateReportedAt).not.toBeDefined();
        expect(response.items[0].dateFundedAt).toBeDefined();
        expect(response.items[0].achId).toBeDefined();
        expect(response.items[0].status).toBe(BazaNcReportStatus.ReadyToReport);

        expect(response.items[1].investorName).toBe('E2E USER');
        expect(response.items[1].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[1].amount).toBe('10.00');
        expect(response.items[1].dateReportedAt).not.toBeDefined();
        expect(response.items[1].dateFundedAt).not.toBeDefined();
        expect(response.items[1].achId).not.toBeDefined();
        expect(response.items[1].status).toBe(BazaNcReportStatus.PendingPayment);

        expect(response.items[2].investorName).toBe('E2E USER');
        expect(response.items[2].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[2].amount).toBe('30.00');
        expect(response.items[2].dateReportedAt).not.toBeDefined();
        expect(response.items[2].dateFundedAt).not.toBeDefined();
        expect(response.items[2].achId).not.toBeDefined();
        expect(response.items[2].status).toBe(BazaNcReportStatus.PendingPayment);

        expect(response.items[3].investorName).toBe('E2E USER');
        expect(response.items[3].offeringName).toBe('E2E NC Offering Fixture');
        expect(response.items[3].amount).toBe('20.00');
        expect(response.items[3].dateReportedAt).toBeDefined();
        expect(response.items[3].dateFundedAt).toBeDefined();
        expect(response.items[3].achId).toBeDefined();
        expect(response.items[3].status).toBe(BazaNcReportStatus.Reported);

        expect(response.totalRecordsToSend).toBe(1);

        LIST_RESPONSE = response;
    });
});
