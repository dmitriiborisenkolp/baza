import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DwollaPaymentCancelledEvent, DwollaPaymentFailedEvent } from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcReportService } from '../services/baza-nc-report.service';
import { cqrs } from '@scaliolabs/baza-core-api';

/**
 * Deletes Report Entity when Payment for Purchasing Shares is Cancelled or Failed
 */
@EventsHandler(DwollaPaymentCancelledEvent, DwollaPaymentFailedEvent)
export class PurchaseFlowCancelledEventHandler implements IEventHandler<DwollaPaymentCancelledEvent | DwollaPaymentFailedEvent> {
    constructor(private readonly reportService: BazaNcReportService) {}

    handle(event: DwollaPaymentCancelledEvent): void {
        cqrs(PurchaseFlowCancelledEventHandler.name, async () => {
            const payload = event.payload.payment.payload;

            if (payload.type === BazaDwollaPaymentType.PurchaseShares) {
                const tradeId = payload.ncTradeId;

                await this.reportService.unregisterTrade(tradeId);
            }
        });
    }
}
