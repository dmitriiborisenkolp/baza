import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaDwollaEscrowService, DwollaPaymentCreatedEvent } from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentType } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcTransactionRepository } from '../../../typeorm';
import { BazaNcReportService } from '../services/baza-nc-report.service';
import { cqrs } from '@scaliolabs/baza-core-api';

/**
 * Generates Report Entity when a new Purchase is started
 */
@EventsHandler(DwollaPaymentCreatedEvent)
export class PurchaseFlowPendingPaymentEventHandler implements IEventHandler<DwollaPaymentCreatedEvent> {
    constructor(
        private readonly tradeRepository: BazaNcTransactionRepository,
        private readonly reportService: BazaNcReportService,
        private readonly dwollaEscrow: BazaDwollaEscrowService,
    ) {}

    handle(event: DwollaPaymentCreatedEvent): void {
        cqrs(DwollaPaymentCreatedEvent.name, async () => {
            if (!this.dwollaEscrow.isBankAccount) {
                return;
            }

            const payload = event.payload.payment.payload;

            if (payload.type === BazaDwollaPaymentType.PurchaseShares) {
                const tradeId = payload.ncTradeId;
                const trade = await this.tradeRepository.findByNcTradeId(tradeId);

                if (!trade) {
                    return;
                }

                await this.reportService.registerTrade({
                    tradeId,
                    dwollaTransferId: event.payload.payment.dwollaTransferId,
                });
            }
        });
    }
}
