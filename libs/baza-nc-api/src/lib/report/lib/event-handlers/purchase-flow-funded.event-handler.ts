import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { bazaDwollaExtractId, BazaDwollaPaymentType, DwollaTransferStatus } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcTransactionRepository } from '../../../typeorm';
import { DwollaPaymentCompletedEvent, DwollaTransfersApiNestjsService } from '@scaliolabs/baza-dwolla-api';
import { BazaNcReportService } from '../services/baza-nc-report.service';
import { BazaNcReportStatus } from '@scaliolabs/baza-nc-shared';
import { cqrs } from '@scaliolabs/baza-core-api';

/**
 * Marks Report Entity as Ready to be Sent when related Dwolla Payment is successfully processed
 */
@EventsHandler(DwollaPaymentCompletedEvent)
export class PurchaseFlowFundedEventHandler implements IEventHandler<DwollaPaymentCompletedEvent> {
    constructor(private readonly reportService: BazaNcReportService) {}

    handle(event: DwollaPaymentCompletedEvent): void {
        cqrs(PurchaseFlowFundedEventHandler.name, async () => {
            const payload = event.payload.payment.payload;

            if (payload.type === BazaDwollaPaymentType.PurchaseShares) {
                await this.reportService.syncByDwollaPayment(payload.ncTradeId, event.payload.payment.dwollaTransferId);
            }
        });
    }
}
