import { Injectable } from '@nestjs/common';
import { BazaRegistryService, CsvService, I18nApiService } from '@scaliolabs/baza-core-api';
import { BazaNcReportEntity } from '../../../typeorm';
import { BazaNcReportMapper } from '../mappers/baza-nc-report.mapper';

/**
 * CSV Service for Baza NC Report
 */
@Injectable()
export class BazaNcReportExportToCsvService {
    constructor(
        private readonly csv: CsvService,
        private readonly mapper: BazaNcReportMapper,
        private readonly i18n: I18nApiService,
        private readonly registry: BazaRegistryService,
    ) {}

    /**
     * Exports Report entities to CSV
     * @param entities
     */
    async exportToCSV(entities: Array<BazaNcReportEntity>): Promise<string> {
        const dtos = await this.mapper.entitiesToDTOs(entities);
        const delimiter = this.registry.getValue('bazaNc.escrowCsvDelimiter');

        const header = ['achId', 'tradeId', 'investorName', 'amount', 'offeringName'].map((next) =>
            this.i18n.get(`baza-nc.mail.report.sendToNCEscrowOPS.csv.${next}`),
        );

        const rows = dtos.map((next) => [next.achId || 'N/A', next.tradeId, next.investorName, next.amount, next.offeringName]);

        return this.csv.exportToCSV({
            delimiter,
            rows: [header, ...rows],
        });
    }

    /**
     * Exports Report entities to CSV (with Statuses)
     * @param entities
     */
    async exportToCSVWithStatuses(entities: Array<BazaNcReportEntity>): Promise<string> {
        const dtos = await this.mapper.entitiesToDTOs(entities);
        const delimiter = this.registry.getValue('bazaNc.escrowCsvDelimiter');

        const header = ['achId', 'tradeId', 'investorName', 'amount', 'offeringName', 'status'].map((next) =>
            this.i18n.get(`baza-nc.mail.report.sendToNCEscrowOPS.csv.${next}`),
        );

        const rows = dtos.map((next) => [
            next.achId || 'N/A',
            next.tradeId,
            next.investorName,
            next.amount,
            next.offeringName,
            this.i18n.get(`baza-nc.mail.report.sendToNCEscrowOPS.statuses.${next.status}`),
        ]);

        return this.csv.exportToCSV({
            delimiter,
            rows: [header, ...rows],
        });
    }
}
