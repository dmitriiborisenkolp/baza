import { Injectable } from '@nestjs/common';
import { BazaRegistryService, MailService } from '@scaliolabs/baza-core-api';
import { getBazaProjectCodeName, replaceTags } from '@scaliolabs/baza-core-shared';
import { BazaNcReportEntity } from '../../../typeorm';
import { BazaNcReportMapper } from '../mappers/baza-nc-report.mapper';
import { BazaNcReportExportToCsvService } from './baza-nc-report-export-to-csv.service';
import * as moment from 'moment';
import { BazaNcMail } from '../../../../constants/baza-nc.mail-templates';

/**
 * Mail Service for Baza NC Report Feature
 */
@Injectable()
export class BazaNcReportMailService {
    constructor(
        private readonly mail: MailService,
        private readonly registry: BazaRegistryService,
        private readonly mapper: BazaNcReportMapper,
        private readonly csv: BazaNcReportExportToCsvService,
    ) {}

    /**
     * Sends Daily Payment Notifications to North Capital Escrow OPS
     * @param entities
     */
    async sendToNCEscrowOPS(entities: Array<BazaNcReportEntity>): Promise<void> {
        const email = this.registry.getValue('bazaNc.escrowOpsEmail');
        const subject = this.registry.getValue('bazaNc.escrowOpsEmailSubject');
        const now = moment(new Date()).format('YYYY-MM-DD');
        const entityDTOs = await this.mapper.entitiesToDTOs(entities);

        await this.mail.send({
            name: BazaNcMail.BazaNcReportNcEscrowNotification,
            to: [
                {
                    email,
                },
            ],
            subject: replaceTags(subject, {
                clientName: this.registry.getValue('bazaCommon.clientName'),
                date: now,
            }),
            variables: () => ({
                entities: entityDTOs,
            }),
            attachments: [
                {
                    filename: `escrow-${getBazaProjectCodeName()}-${now}.csv`,
                    content: await this.csv.exportToCSV(entities),
                    contentType: 'text/csv',
                },
            ],
        });
    }
}
