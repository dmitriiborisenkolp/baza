import { Injectable } from '@nestjs/common';
import { CrudService } from '@scaliolabs/baza-core-api';
import { BazaNcReportMapper } from '../mappers/baza-nc-report.mapper';
import { BazaNcReportEntity, BazaNcReportRepository, BazaNcTransactionRepository } from '../../../typeorm';
import {
    BazaNcReportCmsExportToCsvRequest,
    BazaNcReportCmsGetByIdRequest,
    BazaNcReportCmsListRequest,
    BazaNcReportCmsListResponse,
    BazaNcReportCmsResendOneRequest,
    BazaNcReportCmsSendManyRequest,
    BazaNcReportCmsSendManyResponse,
    BazaNcReportCmsSendOneRequest,
    BazaNcReportCmsSyncAllRequest,
    BazaNcReportCmsSyncRequest,
    BazaNcReportDto,
    BazaNcReportStatus,
    bazaNcReportStatusesToSend,
    bazaNcReportStatusesToSync,
    TradeId,
} from '@scaliolabs/baza-nc-shared';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { Between, In } from 'typeorm';
import * as moment from 'moment';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcReportUnregisteredEvent } from '../events/baza-nc-report-unregistered.event';
import { BazaNcReportStatusUpdatedEvent } from '../events/baza-nc-report-status-updated.event';
import { BazaNcReportRegisteredEvent } from '../events/baza-nc-report-registered.event';
import { BazaNcReportAchIdUpdatedEvent } from '../events/baza-nc-report-ach-id-updated.event';
import { BazaNcReportAlreadySentException } from '../exceptions/baza-nc-report-already-sent.exception';
import { BazaNcReportMailService } from './baza-nc-report-mail.service';
import { CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { BazaNcReportExportToCsvService } from './baza-nc-report-export-to-csv.service';
import { BazaNcReportCannotBeSentException } from '../exceptions/baza-nc-report-cannot-be-sent.exception';
import { BazaNcReportCannotBeSyncedException } from '../exceptions/baza-nc-report-cannot-be-synced.exception';
import { BazaDwollaPaymentRepository, BazaDwollaPaymentService, DwollaTransfersApiNestjsService } from '@scaliolabs/baza-dwolla-api';
import { bazaDwollaExtractId, DwollaTransferStatus } from '@scaliolabs/baza-dwolla-shared';

class ListEntitiesRequest extends CrudListRequestDto<BazaNcReportEntity> {
    statuses?: Array<BazaNcReportStatus>;
}

/**
 * Service for Baza NC Report
 */
@Injectable()
export class BazaNcReportService {
    constructor(
        private readonly crud: CrudService,
        private readonly eventBus: EventBus,
        private readonly mapper: BazaNcReportMapper,
        private readonly repository: BazaNcReportRepository,
        private readonly mailService: BazaNcReportMailService,
        private readonly csvService: BazaNcReportExportToCsvService,
        private readonly dwollaTransferApi: DwollaTransfersApiNestjsService,
        private readonly dwollaPaymentService: BazaDwollaPaymentService,
        private readonly dwollaPaymentRepository: BazaDwollaPaymentRepository,
        private readonly transactionRepository: BazaNcTransactionRepository,
    ) {}

    /**
     * Returns List of Reports
     * @see BazaNcReportService.listEntities
     * @param request
     */
    async list(request: BazaNcReportCmsListRequest): Promise<BazaNcReportCmsListResponse> {
        const whereOptions: Array<FindConditions<BazaNcReportEntity>> = [];

        const findOptions: FindManyOptions<BazaNcReportEntity> = {
            where: whereOptions,
            relations: this.repository.relations,
            order: {
                dateCreatedAt: 'DESC',
            },
        };

        if (request.statuses) {
            if (!whereOptions.length) {
                whereOptions.push({});
            }

            for (const condition of whereOptions) {
                condition.status = In(request.statuses);
            }
        }

        if (request.dateFrom && request.dateTo) {
            if (!whereOptions.length) {
                whereOptions.push({});
            }

            for (const condition of whereOptions) {
                condition.dateFundedAt = Between(
                    moment(request.dateFrom)
                        .set({
                            hours: 0,
                            minutes: 0,
                            seconds: 0,
                            milliseconds: 0,
                        })
                        .toDate(),
                    moment(request.dateTo)
                        .set({
                            hours: 23,
                            minutes: 59,
                            seconds: 59,
                            milliseconds: 999,
                        })
                        .toDate(),
                );
            }
        }

        const listResponse = await this.crud.find<BazaNcReportEntity, BazaNcReportDto>({
            request,
            findOptions,
            entity: BazaNcReportEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });

        return {
            ...listResponse,
            totalRecordsToSend: await this.totalCountRecordsToReport(request),
        };
    }

    /**
     * Returns List of Reports (as TypeORM Entities)
     * @see BazaNcReportService.list
     * @param request
     */
    async listEntities(request: ListEntitiesRequest): Promise<CrudListResponseDto<BazaNcReportEntity>> {
        const whereOptions: Array<FindConditions<BazaNcReportEntity>> = [];

        const findOptions: FindManyOptions<BazaNcReportEntity> = {
            where: whereOptions,
            relations: this.repository.relations,
            order: {
                dateCreatedAt: 'DESC',
            },
        };

        if (request.statuses) {
            if (!whereOptions.length) {
                whereOptions.push({});
            }

            for (const condition of whereOptions) {
                condition.status = In(request.statuses);
            }
        }

        if (request.dateFrom && request.dateTo) {
            if (!whereOptions.length) {
                whereOptions.push({});
            }

            for (const condition of whereOptions) {
                condition.dateFundedAt = Between(
                    moment(request.dateFrom)
                        .set({
                            hours: 0,
                            minutes: 0,
                            seconds: 0,
                            milliseconds: 0,
                        })
                        .toDate(),
                    moment(request.dateTo)
                        .set({
                            hours: 23,
                            minutes: 59,
                            seconds: 59,
                            milliseconds: 999,
                        })
                        .toDate(),
                );
            }
        }

        return this.crud.find<BazaNcReportEntity, BazaNcReportEntity>({
            request,
            findOptions,
            entity: BazaNcReportEntity,
        });
    }

    /**
     * Returns Report by ULID
     * @param request
     */
    async getById(request: BazaNcReportCmsGetByIdRequest): Promise<BazaNcReportEntity> {
        return this.repository.getByUlid(request.ulid);
    }

    /**
     * Sends Report
     * @param request
     */
    async sendOne(request: BazaNcReportCmsSendOneRequest): Promise<BazaNcReportEntity> {
        const report = await this.repository.getByUlid(request.ulid);

        if (report.isSent) {
            throw new BazaNcReportAlreadySentException();
        }

        if (!report.canBeSent) {
            throw new BazaNcReportCannotBeSentException();
        }

        await this.mailService.sendToNCEscrowOPS([report]);

        return this.updateReportStatus(BazaNcReportStatus.Reported, {
            tradeId: report.tradeId,
            achId: report.achId,
        });
    }

    /**
     * Sends all Reports. Could be filtered by Date From and Date To
     * @param request
     */
    async sendMany(request: BazaNcReportCmsSendManyRequest): Promise<BazaNcReportCmsSendManyResponse> {
        const reports = (
            await this.listEntities({
                ...request,
                size: -1,
            })
        ).items.filter((next) => !next.isSent && !!next.achId && next.status === BazaNcReportStatus.ReadyToReport);

        if (reports.length === 0) {
            return {
                affected: 0,
            };
        }

        if (reports.length > 0) {
            await this.mailService.sendToNCEscrowOPS(reports);

            await Promise.all(
                reports.map((report) =>
                    this.updateReportStatus(BazaNcReportStatus.Reported, {
                        tradeId: report.tradeId,
                        achId: report.achId,
                    }),
                ),
            );
        }

        return {
            affected: reports.length,
        };
    }

    /**
     * Sends all Reports. Could be filtered by Date From and Date To
     * @param request
     */
    async resendOne(request: BazaNcReportCmsResendOneRequest): Promise<BazaNcReportEntity> {
        const report = await this.repository.getByUlid(request.ulid);

        if (!report.isSent || !report.canBeResent) {
            throw new BazaNcReportCannotBeSentException();
        }

        await this.mailService.sendToNCEscrowOPS([report]);

        return this.updateReportStatus(BazaNcReportStatus.Reported, {
            tradeId: report.tradeId,
            achId: report.achId,
        });
    }

    /**
     * Exports all Reports to CSV. Could be filtered by Date From and Date To
     * @param request
     */
    async exportToCsv(request: BazaNcReportCmsExportToCsvRequest): Promise<string> {
        const reports = (
            await this.listEntities({
                ...request,
                size: -1,
            })
        ).items;

        const isReadyToReportOnly =
            Array.isArray(request.statuses) && request.statuses.length === 1 && request.statuses[0] === BazaNcReportStatus.ReadyToReport;

        return isReadyToReportOnly ? this.csvService.exportToCSV(reports) : this.csvService.exportToCSVWithStatuses(reports);
    }

    /**
     * Sync Status for Report
     * @param request
     */
    async sync(request: BazaNcReportCmsSyncRequest): Promise<BazaNcReportEntity> {
        const report = await this.repository.getByUlid(request.ulid);

        if (!bazaNcReportStatusesToSync.includes(report.status)) {
            throw new BazaNcReportCannotBeSyncedException();
        }

        await this.syncFundedTransfer(report);

        return this.repository.getByUlid(request.ulid);
    }

    /**
     * Sync Funded Transfer Status of Report Entity
     * Additionally will sync Dwolla Payment, if required
     * @param report
     */
    async syncFundedTransfer(report: BazaNcReportEntity): Promise<void> {
        const transfer = await this.dwollaTransferApi.getTransfer(report.dwollaTransferId);

        if (transfer.status === DwollaTransferStatus.Processed) {
            const fundedTransferLink = transfer._links['funded-transfer'];

            if (!fundedTransferLink) {
                return;
            }

            const fundedTransferId = bazaDwollaExtractId(fundedTransferLink.href);

            if (!fundedTransferId) {
                return;
            }

            const fundedTransfer = await this.dwollaTransferApi.getTransfer(fundedTransferId);
            const achId = fundedTransfer.individualAchId;

            if (!achId || fundedTransfer.status !== DwollaTransferStatus.Processed) {
                return;
            }

            await this.updateReportStatus(BazaNcReportStatus.ReadyToReport, {
                tradeId: report.tradeId,
                achId,
            });

            const dwollaPayment = await this.dwollaPaymentRepository.findByDwollaTransferId(report.dwollaTransferId);

            if (dwollaPayment) {
                await this.dwollaPaymentService.sync(dwollaPayment.ulid);
            }
        }
    }

    /**
     * Sync Statuses for all unreported Reports. Works in asyc way.
     * @param request
     */
    async syncAll(request: BazaNcReportCmsSyncAllRequest): Promise<void> {
        const reports = (
            await this.listEntities({
                ...request,
                statuses: bazaNcReportStatusesToSync,
                size: -1,
            })
        ).items;

        if (reports.length === 0) {
            return;
        }

        const promise = Promise.all(reports.map((next) => this.sync({ ulid: next.ulid })));

        if (!request.async) {
            await promise;
        }
    }

    /**
     * Registers Trade for Report processing
     * If Trade is already registered, the method will returns existing report entity. If existing entity has different
     * ACH ID, ACH ID will be updated & saved to DB
     * @param payload
     */
    async registerTrade(payload: { tradeId: TradeId; dwollaTransferId?: string; achId?: string }): Promise<BazaNcReportEntity> {
        const existing = await this.repository.findByNcTradeId(payload.tradeId);

        if (existing) {
            if (payload.achId && existing.achId !== payload.achId) {
                const beforeAchId = existing.achId;
                const afterAchId = payload.achId;

                existing.achId = afterAchId;

                await this.repository.save([existing]);

                this.eventBus.publish(
                    new BazaNcReportAchIdUpdatedEvent({
                        report: existing,
                        beforeAchId,
                        afterAchId,
                    }),
                );
            }

            return existing;
        } else {
            const report = new BazaNcReportEntity();

            report.tradeId = payload.tradeId;
            report.dwollaTransferId = payload.dwollaTransferId;
            report.achId = payload.achId;
            report.status = BazaNcReportStatus.PendingPayment;

            await this.repository.save([report]);

            this.eventBus.publish(
                new BazaNcReportRegisteredEvent({
                    report,
                }),
            );

            return report;
        }
    }

    /**
     * Sync Report with NC Trade Id and Dwolla Transfer Id
     * @param ncTradeId
     * @param dwollaTransferId
     */
    async syncByDwollaPayment(ncTradeId: TradeId, dwollaTransferId: string): Promise<void> {
        const trade = await this.transactionRepository.findByNcTradeId(ncTradeId);

        if (!trade) {
            return;
        }

        const transfer = await this.dwollaTransferApi.getTransfer(dwollaTransferId);
        const fundedTransferLink = transfer._links['funded-transfer'];

        if (!fundedTransferLink) {
            return;
        }

        const fundedTransferId = bazaDwollaExtractId(fundedTransferLink.href);

        if (!fundedTransferId) {
            return;
        }

        const fundedTransfer = await this.dwollaTransferApi.getTransfer(fundedTransferId);
        const achId = fundedTransfer.individualAchId;

        if (!achId || fundedTransfer.status !== DwollaTransferStatus.Processed) {
            return;
        }

        await this.updateReportStatus(BazaNcReportStatus.ReadyToReport, {
            achId,
            tradeId: ncTradeId,
        });
    }

    /**
     * Unregisters Trade from Reports
     * @param tradeId
     */
    async unregisterTrade(tradeId: TradeId): Promise<void> {
        const entity = await this.repository.findByNcTradeId(tradeId);

        if (entity) {
            await this.repository.deleteByTradeId(tradeId);

            this.eventBus.publish(
                new BazaNcReportUnregisteredEvent({
                    tradeId: entity.tradeId,
                    achId: entity.achId,
                }),
            );
        }
    }

    /**
     * Updates Report Status for Report by Trade Id
     * If existing entity has different ACH ID, ACH ID will be updated & saved to DB
     * @param status
     * @param query
     */
    async updateReportStatus(
        status: BazaNcReportStatus,
        query: {
            tradeId: TradeId;
            achId: string;
        },
    ): Promise<BazaNcReportEntity> {
        const report = await this.registerTrade(query);

        if (report.status !== BazaNcReportStatus.Reported) {
            const previousStatus = report.status;
            const newStatus = status;

            report.status = newStatus;

            if (status === BazaNcReportStatus.Reported) {
                report.dateReportedAt = new Date();
            } else if (status === BazaNcReportStatus.ReadyToReport) {
                report.dateFundedAt = new Date();
            }

            await this.repository.save([report]);

            this.eventBus.publish(
                new BazaNcReportStatusUpdatedEvent({
                    report,
                    previousStatus,
                    newStatus,
                }),
            );
        }

        return report;
    }

    /**
     * Returns total count of Reports which are ready to be sent to NC
     */
    async totalCountRecordsToReport(listRequest: Pick<BazaNcReportCmsListRequest, 'dateTo' | 'dateFrom'>): Promise<number> {
        const whereOptions: Array<FindConditions<BazaNcReportEntity>> = [];

        const findOptions: FindManyOptions<BazaNcReportEntity> = {
            where: whereOptions,
        };

        if (listRequest.dateFrom && listRequest.dateTo) {
            if (!whereOptions.length) {
                whereOptions.push({});
            }

            for (const condition of whereOptions) {
                condition.dateFundedAt = Between(
                    moment(listRequest.dateFrom)
                        .set({
                            hours: 0,
                            minutes: 0,
                            seconds: 0,
                            milliseconds: 0,
                        })
                        .toDate(),
                    moment(listRequest.dateTo)
                        .set({
                            hours: 23,
                            minutes: 59,
                            seconds: 59,
                            milliseconds: 999,
                        })
                        .toDate(),
                );
            }
        }

        if (!whereOptions.length) {
            whereOptions.push({});
        }

        for (const condition of whereOptions) {
            condition.status = In(bazaNcReportStatusesToSend);
        }

        return this.repository.repository.count(findOptions);
    }
}
