import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcReportErrorCodes, bazaNcReportErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcReportCannotBeSentException extends BazaAppException {
    constructor() {
        super(
            BazaNcReportErrorCodes.BazaNcReportCannotBeSent,
            bazaNcReportErrorCodesI18n[BazaNcReportErrorCodes.BazaNcReportCannotBeSent],
            HttpStatus.BAD_REQUEST,
        );
    }
}
