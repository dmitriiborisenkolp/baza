import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcReportErrorCodes, bazaNcReportErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcReportAlreadySentException extends BazaAppException {
    constructor() {
        super(
            BazaNcReportErrorCodes.BazaNcReportAlreadySent,
            bazaNcReportErrorCodesI18n[BazaNcReportErrorCodes.BazaNcReportAlreadySent],
            HttpStatus.BAD_REQUEST,
        );
    }
}
