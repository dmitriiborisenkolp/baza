import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcReportErrorCodes, bazaNcReportErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcReportCannotBeSyncedException extends BazaAppException {
    constructor() {
        super(
            BazaNcReportErrorCodes.BazaNcReportCannotBeSynced,
            bazaNcReportErrorCodesI18n[BazaNcReportErrorCodes.BazaNcReportCannotBeSynced],
            HttpStatus.BAD_REQUEST,
        );
    }
}
