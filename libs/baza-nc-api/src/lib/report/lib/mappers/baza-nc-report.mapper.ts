import { Injectable } from '@nestjs/common';
import {
    BazaNcOfferingEntity,
    BazaNcOfferingRepository,
    BazaNcReportEntity,
    BazaNcTransactionEntity,
    BazaNcTransactionRepository,
} from '../../../typeorm';
import { BazaNcReportDto, convertFromCents, OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';
import { AccountEntity, BazaAccountRepository } from '@scaliolabs/baza-core-api';

interface Cache {
    trades: Array<BazaNcTransactionEntity>;
    offerings: Array<BazaNcOfferingEntity>;
    // TODO: Use NC Account Verification Data when Account Verification endpoint will be optimized
    accounts: Array<AccountEntity>;
}

/**
 * Cache Service for Mapper
 */
class MapperCacheManager {
    private readonly cache: {
        trades: Record<string, BazaNcTransactionEntity>;
        offerings: Record<string, BazaNcOfferingEntity>;
        accounts: Record<string, AccountEntity>;
    } = {
        trades: {},
        offerings: {},
        accounts: {},
    };

    constructor(
        private readonly accountRepository: BazaAccountRepository,
        private readonly tradeRepository: BazaNcTransactionRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
        public readonly initial?: Cache,
    ) {
        if (initial) {
            initial.trades.forEach((next) => (this.cache[next.ncTradeId] = next));
            initial.offerings.forEach((next) => (this.cache[next.ncOfferingId] = next));
            initial.accounts.forEach((next) => (this.cache[next.id] = next));
        }
    }

    async account(accountId: number): Promise<AccountEntity | undefined> {
        // eslint-disable-next-line no-prototype-builtins
        if (!this.cache.accounts.hasOwnProperty(accountId)) {
            // eslint-disable-next-line security/detect-object-injection
            this.cache.accounts[accountId] = await this.accountRepository.findActiveOrDeactivatedAccountWithId(accountId);
        }

        // eslint-disable-next-line security/detect-object-injection
        return this.cache.accounts[accountId];
    }

    async trade(ncTradeId: TradeId): Promise<BazaNcTransactionEntity | undefined> {
        // eslint-disable-next-line no-prototype-builtins
        if (!this.cache.trades.hasOwnProperty(ncTradeId)) {
            // eslint-disable-next-line security/detect-object-injection
            this.cache.trades[ncTradeId] = await this.tradeRepository.findByNcTradeId(ncTradeId);
        }

        // eslint-disable-next-line security/detect-object-injection
        return this.cache.trades[ncTradeId];
    }

    async offering(ncOfferingId: OfferingId): Promise<BazaNcOfferingEntity | undefined> {
        // eslint-disable-next-line no-prototype-builtins
        if (!this.cache.offerings.hasOwnProperty(ncOfferingId)) {
            // eslint-disable-next-line security/detect-object-injection
            this.cache.offerings[ncOfferingId] = await this.offeringRepository.findOfferingWithId(ncOfferingId);
        }

        // eslint-disable-next-line security/detect-object-injection
        return this.cache.offerings[ncOfferingId];
    }
}

/**
 * BazaNcReportEntity to BazaNcReportDto Mapper
 */
@Injectable()
export class BazaNcReportMapper {
    constructor(
        private readonly accountRepository: BazaAccountRepository,
        private readonly tradeRepository: BazaNcTransactionRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
    ) {}

    /**
     * Converts BazaNcReportEntity to BazaNcReportDto (Single Entity)
     * @param entity
     * @param withCache
     */
    async entityToDTO(entity: BazaNcReportEntity, withCache?: MapperCacheManager): Promise<BazaNcReportDto> {
        const cache = withCache || new MapperCacheManager(this.accountRepository, this.tradeRepository, this.offeringRepository);

        const result: BazaNcReportDto = {
            ulid: entity.ulid,
            dateCreatedAt: entity.dateCreatedAt.toISOString(),
            dateFundedAt: entity.dateFundedAt?.toISOString(),
            dateReportedAt: entity.dateReportedAt?.toISOString(),
            tradeId: entity.tradeId,
            achId: entity.achId || undefined,
            status: entity.status,
            investorName: 'N/A',
            offeringName: 'N/A',
            amount: 'N/A',
            canBeSent: entity.canBeSent,
            canBeResent: entity.canBeResent,
            canBySynced: entity.canBySynced,
        };

        const trade = await cache.trade(entity.tradeId);

        if (trade) {
            const account = await cache.account(trade.investorAccount.userId);
            const offering = await cache.offering(trade.ncOfferingId);

            if (account) {
                result.investorName = (account.fullName || '').toString().toUpperCase();
            }

            if (offering) {
                result.offeringName = offering.ncOfferingName;
            }

            result.amount = convertFromCents(trade.amountCents).toFixed(2);
        }

        return result;
    }

    /**
     * Converts BazaNcReportEntity to BazaNcReportDto (Many Entity)
     * @param entities
     */
    async entitiesToDTOs(entities: Array<BazaNcReportEntity>): Promise<Array<BazaNcReportDto>> {
        const cache = new MapperCacheManager(this.accountRepository, this.tradeRepository, this.offeringRepository, {
            trades: await this.tradeRepository.findByNcTradeIds(entities.map((next) => next.tradeId)),
            offerings: [],
            accounts: [],
        });

        return Promise.all(entities.map((next) => this.entityToDTO(next, cache)));
    }
}
