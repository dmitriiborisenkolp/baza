import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcInvestorAccountErrorCodes, bazaNcInvestorAccountErrorCodesEnI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcNoInvestorAccountAvailableForAccountException extends BazaAppException {
    constructor() {
        super(
            BazaNcInvestorAccountErrorCodes.NcInvestorAccountIsNotAvailableForAccount,
            bazaNcInvestorAccountErrorCodesEnI18n[BazaNcInvestorAccountErrorCodes.NcInvestorAccountIsNotAvailableForAccount],
            HttpStatus.BAD_REQUEST,
        );
    }
}
