import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalAccountVerificationErrorCodes } from '@scaliolabs/baza-nc-shared';

export class BazaNcInvalidSSNException extends BazaAppException {
    constructor() {
        super(
            BazaNorthCapitalAccountVerificationErrorCodes.NcInvalidSSNException,
            'Your SSN code should have at least 9 numbers',
            HttpStatus.BAD_REQUEST,
        );
    }
}
