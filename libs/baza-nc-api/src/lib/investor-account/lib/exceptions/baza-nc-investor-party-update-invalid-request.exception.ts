import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcInvestorAccountErrorCodes, bazaNcInvestorAccountErrorCodesEnI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcInvestorPartyUpdateInvalidRequestException extends BazaAppException {
    constructor() {
        super(
            BazaNcInvestorAccountErrorCodes.NcInvestorAccountUpdatePartyInvalidPayload,
            bazaNcInvestorAccountErrorCodesEnI18n[BazaNcInvestorAccountErrorCodes.NcInvestorAccountUpdatePartyInvalidPayload],
            HttpStatus.BAD_REQUEST,
        );
    }
}
