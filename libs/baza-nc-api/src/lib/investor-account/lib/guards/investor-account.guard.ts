import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request as ExpressRequest } from 'express';
import {
    AuthAccountResetPasswordRequestedException,
    AuthInvalidJwtException,
    AuthService,
    AuthSessionService,
    BazaAuthApiModuleConfig,
} from '@scaliolabs/baza-core-api';
import { RequestWithInvestor } from '../models/request-with-investor.model';
import { BazaNcInvestorAccountService } from '../services/baza-nc-investor-account.service';

/**
 * InvestorAccountGuard is used sometimes in controller's UseGuards to check if user is authenticated
 * or not. If not, user will not be allowed to use method
 *
 * Also , if user , is not an investor (does not investor account) will not be allowed to use method.
 *
 * InvestorAccountGuard completely duplicated from AuthGuard, so main functionality of AuthGuard -
 * did not change due to this copy.
 *
 * Additional information:
 * InvestorAccountGuard, uses RequestWithInvestor as a type of Request, and it injects investorAccount, into request.
 *
 * Usage: completely like AuthGuard, we can use this guard => @UseGuards(InvestorAccountGuard)
 */
@Injectable()
export class InvestorAccountGuard implements CanActivate {
    constructor(
        private readonly moduleConfig: BazaAuthApiModuleConfig,
        private readonly authSession: AuthSessionService,
        private readonly authService: AuthService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const path = context.switchToHttp().getRequest<ExpressRequest>().path;

        if (this.moduleConfig.skipAuthGuardForEndpoints.includes(path)) {
            return true;
        } else {
            const request = context.switchToHttp().getRequest<RequestWithInvestor>();
            const header = (request.headers.authorization || '').split(' ');

            if (header.length === 2 && header[0].toLowerCase() === 'bearer') {
                const jwt: string = header[1];

                const verifyResponse = await this.authService.verify({
                    jwt: jwt,
                });

                this.authSession.setupSession(jwt, verifyResponse.jwtPayload);

                const account = await this.authSession.getAccount();

                if (account.manualResetPasswordRequest) {
                    throw new AuthAccountResetPasswordRequestedException(account.manualResetPasswordRequest.attemptToSignInMessage);
                }

                request.user = account;

                request.investorAccount = await this.investorAccountService.getInvestorAccountByUserId(account.id);

                return true;
            } else {
                throw new AuthInvalidJwtException();
            }
        }
    }
}
