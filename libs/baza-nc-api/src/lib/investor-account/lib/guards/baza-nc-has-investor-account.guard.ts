import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthSessionService } from '@scaliolabs/baza-core-api';
import { BazaNcInvestorAccountService } from '../services/baza-nc-investor-account.service';
import { BazaNcNoInvestorAccountAvailableForAccountException } from '../exceptions/baza-nc-no-investor-account-available-for-account.exception';

@Injectable()
export class BazaNcHasInvestorAccountGuard implements CanActivate {
    constructor(private readonly authSession: AuthSessionService, private readonly investorAccount: BazaNcInvestorAccountService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const account = await this.authSession.getAccount();
        const investorAccount = await this.investorAccount.findInvestorAccountByUserId(account.id);

        if (!investorAccount?.northCapitalAccountId) {
            throw new BazaNcNoInvestorAccountAvailableForAccountException();
        }

        return true;
    }
}
