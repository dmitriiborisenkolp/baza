import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { DwollaCustomerStatus } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcInvestorAccountUpdatePersonalInfoCommand, KYCStatus } from '@scaliolabs/baza-nc-shared';
import * as moment from 'moment';
import {
    BazaNcAccountVerificationAccountSetupService,
    BazaNcInvestorDobShouldBeAbove18Exception,
    BazaNcInvestorDobYearIsInvalidException,
    SetupResponse,
} from '../../../account-verification';
import { BazaNcInvestorDobIsInvalidException } from '../../../account-verification/lib/exceptions/baza-nc-investor-dob-is-invalid.exception';
import { NorthCapitalAccountsApiNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountEntity, BazaNcInvestorAccountRepository } from '../../../typeorm';
import { BazaNcInvestorPartyUpdateInvalidRequestException } from '../exceptions/baza-nc-investor-party-update-invalid-request.exception';
import { BazaNcNoInvestorAccountAvailableForAccountException } from '../exceptions/baza-nc-no-investor-account-available-for-account.exception';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcInvestorAccountUpdatePersonalInfoCommand)
export class NcInvestorAccountUpdatePersonalInfoCommandHandler implements ICommandHandler<BazaNcInvestorAccountUpdatePersonalInfoCommand> {
    constructor(
        private readonly repository: BazaNcInvestorAccountRepository,
        private readonly accountVerificationService: BazaNcAccountVerificationAccountSetupService,
        private readonly ncAccountsApi: NorthCapitalAccountsApiNestjsService,
    ) {}

    async execute(command: BazaNcInvestorAccountUpdatePersonalInfoCommand): Promise<void> {
        return cqrsCommand<void>(NcInvestorAccountUpdatePersonalInfoCommandHandler.name, async () => {
            const request = command.request;

            const investorAccount = await this.repository.getInvestorAccountById(request.id);
            const isValidUpdatePayload = await this.validateUpdateNcAccounRequest(command, investorAccount);

            if (!investorAccount.northCapitalPartyId || !investorAccount.northCapitalAccountId) {
                throw new BazaNcNoInvestorAccountAvailableForAccountException();
            }

            if (!isValidUpdatePayload) {
                throw new BazaNcInvestorPartyUpdateInvalidRequestException();
            }

            if (request.dateOfBirth) {
                const parsed = moment(request.dateOfBirth, 'MM-DD-YYYY', true);

                if (!parsed.isValid()) {
                    throw new BazaNcInvestorDobIsInvalidException();
                }

                if (parsed.year() < 1900) {
                    throw new BazaNcInvestorDobYearIsInvalidException();
                }

                if (moment(new Date()).diff(parsed, 'years') < 18) {
                    throw new BazaNcInvestorDobShouldBeAbove18Exception();
                }
            }

            const response = await this.accountVerificationService.updateNCAccount(investorAccount.user, request);

            const setupResponse: SetupResponse = {
                isSavedToNC: true,
                wasCreated: false,
                kycStatus: response.partyDetails.KYCstatus,
                accountId: response.accountDetails.accountId,
                partyId: response.partyDetails.partyId,
                shouldRequestDocuments: false,
                shouldRequestSSNDocument: false,
            };

            await this.accountVerificationService.updateDocumentsAndKyc(investorAccount.user, request, response.diffRequest, setupResponse);
        });
    }

    private async validateUpdateNcAccounRequest(
        command: BazaNcInvestorAccountUpdatePersonalInfoCommand,
        investorAccount: BazaNcInvestorAccountEntity,
    ): Promise<boolean> {
        const {
            accountDetails: { kycStatus },
        } = await this.ncAccountsApi.getAccount({
            accountId: investorAccount.northCapitalAccountId,
        });

        const dwollaAccountAvailable = !!investorAccount.dwollaCustomerId;
        const dwollaAccountIsVerified = investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.Verified;

        const isNcKyApproved = (status): boolean =>
            [KYCStatus.AutoApproved, KYCStatus.Approved, KYCStatus.ManuallyApproved].includes(status);

        const validators: Array<boolean> = [
            !isNcKyApproved(kycStatus) && !dwollaAccountAvailable,
            isNcKyApproved(kycStatus) && !dwollaAccountAvailable && !command.request.ssn,
            !isNcKyApproved(kycStatus) && dwollaAccountAvailable && !dwollaAccountIsVerified,
            isNcKyApproved(kycStatus) && dwollaAccountAvailable && !dwollaAccountIsVerified && !command.request.ssn,
            !isNcKyApproved(kycStatus) && dwollaAccountAvailable && dwollaAccountIsVerified && !command.request.ssn,
            isNcKyApproved(kycStatus) && dwollaAccountAvailable && dwollaAccountIsVerified && !command.request.ssn,
        ];

        return validators.some((value) => value === true);
    }
}
