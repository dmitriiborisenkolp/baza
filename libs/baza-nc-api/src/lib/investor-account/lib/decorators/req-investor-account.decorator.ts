import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';

/**
 * ReqInvestorAccount Investor Account Decorator Can use with both `@UseGuards(InvestorAccountGuard)` and `@UseGuards(InvestorAccountOptionalGuard)`.
 * both user and investorAccount object will inject to `request` in both of these guards and `ReqInvestorAccount` will return the `request.investorAccount`.
 * Because `InvestorAccountGuard` and `InvestorAccountOptionalGuard` are using `investorAccountService.getInvestorAccountByUserId`.
 * So it is possible to return BazaNcInvestorAccountEntity (investorAccount) or undefined.
 */
export const ReqInvestorAccount = createParamDecorator((data: unknown, ctx: ExecutionContext): BazaNcInvestorAccountEntity | undefined => {
    const request = ctx.switchToHttp().getRequest();
    return request.investorAccount;
});
