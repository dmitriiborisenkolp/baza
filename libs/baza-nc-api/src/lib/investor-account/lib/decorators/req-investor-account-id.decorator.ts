import { createParamDecorator, ExecutionContext } from '@nestjs/common';

/**
 * ReqInvestorAccountId Decorator Can use with both `@UseGuards(InvestorAccountGuard)` and `@UseGuards(InvestorAccountOptionalGuard)`.
 * both user and investorAccount object will inject to `request` in both of these guards and `ReqInvestorAccountId` will return the `request.investorAccount.id`.
 * Because `InvestorAccountGuard` and `InvestorAccountOptionalGuard` are using `authSession.getAccount()` and `authSession.getAccountOptional()`.
 * So it is possible to return investorAccountId<number> or undefined.
 */
export const ReqInvestorAccountId = createParamDecorator((data: unknown, ctx: ExecutionContext): number | undefined => {
    const request = ctx.switchToHttp().getRequest();

    if (request && request.investorAccount && request.investorAccount.id) {
        return request.investorAccount.id;
    }
    return undefined;
});
