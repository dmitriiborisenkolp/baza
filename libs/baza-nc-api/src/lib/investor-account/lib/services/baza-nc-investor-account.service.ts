import { Injectable } from '@nestjs/common';
import { AccountEntity, BazaAccountRepository, BazaLogger } from '@scaliolabs/baza-core-api';
import { CrudListRequestDto, CrudListResponseDto, postgresLikeEscape } from '@scaliolabs/baza-core-shared';
import { CrudCsvService, CrudService } from '@scaliolabs/baza-core-api';
import {
    BazaNcInvestorAccountCsvDto,
    BazaNcInvestorAccountDto,
    NcAddInvestorAccountRequest,
    NcAddInvestorAccountResponse,
    NcExportToCsvInvestorAccountRequest,
    BazaNcInvestorAccountSyncCommand,
    NcInvestorAccountSyncCommandResponse,
    BazaNcInvestorAccountSyncRemoveCommand,
    PartyDetails,
    BazaNcPurchaseFlowTransactionType,
    BazaNcInvestorAccountUpdatePersonalInformationRequest,
    BazaNcInvestorAccountUpdatePersonalInfoCommand,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountMapper } from '../mappers/baza-nc-investor-account.mapper';
import { BazaNcInvestorAccountCsvMapper } from '../mappers/baza-nc-investor-account-csv.mapper';
import {
    INVESTOR_ACCOUNT_RELATIONS,
    BazaNcInvestorAccountEntity,
    BazaNcInvestorAccountRepository,
    BazaNcInvestorAccountViewEntity,
} from '../../../typeorm';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { ILike } from 'typeorm';
import { CommandBus } from '@nestjs/cqrs';
import { NorthCapitalPartiesApiNestjsService } from '../../../transact-api';
import { BazaNcNoInvestorAccountAvailableForAccountException } from '../exceptions/baza-nc-no-investor-account-available-for-account.exception';

/**
 * Investor Account Service
 */
@Injectable()
export class BazaNcInvestorAccountService {
    constructor(
        private readonly commandBus: CommandBus,
        private readonly crud: CrudService,
        private readonly crudCsvService: CrudCsvService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly mapper: BazaNcInvestorAccountMapper,
        private readonly csvMapper: BazaNcInvestorAccountCsvMapper,
        private readonly ncPartiesApi: NorthCapitalPartiesApiNestjsService,
        private readonly logger: BazaLogger,
    ) {}

    /**
     * Creates Investor Account for Baza Account
     * @param account
     */
    async createInvestorAccountFor(account: AccountEntity, correlationId?: string): Promise<BazaNcInvestorAccountEntity> {
        this.logger.info(`[BazaNcInvestorAccountService.createInvestorAccountFor]`, {
            user: account.id,
            correlationID: correlationId,
        });

        const investorAccount = new BazaNcInvestorAccountEntity();

        investorAccount.user = account;

        await this.investorAccountRepository.save(investorAccount);

        return investorAccount;
    }

    /**
     * [DEPRECATED] Create Investor Accounts for Baza Account or Returns existing Investor Account
     * Please do not use it anymore in your services or mappers. You should not create empty Investor Accounts anymore
     * with new changes.
     * If you need to display Investor Account DTO, use `BazaNcInvestorAccountMapper.virtualInvestorAccountDto` which
     * will generate Virtual (Mock) Investor Account DTO
     * @see BazaNcInvestorAccountMapper.virtualInvestorAccountDto
     * @see BazaNcInvestorAccountMapper.upsertInvestorAccountFor
     * @param account
     * @deprecated
     */
    async touchInvestorAccountFor(account: AccountEntity, correlationId?: string): Promise<BazaNcInvestorAccountEntity> {
        return this.upsertInvestorAccountFor(account, correlationId);
    }

    /**
     * Create Investor Accounts for Baza Account or Returns existing Investor Account
     * Previously it was `touchInvestorAccountFor`, but due of changes we're marking `touchInvestorAccountFor` as deprecated
     * @param account
     */
    async upsertInvestorAccountFor(account: AccountEntity, correlationId?: string): Promise<BazaNcInvestorAccountEntity> {
        this.logger.info(`[BazaNcInvestorAccountService.touchInvestorAccountFor]`, {
            userId: account.id,
            correlationId,
        });

        const existingEntity = await this.investorAccountRepository.findInvestorAccountByUserId(account.id);

        if (existingEntity) {
            return existingEntity;
        } else {
            return this.createInvestorAccountFor(account);
        }
    }

    /**
     * Returns Investor Account by Id
     * @param id
     */
    async getInvestorAccountById(id: number): Promise<BazaNcInvestorAccountEntity> {
        return this.investorAccountRepository.getInvestorAccountById(id);
    }

    /**
     * Returns Investor Account by Baza Account Id
     * @param userId
     */
    async getInvestorAccountByUserId(userId: number): Promise<BazaNcInvestorAccountEntity> {
        return this.investorAccountRepository.getInvestorAccountByUserId(userId);
    }

    /**
     * Returns Investor Account by Baza AccountId,
     * @param userId
     */
    async findInvestorAccountByUserId(userId: number): Promise<BazaNcInvestorAccountEntity> {
        return this.investorAccountRepository.findInvestorAccountByUserId(userId);
    }

    /**
     * Returns true if there is an Investor Account avaialble for given Baza Account Id
     * @param userId
     */
    async isUserHasInvestorAccount(userId: number): Promise<boolean> {
        return !!(await this.investorAccountRepository.findInvestorAccountByUserId(userId));
    }

    /**
     *
     * @param id
     * @returns PartyDetails
     */
    async getInvestorAccountPartyDetails(id: number): Promise<PartyDetails> {
        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(id);

        if (!investorAccount.northCapitalPartyId || !investorAccount.northCapitalAccountId) {
            throw new BazaNcNoInvestorAccountAvailableForAccountException();
        }

        const ncParties = await this.ncPartiesApi.getParty({
            partyId: investorAccount.northCapitalPartyId,
        });

        return ncParties.partyDetails[0];
    }

    /**
     * Update Investor Account Party Details
     * @param request
     * @return void
     */
    async updateInvestorAccountPartyDetails(request: BazaNcInvestorAccountUpdatePersonalInformationRequest): Promise<void> {
        await this.commandBus.execute(new BazaNcInvestorAccountUpdatePersonalInfoCommand(request));
    }

    /**
     * Link NC Account Id and NC Party ID to Investor Account
     * @param request
     */
    async linkInvestorAccount(request: NcAddInvestorAccountRequest): Promise<NcAddInvestorAccountResponse> {
        const bazaAccount = await this.accountRepository.getActiveAccountWithId(request.accountId); // Do not move this call - we should check if user exists before removing existing investor accounts

        const existsByBazaAccount = await this.investorAccountRepository.findInvestorAccountByUserId(request.accountId);
        const existsByNcAccountId = await this.investorAccountRepository.findInvestorAccountByNcAccountId(request.ncAccountId);
        const existsByNcPartyId = await this.investorAccountRepository.findInvestorAccountByNcPartyId(request.ncPartyId);

        const existing = existsByBazaAccount || existsByNcAccountId || existsByNcPartyId;

        if (existing) {
            if (existsByBazaAccount) {
                await this.investorAccountRepository.remove(existsByBazaAccount);
            }

            if (existsByNcAccountId) {
                await this.investorAccountRepository.remove(existsByNcAccountId);
            }

            if (existsByNcPartyId) {
                await this.investorAccountRepository.remove(existsByNcPartyId);
            }
        }

        const newInvestorAccount = await this.createInvestorAccountFor(bazaAccount);

        newInvestorAccount.northCapitalAccountId = request.ncAccountId;
        newInvestorAccount.northCapitalPartyId = request.ncPartyId;

        await this.investorAccountRepository.save(newInvestorAccount);

        if (request.sync) {
            const syncStatus: NcInvestorAccountSyncCommandResponse = await this.commandBus.execute(
                new BazaNcInvestorAccountSyncCommand({
                    investorAccountId: newInvestorAccount.id,
                }),
            );

            return {
                investorAccount: await this.mapper.entityToDTO(newInvestorAccount),
                syncStatus,
            };
        } else {
            return {
                investorAccount: await this.mapper.entityToDTO(newInvestorAccount),
            };
        }
    }

    /**
     * Removes Investor Account
     * @param investorAccount
     */
    async removeInvestorAccount(investorAccount: BazaNcInvestorAccountEntity): Promise<void> {
        await this.commandBus.execute(
            new BazaNcInvestorAccountSyncRemoveCommand({
                investorAccountId: investorAccount.id,
            }),
        );

        await this.investorAccountRepository.remove(investorAccount);
    }

    /**
     * Sets default payment method for Investor
     * @param investorAccount
     * @param paymentMethod
     */
    async setDefaultPaymentMethod(
        investorAccount: BazaNcInvestorAccountEntity,
        paymentMethod: BazaNcPurchaseFlowTransactionType,
    ): Promise<void> {
        investorAccount.defaultPaymentMethod = paymentMethod;

        await this.investorAccountRepository.save(investorAccount);
    }

    /**
     * Returns CRUD List of Investor Accounts
     * @param request
     */
    async listInvestorAccounts(
        request: CrudListRequestDto<BazaNcInvestorAccountDto>,
    ): Promise<CrudListResponseDto<BazaNcInvestorAccountDto>> {
        const findOptions: FindManyOptions<BazaNcInvestorAccountViewEntity> = await this.crud.findOptions(
            {
                order: {
                    id: 'DESC',
                },
            } as FindManyOptions<BazaNcInvestorAccountViewEntity>,
            request,
        );

        if (request.queryString) {
            findOptions.where = [
                {
                    northCapitalAccountId: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                },
                {
                    northCapitalPartyId: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                },
                {
                    fullName: ILike(`%${postgresLikeEscape(request.queryString)}%`),
                },
            ];
        }

        return this.crud.find<BazaNcInvestorAccountEntity, BazaNcInvestorAccountDto>({
            request,
            findOptions: findOptions as any,
            entity: BazaNcInvestorAccountViewEntity,
            mapper: (items) => Promise.resolve(this.mapper.entitiesToDTOs(items)),
            postprocess: async (items) => {
                for (const item of items) {
                    item.user = await this.accountRepository.findActiveAccountWithId(
                        (item as any as BazaNcInvestorAccountViewEntity).userId,
                    );
                }
            },
        });
    }

    /**
     * Exports Investor Accounts to CSV
     * @param request
     */
    async exportInvestorAccountsToCsv(request: NcExportToCsvInvestorAccountRequest): Promise<string> {
        return this.crudCsvService.exportToCsv<BazaNcInvestorAccountEntity, BazaNcInvestorAccountCsvDto>({
            entity: BazaNcInvestorAccountEntity,
            mapper: async (items) => this.csvMapper.entitiesToCSVs(items),
            fields: request.fields,
            request: request.listRequest,
            delimiter: request.delimiter,
            findOptions: {
                order: {
                    id: 'DESC',
                },
                relations: INVESTOR_ACCOUNT_RELATIONS,
            },
        });
    }
}
