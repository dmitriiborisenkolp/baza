import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import {
    BazaNcInvestorBankAccountLinkedEvent,
    BazaNcInvestorBankAccountUnlinkedEvent,
    BazaNcWebhookEvent,
    NorthCapitalTopic,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcWebhookEvent)
export class BankAccountEventHandler implements IEventHandler<BazaNcWebhookEvent> {
    constructor(private readonly eventBus: EventBus, private readonly investorAccountRepository: BazaNcInvestorAccountRepository) {}

    handle(event: BazaNcWebhookEvent): void {
        cqrs(BankAccountEventHandler.name, async () => {
            const message = event.message;

            switch (message.topic) {
                case NorthCapitalTopic.updateExternalAccount: {
                    const isBankAccountLinked = !!message.responseBody && !!message.responseBody.ExtAccountnumber;

                    if (isBankAccountLinked) {
                        const investorAccount = await this.investorAccountRepository.findInvestorAccountByNcAccountId(
                            message.responseBody.accountId,
                        );

                        if (investorAccount) {
                            investorAccount.isBankAccountLinked = true;

                            await this.investorAccountRepository.save(investorAccount);

                            this.eventBus.publish(
                                new BazaNcInvestorBankAccountLinkedEvent({
                                    bazaAccountId: investorAccount.user.id,
                                    bazaNcInvestorAccountId: investorAccount.id,
                                    ncAccountId: investorAccount.northCapitalAccountId,
                                }),
                            );
                        }
                    }

                    break;
                }

                case NorthCapitalTopic.linkExternalAccount:
                case NorthCapitalTopic.updateLinkExternalAccount: {
                    const isBankAccountLinked = !!message.responseBody && !!message.responseBody.account_number;

                    if (isBankAccountLinked) {
                        const investorAccount = await this.investorAccountRepository.findInvestorAccountByNcAccountId(
                            message.responseBody.accountId,
                        );

                        if (investorAccount) {
                            investorAccount.isBankAccountLinked = true;

                            await this.investorAccountRepository.save(investorAccount);

                            this.eventBus.publish(
                                new BazaNcInvestorBankAccountLinkedEvent({
                                    bazaAccountId: investorAccount.user.id,
                                    bazaNcInvestorAccountId: investorAccount.id,
                                    ncAccountId: investorAccount.northCapitalAccountId,
                                }),
                            );
                        }
                    }

                    break;
                }

                case NorthCapitalTopic.deleteExternalAccount: {
                    const investorAccount = await this.investorAccountRepository.findInvestorAccountByNcAccountId(
                        message.responseBody.accountId,
                    );

                    if (investorAccount && investorAccount.user) {
                        investorAccount.isBankAccountLinked = false;

                        await this.investorAccountRepository.save(investorAccount);

                        this.eventBus.publish(
                            new BazaNcInvestorBankAccountUnlinkedEvent({
                                bazaAccountId: investorAccount.user.id,
                                bazaNcInvestorAccountId: investorAccount.id,
                                ncAccountId: investorAccount.northCapitalAccountId,
                            }),
                        );
                    }

                    break;
                }
            }
        });
    }
}
