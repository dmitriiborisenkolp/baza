import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaLogger, cqrs } from '@scaliolabs/baza-core-api';
import { BazaKafkaDwollaWebhookEvent, DwollaDocumentsApiNestjsService } from '@scaliolabs/baza-dwolla-api';
import { DwollaCustomerId, DwollaCustomerStatus, DwollaDocument, DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcInvestorAccountEntity, BazaNcInvestorAccountRepository } from '../../../typeorm';

/**
 * Event handler for Dwolla customer verification status change
 */
@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class DwollaCustomerVerificationEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly dwollaDocumentsApi: DwollaDocumentsApiNestjsService,
        private readonly logger: BazaLogger,
    ) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(DwollaCustomerVerificationEventHandler.name, async () => {
            if (
                [
                    DwollaEventTopic.customer_verified,
                    DwollaEventTopic.customer_reverification_needed,
                    DwollaEventTopic.customer_verification_document_needed,
                    DwollaEventTopic.customer_verification_document_approved,
                    DwollaEventTopic.customer_verification_document_failed,
                    DwollaEventTopic.customer_suspended,
                ].includes(event.payload.responseBody.topic)
            ) {
                const customerId = event.payload.responseBody.resourceId;
                const investorAccount = await this.investorAccountRepository.findInvestorAccountByDwollaId(customerId);

                if (!investorAccount) {
                    this.logger.error(
                        `[DwollaCustomerVerificationEventHandler] Investor Account was not found for customer id ${customerId}`,
                    );

                    return;
                }

                switch (event.payload.responseBody.topic) {
                    case DwollaEventTopic.customer_verified:
                        await this.handleCustomerVerifiedEvent(investorAccount, event);
                        break;

                    case DwollaEventTopic.customer_reverification_needed:
                        await this.handleCustomerRetryEvent(investorAccount, event);
                        break;

                    case DwollaEventTopic.customer_verification_document_needed:
                        await this.handleCustomerDocumentNeeded(investorAccount, event);
                        break;

                    case DwollaEventTopic.customer_verification_document_approved:
                        await this.handleCustomerDocumentApproved(investorAccount, event);
                        break;

                    case DwollaEventTopic.customer_verification_document_failed:
                        await this.handleCustomerDocumentFailed(investorAccount, event);
                        break;

                    case DwollaEventTopic.customer_suspended:
                        await this.handleCustomerVerificationSuspended(investorAccount, event);
                        break;
                }
            }
        });
    }

    private async handleCustomerVerifiedEvent(investorAccount: BazaNcInvestorAccountEntity, event: BazaKafkaDwollaWebhookEvent) {
        investorAccount.dwollaCustomerVerificationStatus = DwollaCustomerStatus.Verified;
        investorAccount.dwollaCustomerError = null;
        investorAccount.dwollaCustomerVerificationFailureReasons = [];
        await this.investorAccountRepository.save(investorAccount);
    }

    private async handleCustomerRetryEvent(investorAccount: BazaNcInvestorAccountEntity, event: BazaKafkaDwollaWebhookEvent) {
        investorAccount.dwollaCustomerVerificationStatus = DwollaCustomerStatus.Retry;
        await this.investorAccountRepository.save(investorAccount);
    }

    private async handleCustomerDocumentNeeded(investorAccount: BazaNcInvestorAccountEntity, event: BazaKafkaDwollaWebhookEvent) {
        investorAccount.dwollaCustomerVerificationStatus = DwollaCustomerStatus.Document;
        await this.investorAccountRepository.save(investorAccount);
    }

    private async handleCustomerDocumentApproved(investorAccount: BazaNcInvestorAccountEntity, event: BazaKafkaDwollaWebhookEvent) {
        investorAccount.dwollaCustomerVerificationStatus = DwollaCustomerStatus.Verified;
        investorAccount.dwollaCustomerError = null;
        investorAccount.dwollaCustomerVerificationFailureReasons = [];
        await this.investorAccountRepository.save(investorAccount);
    }

    private async handleCustomerDocumentFailed(investorAccount: BazaNcInvestorAccountEntity, event: BazaKafkaDwollaWebhookEvent) {
        investorAccount.dwollaCustomerVerificationStatus = DwollaCustomerStatus.Document;
        const lastUploadedDocument = await this.getCustomerLastUploadedDocumentDetails(investorAccount.dwollaCustomerId);

        if (lastUploadedDocument) {
            investorAccount.dwollaCustomerVerificationFailureReasons = lastUploadedDocument.allFailureReasons;
            investorAccount.dwollaCustomerError = lastUploadedDocument.failureReason;
        }

        await this.investorAccountRepository.save(investorAccount);
    }

    private async handleCustomerVerificationSuspended(investorAccount: BazaNcInvestorAccountEntity, event: BazaKafkaDwollaWebhookEvent) {
        investorAccount.dwollaCustomerVerificationStatus = DwollaCustomerStatus.Suspended;
        await this.investorAccountRepository.save(investorAccount);
    }

    private async getCustomerLastUploadedDocumentDetails(dwollaCustomerId: DwollaCustomerId): Promise<DwollaDocument> {
        const response = await this.dwollaDocumentsApi.getDocumentListForCustomer(dwollaCustomerId);
        const documentList = response._embedded.documents;

        return documentList[0];
    }
}
