import { Body, Controller, Post, UseGuards, UsePipes } from '@nestjs/common';
import { BazaNcInvestorAccountService } from '../services/baza-nc-investor-account.service';
import { BazaNcInvestorAccountMapper } from '../mappers/baza-nc-investor-account.mapper';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcInvestorAccountCmsEndpoint,
    BazaNcInvestorAccountCmsEndpointPaths,
    BazaNcInvestorAccountDto,
    NcListInvestorAccountRequest,
    NcListInvestorAccountResponse,
    NcGetInvestorAccountByUserIdRequest,
    NcGetInvestorAccountByUserIdResponse,
    NcGetInvestorAccountRequest,
    NcGetInvestorAccountResponse,
    NcRemoveInvestorAccountRequest,
    NcExportToCsvInvestorAccountRequest,
    NcAddInvestorAccountRequest,
    NcAddInvestorAccountResponse,
    NcGetInvestorAccountPersonalInfoResponse,
    NcGetInvestorAccountPersonalInfoRequest,
    BazaNcInvestorAccountUpdatePersonalInformationRequest,
} from '@scaliolabs/baza-nc-shared';
import { BazaNorthCapitalAcl, BazaNorthCapitalCMSOpenApi } from '@scaliolabs/baza-nc-shared';
import { WithAccessGuard } from '@scaliolabs/baza-core-api';
import { AclNodes } from '@scaliolabs/baza-core-api';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { BazaNcInvestorAccountPartyDetailsMapper } from '../mappers/baza-nc-investory-party-details.mapper';
import { ZipCodeValidationForUsCitizenPipe } from '../pipes/zipcode-validatoin-for-us-addresses.pipe';
import { SsnNumberValidationPipe } from '../pipes/ssn-number-validation.pipe';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalInvestorAccountCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcInvestorAccount])
export class BazaNcInvestorAccountCmsController implements BazaNcInvestorAccountCmsEndpoint {
    constructor(
        private readonly service: BazaNcInvestorAccountService,
        private readonly mapper: BazaNcInvestorAccountMapper,
        private readonly partyDetailsMapper: BazaNcInvestorAccountPartyDetailsMapper,
        private readonly repository: BazaNcInvestorAccountRepository,
    ) {}

    @Post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccount)
    @ApiOperation({
        summary: 'getInvestorAccount',
        description: 'Returns investor account details by investor account id',
    })
    @ApiOkResponse({
        type: BazaNcInvestorAccountDto,
    })
    async getInvestorAccount(@Body() request: NcGetInvestorAccountRequest): Promise<NcGetInvestorAccountResponse> {
        const entity = await this.service.getInvestorAccountById(request.id);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccountPersonalInfo)
    @ApiOperation({
        summary: 'getInvestorAccountPersonalInfo',
        description: 'Returns investor account personal info stored in NC party details by investor account id',
    })
    @ApiOkResponse({
        type: BazaNcInvestorAccountDto,
    })
    async getInvestorAccountPersonalInfo(
        @Body() request: NcGetInvestorAccountPersonalInfoRequest,
    ): Promise<NcGetInvestorAccountPersonalInfoResponse> {
        const partyDetails = await this.service.getInvestorAccountPartyDetails(request.id);

        return this.partyDetailsMapper.partyDetailsToDto(partyDetails);
    }

    @Post(BazaNcInvestorAccountCmsEndpointPaths.updateInvestorAccountPersonalInfo)
    @UsePipes(new ZipCodeValidationForUsCitizenPipe(), new SsnNumberValidationPipe())
    @ApiOperation({
        summary: 'updateInvestorAccountPersonalInfo',
        description: 'Updates investor account personal information at NC ',
    })
    @ApiOkResponse()
    async updateInvestorAccountPersonalInfo(@Body() request: BazaNcInvestorAccountUpdatePersonalInformationRequest): Promise<void> {
        await this.service.updateInvestorAccountPartyDetails(request);
    }

    @Post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccountByUserId)
    @ApiOperation({
        summary: 'getInvestorAccountByUserId',
        description: 'Returns investor account details by account (user) id',
    })
    @ApiOkResponse({
        type: BazaNcInvestorAccountDto,
    })
    async getInvestorAccountByUserId(@Body() request: NcGetInvestorAccountByUserIdRequest): Promise<NcGetInvestorAccountByUserIdResponse> {
        const entity = await this.service.getInvestorAccountByUserId(request.userId);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcInvestorAccountCmsEndpointPaths.listInvestorAccounts)
    @ApiOperation({
        summary: 'listInvestorAccounts',
        description: 'Returns list of investor accounts',
    })
    @ApiOkResponse({
        type: NcListInvestorAccountResponse,
    })
    async listInvestorAccounts(@Body() request: NcListInvestorAccountRequest): Promise<NcListInvestorAccountResponse> {
        return this.service.listInvestorAccounts(request);
    }

    @Post(BazaNcInvestorAccountCmsEndpointPaths.addInvestorAccount)
    @ApiOperation({
        summary: 'addInvestorAccount',
        description:
            'Add (Link) Baza Account with existing Investor Account. If Investor Account is already linked, previous link will be deleted.',
    })
    @ApiOkResponse({
        type: BazaNcInvestorAccountDto,
    })
    async addInvestorAccount(@Body() request: NcAddInvestorAccountRequest): Promise<NcAddInvestorAccountResponse> {
        return this.service.linkInvestorAccount(request);
    }

    @Post(BazaNcInvestorAccountCmsEndpointPaths.removeInvestorAccount)
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaNorthCapitalAcl.BazaNcInvestorAccountManagement])
    @ApiOperation({
        summary: 'removeInvestorAccount',
        description: 'Remove investor account and related transactions',
    })
    @ApiOkResponse()
    async removeInvestorAccount(@Body() request: NcRemoveInvestorAccountRequest): Promise<void> {
        const investorAccount = await this.repository.findInvestorAccountById(request.investorAccountId);

        if (investorAccount) {
            await this.service.removeInvestorAccount(investorAccount);
        }
    }

    @Post(BazaNcInvestorAccountCmsEndpointPaths.exportInvestorAccountsToCsv)
    @ApiOperation({
        summary: 'exportInvestorAccountsToCsv',
        description: 'Export investor account sto CSV',
    })
    @ApiOkResponse()
    async exportInvestorAccountsToCsv(@Body() request: NcExportToCsvInvestorAccountRequest): Promise<string> {
        return this.service.exportInvestorAccountsToCsv(request);
    }
}
