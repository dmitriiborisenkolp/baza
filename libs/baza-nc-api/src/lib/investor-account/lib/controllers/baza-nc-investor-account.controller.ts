import { Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AccountEntity, AuthGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import {
    BazaNcInvestorAccountEndpoint,
    BazaNcInvestorAccountEndpointPaths,
    BazaNcInvestorAccountDto,
    BazaNcInvestorAccountStatusDto,
    BazaNorthCapitalOpenApi,
    BazaNcPurchaseFlowTransactionType,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountService } from '../services/baza-nc-investor-account.service';
import { BazaNcInvestorAccountMapper } from '../mappers/baza-nc-investor-account.mapper';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalInvestorAccount)
@UseGuards(AuthGuard)
export class BazaNcInvestorAccountController implements BazaNcInvestorAccountEndpoint {
    constructor(private readonly service: BazaNcInvestorAccountService, private readonly mapper: BazaNcInvestorAccountMapper) {}

    @Get(BazaNcInvestorAccountEndpointPaths.current)
    @ApiOperation({
        summary: 'current',
        description: 'Returns investor account details',
    })
    @ApiOkResponse({
        type: BazaNcInvestorAccountDto,
    })
    async current(@ReqAccount() account: AccountEntity): Promise<BazaNcInvestorAccountDto> {
        const investorAccount = await this.service.findInvestorAccountByUserId(account.id);

        return investorAccount ? this.mapper.entityToDTO(investorAccount) : this.mapper.virtualInvestorAccountDto(account);
    }

    @Get(BazaNcInvestorAccountEndpointPaths.status)
    @ApiOperation({
        summary: 'status',
        description: 'Returns investor account verification statuses and whether is a foreign investor or not',
    })
    @ApiOkResponse({
        type: BazaNcInvestorAccountStatusDto,
    })
    async status(@ReqAccount() account: AccountEntity): Promise<BazaNcInvestorAccountStatusDto> {
        const investorAccount = await this.service.findInvestorAccountByUserId(account.id);

        return investorAccount ? this.mapper.entityToDTO(investorAccount).status : this.mapper.virtualInvestorAccountDto(account).status;
    }

    @Post(BazaNcInvestorAccountEndpointPaths.setDefaultPaymentMethod)
    @ApiOperation({
        summary: 'setDefaultPaymentMethod',
        description: 'Sets default payment method of Investor',
    })
    @ApiOkResponse()
    async setDefaultPaymentMethod(
        @Param('paymentMethod') paymentMethod: BazaNcPurchaseFlowTransactionType,
        @ReqAccount() account: AccountEntity,
    ): Promise<void> {
        const investorAccount = await this.service.upsertInvestorAccountFor(account);

        await this.service.setDefaultPaymentMethod(investorAccount, paymentMethod);
    }
}
