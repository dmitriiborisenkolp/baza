import { Injectable, PipeTransform } from '@nestjs/common';
import { isValidSSN } from '@scaliolabs/baza-core-shared';
import { BazaNcInvestorAccountUpdatePersonalInformationRequest } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvalidSSNException } from '../exceptions/baza-nc-invalid-ssn.exception';

@Injectable()
export class SsnNumberValidationPipe implements PipeTransform {
    async transform(value: BazaNcInvestorAccountUpdatePersonalInformationRequest) {
        if (value) {
            const { ssn } = value;
            if (ssn && !isValidSSN(ssn)) {
                throw new BazaNcInvalidSSNException();
            }
        }

        return value;
    }
}
