import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcAccountVerificationNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    AccountVerificationStep,
    BazaNcDwollaTouchResult,
    BazaNcKycStatus,
    BazaNcPurchaseFlowTransactionType,
    Domicile,
    KYCStatus,
} from '@scaliolabs/baza-nc-shared';
import { ulid } from 'ulid';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/investor-account/integration-tests/001-baza-nc-investor-account.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessDwolla = new BazaNcDwollaNodeAccess(http);

    const email = `e2e-${ulid()}@scal.io`;
    const password = 'Scalio#14544!';

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will create a new e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email,
            password,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it(`will return disapproved KYC statuses and "isForeign" flag should be true`, async () => {
        const response = await dataAccessInvestorAccount.status();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.nc).toBe(BazaNcKycStatus.Disapproved);
        expect(response.dwolla).toBe(BazaNcKycStatus.Disapproved);
        expect(response.isForeign).toBe(true);
    });

    it('will pass first step in account verification for new user', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it(`will return approved KYC status for NC, disapproved KYC status for Dwolla and "isForeign" flag should be false`, async () => {
        const response = await dataAccessInvestorAccount.status();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.nc).toBe(BazaNcKycStatus.Approved);
        expect(response.dwolla).toBe(BazaNcKycStatus.Disapproved);
        expect(response.isForeign).toBe(false);
    });

    it('will pass second step in account verification for new user', async () => {
        const response = await dataAccessAccountVerification.applyInvestorProfile({
            netWorth: 250,
            currentAnnualHouseholdIncome: 100000,
            isAccreditedInvestor: true,
            isAssociatedWithFINRA: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.kycStatus).toBe(KYCStatus.AutoApproved);
        expect(response.currentStep).toBe(AccountVerificationStep.Completed);
        expect(response.investorProfile.netWorth).toBe(250);
        expect(response.investorProfile.currentAnnualHouseholdIncome).toBe(100000);
        expect(response.investorProfile.isAccreditedInvestor).toBe(true);
        expect(response.investorProfile.isAssociatedWithFINRA).toBe(true);
    });

    it(`will return once again approved KYC status for NC, disapproved KYC status for Dwolla and "isForeign" flag should be false`, async () => {
        const response = await dataAccessInvestorAccount.status();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.nc).toBe(BazaNcKycStatus.Approved);
        expect(response.dwolla).toBe(BazaNcKycStatus.Disapproved);
        expect(response.isForeign).toBe(false);
    });

    it('will try to touch dwolla customer for existing investor', async () => {
        const response = await dataAccessDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect([BazaNcDwollaTouchResult.Created, BazaNcDwollaTouchResult.Exists]).toContain(response.result);
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
        expect(response.failedReason).not.toBeDefined();
    });

    it(`will return approved KYC statuses for NC + Dwolla and "isForeign" flag should be false`, async () => {
        const response = await dataAccessInvestorAccount.status();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.nc).toBe(BazaNcKycStatus.Approved);
        expect(response.dwolla).toBe(BazaNcKycStatus.Approved);
        expect(response.isForeign).toBe(false);
    });

    it('will set default payment method as Credit Card', async () => {
        const response = await dataAccessInvestorAccount.setDefaultPaymentMethod(BazaNcPurchaseFlowTransactionType.CreditCard);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
        expect(investorAccount.defaultPaymentMethod).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);

        const status = await dataAccessInvestorAccount.status();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.defaultPaymentMethod).toBe(BazaNcPurchaseFlowTransactionType.CreditCard);
    });

    it('will set default payment method as Account Balance', async () => {
        const response = await dataAccessInvestorAccount.setDefaultPaymentMethod(BazaNcPurchaseFlowTransactionType.AccountBalance);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(investorAccount)).toBeFalsy();
        expect(investorAccount.defaultPaymentMethod).toBe(BazaNcPurchaseFlowTransactionType.AccountBalance);

        const status = await dataAccessInvestorAccount.status();

        expect(isBazaErrorResponse(status)).toBeFalsy();
        expect(status.defaultPaymentMethod).toBe(BazaNcPurchaseFlowTransactionType.AccountBalance);
    });
});
