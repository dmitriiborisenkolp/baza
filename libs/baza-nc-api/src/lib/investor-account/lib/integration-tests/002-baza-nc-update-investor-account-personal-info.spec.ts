import 'reflect-metadata';
import '@angular/compiler';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcAccountVerificationNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    BazaNcDwollaTouchResult,
    BazaNcInvestorAccountErrorCodes,
    BazaNcInvestorAccountPartyDetailsDto,
    BazaNcKycStatus,
    BazaNorthCapitalAccountVerificationErrorCodes,
    Domicile,
} from '@scaliolabs/baza-nc-shared';
import { ulid } from 'ulid';

jest.setTimeout(540000);

describe('@scaliolabs/baza-nc-api/investor-account/integration-tests/002-baza-nc-update-investor-account-personal-info.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessDwolla = new BazaNcDwollaNodeAccess(http);
    const dataAccessInvestorAccountCms = new BazaNcInvestorAccountCmsNodeAccess(http);

    const email = `e2e-${ulid()}@scal.io`;
    const password = 'Scalio#14544!';

    const email2 = `e2e-${ulid()}@scal.io`;
    const password2 = `Scalio#14544`;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    const authenticateUniqueUser = async (email: string, pass: string) => {
        await http.auth({
            email: email,
            password: pass,
        });
    };

    it('will create a new e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email,
            password,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it(`will return disapproved KYC statuses and "isForeign" flag should be true`, async () => {
        const response = await dataAccessInvestorAccount.status();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.nc).toBe(BazaNcKycStatus.Disapproved);
        expect(response.dwolla).toBe(BazaNcKycStatus.Disapproved);
        expect(response.isForeign).toBe(true);
    });

    it('will pass first step in account verification for new user', async () => {
        await authenticateUniqueUser(email, password);

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it(`will return approved KYC status for NC, disapproved KYC status for Dwolla and "isForeign" flag should be false`, async () => {
        await authenticateUniqueUser(email, password);

        const response = await dataAccessInvestorAccount.status();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.nc).toBe(BazaNcKycStatus.Approved);
        expect(response.dwolla).toBe(BazaNcKycStatus.Disapproved);
        expect(response.isForeign).toBe(false);
    });

    it(`get investor account personal information in the cms`, async () => {
        await authenticateUniqueUser(email, password);

        const currentUserAccount = await dataAccessInvestorAccount.current();

        await http.authE2eAdmin();

        const userPersonalInfo = await dataAccessInvestorAccountCms.getInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
        });

        expect(userPersonalInfo).toEqual(
            expect.objectContaining({
                firstName: 'JOHN',
                lastName: 'SMITH',
                dateOfBirth: '02-28-1975',
                ssn: '112-22-3333',
                residentialStreetAddress1: '222333 PEACHTREE PLACE',
                residentialStreetAddress2: '1414',
                residentialCity: 'ATLANTA',
                residentialState: 'GA',
            } as Partial<BazaNcInvestorAccountPartyDetailsDto>),
        );
    });

    it('will create another e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email: email2,
            password: password2,
            firstName: 'John',
            lastName: 'Doe',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it(`#scenario-1 : should pass updating investor account personal information through cms api with nc kyc not approved (pending) and dwolla not avaiablable`, async () => {
        const updateData = {
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            ssn: '112-22-3333',
        };

        await authenticateUniqueUser(email, password);

        const currentUserAccount = await dataAccessInvestorAccount.current();

        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessInvestorAccountCms.updateInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
            ...updateData,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const userPersonalInfo = await dataAccessInvestorAccountCms.getInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
        });

        expect(userPersonalInfo).toEqual(expect.objectContaining(updateData as Partial<BazaNcInvestorAccountPartyDetailsDto>));
    });

    it(`#scenario-2 : should pass updating investor account personal information without SSN passed through cms api with nc kyc approved and dwolla not avaiablable`, async () => {
        const updateData = {
            firstName: 'JAMES',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1977',
        };

        await authenticateUniqueUser(email, password);

        const currentUserAccount = await dataAccessInvestorAccount.current();

        await http.authE2eAdmin();

        const result = await dataAccessInvestorAccountCms.updateInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
            ...updateData,
        });

        expect(isBazaErrorResponse(result)).toBeFalsy();

        const userPersonalInfo = await dataAccessInvestorAccountCms.getInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
        });

        expect(userPersonalInfo).toEqual(expect.objectContaining(updateData as Partial<BazaNcInvestorAccountPartyDetailsDto>));
    });

    it('will try to touch dwolla customer for existing investor', async () => {
        await authenticateUniqueUser(email, password);

        const response = await dataAccessDwolla.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect([BazaNcDwollaTouchResult.Created, BazaNcDwollaTouchResult.Exists]).toContain(response.result);
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
        expect(response.failedReason).not.toBeDefined();
    });

    it(`#scenario-2 : should fail updating investor account personal information with SSN passed through cms api with nc kyc approved and dwolla not avaiablable `, async () => {
        const updateData = {
            firstName: 'JAMES',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1977',
            ssn: '112-22-3333',
        };

        await authenticateUniqueUser(email, password);

        const currentUserAccount = await dataAccessInvestorAccount.current();

        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessInvestorAccountCms.updateInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
            ...updateData,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcInvestorAccountErrorCodes.NcInvestorAccountUpdatePartyInvalidPayload);
    });

    it(`validation check: invalid US zipcode`, async () => {
        const updateData = {
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            residentialZipCode: 'dummy zipcode',
            residentialCountry: 'US',
        };

        await authenticateUniqueUser(email, password);

        const currentUserAccount = await dataAccessInvestorAccount.current();

        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessInvestorAccountCms.updateInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
            ...updateData,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits);
    });

    it(`validation check: us zipcode should be 5 digits`, async () => {
        const updateData = {
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            residentialZipCode: '1234',
            residentialCountry: 'US',
        };

        await authenticateUniqueUser(email, password);

        const currentUserAccount = await dataAccessInvestorAccount.current();

        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessInvestorAccountCms.updateInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
            ...updateData,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcAccountVerificationZipCodeDoesNotHave5NumericDigits);
    });

    it(`validation check: Ssn number should be at least 9 digits`, async () => {
        const updateData = {
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            residentialZipCode: '12345',
            residentialCountry: 'US',
            ssn: '123',
        };

        await authenticateUniqueUser(email, password);

        const currentUserAccount = await dataAccessInvestorAccount.current();

        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessInvestorAccountCms.updateInvestorAccountPersonalInfo({
            id: currentUserAccount.id,
            ...updateData,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNorthCapitalAccountVerificationErrorCodes.NcInvalidSSNException);
    });
});
