import { Injectable } from '@nestjs/common';
import { BazaNcInvestorAccountMapper } from './baza-nc-investor-account.mapper';
import { BazaNcInvestorAccountCsvDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';

@Injectable()
export class BazaNcInvestorAccountCsvMapper {
    constructor(private readonly baseMapper: BazaNcInvestorAccountMapper) {}

    entityToCSV(input: BazaNcInvestorAccountEntity): BazaNcInvestorAccountCsvDto {
        return this.baseMapper.entityToDTO(input);
    }

    entitiesToCSVs(input: Array<BazaNcInvestorAccountEntity>): Array<BazaNcInvestorAccountCsvDto> {
        return input.map((e) => this.entityToCSV(e));
    }
}
