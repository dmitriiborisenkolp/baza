import { Injectable } from '@nestjs/common';
import { DwollaCustomerStatus } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcInvestorAccountDto, BazaNcKycStatus, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';

/**
 * BazaNcInvestorAccountEntity to BazaNcInvestorAccountDto Mapper
 * Also provides Virtual (Mock) DTO for Investor Account
 */
@Injectable()
export class BazaNcInvestorAccountMapper {
    /**
     * BazaNcInvestorAccountEntity to BazaNcInvestorAccountDto Mapper (Single DTO)
     * @param entity
     */
    entityToDTO(entity: BazaNcInvestorAccountEntity): BazaNcInvestorAccountDto {
        return {
            id: entity.id,
            userId: entity.user?.id,
            userFullName: entity.user?.fullName,
            userEmail: entity.user?.email,
            northCapitalAccountId: entity.northCapitalAccountId,
            northCapitalPartyId: entity.northCapitalPartyId,
            isAccountVerificationInProgress: entity.isAccountVerificationInProgress,
            isAccountVerificationCompleted: entity.isAccountVerificationCompleted,
            isInvestorVerified: entity.isInvestorVerified,
            isAccreditedInvestor: entity.isAccreditedInvestor,
            isBankAccountLinked: entity.isBankAccountLinked,
            isBankAccountNcAchLinked: entity.isBankAccountNcAchLinked,
            isBankAccountCashInLinked: entity.isBankAccountCashInLinked,
            isBankAccountCashOutLinked: entity.isBankAccountCashOutLinked,
            isCreditCardLinked: entity.isCreditCardLinked,
            isPaymentMethodLinked: entity.isPaymentMethodLinked,
            ssnDocumentFileName: entity.ssnDocumentFileName,
            ssnDocumentId: entity.ssnDocumentId,
            isDwollaAvailable: this.isDwollaAvailable(entity),
            dwollaCustomerId: entity.dwollaCustomerId,
            dwollaCustomerError: entity.dwollaCustomerError,
            dwollaCustomerVerificationStatus: entity.dwollaCustomerVerificationStatus,
            dwollaCustomerVerificationFailureReasons: entity.dwollaCustomerVerificationFailureReasons,
            defaultPaymentMethod: entity.defaultPaymentMethod,
            status: {
                nc: entity.isInvestorVerified ? BazaNcKycStatus.Approved : BazaNcKycStatus.Disapproved,
                dwolla: entity.dwollaCustomerId ? BazaNcKycStatus.Approved : BazaNcKycStatus.Disapproved,
                isDwollaAvailable: this.isDwollaAvailable(entity),
                isForeign: entity.isForeignInvestor,
                defaultPaymentMethod: entity.defaultPaymentMethod,
            },
        };
    }

    /**
     * BazaNcInvestorAccountEntity to BazaNcInvestorAccountDto Mapper (Multiple Entities)
     * @param entities
     */
    entitiesToDTOs(entities: Array<BazaNcInvestorAccountEntity>): Array<BazaNcInvestorAccountDto> {
        return entities.map((entity) => this.entityToDTO(entity));
    }

    /**
     * Returns BazaNcInvestorAccountDto Replacement for cases when Investor Account is not available yet
     * @param user
     */
    virtualInvestorAccountDto(user: AccountEntity): BazaNcInvestorAccountDto {
        return {
            id: -1,
            userId: user.id,
            userFullName: user.fullName,
            userEmail: user.email,
            northCapitalAccountId: undefined,
            northCapitalPartyId: undefined,
            isAccountVerificationInProgress: false,
            isAccountVerificationCompleted: false,
            isInvestorVerified: false,
            isAccreditedInvestor: false,
            isBankAccountLinked: false,
            isBankAccountNcAchLinked: false,
            isBankAccountCashInLinked: false,
            isBankAccountCashOutLinked: false,
            isCreditCardLinked: false,
            isPaymentMethodLinked: false,
            ssnDocumentFileName: undefined,
            ssnDocumentId: undefined,
            isDwollaAvailable: false,
            dwollaCustomerId: undefined,
            dwollaCustomerError: undefined,
            dwollaCustomerVerificationStatus: undefined,
            dwollaCustomerVerificationFailureReasons: undefined,
            defaultPaymentMethod: BazaNcPurchaseFlowTransactionType.ACH,
            status: {
                nc: BazaNcKycStatus.Disapproved,
                dwolla: BazaNcKycStatus.Disapproved,
                isDwollaAvailable: false,
                defaultPaymentMethod: BazaNcPurchaseFlowTransactionType.ACH,
                isForeign: true,
            },
        };
    }

    /**
     * Returns true if Dwolla Customer is set and verified for current Investor Account
     * @param investorAccount
     * @private
     */
    private isDwollaAvailable(investorAccount: BazaNcInvestorAccountEntity) {
        return investorAccount.dwollaCustomerId && investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.Verified;
    }
}
