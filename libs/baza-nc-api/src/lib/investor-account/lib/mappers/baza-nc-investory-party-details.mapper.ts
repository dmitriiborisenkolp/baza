import { Injectable } from '@nestjs/common';
import { BazaNcInvestorAccountPartyDetailsDto, PartyDetails } from '@scaliolabs/baza-nc-shared';

@Injectable()
export class BazaNcInvestorAccountPartyDetailsMapper {
    partyDetailsToDto(input: PartyDetails): BazaNcInvestorAccountPartyDetailsDto {
        return {
            firstName: input.firstName,
            lastName: input.lastName,
            ssn: input.socialSecurityNumber,
            dateOfBirth: input.dob,
            residentialStreetAddress1: input.primAddress1,
            residentialStreetAddress2: input.primAddress2,
            residentialCountry: input.primCountry,
            residentialState: input.primState,
            residentialCity: input.primCity,
            residentialZipCode: input.primZip,
        };
    }
}
