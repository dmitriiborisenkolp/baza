import { Global, Module } from '@nestjs/common';
import { BazaNcInvestorAccountMapper } from './mappers/baza-nc-investor-account.mapper';
import { BazaNcInvestorAccountService } from './services/baza-nc-investor-account.service';
import { BazaNcInvestorAccountCmsController } from './controllers/baza-nc-investor-account-cms.controller';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcHasInvestorAccountGuard } from './guards/baza-nc-has-investor-account.guard';
import { BazaNcInvestorAccountCsvMapper } from './mappers/baza-nc-investor-account-csv.mapper';
import { BazaNcInvestorAccountController } from './controllers/baza-nc-investor-account.controller';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { BankAccountEventHandler } from './event-handlers/bank-account.event-handler';
import { DwollaCustomerVerificationEventHandler } from './event-handlers/dwolla-customer-verification.event-handler';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaNcInvestorAccountPartyDetailsMapper } from './mappers/baza-nc-investory-party-details.mapper';
import { NcInvestorAccountUpdatePersonalInfoCommandHandler } from './command-handlers/nc-investor-account-update-personal-info.command-handler';

const eventHandlers = [BankAccountEventHandler, DwollaCustomerVerificationEventHandler];
const commandHandlers = [NcInvestorAccountUpdatePersonalInfoCommandHandler];

@Global()
@Module({
    imports: [CqrsModule, BazaCrudApiModule, BazaNcTypeormApiModule],
    controllers: [BazaNcInvestorAccountController, BazaNcInvestorAccountCmsController],
    providers: [
        BazaNcInvestorAccountMapper,
        BazaNcInvestorAccountService,
        BazaNcHasInvestorAccountGuard,
        BazaNcInvestorAccountCsvMapper,
        BazaNcInvestorAccountPartyDetailsMapper,
        ...eventHandlers,
        ...commandHandlers,
    ],
    exports: [BazaNcInvestorAccountMapper, BazaNcInvestorAccountService, BazaNcHasInvestorAccountGuard],
})
export class BazaNcInvestorAccountApiModule {}
