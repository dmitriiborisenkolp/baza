import { Request } from 'express';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcInvestorAccountEntity } from '../../../typeorm';

export interface RequestWithInvestor extends Request {
    user: AccountEntity;
    investorAccount: BazaNcInvestorAccountEntity;
}
