import 'reflect-metadata';
import {
    bazaNcSetCharset,
    bazaNcSetCustomCharset,
    bazaNcApiConfig,
    NcCharset,
    ncCharsetEn,
    ncCharsetLatin1,
} from '@scaliolabs/baza-nc-shared';
import { bazaNcTransactApiValidateRequest } from './baza-nc-transact-api-validate-request';
import { BazaNcInvalidCharactersException } from '../exceptions/baza-nc-invalid-characters.exception';

interface TestCaseEntity<T> {
    input: T;
    output: T;
}

type TestCases = Array<TestCaseEntity<any>>;

function performTests(testCases: TestCases): void {
    for (const testCase of testCases) {
        const cloned = JSON.parse(JSON.stringify(testCase.input));

        bazaNcTransactApiValidateRequest(cloned);

        expect(cloned).toEqual(testCase.output);
    }
}

describe('@scaliolabs/baza-nc-api/transact-api/pipes/baza-nc-transact-api-validate-request.ts', () => {
    let CURRENT_CONFIG = bazaNcApiConfig();

    beforeAll(() => {
        CURRENT_CONFIG = bazaNcApiConfig();

        bazaNcApiConfig({
            enableAutoTrim: false,
            fixTabSymbolBug: false,
            disableCharsetCheckForFields: [],
            enableCharsetFiltering: false,
        });
    });

    afterAll(() => {
        bazaNcApiConfig(CURRENT_CONFIG);
    });

    it('[enableAutoTrim] [enabled] will automatically trim resources', () => {
        bazaNcApiConfig({
            enableAutoTrim: true,
        });

        performTests([
            {
                input: { a: 0 },
                output: { a: 0 },
            },
            {
                input: { a: 12 },
                output: { a: 12 },
            },
            {
                input: { a: null },
                output: { a: null },
            },
            {
                input: { a: 'foo' },
                output: { a: 'foo' },
            },
            {
                input: { a: ' foo' },
                output: { a: 'foo' },
            },
            {
                input: { a: 'foo ' },
                output: { a: 'foo' },
            },
            {
                input: { a: '  foo  ' },
                output: { a: 'foo' },
            },
        ]);
    });

    it('[enableAutoTrim] [disabled] will not automatically trim resources', () => {
        bazaNcApiConfig({
            enableAutoTrim: false,
        });

        performTests([
            {
                input: { a: 0 },
                output: { a: 0 },
            },
            {
                input: { a: 12 },
                output: { a: 12 },
            },
            {
                input: { a: null },
                output: { a: null },
            },
            {
                input: { a: 'foo' },
                output: { a: 'foo' },
            },
            {
                input: { a: ' foo' },
                output: { a: ' foo' },
            },
            {
                input: { a: 'foo ' },
                output: { a: 'foo ' },
            },
            {
                input: { a: '  foo  ' },
                output: { a: '  foo  ' },
            },
        ]);
    });

    it('[fixTabSymbolBug] [enabled] will automatically removes TAB character', () => {
        bazaNcApiConfig({
            enableAutoTrim: false,
            fixTabSymbolBug: true,
        });

        performTests([
            {
                input: { a: 0 },
                output: { a: 0 },
            },
            {
                input: { a: 12 },
                output: { a: 12 },
            },
            {
                input: { a: null },
                output: { a: null },
            },
            {
                input: { a: 'foo' },
                output: { a: 'foo' },
            },
            {
                input: { a: '\tfoo' },
                output: { a: '  foo' },
            },
            {
                input: { a: 'foo\t' },
                output: { a: 'foo  ' },
            },
            {
                input: { a: '\t\tfoo\t' },
                output: { a: '    foo  ' },
            },
        ]);
    });

    it('[fixTabSymbolBug+enableAutoTrim] [enabled] will automatically removes TAB character & trim string', () => {
        bazaNcApiConfig({
            enableAutoTrim: true,
            fixTabSymbolBug: true,
        });

        performTests([
            {
                input: { a: 0 },
                output: { a: 0 },
            },
            {
                input: { a: 12 },
                output: { a: 12 },
            },
            {
                input: { a: null },
                output: { a: null },
            },
            {
                input: { a: 'foo' },
                output: { a: 'foo' },
            },
            {
                input: { a: '\tfoo' },
                output: { a: 'foo' },
            },
            {
                input: { a: 'foo\t' },
                output: { a: 'foo' },
            },
            {
                input: { a: '\t\tfoo\t' },
                output: { a: 'foo' },
            },
        ]);
    });

    it('[fixTabSymbolBug] [disabled] will not automatically removes TAB character', () => {
        bazaNcApiConfig({
            enableAutoTrim: false,
            fixTabSymbolBug: false,
        });

        performTests([
            {
                input: { a: 0 },
                output: { a: 0 },
            },
            {
                input: { a: 12 },
                output: { a: 12 },
            },
            {
                input: { a: null },
                output: { a: null },
            },
            {
                input: { a: 'foo' },
                output: { a: 'foo' },
            },
            {
                input: { a: '\tfoo' },
                output: { a: '\tfoo' },
            },
            {
                input: { a: 'foo\t' },
                output: { a: 'foo\t' },
            },
            {
                input: { a: '\t\tfoo\t' },
                output: { a: '\t\tfoo\t' },
            },
        ]);
    });

    it('[charset] [enabled] [NcCharset.EN] will throw error on invalid characters', () => {
        bazaNcApiConfig({
            enableCharsetFiltering: true,
        });

        bazaNcSetCharset(NcCharset.En);

        expect(() => {
            bazaNcTransactApiValidateRequest({
                foo: 'Å Æ Ç',
            });
        }).toThrowError(BazaNcInvalidCharactersException);
    });

    it('[charset] [enabled] [NcCharset.EN] will not throw error on valid characters', () => {
        bazaNcApiConfig({
            enableCharsetFiltering: true,
        });

        bazaNcSetCharset(NcCharset.En);

        expect(() => {
            bazaNcTransactApiValidateRequest({
                foo: ncCharsetEn,
            });
        }).not.toThrowError(BazaNcInvalidCharactersException);
    });

    it('[charset] [enabled] [NcCharset.Latin1] will throw error on invalid characters', () => {
        bazaNcApiConfig({
            enableCharsetFiltering: true,
        });

        bazaNcSetCharset(NcCharset.Latin1);

        expect(() => {
            bazaNcTransactApiValidateRequest({
                foo: 'АБВ',
            });
        }).toThrowError(BazaNcInvalidCharactersException);
    });

    it('[charset] [enabled] [NcCharset.Latin1] will not throw error on valid characters', () => {
        bazaNcApiConfig({
            enableCharsetFiltering: true,
        });

        bazaNcSetCharset(NcCharset.Latin1);

        expect(() => {
            bazaNcTransactApiValidateRequest({
                foo: ncCharsetLatin1,
            });
        }).not.toThrowError(BazaNcInvalidCharactersException);
    });

    it('[disableCharsetCheckForFields] will not throw charset error for excluded fields', () => {
        bazaNcApiConfig({
            enableAutoTrim: false,
            fixTabSymbolBug: false,
            enableCharsetFiltering: true,
            disableCharsetCheckForFields: ['foo'],
        });

        bazaNcSetCustomCharset('abc');

        expect(() => {
            bazaNcTransactApiValidateRequest({
                bar: 'baz',
            });
        }).toThrowError(BazaNcInvalidCharactersException);

        expect(() => {
            bazaNcTransactApiValidateRequest({
                foo: 'baz',
            });
        }).not.toThrowError(BazaNcInvalidCharactersException);
    });
});
