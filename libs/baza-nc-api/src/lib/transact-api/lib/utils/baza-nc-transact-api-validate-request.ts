import { bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvalidCharactersException } from '../exceptions/baza-nc-invalid-characters.exception';
import * as FormData from 'form-data';

export function bazaNcTransactApiValidateRequest<T = any>(input: T): void {
    const config = bazaNcApiConfig();

    if (config.fixTabSymbolBug) {
        if (Array.isArray(input)) {
            for (let i = 0; i < input.length; i++) {
                if (typeof input[i] === 'string') {
                    input[i] = input[i].split('\t').join('  ');
                }
            }
        } else if (!!input && typeof input === 'object') {
            for (const key of Object.keys(input)) {
                if (typeof input[key] === 'string') {
                    input[key] = input[key].split('\t').join('  ');
                }
            }
        }
    }

    if (config.enableAutoTrim) {
        if (Array.isArray(input)) {
            for (let i = 0; i < input.length; i++) {
                if (typeof input[i] === 'string') {
                    input[i] = input[i].trim();
                }
            }
        } else if (!!input && typeof input === 'object') {
            for (const key of Object.keys(input)) {
                if (typeof input[key] === 'string') {
                    input[key] = input[key].trim();
                }
            }
        }
    }

    if (config.charset) {
        if (input instanceof Buffer || input instanceof FormData) {
            return;
        } else if (Array.isArray(input)) {
            input.forEach((item) => bazaNcTransactApiValidateRequest(item));
        } else if (!!input && typeof input === 'object') {
            Object.entries(input).forEach(([key, value]) => {
                if (!config.disableCharsetCheckForFields.includes(key)) {
                    bazaNcTransactApiValidateRequest(value);
                }
            });
        } else if (typeof input === 'string') {
            input.split('').forEach((symbol) => {
                if (config.enableCharsetFiltering) {
                    if (!config.charset.includes(symbol)) {
                        throw new BazaNcInvalidCharactersException({
                            failedCharacter: symbol,
                        });
                    }
                }
            });
        }
    }
}
