import { DynamicModule, Global, Module } from '@nestjs/common';
import {
    BazaNcConfigurationNestjsService,
    NorthCapitalConfigurationNestjsServiceConfiguration,
} from './configuration/baza-nc-configuration.nestjs-service';
import { NorthCapitalAccountsApiNestjsService } from './api/north-capital-accounts-api.nestjs-service';
import { NorthCapitalPartiesApiNestjsService } from './api/north-capital-parties-api.nestjs-service';
import { NorthCapitalNestjsService } from './api/north-capital.nestjs-service';
import { NorthCapitalLinksApiNestjsService } from './api/north-capital-links-api.nestjs-service';
import { NorthCapitalEntitiesApiNestjsService } from './api/north-capital-entities-api.nestjs-service';
import { NorthCapitalAccreditationVerificationApiNestjsService } from './api/north-capital-accreditation-verification-api.nestjs-service';
import { NorthCapitalTradesApiNestjsService } from './api/north-capital-trades-api.nestjs-service';
import { NorthCapitalAchTransfersApiNestjsService } from './api/north-capital-ach-transfers-api.nestjs-service';
import { NorthCapitalCreditCardTransactionApiNestjsService } from './api/north-capital-credit-card-transaction-api.nestjs-service';
import { NorthCapitalCustodialAccountNestjsService } from './api/north-capital-custodial-account.nestjs-service';
import { NorthCapitalDocumentsApiNestjsService } from './api/north-capital-documents-api.nestjs-service';
import { NorthCapitalIssuersApiNestjsService } from './api/north-capital-issuers-api.nestjs-service';
import { NorthCapitalKycAmlNestjsService } from './api/north-capital-kyc-aml.nestjs-service';
import { NorthCapitalOfferingsApiNestjsService } from './api/north-capital-offerings-api.nestjs-service';
import { CqrsModule } from '@nestjs/cqrs';
import { HttpModule } from '@nestjs/axios';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<NorthCapitalConfigurationNestjsServiceConfiguration>;
}

const apis = [
    NorthCapitalNestjsService,
    NorthCapitalAccountsApiNestjsService,
    NorthCapitalPartiesApiNestjsService,
    NorthCapitalLinksApiNestjsService,
    NorthCapitalEntitiesApiNestjsService,
    NorthCapitalAccreditationVerificationApiNestjsService,
    NorthCapitalTradesApiNestjsService,
    NorthCapitalAchTransfersApiNestjsService,
    NorthCapitalCreditCardTransactionApiNestjsService,
    NorthCapitalCustodialAccountNestjsService,
    NorthCapitalDocumentsApiNestjsService,
    NorthCapitalIssuersApiNestjsService,
    NorthCapitalKycAmlNestjsService,
    NorthCapitalOfferingsApiNestjsService,
];

export { AsyncOptions as NorthCapitalApiNestjsModuleAsyncOptions };

@Module({})
export class BazaNcTransactApiModule {
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: NorthCapitalApiGlobalNestjsModule,
            providers: [
                {
                    provide: BazaNcConfigurationNestjsService,
                    inject: options.injects,
                    useFactory: async (...injects) => new BazaNcConfigurationNestjsService(await options.useFactory(...injects)),
                },
            ],
            exports: [BazaNcConfigurationNestjsService],
        };
    }
}

@Global()
@Module({
    imports: [HttpModule, CqrsModule],
    providers: [...apis],
    exports: [...apis],
})
class NorthCapitalApiGlobalNestjsModule {}
