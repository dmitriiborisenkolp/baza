export class NcResponseEvent {
    constructor(
        public readonly payload: {
            uri: string;
            request: any;
            requestId: string;
            time: Date;
            response: any;
        },
    ) {}
}