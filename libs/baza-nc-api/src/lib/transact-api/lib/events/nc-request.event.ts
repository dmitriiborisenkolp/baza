export class NcRequestEvent {
    constructor(
        public readonly payload: {
            uri: string;
            request: any;
            requestId: string;
            time: Date;
        },
    ) {}
}