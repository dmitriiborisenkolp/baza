export class NcErrorEvent {
    constructor(
        public readonly payload: {
            uri: string;
            request: any;
            requestId: string;
            time: Date;
            error: any;
        },
    ) {}
}