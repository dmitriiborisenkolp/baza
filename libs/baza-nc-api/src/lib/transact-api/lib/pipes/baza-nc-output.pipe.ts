import { AccountDetails, PartyDetails } from '@scaliolabs/baza-nc-shared';

const isNotNullOrUndefined = (input) => input !== undefined && input !== null;

export enum NcOutputContext {
    GetAccount,
    GetParty,
}

const mapNcInputContextToFields: [
    { context: NcOutputContext.GetAccount; fields: Array<keyof AccountDetails> },
    { context: NcOutputContext.GetParty; fields: Array<keyof PartyDetails> },
] = [
    {
        context: NcOutputContext.GetAccount,
        fields: ['address2'],
    },
    {
        context: NcOutputContext.GetParty,
        fields: [
            'primAddress1',
            'primAddress2',
            'empCity',
            'empCountry',
            'empName',
            'empState',
            'empZip',
            'occupation',
            'empAddress1',
            'empAddress2',
        ],
    },
];

export function bazaNcOutputPipe(input: string): string {
    if (isNotNullOrUndefined(input) && (input || '').toString().trim().length === 0) {
        return '';
    } else {
        return input;
    }
}

export function ncOutput<T>(input: T, context: NcOutputContext): T {
    const newObject: T = {} as T;
    const contextMap = mapNcInputContextToFields.find((c) => c.context === context);

    Object.keys(input).forEach((key) => {
        if (contextMap.fields.includes(key as any)) {
            newObject[key] = bazaNcOutputPipe(input[key]);
        } else {
            newObject[key] = input[key];
        }
    });

    return newObject;
}

export function ncOutputObjectPipe<T>(input: T, context: NcOutputContext): T {
    return ncOutput<T>(input, context);
}
