import { AccountDetails, PartyDetails } from '@scaliolabs/baza-nc-shared';

export enum NcInputContext {
    GetAccount,
    GetParty,
}

const mapNcInputContextToFields: [
    { context: NcInputContext.GetAccount; fields: Array<keyof AccountDetails> },
    { context: NcInputContext.GetParty; fields: Array<keyof PartyDetails> },
] = [
    {
        context: NcInputContext.GetAccount,
        fields: ['address2'],
    },
    {
        context: NcInputContext.GetParty,
        fields: ['primAddress2', 'empAddress1', 'empAddress2', 'empCity', 'empCountry', 'empName', 'empState', 'empZip', 'occupation'],
    },
];

export function bazaNcInputPipe(input: string): string {
    if (input === null || (typeof input === 'string' && (input || '').toString().trim().length === 0)) {
        return ' ';
    } else {
        return input;
    }
}

export function ncInputObjectPipe<T>(input: T, context: NcInputContext): T {
    const newObject: T = {} as T;
    const contextMap = mapNcInputContextToFields.find((c) => c.context === context);

    Object.keys(input).forEach((key) => {
        if (contextMap.fields.includes(key as any)) {
            newObject[key] = bazaNcInputPipe(input[key]);
        } else {
            newObject[key] = input[key];
        }
    });

    return newObject;
}
