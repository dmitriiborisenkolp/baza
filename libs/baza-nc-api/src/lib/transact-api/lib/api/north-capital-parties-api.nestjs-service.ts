import { Injectable } from '@nestjs/common';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import type {
    CreatePartyRequest,
    CreatePartyResponse,
    DeletePartyDocumentRequest,
    DeletePartyDocumentResponse,
    DeletePartyRequest,
    DeletePartyResponse,
    GetPartyDocumentRequest,
    GetPartyDocumentResponse,
    GetPartyRequest,
    GetPartyResponse,
    NorthCapitalPartiesApi,
    SearchPartyRequest,
    SearchPartyResponse,
    UpdatePartyRequest,
    UpdatePartyResponse,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalPartiesApiPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalPartiesApiNestjsService implements NorthCapitalPartiesApi {
    constructor(private readonly api: NorthCapitalNestjsService, private readonly logger: BazaLogger) {}

    async createParty(request: CreatePartyRequest, correlationId?: string): Promise<CreatePartyResponse> {
        this.logger.info(`[NorthCapitalPartiesApiNestjsService.createParty]`, {
            request,
            correlationId,
        });

        return this.api.put<CreatePartyRequest, CreatePartyResponse>(NorthCapitalPartiesApiPaths.CreateParty, request);
    }

    async deleteParty(request: DeletePartyRequest, correlationId?: string): Promise<DeletePartyResponse> {
        this.logger.info(`[NorthCapitalPartiesApiNestjsService.deleteParty]`, {
            request,
            correlationId,
        });

        return this.api.post<DeletePartyRequest, DeletePartyResponse>(NorthCapitalPartiesApiPaths.DeleteParty, request);
    }

    async deletePartyDocument(request: DeletePartyDocumentRequest, correlationId?: string): Promise<DeletePartyDocumentResponse> {
        this.logger.info(`[NorthCapitalPartiesApiNestjsService.deletePartyDocument]`, {
            request,
            correlationId,
        });

        return this.api.post<DeletePartyDocumentRequest, DeletePartyDocumentResponse>(
            NorthCapitalPartiesApiPaths.DeletePartyDocument,
            request,
        );
    }

    async getParty(request: GetPartyRequest, correlationId?: string): Promise<GetPartyResponse> {
        this.logger.info(`[NorthCapitalPartiesApiNestjsService.getParty]`, {
            request,
            correlationId,
        });

        return this.api.post<GetPartyRequest, GetPartyResponse>(NorthCapitalPartiesApiPaths.GetParty, request);
    }

    async getPartyDocument(request: GetPartyDocumentRequest, correlationId?: string): Promise<GetPartyDocumentResponse> {
        this.logger.info(`[NorthCapitalPartiesApiNestjsService.getPartyDocument]`, {
            request,
            correlationId,
        });

        return this.api.post<GetPartyDocumentRequest, GetPartyDocumentResponse>(NorthCapitalPartiesApiPaths.GetPartyDocument, request);
    }

    async searchParty(request: SearchPartyRequest, correlationId?: string): Promise<SearchPartyResponse> {
        this.logger.info(`[NorthCapitalPartiesApiNestjsService.searchParty]`, {
            request,
            correlationId,
        });

        return this.api.post<SearchPartyRequest, SearchPartyResponse>(NorthCapitalPartiesApiPaths.SearchParty, request);
    }

    async updateParty(request: UpdatePartyRequest, correlationId?: string): Promise<UpdatePartyResponse> {
        this.logger.info(`[NorthCapitalPartiesApiNestjsService.updateParty]`, {
            request,
            correlationId,
        });

        return this.api.post<UpdatePartyRequest, UpdatePartyResponse>(NorthCapitalPartiesApiPaths.UpdateParty, request);
    }
}
