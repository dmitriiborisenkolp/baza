import { Injectable } from '@nestjs/common';
import type {
    CreateExternalAccountRequest,
    CreateExternalAccountResponse,
    DeleteExternalAccountRequest,
    DeleteExternalAccountResponse,
    ExternalFundMoveRequest,
    ExternalFundMoveResponse, GetAchPendingIdRequest,
    GetAchPendingIdResponse,
    GetExternalAccountRequest,
    GetExternalAccountResponse,
    GetExternalFundMoveHistoryRequest,
    GetExternalFundMoveHistoryResponse, GetExternalFundMoveInfoRequest,
    GetExternalFundMoveInfoResponse,
    GetExternalFundMoveRequest,
    GetExternalFundMoveResponse, LinkExternalAccountRequest,
    LinkExternalAccountResponse,
    NorthCapitalAchTransfersApi,
    RequestForVoidACHRequest,
    RequestForVoidACHResponse,
    UpdateExternalAccountRequest,
    UpdateExternalAccountResponse,
    UpdateExternalFundMoveStatusRequest, UpdateExternalFundMoveStatusResponse, UpdateLinkExternalAccountRequest, UpdateLinkExternalAccountResponse, ValidateAbaRoutingNumberRequest,
    ValidateAbaRoutingNumberResponse
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalAchTransfersApiPaths } from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalAchTransfersApiNestjsService implements NorthCapitalAchTransfersApi {
    constructor(
        private readonly api: NorthCapitalNestjsService
    ) {
    }

    async createExternalAccount(request: CreateExternalAccountRequest): Promise<CreateExternalAccountResponse> {
        return this.api.post<CreateExternalAccountRequest, CreateExternalAccountResponse>(NorthCapitalAchTransfersApiPaths.CreateExternalAccount, request);
    }

    async deleteExternalAccount(request: DeleteExternalAccountRequest): Promise<DeleteExternalAccountResponse> {
        return this.api.post<DeleteExternalAccountRequest, DeleteExternalAccountResponse>(NorthCapitalAchTransfersApiPaths.DeleteExternalAccount, request);
    }

    async externalFundMove(request: ExternalFundMoveRequest): Promise<ExternalFundMoveResponse> {
        return this.api.post<ExternalFundMoveRequest, ExternalFundMoveResponse>(NorthCapitalAchTransfersApiPaths.ExternalFundMove, request);
    }

    async getAchPendingId(request: GetAchPendingIdRequest): Promise<GetAchPendingIdResponse> {
        return this.api.post<GetAchPendingIdRequest, GetAchPendingIdResponse>(NorthCapitalAchTransfersApiPaths.GetAchPendingId, request);
    }

    async getExternalAccount(request: GetExternalAccountRequest): Promise<GetExternalAccountResponse> {
        return this.api.post<GetExternalAccountRequest, GetExternalAccountResponse>(NorthCapitalAchTransfersApiPaths.GetExternalAccount, request);
    }

    async getExternalFundMove(request: GetExternalFundMoveRequest): Promise<GetExternalFundMoveResponse> {
        return this.api.post<GetExternalFundMoveRequest, GetExternalFundMoveResponse>(NorthCapitalAchTransfersApiPaths.GetExternalFundMove, request);
    }

    async getExternalFundMoveHistory(request: GetExternalFundMoveHistoryRequest): Promise<GetExternalFundMoveHistoryResponse> {
        return this.api.post<GetExternalFundMoveHistoryRequest, GetExternalFundMoveHistoryResponse>(NorthCapitalAchTransfersApiPaths.GetExternalFundMoveHistory, request);
    }

    async getExternalFundMoveInfo(request: GetExternalFundMoveInfoRequest): Promise<GetExternalFundMoveInfoResponse> {
        return this.api.post<GetExternalFundMoveInfoRequest, GetExternalFundMoveInfoResponse>(NorthCapitalAchTransfersApiPaths.GetExternalFundMoveInfo, request);
    }

    async linkExternalAccount(request: LinkExternalAccountRequest): Promise<LinkExternalAccountResponse> {
        return this.api.post<LinkExternalAccountRequest, LinkExternalAccountResponse>(NorthCapitalAchTransfersApiPaths.LinkExternalAccount, request);
    }

    async requestForVoidACH(request: RequestForVoidACHRequest): Promise<RequestForVoidACHResponse> {
        return this.api.post<RequestForVoidACHRequest, RequestForVoidACHResponse>(NorthCapitalAchTransfersApiPaths.RequestForVoidACH, request);
    }

    async updateExternalAccount(request: UpdateExternalAccountRequest): Promise<UpdateExternalAccountResponse> {
        return this.api.post<UpdateExternalAccountRequest, UpdateExternalAccountResponse>(NorthCapitalAchTransfersApiPaths.UpdateExternalAccount, request);
    }

    async validateABARoutingNumber(request: ValidateAbaRoutingNumberRequest): Promise<ValidateAbaRoutingNumberResponse> {
        return this.api.post<ValidateAbaRoutingNumberRequest, ValidateAbaRoutingNumberResponse>(NorthCapitalAchTransfersApiPaths.ValidateABARoutingNumber, request);
    }

    async updateExternalFundMoveStatus(request: UpdateExternalFundMoveStatusRequest): Promise<UpdateExternalFundMoveStatusResponse> {
        return this.api.post<UpdateExternalFundMoveStatusRequest, UpdateExternalFundMoveStatusResponse>(NorthCapitalAchTransfersApiPaths.UpdateExternalFundMoveStatus, request);
    }

    async updateLinkExternalAccount(request: UpdateLinkExternalAccountRequest): Promise<UpdateLinkExternalAccountResponse> {
        return this.api.post<UpdateLinkExternalAccountRequest, UpdateLinkExternalAccountResponse>(NorthCapitalAchTransfersApiPaths.UpdateLinkExternalAccount, request);
    }
}
