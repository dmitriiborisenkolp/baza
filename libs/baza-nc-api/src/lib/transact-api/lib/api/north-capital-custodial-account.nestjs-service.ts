import { Injectable } from '@nestjs/common';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';
import type {
    NorthCapitalCustodialAccountApi,
    RequestCustodialAccountRequest,
    RequestCustodialAccountResponse,
    SendCustodialAccountAgreementRequest,
    SendCustodialAccountAgreementResponse,
    UpdateCustodialAccountRequestRequest,
    UpdateCustodialAccountRequestResponse,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalCustodialAccountApiPaths } from '@scaliolabs/baza-nc-shared';

@Injectable()
export class NorthCapitalCustodialAccountNestjsService implements NorthCapitalCustodialAccountApi {
    constructor(private readonly api: NorthCapitalNestjsService) {}

    async requestCustodialAccount(request: RequestCustodialAccountRequest): Promise<RequestCustodialAccountResponse> {
        return this.api.post<RequestCustodialAccountRequest, RequestCustodialAccountResponse>(
            NorthCapitalCustodialAccountApiPaths.RequestCustodialAccount,
            request,
        );
    }

    async sendCustodialAccountAgreement(request: SendCustodialAccountAgreementRequest): Promise<SendCustodialAccountAgreementResponse> {
        return this.api.post<SendCustodialAccountAgreementRequest, SendCustodialAccountAgreementResponse>(
            NorthCapitalCustodialAccountApiPaths.SendCustodialAccountAgreement,
            request,
        );
    }

    async updateCustodialAccountRequest(request: UpdateCustodialAccountRequestRequest): Promise<UpdateCustodialAccountRequestResponse> {
        return this.api.post<UpdateCustodialAccountRequestRequest, UpdateCustodialAccountRequestResponse>(
            NorthCapitalCustodialAccountApiPaths.UpdateCustodialAccountRequest,
            request,
        );
    }
}
