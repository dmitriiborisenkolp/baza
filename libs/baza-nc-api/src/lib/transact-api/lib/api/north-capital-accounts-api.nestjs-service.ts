import type {
    CalculateSuitabilityRequest,
    CalculateSuitabilityResponse, CreateAccountRequest, CreateAccountResponse,
    DeleteAccountRequest,
    DeleteAccountResponse,
    DeleteSuitabilityRequest, DeleteSuitabilityResponse, GetAccountDocumentRequest,
    GetAccountDocumentResponse,
    GetAccountRequest,
    GetAccountResponse, GetSuitabilityRequest, GetSuitabilityResponse,
    NorthCapitalAccountsApi,
    SearchAccountRequest,
    SearchAccountResponse,
    UpdateAccountArchiveStatusRequest, UpdateAccountArchiveStatusResponse,
    UpdateAccountRequest, UpdateAccountResponse,
    UpdateSuitabilityRequest, UpdateSuitabilityResponse
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalAccountsPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class NorthCapitalAccountsApiNestjsService implements NorthCapitalAccountsApi {
    constructor(
        private readonly api: NorthCapitalNestjsService
    ) {
    }

    async getAccount(request: GetAccountRequest): Promise<GetAccountResponse> {
        return this.api.post<GetAccountRequest, GetAccountResponse>(NorthCapitalAccountsPaths.GetAccount, request);
    }

    async createAccount(request: CreateAccountRequest): Promise<CreateAccountResponse> {
        return this.api.put<CreateAccountRequest, CreateAccountResponse>(NorthCapitalAccountsPaths.CreateAccount, request);
    }

    async calculateSuitability(request: CalculateSuitabilityRequest): Promise<CalculateSuitabilityResponse> {
        return this.api.put<CalculateSuitabilityRequest, CalculateSuitabilityResponse>(NorthCapitalAccountsPaths.CalculateSuitability, request);
    }

    async deleteAccount(request: DeleteAccountRequest): Promise<DeleteAccountResponse> {
        return this.api.post<DeleteAccountRequest, DeleteAccountResponse>(NorthCapitalAccountsPaths.DeleteAccount, request);
    }

    async deleteSuitability(request: DeleteSuitabilityRequest): Promise<DeleteSuitabilityResponse> {
        return this.api.post<DeleteSuitabilityRequest, DeleteSuitabilityResponse>(NorthCapitalAccountsPaths.DeleteSuitability, request);
    }

    async getAccountDocument(request: GetAccountDocumentRequest): Promise<GetAccountDocumentResponse> {
        return this.api.post<GetAccountDocumentRequest, GetAccountDocumentResponse>(NorthCapitalAccountsPaths.GetAccountDocument, request);
    }

    async getSuitability(request: GetSuitabilityRequest): Promise<GetSuitabilityResponse> {
        return this.api.post<GetSuitabilityRequest, GetSuitabilityResponse>(NorthCapitalAccountsPaths.GetSuitability, request);
    }

    async searchAccount(request: SearchAccountRequest): Promise<SearchAccountResponse> {
        return this.api.post<SearchAccountRequest, SearchAccountResponse>(NorthCapitalAccountsPaths.SearchAccount, request);
    }

    async updateAccount(request: UpdateAccountRequest): Promise<UpdateAccountResponse> {
        return this.api.put<UpdateAccountRequest, UpdateAccountResponse>(NorthCapitalAccountsPaths.UpdateAccount, request);
    }

    async updateAccountArchiveStatus(request: UpdateAccountArchiveStatusRequest): Promise<UpdateAccountArchiveStatusResponse> {
        return this.api.post<UpdateAccountArchiveStatusRequest, UpdateAccountArchiveStatusResponse>(NorthCapitalAccountsPaths.UpdateAccountArchiveStatus, request);
    }

    async updateSuitability(request: UpdateSuitabilityRequest): Promise<UpdateSuitabilityResponse> {
        return this.api.put<UpdateSuitabilityRequest, UpdateSuitabilityResponse>(NorthCapitalAccountsPaths.UpdateSuitability, request);
    }
}
