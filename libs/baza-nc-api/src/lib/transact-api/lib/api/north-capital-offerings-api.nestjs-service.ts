import { Injectable } from '@nestjs/common';
import type {
    CancelOfferingRequest,
    CancelOfferingResponse,
    CloseOfferingRequest,
    CloseOfferingResponse,
    CreateEscrowAccountRequest, CreateEscrowAccountResponse,
    CreateOfferingRequest,
    CreateOfferingResponse, DeleteEscrowAccountRequest, DeleteEscrowAccountResponse,
    DeleteOfferingRequest,
    DeleteOfferingResponse,
    GetCustodianDetailsForOfferingRequest,
    GetCustodianDetailsForOfferingResponse, GetEscrowAccountRequest,
    GetEscrowAccountResponse,
    GetOfferingPurchaseHistoryRequest,
    GetOfferingPurchaseHistoryResponse,
    GetOfferingRequest,
    GetOfferingResponse,
    GetOfferingStatusRequest,
    GetOfferingStatusResponse,
    NorthCapitalOfferingsApi,
    SearchOfferingRequest, SearchOfferingResponse,
    UpdateEscrowAccountRequest,
    UpdateEscrowAccountResponse,
    UpdateOfferingRequest, UpdateOfferingResponse
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalOfferingsApiPaths } from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalOfferingsApiNestjsService implements NorthCapitalOfferingsApi {
    constructor(
        private readonly api: NorthCapitalNestjsService
    ) {
    }

    async cancelOffering(request: CancelOfferingRequest): Promise<CancelOfferingResponse> {
        return this.api.post<CancelOfferingRequest, CancelOfferingResponse>(NorthCapitalOfferingsApiPaths.CancelOffering, request);
    }

    async closeOffering(request: CloseOfferingRequest): Promise<CloseOfferingResponse> {
        return this.api.post<CloseOfferingRequest, CloseOfferingResponse>(NorthCapitalOfferingsApiPaths.CloseOffering, request);
    }

    async createEscrowAccount(request: CreateEscrowAccountRequest): Promise<CreateEscrowAccountResponse> {
        return this.api.put<CreateEscrowAccountRequest, CreateEscrowAccountResponse>(NorthCapitalOfferingsApiPaths.CreateEscrowAccount, request);
    }

    async createOffering(request: CreateOfferingRequest): Promise<CreateOfferingResponse> {
        return this.api.put<CreateOfferingRequest, CreateOfferingResponse>(NorthCapitalOfferingsApiPaths.CreateOffering, request);
    }

    async deleteEscrowAccount(request: DeleteEscrowAccountRequest): Promise<DeleteEscrowAccountResponse> {
        return this.api.post<DeleteEscrowAccountRequest, DeleteEscrowAccountResponse>(NorthCapitalOfferingsApiPaths.DeleteEscrowAccount, request);
    }

    async deleteOffering(request: DeleteOfferingRequest): Promise<DeleteOfferingResponse> {
        return this.api.post<DeleteOfferingRequest, DeleteOfferingResponse>(NorthCapitalOfferingsApiPaths.DeleteOffering, request);
    }

    async getCustodianDetailsForOffering(request: GetCustodianDetailsForOfferingRequest): Promise<GetCustodianDetailsForOfferingResponse> {
        return this.api.post<GetCustodianDetailsForOfferingRequest, GetCustodianDetailsForOfferingResponse>(NorthCapitalOfferingsApiPaths.GetCustodianDetailsForOffering, request);
    }

    async getEscrowAccount(request: GetEscrowAccountRequest): Promise<GetEscrowAccountResponse> {
        return this.api.post<GetEscrowAccountRequest, GetEscrowAccountResponse>(NorthCapitalOfferingsApiPaths.GetEscrowAccount, request);
    }

    async getOffering(request: GetOfferingRequest): Promise<GetOfferingResponse> {
        return this.api.post<GetOfferingRequest, GetOfferingResponse>(NorthCapitalOfferingsApiPaths.GetOffering, request);
    }

    async getOfferingPurchaseHistory(request: GetOfferingPurchaseHistoryRequest): Promise<GetOfferingPurchaseHistoryResponse> {
        return this.api.post<GetOfferingPurchaseHistoryRequest, GetOfferingPurchaseHistoryResponse>(NorthCapitalOfferingsApiPaths.GetOfferingPurchaseHistory, request);
    }

    async getOfferingStatus(request: GetOfferingStatusRequest): Promise<GetOfferingStatusResponse> {
        return this.api.post<GetOfferingStatusRequest, GetOfferingStatusResponse>(NorthCapitalOfferingsApiPaths.GetOfferingStatus, request);
    }

    async searchOffering(request: SearchOfferingRequest): Promise<SearchOfferingResponse> {
        return this.api.post<SearchOfferingRequest, SearchOfferingResponse>(NorthCapitalOfferingsApiPaths.SearchOffering, request);
    }

    async updateEscrowAccount(request: UpdateEscrowAccountRequest): Promise<UpdateEscrowAccountResponse> {
        return this.api.post<UpdateEscrowAccountRequest, UpdateEscrowAccountResponse>(NorthCapitalOfferingsApiPaths.UpdateEscrowAccount, request);
    }

    async updateOffering(request: UpdateOfferingRequest): Promise<UpdateOfferingResponse> {
        return this.api.post<UpdateOfferingRequest, UpdateOfferingResponse>(NorthCapitalOfferingsApiPaths.UpdateOffering, request);
    }
}
