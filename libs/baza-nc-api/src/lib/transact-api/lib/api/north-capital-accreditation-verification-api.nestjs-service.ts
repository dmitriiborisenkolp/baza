import { Injectable } from '@nestjs/common';
import * as FormData from 'form-data';
import type {
    GetAiDocumentRequest,
    GetAiDocumentResponse,
    GetAiLetterRequest,
    GetAiLetterResponse,
    GetAiRequestRequest,
    GetAiRequestResponse,
    NorthCapitalAccreditationVerificationApi,
    RequestAiVerificationRequest,
    RequestAiVerificationResponse,
    UpdateAiRequestRequest,
    UpdateAiRequestResponse,
    UpdateAIVerificationRequest,
    UpdateAIVerificationResponse,
    UploadVerificationDocumentRequest,
    UploadVerificationDocumentResponse,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalAccreditationVerificationApiPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalAccreditationVerificationApiNestjsService implements NorthCapitalAccreditationVerificationApi {
    constructor(private readonly api: NorthCapitalNestjsService) {}

    async getAIRequest(request: GetAiRequestRequest): Promise<GetAiRequestResponse> {
        return this.api.post<GetAiRequestRequest, GetAiRequestResponse>(
            NorthCapitalAccreditationVerificationApiPaths.GetAIRequest,
            request,
        );
    }

    async getAiDocument(request: GetAiDocumentRequest): Promise<GetAiDocumentResponse> {
        return this.api.post<GetAiDocumentRequest, GetAiDocumentResponse>(
            NorthCapitalAccreditationVerificationApiPaths.GetAIDocument,
            request,
        );
    }

    async getAiLetter(request: GetAiLetterRequest): Promise<GetAiLetterResponse> {
        return this.api.post<GetAiLetterRequest, GetAiLetterResponse>(NorthCapitalAccreditationVerificationApiPaths.GetAILetter, request);
    }

    async requestAIVerification(request: RequestAiVerificationRequest): Promise<RequestAiVerificationResponse> {
        return this.api.post<RequestAiVerificationRequest, RequestAiVerificationResponse>(
            NorthCapitalAccreditationVerificationApiPaths.RequestAIVerification,
            request,
        );
    }

    async updateAIRequest(request: UpdateAiRequestRequest): Promise<UpdateAiRequestResponse> {
        return this.api.post<UpdateAiRequestRequest, UpdateAiRequestResponse>(
            NorthCapitalAccreditationVerificationApiPaths.UpdateAIRequest,
            request,
        );
    }

    async updateAiVerification(request: UpdateAIVerificationRequest): Promise<UpdateAIVerificationResponse> {
        return this.api.post<UpdateAIVerificationRequest, UpdateAIVerificationResponse>(
            NorthCapitalAccreditationVerificationApiPaths.UpdateAIVerification,
            request,
        );
    }

    async uploadVerificationDocument(
        request: UploadVerificationDocumentRequest,
        file?: Buffer,
    ): Promise<UploadVerificationDocumentResponse> {
        const formData = new FormData();

        formData.append('accountId', request.accountId);
        formData.append('documentTitle', request.documentTitle);
        formData.append('file', file);

        return this.api.postWithFormData<UploadVerificationDocumentRequest, UploadVerificationDocumentResponse>(
            NorthCapitalAccreditationVerificationApiPaths.UploadVerificationDocument,
            formData,
        );
    }
}
