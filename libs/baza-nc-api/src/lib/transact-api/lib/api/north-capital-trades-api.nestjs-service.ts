import { Injectable } from '@nestjs/common';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import type {
    CreateTradeRequest,
    CreateTradeResponse,
    DeleteTradeRequest,
    DeleteTradeResponse,
    EditTradeRequest,
    EditTradeResponse,
    FundReturnRequestRequest,
    FundReturnRequestResponse,
    GetAccountTradeHistoryRequest,
    GetAccountTradeHistoryResponse,
    GetCcFundMoveInfoRequest,
    GetCcFundMoveInfoResponse,
    GetExternalFundMoveInfoRequest,
    GetExternalFundMoveInfoResponse,
    GetTradeRequest,
    GetTradeResponse,
    GetTradesForOfferingRequest,
    GetTradesForOfferingResponse,
    GetTradeStatusRequest,
    GetTradeStatusResponse,
    NorthCapitalTradesApi,
    UpdateDocuSignStatusRequest,
    UpdateDocuSignStatusResponse,
    UpdateTradeArchiveStatusRequest,
    UpdateTradeArchiveStatusResponse,
    UpdateTradeStatusRequest,
    UpdateTradeStatusResponse,
    UpdateTradeTransactionTypeRequest,
    UpdateTradeTransactionTypeResponse,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalTradesApiPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalTradesApiNestjsService implements NorthCapitalTradesApi {
    constructor(private readonly api: NorthCapitalNestjsService, private readonly logger: BazaLogger) {}

    async createTrade(request: CreateTradeRequest, correlationId?: string): Promise<CreateTradeResponse> {
        this.logger.info(`[NorthCapitalTradesApiNestjsService.createTrade]`, {
            correlationId,
            request,
        });

        return this.api.post<CreateTradeRequest, CreateTradeResponse>(NorthCapitalTradesApiPaths.CreateTrade, request);
    }

    async deleteTrade(request: DeleteTradeRequest): Promise<DeleteTradeResponse> {
        return this.api.post<DeleteTradeRequest, DeleteTradeResponse>(NorthCapitalTradesApiPaths.DeleteTrade, request);
    }

    async editTrade(request: EditTradeRequest): Promise<EditTradeResponse> {
        return this.api.put<EditTradeRequest, EditTradeResponse>(NorthCapitalTradesApiPaths.EditTrade, request);
    }

    async fundReturnRequest(request: FundReturnRequestRequest): Promise<FundReturnRequestResponse> {
        return this.api.post<FundReturnRequestRequest, FundReturnRequestResponse>(NorthCapitalTradesApiPaths.FundReturnRequest, request);
    }

    async getAccountTradeHistory(request: GetAccountTradeHistoryRequest): Promise<GetAccountTradeHistoryResponse> {
        return this.api.post<GetAccountTradeHistoryRequest, GetAccountTradeHistoryResponse>(
            NorthCapitalTradesApiPaths.GetAccountTradeHistory,
            request,
        );
    }

    async getTrade(request: GetTradeRequest): Promise<GetTradeResponse> {
        return this.api.post<GetTradeRequest, GetTradeResponse>(NorthCapitalTradesApiPaths.GetTrade, request);
    }

    async getTradeStatus(request: GetTradeStatusRequest, correlationId?: string): Promise<GetTradeStatusResponse> {
        this.logger.info(`[NorthCapitalTradesApiNestjsService.getTradeStatus]`, {
            correlationId,
            request,
        });

        return this.api.post<GetTradeStatusRequest, GetTradeStatusResponse>(NorthCapitalTradesApiPaths.GetTradeStatus, request);
    }

    async updateTradeArchiveStatus(request: UpdateTradeArchiveStatusRequest): Promise<UpdateTradeArchiveStatusResponse> {
        return this.api.post<UpdateTradeArchiveStatusRequest, UpdateTradeArchiveStatusResponse>(
            NorthCapitalTradesApiPaths.UpdateTradeArchiveStatus,
            request,
        );
    }

    async updateTradeStatus(request: UpdateTradeStatusRequest): Promise<UpdateTradeStatusResponse> {
        if (!request.field1) {
            request.field1 = request.tradeId;
        }

        return this.api.post<UpdateTradeStatusRequest, UpdateTradeStatusResponse>(NorthCapitalTradesApiPaths.UpdateTradeStatus, request);
    }

    async updateTradeTransactionType(
        request: UpdateTradeTransactionTypeRequest,
        correlationId?: string,
    ): Promise<UpdateTradeTransactionTypeResponse> {
        this.logger.info(`[NorthCapitalTradesApiNestjsService.updateTradeTransactionType]`, {
            correlationId,
            request,
        });

        return this.api.post<UpdateTradeTransactionTypeRequest, UpdateTradeTransactionTypeResponse>(
            NorthCapitalTradesApiPaths.UpdateTradeTransactionType,
            request,
        );
    }

    async updateDocuSignStatus(request: UpdateDocuSignStatusRequest): Promise<UpdateDocuSignStatusResponse> {
        return this.api.post<UpdateDocuSignStatusRequest, UpdateDocuSignStatusResponse>(
            NorthCapitalTradesApiPaths.UpdateDocuSignStatus,
            request,
        );
    }

    async getTradesForOffering(request: GetTradesForOfferingRequest): Promise<GetTradesForOfferingResponse> {
        return this.api.post<GetTradesForOfferingRequest, GetTradesForOfferingResponse>(
            NorthCapitalTradesApiPaths.GetTradesForOffering,
            request,
        );
    }

    async getExternalFundMoveInfo(request: GetExternalFundMoveInfoRequest): Promise<GetExternalFundMoveInfoResponse> {
        return this.api.post<GetExternalFundMoveInfoRequest, GetExternalFundMoveInfoResponse>(
            NorthCapitalTradesApiPaths.GetExternalFundMoveInfo,
            request,
        );
    }

    async getCCFundMoveInfo(request: GetCcFundMoveInfoRequest): Promise<GetCcFundMoveInfoResponse> {
        return this.api.post<GetCcFundMoveInfoRequest, GetCcFundMoveInfoResponse>(NorthCapitalTradesApiPaths.GetCCFundMoveInfo, request);
    }
}
