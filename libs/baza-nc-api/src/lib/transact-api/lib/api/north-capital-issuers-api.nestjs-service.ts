import { Injectable } from '@nestjs/common';
import type {
    CreateIssuerAccountRequest,
    CreateIssuerAccountResponse,
    CreateIssuerRequest,
    CreateIssuerResponse,
    DeleteIssuerAccountRequest,
    DeleteIssuerAccountResponse,
    DeleteIssuerRequest,
    DeleteIssuerResponse,
    GetIssuerAccountRequest,
    GetIssuerAccountResponse,
    GetIssuerRequest,
    GetIssuerResponse,
    NorthCapitalIssuersApi,
    SearchIssuerRequest,
    SearchIssuerResponse,
    UpdateIssuerAccountRequest,
    UpdateIssuerAccountResponse,
    UpdateIssuerRequest,
    UpdateIssuerResponse,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalIssuersApiPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalIssuersApiNestjsService implements NorthCapitalIssuersApi {
    constructor(private readonly api: NorthCapitalNestjsService) {}

    async createIssuer(request: CreateIssuerRequest): Promise<CreateIssuerResponse> {
        return this.api.put<CreateIssuerRequest, CreateIssuerResponse>(NorthCapitalIssuersApiPaths.CreateIssuer, request);
    }

    async createIssuerAccount(request: CreateIssuerAccountRequest): Promise<CreateIssuerAccountResponse> {
        return this.api.put<CreateIssuerAccountRequest, CreateIssuerAccountResponse>(
            NorthCapitalIssuersApiPaths.CreateIssuerAccount,
            request,
        );
    }

    async deleteIssuer(request: DeleteIssuerRequest): Promise<DeleteIssuerResponse> {
        return this.api.post<DeleteIssuerRequest, DeleteIssuerResponse>(NorthCapitalIssuersApiPaths.DeleteIssuer, request);
    }

    async deleteIssuerAccount(request: DeleteIssuerAccountRequest): Promise<DeleteIssuerAccountResponse> {
        return this.api.post<DeleteIssuerAccountRequest, DeleteIssuerAccountResponse>(
            NorthCapitalIssuersApiPaths.DeleteIssuerAccount,
            request,
        );
    }

    async getIssuer(request: GetIssuerRequest): Promise<GetIssuerResponse> {
        return this.api.post<GetIssuerRequest, GetIssuerResponse>(NorthCapitalIssuersApiPaths.GetIssuer, request);
    }

    async getIssuerAccount(request: GetIssuerAccountRequest): Promise<GetIssuerAccountResponse> {
        return this.api.post<GetIssuerAccountRequest, GetIssuerAccountResponse>(NorthCapitalIssuersApiPaths.GetIssuerAccount, request);
    }

    async searchIssuer(request: SearchIssuerRequest): Promise<SearchIssuerResponse> {
        return this.api.post<SearchIssuerRequest, SearchIssuerResponse>(NorthCapitalIssuersApiPaths.SearchIssuer, request);
    }

    async updateIssuer(request: UpdateIssuerRequest): Promise<UpdateIssuerResponse> {
        return this.api.post<UpdateIssuerRequest, UpdateIssuerResponse>(NorthCapitalIssuersApiPaths.UpdateIssuer, request);
    }

    async updateIssuerAccount(request: UpdateIssuerAccountRequest): Promise<UpdateIssuerAccountResponse> {
        return this.api.post<UpdateIssuerAccountRequest, UpdateIssuerAccountResponse>(
            NorthCapitalIssuersApiPaths.UpdateIssuerAccount,
            request,
        );
    }
}
