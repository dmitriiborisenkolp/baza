import { Injectable, Logger } from '@nestjs/common';
import { BazaNcConfigurationNestjsService } from '../configuration/baza-nc-configuration.nestjs-service';
import * as qs from 'querystring';
import * as FormData from 'form-data';
import { bazaNcApiConfig, isNorthCapitalError, NorthCapitalBaseResponse, NorthCapitalErrorResponse } from '@scaliolabs/baza-nc-shared';
import { default as fetch } from 'node-fetch';
import { NorthCapitalException } from '../exceptions/north-capital.exception';
import { EventBus } from '@nestjs/cqrs';
import { NcRequestEvent } from '../events/nc-request.event';
import { NcErrorEvent } from '../events/nc-error.event';
import { NcResponseEvent } from '../events/nc-response.event';
import { BazaLogger, EnvService } from '@scaliolabs/baza-core-api';
import { bazaNcTransactApiValidateRequest } from '../utils/baza-nc-transact-api-validate-request';
import { HttpService } from '@nestjs/axios';
import { BazaNcRequestFailedException } from '../exceptions/baza-nc-request-failed.exception';
import { BazaNcFailedRequestLogEntity, BazaNcFailedRequestLogRepository } from '../../../typeorm';
import { awaitTimeout } from '@scaliolabs/baza-core-shared';

type RequestId = string;

const generateLogId = () => {
    let result = '';
    const characters = 'abcdef0123456789';

    for (let i = 0; i < 8; i++) {
        result += characters.charAt(Math.floor(Math.random() * characters.length));
    }

    return result;
};

type FetchInputType = {
    uri: string;
    options: any;
};

interface ProcessProcessPayload {
    uri: string;
    input: FetchInputType;
    request: any;
    timesRetried?: number;
    requestId?: string;
    requestDate?: Date;
}

/**
 * Data-Access Service for accessing North Capital API
 * The Service is handling character validation, logging, CQRS events and retrying requests to NC in case if it's
 * returns `undefined` body
 */
@Injectable()
export class NorthCapitalNestjsService {
    constructor(
        private readonly eventBus: EventBus,
        private readonly httpService: HttpService,
        private readonly configuration: BazaNcConfigurationNestjsService,
        private readonly logger: BazaLogger,
        private readonly env: EnvService,
        private readonly failedRequestsRepository: BazaNcFailedRequestLogRepository,
    ) {
        this.logger.setContext('nc-api');
    }

    /**
     * PUT Request to NC
     * @param uri
     * @param request
     */
    async put<T, R extends NorthCapitalBaseResponse<any>>(uri: string, request: T): Promise<R> {
        return this.processPromise({
            uri,
            request,
            input: {
                uri: this.configuration.urlGenerator(uri),
                options: {
                    method: 'put',
                    body: qs.stringify({ ...this.configuration.credentials, ...request }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                },
            },
        });
    }

    /**
     * POST Request to NC
     * @param uri
     * @param request
     */
    async post<T, R extends NorthCapitalBaseResponse<any>>(uri: string, request?: T): Promise<R> {
        return this.processPromise({
            uri,
            request,
            input: {
                uri: this.configuration.urlGenerator(uri),
                options: {
                    method: 'post',
                    body: qs.stringify({ ...this.configuration.credentials, ...request }),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                    },
                },
            },
        });
    }

    /**
     * POST Request to NC (using Form-Data)
     * @param uri
     * @param request
     */
    async postWithFormData<T, R extends NorthCapitalBaseResponse<any>>(uri: string, request: FormData): Promise<R> {
        request.append('clientID', this.configuration.credentials.clientID);
        request.append('developerAPIKey', this.configuration.credentials.developerAPIKey);

        return this.processPromise({
            uri,
            request,
            input: {
                uri: this.configuration.urlGenerator(uri),
                options: {
                    method: 'post',
                    body: request,
                    headers: request.getHeaders(),
                },
            },
        });
    }

    /**
     * Wrapper for all requests to NC API
     * @param payload
     * @private
     */
    private async processPromise<T = any>(payload: ProcessProcessPayload): Promise<T> {
        bazaNcTransactApiValidateRequest(payload.request);

        if (bazaNcApiConfig().requestsDelay) {
            // noinspection JSDeprecatedSymbols
            await awaitTimeout(bazaNcApiConfig().requestsDelay);
        }

        payload.requestId = payload.requestId || generateLogId();
        payload.requestDate = payload.requestDate || new Date();
        payload.timesRetried = payload.timesRetried || 0;

        this.logRequest(payload.requestId, payload.uri, payload.request);

        this.eventBus.publish(
            new NcRequestEvent({
                uri: payload.uri,
                request: payload.request,
                requestId: payload.requestId,
                time: payload.requestDate,
            }),
        );

        return fetch(payload.input.uri, payload.input.options)
            .then((response) => {
                // NC may return 200 OK status & undefined response body. We should retry the request, but only for E2E environment
                // eslint-disable-next-line
                return !!response ? response : this.retryRequest(payload);
            })
            .then((response) => response.text())
            .then((response) => {
                return (response || '').toString().split("\\'").join("'");
            })
            .then((response) => JSON.parse(response))
            .catch((error) => {
                if (error.response && error.response.data) {
                    this.eventBus.publish(
                        new NcErrorEvent({
                            uri: payload.uri,
                            request: payload.request,
                            requestId: payload.requestId,
                            time: new Date(),
                            error: error.response.data,
                        }),
                    );

                    return this.handleNorthCapitalResponse({
                        requestId: payload.requestId,
                        uri: payload.uri,
                        request: payload.request,
                        response: error.response.data,
                        requestDate: payload.requestDate,
                    });
                } else {
                    this.eventBus.publish(
                        new NcErrorEvent({
                            uri: payload.uri,
                            request: payload.request,
                            requestId: payload.requestId,
                            time: new Date(),
                            error: 'JSON parse failed',
                        }),
                    );

                    return this.retryRequest(payload);
                }
            })
            .then((response) => {
                return this.handleNorthCapitalResponse({
                    requestId: payload.requestId,
                    uri: payload.uri,
                    request: payload.request,
                    requestDate: payload.requestDate,
                    response,
                });
            });
    }

    /**
     * Additional post-request handlers for NC Requests
     * @param details
     * @private
     */
    private handleNorthCapitalResponse(details: {
        requestId: RequestId;
        uri: string;
        request: any;
        response: NorthCapitalBaseResponse;
        requestDate: Date;
    }): any {
        this.logResponse(details.requestId, details.uri, details.response);

        if (isNorthCapitalError(details.response)) {
            this.eventBus.publish(
                new NcErrorEvent({
                    uri: details.uri,
                    request: details.request,
                    requestId: details.requestId,
                    time: new Date(),
                    error: details.response,
                }),
            );

            throw new NorthCapitalException(
                details.requestId,
                details.response as any as NorthCapitalErrorResponse,
                details.request,
                details.response,
            );
        } else {
            this.eventBus.publish(
                new NcResponseEvent({
                    uri: details.uri,
                    request: details.request,
                    requestId: details.requestId,
                    time: new Date(),
                    response: details.response,
                }),
            );

            return details.response;
        }
    }

    /**
     * Attempts to retry request to NC API
     * If payload.timesRetried exceeded, method will throw an BazaNcRequestFailedException error
     * @see NorthCapitalNestjsService.failRequest
     * @param payload
     * @private
     */
    private async retryRequest<T = any>(payload: ProcessProcessPayload): Promise<T> {
        const isE2eEnvironment = this.env.isLocalEnvironment || this.env.isTestEnvironment;
        const isRetryEnabledNonE2E = bazaNcApiConfig().enableRetriesForNonE2eEnvironments;
        const maxAttempts = bazaNcApiConfig().maxE2eRequestRetries;

        if ((isE2eEnvironment || isRetryEnabledNonE2E) && payload.timesRetried <= maxAttempts) {
            await awaitTimeout(500);

            this.logger.debug(
                `[NorthCapitalNestjsService] [handleResponse] [${payload.requestId}] Retrying request, attempts ${
                    payload.timesRetried + 1
                }`,
            );

            return this.processPromise({
                ...payload,
                timesRetried: payload.timesRetried + 1,
            });
        } else {
            return this.failRequest(payload.uri, payload.requestId);
        }
    }

    /**
     * Fails request to NC. Additionally it may log error to DB
     * @param uri
     * @param requestId
     * @private
     */
    private async failRequest(uri: string, requestId: string): Promise<never> {
        const enableLogging = bazaNcApiConfig().enableLogFailedRequestRetries;

        if (enableLogging) {
            const logEntity = new BazaNcFailedRequestLogEntity();

            logEntity.uri = uri;
            logEntity.requestId = requestId;
            logEntity.date = new Date();

            await this.failedRequestsRepository.save(logEntity);
        }

        throw new BazaNcRequestFailedException();
    }

    /**
     * Log NC Request
     * @param id
     * @param uri
     * @param request
     * @private
     */
    private logRequest(id: RequestId, uri: string, request: any): void {
        if (this.configuration.configuration.enableNestjsLog) {
            this.logger.log({
                type: 'nc-request',
                uri,
                id,
                body: request,
            });
        }
    }

    /**
     * Log NC Response
     * @param id
     * @param uri
     * @param response
     * @private
     */
    private logResponse(id: RequestId, uri: string, response: any): void {
        if (this.configuration.configuration.enableNestjsLog) {
            this.logger.log({
                type: 'nc-response',
                uri,
                id,
                body: response,
            });
        }
    }
}
