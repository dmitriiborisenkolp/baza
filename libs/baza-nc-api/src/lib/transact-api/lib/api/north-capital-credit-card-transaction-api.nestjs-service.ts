import { Injectable } from '@nestjs/common';
import type {
    AddCreditCardRequest,
    AddCreditCardResponse,
    CcFundMoveRequest,
    CcFundMoveResponse,
    DeleteCreditCardRequest,
    DeleteCreditCardResponse,
    GetCcFundMoveHistoryRequest,
    GetCcFundMoveHistoryResponse,
    GetCcFundMoveInfoRequest,
    GetCcFundMoveInfoResponse,
    GetCreditCardRequest,
    GetCreditCardResponse,
    NorthCapitalCreditCardTransactionApi,
    RequestForVoidCcTransactionRequest,
    RequestForVoidCcTransactionResponse,
    UpdateCcFundMoveStatusRequest,
    UpdateCcFundMoveStatusResponse,
    UpdateCreditCardRequest,
    UpdateCreditCardResponse,
    CcFundMovementRequest,
    CcFundMovementResponse,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalCreditCardTransactionApiPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalCreditCardTransactionApiNestjsService implements NorthCapitalCreditCardTransactionApi {
    constructor(private readonly api: NorthCapitalNestjsService) {}

    async addCreditCard(request: AddCreditCardRequest): Promise<AddCreditCardResponse> {
        return this.api.post<AddCreditCardRequest, AddCreditCardResponse>(NorthCapitalCreditCardTransactionApiPaths.AddCreditCard, request);
    }

    async updateCreditCard(request: UpdateCreditCardRequest): Promise<UpdateCreditCardResponse> {
        return this.api.post<UpdateCreditCardRequest, UpdateCreditCardResponse>(
            NorthCapitalCreditCardTransactionApiPaths.UpdateCreditCard,
            request,
        );
    }

    async deleteCreditCard(request: DeleteCreditCardRequest): Promise<DeleteCreditCardResponse> {
        return this.api.post<DeleteCreditCardRequest, DeleteCreditCardResponse>(
            NorthCapitalCreditCardTransactionApiPaths.DeleteCreditCard,
            request,
        );
    }

    async getCreditCard(request: GetCreditCardRequest): Promise<GetCreditCardResponse> {
        return this.api.post<GetCreditCardRequest, GetCreditCardResponse>(NorthCapitalCreditCardTransactionApiPaths.GetCreditCard, request);
    }

    async cCFundMove(request: CcFundMoveRequest): Promise<CcFundMoveResponse> {
        return this.api.post<CcFundMoveRequest, CcFundMoveResponse>(NorthCapitalCreditCardTransactionApiPaths.CCFundMove, request);
    }

    async cCFundMovement(request: CcFundMovementRequest): Promise<CcFundMovementResponse> {
        return this.api.post<CcFundMovementRequest, CcFundMovementResponse>(
            NorthCapitalCreditCardTransactionApiPaths.CCFundMovement,
            request,
        );
    }

    async getCCFundMoveHistory(request: GetCcFundMoveHistoryRequest): Promise<GetCcFundMoveHistoryResponse> {
        return this.api.post<GetCcFundMoveHistoryRequest, GetCcFundMoveHistoryResponse>(
            NorthCapitalCreditCardTransactionApiPaths.GetCCFundMoveHistory,
            request,
        );
    }

    async getCCFundMoveInfo(request: GetCcFundMoveInfoRequest): Promise<GetCcFundMoveInfoResponse> {
        return this.api.post<GetCcFundMoveInfoRequest, GetCcFundMoveInfoResponse>(
            NorthCapitalCreditCardTransactionApiPaths.GetCCFundMoveInfo,
            request,
        );
    }

    async updateCCFundMoveStatus(request: UpdateCcFundMoveStatusRequest): Promise<UpdateCcFundMoveStatusResponse> {
        return this.api.post<UpdateCcFundMoveStatusRequest, UpdateCcFundMoveStatusResponse>(
            NorthCapitalCreditCardTransactionApiPaths.UpdateCcFundMoveStatus,
            request,
        );
    }

    async requestForVoidCCTransaction(request: RequestForVoidCcTransactionRequest): Promise<RequestForVoidCcTransactionResponse> {
        return this.api.post<RequestForVoidCcTransactionRequest, RequestForVoidCcTransactionResponse>(
            NorthCapitalCreditCardTransactionApiPaths.RequestForVoidCCTransaction,
            request,
        );
    }
}
