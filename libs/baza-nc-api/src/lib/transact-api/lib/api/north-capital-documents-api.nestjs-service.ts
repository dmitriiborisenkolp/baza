import { Injectable } from '@nestjs/common';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';
import * as FormData from 'form-data';
import type {
    AddDocumentsForOfferingRequest,
    AddDocumentsForOfferingResponse,
    AddSubscriptionsForOfferingRequest,
    AddSubscriptionsForOfferingResponse,
    DeleteDocumentsForOfferingRequest,
    DeleteDocumentsForOfferingResponse,
    DeleteSubscriptionsForOfferingRequest,
    DeleteSubscriptionsForOfferingResponse,
    GetDocumentsForOfferingRequest,
    GetDocumentsForOfferingResponse,
    GetSubscriptionsForOfferingRequest,
    GetSubscriptionsForOfferingResponse,
    GetTradeDocumentRequest,
    GetTradeDocumentResponse,
    GetUploadPartyDocumentRequest,
    GetUploadPartyDocumentResponse,
    NorthCapitalDocumentsApi,
    RequestUploadPartyDocumentRequest,
    RequestUploadPartyDocumentResponse,
    ResendPartyAgreementRequest,
    ResendPartyAgreementResponse,
    ResendSubscriptionDocumentsRequest,
    ResendSubscriptionDocumentsResponse,
    SendPartyAgreementRequest,
    SendPartyAgreementResponse,
    SendSubscriptionDocumentRequest,
    SendSubscriptionDocumentResponse,
    UpdateDocumentForOfferingRequest,
    UpdateDocumentForOfferingResponse,
    UpdateSubscriptionsForOfferingRequest,
    UpdateSubscriptionsForOfferingResponse,
    UpdateTradeDocArchiveStatusRequest,
    UpdateTradeDocArchiveStatusResponse,
    UploadAccountDocumentRequest,
    UploadAccountDocumentResponse,
    UploadEntityDocumentRequest,
    UploadEntityDocumentResponse,
    UploadPartyDocumentRequest,
    UploadPartyDocumentResponse,
    UploadTradeDocumentRequest,
    UploadTradeDocumentResponse,
    SendSubscriptionDocumentClientResponse, GetAllSignedDocumentRequest, GetAllSignedDocumentResponse,
} from '@scaliolabs/baza-nc-shared';
import { FetchSubscriptionDocumentsResponse, NorthCapitalDocumentsApiPaths } from '@scaliolabs/baza-nc-shared';

@Injectable()
export class NorthCapitalDocumentsApiNestjsService implements NorthCapitalDocumentsApi {
    constructor(private readonly api: NorthCapitalNestjsService) {}

    async addDocumentsForOffering(
        request: AddDocumentsForOfferingRequest,
        uploadedFile: Buffer,
    ): Promise<AddDocumentsForOfferingResponse> {
        const formData = new FormData();

        formData.append('offeringId', request.offeringId || '');
        formData.append('documentTitle', request.documentTitle || '');
        formData.append(
            'documentFileReferenceCode',
            request.documentFileReferenceCode || '',
        );
        formData.append('file_name', request.file_name || '');
        formData.append('templateName', request.templateName || '');
        formData.append('approval', request.approval || '');
        formData.append('supervisorname', request.supervisorname || '');
        formData.append('date', request.date || '');
        formData.append('createdIpAddress', request.createdIpAddress || '');
        formData.append('userfile0', uploadedFile);

        return this.api.postWithFormData<
            AddDocumentsForOfferingRequest,
            AddDocumentsForOfferingResponse
        >(NorthCapitalDocumentsApiPaths.AddDocumentsForOffering, formData);
    }

    async addSubscriptionsForOffering(
        request: AddSubscriptionsForOfferingRequest,
    ): Promise<AddSubscriptionsForOfferingResponse> {
        return this.api.post<
            AddSubscriptionsForOfferingRequest,
            AddSubscriptionsForOfferingResponse
        >(NorthCapitalDocumentsApiPaths.AddSubscriptionsForOffering, request);
    }

    async deleteDocumentsForOffering(
        request: DeleteDocumentsForOfferingRequest,
    ): Promise<DeleteDocumentsForOfferingResponse> {
        return this.api.post<
            DeleteDocumentsForOfferingRequest,
            DeleteDocumentsForOfferingResponse
        >(NorthCapitalDocumentsApiPaths.DeleteDocumentsForOffering, request);
    }

    async deleteSubscriptionsForOffering(
        request: DeleteSubscriptionsForOfferingRequest,
    ): Promise<DeleteSubscriptionsForOfferingResponse> {
        return this.api.post<
            DeleteSubscriptionsForOfferingRequest,
            DeleteSubscriptionsForOfferingResponse
        >(
            NorthCapitalDocumentsApiPaths.DeleteSubscriptionsForOffering,
            request,
        );
    }

    async getDocumentsForOffering(
        request: GetDocumentsForOfferingRequest,
    ): Promise<GetDocumentsForOfferingResponse> {
        return this.api.post<
            GetDocumentsForOfferingRequest,
            GetDocumentsForOfferingResponse
        >(NorthCapitalDocumentsApiPaths.GetDocumentsForOffering, request);
    }

    async getSubscriptionsForOffering(
        request: GetSubscriptionsForOfferingRequest,
    ): Promise<GetSubscriptionsForOfferingResponse> {
        return this.api.post<
            GetSubscriptionsForOfferingRequest,
            GetSubscriptionsForOfferingResponse
        >(NorthCapitalDocumentsApiPaths.GetSubscriptionsForOffering, request);
    }

    async getTradeDocument(
        request: GetTradeDocumentRequest,
    ): Promise<GetTradeDocumentResponse> {
        return this.api.post<GetTradeDocumentRequest, GetTradeDocumentResponse>(
            NorthCapitalDocumentsApiPaths.GetTradeDocument,
            request,
        );
    }

    async getUploadPartyDocument(
        request: GetUploadPartyDocumentRequest,
    ): Promise<GetUploadPartyDocumentResponse> {
        return this.api.post<
            GetUploadPartyDocumentRequest,
            GetUploadPartyDocumentResponse
        >(NorthCapitalDocumentsApiPaths.GetUploadPartyDocument, request);
    }

    async requestUploadPartyDocument(
        request: RequestUploadPartyDocumentRequest,
        uploadedFile: Buffer,
    ): Promise<RequestUploadPartyDocumentResponse> {
        const formData = new FormData();

        formData.append('requestId', request.requestId);
        formData.append('investorId', request.investorId);
        formData.append(
            'documentTitle',
            `documentTitle0=${request.documentTitle}`,
        );
        formData.append('file_name', `filename0=${request.file_name}`);
        formData.append('createdIpAddress', request.createdIpAddress);
        formData.append('userfile0', uploadedFile, {
            filename: request.file_name,
        });

        return this.api.post<
            RequestUploadPartyDocumentRequest,
            RequestUploadPartyDocumentResponse
        >(NorthCapitalDocumentsApiPaths.RequestUploadPartyDocument, request);
    }

    async resendPartyAgreement(
        request: ResendPartyAgreementRequest,
    ): Promise<ResendPartyAgreementResponse> {
        return this.api.post<
            ResendPartyAgreementRequest,
            ResendPartyAgreementResponse
        >(NorthCapitalDocumentsApiPaths.ResendPartyAgreement, request);
    }

    async resendSubscriptionDocuments(
        request: ResendSubscriptionDocumentsRequest,
    ): Promise<ResendSubscriptionDocumentsResponse> {
        return this.api.post<
            ResendSubscriptionDocumentsRequest,
            ResendSubscriptionDocumentsResponse
        >(NorthCapitalDocumentsApiPaths.ResendSubscriptionDocuments, request);
    }

    async sendPartyAgreement(
        request: SendPartyAgreementRequest,
    ): Promise<SendPartyAgreementResponse> {
        return this.api.post<
            SendPartyAgreementRequest,
            SendPartyAgreementResponse
        >(NorthCapitalDocumentsApiPaths.SendPartyAgreement, request);
    }

    async sendSubscriptionDocument(
        request: SendSubscriptionDocumentRequest,
    ): Promise<SendSubscriptionDocumentResponse> {
        return this.api.post<
            SendSubscriptionDocumentRequest,
            SendSubscriptionDocumentResponse
        >(NorthCapitalDocumentsApiPaths.SendSubscriptionDocument, request);
    }

    async sendSubscriptionDocumentClient(
        request: SendSubscriptionDocumentRequest,
    ): Promise<SendSubscriptionDocumentClientResponse> {
        return this.api.post<
            SendSubscriptionDocumentRequest,
            SendSubscriptionDocumentClientResponse
        >(
            NorthCapitalDocumentsApiPaths.SendSubscriptionDocumentClient,
            request,
        );
    }

    async updateDocumentForOffering(
        request: UpdateDocumentForOfferingRequest,
    ): Promise<UpdateDocumentForOfferingResponse> {
        return this.api.post<
            UpdateDocumentForOfferingRequest,
            UpdateDocumentForOfferingResponse
        >(NorthCapitalDocumentsApiPaths.UpdateDocumentForOffering, request);
    }

    async updateSubscriptionsForOffering(
        request: UpdateSubscriptionsForOfferingRequest,
    ): Promise<UpdateSubscriptionsForOfferingResponse> {
        return this.api.post<
            UpdateSubscriptionsForOfferingRequest,
            UpdateSubscriptionsForOfferingResponse
        >(
            NorthCapitalDocumentsApiPaths.UpdateSubscriptionsForOffering,
            request,
        );
    }

    async updateTradeDocArchiveStatus(
        request: UpdateTradeDocArchiveStatusRequest,
    ): Promise<UpdateTradeDocArchiveStatusResponse> {
        return this.api.post<
            UpdateTradeDocArchiveStatusRequest,
            UpdateTradeDocArchiveStatusResponse
        >(NorthCapitalDocumentsApiPaths.UpdateTradeDocArchiveStatus, request);
    }

    async uploadAccountDocument(
        request: UploadAccountDocumentRequest,
        uploadedFile?: Buffer,
    ): Promise<UploadAccountDocumentResponse> {
        const formData = new FormData();

        formData.append('accountId', request.accountId || '');
        formData.append(
            'documentTitle',
            `documentTitle0=${request.documentTitle}`,
        );
        formData.append('file_name', `filename0=${request.file_name}`);
        formData.append('createdIpAddress', request.createdIpAddress || '');
        formData.append('userfile0', uploadedFile, {
            filename: request.file_name,
        });

        return this.api.postWithFormData<
            UploadAccountDocumentRequest,
            UploadAccountDocumentResponse
        >(NorthCapitalDocumentsApiPaths.UploadAccountDocument, formData);
    }

    async uploadEntityDocument(
        request: UploadEntityDocumentRequest,
        uploadedFile?: any,
    ): Promise<UploadEntityDocumentResponse> {
        const formData = new FormData();

        formData.append('partyId', request.partyId || '');
        formData.append(
            'documentTitle',
            `documentTitle0=${request.documentTitle}`,
        );
        formData.append('file_name', `filename0=${request.file_name}`);
        formData.append('createdIpAddress', request.createdIpAddress || '');
        formData.append('userfile0', uploadedFile, {
            filename: request.file_name,
        });

        return this.api.postWithFormData<
            UploadEntityDocumentRequest,
            UploadEntityDocumentResponse
        >(NorthCapitalDocumentsApiPaths.UploadEntityDocument, formData);
    }

    async uploadPartyDocument(
        request: UploadPartyDocumentRequest,
        uploadedFile?: any,
    ): Promise<UploadPartyDocumentResponse> {
        const formData = new FormData();

        formData.append('partyId', request.partyId || '');
        formData.append(
            'documentTitle',
            `documentTitle0=${request.documentTitle}`,
        );
        formData.append('file_name', `filename0=${request.file_name}`);
        formData.append('createdIpAddress', request.createdIpAddress || '');
        formData.append('userfile0', uploadedFile, {
            filename: request.file_name,
        });

        return this.api.postWithFormData<
            UploadPartyDocumentRequest,
            UploadPartyDocumentResponse
        >(NorthCapitalDocumentsApiPaths.UploadPartyDocument, formData);
    }

    async uploadTradeDocument(
        request: UploadTradeDocumentRequest,
        uploadedFile?: any,
    ): Promise<UploadTradeDocumentResponse> {
        const formData = new FormData();

        formData.append('tradeId', request.tradeId || '');
        formData.append(
            'documentTitle',
            `documentTitle0=${request.documentTitle}`,
        );
        formData.append('file_name', `filename0=${request.file_name}`);
        formData.append('createdIpAddress', request.createdIpAddress || '');
        formData.append('userfile0', uploadedFile, {
            filename: request.file_name,
        });

        return this.api.postWithFormData<
            UploadTradeDocumentRequest,
            UploadTradeDocumentResponse
        >(NorthCapitalDocumentsApiPaths.UploadTradeDocument, formData);
    }

    async getAllSignedDocument(
        request: GetAllSignedDocumentRequest,
    ): Promise<GetAllSignedDocumentResponse> {
        return this.api.post<
            GetAllSignedDocumentRequest,
            GetAllSignedDocumentResponse
            >(NorthCapitalDocumentsApiPaths.GetAllSignedDocument, request);
    }

    async fetchSubscriptionDocuments(): Promise<FetchSubscriptionDocumentsResponse> {
        return this.api.post<
            void,
            FetchSubscriptionDocumentsResponse
        >(NorthCapitalDocumentsApiPaths.FetchSubscriptionDocuments);
    }
}
