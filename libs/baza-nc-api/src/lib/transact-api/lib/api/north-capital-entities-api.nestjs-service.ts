import { Injectable } from '@nestjs/common';
import type {
    CreateEntityRequest,
    CreateEntityResponse,
    DeleteEntityDocumentRequest,
    DeleteEntityDocumentResponse,
    DeleteEntityRequest,
    DeleteEntityResponse,
    GetEntityDocumentRequest,
    GetEntityDocumentResponse,
    GetEntityRequest,
    GetEntityResponse,
    NorthCapitalEntitiesApi,
    SearchEntityRequest,
    SearchEntityResponse,
    UpdateEntityRequest,
    UpdateEntityResponse,
    UpdatePartyEntityArchiveStatusRequest,
    UpdatePartyEntityArchiveStatusResponse,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalEntitiesApiPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalEntitiesApiNestjsService implements NorthCapitalEntitiesApi {
    constructor(private readonly api: NorthCapitalNestjsService) {}

    async createEntity(request: CreateEntityRequest): Promise<CreateEntityResponse> {
        return this.api.put<CreateEntityRequest, CreateEntityResponse>(NorthCapitalEntitiesApiPaths.CreateEntity, request);
    }

    async deleteEntity(request: DeleteEntityRequest): Promise<DeleteEntityResponse> {
        return this.api.post<DeleteEntityRequest, DeleteEntityResponse>(NorthCapitalEntitiesApiPaths.DeleteEntity, request);
    }

    async deleteEntityDocument(request: DeleteEntityDocumentRequest): Promise<DeleteEntityDocumentResponse> {
        return this.api.post<DeleteEntityDocumentRequest, DeleteEntityDocumentResponse>(
            NorthCapitalEntitiesApiPaths.DeleteEntityDocument,
            request,
        );
    }

    async getEntity(request: GetEntityRequest): Promise<GetEntityResponse> {
        return this.api.post<GetEntityRequest, GetEntityResponse>(NorthCapitalEntitiesApiPaths.GetEntity, request);
    }

    async getEntityDocument(request: GetEntityDocumentRequest): Promise<GetEntityDocumentResponse> {
        return this.api.post<GetEntityDocumentRequest, GetEntityDocumentResponse>(NorthCapitalEntitiesApiPaths.GetEntityDocument, request);
    }

    async searchEntity(request: SearchEntityRequest): Promise<SearchEntityResponse> {
        return this.api.post<SearchEntityRequest, SearchEntityResponse>(NorthCapitalEntitiesApiPaths.SearchEntity, request);
    }

    async updateEntity(request: UpdateEntityRequest): Promise<UpdateEntityResponse> {
        return this.api.post<UpdateEntityRequest, UpdateEntityResponse>(NorthCapitalEntitiesApiPaths.UpdateEntity, request);
    }

    async updatePartyEntityArchiveStatus(request: UpdatePartyEntityArchiveStatusRequest): Promise<UpdatePartyEntityArchiveStatusResponse> {
        return this.api.post<UpdatePartyEntityArchiveStatusRequest, UpdatePartyEntityArchiveStatusResponse>(
            NorthCapitalEntitiesApiPaths.UpdatePartyEntityArchiveStatus,
            request,
        );
    }
}
