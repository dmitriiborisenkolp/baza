import { Injectable } from '@nestjs/common';
import type {
    DeleteKycAmlRequest,
    DeleteKycAmlResponse,
    GetKycAmlRequest,
    GetKycAmlResponse,
    NorthCapitalKycAmlApi,
    PerformAmlRequest,
    PerformAmlResponse,
    PerformKycAmlRequest,
    PerformKycAmlResponse,
    RequestKycAmlRequest,
    RequestKycAmlResponse,
    UpdateKycAmlRequest,
    UpdateKycAmlResponse,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalKycAmlApiPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalKycAmlNestjsService implements NorthCapitalKycAmlApi {
    constructor(private readonly api: NorthCapitalNestjsService) {}

    async deleteKycAml(request: DeleteKycAmlRequest): Promise<DeleteKycAmlResponse> {
        return this.api.post<DeleteKycAmlRequest, DeleteKycAmlResponse>(NorthCapitalKycAmlApiPaths.DeleteKycAml, request);
    }

    async getKycAml(request: GetKycAmlRequest): Promise<GetKycAmlResponse> {
        return this.api.post<GetKycAmlRequest, GetKycAmlResponse>(NorthCapitalKycAmlApiPaths.GetKycAml, request);
    }

    async performAml(request: PerformAmlRequest): Promise<PerformAmlResponse> {
        return this.api.post<PerformAmlRequest, PerformAmlResponse>(NorthCapitalKycAmlApiPaths.PerformAml, request);
    }

    async performKycAml(request: PerformKycAmlRequest): Promise<PerformKycAmlResponse> {
        return this.api.post<PerformKycAmlRequest, PerformKycAmlResponse>(NorthCapitalKycAmlApiPaths.PerformKycAml, request);
    }

    async performKycAmlBasic(request: PerformKycAmlRequest): Promise<PerformKycAmlResponse> {
        return this.api.post<PerformKycAmlRequest, PerformKycAmlResponse>(NorthCapitalKycAmlApiPaths.PerformKycAmlBasic, request);
    }

    async requestKycAml(request: RequestKycAmlRequest): Promise<RequestKycAmlResponse> {
        return this.api.post<RequestKycAmlRequest, RequestKycAmlResponse>(NorthCapitalKycAmlApiPaths.RequestKycAml, request);
    }

    async updateKycAml(request: UpdateKycAmlRequest): Promise<UpdateKycAmlResponse> {
        return this.api.put<UpdateKycAmlRequest, UpdateKycAmlResponse>(NorthCapitalKycAmlApiPaths.UpdateKycAml, request);
    }
}
