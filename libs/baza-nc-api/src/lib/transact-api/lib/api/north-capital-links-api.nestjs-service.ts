import { Injectable } from '@nestjs/common';
import type {
    CreateLinkRequest,
    CreateLinkResponse,
    DeleteLinkRequest,
    DeleteLinkResponse,
    GetAllLinksRequest,
    GetAllLinksResponse,
    GetLinkRequest,
    GetLinkResponse,
    NorthCapitalLinksApi,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalLinksApiPaths } from '@scaliolabs/baza-nc-shared';
import { NorthCapitalNestjsService } from './north-capital.nestjs-service';

@Injectable()
export class NorthCapitalLinksApiNestjsService implements NorthCapitalLinksApi {
    constructor(private readonly api: NorthCapitalNestjsService) {}

    async createLink(request: CreateLinkRequest): Promise<CreateLinkResponse> {
        return this.api.put<CreateLinkRequest, CreateLinkResponse>(NorthCapitalLinksApiPaths.CreateLink, request);
    }

    async deleteLink(request: DeleteLinkRequest): Promise<DeleteLinkResponse> {
        return this.api.post<DeleteLinkRequest, DeleteLinkResponse>(NorthCapitalLinksApiPaths.DeleteLink, request);
    }

    async getAllLinks(request: GetAllLinksRequest): Promise<GetAllLinksResponse> {
        return this.api.post<GetAllLinksRequest, GetAllLinksResponse>(NorthCapitalLinksApiPaths.GetAllLinks, request);
    }

    async getLink(request: GetLinkRequest): Promise<GetLinkResponse> {
        return this.api.post<GetLinkRequest, GetLinkResponse>(NorthCapitalLinksApiPaths.GetAllLinks, request);
    }
}
