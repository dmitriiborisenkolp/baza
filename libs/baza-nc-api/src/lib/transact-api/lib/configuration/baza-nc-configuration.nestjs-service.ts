import { Injectable } from '@nestjs/common';

interface Configuration {
    debug?: boolean;
    baseUrl: string;
    credentials?: {
        clientID: string;
        developerAPIKey: string;
    };
    enableNestjsLog: boolean;
}

export { Configuration as NorthCapitalConfigurationNestjsServiceConfiguration };

@Injectable()
export class BazaNcConfigurationNestjsService {
    constructor(private readonly _configuration: Configuration) {}

    get configuration(): Configuration {
        return {
            ...this._configuration,
        };
    }

    get credentials(): Credential | any {
        return this._configuration.credentials || {};
    }

    get isAvailable(): boolean {
        return !!this._configuration && !!this._configuration.baseUrl;
    }

    public urlGenerator(uri: string): string {
        return `${this._configuration.baseUrl}/${uri}`;
    }
}
