import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcApiErrorCodes, bazaNcApiErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcInvalidCharactersException extends BazaAppException {
    constructor(details: {
        failedCharacter: string,
    }) {
        super(
            BazaNcApiErrorCodes.BazaNcCharacterIsNotAllowed,
            bazaNcApiErrorCodesI18n[BazaNcApiErrorCodes.BazaNcCharacterIsNotAllowed],
            HttpStatus.BAD_REQUEST,
            {},
            details,
        );
    }
}
