import { HttpException, HttpStatus } from '@nestjs/common';
import { NorthCapitalErrorCodes, NorthCapitalErrorResponse } from '@scaliolabs/baza-nc-shared';

export class NorthCapitalException<T = NorthCapitalErrorCodes> extends HttpException {
    public ncStatusCode: NorthCapitalErrorCodes;
    public ncStatusDesc: string;

    constructor(
        public readonly requestId: string,
        public readonly ncError: NorthCapitalErrorResponse,
        public readonly ncRequestObject: any,
        public readonly ncResponseObject: any,
    ) {
        super(
            `[NorthCapitalException] [${ncError.statusCode}] [${requestId}] ${ncError.statusDesc}`,
            HttpStatus.INTERNAL_SERVER_ERROR,
        );

        this.ncStatusCode = ncError.statusCode;
        this.ncStatusDesc = ncError.statusDesc;
    }
}

export function isNorthCapitalException(err, errorCode: NorthCapitalErrorCodes): boolean {
    return !! err
        && (err instanceof NorthCapitalException)
        && (err.ncError.statusCode === errorCode);
}
