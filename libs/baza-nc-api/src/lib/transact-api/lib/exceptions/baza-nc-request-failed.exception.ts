import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcApiErrorCodes, bazaNcApiErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcRequestFailedException extends BazaAppException {
    constructor() {
        super(
            BazaNcApiErrorCodes.BazaNcRequestFailed,
            bazaNcApiErrorCodesI18n[BazaNcApiErrorCodes.BazaNcRequestFailed],
            HttpStatus.INTERNAL_SERVER_ERROR,
        );
    }
}
