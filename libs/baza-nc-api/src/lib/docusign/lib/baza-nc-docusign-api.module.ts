import { Module } from '@nestjs/common';
import { BazaNcTransactionApiModule } from '../../transaction';
import { BazaNcDocusignCmsController } from './controllers/baza-nc-docusign-cms.controller';
import { BazaNcDocusignMapper } from './mappers/baza-nc-docusign.mapper';
import { BazaNcDocusignService } from './services/baza-nc-docusign.service';

@Module({
    imports: [BazaNcTransactionApiModule],
    controllers: [BazaNcDocusignCmsController],
    providers: [BazaNcDocusignMapper, BazaNcDocusignService],
    exports: [BazaNcDocusignMapper, BazaNcDocusignService],
})
export class BazaNcDocusignApiModule {}
