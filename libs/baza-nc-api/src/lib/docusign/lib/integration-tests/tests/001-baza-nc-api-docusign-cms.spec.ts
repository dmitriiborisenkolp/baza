import 'reflect-metadata';
import { BazaE2eFixturesNodeAccess, BazaE2eNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcDocusignCmsNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/docusign/integration-tests/tests/001-baza-nc-api-docusign-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDocusignCms = new BazaNcDocusignCmsNodeAccess(http);

    let DOCUSIGN_TEMPLATE_ID: string;
    let DOCUSIGN_TEMPLATE_NAME: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns all available docusign templates', async () => {
        const response = await dataAccessDocusignCms.getAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length > 0).toBeTruthy();

        DOCUSIGN_TEMPLATE_ID = response[0].id;
        DOCUSIGN_TEMPLATE_NAME = response[0].name;
    });

    it('will returns docusign template by id', async () => {
        const response = await dataAccessDocusignCms.getById({
            id: DOCUSIGN_TEMPLATE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(DOCUSIGN_TEMPLATE_ID);
        expect(response.name).toBe(DOCUSIGN_TEMPLATE_NAME);
    });
});
