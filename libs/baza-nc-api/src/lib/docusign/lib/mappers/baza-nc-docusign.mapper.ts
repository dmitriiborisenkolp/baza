import { Injectable } from '@nestjs/common';
import { DocusignTemplateDetails, BazaNcDocusignDto } from '@scaliolabs/baza-nc-shared';

@Injectable()
export class BazaNcDocusignMapper {
    entityToDTO(input: DocusignTemplateDetails): BazaNcDocusignDto {
        return {
            id: input.templateId,
            combinedId: `${input.templateId}--${input.name}`,
            name: input.name,
        };
    }

    entitiesToDTOs(input: Array<DocusignTemplateDetails>): Array<BazaNcDocusignDto> {
        return input.map((entity) => this.entityToDTO(entity));
    }
}
