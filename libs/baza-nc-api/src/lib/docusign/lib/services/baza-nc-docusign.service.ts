import { Injectable } from '@nestjs/common';
import { NorthCapitalDocumentsApiNestjsService } from '../../../transact-api';
import { NcDocusignCmsGetByIdRequest, BazaNcDocusignDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcDocusignMapper } from '../mappers/baza-nc-docusign.mapper';
import { BazaNcDocusignTemplateNotFoundException } from '../exceptions/baza-nc-docusign-template-not-found.exception';

@Injectable()
export class BazaNcDocusignService {
    constructor(private readonly mapper: BazaNcDocusignMapper, private readonly ncDocumentsApi: NorthCapitalDocumentsApiNestjsService) {}

    async getAll(): Promise<Array<BazaNcDocusignDto>> {
        const response = await this.ncDocumentsApi.fetchSubscriptionDocuments();

        return this.mapper.entitiesToDTOs(response.document_details);
    }

    async getById(request: NcDocusignCmsGetByIdRequest): Promise<BazaNcDocusignDto> {
        const document = (await this.getAll()).find((d) => d.id === request.id);

        if (!document) {
            throw new BazaNcDocusignTemplateNotFoundException();
        }

        return document;
    }
}
