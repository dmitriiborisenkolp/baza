import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
    BazaNorthCapitalCMSOpenApi,
    BazaNcDocusignCmsEndpoint,
    NcDocusignCmsEndpointPaths,
    NcDocusignCmsGetByIdRequest,
    BazaNcDocusignDto,
} from '@scaliolabs/baza-nc-shared';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import { BazaNcDocusignService } from '../services/baza-nc-docusign.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalDocusignCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
export class BazaNcDocusignCmsController implements BazaNcDocusignCmsEndpoint {
    constructor(private readonly service: BazaNcDocusignService) {}

    @Post(NcDocusignCmsEndpointPaths.getAll)
    @ApiOperation({
        summary: 'getAll',
        description: 'Returns full list of all available DocuSign templates for offerings',
    })
    @ApiOkResponse({
        type: BazaNcDocusignDto,
        isArray: true,
    })
    async getAll(): Promise<Array<BazaNcDocusignDto>> {
        return this.service.getAll();
    }

    @Post(NcDocusignCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns DocuSign template by TemplateId--Name combination',
    })
    @ApiOkResponse({
        type: BazaNcDocusignDto,
    })
    async getById(@Body() request: NcDocusignCmsGetByIdRequest): Promise<BazaNcDocusignDto> {
        return this.service.getById(request);
    }
}
