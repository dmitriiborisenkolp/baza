import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDocusignErrorCodes, ncDocusignErrorCodesI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDocusignTemplateNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcDocusignErrorCodes.NcDocusignTemplateNotFound,
            ncDocusignErrorCodesI18n[BazaNcDocusignErrorCodes.NcDocusignTemplateNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
