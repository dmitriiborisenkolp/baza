interface CommandRequest {
    offeringName: string;
    targetAmount: number;
    minAmount: number;
    maxAmount: number;
    offeringFees?: number;
    currentValueCents: number;
    maxSharesPerAccount?: number;
    unitPrice: number;
    startDate: string;
    endDate: string;
    offeringText: string;
    stampingText: string;
    subscriptionOfferingIds: Array<string>;
    updatedIPaddress?: string;
    daysToReleaseFailedTrades?: number;
}

export class BazaNcCreateOfferingCommand {
    constructor(public readonly request: CommandRequest) {}
}
