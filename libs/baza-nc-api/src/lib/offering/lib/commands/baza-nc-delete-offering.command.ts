import { OfferingId } from '@scaliolabs/baza-nc-shared';

interface CommandRequest {
    offeringId: OfferingId;
}

export class BazaNcDeleteOfferingCommand {
    constructor(public readonly request: CommandRequest) {}
}
