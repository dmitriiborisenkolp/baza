import { IssuerType, OfferingId } from '@scaliolabs/baza-nc-shared';

interface CommandRequest {
    issuerName: string;
    issuerType: IssuerType;
    targetAmount: number;
    minAmount: number;
    maxAmount: number;
    offeringFees: number;
    currentValueCents: number;
    maxSharesPerAccount: number;
    unitPrice: number;
    startDate: string;
    endDate: string;
    offeringName: string;
    offeringText: string;
    stampingText: string;
    subscriptionOfferingIds: Array<string>;
    updatedIPaddress?: string;
    daysToReleaseFailedTrades?: number;
}

export class BazaNcUpdateOfferingCommand<T = any> {
    constructor(public readonly offeringId: OfferingId, public readonly request: CommandRequest) {}
}
