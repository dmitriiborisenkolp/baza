import { NorthCapitalAccountId, OfferingId, TradeId } from '@scaliolabs/baza-nc-shared';

export class BazaNcRequestSubscriptionDocumentsForOfferingCommand {
    constructor(
        public readonly offeringId: OfferingId,
        public readonly accountId: NorthCapitalAccountId,
        public readonly tradeId: TradeId,
    ) {}
}
