export type NorthCapitalOfferingsSessionId = string;

export function generateSessionId(): NorthCapitalOfferingsSessionId {
    return [...Array(16)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');
}
