import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcOfferingTransactionFeesService } from '../services/baza-nc-offering-transaction-fees.service';
import { BazaNcWebhookEvent, NorthCapitalTopic } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingRepository } from '../../../typeorm';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcWebhookEvent)
export class UpdateOfferingEventHandler implements IEventHandler<BazaNcWebhookEvent> {
    constructor(
        private readonly ncOfferingRepository: BazaNcOfferingRepository,
        private readonly transactionFees: BazaNcOfferingTransactionFeesService,
    ) {}

    handle(event: BazaNcWebhookEvent): void {
        cqrs(UpdateOfferingEventHandler.name, async () => {
            const message = event.message;

            switch (message.topic) {
                case NorthCapitalTopic.updateOffering: {
                    const ncOffering = await this.ncOfferingRepository.findOfferingWithId(message.responseBody.offeringId);

                    if (ncOffering) {
                        ncOffering.ncIssuerId = message.responseBody.issuerId;
                        ncOffering.ncIssueName = message.responseBody.issueName;
                        ncOffering.ncIssueType = message.responseBody.issueType;
                        ncOffering.ncTargetAmount = parseFloat(message.responseBody.targetAmount);
                        ncOffering.ncMinAmount = parseFloat(message.responseBody.minAmount);
                        ncOffering.ncMaxAmount = parseFloat(message.responseBody.maxAmount);
                        ncOffering.ncUnitPrice = parseFloat(message.responseBody.unitPrice);
                        ncOffering.ncTotalShares = Math.floor(parseFloat(message.responseBody.totalShares));
                        ncOffering.ncRemainingShares = Math.floor(parseFloat(message.responseBody.remainingShares));
                        ncOffering.ncStartDate = message.responseBody.startDate;
                        ncOffering.ncEndDate = message.responseBody.endDate;
                        ncOffering.ncOfferingText = message.responseBody.offeringText;
                        ncOffering.ncStampingText = message.responseBody.stampingText;

                        this.transactionFees.decentOfferingEntityFees(ncOffering);

                        await this.ncOfferingRepository.save(ncOffering);
                    }
                }
            }
        });
    }
}
