import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import {
    BazaNcOfferingCreatedEvent,
    BazaNcOfferingUpdatedEvent,
    BazaNcTradeCreatedEvent,
    BazaNcTradeDeletedEvent,
    BazaNcTradeUpdatedEvent,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingsPercentsFundedService } from '../services/baza-nc-offerings-percents-funded.service';
import { cqrs } from '@scaliolabs/baza-core-api';
import { awaitTimeout } from '@scaliolabs/baza-core-shared';

type Event =
    | BazaNcOfferingCreatedEvent
    | BazaNcOfferingUpdatedEvent
    | BazaNcTradeCreatedEvent
    | BazaNcTradeUpdatedEvent
    | BazaNcTradeDeletedEvent;

@EventsHandler(
    BazaNcOfferingCreatedEvent,
    BazaNcOfferingUpdatedEvent,
    BazaNcTradeCreatedEvent,
    BazaNcTradeUpdatedEvent,
    BazaNcTradeDeletedEvent,
)
/**
 * TODO: We should make optimization over it. Make it task-based / queue-based / anything else.
 * Updating percents funded operation is heavy task and we would like to not run this event way too many times
 */
export class UpdatePercentsFundedEventHandler implements IEventHandler<Event> {
    constructor(private readonly percentsFunded: BazaNcOfferingsPercentsFundedService) {}

    handle(event: Event): void {
        cqrs(UpdatePercentsFundedEventHandler.name, async () => {
            await awaitTimeout(1000);

            await this.percentsFunded.updatePercentsFunded(event.request.ncOfferingId);
        });
    }
}
