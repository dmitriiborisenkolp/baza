import { Injectable } from '@nestjs/common';
import { BazaNcOfferingEntity } from '../../../typeorm';
import { BazaNcOfferingListItemDto, bazaNcOfferingName } from '@scaliolabs/baza-nc-shared';

type ListItem = Pick<BazaNcOfferingEntity, 'id' | 'ncOfferingId' | 'ncOfferingName' | 'status'>;

@Injectable()
export class BazaNcOfferingListItemMapper {
    entityToDTO(input: ListItem): BazaNcOfferingListItemDto {
        return {
            id: input.id,
            status: input.status,
            ncOfferingId: input.ncOfferingId,
            ncOfferingName: bazaNcOfferingName(input),
        };
    }

    entitiesToDTOs(input: Array<ListItem>): Array<BazaNcOfferingListItemDto> {
        return input.map((next) => this.entityToDTO(next));
    }
}
