import { Injectable } from '@nestjs/common';
import { bazaNcExtractDocusignTemplateIdUtil, BazaNcOfferingDto, bazaNcOfferingName } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingEntity } from '../../../typeorm';

@Injectable()
export class BazaNcOfferingMapper {
    entityToDTO(input: BazaNcOfferingEntity): BazaNcOfferingDto {
        const ncSubscriptionOfferingIds = (input.ncSubscriptionOfferingIds || []).map((tId) => bazaNcExtractDocusignTemplateIdUtil(tId));

        return {
            id: input.id,
            status: input.status,
            percentsFunded: input.percentsFunded >= 100 ? '100' : (input.percentsFunded || 0).toFixed(2),
            ncOfferingId: input.ncOfferingId,
            ncOfferingName: bazaNcOfferingName(input),
            ncIssuerId: input.ncIssuerId,
            ncIssueName: input.ncIssueName,
            ncIssueType: input.ncIssueType,
            ncTargetAmount: input.ncTargetAmount,
            ncIsTargetAmountHidden: input.ncIsTargetAmountHidden,
            ncTargetAmountPlaceholder: input.ncTargetAmountPlaceholder,
            ncMinAmount: input.ncMinAmount,
            ncMaxAmount: input.ncMaxAmount,
            ncCurrentValueCents: input.ncCurrentValueCents,
            ncMaxSharesPerAccount: input.ncMaxSharesPerAccount,
            ncUnitPrice: input.ncUnitPrice,
            ncOfferingFees: input.ncOfferingFees || 0,
            ncStartDate: input.ncStartDate ? new Date(input.ncStartDate).toISOString() : undefined,
            ncEndDate: input.ncEndDate ? new Date(input.ncEndDate).toISOString() : undefined,
            ncTotalShares: input.ncTotalShares,
            ncRemainingShares: input.ncRemainingShares,
            ncOfferingStatus: input.ncOfferingStatus,
            ncOfferingText: input.ncOfferingText,
            ncStampingText: input.ncStampingText,
            ncUpdatedIPAddress: input.ncUpdatedIPAddress,
            ncSubscriptionOfferingIds,
            ncSubscriptionOfferingId: ncSubscriptionOfferingIds[0],
            daysToReleaseFailedTrades: input.daysToReleaseFailedTrades,
        };
    }

    entitiesToDTOs(input: Array<BazaNcOfferingEntity>): Array<BazaNcOfferingDto> {
        return input.map((e) => this.entityToDTO(e));
    }
}
