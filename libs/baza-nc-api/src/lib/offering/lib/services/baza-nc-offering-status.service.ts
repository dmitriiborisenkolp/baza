import { Injectable } from '@nestjs/common';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcOfferingStatusUpdatedEvent, BazaNcOfferingStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingRepository } from '../../../typeorm';

@Injectable()
export class BazaNcOfferingStatusService {
    constructor(private readonly eventBus: EventBus, private readonly repository: BazaNcOfferingRepository) {}

    async updateStatus(ncOfferingId: OfferingId, newStatus: BazaNcOfferingStatus): Promise<void> {
        const offering = await this.repository.getOfferingWithId(ncOfferingId);

        const origStatus = offering.status;

        offering.status = newStatus;

        await this.repository.save(offering);

        this.eventBus.publish(
            new BazaNcOfferingStatusUpdatedEvent({
                id: offering.id,
                ncOfferingId: offering.ncOfferingId,
                origStatus,
                newStatus,
            }),
        );
    }
}
