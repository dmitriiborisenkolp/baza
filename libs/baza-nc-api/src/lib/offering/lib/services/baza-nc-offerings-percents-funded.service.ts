import { Injectable } from '@nestjs/common';
import { EventBus } from '@nestjs/cqrs';
import {
    BazaNcOfferingPercentsFundUpdatedEvent,
    BazaNcOfferingSyncedEvent,
    convertFromCents,
    convertToCents,
    OfferingId,
} from '@scaliolabs/baza-nc-shared';
import { TransactionState } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingEntity, BazaNcOfferingRepository, BazaNcTransactionRepository } from '../../../typeorm';
import { NorthCapitalOfferingsApiNestjsService } from '../../../transact-api';

/**
 * Percents Funded Service
 * We rely mostly on our calculation for Percents Funded of Offering, but with latest changes
 * we also using `remainingShares` field from NC directly
 */
@Injectable()
export class BazaNcOfferingsPercentsFundedService {
    constructor(
        private readonly eventBus: EventBus,
        private readonly ncOffering: BazaNcOfferingRepository,
        private readonly ncTransactions: BazaNcTransactionRepository,
        private readonly ncOfferingApi: NorthCapitalOfferingsApiNestjsService,
    ) {}

    /**
     * Updates Percents Funded value for all Offerings
     */
    async updateAllOfferings(): Promise<void> {
        const offerings = await this.ncOffering.findAll();

        for (const offering of offerings) {
            await this.updatePercentsFunded(offering.ncOfferingId);
        }
    }

    /**
     * Updates `ncRemainingShares` value with actual value from NC
     * In addition returns BazaNcOfferingEntity
     * @param ncOfferingId
     */
    async updateNcRemainingSharesOfOffering(ncOfferingId: OfferingId): Promise<BazaNcOfferingEntity> {
        const offering = await this.ncOffering.getOfferingWithId(ncOfferingId);

        const ncOffering = (
            await this.ncOfferingApi.getOffering({
                offeringId: ncOfferingId,
            })
        ).offeringDetails[0];

        if (+ncOffering.remainingShares !== offering.ncRemainingShares) {
            offering.ncRemainingShares = +ncOffering.remainingShares;

            await this.ncOffering.save(offering, {
                disableEventPublish: true,
            });
        }

        return offering;
    }

    /**
     * Updates Percents Funded for Offering
     * Additionally it updates `ncRemainingShares` value
     * @param ncOfferingId
     */
    async updatePercentsFunded(ncOfferingId: OfferingId): Promise<void> {
        const offering = await this.updateNcRemainingSharesOfOffering(ncOfferingId);
        const transactions = await this.ncTransactions.findByNcOfferingIdWithStates(ncOfferingId, [
            TransactionState.PendingPayment,
            TransactionState.PaymentFunded,
            TransactionState.PaymentConfirmed,
        ]);

        let totalPurchasedAmount = 0;

        for (const transaction of transactions) {
            totalPurchasedAmount += transaction.amountCents;
        }

        offering.percentsFunded = Math.max(
            0,
            Math.min(100, convertFromCents(convertToCents(totalPurchasedAmount) / offering.ncTargetAmount)),
        );

        await this.ncOffering.save(offering, {
            disableEventPublish: true,
        });

        this.eventBus.publish(
            new BazaNcOfferingPercentsFundUpdatedEvent({
                id: offering.id,
                ncOfferingId: offering.ncOfferingId,
                percentsFunded: offering.percentsFunded,
            }),
        );

        this.eventBus.publish(
            new BazaNcOfferingSyncedEvent({
                id: offering.id,
                ncOfferingId: offering.ncOfferingId,
            }),
        );
    }
}
