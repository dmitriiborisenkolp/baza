import { Injectable, Logger } from '@nestjs/common';
import { BazaNcCreateOfferingCommand } from '../commands/baza-nc-create-offering.command';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcRequestSubscriptionDocumentsForOfferingCommand } from '../commands/baza-nc-request-subscription-documents-for-offering.command';
import { BazaNcUpdateOfferingCommand } from '../commands/baza-nc-update-offering.command';
import { BazaNcDeleteOfferingCommand } from '../commands/baza-nc-delete-offering.command';
import {
    BAZA_NC_TRANSACT_API_PRECISION,
    BazaNcOfferingSyncedEvent,
    convertFromCents,
    convertToCents,
    CreateOfferingRequest,
    GetOfferingResponse,
    IssuerType,
    BazaNcOfferingDetailsDto,
    NcOfferingDetailsRequest,
    BazaNcOfferingDto,
    BazaNcOfferingStatus,
    NorthCapitalErrorCodes,
    OfferingId,
    OfferingStatus,
    UpdateOfferingRequest,
    BazaNcOfferingListItemDto,
} from '@scaliolabs/baza-nc-shared';
import { AccountEntity, BazaLogger, CrudService } from '@scaliolabs/baza-core-api';
import { CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { BazaNcOfferingMapper } from '../mappers/baza-nc-offering.mapper';
import { BazaNcOfferingEntity, BazaNcOfferingNotFoundException, BazaNcOfferingRepository } from '../../../typeorm';
import {
    isNorthCapitalException,
    NorthCapitalDocumentsApiNestjsService,
    NorthCapitalException,
    NorthCapitalOfferingsApiNestjsService,
} from '../../../transact-api';
import { BazaNcOfferingTransactionFeesService } from './baza-nc-offering-transaction-fees.service';
import { BazaNcOfferingCannotBeCreatedWithGivenParamsException } from '../exceptions/baza-nc-offering-cannot-be-created-with-given-params.exception';
import { BazaNcOfferingListItemMapper } from '../mappers/baza-nc-offering-list-item.mapper';
import { BazaNcIssuerService } from '../../../issuer';

/**
 * Baza NC Offering Service
 */
@Injectable()
export class BazaNcOfferingsService {
    constructor(
        private readonly eventBus: EventBus,
        private readonly repository: BazaNcOfferingRepository,
        private readonly ncOfferingApi: NorthCapitalOfferingsApiNestjsService,
        private readonly ncDocumentApi: NorthCapitalDocumentsApiNestjsService,
        private readonly bazaLogger: BazaLogger,
        private readonly crud: CrudService,
        private readonly mapper: BazaNcOfferingMapper,
        private readonly listItemMapper: BazaNcOfferingListItemMapper,
        private readonly transactionFees: BazaNcOfferingTransactionFeesService,
        private readonly logger: BazaLogger,
        private readonly issuer: BazaNcIssuerService,
    ) {}

    /**
     * Returns true if there is an Offering stored in Local DB
     * @param offeringId
     */
    async hasOffering(offeringId: OfferingId): Promise<boolean> {
        return this.repository.hasOfferingWithId(offeringId);
    }

    /**
     * Imports Offering from NC API to Local DB
     * @param offeringId
     */
    async touchOffering(offeringId: OfferingId): Promise<BazaNcOfferingEntity> {
        return this.syncWithNcOffering(offeringId);
    }

    /**
     * Updates (Sync) Local DB Data for Offerings currently stored in Local DB
     */
    async syncAllNcOfferings(): Promise<void> {
        const ncOfferings = await this.repository.findAll();

        for (const ncOffering of ncOfferings) {
            try {
                await this.syncWithNcOffering(ncOffering.ncOfferingId);
            } catch (err) {
                Logger.error(`[NcOfferingsService] [syncAllNcOfferings] Error`);
                Logger.error(err);
            }
        }
    }

    /**
     * Updates (Sync) Local DB Data for Offering
     * @param offeringId
     * @param correlationId
     */
    async syncWithNcOffering(offeringId: OfferingId, correlationId?: string): Promise<BazaNcOfferingEntity> {
        try {
            this.logger.info(`[BazaNcOfferingsService.syncWithNcOffering]`, {
                offeringId,
                correlationId,
            });

            const ncOffering = (
                await this.ncOfferingApi.getOffering({
                    offeringId,
                })
            ).offeringDetails[0];

            const offeringEntity = (await this.repository.hasOfferingWithId(offeringId))
                ? await this.repository.getOfferingWithId(offeringId)
                : new BazaNcOfferingEntity();

            offeringEntity.ncOfferingId = ncOffering.offeringId;
            offeringEntity.ncIssuerId = ncOffering.issuerId;
            offeringEntity.ncIssueName = ncOffering.issueName;
            offeringEntity.ncIssueType = ncOffering.issueType;
            offeringEntity.ncTargetAmount = parseFloat(ncOffering.targetAmount);
            offeringEntity.ncMinAmount = parseFloat(ncOffering.minAmount);
            offeringEntity.ncMaxAmount = parseFloat(ncOffering.maxAmount);
            offeringEntity.ncTotalShares = Math.floor(parseFloat(ncOffering.totalShares));
            offeringEntity.ncRemainingShares = Math.floor(parseFloat(ncOffering.remainingShares));
            offeringEntity.ncUnitPrice = parseFloat(ncOffering.unitPrice);
            offeringEntity.ncStartDate = ncOffering.startDate;
            offeringEntity.ncEndDate = ncOffering.endDate;
            offeringEntity.ncOfferingStatus = ncOffering.offeringStatus;
            offeringEntity.ncOfferingText = ncOffering.offeringText;
            offeringEntity.ncStampingText = ncOffering.stampingText;

            this.transactionFees.decentOfferingEntityFees(offeringEntity);

            if (!offeringEntity.ncMaxSharesPerAccount) {
                offeringEntity.ncMaxSharesPerAccount = Math.floor(offeringEntity.ncMaxAmount / offeringEntity.ncUnitPrice);
            }

            await this.repository.save(offeringEntity);

            this.eventBus.publish(
                new BazaNcOfferingSyncedEvent({
                    id: offeringEntity.id,
                    ncOfferingId: offeringEntity.ncOfferingId,
                }),
            );

            return offeringEntity;
        } catch (err) {
            if (
                err instanceof NorthCapitalException &&
                [NorthCapitalErrorCodes.OfferingIdClientIdDoesNotMatch, NorthCapitalErrorCodes.OfferingIdDoesNotExist].includes(
                    err.ncStatusCode,
                )
            ) {
                throw new BazaNcOfferingNotFoundException({
                    offeringId,
                });
            } else {
                throw err;
            }
        }
    }

    /**
     * Returns list of Offerings stored in Local DB
     * @param request
     */
    async listOfferings(request: CrudListRequestDto<BazaNcOfferingDto>): Promise<CrudListResponseDto<BazaNcOfferingDto>> {
        return this.crud.find<BazaNcOfferingEntity, BazaNcOfferingDto>({
            request,
            entity: BazaNcOfferingEntity,
            mapper: (items) => Promise.resolve(this.mapper.entitiesToDTOs(items)),
            findOptions: {
                order: {
                    id: 'DESC',
                },
            },
        });
    }

    /**
     * Returns all Offerings stored in Local DB
     */
    async listAllOfferings(): Promise<Array<BazaNcOfferingListItemDto>> {
        const entities = await this.repository.findAll();

        return this.listItemMapper.entitiesToDTOs(entities);
    }

    /**
     * Creates a new Offering using NC API and creates an updatable Entity
     * in Local DB
     * @param issuer
     * @param command
     */
    async createOffering(issuer: AccountEntity, command: BazaNcCreateOfferingCommand): Promise<BazaNcOfferingEntity> {
        const issuerType: IssuerType = IssuerType.Fund;
        const issuerName: string = issuer.fullName;

        const ncIssuer = await this.issuer.factoryIssuer(issuer);

        const offeringPayload: CreateOfferingRequest = {
            issuerId: ncIssuer.issuerId,
            issueName: ncIssuer.issuerName || issuer.fullName,
            issueType: issuerType,
            targetAmount: command.request.targetAmount,
            minAmount: command.request.minAmount,
            maxAmount: command.request.maxAmount,
            unitPrice: command.request.unitPrice,
            startDate: command.request.startDate,
            endDate: command.request.endDate,
            offeringText: command.request.offeringText,
            stampingText: command.request.stampingText,
        };

        this.transactionFees.appendOfferingFees(offeringPayload, command.request.offeringFees);

        this.validateOfferingAmounts(offeringPayload);

        const ncOffering = (await this.ncOfferingApi.createOffering(offeringPayload)).offeringDetails[1][0];

        await this.linkDocuSignSubscriptionDocumentsToOffering(ncOffering.offeringId, command.request.subscriptionOfferingIds);

        const offeringEntity = new BazaNcOfferingEntity();

        offeringEntity.status = BazaNcOfferingStatus.ComingSoon;
        offeringEntity.ncOfferingId = ncOffering.offeringId;
        offeringEntity.ncIssuerId = ncIssuer.issuerId;
        offeringEntity.ncIssueName = ncIssuer.issuerName || issuerName;
        offeringEntity.ncIssueType = issuerType;
        offeringEntity.ncTargetAmount = command.request.targetAmount;
        offeringEntity.ncMinAmount = command.request.minAmount;
        offeringEntity.ncMaxAmount = command.request.maxAmount;
        offeringEntity.ncOfferingFees = command.request.offeringFees || 0;
        offeringEntity.ncCurrentValueCents = command.request.currentValueCents;
        offeringEntity.ncTotalShares = Math.floor(command.request.targetAmount / command.request.unitPrice);
        offeringEntity.ncRemainingShares = offeringEntity.ncTotalShares;
        offeringEntity.ncMaxSharesPerAccount = command.request.maxSharesPerAccount;
        offeringEntity.ncUnitPrice = command.request.unitPrice;
        offeringEntity.ncStartDate = command.request.startDate;
        offeringEntity.ncEndDate = command.request.endDate;
        offeringEntity.ncOfferingText = command.request.offeringText;
        offeringEntity.ncOfferingName = command.request.offeringName;
        offeringEntity.ncOfferingStatus = ncOffering.offeringStatus || OfferingStatus.Pending;
        offeringEntity.ncStampingText = command.request.stampingText;
        offeringEntity.ncSubscriptionOfferingIds = command.request.subscriptionOfferingIds;
        offeringEntity.daysToReleaseFailedTrades = command.request.daysToReleaseFailedTrades;

        if (!offeringEntity.ncMaxSharesPerAccount) {
            offeringEntity.ncMaxSharesPerAccount = Math.floor(offeringEntity.ncMaxAmount / offeringEntity.ncUnitPrice);
        }

        await this.repository.save(offeringEntity);

        return offeringEntity;
    }

    /**
     * Updates Offering with NC API and updates data in Local DB
     * @param command
     */
    async updateOffering(command: BazaNcUpdateOfferingCommand): Promise<BazaNcOfferingEntity> {
        const offeringPayload: UpdateOfferingRequest = {
            offeringId: command.offeringId,
            issueName: command.request.issuerName,
            targetAmount: command.request.targetAmount,
            minAmount: command.request.minAmount,
            maxAmount: command.request.maxAmount,
            unitPrice: command.request.unitPrice,
            startDate: command.request.startDate,
            endDate: command.request.endDate,
            offeringText: command.request.offeringText,
            stampingText: command.request.stampingText,
        };

        this.transactionFees.appendOfferingFees(offeringPayload, command.request.offeringFees);

        this.validateOfferingAmounts(offeringPayload);

        await this.ncOfferingApi.updateOffering(offeringPayload);

        await this.linkDocuSignSubscriptionDocumentsToOffering(command.offeringId, command.request.subscriptionOfferingIds);

        const ncOffering = (
            await this.ncOfferingApi.getOffering({
                offeringId: command.offeringId,
            })
        ).offeringDetails[0];

        const offeringEntity = await this.repository.getOfferingWithId(command.offeringId);

        offeringEntity.ncCurrentValueCents = command.request.currentValueCents;
        offeringEntity.ncTargetAmount = parseFloat(ncOffering.targetAmount);
        offeringEntity.ncMinAmount = parseFloat(ncOffering.minAmount);
        offeringEntity.ncMaxAmount = parseFloat(ncOffering.maxAmount);
        offeringEntity.ncUnitPrice = parseFloat(ncOffering.unitPrice);
        offeringEntity.ncStartDate = ncOffering.startDate;
        offeringEntity.ncEndDate = ncOffering.endDate;
        offeringEntity.ncOfferingName = command.request.offeringName;
        offeringEntity.ncOfferingText = ncOffering.offeringText;
        offeringEntity.ncStampingText = ncOffering.stampingText;
        offeringEntity.ncSubscriptionOfferingIds = command.request.subscriptionOfferingIds;
        offeringEntity.ncOfferingFees = command.request.offeringFees;
        offeringEntity.daysToReleaseFailedTrades = command.request.daysToReleaseFailedTrades;

        this.transactionFees.decentOfferingEntityFees(offeringEntity);

        if (command.request.maxSharesPerAccount) {
            offeringEntity.ncMaxSharesPerAccount = command.request.maxSharesPerAccount;
        }

        if (!offeringEntity.ncMaxSharesPerAccount) {
            offeringEntity.ncMaxSharesPerAccount = Math.floor(offeringEntity.ncMaxAmount / offeringEntity.ncUnitPrice);
        }

        await this.repository.save(offeringEntity);

        return offeringEntity;
    }

    /**
     * Links DocuSign Subscription Documents for Offerings
     * @param offeringId
     * @param subscriptionOfferingIds
     */
    async linkDocuSignSubscriptionDocumentsToOffering(offeringId: OfferingId, subscriptionOfferingIds: Array<string>): Promise<void> {
        const currentSubscriptionsOfOffering = await this.getDocuSignSubscriptionDocumentsOfOffering(offeringId);

        for (const templateId of currentSubscriptionsOfOffering) {
            await this.ncDocumentApi.deleteSubscriptionsForOffering({
                templateId,
            });
        }

        for (const template of subscriptionOfferingIds) {
            await this.ncDocumentApi.addSubscriptionsForOffering({
                offeringId: offeringId,
                file_name: `templatename0=${template}`,
            });
        }
    }

    /**
     * Returns Array of DocuSign Template IDs which are used for Subscription Documents
     * Baza potentially allow multiple Subscription Documents for Offerings, but currently
     * and by UI (FE) only 1 Subscription Document per Offering is supported
     * @param offeringId
     */
    async getDocuSignSubscriptionDocumentsOfOffering(offeringId): Promise<Array<string>> {
        try {
            const response = await this.ncDocumentApi.getSubscriptionsForOffering({
                offeringId,
            });

            return response['document_details'].map((d) => `${d.templateId}--${d.templateName}`);
        } catch (err) {
            if (err instanceof NorthCapitalException && err.ncStatusCode === NorthCapitalErrorCodes.NotFound) {
                return [];
            } else {
                throw err;
            }
        }
    }

    /**
     * Requests Subscription Documents for Offering
     * Subscription Documents will be sent as Email to Account's Email Address
     * @param command
     */
    async requestSubscriptionDocumentsForOffering(command: BazaNcRequestSubscriptionDocumentsForOfferingCommand): Promise<void> {
        await this.ncDocumentApi.sendSubscriptionDocumentClient({
            tradeId: command.tradeId,
            accountId: command.accountId,
            offeringId: command.offeringId,
        });
    }

    /**
     * Removes Offering from Local DB
     * @param command
     */
    async forgetOffering(command: BazaNcDeleteOfferingCommand): Promise<void> {
        if (await this.repository.hasOfferingWithId(command.request.offeringId)) {
            const offeringEntity = await this.repository.getOfferingWithId(command.request.offeringId);

            await this.repository.remove(offeringEntity);
        }
    }

    /**
     * Returns Offering by Offering Id
     * Only Offerings stored in Local DB could be returned
     * @param offeringId
     */
    async getOffering(offeringId: OfferingId): Promise<BazaNcOfferingEntity> {
        return this.repository.getOfferingWithId(offeringId);
    }

    /**
     * Returns Offering Details (for Purchase Flow)
     * Used for Stats endpoint and for some internal implementations
     * @param request
     */
    async ncOfferingDetails(request: NcOfferingDetailsRequest): Promise<BazaNcOfferingDetailsDto> {
        try {
            const response = await this.ncOfferingApi.getOffering({
                offeringId: request.ncOfferingId,
            });

            const ncOffering = (response as GetOfferingResponse).offeringDetails[0];
            const offeringDetails = {
                ncOfferingId: ncOffering.offeringId,
                minAmount: parseFloat(ncOffering.minAmount),
                maxAmount: parseFloat(ncOffering.maxAmount),
                targetAmount: parseFloat(ncOffering.targetAmount),
                unitPrice: parseFloat(ncOffering.unitPrice),
                startDate: new Date(ncOffering.startDate).toISOString(),
                endDate: new Date(ncOffering.endDate).toISOString(),
            };

            const offering = await this.repository.findOfferingWithId(ncOffering.offeringId);

            if (offering && offering.ncOfferingFees) {
                this.transactionFees.decentOfferingFees(offeringDetails, offering.ncOfferingFees);
            }

            return offeringDetails;
        } catch (err) {
            if (isNorthCapitalException(err, NorthCapitalErrorCodes.OfferingIdClientIdDoesNotMatch)) {
                throw new BazaNcOfferingNotFoundException({
                    offeringId: request.ncOfferingId,
                });
            } else {
                throw err;
            }
        }
    }

    /**
     * Method validates if given Min/Max/Target Amounts and Unit Price will not lead to issues like purchasing
     * last shares or missing auto-close for offerings
     */
    private validateOfferingAmounts(request: CreateOfferingRequest | UpdateOfferingRequest): void {
        const validatePrecision = (input: number) => {
            if (parseFloat(input.toFixed(BAZA_NC_TRANSACT_API_PRECISION)) !== input) {
                throw new BazaNcOfferingCannotBeCreatedWithGivenParamsException();
            }
        };

        const validateInputPrecision = () => {
            validatePrecision(request.minAmount);
            validatePrecision(request.maxAmount);
            validatePrecision(request.targetAmount);
            validatePrecision(request.unitPrice);
        };

        const validateEachPossiblePurchaseAmount = () => {
            for (let i = 1; i <= request.unitPrice; i++) {
                const result = i * convertToCents(convertFromCents(request.unitPrice)); // We emulate $ <=> cents conversion here

                validatePrecision(result);
            }
        };

        [validateInputPrecision, validateEachPossiblePurchaseAmount].forEach((validator) => validator());
    }
}
