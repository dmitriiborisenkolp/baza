import { Injectable } from '@nestjs/common';
import {
    BAZA_NC_TRANSACT_API_PRECISION,
    convertFromCents,
    BazaNcOfferingHealthCheckIssue,
    BazaNcOfferingHealthCheckIssueDto,
    NcOfferingHealthCheckRequest,
    NcOfferingHealthCheckResponse,
    OrderStatus,
} from '@scaliolabs/baza-nc-shared';
import { NorthCapitalTradesApiNestjsService } from '../../../transact-api';
import { BazaNcInvestorAccountRepository, BazaNcTransactionRepository } from '../../../typeorm';

@Injectable()
export class BazaNcOfferingHealthCheckService {
    constructor(
        private readonly ncTradeApi: NorthCapitalTradesApiNestjsService,
        private readonly transactionRepository: BazaNcTransactionRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    async healthCheck(request: NcOfferingHealthCheckRequest): Promise<NcOfferingHealthCheckResponse> {
        const issues: Array<BazaNcOfferingHealthCheckIssueDto> = [];

        issues.push(...(await this.healthCheckTransactions(request)));

        return {
            issues,
            healthy: issues.length === 0,
        };
    }

    private async healthCheckTransactions(request: NcOfferingHealthCheckRequest): Promise<Array<BazaNcOfferingHealthCheckIssueDto>> {
        const issues: Array<BazaNcOfferingHealthCheckIssueDto> = [];

        const ncTrades = (await this.ncTradeApi.getTradesForOffering({ offeringId: request.ncOfferingId }))['Offering purchased details'];
        const cmsTrades = await this.transactionRepository.findAllTransactionsOfOffering(request.ncOfferingId);
        const ncTradeIds = ncTrades.map((t) => t.orderId);
        const cmsTradeIds = cmsTrades.map((t) => t.ncTradeId);

        const unknownTrades = cmsTradeIds.filter((next) => !ncTradeIds.includes(next));

        for (const tradeId of unknownTrades) {
            issues.push({
                source: `Trade (#${tradeId})`,
                issue: BazaNcOfferingHealthCheckIssue.UnknownTrade,
            });
        }

        for (const trade of ncTrades) {
            const source = `Trade (#${trade.orderId})`;
            const exists = cmsTrades.find((t) => t.ncTradeId === trade.orderId);

            if (exists) {
                if (trade.orderStatus === OrderStatus.Canceled) {
                    issues.push({
                        source,
                        issue: BazaNcOfferingHealthCheckIssue.TradeCancelledButStillInDatabase,
                    });
                } else {
                    if (exists.investorAccount.northCapitalAccountId !== trade.accountId) {
                        issues.push({
                            source,
                            issue: BazaNcOfferingHealthCheckIssue.TradeAssignedToWrongInvestorAccount,
                        });
                    }

                    const sameUnitPrice =
                        convertFromCents(exists.pricePerShareCents).toFixed(BAZA_NC_TRANSACT_API_PRECISION) ===
                        parseFloat(trade.unitPrice).toFixed(BAZA_NC_TRANSACT_API_PRECISION);
                    const sameTotalAmount =
                        convertFromCents(exists.amountCents).toFixed(BAZA_NC_TRANSACT_API_PRECISION) ===
                        parseFloat(trade.totalAmount).toFixed(BAZA_NC_TRANSACT_API_PRECISION);
                    const sameSharesAmount =
                        exists.shares.toFixed(BAZA_NC_TRANSACT_API_PRECISION) ===
                        parseFloat(trade.totalShares).toFixed(BAZA_NC_TRANSACT_API_PRECISION);

                    const isSame = sameUnitPrice && sameTotalAmount && sameSharesAmount;

                    if (!isSame) {
                        issues.push({
                            source,
                            issue: BazaNcOfferingHealthCheckIssue.TradeDataMismatch,
                        });
                    }

                    if (trade.orderStatus !== exists.ncOrderStatus) {
                        issues.push({
                            source,
                            issue: BazaNcOfferingHealthCheckIssue.TradeStatusMismatch,
                        });
                    }
                }
            } else {
                if (trade.orderStatus !== OrderStatus.Canceled) {
                    const existingInvestorAccount = await this.investorAccountRepository.findInvestorAccountByNcAccountId(trade.accountId);

                    if (existingInvestorAccount) {
                        issues.push({
                            source,
                            issue: BazaNcOfferingHealthCheckIssue.MissingTrade,
                        });
                    } else {
                        issues.push({
                            source,
                            issue: BazaNcOfferingHealthCheckIssue.MissingInvestorAccount,
                        });
                    }
                }
            }
        }

        return issues;
    }
}
