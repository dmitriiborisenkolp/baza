import { Injectable } from '@nestjs/common';
import { BazaNcOfferingEntity } from '../../../typeorm';
import { BAZA_NC_TRANSACT_API_PRECISION, bazaNcApiConfig, convertFromCents, convertToCents } from '@scaliolabs/baza-nc-shared';

type SupportedTransactionFeePayloads = Partial<{
    targetAmount: number;
    maxAmount: number;
    minAmount: number;
    unitPrice: number;
}>;

/**
 * Baza NC Transaction Fee Service
 * This service is responsible for updating `targetAmount`, `maxAmount`, `minAmount` and `unitPrice` values of offering
 * to reflect Transaction Fees
 *
 * NOTE: standard solution for quadratic equation could be used for better accuracy as per QA goes around rounding of amount values.
 *
 * quadratic = ax^2 + bx + c = 0
 * solution = -b +- (√b^2 -4ac)/2a
 *
 * as our value for NC percentage of fees will be integer our 'a' will always be 1
 * with this equation will also negate positive value for square as we are calculating amount.
 *
 * hence solution becomes -b +- (√b^2 -4c)/2 but we keep A just for equation consistency.
 * Math.abs((-1 * b - Math.sqrt(Math.abs(Math.pow(b, 2) - (4 * a * c)))) / (2 * a));
 */
@Injectable()
export class BazaNcOfferingTransactionFeesService {
    /**
     * Default Fees for all Offerings
     */
    public DEFAULT_FEES = 0;

    /**
     * Returns True if Transaction Fee feature is Enabled
     */
    public get shouldChargeTransactionFees(): boolean {
        return bazaNcApiConfig().withTransactionFeeFeature;
    }

    /**
     * (Payload version) Appends Transaction Fee numbers to `targetAmount`, `maxAmount`, `minAmount` and `unitPrice` values of offering
     * @param offeringPayload
     * @param fees
     */
    appendOfferingFees(offeringPayload: SupportedTransactionFeePayloads, fees: number = this.DEFAULT_FEES): void {
        offeringPayload.targetAmount = this.percentageIncrementedFees(offeringPayload.targetAmount, fees);
        offeringPayload.maxAmount = this.percentageIncrementedFees(offeringPayload.maxAmount, fees);
        offeringPayload.minAmount = this.percentageIncrementedFees(offeringPayload.minAmount, fees);
        offeringPayload.unitPrice = this.percentageIncrementedFees(offeringPayload.unitPrice, fees);
    }

    /**
     * (Payload version) Removes Transaction Fee numbers from `targetAmount`, `maxAmount`, `minAmount` and `unitPrice` values of offering
     * @param offeringPayload
     * @param fees
     */
    decentOfferingFees(offeringPayload: SupportedTransactionFeePayloads, fees: number = this.DEFAULT_FEES): void {
        offeringPayload.targetAmount = this.percentageDecrementedFees(offeringPayload.targetAmount, fees);
        offeringPayload.maxAmount = this.percentageDecrementedFees(offeringPayload.maxAmount, fees);
        offeringPayload.minAmount = this.percentageDecrementedFees(offeringPayload.minAmount, fees);
        offeringPayload.unitPrice = this.percentageDecrementedFees(offeringPayload.unitPrice, fees);
    }

    /**
     * (Offering version) Appends Transaction Fee numbers to `targetAmount`, `maxAmount`, `minAmount` and `unitPrice` values of offering
     * Numbers will be appended only if `ncOfferingFees` is set
     * @param offering
     */
    appendOfferingEntityFees(offering: BazaNcOfferingEntity): void {
        if (offering.ncOfferingFees) {
            offering.ncTargetAmount = this.percentageIncrementedFees(offering.ncTargetAmount, offering.ncOfferingFees);
            offering.ncMaxAmount = this.percentageIncrementedFees(offering.ncMaxAmount, offering.ncOfferingFees);
            offering.ncMinAmount = this.percentageIncrementedFees(offering.ncMinAmount, offering.ncOfferingFees);
            offering.ncUnitPrice = this.percentageIncrementedFees(offering.ncUnitPrice, offering.ncOfferingFees);
        }
    }

    /**
     * (Offering version) Removes Transaction Fee numbers from `targetAmount`, `maxAmount`, `minAmount` and `unitPrice` values of offering
     * Numbers will be removed only if `ncOfferingFees` is set
     * @param offering
     */
    decentOfferingEntityFees(offering: BazaNcOfferingEntity): void {
        if (offering.ncOfferingFees) {
            offering.ncTargetAmount = this.percentageDecrementedFees(offering.ncTargetAmount, offering.ncOfferingFees);
            offering.ncMaxAmount = this.percentageDecrementedFees(offering.ncMaxAmount, offering.ncOfferingFees);
            offering.ncMinAmount = this.percentageDecrementedFees(offering.ncMinAmount, offering.ncOfferingFees);
            offering.ncUnitPrice = this.percentageDecrementedFees(offering.ncUnitPrice, offering.ncOfferingFees);
        }
    }

    /**
     * Calculate fees (Increment)
     * @param total
     * @param per
     */
    percentageIncrementedFees(total, per: number = this.DEFAULT_FEES) {
        const fees = total + convertFromCents(per) * total;

        return Math.round(Number(fees.toFixed(BAZA_NC_TRANSACT_API_PRECISION)));
    }

    /**
     * Calculates fees (Decrement)
     * @param amount
     * @param fee
     */
    percentageDecrementedFees(amount: number, fee: number = this.DEFAULT_FEES): number {
        const fees = convertToCents(amount) / (fee + 100);

        return Math.round(Number(fees.toFixed(2)));
    }
}
