import { Injectable } from '@nestjs/common';
import { BazaNcOfferingStatusService } from './baza-nc-offering-status.service';
import { BazaNcOfferingsService } from './baza-nc-offerings.service';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import * as moment from 'moment';
import { BazaNcOfferingEntity } from '../../../typeorm';
import { bazaNcApiConfig, BazaNcUiOfferingDto, BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { getBazaProjectCodeName } from '@scaliolabs/baza-core-shared';
import { BazaNcDocusignService, BazaNcDocusignTemplateNotFoundException } from '../../../docusign';
import { BazaNcTransactionRepository } from '../../../typeorm';
import { BazaNcOfferingTransactionFeesService } from './baza-nc-offering-transaction-fees.service';
import { BazaNcOfferingFeesCannotBeUpdatedWithExistingTradeException } from '../exceptions/baza-nc-offering-fees-cannot-be-updated-with-existing-trade.exception';
import { BazaNcOfferingFinancialCannotBeUpdatedWithExistingTradeException } from '../exceptions/baza-nc-offering-financial-cannot-be-updated-with-existing-trade.exception';

export interface NcOfferingSupportedEntity {
    offering: BazaNcOfferingEntity;
}

export interface NcOfferingPopulateOptions {
    offeringName: string;
    offeringDescription: string;
    offeringStampText?: string;
    offeringStatus?: BazaNcOfferingStatus;
}

@Injectable()
export class BazaNcOfferingPopulateService {
    constructor(
        private readonly ncOfferingStatus: BazaNcOfferingStatusService,
        private readonly ncOfferingService: BazaNcOfferingsService,
        private readonly ncDocuSignService: BazaNcDocusignService,
        private readonly transactionsRepository: BazaNcTransactionRepository,
        private readonly ncTransactionFeeService: BazaNcOfferingTransactionFeesService,
    ) {}

    async populateOffering(
        issuer: AccountEntity,
        entity: NcOfferingSupportedEntity,
        request: BazaNcUiOfferingDto,
        options: NcOfferingPopulateOptions,
    ): Promise<void> {
        if (!options.offeringStampText) {
            options.offeringStampText = getBazaProjectCodeName();
        }

        const offeringId = request.ncOfferingId || entity.offering?.ncOfferingId;
        const shouldCreateOffering = !request.ncOfferingId;
        const docuSignTemplates = await this.ncDocuSignService.getAll();

        const ncSubscriptionOffering = docuSignTemplates.find((d) => d.id === request.ncSubscriptionOfferingId);

        if (!ncSubscriptionOffering) {
            throw new BazaNcDocusignTemplateNotFoundException();
        }

        if (shouldCreateOffering) {
            const startDate = new Date(request.ncStartDate);
            const endDate = new Date(request.ncEndDate);

            request.daysToReleaseFailedTrades =
                typeof request.daysToReleaseFailedTrades !== 'number' ? null : request.daysToReleaseFailedTrades;

            entity.offering = await this.ncOfferingService.createOffering(issuer, {
                request: {
                    offeringName: options.offeringName,
                    maxAmount: request.ncMaxAmount,
                    maxSharesPerAccount: request.ncMaxSharesPerAccount,
                    minAmount: request.ncMinAmount,
                    unitPrice: request.ncUnitPrice,
                    targetAmount: request.ncTargetAmount,
                    offeringFees: this.ncTransactionFeeService.shouldChargeTransactionFees
                        ? request.ncOfferingFees
                        : entity.offering?.ncOfferingFees,
                    currentValueCents: request.ncCurrentValueCents,
                    offeringText: options.offeringDescription,
                    stampingText: options.offeringStampText,
                    startDate: moment(startDate).format('MM-DD-YYYY'),
                    endDate: moment(endDate).format('MM-DD-YYYY'),
                    subscriptionOfferingIds: [ncSubscriptionOffering.combinedId],
                    daysToReleaseFailedTrades: request.daysToReleaseFailedTrades,
                },
            });
        } else {
            const ncOffering = await this.ncOfferingService.touchOffering(offeringId);

            await this.validateOfferingFeesUpdate(ncOffering, request);

            const startDate = new Date(request.ncStartDate);
            const endDate = new Date(request.ncEndDate);

            ncOffering.ncStampingText = options.offeringStampText;
            ncOffering.ncOfferingText = options.offeringDescription;
            ncOffering.ncIssueName = options.offeringName;

            request.daysToReleaseFailedTrades =
                typeof request.daysToReleaseFailedTrades !== 'number' ? null : request.daysToReleaseFailedTrades;

            await this.ncOfferingService.updateOffering({
                offeringId: ncOffering.ncOfferingId,
                request: {
                    issuerType: ncOffering.ncIssueType,
                    issuerName: ncOffering.ncIssueName,
                    offeringName: options.offeringName,
                    offeringText: ncOffering.ncOfferingText,
                    stampingText: ncOffering.ncStampingText,
                    maxAmount: request.ncMaxAmount,
                    maxSharesPerAccount: request.ncMaxSharesPerAccount,
                    minAmount: request.ncMinAmount,
                    offeringFees: request.ncOfferingFees,
                    currentValueCents: request.ncCurrentValueCents,
                    unitPrice: request.ncUnitPrice,
                    targetAmount: request.ncTargetAmount,
                    startDate: moment(startDate).format('MM-DD-YYYY'),
                    endDate: moment(endDate).format('MM-DD-YYYY'),
                    subscriptionOfferingIds: [ncSubscriptionOffering.combinedId],
                    daysToReleaseFailedTrades: request.daysToReleaseFailedTrades,
                },
            });

            entity.offering = ncOffering;
        }

        if (options.offeringStatus) {
            await this.ncOfferingStatus.updateStatus(entity.offering.ncOfferingId, options.offeringStatus);
        }

        if (!entity.offering.status) {
            await this.ncOfferingStatus.updateStatus(entity.offering.ncOfferingId, BazaNcOfferingStatus.ComingSoon);
        }

        entity.offering = await this.ncOfferingService.getOffering(entity.offering.ncOfferingId);
    }

    async validateOfferingFeesUpdate(offering: BazaNcOfferingEntity, request: BazaNcUiOfferingDto): Promise<void> {
        if (!bazaNcApiConfig().withTransactionFeeFeature) {
            return;
        }

        const existingTrade = await this.transactionsRepository.repository.findOne({
            where: {
                ncOfferingId: offering.ncOfferingId,
            },
        });

        if (existingTrade) {
            if (this.ncTransactionFeeService.shouldChargeTransactionFees && request.ncOfferingFees !== offering.ncOfferingFees) {
                throw new BazaNcOfferingFeesCannotBeUpdatedWithExistingTradeException();
            }

            const offeringValueUpdated = this.offeringValueUpdated(offering, request);

            if (offeringValueUpdated) {
                throw new BazaNcOfferingFinancialCannotBeUpdatedWithExistingTradeException();
            }
        }
    }

    private offeringValueUpdated(offering: BazaNcOfferingEntity, request: BazaNcUiOfferingDto): boolean {
        const conditions = [
            offering.ncUnitPrice !== request.ncUnitPrice,
            offering.ncTargetAmount !== request.ncTargetAmount,
            offering.ncMaxAmount !== request.ncMaxAmount,
        ];

        return !!conditions.find((c) => c === true);
    }
}
