import 'reflect-metadata';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaE2eFixturesNodeAccess, BazaE2eNodeAccess, BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaNcOfferingCmsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcApiOfferingFixtures } from '../baza-nc-api-offering-fixtures';
import { BazaNcOfferingsErrorCodes } from '@scaliolabs/baza-nc-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/offering/integration-tests/tests/001-baza-nc-api-offering-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessOffering = new BazaNcOfferingCmsNodeAccess(http);

    let NC_OFFERING_ID: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcApiOfferingFixtures.BazaNcOfferingFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will has an offering', async () => {
        const listResponse = await dataAccessOffering.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(1);

        NC_OFFERING_ID = listResponse.items[0].ncOfferingId;
    });

    it('will display offering in list all response', async () => {
        const response = await dataAccessOffering.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(1);
        expect(response[0].ncOfferingId).toBe(NC_OFFERING_ID);
    });

    it('will successfully forget offering', async () => {
        const response = await dataAccessOffering.forgetNcOffering({
            ncOfferingId: NC_OFFERING_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display offering in CMS anymore', async () => {
        const listResponse = await dataAccessOffering.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(0);
    });

    it('will successfully import offering from NC', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessOffering.importNcOffering({
                    ncOfferingId: NC_OFFERING_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.ncOfferingId).toBe(NC_OFFERING_ID);
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will display imported offering', async () => {
        const listResponse = await dataAccessOffering.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();
        expect(listResponse.items.length).toBe(1);

        NC_OFFERING_ID = listResponse.items[0].ncOfferingId;
    });

    it('will not display NC error response for attempt to import unknown offering', async () => {
        const response: BazaError = (await dataAccessOffering.importNcOffering({
            ncOfferingId: '99999999',
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.message.includes('offeringId/clientID does not match')).toBeFalsy();
        expect(response.code).toBe(BazaNcOfferingsErrorCodes.NcOfferingNotFound);
    });
});
