export enum BazaNcApiOfferingFixtures {
    BazaNcOfferingFixture = 'BazaNcOfferingFixture',
    BazaNcOfferingMultipleFixture = 'BazaNcOfferingMultipleFixture',
    BazaNcApiOfferingWithFeeFixture = 'BazaNcApiOfferingWithFeeFixture',
    BazaNcOfferingStatsFixture = 'BazaNcOfferingStatsFixture',
    BazaNcOfferingAllowBuyAllSharesFixture = 'BazaNcOfferingAllowBuyAllSharesFixture',
    BazaNcOfferingAllowBuyAllSharesWithTransactionFeeFixture = 'BazaNcOfferingAllowBuyAllSharesWithTransactionFeeFixture',
    BazaNcOfferingNoAccountLimitFixture = 'BazaNcOfferingNoAccountLimitFixture',
}
