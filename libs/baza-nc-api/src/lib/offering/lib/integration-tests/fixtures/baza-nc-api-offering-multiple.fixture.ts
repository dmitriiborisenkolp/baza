import { Injectable } from '@nestjs/common';
import { NC_OFFERING_E2E, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaCoreAuthUniqueUsersFixture, BazaCoreAuthUsersFixture, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcOfferingsService } from '../../services/baza-nc-offerings.service';
import { BazaNcApiOfferingFixtures } from '../baza-nc-api-offering-fixtures';
import { BazaNcOfferingStatusService } from '../../services/baza-nc-offering-status.service';
import { awaitTimeout } from '@scaliolabs/baza-core-shared';
import { BazaNcOfferingEntity } from '../../../../typeorm';

@Injectable()
export class BazaNcOfferingMultipleFixture implements BazaE2eFixture {
    static NC_OFFERING_ID_1: OfferingId;
    static NC_OFFERING_ENTITY_1: BazaNcOfferingEntity;

    static NC_OFFERING_ID_2: OfferingId;
    static NC_OFFERING_ENTITY_2: BazaNcOfferingEntity;

    constructor(
        private readonly e2eUserFixture: BazaCoreAuthUsersFixture,
        private readonly ncOfferingService: BazaNcOfferingsService,
        private readonly ncOfferingStatus: BazaNcOfferingStatusService,
        private readonly e2eUniqueUserFixture: BazaCoreAuthUniqueUsersFixture,
    ) {}

    async up<E2eNcOfferingFixtures>(): Promise<void> {
        BazaNcOfferingMultipleFixture.NC_OFFERING_ENTITY_1 = await this.factory('E2E NC Offering Fixture ALPHA');
        BazaNcOfferingMultipleFixture.NC_OFFERING_ENTITY_2 = await this.factory('E2E NC Offering Fixture BETA');

        BazaNcOfferingMultipleFixture.NC_OFFERING_ID_1 = BazaNcOfferingMultipleFixture.NC_OFFERING_ENTITY_1.ncOfferingId;
        BazaNcOfferingMultipleFixture.NC_OFFERING_ID_2 = BazaNcOfferingMultipleFixture.NC_OFFERING_ENTITY_2.ncOfferingId;

        await awaitTimeout(3000);
    }

    private async factory(offeringName: string): Promise<BazaNcOfferingEntity> {
        const account = this.e2eUserFixture.e2eUser || this.e2eUniqueUserFixture.e2eUser;

        return this.ncOfferingService.createOffering(account, {
            request: {
                offeringName: offeringName,
                targetAmount: 10000,
                maxAmount: 10000,
                currentValueCents: 1000000,
                unitPrice: 10,
                minAmount: 10,
                maxSharesPerAccount: 100,
                startDate: '01-01-2021',
                endDate: '01-01-2021',
                offeringText: 'Example E2E NC Offering Fixture',
                stampingText: 'E2E',
                updatedIPaddress: '0.0.0.0',
                subscriptionOfferingIds: NC_OFFERING_E2E.docuSignExampleTemplateIds,
                offeringFees: 1,
            },
        });
    }
}

defineE2eFixtures<BazaNcApiOfferingFixtures>([
    {
        fixture: BazaNcApiOfferingFixtures.BazaNcOfferingMultipleFixture,
        provider: BazaNcOfferingMultipleFixture,
    },
]);
