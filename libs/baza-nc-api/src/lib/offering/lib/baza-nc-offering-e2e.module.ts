import { forwardRef, Module } from '@nestjs/common';
import { BazaNcOfferingApiModule } from './baza-nc-offering-api.module';
import { BazaNcOfferingFixture } from './integration-tests/fixtures/baza-nc-api-offering.fixture';
import { BazaNcOfferingAllowBuyAllSharesFixture } from './integration-tests/fixtures/baza-nc-offering-allow-buy-all-shares.fixture';
import { BazaNcOfferingAllowBuyAllSharesWithTransactionFeeFixture } from './integration-tests/fixtures/baza-nc-offering-allow-buy-all-shares-with-transaction-fee.fixture';
import { BazaNcOfferingNoAccountLimitFixture } from './integration-tests/fixtures/baza-nc-offering-no-account-limit.fixture';
import { BazaApiBundleModule, BazaE2eFixturesApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcApiOfferingStatsFixture } from './integration-tests/fixtures/baza-nc-api-offering-stats.fixture';
import { BazaNcApiOfferingWithFeeFixture } from './integration-tests/fixtures/baza-nc-api-offering-with-fee.fixture';
import { BazaNcOfferingMultipleFixture } from './integration-tests/fixtures/baza-nc-api-offering-multiple.fixture';

const E2E_FIXTURES = [
    BazaNcOfferingFixture,
    BazaNcOfferingAllowBuyAllSharesFixture,
    BazaNcOfferingAllowBuyAllSharesWithTransactionFeeFixture,
    BazaNcOfferingNoAccountLimitFixture,
    BazaNcApiOfferingStatsFixture,
    BazaNcApiOfferingWithFeeFixture,
    BazaNcOfferingMultipleFixture,
];

@Module({
    imports: [
        // TODO: Remove it after baza-core e2e modules migration
        forwardRef(() => BazaApiBundleModule),
        forwardRef(() => BazaE2eFixturesApiModule),
        forwardRef(() => BazaNcOfferingApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcOfferingE2eModule {}
