import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import {
    BazaNcOfferingListItemDto,
    BazaNorthCapitalAcl,
    BazaNorthCapitalCMSOpenApi,
    NcOfferingForgetRequest,
    NcOfferingHealthCheckRequest,
    NcOfferingHealthCheckResponse,
    NcOfferingImportRequest,
} from '@scaliolabs/baza-nc-shared';
import {
    BazaNcOfferingCmsEndpoint,
    BazaNcOfferingCmsEndpointPaths,
    BazaNcOfferingDetailsDto,
    NcOfferingDetailsRequest,
    NcOfferingDetailsResponse,
    BazaNcOfferingDto,
    NcOfferingGetByIdRequest,
    NcOfferingGetByIdResponse,
    NcOfferingListRequest,
    NcOfferingListResponse,
    NcOfferingSyncAllOfferingsResponse,
} from '@scaliolabs/baza-nc-shared';
import { WithAccessGuard } from '@scaliolabs/baza-core-api';
import { AclNodes } from '@scaliolabs/baza-core-api';
import { BazaNcOfferingsService } from '../services/baza-nc-offerings.service';
import { BazaNcOfferingMapper } from '../mappers/baza-nc-offering.mapper';
import { BazaNcOfferingHealthCheckService } from '../services/baza-nc-offering-health-check.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalOfferingCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNorthCapitalAcl.BazaNcOfferings])
export class BazaNcOfferingCmsController implements BazaNcOfferingCmsEndpoint {
    constructor(
        private readonly service: BazaNcOfferingsService,
        private readonly mapper: BazaNcOfferingMapper,
        private readonly healthCheckService: BazaNcOfferingHealthCheckService,
    ) {}

    @Post(BazaNcOfferingCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of offerings',
    })
    @ApiOkResponse({
        type: NcOfferingListResponse,
    })
    async list(@Body() request: NcOfferingListRequest): Promise<NcOfferingListResponse> {
        return this.service.listOfferings(request);
    }

    @Get(BazaNcOfferingCmsEndpointPaths.listAll)
    @ApiOperation({
        summary: 'listAll',
        description: 'Returns all offerings',
    })
    @ApiOkResponse({
        type: BazaNcOfferingListItemDto,
        isArray: true,
    })
    listAll(): Promise<Array<BazaNcOfferingListItemDto>> {
        return this.service.listAllOfferings();
    }

    @Post(BazaNcOfferingCmsEndpointPaths.getByNcOfferingId)
    @ApiOperation({
        summary: 'getByNcOfferingId',
        description: 'Returns offering by ncOfferingId',
    })
    @ApiOkResponse({
        type: BazaNcOfferingDto,
    })
    async getByNcOfferingId(@Body() request: NcOfferingGetByIdRequest): Promise<NcOfferingGetByIdResponse> {
        const offering = await this.service.getOffering(request.ncOfferingId);

        return this.mapper.entityToDTO(offering);
    }

    @Post(BazaNcOfferingCmsEndpointPaths.ncOfferingDetails)
    @ApiOperation({
        summary: 'ncOfferingDetails',
        description: 'Returns offering details directly from NC',
    })
    @ApiOkResponse({
        type: BazaNcOfferingDetailsDto,
    })
    async ncOfferingDetails(@Body() request: NcOfferingDetailsRequest): Promise<NcOfferingDetailsResponse> {
        return this.service.ncOfferingDetails(request);
    }

    @Post(BazaNcOfferingCmsEndpointPaths.syncAllOfferings)
    @ApiOperation({
        summary: 'syncAllOfferings',
        description: 'Sync all existing offerings',
    })
    @ApiOkResponse()
    async syncAllOfferings(): Promise<NcOfferingSyncAllOfferingsResponse> {
        return this.service.syncAllNcOfferings();
    }

    @Post(BazaNcOfferingCmsEndpointPaths.importNcOffering)
    @ApiOperation({
        summary: 'importNcOffering',
        description: 'Import Offering from NC',
    })
    @ApiOkResponse()
    async importNcOffering(@Body() request: NcOfferingImportRequest): Promise<BazaNcOfferingDto> {
        const offering = await this.service.syncWithNcOffering(request.ncOfferingId);

        return this.mapper.entityToDTO(offering);
    }

    @Post(BazaNcOfferingCmsEndpointPaths.forgetNcOffering)
    @ApiOperation({
        summary: 'forgetNcOffering',
        description: 'Forget about offering (remove from DB but keep offering on NC side)',
    })
    @ApiOkResponse()
    async forgetNcOffering(@Body() request: NcOfferingForgetRequest): Promise<void> {
        await this.service.forgetOffering({
            request: {
                offeringId: request.ncOfferingId,
            },
        });
    }

    @Post(BazaNcOfferingCmsEndpointPaths.healthCheck)
    @ApiOperation({
        summary: 'healthCheck',
        description: 'Compares offering with actual data from NC and returns health status and list of issues',
    })
    @ApiOkResponse({
        type: NcOfferingHealthCheckResponse,
    })
    async healthCheck(@Body() request: NcOfferingHealthCheckRequest): Promise<NcOfferingHealthCheckResponse> {
        return this.healthCheckService.healthCheck(request);
    }
}
