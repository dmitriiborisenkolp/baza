import { forwardRef, Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BazaNcOfferingsService } from './services/baza-nc-offerings.service';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaLoggerModule } from '@scaliolabs/baza-core-api';
import { BazaNcOfferingMapper } from './mappers/baza-nc-offering.mapper';
import { BazaNcOfferingCmsController } from './controllers/baza-nc-offering-cms.controller';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcOfferingsPercentsFundedService } from './services/baza-nc-offerings-percents-funded.service';
import { BazaNcOfferingStatusService } from './services/baza-nc-offering-status.service';
import { BazaNcWebhookApiModule } from '../../webhook';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { UpdatePercentsFundedEventHandler } from './event-handlers/update-percents-funded.event-handler';
import { UpdateOfferingEventHandler } from './event-handlers/update-offering.event-handler';
import { BazaNcOfferingPopulateService } from './services/baza-nc-offering-populate.service';
import { BazaNcDocusignApiModule } from '../../docusign';
import { BazaNcOfferingHealthCheckService } from './services/baza-nc-offering-health-check.service';
import { BazaNcOfferingTransactionFeesService } from './services/baza-nc-offering-transaction-fees.service';
import { BazaNcOfferingListItemMapper } from './mappers/baza-nc-offering-list-item.mapper';
import { BazaNcIssuerApiModule } from '../../issuer';

const EVENT_HANDLERS = [UpdatePercentsFundedEventHandler, UpdateOfferingEventHandler];

@Global()
@Module({
    imports: [
        BazaLoggerModule,
        TypeOrmModule,
        CqrsModule,
        BazaNcWebhookApiModule,
        BazaCrudApiModule,
        BazaNcTypeormApiModule,
        BazaNcIssuerApiModule,
        forwardRef(() => BazaNcDocusignApiModule),
    ],
    controllers: [BazaNcOfferingCmsController],
    providers: [
        BazaNcOfferingsService,
        BazaNcOfferingMapper,
        BazaNcOfferingListItemMapper,
        BazaNcOfferingsPercentsFundedService,
        BazaNcOfferingStatusService,
        BazaNcOfferingPopulateService,
        BazaNcOfferingHealthCheckService,
        BazaNcOfferingTransactionFeesService,
        ...EVENT_HANDLERS,
    ],
    exports: [
        BazaNcOfferingsService,
        BazaNcOfferingMapper,
        BazaNcOfferingListItemMapper,
        BazaNcOfferingsPercentsFundedService,
        BazaNcOfferingStatusService,
        BazaNcOfferingPopulateService,
        BazaNcOfferingTransactionFeesService,
    ],
})
export class BazaNcOfferingApiModule {}
