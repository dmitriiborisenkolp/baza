import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalOfferingsErrorCodesI18n, BazaNcOfferingsErrorCodes } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcOfferingCannotBeCreatedWithGivenParamsException extends BazaAppException {
    constructor() {
        super(
            BazaNcOfferingsErrorCodes.NcOfferingCannotBeCreatedWithPassedParams,
            BazaNorthCapitalOfferingsErrorCodesI18n[BazaNcOfferingsErrorCodes.NcOfferingCannotBeCreatedWithPassedParams],
            HttpStatus.BAD_REQUEST,
        );
    }
}
