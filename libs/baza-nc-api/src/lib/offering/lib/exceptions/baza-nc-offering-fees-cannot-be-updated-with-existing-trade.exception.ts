import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalOfferingsErrorCodesI18n, BazaNcOfferingsErrorCodes } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcOfferingFeesCannotBeUpdatedWithExistingTradeException extends BazaAppException {
    constructor() {
        super(
            BazaNcOfferingsErrorCodes.NcOfferingFeesCannotBeUpdatedWithExistingTrade,
            BazaNorthCapitalOfferingsErrorCodesI18n[BazaNcOfferingsErrorCodes.NcOfferingFeesCannotBeUpdatedWithExistingTrade],
            HttpStatus.BAD_REQUEST,
        );
    }
}
