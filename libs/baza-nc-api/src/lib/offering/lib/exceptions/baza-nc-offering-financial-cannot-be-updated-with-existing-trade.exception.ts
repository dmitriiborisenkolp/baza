import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNorthCapitalOfferingsErrorCodesI18n, BazaNcOfferingsErrorCodes } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcOfferingFinancialCannotBeUpdatedWithExistingTradeException extends BazaAppException {
    constructor() {
        super(
            BazaNcOfferingsErrorCodes.NcOfferingFinancialCannotBeUpdatedWithExistingTrade,
            BazaNorthCapitalOfferingsErrorCodesI18n[BazaNcOfferingsErrorCodes.NcOfferingFinancialCannotBeUpdatedWithExistingTrade],
            HttpStatus.BAD_REQUEST,
        );
    }
}
