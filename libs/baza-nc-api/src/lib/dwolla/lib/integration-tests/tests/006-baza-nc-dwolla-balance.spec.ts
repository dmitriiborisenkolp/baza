import 'reflect-metadata';
import {
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
    BazaDataAccessNode,
    BazaAccountCmsNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { AccountRole, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcDwollaNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { Domicile } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dwolla/integration-tests/006-baza-nc-dwolla-balance.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcDwollaNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let USER_EMAIL: string;

    const authUniqueUser = async () => {
        await http.auth({
            email: USER_EMAIL,
            password: 'e2e-user-password',
        });
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
        });

        USER_EMAIL = accounts.items[0].email;
    });

    beforeEach(async () => {
        await authUniqueUser();
    });

    it('will allow to call balance endpoint when Account Verification is not processed yet and Dwolla Balance is not available yet', async () => {
        const response = await dataAccess.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isDwollaAvailable).toBeFalsy();
        expect(response.balance).toBeDefined();
        expect(response.balance.currency).toBe('USD');
        expect(response.balance.value).toBe('0.00');
    });

    it('will set up Investor as Foreign', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'DOE',
            dateOfBirth: '01-01-1990',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: 'FOREIGN ADDRESS',
            residentialStreetAddress2: '',
            residentialCity: 'PERM',
            residentialState: 'PERM REGION',
            residentialZipCode: '127000',
            residentialCountry: 'Russia',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to call balance endpoint when Account Verification as Foreign investor and Dwolla Balance is not available yet', async () => {
        const response = await dataAccess.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isDwollaAvailable).toBeFalsy();
        expect(response.balance).toBeDefined();
        expect(response.balance.currency).toBe('USD');
        expect(response.balance.value).toBe('0.00');
    });

    it('will set up Investor as US', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to call balance endpoint when Dwolla Balance is not available yet', async () => {
        const response = await dataAccess.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isDwollaAvailable).toBeFalsy();
        expect(response.balance).toBeDefined();
        expect(response.balance.currency).toBe('USD');
        expect(response.balance.value).toBe('0.00');
    });

    it('will allow to touch Dwolla Customer', async () => {
        const response = await dataAccess.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will allow to call balance endpoint when Dwolla Balance is available', async () => {
        const response = await dataAccess.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isDwollaAvailable).toBeTruthy();
        expect(response.balance).toBeDefined();
        expect(response.balance.currency).toBe('USD');
        expect(response.balance.value).toBe('0.00');
    });
});
