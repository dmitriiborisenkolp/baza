import 'reflect-metadata';
import {
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
    BazaDataAccessNode,
    BazaAccountCmsNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcAccountVerificationNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { DwollaCurrency, DwollaCustomerId } from '@scaliolabs/baza-dwolla-shared';
import { ulid } from 'ulid';
import { BazaNcDwollaTouchResult, Domicile } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dwolla/integration-tests/001-baza-nc-dwolla.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcDwollaNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let dwollaCustomerId: DwollaCustomerId;

    const email = `e2e-${ulid()}@scal.io`;
    const password = 'Scalio#14544!';

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will create a new e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email,
            password,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will returns current info even without account verification passed', async () => {
        await http.auth({
            email,
            password,
        });

        const response = await dataAccess.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isDwollaAvailable).toBeFalsy();
        expect(response.balance).toBeDefined();
        expect(response.balance.currency).toBe(DwollaCurrency.USD);
        expect(response.balance.value).toBe('0.00');
    });

    it('will pass account verification for new user', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it(`will return current info and dwolla shouldn't be available`, async () => {
        const response = await dataAccess.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isDwollaAvailable).toBeFalsy();
    });

    it('will touch dwolla customer for existing investor', async () => {
        const response = await dataAccess.touch();
        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect([BazaNcDwollaTouchResult.Created, BazaNcDwollaTouchResult.Exists]).toContain(response.result);
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
        expect(response.failedReason).not.toBeDefined();
    });

    it('will display that dwolla is attached to investor', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.dwollaCustomerId).toBeDefined();

        dwollaCustomerId = response.dwollaCustomerId;
    });

    it('will touch dwolla customer for existing investor (2)', async () => {
        const response = await dataAccess.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that same dwolla customer is attached', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.dwollaCustomerId).toBeDefined();
        expect(response.dwollaCustomerId).toBe(dwollaCustomerId);
    });

    it('will display balance of dwolla customer', async () => {
        const response = await dataAccess.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isDwollaAvailable).toBeTruthy();
        expect(response.balance).toBeDefined();
        expect(response.balance.currency).toBe(DwollaCurrency.USD);
    });
});
