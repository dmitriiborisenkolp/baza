import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcAccountVerificationNodeAccess,
    BazaNcDwollaCmsNodeAccess,
    BazaNcInvestorAccountCmsNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcDwollaTouchResult, Domicile } from '@scaliolabs/baza-nc-shared';
import { ulid } from 'ulid';
import { CreateDocumentRequest, DwollaCustomerStatus, DwollaDocumentType, DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaE2eNodeAccess } from '@scaliolabs/baza-dwolla-node-access';

import * as fs from 'fs';
import * as path from 'path';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/dwolla/integration-tests/005-baza-nc-dwolla-retry-verification-by-document-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDwollaCms = new BazaNcDwollaCmsNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessInvestorAccountCms = new BazaNcInvestorAccountCmsNodeAccess(http);
    const dataAccessDwollaE2e = new BazaDwollaE2eNodeAccess(http);

    let USER_ID: number;
    let DWOLLA_CUSTOMER_ID: string;

    const email = `e2e-${ulid()}@scal.io`;
    const password = 'Scalio#14544!';

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will create a new e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email,
            password,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        USER_ID = response.id;

        await http.auth({
            email,
            password,
        });
    });

    it('will perform account verification for new user valid SSN and Data', async () => {
        await http.auth({ email, password });

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: DwollaCustomerStatus.Document,
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and it will process it', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaCms.touch({ userId: USER_ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect([BazaNcDwollaTouchResult.Created, BazaNcDwollaTouchResult.Exists]).toContain(response.result);
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
        expect(response.isDwollaAvailable).toBeFalsy();
    });

    it('will try to get Dwolla Customer verification status', async () => {
        await http.auth({ email, password });

        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.dwollaCustomerVerificationStatus).toBe(DwollaCustomerStatus.Document);
        DWOLLA_CUSTOMER_ID = response.dwollaCustomerId;
    });

    it('will try to retry Dwolla Customer verification by sending invalid document', async () => {
        await http.authE2eAdmin();

        const document: CreateDocumentRequest = {
            documentType: DwollaDocumentType.IdCard,
            userId: USER_ID,
        };

        const response = await dataAccessDwollaCms.createDocumentForCustomer(
            fs.createReadStream(path.join(__dirname, '../files/test-document-upload-fail.png')),
            document,
        );

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investorAccountResponse = await dataAccessInvestorAccountCms.getInvestorAccountByUserId({
            userId: USER_ID,
        });

        expect(isBazaErrorResponse(investorAccountResponse)).toBeFalsy();
        expect(investorAccountResponse.dwollaCustomerVerificationStatus).toBe(DwollaCustomerStatus.DocumentProcessing);
    });

    it('will send simulated Dwolla webhook with customer_verification_document_failed topic', async () => {
        const response = await dataAccessDwollaE2e.simulateWebhookEvent({
            topic: DwollaEventTopic.customer_verification_document_failed,
            id: DWOLLA_CUSTOMER_ID,
            resourceId: DWOLLA_CUSTOMER_ID,
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to verify Dwolla Customer status has been stayed as "document"', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessInvestorAccountCms.getInvestorAccountByUserId({
                    userId: USER_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.dwollaCustomerVerificationStatus).toBe(DwollaCustomerStatus.Document);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will try to retry Dwolla Customer verification by sending valid document', async () => {
        await http.authE2eAdmin();

        const document: CreateDocumentRequest = {
            documentType: DwollaDocumentType.IdCard,
            userId: USER_ID,
        };

        const response = await dataAccessDwollaCms.createDocumentForCustomer(
            fs.createReadStream(path.join(__dirname, '../files/test-document-upload-success.png')),
            document,
        );

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const investorAccountResponse = await dataAccessInvestorAccountCms.getInvestorAccountByUserId({
            userId: USER_ID,
        });

        expect(isBazaErrorResponse(investorAccountResponse)).toBeFalsy();
        expect(investorAccountResponse.dwollaCustomerVerificationStatus).toBe(DwollaCustomerStatus.DocumentProcessing);
    });

    it('will send simulated Dwolla webhook with customer_verification_document_approved topic', async () => {
        const response = await dataAccessDwollaE2e.simulateWebhookEvent({
            topic: DwollaEventTopic.customer_verification_document_approved,
            id: DWOLLA_CUSTOMER_ID,
            resourceId: DWOLLA_CUSTOMER_ID,
            created: new Date().toISOString(),
            timestamp: new Date().toISOString(),
            _links: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to verify Dwolla Customer status has been changed to "veriified"', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessInvestorAccountCms.getInvestorAccountByUserId({
                    userId: USER_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.dwollaCustomerVerificationStatus).toBe(DwollaCustomerStatus.Verified);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will try to touch Dwolla Customer and it will process it', async () => {
        await http.authE2eAdmin();

        await asyncExpect(async () => {
            const response = await dataAccessDwollaCms.touch({ userId: USER_ID });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect([BazaNcDwollaTouchResult.Created, BazaNcDwollaTouchResult.Exists]).toContain(response.result);
            expect(response.isDwollaCustomerAvailable).toBeTruthy();
            expect(response.isDwollaAvailable).toBeTruthy();
        });
    });
});
