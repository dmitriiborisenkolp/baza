import 'reflect-metadata';
import {
    BazaAccountCmsNodeAccess,
    BazaDataAccessNode,
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationNodeAccess, BazaNcDwollaCmsNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcDwollaTouchResult, Domicile } from '@scaliolabs/baza-nc-shared';
import { ulid } from 'ulid';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-api/dwolla/integration-tests/003-baza-nc-dwolla-touch-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessDwollaCms = new BazaNcDwollaCmsNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);

    let USER_ID: number;

    const email = `e2e-${ulid()}@scal.io`;
    const password = 'Scalio#14544!';

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will create a new e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email,
            password,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        USER_ID = response.id;

        await http.auth({
            email,
            password,
        });
    });

    it('will try to touch Dwolla Customer and will not process it', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaCms.touch({ userId: USER_ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.Failed);
        expect(response.isDwollaCustomerAvailable).toBeFalsy();
        expect(response.isDwollaAvailable).toBeFalsy();
    });

    it('will perform account verification with invalid SSN', async () => {
        await http.auth({ email, password });

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'FOO',
            lastName: 'BAR',
            dateOfBirth: '02-28-1999',
            hasSsn: true,
            ssn: '000-00-0000',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: 'UNKNOWN ADDRESS',
            residentialStreetAddress2: '1',
            residentialCity: 'TEXAS',
            residentialState: 'TX',
            residentialZipCode: '30034',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and will not process it', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaCms.touch({ userId: USER_ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.NonUsResident);
        expect(response.isDwollaCustomerAvailable).toBeFalsy();
        expect(response.isDwollaAvailable).toBeFalsy();
    });

    it('will perform account verification for new user without SSN', async () => {
        await http.auth({ email, password });

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: false,
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and will not process it (2)', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaCms.touch({ userId: USER_ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.NonUsResident);
        expect(response.isDwollaCustomerAvailable).toBeFalsy();
        expect(response.isDwollaAvailable).toBeFalsy();
    });

    it('will update account verification once more with valid SSN and Data', async () => {
        await http.auth({ email, password });

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and it will process it', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaCms.touch({ userId: USER_ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect([BazaNcDwollaTouchResult.Created, BazaNcDwollaTouchResult.Exists]).toContain(response.result);
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });

    it('will re-perform account verification with invalid SSN and invalid data', async () => {
        await http.auth({ email, password });

        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'FOO',
            lastName: 'BAR',
            dateOfBirth: '02-28-1999',
            hasSsn: true,
            ssn: '000-00-0000',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: 'UNKNOWN ADDRESS',
            residentialStreetAddress2: '1',
            residentialCity: 'TEXAS',
            residentialState: 'TX',
            residentialZipCode: '30034',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and it will still display previous Dwolla Customer account', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessDwollaCms.touch({ userId: USER_ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.Exists);
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });
});
