import 'reflect-metadata';
import {
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
    BazaDataAccessNode,
    BazaAccountCmsNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcAccountVerificationNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { DwollaCustomerId, DwollaCustomerStatus } from '@scaliolabs/baza-dwolla-shared';
import { ulid } from 'ulid';
import { BazaNcDwollaTouchResult, Domicile } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dwolla/integration-tests/002-baza-nc-dwolla-touch.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcDwollaNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let dwollaCustomerId: DwollaCustomerId;

    const email = `e2e-${ulid()}@scal.io`;
    const password = 'Scalio#14544!';

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    it('will create a new e2e user', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessAccountCms.registerUserAccount({
            email,
            password,
            firstName: 'John',
            lastName: 'Smith',
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await http.auth({
            email,
            password,
        });
    });

    it('will try to touch Dwolla Customer and will not process it', async () => {
        const response = await dataAccess.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.Failed);
        expect(response.isDwollaCustomerAvailable).toBeFalsy();
        expect(response.isDwollaAvailable).toBeFalsy();
    });

    it('will perform account verification with invalid SSN', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'FOO',
            lastName: 'BAR',
            dateOfBirth: '02-28-1999',
            hasSsn: true,
            ssn: '000-00-0000',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: 'UNKNOWN ADDRESS',
            residentialStreetAddress2: '1',
            residentialCity: 'TEXAS',
            residentialState: 'TX',
            residentialZipCode: '30034',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and will not process it', async () => {
        const response = await dataAccess.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.NonUsResident);
        expect(response.isDwollaCustomerAvailable).toBeFalsy();
        expect(response.isDwollaAvailable).toBeFalsy();
    });

    it('will perform account verification for new user without SSN', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: false,
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and will not process it (2)', async () => {
        const response = await dataAccess.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.NonUsResident);
        expect(response.isDwollaCustomerAvailable).toBeFalsy();
        expect(response.isDwollaAvailable).toBeFalsy();
    });

    it('will update account verification once more with valid SSN and Data', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'GA',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and it will process it', async () => {
        const response = await dataAccess.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect([BazaNcDwollaTouchResult.Created, BazaNcDwollaTouchResult.Exists]).toContain(response.result);
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });

    it('will re-perform account verification with invalid SSN and invalid data', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'FOO',
            lastName: 'BAR',
            dateOfBirth: '02-28-1999',
            hasSsn: true,
            ssn: '000-00-0000',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: 'UNKNOWN ADDRESS',
            residentialStreetAddress2: '1',
            residentialCity: 'TEXAS',
            residentialState: 'TX',
            residentialZipCode: '30034',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will try to touch Dwolla Customer and it will still display previous Dwolla Customer account', async () => {
        const response = await dataAccess.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.Exists);
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
    });

    it('investor account dwolla customer will have verified status', async () => {
        const currentInvestorAccount = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(currentInvestorAccount)).toBeFalsy();
        expect(currentInvestorAccount.dwollaCustomerVerificationStatus).toBe(DwollaCustomerStatus.Verified);
    });
});
