import 'reflect-metadata';
import {
    BazaE2eFixturesNodeAccess,
    BazaE2eNodeAccess,
    BazaDataAccessNode,
    BazaAccountCmsNodeAccess,
} from '@scaliolabs/baza-core-node-access';
import { AccountRole, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcAccountVerificationNodeAccess,
    BazaNcDwollaNodeAccess,
    BazaNcInvestorAccountNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcDwollaTouchResult, BazaNcKycStatus, Domicile, KYCStatus } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-api/dwolla/integration-tests/007-baza-nc-dwolla-us-pr-states.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcDwollaNodeAccess(http);
    const dataAccessAccountVerification = new BazaNcAccountVerificationNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessAccountCms = new BazaAccountCmsNodeAccess(http);

    let USER_EMAIL: string;

    const authUniqueUser = async () => {
        await http.auth({
            email: USER_EMAIL,
            password: 'e2e-user-password',
        });
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUniqueUser],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        const accounts = await dataAccessAccountCms.listAccounts({
            roles: [AccountRole.User],
        });

        USER_EMAIL = accounts.items[0].email;
    });

    beforeEach(async () => {
        await authUniqueUser();
    });

    it('will pass account verification as NC Approved', async () => {
        const response = await dataAccessAccountVerification.applyPersonalInformation({
            phone: '2292201647',
            phoneCountryCode: '1',
            phoneCountryNumericCode: '14',
            firstName: 'JOHN',
            lastName: 'SMITH',
            dateOfBirth: '02-28-1975',
            hasSsn: true,
            ssn: '112-22-3333',
            citizenship: Domicile.USCitizen,
            residentialStreetAddress1: '222333 PEACHTREE PLACE',
            residentialStreetAddress2: '1414',
            residentialCity: 'ATLANTA',
            residentialState: 'PR',
            residentialZipCode: '30033',
            residentialCountry: 'USA',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.personalInformation.residentialCountry).toBe('USA');
        expect(response.personalInformation.residentialState).toBe('PR');
        expect(response.kycStatus).toBe(KYCStatus.AutoApproved);
    });

    it('will display that investor is not considered as Foreign', async () => {
        const response = await dataAccessInvestorAccount.current();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isInvestorVerified).toBeTruthy();
        expect(response.status.isForeign).toBeFalsy();
        expect(response.status.nc).toBe(BazaNcKycStatus.Approved);
    });

    it('will allow to touch Dwolla account', async () => {
        const response = await dataAccess.touch();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.result).toBe(BazaNcDwollaTouchResult.Created);
        expect(response.isDwollaAvailable).toBeTruthy();
        expect(response.isDwollaCustomerAvailable).toBeTruthy();
        expect(response.failedReason).not.toBeDefined();
    });
});
