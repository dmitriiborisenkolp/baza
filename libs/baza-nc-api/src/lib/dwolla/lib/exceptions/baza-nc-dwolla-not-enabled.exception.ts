import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDwollaErrorCodes, bazaNcDwollaErrorCodesEnI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDwollaNotEnabledException extends BazaAppException {
    constructor() {
        super(
            BazaNcDwollaErrorCodes.BazaNcDwollaNotEnabled,
            bazaNcDwollaErrorCodesEnI18n[BazaNcDwollaErrorCodes.BazaNcDwollaNotEnabled],
            HttpStatus.BAD_REQUEST,
        );
    }
}
