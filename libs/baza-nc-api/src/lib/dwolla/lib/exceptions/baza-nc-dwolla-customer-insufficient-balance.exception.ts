import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDwollaErrorCodes, bazaNcDwollaErrorCodesEnI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDwollaCustomerInsufficientBalanceException extends BazaAppException {
    constructor() {
        super(
            BazaNcDwollaErrorCodes.BazaNcDwollaCustomerInsufficientBalance,
            bazaNcDwollaErrorCodesEnI18n[BazaNcDwollaErrorCodes.BazaNcDwollaCustomerInsufficientBalance],
            HttpStatus.BAD_REQUEST,
        );
    }
}
