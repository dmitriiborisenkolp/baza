import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDwollaErrorCodes, bazaNcDwollaErrorCodesEnI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDwollaCustomerFundingSourceNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcDwollaErrorCodes.BazaNcDwollaCustomerFundingSourceNotFound,
            bazaNcDwollaErrorCodesEnI18n[BazaNcDwollaErrorCodes.BazaNcDwollaCustomerFundingSourceNotFound],
            HttpStatus.BAD_REQUEST,
        );
    }
}
