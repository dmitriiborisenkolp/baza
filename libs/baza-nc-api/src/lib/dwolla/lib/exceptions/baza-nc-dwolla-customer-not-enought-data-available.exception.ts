import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDwollaErrorCodes, bazaNcDwollaErrorCodesEnI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDwollaCustomerNotEnoughAvailableException extends BazaAppException {
    constructor() {
        super(
            BazaNcDwollaErrorCodes.BazaNcDwollaCustomerNotEnoughDataToUpdate,
            bazaNcDwollaErrorCodesEnI18n[BazaNcDwollaErrorCodes.BazaNcDwollaCustomerNotEnoughDataToUpdate],
            HttpStatus.BAD_REQUEST,
        );
    }
}
