import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcDwollaErrorCodes, bazaNcDwollaErrorCodesEnI18n } from '@scaliolabs/baza-nc-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcDwollaCustomerIsNotAvailableException extends BazaAppException {
    constructor() {
        super(
            BazaNcDwollaErrorCodes.BazaNcDwollaCustomerIsNotAvailable,
            bazaNcDwollaErrorCodesEnI18n[BazaNcDwollaErrorCodes.BazaNcDwollaCustomerIsNotAvailable],
            HttpStatus.BAD_REQUEST,
        );
    }
}
