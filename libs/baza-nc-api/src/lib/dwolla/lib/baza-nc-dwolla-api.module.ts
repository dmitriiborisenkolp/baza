import { forwardRef, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaDwollaApiModule } from '@scaliolabs/baza-dwolla-api';
import { BazaNcInvestorAccountApiModule } from '../../investor-account';
import { BazaNcTypeormApiModule } from '../../typeorm';
import { BazaNcDwollaCustomerCmsController } from './controllers/baza-nc-dwolla-cms.controller';
import { BazaNcDwollaCustomerController } from './controllers/baza-nc-dwolla.controller';
import { CustomerSuspendedEventHandler } from './event-handlers/customer-suspended.event-handler';
import { CustomerVerificationDocumentApprovedEventHandler } from './event-handlers/customer-verification-document-approved.event-handler';
import { CustomerVerificationDocumentFailedEventHandler } from './event-handlers/customer-verification-document-failed.event-handler';
import { CustomerVerifiedEventHandler } from './event-handlers/customer-verified.event-handler';
import { BazaNcDwollaCustomerService } from './services/baza-nc-dwolla-customer.service';
import { BazaNcDwollaTransferService } from './services/baza-nc-dwolla-transfer.service';

const EVENT_HANDLERS = [
    CustomerSuspendedEventHandler,
    CustomerVerificationDocumentApprovedEventHandler,
    CustomerVerificationDocumentFailedEventHandler,
    CustomerVerifiedEventHandler,
];

@Module({
    controllers: [BazaNcDwollaCustomerController, BazaNcDwollaCustomerCmsController],
    imports: [CqrsModule, BazaNcTypeormApiModule, forwardRef(() => BazaNcInvestorAccountApiModule), BazaDwollaApiModule],
    providers: [BazaNcDwollaCustomerService, BazaNcDwollaTransferService, ...EVENT_HANDLERS],
    exports: [BazaNcDwollaCustomerService, BazaNcDwollaTransferService],
})
export class BazaNcDwollaApiModule {}
