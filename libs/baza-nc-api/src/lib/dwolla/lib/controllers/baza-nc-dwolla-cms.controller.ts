import { Body, Controller, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import {} from 'multer';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { CreateDocumentRequest, BazaDwollaCreatePersonalCustomerResponse } from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcDwollaCmsEndpoint,
    BazaNcDwollaCmsEndpointPaths,
    BazaNorthCapitalCMSOpenApi,
    BazaNcTouchDwollaCustomerRequest,
    BazaNcTouchDwollaCustomerResponse,
    BazaNcDwollaCmsTouchAllRequest,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDwollaCustomerService } from '../services/baza-nc-dwolla-customer.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalCMSOpenApi.BazaNorthCapitalDwollaCMS)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
export class BazaNcDwollaCustomerCmsController implements BazaNcDwollaCmsEndpoint {
    constructor(private readonly ncDwollaCustomerService: BazaNcDwollaCustomerService) {}

    @Post(BazaNcDwollaCmsEndpointPaths.touch)
    @ApiOperation({
        summary: 'touch',
        description: 'Attempts to create a Dwolla Customer for a specific Investor Account',
    })
    @ApiOkResponse()
    async touch(@Body() { userId }: BazaNcTouchDwollaCustomerRequest): Promise<BazaNcTouchDwollaCustomerResponse> {
        return this.ncDwollaCustomerService.touchCustomer(userId);
    }

    @Post(BazaNcDwollaCmsEndpointPaths.touchAllInvestors)
    @ApiOperation({
        summary: 'touchAllInvestors',
        description: 'Executes "touch" operation for all existing Investor Accounts',
    })
    @ApiOkResponse()
    async touchAllInvestors(@Body() request: BazaNcDwollaCmsTouchAllRequest): Promise<void> {
        const promise = this.ncDwollaCustomerService.touchCustomerForAllInvestorAccounts();

        if (!request.async) {
            await promise;
        }
    }

    @Post(BazaNcDwollaCmsEndpointPaths.retryVerification)
    @ApiOperation({
        summary: 'retryVerification',
        description: 'Attempts to retry customer verification for Dwolla Customer',
    })
    @ApiOkResponse({
        type: BazaDwollaCreatePersonalCustomerResponse,
    })
    async retryVerificationForPersonalCustomer(
        @Body() request: BazaNcTouchDwollaCustomerRequest,
    ): Promise<BazaDwollaCreatePersonalCustomerResponse> {
        return this.ncDwollaCustomerService.retryVerificationForPersonalCustomer(request.userId);
    }

    @Post(BazaNcDwollaCmsEndpointPaths.createDocument)
    @UseInterceptors(FileInterceptor('file'))
    @ApiOperation({
        summary: 'createDocument',
        description: 'Attempts to upload document for Dwolla Customer',
    })
    @ApiOkResponse()
    async createDocumentForCustomer(@UploadedFile() file: Express.Multer.File, @Body() request: CreateDocumentRequest): Promise<void> {
        return this.ncDwollaCustomerService.createDocumentForCustomer(file, request);
    }
}
