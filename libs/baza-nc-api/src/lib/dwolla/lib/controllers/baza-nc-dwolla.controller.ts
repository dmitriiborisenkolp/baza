import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthGuard, ReqAccountId } from '@scaliolabs/baza-core-api';
import {
    BazaNcDwollaCustomerDetailDto,
    BazaNcDwollaEndpoint,
    BazaNcDwollaEndpointPaths,
    BazaNcDwollaTouchResponse,
    BazaNorthCapitalOpenApi,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcDwollaCustomerService } from '../services/baza-nc-dwolla-customer.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNorthCapitalOpenApi.BazaNorthCapitalDwolla)
@UseGuards(AuthGuard)
export class BazaNcDwollaCustomerController implements BazaNcDwollaEndpoint {
    constructor(private readonly ncDwollaCustomerService: BazaNcDwollaCustomerService) {}

    @Post(BazaNcDwollaEndpointPaths.touch)
    @ApiOperation({
        summary: 'touch',
        description: 'Attempts to create a Dwolla Customer for current Investor Account',
    })
    @ApiOkResponse()
    async touch(@ReqAccountId() accountId: number): Promise<BazaNcDwollaTouchResponse> {
        return this.ncDwollaCustomerService.touchCustomer(accountId);
    }

    @Get(BazaNcDwollaEndpointPaths.current)
    @ApiOperation({
        summary: 'current',
        description:
            'Returns balance from the Dwolla. The endpoint will not fail in case if Dwolla Customer is not created yet for current investor.',
    })
    @ApiOkResponse({
        type: BazaNcDwollaCustomerDetailDto,
    })
    async current(@ReqAccountId() accountId: number): Promise<BazaNcDwollaCustomerDetailDto> {
        return this.ncDwollaCustomerService.getDwollaCustomerBalance(accountId);
    }
}
