import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '@scaliolabs/baza-dwolla-api';
import { DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class CustomerVerificationDocumentFailedEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(private readonly eventBus: EventBus) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(CustomerVerificationDocumentFailedEventHandler.name, async () => {
            if ([DwollaEventTopic.customer_verification_document_failed].includes(event.payload.responseBody.topic)) {
                // TODO: add customer document approval logic
            }
        });
    }
}
