import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaKafkaDwollaWebhookEvent } from '@scaliolabs/baza-dwolla-api';
import { DwollaEventTopic } from '@scaliolabs/baza-dwolla-shared';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaKafkaDwollaWebhookEvent)
export class CustomerVerifiedEventHandler implements IEventHandler<BazaKafkaDwollaWebhookEvent> {
    constructor(private readonly eventBus: EventBus) {}

    handle(event: BazaKafkaDwollaWebhookEvent): void {
        cqrs(CustomerVerifiedEventHandler.name, async () => {
            if ([DwollaEventTopic.customer_activated, DwollaEventTopic.customer_verified].includes(event.payload.responseBody.topic)) {
                // TODO: add customer verification success logic
            }
        });
    }
}
