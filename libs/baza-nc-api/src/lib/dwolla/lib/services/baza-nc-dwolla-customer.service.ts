import { Injectable } from '@nestjs/common';
import { RedisService } from '@liaoliaots/nestjs-redis';
import * as moment from 'moment';
import {} from 'multer';
import {
    BazaDwollaErrorCodes,
    DWOLLA_CUSTOMER_ID_LENGTH,
    DwollaCustomerId,
    DwollaFundingSource,
    DwollaFundingSourceType,
    DwollaUsStates,
    GetFundingSourceListResponse,
    UpdateCustomerRequest,
    DwollaCustomerStatus,
    CreatePersonalCustomerRequest,
    CreateDocumentRequest,
    BazaDwollaCreatePersonalCustomerResponse,
    DwollaCustomerType,
} from '@scaliolabs/baza-dwolla-shared';
import {
    DwollaCustomersApiNestjsService,
    DwollaDocumentsApiNestjsService,
    DwollaFundingSourcesApiNestjsService,
} from '@scaliolabs/baza-dwolla-api';
import {
    bazaNcApiConfig,
    BazaNcDwollaCustomerDetailDto,
    BazaNcDwollaErrorCodes,
    bazaNcDwollaErrorCodesEnI18n,
    BazaNcDwollaTouchResponse,
    BazaNcDwollaTouchResult,
    BazaNcInvestorAccountErrorCodes,
    bazaNcInvestorAccountErrorCodesEnI18n,
    PartyDetails,
    PartyId,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcNoInvestorAccountAvailableForAccountException } from '../../../investor-account';
import { BazaNcInvestorAccountRepository } from '../../../typeorm';
import { BazaNcDwollaCustomerBalanceIsNotAvailableException } from '../exceptions/baza-nc-dwolla-customer-balance-is-not-available.exception';
import { BazaNcDwollaCustomerIsNotAvailableException } from '../exceptions/baza-nc-dwolla-customer-is-not-available.exception';
import { NorthCapitalPartiesApiNestjsService } from '../../../transact-api';
import { isEmpty, isValidSSN, isValidUsZipCodeUtil } from '@scaliolabs/baza-core-shared';
import { BazaAccountRepository, BazaAppException, BazaLogger } from '@scaliolabs/baza-core-api';
import { BazaNcDwollaCustomerNotEnoughAvailableException } from '../exceptions/baza-nc-dwolla-customer-not-enought-data-available.exception';

const customerBalanceRedisKeyFactory = (investorAccountId: number) => `BAZA_NC_DWOLLA_CUSTOMER_BALANCE_FS_${investorAccountId}`;
const customerCustomerIdRedisKeyFactory = (investorAccountId: number) => `BAZA_NC_DWOLLA_CUSTOMER_ID_${investorAccountId}`;

@Injectable()
export class BazaNcDwollaCustomerService {
    constructor(
        private readonly redis: RedisService,
        private readonly logger: BazaLogger,
        private readonly accountRepository: BazaAccountRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly ncPartyApi: NorthCapitalPartiesApiNestjsService,
        private readonly dwollaCustomerApi: DwollaCustomersApiNestjsService,
        private readonly dwollaFundingSourceApi: DwollaFundingSourcesApiNestjsService,
        private readonly dwollaDocumentsApi: DwollaDocumentsApiNestjsService,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    /**
     * Returns true if user is able to create a Dwolla Customer and use Dwolla tools later
     * The method is not checking is Dwolla Customer already created for investor or not
     * The method will not fail with exception if Investor Account is missing
     * The method will not check for KYC/AML status. You should create Dwolla Customer only in case if KYC/AML by NC
     * is passed
     * Warning: The method is slow due of using NC API. You can pass `partyDetails` to avoid additional NC API usage
     * @param userId Baza Account Id (Do not use Investor Account Id)
     * @param partyDetails
     */
    async canHaveDwollaCustomer(userId: number, partyDetails?: PartyDetails, partyId?: PartyId): Promise<boolean> {
        if (partyId && !partyDetails) {
            const ncParties = await this.ncPartyApi.getParty({
                partyId: partyId,
            });

            partyDetails = ncParties.partyDetails[0];
        } else if (!partyDetails) {
            const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(userId);

            if (!investorAccount) {
                return false;
            }

            const ncParties = await this.ncPartyApi.getParty({
                partyId: investorAccount.northCapitalPartyId,
            });

            partyDetails = ncParties.partyDetails[0];
        }

        const { firstName, lastName, dob, socialSecurityNumber, primAddress1, primCity, primState, primZip } = partyDetails;

        const validators: Array<() => boolean> = [
            () => !isEmpty(firstName),
            () => !isEmpty(lastName),
            () => !isEmpty(dob),
            () => !isEmpty(socialSecurityNumber),
            () => !isEmpty(primAddress1),
            () => !isEmpty(primCity),
            () => !isEmpty(primState),
            () => Object.values(DwollaUsStates).includes(primState as DwollaUsStates),
            () => isValidSSN(socialSecurityNumber),
            () => isValidUsZipCodeUtil(primZip),
            () => {
                if (dob && typeof dob === 'string') {
                    const [month, date, year] = dob?.split('-');
                    const dateDoB = new Date(+year, +month - 1, +date, 0, 0, 0, 0);
                    const dateNow = new Date();

                    return moment(dateNow).diff(dateDoB, 'years') >= 18;
                } else {
                    return false;
                }
            },
        ];

        return validators.every((next) => next());
    }

    /**
     * Attempts to create a Dwolla Customer for Investor
     * The method works "silently" and will not fail with exception for all common cases. Instead of errors,
     * response DTO will contain result of touch operation:
     *
     *  - Created - Investor did not have Dwolla Customer created before; Dwolla Customer created
     *  - Exists - Investor already have Dwolla Customer created before
     *  - Failed - Failed to create Dwolla Customer for some reason; `failureReason` will have details
     *  - NonUsResident - Failed to create Dwolla Customer because Investor does not have enough data to create a Dwolla account
     *  - NotVerified - Failed to create Dwolla Customer because Investor is not verified yet by NC KYC/AML
     *
     * @param userId Baza Account Id (Do not use Investor Account Id)
     */
    async touchCustomer(userId: number): Promise<BazaNcDwollaTouchResponse> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(userId);

        if (!investorAccount || !investorAccount.northCapitalAccountId || !investorAccount.northCapitalPartyId) {
            if (investorAccount) {
                investorAccount.dwollaCustomerError =
                    bazaNcInvestorAccountErrorCodesEnI18n[BazaNcInvestorAccountErrorCodes.NcInvestorAccountIsNotAvailableForAccount];
                await this.investorAccountRepository.save(investorAccount);
            }

            return {
                result: BazaNcDwollaTouchResult.Failed,
                isDwollaCustomerAvailable: false,
                isDwollaAvailable: false,
                failedReason:
                    bazaNcInvestorAccountErrorCodesEnI18n[BazaNcInvestorAccountErrorCodes.NcInvestorAccountIsNotAvailableForAccount],
            };
        }

        const {
            _embedded: { customers },
        } = await this.dwollaCustomerApi.getCustomerList({ email: investorAccount.user.email });

        if (Array.isArray(customers) && customers.length) {
            investorAccount.dwollaCustomerId = customers[0].id;
            investorAccount.dwollaCustomerError = null;
            investorAccount.dwollaCustomerVerificationStatus =
                investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.DocumentProcessing
                    ? DwollaCustomerStatus.DocumentProcessing
                    : customers[0].status;

            await this.investorAccountRepository.save(investorAccount);

            return {
                result: BazaNcDwollaTouchResult.Exists,
                isDwollaCustomerAvailable: true,
                isDwollaAvailable: customers[0].status === DwollaCustomerStatus.Verified,
            };
        }

        const ncParties = await this.ncPartyApi.getParty({
            partyId: investorAccount.northCapitalPartyId,
        });

        if (await this.canHaveDwollaCustomer(userId, ncParties.partyDetails[0])) {
            const { firstName, lastName, dob, socialSecurityNumber, primAddress1, primAddress2, primCity, primState, primZip } =
                ncParties.partyDetails[0];
            const [month, date, year] = dob?.split('-');

            try {
                investorAccount.dwollaCustomerId = await this.dwollaCustomerApi.createPersonalCustomer({
                    firstName,
                    lastName,
                    email: investorAccount?.user.email,
                    dateOfBirth: `${year}-${month}-${date}`,
                    ssn: socialSecurityNumber,
                    address1: primAddress1,
                    address2: primAddress2,
                    state: DwollaUsStates[primState],
                    city: primCity,
                    postalCode: primZip,
                });

                const dwollaCustomer = await this.dwollaCustomerApi.getCustomer({
                    customerId: investorAccount.dwollaCustomerId,
                });

                investorAccount.dwollaCustomerVerificationStatus =
                    investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.DocumentProcessing
                        ? DwollaCustomerStatus.DocumentProcessing
                        : dwollaCustomer.status;

                investorAccount.dwollaCustomerError = null;

                await this.investorAccountRepository.save(investorAccount);

                return {
                    result: BazaNcDwollaTouchResult.Created,
                    isDwollaCustomerAvailable: true,
                    isDwollaAvailable: dwollaCustomer.status === DwollaCustomerStatus.Verified,
                };
            } catch (err) {
                if (err instanceof BazaAppException && err.errorCode === BazaDwollaErrorCodes.BazaDwollaApiError) {
                    investorAccount.dwollaCustomerError = err.message;
                    await this.investorAccountRepository.save(investorAccount);

                    return {
                        result: BazaNcDwollaTouchResult.Failed,
                        isDwollaCustomerAvailable: false,
                        isDwollaAvailable: false,
                        failedReason: err.message,
                    };
                } else {
                    throw err;
                }
            }
        } else {
            investorAccount.dwollaCustomerError = BazaNcDwollaTouchResult.NonUsResident;

            await this.investorAccountRepository.save(investorAccount);

            return {
                result: BazaNcDwollaTouchResult.NonUsResident,
                isDwollaCustomerAvailable: false,
                isDwollaAvailable: false,
            };
        }
    }

    /**
     * Executes `touchCustomer` operation for all existing Dwolla Customers
     */
    async touchCustomerForAllInvestorAccounts(): Promise<void> {
        const investorAccountIds = await this.investorAccountRepository.findAllInvestorAccountIds();

        this.logger.log('Running "touchCustomerForAllInvestorAccounts" command', BazaNcDwollaCustomerService.name);

        for (const investorAccountId of investorAccountIds) {
            const investorAccount = await this.investorAccountRepository.findInvestorAccountById(investorAccountId);

            if (investorAccount) {
                this.logger.log(
                    `Updating Dwolla Customer for Investor Account "${investorAccount.user.email}"`,
                    BazaNcDwollaCustomerService.name,
                );

                await this.touchCustomer(investorAccount.userId);
            }
        }

        this.logger.log('Command "touchCustomerForAllInvestorAccounts" completed', BazaNcDwollaCustomerService.name);
    }

    /**
     * Retry Dwolla Customer Verification
     * @param userId
     */
    async retryVerificationForPersonalCustomer(userId: number): Promise<BazaDwollaCreatePersonalCustomerResponse> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(userId);

        if (!investorAccount || !investorAccount.northCapitalAccountId || !investorAccount.northCapitalPartyId) {
            throw new BazaNcNoInvestorAccountAvailableForAccountException();
        }

        if (!investorAccount.dwollaCustomerId) {
            throw new BazaNcDwollaCustomerIsNotAvailableException();
        }

        const ncParties = await this.ncPartyApi.getParty({
            partyId: investorAccount.northCapitalPartyId,
        });

        const partyDetails: PartyDetails = ncParties.partyDetails[0];

        if (!(await this.canHaveDwollaCustomer(userId, partyDetails))) {
            throw new BazaNcDwollaCustomerNotEnoughAvailableException();
        }

        const createPersonalCustomerRequest: CreatePersonalCustomerRequest = {
            firstName: partyDetails.firstName,
            lastName: partyDetails.lastName,
            email: partyDetails.emailAddress,
            address1: partyDetails.primAddress1,
            address2: partyDetails.primAddress2,
            city: partyDetails.primCity,
            state: partyDetails.primState as DwollaUsStates,
            postalCode: partyDetails.primZip,
            dateOfBirth: partyDetails.dob.replace(/(..).(..).(....)/, '$3-$1-$2'),
            ssn: partyDetails.socialSecurityNumber,
            phone: partyDetails.phone,
            type: DwollaCustomerType.Personal,
        };

        return this.dwollaCustomerApi.retryVerificationForPersonalCustomer(investorAccount.dwollaCustomerId, createPersonalCustomerRequest);
    }

    /**
     * Uploads Documentation for Dwolla Customer Verification
     * @param file
     * @param request
     */
    async createDocumentForCustomer(file: Express.Multer.File, request: CreateDocumentRequest): Promise<void> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(+request.userId);

        if (!investorAccount || !investorAccount.northCapitalAccountId || !investorAccount.northCapitalPartyId) {
            throw new BazaNcNoInvestorAccountAvailableForAccountException();
        }

        if (!investorAccount.dwollaCustomerId) {
            throw new BazaNcDwollaCustomerIsNotAvailableException();
        }

        await this.dwollaDocumentsApi.createDocumentForCustomer(investorAccount.dwollaCustomerId, file, request);

        investorAccount.dwollaCustomerVerificationStatus = DwollaCustomerStatus.DocumentProcessing;
        await this.investorAccountRepository.save(investorAccount);
    }

    /**
     * Updates Dwolla Customer with information from NC Party
     * The method sync primAddress1, primAddress2, primCity, primState and primZip fields
     * First Name, Last Name, SSN and email CANNOT BE UPDATED due of Dwolla limitations
     * @param userId
     * @param partyDetails
     */
    async updateDwollaCustomer(userId: number, partyDetails?: PartyDetails): Promise<BazaNcDwollaTouchResponse> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(userId);

        if (!investorAccount || !investorAccount.northCapitalAccountId || !investorAccount.northCapitalPartyId) {
            return {
                result: BazaNcDwollaTouchResult.Failed,
                isDwollaCustomerAvailable: false,
                isDwollaAvailable: false,
                failedReason:
                    bazaNcInvestorAccountErrorCodesEnI18n[BazaNcInvestorAccountErrorCodes.NcInvestorAccountIsNotAvailableForAccount],
            };
        }

        if (!investorAccount.dwollaCustomerId) {
            return {
                result: BazaNcDwollaTouchResult.Failed,
                isDwollaCustomerAvailable: false,
                isDwollaAvailable: false,
                failedReason: bazaNcDwollaErrorCodesEnI18n[BazaNcDwollaErrorCodes.BazaNcDwollaCustomerIsNotAvailable],
            };
        }

        if (!partyDetails) {
            const ncParties = await this.ncPartyApi.getParty({
                partyId: investorAccount.northCapitalPartyId,
            });

            partyDetails = ncParties.partyDetails[0];
        }

        if (!(await this.canHaveDwollaCustomer(userId, partyDetails))) {
            return {
                result: BazaNcDwollaTouchResult.NonUsResident,
                isDwollaCustomerAvailable: true,
                isDwollaAvailable: investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.Verified,
            };
        }

        const hasEnoughData =
            partyDetails &&
            partyDetails.primAddress1 &&
            partyDetails.primCity &&
            partyDetails.primState &&
            DwollaUsStates[partyDetails.primState] &&
            partyDetails.primZip;

        if (!hasEnoughData) {
            return {
                result: BazaNcDwollaTouchResult.Failed,
                isDwollaCustomerAvailable: true,
                isDwollaAvailable: investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.Verified,
                failedReason: bazaNcDwollaErrorCodesEnI18n[BazaNcDwollaErrorCodes.BazaNcDwollaCustomerNotEnoughDataToUpdate],
            };
        }

        try {
            await this.dwollaCustomerApi.updateCustomer(investorAccount.dwollaCustomerId, {
                address1: partyDetails.primAddress1,
                address2: partyDetails.primAddress2,
                city: partyDetails.primCity,
                state: DwollaUsStates[partyDetails.primState],
                postalCode: partyDetails.primZip,
            } as UpdateCustomerRequest);

            return {
                result: BazaNcDwollaTouchResult.Exists,
                isDwollaCustomerAvailable: true,
                isDwollaAvailable: investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.Verified,
            };
        } catch (err) {
            if (err instanceof BazaAppException && err.errorCode === BazaDwollaErrorCodes.BazaDwollaApiError) {
                return {
                    result: BazaNcDwollaTouchResult.Failed,
                    isDwollaCustomerAvailable: true,
                    isDwollaAvailable: investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.Verified,
                    failedReason: err.message,
                };
            } else {
                throw err;
            }
        }
    }

    /**
     *  Returns Dwolla Balance for Investor
     *  The method works silently. A `0.00 USD` result will be returned if Dwolla Customer is missing or anything else
     * @param userId Baza Account Id (Do not use Investor Account Id)
     */
    async getDwollaCustomerBalance(userId: number): Promise<BazaNcDwollaCustomerDetailDto> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(userId);

        const emptyResponse = () => ({
            isDwollaAvailable: false,
            balance: {
                value: '0.00',
                currency: 'USD',
            },
        });

        const hasVerifiedDwollaCustomer =
            investorAccount &&
            investorAccount.dwollaCustomerId &&
            investorAccount.dwollaCustomerVerificationStatus === DwollaCustomerStatus.Verified;

        if (!hasVerifiedDwollaCustomer) {
            return emptyResponse();
        }

        const fundingSourceList = await this.dwollaFundingSourceApi.getFundingSourceListForCustomer(
            investorAccount.dwollaCustomerId,
            false,
        );

        const dwollaFundingSource = fundingSourceList?._embedded['funding-sources']?.find(
            (fundingSource) => fundingSource.type === DwollaFundingSourceType.Balance,
        );

        if (dwollaFundingSource) {
            const dwollaBalance = await this.dwollaFundingSourceApi.getFundingSourceBalance(dwollaFundingSource.id);

            return dwollaBalance.balance
                ? {
                      isDwollaAvailable: true,
                      balance: dwollaBalance.balance,
                  }
                : emptyResponse();
        } else {
            return emptyResponse();
        }
    }

    /**
     * Returns Dwolla Customer Id for Investor
     * The method will attempts to automatically create a Dwolla Customer in case if it's still missing
     * The method will fail with exception is case if Dwolla Customer will not be available even after `touch` operation
     * @param investorAccountId Investor Account Id (do not use Baza Account Id)
     */
    async getDwollaCustomerId(investorAccountId: number, correlationId?: string): Promise<DwollaCustomerId> {
        this.logger.info(`${this.constructor.name}.${this.getDwollaCustomerId.name}`, {
            correlationId,
            investorAccountId,
        });

        const key = customerCustomerIdRedisKeyFactory(investorAccountId);
        const cached = await this.redis.getClient().get(key);

        const fetchDwollaCustomerId = async () => {
            const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);

            if (!investorAccount.dwollaCustomerId) {
                if (bazaNcApiConfig().dwollaAutoTouchCustomers) {
                    const touchResponse = await this.touchCustomer(investorAccount.user.id);

                    if (touchResponse.isDwollaCustomerAvailable) {
                        const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);

                        await this.redis.getClient().set(key, investorAccount.dwollaCustomerId);

                        return investorAccount.dwollaCustomerId;
                    } else {
                        throw new BazaNcDwollaCustomerIsNotAvailableException();
                    }
                } else {
                    throw new BazaNcDwollaCustomerIsNotAvailableException();
                }
            } else {
                await this.redis.getClient().set(key, investorAccount.dwollaCustomerId);

                return investorAccount.dwollaCustomerId;
            }
        };

        return cached && String(cached).length === DWOLLA_CUSTOMER_ID_LENGTH ? cached : await fetchDwollaCustomerId();
    }

    /**
     * Returns Dwolla Customer Funding Sources
     * FS List is not cached in Redis
     * @param investorAccountId Investor Account Id (do not use Baza Account Id)
     */
    async getDwollaCustomerFundingSources(investorAccountId: number, correlationId?: string): Promise<GetFundingSourceListResponse> {
        this.logger.info(`${this.constructor.name}.${this.getDwollaCustomerFundingSources.name}`, {
            correlationId,
            investorAccountId,
        });

        return this.dwollaFundingSourceApi.getFundingSourceListForCustomer(await this.getDwollaCustomerId(investorAccountId));
    }

    /**
     * Returns Dwolla Customer Balance Funding Source
     * Balance Funding Source is not cached in redis
     * @param investorAccountId Investor Account Id (do not use Baza Account Id)
     */
    async getDwollaCustomerBalanceFundingSource(investorAccountId: number, correlationId?: string): Promise<DwollaFundingSource> {
        this.logger.info(`${this.constructor.name}.${this.getDwollaCustomerBalanceFundingSource.name}`, {
            correlationId,
            investorAccountId,
        });

        const dwollaCustomerFundingSources = await this.getDwollaCustomerFundingSources(investorAccountId, correlationId);

        const fundingSource = dwollaCustomerFundingSources._embedded['funding-sources'].find(
            (next) => next.type === DwollaFundingSourceType.Balance,
        );

        if (!fundingSource) {
            throw new BazaNcDwollaCustomerBalanceIsNotAvailableException();
        }

        return fundingSource;
    }

    /**
     * Returns HREF of Dwolla Customer Balance which should be used for Transfer requests.
     * The method caches its result in Redis
     * @param investorAccountId Investor Account Id (do not use Baza Account Id)
     */
    async getDwollaCustomerBalanceDestinationHref(investorAccountId: number): Promise<string> {
        const key = customerBalanceRedisKeyFactory(investorAccountId);
        const cached = await this.redis.getClient().get(key);

        if (cached) {
            return cached;
        } else {
            const balanceFS = await this.getDwollaCustomerBalanceFundingSource(investorAccountId);
            const balanceLink = balanceFS._links.self.href;

            await this.redis.getClient().set(key, balanceLink);

            return balanceLink;
        }
    }
}
