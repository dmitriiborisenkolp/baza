import { Injectable } from '@nestjs/common';
import { DwollaTransferId, DwollaTransferStatus } from '@scaliolabs/baza-dwolla-shared';
import { DwollaTransfersApiNestjsService } from '@scaliolabs/baza-dwolla-api';

@Injectable()
export class BazaNcDwollaTransferService {
    constructor(private readonly dwollaTransferApi: DwollaTransfersApiNestjsService) {}

    /**
     * Returns status of Dwolla Transfer
     * @param id
     */
    async getStatus(id: DwollaTransferId): Promise<DwollaTransferStatus> {
        const response = await this.dwollaTransferApi.getTransfer(id);

        return response.status;
    }
}
