import { registerMailTemplates } from '@scaliolabs/baza-core-api';
import { bazaNcDividendTransactionMailTemplateVariable } from './mail-helpers/baza-nc-dividend-transaction.mail-template-variable';
import { bazaNcDividendTransactionEntryMailTemplateVariable } from './mail-helpers/baza-nc-dividend-transaction-entry.mail-template-variable';
import { bazaNcInvestorAccountMailTemplateVariable } from './mail-helpers/baza-nc-investor-account.mail-template-variable';
import { bazaNcReportMailTemplateVariable } from './mail-helpers/baza-nc-report.mail-template-variable';

/**
 * Mail Templates of baza-nc package
 */
export enum BazaNcMail {
    BazaNcDividendDwollaTransferInitiated = 'BazaNcDividendDwollaTransferInitiated',
    BazaNcDividendDwollaTransferCanceled = 'BazaNcDividendDwollaTransferCanceled',
    BazaNcDividendDwollaTransferCompleted = 'BazaNcDividendDwollaTransferCompleted',
    BazaNcDividendDwollaTransferFailed = 'BazaNcDividendDwollaTransferFailed',
    BazaNcDividendDwollaReport = 'BazaNcDividendDwollaReport',
    BazaNcDividendConfirmProcessTransaction = 'BazaNcDividendConfirmProcessTransaction',
    BazaNcDividendSuccessfulPaymentWithOffering = 'BazaNcDividendSuccessfulPaymentWithOffering',
    BazaNcDividendSuccessfulPaymentWithoutOffering = 'BazaNcDividendSuccessfulPaymentWithoutOffering',
    BazaNcOfferingClientNotification = 'BazaNcOfferingClientNotification',
    BazaNcReportNcEscrowNotification = 'BazaNcReportNcEscrowNotification',
    BazaNcFundsTransferred = 'BazaNcFundsTransferred',
    BazaNcInvestmentNotification = 'BazaNcInvestmentNotification',
    BazaNcInvestmentSubmitted = 'BazaNcInvestmentSubmitted',
    BazaNcPaymentReturned = 'BazaNcPaymentReturned',
    BazaNcTradeCanceled = 'BazaNcTradeCanceled',
}

/**
 * Tags used for Mail Templates of baza-nc package
 */
export enum BazaNcMailTags {
    BazaNcDividend = 'Dividends',
    BazaNcOffering = 'Offerings',
    BazaNcPurchaseFlow = 'Purchase Flow',
    BazaNcReport = 'Reports for NC Escrow Ops',
}

/**
 * Registers Mail Templates of baza-nc package
 * @see BazaNcMail
 * @see BazaNcMailTags
 */
registerMailTemplates([
    {
        name: BazaNcMail.BazaNcDividendDwollaTransferInitiated,
        title: 'Dividends – Initiating Dwolla Transfer',
        description: 'A notification which will be sent to CMS Admin after initiating Dividend Transaction',
        tag: BazaNcMailTags.BazaNcDividend,
        filePath: 'baza-nc/dividend/client-notifications/dwolla/individual/transfer-initiated/transfer-initiated.{type}.hbs',
        variables: [
            {
                field: 'amount',
                description: 'Amount ($)',
                optional: false,
            },
            {
                field: 'customer',
                description: 'Investor Account Full Name',
                optional: false,
            },
            {
                field: 'customerDwollaId',
                description: 'Investor Account Dwolla Customer Id',
                optional: false,
            },
            {
                field: 'dwollaTransferId',
                description: 'Dwolla Transfer Id',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcDividendDwollaTransferCanceled,
        title: 'Dividends – Canceled Dwolla Transfer',
        description: 'A notification which will be sent to CMS Admin when Dwolla API reports about cancelling Dwolla Transfer',
        tag: BazaNcMailTags.BazaNcDividend,
        filePath: 'baza-nc/dividend/client-notifications/dwolla/individual/transfer-canceled/transfer-canceled.{type}.hbs',
        variables: [
            {
                field: 'amount',
                description: 'Amount ($)',
                optional: false,
            },
            {
                field: 'customer',
                description: 'Investor Account Full Name',
                optional: false,
            },
            {
                field: 'customerDwollaId',
                description: 'Investor Account Dwolla Customer Id',
                optional: false,
            },
            {
                field: 'dwollaTransferId',
                description: 'Dwolla Transfer Id',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcDividendDwollaTransferCompleted,
        title: 'Dividends – Completed Dwolla Transfer',
        description: 'A notification which will be sent to CMS Admin when Dwolla API reports about completed Dwolla Transfer',
        tag: BazaNcMailTags.BazaNcDividend,
        filePath: 'baza-nc/dividend/client-notifications/dwolla/individual/transfer-completed/transfer-completed.{type}.hbs',
        variables: [
            {
                field: 'amount',
                description: 'Amount ($)',
                optional: false,
            },
            {
                field: 'customer',
                description: 'Investor Account Full Name',
                optional: false,
            },
            {
                field: 'customerDwollaId',
                description: 'Investor Account Dwolla Customer Id',
                optional: false,
            },
            {
                field: 'dwollaTransferId',
                description: 'Dwolla Transfer Id',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcDividendDwollaTransferFailed,
        title: 'Dividends – Failed Dwolla Transfer',
        description: 'A notification which will be sent to CMS Admin when Dwolla API reports about failed Dwolla Transfer',
        tag: BazaNcMailTags.BazaNcDividend,
        filePath: 'baza-nc/dividend/client-notifications/dwolla/individual/transfer-failed/transfer-failed.{type}.hbs',
        variables: [
            {
                field: 'amount',
                description: 'Amount ($)',
                optional: false,
            },
            {
                field: 'customer',
                description: 'Investor Account Full Name',
                optional: false,
            },
            {
                field: 'customerDwollaId',
                description: 'Investor Account Dwolla Customer Id',
                optional: false,
            },
            {
                field: 'dwollaTransferId',
                description: 'Dwolla Transfer Id',
                optional: false,
            },
            {
                field: 'reason',
                description: 'Reason why Dwolla Transfer was failed',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcDividendDwollaReport,
        title: 'Dividends – Report',
        description:
            'A notification email which will be sent to CMS Admin when all Dwolla Transfers will be initiated for Dividends Transaction',
        tag: BazaNcMailTags.BazaNcDividend,
        filePath: 'baza-nc/dividend/client-notifications/dwolla/report/report.{type}.hbs',
        variables: [
            {
                field: 'message',
                description: 'i18n Message (Success or Failed Payment Initiation)',
                optional: false,
            },
            bazaNcDividendTransactionMailTemplateVariable(),
            bazaNcDividendTransactionEntryMailTemplateVariable(
                {
                    field: 'entries',
                    isArray: true,
                },
                {
                    failureReasonAsString: true,
                },
            ),
        ],
    },
    {
        name: BazaNcMail.BazaNcDividendConfirmProcessTransaction,
        title: 'Dividends – Confirm Process Transaction',
        description: 'Confirmation email with link and/or token to proceed with processing dividends',
        tag: BazaNcMailTags.BazaNcDividend,
        filePath: 'baza-nc/dividend/confirm-process-transaction/confirm-process-transaction.{type}.hbs',
        variables: [
            {
                field: 'dividendConfirmationToken',
                description: 'Confirmation Token',
                optional: false,
            },
            {
                field: 'dividendConfirmationLink',
                description: 'Confirmation Link',
                optional: false,
            },
            {
                field: 'dividendTransactionUlid',
                description: 'ULID of Dividend Transaction',
                optional: false,
            },
            {
                field: 'dividendTransactionTitle',
                description: 'Dividend Transaction Title',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcDividendSuccessfulPaymentWithOffering,
        title: 'Dividends – Customer Notification (With Offering)',
        description: 'Notification email which will be sent to Investor when Dividend was successfully processed (With Offering specified)',
        tag: BazaNcMailTags.BazaNcDividend,
        filePath: 'baza-nc/dividend/successful-payment/with-offering/successful-payment.{type}.hbs',
        variables: [
            {
                field: 'amount',
                description: 'Amount ($)',
                optional: false,
            },
            {
                field: 'offeringName',
                description: 'Offering Name',
                optional: false,
            },
            bazaNcInvestorAccountMailTemplateVariable(),
        ],
    },
    {
        name: BazaNcMail.BazaNcDividendSuccessfulPaymentWithoutOffering,
        title: 'Dividends – Customer Notification (Without Offering)',
        description:
            'Notification email which will be sent to Investor when Dividend was successfully processed (Without Offering specified)',
        tag: BazaNcMailTags.BazaNcDividend,
        filePath: 'baza-nc/dividend/successful-payment/without-offering/successful-payment.{type}.hbs',
        variables: [
            {
                field: 'amount',
                description: 'Amount ($)',
                optional: false,
            },
            bazaNcInvestorAccountMailTemplateVariable(),
        ],
    },
    {
        name: BazaNcMail.BazaNcOfferingClientNotification,
        title: 'Offering – Client Notification',
        description: 'Notification to subscribed Investor about opening an Offering',
        tag: BazaNcMailTags.BazaNcOffering,
        filePath: 'baza-nc/offering/client-notification/offering-notification.{type}.hbs',
        variables: [
            {
                field: 'userFullName',
                description: 'Account (User) Full Name',
                optional: false,
            },
            {
                field: 'offeringName',
                description: 'Offering Name',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcReportNcEscrowNotification,
        title: 'Report – NC Escrow Notification',
        description: 'Notification Email which will be sent to North Capital Ops about payment transactions for trades made via Dwolla',
        tag: BazaNcMailTags.BazaNcReport,
        filePath: 'baza-nc/report/nc-escrow-notification/nc-escrow-notification.{type}.hbs',
        variables: [
            bazaNcReportMailTemplateVariable({
                field: 'entities',
                isArray: true,
            }),
        ],
        attachments: [
            {
                example: 'escrow-my-project-2022-12-26.csv',
                contentType: 'text/csv',
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcFundsTransferred,
        title: 'Purchase Flow – Funds Transferred',
        description: 'Notification Email to Investor about funds have been transferred from escrow and investment is approved',
        tag: BazaNcMailTags.BazaNcPurchaseFlow,
        filePath: 'baza-nc/purchase-flow/funds-transferred/funds-transferred.{type}.hbs',
        variables: [],
    },
    {
        name: BazaNcMail.BazaNcInvestmentNotification,
        title: 'Purchase Flow – Investment Notification',
        description: 'Notification Email to CMS Admin about new Purchase',
        tag: BazaNcMailTags.BazaNcPurchaseFlow,
        filePath: 'baza-nc/purchase-flow/investment-notification/investment-notification.{type}.hbs',
        variables: [
            {
                field: 'offeringName',
                description: 'Offering or Listing Name',
                optional: false,
            },
            {
                field: 'numberOfShares',
                description: 'Number of Shares',
                optional: false,
            },
            {
                field: 'shares',
                description: '"share" or "shares" string',
                optional: false,
            },
            {
                field: 'totalInvestments',
                description: 'Amount ($)',
                optional: false,
            },
            {
                field: 'reservedBy',
                description: 'Investor Account Full Name',
                optional: false,
            },
            {
                field: 'reservedByEmail',
                description: 'Investor Account Email',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcInvestmentSubmitted,
        title: 'Purchase Flow – Investment Submitted',
        description: 'Notification Email to Investment about successful Purchase',
        tag: BazaNcMailTags.BazaNcPurchaseFlow,
        filePath: 'baza-nc/purchase-flow/investment-submitted/investment-submitted.{type}.hbs',
        variables: [
            {
                field: 'offeringName',
                description: 'Offering or Listing Name',
                optional: false,
            },
            {
                field: 'numberOfShares',
                description: 'Number of Shares',
                optional: false,
            },
            {
                field: 'shares',
                description: '"share" or "shares" string',
                optional: false,
            },
            {
                field: 'totalInvestments',
                description: 'Amount ($)',
                optional: false,
            },
            {
                field: 'reservedBy',
                description: 'Investor Account Full Name',
                optional: false,
            },
            {
                field: 'reservedByEmail',
                description: 'Investor Account Email',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcPaymentReturned,
        title: 'Purchase Flow – Payment Returned',
        description:
            'Notification Email to Investor about failures in processing payment for Purchase. This email should ask Investor to reprocess payment.',
        tag: BazaNcMailTags.BazaNcPurchaseFlow,
        filePath: 'baza-nc/purchase-flow/payment-returned/payment-returned.{type}.hbs',
        variables: [
            {
                field: 'investorName',
                description: 'Investor Account Full Name',
                optional: false,
            },
        ],
    },
    {
        name: BazaNcMail.BazaNcTradeCanceled,
        title: 'Purchase Flow – Trade Canceled',
        description: 'Notification Email to Investor about canceling payment for Purchase',
        tag: BazaNcMailTags.BazaNcPurchaseFlow,
        filePath: 'baza-nc/purchase-flow/trade-canceled/trade-canceled.{type}.hbs',
        variables: [
            {
                field: 'investorName',
                description: 'Investor Account Full Name',
                optional: false,
            },
            {
                field: 'email',
                description: 'Investor Account Email Address',
                optional: false,
            },
        ],
    },
]);
