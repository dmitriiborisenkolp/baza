import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';
import { bazaNcDwollaFailureReasonMailTemplateVariable } from './baza-nc-dwolla-failure-reason.mail-template-variable';
import { BazaNcDividendPaymentSource, BazaNcDividendPaymentStatus } from '@scaliolabs/baza-nc-shared';
import { bazaNcInvestorAccountMailTemplateVariable } from './baza-nc-investor-account.mail-template-variable';

interface Options {
    failureReasonAsString: boolean;
}

export const bazaNcDividendTransactionEntryMailTemplateVariable: (
    partial: Partial<BazaMailTemplateVariable>,
    options: Options,
) => BazaMailTemplateVariable = (partial: Partial<BazaMailTemplateVariable>, options: Options) => ({
    field: 'entries',
    description: 'Dividend Transaction Entry',
    optional: false,
    isArray: true,
    variables: [
        {
            field: 'ulid',
            description: 'ULID (ID)',
            optional: false,
        },
        {
            field: 'dateCreatedAt',
            description: 'Date Created At (ISO Date)',
            optional: false,
        },
        {
            field: 'dateUpdatedAt',
            description: 'Date Updated At (ISO Date)',
            optional: true,
        },
        {
            field: 'dateProcessedAt',
            description: 'Date Processed At (ISO Date)',
            optional: true,
        },
        {
            field: 'date',
            description: 'Date (ISO Date)',
            optional: true,
        },
        {
            field: 'offeringId',
            description: 'NC Offering ID',
            optional: true,
        },
        {
            field: 'offeringTitle',
            description: 'NC Offering Title',
            optional: true,
        },
        {
            field: 'source',
            description: `Source (one of ${Object.values(BazaNcDividendPaymentSource).join(', ')})`,
            optional: false,
        },
        bazaNcInvestorAccountMailTemplateVariable(),
        {
            field: 'amount',
            description: 'Amount (as $)',
            optional: false,
        },
        {
            field: 'amountCents',
            description: 'Amount (as cents)',
            optional: false,
        },
        {
            field: 'status',
            description: `Status (one of ${Object.values(BazaNcDividendPaymentStatus).join(', ')})`,
            optional: false,
        },
        {
            field: 'canBeReprocessed',
            description: 'Can be reprocessed? (boolean)',
            optional: false,
        },
        {
            field: 'canBeUpdated',
            description: 'Can be updated? (boolean)',
            optional: false,
        },
        {
            field: 'canBeDeleted',
            description: 'Can be deleted? (boolean)',
            optional: false,
        },
        options.failureReasonAsString
            ? {
                  field: 'failureReason',
                  description: 'Failure reason why Dwolla Transfer was failed',
                  optional: false,
              }
            : bazaNcDwollaFailureReasonMailTemplateVariable(),
        {
            field: 'dwollaTransferId',
            description: 'Dwolla Transfer Id',
            optional: true,
        },
    ],
    ...partial,
});
