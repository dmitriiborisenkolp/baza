import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendTransactionStatus } from '@scaliolabs/baza-nc-shared';

export const bazaNcDividendTransactionMailTemplateVariable: (partial?: Partial<BazaMailTemplateVariable>) => BazaMailTemplateVariable = (
    partial: Partial<BazaMailTemplateVariable> = {},
) => ({
    field: 'transaction',
    description: 'Dividend Transaction',
    optional: false,
    variables: [
        {
            field: 'ulid',
            description: 'ULID (ID)',
            optional: false,
        },
        {
            field: 'dateCreatedAt',
            description: 'Date Created At (ISO Date)',
            optional: false,
        },
        {
            field: 'dateUpdatedAt',
            description: 'Date Updated At (ISO Date)',
            optional: true,
        },
        {
            field: 'dateProcessedAt',
            description: 'Date Processed At (ISO Date)',
            optional: true,
        },
        {
            field: 'status',
            description: `Status (one of ${Object.values(BazaNcDividendTransactionStatus).join(', ')})`,
            optional: false,
        },
        {
            field: 'title',
            description: 'Transaction Title',
            optional: false,
        },
        {
            field: 'canBeDeleted',
            description: 'Can be deleted? (boolean)',
            optional: false,
        },
        {
            field: 'isLocked',
            description: 'Is Dividend Transaction locked? (boolean)',
            optional: false,
        },
        {
            field: 'isProcessing',
            description: 'Is Dividend Transaction processing now? (boolean)',
            optional: false,
        },
    ],
    ...partial,
});
