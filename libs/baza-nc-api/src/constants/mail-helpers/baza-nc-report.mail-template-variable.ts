import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';
import { BazaNcReportStatus } from '@scaliolabs/baza-nc-shared';

export const bazaNcReportMailTemplateVariable: (partial?: Partial<BazaMailTemplateVariable>) => BazaMailTemplateVariable = (
    partial: Partial<BazaMailTemplateVariable> = {},
) => ({
    field: 'report',
    description: 'Report Entry for NC Escrow Ops Notifications',
    optional: false,
    isArray: false,
    variables: [
        {
            field: 'ulid',
            description: 'ULID (ID)',
            optional: false,
        },
        {
            field: 'dateCreatedAt',
            description: 'Date Created At (ISO Date)',
            optional: false,
        },
        {
            field: 'dateFundedAt',
            description: 'Date Funded At (ISO Date)',
            optional: true,
        },
        {
            field: 'dateReportedAt',
            description: 'Date Reported At (ISO Date)',
            optional: true,
        },
        {
            field: 'tradeId',
            description: 'NC Trade Id',
            optional: false,
        },
        {
            field: 'achId',
            description: 'Individual ACH ID assigned by Dwolla',
            optional: false,
        },
        {
            field: 'investorName',
            description: 'Investor Account Full Name',
            optional: false,
        },
        {
            field: 'offeringName',
            description: 'NC Offering Name',
            optional: true,
        },
        {
            field: 'amount',
            description: 'Total Amount',
            optional: true,
        },
        {
            field: 'status',
            description: `Status (one of ${Object.values(BazaNcReportStatus).join(', ')})`,
            optional: false,
        },
        {
            field: 'canBeSent',
            description: 'Can be sent to NC Escrow OPS? (boolean)',
            optional: false,
        },
        {
            field: 'canBeResent',
            description: 'Can be re-sent to NC Escrow OPS? (boolean)',
            optional: false,
        },
        {
            field: 'canBySynced',
            description: 'Can be synced with Dwolla? (boolean)',
            optional: false,
        },
    ],
    ...partial,
});
