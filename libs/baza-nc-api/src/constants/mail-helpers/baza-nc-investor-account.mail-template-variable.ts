import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';
import { BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';

export const bazaNcInvestorAccountMailTemplateVariable: (partial?: Partial<BazaMailTemplateVariable>) => BazaMailTemplateVariable = (
    partial: Partial<BazaMailTemplateVariable> = {},
) => ({
    field: 'investorAccount',
    description: 'Investor Account Details',
    optional: false,
    variables: [
        {
            field: 'id',
            description: 'Investor Account Id (not same as userId (Account Id!))',
            optional: false,
        },
        {
            field: 'userId',
            description: 'Account Id',
            optional: false,
        },
        {
            field: 'userEmail',
            description: 'Email Address',
            optional: false,
        },
        {
            field: 'userFullName',
            description: 'Account Full Name',
            optional: false,
        },
        {
            field: 'northCapitalAccountId',
            description: 'NC Account Id',
            optional: false,
        },
        {
            field: 'northCapitalPartyId',
            description: 'NC Party Id',
            optional: false,
        },
        {
            field: 'isAccountVerificationInProgress',
            description: 'Is Account Verification in Progress?',
            optional: false,
        },
        {
            field: 'isAccountVerificationCompleted',
            description: 'Is Account Verification Completed?',
            optional: false,
        },
        {
            field: 'isInvestorVerified',
            description: 'Is Investor Account Verified (KYC/AML)?',
            optional: false,
        },
        {
            field: 'isAccreditedInvestor',
            description: 'Is Accredited Investor?',
            optional: false,
        },
        {
            field: 'isBankAccountLinked',
            description: 'Is Cash In (Purchase, Cash-In) or NC ACH (Purchase) Bank Account available?',
            optional: false,
        },
        {
            field: 'isBankAccountNcAchLinked',
            description: 'Is NC ACH (Purchase) Bank Account available?',
            optional: false,
        },
        {
            field: 'isBankAccountCashInLinked',
            description: 'Is Cash In (Purchase, Cash-In) Bank Account available?',
            optional: false,
        },
        {
            field: 'isBankAccountCashOutLinked',
            description: 'Is Cash Out (Withdraw Dividends) Bank Account available?',
            optional: false,
        },
        {
            field: 'isCreditCardLinked',
            description: 'Is credit card linked?',
            optional: false,
        },
        {
            field: 'isPaymentMethodLinked',
            description: 'Is payment method linked?',
            optional: false,
        },
        {
            field: 'ssnDocumentFileName',
            description: 'SSN Document FileName',
            optional: true,
        },
        {
            field: 'ssnDocumentId',
            description: 'SSN Document ID',
            optional: true,
        },
        {
            field: 'dwollaCustomerId',
            description: 'Dwolla Customer ID',
            optional: true,
        },
        {
            field: 'dwollaCustomerError',
            description: 'Error message for last failed attempt to create a Dwolla Customer for Investor Account',
            optional: true,
        },
        {
            field: 'dwollaCustomerVerificationStatus',
            description: 'Verification Status of Dwolla Customer',
            optional: true,
        },
        {
            field: 'isDwollaAvailable',
            description: 'Is Dwolla Balance Available to use for purchases?',
            optional: true,
        },
        {
            field: 'status',
            description: 'Investor account status',
            optional: true,
            variables: [
                {
                    field: 'nc',
                    description: 'KYC status for NC',
                    optional: false,
                },
                {
                    field: 'dwolla',
                    description: 'KYC status for Dwolla',
                    optional: false,
                },
                {
                    field: 'isDwollaAvailable',
                    description: 'Is Dwolla Customer created & available for Investor?',
                    optional: false,
                },
                {
                    field: 'isForeign',
                    description: 'Is Foreign Investor?',
                    optional: false,
                },
                {
                    field: 'defaultPaymentMethod',
                    description: `Default Payment Method which selected by Investor. Should be one of ${BazaNcPurchaseFlowTransactionType}`,
                    optional: false,
                },
            ],
        },
        {
            field: 'defaultPaymentMethod',
            description: `Default Payment Method (one of ${Object.values(BazaNcPurchaseFlowTransactionType).join(', ')})`,
            optional: false,
        },
        {
            field: 'dwollaCustomerVerificationFailureReasons',
            description: 'List of Dwolla Customer validation errors',
            optional: true,
            isArray: true,
            variables: [
                {
                    field: 'reason',
                    description: 'Reason (Code)',
                    optional: false,
                },
                {
                    field: 'description',
                    description: 'Description',
                    optional: false,
                },
            ],
        },
    ],
    ...partial,
});
