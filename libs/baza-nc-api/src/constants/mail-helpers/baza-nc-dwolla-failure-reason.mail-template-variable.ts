import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';

export const bazaNcDwollaFailureReasonMailTemplateVariable: (partial?: Partial<BazaMailTemplateVariable>) => BazaMailTemplateVariable = (
    partial: Partial<BazaMailTemplateVariable> = {},
) => ({
    field: 'failureReason',
    description: 'Failure reason why Dwolla Transfer was failed',
    optional: false,
    variables: [
        {
            field: 'code',
            description:
                'Failure Code (https://developers.dwolla.com/concepts/transfer-failures#list-of-possible-return-codes-descriptions-and-actions) Or Baza Error Code',
            optional: false,
        },
        {
            field: 'reason',
            description: 'Human-Readable message (short)',
            optional: false,
        },
        {
            field: 'details',
            description: 'Human-Readable message (detailed)',
            optional: true,
        },
    ],
    ...partial,
});
