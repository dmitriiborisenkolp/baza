import { BazaNcReportStatus } from '../../../../lib/report';

/**
 * TODO: Move resources to libs/ of baza-nc-shared library
 */
export const bazaNcApiEnI18n = {
    'baza-nc': {
        mail: {
            investmentSubmitted: {
                subject: '{{ clientName }}: Purchase Confirmed!',
            },
            investmentNotification: {
                subject: 'New Investment',
            },
            paymentFailed: {
                subject: '{{ clientName }}: Payment Failed',
            },
            tradeCanceled: {
                subject: '{{ clientName }}: Trade Canceled',
            },
            fundsTransferred: {
                subject: 'Your Funds Have Been Transferred',
            },
            confirmDividendTransaction: {
                subject: 'Confirm dividend transaction "{{ dividendTransactionTitle }}"',
            },
            clientNotifications: {
                dwolla: {
                    individual: {
                        transferInitiated: {
                            subject:
                                '[Investment Platform]: You have successfully initiated the transfer of {{ amount }} USD as dividends to customer {{ customer }} and Dwolla ID {{ customerDwollaId }}',
                        },
                        transferCancelled: {
                            subject:
                                '[Investment Platform]: The transfer of {{ amount }} USD as dividends to customer {{ customer }} and Dwolla ID {{ customerDwollaId }} cancelled',
                        },
                        transferCompleted: {
                            subject:
                                '[Investment Platform]: You have successfully completed the transfer of {{ amount }} USD as dividends to customer {{ customer }} and Dwolla ID {{ customerDwollaId }}',
                        },
                        transferFailed: {
                            subject:
                                '[Investment Platform]: We are sorry to inform the transfer of {{ amount }} USD as dividends to customer {{ customer }} and Dwolla ID {{ customerDwollaId }} failed',
                        },
                    },
                    report: {
                        subject: '{{ clientName }}: Dividends Confirmed',
                        success: '[Investment Platform]: You have successfully published dividends to your investors',
                        failed: '[Investment Platform]: You have published dividends to your investors, but some of payments failed',
                    },
                },
            },
            report: {
                sendToNCEscrowOPS: {
                    csv: {
                        achId: 'Individual ACH ID',
                        tradeId: 'Trade ID',
                        investorName: 'Investor name',
                        amount: 'Amount',
                        offeringName: 'Offering name',
                        status: 'Status',
                    },
                    statuses: {
                        [BazaNcReportStatus.PendingPayment]: 'Pending Payment',
                        [BazaNcReportStatus.ReadyToReport]: 'Ready to Report',
                        [BazaNcReportStatus.Reported]: 'Reported',
                    },
                },
            },
        },
        purchaseFlow: {
            purchaseFlowStats: {
                youCanPurchaseUpTo: 'You can purchase up to {{ canPurchase }} shares.',
                onlyNumberOfSharesRemaining: 'Only {{ canPurchase }} number of shares remaining for purchase!',
                youHaveMadePreviousPurchase:
                    'You have made a previous purchase, and may only purchase {{ canPurchase }} more shares before reaching your maximum for this offering.',
                noSharesAvailable: 'No shares available for purchase.',
                youReachedMaximumAmount: 'You have reached the maximum amount of shares per user.',
            },
        },
    },
};
