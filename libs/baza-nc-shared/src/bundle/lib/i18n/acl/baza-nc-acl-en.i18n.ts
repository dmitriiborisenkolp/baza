import { BazaNorthCapitalAcl, BazaNorthCapitalAclGroup } from '../../acl';

type BazaNcAclEnI18n = {
    [BazaNorthCapitalAclGroup.BazaNorthCapital]: {
        group: string;
        nodes: {
            [key in keyof typeof BazaNorthCapitalAcl]: string;
        };
    };
};

export const bazaNcAclEnI18n: BazaNcAclEnI18n = {
    [BazaNorthCapitalAclGroup.BazaNorthCapital]: {
        group: 'North Capital',
        nodes: {
            [BazaNorthCapitalAcl.BazaNcInvestorAccount]: 'Investor account',
            [BazaNorthCapitalAcl.BazaNcInvestorAccountManagement]: 'Investor account management',
            [BazaNorthCapitalAcl.BazaNcTransactions]: 'Transactions',
            [BazaNorthCapitalAcl.BazaNcTransactionsManagement]: 'Transactions management',
            [BazaNorthCapitalAcl.BazaNcOfferings]: 'Offerings',
            [BazaNorthCapitalAcl.BazaNcKycLogs]: 'KYC/AML logs',
            [BazaNorthCapitalAcl.BazaNcSync]: 'Sync tools (for Investor Account/Transaction)',
            [BazaNorthCapitalAcl.BazaNcTaxDocument]: 'Tax Documents',
            [BazaNorthCapitalAcl.BazaNcDividend]: 'Dividends',
            [BazaNorthCapitalAcl.BazaNcDividendManagement]: 'Dividends management',
            [BazaNorthCapitalAcl.BazaNcDividendTransaction]: 'Dividend transactions',
            [BazaNorthCapitalAcl.BazaNcDividendTransactionManagement]: 'Dividend transaction management',
            [BazaNorthCapitalAcl.BazaNcDividendTransactionConfirm]: 'Dividend transaction confirmation',
            [BazaNorthCapitalAcl.BazaNcReport]: 'Daily Reports',
        },
    },
};
