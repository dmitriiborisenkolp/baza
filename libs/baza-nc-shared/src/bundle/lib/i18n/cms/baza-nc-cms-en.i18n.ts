import { bazaNcAclEnI18n } from '../acl/baza-nc-acl-en.i18n';
import { bazaNcOfferingCmsEnI18n } from '../../../../lib/offering';
import { bazaNcAccountVerificationCmsEnI18n } from '../../../../lib/account-verification';
import { bazaNcInvestorAccountCmsEnI18n } from '../../../../lib/investor-account';
import { bazaNcTransactionCmsEnI18n } from '../../../../lib/transaction';
import { bazaNcPurchaseFlowCmsI18n } from '../../../../lib/purchase-flow';
import { bazaNcTaxDocumentCmsEnI18n } from '../../../../lib/tax-document';
import { bazaNcUiCmsEnI18n } from '../../../../lib/ui';
import { bazaNcDividendCmsEnI18n } from '../../../../lib/dividend/src';
import { bazaNcReportCmsI18n } from '../../../../lib/report';

export const bazaNcCmsEnI18n = {
    __acl: {
        ...bazaNcAclEnI18n,
    },
    'baza-nc': {
        title: 'North Capital',
        _: {
            validators: {
                bazaNcCharsetValidator: 'There are unacceptable symbols in input',
                bazaNcCharsetValidatorDocuSign: 'There are unacceptable symbols in DocuSign template',
                bazaNcSchemaDuplicatedTab: 'Schema tab title already exists',
                bazaNcSchemaDuplicatedField: 'Schema field already exists',
            },
            types: {
                offeringStatus: {
                    open: 'Open',
                    'coming-soon': 'Coming Soon',
                    closed: 'Closed',
                },
                offeringHealthCheckIssue: {
                    enum: {
                        MissingInvestorAccount: 'Investor Account was not created and Trade will not be counted to total Percents Funded',
                        MissingTrade: 'Trade is missing in local DB',
                        TradeDataMismatch: 'Unit Price, Total Shares or Total Amount is not synced',
                        TradeStatusMismatch: 'Trade Status Mismatch',
                        UnknownTrade: 'Trade exists in CMS but does not actually exists in CMS',
                        TradeAssignedToWrongInvestorAccount: 'Trade is owned by wrong Investor Account',
                        TradeCancelledButStillInDatabase: 'Trade is cancelled and still exists in local DB',
                    },
                    action: {
                        MissingInvestorAccount:
                            'Find North Capital Account and Party ID of Trade in NC Client Admin panel and add Investor Account with CMS',
                        MissingTrade: 'Sync transactions of offering',
                        TradeDataMismatch: 'Sync transactions of offering',
                        TradeStatusMismatch: 'Sync transactions of offering',
                        UnknownTrade: 'Sync transactions of offering',
                        TradeAssignedToWrongInvestorAccount: 'Sync transactions of offering',
                        TradeCancelledButStillInDatabase: 'Sync transactions of offering',
                    },
                },
            },
        },
        ...bazaNcInvestorAccountCmsEnI18n,
        ...bazaNcAccountVerificationCmsEnI18n,
        ...bazaNcTransactionCmsEnI18n,
        ...bazaNcOfferingCmsEnI18n,
        ...bazaNcPurchaseFlowCmsI18n,
        ...bazaNcTaxDocumentCmsEnI18n,
        ...bazaNcDividendCmsEnI18n,
        ...bazaNcUiCmsEnI18n,
        ...bazaNcReportCmsI18n,
    },
};
