export enum BazaNorthCapitalAclGroup {
    BazaNorthCapital = 'BazaNorthCapital',
}

export enum BazaNorthCapitalAcl {
    BazaNcInvestorAccount = 'BazaNcInvestorAccount',
    BazaNcInvestorAccountManagement = 'BazaNcInvestorAccountManagement',
    BazaNcTransactions = 'BazaNcTransactions',
    BazaNcTransactionsManagement = 'BazaNcTransactionsManagement',
    BazaNcOfferings = 'BazaNcOfferings',
    BazaNcKycLogs = 'BazaNcKycLogs',
    BazaNcSync = 'BazaNcSync',
    BazaNcTaxDocument = 'BazaNcTaxDocument',
    BazaNcDividend = 'BazaNcDividend',
    BazaNcDividendManagement = 'BazaNcDividendManagement',
    BazaNcDividendTransaction = 'BazaNcDividendTransaction',
    BazaNcDividendTransactionManagement = 'BazaNcDividendTransactionManagement',
    BazaNcDividendTransactionConfirm = 'BazaNcDividendTransactionConfirm',
    BazaNcReport = 'BazaNcReport',
}
