export enum BazaNcIssuerErrorCodes {
    BazaNcIssuerNotDefined = 'BazaNcIssuerNotDefined',
    BazaNcIssuerNotFound = 'BazaNcIssuerNotFound',
}

export const bazaNcIssuerErrorCodesI18n: Record<BazaNcIssuerErrorCodes, string> = {
    [BazaNcIssuerErrorCodes.BazaNcIssuerNotDefined]: 'Issuer ID for predefined strategy is not available',
    [BazaNcIssuerErrorCodes.BazaNcIssuerNotFound]: 'Issuer Not Found',
};
