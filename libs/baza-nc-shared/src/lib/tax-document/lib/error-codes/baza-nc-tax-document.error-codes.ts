export enum BazaNcTaxDocumentErrorCodes {
    BazaNcTaxDocumentsNotFound = 'BazaNcTaxDocumentsNotFound',
}

export const bazaNcTaxDocumentsErrorCodesI18n = {
    [BazaNcTaxDocumentErrorCodes.BazaNcTaxDocumentsNotFound]: 'Tax Document not found',
};
