export const bazaNcTaxDocumentCmsEnI18n = {
    taxDocument: {
        components: {
            bazaNcTaxDocument: {
                title: ' {{ fullName }} / Tax Documents',
                fields: {
                    id: 'ID',
                    dateCreatedAt: 'Date Created At',
                    dateUpdatedAt: 'Date Updated At',
                    title: 'Title',
                    description: 'Description',
                    fileS3Key: 'File',
                    isPublished: 'Is Published?',
                },
                labels: {
                    fileS3Key: 'Attach PDF up to {{ maxSizeMb }}MB size.',
                },
                actions: {
                    download: 'Download',
                    create: 'Upload new Tax Document',
                    update: 'Edit Tax Document',
                    delete: {
                        title: 'Delete Tax Document',
                        confirm: 'Do you really want to delete Tax Document "{{ title }}"?',
                        success: 'Tax Document "{{ title }}" deleted',
                        failed: 'Failed to delete Tax Document "{{ title }}"',
                    },
                },
            },
        },
    },
};
