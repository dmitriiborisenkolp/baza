export enum BazaNcSyncErrorCodes {
    BazaNcSyncAccountOrPartyNotFound = 'BazaNcSyncAccountOrPartyNotFound',
    BazaNcSyncPartyIsNotOwnedByAccount = 'BazaNcSyncPartyIsNotOwnedByAccount',
}
