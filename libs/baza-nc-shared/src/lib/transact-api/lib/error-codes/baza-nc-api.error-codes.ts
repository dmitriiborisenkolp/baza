export enum BazaNcApiErrorCodes {
    BazaNcRequestFailed = 'BazaNcRequestFailed',
    BazaNcCharacterIsNotAllowed = 'BazaNcCharacterIsNotAllowed',
}

/**
 * You can override default error message with `bazaAppExceptionOverrideMessage` helper
 *
 * @see bazaAppExceptionOverrideMessage
 */
export const bazaNcApiErrorCodesI18n = {
    [BazaNcApiErrorCodes.BazaNcRequestFailed]: 'Failed to request external resource',
    [BazaNcApiErrorCodes.BazaNcCharacterIsNotAllowed]: 'Invalid characters in request body',
};
