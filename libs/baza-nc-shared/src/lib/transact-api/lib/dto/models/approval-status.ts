export enum ApprovalStatus {
    Pending = 'Pending',
    Approved = 'Approved',
    NotApproved = 'Not Approved',
}
