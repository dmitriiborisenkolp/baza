import { NorthCapitalFloatAsString } from '../common-types/north-capital-float-as-string';
import { OfferingId } from './offering-id';
import { OfferingPurchasedDetailsHistoryEntry } from './offering-purchased-details-history-entry';


export class OfferingPurchasedDetails {
    offeringId: OfferingId;

    totalAmountRaised: number;

    totalShares: number;

    totalSharesPurchased: number;

    totalSharesLeft: NorthCapitalFloatAsString;

    balanceAmount: NorthCapitalFloatAsString;

    offering_purchase_history: OfferingPurchasedDetailsHistoryEntry[];
}
