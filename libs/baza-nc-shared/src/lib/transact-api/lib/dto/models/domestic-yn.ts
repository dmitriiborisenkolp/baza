export enum DomesticYN {
    DomesticAccount = 'domestic_account',
    InternationalAccount = 'international_account',
}
