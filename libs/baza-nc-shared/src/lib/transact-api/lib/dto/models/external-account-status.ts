export enum ExternalAccountStatus {
    Deleted = 'Deleted',
    NotDeleted = 'Not Deleted',
}
