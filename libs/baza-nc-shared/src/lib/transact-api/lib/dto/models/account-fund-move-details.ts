import { NorthCapitalAccountId } from './account-id';
import { TradeId } from './trade-id';
import { OfferingId } from './offering-id';
import { NorthCapitalFloatAsString } from '../common-types/north-capital-float-as-string';
import { RefNum } from './ref-num';
import { TransactionStatus } from './transaction-status';
import { RoutingNumberStatus } from './routing-number-status';
import { FundStatus } from './fund-status';
import { NorthCapitalDateTime } from '../common-types';

export class AccountFundMoveDetails {
    accountId: NorthCapitalAccountId;

    tradeId: TradeId;

    offeringId: OfferingId;

    Bankname: string;

    totalAmount: NorthCapitalFloatAsString;

    Accountnumber: string;

    Routingnumber: string;

    RefNum: RefNum;

    Accountfullname: string;

    transactionstatus: TransactionStatus;

    routingNumberStatus: RoutingNumberStatus;

    fundStatus: FundStatus;

    errors: unknown;

    createdDate: NorthCapitalDateTime;
}
