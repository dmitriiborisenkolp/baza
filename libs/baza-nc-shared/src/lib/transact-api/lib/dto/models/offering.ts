import { IssuerId } from './issuer-id';
import { OfferingId } from './offering-id';
import { IssuerType } from './issuer-type';
import { NorthCapitalFloatAsString } from '../common-types/north-capital-float-as-string';
import { NorthCapitalDate } from '../common-types';
import { OfferingStatus } from './offering-status';

export class Offering {
    issuerId: IssuerId;
    issueName: string;
    issueType: IssuerType;
    offeringId: OfferingId;
    targetAmount: NorthCapitalFloatAsString;
    minAmount: NorthCapitalFloatAsString;
    maxAmount: NorthCapitalFloatAsString;
    unitPrice: NorthCapitalFloatAsString;
    totalShares: NorthCapitalFloatAsString;
    remainingShares: NorthCapitalFloatAsString;
    startDate: NorthCapitalDate;
    endDate: NorthCapitalDate;
    offeringStatus: OfferingStatus;
    offeringText: NorthCapitalFloatAsString;
    stampingText: NorthCapitalFloatAsString;
}
