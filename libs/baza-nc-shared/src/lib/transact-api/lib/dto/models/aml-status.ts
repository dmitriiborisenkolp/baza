// Warning: it can be lowercase or capitalized.
// Consider to capitalize or .toUpperCase() values before comparing values

export enum AmlStatus {
    Pending = 'Pending',
    AutoApproved = 'Auto Approved',
    ManuallyApproved = 'Manually Approved',
    Disapproved = 'Disapproved',
}
