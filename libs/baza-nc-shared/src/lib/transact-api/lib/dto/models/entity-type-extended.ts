// Take create: there is EntityType also!

export enum EntityTypeExtended {
    Corporation = 'Corporation',
    IrrevocableTrust = 'Irrevocable Trust',
    LimitedLiabilityCompany = 'Limited Liability Company',
    LimitedPartnership = 'Limited Partnership',
    RevocableTrust = 'Revocable Trust',
}