export enum Allow {
    Pending = 'Pending',
    Income = 'income',
    Assets = 'assets',
    AllPartiesAccredited = 'all_parties_accredited',
}
