export enum InvestmentObjective {
    PrimarilyOnCapitalPreservation = 'Primarily on Capital Preservation',
    BothCapitalPreservationAndGrowth = 'Both Capital Preservation and Growth',
    PrimarilyFocusedOnGrowth = 'Primarily Focused on growth',
}
