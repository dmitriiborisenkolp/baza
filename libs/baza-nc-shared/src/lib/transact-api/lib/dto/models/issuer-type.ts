export enum IssuerType {
    Equity = 'Equity',
    Debt = 'Debt',
    Hybrid = 'Hybrid',
    Fund = 'Fund',
}