import type { NorthCapitalDate, NorthCapitalDateTime, NorthCapitalNumberAsString } from '../common-types';
import { NorthCapitalFloatAsString } from '../common-types/north-capital-float-as-string';
import { TransactionStatus } from './transaction-status';
import { RRApprovalStatus } from './rr-approval-status';
import { PrincipalApprovalStatus } from './principal-approval-status';


export class PurchaseDetails {
    tradeId: NorthCapitalNumberAsString;

    transactionId: NorthCapitalNumberAsString;

    transactionAmount: NorthCapitalFloatAsString;

    transactionDate: NorthCapitalDateTime;

    transactionStatus: TransactionStatus;

    RRApprovalStatus: RRApprovalStatus;

    RRName: string;

    RRApprovalDate: NorthCapitalDate;

    PrincipalApprovalStatus: PrincipalApprovalStatus;

    PrincipalName: string;

    PrincipalDate: NorthCapitalDate;
}
