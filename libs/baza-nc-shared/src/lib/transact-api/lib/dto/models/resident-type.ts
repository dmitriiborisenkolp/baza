export enum ResidentType {
    DomesticAccount = 'domestic account',
    InternationalAccount = 'international account',
}
