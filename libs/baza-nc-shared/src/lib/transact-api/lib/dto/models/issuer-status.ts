export enum IssuerStatus {
    Pending = 'Pending',
    Approved = 'Approved',
    NotApproved = 'Not Approved',
}
