import type { IssuerId } from './issuer-id';
import type { OfferingId } from './offering-id';
import type { IssuerType } from './issuer-type';
import type { NorthCapitalDate, NorthCapitalDateTime, NorthCapitalNumberAsString } from '../common-types';
import type { OfferingStatus } from './offering-status';

export class OfferingDetailsFull {
    issuerId: IssuerId;
    offeringId: OfferingId;
    developerAPIKey: string;
    issueName: string;
    issueType: IssuerType;
    targetAmount: NorthCapitalNumberAsString;
    minAmount: NorthCapitalNumberAsString;
    maxAmount: NorthCapitalNumberAsString;
    unitPrice: NorthCapitalNumberAsString;
    totalShares: NorthCapitalNumberAsString;
    remainingShares: NorthCapitalNumberAsString;
    startDate: NorthCapitalDate;
    endDate: NorthCapitalDate;
    offeringStatus: OfferingStatus;
    offeringText: string;
    stampingText: string;
    createdDate: NorthCapitalDateTime;
    createdIPAddress: string;
}
