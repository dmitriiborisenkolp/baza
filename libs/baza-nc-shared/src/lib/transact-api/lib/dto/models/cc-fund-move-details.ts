import { NorthCapitalAccountId } from './account-id';
import { TradeId } from './trade-id';
import { OfferingId } from './offering-id';
import { NorthCapitalFloatAsString } from '../common-types/north-capital-float-as-string';
import { RefNum } from './ref-num';
import { TransactionStatus } from './transaction-status';
import { FundStatus } from './fund-status';
import { NorthCapitalDateTime } from '../common-types';

export class CCFundMoveDetails {
    accountId: NorthCapitalAccountId;

    tradeId: TradeId;

    offeringId: OfferingId;

    totalAmount: NorthCapitalFloatAsString;

    RefNum: RefNum;

    transactionstatus: TransactionStatus;

    fundStatus: FundStatus;

    errors: unknown;

    createdDate: NorthCapitalDateTime;
}
