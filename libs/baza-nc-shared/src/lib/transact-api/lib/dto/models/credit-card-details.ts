import { NorthCapitalAccountId } from './account-id';
import { NorthCapitalNumberAsString } from '../common-types';


export class CreditCardDetails {
    accountId: NorthCapitalAccountId;

    creditCardName: string;

    creditCardNumber: NorthCapitalNumberAsString;

    expirationDate: NorthCapitalNumberAsString;

    cvvNumber: NorthCapitalNumberAsString;
}
