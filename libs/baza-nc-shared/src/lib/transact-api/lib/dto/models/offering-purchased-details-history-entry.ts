import { TradeId } from './trade-id';
import { NorthCapitalAccountId } from './account-id';
import { OfferingId } from './offering-id';
import { NorthCapitalFloatAsString } from '../common-types/north-capital-float-as-string';
import { NorthCapitalDateTime } from '../common-types';


export class OfferingPurchasedDetailsHistoryEntry {
    tradeId: TradeId;

    accountId: NorthCapitalAccountId;

    offeringId: OfferingId;

    totalAmount: NorthCapitalFloatAsString;

    totalShares: NorthCapitalFloatAsString;

    purchaseDate: NorthCapitalDateTime;

    accountName: string;

    offeringTotalShares: NorthCapitalFloatAsString;

    targetAmount: NorthCapitalFloatAsString;

    remainingShares: NorthCapitalFloatAsString;
}
