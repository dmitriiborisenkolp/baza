import { AccountType } from './account-type';
import { EntityType } from './entity-type';
import { ResidentType } from './resident-type';
import { KYCStatus } from './kyc-status';
import { AmlStatus } from './aml-status';
import { SuitabilityScore } from './suitability-score';
import { AccreditedStatus } from './accredited-status';
import { AccreditedInvestor } from './accredited-investor';
import { ApprovalStatus } from './approval-status';
import type {
    NorthCapitalDate,
    NorthCapitalDateTime,
    NorthCapitalNumberAsString,
    NorthCapitalYesNoBoolean
} from '../common-types';
import { NorthCapitalAccountId } from './account-id';
import { OrderStatus } from './order-status';
import { EsignStatus } from './esign-status';
import { NorthCapitalFloatAsString } from '../common-types/north-capital-float-as-string';


export class AccountDetails {
    accountId: NorthCapitalAccountId;

    accountName: string;

    type: AccountType;

    entityType: EntityType;

    residentType: ResidentType;

    dob: NorthCapitalDate;

    address1: string;

    address2: string;

    city: string;

    state: string;

    zip: NorthCapitalNumberAsString;

    country: string;

    email: string;

    phone: NorthCapitalNumberAsString;

    taxID: NorthCapitalNumberAsString;

    kycStatus: KYCStatus;

    kycDate: NorthCapitalDateTime;

    amlStatus: AmlStatus;

    amlDate;

    suitabilityScore: SuitabilityScore;

    suitabilityDate: NorthCapitalDateTime;

    suitabilityApprover: string;

    accreditedStatus: AccreditedStatus;

    accreditedInvestor: AccreditedInvestor;

    accreditedInvestorDate: NorthCapitalDate;

    '506cLimit': NorthCapitalNumberAsString;

    accountTotalLimit: NorthCapitalNumberAsString;

    singleInvestmentLimit: NorthCapitalNumberAsString;

    associatedAC: NorthCapitalYesNoBoolean;

    syndicate: NorthCapitalYesNoBoolean;

    tags: string;

    notes: string;

    approvalStatus: ApprovalStatus;

    approvalPrincipal: string;

    approvalLastReview: NorthCapitalDate;

    field1: string;

    field2: string;

    field3: string;

    issueName: string;

    totalAmount: NorthCapitalFloatAsString;

    esignStatus: EsignStatus;

    trade_developerAPIKey: string;

    trade_createdDate: string;

    orderStatus: OrderStatus;
}
