export enum Education {
    HighSchoolOrGED = 'High School or GED',
    FourYearCollegeOrUniversity = '4 Year College or University',
    GraduateDegree = 'Graduate Degree',
    Other = 'Other',
}
