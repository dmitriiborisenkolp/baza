import { IssuerId } from './issuer-id';
import { EscrowAccountStatus } from './escrow-account-status';


export class FinancialEscrowDetails {
    issuerId: IssuerId;

    escrowAccountStatus: EscrowAccountStatus;
}
