export enum SuitabilityScore {
    Score1 = 1,
    Score2 = 2,
    Score3 = 3,
    Score4 = 4,
    Score5 = 5,
}
