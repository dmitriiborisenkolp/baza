import { NorthCapitalNumberAsString } from '../common-types';
import { AmlRestriction } from './aml-restriction';


export class AmlResponse {
    'id-number': NorthCapitalNumberAsString;

    restriction: AmlRestriction;
}
