export type TransactionStatus =
    | 'PENDING'
    | 'CREATED'
    | 'DECLINED'
    | 'CANCELLED'
    | 'APPROVED'
    | 'VOID'
    | 'VOIDED'
    | 'RETURNED';

export function isTransactionStatusCancelled(transactionStatus: TransactionStatus): boolean {
    return ['CREATED', 'DECLINED', 'CANCELLED', 'VOID', 'VOIDED', 'RETURNED'].includes(transactionStatus);
}
