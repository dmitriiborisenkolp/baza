export enum UsState {
    /**
     * U.S. States
     */
    AL = 'AL',
    AK = 'AK',
    AZ = 'AZ',
    AR = 'AR',
    CA = 'CA',
    CT = 'CT',
    DE = 'DE',
    FL = 'FL',
    GA = 'GA',
    HI = 'HI',
    ID = 'ID',
    IL = 'IL',
    IN = 'IN',
    IA = 'IA',
    KS = 'KS',
    KY = 'KY',
    LA = 'LA',
    ME = 'ME',
    MD = 'MD',
    MA = 'MA',
    MI = 'MI',
    MN = 'MN',
    MS = 'MS',
    MO = 'MO',
    MT = 'MT',
    NE = 'NE',
    NV = 'NV',
    NH = 'NH',
    NJ = 'NJ',
    NM = 'NM',
    NY = 'NY',
    NC = 'NC',
    ND = 'ND',
    OH = 'OH',
    OK = 'OK',
    OR = 'OR',
    PA = 'PA',
    RI = 'RI',
    SC = 'SC',
    SD = 'SD',
    TN = 'TN',
    TX = 'TX',
    UT = 'UT',
    VT = 'VT',
    VA = 'VA',
    WA = 'WA',
    WV = 'WV',
    WI = 'WI',
    WY = 'WY',
    CO = 'CO',
    DC = 'DC',

    /**
     * Non-U.S. States
     */
    PR = 'PR', // Puerto Rico
    VI = 'VI', // Virgin Islands, U.S.
    GU = 'GU', // Guam
    MP = 'MP', // Northern Mariana Islands
    AS = 'AS', // American Samoa
}

/**
 * U.S.-only States List
 */
export const US_STATES_PRIMARY: Array<UsState> = [
    UsState.AL,
    UsState.AK,
    UsState.AZ,
    UsState.AR,
    UsState.CA,
    UsState.CT,
    UsState.DE,
    UsState.FL,
    UsState.GA,
    UsState.HI,
    UsState.ID,
    UsState.IL,
    UsState.IN,
    UsState.IA,
    UsState.KS,
    UsState.KY,
    UsState.LA,
    UsState.ME,
    UsState.MD,
    UsState.MA,
    UsState.MI,
    UsState.MN,
    UsState.MS,
    UsState.MO,
    UsState.MT,
    UsState.NE,
    UsState.NV,
    UsState.NH,
    UsState.NJ,
    UsState.NM,
    UsState.NY,
    UsState.NC,
    UsState.ND,
    UsState.OH,
    UsState.OK,
    UsState.OR,
    UsState.PA,
    UsState.RI,
    UsState.SC,
    UsState.SD,
    UsState.TN,
    UsState.TX,
    UsState.UT,
    UsState.VT,
    UsState.VA,
    UsState.WA,
    UsState.WV,
    UsState.WI,
    UsState.WY,
    UsState.CO,
    UsState.DC,
];

/**
 * Non-U.S. US States List
 */
export const US_STATES_SECONDARY: Array<UsState> = [UsState.VI, UsState.PR, UsState.GU, UsState.AS, UsState.MP];

/**
 * List of Countries which are considered as U.S.-supportable (i.e. could pass KYC & Dwolla Verification)
 */
export const US_STATES_NON_US_COUNTRIES: Array<string> = [
    'Puerto Rico',
    'Virgin Islands, U.S.',
    'Guam',
    'Northern Mariana Islands',
    'American Samoa',
];
