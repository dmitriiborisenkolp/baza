import { ExternalAccountDetails } from './external-account-details';
import { FundStatus } from './fund-status';


export class InvestorExternalAccountDetails extends ExternalAccountDetails {
    fundStatus: FundStatus;
}
