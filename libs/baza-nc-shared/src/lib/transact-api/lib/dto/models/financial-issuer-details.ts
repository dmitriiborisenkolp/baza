import { IssuerId } from './issuer-id';
import { NorthCapitalDateTime, NorthCapitalNumberAsString, NorthCapitalYesNoBoolean } from '../common-types';
import { EntityType } from './entity-type';
import { AccountTypeCheckingSaving } from './account-type-checking-saving';
import { IssuerStatus } from './issuer-status';
import { NorthCapitalIpAddress } from '../common-types/north-capital-ip-address';


export class FinancialIssuerDetails {
    issuerId: IssuerId;

    companyName: string;

    companyState: string;

    companyTaxID: NorthCapitalNumberAsString;

    entityType: EntityType;

    issuingCountry: string;

    accountType: AccountTypeCheckingSaving;

    accountMiddleInitial: string;

    socialSecurityNumber: string;

    residentUS: NorthCapitalYesNoBoolean;

    citizenUS: NorthCapitalYesNoBoolean;

    addressLine1: string;

    addressLine2: string;

    city: string;

    zip: NorthCapitalNumberAsString;

    country: string;

    additionalInfo: string;

    issuerStatus: IssuerStatus;

    createdDate: NorthCapitalDateTime;

    createdIpAddress: NorthCapitalIpAddress;
}
