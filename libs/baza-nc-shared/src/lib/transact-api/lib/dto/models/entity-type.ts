// Take create: there is EntityTypeExtended also!

export enum EntityType {
    Corporation = 'corporation',
    Irrevocable = 'irrevocable',
    LimitedPartnership = 'limited_partner',
    LLC = 'llc',
    Revocable_Trust = 'revocable_trust',
}
