import { NorthCapitalNumberAsString } from '../common-types';
import { KycSummaryResult } from './kyc-summary-result';
import { Questions } from './questions';


export class KycResponse {
    'id-number': NorthCapitalNumberAsString;

    'summary-result': {
        key: KycSummaryResult;
        message: string;
    };

    results: {
        key: string;
        message: string;
    };

    'idliveq-error': {
        key: string;
        message: string;
    }

    qualifiers: {
        qualifier: Array<{
            key: string;
            message: string;
        }>;
    };

    idnotescore: NorthCapitalNumberAsString;

    questions: Questions;
}
