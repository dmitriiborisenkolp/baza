import { OfferingId } from './offering-id';
import { OfferingStatus } from './offering-status';


export class OfferingDetails {
    offeringId: OfferingId;

    offeringStatus: OfferingStatus;
}
