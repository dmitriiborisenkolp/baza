import { NorthCapitalAccountId } from './account-id';
import { AccountTypeCheckingSaving } from './account-type-checking-saving';

export class ExternalAccountLinkDetails {
    accountId: NorthCapitalAccountId;
    refNum: string;
    account_number: string;
    routing_number: string;
    account_name: string;
    account_type: AccountTypeCheckingSaving;
    institutionId: string;
    institution_name: string;
}
