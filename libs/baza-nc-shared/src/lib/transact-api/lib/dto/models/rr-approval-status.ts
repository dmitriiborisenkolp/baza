export type RRApprovalStatus =
    | 'Pending'
    | 'Approved'
    | 'Disapproved'
    | 'Under Review';
