export enum AccreditedStatus {
    Pending = 'Pending',
    SelfAccredited = 'Self Accredited',
    VerifiedAccredited = 'Verified Accredited',
    NotAccredited = 'Not Accredited',
}
