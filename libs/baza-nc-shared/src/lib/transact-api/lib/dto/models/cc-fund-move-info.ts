import { FundStatus } from './fund-status';

export class CcFundMoveInfo {
    fundStatus: FundStatus;

    error: unknown;
}
