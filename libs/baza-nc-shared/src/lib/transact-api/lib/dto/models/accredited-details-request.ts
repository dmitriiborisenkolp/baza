import { NorthCapitalAccountId } from './account-id';
import { AiMethod } from './ai-method';
import { Allow } from './allow';
import { AiRequestStatus } from './ai-request-status';
import { AccreditedStatus } from './accredited-status';
import { NorthCapitalReverseDate } from '../common-types';


export class AccreditedDetailsRequest {
    airequestId: string;

    aiRequestStatus: AiRequestStatus;

    accountId: NorthCapitalAccountId;

    aiMethod: AiMethod;

    allow: Allow;

    aiDate: NorthCapitalReverseDate;

    reviewedBy: string;

    notes: string;

    accreditedStatus: AccreditedStatus;
}
