export class DocusignTemplateDetails {
    templateId: string;
    name: string;
    shared: 'true' | 'false';
    password: string;
    description: string;
    lastModified: string; // 2021-09-14T10:51:58.3430000Z
    created: string; // 2021-09-14T10:51:58.3430000Z
    pageCount: number;
    uri: string; // /templates/b521c91a-3684-4004-ae59-900e046fa24e
    folderName: string;
    folderId: string;
    folderUri: string;
    owner: {
        userName: string;
        userId: string;
        email: string;
    };
    emailSubject: string;
    emailBlurb: string;
    signingLocation: string;
    authoritativeCopy: 'true' | 'false'
    enforceSignerVisibility: 'true' | 'false'
    enableWetSign: 'true' | 'false'
    allowMarkup: 'true' | 'false'
    allowReassign: 'true' | 'false'
}
