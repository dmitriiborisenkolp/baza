import { AmlResponse } from './aml-response';
import { AmlStatus } from './aml-status';


export class PerformAmlPartyDetails {
    response: AmlResponse;

    amlStatus: AmlStatus;
}
