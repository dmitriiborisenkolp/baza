export class Questions {
    question: Question[];
}

export class Question {
    prompt: string;
    type: string;
    answer: string[];
}
