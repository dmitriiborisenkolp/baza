import { KycResponse } from './kyc-response';
import { AmlStatus } from './aml-status';
import { KYCStatus } from './kyc-status';

export class Kyc {
    response: KycResponse;
    kycstatus: KYCStatus;
    amlstatus: AmlStatus;
}
