export class TradeFinancialDetails {
    accountId: string;
    tradeId: string;
    offeringId: string;
    totalAmount: string;
    RefNum: string;
    fundStatus: string;
}
