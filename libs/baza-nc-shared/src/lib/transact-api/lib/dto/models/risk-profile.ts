export enum RiskProfile {
    Risk1 = 1, // Less risk
    Risk2 = 2,
    Risk3 = 3,
    Risk4 = 4,
    Risk5 = 5, // Most aggressive
}
