export enum ArchiveStatus {
    IsArchived = '1',
    IsNotArchived = '0',
}
