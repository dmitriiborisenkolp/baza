import type { NorthCapitalDateTime } from '../common-types';
import { NorthCapitalAccountId } from './account-id';
import { FundStatus } from './fund-status';

export class NcACHDetails {
    accountId: NorthCapitalAccountId;
    fundStatus: FundStatus;
    createdDate: NorthCapitalDateTime;
    developerAPIKey: string;
    accountName: string;
    clientName: string;
    clientId: string;
    issueName: string;
}
