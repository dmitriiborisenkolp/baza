export enum EsignStatus {
    NotSigned = 'NOTSIGNED',
    Signed = 'SIGNED',
}
