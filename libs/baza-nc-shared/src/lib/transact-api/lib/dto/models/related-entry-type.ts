export enum RelatedEntryType {
    IndivACParty = 'IndivACParty',
    EntityACParty = 'EntityACParty',
    Account = 'Account',
}
