import { Domicile } from './domicile';
import { EntityType } from './entity-type';
import type { NorthCapitalDate, NorthCapitalNumberAsString, NorthCapitalYesNoBoolean } from '../common-types';
import { KYCStatus } from './kyc-status';
import { AmlStatus } from './aml-status';
import { PartyId } from './party-id';


export class EntityDetails {
    partyId: PartyId;

    domicile: Domicile;

    entityName: string;

    entityType: EntityType;

    entityDesc: string;

    ein: NorthCapitalNumberAsString;

    primCountry: string;

    primAddress1: string;

    primAddress2: string;

    primCity: string;

    primState: string;

    primZip: string;

    emailAddress: string;

    emailAddress2: string;

    phone: NorthCapitalNumberAsString;

    phone2: NorthCapitalNumberAsString;

    totalAssets: NorthCapitalNumberAsString;

    ownersAI: NorthCapitalYesNoBoolean;

    KYCstatus: KYCStatus;

    AMLstatus: AmlStatus;

    AMLdate: NorthCapitalDate;

    tags: string;

    notes: string;
}
