export enum AccountTypeCheckingSaving {
    Checking = 'Checking',
    Savings = 'Savings',
}
