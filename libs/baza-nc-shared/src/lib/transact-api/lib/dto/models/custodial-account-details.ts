import { NorthCapitalAccountId } from './account-id';
import { CustodialAccountRequestId } from './custodial-account-request-id';
import { ApprovalStatus } from './approval-status';
import { NorthCapitalDateTime } from '../common-types';
import { CustodialAccountStatus } from './custodial-account-status';


export class CustodialAccountDetails {
    accountId: NorthCapitalAccountId;

    custAccRequestID: CustodialAccountRequestId;

    custAccStatus: CustodialAccountStatus;

    createdDate: NorthCapitalDateTime;

    approvalStatus: ApprovalStatus;
}
