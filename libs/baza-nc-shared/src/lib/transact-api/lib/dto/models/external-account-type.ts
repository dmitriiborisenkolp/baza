export enum ExternalAccountType {
    Account = 'Account',
    Issuer = 'Issuer',
}
