export enum CardType {
    Visa = 'VI',
    MasterCard = 'MC',
    DI = 'DI',
}
