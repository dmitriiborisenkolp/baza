import { IssuerId } from './issuer-id';
import { IssuerStatus } from './issuer-status';


export class IssuerDetails {
    issuerId: IssuerId;

    issuerStatus: IssuerStatus;

    issuerName: string;

    firstName: string;

    lastName: string;

    email: string;

    phoneNumber: string | null;
}
