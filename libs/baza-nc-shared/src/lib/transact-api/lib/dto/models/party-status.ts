import { PartyId } from './party-id';
import { AmlStatus } from './aml-status';
import type { NorthCapitalDate } from '../common-types';


export class PartyStatus {
    PartyID: PartyId;

    'AML status': AmlStatus;

    'AML date': NorthCapitalDate;
}
