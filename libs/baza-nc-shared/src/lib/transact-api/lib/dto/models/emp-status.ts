export enum EmpStatus {
    Employed = 'Employed',
    NotEmployed = 'Not Employed',
    Retired = 'Retired',
    Student = 'Student',
}
