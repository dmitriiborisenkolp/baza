export enum FundStatus {
    Pending = 'Pending',
    Settled = 'Settled',
    Returned = 'Returned',
    Submitted = 'Submitted',
    VerificationPending = 'VerificationPending',
    Voided = 'Voided',
}
