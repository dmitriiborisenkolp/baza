export enum TransactionType {
    ACH = 'ACH',
    WIRE = 'WIRE',
    CHECK = 'CHECK',
    TBD = 'TBD',
    ETH = 'ETH',
    BTC = 'BTC',
    CreditCard = 'CreditCard',
}
