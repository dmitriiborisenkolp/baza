import { TransactionStatus } from './transaction-status';
import { FundStatus } from './fund-status';

export interface TradeStatus {
    transactionstatus: TransactionStatus;
    fundStatus: FundStatus;
}

export type TradeStatusMap<T extends TradeStatus = TradeStatus> = Record<string, T>;
