import { NorthCapitalAccountId } from './account-id';
import { NorthCapitalNumberAsString, NorthCapitalReverseDate } from '../common-types';
import { DocumentId } from './document-id';
import { EsignStatus } from './esign-status';


export class DocumentDetails {
    accountId: NorthCapitalAccountId;

    documentId: DocumentId;

    documentTitle: string;

    documentName?: string;

    documentKey?: string;

    documentFileReferenceCode?: string;

    documentReferenceCode?: string;

    url?: string;

    offeringId?: string;

    createdDate: NorthCapitalReverseDate;

    templateId?: string;

    templateName?: string;

    templateNameID?: NorthCapitalNumberAsString;

    esignstatus?: EsignStatus;
}
