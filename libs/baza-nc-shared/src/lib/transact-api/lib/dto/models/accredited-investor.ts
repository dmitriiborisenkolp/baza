export enum AccreditedInvestor {
    Pending = 'Pending',
    SelfAccredited = 'Self Accredited',
    VerifiedAccredited = 'Verified Accredited',
    NotAccredited = 'Not Accredited',
}
