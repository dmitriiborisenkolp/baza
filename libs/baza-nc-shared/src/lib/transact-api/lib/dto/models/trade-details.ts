import { OrderStatus } from './order-status';
import { TransactionStatus } from './transaction-status';

export class TradeDetails {
    partyId: string;
    offeringId: string;
    orderStatus: OrderStatus;
    RRApprovalStatus: string;
    RRName: string;
    RRApprovalDate: string;
    PrincipalApprovalStatus: string;
    PrincipalName: string;
    PrincipalDate: string;
    field1: string;
    field2: string;
    field3: string;
    closeId: string;
    accountId: string;
    tradeId: string;
    orderId: string;
    transactionstatus?: TransactionStatus;
}
