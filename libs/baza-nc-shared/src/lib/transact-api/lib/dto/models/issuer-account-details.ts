import { IssuerId } from './issuer-id';
import { AccountTypeCheckingSaving } from './account-type-checking-saving';
import { EntityType } from './entity-type';
import { NorthCapitalYesNoBoolean } from '../common-types';
import { IssuerAccountStatus } from './issuer-account-status';


export class IssuerAccountDetails {
    issuerId: IssuerId;

    accountfirstName: string;

    accountlastName: string;

    accountMiddleInitial: string;

    accountType: AccountTypeCheckingSaving;

    companyName: string;

    companyState: string;

    entityType: EntityType;

    companyTaxID: string;

    socialSecurityNumber: string;

    dob: string;

    residentUS: NorthCapitalYesNoBoolean;

    citizenUS: NorthCapitalYesNoBoolean;

    addressLine1: string;

    addressLine2: string;

    city: string;

    state: string;

    zip: string;

    country: string;

    issuingCountry: string;

    additionalInfo: string;

    issuerAccountStatus: IssuerAccountStatus;
}
