import { NorthCapitalAccountId } from './account-id';
import { ExternalAccountType } from './external-account-type';
import { AccountTypeCheckingSaving } from './account-type-checking-saving';


export class ExternalAccountDetails {
    accountId: NorthCapitalAccountId;

    ExtBankname: string;

    ExtAccountfullname: string;

    Extnickname: string;

    ExtRoutingnumber: string;

    ExtAccountnumber: string;

    types: ExternalAccountType;

    accountType: AccountTypeCheckingSaving;
}
