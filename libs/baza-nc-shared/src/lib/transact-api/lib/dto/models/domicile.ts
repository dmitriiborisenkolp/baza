export enum Domicile {
    USCitizen = 'U.S. citizen',
    USResident = 'U.S. resident',
    NonResident = 'non-resident',
}
