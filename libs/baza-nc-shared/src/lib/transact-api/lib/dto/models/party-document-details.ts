import { NorthCapitalReverseDate } from '../common-types';
import { PartyId } from './party-id';
import { DocumentId } from './document-id';


export class PartyDocumentDetails {
    partyid: PartyId;

    documentid: DocumentId;

    documentTitle: string;

    documentFileName: string;

    documentFileReferenceCode: string;

    createdDate: NorthCapitalReverseDate;

    documentUrl: string;
}
