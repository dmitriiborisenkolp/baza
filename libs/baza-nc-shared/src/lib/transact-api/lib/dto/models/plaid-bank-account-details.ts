import { AccountTypeCheckingSaving } from './account-type-checking-saving';

export interface PlaidBankAccountDetails {
    AccountName: string;
    AccountNickName: string;
    AccountRoutingNumber: string;
    AccountNumber: string;
    accountType: AccountTypeCheckingSaving;
}
