import { IsNotEmpty, IsString } from 'class-validator';
import { TradeId } from '../models/trade-id';

export class GetTradeStatusRequest {
    @IsString()
    @IsNotEmpty()
    tradeId: TradeId;
}
