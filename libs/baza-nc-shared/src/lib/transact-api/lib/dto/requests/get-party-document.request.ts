import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { PartyId } from '../models/party-id';
import { DocumentId } from '../models/document-id';

export class GetPartyDocumentRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;

    @IsString()
    @IsOptional()
    documentId?: DocumentId;
}
