import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';

export class GetAchPendingIdRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
