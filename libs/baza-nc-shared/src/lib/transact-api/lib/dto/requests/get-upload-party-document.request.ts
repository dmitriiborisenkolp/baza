import { PartyId } from '../models/party-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetUploadPartyDocumentRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
