import { IsNotEmpty, IsString } from 'class-validator';
import { IssuerId } from '../models/issuer-id';

export class GetIssuerRequest {
    @IsString()
    @IsNotEmpty()
    issuerId: IssuerId;
}
