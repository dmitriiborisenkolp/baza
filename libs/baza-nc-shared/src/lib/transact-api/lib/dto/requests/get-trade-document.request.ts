import { TradeId } from '../models/trade-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetTradeDocumentRequest {
    @IsString()
    @IsNotEmpty()
    tradeId: TradeId;
}
