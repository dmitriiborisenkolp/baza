import { IsInt, IsNotEmpty, IsPositive } from 'class-validator';

export class ValidateAbaRoutingNumberRequest {
    @IsPositive()
    @IsInt()
    @IsNotEmpty()
    routingNumber: number;
}
