import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';

export class UpdateLinkExternalAccountRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
