import { RefNum } from '../models/ref-num';
import { IsNotEmpty, IsString } from 'class-validator';

export class RequestForVoidCcTransactionRequest {
    @IsString()
    @IsNotEmpty()
    RefNum: RefNum;
}
