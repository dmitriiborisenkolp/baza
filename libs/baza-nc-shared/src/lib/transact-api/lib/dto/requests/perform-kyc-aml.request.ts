import { PartyId } from '../models/party-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class PerformKycAmlRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
