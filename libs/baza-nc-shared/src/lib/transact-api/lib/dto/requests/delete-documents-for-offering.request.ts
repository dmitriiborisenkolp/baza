import { IsNotEmpty, IsString } from 'class-validator';

export class DeleteDocumentsForOfferingRequest {
    @IsString()
    @IsNotEmpty()
    documentId: string;
}
