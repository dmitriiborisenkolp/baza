import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';

export class LinkExternalAccountRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
