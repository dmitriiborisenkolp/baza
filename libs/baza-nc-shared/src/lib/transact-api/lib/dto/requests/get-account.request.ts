import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';


export class GetAccountRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
