import { PartyId } from '../models/party-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class ResendPartyAgreementRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
