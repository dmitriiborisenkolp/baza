import { NorthCapitalAccountId } from '../models/account-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetAccountTradeHistoryRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
