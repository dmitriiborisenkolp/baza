import { IsNotEmpty, IsString } from 'class-validator';

export class SearchEntityRequest {
    @IsString()
    @IsNotEmpty()
    searchKeyword: string;
}
