import { NorthCapitalAccountId } from '../models/account-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class SendCustodialAccountAgreementRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
