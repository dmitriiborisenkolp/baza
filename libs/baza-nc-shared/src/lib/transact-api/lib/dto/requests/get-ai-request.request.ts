import { NorthCapitalAccountId } from '../models/account-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetAiRequestRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
