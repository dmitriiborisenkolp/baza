import { IsNotEmpty, IsString } from 'class-validator';
import { PartyId } from '../models/party-id';

export class DeletePartyRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
