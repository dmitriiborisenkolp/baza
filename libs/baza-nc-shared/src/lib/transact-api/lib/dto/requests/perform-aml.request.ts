import { PartyId } from '../models/party-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class PerformAmlRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
