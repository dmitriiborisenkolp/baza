import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';

export class GetExternalFundMoveHistoryRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
