import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';

export class GetTradesForOfferingRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
