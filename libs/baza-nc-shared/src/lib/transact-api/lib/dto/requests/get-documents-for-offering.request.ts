import { OfferingId } from '../models/offering-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetDocumentsForOfferingRequest {
    @IsString()
    @IsNotEmpty()
    offeringId: OfferingId;
}
