import { TradeId } from '../models/trade-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetAllSignedDocumentRequest {
    @IsString()
    @IsNotEmpty()
    tradeId: TradeId;
}
