import { TemplateId } from '../models/template-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class DeleteSubscriptionsForOfferingRequest {
    @IsString()
    @IsNotEmpty()
    templateId: TemplateId;
}
