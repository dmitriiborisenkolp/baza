import { IsNotEmpty, IsString } from 'class-validator';
import { PartyId } from '../models/party-id';

export class DeleteEntityRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
