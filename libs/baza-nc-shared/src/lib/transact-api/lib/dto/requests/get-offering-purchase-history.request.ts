import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';

export class GetOfferingPurchaseHistoryRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
