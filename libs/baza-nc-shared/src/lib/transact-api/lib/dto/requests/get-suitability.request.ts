import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';

export class GetSuitabilityRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
