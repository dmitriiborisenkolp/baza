import { PartyId } from '../models/party-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class DeleteKycAmlRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
