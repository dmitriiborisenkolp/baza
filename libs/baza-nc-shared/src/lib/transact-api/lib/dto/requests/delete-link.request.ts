import { IsNotEmpty, IsString } from 'class-validator';

export class DeleteLinkRequest {
    @IsString()
    @IsNotEmpty()
    id: string;
}
