import { IsNotEmpty, IsString } from 'class-validator';

export class GetLinkRequest {
    @IsString()
    @IsNotEmpty()
    id: string;
}
