import { IsNotEmpty, IsString } from 'class-validator';
import { PartyId } from '../models/party-id';

export class GetEntityRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
