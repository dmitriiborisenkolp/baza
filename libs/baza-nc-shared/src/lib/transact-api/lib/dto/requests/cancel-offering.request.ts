import { OfferingId } from '../models/offering-id';
import { IsNotEmpty, IsString } from 'class-validator';


export class CancelOfferingRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
