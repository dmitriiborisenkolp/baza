import { PartyId } from '../models/party-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class SendPartyAgreementRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
