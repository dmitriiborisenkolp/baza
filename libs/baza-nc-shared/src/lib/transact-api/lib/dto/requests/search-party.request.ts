import { IsNotEmpty, IsString } from 'class-validator';

export class SearchPartyRequest {
    @IsString()
    @IsNotEmpty()
    searchKeyword: string;
}
