import { NorthCapitalAccountId } from '../models/account-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetAllLinksRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
