import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';

export class DeleteOfferingRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
