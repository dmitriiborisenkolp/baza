import { IsNotEmpty, IsString } from 'class-validator';
import { RefNum } from '../models/ref-num';

export class RequestForVoidACHRequest {
    @IsString()
    @IsNotEmpty()
    RefNum: RefNum;
}
