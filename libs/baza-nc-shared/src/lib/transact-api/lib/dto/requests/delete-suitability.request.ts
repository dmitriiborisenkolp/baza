import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';

export class DeleteSuitabilityRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
