import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';

export class GetOfferingRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
