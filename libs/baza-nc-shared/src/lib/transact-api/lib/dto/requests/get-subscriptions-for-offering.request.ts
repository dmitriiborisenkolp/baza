import { OfferingId } from '../models/offering-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetSubscriptionsForOfferingRequest {
    @IsString()
    @IsNotEmpty()
    offeringId: OfferingId;
}
