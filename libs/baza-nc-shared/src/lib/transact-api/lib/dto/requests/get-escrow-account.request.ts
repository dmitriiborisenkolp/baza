import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';

export class GetEscrowAccountRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
