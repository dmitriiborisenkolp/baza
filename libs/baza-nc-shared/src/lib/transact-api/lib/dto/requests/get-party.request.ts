import { IsNotEmpty, IsString } from 'class-validator';
import { PartyId } from '../models/party-id';

export class GetPartyRequest {
    @IsString()
    @IsNotEmpty()
    partyId: PartyId;
}
