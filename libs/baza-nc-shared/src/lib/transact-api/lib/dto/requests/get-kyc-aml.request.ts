import { PartyId } from '../models/party-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class GetKycAmlRequest {
    @IsNotEmpty()
    @IsString()
    partyId: PartyId;
}
