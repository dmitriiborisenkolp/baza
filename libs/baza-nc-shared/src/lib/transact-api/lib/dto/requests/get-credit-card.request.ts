import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';

export class GetCreditCardRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
