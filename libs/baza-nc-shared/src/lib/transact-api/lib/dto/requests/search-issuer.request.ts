import { IsNotEmpty, IsString } from 'class-validator';

export class SearchIssuerRequest {
    @IsString()
    @IsNotEmpty()
    searchKeyword: string;
}
