import { IsNotEmpty, IsString } from 'class-validator';

export class SearchAccountRequest {
    @IsString()
    @IsNotEmpty()
    searchKeyword: string;
}
