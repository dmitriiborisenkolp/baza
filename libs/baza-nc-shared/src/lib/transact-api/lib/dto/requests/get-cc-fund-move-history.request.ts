import { IsNotEmpty, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';

export class GetCcFundMoveHistoryRequest {
    @IsString()
    @IsNotEmpty()
    accountId: NorthCapitalAccountId;
}
