import { IssuerId } from '../models/issuer-id';
import { IsNotEmpty, IsString } from 'class-validator';

export class DeleteIssuerRequest {
    @IsString()
    @IsNotEmpty()
    issuerId: IssuerId;
}
