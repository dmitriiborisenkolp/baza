import { IsNotEmpty, IsString } from 'class-validator';
import { IssuerId } from '../models/issuer-id';

export class GetIssuerAccountRequest {
    @IsString()
    @IsNotEmpty()
    issuerId: IssuerId;
}
