import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';


export class DeleteEscrowAccountRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
