import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';

export class GetCustodianDetailsForOfferingRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
