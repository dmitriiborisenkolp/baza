import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';

export class CloseOfferingRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
