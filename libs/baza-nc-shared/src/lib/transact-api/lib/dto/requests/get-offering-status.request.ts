import { IsNotEmpty, IsString } from 'class-validator';
import { OfferingId } from '../models/offering-id';

export class GetOfferingStatusRequest {
    @IsNotEmpty()
    @IsString()
    offeringId: OfferingId;
}
