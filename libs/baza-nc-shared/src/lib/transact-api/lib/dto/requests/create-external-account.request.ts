import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { NorthCapitalAccountId } from '../models/account-id';
import { IssuerId } from '../models/issuer-id';
import { NorthCapitalIpAddress } from '../common-types/north-capital-ip-address';
import { AccountTypeCheckingSaving } from '../models/account-type-checking-saving';
import { ExternalAccountType } from '../models/external-account-type';

export class CreateExternalAccountRequest {
    @IsEnum(ExternalAccountType)
    @IsNotEmpty()
    types: ExternalAccountType;

    @IsString()
    @IsOptional()
    accountId?: NorthCapitalAccountId;

    @IsString()
    @IsOptional()
    issuerId?: IssuerId;

    @IsString()
    @IsNotEmpty()
    ExtAccountfullname: string;

    @IsString()
    @IsNotEmpty()
    Extnickname: string;

    @IsString()
    @IsNotEmpty()
    ExtRoutingnumber: string;

    @IsString()
    @IsNotEmpty()
    ExtAccountnumber: string;

    @IsString()
    @IsNotEmpty()
    updatedIpAddress: NorthCapitalIpAddress;

    @IsEnum(AccountTypeCheckingSaving)
    @IsOptional()
    accountType?: AccountTypeCheckingSaving;
}
