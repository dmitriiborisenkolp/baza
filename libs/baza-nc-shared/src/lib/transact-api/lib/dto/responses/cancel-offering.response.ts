import { NorthCapitalBaseResponse } from '../../common';
import { NorthCapitalNumberAsString } from '../common-types';


export class CancelOfferingResponse extends NorthCapitalBaseResponse {
    'Financial offering details': NorthCapitalNumberAsString;
}
