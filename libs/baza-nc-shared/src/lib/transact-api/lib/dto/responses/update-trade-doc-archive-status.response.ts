import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class UpdateTradeDocArchiveStatusResponse extends NorthCapitalBaseResponse {
    tradeDocumentDetails: DocumentDetails[];
}
