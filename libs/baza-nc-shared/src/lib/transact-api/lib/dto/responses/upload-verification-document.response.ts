import { NorthCapitalBaseResponse } from '../../common';


export class UploadVerificationDocumentResponse extends NorthCapitalBaseResponse {
    document_details: string; // "Document has been uploaded Successfully"
}
