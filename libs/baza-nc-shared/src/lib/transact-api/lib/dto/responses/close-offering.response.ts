import { NorthCapitalBaseResponse } from '../../common';
import { NorthCapitalNumberAsString } from '../common-types';


export class CloseOfferingResponse extends NorthCapitalBaseResponse {
    OfferID: NorthCapitalNumberAsString;
}
