import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class GetAiDocumentResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
