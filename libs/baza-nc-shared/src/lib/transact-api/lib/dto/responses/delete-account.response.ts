import { NorthCapitalBaseResponse } from '../../common';


export class DeleteAccountResponse extends NorthCapitalBaseResponse {
    accountDetails: 'Account deleted successfully!';
}
