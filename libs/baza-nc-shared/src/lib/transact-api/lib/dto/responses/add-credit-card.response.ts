import { NorthCapitalBaseResponse } from '../../common';


export class AddCreditCardResponse extends NorthCapitalBaseResponse {
    creditcardDetails: 'Credit Card details added successfully';
}
