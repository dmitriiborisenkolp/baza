import { NorthCapitalBaseResponse } from '../../common';


export class DeleteSuitabilityResponse extends NorthCapitalBaseResponse {
    accountDetails: 'Account deleted successfully!';
}
