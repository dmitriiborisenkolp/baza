import { NorthCapitalBaseResponse } from '../../common';
import { UrlString } from '../models/url-string';


export class LinkExternalAccountResponse extends NorthCapitalBaseResponse {
    accountDetails: UrlString; // Example: https://api-sandboxdash.norcapsecurities.com/apiscript/linkPlaidAccount.php?params=[param_value]
}
