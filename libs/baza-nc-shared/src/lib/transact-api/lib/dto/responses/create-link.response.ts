import { NorthCapitalBaseResponse } from '../../common';
import { LinkDetails } from '../models/link-details';


export class CreateLinkResponse extends NorthCapitalBaseResponse {
    linkDetails: [boolean, LinkDetails[]];
}
