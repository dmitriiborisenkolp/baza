import { NorthCapitalBaseResponse } from '../../common';
import { AccountDetails } from '../models/account-details';


export class CalculateSuitabilityResponse extends NorthCapitalBaseResponse {
    accountDetails: AccountDetails[];
}
