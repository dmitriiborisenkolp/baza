import { NorthCapitalBaseResponse } from '../../common';
import { AccountFundMoveDetails } from '../models/account-fund-move-details';


export class CcFundMoveResponse extends NorthCapitalBaseResponse {
    transactionDetails: AccountFundMoveDetails;
}
