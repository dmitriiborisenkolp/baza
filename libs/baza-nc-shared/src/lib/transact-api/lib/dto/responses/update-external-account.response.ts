import { NorthCapitalBaseResponse } from '../../common';
import { ExternalAccountDetails } from '../models/external-account-details';


export class UpdateExternalAccountResponse extends NorthCapitalBaseResponse {
    'Account External Account Details': [true, ExternalAccountDetails[]];
}
