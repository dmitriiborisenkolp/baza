import { NorthCapitalBaseResponse } from '../../common';
import { PurchaseDetails } from '../models/purchase-details';

export class CreateTradeWebhookResponse extends NorthCapitalBaseResponse {
    purchaseDetails: [true, PurchaseDetails[]];
}
