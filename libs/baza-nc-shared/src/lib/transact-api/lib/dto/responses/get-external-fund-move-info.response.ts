import { NorthCapitalBaseResponse } from '../../common';
import { InvestorExternalAccountDetails } from '../models/investor-external-account-details';
import { NcACHDetails } from '../models/nc-ach-details';

export class GetExternalFundMoveInfoResponse extends NorthCapitalBaseResponse {
    investorExternalAccountDetails: InvestorExternalAccountDetails;
    ACHDetails?: NcACHDetails; // its not mentioned on api docs but received on api call so i make it optional just in case
}
