import { NorthCapitalBaseResponse } from '../../common';

import { DocumentDetails } from '../models/document-details';

export class GetAiLetterResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails;
}
