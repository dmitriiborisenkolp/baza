import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class AddSubscriptionsForOfferingResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
