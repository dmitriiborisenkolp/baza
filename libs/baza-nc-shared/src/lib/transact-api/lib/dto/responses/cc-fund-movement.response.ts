import { NorthCapitalBaseResponse } from '../../common';
import { AccountFundMoveDetails } from '../models/account-fund-move-details';

export class CcFundMovementResponse extends NorthCapitalBaseResponse {
    transactionDetails: AccountFundMoveDetails;
}
