import { NorthCapitalBaseResponse } from '../../common';
import { PartyId } from '../models/party-id';


export class UpdateEntityResponse extends NorthCapitalBaseResponse {
    entityDetails: {
        '1': {
            partyId: PartyId;
        }[],
    };
}
