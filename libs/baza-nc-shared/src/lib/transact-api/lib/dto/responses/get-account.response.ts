import { NorthCapitalBaseResponse } from '../../common';
import { AccountDetails } from '../models/account-details';


export class GetAccountResponse extends NorthCapitalBaseResponse {
    accountDetails: AccountDetails;
}
