import { NorthCapitalBaseResponse } from '../../common';
import { CustodialAccountDetails } from '../models/custodial-account-details';


export class UpdateCustodialAccountRequestResponse extends NorthCapitalBaseResponse {
    custodialAccountDetails: CustodialAccountDetails[];
}
