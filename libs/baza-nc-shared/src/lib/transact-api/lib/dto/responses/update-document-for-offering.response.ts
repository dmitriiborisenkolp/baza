import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class UpdateDocumentForOfferingResponse extends NorthCapitalBaseResponse {
    'document details': DocumentDetails[];
}
