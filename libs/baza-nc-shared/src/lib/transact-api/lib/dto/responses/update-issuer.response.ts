import { NorthCapitalBaseResponse } from '../../common';
import { IssuerId } from '../models/issuer-id';
import { IssuerStatus } from '../models/issuer-status';


export class UpdateIssuerResponse extends NorthCapitalBaseResponse {
    issuerDetails: [true, Array<{
        issuerId: IssuerId;
        issuerStatus: IssuerStatus;
    }>];
}
