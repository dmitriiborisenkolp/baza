import { NorthCapitalBaseResponse } from '../../common';
import { TradeFinancialDetails } from '../models/trade-financial-details';


export class EditTradeResponse extends NorthCapitalBaseResponse {
    TradeFinancialDetails: TradeFinancialDetails;
}
