import { NorthCapitalBaseResponse } from '../../common';
import { IssuerDetails } from '../models/issuer-details';


export class GetIssuerResponse extends NorthCapitalBaseResponse {
    issuerDetails: IssuerDetails[];
}
