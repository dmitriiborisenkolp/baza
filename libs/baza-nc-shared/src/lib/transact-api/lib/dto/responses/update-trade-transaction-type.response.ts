import { TransactionType } from '../models/transaction-type';
import { TradeId } from '../models/trade-id';
import { NorthCapitalBaseResponse } from '../../common';
export class UpdateTradeTransactionTypeResponse extends NorthCapitalBaseResponse {
    tradeId: TradeId;
    transactionType: TransactionType;
}
