import { NorthCapitalBaseResponse } from '../../common';
import { PartyDetails } from '../models/party-details';

export class GetPartyResponse extends NorthCapitalBaseResponse {
    partyDetails: [PartyDetails];
}
