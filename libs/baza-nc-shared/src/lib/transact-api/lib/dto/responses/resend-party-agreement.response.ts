import { NorthCapitalBaseResponse } from '../../common';

import { PartyDetails } from '../models/party-details';

export class ResendPartyAgreementResponse extends NorthCapitalBaseResponse {
    partyDetails: PartyDetails;
}
