import { NorthCapitalBaseResponse } from '../../common';

export class SendSubscriptionDocumentClientResponse extends NorthCapitalBaseResponse {
    document_details: { url: string };
}
