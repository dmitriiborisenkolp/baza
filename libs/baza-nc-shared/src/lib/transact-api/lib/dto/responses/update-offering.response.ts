import { NorthCapitalBaseResponse } from '../../common';
import { OfferingId } from '../models/offering-id';
import { OfferingStatus } from '../models/offering-status';


export class UpdateOfferingResponse extends NorthCapitalBaseResponse {
    offeringDetails: [true, Array<{
        offeringId: OfferingId;
        offeringStatus: OfferingStatus;
    }>];
}
