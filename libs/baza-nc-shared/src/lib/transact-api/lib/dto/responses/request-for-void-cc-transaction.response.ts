import { NorthCapitalBaseResponse } from '../../common';


export class RequestForVoidCcTransactionResponse extends NorthCapitalBaseResponse {
    investorExternalAccountDetails: 'Status Updated Successfully';
}
