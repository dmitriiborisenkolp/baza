import { NorthCapitalBaseResponse } from '../../common';


export class RequestForVoidACHResponse extends NorthCapitalBaseResponse {
    investorExternalAccountDetails: 'Status Updated Successfully';
}
