import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class GetTradeDocumentResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
