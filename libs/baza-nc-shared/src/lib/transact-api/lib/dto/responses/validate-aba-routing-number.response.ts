import { NorthCapitalBaseResponse } from '../../common';


export class ValidateAbaRoutingNumberResponse extends NorthCapitalBaseResponse {
    accountDetails: 'Valid routing number' /* verified */ | 'Invalid routing number' /* Warning: not verified, may be not actual value */;
}
