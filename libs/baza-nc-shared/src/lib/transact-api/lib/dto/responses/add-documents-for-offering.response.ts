import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class AddDocumentsForOfferingResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
