import { NorthCapitalBaseResponse } from '../../common';
import { NorthCapitalAccountId } from '../models/account-id';
import { IssuerId } from '../models/issuer-id';
import { ExternalAccountStatus } from '../models/external-account-status';


export class DeleteExternalAccountResponse extends NorthCapitalBaseResponse {
    accountId: NorthCapitalAccountId;

    issuerId: IssuerId;

    Ext_status: ExternalAccountStatus;
}
