import { AccreditedDetails } from '../models/accredited-details';
import { NorthCapitalBaseResponse } from '../../common';


export class GetAiRequestResponse extends NorthCapitalBaseResponse {
    accreditedDetails: AccreditedDetails;
}
