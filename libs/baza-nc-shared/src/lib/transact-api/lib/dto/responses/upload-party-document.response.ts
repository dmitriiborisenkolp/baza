import { NorthCapitalBaseResponse } from '../../common';


export class UploadPartyDocumentResponse extends NorthCapitalBaseResponse {
    document_details: 'Document has been uploaded Successfully';
}
