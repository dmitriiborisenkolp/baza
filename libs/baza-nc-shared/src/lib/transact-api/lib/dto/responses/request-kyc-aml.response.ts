import { NorthCapitalBaseResponse } from '../../common';
import { PartyStatus } from '../models/party-status';
import { KYCStatus } from '../models/kyc-status';

export class RequestKycAmlResponse extends NorthCapitalBaseResponse {
    'KYC Status': KYCStatus;

    'Party Status': PartyStatus;
}
