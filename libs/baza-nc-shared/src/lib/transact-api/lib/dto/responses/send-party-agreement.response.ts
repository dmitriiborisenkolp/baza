import { NorthCapitalBaseResponse } from '../../common';
import { PartyId } from '../models/party-id';


class PartyDetails {
    partyId: PartyId;

    documentKey: string;
}

export class SendPartyAgreementResponse extends NorthCapitalBaseResponse {
    partyDetails: PartyDetails;
}
