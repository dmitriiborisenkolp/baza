import { NorthCapitalBaseResponse } from '../../common';


export class DeleteEscrowAccountResponse extends NorthCapitalBaseResponse {
    statusDesc: 'Escrow Financial  Account deleted successfully!';
}
