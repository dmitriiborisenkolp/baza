import { TradeDetails } from '../models/trade-details';
import { NorthCapitalBaseResponse } from '../../common';

export class UpdateTradeStatusResponse extends NorthCapitalBaseResponse<TradeDetails> {
    statusDesc: TradeDetails;
}
