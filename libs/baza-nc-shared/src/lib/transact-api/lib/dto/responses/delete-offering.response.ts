import { NorthCapitalBaseResponse } from '../../common';
import { NorthCapitalNumberAsString } from '../common-types';


export class DeleteOfferingResponse extends NorthCapitalBaseResponse {
    offeringDetails: NorthCapitalNumberAsString;
}
