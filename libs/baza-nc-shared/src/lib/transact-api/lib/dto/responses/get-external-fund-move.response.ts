import { NorthCapitalBaseResponse } from '../../common';
import { InvestorExternalAccountDetails } from '../models/investor-external-account-details';


export class GetExternalFundMoveResponse extends NorthCapitalBaseResponse {
    investorExternalAccountDetails: InvestorExternalAccountDetails[];
}
