import { NorthCapitalBaseResponse } from '../../common';
import { AccountDetails } from '../models/account-details';

export class CreateAccountResponse extends NorthCapitalBaseResponse {
    accountDetails: [AccountDetails];
}
