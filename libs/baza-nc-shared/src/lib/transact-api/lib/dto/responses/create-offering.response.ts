import { NorthCapitalBaseResponse } from '../../common';
import { OfferingDetails } from '../models/offering-details';


export class CreateOfferingResponse extends NorthCapitalBaseResponse {
    offeringDetails: [true, OfferingDetails[]];
}
