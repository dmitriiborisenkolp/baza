import { NorthCapitalBaseResponse } from '../../common';
import { CcFundMoveInfo } from '../models/cc-fund-move-info';


export class GetCcFundMoveInfoResponse extends NorthCapitalBaseResponse {
    investorExternalAccountDetails: CcFundMoveInfo[];
}
