import { NorthCapitalBaseResponse } from '../../common';
import { IssuerId } from '../models/issuer-id';


export class UpdateIssuerAccountResponse extends NorthCapitalBaseResponse {
    'Financial issuer details': {
        1: {
            issuerId: IssuerId;
        };
    };
}
