import { NorthCapitalBaseResponse } from '../../common';


export class UploadTradeDocumentResponse extends NorthCapitalBaseResponse {
    document_details: 'Document has been uploaded Successfully';
}
