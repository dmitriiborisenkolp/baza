import { NorthCapitalBaseResponse } from '../../common';
import { IssuerDetails } from '../models/issuer-details';

export class CreateIssuerResponse extends NorthCapitalBaseResponse {
    issuerDetails: [true, IssuerDetails[]];
}
