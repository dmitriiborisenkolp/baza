import { NorthCapitalBaseResponse } from '../../common';
import { LinkDetails } from '../models/link-details';


export class GetAllLinksResponse extends NorthCapitalBaseResponse {
    linkDetails: LinkDetails[];
}
