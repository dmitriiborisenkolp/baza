import { NorthCapitalBaseResponse } from '../../common';
import { CustodianDetails } from '../models/custodian-details';


export class GetCustodianDetailsForOfferingResponse extends NorthCapitalBaseResponse {
    custodianDetails: CustodianDetails;
}
