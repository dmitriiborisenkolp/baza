import { NorthCapitalBaseResponse } from '../../common';
import { CreditCardDetails } from '../models/credit-card-details';


export class GetCreditCardResponse extends NorthCapitalBaseResponse {
    creditcardDetails: CreditCardDetails;
}
