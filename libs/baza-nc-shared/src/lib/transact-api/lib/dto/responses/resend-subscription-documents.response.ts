import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class ResendSubscriptionDocumentsResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
