import { AccreditedDetailsRequest } from '../models/accredited-details-request';
import { NorthCapitalBaseResponse } from '../../common';


export class UpdateAiRequestResponse extends NorthCapitalBaseResponse {
    accreditedDetails: AccreditedDetailsRequest[];
}
