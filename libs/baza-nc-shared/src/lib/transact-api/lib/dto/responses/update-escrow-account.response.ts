import { NorthCapitalBaseResponse } from '../../common';
import { EscrowAccountDetails } from '../models/escrow-account-details';


export class UpdateEscrowAccountResponse extends NorthCapitalBaseResponse {
    ESCROW_DETAILS: [true, EscrowAccountDetails[]];
}
