import { NorthCapitalBaseResponse } from '../../common';
import { PartyDocumentDetails } from '../models/party-document-details';


export class GetEntityDocumentResponse extends NorthCapitalBaseResponse {
    partyDocumentDetails: PartyDocumentDetails;
}
