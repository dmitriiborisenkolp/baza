import { NorthCapitalBaseResponse } from '../../common';
import { AccountFundMoveDetails } from '../models/account-fund-move-details';


export class GetExternalFundMoveHistoryResponse extends NorthCapitalBaseResponse {
    accountDetails: AccountFundMoveDetails[];
}
