import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class SendSubscriptionDocumentResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
