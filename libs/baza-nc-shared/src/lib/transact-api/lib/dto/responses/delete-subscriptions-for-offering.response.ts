import { NorthCapitalBaseResponse } from '../../common';


export class DeleteSubscriptionsForOfferingResponse extends NorthCapitalBaseResponse {
    'document details': 'Template deleted successfully';
}
