import { NorthCapitalBaseResponse } from '../../common';
import { PartyId } from '../models/party-id';
import { KYCStatus } from '..';


export class UpdateKycAmlResponse extends NorthCapitalBaseResponse {
    'Financial Party details': {
        partyId: PartyId;
        kycStatus: KYCStatus;
        KYCStatus: KYCStatus;
    }
}
