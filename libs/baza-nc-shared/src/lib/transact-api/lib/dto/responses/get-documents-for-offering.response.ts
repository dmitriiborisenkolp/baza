import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class GetDocumentsForOfferingResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
