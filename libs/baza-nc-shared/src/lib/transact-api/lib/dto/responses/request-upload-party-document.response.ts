import { NorthCapitalBaseResponse } from '../../common';


export class RequestUploadPartyDocumentResponse extends NorthCapitalBaseResponse {
    document_details: 'Document has been uploaded Successfully';
}
