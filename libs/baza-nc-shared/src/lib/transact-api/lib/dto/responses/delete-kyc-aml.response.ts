import { NorthCapitalBaseResponse } from '../../common';


export class DeleteKycAmlResponse extends NorthCapitalBaseResponse {
    statusDesc: 'Kyc Aml values has been deleted!';
}
