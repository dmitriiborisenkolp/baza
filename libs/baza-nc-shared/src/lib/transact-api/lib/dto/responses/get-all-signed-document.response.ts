import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';

export class GetAllSignedDocumentResponse extends NorthCapitalBaseResponse {
    SignedDocumentDetails: DocumentDetails[];
}
