import { NorthCapitalBaseResponse } from '../../common';
import { PlaidBankAccountDetails } from '../models/plaid-bank-account-details';

export class GetExternalAccountResponse extends NorthCapitalBaseResponse<PlaidBankAccountDetails> {
}
