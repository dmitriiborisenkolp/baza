import { NorthCapitalBaseResponse } from '../../common';
import { EntityDetails } from '../models/entity-details';


export class GetEntityResponse extends NorthCapitalBaseResponse {
    entityDetails: EntityDetails[];
}
