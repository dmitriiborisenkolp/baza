import { NorthCapitalBaseResponse } from '../../common';


class DocumentDetails {
    templateName: string;

    documentKey: string;
}

export class SendCustodialAccountAgreementResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails;
}
