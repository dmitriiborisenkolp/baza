import { NorthCapitalBaseResponse } from '../../common';
import { AccountDetails } from '../models/account-details';

export class SearchAccountResponse extends NorthCapitalBaseResponse {
    accountDetails: AccountDetails[];
}
