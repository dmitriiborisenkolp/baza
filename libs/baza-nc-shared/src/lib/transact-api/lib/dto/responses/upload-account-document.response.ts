import { NorthCapitalBaseResponse } from '../../common';


export class UploadAccountDocumentResponse extends NorthCapitalBaseResponse {
    document_details: 'Document has been uploaded Successfully';
}
