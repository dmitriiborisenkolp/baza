import { NorthCapitalBaseResponse } from '../../common';


export class UpdateCreditCardResponse extends NorthCapitalBaseResponse {
    creditcardDetails: 'Credit Card details added successfully';
}
