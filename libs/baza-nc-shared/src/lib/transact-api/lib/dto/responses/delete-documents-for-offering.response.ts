import { NorthCapitalBaseResponse } from '../../common';


export class DeleteDocumentsForOfferingResponse extends NorthCapitalBaseResponse {
    'document details': string;
}
