import { NorthCapitalBaseResponse } from '../../common';
import { PurchaseDetails } from '../models/purchase-details';

export class CreateTradeResponse extends NorthCapitalBaseResponse {
    purchaseDetails: [true, PurchaseDetails[]];
}
