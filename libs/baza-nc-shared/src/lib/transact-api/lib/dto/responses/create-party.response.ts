import { NorthCapitalBaseResponse } from '../../common';
import { PartyDetails } from '../models/party-details';


export class CreatePartyResponse extends NorthCapitalBaseResponse {
    partyDetails: [boolean, PartyDetails[]];
}
