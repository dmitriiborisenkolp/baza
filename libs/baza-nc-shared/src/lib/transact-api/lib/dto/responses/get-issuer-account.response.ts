import { FinancialIssuerDetails } from '../models/financial-issuer-details';
import { NorthCapitalBaseResponse } from '../../common';
import { IssuerAccountDetails } from '../models/issuer-account-details';


export class GetIssuerAccountResponse extends NorthCapitalBaseResponse<IssuerAccountDetails> {
    'Financial issuer details': Array<FinancialIssuerDetails>;

    statusDesc: IssuerAccountDetails;
}
