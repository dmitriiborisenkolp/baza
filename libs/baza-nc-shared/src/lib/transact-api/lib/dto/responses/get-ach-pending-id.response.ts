import { NorthCapitalBaseResponse } from '../../common';
import { ExternalAccountDetails } from '../models/external-account-details';


export class GetAchPendingIdResponse extends NorthCapitalBaseResponse {
    investorExternalAccountDetails: ExternalAccountDetails[];
}
