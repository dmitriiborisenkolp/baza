import { NorthCapitalBaseResponse } from '../../common';


export class DeletePartyDocumentResponse extends NorthCapitalBaseResponse {
    document_details: 'Document deleted successfully';
}
