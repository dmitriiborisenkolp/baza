import { NorthCapitalBaseResponse } from '../../common';
import { ArchiveStatus } from '../models/archive-status';


class AccountDetails {
    accountId: string;

    archivestatus: ArchiveStatus;
}

export class UpdateAccountArchiveStatusResponse extends NorthCapitalBaseResponse {
    accountDetails: AccountDetails[];
}
