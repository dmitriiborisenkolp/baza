import { NorthCapitalBaseResponse } from '../../common';
import { DocusignTemplateDetails } from '../models/docusign-template-details';

export class FetchSubscriptionDocumentsResponse extends NorthCapitalBaseResponse {
    document_details: Array<DocusignTemplateDetails>;
}
