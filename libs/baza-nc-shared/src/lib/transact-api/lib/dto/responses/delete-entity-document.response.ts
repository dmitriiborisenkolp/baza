import { NorthCapitalBaseResponse } from '../../common';


export class DeleteEntityDocumentResponse extends NorthCapitalBaseResponse {
    document_details: 'Document deleted successfully';
}
