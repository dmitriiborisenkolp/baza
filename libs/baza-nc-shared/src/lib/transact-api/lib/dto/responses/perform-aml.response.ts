import { NorthCapitalBaseResponse } from '../../common';
import { PerformAmlPartyDetails } from '../models/perform-aml-party-details';


export class PerformAmlResponse extends NorthCapitalBaseResponse {
    partyDetails: PerformAmlPartyDetails;
}
