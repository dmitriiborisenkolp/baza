import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class GetUploadPartyDocumentResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
