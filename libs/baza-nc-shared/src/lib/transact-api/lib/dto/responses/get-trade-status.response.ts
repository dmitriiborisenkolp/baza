import { NorthCapitalBaseResponse } from '../../common';
import { TradeDetails } from '../models/trade-details';


export class GetTradeStatusResponse extends NorthCapitalBaseResponse {
    tradeDetails: TradeDetails[];
}
