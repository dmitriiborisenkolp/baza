import { NorthCapitalBaseResponse } from '../../common';
import { Suitability } from '../models/suitability';

export class GetSuitabilityResponse extends NorthCapitalBaseResponse {
    accountDetails: Suitability;
}
