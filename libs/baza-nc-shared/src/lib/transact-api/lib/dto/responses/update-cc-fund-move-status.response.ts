import { TransactionStatus } from '../../../../transact-api';
import { NorthCapitalBaseResponse } from '../../common';

export class UpdateCcFundMoveStatusResponse extends NorthCapitalBaseResponse {
    tradeId: string;
    accountId: string;
    transactionstatus: string;
    fundStatus: TransactionStatus;
    RefNum: string;
    errors: string;
}
