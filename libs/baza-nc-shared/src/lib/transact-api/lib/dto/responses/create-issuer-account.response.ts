import { NorthCapitalBaseResponse } from '../../common';
import { FinancialIssuerDetails } from '..';


export class CreateIssuerAccountResponse extends NorthCapitalBaseResponse {
    'Financial issuer details': {
        '1': [FinancialIssuerDetails];
    };
}
