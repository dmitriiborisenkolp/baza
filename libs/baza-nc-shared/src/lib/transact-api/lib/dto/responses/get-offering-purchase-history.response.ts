import { NorthCapitalBaseResponse } from '../../common';
import { OfferingPurchasedDetails } from '../models/offering-purchased-details';


export class GetOfferingPurchaseHistorySubResponse {
    offering_purchase_history: OfferingPurchasedDetails[];
}

export class GetOfferingPurchaseHistoryResponse extends NorthCapitalBaseResponse {
    'Offering purchased details': GetOfferingPurchaseHistorySubResponse;
}
