import { NorthCapitalBaseResponse } from '../../common';
import { CCFundMoveDetails } from '../models/cc-fund-move-details';


export class GetCcFundMoveHistoryResponse extends NorthCapitalBaseResponse {
    creditcardDetails: CCFundMoveDetails[];
}
