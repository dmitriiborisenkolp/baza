import { NorthCapitalBaseResponse } from '../../common';
import { CustodialAccountDetails } from '../models/custodial-account-details';


export class RequestCustodialAccountResponse extends NorthCapitalBaseResponse {
    custodialAccountDetails: CustodialAccountDetails[];
}
