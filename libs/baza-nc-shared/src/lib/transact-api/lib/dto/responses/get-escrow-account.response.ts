import { NorthCapitalBaseResponse } from '../../common';
import { FinancialEscrowDetails } from '../models/financial-escrow-details';


export class GetEscrowAccountResponse extends NorthCapitalBaseResponse {
    'Financial Escrow Details': FinancialEscrowDetails;
}
