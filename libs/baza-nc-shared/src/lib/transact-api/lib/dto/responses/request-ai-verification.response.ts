import { NorthCapitalBaseResponse } from '../../common';
import { AccreditedDetailsRequest } from '../models/accredited-details-request';


export class RequestAiVerificationResponse extends NorthCapitalBaseResponse {
    accreditedDetails: AccreditedDetailsRequest[];
}
