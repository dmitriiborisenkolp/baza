import { NorthCapitalBaseResponse } from '../../common';

import { TradeDetails } from '../models/trade-details';

export class DeleteTradeResponse extends NorthCapitalBaseResponse {
    tradeDetails: TradeDetails;
}
