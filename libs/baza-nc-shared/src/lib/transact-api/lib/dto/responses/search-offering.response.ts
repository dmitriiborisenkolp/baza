import { NorthCapitalBaseResponse } from '../../common';
import { OfferingDetails } from '../models/offering-details';


export class SearchOfferingResponse extends NorthCapitalBaseResponse {
    offeringDetails: OfferingDetails[];
}
