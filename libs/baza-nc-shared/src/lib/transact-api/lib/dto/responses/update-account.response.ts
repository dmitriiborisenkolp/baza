import { NorthCapitalBaseResponse } from '../../common';
import { AccountDetails } from '../models/account-details';


export class UpdateAccountResponse extends NorthCapitalBaseResponse {
    accountDetails: AccountDetails;
}
