import { NorthCapitalBaseResponse } from '../../common';
import { PartyDetails } from '../models/party-details';


export class SearchPartyResponse extends NorthCapitalBaseResponse {
    partyDetails: PartyDetails[];
}
