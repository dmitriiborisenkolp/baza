import { NorthCapitalBaseResponse } from '../../common';
import { TradeFinancialDetails } from '../models/trade-financial-details';


export class ExternalFundMoveResponse extends NorthCapitalBaseResponse {
    TradeFinancialDetails: TradeFinancialDetails[];
}
