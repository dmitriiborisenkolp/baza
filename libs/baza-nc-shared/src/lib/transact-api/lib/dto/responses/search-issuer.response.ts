import { NorthCapitalBaseResponse } from '../../common';
import { IssuerDetails } from '../models/issuer-details';


export class SearchIssuerResponse extends NorthCapitalBaseResponse {
    issuerDetails: Array<IssuerDetails>;
}
