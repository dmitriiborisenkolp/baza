import { NorthCapitalBaseResponse } from '../../common';
import { Offering } from '../models/offering';


export class GetOfferingResponse extends NorthCapitalBaseResponse {
    offeringDetails: Offering[];
}
