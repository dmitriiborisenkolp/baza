import { NorthCapitalAccountId } from '../models/account-id';
import { TransactionType } from '../models/transaction-type';
import { OrderStatus } from '../models/order-status';
import { TradeId } from '../models/trade-id';
import { NorthCapitalBaseResponse } from '../../common';

export class GetTradesForOfferingResponse extends NorthCapitalBaseResponse {
    'Offering purchased details': Array<GetTradesForOfferingEntry>;
}

export class GetTradesForOfferingEntry {
    orderId: TradeId;
    accountId: NorthCapitalAccountId;
    transactionType: TransactionType;
    totalShares: string;
    unitPrice: string;
    totalAmount: string;
    orderStatus: OrderStatus;
    field1: string;
    field2: string;
    field3: string;
}
