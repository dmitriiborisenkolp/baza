import { NorthCapitalBaseResponse } from '../../common';


export class UploadEntityDocumentResponse extends NorthCapitalBaseResponse {
    document_details: 'Document has been uploaded Successfully';
}
