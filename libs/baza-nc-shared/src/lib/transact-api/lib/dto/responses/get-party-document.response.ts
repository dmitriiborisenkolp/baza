import { NorthCapitalBaseResponse } from '../../common';
import { PartyDocumentDetails } from '../models/party-document-details';


export class GetPartyDocumentResponse extends NorthCapitalBaseResponse {
    partyDocumentDetails: PartyDocumentDetails[];
}
