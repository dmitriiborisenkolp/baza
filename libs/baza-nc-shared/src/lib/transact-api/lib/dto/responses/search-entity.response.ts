import { NorthCapitalBaseResponse } from '../../common';
import { EntityDetails } from '../models/entity-details';


export class SearchEntityResponse extends NorthCapitalBaseResponse {
    entityDetails: EntityDetails[];
}
