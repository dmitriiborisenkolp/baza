import { NorthCapitalBaseResponse } from '../../common';
import { NorthCapitalDateTime } from '../common-types';
import { NorthCapitalAccountId } from '../models/account-id';
import { AmlStatus } from '../models/aml-status';
import { EsignStatus } from '../models/esign-status';
import { KYCStatus } from '../models/kyc-status';
import { OfferingId } from '../models/offering-id';
import { OrderStatus } from '../models/order-status';
import { PartyId } from '../models/party-id';
import { TransactionType } from '../models/transaction-type';

export class GetAccountTradeHistoryResponse extends NorthCapitalBaseResponse {
    accountDetails: GetAccountTradeHistoryResponseEntry[];
}

export class GetAccountTradeHistoryResponseEntry {
    in_id: string;
    offeringId: OfferingId;
    orderId: string;
    partyId: PartyId;
    accountId: NorthCapitalAccountId;
    totalAmount: string;
    esignStatus: EsignStatus;
    clientId: string;
    trade_developerAPIKey: string;
    accountName: string;
    accreditedStatus: string;
    kycStatus: KYCStatus;
    amlStatus: AmlStatus;
    issueName: string;
    transactionType: TransactionType;
    orderStatus: OrderStatus;
    trade_createdDate: NorthCapitalDateTime;
}
