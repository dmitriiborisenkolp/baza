import { NorthCapitalBaseResponse } from '../../common';
import { AccountDetails } from '../models/account-details';


export class UpdateSuitabilityResponse extends NorthCapitalBaseResponse {
    accountDetails: AccountDetails[];
}
