import { NorthCapitalBaseResponse } from '../../common';
import { TradeDetails } from '../models/trade-details';


export class FundReturnRequestResponse extends NorthCapitalBaseResponse {
    tradeDetails: TradeDetails;
}
