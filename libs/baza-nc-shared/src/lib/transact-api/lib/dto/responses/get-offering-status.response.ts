import { NorthCapitalBaseResponse } from '../../common';
import { OfferingDetails } from '../models/offering-details';


export class GetOfferingStatusResponse extends NorthCapitalBaseResponse {
    offeringDetails: OfferingDetails;
}
