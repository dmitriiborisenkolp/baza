import { NorthCapitalBaseResponse } from '../../common';
import { Kyc } from '../models/kyc';


export class PerformKycAmlResponse extends NorthCapitalBaseResponse {
    kyc: Kyc;
}
