import { NorthCapitalBaseResponse } from '../../common';
import { ExternalAccountDetails } from '../models/external-account-details';

export class CreateExternalAccountResponse extends NorthCapitalBaseResponse {
    'External Account Details': [true, ExternalAccountDetails[]];
}
