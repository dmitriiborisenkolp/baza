import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class GetAccountDocumentResponse extends NorthCapitalBaseResponse {
    document_details: DocumentDetails[];
}
