import { NorthCapitalBaseResponse } from '../../common';
import { TransactionStatus } from '../../../../transact-api';

export class UpdateExternalFundMoveStatusResponse extends NorthCapitalBaseResponse {
    tradeId: string;
    accountId: string;
    transactionstatus: string;
    fundStatus: TransactionStatus;
    RefNum: string;
    errors: string;
}
