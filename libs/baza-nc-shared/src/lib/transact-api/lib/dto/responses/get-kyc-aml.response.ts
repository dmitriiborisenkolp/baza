import { NorthCapitalBaseResponse } from '../../common';
import { KYCStatus } from '../models/kyc-status';
import { PartyStatus } from '../models/party-status';


export class GetKycAmlResponse extends NorthCapitalBaseResponse {
    'KYC Status': KYCStatus;

    'Party Status': PartyStatus;
}
