import { NorthCapitalBaseResponse } from '../../common';
import { FinancialEscrowDetails } from '../models/financial-escrow-details';


export class CreateEscrowAccountResponse extends NorthCapitalBaseResponse {
    'Financial Escrow Details': [true, FinancialEscrowDetails[]];
}
