import { NorthCapitalBaseResponse } from '../../common';


export class UpdateDocuSignStatusResponse extends NorthCapitalBaseResponse {
    document_details: 'Signed Status Update Successfully';
}
