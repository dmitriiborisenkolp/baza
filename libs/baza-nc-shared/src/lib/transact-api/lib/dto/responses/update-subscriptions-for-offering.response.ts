import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class UpdateSubscriptionsForOfferingResponse extends NorthCapitalBaseResponse {
    'document details': DocumentDetails;
}
