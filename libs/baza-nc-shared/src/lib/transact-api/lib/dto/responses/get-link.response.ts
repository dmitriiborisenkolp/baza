import { NorthCapitalBaseResponse } from '../../common';
import { LinkDetails } from '../models/link-details';


export class GetLinkResponse extends NorthCapitalBaseResponse {
    linkDetails: LinkDetails[];
}
