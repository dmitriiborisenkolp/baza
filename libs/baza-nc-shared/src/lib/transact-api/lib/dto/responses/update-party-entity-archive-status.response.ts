import { NorthCapitalBaseResponse } from '../../common';
import { ArchiveStatus } from '../models/archive-status';
import { PartyId } from '../models/party-id';


export class UpdatePartyEntityArchiveStatusResponse extends NorthCapitalBaseResponse {
    partyDetails: {
        partyId: PartyId;
        archivestatus: ArchiveStatus;
    }[];
}
