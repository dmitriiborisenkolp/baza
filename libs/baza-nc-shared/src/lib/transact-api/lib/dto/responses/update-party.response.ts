import { NorthCapitalBaseResponse } from '../../common';
import { PartyDetails } from '../models/party-details';


export class UpdatePartyResponse extends NorthCapitalBaseResponse {
    partyDetails: [true, PartyDetails[]];
}
