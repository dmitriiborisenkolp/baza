import { NorthCapitalBaseResponse } from '../../common';
import { PartyId } from '../models/party-id';
import { OfferingId } from '../models/offering-id';
import { ArchiveStatus } from '../models/archive-status';


export class UpdateTradeArchiveStatusResponse extends NorthCapitalBaseResponse {
    tradeDetails: {
        partyId: PartyId;
        offeringId: OfferingId;
        archivestatus: ArchiveStatus;
    }[];
}
