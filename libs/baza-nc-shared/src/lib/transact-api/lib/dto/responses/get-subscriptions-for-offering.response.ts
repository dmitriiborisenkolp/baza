import { NorthCapitalBaseResponse } from '../../common';
import { DocumentDetails } from '../models/document-details';


export class GetSubscriptionsForOfferingResponse extends NorthCapitalBaseResponse {
    'document_details': DocumentDetails[];
}
