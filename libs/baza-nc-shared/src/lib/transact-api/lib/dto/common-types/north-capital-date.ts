/* Dates are formatted by PHP. Check https://www.php.net/manual/ru/datetime.format.php documentation about it */
export type NorthCapitalDate = string; // Example date: "31-01-2016", i.e. MM-DD-YYYY. Take care it can be an empty string (not null or undefined)
