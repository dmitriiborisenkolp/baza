/* Dates are formatted by PHP. Check https://www.php.net/manual/ru/datetime.format.php documentation about it */
export type NorthCapitalReverseDate = string; // Example date: "2016-09-18". Take care it can be an empty string (not null or undefined)
