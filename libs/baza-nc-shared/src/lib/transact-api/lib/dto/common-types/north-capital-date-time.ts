/* Dates are formatted by PHP. Check https://www.php.net/manual/ru/datetime.format.php documentation about it */
export type NorthCapitalDateTime = string; // Example date: "2016-02-23 13:29:30". Take care it can be an empty string (not null or undefined)
