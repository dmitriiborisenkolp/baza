import { GetAccountRequest } from '../dto/requests/get-account.request';
import { Observable } from 'rxjs';
import type { GetAccountResponse } from '../dto/responses/get-account.response';
import type { CreateAccountRequest } from '../dto/requests/create-account.request';
import type { CreateAccountResponse } from '../dto/responses/create-account.response';
import type { CalculateSuitabilityRequest } from '../dto/requests/calculate-suitability.request';
import type { CalculateSuitabilityResponse } from '../dto/responses/calculate-suitability.response';
import type { DeleteAccountRequest } from '../dto/requests/delete-account.request';
import type { DeleteAccountResponse } from '../dto/responses/delete-account.response';
import type { DeleteSuitabilityRequest } from '../dto/requests/delete-suitability.request';
import type { DeleteSuitabilityResponse } from '../dto/responses/delete-suitability.response';
import type { GetAccountDocumentRequest } from '../dto/requests/get-account-document.request';
import type { GetAccountDocumentResponse } from '../dto/responses/get-account-document.response';
import type { GetSuitabilityRequest } from '../dto/requests/get-suitability.request';
import type { GetSuitabilityResponse } from '../dto/responses/get-suitability.response';
import type { SearchAccountRequest } from '../dto/requests/search-account.request';
import type { SearchAccountResponse } from '../dto/responses/search-account.response';
import type { UpdateAccountRequest } from '../dto/requests/update-account.request';
import type { UpdateAccountResponse } from '../dto/responses/update-account.response';
import type { UpdateAccountArchiveStatusRequest } from '../dto/requests/update-account-archive-status.request';
import type { UpdateAccountArchiveStatusResponse } from '../dto/responses/update-account-archive-status.response';
import type { UpdateSuitabilityRequest } from '../dto/requests/update-suitability.request';
import type { UpdateSuitabilityResponse } from '../dto/responses/update-suitability.response';

export enum NorthCapitalAccountsPaths {
    GetAccount = 'getAccount',
    CreateAccount = 'createAccount',
    CalculateSuitability = 'calculateSuitability',
    DeleteAccount = 'deleteAccount',
    DeleteSuitability = 'deleteSuitability',
    GetAccountDocument = 'getAccountDocument',
    GetSuitability = 'getSuitability',
    SearchAccount = 'searchAccount',
    UpdateAccount = 'updateAccount',
    UpdateAccountArchiveStatus = 'updateAccountArchivestatus',
    UpdateSuitability = 'updateSuitability',
}

export interface NorthCapitalAccountsApi {
    getAccount(request: GetAccountRequest): Observable<GetAccountResponse> | Promise<GetAccountResponse>;
    createAccount(request: CreateAccountRequest): Observable<CreateAccountResponse> | Promise<CreateAccountResponse>;
    calculateSuitability(request: CalculateSuitabilityRequest): Observable<CalculateSuitabilityResponse> | Promise<CalculateSuitabilityResponse>;
    deleteAccount(request: DeleteAccountRequest): Observable<DeleteAccountResponse> | Promise<DeleteAccountResponse>;
    deleteSuitability(request: DeleteSuitabilityRequest): Observable<DeleteSuitabilityResponse> | Promise<DeleteSuitabilityResponse>;
    getAccountDocument(request: GetAccountDocumentRequest): Observable<GetAccountDocumentResponse> | Promise<GetAccountDocumentResponse>;
    getSuitability(request: GetSuitabilityRequest): Observable<GetSuitabilityResponse> | Promise<GetSuitabilityResponse>;
    searchAccount(request: SearchAccountRequest): Observable<SearchAccountResponse> | Promise<SearchAccountResponse>;
    updateAccount(request: UpdateAccountRequest): Observable<UpdateAccountResponse> | Promise<UpdateAccountResponse>;
    updateAccountArchiveStatus(request: UpdateAccountArchiveStatusRequest): Observable<UpdateAccountArchiveStatusResponse> | Promise<UpdateAccountArchiveStatusResponse>;
    updateSuitability(request: UpdateSuitabilityRequest): Observable<UpdateSuitabilityResponse> | Promise<UpdateSuitabilityResponse>;
}
