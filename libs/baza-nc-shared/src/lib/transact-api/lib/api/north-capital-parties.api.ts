import type { CreatePartyRequest } from '../dto/requests/create-party.request';
import type { Observable } from 'rxjs';
import type { CreatePartyResponse } from '../dto/responses/create-party.response';
import type { DeletePartyRequest } from '../dto/requests/delete-party.request';
import type { DeletePartyResponse } from '../dto/responses/delete-party.response';
import type { DeletePartyDocumentRequest } from '../dto/requests/delete-party-document.request';
import type { DeletePartyDocumentResponse } from '../dto/responses/delete-party-document.response';
import type { GetPartyRequest } from '../dto/requests/get-party.request';
import type { GetPartyResponse } from '../dto/responses/get-party.response';
import type { GetPartyDocumentRequest } from '../dto/requests/get-party-document.request';
import type { GetPartyDocumentResponse } from '../dto/responses/get-party-document.response';
import type { SearchPartyRequest } from '../dto/requests/search-party.request';
import type { SearchPartyResponse } from '../dto/responses/search-party.response';
import type { UpdatePartyRequest } from '../dto/requests/update-party.request';
import type { UpdatePartyResponse } from '../dto/responses/update-party.response';

export enum NorthCapitalPartiesApiPaths {
    CreateParty = 'createParty',
    DeleteParty = 'deleteParty',
    DeletePartyDocument = 'deletePartyDocument',
    GetParty = 'getParty',
    GetPartyDocument = 'getPartyDocument',
    SearchParty = 'searchParty',
    UpdateParty = 'updateParty',
}

export interface NorthCapitalPartiesApi {
    createParty(request: CreatePartyRequest): Observable<CreatePartyResponse> | Promise<CreatePartyResponse>;
    deleteParty(request: DeletePartyRequest): Observable<DeletePartyResponse> | Promise<DeletePartyResponse>;
    deletePartyDocument(request: DeletePartyDocumentRequest): Observable<DeletePartyDocumentResponse> | Promise<DeletePartyDocumentResponse>;
    getParty(request: GetPartyRequest): Observable<GetPartyResponse> | Promise<GetPartyResponse>;
    getPartyDocument(request: GetPartyDocumentRequest): Observable<GetPartyDocumentResponse> | Promise<GetPartyDocumentResponse>;
    searchParty(request: SearchPartyRequest): Observable<SearchPartyResponse> | Promise<SearchPartyResponse>;
    updateParty(request: UpdatePartyRequest): Observable<UpdatePartyResponse> | Promise<UpdatePartyResponse>;
}
