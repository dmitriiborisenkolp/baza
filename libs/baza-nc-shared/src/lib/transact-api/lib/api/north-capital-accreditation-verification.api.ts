// https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation?mid=Mjgx

import { Observable } from 'rxjs';
import type { GetAiDocumentRequest } from '../dto/requests/get-ai-document.request';
import type { GetAiDocumentResponse } from '../dto/responses/get-ai-document.response';
import type { GetAiLetterRequest } from '../dto/requests/get-ai-letter.request';
import type { GetAiLetterResponse } from '../dto/responses/get-ai-letter.response';
import type { GetAiRequestRequest } from '../dto/requests/get-ai-request.request';
import type { GetAiRequestResponse } from '../dto/responses/get-ai-request.response';
import type { RequestAiVerificationRequest } from '../dto/requests/request-ai-verification.request';
import type { RequestAiVerificationResponse } from '../dto/responses/request-ai-verification.response';
import type { UpdateAiRequestRequest } from '../dto/requests/update-ai-request.request';
import type { UpdateAiRequestResponse } from '../dto/responses/update-ai-request.response';
import type { UploadVerificationDocumentRequest } from '../dto/requests/upload-verification-document.request';
import type { UploadVerificationDocumentResponse } from '../dto/responses/upload-verification-document.response';
import type { UpdateAIVerificationRequest } from '../dto/requests/update-ai-verification.request';
import type { UpdateAIVerificationResponse } from '../dto/responses/update-ai-verification.response';

export enum NorthCapitalAccreditationVerificationApiPaths {
    GetAIDocument = 'getAiDocument',
    GetAILetter = 'getAiLetter',
    GetAIRequest = 'getAiRequest',
    RequestAIVerification = 'requestAiVerification',
    UpdateAIRequest = 'updateAiRequest',
    UpdateAIVerification = 'updateAiVerification',
    UploadVerificationDocument = 'uploadVerificationDocument',
}

export interface NorthCapitalAccreditationVerificationApi {
    getAiDocument(request: GetAiDocumentRequest): Observable<GetAiDocumentResponse> | Promise<GetAiDocumentResponse>;
    getAiLetter(request: GetAiLetterRequest): Observable<GetAiLetterResponse> | Promise<GetAiLetterResponse>;
    getAIRequest(request: GetAiRequestRequest): Observable<GetAiRequestResponse> | Promise<GetAiRequestResponse>;
    requestAIVerification(
        request: RequestAiVerificationRequest,
    ): Observable<RequestAiVerificationResponse> | Promise<RequestAiVerificationResponse>;
    updateAIRequest(request: UpdateAiRequestRequest): Observable<UpdateAiRequestResponse> | Promise<UpdateAiRequestResponse>;
    updateAiVerification(
        request: UpdateAIVerificationRequest,
    ): Observable<UpdateAIVerificationResponse> | Promise<UpdateAIVerificationResponse>;
    uploadVerificationDocument(
        request: UploadVerificationDocumentRequest,
        uploadedFile?: File | Buffer | unknown,
    ): Observable<UploadVerificationDocumentResponse> | Promise<UploadVerificationDocumentResponse>;
}
