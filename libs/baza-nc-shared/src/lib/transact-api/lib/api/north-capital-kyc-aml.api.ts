import type { DeleteKycAmlRequest } from '../dto/requests/delete-kyc-aml.request';
import type { Observable } from 'rxjs';
import type { DeleteKycAmlResponse } from '../dto/responses/delete-kyc-aml.response';
import type { RequestKycAmlRequest } from '../dto/requests/request-kyc-aml.request';
import type { RequestKycAmlResponse } from '../dto/responses/request-kyc-aml.response';
import type { PerformAmlRequest } from '../dto/requests/perform-aml.request';
import type { PerformAmlResponse } from '../dto/responses/perform-aml.response';
import type { PerformKycAmlRequest } from '../dto/requests/perform-kyc-aml.request';
import type { GetKycAmlRequest } from '../dto/requests/get-kyc-aml.request';
import type { GetKycAmlResponse } from '../dto/responses/get-kyc-aml.response';
import type { UpdateKycAmlRequest } from '../dto/requests/update-kyc-aml.request';
import type { UpdateKycAmlResponse } from '../dto/responses/update-kyc-aml.response';
import type { PerformKycAmlResponse } from '../dto/responses/perform-kyc-aml.response';

export enum NorthCapitalKycAmlApiPaths {
    DeleteKycAml = 'deleteKycAml',
    GetKycAml = 'getKycAml',
    RequestKycAml = 'requestKycAml',
    PerformAml = 'performAml',
    PerformKycAml = 'performKycAml',
    PerformKycAmlBasic = 'performKycAmlBasic',
    UpdateKycAml = 'updateKycAml',
}

export interface NorthCapitalKycAmlApi {
    deleteKycAml(request: DeleteKycAmlRequest): Observable<DeleteKycAmlResponse> | Promise<DeleteKycAmlResponse>;
    getKycAml(request: GetKycAmlRequest): Observable<GetKycAmlResponse> | Promise<GetKycAmlResponse>;
    requestKycAml(request: RequestKycAmlRequest): Observable<RequestKycAmlResponse> | Promise<RequestKycAmlResponse>;
    performAml(request: PerformAmlRequest): Observable<PerformAmlResponse> | Promise<PerformAmlResponse>;
    performKycAml(request: PerformKycAmlRequest): Observable<PerformKycAmlResponse> | Promise<PerformKycAmlResponse>;
    performKycAmlBasic(request: PerformKycAmlRequest): Observable<PerformKycAmlResponse> | Promise<PerformKycAmlResponse>;
    updateKycAml(request: UpdateKycAmlRequest): Observable<UpdateKycAmlResponse> | Promise<UpdateKycAmlResponse>;
}
