import type { Observable } from 'rxjs';
import type { AddDocumentsForOfferingRequest } from '../dto/requests/add-documents-for-offering.request';
import type { AddDocumentsForOfferingResponse } from '../dto/responses/add-documents-for-offering.response';
import type { AddSubscriptionsForOfferingRequest } from '../dto/requests/add-subscriptions-for-offering.request';
import type { AddSubscriptionsForOfferingResponse } from '../dto/responses/add-subscriptions-for-offering.response';
import type { DeleteDocumentsForOfferingRequest } from '../dto/requests/delete-documents-for-offering.request';
import type { DeleteDocumentsForOfferingResponse } from '../dto/responses/delete-documents-for-offering.response';
import type { DeleteSubscriptionsForOfferingRequest } from '../dto/requests/delete-subscriptions-for-offering.request';
import type { DeleteSubscriptionsForOfferingResponse } from '../dto/responses/delete-subscriptions-for-offering.response';
import type { GetDocumentsForOfferingRequest } from '../dto/requests/get-documents-for-offering.request';
import type { GetDocumentsForOfferingResponse } from '../dto/responses/get-documents-for-offering.response';
import type { GetSubscriptionsForOfferingRequest } from '../dto/requests/get-subscriptions-for-offering.request';
import type { GetSubscriptionsForOfferingResponse } from '../dto/responses/get-subscriptions-for-offering.response';
import type { GetTradeDocumentRequest } from '../dto/requests/get-trade-document.request';
import type { GetTradeDocumentResponse } from '../dto/responses/get-trade-document.response';
import type { GetUploadPartyDocumentRequest } from '../dto/requests/get-upload-party-document.request';
import type { GetUploadPartyDocumentResponse } from '../dto/responses/get-upload-party-document.response';
import type { RequestUploadPartyDocumentRequest } from '../dto/requests/request-upload-party-document.request';
import type { RequestUploadPartyDocumentResponse } from '../dto/responses/request-upload-party-document.response';
import type { ResendPartyAgreementRequest } from '../dto/requests/resend-party-agreement.request';
import type { ResendPartyAgreementResponse } from '../dto/responses/resend-party-agreement.response';
import type { SendPartyAgreementRequest } from '../dto/requests/send-party-agreement.request';
import type { SendPartyAgreementResponse } from '../dto/responses/send-party-agreement.response';
import type { SendSubscriptionDocumentRequest } from '../dto/requests/send-subscription-document.request';
import type { SendSubscriptionDocumentResponse } from '../dto/responses/send-subscription-document.response';
import type { UpdateDocumentForOfferingRequest } from '../dto/requests/update-document-for-offering.request';
import type { UpdateDocumentForOfferingResponse } from '../dto/responses/update-document-for-offering.response';
import type { UpdateSubscriptionsForOfferingRequest } from '../dto/requests/update-subscriptions-for-offering.request';
import type { UpdateSubscriptionsForOfferingResponse } from '../dto/responses/update-subscriptions-for-offering.response';
import type { UpdateTradeDocArchiveStatusRequest } from '../dto/requests/update-trade-doc-archive-status.request';
import type { UpdateTradeDocArchiveStatusResponse } from '../dto/responses/update-trade-doc-archive-status.response';
import type { UploadAccountDocumentRequest } from '../dto/requests/upload-account-document.request';
import type { UploadAccountDocumentResponse } from '../dto/responses/upload-account-document.response';
import type { UploadEntityDocumentRequest } from '../dto/requests/upload-entity-document.request';
import type { UploadEntityDocumentResponse } from '../dto/responses/upload-entity-document.response';
import type { UploadPartyDocumentRequest } from '../dto/requests/upload-party-document.request';
import type { UploadPartyDocumentResponse } from '../dto/responses/upload-party-document.response';
import type { UploadTradeDocumentRequest } from '../dto/requests/upload-trade-document.request';
import type { UploadTradeDocumentResponse } from '../dto/responses/upload-trade-document.response';
import type { ResendSubscriptionDocumentsRequest } from '../dto/requests/resend-subscription-documents.request';
import type { ResendSubscriptionDocumentsResponse } from '../dto/responses/resend-subscription-documents.response';
import type { GetAllSignedDocumentRequest } from '../dto/requests/get-all-signed-document.request';
import type { GetAllSignedDocumentResponse } from '../dto/responses/get-all-signed-document.response';
import type { FetchSubscriptionDocumentsResponse } from '../dto/responses/fetch-subscription-documents.response';

export enum NorthCapitalDocumentsApiPaths {
    AddDocumentsForOffering = 'addDocumentsforOffering',
    AddSubscriptionsForOffering = 'addSubscriptionsforOffering',
    DeleteDocumentsForOffering = 'deleteDocumentsforOffering',
    DeleteSubscriptionsForOffering = 'deleteSubscriptionsforOffering',
    GetDocumentsForOffering = 'getDocumentsforOffering',
    GetSubscriptionsForOffering = 'getSubscriptionsforOffering',
    GetTradeDocument = 'getTradeDocument',
    GetUploadPartyDocument = 'getuploadPartyDocument',
    RequestUploadPartyDocument = 'requestUploadPartyDocument',
    ResendPartyAgreement = 'resendPartyAgreement',
    ResendSubscriptionDocuments = 'resendSubscriptionDocuments',
    SendPartyAgreement = 'sendPartyAgreement',
    SendSubscriptionDocument = 'sendSubscriptionDocument',
    SendSubscriptionDocumentClient = 'sendSubscriptionDocumentClient',
    UpdateDocumentForOffering = 'updateDocumentforOffering',
    UpdateSubscriptionsForOffering = 'updateSubscriptionsforOffering',
    UpdateTradeDocArchiveStatus = 'updateTradeDocArchivestatus',
    UploadAccountDocument = 'uploadAccountDocument',
    UploadEntityDocument = 'uploadEntityDocument',
    UploadPartyDocument = 'uploadPartyDocument',
    UploadTradeDocument = 'uploadTradeDocument',
    GetAllSignedDocument = 'getAllSignedDocument',
    FetchSubscriptionDocuments = 'fetchSubscriptionDocuments',
}

export interface NorthCapitalDocumentsApi {
    addDocumentsForOffering(
        request: AddDocumentsForOfferingRequest,
        uploadedFile?: File | Buffer | unknown,
    ): Observable<AddDocumentsForOfferingResponse> | Promise<AddDocumentsForOfferingResponse>;
    addSubscriptionsForOffering(
        request: AddSubscriptionsForOfferingRequest,
    ): Observable<AddSubscriptionsForOfferingResponse> | Promise<AddSubscriptionsForOfferingResponse>;
    deleteDocumentsForOffering(
        request: DeleteDocumentsForOfferingRequest,
    ): Observable<DeleteDocumentsForOfferingResponse> | Promise<DeleteDocumentsForOfferingResponse>;
    deleteSubscriptionsForOffering(
        request: DeleteSubscriptionsForOfferingRequest,
    ): Observable<DeleteSubscriptionsForOfferingResponse> | Promise<DeleteSubscriptionsForOfferingResponse>;
    getDocumentsForOffering(
        request: GetDocumentsForOfferingRequest,
    ): Observable<GetDocumentsForOfferingResponse> | Promise<GetDocumentsForOfferingResponse>;
    getSubscriptionsForOffering(
        request: GetSubscriptionsForOfferingRequest,
    ): Observable<GetSubscriptionsForOfferingResponse> | Promise<GetSubscriptionsForOfferingResponse>;
    getTradeDocument(request: GetTradeDocumentRequest): Observable<GetTradeDocumentResponse> | Promise<GetTradeDocumentResponse>;
    getUploadPartyDocument(
        request: GetUploadPartyDocumentRequest,
    ): Observable<GetUploadPartyDocumentResponse> | Promise<GetUploadPartyDocumentResponse>;
    requestUploadPartyDocument(
        request: RequestUploadPartyDocumentRequest,
        uploadedFile?: File | Buffer | unknown,
    ): Observable<RequestUploadPartyDocumentResponse> | Promise<RequestUploadPartyDocumentResponse>;
    resendPartyAgreement(
        request: ResendPartyAgreementRequest,
    ): Observable<ResendPartyAgreementResponse> | Promise<ResendPartyAgreementResponse>;
    resendSubscriptionDocuments(
        request: ResendSubscriptionDocumentsRequest,
    ): Observable<ResendSubscriptionDocumentsResponse> | Promise<ResendSubscriptionDocumentsResponse>;
    sendPartyAgreement(request: SendPartyAgreementRequest): Observable<SendPartyAgreementResponse> | Promise<SendPartyAgreementResponse>;
    sendSubscriptionDocument(
        request: SendSubscriptionDocumentRequest,
    ): Observable<SendSubscriptionDocumentResponse> | Promise<SendSubscriptionDocumentResponse>;
    updateDocumentForOffering(
        request: UpdateDocumentForOfferingRequest,
    ): Observable<UpdateDocumentForOfferingResponse> | Promise<UpdateDocumentForOfferingResponse>;
    updateSubscriptionsForOffering(
        request: UpdateSubscriptionsForOfferingRequest,
    ): Observable<UpdateSubscriptionsForOfferingResponse> | Promise<UpdateSubscriptionsForOfferingResponse>;
    updateTradeDocArchiveStatus(
        request: UpdateTradeDocArchiveStatusRequest,
    ): Observable<UpdateTradeDocArchiveStatusResponse> | Promise<UpdateTradeDocArchiveStatusResponse>;
    uploadAccountDocument(
        request: UploadAccountDocumentRequest,
        uploadedFile?: File | Buffer | unknown,
    ): Observable<UploadAccountDocumentResponse> | Promise<UploadAccountDocumentResponse>;
    uploadEntityDocument(
        request: UploadEntityDocumentRequest,
        uploadedFile?: File | Buffer | unknown,
    ): Observable<UploadEntityDocumentResponse> | Promise<UploadEntityDocumentResponse>;
    uploadPartyDocument(
        request: UploadPartyDocumentRequest,
        uploadedFile?: File | Buffer | unknown,
    ): Observable<UploadPartyDocumentResponse> | Promise<UploadPartyDocumentResponse>;
    uploadTradeDocument(
        request: UploadTradeDocumentRequest,
        uploadedFile?: File | Buffer | unknown,
    ): Observable<UploadTradeDocumentResponse> | Promise<UploadTradeDocumentResponse>;
    getAllSignedDocument(
        request: GetAllSignedDocumentRequest,
    ): Observable<GetAllSignedDocumentResponse> | Promise<GetAllSignedDocumentResponse>;
    fetchSubscriptionDocuments(): Observable<FetchSubscriptionDocumentsResponse> | Promise<FetchSubscriptionDocumentsResponse>;
}
