import type { AddCreditCardRequest } from '../dto/requests/add-credit-card.request';
import type { AddCreditCardResponse } from '../dto/responses/add-credit-card.response';
import { Observable } from 'rxjs';
import type { DeleteCreditCardRequest } from '../dto/requests/delete-credit-card.request';
import type { DeleteCreditCardResponse } from '../dto/responses/delete-credit-card.response';
import type { GetCreditCardRequest } from '../dto/requests/get-credit-card.request';
import type { GetCreditCardResponse } from '../dto/responses/get-credit-card.response';
import type { UpdateCreditCardRequest } from '../dto/requests/update-credit-card.request';
import type { UpdateCreditCardResponse } from '../dto/responses/update-credit-card.response';
import type { GetCcFundMoveInfoRequest } from '../dto/requests/get-cc-fund-move-info.request';
import type { GetCcFundMoveInfoResponse } from '../dto/responses/get-cc-fund-move-info.response';
import type { GetCcFundMoveHistoryRequest } from '../dto/requests/get-cc-fund-move-history.request';
import type { GetCcFundMoveHistoryResponse } from '../dto/responses/get-cc-fund-move-history.response';
import type { CcFundMoveRequest } from '../dto/requests/cc-fund-move.request';
import type { CcFundMoveResponse } from '../dto/responses/cc-fund-move.response';
import type { RequestForVoidCcTransactionRequest } from '../dto/requests/request-for-void-cc-transaction.request';
import type { RequestForVoidCcTransactionResponse } from '../dto/responses/request-for-void-cc-transaction.response';
import type { UpdateCcFundMoveStatusRequest } from '../dto/requests/update-cc-fund-move-status.request';
import type { UpdateCcFundMoveStatusResponse } from '../dto/responses/update-cc-fund-move-status.response';
import { CcFundMovementRequest } from '../dto/requests/cc-fund-movement.request';
import { CcFundMovementResponse } from '../dto/responses/cc-fund-movement.response';

export enum NorthCapitalCreditCardTransactionApiPaths {
    AddCreditCard = 'addCreditCard',
    DeleteCreditCard = 'deleteCreditCard',
    GetCreditCard = 'getCreditCard',
    UpdateCreditCard = 'updateCreditCard',
    GetCCFundMoveHistory = 'getCCFundMoveHistory',
    GetCCFundMoveInfo = 'getCCFundMoveInfo',
    CCFundMove = 'ccFundMove',
    CCFundMovement = 'ccFundMovement',
    UpdateCcFundMoveStatus = 'updateCCFundMoveStatus',
    RequestForVoidCCTransaction = 'requestForVoidCCTransaction',
}

export interface NorthCapitalCreditCardTransactionApi {
    addCreditCard(request: AddCreditCardRequest): Observable<AddCreditCardResponse> | Promise<AddCreditCardResponse>;
    deleteCreditCard(request: DeleteCreditCardRequest): Observable<DeleteCreditCardResponse> | Promise<DeleteCreditCardResponse>;
    getCreditCard(request: GetCreditCardRequest): Observable<GetCreditCardResponse> | Promise<GetCreditCardResponse>;
    updateCreditCard(request: UpdateCreditCardRequest): Observable<UpdateCreditCardResponse> | Promise<UpdateCreditCardResponse>;
    getCCFundMoveHistory(request: GetCcFundMoveHistoryRequest): Observable<GetCcFundMoveHistoryResponse> | Promise<GetCcFundMoveHistoryResponse>;
    getCCFundMoveInfo(request: GetCcFundMoveInfoRequest): Observable<GetCcFundMoveInfoResponse> | Promise<GetCcFundMoveInfoResponse>;
    cCFundMove(request: CcFundMoveRequest): Observable<CcFundMoveResponse> | Promise<CcFundMoveResponse>;
    cCFundMovement(request: CcFundMovementRequest): Observable<CcFundMovementResponse> | Promise<CcFundMovementResponse>;
    updateCCFundMoveStatus(request: UpdateCcFundMoveStatusRequest): Observable<UpdateCcFundMoveStatusResponse> | Promise<UpdateCcFundMoveStatusResponse>;
    requestForVoidCCTransaction(request: RequestForVoidCcTransactionRequest): Observable<RequestForVoidCcTransactionResponse> | Promise<RequestForVoidCcTransactionResponse>;
}
