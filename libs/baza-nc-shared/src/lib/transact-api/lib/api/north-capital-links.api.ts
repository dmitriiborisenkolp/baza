import type { CreateLinkRequest } from '../dto/requests/create-link.request';
import type { Observable } from 'rxjs';
import type { CreateLinkResponse } from '../dto/responses/create-link.response';
import type { DeleteLinkRequest } from '../dto/requests/delete-link.request';
import type { DeleteLinkResponse } from '../dto/responses/delete-link.response';
import type { GetAllLinksRequest } from '../dto/requests/get-all-links.request';
import type { GetAllLinksResponse } from '../dto/responses/get-all-links.response';
import type { GetLinkRequest } from '../dto/requests/get-link.request';
import type { GetLinkResponse } from '../dto/responses/get-link.response';

export enum NorthCapitalLinksApiPaths {
    CreateLink = 'createLink',
    DeleteLink = 'deleteLink',
    GetAllLinks = 'getAllLinks',
    GetLink = 'getLink',
}

export interface NorthCapitalLinksApi {
    createLink(request: CreateLinkRequest): Observable<CreateLinkResponse> | Promise<CreateLinkResponse>;
    deleteLink(request: DeleteLinkRequest): Observable<DeleteLinkResponse> | Promise<DeleteLinkResponse>;
    getAllLinks(request: GetAllLinksRequest): Observable<GetAllLinksResponse> | Promise<GetAllLinksResponse>;
    getLink(request: GetLinkRequest): Observable<GetLinkResponse> | Promise<GetLinkResponse>;
}
