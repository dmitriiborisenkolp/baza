import type { RequestCustodialAccountRequest } from '../dto/requests/request-custodial-account.request';
import { Observable } from 'rxjs';
import type { RequestCustodialAccountResponse } from '../dto/responses/request-custodial-account.response';
import type { SendCustodialAccountAgreementRequest } from '../dto/requests/send-custodial-account-agreement.request';
import type { SendCustodialAccountAgreementResponse } from '../dto/responses/send-custodial-account-agreement.response';
import type { UpdateCustodialAccountRequestResponse } from '../dto/responses/update-custodial-account-request.response';
import type { UpdateCustodialAccountRequestRequest } from '../dto/requests/update-custodial-account-request.request';

export enum NorthCapitalCustodialAccountApiPaths {
    RequestCustodialAccount = 'requestCustodialAccount',
    SendCustodialAccountAgreement = 'sendCustodialAccountAgreement',
    UpdateCustodialAccountRequest = 'updateCustodialAccountRequest',
}

export interface NorthCapitalCustodialAccountApi {
    requestCustodialAccount(request: RequestCustodialAccountRequest): Observable<RequestCustodialAccountResponse> | Promise<RequestCustodialAccountResponse>;
    sendCustodialAccountAgreement(request: SendCustodialAccountAgreementRequest): Observable<SendCustodialAccountAgreementResponse> | Promise<SendCustodialAccountAgreementResponse>;
    updateCustodialAccountRequest(request: UpdateCustodialAccountRequestRequest): Observable<UpdateCustodialAccountRequestResponse> | Promise<UpdateCustodialAccountRequestResponse>;
}
