import type { CreateEntityRequest } from '../dto/requests/create-entity.request';
import { Observable } from 'rxjs';
import type { CreateEntityResponse } from '../dto/responses/create-entity.response';
import type { DeleteEntityRequest } from '../dto/requests/delete-entity.request';
import type { DeleteEntityResponse } from '../dto/responses/delete-entity.response';
import type { DeleteEntityDocumentRequest } from '../dto/requests/delete-entity-document.request';
import type { DeleteEntityDocumentResponse } from '../dto/responses/delete-entity-document.response';
import type { GetEntityRequest } from '../dto/requests/get-entity.request';
import type { GetEntityResponse } from '../dto/responses/get-entity.response';
import type { GetEntityDocumentRequest } from '../dto/requests/get-entity-document.request';
import type { GetEntityDocumentResponse } from '../dto/responses/get-entity-document.response';
import type { SearchEntityRequest } from '../dto/requests/search-entity.request';
import type { SearchEntityResponse } from '../dto/responses/search-entity.response';
import type { UpdateEntityRequest } from '../dto/requests/update-entity.request';
import type { UpdateEntityResponse } from '../dto/responses/update-entity.response';
import type { UpdatePartyEntityArchiveStatusRequest } from '../dto/requests/update-party-entity-archive-status.request';
import type { UpdatePartyEntityArchiveStatusResponse } from '../dto/responses/update-party-entity-archive-status.response';

export enum NorthCapitalEntitiesApiPaths {
    CreateEntity = 'createEntity',
    DeleteEntity = 'deleteEntity',
    DeleteEntityDocument = 'deleteEntityDocument',
    GetEntity = 'getEntity',
    GetEntityDocument = 'getEntityDocument',
    SearchEntity = 'searchEntity',
    UpdateEntity = 'updateEntity',
    UpdatePartyEntityArchiveStatus = 'updatePartyEntityArchivestatus',
}

export interface NorthCapitalEntitiesApi {
    createEntity(request: CreateEntityRequest): Observable<CreateEntityResponse> | Promise<CreateEntityResponse>;
    deleteEntity(request: DeleteEntityRequest): Observable<DeleteEntityResponse> | Promise<DeleteEntityResponse>;
    deleteEntityDocument(request: DeleteEntityDocumentRequest): Observable<DeleteEntityDocumentResponse> | Promise<DeleteEntityDocumentResponse>;
    getEntity(request: GetEntityRequest): Observable<GetEntityResponse> | Promise<GetEntityResponse>;
    getEntityDocument(request: GetEntityDocumentRequest): Observable<GetEntityDocumentResponse> | Promise<GetEntityDocumentResponse>;
    searchEntity(request: SearchEntityRequest): Observable<SearchEntityResponse> | Promise<SearchEntityResponse>;
    updateEntity(request: UpdateEntityRequest): Observable<UpdateEntityResponse> | Promise<UpdateEntityResponse>;
    updatePartyEntityArchiveStatus(request: UpdatePartyEntityArchiveStatusRequest): Observable<UpdatePartyEntityArchiveStatusResponse> | Promise<UpdatePartyEntityArchiveStatusResponse>;
}
