import { CreateExternalAccountRequest } from '../dto/requests/create-external-account.request';
import { Observable } from 'rxjs';
import type { CreateExternalAccountResponse } from '../dto/responses/create-external-account.response';
import type { DeleteExternalAccountRequest } from '../dto/requests/delete-external-account.request';
import type { DeleteExternalAccountResponse } from '../dto/responses/delete-external-account.response';
import type { ExternalFundMoveRequest } from '../dto/requests/external-fund-move.request';
import type { ExternalFundMoveResponse } from '../dto/responses/external-fund-move.response';
import type { GetAchPendingIdRequest } from '../dto/requests/get-ach-pending-id.request';
import type { GetAchPendingIdResponse } from '../dto/responses/get-ach-pending-id.response';
import type { GetExternalAccountRequest } from '../dto/requests/get-external-account.request';
import type { GetExternalAccountResponse } from '../dto/responses/get-external-account.response';
import type { GetExternalFundMoveRequest } from '../dto/requests/get-external-fund-move.request';
import type { GetExternalFundMoveResponse } from '../dto/responses/get-external-fund-move.response';
import type { GetExternalFundMoveHistoryResponse } from '../dto/responses/get-external-fund-move-history.response';
import type { GetExternalFundMoveInfoRequest } from '../dto/requests/get-external-fund-move-info.request';
import type { GetExternalFundMoveInfoResponse } from '../dto/responses/get-external-fund-move-info.response';
import type { LinkExternalAccountRequest } from '../dto/requests/link-external-account.request';
import type { LinkExternalAccountResponse } from '../dto/responses/link-external-account.response';
import type { RequestForVoidACHRequest } from '../dto/requests/request-for-void-ach.request';
import type { RequestForVoidACHResponse } from '../dto/responses/request-for-void-ach.response';
import type { UpdateExternalAccountRequest } from '../dto/requests/update-external-account.request';
import type { UpdateExternalAccountResponse } from '../dto/responses/update-external-account.response';
import type { ValidateAbaRoutingNumberRequest } from '../dto/requests/validate-aba-routing-number.request';
import type { ValidateAbaRoutingNumberResponse } from '../dto/responses/validate-aba-routing-number.response';
import type { GetExternalFundMoveHistoryRequest } from '../dto/requests/get-external-fund-move-history.request';
import type { UpdateExternalFundMoveStatusRequest } from '../dto/requests/update-external-fund-move-status.request';
import type { UpdateExternalFundMoveStatusResponse } from '../dto/responses/update-external-fund-move-status.response';
import type { UpdateLinkExternalAccountRequest } from '../dto/requests/update-link-external-account.request';
import type { UpdateLinkExternalAccountResponse } from '../dto/responses/update-link-external-account.response';

export enum NorthCapitalAchTransfersApiPaths {
    CreateExternalAccount = 'createExternalAccount',
    DeleteExternalAccount = 'deleteExternalAccount',
    ExternalFundMove = 'externalFundMove',
    GetAchPendingId = 'getAchPendingId',
    GetExternalAccount = 'getExternalAccount',
    GetExternalFundMove = 'getExternalFundMove',
    GetExternalFundMoveHistory = 'getExternalFundMoveHistory',
    GetExternalFundMoveInfo = 'getExternalFundMoveInfo',
    LinkExternalAccount = 'linkExternalAccount',
    RequestForVoidACH = 'requestForVoidACH',
    UpdateExternalAccount = 'updateExternalAccount',
    ValidateABARoutingNumber = 'validateABARoutingNumber',
    UpdateExternalFundMoveStatus = 'updateExternalFundMoveStatus',
    UpdateLinkExternalAccount = 'updateLinkExternalAccount',
}

export interface NorthCapitalAchTransfersApi {
    createExternalAccount(request: CreateExternalAccountRequest): Observable<CreateExternalAccountResponse> | Promise<CreateExternalAccountResponse>;
    deleteExternalAccount(request: DeleteExternalAccountRequest): Observable<DeleteExternalAccountResponse> | Promise<DeleteExternalAccountResponse>;
    externalFundMove(request: ExternalFundMoveRequest): Observable<ExternalFundMoveResponse> | Promise<ExternalFundMoveResponse>;
    getAchPendingId(request: GetAchPendingIdRequest): Observable<GetAchPendingIdResponse> | Promise<GetAchPendingIdResponse>;
    getExternalAccount(request: GetExternalAccountRequest): Observable<GetExternalAccountResponse> | Promise<GetExternalAccountResponse>;
    getExternalFundMove(request: GetExternalFundMoveRequest): Observable<GetExternalFundMoveResponse> | Promise<GetExternalFundMoveResponse>;
    getExternalFundMoveHistory(request: GetExternalFundMoveHistoryRequest): Observable<GetExternalFundMoveHistoryResponse> | Promise<GetExternalFundMoveHistoryResponse>;
    getExternalFundMoveInfo(request: GetExternalFundMoveInfoRequest): Observable<GetExternalFundMoveInfoResponse> | Promise<GetExternalFundMoveInfoResponse>;
    linkExternalAccount(request: LinkExternalAccountRequest): Observable<LinkExternalAccountResponse> | Promise<LinkExternalAccountResponse>;
    requestForVoidACH(request: RequestForVoidACHRequest): Observable<RequestForVoidACHResponse> | Promise<RequestForVoidACHResponse>;
    updateExternalAccount(request: UpdateExternalAccountRequest): Observable<UpdateExternalAccountResponse> | Promise<UpdateExternalAccountResponse>;
    validateABARoutingNumber(request: ValidateAbaRoutingNumberRequest): Observable<ValidateAbaRoutingNumberResponse> | Promise<ValidateAbaRoutingNumberResponse>;
    updateExternalFundMoveStatus(request: UpdateExternalFundMoveStatusRequest): Observable<UpdateExternalFundMoveStatusResponse> | Promise<UpdateExternalFundMoveStatusResponse>;
    updateLinkExternalAccount(request: UpdateLinkExternalAccountRequest): Observable<UpdateLinkExternalAccountResponse> | Promise<UpdateLinkExternalAccountResponse>;
}
