import type { CancelOfferingRequest } from '../dto/requests/cancel-offering.request';
import type { Observable } from 'rxjs';
import type { CancelOfferingResponse } from '../dto/responses/cancel-offering.response';
import type { CloseOfferingRequest } from '../dto/requests/close-offering.request';
import type { CreateEscrowAccountRequest } from '../dto/requests/create-escrow-account.request';
import type { CreateOfferingRequest } from '../dto/requests/create-offering.request';
import type { DeleteEscrowAccountRequest } from '../dto/requests/delete-escrow-account.request';
import type { DeleteOfferingRequest } from '../dto/requests/delete-offering.request';
import type { GetCustodianDetailsForOfferingRequest } from '../dto/requests/get-custodian-details-for-offering.request';
import type { GetEscrowAccountRequest } from '../dto/requests/get-escrow-account.request';
import type { GetOfferingRequest } from '../dto/requests/get-offering.request';
import type { GetOfferingPurchaseHistoryRequest } from '../dto/requests/get-offering-purchase-history.request';
import type { GetOfferingStatusRequest } from '../dto/requests/get-offering-status.request';
import type { SearchOfferingRequest } from '../dto/requests/search-offering.request';
import type { UpdateEscrowAccountRequest } from '../dto/requests/update-escrow-account.request';
import type { UpdateOfferingRequest } from '../dto/requests/update-offering.request';
import type { CloseOfferingResponse } from '../dto/responses/close-offering.response';
import type { CreateEscrowAccountResponse } from '../dto/responses/create-escrow-account.response';
import type { CreateOfferingResponse } from '../dto/responses/create-offering.response';
import type { DeleteEscrowAccountResponse } from '../dto/responses/delete-escrow-account.response';
import type { DeleteOfferingResponse } from '../dto/responses/delete-offering.response';
import type { GetCustodianDetailsForOfferingResponse } from '../dto/responses/get-custodian-details-for-offering.response';
import type { GetEscrowAccountResponse } from '../dto/responses/get-escrow-account.response';
import type { GetOfferingPurchaseHistoryResponse } from '../dto/responses/get-offering-purchase-history.response';
import type { GetOfferingStatusResponse } from '../dto/responses/get-offering-status.response';
import type { SearchOfferingResponse } from '../dto/responses/search-offering.response';
import type { UpdateEscrowAccountResponse } from '../dto/responses/update-escrow-account.response';
import type { UpdateOfferingResponse } from '../dto/responses/update-offering.response';
import type { GetOfferingResponse } from '../dto/responses/get-offering.response';

export enum NorthCapitalOfferingsApiPaths {
    CancelOffering = 'cancelOffering',
    CloseOffering = 'closeOffering',
    CreateEscrowAccount = 'createEscrowAccount',
    CreateOffering = 'createOffering',
    DeleteEscrowAccount = 'deleteEscrowAccount',
    DeleteOffering = 'deleteOffering',
    GetCustodianDetailsForOffering = 'getCustodianDetailsforOffering',
    GetEscrowAccount = 'getEscrowAccount',
    GetOffering = 'getOffering',
    GetOfferingPurchaseHistory = 'getOfferingPurchaseHistory',
    GetOfferingStatus = 'getOfferingStatus',
    SearchOffering = 'searchOffering',
    UpdateEscrowAccount = 'updateEscrowAccount',
    UpdateOffering = 'updateOffering',
}

export interface NorthCapitalOfferingsApi {
    cancelOffering(request: CancelOfferingRequest): Observable<CancelOfferingResponse> | Promise<CancelOfferingResponse>;
    closeOffering(request: CloseOfferingRequest): Observable<CloseOfferingResponse> | Promise<CloseOfferingResponse>;
    createEscrowAccount(request: CreateEscrowAccountRequest): Observable<CreateEscrowAccountResponse> | Promise<CreateEscrowAccountResponse>;
    createOffering(request: CreateOfferingRequest): Observable<CreateOfferingResponse> | Promise<CreateOfferingResponse>;
    deleteEscrowAccount(request: DeleteEscrowAccountRequest): Observable<DeleteEscrowAccountResponse> | Promise<DeleteEscrowAccountResponse>;
    deleteOffering(request: DeleteOfferingRequest): Observable<DeleteOfferingResponse> | Promise<DeleteOfferingResponse>;
    getCustodianDetailsForOffering(request: GetCustodianDetailsForOfferingRequest): Observable<GetCustodianDetailsForOfferingResponse> | Promise<GetCustodianDetailsForOfferingResponse>;
    getEscrowAccount(request: GetEscrowAccountRequest): Observable<GetEscrowAccountResponse> | Promise<GetEscrowAccountResponse>;
    getOffering(request: GetOfferingRequest): Observable<GetOfferingResponse> | Promise<GetOfferingResponse>;
    getOfferingPurchaseHistory(request: GetOfferingPurchaseHistoryRequest): Observable<GetOfferingPurchaseHistoryResponse> | Promise<GetOfferingPurchaseHistoryResponse>;
    getOfferingStatus(request: GetOfferingStatusRequest): Observable<GetOfferingStatusResponse> | Promise<GetOfferingStatusResponse>;
    searchOffering(request: SearchOfferingRequest): Observable<SearchOfferingResponse> | Promise<SearchOfferingResponse>;
    updateEscrowAccount(request: UpdateEscrowAccountRequest): Observable<UpdateEscrowAccountResponse> | Promise<UpdateEscrowAccountResponse>;
    updateOffering(request: UpdateOfferingRequest): Observable<UpdateOfferingResponse> | Promise<UpdateOfferingResponse>;
}
