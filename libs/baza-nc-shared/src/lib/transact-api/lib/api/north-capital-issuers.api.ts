import type { CreateIssuerRequest } from '../dto/requests/create-issuer.request';
import type { Observable } from 'rxjs';
import type { CreateIssuerResponse } from '../dto/responses/create-issuer.response';
import type { CreateIssuerAccountRequest } from '../dto/requests/create-issuer-account.request';
import type { CreateIssuerAccountResponse } from '../dto/responses/create-issuer-account.response';
import type { DeleteIssuerRequest } from '../dto/requests/delete-issuer.request';
import type { DeleteIssuerResponse } from '../dto/responses/delete-issuer.response';
import type { DeleteIssuerAccountRequest } from '../dto/requests/delete-issuer-account.request';
import type { DeleteIssuerAccountResponse } from '../dto/responses/delete-issuer-account.response';
import type { GetIssuerRequest } from '../dto/requests/get-issuer.request';
import type { GetIssuerResponse } from '../dto/responses/get-issuer.response';
import type { SearchIssuerRequest } from '../dto/requests/search-issuer.request';
import type { SearchIssuerResponse } from '../dto/responses/search-issuer.response';
import type { UpdateIssuerRequest } from '../dto/requests/update-issuer.request';
import type { UpdateIssuerResponse } from '../dto/responses/update-issuer.response';
import type { UpdateIssuerAccountRequest } from '../dto/requests/update-issuer-account.request';
import type { UpdateIssuerAccountResponse } from '../dto/responses/update-issuer-account.response';
import type { GetIssuerAccountRequest } from '../dto/requests/get-issuer-account.request';
import type { GetIssuerAccountResponse } from '../dto/responses/get-issuer-account.response';

export enum NorthCapitalIssuersApiPaths {
    CreateIssuer = 'createIssuer',
    CreateIssuerAccount = 'createIssuerAccount',
    DeleteIssuer = 'deleteIssuer',
    DeleteIssuerAccount = 'deleteIssuerAccount',
    GetIssuer = 'getIssuer',
    GetIssuerAccount = 'getIssuerAccount',
    SearchIssuer = 'searchIssuer',
    UpdateIssuer = 'updateIssuer',
    UpdateIssuerAccount = 'updateIssuerAccount',
}

export interface NorthCapitalIssuersApi {
    createIssuer(request: CreateIssuerRequest): Observable<CreateIssuerResponse> | Promise<CreateIssuerResponse>;
    createIssuerAccount(request: CreateIssuerAccountRequest): Observable<CreateIssuerAccountResponse> | Promise<CreateIssuerAccountResponse>;
    deleteIssuer(request: DeleteIssuerRequest): Observable<DeleteIssuerResponse> | Promise<DeleteIssuerResponse>;
    deleteIssuerAccount(request: DeleteIssuerAccountRequest): Observable<DeleteIssuerAccountResponse> | Promise<DeleteIssuerAccountResponse>;
    getIssuer(request: GetIssuerRequest): Observable<GetIssuerResponse> | Promise<GetIssuerResponse>;
    getIssuerAccount(request: GetIssuerAccountRequest): Observable<GetIssuerAccountResponse> | Promise<GetIssuerAccountResponse>;
    searchIssuer(request: SearchIssuerRequest): Observable<SearchIssuerResponse> | Promise<SearchIssuerResponse>;
    updateIssuer(request: UpdateIssuerRequest): Observable<UpdateIssuerResponse> | Promise<UpdateIssuerResponse>;
    updateIssuerAccount(request: UpdateIssuerAccountRequest): Observable<UpdateIssuerAccountResponse> | Promise<UpdateIssuerAccountResponse>;
}
