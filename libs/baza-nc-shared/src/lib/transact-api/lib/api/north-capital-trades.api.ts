import type { CreateTradeRequest } from '../dto/requests/create-trade.request';
import type { Observable } from 'rxjs';
import type { CreateTradeResponse } from '../dto/responses/create-trade.response';
import type { DeleteTradeRequest } from '../dto/requests/delete-trade.request';
import type { DeleteTradeResponse } from '../dto/responses/delete-trade.response';
import type { EditTradeRequest } from '../dto/requests/edit-trade.request';
import type { EditTradeResponse } from '../dto/responses/edit-trade.response';
import type { FundReturnRequestRequest } from '../dto/requests/fund-return-request.request';
import type { FundReturnRequestResponse } from '../dto/responses/fund-return-request.response';
import type { GetAccountTradeHistoryRequest } from '../dto/requests/get-account-trade-history.request';
import type { GetAccountTradeHistoryResponse } from '../dto/responses/get-account-trade-history.response';
import type { GetTradeRequest } from '../dto/requests/get-trade.request';
import type { GetTradeResponse } from '../dto/responses/get-trade.response';
import type { GetTradeStatusRequest } from '../dto/requests/get-trade-status.request';
import type { GetTradeStatusResponse } from '../dto/responses/get-trade-status.response';
import type { UpdateTradeArchiveStatusRequest } from '../dto/requests/update-trade-archive-status.request';
import type { UpdateTradeArchiveStatusResponse } from '../dto/responses/update-trade-archive-status.response';
import type { UpdateTradeStatusRequest } from '../dto/requests/update-trade-status.request';
import type { UpdateTradeStatusResponse } from '../dto/responses/update-trade-status.response';
import type { UpdateTradeTransactionTypeRequest } from '../dto/requests/update-trade-transaction-type.request';
import type { UpdateTradeTransactionTypeResponse } from '../dto/responses/update-trade-transaction-type.response';
import type { UpdateDocuSignStatusRequest } from '../dto/requests/update-docu-sign-status.request';
import type { UpdateDocuSignStatusResponse } from '../dto/responses/update-docu-sign-status.response';
import { GetTradesForOfferingRequest } from '../dto/requests/get-trades-for-offering.request';
import { GetTradesForOfferingResponse } from '../dto/responses/get-trades-for-offering.response';
import { GetCcFundMoveInfoResponse } from '../dto/responses/get-cc-fund-move-info.response';
import { GetCcFundMoveInfoRequest } from '../dto/requests/get-cc-fund-move-info.request';
import { GetExternalFundMoveInfoRequest } from '../dto/requests/get-external-fund-move-info.request';
import { GetExternalFundMoveInfoResponse } from '../dto/responses/get-external-fund-move-info.response';

export enum NorthCapitalTradesApiPaths {
    CreateTrade = 'createTrade',
    DeleteTrade = 'deleteTrade',
    EditTrade = 'editTrade',
    FundReturnRequest = 'fundReturnRequest',
    GetAccountTradeHistory = 'getAccountTradeHistory',
    GetTrade = 'getTrade',
    GetTradeStatus = 'getTradeStatus',
    UpdateTradeArchiveStatus = 'updateTradeArchivestatus',
    UpdateTradeStatus = 'updateTradeStatus',
    UpdateTradeTransactionType = 'updateTradeTransactionType',
    UpdateDocuSignStatus = 'updateDocuSignStatus',
    GetTradesForOffering = 'getTradesForOffering',
    GetExternalFundMoveInfo = 'getExternalFundMoveInfo',
    GetCCFundMoveInfo = 'getCCFundMoveInfo',
}

export interface NorthCapitalTradesApi {
    createTrade(request: CreateTradeRequest): Observable<CreateTradeResponse> | Promise<CreateTradeResponse>;
    deleteTrade(request: DeleteTradeRequest): Observable<DeleteTradeResponse> | Promise<DeleteTradeResponse>;
    editTrade(request: EditTradeRequest): Observable<EditTradeResponse> | Promise<EditTradeResponse>;
    fundReturnRequest(
        request: FundReturnRequestRequest,
    ): Observable<FundReturnRequestResponse> | Promise<FundReturnRequestResponse>;
    getAccountTradeHistory(
        request: GetAccountTradeHistoryRequest,
    ): Observable<GetAccountTradeHistoryResponse> | Promise<GetAccountTradeHistoryResponse>;
    getTrade(request: GetTradeRequest): Observable<GetTradeResponse> | Promise<GetTradeResponse>;
    getTradeStatus(
        request: GetTradeStatusRequest,
    ): Observable<GetTradeStatusResponse> | Promise<GetTradeStatusResponse>;
    updateTradeArchiveStatus(
        request: UpdateTradeArchiveStatusRequest,
    ): Observable<UpdateTradeArchiveStatusResponse> | Promise<UpdateTradeArchiveStatusResponse>;
    updateTradeStatus(
        request: UpdateTradeStatusRequest,
    ): Observable<UpdateTradeStatusResponse> | Promise<UpdateTradeStatusResponse>;
    updateTradeTransactionType(
        request: UpdateTradeTransactionTypeRequest,
    ): Observable<UpdateTradeTransactionTypeResponse> | Promise<UpdateTradeTransactionTypeResponse>;
    updateDocuSignStatus(
        request: UpdateDocuSignStatusRequest,
    ): Observable<UpdateDocuSignStatusResponse> | Promise<UpdateDocuSignStatusResponse>;
    getTradesForOffering(
        request: GetTradesForOfferingRequest,
    ): Observable<GetTradesForOfferingResponse> | Promise<GetTradesForOfferingResponse>;
    getExternalFundMoveInfo(
        request: GetExternalFundMoveInfoRequest,
    ): Observable<GetExternalFundMoveInfoRequest> | Promise<GetExternalFundMoveInfoResponse>;
    getCCFundMoveInfo(
        request: GetCcFundMoveInfoRequest,
    ): Observable<GetCcFundMoveInfoResponse> | Promise<GetCcFundMoveInfoResponse>;
}
