export enum NorthCapitalApiDocsTags {
    Account = 'North Capital Proxy – API – Accounts',
    AccreditationVerification = 'North Capital Proxy – API – Accreditation Verification',
    ACHDistributions = 'North Capital Proxy – API – ACH Distributions',
    ACHTransfers = 'North Capital Proxy – API – ACH Transfers',
    CreditCardTransactions = 'North Capital Proxy – API – Credit Card Transactions',
    CustodialAccount = 'North Capital Proxy – API – Custodial Account',
    Documents = 'North Capital Proxy – API – Documents',
    Issuers = 'North Capital Proxy – API – Issuers',
    KYC_AML = 'North Capital Proxy – API – KYC / AML',
    Links = 'North Capital Proxy – API – Links',
    Offerings = 'North Capital Proxy – API – Offerings',
    Parties = 'North Capital Proxy – API – Parties',
    Entities = 'North Capital Proxy – API – Parties (Entities)',
    Trades = 'North Capital Proxy – API – Trades',
}

export enum NorthCapitalApiDocsWorkflowsTags {
    AccreditedInvestorVerificationRequest = 'North Capital Proxy – Workflows – Accredited Investor Request',
    CreateAnOffering = 'North Capital Proxy – Workflows – Create an Offering',
    FundMovementProcess = 'North Capital Proxy – Workflows – Fund Movement Process',
    ManualEntityKycAmlReview = 'North Capital Proxy – Workflows – Manual Entity KYC/AML Review',
    OnboardAnEntity = 'North Capital Proxy – Workflows – Onboard an Entity',
    OnboardAnIndividual = 'North Capital Proxy – Workflows – Onboard an Individual',
    PerformKycYml = 'North Capital Proxy – Workflows – Perform KYC/AML',
    SendSubscriptionDocumentsViaDocuSign = 'North Capital Proxy – Workflows – Subscription Docs via DocuSign',
    Suitability = 'North Capital Proxy – Workflows – Suitability',
    TrackingTradeStatuses = 'North Capital Proxy – Workflows – Tracking Trade Statuses',
}
