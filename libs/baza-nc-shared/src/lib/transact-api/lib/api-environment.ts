// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace NorthCapitalApiEnvironments {
    export interface ApiEnvironment {
        /**
         * Credentials - Client Id
         * Could be found in Client Admin Panel -> Administrative
         */
        BAZA_NORTH_CAPITAL_CLIENT_ID: string;

        /**
         * Credentials - Developer API key
         * Could be found in Client Admin Panel -> Administrative
         */
        BAZA_NORTH_CAPITAL_DEVELOPER_API_KEY: string;

        /**
         * Base URL for all NC API requests
         * Sandbox - https://api-sandboxdash.norcapsecurities.com
         * Production - https://api.norcapsecurities.com
         */
        BAZA_NORTH_CAPITAL_BASE_URL: string;

        /**
         * Enables additional Debug tool
         */
        BAZA_NORTH_CAPITAL_DEBUG?: string;

        /**
         * Enables additional Logs
         */
        BAZA_NORTH_CAPITAL_DEBUG_NESTJS_LOG?: string;

        /**
         * Purchase Flow TTL
         * Controls how long active session will be active w/o actions from user
         * Inactive sessions will be automatically cancelled and shared will be
         * free after Purchase Flow TTL
         */
        BAZA_NORTH_CAPITAL_PURCHASE_FLOW_TTL: string;
    }

    export interface WebhookEnvironment {
        /**
         * Webhook Key
         * Used as part of URL for Webhook Catcher endpoint
         * Used as very basic security measure
         * It is recommended to specify BAZA_NORTH_CAPITAL_WEBHOOK_ENCRYPTION_KEY,
         * especially for production environments
         * @see BAZA_NORTH_CAPITAL_WEBHOOK_ENCRYPTION_KEY
         */
        BAZA_NORTH_CAPITAL_WEBHOOK_KEY: string;

        /**
         * Webhook Encryption Key
         * Could be Set Up / Found at Administrative -> Update Webhook Auth Key
         * Adds sha256 encryption / parsing for Webhook Body received by Webhook
         * Catcher endpoint
         * It's mandatory to enable it for Production environments
         */
        BAZA_NORTH_CAPITAL_WEBHOOK_ENCRYPTION_KEY?: string;

        /**
         * Enables additional Debug tools for Webhooks
         */
        BAZA_NORTH_CAPITAL_WEBHOOK_DEBUG?: string;

        /**
         * Pipes (Duplicates) Webhooks to Kafka
         * Used primary for Development reasons to allow developers to receive
         * webhooks
         * Usually enabled for Test environment and disabled for all other environments
         * BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND and BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_READ
         * must not be enabled at same time!
         * @see BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_READ
         */
        BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND?: string;

        /**
         * Reads Webhooks from Kafka
         * Used primary for Development reasons to allow developers to receive
         * webhooks
         * Usually enabled for Local (development) environment and disabled for all other environments
         * BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND and BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_READ
         * must not be enabled at same time!
         * @see BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND
         */
        BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_READ?: string;
    }

    export interface IssuerEnvironment {
        /**
         * Provides default Issuer which will be used for creating new Offerings
         * It's recommended to provided it for all environments
         * If environment variable is not provided, a new Issuer will be generated
         * for each offering
         */
        BAZA_NORTH_CAPITAL_ISSUER_ID?: string;
    }
}
