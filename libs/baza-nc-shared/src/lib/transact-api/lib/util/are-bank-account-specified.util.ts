import { PlaidBankAccountDetails } from '../dto';

export function areBankAccountDetailsSpecified(details: PlaidBankAccountDetails): boolean {
    return !! details.AccountName
        && !! details.AccountNumber
        && !! details.AccountRoutingNumber;
}
