import { BAZA_NC_TRANSACT_API_PRECISION } from '../common';

export function convertToCents(input: number): number {
    return Math.round(input * Math.pow(10, BAZA_NC_TRANSACT_API_PRECISION - (BAZA_NC_TRANSACT_API_PRECISION - 2)));
}
