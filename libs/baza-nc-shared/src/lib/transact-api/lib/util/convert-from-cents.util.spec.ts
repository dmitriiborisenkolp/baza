import { convertFromCents } from './convert-from-cents.util';

describe('@scaliolabs/baza-nc-shares/src/lib/transact-api/lib/util/convert-from-cents.util.spec.ts', () => {
    const testCases: Array<{
        input: number;
        output: number;
    }> = [
        {
            input: 0,
            output: 0,
        },
        {
            input: 1,
            output: 0.01,
        },
        {
            input: 10,
            output: 0.1,
        },
        {
            input: 15,
            output: 0.15,
        },
        {
            input: 121,
            output: 1.21,
        },
        {
            input: 3112,
            output: 31.12,
        },
        {
            input: 5113,
            output: 51.13,
        },
    ];

    it('will correctly convert dollars to cents', async () => {
        for (const testCase of testCases) {
            expect(convertFromCents(testCase.input)).toBe(testCase.output);
        }
    });
});
