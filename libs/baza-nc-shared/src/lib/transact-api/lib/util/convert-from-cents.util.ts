export function convertFromCents(input: number): number {
    const value = (input + '').replace(/[^\d.-]/g, '');

    return parseFloat(value) ? parseFloat(value) / 100 : 0;
}
