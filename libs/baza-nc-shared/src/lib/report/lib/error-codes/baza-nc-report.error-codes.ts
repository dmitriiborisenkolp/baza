export enum BazaNcReportErrorCodes {
    BazaNcReportNotFound = 'BazaNcReportNotFound',
    BazaNcReportCannotBeSent = 'BazaNcReportCannotBeSent',
    BazaNcReportAlreadySent = 'BazaNcReportAlreadySent',
    BazaNcReportCannotBeSynced = 'BazaNcReportCannotBeSynced',
}

export const bazaNcReportErrorCodesI18n = {
    [BazaNcReportErrorCodes.BazaNcReportNotFound]: 'Report Not Found',
    [BazaNcReportErrorCodes.BazaNcReportCannotBeSent]: 'Report cannot be sent',
    [BazaNcReportErrorCodes.BazaNcReportAlreadySent]: 'Report is already sent',
    [BazaNcReportErrorCodes.BazaNcReportCannotBeSynced]: 'Report cannot be synced',
};
