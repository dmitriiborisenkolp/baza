import { BazaNcReportStatus } from '../models/baza-nc-report-status';

export const bazaNcReportCmsI18n = {
    __types: {
        BazaNcReportStatus: {
            [BazaNcReportStatus.PendingPayment]: 'Pending Payment',
            [BazaNcReportStatus.ReadyToReport]: 'Ready to Report',
            [BazaNcReportStatus.Reported]: 'Reported',
        },
    },
    report: {
        title: 'Daily Reports',
        components: {
            bazaNcReportCms: {
                title: 'Daily Reports',
                noRecordsInDateRange: 'There are no transaction ready to report for selected date. Please choose another date instead.',
                columns: {
                    tradeId: 'Trade ID',
                    achId: 'Individual ACH ID',
                    dateCreatedAt: 'Date Created',
                    dateFundedAt: 'Date Funded',
                    dateReportedAt: 'Date Reported',
                    status: 'Status',
                },
                actions: {
                    exportToCSV: 'Export to CSV',
                    sendOne: {
                        title: 'Send Report to North Capital Escrow OPS',
                        confirm: 'Do you really want to send report for Trade {{ tradeId }}?',
                        success: 'Report for Trade {{ tradeId }} sent to North Capital Escrow OPS',
                    },
                    resendOne: {
                        title: 'Resend Report to North Capital Escrow OPS',
                        confirm: 'Do you really want to resend report for Trade {{ tradeId }}?',
                        success: 'Report for Trade {{ tradeId }} sent to North Capital Escrow OPS',
                    },
                    sendMany: {
                        title: 'Send Bulk Report',
                        confirm:
                            'Reports with "Ready to report" statuses will be sent to North Capital Escrow OPS. Do you want to proceed?',
                        success: 'Reports sent to North Capital Escrow OPS',
                    },
                    sync: {
                        title: 'Update (Sync) Report Status',
                        success: 'Report for Trade {{ tradeId }} was updated',
                    },
                    syncAll: {
                        title: 'Update (Sync) All Reports',
                        confirm: 'All reports with "Pending Payment" statuses will be synced with Dwolla API. Do you want to proceed?',
                        success: 'Synchronisation with Dwolla API started. Please update page later when synchronisation will be finished',
                    },
                },
            },
        },
    },
};
