import { OrderStatus } from '../../../transact-api';

export enum UserPurchaseState {
    None = 'NONE',
    PendingPayment = 'PENDING_PAYMENT',
    PaymentRejected = 'PAYMENT_REJECTED',
    PaymentCancelled = 'PAYMENT_CANCELLED',
    PaymentFunded = 'PAYMENT_FUNDED',
    PaymentConfirmed = 'INVESTMENT_IS_CONFIRMED',
    UnwindPending = 'UNWIND_PENDING',
    UnwindCanceled = 'UNWIND_CANCELLED',
    UnwindSettled = 'UNWIND_SETTLED',
}

export const mapUserPurchaseState: Array<{ match: OrderStatus[]; setStatus: UserPurchaseState }> = [
    {
        match: [OrderStatus.Created, OrderStatus.Pending],
        setStatus: UserPurchaseState.PendingPayment,
    },
    {
        match: [OrderStatus.Funded],
        setStatus: UserPurchaseState.PaymentFunded,
    },
    {
        match: [OrderStatus.Settled],
        setStatus: UserPurchaseState.PaymentConfirmed,
    },
    {
        match: [OrderStatus.Canceled],
        setStatus: UserPurchaseState.PaymentCancelled,
    },
    {
        match: [OrderStatus.Rejected],
        setStatus: UserPurchaseState.PaymentRejected,
    },
    {
        match: [OrderStatus.UnwindCreated, OrderStatus.UnwindPending],
        setStatus: UserPurchaseState.UnwindPending,
    },
    {
        match: [OrderStatus.UnwindCanceled],
        setStatus: UserPurchaseState.UnwindCanceled,
    },
    {
        match: [OrderStatus.UnwindSettled],
        setStatus: UserPurchaseState.UnwindSettled,
    },
];
