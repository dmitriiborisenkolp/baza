import { BazaCreditCardType } from '@scaliolabs/baza-core-shared';
import { CardType } from '../../../transact-api';

export { CardType as BazaNcPurchaseFlowCreditCardType };

export const bazaNcPurchaseFlowCreditCardTypes: () => Array<{
    nc: CardType;
    baza: BazaCreditCardType;
}> = () => [
    {
        nc: CardType.Visa,
        baza: BazaCreditCardType.Visa,
    },
    {
        nc: CardType.MasterCard,
        baza: BazaCreditCardType.MasterCard,
    },
    {
        nc: CardType.DI,
        baza: BazaCreditCardType.Discover,
    },
];
