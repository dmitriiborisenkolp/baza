export enum BazaNcPurchaseFlowErrorCodes {
    OfferingIsComingSoon = 'OfferingIsComingSoon',
    OfferingIsClosed = 'OfferingIsClosed',
    SessionCanNotBeStarted = 'SessionCanNotBeStarted',
    DocumentsAreNotSigned = 'DocumentsAreNotSigned',
    TradeSessionNotFound = 'TradeSessionNotFound',
    NoBankAccount = 'NoBankAccount',
    NoCreditCard = 'NoCreditCard',
    NorthCapitalAccountIsNotReady = 'NorthCapitalAccountIsNotReady',
    AmountMismatch = 'AmountMismatch',
    UnknownTransactionType = 'UnknownTransactionType',
    InvalidPaymentReprocess = 'InvalidPaymentReprocess',
    InvalidRoutingNumber = 'InvalidRoutingNumber',
    CreditCardIsInvalid = 'CreditCardIsInvalid',
    CreditCardIsNotSupported = 'CreditCardIsNotSupported',
    CreditCardIsExpired = 'CreditCardIsExpired',
    CreditCardIsOutOfRange = 'CreditCardIsOutOfRange',
    PurchaseFlowLimitsViolation = 'PurchaseFlowLimitsViolation',
    InvalidCardDetails = 'InvalidCardDetails',
    DocuSignUrlsAreDisabledForCurrentEnvironment = 'DocuSignUrlsAreDisabledForCurrentEnvironment',
    InvestorPurchaseDeniedException = 'InvestorPurchaseDeniedException',
    LegacyBankAccountSystemDisabled = 'LegacyBankAccountSystemDisabled',
    UnknownPurchaseFlowStrategy = 'UnknownPurchaseFlowStrategy',
    NoDwollaCustomer = 'NoDwollaCustomer',
    DwollaInsufficientFunds = 'DwollaInsufficientFunds',
    NotEnoughShares = 'NotEnoughShares',
}

export const bazaNcPurchaseFlowErrorCodesI18n = {
    [BazaNcPurchaseFlowErrorCodes.OfferingIsClosed]: 'Offering is closed',
    [BazaNcPurchaseFlowErrorCodes.OfferingIsComingSoon]: 'Offering is not open for purchases yet',
    [BazaNcPurchaseFlowErrorCodes.DocumentsAreNotSigned]: 'Documents are not signed yet',
    [BazaNcPurchaseFlowErrorCodes.TradeSessionNotFound]:
        'Your purchase session has timed out. Your shares will be released and no charges will be processed. Please proceed to the buy screen to make a purchase',
    [BazaNcPurchaseFlowErrorCodes.NoBankAccount]: 'Please link your bank account in order to submit your investment.',
    [BazaNcPurchaseFlowErrorCodes.NoCreditCard]: 'Please add your credit card in order to submit your investment.',
    [BazaNcPurchaseFlowErrorCodes.NorthCapitalAccountIsNotReady]:
        'There is no North Capital account associated with {{ projectName }} account yet',
    [BazaNcPurchaseFlowErrorCodes.AmountMismatch]: 'Amount does not suit with requested number of shares',
    [BazaNcPurchaseFlowErrorCodes.UnknownTransactionType]: 'Unknown transaction type',
    [BazaNcPurchaseFlowErrorCodes.InvalidPaymentReprocess]:
        'Payment Reprocess request is invalid, only failed (returned ) payments can be reprocessed',
    [BazaNcPurchaseFlowErrorCodes.InvalidRoutingNumber]: 'Invalid Routing Number. Please enter a valid routing number and try again.',
    [BazaNcPurchaseFlowErrorCodes.CreditCardIsInvalid]: 'Invalid Credit Card Number',
    [BazaNcPurchaseFlowErrorCodes.CreditCardIsNotSupported]: '{{ creditCard }} card is not supported',
    [BazaNcPurchaseFlowErrorCodes.CreditCardIsExpired]: 'You have entered an expired credit card',
    [BazaNcPurchaseFlowErrorCodes.CreditCardIsOutOfRange]: 'You have entered an invalid credit card',
    [BazaNcPurchaseFlowErrorCodes.PurchaseFlowLimitsViolation]:
        'Total purchase amount exceeds available limits for selected payment method',
    [BazaNcPurchaseFlowErrorCodes.InvalidCardDetails]: 'Invalid card details',
    [BazaNcPurchaseFlowErrorCodes.DocuSignUrlsAreDisabledForCurrentEnvironment]:
        'DocuSign URLs are disabled for current environments. Please note that every DocuSign URL costs ~$3.5 each.',
    [BazaNcPurchaseFlowErrorCodes.LegacyBankAccountSystemDisabled]: 'Legacy bank account system is disabled',
    [BazaNcPurchaseFlowErrorCodes.UnknownPurchaseFlowStrategy]: 'Unknown Purchase Flow strategy',
    [BazaNcPurchaseFlowErrorCodes.NoDwollaCustomer]: "You don't have Dwolla Customer yet",
    [BazaNcPurchaseFlowErrorCodes.DwollaInsufficientFunds]: 'Insufficient funds',
    [BazaNcPurchaseFlowErrorCodes.NotEnoughShares]: 'Offering doesn`t have enough shares',
};
