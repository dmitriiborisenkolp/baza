import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class PurchaseFlowPlaidLinkDto {
    @ApiModelProperty({
        type: 'string',
        description: 'URL to iframe/inject it any other way',
    })
    link: string;
}
