/**
 * Method used to add Bank Account
 *  - Manual - Bank Account is set by user providing all fields with inputs
 *  - Plaid - Bank Account created with Plaid integration
 */
export enum BazaNcBankAccountSource {
    Manual = 'manual',
    Plaid = 'plaid',
}
