// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace BazaNcBankAccountE2eConstants {
    export const E2E_PLAID_TOKEN_CHECKING = 'e2e-plaid-token-checking';
    export const E2E_PLAID_TOKEN_SAVINGS = 'e2e-plaid-token-savings';
}
