export function bazaNcExtractDocusignTemplateIdUtil(input: string): string {
    const matches = input.match(/^([a-zA-Z\d-]+)--(.*)$/);

    if (matches && matches.length === 3) {
        return matches[1];
    } else {
        return input;
    }
}
