export function bazaNcExtractDocusignTemplateNameUtil(input: string): string {
    const matches = input.match(/^([a-zA-Z\d-]+)--(.*)$/);

    if (matches && matches.length === 3) {
        return matches[2];
    } else {
        return input;
    }
}
