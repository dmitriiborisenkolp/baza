export enum BazaNcDocusignErrorCodes {
    NcDocusignTemplateNotFound = 'NcDocusignTemplateNotFound',
}

export const ncDocusignErrorCodesI18n = {
    [BazaNcDocusignErrorCodes.NcDocusignTemplateNotFound]: 'DocuSign template not found',
};
