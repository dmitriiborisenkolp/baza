import { Observable } from 'rxjs';

export enum BazaNcWebhooksEndpointPaths {
    publish = '/north-capital-proxy/webhooks/publish/:secret/:topic',
}

export interface BazaNcWebhooksEndpoint {
    publish(payload: unknown, secret: string, topic: string, correlationId: string): Promise<void> | Observable<void>;
}
