export const bazaNcDividendCmsEnI18n = {
    dividend: {
        title: 'Dividends',
        defaultDividendTitle: 'Dividends – {{ date }}',
        types: {
            BazaNcDividendPaymentSource: {
                NC: 'North Capital',
                Dwolla: 'Dwolla',
            },
            BazaNcDividendPaymentStatus: {
                Pending: 'Pending',
                Processed: 'Processed',
                Failed: 'Failed',
                Cancelled: 'Cancelled',
            },
            BazaNcDividendTransactionStatus: {
                Draft: 'Draft',
                Started: 'Started',
                InProgress: 'InProgress',
                Failed: 'Failed',
                Cancelled: 'Cancelled',
                Successful: 'Successful',
            },
            BazaNcDividendImportCsvEntryError: {
                enum: {
                    Exception: 'API Exception',
                    NotEnoughData: 'Not Enough Data',
                    InvestorAccountNotFound: 'Unknown Investor',
                    FailedToParseAmount: 'Invalid Amount',
                    AmountIsLessThanZero: 'Amount is Less Than 0',
                    FailedToParseDate: 'Invalid Date',
                    DateIsInFuture: 'Date in Future',
                    OfferingNotFound: 'Unknown Offering',
                },
                details: {
                    Exception: '{{ message }}',
                    NotEnoughData: 'CSV record should have Investor, Amount, Date and Offering columns',
                    InvestorAccountNotFound:
                        'Investor Account not found. Use Baza Account ID or North Capital Account ID to specify Investor',
                    FailedToParseAmount: 'Amount should contains valid value (Examples: "$1,00", "$1.00", "1 000 00,00" or "1.00")',
                    AmountIsLessThanZero: 'Amount should be more than zero',
                    FailedToParseDate: 'Date should contains valid value (Examples: "01.01.01", "4/15/2022")',
                    DateIsInFuture: 'Dividends should be marked as paid today or less',
                    OfferingNotFound: 'Offering not found. Use North Capital Offering ID to specify Offering',
                },
            },
        },
        components: {
            bazaNcDividend: {
                title: 'Dividends',
                search: 'Investor Account or Amount',
                resolve: {
                    errorInvestorAccount: 'Investor Account not found',
                },
                navigation: {
                    allDividends: 'Dividends',
                    investorAccount: 'Investor',
                    accountDividends: '{{ investorAccount.userFullName }}',
                },
                items: {
                    createDividendTransaction: 'Add Dividends',
                    navigateToDividendTransactions: 'Dividend Transactions',
                    exportToCsv: {
                        title: 'Export to CSV',
                        fileName: 'dividends-{{ date }}',
                        fields: {
                            ulid: 'ULID',
                            source: 'Source',
                            date: 'Date',
                            investorAccount: 'Investor Account',
                            ncAccountId: 'NC Account Id',
                            ncPartyId: 'NC Party Id',
                            amount: 'Amount',
                            status: 'Status',
                            offering: 'Offering',
                            ncOfferingId: 'NC Offering ID',
                        },
                    },
                },
                columns: {
                    investorAccount: 'Investor',
                    offeringId: 'NC Offering Id',
                    offeringTitle: 'Offering',
                    source: 'Source',
                    date: 'Date',
                    amount: 'Amount',
                    status: 'Status',
                },
                actions: {
                    navigateToDividendTransaction: 'Navigate to Dividend Transaction',
                    delete: {
                        title: 'Delete Dividend',
                        confirm: 'Do you really want to delete dividend (${{ amount }}, {{ investor }})?',
                    },
                },
                createDividendTransaction: {
                    title: 'New Dividends Transaction',
                    fields: {
                        title: 'Title (Optional)',
                    },
                },
            },
            bazaNcDividendTransaction: {
                title: 'Dividend Transactions',
                search: 'Search',
                navigation: {
                    allDividends: 'Dividends',
                    dividendTransactions: 'Dividend Transactions',
                },
                columns: {
                    title: 'Title',
                    dateCreatedAt: 'Date Created At',
                    dateUpdatedAt: 'Date Updated At',
                    dateProcessedAt: 'Date Processed At',
                    status: 'Status',
                },
                items: {
                    createDividendTransaction: 'Add Dividends',
                    exportToCsv: {
                        title: 'Export to CSV',
                        fields: {
                            ulid: 'ULID',
                            dateCreatedAt: 'Date Created At',
                            dateUpdatedAt: 'Date Updated At',
                            dateProcessedAt: 'Date Processed At',
                            status: 'Status',
                            title: 'Title',
                        },
                        fileName: 'dividend-transactions-{{ date }}',
                    },
                },
                actions: {
                    delete: {
                        title: 'Delete Dividends Transaction',
                        confirm: 'Do you really want to remove Dividends Transaction "{{ title }}"?',
                    },
                    process: 'Start (Process) Dividends Transaction',
                    navigate: 'Dividends',
                    sync: {
                        title: 'Sync',
                        confirm: 'Do you really want to synchronize payment statuses of dividends?',
                        success: 'Synchronization started. Refresh the page later to receive updates',
                    },
                },
                createDividendTransaction: {
                    title: 'New Dividends Transaction',
                    fields: {
                        title: 'Title (Optional)',
                    },
                },
            },
            bazaNcDividendTransactionEntries: {
                title: '{{ title }}',
                search: 'Investor Account or Amount',
                cannotBeProcessed: 'Dividends Transaction cannot be started',
                resolve: {
                    errDividendTransactionNotFound: 'Dividend Transaction not found',
                },
                navigation: {
                    allDividends: 'Dividends',
                    dividendTransactions: 'Dividend Transactions',
                    dividendTransactionEntries: 'Entries',
                },
                columns: {
                    date: 'Date',
                    dateCreatedAt: 'Date Created At',
                    dateUpdatedAt: 'Date Updated At',
                    dateProcessedAt: 'Date Processed At',
                    status: 'Status',
                    source: 'Source',
                    investorAccount: 'Investor',
                    offeringId: 'NC Offering Id',
                    offeringTitle: 'Offering',
                    amount: 'Amount',
                },
                actions: {
                    create: 'Add Dividend',
                    update: 'Edit Dividend',
                    refresh: 'Refresh',
                    delete: {
                        title: 'Delete Dividend',
                        confirm: 'Do you really want to delete dividend (${{ amount }}, {{ userFullName }})?',
                    },
                    process: {
                        title: 'Apply Dividends',
                        success: 'Check your mailbox for confirmation link',
                    },
                    reprocess: {
                        title: 'Reprocess',
                        confirm: 'Do you really want to reprocess payment (${{ amount }}, {{ userFullName }})?',
                        success: 'Reprocessing successfully started for Dividend (${{ amount }}, {{ userFullName }})',
                    },
                    sync: {
                        title: 'Sync',
                        confirm: 'Do you really want to synchronize payment statuses of dividends?',
                        success: 'Synchronization started. Refresh the page later to receive updates',
                    },
                    importCsv: 'Import CSV',
                    exportToCsv: {
                        title: 'Export to CSV',
                        fields: {
                            ulid: 'ULID',
                            source: 'Source',
                            date: 'Date',
                            investorAccount: 'Investor Account',
                            ncAccountId: 'NC Account Id',
                            ncPartyId: 'NC Party Id',
                            amount: 'Amount',
                            status: 'Status',
                            offering: 'Offering',
                            ncOfferingId: 'NC Offering ID',
                            dateCreatedAt: 'Date Created At',
                            dateUpdatedAt: 'Date Updated At',
                            dateProcessedAt: 'Date Processed At',
                        },
                        fileName: 'dividend-transaction-entries-{{ date }}',
                    },
                },
                form: {
                    titleNew: 'Add Dividend',
                    titleExisting: 'Edit Dividend',
                    fields: {
                        date: 'Date',
                        source: 'Source',
                        investorAccount: 'Investor',
                        amount: 'Amount',
                        offering: 'Offering',
                    },
                },
            },
            bazaNcDividendImportCsvModal: {
                selectFile: {
                    title: 'Import Dividends - CSV File',
                    message: 'Drag and drop the CSV file or click the button below to select the CSV file',
                    csvContents: 'Your CSV file should have a header containing these columns:',
                    ul: {
                        li_1_dl: 'Account',
                        li_1_dt: 'you can use "Account", "North Capital Account" or "NC Account" as the column name and data source',
                        li_2_dl: 'Amount',
                        li_2_dt: 'For clarity purposes, "$" sign may be added',
                        li_3_dl: 'Date',
                        li_3_dt: '"mm.dd.yyyy" format required. As an examples, "09/01/2022", "01.01.2022" or any other valid dates',
                        li_4_dl: 'Offering',
                        li_4_dt: 'you should use NC Offering Id as the column name',
                    },
                    delimiters: {
                        semicolon: 'Import from Microsoft Excel (";" separator)',
                        commas: 'Import from other applications ("," separator)',
                    },
                    source: {
                        nc: 'Dividends paid with North Capital',
                        dwolla: 'Pay dividends with Dwolla',
                    },
                    upload: 'Select CSV File',
                    cancel: 'Cancel',
                },
                errors: {
                    title: 'Failed to Import CSV File',
                    message: 'Please review errors below, update your CSV file and retry again.',
                    close: 'Close',
                    items: {
                        row: 'Row',
                        type: 'Error',
                        details: 'Details',
                    },
                },
                success: 'CSV file successfully imported. Please review new entries before publishing it to users',
            },
            bazaNcDividendConfirmModal: {
                title: 'Confirm dividends transaction',
                message:
                    'All dividends will be processed and published to users. Please type your account password to confirm dividends transaction.',
                password: 'Admin Password',
                submit: 'Submit',
                cancel: 'Cancel',
                success: 'Dividends transaction started',
                errors: {
                    BazaNcDividendConfirmationNotFound: 'Token is expired',
                    BazaNcDividendConfirmationInvalidPassword: 'Invalid Password',
                    BazaNcDividendConfirmationUnauthorized: "You're not authorised to process dividends transaction",
                },
            },
        },
    },
};
