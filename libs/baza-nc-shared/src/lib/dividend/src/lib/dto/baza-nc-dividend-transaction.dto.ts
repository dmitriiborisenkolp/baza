import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { ulid } from 'ulid';
import { BazaNcDividendTransactionStatus } from '../models/baza-nc-dividend-transaction-status';

export class BazaNcDividendTransactionDto {
    @ApiModelProperty({
        description: 'ULID (ID)',
        example: ulid(),
    })
    ulid: string;

    @ApiModelProperty({
        description: 'Date Created At',
        example: new Date().toISOString(),
    })
    dateCreatedAt: string;

    @ApiModelProperty({
        description: 'Date Updated At',
        example: new Date().toISOString(),
        required: false,
    })
    dateUpdatedAt?: string;

    @ApiModelProperty({
        description: 'Date Processed At',
        example: new Date().toISOString(),
        required: false,
    })
    dateProcessedAt?: string;

    @ApiModelProperty({
        type: 'string',
        description: 'Status',
        enum: Object.values(BazaNcDividendTransactionStatus),
        example: BazaNcDividendTransactionStatus.Draft,
    })
    status: BazaNcDividendTransactionStatus;

    @ApiModelProperty({
        description: 'Transaction Title',
    })
    title: string;

    @ApiModelProperty({
        description: 'Can be deleted?',
        example: 'Dividend Transaction 1',
    })
    canBeDeleted: boolean;

    @ApiModelProperty({
        description: 'Is Dividend Transaction locked?',
    })
    isLocked: boolean;

    @ApiModelProperty({
        description: 'Is Dividend Transaction processing now?',
    })
    isProcessing: boolean;
}
