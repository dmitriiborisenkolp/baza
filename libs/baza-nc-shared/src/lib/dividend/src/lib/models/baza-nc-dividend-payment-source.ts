export enum BazaNcDividendPaymentSource {
    NC = 'NC',
    Dwolla = 'Dwolla',
}

export const bazaNcDividendReprocessablePaymentSources: Array<BazaNcDividendPaymentSource> = [BazaNcDividendPaymentSource.Dwolla];
