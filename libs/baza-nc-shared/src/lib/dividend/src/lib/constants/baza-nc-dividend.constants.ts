export const BAZA_NC_DIVIDEND_CONSTANTS = {
    /**
     * Maximum records allowed for single CSV upload (Import Dividends with CSV)
     */
    maxCsvFileLines: 1000,
};
