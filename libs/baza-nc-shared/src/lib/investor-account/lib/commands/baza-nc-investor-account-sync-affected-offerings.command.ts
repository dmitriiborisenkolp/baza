import { OfferingId } from '../../../transact-api';

export class BazaNcInvestorAccountSyncAffectedOfferingsCommand {
    constructor(
        public payload: {
            investorAccountId: number;
        },
    ) {}
}

export class NcInvestorAccountSyncAffectedOfferingsCommandResponse {
    affectedOfferingIds: Array<OfferingId>;
}
