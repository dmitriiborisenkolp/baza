import { OfferingId } from '../../../transact-api';

export class BazaNcInvestorAccountSyncRemoveCommand {
    constructor(
        public payload: {
            investorAccountId: number;
        },
    ) {}
}

export class NcInvestorAccountSyncRemoveCommandResponse {
    affectedOfferingIds: Array<OfferingId>;
}
