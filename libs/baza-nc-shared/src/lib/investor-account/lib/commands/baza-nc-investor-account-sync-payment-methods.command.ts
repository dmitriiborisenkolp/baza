export class BazaNcInvestorAccountSyncPaymentMethodsCommand {
    constructor(
        public payload: {
            investorAccountId: number;
        },
    ) {}
}

export class NcInvestorAccountSyncPaymentMethodsCommandResponse {
    isBankAccountLinked: boolean;
    isCreditCardLinked: boolean;
}
