export class BazaNcInvestorAccountSyncTransactionsCommand {
    constructor(
        public payload: {
            investorAccountId: number;
        },
    ) {}
}

export class NcInvestorAccountSyncTransactionsCommandResponse {
    countTransactions: number;
}
