import { OfferingId } from '../../../transact-api';

export class BazaNcInvestorAccountSyncCommand {
    constructor(
        public payload: {
            investorAccountId: number;
        },
    ) {}
}

export class NcInvestorAccountSyncCommandResponse {
    email: string;
    affectedOfferingIds: Array<OfferingId>;
    isBankAccountLinked: boolean;
    isCreditCardLinked: boolean;
    countTransactions: number;
}
