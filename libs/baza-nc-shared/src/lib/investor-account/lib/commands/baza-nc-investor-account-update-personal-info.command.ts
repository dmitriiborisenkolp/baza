import { NorthCapitalDate } from "../../../transact-api";

interface CommandRequest {
    id: number;
    firstName?: string;
    lastName?: string;
    dateOfBirth?: NorthCapitalDate;
    ssn?: string;
    residentialCountry?: string;
    residentialCity?: string;
    residentialState?: string;
    residentialZipCode?: string;
    residentialStreetAddress1?: string;
    residentialStreetAddress2?: string;
}

export class BazaNcInvestorAccountUpdatePersonalInfoCommand {
    constructor(public readonly request: CommandRequest) {}
}