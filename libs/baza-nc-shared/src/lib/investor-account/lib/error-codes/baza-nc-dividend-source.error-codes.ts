export enum BazaNcDividendSourceErrorCodes {
    DividendSourceNotFound = 'DividendSourceNotFound',
    DividendSourceMustBeArray = 'DividendSourceMustBeArray',
    DividendSourceMustBeWithdrawType = 'DividendSourceMustBeWithdrawType',
    DividendSourceErrorType = 'DividendSourceErrorType',
}

export const bazaDividendSourceErrorCodesEnI18n = {
    [BazaNcDividendSourceErrorCodes.DividendSourceNotFound]: 'Dividend source not found',
    [BazaNcDividendSourceErrorCodes.DividendSourceMustBeArray]: 'Dividend source must be array',
    [BazaNcDividendSourceErrorCodes.DividendSourceMustBeWithdrawType]: 'Dividend sources must be of the withdrawal type',
    [BazaNcDividendSourceErrorCodes.DividendSourceErrorType]: 'Plaid type invalid',
};
