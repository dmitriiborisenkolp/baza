export const bazaNcInvestorAccountCmsEnI18n = {
    investorAccount: {
        title: 'Investor accounts',
        components: {
            ncInvestorAccountCms: {
                title: 'Investor accounts',
                search: 'Search...',
                forms: {
                    updateInvestorAccount: {
                        title: 'Edit account',
                        sections: {
                            personalInformation: 'Personal Information',
                            residentialAddress: 'Residential Address',
                        },
                        fields: {
                            firstName: 'First name',
                            lastName: 'Last name',
                            ssn: 'SSN',
                            dateOfBirth: 'Date of Birth',
                            country: 'Country',
                            streetAddress: 'Street Address',
                            streetAddress2: 'Street Address 2',
                            city: 'City',
                            state: 'State',
                            zipCode: 'Zip Code',
                        },
                        actions: {
                            submit: 'Save',
                            cancel: 'Cancel',
                        },
                        success: 'Investor account updated successfully',
                    },
                },
                actions: {
                    exportToCsv: 'Export investor accounts to CSV',
                    add: {
                        title: 'Add Investor Account',
                        form: {
                            fields: {
                                accountId: 'Account ID',
                                ncAccountId: 'North Capital Account ID',
                                ncPartyId: 'North Capital Party ID',
                                sync: 'Sync Transactions & Payment Methods',
                            },
                            actions: {
                                submit: 'Submit',
                                cancel: 'Cancel',
                            },
                            success: {
                                withSync: 'Investor account added with {{ countTransactions }} transactions',
                                withoutSync: 'Investor account added',
                            },
                        },
                    },
                    edit: {
                        title: 'Edit investor account',
                    },
                    remove: {
                        title: 'Remove (unlink) investor account',
                        confirm: 'Do you really want to unlink investor account?',
                        success: 'Investor account {{ northCapitalAccountId }}/{{ northCapitalPartyId }} removed',
                    },
                    dwolla: {
                        create: {
                            title: 'Create Dwolla customer',
                            success:
                                'Dwolla Customer was created for investor account {{ northCapitalAccountId }}/{{ northCapitalPartyId }}',
                        },
                        update: {
                            title: 'Update/Relink Dwolla customer',
                            success:
                                'Dwolla Customer link was updated for investor account {{ northCapitalAccountId }}/{{ northCapitalPartyId }}',
                        },
                        retryVerification: {
                            title: 'Retry Dwolla customer verification',
                        },
                    },
                    sync: {
                        title: 'Sync investor account and transactions',
                        success: 'Investor account and transactions {{ northCapitalAccountId }}/{{ northCapitalPartyId }} synced',
                        form: {
                            title: 'Sync investor account',
                            fields: {
                                ncAccountId: 'North Capital Account ID',
                                ncPartyId: 'North Capital Party ID',
                            },
                            actions: {
                                submit: 'Sync investor account & transactions',
                                submitTransactionsOnly: 'Sync transactions only',
                                cancel: 'Cancel',
                            },
                        },
                    },
                    syncTransactions: {
                        title: 'Sync transactions',
                        confirm: 'Do you really want to sync transactions?',
                        success:
                            'Transactions of investor account {{ northCapitalAccountId }}/{{ northCapitalPartyId }} started. Transactions will be soon added to Investor Account Transactions list',
                    },
                    syncAll: {
                        title: 'Sync all investor accounts',
                        confirm: 'Do you really want to sync ALL investor accounts?',
                        success: 'Syncing investor accounts started. Updates will be available later.',
                    },
                    syncPaymentMethods: {
                        single: {
                            title: 'Sync payment methods',
                            confirm:
                                'Do you really update payment methods for investor accounts ({{ northCapitalAccountId }}/{{ northCapitalPartyId }})?',
                            success: 'Payment method update for investor account ({{ northCapitalAccountId }}/{{ northCapitalPartyId }})',
                        },
                        bulk: {
                            title: 'Sync payment methods for all investor accounts',
                            confirm: 'Do you really update payment methods for ALL investor accounts?',
                            success: 'Syncing payment methods for investor accounts started. Updates will be available later',
                        },
                    },
                    syncAllDwollaCustomers: {
                        title: 'Update/Relink Dwolla Customer for All Investors',
                        confirm: 'Do you really want to start updating Dwolla Customer information for All Investors?',
                        success: 'Updating Dwolla Customer for All Investors started. Updates will be available later.',
                    },
                },
                columns: {
                    id: 'ID',
                    userId: 'Account',
                    northCapitalAccountId: 'NC AccountId',
                    northCapitalPartyId: 'Nc PartyId',
                    dwollaCustomerId: 'Dwolla Customer Id',
                    isAccountVerificationCompleted: 'Account verification completed',
                    isInvestorVerified: 'KYC/AML checked',
                    isBankAccountLinked: 'Has bank account',
                    isCreditCardLinked: 'Has credit card',
                    transactions: 'Transactions',
                    taxDocuments: 'Tax Documents',
                    dividends: 'Dividends',
                },
                hints: {
                    retry: 'Retry',
                    document: 'Document required',
                    suspended: 'Customer is Suspended. Please contact to Dwolla support.',
                    document_processing: 'Document is being verified.',
                },
            },
            ncInvestorAccountRetryCms: {
                bodyContent:
                    'The verification of this customer failed on Dwolla due to some data discrepancy. Please make sure to review/edit investor details and then click “Retry“ button on this dialogue.',
                actions: {
                    cancel: 'Cancel',
                    retry: 'Retry',
                },
                notifications: {
                    success: 'Retry verification attempt was successful',
                },
            },
            ncInvestorAccountRetryDocumentCms: {
                title: 'Upload a Document',
                bodyContent: 'Drag and Drop the picture or click Select File',
                bodyContentSelected:
                    'Make sure the picture corresponds to the requirements below and click Upload. Otherwise, Select new file',
                rules: [
                    'The file must be either a .jpg, .jpeg, or .png. ',
                    'Files must be no larger than 10MB in size. ',
                    'All 4 Edges of the document should be visible',
                    'A dark/high contrast background should be used',
                    'At least 90% of the image should be the document',
                    'Should be at least 300dpi',
                    'Capture image from directly above the document',
                    'Make sure that the image is properly aligned, not rotated, tilted or skewed',
                    'No flash to reduce glare',
                    'No black and white documents',
                    'No expired IDs',
                ],
                validationErrors: {
                    fileType: 'The uploaded file must be {{allowedFileTypes}}.',
                    fileMaxSize: `The uploaded file cannot be larger than {{allowedFileMaxSize}} {{allowedFileMaxSizeUnit}} in size.`,
                },
                actions: {
                    selectNewFile: 'Select new File',
                    cancel: 'Cancel',
                    upload: 'Upload',
                },
                notifications: {
                    success: 'File uploaded successfully',
                },
            },
        },
    },
};
