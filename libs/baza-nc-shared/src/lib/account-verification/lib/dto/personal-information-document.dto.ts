import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class AccountVerificationPersonalInformationDocumentDto {
    @ApiModelProperty({
        type: 'string',
    })
    @IsString()
    @IsNotEmpty()
    sid: string;

    @ApiModelProperty({
        type: 'string',
    })
    @IsString()
    @IsNotEmpty()
    fileName: string;

    @ApiModelProperty({
        type: 'string',
    })
    @IsString()
    @IsNotEmpty()
    url: string;

    @ApiModelProperty({
        type: 'boolean',
    })
    @IsBoolean()
    @IsNotEmpty()
    canBeDeleted: boolean;

    @ApiModelProperty({
        type: 'boolean',
    })
    @IsBoolean()
    @IsNotEmpty()
    isUploadedToNorthCapital: boolean;
}
