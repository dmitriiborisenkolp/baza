import { IsBoolean, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class AccountVerificationInvestorProfileDto {
    @ApiModelProperty({
        type: 'boolean',
    })
    @IsBoolean()
    @IsNotEmpty()
    isAccreditedInvestor: boolean;

    @ApiModelProperty({
        type: 'boolean',
    })
    @IsBoolean()
    @IsNotEmpty()
    isAssociatedWithFINRA: boolean;

    @ApiModelProperty({
        description: 'What is your current annual household income (including your spouse)? (Income)',
        required: false,
    })
    @IsNumber()
    @IsOptional()
    currentAnnualHouseholdIncome?: number;

    @ApiModelProperty({
        description: 'What is your net worth (excluding primary residence)? (Worth)',
        required: false,
    })
    @IsNumber()
    @IsOptional()
    netWorth?: number;
}
