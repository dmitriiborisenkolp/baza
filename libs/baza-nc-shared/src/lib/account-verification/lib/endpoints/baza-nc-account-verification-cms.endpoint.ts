import { Observable } from 'rxjs';
import { CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { KycLogDto } from '../dto/kyc-log.dto';
import { BazaNcCmsAccountVerificationFormResourcesDto } from '../dto/form-resources-cms.dto';
import { ListStatesRequestCmsDto } from '../dto/list-states-request-cms.dto';
import { ListStatesResponseCmsDto } from '../dto/list-states-response-cms.dto';

export enum BazaNcAccountVerificationCmsEndpointPaths {
    kycLogs = '/baza-nc/account-verification/cms/kycLogs',
    formResources = '/baza-nc/account-verification/cms/formResources',
    listStates = '/baza-nc/account-verification/cms/listStates',
}

export interface BazaNcAccountVerificationCmsEndpoint {
    kycLogs(
        request: NcAccountVerificationKycLogsRequest,
    ): Promise<NcAccountVerificationKycLogsResponse> | Observable<NcAccountVerificationKycLogsResponse>;
    formResources(
        ...args: unknown[]
    ): Promise<BazaNcCmsAccountVerificationFormResourcesDto> | Observable<BazaNcCmsAccountVerificationFormResourcesDto>;
    listStates(request: ListStatesRequestCmsDto): Promise<ListStatesResponseCmsDto> | Observable<ListStatesResponseCmsDto>;
}

export class NcAccountVerificationKycLogsRequest extends CrudListRequestDto<KycLogDto> {}
export class NcAccountVerificationKycLogsResponse extends CrudListResponseDto<KycLogDto> {}
