import { KYCStatus, PartyId } from '../../../transact-api';

export class AccountVerificationKycStatusUpdateEvent {
    constructor(
        public readonly request: {
            partyId: PartyId,
            kycStatus: KYCStatus,
        },
    ) {}
}
