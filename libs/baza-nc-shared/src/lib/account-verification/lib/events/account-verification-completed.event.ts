import { NorthCapitalAccountId, PartyId } from '../../../transact-api';

export class AccountVerificationCompletedEvent {
    constructor(
        public readonly request: {
            userId: number,
            ncAccountId: NorthCapitalAccountId,
            ncPartyId: PartyId,
        },
    ) {}
}
