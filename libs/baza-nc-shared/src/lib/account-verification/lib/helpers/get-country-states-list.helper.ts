import { isUSACountry } from './is-usa-country.helper';
import { NnUsStates, UsState } from '../../../transact-api';

export function getCountryStatesList(country: string): Array<NnUsStates> {
    if (isUSACountry(country)) {
        return Object.values(UsState);
    } else {
        return [];
    }
}
