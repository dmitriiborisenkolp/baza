export function isDocumentValidForUploadToNorthCapital(fileName: string): boolean {
    const ext = fileName.split('.').pop().toLowerCase();

    return [
        'jpg',
        'jpeg',
        'png',
        'pdf',
    ].includes(ext);
}
