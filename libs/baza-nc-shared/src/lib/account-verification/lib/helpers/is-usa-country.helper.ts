export function isUSACountry(country: string): boolean {
    return [
        'US',
        'U.S.',
        'U.S',
        'USA',
        'United States',
    ].includes(country);
}
