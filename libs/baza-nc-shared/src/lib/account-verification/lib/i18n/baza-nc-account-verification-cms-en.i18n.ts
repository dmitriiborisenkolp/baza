export const bazaNcAccountVerificationCmsEnI18n = {
    accountVerification: {
        components: {
            ncAccountVerificationKycLogsCms: {
                title: 'KYC/AML logs',
                columns: {
                    id: 'ID',
                    createdAt: 'Date',
                    userEmail: 'User',
                    ncAccountId: 'NC Account Id',
                    ncPartyId: 'NC Party Id',
                    previousKycStatus: 'Previous KYC',
                    newKycStatus: 'New KYC',
                    kycDetails: 'Details',
                },
            },
        },
    },
};
