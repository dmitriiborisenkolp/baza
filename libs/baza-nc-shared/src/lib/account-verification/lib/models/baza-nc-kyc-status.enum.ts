export enum BazaNcKycStatus {
    Approved = 'approved',
    Disapproved = 'disapproved',
    Kba = 'kba',
    RequestDocuments = 'requestDocuments',
    Pending = 'pending',
}
