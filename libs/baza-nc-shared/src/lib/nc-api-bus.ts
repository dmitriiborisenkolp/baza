import { NorthCapitalPayload } from './nc-kafka-topics';

export enum NcApiEvent {
    BazaNcWebhook = 'BazaNorthCapitalWebhook',
}

export type NcApiEvents =
    { topic: NcApiEvent.BazaNcWebhook, payload: NorthCapitalPayload }
;
