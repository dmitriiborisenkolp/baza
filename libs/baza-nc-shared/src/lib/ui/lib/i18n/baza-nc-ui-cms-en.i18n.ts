export const bazaNcUiCmsEnI18n = {
    ui: {
        formControls: {
            bazaNcCmsOfferingField: {
                form: {
                    errors: {
                        ncDateRangeError: 'End date should be greater than Start Date',
                        ncOfferingStatus: {
                            noData: 'No data available for offering',
                            notFound: 'Offering with given OfferingId not found',
                            used: 'Offering is already used',
                        },
                        ncMaxSharesPerAccount: 'North Capital does not support transactions with more than ${{ limit }} total amount',
                        ncMinAmount: {
                            lessThanUnitPrice: 'Minimal Amount should be more than Unit Price',
                            notIntegerShares: 'Minimal Amount should be a divisible number (Minimal Amount / Unit Price)',
                        },
                        ncMaxAmount: 'Maximum amount should be greater or equal to target amount',
                        ncUnitPrice: 'Unit price should be less than target amount',
                    },
                    fields: {
                        ncOfferingId: 'NC Offering ID',
                        ncTargetAmount: 'Target Amount ($)',
                        ncMinAmount: 'Minimal Amount ($)',
                        ncMaxAmount: 'Max Amount ($)',
                        ncCurrentValue: 'Current Value ($)',
                        ncMaxSharesPerAccount: 'Max Shares per Account',
                        ncOfferingFees: 'Transaction Fees (%)',
                        ncUnitPrice: 'Unit Price ($)',
                        ncStartDate: 'Start Date',
                        ncEndDate: 'End Date',
                        ncSubscriptionOfferingId: 'DocuSign Template',
                        daysToReleaseFailedTrades: 'Days To Release Failed Trades',
                    },
                    help: {
                        ncOfferingFees:
                            'Feels will be appended for (targetAmount, maxAmount, minAmount, unitPrice) for NC side and decedent for transactions, webhook and sync.',
                    },
                    hints: {
                        ncOfferingId:
                            'The field is optional when adding new offering. You may assign existing North Capital offering or leave it empty to generate new offering. Do not change field value if offering is open or has finished purchases',
                        ncTargetAmount: 'This is what will be displayed to the user as “Offering Amount”',
                        ncMinAmount: 'Minimal amount ($) to purchase',
                        ncMaxAmount: 'Max amount is typically the same as target amount',
                        ncCurrentValue: 'Displayed as Share Value in the app, and is used to calculate Return',
                        ncMaxSharesPerAccount: 'Maximum number of shares available per each user',
                        ncOfferingFees:
                            'Following fee will be included as part of transaction and will be charged on each purchase for following offering, Fees could only be updated when there are no existing trade for following offering.',
                        ncUnitPrice: 'Price Per Share',
                        ncStartDate: 'Start Date since North Capital will be able to accepts purchases',
                        ncEndDate: 'North Capital will not be able to accept new purchases after End Date',
                    },
                    placeholders: {
                        ncOfferingId: 'Leave it empty if you need to create new offering',
                    },
                },
            },
            bazaNcCmsInvestorAccountSearchModal: {
                title: 'Investor Accounts',
                columns: {
                    userId: 'ID',
                    userEmail: 'Email',
                    userFullName: 'Full Name',
                    northCapitalAccountId: 'NC Account Id',
                    northCapitalPartyId: 'NC Party Id',
                    dateCreated: 'Date Created At',
                },
            },
        },
    },
};
