export enum BazaNcDwollaErrorCodes {
    BazaNcDwollaNotEnabled = 'BazaNcDwollaNotEnabled',
    BazaNcDwollaCustomerExists = 'BazaNcDwollaCustomerExists',
    BazaNcDwollaCustomerIsNotAvailable = 'BazaNcDwollaCustomerIsNotAvailable',
    BazaNcDwollaCustomerBalanceIsNotAvailable = 'BazaNcDwollaCustomerBalanceIsNotAvailable',
    BazaNcDwollaCustomerInsufficientBalance = 'BazaNcDwollaCustomerInsufficientBalance',
    BazaNcDwollaCustomerNotEnoughDataToUpdate = 'BazaNcDwollaCustomerNotEnoughDataToUpdate',
    BazaNcDwollaCustomerNotCreated = 'BazaNcDwollaCustomerNotCreated',
    BazaNcDwollaCustomerFundingSourceNotFound = 'BazaNcDwollaCustomerFundingSourceNotFound',
}

export const bazaNcDwollaErrorCodesEnI18n = {
    [BazaNcDwollaErrorCodes.BazaNcDwollaNotEnabled]: 'Dwolla support is not enabled',
    [BazaNcDwollaErrorCodes.BazaNcDwollaCustomerExists]: 'Dwolla Customer already exists',
    [BazaNcDwollaErrorCodes.BazaNcDwollaCustomerIsNotAvailable]: 'Dwolla Customer is not available',
    [BazaNcDwollaErrorCodes.BazaNcDwollaCustomerBalanceIsNotAvailable]: 'Balance is not available',
    [BazaNcDwollaErrorCodes.BazaNcDwollaCustomerInsufficientBalance]: 'Insufficient balance',
    [BazaNcDwollaErrorCodes.BazaNcDwollaCustomerNotEnoughDataToUpdate]: 'Not enough data to update Dwolla Customer',
    [BazaNcDwollaErrorCodes.BazaNcDwollaCustomerNotCreated]: 'Dwolla Customer is not created',
    [BazaNcDwollaErrorCodes.BazaNcDwollaCustomerFundingSourceNotFound]: 'Dwolla Customer funding source not found',
};
