export const bazaNcTransactionCmsEnI18n = {
    transaction: {
        title: 'Transactions',
        components: {
            ncTransactionsCms: {
                title: 'Transactions',
                withDetails: 'Show additional details',
                search: 'Search in transactions',
                actions: {
                    exportToCsv: 'Export transactions to CSV',
                },
                columns: {
                    id: 'ID',
                    ncOfferingName: 'Offering',
                    createdAt: 'Date',
                    accountFullName: 'Account',
                    accountId: 'Account Id',
                    ncOfferingId: 'NC Offering Id',
                    ncTradeId: 'NC Trade Id',
                    ncOrderStatus: 'NC Order Status',
                    amountCents: 'Amount',
                    feeCents: 'Fees',
                    totalCents: 'Total',
                    shares: 'Shares',
                    pricePerShareCents: 'Price Per Share',
                    state: 'State',
                },
            },
        },
    },
};
