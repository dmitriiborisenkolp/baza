import { OfferingId } from '../transact-api';

export class BazaNcOfferingUpdatedEvent {
    constructor(
        public request: {
            id: number;
            ncOfferingId: OfferingId
        }
    ) {}
}
