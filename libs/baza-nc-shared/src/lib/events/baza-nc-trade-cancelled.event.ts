import { OfferingId, TradeId } from '../transact-api';

export class BazaNcTradeCancelledEvent {
    constructor(
        public request: {
            id: number;
            ncTradeId: TradeId;
            ncOfferingId: OfferingId;
        }
    ) {}
}
