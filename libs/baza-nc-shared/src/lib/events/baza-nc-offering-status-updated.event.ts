import { BazaNcOfferingStatus } from '../offering';
import { OfferingId } from '../transact-api';

export class BazaNcOfferingStatusUpdatedEvent {
    constructor(
        public request: {
            id: number;
            ncOfferingId: OfferingId;
            origStatus: BazaNcOfferingStatus;
            newStatus: BazaNcOfferingStatus;
        },
    ) {}
}
