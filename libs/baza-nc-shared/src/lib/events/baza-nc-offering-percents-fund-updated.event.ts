import { OfferingId } from '../transact-api';

export class BazaNcOfferingPercentsFundUpdatedEvent {
    constructor(
        public request: {
            id: number;
            ncOfferingId: OfferingId;
            percentsFunded: number;
        }
    ) {}
}
