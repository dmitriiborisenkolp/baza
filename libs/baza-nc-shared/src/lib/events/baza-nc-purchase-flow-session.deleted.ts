import { TradeId } from '../transact-api';
import { OfferingId } from '../transact-api';

export class BazaNcPurchaseFlowSessionDeleted {
    constructor(
        public readonly request: {
            ncOfferingId: OfferingId;
            ncTradeId: TradeId;
        },
    ) {}
}
