import { NorthCapitalPayload } from '../nc-kafka-topics';

export class BazaNcWebhookEvent {
    constructor(
        public message: NorthCapitalPayload,
    ) {}
}
