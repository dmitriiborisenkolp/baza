import { OfferingId, TradeId } from '../transact-api';

export class BazaNcTransactionReturnedEvent {
    constructor(
        public readonly request: {
            id: number;
            ncTradeId: TradeId;
            ncOfferingId: OfferingId;
        },
    ) {}
}
