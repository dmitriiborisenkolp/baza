import { BazaNcPurchaseFlowTransactionType } from '../purchase-flow';
import { TradeId } from '../transact-api';

export class BazaNcPurchaseFlowSessionUpdated {
    constructor(
        public readonly request: {
            ncTradeId: TradeId;
            amountAsCents: number;
            feeAsCents: number;
            totalAsCents: number;
            pricePerSharesAsCents: number;
            numberOfShares: number;
            transactionType?: BazaNcPurchaseFlowTransactionType;
        },
    ) {}
}
