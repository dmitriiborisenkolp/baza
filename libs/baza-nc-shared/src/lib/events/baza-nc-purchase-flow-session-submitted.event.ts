import { NorthCapitalAccountId, OrderStatus, OfferingId, PartyId, TradeId } from '../transact-api';
import { TransactionState } from '../transaction';

export class BazaNcPurchaseFlowSessionSubmittedEvent {
    constructor(
        public readonly request: {
            userId: number,
            ncTradeId: TradeId,
            ncAccountId: NorthCapitalAccountId,
            ncPartyId: PartyId,
            ncOfferingId: OfferingId,
            ncOfferingName: string,
            ncOrderStatus: OrderStatus,
            state: TransactionState,
            transactionFeesCents: number,
            transactionFees: number,
            amountAsCents: number,
            feeAsCents: number,
            totalAsCents: number,
            pricePerSharesAsCents: number;
            numberOfShares: number,
        }
    ) {}
}
