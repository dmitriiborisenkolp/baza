import { OfferingId, OrderStatus, TradeId } from '../transact-api';
import { TransactionState } from '../transaction';

export class BazaNcTradeStatusUpdatedEvent {
    constructor(
        public request: {
            id: number;
            ncTradeId: TradeId;
            ncOfferingId: OfferingId;
            previousOrderStatus: OrderStatus;
            previousTransactionState: TransactionState;
            newOrderStatus: OrderStatus;
            newTransactionState: TransactionState;
        }
    ) {}
}
