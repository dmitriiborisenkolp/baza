import { OfferingId } from '../transact-api';

export class BazaNcOfferingCreatedEvent {
    constructor(
        public request: {
            id: number;
            ncOfferingId: OfferingId;
        }
    ) {}
}
