import { OfferingId } from '../transact-api';

export class BazaNcOfferingDeletedEvent {
    constructor(
        public request: {
            id: number;
            ncOfferingId: OfferingId;
        }
    ) {}
}
