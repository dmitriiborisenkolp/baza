import { OfferingId } from '../transact-api';

export class BazaNcOfferingSyncedEvent {
    constructor(
        public request: {
            id: number;
            ncOfferingId: OfferingId;
        }
    ) {}
}
