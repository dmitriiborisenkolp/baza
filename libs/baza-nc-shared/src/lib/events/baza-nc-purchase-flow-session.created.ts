import { NorthCapitalAccountId, OfferingId, OrderStatus, PartyId, TradeId } from '../transact-api';
import { TransactionState } from '../transaction';
import { BazaNcPurchaseFlowTransactionType } from '../purchase-flow';

export class BazaNcPurchaseFlowSessionCreated {
    constructor(
        public readonly request: {
            userId: number;
            ncTradeId: TradeId;
            ncAccountId: NorthCapitalAccountId;
            ncPartyId: PartyId;
            ncOfferingId: OfferingId;
            ncOfferingName: string;
            ncOrderStatus: OrderStatus;
            state: TransactionState;
            amountAsCents: number;
            feeAsCents: number;
            totalAsCents: number;
            pricePerSharesAsCents: number;
            numberOfShares: number;
            transactionType: BazaNcPurchaseFlowTransactionType;
            transactionFeesCents?: number;
            transactionFees?: number;
        },
    ) {}
}
