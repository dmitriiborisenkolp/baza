export class BazaNcInvestorBankAccountLinkedEvent {
    constructor(
        public request: {
            bazaAccountId: number;
            bazaNcInvestorAccountId: number;
            ncAccountId: string;
        },
    ) {}
}
