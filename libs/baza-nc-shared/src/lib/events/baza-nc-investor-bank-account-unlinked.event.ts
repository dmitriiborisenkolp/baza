export class BazaNcInvestorBankAccountUnlinkedEvent {
    constructor(
        public request: {
            bazaAccountId: number;
            bazaNcInvestorAccountId: number;
            ncAccountId: string;
        },
    ) {}
}
