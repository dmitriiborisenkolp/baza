import { OfferingId, TradeId } from '../transact-api';

export class BazaNcTradeUpdatedEvent {
    constructor(
        public request: {
            id: number;
            ncTradeId: TradeId;
            ncOfferingId: OfferingId;
        }
    ) {}
}
