import { OfferingId, TradeId } from '../transact-api';

export class BazaNcTradeDeletedEvent {
    constructor(
        public request: {
            ncTradeId: TradeId;
            ncOfferingId: OfferingId;
        }
    ) {}
}
