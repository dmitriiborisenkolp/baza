export class BazaNcInvestorCreditCardUnlinkedEvent {
    constructor(
        public request: {
            bazaAccountId: number;
            bazaNcInvestorAccountId: number;
            ncAccountId: string;
        },
    ) {}
}
