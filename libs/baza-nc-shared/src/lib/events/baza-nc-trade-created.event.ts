import { OfferingId, TradeId } from '../transact-api';

export class BazaNcTradeCreatedEvent {
    constructor(
        public request: {
            id: number;
            ncTradeId: TradeId;
            ncOfferingId: OfferingId;
        }
    ) {}
}
