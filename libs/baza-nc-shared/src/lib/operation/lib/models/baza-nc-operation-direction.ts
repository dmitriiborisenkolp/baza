/**
 * Direction of Payment
 * In -> dividends, transfers
 * Out -> purchase, withdraw
 */
export enum BazaNcOperationDirection {
    In = 'in',
    Out = 'out',
}
