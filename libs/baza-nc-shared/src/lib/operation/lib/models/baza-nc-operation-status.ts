import { TransactionState } from '../../../transaction';

/**
 * Statuses for Operations endpoint
 */
export enum BazaNcOperationStatus {
    None = 'None',
    Created = 'Created',
    Pending = 'Pending',
    Completed = 'Completed',
    Cancelled = 'Cancelled',
    Funded = 'Funded',
    Rejected = 'Rejected',
    Returned = 'Returned',
    UnwindCancelled = 'UnwindCancelled',
    Refunded = 'Refunded',
}

/**
 * Mappings for status mappings
 */
export const bazaNcOperationStatusMappings: {
    transactions: Record<TransactionState, BazaNcOperationStatus>;
} = {
    transactions: {
        [TransactionState.None]: BazaNcOperationStatus.None,
        [TransactionState.Created]: BazaNcOperationStatus.Created,
        [TransactionState.PendingPayment]: BazaNcOperationStatus.Pending,
        [TransactionState.PaymentRejected]: BazaNcOperationStatus.Rejected,
        [TransactionState.PaymentCancelled]: BazaNcOperationStatus.Cancelled,
        [TransactionState.PaymentReturned]: BazaNcOperationStatus.Returned,
        [TransactionState.PaymentFunded]: BazaNcOperationStatus.Funded,
        [TransactionState.PaymentConfirmed]: BazaNcOperationStatus.Completed,
        [TransactionState.UnwindPending]: BazaNcOperationStatus.Pending,
        [TransactionState.UnwindCanceled]: BazaNcOperationStatus.UnwindCancelled,
        [TransactionState.UnwindSettled]: BazaNcOperationStatus.Refunded,
    },
};
