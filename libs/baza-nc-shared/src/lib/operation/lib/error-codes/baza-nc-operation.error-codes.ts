export enum BazaNcOperationErrorCodes {
    BazaNcOperationUnknownStatus = 'BazaNcOperationUnknownStatus',
}

export const bazaNcOperationErrorCodesI18n = {
    [BazaNcOperationErrorCodes.BazaNcOperationUnknownStatus]: 'Unknown operation status',
};
