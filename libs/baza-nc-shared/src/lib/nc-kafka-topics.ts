import {
    CcFundMoveResponse,
    CreateAccountResponse,
    CreateLinkResponse,
    CreatePartyResponse,
    ExternalAccountDetails,
    ExternalAccountLinkDetails,
    ExternalFundMoveResponse,
    NorthCapitalAccountId,
    OfferingDetailsFull,
    PartyDetails,
    RequestAiVerificationResponse,
    TradeDetails,
    TradeId,
    UpdateAccountArchiveStatusResponse,
    UpdateAccountResponse,
    UpdateAiRequestResponse,
    UpdateAIVerificationResponse,
    UpdateCcFundMoveStatusResponse,
    UpdateDocuSignStatusResponse,
    UpdateEntityResponse,
    UpdateExternalFundMoveStatusResponse,
    UpdateKycAmlResponse,
    UpdatePartyEntityArchiveStatusResponse,
    UpdateTradeArchiveStatusResponse,
    UpdateTradeTransactionTypeResponse,
    UploadPartyDocumentResponse,
    UploadVerificationDocumentResponse,
} from './transact-api';

export enum NcKafkaTopic {
    BazaNcWebhook = 'BazaNorthCapitalWebhook',
}

export type NcKafkaMessage = { topic: NcKafkaTopic.BazaNcWebhook; payload: NorthCapitalPayload };

export enum NorthCapitalTopic {
    createParty = 'createParty',
    updateParty = 'updateParty',
    uploadPartyDocument = 'uploadPartyDocument',
    updateKycAml = 'updateKycAml',
    createAccount = 'createAccount',
    createLink = 'createLink',
    createTrade = 'createTrade',
    deleteTrade = 'deleteTrade',
    externalFundMove = 'externalFundMove',
    ccFundMove = 'ccFundMove',
    updateCCFundMoveStatus = 'updateCCFundMoveStatus',
    updateExternalAccount = 'updateExternalAccount',
    linkExternalAccount = 'linkExternalAccount',
    updateLinkExternalAccount = 'updateLinkExternalAccount',
    deleteExternalAccount = 'deleteExternalAccount',
    updateExternalFundMoveStatus = 'updateExternalFundMoveStatus',
    updateTradeTransactionType = 'updateTradeTransactionType',
    updateTradeStatus = 'updateTradeStatus',
    updateEntity = 'updateEntity',
    updateAiVerification = 'updateAiVerification',
    updateAiRequest = 'updateAiRequest',
    requestAiVerification = 'requestAiVerification',
    uploadVerificationDocument = 'uploadVerificationDocument',
    updateDocuSignStatus = 'updateDocuSignStatus',
    updateAccount = 'updateAccount',
    updateTradeArchivestatus = 'updateTradeArchivestatus',
    updateAccountArchivestatus = 'updateAccountArchivestatus',
    updatePartyEntityArchivestatus = 'updatePartyEntityArchivestatus',
    updateOffering = 'updateOffering',
}

export type NorthCapitalPayload =
    | { topic: NorthCapitalTopic.createParty; responseBody: CreatePartyResponse }
    | { topic: NorthCapitalTopic.updateParty; responseBody: PartyDetails }
    | { topic: NorthCapitalTopic.uploadPartyDocument; responseBody: UploadPartyDocumentResponse }
    | { topic: NorthCapitalTopic.updateKycAml; responseBody: UpdateKycAmlResponse }
    | { topic: NorthCapitalTopic.createAccount; responseBody: CreateAccountResponse }
    | { topic: NorthCapitalTopic.createLink; responseBody: CreateLinkResponse }
    | { topic: NorthCapitalTopic.createTrade; responseBody: TradeDetails }
    | { topic: NorthCapitalTopic.deleteTrade; responseBody: { tradeId: TradeId } }
    | { topic: NorthCapitalTopic.externalFundMove; responseBody: ExternalFundMoveResponse }
    | { topic: NorthCapitalTopic.ccFundMove; responseBody: CcFundMoveResponse }
    | { topic: NorthCapitalTopic.updateCCFundMoveStatus; responseBody: UpdateCcFundMoveStatusResponse }
    | { topic: NorthCapitalTopic.updateExternalFundMoveStatus; responseBody: UpdateExternalFundMoveStatusResponse }
    | { topic: NorthCapitalTopic.updateTradeTransactionType; responseBody: UpdateTradeTransactionTypeResponse }
    | { topic: NorthCapitalTopic.updateTradeStatus; responseBody: TradeDetails }
    | { topic: NorthCapitalTopic.updateEntity; responseBody: UpdateEntityResponse }
    | { topic: NorthCapitalTopic.updateAiVerification; responseBody: UpdateAIVerificationResponse }
    | { topic: NorthCapitalTopic.updateAiRequest; responseBody: UpdateAiRequestResponse }
    | { topic: NorthCapitalTopic.requestAiVerification; responseBody: RequestAiVerificationResponse }
    | { topic: NorthCapitalTopic.uploadVerificationDocument; responseBody: UploadVerificationDocumentResponse }
    | { topic: NorthCapitalTopic.updateDocuSignStatus; responseBody: UpdateDocuSignStatusResponse }
    | { topic: NorthCapitalTopic.updateTradeArchivestatus; responseBody: UpdateTradeArchiveStatusResponse }
    | { topic: NorthCapitalTopic.updateAccountArchivestatus; responseBody: UpdateAccountArchiveStatusResponse }
    | { topic: NorthCapitalTopic.updatePartyEntityArchivestatus; responseBody: UpdatePartyEntityArchiveStatusResponse }
    | { topic: NorthCapitalTopic.linkExternalAccount; responseBody: ExternalAccountLinkDetails }
    | { topic: NorthCapitalTopic.updateExternalAccount; responseBody: ExternalAccountDetails }
    | { topic: NorthCapitalTopic.updateLinkExternalAccount; responseBody: ExternalAccountLinkDetails }
    | { topic: NorthCapitalTopic.deleteExternalAccount; responseBody: { accountId: NorthCapitalAccountId } }
    | { topic: NorthCapitalTopic.updateAccount; responseBody: UpdateAccountResponse }
    | { topic: NorthCapitalTopic.updateOffering; responseBody: OfferingDetailsFull };
