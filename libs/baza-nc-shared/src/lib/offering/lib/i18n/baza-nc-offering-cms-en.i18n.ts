export const bazaNcOfferingCmsEnI18n = {
    offering: {
        title: 'Offerings',
        components: {
            ncOfferingCms: {
                title: 'Offerings',
                withDetails: 'Show additional details',
                actions: {
                    syncAllOfferings: {
                        label: 'Sync all offerings',
                        confirm: 'Do you really want to sync all offerings?',
                        success: 'Start syncing all offerings',
                    },
                    syncAllPercentsFunded: {
                        label: 'Sync all percents funded',
                        confirm: 'Do you really want to sync percents funded for all offerings?',
                        success: 'Start syncing percents funded for all offerings',
                    },
                    importOffering: 'Import Offering',
                    forgetOffering: {
                        title: 'Forget Offering',
                        message:
                            'Offering will be removed only on API side, but actual NC offering will not be removed. Do you really want to forget offering {{ ncOfferingId }}?',
                        success: 'Offering removed from API',
                        failed: 'Failed to remove offering',
                    },
                    healthCheck: 'Health Check',
                    syncPercentsFunded: {
                        title: 'Sync Percents Funded',
                        success: 'Percents funded updated for offering',
                    },
                },
                columns: {
                    id: 'ID',
                    ncOfferingId: 'Nc Offering ID',
                    status: 'Status',
                    ncOfferingStatus: 'NC Offering Status',
                    percentsFunded: 'Percents Funded',
                    ncUnitPrice: 'Price per share',
                    ncMinAmount: 'Min amount',
                    ncMaxAmount: 'Max amount',
                    ncTargetAmount: 'Target Amount',
                    transactions: 'Transactions',
                },
                forms: {
                    importOffering: {
                        title: 'Import Offering',
                        fields: {
                            ncOfferingId: 'Offering ID',
                        },
                    },
                },
            },
            offeringHealthCheckCms: {
                title: 'Health Check Report for Offering #{{ ncOfferingId }}',
                columns: {
                    source: 'Source',
                    issue: 'Issue',
                    action: 'Action',
                },
                actions: {
                    refresh: 'Refresh',
                },
                messages: {
                    ncBugMessage:
                        'Please note that you may see false “Trade Status” issues below due to a North Capital bug. This tool should be used in conjunction with other reports and is not a single source of truth.',
                    healthy: 'Offering is healthy and all known transactions should be counted in Percents Funded',
                    notHealthy:
                        'This offering may be out of sync! If you observe data inconsistencies elsewhere, you may want to follow the proposed actions below.',
                },
            },
        },
    },
};
