export enum BazaNcOfferingsErrorCodes {
    NcOfferingNotFound = 'NcOfferingNotFound',
    NcOfferingFeesCannotBeUpdatedWithExistingTrade = 'NcOfferingFeesCannotBeUpdatedWithExistingTrade',
    NcOfferingFinancialCannotBeUpdatedWithExistingTrade = 'NcOfferingFinancialCannotBeUpdatedWithExistingTrade',
    NcOfferingCannotBeCreatedWithPassedParams = 'NcOfferingCannotBeCreatedWithPassedParams',
}

export const BazaNorthCapitalOfferingsErrorCodesI18n = {
    [BazaNcOfferingsErrorCodes.NcOfferingFeesCannotBeUpdatedWithExistingTrade]:
        'Transaction Fees for Offering having an existing linked transactions cannot be updated.',
    [BazaNcOfferingsErrorCodes.NcOfferingFinancialCannotBeUpdatedWithExistingTrade]:
        'Offering financial cannot be updated with existing transactions.',
    [BazaNcOfferingsErrorCodes.NcOfferingCannotBeCreatedWithPassedParams]:
        'Offering cannot be created with given Unit Price, Target / Max / Min Amount or Transaction Fee',
};
