import { OfferingId } from '../../../transact-api';

export function bazaNcOfferingName(partial: { ncOfferingId: OfferingId; ncOfferingName: string }): string {
    return partial ? partial.ncOfferingName || `NC Offering Id: #${partial.ncOfferingId}` : 'N/A';
}
