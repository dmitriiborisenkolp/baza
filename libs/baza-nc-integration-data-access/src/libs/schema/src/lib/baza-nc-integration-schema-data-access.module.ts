import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationSchemaDataAccess } from './ng/baza-nc-integration-schema.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaNcIntegrationSchemaDataAccess],
})
export class BazaNcIntegrationSchemaDataAccessModule {}
