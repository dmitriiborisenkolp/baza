import { Injectable } from '@angular/core';
import { BazaNcIntegrationSchemaEndpoint, BazaNcIntegrationSchemaEndpointPaths } from '@scaliolabs/baza-nc-integration-shared';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcIntegrationSchemaDataAccess implements BazaNcIntegrationSchemaEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    /**
     * Returns list of Schema (Public-Schema only)
     * Use received strings as value(s) for filtering data
     */
    list(): Observable<Array<string>> {
        return this.http.get(BazaNcIntegrationSchemaEndpointPaths.list);
    }
}
