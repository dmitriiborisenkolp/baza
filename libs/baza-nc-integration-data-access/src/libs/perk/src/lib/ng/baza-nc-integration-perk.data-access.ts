import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationPerkDto, BazaNcIntegrationPerkEndpoint, BazaNcIntegrationPerkEndpointPaths } from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaNcIntegrationPerkDataAccess implements BazaNcIntegrationPerkEndpoint {
    constructor(
        private readonly http: BazaDataAccessService,
    ) {}

    getById(id: number): Observable<BazaNcIntegrationPerkDto> {
        return this.http.get(replacePathArgs(BazaNcIntegrationPerkEndpointPaths.getById, { id }));
    }

    list(listingId: number): Observable<Array<BazaNcIntegrationPerkDto>> {
        return this.http.get(replacePathArgs(BazaNcIntegrationPerkEndpointPaths.list, { listingId }));
    }
}
