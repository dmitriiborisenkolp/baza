import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationPerkDataAccess } from './ng/baza-nc-integration-perk.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaNcIntegrationPerkDataAccess],
})
export class BazaNcIntegrationPerkDataAccessModule {}
