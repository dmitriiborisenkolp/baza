import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationOperationDataAccess } from './ng/baza-nc-integration-operation.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaNcIntegrationOperationDataAccess],
})
export class BazaNcIntegrationOperationDataAccessModule {}
