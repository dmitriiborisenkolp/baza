import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import {
    BazaNcIntegrationOperationEndpoint,
    BazaNcIntegrationOperationEndpointPaths,
    BazaNcIntegrationOperationFiltersDto,
    BazaNcIntegrationOperationListRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';
import { BazaNcOperationListResponse, BazaNcOperationStatsResponse } from '@scaliolabs/baza-nc-shared';

@Injectable()
export class BazaNcIntegrationOperationDataAccess implements BazaNcIntegrationOperationEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    /**
     * Returns filters & other settings which could be used to fully automate FE implementation
     */
    filters(): Observable<BazaNcIntegrationOperationFiltersDto> {
        return this.http.get(BazaNcIntegrationOperationEndpointPaths.filters);
    }

    /**
     * Operation stats for Investor Account
     */
    stats(): Observable<BazaNcOperationStatsResponse> {
        return this.http.get(BazaNcIntegrationOperationEndpointPaths.filters);
    }

    /**
     * Returns operations search results
     */
    list(request: BazaNcIntegrationOperationListRequest): Observable<BazaNcOperationListResponse> {
        return this.http.get(BazaNcIntegrationOperationEndpointPaths.list, request);
    }
}
