import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationListingsDataAccess } from './ng/baza-nc-integration-listings.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaNcIntegrationListingsDataAccess],
})
export class BazaNcIntegrationListingsDataAccessModule {}
