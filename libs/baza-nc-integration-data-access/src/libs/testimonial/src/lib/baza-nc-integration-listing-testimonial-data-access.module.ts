import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationListingTestimonialDataAccess } from './ng/baza-nc-integration-listing-testimonial.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaNcIntegrationListingTestimonialDataAccess],
})
export class BazaNcIntegrationListingTestimonialDataAccessModule {}
