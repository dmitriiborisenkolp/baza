import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import {
    BazaNcIntegrationListingTestimonialDto,
    BazaNcIntegrationListingTestimonialEndpoint,
    BazaNcIntegrationListingTestimonialEndpointPaths,
    BazaNcIntegrationListingTestimonialListRequest,
    BazaNcIntegrationListingTestimonialListResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaNcIntegrationListingTestimonialDataAccess implements BazaNcIntegrationListingTestimonialEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    list(
        ulid: string,
        request: BazaNcIntegrationListingTestimonialListRequest,
    ): Observable<BazaNcIntegrationListingTestimonialListResponse> {
        return this.http.get(
            replacePathArgs(BazaNcIntegrationListingTestimonialEndpointPaths.list, {
                ulid,
            }),
            request,
        );
    }

    getByUlid(ulid: string): Observable<BazaNcIntegrationListingTestimonialDto> {
        return this.http.get(
            replacePathArgs(BazaNcIntegrationListingTestimonialEndpointPaths.getByUlid, {
                ulid,
            }),
        );
    }
}
