import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationSubscriptionDataAccess } from './ng/baza-nc-integration-subscription.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaNcIntegrationSubscriptionDataAccess],
})
export class BazaNcIntegrationSubscriptionDataAccessModule {}
