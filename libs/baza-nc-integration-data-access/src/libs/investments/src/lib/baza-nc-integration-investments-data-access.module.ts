import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationInvestmentsDataAccess } from './ng/baza-nc-integration-investments.data-access';

@NgModule({
    imports: [
        BazaDataAccessModule,
    ],
    providers: [
        BazaNcIntegrationInvestmentsDataAccess,
    ],
})
export class BazaNcIntegrationInvestmentsDataAccessModule {}
