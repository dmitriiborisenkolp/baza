import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationInvestmentsEndpoint, BazaNcIntegrationInvestmentsEndpointPaths, BazaNcIntegrationsInvestmentsListRequest, BazaNcIntegrationsInvestmentsListResponse } from '@scaliolabs/baza-nc-integration-shared';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcIntegrationInvestmentsDataAccess implements BazaNcIntegrationInvestmentsEndpoint {
    constructor(
        private readonly http: BazaDataAccessService,
    ) {}

    investments(request: BazaNcIntegrationsInvestmentsListRequest): Observable<BazaNcIntegrationsInvestmentsListResponse> {
        return this.http.post(BazaNcIntegrationInvestmentsEndpointPaths.investments, request);
    }
}
