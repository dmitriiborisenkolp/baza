import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationFeedDataAccess } from './ng/baza-nc-integration-feed.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaNcIntegrationFeedDataAccess],
})
export class BazaNcIntegrationFeedDataAccessModule {}
