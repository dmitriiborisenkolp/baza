import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaNcIntegrationSearchDataAccess } from './ng/baza-nc-integration-search.data-access';

@NgModule({
    imports: [BazaDataAccessModule],
    providers: [BazaNcIntegrationSearchDataAccess],
})
export class BazaNcIntegrationSearchDataAccessModule {}
