import { Injectable } from '@angular/core';
import { BazaNcIntegrationFavoriteDataAccess } from '../libs/favorite/src';
import { BazaNcIntegrationFeedDataAccess } from '../libs/feed/src';
import { BazaNcIntegrationInvestmentsDataAccess } from '../libs/investments/src';
import { BazaNcIntegrationListingsDataAccess } from '../libs/listings/src';
import { BazaNcIntegrationPerkDataAccess } from '../libs/perk/src';
import { BazaNcIntegrationSearchDataAccess } from '../libs/search/src';
import { BazaNcIntegrationSubscriptionDataAccess } from '../libs/subscription/src';
import { BazaNcIntegrationListingTestimonialDataAccess } from '../libs/testimonial/src';
import { BazaPortfolioDataAccess } from '../libs/portfolio/src';

@Injectable()
/**
 * This is Bundle service which contains all public Data Access services
 */
export class BazaNcIntegrationDataAccess {
    constructor(
        public readonly favorites: BazaNcIntegrationFavoriteDataAccess,
        public readonly feed: BazaNcIntegrationFeedDataAccess,
        public readonly investments: BazaNcIntegrationInvestmentsDataAccess,
        public readonly listings: BazaNcIntegrationListingsDataAccess,
        public readonly perks: BazaNcIntegrationPerkDataAccess,
        public readonly portfolio: BazaPortfolioDataAccess,
        public readonly search: BazaNcIntegrationSearchDataAccess,
        public readonly subscriptions: BazaNcIntegrationSubscriptionDataAccess,
        public readonly testimonials: BazaNcIntegrationListingTestimonialDataAccess,
    ) {}
}
