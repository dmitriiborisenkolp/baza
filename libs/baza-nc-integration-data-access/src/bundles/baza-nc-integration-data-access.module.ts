import { NgModule } from '@angular/core';
import { BazaNcIntegrationFavoriteDataAccessModule } from '../libs/favorite/src';
import { BazaNcIntegrationFeedDataAccessModule } from '../libs/feed/src';
import { BazaNcIntegrationInvestmentsDataAccessModule } from '../libs/investments/src';
import { BazaNcIntegrationListingsDataAccessModule } from '../libs/listings/src';
import { BazaNcIntegrationPerkDataAccessModule } from '../libs/perk/src';
import { BazaNcIntegrationPortfolioDataAccessModule } from '../libs/portfolio/src';
import { BazaNcIntegrationSearchDataAccessModule } from '../libs/search/src';
import { BazaNcIntegrationSubscriptionDataAccessModule } from '../libs/subscription/src';
import { BazaNcIntegrationListingTestimonialDataAccessModule } from '../libs/testimonial/src';
import { BazaNcIntegrationDataAccess } from './baza-nc-integration-data-access';

const MODULES = [
    BazaNcIntegrationFavoriteDataAccessModule,
    BazaNcIntegrationFeedDataAccessModule,
    BazaNcIntegrationInvestmentsDataAccessModule,
    BazaNcIntegrationListingsDataAccessModule,
    BazaNcIntegrationPerkDataAccessModule,
    BazaNcIntegrationPortfolioDataAccessModule,
    BazaNcIntegrationSearchDataAccessModule,
    BazaNcIntegrationSubscriptionDataAccessModule,
    BazaNcIntegrationListingTestimonialDataAccessModule,
];

@NgModule({
    imports: MODULES,
    providers: [BazaNcIntegrationDataAccess],
})
export class BazaNcIntegrationDataAccessModule {}
