export enum BazaPlaidErrorCodes {
    BazaPlaidApiError = 'BazaPlaidApiError',
    BazaPlaidInvalidAccountsNumber = 'BazaPlaidInvalidAccountsNumber',
    BazaPlaidACHNumbersNotFound = 'BazaPlaidACHNumbersNotFound',
    BazaPlaidUnsupporterAccountType = 'BazaPlaidUnsupporterAccountType',
}

export const bazaPlaidErrorCodesI18n = {
    [BazaPlaidErrorCodes.BazaPlaidInvalidAccountsNumber]:
        'Invalid number of accounts returned by Plaid. Please configure Plaid to return only 1 account',
    [BazaPlaidErrorCodes.BazaPlaidACHNumbersNotFound]: 'No ACH numbers found',
    [BazaPlaidErrorCodes.BazaPlaidUnsupporterAccountType]: 'Only Checking or Savings Bank Accounts',
};
