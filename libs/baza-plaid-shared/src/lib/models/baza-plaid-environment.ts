/**
 * Plaid Environment
 * @see https://plaid.com/docs/sandbox/
 */
export enum BazaPlaidEnvironment {
    development = 'development',
    production = 'production',
    sandbox = 'sandbox',
}
