import { RegistrySchema, RegistryType } from '@scaliolabs/baza-core-shared';

export const bazaNcIntegrationApiRegistry: RegistrySchema = {
    adminEmail: {
        name: 'Admin Email',
        hint: 'This is the email address that will receive admin emails when new users sign up or request to buy shares',
        type: RegistryType.EmailRecipient,
        hiddenFromList: false,
        public: false,
        defaults: {
            name: 'Scalio Administrator',
            email: 'scalio-admin@scal.io',
            cc: [
                {
                    name: 'Anastasiia Zavolozhina',
                    email: 'anastasiia@scal.io',
                },
            ],
        },
    },
};
