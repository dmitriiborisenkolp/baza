import { DynamicModule, Global, Module } from '@nestjs/common';
import { EnvService } from '@scaliolabs/baza-core-api';
import { BazaNcApiBundleModule } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationListingsApiModule } from '../libs/listings/src';
import { BazaNcIntegrationBitlyEnv, BazaNcIntegrationBitlyModule } from '../libs/bitly/src';
import { BazaNcIntegrationFeedCmsApiModule, BazaNcIntegrationFeedApiModule } from '../libs/feed/src';
import { BazaNcIntegrationEliteInvestorApiModule } from '../libs/elite-investor/src';
import { BazaNcIntegrationInvestmentsModule } from '../libs/investments/src';
import { BazaNcIntegrationSubscriptionApiModule } from '../libs/subscription/src';
import { BazaNcIntegrationSchemaApiConfig, BazaNcIntegrationSchemaApiModule } from '../libs/schema/src';
import { BazaNcIntegrationPerkApiModule, BazaNcIntegrationPerkE2eModule } from '../libs/perk/src';
import { BazaNcIntegrationPortfolioApiModule } from '../libs/portfolio/src';
import { BazaNcIntegrationTestimonialApiModule } from '../libs/testimonial/src';
import { BazaNcIntegrationSearchApiModule, BazaNcIntegrationSearchApiConfig } from '../libs/search/src';
import { BazaNcIntegrationOperationApiModule } from '../libs/operation/src';

const MODULES = [
    BazaNcIntegrationListingsApiModule,
    BazaNcIntegrationFeedApiModule,
    BazaNcIntegrationFeedCmsApiModule,
    BazaNcIntegrationEliteInvestorApiModule,
    BazaNcIntegrationInvestmentsModule,
    BazaNcIntegrationSubscriptionApiModule,
    BazaNcIntegrationPerkApiModule,
    BazaNcIntegrationPortfolioApiModule,
    BazaNcIntegrationTestimonialApiModule,
    BazaNcIntegrationOperationApiModule,
];

const E2E_MODULES = [BazaNcIntegrationPerkE2eModule];

interface AsyncOptions {
    search: BazaNcIntegrationSearchApiConfig;
    schema: BazaNcIntegrationSchemaApiConfig;
}

@Module({
    imports: [BazaNcApiBundleModule],
})
export class BazaNcIntegrationApiModule {
    static forRootAsync(config: AsyncOptions): DynamicModule {
        return {
            module: BazaNcIntegrationApiGlobalModule,
            imports: [
                BazaNcIntegrationSearchApiModule.forRootAsync(config.search),
                BazaNcIntegrationSchemaApiModule.forRootAsync(config.schema),
                BazaNcIntegrationBitlyModule.forRootAsync({
                    injects: [EnvService],
                    useFactory: async (env: EnvService) => ({
                        accessToken: env.getAsString<BazaNcIntegrationBitlyEnv>('BAZA_NC_INTEGRATION_BITLY_ACCESS_TOKEN', {
                            defaultValue: undefined,
                        }),
                    }),
                }),
            ],
        };
    }
}

@Global()
@Module({
    imports: [...MODULES, ...E2E_MODULES],
    exports: [...MODULES, ...E2E_MODULES],
})
class BazaNcIntegrationApiGlobalModule {}
