import { Global, Module } from '@nestjs/common';
import { BazaNcIntegrationFeedE2eModule } from '../libs/feed/src';
import { BazaNcIntegrationListingsE2eModule } from '../libs/listings/src';
import { BazaNcIntegrationPerkE2eModule } from '../libs/perk/src';
import { BazaNcIntegrationTestimonialE2eModule } from '../libs/testimonial/src';
import { BazaNcIntegrationSearchE2eModule } from '../libs/search/src';
import { BazaNcIntegrationSchemaE2eModule } from '../libs/schema/src';
import { BazaNcIntegrationOperationE2eModule } from '../libs/operation/src';

const E2E_MODULES = [
    BazaNcIntegrationFeedE2eModule,
    BazaNcIntegrationListingsE2eModule,
    BazaNcIntegrationPerkE2eModule,
    BazaNcIntegrationTestimonialE2eModule,
    BazaNcIntegrationSearchE2eModule,
    BazaNcIntegrationSchemaE2eModule,
    BazaNcIntegrationOperationE2eModule,
];

@Global()
@Module({
    imports: E2E_MODULES,
    exports: E2E_MODULES,
})
export class BazaNcIntegrationE2eModule {}
