import { Module } from '@nestjs/common';
import { BazaNcIntegrationFeedPostMapper } from './mappers/baza-nc-integration-feed-post.mapper';
import { BazaNcIntegrationFeedListingsRepository } from './repositories/baza-nc-integration-feed-listings.repository';
import { BazaNcIntegrationFeedPostApplauseRepository } from './repositories/baza-nc-integration-feed-post-applause.repository';
import { BazaNcIntegrationFeedPostRepository } from './repositories/baza-nc-integration-feed-post.repository';
import { BazaNcIntegrationFeedUserUpdatesRepository } from './repositories/baza-nc-integration-feed-user-updates.repository';
import { BazaNcIntegrationFeedPostApplauseService } from './services/baza-nc-integration-feed-post-applause.service';
import { BazaNcIntegrationFeedPostService } from './services/baza-nc-integration-feed-post.service';
import { BazaNcIntegrationFeedShortLinksService } from './services/baza-nc-integration-feed-short-links.service';
import { BazaNcIntegrationFeedUpdatesIndicatorService } from './services/baza-nc-integration-feed-updates-indicator.service';
import { BazaNcIntegrationFeedVideoPreviewGeneratorService } from './services/baza-nc-integration-feed-video-preview-generator.service';
import { BazaNcIntegrationBitlyModule } from '../../../bitly/src';
import { BazaNcIntegrationListingsApiModule } from '../../../listings/src';
import { BazaNcIntegrationFeedController } from './controllers/baza-nc-integration-feed.controller';
import { BazaAttachmentModule } from '@scaliolabs/baza-core-api';

const repositories = [
    BazaNcIntegrationFeedPostRepository,
    BazaNcIntegrationFeedListingsRepository,
    BazaNcIntegrationFeedUserUpdatesRepository,
];

const services = [
    BazaNcIntegrationFeedPostApplauseRepository,
    BazaNcIntegrationFeedPostMapper,
    BazaNcIntegrationFeedPostService,
    BazaNcIntegrationFeedPostApplauseService,
    BazaNcIntegrationFeedUpdatesIndicatorService,
    BazaNcIntegrationFeedShortLinksService,
    BazaNcIntegrationFeedVideoPreviewGeneratorService,
];

@Module({
    imports: [
        BazaNcIntegrationBitlyModule,
        BazaNcIntegrationListingsApiModule,
        BazaAttachmentModule,
    ],
    controllers: [
        BazaNcIntegrationFeedController,
    ],
    providers: [
        ...repositories,
        ...services,
    ],
    exports: [
        ...repositories,
        ...services,
    ],
})
export class BazaNcIntegrationFeedApiModule
{}
