const REGEX_STYLES = /style="(.*?)"/;

export const cleanUpHTML = (input: string) => {
    let result = (input || '').toString();

    result = result.split('<br>').join('');
    result = result.split('<br/>').join('');
    result = result.split('<br />').join('');
    result = result.split('<p></p>').join('');
    result = result.split('<h3>').join('<h2>');
    result = result.split('</h3>').join('</h2>');
    result = result.split('<h4>').join('<h2>');
    result = result.split('</h4>').join('</h2>');
    result = result.split('<h5>').join('<h2>');
    result = result.split('</h5>').join('</h2>');
    result = result.split('<h6>').join('<h2>');
    result = result.split('</h6>').join('</h2>');

    while(REGEX_STYLES.test(result)) {
        const match = result.match(REGEX_STYLES);

        result = result.split(match[0]).join('');
    }

    return result;
};
