import { forwardRef, Module } from '@nestjs/common';
import { BazaNcIntegrationFeedApiModule } from './baza-nc-integration-feed-api.module';
import { BazaNcIntegrationFeedPublicPostsFixture } from './integration-tests/fixtures/baza-nc-integration-feed-public-posts.fixture';
import { BazaNcIntegrationFeedCmsApiModule } from './baza-nc-integration-feed-cms-api.module';
import { BazaNcIntegrationFeedListingsPostsFixture } from './integration-tests/fixtures/baza-nc-integration-feed-listings-posts.fixture';
import { BazaNcIntegrationFeedLockedPostsFixture } from './integration-tests/fixtures/baza-nc-integration-feed-locked-posts.fixture';
import { BazaNcIntegrationFeedHiddenPostsFixture } from './integration-tests/fixtures/baza-nc-integration-feed-hidden-posts.fixture';
import { BazaNcIntegrationFeedPublicFlagsPostsFixture } from './integration-tests/fixtures/baza-nc-integration-feed-public-flags-posts.fixture';
import { BazaNcIntegrationFeedSortPostsFixture } from './integration-tests/fixtures/baza-nc-integration-feed-sort-posts.fixture';

const FIXTURES = [
    BazaNcIntegrationFeedPublicPostsFixture,
    BazaNcIntegrationFeedListingsPostsFixture,
    BazaNcIntegrationFeedLockedPostsFixture,
    BazaNcIntegrationFeedHiddenPostsFixture,
    BazaNcIntegrationFeedPublicFlagsPostsFixture,
    BazaNcIntegrationFeedSortPostsFixture,
];

@Module({
    imports: [
        forwardRef(() => BazaNcIntegrationFeedApiModule),
        forwardRef(() => BazaNcIntegrationFeedCmsApiModule),
    ],
    providers: [
        ...FIXTURES,
    ],
    exports: [
        ...FIXTURES,
    ],
})
export class BazaNcIntegrationFeedE2eModule {}
