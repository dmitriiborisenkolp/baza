import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn, RelationId } from 'typeorm';
import { BazaNcIntegrationFeedPostType } from '@scaliolabs/baza-nc-integration-shared';
import { bazaSeoDefault, BazaSeoDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';
import { TagEntity } from '@scaliolabs/baza-content-types-api';
import { BazaNcIntegrationFeedDisplay } from '@scaliolabs/baza-nc-integration-shared';

@Entity()
export class BazaNcIntegrationFeedPostEntity implements CrudSortableEntity
{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: 0,
    })
    sortOrder: number;

    @Column({
        default: new Date(),
    })
    datePublishedAt: Date;

    @Column({
        default: true,
    })
    isPublished: boolean;

    @Column({
        default: true,
    })
    allowPublicAccess: boolean;

    @Column({
        type: 'varchar',
        nullable: false,
    })
    type: BazaNcIntegrationFeedPostType;

    @Column({
        nullable: true,
    })
    title: string;

    @Column({
        nullable: true,
    })
    intro: string;

    @Column({
        nullable: true,
    })
    previewImage?: string;

    @Column({
        nullable: true,
    })
    imageUrl?: string;

    @Column({
        nullable: true,
    })
    previewVideo?: string;

    @Column({
        nullable: true,
    })
    videoUrl?: string;

    @Column({
        default: 0,
    })
    applause: number;

    @Column({
        nullable: true,
    })
    contents: string;

    @ManyToMany(() => TagEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinTable()
    tags: Array<TagEntity>;

    @Column({
        type: 'json',
        default: bazaSeoDefault(),
    })
    seo: BazaSeoDto = bazaSeoDefault();

    @ManyToOne(() => BazaNcIntegrationListingsEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    listing?: BazaNcIntegrationListingsEntity;

    @RelationId((entity: BazaNcIntegrationFeedPostEntity) => entity.listing)
    listingId?: number;

    @Column({
        type: 'varchar',
        nullable: true,
    })
    display: BazaNcIntegrationFeedDisplay;
}
