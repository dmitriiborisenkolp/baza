import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BazaNcIntegrationFeedPostEntity } from './baza-nc-integration-feed-post.entity';
import { AccountEntity } from '@scaliolabs/baza-core-api';

@Entity()
export class BazaNcIntegrationFeedPostApplauseEntity
{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => AccountEntity, {
        cascade: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    user: AccountEntity;

    @ManyToOne(() => BazaNcIntegrationFeedPostEntity, {
        cascade: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    post: BazaNcIntegrationFeedPostEntity;
}
