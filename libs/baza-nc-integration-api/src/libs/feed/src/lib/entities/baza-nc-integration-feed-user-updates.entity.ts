import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';

@Entity()
export class BazaNcIntegrationFeedUserUpdatesEntity
{
    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(() => AccountEntity, {
        cascade: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    user: AccountEntity;

    @Column({
        nullable: true,
    })
    lastSeenId: number;
}
