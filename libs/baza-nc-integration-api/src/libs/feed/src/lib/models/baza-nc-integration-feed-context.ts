import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedListFilter } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedContext {
    //  Feed works relative to current user
    user?: AccountEntity;

    // Filter options
    filter?: BazaNcIntegrationFeedListFilter;
}
