import { Injectable } from '@nestjs/common';
import { AccountEntity, AttachmentImageService, AwsService, BazaHtmlService } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedDisplay, BazaNcIntegrationFeedPostDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationFeedPostEntity } from '../entities/baza-nc-integration-feed-post.entity';
import { BazaNcIntegrationFeedListingsRepository } from '../repositories/baza-nc-integration-feed-listings.repository';
import { BazaNcIntegrationFeedPostApplauseRepository } from '../repositories/baza-nc-integration-feed-post-applause.repository';
import { BazaNcIntegrationListingsListItemMapper } from '../../../../listings/src';
import { TagsMapper } from '@scaliolabs/baza-content-types-api';

interface Context {
    user: AccountEntity;
}

@Injectable()
export class BazaNcIntegrationFeedPostMapper
{
    constructor(
        private readonly awsService: AwsService,
        private readonly bazaHtmlHelper: BazaHtmlService,
        private readonly applauseRepository: BazaNcIntegrationFeedPostApplauseRepository,
        private readonly listingsRepository: BazaNcIntegrationFeedListingsRepository,
        private readonly listingsListMapper: BazaNcIntegrationListingsListItemMapper,
        private readonly attachmentImageHelper: AttachmentImageService,
        private readonly tagsMapper: TagsMapper,
    ) {}

    async entityToDTO(input: BazaNcIntegrationFeedPostEntity, context: Context): Promise<BazaNcIntegrationFeedPostDto> {
        const purchasedListingIds = (await this.listingsRepository.listAllPurchasedListings({
            user: context.user,
        })).map((listings) => listings.id);

        return {
            id: input.id,
            datePublishedAt: input.datePublishedAt.toISOString(),
            type: input.type,
            title: input.title,
            intro: input.intro,
            contents: await this.bazaHtmlHelper.process(input.contents),
            applause: input.applause,
            isApplause: await this.applauseRepository.hasApplauseFor({
                user: context.user,
                post: input,
            }),
            previewImageUrl: input.previewImage
                ? (input.imageUrl || await this.awsService.presignedUrl({ s3ObjectId: input.previewImage }))
                : undefined,
            previewVideoUrl: input.previewVideo
                ? (input.videoUrl || await this.awsService.presignedUrl({ s3ObjectId: input.previewVideo }))
                : undefined,
            tags: await this.tagsMapper.entitiesToDTOs(input.tags || []),
            seo: input.seo,
            isLocked: (input.listingId && input.display === BazaNcIntegrationFeedDisplay.LockedForNonInvestors)
                ? ! purchasedListingIds.includes(input.listingId)
                : false,
            listing: input.listing
                ? await this.listingsListMapper.entityToListDTO(input.listing)
                : undefined,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcIntegrationFeedPostEntity>, context: Context): Promise<Array<BazaNcIntegrationFeedPostDto>> {
        const result: Array<BazaNcIntegrationFeedPostDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity, context));
        }

        return result;
    }
}
