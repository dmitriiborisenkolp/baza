import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationFeedPostEntity } from '../entities/baza-nc-integration-feed-post.entity';
import { BazaNcIntegrationFeedCmsPostDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaHtmlService } from '@scaliolabs/baza-core-api';
import { TagsMapper } from '@scaliolabs/baza-content-types-api';
import { BazaNcIntegrationListingsListItemMapper } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationFeedCmsPostMapper
{
    constructor(
        private readonly bazaHtmlHelper: BazaHtmlService,
        private readonly tagsMapper: TagsMapper,
        private readonly listingListMapper: BazaNcIntegrationListingsListItemMapper,
    ) {}

    async entityToDTO(input: BazaNcIntegrationFeedPostEntity): Promise<BazaNcIntegrationFeedCmsPostDto> {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            isPublished: input.isPublished,
            allowPublicAccess: input.allowPublicAccess,
            datePublishedAt: input.datePublishedAt.toISOString(),
            type: input.type,
            previewImage: input.previewImage,
            previewVideo: input.previewVideo,
            title: input.title,
            intro: input.intro,
            contents: input.contents
                ? await this.bazaHtmlHelper.process(input.contents)
                : undefined,
            applause: input.applause,
            tags: await this.tagsMapper.entitiesToDTOs(input.tags || []),
            seo: input.seo,
            listingId: input.listingId,
            listing: input.listing
                ? await this.listingListMapper.entityToListDTO(input.listing)
                : undefined,
            display: input.display,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcIntegrationFeedPostEntity>): Promise<Array<BazaNcIntegrationFeedCmsPostDto>> {
        const result: Array<BazaNcIntegrationFeedCmsPostDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
