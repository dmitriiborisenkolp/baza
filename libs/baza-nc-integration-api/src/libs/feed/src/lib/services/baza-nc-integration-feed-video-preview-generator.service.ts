import { Injectable } from "@nestjs/common";
import * as ffmpeg from 'fluent-ffmpeg'
import { spawn } from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import * as process from 'process';
import * as nodeUtil from 'util';
import { AwsService } from '@scaliolabs/baza-core-api';

function generateRandomString(length = 8): string {
    return Math.random().toString(length).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

@Injectable()
export class BazaNcIntegrationFeedVideoPreviewGeneratorService
{
    constructor(
        private readonly s3: AwsService,
    ) {}

    private fsExists = nodeUtil.promisify(fs.exists);

    async generateVideoPreview(request:{
        url: string;
    }): Promise<{
        previewImageS3ObjectId: string;
    }> {
        let previewImageS3ObjectId: string;

        const imagePath = path.resolve(process.cwd(), `tmp/${generateRandomString()}.jpg`);
        const imagPathParsed = path.parse(imagePath);

        const ffmpegPromise = () => new Promise((resolve, reject) => {
            const childProcess = spawn('ffmpeg', [
                '-i', request.url,
                '-ss', '00:00:00',
                '-vframes', '1',
                '-f', 'image2', imagePath,
            ]);

            childProcess.on('close', () => {
                resolve({
                    previewImageUrl: undefined,
                });
            });

            childProcess.on('error', () => {
                reject();
            });
        });

        const saveThumbnailToS3 = async () => {
            const buffer = fs.createReadStream(imagePath);
            const s3Response = await this.s3.uploadFromBuffer({
                fileBuffer: buffer,
                fileName: imagPathParsed.base,
                contentType: 'image/jpeg',
            });

            previewImageS3ObjectId = s3Response.s3ObjectId;
        };

        const deleteThumbnailAfterS3Upload = () => new Promise((resolve, reject) => {
            fs.unlink(imagePath, (err) => err ? reject() : resolve({
                previewImageUrl: undefined,
            }));
        })

        await ffmpegPromise();

        if (await this.fsExists(imagePath)) {
            await saveThumbnailToS3();
            await deleteThumbnailAfterS3Upload();

            return {
                previewImageS3ObjectId,
            }
        } else {
            return {
                previewImageS3ObjectId: undefined,
            };
        }
    }
}
