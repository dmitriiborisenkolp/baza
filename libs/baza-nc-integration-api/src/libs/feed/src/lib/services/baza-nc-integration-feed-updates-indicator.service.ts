import { Injectable } from "@nestjs/common";
import { BazaNcIntegrationFeedUserUpdatesRepository } from '../repositories/baza-nc-integration-feed-user-updates.repository';
import { BazaNcIntegrationFeedPostRepository } from '../repositories/baza-nc-integration-feed-post.repository';
import { FeedHasAnyUpdatesResponse, FeedMarkAllAsReadRequest } from '@scaliolabs/baza-nc-integration-shared';
import { AccountEntity } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcIntegrationFeedUpdatesIndicatorService
{
    constructor(
        private readonly repository: BazaNcIntegrationFeedUserUpdatesRepository,
        private readonly postRepository: BazaNcIntegrationFeedPostRepository,
    ) {}

    async markAllAsRead(request: FeedMarkAllAsReadRequest, user: AccountEntity): Promise<void> {
        const updatesIndicator = await this.repository.touch({ user });

        updatesIndicator.lastSeenId = request.lastSeenId;

        await this.repository.save(updatesIndicator);
    }

    async hasAnyUpdates(user?: AccountEntity): Promise<FeedHasAnyUpdatesResponse> {
        if (user) {
            const lastId = await this.postRepository.lastId({
                user,
            });

            if (! lastId) {
                return {
                    hasAnyUpdates: false,
                    lastSeenId: undefined,
                };
            }

            const updatesIndicator = await this.repository.touch({ user });

            return {
                lastSeenId: updatesIndicator.lastSeenId,
                hasAnyUpdates: updatesIndicator.lastSeenId !== lastId,
            };
        } else {
            return {
                hasAnyUpdates: false,
                lastSeenId: undefined,
            };
        }
    }
}
