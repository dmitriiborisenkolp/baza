import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationFeedPostRepository } from '../repositories/baza-nc-integration-feed-post.repository';
import { BazaNcIntegrationFeedPostApplauseRepository } from '../repositories/baza-nc-integration-feed-post-applause.repository';
import { FeedApplauseRequest, FeedApplauseResponse, FeedUnapplauseRequest, FeedUnapplauseResponse } from '@scaliolabs/baza-nc-integration-shared';
import { AccountEntity } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcIntegrationFeedPostApplauseService
{
    constructor(
        private readonly postRepository: BazaNcIntegrationFeedPostRepository,
        private readonly applauseRepository: BazaNcIntegrationFeedPostApplauseRepository,
    ) {}

    async applause(request: FeedApplauseRequest, user: AccountEntity): Promise<FeedApplauseResponse> {
        const post = await this.postRepository.getByIdWithContext({
            user,
        }, {
            id: request.id,
        });

        const hasApplause = await this.applauseRepository.hasApplauseFor({
            user,
            post,
        });

        if (hasApplause) {
            return {
                isApplied: false,
                applause: post.applause,
            };
        } else {
            await this.applauseRepository.pushApplauseFor({
                user,
                post,
            });

            post.applause++;

            await this.postRepository.repository.save(post);

            return {
                isApplied: true,
                applause: post.applause,
            };
        }
    }

    async unapplause(request: FeedUnapplauseRequest, user: AccountEntity): Promise<FeedUnapplauseResponse> {
        const post = await this.postRepository.getByIdWithContext({
            user,
        }, {
            id: request.id,
        });

        const hasApplause = await this.applauseRepository.hasApplauseFor({
            user,
            post,
        });

        if (hasApplause) {
            await this.applauseRepository.dropApplauseFor({
                user,
                post,
            });

            post.applause--;

            await this.postRepository.repository.save(post);

            return {
                isApplied: true,
                applause: post.applause,
            };
        } else {
            return {
                applause: post.applause,
                isApplied: false,
            };
        }
    }
}
