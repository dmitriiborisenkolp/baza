import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationFeedPostEntity } from '../entities/baza-nc-integration-feed-post.entity';
import { BAZA_NC_INTEGRATION_FEED_POST_RELATIONS, BazaNcIntegrationFeedPostRepository } from '../repositories/baza-nc-integration-feed-post.repository';
import { BazaNcIntegrationFeedListingsRepository } from '../repositories/baza-nc-integration-feed-listings.repository';
import { BazaNcIntegrationFeedShortLinksService } from './baza-nc-integration-feed-short-links.service';
import { BazaNcIntegrationFeedVideoPreviewGeneratorService } from './baza-nc-integration-feed-video-preview-generator.service';
import { cleanUpHTML } from '../functions/clean-up-html.function';
import { BazaNcIntegrationFeedCmsPostDto, FeedCmsPostsCreateRequest, FeedCmsPostsDeleteRequest, FeedCmsPostsEntityBody, FeedCmsPostsGetByIdRequest, FeedCmsPostsListRequest, FeedCmsPostsUpdateRequest, FeedCmsSetSortOrderRequest } from '@scaliolabs/baza-nc-integration-shared';
import { AwsService, CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsPostMapper } from '../mappers/baza-nc-integration-feed-cms-post.mapper';
import { TagsRepository } from '@scaliolabs/baza-content-types-api';
import { BazaNcIntegrationFeedCmsInvalidPostPayloadException } from '../exceptions/baza-nc-integration-feed-cms-invalid-post-payload.exception';

@Injectable()
export class BazaNcIntegrationFeedCmsService
{
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly repository: BazaNcIntegrationFeedPostRepository,
        private readonly listingsRepository: BazaNcIntegrationFeedListingsRepository,
        private readonly awsService: AwsService,
        private readonly shortLinksService: BazaNcIntegrationFeedShortLinksService,
        private readonly videoThumbnailService: BazaNcIntegrationFeedVideoPreviewGeneratorService,
        private readonly cmsMapper: BazaNcIntegrationFeedCmsPostMapper,
        private readonly tagsRepository: TagsRepository,
    ) {}

    async create(request: FeedCmsPostsCreateRequest): Promise<BazaNcIntegrationFeedPostEntity> {
        const entity = new BazaNcIntegrationFeedPostEntity();

        await this.populate(entity, request);
        await this.repository.save(entity);

        await this.shortLinksService.setupShortLinks(entity);

        return entity;
    }

    async update(request: FeedCmsPostsUpdateRequest): Promise<BazaNcIntegrationFeedPostEntity> {
        const entity = await this.repository.getById({
            id: request.id,
        });

        await this.populate(entity, request);
        await this.repository.save(entity);

        await this.shortLinksService.setupShortLinks(entity);

        return entity;
    }

    async delete(request: FeedCmsPostsDeleteRequest): Promise<void> {
        await this.repository.deleteById(request);
    }

    async getById(request: FeedCmsPostsGetByIdRequest): Promise<BazaNcIntegrationFeedPostEntity> {
        return this.repository.getById({
            id: request.id,
        });
    }

    async list(request: FeedCmsPostsListRequest): Promise<CrudListResponseDto<BazaNcIntegrationFeedCmsPostDto>> {
        return this.crud.find<BazaNcIntegrationFeedPostEntity, BazaNcIntegrationFeedCmsPostDto>({
            request,
            entity: BazaNcIntegrationFeedPostEntity,
            findOptions: {
                order: {
                    sortOrder: 'DESC',
                },
                relations: BAZA_NC_INTEGRATION_FEED_POST_RELATIONS,
            },
            mapper: async (items) => this.cmsMapper.entitiesToDTOs(items),
            withMaxSortOrder: true,
        });
    }

    async setSortOrder(request: FeedCmsSetSortOrderRequest): Promise<void> {
        const entities = await this.repository.findAllForSort()

        const response = await this.crudSort.setSortOrder<BazaNcIntegrationFeedPostEntity>({
            ...request,
            entities,
        });

        await this.repository.save(response.affected);
    }

    private async populate(entity: BazaNcIntegrationFeedPostEntity, entityBody: FeedCmsPostsEntityBody): Promise<void> {
        if (! entityBody.listingId && entityBody.display) {
            throw new BazaNcIntegrationFeedCmsInvalidPostPayloadException();
        }

        if (! entity.datePublishedAt) {
            entity.datePublishedAt = new Date();
        }

        if (! entity.id) {
            entity.sortOrder = await this.crudSort.getLastSortOrder(BazaNcIntegrationFeedPostEntity);
        }

        entity.isPublished = entityBody.isPublished;
        entity.allowPublicAccess = entityBody.allowPublicAccess;
        entity.type = entityBody.type;
        entity.title = entityBody.title;
        entity.intro = entityBody.intro;
        entity.contents = entityBody.contents
            ? cleanUpHTML(entityBody.contents)
            : undefined;
        entity.display = entityBody.listingId
            ? entityBody.display
            : undefined;
        entity.previewImage = entityBody.previewImage;
        entity.previewVideo = entityBody.previewVideo;
        entity.tags = await this.tagsRepository.findByIds(entityBody.tagIds);
        entity.seo = entityBody.seo;

        if (entityBody.listingId && entityBody.listingId > 0) {
            entity.listing = await this.listingsRepository.getListingsById({ id: entityBody.listingId });
        } else {
            entity.listing = null;
        }

        if (entity.previewVideo && ! (entity.previewImage)) {
            entity.previewImage = (await this.videoThumbnailService.generateVideoPreview({
                url: await this.awsService.presignedUrl({
                    s3ObjectId: entity.previewVideo,
                }),
            })).previewImageS3ObjectId;
        }
    }
}
