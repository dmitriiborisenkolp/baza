import { BazaNcIntegrationFeedPostEntity } from '../entities/baza-nc-integration-feed-post.entity';
import { Injectable } from '@nestjs/common';
import { BazaCommonEnvironments, EnvService } from '@scaliolabs/baza-core-api';
import { replacePathArgs, withoutEndingSlash, withoutTrailingSlash } from '@scaliolabs/baza-core-shared';
import { FeedEndpointPaths } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationBitlyService } from '../../../../bitly/src';
import { BazaNcIntegrationFeedPostRepository } from '../repositories/baza-nc-integration-feed-post.repository';

@Injectable()
export class BazaNcIntegrationFeedShortLinksService
{
    constructor(
        private readonly bitly: BazaNcIntegrationBitlyService,
        private readonly env: EnvService<BazaCommonEnvironments.AppEndpointsEnvironment>,
        private readonly repository: BazaNcIntegrationFeedPostRepository,
    ) {}

    async setupShortLinks(entity: BazaNcIntegrationFeedPostEntity): Promise<void> {
        if (entity.previewImage) {
            const uri = replacePathArgs(FeedEndpointPaths.s3ImageUrl, {
                sid: entity.id,
            });

            const url = `${withoutEndingSlash(this.env.getAsString('BAZA_APP_API_URL'))}/${withoutTrailingSlash(uri)}`;

            entity.imageUrl = (await this.bitly.shorten({
                originalUrl: url,
            })).shortenUrl;
        }

        if (entity.previewVideo) {
            const uri = replacePathArgs(FeedEndpointPaths.s3VideoUrl, {
                sid: entity.id,
            });

            const url = `${withoutEndingSlash(this.env.getAsString('BAZA_APP_API_URL'))}/${withoutTrailingSlash(uri)}`;

            entity.videoUrl = (await this.bitly.shorten({
                originalUrl: url,
            })).shortenUrl;
        }

        await this.repository.save(entity);
    }
}
