import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationFeedPostRepository } from '../repositories/baza-nc-integration-feed-post.repository';
import { BazaNcIntegrationFeedPostEntity } from '../entities/baza-nc-integration-feed-post.entity';
import { BazaNcIntegrationFeedContext } from '../models/baza-nc-integration-feed-context';
import { FeedGetByIdRequest, FeedListRequest } from '@scaliolabs/baza-nc-integration-shared';
import { AccountEntity } from '@scaliolabs/baza-core-api';

interface BazaNcIntegrationFeedListServiceResponse {
    posts: Array<BazaNcIntegrationFeedPostEntity>;
    total: number;
    isEndOfFeed: boolean;
}

@Injectable()
export class BazaNcIntegrationFeedPostService
{
    constructor(
        private readonly repository: BazaNcIntegrationFeedPostRepository,
    ) {}

    async list(request: FeedListRequest, user: AccountEntity): Promise<BazaNcIntegrationFeedListServiceResponse> {
        const context: BazaNcIntegrationFeedContext = {
            user,
            filter: request.filter,
        };

        const take = request.take ? request.take + 1 : 11;
        const total = await this.repository.count(context);
        const posts = await this.repository.list(context, {
            take: take,
            lastId: request.lastId,
        });

        const isEndOfFeed = posts.length < take;

        return {
            total,
            isEndOfFeed,
            posts: isEndOfFeed
                ? posts
                : posts.slice(0, posts.length - 1),
        };
    }

    async getById(request: FeedGetByIdRequest, user: AccountEntity): Promise<BazaNcIntegrationFeedPostEntity> {
        const context: BazaNcIntegrationFeedContext = {
            user,
        };

        return this.repository.getByIdWithContext(context, {
            id: request.id,
        });
    }
}
