import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { FeedCmsPostsCreateRequest, BazaNcIntegrationFeedPostType } from '@scaliolabs/baza-nc-integration-shared';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsService } from '../../services/baza-nc-integration-feed-cms.service';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';

const BAZA_NC_INTEGRATION_PUBLIC_POSTS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    createRequest: FeedCmsPostsCreateRequest;
}> = [
    {
        $refId: 1,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Text Post',
            intro: 'Example Text Post Intro',
            tagIds: [],
        },
    },
    {
        $refId: 2,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Article,
            title: 'Example Article Post',
            intro: 'Example Article Post Intro',
            contents: 'Example Article Post Contents',
            previewImage: 'example-aws-s3-id',
            tagIds: [],
        },
    },
    {
        $refId: 3,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Image,
            intro: 'Example Image Post Intro',
            previewImage: 'example-aws-s3-id',
            tagIds: [],
        },
    },
    {
        $refId: 4,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Video,
            title: 'Example Video Post',
            intro: 'Example Video Post Intro',
            previewImage: 'example-aws-s3-id',
            previewVideo: 'example-aws-video-s3-id',
            tagIds: [],
        },
    },
];

@Injectable()
export class BazaNcIntegrationFeedPublicPostsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcIntegrationFeedCmsService,
    ) {
    }

    async up(): Promise<void> {
        for (const fixtureRequest of BAZA_NC_INTEGRATION_PUBLIC_POSTS_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.createRequest);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaNcIntegrationFeedFixtures>([{
    fixture: BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedPublicPostsFixture,
    provider: BazaNcIntegrationFeedPublicPostsFixture,
}]);

