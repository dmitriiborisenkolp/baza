import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';
import { BazaNcIntegrationFeedErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/002-baza-nc-integration-feed-public.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeed = new BazaNcIntegrationFeedNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedPublicPostsFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will correctly displays all public posts', async () => {
        const response = await dataAccessFeed.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.posts.length).toBe(4);

        expect(response.posts[0].title).toBe('Example Video Post');
        expect(response.posts[1].intro).toBe('Example Image Post Intro');
        expect(response.posts[2].title).toBe('Example Article Post');
        expect(response.posts[3].title).toBe('Example Text Post');

        ID = response.posts[0].id;
    });

    it('will correctly returns return post by id', async () => {
        const response = await dataAccessFeed.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.post.title).toBe('Example Video Post');
        expect(response.post.applause).toBe(0);
    });

    it('will not returns unknown post by id', async () => {
        const response: BazaError = (await dataAccessFeed.getById({
            id: ID + 1000,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcIntegrationFeedErrorCodes.FeedPostNotFound);
    });

    it('will successfully applause post', async () => {
        const response = await dataAccessFeed.applause({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isApplied).toBeTruthy();
        expect(response.applause).toBe(1);
    });

    it('will successfully applause same post again, but will not increase applause counter', async () => {
        const response = await dataAccessFeed.applause({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isApplied).toBeFalsy();
        expect(response.applause).toBe(1);
    });

    it('will displays correct applause count', async () => {
        const response = await dataAccessFeed.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.post.title).toBe('Example Video Post');
        expect(response.post.applause).toBe(1);
    });

    it('will successfully applause same post with different user and applauyse counter will be incremented', async () => {
        await http.authE2eUser2();

        const response = await dataAccessFeed.applause({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isApplied).toBeTruthy();
        expect(response.applause).toBe(2);
    });

    it('will displays correct applause count after 2 users applaused', async () => {
        const response = await dataAccessFeed.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.post.title).toBe('Example Video Post');
        expect(response.post.applause).toBe(2);
    });

    it('will successfully unapplause post', async () => {
        const response = await dataAccessFeed.unapplause({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isApplied).toBeTruthy();
        expect(response.applause).toBe(1);
    });

    it('will successfully unapplause same post but will not decrement again applause count', async () => {
        const response = await dataAccessFeed.unapplause({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isApplied).toBeFalsy();
        expect(response.applause).toBe(1);
    });

    it('will successfully unapplause post by different user', async () => {
        await http.authE2eUser2();

        const response = await dataAccessFeed.unapplause({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.isApplied).toBeTruthy();
        expect(response.applause).toBe(0);
    });

    it('will displays correct applause count after 2 users unapplaused', async () => {
        const response = await dataAccessFeed.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.post.title).toBe('Example Video Post');
        expect(response.post.applause).toBe(0);
    });
});
