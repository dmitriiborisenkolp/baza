import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationFeedNodeAccess,
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationFeedListFilter, FeedListResponse } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import { BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE } from '../fixtures/baza-nc-integration-feed-public-flags-posts.fixture';
import { AccountTypeCheckingSaving, BazaNcOfferingStatus, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/010-baza-nc-integration-feed-list-filter-options.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeed = new BazaNcIntegrationFeedNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);

    let LISTING_ID: number;

    const validate = (response: FeedListResponse, $refIds: Array<number>) => {
        const posts = response.posts;

        expect(response.total).toBe($refIds.length);
        expect(posts.length).toBe($refIds.length);

        for (let i = 0; i < $refIds.length; i++) {
            expect(posts[i].title).toBe(`Example ${$refIds[i]}`);
        }
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedPublicFlagsPostsFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display only public posts with Filter Option "public-only"', async () => {
        const response = await dataAccessFeed.list({
            take: 20,
            filter: BazaNcIntegrationFeedListFilter.PublicOnly,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE.filter(
            (next) => !next.createRequest.listingId && next.createRequest.isPublished === true,
        )
            .map((next) => next.$refId)
            .reverse();

        validate(response, expected);
    });

    it('will display only listing posts with Filter Option "listings-only"', async () => {
        const response = await dataAccessFeed.list({
            take: 20,
            filter: BazaNcIntegrationFeedListFilter.ListingOnly,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE.filter(
            (next) => !!next.createRequest.listingId && next.createRequest.isPublished === true,
        )
            .map((next) => next.$refId)
            .reverse();

        validate(response, expected);
    });

    it('will not display any listing posts before purchases with Filter Option "purchased-only"', async () => {
        const response = await dataAccessFeed.list({
            take: 20,
            filter: BazaNcIntegrationFeedListFilter.PurchasedOnly,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.posts.length).toBe(0);
    });

    it('will successfully purchase Listing for User', async () => {
        await http.authE2eAdmin();

        const listingsResponse = await dataAccessListings.list({});

        expect(isBazaErrorResponse(listingsResponse)).toBeFalsy();
        expect(listingsResponse.items.length).toBe(3);

        LISTING_ID = listingsResponse.items[2].id;

        const updateStatusResponse = await dataAccessListingsCms.changeStatus({
            id: listingsResponse.items[2].id,
            newStatus: BazaNcOfferingStatus.Open,
        });

        expect(isBazaErrorResponse(updateStatusResponse)).toBeFalsy();

        await http.authE2eUser();

        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: listingsResponse.items[2].offeringId,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display only listing posts which are purchased with Filter Option "purchased-only"', async () => {
        const response = await dataAccessFeed.list({
            take: 20,
            filter: BazaNcIntegrationFeedListFilter.PurchasedOnly,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE.filter(
            (next) => next.createRequest.listingId === LISTING_ID && next.createRequest.isPublished === true,
        )
            .map((next) => next.$refId)
            .reverse();

        validate(response, expected);
    });
});
