import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import { AccountTypeCheckingSaving, OfferingId, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/005-baza-nc-integration-feed-public-listings.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeed = new BazaNcIntegrationFeedNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);

    let LISTING_OFFERING_ID: OfferingId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedPublicPostsFixture,
                BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedListingsPostsFixture,
                BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedLockedPostsFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will correctly display list of all public and listings post', async () => {
        const response = await dataAccessFeed.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.posts.length).toBe(10);
        expect(response.total).toBe(10);

        // Locked posts
        expect(response.posts[0].title).toBe('Example Locked 3 Post');
        expect(response.posts[0].listing.offeringId).toBeDefined();
        expect(response.posts[0].isLocked).toBeTruthy();

        expect(response.posts[1].title).toBe('Example Locked 2 Post');
        expect(response.posts[1].listing.offeringId).toBeDefined();
        expect(response.posts[1].isLocked).toBeTruthy();

        expect(response.posts[2].title).toBe('Example Locked 1 Post');
        expect(response.posts[2].listing.offeringId).toBeDefined();
        expect(response.posts[2].isLocked).toBeTruthy();

        // Public listings posts
        expect(response.posts[3].title).toBe('Example Listing 3 Post');
        expect(response.posts[3].listing.offeringId).toBeDefined();
        expect(response.posts[3].isLocked).toBeFalsy();

        expect(response.posts[4].title).toBe('Example Listing 2 Post');
        expect(response.posts[4].listing.offeringId).toBeDefined();
        expect(response.posts[4].isLocked).toBeFalsy();

        expect(response.posts[5].title).toBe('Example Listing 1 Post');
        expect(response.posts[5].listing.offeringId).toBeDefined();
        expect(response.posts[5].isLocked).toBeFalsy();

        // Public posts
        expect(response.posts[6].title).toBe('Example Video Post');
        expect(response.posts[7].intro).toBe('Example Image Post Intro');
        expect(response.posts[8].title).toBe('Example Article Post');
        expect(response.posts[9].title).toBe('Example Text Post');

        LISTING_OFFERING_ID = response.posts[0].listing.offeringId;
    });

    it('will successfully purchase listing 1', async () => {
        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display locked post for Listing 1 after purchase', async () => {
        const response = await dataAccessFeed.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.posts.length).toBe(10);
        expect(response.total).toBe(10);

        // Locked posts
        expect(response.posts[0].title).toBe('Example Locked 3 Post');
        expect(response.posts[0].listing.offeringId).toBeDefined();
        expect(response.posts[0].isLocked).toBeFalsy();

        expect(response.posts[1].title).toBe('Example Locked 2 Post');
        expect(response.posts[1].listing.offeringId).toBeDefined();
        expect(response.posts[1].isLocked).toBeTruthy();

        expect(response.posts[2].title).toBe('Example Locked 1 Post');
        expect(response.posts[2].listing.offeringId).toBeDefined();
        expect(response.posts[2].isLocked).toBeTruthy();

        // Public listings posts
        expect(response.posts[3].title).toBe('Example Listing 3 Post');
        expect(response.posts[3].listing.offeringId).toBeDefined();
        expect(response.posts[3].isLocked).toBeFalsy();

        expect(response.posts[4].title).toBe('Example Listing 2 Post');
        expect(response.posts[4].listing.offeringId).toBeDefined();
        expect(response.posts[4].isLocked).toBeFalsy();

        expect(response.posts[5].title).toBe('Example Listing 1 Post');
        expect(response.posts[5].listing.offeringId).toBeDefined();
        expect(response.posts[5].isLocked).toBeFalsy();

        // Public posts
        expect(response.posts[6].title).toBe('Example Video Post');
        expect(response.posts[7].intro).toBe('Example Image Post Intro');
        expect(response.posts[8].title).toBe('Example Article Post');
        expect(response.posts[9].title).toBe('Example Text Post');

        LISTING_OFFERING_ID = response.posts[0].listing.offeringId;
    });
});
