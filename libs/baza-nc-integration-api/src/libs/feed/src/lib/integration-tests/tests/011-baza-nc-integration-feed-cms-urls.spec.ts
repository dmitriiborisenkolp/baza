import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse, replacePathArgs } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsNodeAccess, BazaNcIntegrationFeedNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationFeedPostType, FeedEndpointPaths } from '@scaliolabs/baza-nc-integration-shared';

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/011-baza-nc-integration-feed-cms-urls.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeedCms = new BazaNcIntegrationFeedCmsNodeAccess(http);
    const dataAccessFeed = new BazaNcIntegrationFeedNodeAccess(http);

    let id: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create feed post', async () => {
        const response = await dataAccessFeedCms.create({
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Video,
            title: 'Example Post',
            intro: 'Example Post Intro',
            tagIds: [],
            previewImage: 'example-image-s3-object-id',
            previewVideo: 'example-video-s3-object-id',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        id = response.id;

        expect(response.previewImage).toBe('example-image-s3-object-id');
        expect(response.previewVideo).toBe('example-video-s3-object-id');
    });

    it('will returns correct urls in get by id response', async () => {
        const response = await dataAccessFeed.getById({ id });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.post.previewImageUrl).toBeDefined();
        expect(response.post.previewVideoUrl).toBeDefined();

        expect(
            response.post.previewImageUrl.includes(
                replacePathArgs(FeedEndpointPaths.s3ImageUrl, {
                    sid: id,
                }),
            ),
        ).toBeTruthy();

        expect(
            response.post.previewVideoUrl.includes(
                replacePathArgs(FeedEndpointPaths.s3VideoUrl, {
                    sid: id,
                }),
            ),
        ).toBeTruthy();
    });

    it('will returns correct urls in list response', async () => {
        const response = await dataAccessFeed.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.posts[0].previewImageUrl).toBeDefined();
        expect(response.posts[0].previewVideoUrl).toBeDefined();

        expect(
            response.posts[0].previewImageUrl.includes(
                replacePathArgs(FeedEndpointPaths.s3ImageUrl, {
                    sid: id,
                }),
            ),
        ).toBeTruthy();

        expect(
            response.posts[0].previewVideoUrl.includes(
                replacePathArgs(FeedEndpointPaths.s3VideoUrl, {
                    sid: id,
                }),
            ),
        ).toBeTruthy();
    });

    it('will correctly updates post and keep s3 object ids same', async () => {
        const response = await dataAccessFeedCms.update({
            id,
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Video,
            title: 'Example Post',
            intro: 'Example Post Intro',
            tagIds: [],
            previewImage: 'example-image-s3-object-id',
            previewVideo: 'example-video-s3-object-id',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.previewImage).toBe('example-image-s3-object-id');
        expect(response.previewVideo).toBe('example-video-s3-object-id');
    });

    it('[AFTER_UPDATE] will returns correct urls in get by id response', async () => {
        const response = await dataAccessFeed.getById({ id });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.post.previewImageUrl).toBeDefined();
        expect(response.post.previewVideoUrl).toBeDefined();

        expect(
            response.post.previewImageUrl.includes(
                replacePathArgs(FeedEndpointPaths.s3ImageUrl, {
                    sid: id,
                }),
            ),
        ).toBeTruthy();

        expect(
            response.post.previewVideoUrl.includes(
                replacePathArgs(FeedEndpointPaths.s3VideoUrl, {
                    sid: id,
                }),
            ),
        ).toBeTruthy();
    });

    it('[AFTER_UPDATE] will returns correct urls in list response', async () => {
        const response = await dataAccessFeed.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.posts[0].previewImageUrl).toBeDefined();
        expect(response.posts[0].previewVideoUrl).toBeDefined();

        expect(
            response.posts[0].previewImageUrl.includes(
                replacePathArgs(FeedEndpointPaths.s3ImageUrl, {
                    sid: id,
                }),
            ),
        ).toBeTruthy();

        expect(
            response.posts[0].previewVideoUrl.includes(
                replacePathArgs(FeedEndpointPaths.s3VideoUrl, {
                    sid: id,
                }),
            ),
        ).toBeTruthy();
    });
});
