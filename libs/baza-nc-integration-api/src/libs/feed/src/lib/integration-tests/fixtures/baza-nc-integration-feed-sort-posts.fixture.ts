import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { FeedCmsPostsCreateRequest, BazaNcIntegrationFeedPostType } from '@scaliolabs/baza-nc-integration-shared';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsService } from '../../services/baza-nc-integration-feed-cms.service';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';

const BAZA_NC_INTEGRATIONS_SORT_POSTS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    createRequest: FeedCmsPostsCreateRequest;
}> = [
    {
        $refId: 1,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Post 1',
            intro: 'Post 1 Intro',
            tagIds: [],
        },
    },
    {
        $refId: 2,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Post 2',
            intro: 'Post 2 Intro',
            tagIds: [],
        },
    },
    {
        $refId: 3,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Post 3',
            intro: 'Post 3 Intro',
            tagIds: [],
        },
    },
    {
        $refId: 4,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Post 4',
            intro: 'Post 4 Intro',
            tagIds: [],
        },
    },
    {
        $refId: 5,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Post 5',
            intro: 'Post 5 Intro',
            tagIds: [],
        },
    },
];

@Injectable()
export class BazaNcIntegrationFeedSortPostsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcIntegrationFeedCmsService,
    ) {
    }

    async up(): Promise<void> {
        for (const fixtureRequest of BAZA_NC_INTEGRATIONS_SORT_POSTS_FIXTURE) {
            const entity = await this.service.create(fixtureRequest.createRequest);

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaNcIntegrationFeedFixtures>([{
    fixture: BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedSortPostsFixture,
    provider: BazaNcIntegrationFeedSortPostsFixture,
}]);

