import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationFeedCmsPostDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/009-baza-nc-integration-feed-cms-sort.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeedCms = new BazaNcIntegrationFeedCmsNodeAccess(http);

    let FEED_POSTS: Array<BazaNcIntegrationFeedCmsPostDto>;

    const validate = async (order: Array<number>) => {
        const response = await dataAccessFeedCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const posts = response.items;

        if (!FEED_POSTS) {
            FEED_POSTS = posts;
        }

        expect(posts.length).toBe(order.length);

        for (let i = 0; i < posts.length; i++) {
            expect(posts[i].title).toBe(`Post ${order[i]}`);
        }
    };

    const pick = ($refId: number) => FEED_POSTS.find((p) => p.title === `Post ${$refId}`);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedSortPostsFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns list of all public posts', async () => {
        await validate([5, 4, 3, 2, 1]);
    });

    it('will successfully move up Post 4', async () => {
        const response = await dataAccessFeedCms.setSortOrder({
            id: pick(4).id,
            setSortOrder: pick(4).sortOrder + 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await validate([4, 5, 3, 2, 1]);
    });

    it('will attempt to move up Post 4 but no changes will be applied', async () => {
        const response = await dataAccessFeedCms.setSortOrder({
            id: pick(4).id,
            setSortOrder: pick(4).sortOrder + 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await validate([4, 5, 3, 2, 1]);
    });

    it('will move move up Post 5 and restore correct order', async () => {
        const response = await dataAccessFeedCms.setSortOrder({
            id: pick(5).id,
            setSortOrder: pick(5).sortOrder + 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await validate([5, 4, 3, 2, 1]);
    });

    it('will successfully move down Post 2', async () => {
        const response = await dataAccessFeedCms.setSortOrder({
            id: pick(2).id,
            setSortOrder: pick(2).sortOrder - 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await validate([5, 4, 3, 1, 2]);
    });

    it('will attempt move down Post 2 again but no changes will be applied', async () => {
        const response = await dataAccessFeedCms.setSortOrder({
            id: pick(2).id,
            setSortOrder: pick(2).sortOrder - 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await validate([5, 4, 3, 1, 2]);
    });

    it('will successfully move down Post 1 and restore correct order', async () => {
        const response = await dataAccessFeedCms.setSortOrder({
            id: pick(1).id,
            setSortOrder: pick(1).sortOrder - 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await validate([5, 4, 3, 2, 1]);
    });

    it('will successfully move up Post 3', async () => {
        const response = await dataAccessFeedCms.setSortOrder({
            id: pick(3).id,
            setSortOrder: pick(3).sortOrder + 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await validate([5, 3, 4, 2, 1]);
    });

    it('will successfully move down Post 5', async () => {
        const response = await dataAccessFeedCms.setSortOrder({
            id: pick(5).id,
            setSortOrder: pick(5).sortOrder - 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        await validate([3, 5, 4, 2, 1]);
    });
});
