import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsNodeAccess, BazaNcIntegrationFeedNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';
import { BazaNcIntegrationFeedPostType } from '@scaliolabs/baza-nc-integration-shared';

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/003-baza-nc-integration-feed-read-cursor.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeed = new BazaNcIntegrationFeedNodeAccess(http);
    const dataAccessFeedCms = new BazaNcIntegrationFeedCmsNodeAccess(http);

    let ID_1: number;
    let ID_2: number;
    let ID_3: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedPublicPostsFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will displays that there are new posts available', async () => {
        const response = await dataAccessFeed.hasAnyUpdates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.lastSeenId).toBeNull();
        expect(response.hasAnyUpdates).toBeTruthy();
    });

    it('will not reset read cursor after fetching list', async () => {
        const listResponse = await dataAccessFeed.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        ID_1 = listResponse.posts[1].id;
        ID_2 = listResponse.posts[0].id;

        const response = await dataAccessFeed.hasAnyUpdates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.lastSeenId).toBeNull();
        expect(response.hasAnyUpdates).toBeTruthy();
    });

    it('will successfully mark all posts as read except last one', async () => {
        const response = await dataAccessFeed.markAllAsRead({
            lastSeenId: ID_1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will still displays that there are still new posts to read available', async () => {
        const response = await dataAccessFeed.hasAnyUpdates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.lastSeenId).toBe(ID_1);
        expect(response.hasAnyUpdates).toBeTruthy();
    });

    it('will not affect other users', async () => {
        await http.authE2eUser2();

        const response = await dataAccessFeed.hasAnyUpdates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.lastSeenId).toBeNull();
        expect(response.hasAnyUpdates).toBeTruthy();
    });

    it('will allow to mark all posts as read including last one', async () => {
        const response = await dataAccessFeed.markAllAsRead({
            lastSeenId: ID_2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will displays that there are no new posts to read available', async () => {
        const response = await dataAccessFeed.hasAnyUpdates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.lastSeenId).toBe(ID_2);
        expect(response.hasAnyUpdates).toBeFalsy();
    });

    it('will displays that new posts are available after new post added to CMS', async () => {
        await http.authE2eAdmin();

        const createResponse = await dataAccessFeedCms.create({
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example New Text Post',
            intro: 'Example New Text Post Intro',
            tagIds: [],
        });

        expect(isBazaErrorResponse(createResponse)).toBeFalsy();

        await http.authE2eUser();

        const response = await dataAccessFeed.hasAnyUpdates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.lastSeenId).toBe(ID_2);
        expect(response.hasAnyUpdates).toBeTruthy();

        ID_3 = createResponse.id;
    });

    it('will mark new post as read and after no new posts will be indicated to user', async () => {
        const markAllAsReadResponse = await dataAccessFeed.markAllAsRead({
            lastSeenId: ID_3,
        });

        expect(isBazaErrorResponse(markAllAsReadResponse)).toBeFalsy();

        const response = await dataAccessFeed.hasAnyUpdates();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.lastSeenId).toBe(ID_3);
        expect(response.hasAnyUpdates).toBeFalsy();
    });
});
