import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedPostType, FeedCmsPostsCreateRequest } from '@scaliolabs/baza-nc-integration-shared';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsService } from '../../services/baza-nc-integration-feed-cms.service';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';
import { BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY } from '../../../../../listings/src';
import { BazaNcIntegrationFeedDisplay } from '@scaliolabs/baza-nc-integration-shared';

export const BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    createRequest: FeedCmsPostsCreateRequest;
}> = [
    // Public Posts
    {
        $refId: 1,
        createRequest: {
            isPublished: false,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 1',
            intro: 'Example 1',
            tagIds: [],
        },
    },
    {
        $refId: 2,
        createRequest: {
            isPublished: true,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 2',
            intro: 'Example 2',
            tagIds: [],
        },
    },
    {
        $refId: 3,
        createRequest: {
            isPublished: false,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 3',
            intro: 'Example 3',
            tagIds: [],
        },
    },

    // Public Listing Posts
    {
        $refId: 4,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 4',
            intro: 'Example 4',
            tagIds: [],
        },
    },
    {
        $refId: 5,
        createRequest: {
            isPublished: false,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 5',
            intro: 'Example 5',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.Public,
        },
    },
    {
        $refId: 6,
        createRequest: {
            isPublished: true,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 6',
            intro: 'Example 6',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.Public,
        },
    },
    {
        $refId: 7,
        createRequest: {
            isPublished: false,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 7',
            intro: 'Example 7',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.Public,
        },
    },
    {
        $refId: 8,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 8',
            intro: 'Example 8',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.Public,
        },
    },

    // Listings Posts with Content-Lock for Non-Investor accounts
    {
        $refId: 9,
        createRequest: {
            isPublished: false,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 9',
            intro: 'Example 9',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.LockedForNonInvestors,
        },
    },
    {
        $refId: 10,
        createRequest: {
            isPublished: true,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 10',
            intro: 'Example 10',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.LockedForNonInvestors,
        },
    },
    {
        $refId: 11,
        createRequest: {
            isPublished: false,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 11',
            intro: 'Example 11',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.LockedForNonInvestors,
        },
    },
    {
        $refId: 12,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 12',
            intro: 'Example 12',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.LockedForNonInvestors,
        },
    },

    // Listings Posts which are Hidden for Non-Investor accounts
    {
        $refId: 13,
        createRequest: {
            isPublished: false,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 13',
            intro: 'Example 13',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        },
    },
    {
        $refId: 14,
        createRequest: {
            isPublished: true,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 14',
            intro: 'Example 14',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        },
    },
    {
        $refId: 15,
        createRequest: {
            isPublished: false,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 15',
            intro: 'Example 15',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        },
    },
    {
        $refId: 16,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 16',
            intro: 'Example 16',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        },
    },

    // Listings Posts which are Hidden for Non-Investor accounts (another listing)
    {
        $refId: 17,
        createRequest: {
            isPublished: false,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 17',
            intro: 'Example 17',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[1].$refId,
            display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        },
    },
    {
        $refId: 18,
        createRequest: {
            isPublished: true,
            allowPublicAccess: false,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 18',
            intro: 'Example 18',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[1].$refId,
            display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        },
    },
    {
        $refId: 19,
        createRequest: {
            isPublished: false,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 19',
            intro: 'Example 19',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[1].$refId,
            display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        },
    },
    {
        $refId: 20,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example 20',
            intro: 'Example 20',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[1].$refId,
            display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        },
    },
];

@Injectable()
export class BazaNcIntegrationFeedPublicFlagsPostsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcIntegrationFeedCmsService,
    ) {
    }

    async up(): Promise<void> {
        for (const fixtureRequest of BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE) {
            const entity = await this.service.create({
                ...fixtureRequest.createRequest,
                listingId: fixtureRequest.createRequest.listingId
                    ? BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY.find((f) => f.$refId === fixtureRequest.createRequest.listingId).$id
                    : undefined,
            });

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaNcIntegrationFeedFixtures>([{
    fixture: BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedPublicFlagsPostsFixture,
    provider: BazaNcIntegrationFeedPublicFlagsPostsFixture,
}]);
