export enum BazaNcIntegrationFeedFixtures {
    // Only public posts
    BazaNcIntegrationFeedPublicPostsFixture = 'BazaNcIntegrationFeedPublicPostsFixture',

    // Posts attached to Listings
    BazaNcIntegrationFeedListingsPostsFixture = 'BazaNcIntegrationFeedListingsPostsFixture',

    // Locked Posts attached to Listings
    BazaNcIntegrationFeedLockedPostsFixture = 'BazaNcIntegrationFeedLockedPostsFixture',

    // Hidden Posts attached to Listings
    BazaNcIntegrationFeedHiddenPostsFixture = 'BazaNcIntegrationFeedHiddenPostsFixture',

    // Mixed Public / Listing posts with different publish flags
    BazaNcIntegrationFeedPublicFlagsPostsFixture = 'BazaNcIntegrationFeedPublicFlagsPostsFixture',

    // Public posts to test Sort feature
    BazaNcIntegrationFeedSortPostsFixture = 'BazaNcIntegrationFeedSortPostsFixture',
}
