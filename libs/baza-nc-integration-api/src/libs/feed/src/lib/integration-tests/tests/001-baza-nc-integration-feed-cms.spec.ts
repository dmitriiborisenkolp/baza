import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationFeedErrorCodes, BazaNcIntegrationFeedPostType } from '@scaliolabs/baza-nc-integration-shared';

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/001-baza-nc-integration-feed-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeedCms = new BazaNcIntegrationFeedCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create feed post', async () => {
        const response = await dataAccessFeedCms.create({
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Text Post',
            intro: 'Example Text Post Intro',
            tagIds: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        ID = response.id;
    });

    it('will returns created feed post by id', async () => {
        const response = await dataAccessFeedCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Example Text Post');
    });

    it('will not returns unknown post by id', async () => {
        const response: BazaError = (await dataAccessFeedCms.getById({
            id: ID + 1000,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcIntegrationFeedErrorCodes.FeedPostNotFound);
    });

    it('will display created feed post in list', async () => {
        const response = await dataAccessFeedCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].id).toBe(ID);
    });

    it('will successfully update feed post', async () => {
        const response = await dataAccessFeedCms.update({
            id: ID,
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Text Post *',
            intro: 'Example Text Post Intro *',
            tagIds: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.title).toBe('Example Text Post *');
    });

    it('will displays update in list', async () => {
        const response = await dataAccessFeedCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].id).toBe(ID);
        expect(response.items[0].title).toBe('Example Text Post *');
    });

    it('will successfully delete feed post', async () => {
        const response = await dataAccessFeedCms.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not allow to get deleted feed post by id', async () => {
        const response: BazaError = (await dataAccessFeedCms.getById({
            id: ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcIntegrationFeedErrorCodes.FeedPostNotFound);
    });

    it('will not display deleted post in list', async () => {
        const response = await dataAccessFeedCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });
});
