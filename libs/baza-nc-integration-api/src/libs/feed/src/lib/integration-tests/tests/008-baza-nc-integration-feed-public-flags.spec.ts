import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationFeedNodeAccess,
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';
import { BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY, BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationFeedDisplay, BazaNcIntegrationFeedPostDto } from '@scaliolabs/baza-nc-integration-shared';
import { BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE } from '../fixtures/baza-nc-integration-feed-public-flags-posts.fixture';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { AccountTypeCheckingSaving, BazaNcOfferingStatus, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/008-baza-nc-integration-feed-public-flags.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeed = new BazaNcIntegrationFeedNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);

    let LISTING_ID: number;

    const validate = (posts: Array<BazaNcIntegrationFeedPostDto>, $refIds: Array<number>) => {
        expect(posts.length).toBe($refIds.length);

        for (let i = 0; i < $refIds.length; i++) {
            expect(posts[i].title).toBe(`Example ${$refIds[i]}`);
        }
    };

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedPublicFlagsPostsFixture,
            ],
            specFile: __filename,
        });
    });

    it('[NO-AUTH] will returns all published posts and listings posts which are not hidden for non-auth users', async () => {
        http.noAuth();

        const response = await dataAccessFeed.list({
            take: 20,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE.filter(
            (next) =>
                next.createRequest.isPublished &&
                next.createRequest.allowPublicAccess &&
                next.createRequest.display !== BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        )
            .map((next) => next.$refId)
            .reverse();

        validate(response.posts, expected);
    });

    it('[AUTHENTICATED] will returns all published posts and listings posts which are hidden for non-auth users', async () => {
        await http.authE2eUser();

        const response = await dataAccessFeed.list({
            take: 20,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE.filter(
            (next) => next.createRequest.isPublished && next.createRequest.display !== BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        )
            .map((next) => next.$refId)
            .reverse();

        validate(response.posts, expected);
    });

    it('will successfully purchase Listing for User', async () => {
        await http.authE2eAdmin();

        const listingsResponse = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(listingsResponse)).toBeFalsy();
        expect(listingsResponse.items.length).toBe(3);

        LISTING_ID = listingsResponse.items[2].id;

        const updateStatusResponse = await dataAccessListingsCms.changeStatus({
            id: listingsResponse.items[2].id,
            newStatus: BazaNcOfferingStatus.Open,
        });

        expect(isBazaErrorResponse(updateStatusResponse)).toBeFalsy();

        await http.authE2eUser();

        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: listingsResponse.items[2].offeringId,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('[NO-AUTH] [AFTER_PURCHASE] will returns all published posts and listings posts which are not hidden for non-auth users', async () => {
        http.noAuth();

        const response = await dataAccessFeed.list({
            take: 20,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE.filter(
            (next) =>
                next.createRequest.isPublished &&
                next.createRequest.allowPublicAccess &&
                next.createRequest.display !== BazaNcIntegrationFeedDisplay.HiddenForNonInvestors,
        )
            .map((next) => next.$refId)
            .reverse();

        validate(response.posts, expected);
    });

    it('[AUTHENTICATED] [AFTER_PURCHASE] will returns all published posts and listings posts of Listing 1 only which are hidden for non-auth users', async () => {
        await http.authE2eUser();

        const response = await dataAccessFeed.list({
            take: 20,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const expected = BAZA_NC_INTEGRATION_PUBLIC_FLAGS_POSTS_FIXTURE.filter(
            (next) =>
                next.createRequest.isPublished &&
                (!next.createRequest.listingId || next.createRequest.listingId === BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId),
        )
            .map((next) => next.$refId)
            .reverse();

        validate(response.posts, expected);

        expect(response.posts.filter((p) => !!p.listing && p.listing.id !== LISTING_ID).length).toBe(0);
    });
});
