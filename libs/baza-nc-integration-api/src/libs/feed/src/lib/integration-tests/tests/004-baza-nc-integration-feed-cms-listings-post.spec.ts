import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, BazaError, bazaSeoDefault, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsNodeAccess, BazaNcIntegrationListingsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import {
    BazaNcIntegrationFeedCmsErrorCodes,
    BazaNcIntegrationFeedDisplay,
    BazaNcIntegrationFeedPostType,
} from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/004-baza-nc-integration-feed-cms-listings-post.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeedCms = new BazaNcIntegrationFeedCmsNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);

    let ID: number;
    let LISTING_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListings],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns list of listings', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);

        LISTING_ID = response.items[0].id;
    });

    it('will allow to create feed post which is linked with listing', async () => {
        const response = await dataAccessFeedCms.create({
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Text Post',
            intro: 'Example Text Post Intro',
            listingId: LISTING_ID,
            display: BazaNcIntegrationFeedDisplay.LockedForNonInvestors,
            tagIds: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.listingId).toBe(LISTING_ID);

        ID = response.id;
    });

    it('will displays listings in get by id response', async () => {
        const response = await dataAccessFeedCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.listingId).toBe(LISTING_ID);
        expect(response.display).toBe(BazaNcIntegrationFeedDisplay.LockedForNonInvestors);
    });

    it('will allow to create feed post which is linked with listing and content is locked for non-investor accounts', async () => {
        const response = await dataAccessFeedCms.create({
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Text Post',
            intro: 'Example Text Post Intro',
            listingId: LISTING_ID,
            display: BazaNcIntegrationFeedDisplay.LockedForNonInvestors,
            tagIds: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.listingId).toBe(LISTING_ID);
        expect(response.display).toBe(BazaNcIntegrationFeedDisplay.LockedForNonInvestors);

        ID = response.id;
    });

    it('will returns content-locked post by id', async () => {
        const response = await dataAccessFeedCms.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.listingId).toBe(LISTING_ID);
        expect(response.display).toBe(BazaNcIntegrationFeedDisplay.LockedForNonInvestors);
    });

    it('will not allow to create post which is linked with listings but without display option', async () => {
        const response: BazaError = (await dataAccessFeedCms.create({
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Text Post',
            intro: 'Example Text Post Intro',
            display: BazaNcIntegrationFeedDisplay.LockedForNonInvestors,
            tagIds: [],
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcIntegrationFeedCmsErrorCodes.FeedCMSInvalidPostPayload);
    });
});
