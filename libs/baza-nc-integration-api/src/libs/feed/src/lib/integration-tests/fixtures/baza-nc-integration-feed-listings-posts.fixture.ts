import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedPostType, FeedCmsPostsCreateRequest } from '@scaliolabs/baza-nc-integration-shared';
import { bazaSeoDefault } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedCmsService } from '../../services/baza-nc-integration-feed-cms.service';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';
import { BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY } from '../../../../../listings/src';
import { BazaNcIntegrationFeedDisplay } from '@scaliolabs/baza-nc-integration-shared';

const BAZA_NC_INTEGRATION_LISTINGS_POSTS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    createRequest: FeedCmsPostsCreateRequest;
}> = [
    {
        $refId: 1,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Listing 1 Post',
            intro: 'Example Listing 1 Post Intro',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[0].$refId,
            display: BazaNcIntegrationFeedDisplay.Public,
        },
    },
    {
        $refId: 2,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Listing 2 Post',
            intro: 'Example Listing 2 Post Intro',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[1].$refId,
            display: BazaNcIntegrationFeedDisplay.Public,
        },
    },
    {
        $refId: 3,
        createRequest: {
            isPublished: true,
            allowPublicAccess: true,
            seo: bazaSeoDefault(),
            type: BazaNcIntegrationFeedPostType.Text,
            title: 'Example Listing 3 Post',
            intro: 'Example Listing 3 Post Intro',
            tagIds: [],
            listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY[2].$refId,
            display: BazaNcIntegrationFeedDisplay.Public,
        },
    },
];

@Injectable()
export class BazaNcIntegrationFeedListingsPostsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcIntegrationFeedCmsService,
    ) {
    }

    async up(): Promise<void> {
        for (const fixtureRequest of BAZA_NC_INTEGRATION_LISTINGS_POSTS_FIXTURE) {
            const entity = await this.service.create({
                ...fixtureRequest.createRequest,
                listingId: BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY.find((f) => f.$refId === fixtureRequest.createRequest.listingId).$id,
            });

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaNcIntegrationFeedFixtures>([{
    fixture: BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedListingsPostsFixture,
    provider: BazaNcIntegrationFeedListingsPostsFixture,
}]);

