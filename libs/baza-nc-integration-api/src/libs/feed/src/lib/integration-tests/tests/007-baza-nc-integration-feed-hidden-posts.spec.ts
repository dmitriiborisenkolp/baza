import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFeedNodeAccess, BazaNcIntegrationListingsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationFeedFixtures } from '../baza-nc-integration-feed.fixtures';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import { AccountTypeCheckingSaving, OfferingId, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-api/feed/integration-tests/tests/007-baza-nc-integration-feed-hidden-posts.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFeed = new BazaNcIntegrationFeedNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);

    let LISTING_OFFERING_ID: OfferingId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcIntegrationFeedFixtures.BazaNcIntegrationFeedHiddenPostsFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will correctly display list of all public and listings post', async () => {
        const response = await dataAccessFeed.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.posts.length).toBe(0);
        expect(response.total).toBe(0);

        const listingsResponse = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(listingsResponse)).toBeFalsy();
        expect(listingsResponse.items.length).toBe(3);

        LISTING_OFFERING_ID = listingsResponse.items[2].offeringId;
    });

    it('will successfully purchase listing 1', async () => {
        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display locked post for Listing 1 after purchase', async () => {
        const response = await dataAccessFeed.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.posts.length).toBe(1);
        expect(response.total).toBe(1);

        expect(response.posts[0].title).toBe('Example Hidden 1 Post');
        expect(response.posts[0].listing.offeringId).toBeDefined();
        expect(response.posts[0].listing.offeringId).toBe(LISTING_OFFERING_ID);
        expect(response.posts[0].isLocked).toBeFalsy();

        LISTING_OFFERING_ID = response.posts[0].listing.offeringId;
    });
});
