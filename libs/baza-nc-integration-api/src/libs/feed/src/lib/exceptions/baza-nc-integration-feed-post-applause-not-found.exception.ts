import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedPostNotFoundException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationFeedErrorCodes.FeedPostApplauseNotFound, 'Post applause with given ID was not found', HttpStatus.NOT_FOUND);
    }
}
