import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedCmsErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedCmsInvalidPostPayloadException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationFeedCmsErrorCodes.FeedCMSInvalidPostPayload, 'Invalid payload provided for the post', HttpStatus.NOT_FOUND);
    }
}
