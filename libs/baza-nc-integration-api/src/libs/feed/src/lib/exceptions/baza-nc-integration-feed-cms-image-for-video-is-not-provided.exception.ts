import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedCmsErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedCmsImageForVideoIsNotProvidedException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationFeedCmsErrorCodes.FeedImageForVideoIsNotProvided, 'Image for video is not provided', HttpStatus.NOT_FOUND);
    }
}
