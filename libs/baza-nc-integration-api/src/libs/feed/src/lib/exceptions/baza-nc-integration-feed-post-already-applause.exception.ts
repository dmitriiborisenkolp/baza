import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedPostAlreadyApplauseException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationFeedErrorCodes.FeedPostAlreadyApplause, 'Post with given ID already applause', HttpStatus.CONFLICT);
    }
}
