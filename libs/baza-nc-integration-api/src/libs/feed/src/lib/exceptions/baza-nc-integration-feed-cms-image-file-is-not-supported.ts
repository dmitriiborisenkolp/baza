import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedCmsErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedCmsImageFileIsNotSupported extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationFeedCmsErrorCodes.FeedCMSImageFileIsNotSupported, 'You should provide PNG or JPEG image file for preview', HttpStatus.BAD_REQUEST);
    }
}
