import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedListingsNotFoundException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationFeedErrorCodes.FeedListingsNotFound, 'Listings was not found', HttpStatus.NOT_FOUND);
    }
}
