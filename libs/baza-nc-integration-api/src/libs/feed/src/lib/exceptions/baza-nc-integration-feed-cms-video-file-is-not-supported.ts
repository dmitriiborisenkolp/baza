import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedCmsErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationFeedCmsVideoFileIsNotSupported extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationFeedCmsErrorCodes.FeedCMSVideoFileIsNotSupported, 'You should provide MP4 video file for preview', HttpStatus.BAD_REQUEST);
    }
}
