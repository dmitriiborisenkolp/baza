import { Module } from '@nestjs/common';
import { BazaAttachmentModule } from '@scaliolabs/baza-core-api';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedApiModule } from './baza-nc-integration-feed-api.module';
import { BazaNcIntegrationBitlyModule } from '../../../bitly/src';
import { BazaNcIntegrationFeedCmsController } from './controllers/baza-nc-integration-feed-cms.controller';
import { BazaNcIntegrationFeedCmsPostMapper } from './mappers/baza-nc-integration-feed-cms-post.mapper';
import { BazaNcIntegrationFeedCmsService } from './services/baza-nc-integration-feed-cms.service';

@Module({
    imports: [
        BazaNcIntegrationFeedApiModule,
        BazaAttachmentModule,
        BazaNcIntegrationBitlyModule,
        BazaCrudApiModule,
        BazaAttachmentModule,
    ],
    controllers: [
        BazaNcIntegrationFeedCmsController,
    ],
    providers: [
        BazaNcIntegrationFeedCmsPostMapper,
        BazaNcIntegrationFeedCmsService,
    ],
    exports: [
        BazaNcIntegrationFeedCmsPostMapper,
        BazaNcIntegrationFeedCmsService,
    ],
})
export class BazaNcIntegrationFeedCmsApiModule
{}
