import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationFeedCmsPostDto,
    FeedCmsPostsCreateRequest,
    FeedCmsPostsCreateResponse,
    FeedCmsPostsDeleteRequest,
    FeedCmsPostsDeleteResponse,
    BazaNcIntegrationFeedCmsEndpoint,
    FeedCmsPaths,
    FeedCmsPostsGetByIdRequest,
    FeedCmsPostsGetByIdResponse,
    FeedCmsPostsListRequest,
    FeedCmsPostsListResponse,
    FeedCmsPostsUpdateRequest,
    FeedCmsPostsUpdateResponse,
    BazaNcIntegrationCmsOpenApi,
    FeedCmsSetSortOrderRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFeedCmsService } from '../services/baza-nc-integration-feed-cms.service';
import { BazaNcIntegrationFeedCmsPostMapper } from '../mappers/baza-nc-integration-feed-cms-post.mapper';

@Controller()
@ApiTags(BazaNcIntegrationCmsOpenApi.Feed)
export class BazaNcIntegrationFeedCmsController implements BazaNcIntegrationFeedCmsEndpoint {
    constructor(private readonly service: BazaNcIntegrationFeedCmsService, private readonly mapper: BazaNcIntegrationFeedCmsPostMapper) {}

    @Post(FeedCmsPaths.create)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'create',
        description: 'Create new post for  feed',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationFeedCmsPostDto,
    })
    async create(@Body() request: FeedCmsPostsCreateRequest): Promise<FeedCmsPostsCreateResponse> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(FeedCmsPaths.update)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'update',
        description: 'Update post in  feed',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationFeedCmsPostDto,
    })
    async update(@Body() request: FeedCmsPostsUpdateRequest): Promise<FeedCmsPostsUpdateResponse> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(FeedCmsPaths.delete)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'delete',
        description: 'Delete post from  feed',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationFeedCmsPostDto,
    })
    async delete(@Body() request: FeedCmsPostsDeleteRequest): Promise<FeedCmsPostsDeleteResponse> {
        await this.service.delete(request);
    }

    @Post(FeedCmsPaths.getById)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getById',
        description: 'Returns post CMS DTO',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationFeedCmsPostDto,
    })
    async getById(@Body() request: FeedCmsPostsGetByIdRequest): Promise<FeedCmsPostsGetByIdResponse> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(FeedCmsPaths.list)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of post CMS DTOs',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationFeedCmsPostDto,
    })
    async list(@Body() request: FeedCmsPostsListRequest): Promise<FeedCmsPostsListResponse> {
        return this.service.list(request);
    }

    @Post(FeedCmsPaths.setSortOrder)
    @UseGuards(AuthGuard, AuthRequireAdminRoleGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'setSortOrder',
        description: 'Set sort order of Feed Post',
    })
    @ApiOkResponse()
    async setSortOrder(@Body() request: FeedCmsSetSortOrderRequest): Promise<void> {
        await this.service.setSortOrder(request);
    }
}
