import { Body, Controller, Get, HttpStatus, Param, Post, Res, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaNcIntegrationFeedPostService } from '../services/baza-nc-integration-feed-post.service';
import { BazaNcIntegrationFeedPostMapper } from '../mappers/baza-nc-integration-feed-post.mapper';
import { BazaNcIntegrationFeedPostApplauseService } from '../services/baza-nc-integration-feed-post-applause.service';
import { BazaNcIntegrationFeedUpdatesIndicatorService } from '../services/baza-nc-integration-feed-updates-indicator.service';
import {
    FeedApplauseRequest,
    FeedApplauseResponse,
    BazaNcIntegrationFeedEndpoint,
    FeedEndpointPaths,
    FeedGetByIdRequest,
    FeedGetByIdResponse,
    FeedHasAnyUpdatesResponse,
    FeedListRequest,
    FeedListResponse,
    FeedMarkAllAsReadRequest,
    FeedMarkAllAsReadResponse,
    FeedUnapplauseRequest,
    FeedUnapplauseResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import {
    AccountEntity,
    AuthGuard,
    AuthOptionalGuard,
    AuthOptionalNoFailOnInvalidTokenGuard,
    AuthSessionService,
    AwsService,
    ReqAccount,
} from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationPublicOpenApi } from '@scaliolabs/baza-nc-integration-shared';
import { Response } from 'express';
import { BazaNcIntegrationFeedPostRepository } from '../repositories/baza-nc-integration-feed-post.repository';

@Controller()
@ApiTags(BazaNcIntegrationPublicOpenApi.Feed)
export class BazaNcIntegrationFeedController implements BazaNcIntegrationFeedEndpoint {
    constructor(
        private readonly aws: AwsService,
        private readonly authSession: AuthSessionService,
        private readonly mapper: BazaNcIntegrationFeedPostMapper,
        private readonly service: BazaNcIntegrationFeedPostService,
        private readonly repository: BazaNcIntegrationFeedPostRepository,
        private readonly applauseService: BazaNcIntegrationFeedPostApplauseService,
        private readonly updatesIndicatorService: BazaNcIntegrationFeedUpdatesIndicatorService,
    ) {}

    @Post(FeedEndpointPaths.list)
    @UseGuards(AuthOptionalGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'list',
        description: 'List of posts for feed',
    })
    @ApiOkResponse({
        type: FeedListResponse,
    })
    async list(@Body() request: FeedListRequest, @ReqAccount() account: AccountEntity): Promise<FeedListResponse> {
        const serviceResponse = await this.service.list(request, account);

        return {
            posts: await this.mapper.entitiesToDTOs(serviceResponse.posts, { user: account }),
            isEndOfFeed: serviceResponse.isEndOfFeed,
            total: serviceResponse.total,
        };
    }

    @Post(FeedEndpointPaths.getById)
    @UseGuards(AuthOptionalGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getById',
        description:
            'Returns post by ID. Please mind that /feed/list endpoint returns complete & full DTOs with full HTML content as well as getById endpoint',
    })
    @ApiOkResponse({
        type: FeedGetByIdResponse,
    })
    async getById(@Body() request: FeedGetByIdRequest, @ReqAccount() account: AccountEntity): Promise<FeedGetByIdResponse> {
        const post = await this.service.getById(request, account);

        return {
            post: await this.mapper.entityToDTO(post, { user: account }),
        };
    }

    @Post(FeedEndpointPaths.applause)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'applause',
        description: 'Applause post',
    })
    @ApiOkResponse({
        type: FeedApplauseResponse,
    })
    async applause(@Body() request: FeedApplauseRequest, @ReqAccount() account: AccountEntity): Promise<FeedApplauseResponse> {
        return this.applauseService.applause(request, account);
    }

    @Post(FeedEndpointPaths.unapplause)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'unapplause',
        description: 'Unapplause post',
    })
    @ApiOkResponse({
        type: FeedUnapplauseResponse,
    })
    async unapplause(@Body() request: FeedUnapplauseRequest, @ReqAccount() account: AccountEntity): Promise<FeedUnapplauseResponse> {
        return this.applauseService.unapplause(request, account);
    }

    @Post(FeedEndpointPaths.markAllAsRead)
    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'markAllAsRead',
        description: 'Mark feed as read',
    })
    @ApiOkResponse({
        type: FeedMarkAllAsReadRequest,
    })
    async markAllAsRead(
        @Body() request: FeedMarkAllAsReadRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<FeedMarkAllAsReadResponse> {
        await this.updatesIndicatorService.markAllAsRead(request, account);

        return {
            success: true,
        };
    }

    @Post(FeedEndpointPaths.hasAnyUpdates)
    @UseGuards(AuthOptionalNoFailOnInvalidTokenGuard)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'hasAnyUpdates',
        description: ' Feed updates indicator',
    })
    @ApiOkResponse({
        type: FeedMarkAllAsReadResponse,
    })
    async hasAnyUpdates(@ReqAccount() account: AccountEntity): Promise<FeedHasAnyUpdatesResponse> {
        if (this.authSession.hasSession) {
            return this.updatesIndicatorService.hasAnyUpdates(account);
        } else {
            return {
                hasAnyUpdates: false,
            };
        }
    }

    @Get(FeedEndpointPaths.s3ImageUrl)
    @ApiOperation({
        summary: 's3ImageUrl',
        description: 'Redirects user to image of post',
    })
    @ApiOkResponse()
    async s3ImageUrl(@Param('sid') sid: string, @Res() res: Response): Promise<void> {
        const notFound = async () => {
            await res.status(HttpStatus.NOT_FOUND).send();
        };

        const id = parseInt(sid, 10);

        if (!id) {
            return notFound();
        }

        const FeedPost = await this.repository.findById({
            id,
        });

        if (!FeedPost || !FeedPost.previewImage) {
            return notFound();
        }

        const presignedUrl = await this.aws.presignedUrl({
            s3ObjectId: FeedPost.previewImage,
        });

        return res.redirect(presignedUrl);
    }

    @Get(FeedEndpointPaths.s3VideoUrl)
    @ApiOperation({
        summary: 's3VideoUrl',
        description: 'Redirects user to video of post',
    })
    @ApiOkResponse()
    async s3VideoUrl(@Param('sid') sid: string, @Res() res: Response): Promise<void> {
        const notFound = async () => {
            await res.status(HttpStatus.NOT_FOUND).send();
        };

        const id = parseInt(sid, 10);

        if (!id) {
            return notFound();
        }

        const FeedPost = await this.repository.findById({
            id,
        });

        if (!FeedPost || !FeedPost.previewVideo) {
            return notFound();
        }

        const presignedUrl = await this.aws.presignedUrl({
            s3ObjectId: FeedPost.previewVideo,
        });

        return res.redirect(presignedUrl);
    }
}
