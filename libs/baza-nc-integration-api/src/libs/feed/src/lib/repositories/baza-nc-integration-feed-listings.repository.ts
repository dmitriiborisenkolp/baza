import { Injectable } from '@nestjs/common';
import { Connection, In, Repository } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { TransactionState } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingEntity, BazaNcTransactionEntity } from '@scaliolabs/baza-nc-api';
import { BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS, BazaNcIntegrationListingsEntity, BazaNcIntegrationListingsRepository } from '../../../../listings/src';
import { BazaNcIntegrationFeedListingsNotFoundException } from '../exceptions/baza-nc-integration-feed-listings-not-found.exception';

@Injectable()
export class BazaNcIntegrationFeedListingsRepository
{
    constructor(
        private readonly connection: Connection,
        private readonly listingsRepository: BazaNcIntegrationListingsRepository,
    ) {}

    private get repository(): Repository<BazaNcIntegrationListingsEntity> {
        return this.connection.getRepository(BazaNcIntegrationListingsEntity);
    }

    private get transactionRepository(): Repository<BazaNcTransactionEntity> {
        return this.connection.getRepository(BazaNcTransactionEntity);
    }

    private get offeringRepository(): Repository<BazaNcOfferingEntity> {
        return this.connection.getRepository(BazaNcOfferingEntity);
    }

    async getListingsById(request: {
        id: number;
    }): Promise<BazaNcIntegrationListingsEntity> {
        const entity = await this.repository.findOne({
            where: [{
                id: request.id,
            }],
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        });

        if (! entity) {
            throw new BazaNcIntegrationFeedListingsNotFoundException();
        }

        return entity;
    }

    async listAll(): Promise<Array<BazaNcIntegrationListingsEntity>> {
        return this.repository.find({
            order: {
                id: 'DESC',
            },
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        });
    }

    async listAllPurchasedListings(request: {
        user: AccountEntity;
    }): Promise<Array<BazaNcIntegrationListingsEntity>> {
        const purchases = await this.transactionRepository.find({
            select: [
                'id', 'ncOfferingId',
            ],
            where: [{
                account: request.user,
                state: In([
                    TransactionState.None,
                    TransactionState.PendingPayment,
                    TransactionState.PaymentFunded,
                    TransactionState.PaymentConfirmed,
                ]),
            }],
            relations: [],
        });

        const purchasesOfferingIds = purchases.map((p) => p.ncOfferingId, 10);

        if (purchasesOfferingIds.length > 0) {
            const offerings = await this.offeringRepository.find({
                where: [{
                    ncOfferingId: In(purchasesOfferingIds),
                }],
            });

            if (offerings.length) {
                return this.listingsRepository.findManyByOfferingIds(
                    offerings.map((offering) => offering.ncOfferingId),
                );
            } else {
                return [];
            }
        } else {
            return [];
        }
    }
}
