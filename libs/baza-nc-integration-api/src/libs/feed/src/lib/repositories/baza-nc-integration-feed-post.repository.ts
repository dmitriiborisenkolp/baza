import { Injectable } from '@nestjs/common';
import { Connection, In, IsNull, LessThan, Not, Repository } from 'typeorm';
import { BazaNcIntegrationFeedPostEntity } from '../entities/baza-nc-integration-feed-post.entity';
import { BazaNcIntegrationFeedContext } from '../models/baza-nc-integration-feed-context';
import { BazaNcIntegrationFeedPostNotFoundException } from '../exceptions/baza-nc-integration-feed-post-not-found.exception';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { BazaNcIntegrationFeedListingsRepository } from './baza-nc-integration-feed-listings.repository';
import { BazaNcIntegrationFeedDisplay, BazaNcIntegrationFeedListFilter } from '@scaliolabs/baza-nc-integration-shared';

export const BAZA_NC_INTEGRATION_FEED_POST_RELATIONS = ['listing', 'listing.offering', 'tags'];

@Injectable()
export class BazaNcIntegrationFeedPostRepository
{
    constructor(
        private readonly connection: Connection,
        private readonly listingsRepository: BazaNcIntegrationFeedListingsRepository,
    ) {}

    get repository(): Repository<BazaNcIntegrationFeedPostEntity> {
        return this.connection.getRepository(BazaNcIntegrationFeedPostEntity);
    }

    async save(entities: BazaNcIntegrationFeedPostEntity | Array<BazaNcIntegrationFeedPostEntity>): Promise<void> {
        await this.repository.save(
            Array.isArray(entities)
                ? entities
                : [entities],
        );
    }

    async count(context: BazaNcIntegrationFeedContext): Promise<number> {
        const where = await this.whereConditions(context);

        if (where) {
            return this.repository.count({
                where,
            });
        } else {
            return 0;
        }
    }

    async list(context: BazaNcIntegrationFeedContext, request: {
        take: number;
        lastId?: number;
    }): Promise<Array<BazaNcIntegrationFeedPostEntity>> {
        const where = await this.whereConditions(context);

        if (where) {
            if (request.lastId) {
                for (const whereCond of where) {
                    whereCond.id = LessThan(request.lastId);
                }
            }

            return this.repository.find({
                where,
                order: {
                    sortOrder: 'DESC',
                },
                take: request.take,
                relations: BAZA_NC_INTEGRATION_FEED_POST_RELATIONS,
            });
        } else {
            return [];
        }
    }

    async getById(request: {
        id: number;
    }): Promise<BazaNcIntegrationFeedPostEntity> {
        const entity = await this.repository.findOne({
            where: [{
                id: request.id,
            }],
            relations: BAZA_NC_INTEGRATION_FEED_POST_RELATIONS,
        });

        if (! entity) {
            throw new BazaNcIntegrationFeedPostNotFoundException();
        }

        return entity;
    }

    async findById(request: {
        id: number;
    }): Promise<BazaNcIntegrationFeedPostEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id: request.id,
            }],
            relations: BAZA_NC_INTEGRATION_FEED_POST_RELATIONS,
        });
    }

    async findAllForSort(): Promise<Array<BazaNcIntegrationFeedPostEntity>> {
        return this.repository.find({
            select: ['id', 'sortOrder'],
        });
    }

    async getByIdWithContext(context: BazaNcIntegrationFeedContext, request: {
        id: number;
    }): Promise<BazaNcIntegrationFeedPostEntity> {
        const entity = await this.repository.findOne({
            where: [
                { id: request.id },
            ],
            relations: BAZA_NC_INTEGRATION_FEED_POST_RELATIONS,
        });

        if (! entity) {
            throw new BazaNcIntegrationFeedPostNotFoundException();
        }

        return entity;
    }

    async deleteById(request: {
        id: number;
    }): Promise<void> {
        const entity = await this.getById({
            id: request.id,
        });

        await this.repository.remove(entity);
    }

    async lastId(context: BazaNcIntegrationFeedContext): Promise<number> {
        const lastEntityInList = await this.list(context, {
            take: 1,
        });

        return lastEntityInList.length > 0
            ? lastEntityInList[0].id
            : undefined;
    }

    private async whereConditions(context: BazaNcIntegrationFeedContext): Promise<Array<FindConditions<BazaNcIntegrationFeedPostEntity>> | null> {
        const purchasedListings = context.user
            ? await this.listingsRepository.listAllPurchasedListings({
                user: context.user,
            })
            : [];

        const where: Array<FindConditions<BazaNcIntegrationFeedPostEntity>> = [

        ];

        switch (context.filter) {
            default: {
                where.push(...[
                    { isPublished: true, listing: IsNull() },
                    { isPublished: true, display: BazaNcIntegrationFeedDisplay.Public },
                    { isPublished: true, display: BazaNcIntegrationFeedDisplay.LockedForNonInvestors },
                ]);

                if (purchasedListings.length > 0) {
                    where.push(
                        { isPublished: true, display: BazaNcIntegrationFeedDisplay.HiddenForNonInvestors, listing: In(purchasedListings.map((listing) => listing.id)) as any },
                    );
                }

                break;
            }

            case BazaNcIntegrationFeedListFilter.PublicOnly: {
                where.push({ isPublished: true, listing: IsNull() });

                break;
            }

            case BazaNcIntegrationFeedListFilter.ListingOnly: {
                where.push({ isPublished: true, listing: Not(IsNull()) });

                break;
            }

            case BazaNcIntegrationFeedListFilter.PurchasedOnly: {
                if (purchasedListings.length > 0) {
                    where.push(
                        { isPublished: true, listing: In(purchasedListings.map((listing) => listing.id)) as any },
                    );
                } else {
                    return null;
                }

                break;
            }
        }

        if (! where.length) {
            where.push({});
        }

        where.forEach((next) => next.isPublished = true);

        if (! context.user) {
            where.forEach((next) => next.allowPublicAccess = true);
        }

        return where;
    }
}
