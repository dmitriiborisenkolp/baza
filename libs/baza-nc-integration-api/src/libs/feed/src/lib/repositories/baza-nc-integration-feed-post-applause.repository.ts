import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BazaNcIntegrationFeedPostApplauseEntity } from '../entities/baza-nc-integration-feed-post-applause.entity';
import { BazaNcIntegrationFeedPostEntity } from '../entities/baza-nc-integration-feed-post.entity';
import { BazaNcIntegrationFeedPostNotFoundException } from '../exceptions/baza-nc-integration-feed-post-not-found.exception';
import { BazaNcIntegrationFeedPostAlreadyApplauseException } from '../exceptions/baza-nc-integration-feed-post-already-applause.exception';
import { AccountEntity } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcIntegrationFeedPostApplauseRepository
{
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaNcIntegrationFeedPostApplauseEntity> {
        return this.connection.getRepository(BazaNcIntegrationFeedPostApplauseEntity);
    }

    async hasApplauseFor(request: {
        user: AccountEntity;
        post: BazaNcIntegrationFeedPostEntity;
    }): Promise<boolean> {
        return !! (await this.repository.findOne({
            where: [{
                user: request.user,
                post: request.post,
            }],
        }));
    }

    async getApplauseFor(request: {
        user: AccountEntity;
        post: BazaNcIntegrationFeedPostEntity;
    }): Promise<BazaNcIntegrationFeedPostApplauseEntity> {
        const entity = this.repository.findOne({
            where: [{
                user: request.user,
                post: request.post,
            }],
        });

        if (! entity) {
            throw new BazaNcIntegrationFeedPostNotFoundException();
        }

        return entity;
    }

    async pushApplauseFor(request: {
        user: AccountEntity;
        post: BazaNcIntegrationFeedPostEntity;
    }): Promise<BazaNcIntegrationFeedPostApplauseEntity> {
        if (await this.hasApplauseFor(request)) {
            throw new BazaNcIntegrationFeedPostAlreadyApplauseException();
        }

        const entity = new BazaNcIntegrationFeedPostApplauseEntity();

        entity.post = request.post;
        entity.user = request.user;

        await this.repository.save(entity);

        return entity;
    }

    async dropApplauseFor(request: {
        user: AccountEntity;
        post: BazaNcIntegrationFeedPostEntity;
    }): Promise<void> {
        const applause = await this.getApplauseFor(request);

        await this.repository.remove(applause);
    }
}

