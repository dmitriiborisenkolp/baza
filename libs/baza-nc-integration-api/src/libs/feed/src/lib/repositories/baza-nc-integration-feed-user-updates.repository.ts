import { Injectable } from "@nestjs/common";
import { Connection, Repository } from 'typeorm';
import { BazaNcIntegrationFeedUserUpdatesEntity } from '../entities/baza-nc-integration-feed-user-updates.entity';
import { AccountEntity } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcIntegrationFeedUserUpdatesRepository
{
    constructor(
        private readonly connection: Connection,
    ) {}

    private get repository(): Repository<BazaNcIntegrationFeedUserUpdatesEntity> {
        return this.connection.getRepository(BazaNcIntegrationFeedUserUpdatesEntity);
    }

    async touch(request: {
        user: AccountEntity;
    }): Promise<BazaNcIntegrationFeedUserUpdatesEntity> {
        const found = await this.repository.findOne({
            where: [{
                user: request.user,
            }],
            relations: ['user'],
        });

        if (found) {
            return found;
        } else {
            const newEntity = new BazaNcIntegrationFeedUserUpdatesEntity();

            newEntity.user = request.user;

            await this.save(newEntity);

            return newEntity;
        }
    }

    async save(entity: BazaNcIntegrationFeedUserUpdatesEntity): Promise<void> {
        await this.repository.save(entity);
    }
}
