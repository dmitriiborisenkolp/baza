import { DynamicModule, Global, Module } from '@nestjs/common';
import { SchemaCmsController } from './controllers/schema-cms.controller';
import { BazaNcIntegrationSchemaMapper } from './mappers/baza-nc-integration-schema.mapper';
import { BazaNcIntegrationSchemaRepository } from './repositories/baza-nc-integration-schema.repository';
import { BazaNcIntegrationSchemaService } from './services/baza-nc-integration-schema.service';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { CqrsModule } from '@nestjs/cqrs';
import { BAZA_NC_INTEGRATION_SCHEMA_API_CONFIG_TOKEN, BazaNcIntegrationSchemaApiConfig } from './baza-nc-integration-schema-api.config';
import { BazaNcIntegrationSchemaBootstrapService } from './services/baza-nc-integration-schema-bootstrap.service';
import { SchemaController } from './controllers/schema.controller';

@Module({})
export class BazaNcIntegrationSchemaApiModule {
    static forRootAsync(config: BazaNcIntegrationSchemaApiConfig): DynamicModule {
        return {
            module: BazaNcIntegrationSchemaApiGlobalModule,
            providers: [
                {
                    provide: BAZA_NC_INTEGRATION_SCHEMA_API_CONFIG_TOKEN,
                    useValue: config,
                },
            ],
            exports: [BAZA_NC_INTEGRATION_SCHEMA_API_CONFIG_TOKEN],
        };
    }
}

@Global()
@Module({
    imports: [CqrsModule, BazaCrudApiModule],
    controllers: [SchemaCmsController, SchemaController],
    providers: [
        BazaNcIntegrationSchemaMapper,
        BazaNcIntegrationSchemaRepository,
        BazaNcIntegrationSchemaService,
        BazaNcIntegrationSchemaBootstrapService,
    ],
    exports: [BazaNcIntegrationSchemaMapper, BazaNcIntegrationSchemaRepository, BazaNcIntegrationSchemaService],
})
class BazaNcIntegrationSchemaApiGlobalModule {}
