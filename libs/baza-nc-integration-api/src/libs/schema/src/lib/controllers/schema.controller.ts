import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationPublicOpenApi,
    BazaNcIntegrationSchemaEndpoint,
    BazaNcIntegrationSchemaEndpointPaths,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSchemaService } from '../services/baza-nc-integration-schema.service';

@Controller()
@ApiTags(BazaNcIntegrationPublicOpenApi.Schema)
export class SchemaController implements BazaNcIntegrationSchemaEndpoint {
    constructor(private readonly service: BazaNcIntegrationSchemaService) {}

    @Get(BazaNcIntegrationSchemaEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Schema (Public-Schema only). Use received strings as value(s) for filtering data',
    })
    @ApiOkResponse({
        type: String,
        isArray: true,
        description: 'List of Schema',
    })
    async list(): Promise<Array<string>> {
        return this.service.listPublicSchema();
    }
}
