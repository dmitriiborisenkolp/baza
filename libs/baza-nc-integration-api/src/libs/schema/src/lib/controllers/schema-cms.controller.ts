import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiExtraModels, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationCmsOpenApi,
    BazaNcIntegrationSchemaCmsCreateRequest,
    BazaNcIntegrationSchemaCmsDeleteRequest,
    BazaNcIntegrationSchemaCmsEndpoint,
    BazaNcIntegrationSchemaCmsEndpointPaths,
    BazaNcIntegrationSchemaCmsGetByIdRequest,
    BazaNcIntegrationSchemaCmsListRequest,
    BazaNcIntegrationSchemaCmsListResponse,
    BazaNcIntegrationSchemaCmsRenameRequest,
    BazaNcIntegrationSchemaCmsSetSortOrderRequest,
    BazaNcIntegrationSchemaCmsUpdateRequest,
    bazaNcIntegrationSchemaDefinitionSwaggerDefinitions,
    BazaNcIntegrationSchemaDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaMapper } from '../mappers/baza-nc-integration-schema.mapper';
import { BazaNcIntegrationSchemaService } from '../services/baza-nc-integration-schema.service';

@Controller()
@ApiExtraModels(...bazaNcIntegrationSchemaDefinitionSwaggerDefinitions)
@ApiTags(BazaNcIntegrationCmsOpenApi.Schema)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationSchemaManagement])
export class SchemaCmsController implements BazaNcIntegrationSchemaCmsEndpoint {
    constructor(private readonly mapper: BazaNcIntegrationSchemaMapper, private readonly service: BazaNcIntegrationSchemaService) {}

    @Post(BazaNcIntegrationSchemaCmsEndpointPaths.create)
    @ApiOperation({
        summary: 'create',
        description: 'Creates a new Schema',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSchemaDto,
    })
    async create(@Body() request: BazaNcIntegrationSchemaCmsCreateRequest): Promise<BazaNcIntegrationSchemaDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationSchemaCmsEndpointPaths.update)
    @ApiOperation({
        summary: 'update',
        description: 'Updates existing Schema',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSchemaDto,
    })
    async update(@Body() request: BazaNcIntegrationSchemaCmsUpdateRequest): Promise<BazaNcIntegrationSchemaDto> {
        await this.service.preventLockedChanges(request.id);

        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationSchemaCmsEndpointPaths.rename)
    @ApiOperation({
        summary: 'rename',
        description: 'Renames existing Schema',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSchemaDto,
    })
    async rename(@Body() request: BazaNcIntegrationSchemaCmsRenameRequest): Promise<BazaNcIntegrationSchemaDto> {
        await this.service.preventLockedChanges(request.id);

        const entity = await this.service.rename(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationSchemaCmsEndpointPaths.delete)
    @ApiOperation({
        summary: 'delete',
        description: 'Deletes existing Schema',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSchemaDto,
    })
    async delete(@Body() request: BazaNcIntegrationSchemaCmsDeleteRequest): Promise<void> {
        await this.service.preventLockedChanges(request.id);

        await this.service.delete(request);
    }

    @Post(BazaNcIntegrationSchemaCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of Schemas',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSchemaDto,
    })
    async list(@Body() request: BazaNcIntegrationSchemaCmsListRequest): Promise<BazaNcIntegrationSchemaCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcIntegrationSchemaCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns Schema by ID',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSchemaDto,
    })
    async getById(@Body() request: BazaNcIntegrationSchemaCmsGetByIdRequest): Promise<BazaNcIntegrationSchemaDto> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationSchemaCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
        description: 'Set Sort Order for Schema',
    })
    @ApiOkResponse()
    async setSortOrder(@Body() request: BazaNcIntegrationSchemaCmsSetSortOrderRequest): Promise<void> {
        await this.service.setSortOrder(request);
    }
}
