/**
 * This event will be raised when Schema is Updated
 * For renaming operation an additional BazaNcIntegrationSchemaRenamedEvent event will be raised
 */
export class BazaNcIntegrationSchemaUpdatedEvent {
    constructor(
        public readonly payload: {
            schemaId: number;
        },
    ) {}
}
