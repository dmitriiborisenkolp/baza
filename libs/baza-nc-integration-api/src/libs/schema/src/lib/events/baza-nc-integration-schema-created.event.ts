/**
 * This event will be raised when new Schema is added
 */
export class BazaNcIntegrationSchemaCreatedEvent {
    constructor(
        public readonly payload: {
            schemaId: number;
        },
    ) {}
}
