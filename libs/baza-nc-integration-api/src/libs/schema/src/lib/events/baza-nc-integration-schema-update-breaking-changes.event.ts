/**
 * This event will be raised when new Schema is updated with breaking changes to Listings
 */
export class BazaNcIntegrationSchemaUpdateBreakingChangesEvent {
    constructor(
        public readonly payload: {
            schemaId: number;
            oldUpdateId: string;
            newUpdateId: string;
            oldSchemaTitle: string;
            newSchemaTitle: string;
        },
    ) {}
}
