/**
 * This event will be raised when Schema is Renamed
 */
export class BazaNcIntegrationSchemaRenamedEvent {
    constructor(
        public readonly payload: {
            schemaId: number;
            oldSchemaTitle: string;
            newSchemaTitle: string;
        },
    ) {}
}
