/**
 * This event will be raised when Schema is deleted
 */
export class BazaNcIntegrationSchemaDeletedEvent {
    constructor(
        public readonly payload: {
            schemaId: number;
        },
    ) {}
}
