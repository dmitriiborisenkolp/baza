/**
 * This event will be raised when new Schema is updated with soft (non-breaking) changes to Listings
 */
export class BazaNcIntegrationSchemaUpdateSoftChangesEvent {
    constructor(
        public readonly payload: {
            schemaId: number;
            oldUpdateId: string;
            newUpdateId: string;
            oldSchemaTitle: string;
            newSchemaTitle: string;
        },
    ) {}
}
