import { Module } from '@nestjs/common';
import { BazaNcIntegrationSchemaPublicFixture } from './integration-tests/fixtures/baza-nc-integration-schema-public.fixture';
import { BazaNcIntegrationSchemaApiModule } from './baza-nc-integration-schema-api.module';

const E2E_FIXTURES = [BazaNcIntegrationSchemaPublicFixture];

@Module({
    imports: [BazaNcIntegrationSchemaApiModule],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcIntegrationSchemaE2eModule {}
