import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationSchemaEntity } from '../entities/baza-nc-integration-schema.entity';
import {
    bazaNcIntegrationSchemaConfig,
    BazaNcIntegrationSchemaDefinition,
    BazaNcIntegrationSchemaDto,
    bazaNcIntegrationSchemaFieldId,
} from '@scaliolabs/baza-nc-integration-shared';

/**
 * BazaNcIntegrationSchemaEntity to BazaNcIntegrationSchemaDto Mapper
 */
@Injectable()
export class BazaNcIntegrationSchemaMapper {
    /**
     * (Single Entity) BazaNcIntegrationSchemaEntity to BazaNcIntegrationSchemaDto Mapper
     * @param input
     */
    entityToDTO(input: BazaNcIntegrationSchemaEntity): BazaNcIntegrationSchemaDto {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            updateId: input.updateId,
            title: input.title,
            displayName: input.published ? input.displayName : undefined,
            published: input.published,
            locked: input.isLocked,
            tabs: input.tabs || [],
            definitions: input.definitions.map((next) => ({
                ...next,
                payload: {
                    ...next.payload,
                    id: bazaNcIntegrationSchemaFieldId(next.type, next.payload),
                    required: next.payload.required && bazaNcIntegrationSchemaConfig().allowedRequiredFlagForTypes.includes(next.type),
                    isDisplayedInDetails:
                        next.payload.isDisplayedInDetails &&
                        bazaNcIntegrationSchemaConfig().allowedSchemaFieldTypesForDetails.includes(next.type),
                },
            })) as Array<BazaNcIntegrationSchemaDefinition>,
        };
    }

    /**
     * (Many Entities) BazaNcIntegrationSchemaEntity to BazaNcIntegrationSchemaDto Mapper
     * @param input
     */
    entitiesToDTOs(input: Array<BazaNcIntegrationSchemaEntity>): Array<BazaNcIntegrationSchemaDto> {
        return input.map((entity) => this.entityToDTO(entity));
    }
}
