import { Inject, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { BazaNcIntegrationSchemaRepository } from '../repositories/baza-nc-integration-schema.repository';
import { BAZA_NC_INTEGRATION_SCHEMA_API_CONFIG_TOKEN, BazaNcIntegrationSchemaApiConfig } from '../baza-nc-integration-schema-api.config';
import { BazaLogger } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaService } from './baza-nc-integration-schema.service';
import { bazaNcIntegrationSchemaId } from '@scaliolabs/baza-nc-integration-shared';

/**
 * Bootstrap or Sync Schemas from Bundle Configuration
 */
@Injectable()
export class BazaNcIntegrationSchemaBootstrapService implements OnApplicationBootstrap {
    constructor(
        @Inject(BAZA_NC_INTEGRATION_SCHEMA_API_CONFIG_TOKEN) private readonly bundleConfig: BazaNcIntegrationSchemaApiConfig,
        private readonly logger: BazaLogger,
        private readonly repository: BazaNcIntegrationSchemaRepository,
        private readonly service: BazaNcIntegrationSchemaService,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    async onApplicationBootstrap(): Promise<void> {
        await this.syncSchemas();
        await this.deleteOutdatedSchemas();
    }

    /**
     * Sync Schemas (Create / Update)
     * @private
     */
    private async syncSchemas(): Promise<void> {
        for (const bootstrap of this.bundleConfig.schemas) {
            const updateId = bazaNcIntegrationSchemaId(bootstrap.definitions);

            const existingByTitle = await this.repository.findByTitle(bootstrap.title);
            const existingByUpdateId = await this.repository.findBySchemaUpdateId(updateId);

            if (existingByTitle) {
                if (updateId !== existingByTitle.updateId) {
                    await this.service.update(
                        {
                            ...bootstrap,
                            id: existingByTitle.id,
                        },
                        {
                            locked: true,
                        },
                    );

                    this.logger.log(`Schema "${existingByTitle.title}" was updated`);
                }
            } else if (existingByUpdateId) {
                if (existingByUpdateId.title !== bootstrap.title) {
                    await this.service.rename({
                        id: existingByUpdateId.id,
                        title: bootstrap.title,
                    });

                    this.logger.log(`Schema "${existingByUpdateId.title}" was renamed to "${bootstrap.title}"`);
                }
            } else {
                await this.service.create(bootstrap, {
                    locked: true,
                });

                this.logger.log(`New schema added: "${bootstrap.title}"`);
            }
        }
    }

    /**
     * Deletes Outdated Schemas
     * @private
     */
    private async deleteOutdatedSchemas(): Promise<void> {
        const generated = await this.repository.findGeneratedSchemas();
        const titles = this.bundleConfig.schemas.map((next) => next.title);

        for (const schema of generated) {
            if (!titles.includes(schema.title)) {
                await this.service.delete({
                    id: schema.id,
                });
            }
        }
    }
}
