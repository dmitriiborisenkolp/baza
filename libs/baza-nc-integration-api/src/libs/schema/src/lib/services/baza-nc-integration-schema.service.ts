import { Injectable } from '@nestjs/common';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import {
    BazaNcIntegrationSchemaCmsCreateRequest,
    BazaNcIntegrationSchemaCmsDeleteRequest,
    BazaNcIntegrationSchemaCmsEntityBody,
    BazaNcIntegrationSchemaCmsGetByIdRequest,
    BazaNcIntegrationSchemaCmsListRequest,
    BazaNcIntegrationSchemaCmsListResponse,
    BazaNcIntegrationSchemaCmsRenameRequest,
    BazaNcIntegrationSchemaCmsSetSortOrderRequest,
    BazaNcIntegrationSchemaCmsUpdateRequest,
    BazaNcIntegrationSchemaDefinition,
    bazaNcIntegrationSchemaFieldId,
    bazaNcIntegrationSchemaId,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSchemaEntity } from '../entities/baza-nc-integration-schema.entity';
import { BazaNcIntegrationSchemaDuplicateTitleException } from '../exceptions/baza-nc-integration-schema-duplicate-title.exception';
import { BazaNcIntegrationSchemaLockedException } from '../exceptions/baza-nc-integration-schema-locked.exception';
import { BazaNcIntegrationSchemaMapper } from '../mappers/baza-nc-integration-schema.mapper';
import { BazaNcIntegrationSchemaRepository } from '../repositories/baza-nc-integration-schema.repository';

interface SaveOptions {
    locked: boolean;
}

const defaultSaveOptions: () => SaveOptions = () => ({
    locked: false,
});

/**
 * Schema Service
 */
@Injectable()
export class BazaNcIntegrationSchemaService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly mapper: BazaNcIntegrationSchemaMapper,
        private readonly repository: BazaNcIntegrationSchemaRepository,
    ) {}

    /**
     * Creates a new Schema
     * @param request
     */
    async create(
        request: BazaNcIntegrationSchemaCmsCreateRequest,
        options: SaveOptions = defaultSaveOptions(),
    ): Promise<BazaNcIntegrationSchemaEntity> {
        const entity = new BazaNcIntegrationSchemaEntity();

        await this.populate(entity, request, options);
        await this.repository.save([entity]);

        return entity;
    }

    /**
     * Updates existing Schema
     * @param request
     * @param options
     */
    async update(
        request: BazaNcIntegrationSchemaCmsUpdateRequest,
        options: SaveOptions = defaultSaveOptions(),
    ): Promise<BazaNcIntegrationSchemaEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request, options);
        await this.repository.save([entity]);

        return entity;
    }

    /**
     * Renames existing Schema
     * @param request
     */
    async rename(request: BazaNcIntegrationSchemaCmsRenameRequest): Promise<BazaNcIntegrationSchemaEntity> {
        const entity = await this.repository.getById(request.id);
        const existing = await this.repository.findByTitle(request.title);

        if (existing && existing.id !== request.id) {
            throw new BazaNcIntegrationSchemaDuplicateTitleException();
        }

        entity.title = request.title;

        await this.repository.save([entity]);

        return entity;
    }

    /**
     * Deletes existing Schema
     * @param request
     */
    async delete(request: BazaNcIntegrationSchemaCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);
    }

    /**
     * Returns list of Schemas
     * @param request
     */
    async list(request: BazaNcIntegrationSchemaCmsListRequest): Promise<BazaNcIntegrationSchemaCmsListResponse> {
        return this.crud.find({
            request,
            entity: BazaNcIntegrationSchemaEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            findOptions: {
                order: {
                    sortOrder: 'ASC',
                },
            },
            withMaxSortOrder: true,
        });
    }

    /**
     * Returns list of Public Schema
     * Returns unique list of Display Name only
     */
    async listPublicSchema(): Promise<Array<string>> {
        return this.repository.findPublicSchema();
    }

    /**
     * Returns Schema by Id
     * @param request
     */
    async getById(request: BazaNcIntegrationSchemaCmsGetByIdRequest): Promise<BazaNcIntegrationSchemaEntity> {
        return this.repository.getById(request.id);
    }

    /**
     * Prevents changes for Locked Schemas
     * @param schemaId
     */
    async preventLockedChanges(schemaId: number): Promise<void> {
        const entity = await this.repository.getById(schemaId);

        if (entity.isLocked) {
            throw new BazaNcIntegrationSchemaLockedException();
        }
    }

    /**
     * Updates Sort Order for Schema
     * @param request
     */
    async setSortOrder(request: BazaNcIntegrationSchemaCmsSetSortOrderRequest): Promise<void> {
        const entities = await this.repository.findAll();

        await this.crudSort.setSortOrder<BazaNcIntegrationSchemaEntity>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities,
        });

        await this.repository.save(entities);
    }

    /**
     * Populates data for Schema entity
     * @param target
     * @param entityBody
     * @param options
     * @private
     */
    private async populate(
        target: BazaNcIntegrationSchemaEntity,
        entityBody: BazaNcIntegrationSchemaCmsEntityBody,
        options: SaveOptions,
    ): Promise<void> {
        const existing = await this.repository.findByTitle(entityBody.title);

        if (existing && existing.id !== target.id) {
            throw new BazaNcIntegrationSchemaDuplicateTitleException();
        }

        if (!target.id) {
            target.sortOrder = await this.crudSort.getLastSortOrder(BazaNcIntegrationSchemaEntity);
        }

        if (!Array.isArray(entityBody.definitions)) {
            entityBody.definitions = [];
        }

        for (const field of (entityBody.definitions || []) as Array<BazaNcIntegrationSchemaDefinition>) {
            field.payload.id = bazaNcIntegrationSchemaFieldId(field.type, field.payload);
        }

        for (let i = 0; i < entityBody.definitions.length; i++) {
            if (entityBody.definitions[i].payload.sortId === undefined || entityBody.definitions[i].payload.sortId === null) {
                entityBody.definitions[i].payload.sortId = i;
            }
        }

        target.published = entityBody.published;
        target.updateId = bazaNcIntegrationSchemaId(entityBody.definitions);
        target.title = entityBody.title;
        target.displayName = target.published ? entityBody.displayName : null;
        target.definitions = entityBody.definitions;
        target.generated = options.locked;

        if (Array.isArray(entityBody.tabs)) {
            target.tabs = entityBody.tabs;
        }
    }
}
