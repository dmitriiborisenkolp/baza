import { BazaNcIntegrationSchemaCmsCreateRequest } from '@scaliolabs/baza-nc-integration-shared';

/**
 * Configuration for Schema API
 */
export interface BazaNcIntegrationSchemaApiConfig {
    /**
     * Schemas which will be created or synced on App start
     * If your project don't need any, pass `[]` empty array
     */
    schemas: Array<BazaNcIntegrationSchemaCmsCreateRequest>;
}

/**
 * Injection Token for Nest.JS DI
 */
export const BAZA_NC_INTEGRATION_SCHEMA_API_CONFIG_TOKEN = Symbol();
