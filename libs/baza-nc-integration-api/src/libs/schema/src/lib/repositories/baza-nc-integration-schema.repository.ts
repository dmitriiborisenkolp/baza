import { Injectable } from '@nestjs/common';
import { Connection, In, IsNull, Not, Repository } from 'typeorm';
import { BazaNcIntegrationSchemaEntity } from '../entities/baza-nc-integration-schema.entity';
import { BazaNcIntegrationSchemaNotFoundException } from '../exceptions/baza-nc-integration-schema-not-found.exception';
import { EventBus } from '@nestjs/cqrs';
import { BazaNcIntegrationSchemaCreatedEvent } from '../events/baza-nc-integration-schema-created.event';
import { BazaNcIntegrationSchemaUpdatedEvent } from '../events/baza-nc-integration-schema-updated.event';
import { BazaNcIntegrationSchemaDeletedEvent } from '../events/baza-nc-integration-schema-deleted.event';
import { BazaNcIntegrationSchemaRenamedEvent } from '../events/baza-nc-integration-schema-renamed.event';
import { bazaNcIntegrationSchemaId } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSchemaUpdateBreakingChangesEvent } from '../events/baza-nc-integration-schema-update-breaking-changes.event';
import { BazaNcIntegrationSchemaUpdateSoftChangesEvent } from '../events/baza-nc-integration-schema-update-soft-changes.event';
import * as _ from 'lodash';

/**
 * Repository for BazaNcIntegrationSchemaEntity entity
 */
@Injectable()
export class BazaNcIntegrationSchemaRepository {
    constructor(private readonly eventBus: EventBus, private readonly connection: Connection) {}

    /**
     * Returns TypeORM Repository for BazaNcIntegrationSchemaEntity
     */
    get repository(): Repository<BazaNcIntegrationSchemaEntity> {
        return this.connection.getRepository(BazaNcIntegrationSchemaEntity);
    }

    /**
     * Saves BazaNcIntegrationSchemaEntity entities to DB
     * @param entities
     */
    async save(entities: Array<BazaNcIntegrationSchemaEntity>): Promise<void> {
        const createdEntities = entities.filter((next) => !next.id);
        const updatedEntities = entities.filter((next) => !!next.id);
        const renamedEntities: Array<{
            entity: BazaNcIntegrationSchemaEntity;
            oldTitle: string;
        }> = [];
        const breakingChangesEntities: Array<{
            entity: BazaNcIntegrationSchemaEntity;
            oldTitle: string;
            oldSchemaUpdateId: string;
        }> = [];
        const softChangesEntities: Array<{
            oldTitle: string;
            oldSchemaUpdateId: string;
            entity: BazaNcIntegrationSchemaEntity;
        }> = [];

        for (const newState of updatedEntities) {
            const currentState = await this.getById(newState.id);

            const newStateUpdateId = bazaNcIntegrationSchemaId(newState.definitions);
            const currentStateUpdateId = bazaNcIntegrationSchemaId(currentState.definitions);

            if (currentState.title !== newState.title) {
                renamedEntities.push({
                    entity: newState,
                    oldTitle: currentState.title,
                });
            }

            const updateState = {
                entity: newState,
                oldTitle: currentState.title,
                oldSchemaUpdateId: currentState.updateId,
            };

            newStateUpdateId === currentStateUpdateId ? softChangesEntities.push(updateState) : breakingChangesEntities.push(updateState);
        }

        await this.repository.save(entities);

        createdEntities.forEach((next) => {
            this.eventBus.publish(
                new BazaNcIntegrationSchemaCreatedEvent({
                    schemaId: next.id,
                }),
            );
        });

        updatedEntities.forEach((next) => {
            this.eventBus.publish(
                new BazaNcIntegrationSchemaUpdatedEvent({
                    schemaId: next.id,
                }),
            );
        });

        renamedEntities.forEach((next) => {
            this.eventBus.publish(
                new BazaNcIntegrationSchemaRenamedEvent({
                    schemaId: next.entity.id,
                    newSchemaTitle: next.entity.title,
                    oldSchemaTitle: next.oldTitle,
                }),
            );
        });

        breakingChangesEntities.forEach((next) => {
            this.eventBus.publish(
                new BazaNcIntegrationSchemaUpdateBreakingChangesEvent({
                    schemaId: next.entity.id,
                    newSchemaTitle: next.entity.title,
                    oldSchemaTitle: next.oldTitle,
                    newUpdateId: next.entity.updateId,
                    oldUpdateId: next.oldSchemaUpdateId,
                }),
            );
        });

        softChangesEntities.forEach((next) => {
            this.eventBus.publish(
                new BazaNcIntegrationSchemaUpdateSoftChangesEvent({
                    schemaId: next.entity.id,
                    newSchemaTitle: next.entity.title,
                    oldSchemaTitle: next.oldTitle,
                    newUpdateId: next.entity.updateId,
                    oldUpdateId: next.oldSchemaUpdateId,
                }),
            );
        });
    }

    /**
     * Removes BazaNcIntegrationSchemaEntity entities from DB
     * @param entities
     */
    async remove(entities: Array<BazaNcIntegrationSchemaEntity>): Promise<void> {
        const ids = entities.map((next) => next.id);

        await this.repository.remove(entities);

        ids.forEach((schemaId) => {
            this.eventBus.publish(
                new BazaNcIntegrationSchemaDeletedEvent({
                    schemaId,
                }),
            );
        });
    }

    /**
     * Returns BazaNcIntegrationSchemaEntity by ID
     * If not found, method will returns `undefined`
     * @param id
     */
    async findById(id: number): Promise<BazaNcIntegrationSchemaEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    id,
                },
            ],
        });
    }

    /**
     * Returns BazaNcIntegrationSchemaEntity by ID
     * If not found, method will throw an exception
     * @param id
     */
    async getById(id: number): Promise<BazaNcIntegrationSchemaEntity> {
        const entity = await this.findById(id);

        if (!entity) {
            throw new BazaNcIntegrationSchemaNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcIntegrationSchemaEntity by Title
     * If not found, method will returns `undefined`
     * @param title
     */
    async findByTitle(title: string): Promise<BazaNcIntegrationSchemaEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    title,
                },
            ],
        });
    }

    /**
     * Returns Schemas generated by Schema Bootstrap
     */
    async findGeneratedSchemas(): Promise<Array<BazaNcIntegrationSchemaEntity>> {
        return this.repository.find({
            where: [
                {
                    generated: true,
                },
            ],
        });
    }

    /**
     * Returns BazaNcIntegrationSchemaEntity by Update Id
     * If not found, method will returns `undefined`
     * @param updateId
     */
    async findBySchemaUpdateId(updateId: string): Promise<BazaNcIntegrationSchemaEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    updateId,
                },
            ],
        });
    }

    /**
     * Returns list of Public Schema
     * Returns unique list of Display Name only
     */
    async findPublicSchema(): Promise<Array<string>> {
        return _.uniq(
            (
                await this.repository.find({
                    where: [
                        {
                            published: true,
                            displayName: Not(IsNull()),
                        },
                    ],
                    select: ['displayName'],
                    order: {
                        sortOrder: 'ASC',
                    },
                })
            ).map((next) => next.displayName.trim()),
        );
    }

    /**
     * Returns all Schema
     */
    async findAll(): Promise<Array<BazaNcIntegrationSchemaEntity>> {
        return this.repository.find();
    }

    /**
     * Returns all Schema IDs by Display Name
     * @param displayNames
     */
    async findAllPublishedSchemaIdsByDisplayNames(displayNames: Array<string>): Promise<Array<number>> {
        const response = await this.repository.find({
            select: ['updateId'],
            where: [{ displayName: In(displayNames), published: true }],
        });

        return response.map((next) => next.id);
    }
}
