import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSchemaCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';
import { ulid } from 'ulid';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-schema/api/integration-tests/tests/005-baza-nc-integration-schema-tabs-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let SCHEMA_ID: number;
    let SCHEMA_UPDATE_ID: string;
    let SCHEMA_FIELD_1_UPDATE_ID: string;
    let SCHEMA_FIELD_2_UPDATE_ID: string;
    let ULID_TAB_1: string;
    let ULID_TAB_2: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBeDefined();

        SCHEMA_ID = response.id;
        SCHEMA_UPDATE_ID = response.updateId;
        SCHEMA_FIELD_1_UPDATE_ID = response.definitions[0].payload.id;
        SCHEMA_FIELD_2_UPDATE_ID = response.definitions[1].payload.id;
    });

    it('will add a new tab and will not change Update ID of Schema', async () => {
        const ulidTab1 = ulid();
        const ulidTab2 = ulid();

        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
            tabs: [
                {
                    id: ulidTab1,
                    title: 'Tab 1',
                },
                {
                    id: ulidTab2,
                    title: 'Tab 2',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.tabs).toBeDefined();
        expect(Array.isArray(response.tabs) && response.tabs.length === 2).toBeTruthy();
        expect(response.tabs).toEqual([
            {
                id: ulidTab1,
                title: 'Tab 1',
            },
            {
                id: ulidTab2,
                title: 'Tab 2',
            },
        ]);

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID);
        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_1_UPDATE_ID);
        expect(response.definitions[1].payload.id).toBe(SCHEMA_FIELD_2_UPDATE_ID);

        ULID_TAB_1 = response.tabs[0].id;
        ULID_TAB_2 = response.tabs[1].id;
    });

    it('will display that update ID is unchanged', async () => {
        const response = await dataAccessSchemaCms.getById({
            id: SCHEMA_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.tabs).toBeDefined();
        expect(Array.isArray(response.tabs) && response.tabs.length === 2).toBeTruthy();
        expect(response.tabs).toEqual([
            {
                id: ULID_TAB_1,
                title: 'Tab 1',
            },
            {
                id: ULID_TAB_2,
                title: 'Tab 2',
            },
        ]);

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID);
        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_1_UPDATE_ID);
        expect(response.definitions[1].payload.id).toBe(SCHEMA_FIELD_2_UPDATE_ID);
        expect(response.tabs[0].id).toBe(ULID_TAB_1);
        expect(response.tabs[1].id).toBe(ULID_TAB_2);
    });

    it('will assigned Text field to Tab 1, and it will not be marked as Breaking Change', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                        tabId: ULID_TAB_1,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
            tabs: [
                {
                    id: ULID_TAB_1,
                    title: 'Tab 1',
                },
                {
                    id: ULID_TAB_2,
                    title: 'Tab 2',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.tabs).toBeDefined();
        expect(Array.isArray(response.tabs) && response.tabs.length === 2).toBeTruthy();
        expect(response.tabs).toEqual([
            {
                id: ULID_TAB_1,
                title: 'Tab 1',
            },
            {
                id: ULID_TAB_2,
                title: 'Tab 2',
            },
        ]);

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID);
        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_1_UPDATE_ID);
        expect(response.definitions[0].payload.tabId).toBe(ULID_TAB_1);
        expect(response.definitions[1].payload.tabId).not.toBeDefined();
        expect(response.definitions[1].payload.id).toBe(SCHEMA_FIELD_2_UPDATE_ID);
    });

    it('will assigned Number field to Tab 2, and it will not be marked as Breaking Change', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                        tabId: ULID_TAB_1,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                        tabId: ULID_TAB_2,
                    },
                },
            ],
            tabs: [
                {
                    id: ULID_TAB_1,
                    title: 'Tab 1',
                },
                {
                    id: ULID_TAB_2,
                    title: 'Tab 2',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.tabs).toBeDefined();
        expect(Array.isArray(response.tabs) && response.tabs.length === 2).toBeTruthy();
        expect(response.tabs).toEqual([
            {
                id: ULID_TAB_1,
                title: 'Tab 1',
            },
            {
                id: ULID_TAB_2,
                title: 'Tab 2',
            },
        ]);

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID);
        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_1_UPDATE_ID);
        expect(response.definitions[1].payload.id).toBe(SCHEMA_FIELD_2_UPDATE_ID);
        expect(response.definitions[0].payload.tabId).toBe(ULID_TAB_1);
        expect(response.definitions[1].payload.tabId).toBe(ULID_TAB_2);
    });
});
