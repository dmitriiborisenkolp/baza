import { BazaNcIntegrationSchemaCmsCreateRequest, BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaService } from '../../services/baza-nc-integration-schema.service';
import { BazaNcIntegrationSchemaFixtures } from '../baza-nc-integration-schema.fixtures';
import { Injectable } from '@nestjs/common';

const FIXTURE: Array<BazaNcIntegrationSchemaCmsCreateRequest> = [
    {
        title: 'schema-a',
        displayName: 'Schema A',
        published: true,
        definitions: [
            {
                type: BazaNcIntegrationSchemaType.Text,
                payload: {
                    field: 'text',
                    title: 'Example Text Field',
                    required: false,
                    isDisplayedInDetails: true,
                },
            },
            {
                type: BazaNcIntegrationSchemaType.Select,
                payload: {
                    field: 'select',
                    title: 'Example Select Field',
                    required: false,
                    isDisplayedInDetails: true,
                    values: {
                        foo: 'Foo',
                        bar: 'Bar',
                        baz: 'Baz',
                    },
                },
            },
        ],
    },
    {
        title: 'schema-b',
        displayName: 'Schema B',
        published: true,
        definitions: [
            {
                type: BazaNcIntegrationSchemaType.Text,
                payload: {
                    field: 'text',
                    title: 'Example Text Field',
                    required: false,
                    isDisplayedInDetails: true,
                },
            },
            {
                type: BazaNcIntegrationSchemaType.Select,
                payload: {
                    field: 'select',
                    title: 'Example Select Field',
                    required: false,
                    isDisplayedInDetails: true,
                    values: {
                        foo: 'Foo',
                        bar: 'Bar',
                        baz: 'Baz',
                    },
                },
            },
        ],
    },
    {
        title: 'Schema C',
        published: false,
        definitions: [
            {
                type: BazaNcIntegrationSchemaType.Text,
                payload: {
                    field: 'text',
                    title: 'Example Text Field',
                    required: false,
                    isDisplayedInDetails: true,
                },
            },
            {
                type: BazaNcIntegrationSchemaType.Select,
                payload: {
                    field: 'select',
                    title: 'Example Select Field',
                    required: false,
                    isDisplayedInDetails: true,
                    values: {
                        foo: 'Foo',
                        bar: 'Bar',
                        baz: 'Baz',
                    },
                },
            },
        ],
    },
    {
        title: 'schema-b1',
        displayName: 'Schema B',
        published: true,
        definitions: [
            {
                type: BazaNcIntegrationSchemaType.Text,
                payload: {
                    field: 'text',
                    title: 'Example Text Field',
                    required: false,
                    isDisplayedInDetails: true,
                },
            },
            {
                type: BazaNcIntegrationSchemaType.Select,
                payload: {
                    field: 'select',
                    title: 'Example Select Field',
                    required: false,
                    isDisplayedInDetails: true,
                    values: {
                        foo: 'Foo',
                        bar: 'Bar',
                        baz: 'Baz',
                    },
                },
            },
        ],
    },
];

@Injectable()
export class BazaNcIntegrationSchemaPublicFixture implements BazaE2eFixture {
    constructor(private readonly schema: BazaNcIntegrationSchemaService) {}

    async up(): Promise<void> {
        for (const fixtureRequest of FIXTURE) {
            await this.schema.create(fixtureRequest);
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaNcIntegrationSchemaFixtures.BazaNcIntegrationSchemaPublicFixture,
        provider: BazaNcIntegrationSchemaPublicFixture,
    },
]);
