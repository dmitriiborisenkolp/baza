import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSchemaCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-schema/api/integration-tests/tests/003-baza-nc-integration-schema-details-flag.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let SCHEMA_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema 0',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: false,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBeDefined();

        expect(response.definitions[0].payload.required).toBeFalsy();
        expect(response.definitions[0].payload.isDisplayedInDetails).toBeFalsy();

        SCHEMA_ID = response.id;
    });

    it('will display "isDisplayedInDetails" flag as truthy after updating text field', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema 1',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBeDefined();

        expect(response.definitions[0].payload.required).toBeFalsy();
        expect(response.definitions[0].payload.isDisplayedInDetails).toBeTruthy();
    });

    it('will not display required and isDisplayedInDetails flag as truthy for key/value field', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema 2',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.KeyValue,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBeDefined();

        expect(response.definitions[0].payload.required).toBeFalsy();
        expect(response.definitions[0].payload.isDisplayedInDetails).toBeTruthy();

        expect(response.definitions[1].payload.required).toBeFalsy();
        expect(response.definitions[1].payload.isDisplayedInDetails).toBeFalsy();
    });
});
