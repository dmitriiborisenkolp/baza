import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSchemaCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-schema/api/integration-tests/tests/004-baza-nc-integration-schema-field-update-id.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let SCHEMA_ID: number;

    let SCHEMA_FIELD_ID: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: false,
                        title: 'Example Field',
                        field: 'example_field',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBeDefined();
        expect(response.definitions[0].payload.id).toBeDefined();

        SCHEMA_ID = response.id;
        SCHEMA_FIELD_ID = response.definitions[0].payload.id;
    });

    it('will not refresh field id with same update', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: false,
                        title: 'Example Field',
                        field: 'example_field',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_ID);
    });

    it('will not refresh field id with changing field title', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: false,
                        title: 'Example Field *',
                        field: 'example_field',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_ID);
    });

    it('will not refresh field id with changing isDisplayedInDetails flag', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field *',
                        field: 'example_field',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_ID);
    });

    it('will refresh field id with changing required flag', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field *',
                        field: 'example_field',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.definitions[0].payload.id).not.toBe(SCHEMA_FIELD_ID);

        SCHEMA_FIELD_ID = response.definitions[0].payload.id;
    });

    it('will refresh field id with changing field type to Slider', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Slider,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field *',
                        field: 'example_field',
                        min: 0,
                        max: 100,
                        step: 1,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.definitions[0].payload.id).not.toBe(SCHEMA_FIELD_ID);

        SCHEMA_FIELD_ID = response.definitions[0].payload.id;
    });

    it('will not refresh field id with with same field type as Slider', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Slider,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field',
                        field: 'example_field',
                        min: 0,
                        max: 100,
                        step: 1,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_ID);
    });

    it('will refresh field id with changing field "min" option of Slider', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Slider,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field *',
                        field: 'example_field',
                        min: 10,
                        max: 100,
                        step: 1,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.definitions[0].payload.id).not.toBe(SCHEMA_FIELD_ID);

        SCHEMA_FIELD_ID = response.definitions[0].payload.id;
    });
});
