import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSchemaCmsNodeAccess, BazaNcIntegrationSchemaNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';
import { ulid } from 'ulid';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-schema/api/integration-tests/tests/006-baza-nc-integration-schema-public.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSchema = new BazaNcIntegrationSchemaNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let PRIVATE_SCHEMA_ID: number;
    let PRIVATE_SCHEMA_UPDATE_ID: string;
    let SCHEMA_A_SORT_ORDER: number;
    let SCHEMA_B_SORT_ORDER: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will display that no public schema available', async () => {
        const response = await dataAccessSchema.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response)).toBeTruthy();
        expect(response.length).toBe(0);
    });

    it('will create a private schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'schema-a',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.published).toBeFalsy();
        expect(response.sortOrder).toBe(1);

        PRIVATE_SCHEMA_ID = response.id;
        PRIVATE_SCHEMA_UPDATE_ID = response.updateId;

        SCHEMA_A_SORT_ORDER = response.sortOrder;
    });

    it('will still display that no public schema available', async () => {
        const response = await dataAccessSchema.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response)).toBeTruthy();
        expect(response.length).toBe(0);
    });

    it('will create a public schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'schema-b',
            displayName: 'Schema B',
            published: true,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.published).toBeTruthy();
        expect(response.sortOrder).toBe(2);

        SCHEMA_B_SORT_ORDER = response.sortOrder;
    });

    it('will display that Schema B is available', async () => {
        await http.authE2eUser();

        const response = await dataAccessSchema.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response)).toBeTruthy();
        expect(response.length).toBe(1);
        expect(response).toEqual(['Schema B']);
    });

    it('will update Schema A to Public and keep same UpdateId', async () => {
        const response = await dataAccessSchemaCms.update({
            id: PRIVATE_SCHEMA_ID,
            title: 'schema-a',
            displayName: 'Schema A',
            published: true,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(PRIVATE_SCHEMA_ID);
        expect(response.updateId).toBe(PRIVATE_SCHEMA_UPDATE_ID);
    });

    it('will display that both Schema A and Schema B are available', async () => {
        await http.authE2eUser();

        const response = await dataAccessSchema.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response)).toBeTruthy();
        expect(response.length).toBe(2);
        expect(response).toEqual(['Schema A', 'Schema B']);
    });

    it('will create a public schema  but w/o Display Name specified', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'schema-c',
            published: true,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.published).toBeTruthy();
    });

    it('will still display that both Schema A and Schema B are available', async () => {
        await http.authE2eUser();

        const response = await dataAccessSchema.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response)).toBeTruthy();
        expect(response.length).toBe(2);
        expect(response).toEqual(['Schema A', 'Schema B']);
    });

    it('will move down Schema A', async () => {
        const response = await dataAccessSchemaCms.setSortOrder({
            id: PRIVATE_SCHEMA_ID,
            setSortOrder: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that Schema A and Schema B are swapped', async () => {
        await http.authE2eUser();

        const response = await dataAccessSchema.list();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(Array.isArray(response)).toBeTruthy();
        expect(response.length).toBe(2);
        expect(response).toEqual(['Schema B', 'Schema A']);
    });

    it('will display that CMS list endpoint also reflected changes', async () => {
        const response = await dataAccessSchemaCms.list({});

        expect(response.items.map((next) => next.displayName)).toEqual(['Schema B', 'Schema A', null]);
    });
});
