import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSchemaCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-schema/api/integration-tests/tests/001-baza-nc-integration-schema-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let SCHEMA_ID: number;
    let SCHEMA_UPDATE_ID: string;
    let SCHEMA_FIELD_1_UPDATE_ID: string;
    let SCHEMA_FIELD_2_UPDATE_ID: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBeDefined();

        SCHEMA_ID = response.id;
        SCHEMA_UPDATE_ID = response.updateId;
        SCHEMA_FIELD_1_UPDATE_ID = response.definitions[0].payload.id;
        SCHEMA_FIELD_2_UPDATE_ID = response.definitions[1].payload.id;
    });

    it('will display created schema in list', async () => {
        const response = await dataAccessSchemaCms.list({
            index: 1,
            size: -1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);
        expect(response.items[0].id).toBe(SCHEMA_ID);
    });

    it('will returns schema by id', async () => {
        const response = await dataAccessSchemaCms.getById({
            id: SCHEMA_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(SCHEMA_ID);
        expect(response.updateId).toBe(SCHEMA_UPDATE_ID);
        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_1_UPDATE_ID);
        expect(response.definitions[1].payload.id).toBe(SCHEMA_FIELD_2_UPDATE_ID);
    });

    it('will successfully update schema', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(SCHEMA_ID);
        expect(response.title).toBe('Example Schema *');
        expect(response.updateId).toBe(SCHEMA_UPDATE_ID);
        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_1_UPDATE_ID);
        expect(response.definitions[1].payload.id).toBe(SCHEMA_FIELD_2_UPDATE_ID);
    });

    it('will actually update schema', async () => {
        const response = await dataAccessSchemaCms.getById({
            id: SCHEMA_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(SCHEMA_ID);
        expect(response.title).toBe('Example Schema *');
        expect(response.updateId).toBe(SCHEMA_UPDATE_ID);
        expect(response.definitions[0].payload.id).toBe(SCHEMA_FIELD_1_UPDATE_ID);
        expect(response.definitions[1].payload.id).toBe(SCHEMA_FIELD_2_UPDATE_ID);
    });

    it('will rename schema', async () => {
        const response = await dataAccessSchemaCms.rename({
            id: SCHEMA_ID,
            title: 'Renamed Schema',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Renamed Schema');
    });

    it('will updates schema name of renamed schema', async () => {
        const response = await dataAccessSchemaCms.getById({
            id: SCHEMA_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Renamed Schema');
    });

    it('will successfully delete schema', async () => {
        const response = await dataAccessSchemaCms.delete({
            id: SCHEMA_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted schema in list', async () => {
        const response = await dataAccessSchemaCms.list({
            index: 1,
            size: -1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(0);
    });
});
