import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSchemaCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-schema/api/integration-tests/tests/002-baza-nc-integration-schema-cms-update-id.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let SCHEMA_ID: number;

    let SCHEMA_UPDATE_ID_1: string;
    let SCHEMA_UPDATE_ID_2: string;
    let SCHEMA_UPDATE_ID_3: string;
    let SCHEMA_UPDATE_ID_4: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema',
            definitions: [],
            published: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBeDefined();

        SCHEMA_ID = response.id;
        SCHEMA_UPDATE_ID_1 = response.updateId;
    });

    it('will not update updateId with changing title of Schema', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            definitions: [],
            published: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID_1);
    });

    it('will update updateId after adding new fields', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).not.toBe(SCHEMA_UPDATE_ID_1);

        SCHEMA_UPDATE_ID_2 = response.updateId;
    });

    it('will revert updateId to previous one after rollback', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            definitions: [],
            published: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID_1);
    });

    it('will revert updateId to previous one after rollback & updating schema title', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema',
            definitions: [],
            published: false,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID_1);
    });

    it('will keep same updateId after adding same fields with different titles', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1 *',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2 *',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID_2);
    });

    it('will update updateId after adding updating required flag for same fields', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1 *',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2 *',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).not.toBe(SCHEMA_UPDATE_ID_2);

        SCHEMA_UPDATE_ID_3 = response.updateId;
    });

    it('will update updateId after adding updating type for same fields', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.TextArea,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field 1 *',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: true,
                        isDisplayedInDetails: true,
                        title: 'Example Field 2 *',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).not.toBe(SCHEMA_UPDATE_ID_3);

        SCHEMA_UPDATE_ID_4 = response.updateId;
    });

    it('will keep same updateId after updating isDisplayedInDetails flag for same fields', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.TextArea,
                    payload: {
                        required: true,
                        isDisplayedInDetails: false,
                        title: 'Example Field 1 *',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: true,
                        isDisplayedInDetails: false,
                        title: 'Example Field 2 *',
                        field: 'example_field_2',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBe(SCHEMA_UPDATE_ID_4);
    });

    it('will update updateId after adding new field', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA_ID,
            title: 'Example Schema *',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.TextArea,
                    payload: {
                        required: true,
                        isDisplayedInDetails: false,
                        title: 'Example Field 1 *',
                        field: 'example_field_1',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        required: true,
                        isDisplayedInDetails: false,
                        title: 'Example Field 2',
                        field: 'example_field_2',
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Switcher,
                    payload: {
                        required: true,
                        isDisplayedInDetails: false,
                        title: 'Example Field 3',
                        field: 'example_field_3',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).not.toBe(SCHEMA_UPDATE_ID_4);
    });
});
