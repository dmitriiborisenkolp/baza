import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaErrorCodes, bazaNcIntegrationSchemaErrorCodesI18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationSchemaNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaNotFound,
            bazaNcIntegrationSchemaErrorCodesI18n[BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
