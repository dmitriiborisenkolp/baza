import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaErrorCodes, bazaNcIntegrationSchemaErrorCodesI18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationSchemaLockedException extends BazaAppException {
    constructor() {
        super(
            BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaLocked,
            bazaNcIntegrationSchemaErrorCodesI18n[BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaLocked],
            HttpStatus.BAD_REQUEST,
        );
    }
}
