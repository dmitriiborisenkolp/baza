import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaErrorCodes, bazaNcIntegrationSchemaErrorCodesI18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationSchemaDuplicateTitleException extends BazaAppException {
    constructor() {
        super(
            BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaDuplicateTitle,
            bazaNcIntegrationSchemaErrorCodesI18n[BazaNcIntegrationSchemaErrorCodes.BazaNcIntegrationSchemaDuplicateTitle],
            HttpStatus.CONFLICT,
        );
    }
}
