import { BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import {
    BazaNcIntegrationSchemaDefinition,
    bazaNcIntegrationSchemaId,
    BazaNcIntegrationSchemaTab,
} from '@scaliolabs/baza-nc-integration-shared';
import { CrudSortableEntity } from '@scaliolabs/baza-core-shared';

@Entity()
export class BazaNcIntegrationSchemaEntity implements CrudSortableEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: 0,
        type: 'integer',
    })
    sortOrder: number;

    @Column()
    updateId: string;

    @Column()
    title: string;

    @Column({
        nullable: true,
    })
    displayName?: string;

    @Column({
        default: false,
    })
    generated: boolean;

    @Column({
        default: false,
    })
    published: boolean;

    @Column({
        type: 'json', // Do not change it to jsonb - sorting will be broken
        default: [],
    })
    definitions: Array<BazaNcIntegrationSchemaDefinition>;

    @Column({
        type: 'json', // Do not change it to jsonb - sorting will be broken
        default: [],
    })
    tabs: Array<BazaNcIntegrationSchemaTab>;

    @BeforeInsert()
    @BeforeUpdate()
    _updateId(): void {
        this.updateId = bazaNcIntegrationSchemaId(this.definitions || []);
    }

    @BeforeInsert()
    @BeforeUpdate()
    _updateDefinitionIds(): void {
        for (const definition of this.definitions) {
            if (!definition.payload.id) {
                definition.payload.id = bazaNcIntegrationSchemaId(this.definitions || []);
            }
        }
    }

    get isLocked(): boolean {
        return this.generated;
    }
}
