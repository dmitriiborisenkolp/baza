import { CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';

export const BAZA_NC_INTEGRATION_FAVORITE_RELATIONS = [
    'account',
    'listing',
];

@Entity()
export class BazaNcIntegrationFavoriteEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @CreateDateColumn()
    dateCreated: Date;

    @ManyToOne(() => AccountEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity;

    @ManyToOne(() => BazaNcIntegrationListingsEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    listing: BazaNcIntegrationListingsEntity;
}
