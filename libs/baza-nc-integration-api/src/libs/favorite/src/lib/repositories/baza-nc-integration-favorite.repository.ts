import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BAZA_NC_INTEGRATION_FAVORITE_RELATIONS, BazaNcIntegrationFavoriteEntity } from '../entities/baza-nc-integration-favorite.entity';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationFavoriteRepository {
    constructor(
        private readonly connection: Connection,
    ) {
    }

    get repository(): Repository<BazaNcIntegrationFavoriteEntity> {
        return this.connection.getRepository(BazaNcIntegrationFavoriteEntity);
    }

    async save(entities: Array<BazaNcIntegrationFavoriteEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaNcIntegrationFavoriteEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findByAccountAndListing(account: AccountEntity, listing: BazaNcIntegrationListingsEntity): Promise<BazaNcIntegrationFavoriteEntity | undefined> {
        return this.repository.findOne({
            where: [{
                account,
                listing,
            }],
            relations: BAZA_NC_INTEGRATION_FAVORITE_RELATIONS,
        });
    }

    async deleteByAccountAndListing(account: AccountEntity, listing: BazaNcIntegrationListingsEntity): Promise<void> {
        await this.repository.delete({
            listing,
            account,
        });
    }

    async findAllFavoriteListing(account: AccountEntity): Promise<Array<BazaNcIntegrationFavoriteEntity>> {
        return this.repository.find({
            where: [{
                account,
            }],
            relations: BAZA_NC_INTEGRATION_FAVORITE_RELATIONS,
        });
    }
}
