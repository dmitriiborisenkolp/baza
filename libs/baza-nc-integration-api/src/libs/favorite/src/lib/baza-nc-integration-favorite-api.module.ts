import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaNcIntegrationFavoriteRepository } from './repositories/baza-nc-integration-favorite.repository';
import { BazaNcIntegrationFavoriteService } from './services/baza-nc-integration-favorite.service';
import { BazaNcIntegrationFavoriteController } from './controllers/baza-nc-integration-favorite.controller';

@Module({
    imports: [
        CqrsModule,
    ],
    controllers: [
        BazaNcIntegrationFavoriteController,
    ],
    providers: [
        BazaNcIntegrationFavoriteRepository,
        BazaNcIntegrationFavoriteService,
    ],
    exports: [
        BazaNcIntegrationFavoriteRepository,
        BazaNcIntegrationFavoriteService,
    ],
})
export class BazaNcIntegrationFavoriteApiModule {}
