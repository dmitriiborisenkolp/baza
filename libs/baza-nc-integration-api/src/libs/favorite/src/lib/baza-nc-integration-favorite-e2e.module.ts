import { forwardRef, Module } from '@nestjs/common';
import { BazaNcIntegrationFavoriteApiModule } from './baza-nc-integration-favorite-api.module';

const FIXTURES = [];

@Module({
    imports: [
        forwardRef(() => BazaNcIntegrationFavoriteApiModule),
    ],
    providers: [
        ...FIXTURES,
    ],
    exports: [
        ...FIXTURES,
    ],
})
export class BazaNcIntegrationFavoriteE2eModule {}
