import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationFavoriteEndpoint,
    BazaNcIntegrationFavoriteEndpointPaths,
    BazaNcIntegrationFavoriteExcludeRequest,
    BazaNcIntegrationFavoriteIncludeRequest,
    BazaNcIntegrationPublicOpenApi,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationFavoriteService } from '../services/baza-nc-integration-favorite.service';
import { AccountEntity, AuthGuard, ReqAccount } from '@scaliolabs/baza-core-api';

@Controller()
@ApiTags(BazaNcIntegrationPublicOpenApi.Favorite)
@UseGuards(AuthGuard)
export class BazaNcIntegrationFavoriteController implements BazaNcIntegrationFavoriteEndpoint {
    constructor(private readonly service: BazaNcIntegrationFavoriteService) {}

    @Post(BazaNcIntegrationFavoriteEndpointPaths.include)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'include',
    })
    @ApiOkResponse()
    async include(@Body() request: BazaNcIntegrationFavoriteIncludeRequest, @ReqAccount() account: AccountEntity): Promise<void> {
        await this.service.include(account, request);
    }

    @Post(BazaNcIntegrationFavoriteEndpointPaths.exclude)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'exclude',
    })
    @ApiOkResponse()
    async exclude(@Body() request: BazaNcIntegrationFavoriteExcludeRequest, @ReqAccount() account: AccountEntity): Promise<void> {
        await this.service.exclude(account, request);
    }
}
