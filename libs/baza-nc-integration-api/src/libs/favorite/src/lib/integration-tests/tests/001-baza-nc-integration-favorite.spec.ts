import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationFavoriteNodeAccess, BazaNcIntegrationListingsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/favorite/integration-tests/tests/001-baza-nc-integration-favorite.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessFavorite = new BazaNcIntegrationFavoriteNodeAccess(http);
    const dataAccessListing = new BazaNcIntegrationListingsNodeAccess(http);

    let listingId: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display there are no listings in favorites', async () => {
        const response = await dataAccessListing.list({});

        expect(response.items[0].isFavorite).toBeFalsy();
        expect(response.items[1].isFavorite).toBeFalsy();
        expect(response.items[2].isFavorite).toBeFalsy();

        listingId = response.items[0].id;
    });

    it('will display empty list for listing list request with isFavorite flag enabled', async () => {
        const response = await dataAccessListing.list({
            isFavorite: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will successfully mark listing as favorite', async () => {
        const response = await dataAccessFavorite.include({
            listingId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display listing marked as favorite', async () => {
        const response = await dataAccessListing.list({});

        expect(response.items[0].isFavorite).toBeTruthy();
        expect(response.items[0].dateAddedToFavorite).toBeDefined();
        expect(response.items[1].isFavorite).toBeFalsy();
        expect(response.items[2].isFavorite).toBeFalsy();
    });

    it('will display favorites in list request with isFavorite flag enabled [1]', async () => {
        const response = await dataAccessListing.list({
            isFavorite: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].id).toBe(listingId);
    });

    it('will successfully mark same listing as favorite again', async () => {
        const response = await dataAccessFavorite.include({
            listingId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display listing still marked as favorite', async () => {
        const response = await dataAccessListing.list({});

        expect(response.items[0].isFavorite).toBeTruthy();
        expect(response.items[0].dateAddedToFavorite).toBeDefined();
        expect(response.items[1].isFavorite).toBeFalsy();
        expect(response.items[2].isFavorite).toBeFalsy();
    });

    it('will display favorites in list request with isFavorite flag enabled [2]', async () => {
        const response = await dataAccessListing.list({
            isFavorite: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].id).toBe(listingId);
    });

    it('will successfully exclude listing as favorite', async () => {
        const response = await dataAccessFavorite.exclude({
            listingId,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display there are no listings in favorites', async () => {
        const response = await dataAccessListing.list({});

        expect(response.items[0].isFavorite).toBeFalsy();
        expect(response.items[0].dateAddedToFavorite).not.toBeDefined();
        expect(response.items[1].isFavorite).toBeFalsy();
        expect(response.items[2].isFavorite).toBeFalsy();

        listingId = response.items[0].id;
    });

    it('will display empty list for listing list request with isFavorite flag enabled after excluding listing from favorite', async () => {
        const response = await dataAccessListing.list({
            isFavorite: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });
});
