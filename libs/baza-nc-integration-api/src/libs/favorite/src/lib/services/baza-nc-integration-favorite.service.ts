import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { BazaNcIntegrationFavoriteRepository } from '../repositories/baza-nc-integration-favorite.repository';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationFavoriteExcludeRequest, BazaNcIntegrationFavoriteIncludeRequest } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationFavoriteEntity } from '../entities/baza-nc-integration-favorite.entity';
import { BazaNcIntegrationGetListingCommand } from '../../../../listings/src/lib/commands/baza-nc-integration-get-listing.command';

@Injectable()
export class BazaNcIntegrationFavoriteService {
    constructor(
        private readonly cqrs: CommandBus,
        private readonly repository: BazaNcIntegrationFavoriteRepository,
    ) {}

    async include(account: AccountEntity, request: BazaNcIntegrationFavoriteIncludeRequest): Promise<void> {
        const listing = await this.cqrs.execute<BazaNcIntegrationGetListingCommand>(
            new BazaNcIntegrationGetListingCommand(request.listingId),
        );

        const existing = await this.repository.findByAccountAndListing(account, listing);

        if (! existing) {
            const entity = new BazaNcIntegrationFavoriteEntity();

            entity.account = account;
            entity.listing = listing;

            await this.repository.save([entity]);
        }
    }

    async exclude(account: AccountEntity, request: BazaNcIntegrationFavoriteExcludeRequest): Promise<void> {
        const listing = await this.cqrs.execute<BazaNcIntegrationGetListingCommand>(
            new BazaNcIntegrationGetListingCommand(request.listingId),
        );

        await this.repository.deleteByAccountAndListing(account, listing);
    }
}
