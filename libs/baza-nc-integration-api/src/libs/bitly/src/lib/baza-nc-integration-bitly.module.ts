import { DynamicModule, Global, Module } from '@nestjs/common';
import { BazaNcIntegrationBitlyService } from './services/baza-nc-integration-bitly.service';
import { BITLY_MODULE_CONFIG, BitlyModuleConfig } from './baza-nc-integration-bitly.config';

interface AsyncOptions {
    injects: Array<any>;
    useFactory(...args: Array<any>): Promise<BitlyModuleConfig>;
}

@Module({})
export class BazaNcIntegrationBitlyModule
{
    static forRootAsync(options: AsyncOptions): DynamicModule {
        return {
            module: BitlyGlobalModule,
            providers: [
                {
                    provide: BITLY_MODULE_CONFIG,
                    inject: options.injects,
                    useFactory: options.useFactory,
                },
            ],
            exports: [
                BITLY_MODULE_CONFIG,
            ],
        };
    }
}

@Global()
@Module({
    providers: [
        BazaNcIntegrationBitlyService,
    ],
    exports: [
        BazaNcIntegrationBitlyService,
    ],
})
class BitlyGlobalModule {
}
