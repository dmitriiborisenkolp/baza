export interface BitlyModuleConfig {
    accessToken: string;
}

export const BITLY_MODULE_CONFIG = Symbol();
