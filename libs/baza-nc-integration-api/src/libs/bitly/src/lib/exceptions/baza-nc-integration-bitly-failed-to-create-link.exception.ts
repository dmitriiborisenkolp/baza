import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationBitlyErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationBitlyFailedToCreateLinkException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationBitlyErrorCodes.BitlyFailedToCreateLink, 'Failed to create link', HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
