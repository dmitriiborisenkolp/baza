import { HttpStatus } from '@nestjs/common';
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationBitlyErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationBitlyNoAccessTokenException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationBitlyErrorCodes.BitlyNoAccessToken, 'No access token set for bitly', HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
