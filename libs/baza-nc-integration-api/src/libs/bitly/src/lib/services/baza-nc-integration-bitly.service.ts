import { Injectable, Inject } from "@nestjs/common";
import { BitlyModuleConfig, BITLY_MODULE_CONFIG } from '../baza-nc-integration-bitly.config';
import { default as fetch } from 'node-fetch';
import { BazaNcIntegrationBitlyFailedToCreateLinkException } from '../exceptions/baza-nc-integration-bitly-failed-to-create-link.exception';

const RPC_CALL_URL = `https://api-ssl.bitly.com/v4/shorten`;

interface BitlyShortenRequest {
    group_guid?: string;
    domain?: string;
    long_url: string;
}

interface BitlyShortenResponse {
    id: string;
    link: string;
    long_url: string;
}

export interface ShortenRequest {
    originalUrl: string;
}

export interface ShortenResponse {
    originalUrl: string;
    shortenUrl: string;
}

@Injectable()
export class BazaNcIntegrationBitlyService
{
    constructor(
        @Inject(BITLY_MODULE_CONFIG) private readonly moduleConfig: BitlyModuleConfig,
    ) {}

    async shorten(request: ShortenRequest): Promise<ShortenResponse> {
        if (! request.originalUrl) {
            return {
                shortenUrl: undefined,
                originalUrl: undefined,
            };
        }

        if (! this.moduleConfig.accessToken) {
            return {
                shortenUrl: request.originalUrl,
                originalUrl: request.originalUrl,
            };
        } else {
            const rpcRequest: BitlyShortenRequest = {
                long_url: request.originalUrl,
            };

            const rpcResponse: BitlyShortenResponse = await (await fetch(RPC_CALL_URL, {
                method: 'post',
                headers: {
                    'Authorization': `Bearer ${this.moduleConfig.accessToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(rpcRequest),
            })).json() as BitlyShortenResponse;

            if (! rpcResponse) {
                throw new BazaNcIntegrationBitlyFailedToCreateLinkException();
            }

            return {
                originalUrl: request.originalUrl,
                shortenUrl: rpcResponse.link,
            };
        }
    }
}
