import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationSearchNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, CrudListSortOrder, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcSearchFields } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/search/integration-tests/tests/002-baza-nc-integration-search-case-b.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSearch = new BazaNcIntegrationSearchNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchAFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns correct ranges response', async () => {
        const response = await dataAccessSearch.ranges();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.status).toEqual({
            values: [BazaNcOfferingStatus.Open, BazaNcOfferingStatus.ComingSoon, BazaNcOfferingStatus.Closed],
        });

        expect(response.dateCreatedAt?.min).toBeDefined();
        expect(response.dateCreatedAt?.max).toBeDefined();

        expect(response.dateUpdatedAt?.min).toBeDefined();
        expect(response.dateUpdatedAt?.max).toBeDefined();

        const minDateCreatedAt = new Date(response.dateCreatedAt.min);
        const maxDateCreatedAt = new Date(response.dateCreatedAt.max);
        const minDateUpdatedAt = new Date(response.dateUpdatedAt.min);
        const maxDateUpdatedAt = new Date(response.dateUpdatedAt.max);

        expect(minDateCreatedAt.getTime()).toBeLessThan(maxDateCreatedAt.getTime());
        expect(minDateUpdatedAt.getTime()).toBeLessThan(maxDateUpdatedAt.getTime());
    });

    it('will sort listings by date created at (ASC)', async () => {
        const response = await dataAccessSearch.search({
            sort: [
                {
                    field: BazaNcSearchFields.DateCreatedAt,
                    order: CrudListSortOrder.Asc,
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 1');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 3');
    });

    it('will sort listings by date created at (DESC)', async () => {
        const response = await dataAccessSearch.search({
            sort: [
                {
                    field: BazaNcSearchFields.DateCreatedAt,
                    order: CrudListSortOrder.Desc,
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 1');
    });

    it('will sort listings by date updated at (ASC)', async () => {
        const response = await dataAccessSearch.search({
            sort: [
                {
                    field: BazaNcSearchFields.DateUpdatedAt,
                    order: CrudListSortOrder.Asc,
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 1');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 3');
    });

    it('will sort listings by date updated at (DESC)', async () => {
        const response = await dataAccessSearch.search({
            sort: [
                {
                    field: BazaNcSearchFields.DateUpdatedAt,
                    order: CrudListSortOrder.Desc,
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 1');
    });
});
