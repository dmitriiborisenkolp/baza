import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationSearchNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, CrudListSortOrder, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';
import { BazaNcSearchFields, BazaNcSearchOperator } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/search/integration-tests/tests/006-baza-nc-integration-search-case-c.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSearch = new BazaNcIntegrationSearchNodeAccess(http);
    const dataAccessListingCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessListing = new BazaNcIntegrationListingsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchBFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will correctly displays list of schemas in ranges call', async () => {
        const response = await dataAccessSearch.ranges();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.schemas.values).toEqual(['Schema A', 'Schema B']);
    });

    // Using Search API

    it('[USING-SEARCH-API] will search for schema with Schema A', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    field: BazaNcSearchFields.Schema,
                    value: 'Schema A',
                },
            ],
            sort: [
                {
                    field: BazaNcSearchFields.DateCreatedAt,
                    order: CrudListSortOrder.Asc,
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 1.1');
        expect(response.items[1].title).toBe('Example Listings 1.2');
        expect(response.items[2].title).toBe('Example Listings 1.3');
    });

    it('[USING-SEARCH-API] will search for schema with Schema B', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    field: BazaNcSearchFields.Schema,
                    value: 'Schema B',
                },
            ],
            sort: [
                {
                    field: BazaNcSearchFields.DateCreatedAt,
                    order: CrudListSortOrder.Asc,
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 2.1');
        expect(response.items[1].title).toBe('Example Listings 2.2');
        expect(response.items[2].title).toBe('Example Listings 2.3');
    });

    it('[USING-SEARCH-API] will search for listings w/o Schema', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.NotExists,
                    field: BazaNcSearchFields.Schema,
                },
            ],
            sort: [
                {
                    field: BazaNcSearchFields.DateCreatedAt,
                    order: CrudListSortOrder.Asc,
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Example Listings 3.1');
        expect(response.items[1].title).toBe('Example Listings 3.2');
    });

    // Using Listings CMS API

    it('[USING-LISTINGS_CMS-API] will search for schema with Schema A', async () => {
        const response = await dataAccessListingCms.list({
            schemas: ['Schema A'],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[2].title).toBe('Example Listings 1.1');
        expect(response.items[1].title).toBe('Example Listings 1.2');
        expect(response.items[0].title).toBe('Example Listings 1.3');
    });

    it('[USING-LISTINGS_CMS-API] will search for schema with Schema B', async () => {
        const response = await dataAccessListingCms.list({
            schemas: ['Schema B'],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[2].title).toBe('Example Listings 2.1');
        expect(response.items[1].title).toBe('Example Listings 2.2');
        expect(response.items[0].title).toBe('Example Listings 2.3');
    });

    it('[USING-LISTINGS_CMS-API] will search for schema both with Schema A and Schema B', async () => {
        const response = await dataAccessListingCms.list({
            schemas: ['Schema A', 'Schema B'],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(6);
        expect(response.items[0].title).toBe('Example Listings 2.3');
        expect(response.items[1].title).toBe('Example Listings 2.2');
        expect(response.items[2].title).toBe('Example Listings 2.1');
        expect(response.items[3].title).toBe('Example Listings 1.3');
        expect(response.items[4].title).toBe('Example Listings 1.2');
        expect(response.items[5].title).toBe('Example Listings 1.1');
    });

    // Using Listings API

    it('[USING-LISTINGS-API] will search for schema with Schema A', async () => {
        const response = await dataAccessListing.list({
            schemas: ['Schema A'],
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[2].title).toBe('Example Listings 1.1');
        expect(response.items[1].title).toBe('Example Listings 1.2');
        expect(response.items[0].title).toBe('Example Listings 1.3');
    });

    it('[USING-LISTINGS-API] will search for schema with Schema B', async () => {
        const response = await dataAccessListing.list({
            schemas: ['Schema B'],
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[2].title).toBe('Example Listings 2.1');
        expect(response.items[1].title).toBe('Example Listings 2.2');
        expect(response.items[0].title).toBe('Example Listings 2.3');
    });

    it('[USING-LISTINGS-API] will search for schema both with Schema A and Schema B', async () => {
        const response = await dataAccessListing.list({
            schemas: ['Schema A', 'Schema B'],
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(6);
        expect(response.items[0].title).toBe('Example Listings 2.3');
        expect(response.items[1].title).toBe('Example Listings 2.2');
        expect(response.items[2].title).toBe('Example Listings 2.1');
        expect(response.items[3].title).toBe('Example Listings 1.3');
        expect(response.items[4].title).toBe('Example Listings 1.2');
        expect(response.items[5].title).toBe('Example Listings 1.1');
    });
});
