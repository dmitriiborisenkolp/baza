import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationSearchCmsNodeAccess, BazaNcIntegrationSearchNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcSearchOperator } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/search/integration-tests/tests/005-baza-nc-integration-search-cms.endpoint.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSearch = new BazaNcIntegrationSearchNodeAccess(http);
    const dataAccessSearchCms = new BazaNcIntegrationSearchCmsNodeAccess(http);

    let listingUlid: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchAFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will search for Listing with "Athena" text field value', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    value: 'Athena',
                    field: 'text',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings 2');
        expect(response.items[0].status).toBe(BazaNcOfferingStatus.ComingSoon);
        expect(response.items[0].percentFunded).toBe(0);

        listingUlid = response.items[0].sid;
    });

    it('will reindex Listing', async () => {
        const response = await dataAccessSearchCms.reindex(listingUlid);

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will reindex all Listings', async () => {
        const response = await dataAccessSearchCms.reindexAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });
});
