import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess, BazaNcIntegrationSearchNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';
import { AccountTypeCheckingSaving, BazaNcOfferingStatus, BazaNcPurchaseFlowTransactionType, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcSearchOperator } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/search/integration-tests/tests/004-baza-nc-integration-search-update-fund-percents.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSearch = new BazaNcIntegrationSearchNodeAccess(http);
    const dataAccessListingCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessPurchaseFlowSession = new BazaNcPurchaseFlowNodeAccess(http);

    let listingId: number;
    let offeringId: OfferingId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchAFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will search for Listing with "Athena" text field value', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    value: 'Athena',
                    field: 'text',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings 2');
        expect(response.items[0].status).toBe(BazaNcOfferingStatus.ComingSoon);
        expect(response.items[0].percentFunded).toBe(0);

        listingId = response.items[0].id;
        offeringId = response.items[0].offeringId;
    });

    it('will update offering status', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessListingCms.changeStatus({
            id: listingId,
            newStatus: BazaNcOfferingStatus.Open,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will successfully purchase share', async () => {
        await asyncExpect(
            async () => {
                const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
                    accountName: 'Foo Bar',
                    accountRoutingNumber: '011401533',
                    accountNumber: '1111222233330000',
                    accountType: AccountTypeCheckingSaving.Savings,
                });

                expect(isBazaErrorResponse(setBankAccountResponse)).toBeFalsy();
            },
            null,
            {
                intervalMillis: 2000,
            },
        );

        const sessionResponse = await asyncExpect(
            async () => {
                const sessionResponse = await dataAccessPurchaseFlowSession.session({
                    offeringId,
                    amount: 2500,
                    numberOfShares: 1,
                    transactionType: BazaNcPurchaseFlowTransactionType.ACH,
                });

                expect(isBazaErrorResponse(sessionResponse)).toBeFalsy();

                return sessionResponse;
            },
            null,
            { intervalMillis: 2000 },
        );

        await asyncExpect(
            async () => {
                const submitResponse = await dataAccessPurchaseFlowSession.submit({
                    id: sessionResponse.id,
                });

                expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will update status and percents funded in Listing Search response', async () => {
        const response = await asyncExpect(
            async () => {
                const response = await dataAccessSearch.search({
                    filter: [
                        {
                            type: BazaNcSearchOperator.Equal,
                            value: 'Athena',
                            field: 'text',
                        },
                    ],
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                return response;
            },
            (response) =>
                response.items.length === 1 &&
                response.items[0].title === 'Example Listings 2' &&
                response.items[0].status === BazaNcOfferingStatus.Open &&
                response.items[0].percentFunded === 25,
            {
                intervalMillis: 2000,
            },
        );

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings 2');
        expect(response.items[0].status).toBe(BazaNcOfferingStatus.Open);
        expect(response.items[0].percentFunded).toBe(25);
    });

    it('will successfully purchase rest of shares', async () => {
        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse)).toBeFalsy();

        const sessionResponse = await dataAccessPurchaseFlowSession.session({
            offeringId,
            amount: 7500,
            numberOfShares: 3,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(sessionResponse)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlowSession.submit({
            id: sessionResponse.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will update status and percents funded in Listing Search response', async () => {
        const response = await asyncExpect(
            async () => {
                const response = await dataAccessSearch.search({
                    filter: [
                        {
                            type: BazaNcSearchOperator.Equal,
                            value: 'Athena',
                            field: 'text',
                        },
                    ],
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                return response;
            },
            (response) =>
                response.items.length === 1 &&
                response.items[0].title === 'Example Listings 2' &&
                response.items[0].status === BazaNcOfferingStatus.Closed &&
                response.items[0].percentFunded === 100,
            { intervalMillis: 2000 },
        );

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings 2');
        expect(response.items[0].status).toBe(BazaNcOfferingStatus.Closed);
        expect(response.items[0].percentFunded).toBe(100);
    });
});
