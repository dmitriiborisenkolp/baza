import { Injectable } from '@nestjs/common';
import { BazaAccountCmsService, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaService } from '../../../../../schema/src';
import { BazaNcIntegrationListingsCmsService } from '../../../../../listings/src';
import {
    BazaNcIntegrationListingsCmsCreateRequest,
    BazaNcIntegrationSchemaCmsCreateRequest,
    BazaNcIntegrationSchemaType,
} from '@scaliolabs/baza-nc-integration-shared';
import { e2eExampleAttachment } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';

const FIXTURE: Array<{
    schema: BazaNcIntegrationSchemaCmsCreateRequest;
    listings: Array<BazaNcIntegrationListingsCmsCreateRequest>;
}> = [
    {
        schema: {
            title: 'Schema A',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'text',
                        title: 'Example Text Field',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Select,
                    payload: {
                        field: 'select',
                        title: 'Example Select Field',
                        required: false,
                        isDisplayedInDetails: true,
                        values: {
                            foo: 'Foo',
                            bar: 'Bar',
                            baz: 'Baz',
                        },
                    },
                },
            ],
        },
        listings: [
            {
                isPublished: true,
                title: 'Example Listings 1',
                description: 'Example Listings Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 1,
                offering: {
                    ncTargetAmount: 1000,
                    ncMinAmount: 10,
                    ncMaxAmount: 1000,
                    ncCurrentValueCents: 1000,
                    ncMaxSharesPerAccount: 10,
                    ncUnitPrice: 10,
                    ncStartDate: '01-01-2021',
                    ncEndDate: '01-01-2022',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Poseidon',
                    select: 'Foo',
                },
                statements: [],
            },
            {
                isPublished: false,
                title: 'Example Listings 2',
                description: 'Example Listings Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 1,
                offering: {
                    ncTargetAmount: 100,
                    ncMinAmount: 10,
                    ncMaxAmount: 100,
                    ncCurrentValueCents: 100,
                    ncMaxSharesPerAccount: 15,
                    ncUnitPrice: 25,
                    ncStartDate: '02-03-2020',
                    ncEndDate: '02-05-2023',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Athena',
                    select: 'Bar',
                },
                statements: [],
            },
            {
                isPublished: true,
                title: 'Example Listings 3',
                description: 'Example Listings Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 10,
                offering: {
                    ncTargetAmount: 5000,
                    ncMinAmount: 150,
                    ncMaxAmount: 5000,
                    ncCurrentValueCents: 5000,
                    ncMaxSharesPerAccount: 5,
                    ncUnitPrice: 50,
                    ncStartDate: '02-01-2021',
                    ncEndDate: '10-01-2023',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Hermes',
                },
                statements: [],
            },
        ],
    },
];

@Injectable()
export class BazaNcIntegrationSearchDFixture implements BazaE2eFixture {
    constructor(
        private readonly account: BazaAccountCmsService,
        private readonly schema: BazaNcIntegrationSchemaService,
        private readonly listings: BazaNcIntegrationListingsCmsService,
    ) {}

    async up(): Promise<void> {
        const account = (await this.account.listAdminAccounts())[0];

        for (const fixtureRequest of FIXTURE) {
            const schema = await this.schema.create(fixtureRequest.schema);

            for (const listingFixtureRequest of fixtureRequest.listings) {
                await this.listings.create(account, {
                    ...listingFixtureRequest,
                    schemaId: schema.id,
                });
            }
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchDFixture,
        provider: BazaNcIntegrationSearchDFixture,
    },
]);
