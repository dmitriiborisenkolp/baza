import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationSearchCmsNodeAccess, BazaNcIntegrationSearchNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { awaitTimeout, BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/search/integration-tests/tests/001-baza-nc-integration-search-case-a.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSearch = new BazaNcIntegrationSearchNodeAccess(http);
    const dataAccessSearchCms = new BazaNcIntegrationSearchCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchDFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('[BEFORE-REINDEX] will not returns unpublished listings', async () => {
        const response = await dataAccessSearch.search({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.pager.total).toBe(2);

        expect(response.items[0].title).toBe('Example Listings 1');
        expect(response.items[1].title).toBe('Example Listings 3');
    });

    it('will returns correct ranges response', async () => {
        const response = await dataAccessSearch.ranges();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.status).toEqual({
            values: [BazaNcOfferingStatus.Open, BazaNcOfferingStatus.ComingSoon, BazaNcOfferingStatus.Closed],
        });

        expect(response.pricePerShareCents.min).toBe(1000);
        expect(response.pricePerShareCents.max).toBe(5000);
        expect(response.currentValueCents.min).toBe(1000);
        expect(response.currentValueCents.max).toBe(5000);
        expect(response.numSharesAvailable.min).toBe(100);
        expect(response.numSharesAvailable.max).toBe(100);
        expect(response.percentFunded.min).toBe(0);
        expect(response.percentFunded.max).toBe(0);
        expect(response.totalSharesValueCents.min).toBe(100000);
        expect(response.totalSharesValueCents.max).toBe(500000);
        expect(response.numSharesMin.min).toBe(1);
        expect(response.numSharesMin.max).toBe(3);
        expect(response.sharesToJoinEliteInvestors.min).toBe(1);
        expect(response.sharesToJoinEliteInvestors.max).toBe(10);

        expect(response.select).toEqual({
            values: ['Foo', 'Bar', 'Baz'],
        });
    });

    it('will successfully reindex listings', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessSearchCms.reindexAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[AFTER-REINDEX] will not returns unpublished listings', async () => {
        await awaitTimeout(3000);

        await asyncExpect(async () => {
            const response = await dataAccessSearch.search({});

            expect(isBazaErrorResponse(response)).toBeFalsy();

            expect(response.items.length).toBe(2);
            expect(response.pager.total).toBe(2);

            expect(response.items[0].title).toBe('Example Listings 1');
            expect(response.items[1].title).toBe('Example Listings 3');
        });
    });
});
