import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationSearchNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcSearchOperator } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/search/integration-tests/tests/001-baza-nc-integration-search-case-a.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSearch = new BazaNcIntegrationSearchNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchAFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns correct ranges response', async () => {
        const response = await dataAccessSearch.ranges();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.status).toEqual({
            values: [BazaNcOfferingStatus.Open, BazaNcOfferingStatus.ComingSoon, BazaNcOfferingStatus.Closed],
        });

        expect(response.pricePerShareCents.min).toBe(1000);
        expect(response.pricePerShareCents.max).toBe(5000);
        expect(response.currentValueCents.min).toBe(100);
        expect(response.currentValueCents.max).toBe(5000);
        expect(response.numSharesAvailable.min).toBe(4);
        expect(response.numSharesAvailable.max).toBe(100);
        expect(response.percentFunded.min).toBe(0);
        expect(response.percentFunded.max).toBe(0);
        expect(response.totalSharesValueCents.min).toBe(10000);
        expect(response.totalSharesValueCents.max).toBe(500000);
        expect(response.numSharesMin.min).toBe(1);
        expect(response.numSharesMin.max).toBe(3);
        expect(response.sharesToJoinEliteInvestors.min).toBe(1);
        expect(response.sharesToJoinEliteInvestors.max).toBe(10);

        expect(response.select).toEqual({
            values: ['Foo', 'Bar', 'Baz'],
        });
    });

    it('will search for Listing with "Athena" text field value', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    value: 'Athena',
                    field: 'text',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings 2');
    });

    it('will search for Listing for "Foo" select field value', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    value: 'Foo',
                    field: 'select',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings 1');
    });
});
