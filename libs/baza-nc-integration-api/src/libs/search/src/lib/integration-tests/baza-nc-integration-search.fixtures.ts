export enum BazaNcIntegrationSearchFixtures {
    BazaNcIntegrationSearchAFixture = 'BazaNcIntegrationSearchAFixture',
    BazaNcIntegrationSearchBFixture = 'BazaNcIntegrationSearchBFixture',
    BazaNcIntegrationSearchCFixture = 'BazaNcIntegrationSearchCFixture',
    BazaNcIntegrationSearchDFixture = 'BazaNcIntegrationSearchDFixture',
}
