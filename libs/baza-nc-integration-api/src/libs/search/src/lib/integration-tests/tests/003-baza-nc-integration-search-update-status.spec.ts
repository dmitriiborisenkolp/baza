import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess, BazaNcIntegrationSearchNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';
import { BazaNcOfferingStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcSearchOperator } from '@scaliolabs/baza-nc-integration-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/search/integration-tests/tests/003-baza-nc-integration-search-update-status.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSearch = new BazaNcIntegrationSearchNodeAccess(http);
    const dataAccessListingCms = new BazaNcIntegrationListingsCmsNodeAccess(http);

    let listingId: number;
    let offeringId: OfferingId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchAFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will search for Listing with "Athena" text field value', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    value: 'Athena',
                    field: 'text',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings 2');
        expect(response.items[0].status).toBe(BazaNcOfferingStatus.ComingSoon);

        listingId = response.items[0].id;
        offeringId = response.items[0].offeringId;
    });

    it('will update offering status', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessListingCms.changeStatus({
            id: listingId,
            newStatus: BazaNcOfferingStatus.Open,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will update status in Listing Search response', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessSearch.search({
                    filter: [
                        {
                            type: BazaNcSearchOperator.Equal,
                            value: 'Athena',
                            field: 'text',
                        },
                    ],
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.items[0].title).toBe('Example Listings 2');
                expect(response.items[0].status).toBe(BazaNcOfferingStatus.Open);
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
