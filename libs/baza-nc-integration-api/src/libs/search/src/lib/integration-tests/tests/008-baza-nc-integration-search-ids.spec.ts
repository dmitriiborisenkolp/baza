import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationSearchCmsNodeAccess, BazaNcIntegrationSearchNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFixtures } from '../baza-nc-integration-search.fixtures';
import { BazaNcSearchFields, BazaNcSearchOperator } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/search/integration-tests/tests/008-baza-nc-integration-search-ids.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessSearch = new BazaNcIntegrationSearchNodeAccess(http);
    const dataAccessSearchCms = new BazaNcIntegrationSearchCmsNodeAccess(http);

    let LISTING_SIDS: Array<string>;
    let LISTING_OFFERING_IDS: Array<string>;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationSearchFixtures.BazaNcIntegrationSearchCFixture],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns all Listings', async () => {
        const response = await dataAccessSearch.search({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(8);

        LISTING_SIDS = [response.items[0].sid, response.items[1].sid];
        LISTING_OFFERING_IDS = [response.items[0].offeringId, response.items[1].offeringId];
    });

    it('will returns Listings by Schema A', async () => {
        const response = await dataAccessSearch.search({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    field: BazaNcSearchFields.Schema,
                    value: 'Schema A',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);

        expect(response.items[0].title).toBe('Example Listings 1.1');
        expect(response.items[1].title).toBe('Example Listings 1.2');
        expect(response.items[2].title).toBe('Example Listings 1.3');
    });

    it('will returns all Listing Offering IDs', async () => {
        const response = await dataAccessSearch.searchOfferingIds({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(8);
        expect(response.includes(LISTING_OFFERING_IDS[0])).toBeTruthy();
        expect(response.includes(LISTING_OFFERING_IDS[1])).toBeTruthy();
    });

    it('will returns all Listing Offering ULIDs', async () => {
        const response = await dataAccessSearch.searchULIDs({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(8);
        expect(response.includes(LISTING_SIDS[0])).toBeTruthy();
        expect(response.includes(LISTING_SIDS[1])).toBeTruthy();
    });

    it('will filter Listing Offering IDs by Scheme A', async () => {
        const response = await dataAccessSearch.searchOfferingIds({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    field: BazaNcSearchFields.Schema,
                    value: 'Schema A',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(3);
        expect(response.includes(LISTING_OFFERING_IDS[0])).toBeTruthy();
        expect(response.includes(LISTING_OFFERING_IDS[1])).toBeTruthy();
    });

    it('will filter Listing Listing UIDs by Scheme A', async () => {
        const response = await dataAccessSearch.searchULIDs({
            filter: [
                {
                    type: BazaNcSearchOperator.Equal,
                    field: BazaNcSearchFields.Schema,
                    value: 'Schema A',
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(3);
        expect(response.includes(LISTING_SIDS[0])).toBeTruthy();
        expect(response.includes(LISTING_SIDS[1])).toBeTruthy();
    });
});
