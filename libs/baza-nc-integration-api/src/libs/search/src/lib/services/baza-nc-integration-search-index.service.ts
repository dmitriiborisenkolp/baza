import { Inject, Injectable } from '@nestjs/common';
import {
    BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
    BazaNcIntegrationSearchIndexHandler,
} from '../strategies/baza-nc-integration-search-strategy';
import { BazaNcIntegrationListingsRepository } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationSearchIndexService {
    constructor(
        @Inject(BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER) private readonly searchIndexHandler: BazaNcIntegrationSearchIndexHandler,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
    ) {}

    /**
     * Reindex all Listings
     */
    async reindex(): Promise<void> {
        await this.searchIndexHandler.reindex();
    }

    /**
     * Updates Listing in Search Index
     * @param listingUlid
     */
    async putListing(listingUlid: string): Promise<void> {
        const listing = await this.listingRepository.findByUlid(listingUlid);

        if (!listing || !listing.isPublished) {
            await this.searchIndexHandler.deleteListing(listingUlid);
        } else {
            await this.searchIndexHandler.putListing(listingUlid);
        }
    }

    /**
     * Removes Listing from Search Index
     * @param listingUlid
     */
    async deleteListing(listingUlid: string): Promise<void> {
        await this.searchIndexHandler.deleteListing(listingUlid);
    }
}
