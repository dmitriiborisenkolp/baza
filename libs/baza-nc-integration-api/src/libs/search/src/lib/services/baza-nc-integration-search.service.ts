import { Inject, Injectable } from '@nestjs/common';
import { BAZA_NC_INTEGRATION_SEARCH_HANDLER, BazaNcIntegrationSearchHandler } from '../strategies/baza-nc-integration-search-strategy';
import {
    BazaNcSearchRangesResponseDto,
    BazaNcSearchRequestBaseDto,
    BazaNcSearchRequestDto,
    BazaNcSearchResponseDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsMapper, BazaNcIntegrationListingsRepository } from '../../../../listings/src';
import { AccountEntity } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcIntegrationSearchService {
    constructor(
        @Inject(BAZA_NC_INTEGRATION_SEARCH_HANDLER) private readonly searchHandler: BazaNcIntegrationSearchHandler,
        private readonly repository: BazaNcIntegrationListingsRepository,
        private readonly mapper: BazaNcIntegrationListingsMapper,
    ) {}

    /**
     * Search Listings with given request
     * Additionally it will execute Mapper once again in order to update Image URLs
     * @param request
     * @param viewer
     */
    async search(request: BazaNcSearchRequestDto, viewer?: AccountEntity): Promise<BazaNcSearchResponseDto> {
        const result = await this.searchHandler.search(request);
        const resultIds = result.items.map((next) => next.id);
        const entities = await this.repository.findByIds(resultIds);

        result.items = await this.mapper.entitiesToDTOs(
            resultIds.map((id) => entities.find((next) => next.id === id)).filter((next) => !!next),
            viewer,
        );

        return result;
    }

    /**
     * Search Listings with given request
     * Returns Array of Listing ULIDs only
     * Additionally it will execute Mapper once again in order to update Image URLs
     * @param request
     * @param viewer
     */
    async searchULIDs(request: BazaNcSearchRequestBaseDto, viewer?: AccountEntity): Promise<Array<string>> {
        const result = await this.search({ ...request, size: -1, index: 1 }, viewer);

        return result.items.map((next) => next.sid);
    }

    /**
     * Search Listings with given request
     * Returns Array of Offering IDs only
     * Additionally it will execute Mapper once again in order to update Image URLs
     * @param request
     * @param viewer
     */
    async searchOfferingIds(request: BazaNcSearchRequestBaseDto, viewer?: AccountEntity): Promise<Array<string>> {
        const result = await this.search({ ...request, size: -1, index: 1 }, viewer);

        return result.items.map((next) => next.offeringId);
    }

    /**
     * Returns Ranges data for Search
     */
    async ranges(): Promise<BazaNcSearchRangesResponseDto> {
        return this.searchHandler.ranges();
    }
}
