import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationSearchIndexService } from './baza-nc-integration-search-index.service';

/**
 * Search CMS Service
 */
@Injectable()
export class BazaNcIntegrationSearchCmsService {
    constructor(private readonly service: BazaNcIntegrationSearchIndexService) {}

    /**
     * Reindex Listing in Search Index
     * @param ulid
     */
    async reindex(ulid: string): Promise<void> {
        await this.service.putListing(ulid);
    }

    /**
     * Reindex all Listings in Search Index
     */
    async reindexAll(): Promise<void> {
        await this.service.reindex();
    }
}
