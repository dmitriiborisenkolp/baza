import { Injectable } from '@nestjs/common';
import {
    BazaNcIntegrationListingsDto,
    bazaNcIntegrationSchemaNumberTypes,
    BazaNcIntegrationSchemaType,
    BazaNcRangesDate,
    BazaNcRangesEnum,
    BazaNcRangesNumber,
    bazaNcSchemaDateTypes,
    bazaNcSchemaEnumTypes,
    BazaNcSearchOperator,
    BazaNcSearchRangesResponseDto,
    BazaNcSearchRequestDto,
    BazaNcSearchResponseDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { BAZA_CRUD_DEFAULT_SIZE } from '@scaliolabs/baza-core-api';
import { CrudListSortOrder, isNotNullOrUndefined, naturalCompare, paginate } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSearchFieldUnknownOperatorException } from '../exceptions/baza-nc-integration-search-field-unknown-operator.exception';
import { BazaNcIntegrationSearchFieldType } from '../models/baza-nc-integration-search-field-type';
import { BAZA_NC_INTEGRATION_SEARCH_STATUS_WEIGHTS } from '../models/baza-nc-integration-search-status-weights';
import { $value } from '../util/$value';
import { $eq } from '../util/$eq';
import { $exists } from '../util/$exists';
import { $in } from '../util/$in';
import { $lt } from '../util/$lt';
import { $gt } from '../util/$gt';
import { $lte } from '../util/$lte';
import { $gte } from '../util/$gte';
import * as _ from 'lodash';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationSchemaRepository } from '../../../../schema/src';
import { $null } from '../util/$null';

@Injectable()
export class BazaNcIntegrationSearchListingDtoService {
    constructor(private readonly schemaRepository: BazaNcIntegrationSchemaRepository) {}

    async ranges(listings: Array<BazaNcIntegrationListingsDto>): Promise<BazaNcSearchRangesResponseDto> {
        if (!listings.length) {
            return {
                status: {
                    values: Object.values(BazaNcOfferingStatus),
                },
                dateCreatedAt: {
                    min: new Date().toISOString(),
                    max: new Date().toISOString(),
                },
                dateUpdatedAt: {
                    min: new Date().toISOString(),
                    max: new Date().toISOString(),
                },
                pricePerShareCents: {
                    min: 0,
                    max: 0,
                },
                currentValueCents: {
                    min: 0,
                    max: 0,
                },
                numSharesAvailable: {
                    min: 0,
                    max: 0,
                },
                percentFunded: {
                    min: 0,
                    max: 0,
                },
                totalSharesValueCents: {
                    min: 0,
                    max: 0,
                },
                numSharesMin: {
                    min: 0,
                    max: 0,
                },
                sharesToJoinEliteInvestors: {
                    min: 0,
                    max: 0,
                },
                startDate: {
                    min: new Date().toISOString(),
                    max: new Date().toISOString(),
                },
                endDate: {
                    min: new Date().toISOString(),
                    max: new Date().toISOString(),
                },
                schemas: {
                    values: [],
                },
            };
        } else {
            const result: BazaNcSearchRangesResponseDto = {
                status: {
                    values: Object.values(BazaNcOfferingStatus),
                },
                dateCreatedAt: {
                    min: new Date(_.min(listings.map((listing) => new Date(listing.dateCreatedAt).getTime()))).toISOString(),
                    max: new Date(_.max(listings.map((listing) => new Date(listing.dateCreatedAt).getTime()))).toISOString(),
                },
                dateUpdatedAt: {
                    min: new Date(
                        _.min(listings.map((listing) => new Date(listing.dateUpdatedAt || listing.dateCreatedAt).getTime())),
                    ).toISOString(),
                    max: new Date(
                        _.max(listings.map((listing) => new Date(listing.dateUpdatedAt || listing.dateCreatedAt).getTime())),
                    ).toISOString(),
                },
                pricePerShareCents: {
                    min: _.min(listings.map((listing) => listing.pricePerShareCents)),
                    max: _.max(listings.map((listing) => listing.pricePerShareCents)),
                },
                currentValueCents: {
                    min: _.min(listings.map((listing) => listing.offering.ncCurrentValueCents)),
                    max: _.max(listings.map((listing) => listing.offering.ncCurrentValueCents)),
                },
                numSharesAvailable: {
                    min: _.min(listings.map((listing) => listing.numSharesAvailable)),
                    max: _.max(listings.map((listing) => listing.numSharesAvailable)),
                },
                percentFunded: {
                    min: _.min(listings.map((listing) => listing.percentFunded)),
                    max: _.max(listings.map((listing) => listing.percentFunded)),
                },
                totalSharesValueCents: {
                    min: _.min(listings.map((listing) => listing.totalSharesValueCents)),
                    max: _.max(listings.map((listing) => listing.totalSharesValueCents)),
                },
                numSharesMin: {
                    min: _.min(listings.map((listing) => listing.numSharesMin)),
                    max: _.max(listings.map((listing) => listing.numSharesMin)),
                },
                sharesToJoinEliteInvestors: {
                    min: _.min(listings.map((listing) => listing.sharesToJoinEliteInvestors)),
                    max: _.max(listings.map((listing) => listing.sharesToJoinEliteInvestors)),
                },
                startDate: {
                    min: new Date(_.min(listings.map((listing) => new Date(listing.offering.ncStartDate).getTime()))).toISOString(),
                    max: new Date(_.max(listings.map((listing) => new Date(listing.offering.ncStartDate).getTime()))).toISOString(),
                },
                endDate: {
                    min: new Date(_.min(listings.map((listing) => new Date(listing.offering.ncEndDate).getTime()))).toISOString(),
                    max: new Date(_.max(listings.map((listing) => new Date(listing.offering.ncEndDate).getTime()))).toISOString(),
                },
                schemas: {
                    values: _.uniq(listings.filter((next) => !!next.schema).map((next) => next.schema.title)),
                },
            };

            const listingsWithSchema = listings.filter((next) => !!next.schema);

            for (const listing of listingsWithSchema) {
                const schema = await this.schemaRepository.findById(listing.schema.id);

                if (!schema) {
                    continue;
                }

                for (const schemaDefinition of schema.definitions) {
                    const field = schemaDefinition.payload.field;
                    const value = listing.properties[schemaDefinition.payload.field];

                    if (!isNotNullOrUndefined(value)) {
                        continue;
                    }

                    if (bazaNcIntegrationSchemaNumberTypes.includes(schemaDefinition.type)) {
                        if (!result[field]) {
                            result[field] = {
                                min: value,
                                max: value,
                            } as BazaNcRangesNumber;
                        } else {
                            (result[field] as BazaNcRangesNumber).min = Math.min((result[field] as BazaNcRangesNumber).min, value);
                            (result[field] as BazaNcRangesNumber).max = Math.max((result[field] as BazaNcRangesNumber).max, value);
                        }
                    } else if (bazaNcSchemaDateTypes.includes(schemaDefinition.type)) {
                        if (!result[field]) {
                            result[field] = {
                                min: value,
                                max: value,
                            } as BazaNcRangesDate;
                        } else {
                            const sourceDateMin = new Date((result[field] as BazaNcRangesDate).min).getTime();
                            const sourceDateMax = new Date((result[field] as BazaNcRangesDate).max).getTime();

                            const valueDate = new Date(value).getTime();

                            (result[field] as BazaNcRangesDate).min = new Date(Math.min(sourceDateMin, valueDate)).toISOString();
                            (result[field] as BazaNcRangesDate).max = new Date(Math.max(sourceDateMax, valueDate)).toISOString();
                        }
                    } else if (bazaNcSchemaEnumTypes.includes(schemaDefinition.type)) {
                        if (!result[field]) {
                            switch (schemaDefinition.type) {
                                case BazaNcIntegrationSchemaType.Select: {
                                    result[field] = {
                                        values: Object.values(schemaDefinition.payload.values),
                                    } as BazaNcRangesEnum;
                                }
                            }
                        }
                    }
                }
            }

            return result;
        }
    }

    search(request: BazaNcSearchRequestDto, listings: Array<BazaNcIntegrationListingsDto>): BazaNcSearchResponseDto {
        const filtered = this.applyFilters(request, listings);

        this.applySort(request, filtered);

        const size = request.size || BAZA_CRUD_DEFAULT_SIZE;
        const index = Math.max(1, request.index || 1);

        return {
            pager: {
                size,
                index,
                total: filtered.length,
            },
            items: paginate(filtered, size * Math.max(index - 1, 0), size),
        };
    }

    private applyFilters(
        request: BazaNcSearchRequestDto,
        listings: Array<BazaNcIntegrationListingsDto>,
    ): Array<BazaNcIntegrationListingsDto> {
        if (!(Array.isArray(request.filter) && request.filter.length > 0)) {
            return listings;
        }

        let filtered = listings;

        for (const filterRequest of request.filter) {
            filtered = filtered.filter((listing) => {
                const operator = filterRequest.type;
                const fieldValue = $value(listing, filterRequest.field);

                switch (filterRequest.type) {
                    default: {
                        throw new BazaNcIntegrationSearchFieldUnknownOperatorException(operator);
                    }

                    case BazaNcSearchOperator.Equal: {
                        return $eq(fieldValue, filterRequest.value);
                    }

                    case BazaNcSearchOperator.NotEqual: {
                        return !$eq(fieldValue, filterRequest.value);
                    }

                    case BazaNcSearchOperator.Exists: {
                        return $exists(fieldValue);
                    }

                    case BazaNcSearchOperator.NotExists: {
                        return $null(fieldValue);
                    }

                    case BazaNcSearchOperator.In: {
                        return $in(fieldValue, filterRequest.values);
                    }

                    case BazaNcSearchOperator.NotIn: {
                        return !$in(fieldValue, filterRequest.values);
                    }

                    case BazaNcSearchOperator.LessThan: {
                        return $lt(fieldValue, filterRequest.value);
                    }

                    case BazaNcSearchOperator.LessThanOrEqual: {
                        return $lte(fieldValue, filterRequest.value);
                    }

                    case BazaNcSearchOperator.GreaterThan: {
                        return $gt(fieldValue, filterRequest.value);
                    }

                    case BazaNcSearchOperator.GreaterThanOrEqual: {
                        return $gte(fieldValue, filterRequest.value);
                    }
                }
            });
        }

        return filtered;
    }

    private applySort(request: BazaNcSearchRequestDto, filtered: Array<BazaNcIntegrationListingsDto>): void {
        if (!(Array.isArray(request.sort) && request.sort.length > 0)) {
            return;
        }

        for (const sortRequest of request.sort) {
            const multiplier = sortRequest.order === CrudListSortOrder.Asc ? 1 : -1;

            filtered.sort((a, b) => {
                const valueA = $value(a, sortRequest.field);
                const valueB = $value(b, sortRequest.field);

                if (valueA.type !== valueB.type) {
                    return 0;
                }

                const isValueANull = !isNotNullOrUndefined(valueA.value);
                const isValueBNull = !isNotNullOrUndefined(valueB.value);

                if (isValueANull && isValueBNull) {
                    return 0;
                } else if (isValueANull && !isValueBNull) {
                    return -1 * multiplier;
                } else if (!isValueANull && isValueBNull) {
                    return multiplier;
                }

                switch (valueA.type) {
                    default: {
                        return 0;
                    }

                    case BazaNcIntegrationSearchFieldType.Number:
                    case BazaNcIntegrationSearchFieldType.Boolean: {
                        return multiplier * (+valueA.value - +valueB.value);
                    }

                    case BazaNcIntegrationSearchFieldType.String: {
                        return multiplier * naturalCompare(valueA.value, valueB.value);
                    }

                    case BazaNcIntegrationSearchFieldType.Date: {
                        return multiplier * ((valueA.value as any as Date).getTime() - (valueB.value as any as Date).getTime());
                    }

                    case BazaNcIntegrationSearchFieldType.Status: {
                        const weightA = BAZA_NC_INTEGRATION_SEARCH_STATUS_WEIGHTS[valueA.value as any] || 0;
                        const weightB = BAZA_NC_INTEGRATION_SEARCH_STATUS_WEIGHTS[valueB.value as any] || 0;

                        return multiplier * (weightA - weightB);
                    }
                }
            });
        }
    }
}
