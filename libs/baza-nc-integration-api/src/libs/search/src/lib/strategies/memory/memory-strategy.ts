import { BazaNcIntegrationSearchStrategy } from '../baza-nc-integration-search-strategy';

export class MemoryStrategyConfig {
    type: BazaNcIntegrationSearchStrategy.Memory;
}
