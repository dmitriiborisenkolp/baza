import {
    BazaNcIntegrationSearchHandler,
    BazaNcIntegrationSearchIndexHandler,
    BazaNcIntegrationSearchStrategy,
} from '../baza-nc-integration-search-strategy';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';

interface ComponentType<T> {
    new (...args: any[]): T;
}

export interface CustomStrategyConfig {
    type: BazaNcIntegrationSearchStrategy.Custom;
    module: {
        imports: Array<any>;
        providers: Array<Provider>;
    };
    searchHandler: ComponentType<BazaNcIntegrationSearchHandler>;
    searchIndexHandler: ComponentType<BazaNcIntegrationSearchIndexHandler>;
}
