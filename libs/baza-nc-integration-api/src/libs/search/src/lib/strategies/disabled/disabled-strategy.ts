import { BazaNcIntegrationSearchStrategy } from '../baza-nc-integration-search-strategy';

export interface DisabledStrategyConfig {
    type: BazaNcIntegrationSearchStrategy.Disabled;
}
