import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationSearchHandler } from '../baza-nc-integration-search-strategy';
import { BazaNcIntegrationSearchDisabledException } from '../../exceptions/baza-nc-integration-search-disabled.exception';
import { BazaNcSearchRangesResponseDto, BazaNcSearchResponseDto } from '@scaliolabs/baza-nc-integration-shared';

@Injectable()
export class RedisStrategyHandler implements BazaNcIntegrationSearchHandler {
    async search(): Promise<BazaNcSearchResponseDto> {
        throw new BazaNcIntegrationSearchDisabledException();
    }

    async ranges(): Promise<BazaNcSearchRangesResponseDto> {
        throw new BazaNcIntegrationSearchDisabledException();
    }
}
