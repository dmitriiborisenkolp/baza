import { BazaNcIntegrationSearchStrategy } from '../baza-nc-integration-search-strategy';

export class RedisStrategyConfig {
    type: BazaNcIntegrationSearchStrategy.Redis;
}
