import { BazaNcSearchRangesResponseDto, BazaNcSearchRequestDto, BazaNcSearchResponseDto } from '@scaliolabs/baza-nc-integration-shared';

export enum BazaNcIntegrationSearchStrategy {
    /**
     * Disabled strategy - No indexing & Search API methods will response with exceptions
     */
    Disabled = 'Disabled',

    /**
     * In-Memory (current Node.JS node) storage
     * This strategy stores Listings as Listing DTOs in Memory
     * This strategy will works with cluster environments
     */
    Memory = 'Memory',

    /**
     * Redis storage
     * This strategy stores Listings as Listing DTOs in Redis
     */
    Redis = 'Redis',

    /**
     * Custom strategy
     * Implement your own way how to index & search listings
     */
    Custom = 'Custom',
}

export interface BazaNcIntegrationSearchHandler {
    search(request: BazaNcSearchRequestDto): Promise<BazaNcSearchResponseDto>;
    ranges(): Promise<BazaNcSearchRangesResponseDto>;
}

export interface BazaNcIntegrationSearchIndexHandler {
    reindex(): Promise<void>;
    putListing(listingUlid: string): Promise<void>;
    deleteListing(listingUlid: string): Promise<void>;

    onNestjsModuleInit?(): Promise<void>;
    onNestjsApplicationStart?(): Promise<void>;
    onNestjsApplicationShutdown?(): Promise<void>;

    onSchemaCreated?(schemaId: number): Promise<void>;
    onSchemaUpdated?(schemaId: number): Promise<void>;
    onSchemaDeleted?(schemaId: number): Promise<void>;
}

export const BAZA_NC_INTEGRATION_SEARCH_CONFIG = Symbol();
export const BAZA_NC_INTEGRATION_SEARCH_HANDLER = Symbol();
export const BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER = Symbol();
