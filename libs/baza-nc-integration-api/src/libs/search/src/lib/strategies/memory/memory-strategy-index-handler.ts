import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationSearchIndexHandler } from '../baza-nc-integration-search-strategy';
import { BazaNcIntegrationSearchListingDtoService } from '../../services/baza-nc-integration-search-listing-dto.service';
import { BazaNcIntegrationListingsDto, BazaNcSearchRangesResponseDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsMapper, BazaNcIntegrationListingsRepository } from '../../../../../listings/src';
import { ApiEventBus, BazaLogger } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSearchEvent, BazaNcIntegrationSearchEvents } from '../../baza-nc-integration-search.events';
import { ApiEventSource } from '@scaliolabs/baza-core-shared';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class MemoryStrategyIndexHandler implements BazaNcIntegrationSearchIndexHandler {
    public listings: Array<BazaNcIntegrationListingsDto> = [];
    public ranges: BazaNcSearchRangesResponseDto;

    private onApplicationShutdown$ = new Subject<void>();

    constructor(
        private readonly logger: BazaLogger,
        private readonly bazaCqrs: ApiEventBus<BazaNcIntegrationSearchEvent, BazaNcIntegrationSearchEvents>,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
        private readonly listingMapper: BazaNcIntegrationListingsMapper,
        private readonly listingHelper: BazaNcIntegrationSearchListingDtoService,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    async onNestjsModuleInit(): Promise<void> {
        await this.reindexCurrentNode();

        this.bazaCqrs.everyNodeEvents$.pipe(takeUntil(this.onApplicationShutdown$)).subscribe((next) => {
            switch (next.event.topic) {
                case BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchReindex: {
                    this.reindexCurrentNode().catch((err) => this.logger.error(`[BAZA_CQRS] Reindex Error: ${err}`));

                    break;
                }

                case BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchPutListing: {
                    this.putListingCurrentNode(next.event.payload.listingUlid).catch((err) =>
                        this.logger.error(`[BAZA_CQRS] Put Listing Error: ${err}`),
                    );

                    break;
                }

                case BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchExcludeListing: {
                    this.deleteListingCurrentNode(next.event.payload.listingUlid).catch((err) =>
                        this.logger.error(`[BAZA_CQRS] Exclude Listing Error: ${err}`),
                    );

                    break;
                }
            }
        });
    }

    async onNestjsApplicationShutdown(): Promise<void> {
        this.onApplicationShutdown$.next();
        this.onApplicationShutdown$.complete();
    }

    async reindex(): Promise<void> {
        await this.reindexCurrentNode();

        await this.bazaCqrs.publish(
            {
                topic: BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchReindex,
                payload: undefined,
            },
            {
                target: ApiEventSource.EveryNode,
            },
        );
    }

    async putListing(listingUlid: string): Promise<void> {
        await this.putListingCurrentNode(listingUlid);

        await this.bazaCqrs.publish(
            {
                topic: BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchPutListing,
                payload: {
                    listingUlid,
                },
            },
            {
                target: ApiEventSource.EveryNode,
            },
        );
    }

    async deleteListing(listingUlid: string): Promise<void> {
        await this.deleteListingCurrentNode(listingUlid);

        await this.bazaCqrs.publish(
            {
                topic: BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchExcludeListing,
                payload: {
                    listingUlid,
                },
            },
            {
                target: ApiEventSource.EveryNode,
            },
        );
    }

    async onSchemaCreated(): Promise<void> {
        await this.reindex();
    }

    async onSchemaUpdated(): Promise<void> {
        await this.reindex();
    }

    async onSchemaDeleted(): Promise<void> {
        await this.reindex();
    }

    private async reindexCurrentNode(): Promise<void> {
        const listings: Array<BazaNcIntegrationListingsDto> = [];
        const listingULIDs = await this.listingRepository.findAllULIDs();

        for (const ulid of listingULIDs) {
            const listing = await this.listingRepository.findByUlid(ulid);

            if (listing && listing.isPublished) {
                listings.push(await this.listingMapper.entityToDTO(listing));
            }
        }

        this.listings = listings;
        this.listings.sort((a, b) => a.sortOrder - b.sortOrder);

        await this.reindexRanges();
    }

    private async reindexRanges(): Promise<void> {
        this.ranges = await this.listingHelper.ranges(this.listings);
    }

    private async putListingCurrentNode(listingUlid: string): Promise<void> {
        const listings = this.listings.filter((next) => next.sid !== listingUlid);
        const listing = await this.listingRepository.findByUlid(listingUlid);

        if (listing && listing.isPublished) {
            listings.push(await this.listingMapper.entityToDTO(listing));
        }

        this.listings = listings;
        this.listings.sort((a, b) => a.sortOrder - b.sortOrder);

        await this.reindexRanges();
    }

    private async deleteListingCurrentNode(listingUlid: string): Promise<void> {
        this.listings = this.listings.filter((next) => next.sid !== listingUlid);
        this.listings.sort((a, b) => a.sortOrder - b.sortOrder);

        await this.reindexRanges();
    }
}
