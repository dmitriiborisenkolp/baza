import { Inject, Injectable } from '@nestjs/common';
import { BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER, BazaNcIntegrationSearchHandler } from '../baza-nc-integration-search-strategy';
import { BazaNcSearchRangesResponseDto, BazaNcSearchRequestDto, BazaNcSearchResponseDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSearchListingDtoService } from '../../services/baza-nc-integration-search-listing-dto.service';
import { MemoryStrategyIndexHandler } from './memory-strategy-index-handler';

@Injectable()
export class MemoryStrategyHandler implements BazaNcIntegrationSearchHandler {
    constructor(
        @Inject(BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER) private readonly index: MemoryStrategyIndexHandler,
        private readonly helper: BazaNcIntegrationSearchListingDtoService,
    ) {}

    async search(request: BazaNcSearchRequestDto): Promise<BazaNcSearchResponseDto> {
        return this.helper.search(request, this.index.listings);
    }

    async ranges(): Promise<BazaNcSearchRangesResponseDto> {
        return this.index.ranges;
    }
}
