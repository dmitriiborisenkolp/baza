import { CustomStrategyConfig } from './custom/custom-strategy';
import { DisabledStrategyConfig } from './disabled/disabled-strategy';
import { MemoryStrategyConfig } from './memory/memory-strategy';
import { RedisStrategyConfig } from './redis/redis-strategy';

export type BazaNcIntegrationSearchStrategies = CustomStrategyConfig | DisabledStrategyConfig | MemoryStrategyConfig | RedisStrategyConfig;
