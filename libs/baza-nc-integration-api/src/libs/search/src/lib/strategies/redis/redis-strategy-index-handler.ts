import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationSearchIndexHandler } from '../baza-nc-integration-search-strategy';
import { BazaNcIntegrationSearchDisabledException } from '../../exceptions/baza-nc-integration-search-disabled.exception';

@Injectable()
export class RedisStrategyIndexHandler implements BazaNcIntegrationSearchIndexHandler {
    async reindex(): Promise<void> {
        throw new BazaNcIntegrationSearchDisabledException();
    }

    async putListing(listingUlid: string): Promise<void> {
        throw new BazaNcIntegrationSearchDisabledException();
    }

    async deleteListing(listingUlid: string): Promise<void> {
        throw new BazaNcIntegrationSearchDisabledException();
    }
}
