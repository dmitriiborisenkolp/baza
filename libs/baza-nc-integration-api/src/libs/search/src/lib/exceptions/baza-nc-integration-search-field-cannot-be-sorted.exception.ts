import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSearchErrorCodes, bazaNcListingSearchErrorCodesEni18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationSearchFieldCannotBeSortedException extends BazaAppException {
    constructor(field: string) {
        super(
            BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchDisabled,
            bazaNcListingSearchErrorCodesEni18n[BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchDisabled],
            HttpStatus.BAD_REQUEST,
            { field },
        );
    }
}
