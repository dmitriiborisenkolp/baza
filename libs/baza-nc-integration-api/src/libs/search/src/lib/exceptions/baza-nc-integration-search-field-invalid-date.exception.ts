import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSearchErrorCodes, bazaNcListingSearchErrorCodesEni18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationSearchFieldInvalidDateException extends BazaAppException {
    constructor() {
        super(
            BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchFieldInvalidDate,
            bazaNcListingSearchErrorCodesEni18n[BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchFieldInvalidDate],
            HttpStatus.BAD_REQUEST,
        );
    }
}
