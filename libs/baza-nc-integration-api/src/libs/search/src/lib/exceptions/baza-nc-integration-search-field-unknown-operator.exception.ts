import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSearchErrorCodes, bazaNcListingSearchErrorCodesEni18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationSearchFieldUnknownOperatorException extends BazaAppException {
    constructor(operator: string) {
        super(
            BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchUnknownOperator,
            bazaNcListingSearchErrorCodesEni18n[BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchUnknownOperator],
            HttpStatus.BAD_REQUEST,
            { operator },
        );
    }
}
