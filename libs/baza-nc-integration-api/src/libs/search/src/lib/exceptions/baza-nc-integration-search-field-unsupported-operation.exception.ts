import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSearchErrorCodes, bazaNcListingSearchErrorCodesEni18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationSearchFieldUnsupportedOperationException extends BazaAppException {
    constructor(fieldType: string, operator: string, value: any) {
        super(
            BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchFieldUnsupportedOperation,
            bazaNcListingSearchErrorCodesEni18n[BazaNcIntegrationSearchErrorCodes.BazaNcIntegrationSearchFieldUnsupportedOperation],
            HttpStatus.BAD_REQUEST,
            { operator, value, fieldType },
        );
    }
}
