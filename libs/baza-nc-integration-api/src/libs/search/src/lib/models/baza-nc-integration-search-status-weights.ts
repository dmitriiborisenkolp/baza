import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';

export const BAZA_NC_INTEGRATION_SEARCH_STATUS_WEIGHTS = {
    [BazaNcOfferingStatus.ComingSoon]: 0,
    [BazaNcOfferingStatus.Open]: 1,
    [BazaNcOfferingStatus.Closed]: 2,
};
