export enum BazaNcIntegrationSearchFieldType {
    Number = 'number',
    String = 'string',
    Boolean = 'boolean',
    Date = 'date',
    Status = 'status',
    Enum = 'enum',
    Unknown = 'unknown',
}
