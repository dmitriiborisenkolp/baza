import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationSearchFieldType } from './baza-nc-integration-search-field-type';

export type BazaNcIntegrationSearchFieldValue =
    | { type: BazaNcIntegrationSearchFieldType.String; value: string }
    | { type: BazaNcIntegrationSearchFieldType.Number; value: number }
    | { type: BazaNcIntegrationSearchFieldType.Boolean; value: boolean }
    | { type: BazaNcIntegrationSearchFieldType.Date; value: Date }
    | { type: BazaNcIntegrationSearchFieldType.Status; value: BazaNcOfferingStatus }
    | { type: BazaNcIntegrationSearchFieldType.Enum; value: Array<string> }
    | { type: BazaNcIntegrationSearchFieldType.Unknown; value: any };
