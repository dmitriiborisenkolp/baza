import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaNcIntegrationListingIndexDeleteCommand } from '../../../../listings/src';
import { BazaNcIntegrationSearchIndexService } from '../services/baza-nc-integration-search-index.service';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcIntegrationListingIndexDeleteCommand)
export class ExcludeListingFromIndexCommandHandler implements ICommandHandler<BazaNcIntegrationListingIndexDeleteCommand> {
    constructor(private readonly indexService: BazaNcIntegrationSearchIndexService) {}

    async execute(command: BazaNcIntegrationListingIndexDeleteCommand): Promise<void> {
        return cqrsCommand<void>(ExcludeListingFromIndexCommandHandler.name, async () => {
            for (const listing of command.payload) {
                await this.indexService.deleteListing(listing.listingUlid);
            }
        });
    }
}
