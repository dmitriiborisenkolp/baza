import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaNcIntegrationListingIndexPutCommand } from '../../../../listings/src';
import { BazaNcIntegrationSearchIndexService } from '../services/baza-nc-integration-search-index.service';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcIntegrationListingIndexPutCommand)
export class PutListingToIndexCommandHandler implements ICommandHandler<BazaNcIntegrationListingIndexPutCommand> {
    constructor(private readonly indexService: BazaNcIntegrationSearchIndexService) {}

    async execute(command: BazaNcIntegrationListingIndexPutCommand): Promise<void> {
        return cqrsCommand<void>(PutListingToIndexCommandHandler.name, async () => {
            for (const listing of command.payload) {
                await this.indexService.putListing(listing.listingUlid);
            }
        });
    }
}
