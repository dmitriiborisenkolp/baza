import { BazaNcIntegrationSearchStrategies } from './strategies/baza-nc-integration-search-strategies';

export interface BazaNcIntegrationSearchApiConfig {
    strategy: BazaNcIntegrationSearchStrategies;
}
