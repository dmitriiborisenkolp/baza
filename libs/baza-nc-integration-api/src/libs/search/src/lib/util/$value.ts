import {
    BazaNcIntegrationListingsDto,
    bazaNcIntegrationSchemaHtmlTypes,
    bazaNcIntegrationSchemaNumberTypes,
    bazaNcIntegrationSchemaTextTypes,
    bazaNcSchemaBooleanTypes,
    bazaNcSchemaDateTypes,
    bazaNcSchemaEnumTypes,
    bazaNcSearchBooleanFields,
    bazaNcSearchDateFields,
    BazaNcSearchField,
    BazaNcSearchFields,
    bazaNcSearchNumberFields,
    bazaNcSearchStatusFields,
    bazaNcSearchStringFields,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSearchFieldType } from '../models/baza-nc-integration-search-field-type';
import { BazaNcIntegrationSearchFieldValue } from '../models/baza-nc-integration-search-field-value';

export function $value(listing: BazaNcIntegrationListingsDto, field: BazaNcSearchField): BazaNcIntegrationSearchFieldValue {
    if (bazaNcSearchStringFields.includes(field)) {
        if (field === BazaNcSearchFields.Schema) {
            return {
                type: BazaNcIntegrationSearchFieldType.String,
                value: listing.schema?.title,
            };
        } else {
            return {
                type: BazaNcIntegrationSearchFieldType.String,
                value: listing[field],
            };
        }
    } else if (bazaNcSearchNumberFields.includes(field)) {
        return {
            type: BazaNcIntegrationSearchFieldType.Number,
            value: listing[field],
        };
    } else if (bazaNcSearchBooleanFields.includes(field)) {
        return {
            type: BazaNcIntegrationSearchFieldType.Boolean,
            value: listing[field],
        };
    } else if (bazaNcSearchDateFields.includes(field)) {
        return {
            type: BazaNcIntegrationSearchFieldType.Date,
            value: listing[field] ? new Date(listing[field]) : undefined,
        };
    } else if (bazaNcSearchStatusFields.includes(field)) {
        return {
            type: BazaNcIntegrationSearchFieldType.Status,
            value: listing[field],
        };
    } else if (listing.schema && (listing.schema.definitions || []).length > 0) {
        const schemaDefinition = listing.schema.definitions.find((next) => next.payload.field === field);

        if (schemaDefinition) {
            if (bazaNcIntegrationSchemaNumberTypes.includes(schemaDefinition.type)) {
                return {
                    type: BazaNcIntegrationSearchFieldType.Number,
                    value: listing.properties[field] as number,
                };
            } else if (bazaNcIntegrationSchemaHtmlTypes.includes(schemaDefinition.type)) {
                return {
                    type: BazaNcIntegrationSearchFieldType.String,
                    value: (listing[field] || '').toString().replace(/<[^>]*>?/gm, ''),
                };
            } else if (bazaNcIntegrationSchemaTextTypes.includes(schemaDefinition.type)) {
                return {
                    type: BazaNcIntegrationSearchFieldType.String,
                    value: listing.properties[field],
                };
            } else if (bazaNcSchemaBooleanTypes.includes(schemaDefinition.type)) {
                return {
                    type: BazaNcIntegrationSearchFieldType.Boolean,
                    value: listing.properties[field],
                };
            } else if (bazaNcSchemaDateTypes.includes(schemaDefinition.type)) {
                const rawValue = listing.properties[field];

                if (!rawValue) {
                    const dateValue = new Date(rawValue);

                    if (!isNaN(dateValue.getTime())) {
                        return {
                            type: BazaNcIntegrationSearchFieldType.Date,
                            value: dateValue,
                        };
                    } else {
                        return {
                            type: BazaNcIntegrationSearchFieldType.Unknown,
                            value: rawValue,
                        };
                    }
                } else {
                    return {
                        type: BazaNcIntegrationSearchFieldType.Unknown,
                        value: rawValue,
                    };
                }
            } else if (bazaNcSchemaEnumTypes.includes(schemaDefinition.type)) {
                return {
                    type: BazaNcIntegrationSearchFieldType.Enum,
                    value: listing.properties[field],
                };
            } else {
                return {
                    type: BazaNcIntegrationSearchFieldType.Unknown,
                    value: listing.properties[field],
                };
            }
        } else {
            return {
                type: BazaNcIntegrationSearchFieldType.Unknown,
                value: undefined,
            };
        }
    } else {
        return {
            type: BazaNcIntegrationSearchFieldType.Unknown,
            value: undefined,
        };
    }
}
