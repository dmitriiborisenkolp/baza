import { BazaNcIntegrationSearchFieldValue } from '../models/baza-nc-integration-search-field-value';
import { BazaNcIntegrationSearchFieldType } from '../models/baza-nc-integration-search-field-type';
import { $exists } from './$exists';
import { BazaNcIntegrationSearchFieldUnsupportedOperationException } from '../exceptions/baza-nc-integration-search-field-unsupported-operation.exception';
import { BazaNcSearchOperator } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSearchFieldInvalidDateException } from '../exceptions/baza-nc-integration-search-field-invalid-date.exception';

export function $gt(fieldValue: BazaNcIntegrationSearchFieldValue, compare: any): boolean {
    if (! $exists(fieldValue)) {
        return false;
    }

    switch (fieldValue.type) {
        default: {
            return false;
        }

        case BazaNcIntegrationSearchFieldType.Number: {
            const isNumberValue = typeof fieldValue.value === 'number';

            if (isNumberValue) {
                return fieldValue.value > compare;
            } else {
                return false;
            }
        }

        case BazaNcIntegrationSearchFieldType.String: {
            throw new BazaNcIntegrationSearchFieldUnsupportedOperationException(
                fieldValue.type,
                BazaNcSearchOperator.LessThan,
                fieldValue.value,
            );
        }

        case BazaNcIntegrationSearchFieldType.Boolean: {
            throw new BazaNcIntegrationSearchFieldUnsupportedOperationException(
                fieldValue.type,
                BazaNcSearchOperator.LessThan,
                fieldValue.value,
            );
        }

        case BazaNcIntegrationSearchFieldType.Date: {
            const dateValue = new Date(compare);

            if (!isNaN(dateValue.getTime())) {
                return fieldValue.value.getTime() > dateValue.getTime();
            } else {
                throw new BazaNcIntegrationSearchFieldInvalidDateException();
            }
        }

        case BazaNcIntegrationSearchFieldType.Status: {
            throw new BazaNcIntegrationSearchFieldUnsupportedOperationException(
                fieldValue.type,
                BazaNcSearchOperator.LessThan,
                fieldValue.value,
            );
        }
    }
}
