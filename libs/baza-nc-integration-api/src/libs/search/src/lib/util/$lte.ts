import { BazaNcIntegrationSearchFieldValue } from '../models/baza-nc-integration-search-field-value';
import { $eq } from './$eq';
import { $lt } from './$lt';
import { $exists } from './$exists';

export function $lte(fieldValue: BazaNcIntegrationSearchFieldValue, compare: any): boolean {
    if (! $exists(fieldValue)) {
        return true;
    }

    return $eq(fieldValue, compare) || $lt(fieldValue, compare);
}
