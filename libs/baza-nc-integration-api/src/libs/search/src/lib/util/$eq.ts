import { BazaNcIntegrationSearchFieldValue } from '../models/baza-nc-integration-search-field-value';
import { $exists } from './$exists';
import { BazaNcIntegrationSearchFieldType } from '../models/baza-nc-integration-search-field-type';
import { BazaNcIntegrationSearchFieldInvalidDateException } from '../exceptions/baza-nc-integration-search-field-invalid-date.exception';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { isNotNullOrUndefined } from '@scaliolabs/baza-core-shared';

export function $eq(fieldValue: BazaNcIntegrationSearchFieldValue, compare: any): boolean {
    if (! $exists(fieldValue) || ! isNotNullOrUndefined(compare)) {
        return false;
    }

    switch (fieldValue.type) {
        default: {
            return false;
        }

        case BazaNcIntegrationSearchFieldType.Number: {
            const isNumberValue = typeof fieldValue.value === 'number';

            if (isNumberValue) {
                return fieldValue.value === compare;
            } else {
                return false;
            }
        }

        case BazaNcIntegrationSearchFieldType.Enum:
        case BazaNcIntegrationSearchFieldType.String: {
            return fieldValue.value === compare.toString();
        }

        case BazaNcIntegrationSearchFieldType.Boolean: {
            return Boolean(fieldValue.value) === Boolean(compare);
        }

        case BazaNcIntegrationSearchFieldType.Date: {
            const dateValue = new Date(compare);

            if (! isNaN(dateValue.getTime())) {
                return fieldValue.value.getTime() === dateValue.getTime();
            } else {
                throw new BazaNcIntegrationSearchFieldInvalidDateException();
            }
        }

        case BazaNcIntegrationSearchFieldType.Status: {
            return Object.values(BazaNcOfferingStatus).includes(compare)
                && Object.values(BazaNcOfferingStatus).includes(fieldValue.value)
                && fieldValue.value === compare;
        }
    }

    return false;
}
