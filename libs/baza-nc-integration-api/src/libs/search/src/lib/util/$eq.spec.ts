import 'reflect-metadata';
import '@angular/compiler';
import { BazaNcIntegrationSearchFieldValue } from '../models/baza-nc-integration-search-field-value';
import { $eq } from './$eq';
import { BazaNcIntegrationSearchFieldType } from '../models/baza-nc-integration-search-field-type';
import { BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';

interface TestCase {
    field: BazaNcIntegrationSearchFieldValue;
    compare: any;
    result: boolean;
}

describe('@scaliolabs/baza-nc-integration-api/search/util/eq.spec.ts', () => {
    it('will correctly handle null value', async () => {
        const testCases: Array<TestCase> = [
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Number,
                    value: null,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.String,
                    value: null,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Boolean,
                    value: null,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Date,
                    value: null,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Status,
                    value: null,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Unknown,
                    value: null,
                },
                compare: 'example',
                result: false,
            },
        ];

        for (const testCase of testCases) {
            expect($eq(testCase.field, testCase.compare)).toBe(testCase.result);
        }
    });

    it('will correctly handle undefined value', async () => {
        const testCases: Array<TestCase> = [
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Number,
                    value: undefined,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.String,
                    value: undefined,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Boolean,
                    value: undefined,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Date,
                    value: undefined,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Status,
                    value: undefined,
                },
                compare: 'example',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Unknown,
                    value: undefined,
                },
                compare: 'example',
                result: false,
            },
        ];

        for (const testCase of testCases) {
            expect($eq(testCase.field, testCase.compare)).toBe(testCase.result);
        }
    });

    it('will correctly handle number value', async () => {
        const testCases: Array<TestCase> = [
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Number,
                    value: 0,
                },
                compare: 0,
                result: true,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Number,
                    value: 0,
                },
                compare: 1,
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Number,
                    value: -1,
                },
                compare: -1,
                result: true,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Number,
                    value: 1.75,
                },
                compare: 1.75,
                result: true,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Number,
                    value: 1.75,
                },
                compare: 2,
                result: false,
            },
        ];

        for (const testCase of testCases) {
            expect($eq(testCase.field, testCase.compare)).toBe(testCase.result);
        }
    });

    it('will correctly handle boolean value', async () => {
        const testCases: Array<TestCase> = [
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Boolean,
                    value: false,
                },
                compare: false,
                result: true,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Boolean,
                    value: false,
                },
                compare: true,
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Boolean,
                    value: true,
                },
                compare: false,
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Boolean,
                    value: true,
                },
                compare: true,
                result: true,
            },
        ];

        for (const testCase of testCases) {
            expect($eq(testCase.field, testCase.compare)).toBe(testCase.result);
        }
    });

    it('will correctly handle date value', async () => {
        const testCases: Array<TestCase> = [
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Date,
                    value: new Date('2022-06-26T23:49:47.700Z'),
                },
                compare: '2022-06-26T23:49:47.700Z',
                result: true,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Date,
                    value: new Date('2021-06-26T23:49:47.700Z'),
                },
                compare: '2022-06-26T23:49:47.700Z',
                result: false,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Date,
                    value: new Date('2023-06-26T23:49:47.700Z'),
                },
                compare: '2022-06-26T23:49:47.700Z',
                result: false,
            },
        ];

        for (const testCase of testCases) {
            expect($eq(testCase.field, testCase.compare)).toBe(testCase.result);
        }
    });

    it('will correctly handle status value', async () => {
        const testCases: Array<TestCase> = [
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Status,
                    value: BazaNcOfferingStatus.Open,
                },
                compare: BazaNcOfferingStatus.Open,
                result: true,
            },
            {
                field: {
                    type: BazaNcIntegrationSearchFieldType.Status,
                    value: BazaNcOfferingStatus.Closed,
                },
                compare: BazaNcOfferingStatus.Open,
                result: false,
            },
        ];

        for (const testCase of testCases) {
            expect($eq(testCase.field, testCase.compare)).toBe(testCase.result);
        }
    });
});
