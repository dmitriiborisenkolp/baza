import { BazaNcIntegrationSearchFieldValue } from '../models/baza-nc-integration-search-field-value';
import { isNotNullOrUndefined } from '@scaliolabs/baza-core-shared';

export function $exists(fieldValue: BazaNcIntegrationSearchFieldValue): boolean {
    return isNotNullOrUndefined(fieldValue.value);
}
