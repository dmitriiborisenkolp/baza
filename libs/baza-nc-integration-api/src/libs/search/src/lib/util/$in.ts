import { BazaNcIntegrationSearchFieldValue } from '../models/baza-nc-integration-search-field-value';
import { $eq } from './$eq';
import { $exists } from './$exists';

export function $in(fieldValue: BazaNcIntegrationSearchFieldValue, compare: Array<any>): boolean {
    if (! $exists(fieldValue)) {
        return false;
    }

    return Array.isArray(compare)
        && compare.length > 0
        && compare.some((next) => $eq(fieldValue, next));
}
