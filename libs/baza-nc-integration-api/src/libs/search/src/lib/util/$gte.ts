import { BazaNcIntegrationSearchFieldValue } from '../models/baza-nc-integration-search-field-value';
import { $eq } from './$eq';
import { $gt } from './$gt';
import { $exists } from './$exists';

export function $gte(fieldValue: BazaNcIntegrationSearchFieldValue, compare: any): boolean {
    if (! $exists(fieldValue)) {
        return false;
    }

    return $eq(fieldValue, compare) || $gt(fieldValue, compare);
}
