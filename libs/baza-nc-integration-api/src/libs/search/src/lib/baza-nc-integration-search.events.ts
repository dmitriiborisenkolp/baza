export enum BazaNcIntegrationSearchEvent {
    BazaNcIntegrationSearchReindex = 'BazaNcIntegrationSearchReindex',
    BazaNcIntegrationSearchPutListing = 'BazaNcIntegrationSearchPutListing',
    BazaNcIntegrationSearchExcludeListing = 'BazaNcIntegrationSearchExcludeListing',
}

class BazaNcIntegrationSearchEventPayload {
    listingUlid: string;
}

export class BazaNcIntegrationSearchReindexEvent {
    topic: BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchReindex;
    payload: undefined;
}
export class BazaNcIntegrationSearchPutListingEvent {
    topic: BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchPutListing;
    payload: BazaNcIntegrationSearchEventPayload;
}
export class BazaNcIntegrationSearchExcludeListingEvent {
    topic: BazaNcIntegrationSearchEvent.BazaNcIntegrationSearchExcludeListing;
    payload: BazaNcIntegrationSearchEventPayload;
}

export type BazaNcIntegrationSearchEvents =
    | BazaNcIntegrationSearchReindexEvent
    | BazaNcIntegrationSearchPutListingEvent
    | BazaNcIntegrationSearchExcludeListingEvent;
