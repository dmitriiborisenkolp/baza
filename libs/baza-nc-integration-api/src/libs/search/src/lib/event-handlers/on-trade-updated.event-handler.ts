import { EventBus, EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcTradeStatusUpdatedEvent } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationListingIndexPutCommand, BazaNcIntegrationListingsRepository } from '../../../../listings/src';

@EventsHandler(BazaNcTradeStatusUpdatedEvent)
export class OnTradeUpdatedEventHandler implements IEventHandler<BazaNcTradeStatusUpdatedEvent> {
    constructor(private readonly eventBus: EventBus, private readonly repository: BazaNcIntegrationListingsRepository) {}

    async handle(event: BazaNcTradeStatusUpdatedEvent): Promise<void> {
        const offeringId = event.request.ncOfferingId;
        const listing = await this.repository.findByOfferingId(offeringId);

        if (listing) {
            await this.eventBus.publish(
                new BazaNcIntegrationListingIndexPutCommand([
                    {
                        listingId: listing.id,
                        listingUlid: listing.sid,
                        listingEntity: listing,
                    },
                ]),
            );
        }
    }
}
