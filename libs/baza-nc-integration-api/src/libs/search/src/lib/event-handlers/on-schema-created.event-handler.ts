import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcIntegrationSchemaCreatedEvent } from '../../../../schema/src';
import { Inject } from '@nestjs/common';
import {
    BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
    BazaNcIntegrationSearchIndexHandler,
} from '../strategies/baza-nc-integration-search-strategy';

@EventsHandler(BazaNcIntegrationSchemaCreatedEvent)
export class OnSchemaCreatedEventHandler implements IEventHandler<BazaNcIntegrationSchemaCreatedEvent> {
    constructor(
        @Inject(BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER) private readonly searchIndexHandler: BazaNcIntegrationSearchIndexHandler,
    ) {}

    async handle(event: BazaNcIntegrationSchemaCreatedEvent): Promise<void> {
        if (this.searchIndexHandler.onSchemaCreated) {
            await this.searchIndexHandler.onSchemaCreated(event.payload.schemaId);
        }
    }
}
