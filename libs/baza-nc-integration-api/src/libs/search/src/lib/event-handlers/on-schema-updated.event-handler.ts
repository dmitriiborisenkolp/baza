import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcIntegrationSchemaUpdatedEvent } from '../../../../schema/src';
import { Inject } from '@nestjs/common';
import {
    BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
    BazaNcIntegrationSearchIndexHandler,
} from '../strategies/baza-nc-integration-search-strategy';

@EventsHandler(BazaNcIntegrationSchemaUpdatedEvent)
export class OnSchemaUpdatedEventHandler implements IEventHandler<BazaNcIntegrationSchemaUpdatedEvent> {
    constructor(
        @Inject(BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER) private readonly searchIndexHandler: BazaNcIntegrationSearchIndexHandler,
    ) {}

    async handle(event: BazaNcIntegrationSchemaUpdatedEvent): Promise<void> {
        if (this.searchIndexHandler.onSchemaUpdated) {
            await this.searchIndexHandler.onSchemaUpdated(event.payload.schemaId);
        }
    }
}
