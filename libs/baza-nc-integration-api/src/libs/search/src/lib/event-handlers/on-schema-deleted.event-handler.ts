import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcIntegrationSchemaDeletedEvent } from '../../../../schema/src';
import { Inject } from '@nestjs/common';
import {
    BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
    BazaNcIntegrationSearchIndexHandler,
} from '../strategies/baza-nc-integration-search-strategy';

@EventsHandler(BazaNcIntegrationSchemaDeletedEvent)
export class OnSchemaDeletedEventHandler implements IEventHandler<BazaNcIntegrationSchemaDeletedEvent> {
    constructor(
        @Inject(BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER) private readonly searchIndexHandler: BazaNcIntegrationSearchIndexHandler,
    ) {}

    async handle(event: BazaNcIntegrationSchemaDeletedEvent): Promise<void> {
        if (this.searchIndexHandler.onSchemaDeleted) {
            await this.searchIndexHandler.onSchemaDeleted(event.payload.schemaId);
        }
    }
}
