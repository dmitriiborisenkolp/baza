import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiExtraModels, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    allBazaNcSearchOperators,
    allBazaNcSearchRanges,
    BazaNcIntegrationPublicOpenApi,
    BazaNcSearchEndpoint,
    BazaNcSearchEndpointPaths,
    BazaNcSearchRangesResponseDto,
    BazaNcSearchRequestBaseDto,
    BazaNcSearchRequestDto,
    BazaNcSearchResponseDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { AccountEntity, AuthOptionalGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSearchService } from '../services/baza-nc-integration-search.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNcIntegrationPublicOpenApi.Search)
@ApiExtraModels(...allBazaNcSearchOperators, ...allBazaNcSearchRanges)
@UseGuards(AuthOptionalGuard)
export class SearchController implements BazaNcSearchEndpoint {
    constructor(private readonly service: BazaNcIntegrationSearchService) {}

    @Post(BazaNcSearchEndpointPaths.search)
    @ApiOperation({
        summary: 'search',
        description: 'Search & Filter Listings',
    })
    @ApiOkResponse({
        type: BazaNcSearchResponseDto,
    })
    async search(@Body() request: BazaNcSearchRequestDto, @ReqAccount() account: AccountEntity): Promise<BazaNcSearchResponseDto> {
        return this.service.search(request, account);
    }

    @Post(BazaNcSearchEndpointPaths.searchULIDs)
    @ApiOperation({
        summary: 'searchULIDs',
        description: 'Search & Filter Listings (Returns Array of Listing ULIDs only)',
    })
    @ApiOkResponse({
        type: BazaNcSearchResponseDto,
    })
    searchULIDs(@Body() request: BazaNcSearchRequestBaseDto, @ReqAccount() account: AccountEntity): Promise<Array<string>> {
        return this.service.searchULIDs(request, account);
    }

    @Post(BazaNcSearchEndpointPaths.searchOfferingIds)
    @ApiOperation({
        summary: 'searchOfferingIds',
        description: 'Search & Filter Listings (Returns Array of Offering IDs only)',
    })
    @ApiOkResponse({
        type: BazaNcSearchResponseDto,
    })
    searchOfferingIds(@Body() request: BazaNcSearchRequestBaseDto, @ReqAccount() account: AccountEntity): Promise<Array<string>> {
        return this.service.searchOfferingIds(request, account);
    }

    @Get(BazaNcSearchEndpointPaths.ranges)
    @ApiOperation({
        summary: 'ranges',
        description: 'Returns possible values for filters',
    })
    @ApiOkResponse({
        type: BazaNcSearchRangesResponseDto,
    })
    async ranges(): Promise<BazaNcSearchRangesResponseDto> {
        return this.service.ranges();
    }
}
