import { Controller, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationCmsOpenApi,
    BazaNcSearchCmsEndpoint,
    BazaNcSearchCmsEndpointPaths,
} from '@scaliolabs/baza-nc-integration-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSearchCmsService } from '../services/baza-nc-integration-search-cms.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNcIntegrationCmsOpenApi.Search)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListing])
export class SearchCmsController implements BazaNcSearchCmsEndpoint {
    constructor(private readonly service: BazaNcIntegrationSearchCmsService) {}

    @Post(BazaNcSearchCmsEndpointPaths.reindex)
    @ApiOperation({
        summary: 'reindex',
        description: 'Reindex Listing in Search Index',
    })
    @ApiOkResponse()
    async reindex(@Param('ulid') ulid: string): Promise<void> {
        return this.service.reindex(ulid);
    }

    @Post(BazaNcSearchCmsEndpointPaths.reindexAll)
    @ApiOperation({
        summary: 'reindexAll',
        description: 'Reindex all Listings in Search Index',
    })
    @ApiOkResponse()
    async reindexAll(): Promise<void> {
        return this.service.reindexAll();
    }
}
