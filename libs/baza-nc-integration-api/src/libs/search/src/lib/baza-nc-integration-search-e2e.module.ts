import { forwardRef, Module } from '@nestjs/common';
import { BazaNcIntegrationListingsApiModule } from '../../../listings/src';
import { BazaNcIntegrationSearchAFixture } from './integration-tests/fixtures/baza-nc-integration-search-a.fixture';
import { BazaNcIntegrationSearchBFixture } from './integration-tests/fixtures/baza-nc-integration-search-b.fixture';
import { BazaNcIntegrationSearchCFixture } from './integration-tests/fixtures/baza-nc-integration-search-c.fixture';
import { BazaNcIntegrationSearchDFixture } from './integration-tests/fixtures/baza-nc-integration-search-d.fixture';

const E2E_FIXTURES = [
    BazaNcIntegrationSearchAFixture,
    BazaNcIntegrationSearchBFixture,
    BazaNcIntegrationSearchCFixture,
    BazaNcIntegrationSearchDFixture,
];

@Module({
    imports: [forwardRef(() => BazaNcIntegrationListingsApiModule)],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcIntegrationSearchE2eModule {}
