import { DynamicModule, Global, Inject, Module, OnApplicationBootstrap, OnApplicationShutdown, OnModuleInit } from '@nestjs/common';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import { SearchController } from './controllers/search.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { ExcludeListingFromIndexCommandHandler } from './command-handlers/exclude-listing-from-index.command-handler';
import { PutListingToIndexCommandHandler } from './command-handlers/put-listing-to-index.command-handler';
import { BazaNcIntegrationSearchApiConfig } from './baza-nc-integration-search-api.config';
import {
    BAZA_NC_INTEGRATION_SEARCH_CONFIG,
    BAZA_NC_INTEGRATION_SEARCH_HANDLER,
    BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
    BazaNcIntegrationSearchIndexHandler,
    BazaNcIntegrationSearchStrategy,
} from './strategies/baza-nc-integration-search-strategy';
import { DisabledStrategyHandler } from './strategies/disabled/disabled-strategy-handler';
import { DisabledStrategyIndexHandler } from './strategies/disabled/disabled-strategy-index-handler';
import { MemoryStrategyHandler } from './strategies/memory/memory-strategy-handler';
import { MemoryStrategyIndexHandler } from './strategies/memory/memory-strategy-index-handler';
import { RedisStrategyHandler } from './strategies/redis/redis-strategy-handler';
import { RedisStrategyIndexHandler } from './strategies/redis/redis-strategy-index-handler';
import { BazaNcIntegrationSearchIndexService } from './services/baza-nc-integration-search-index.service';
import { BazaNcIntegrationSearchService } from './services/baza-nc-integration-search.service';
import { BazaNcIntegrationListingsApiModule } from '../../../listings/src';
import { BazaNcIntegrationSearchListingDtoService } from './services/baza-nc-integration-search-listing-dto.service';
import { OnSchemaCreatedEventHandler } from './event-handlers/on-schema-created.event-handler';
import { OnSchemaDeletedEventHandler } from './event-handlers/on-schema-deleted.event-handler';
import { OnSchemaUpdatedEventHandler } from './event-handlers/on-schema-updated.event-handler';
import { OnOfferingStatusUpdatedEventHandler } from './event-handlers/on-offering-status-updated.event-handler';
import { OnPurchaseFlowSubmittedEventHandler } from './event-handlers/on-purchase-flow-submitted.event-handler';
import { OnTradeUpdatedEventHandler } from './event-handlers/on-trade-updated.event-handler';
import { OnPercentsFundUpdatedEventHandler } from './event-handlers/on-percents-fund-updated.event-handler';
import { BazaNcIntegrationSearchCmsService } from './services/baza-nc-integration-search-cms.service';
import { SearchCmsController } from './controllers/search-cms.controller';
import { BazaNcIntegrationSchemaApiModule } from '../../../schema/src';

const COMMAND_HANDLERS = [ExcludeListingFromIndexCommandHandler, PutListingToIndexCommandHandler];

const EVENT_HANDLERS = [
    OnSchemaCreatedEventHandler,
    OnSchemaDeletedEventHandler,
    OnSchemaUpdatedEventHandler,
    OnOfferingStatusUpdatedEventHandler,
    OnPurchaseFlowSubmittedEventHandler,
    OnTradeUpdatedEventHandler,
    OnPercentsFundUpdatedEventHandler,
];

@Module({
    imports: [BazaNcIntegrationSchemaApiModule],
})
export class BazaNcIntegrationSearchApiModule {
    static forRootAsync(config: BazaNcIntegrationSearchApiConfig): DynamicModule {
        const imports = [];
        const providers: Array<Provider> = [
            {
                provide: BAZA_NC_INTEGRATION_SEARCH_CONFIG,
                useValue: config,
            },
        ];

        const type = config.strategy.type;

        switch (config.strategy.type) {
            default: {
                throw new Error(`Unknown search strategy "${type}"`);
            }

            case BazaNcIntegrationSearchStrategy.Disabled: {
                providers.push(
                    ...[
                        {
                            provide: BAZA_NC_INTEGRATION_SEARCH_HANDLER,
                            useClass: DisabledStrategyHandler,
                        },
                        {
                            provide: BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
                            useClass: DisabledStrategyIndexHandler,
                        },
                    ],
                );

                break;
            }

            case BazaNcIntegrationSearchStrategy.Memory: {
                providers.push(
                    ...[
                        {
                            provide: BAZA_NC_INTEGRATION_SEARCH_HANDLER,
                            useClass: MemoryStrategyHandler,
                        },
                        {
                            provide: BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
                            useClass: MemoryStrategyIndexHandler,
                        },
                    ],
                );

                break;
            }

            case BazaNcIntegrationSearchStrategy.Redis: {
                providers.push(
                    ...[
                        {
                            provide: BAZA_NC_INTEGRATION_SEARCH_HANDLER,
                            useClass: RedisStrategyHandler,
                        },
                        {
                            provide: BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
                            useClass: RedisStrategyIndexHandler,
                        },
                    ],
                );

                break;
            }

            case BazaNcIntegrationSearchStrategy.Custom: {
                imports.push(...config.strategy.module.imports);

                providers.push(
                    ...[
                        {
                            provide: BAZA_NC_INTEGRATION_SEARCH_HANDLER,
                            useClass: config.strategy.searchHandler,
                        },
                        {
                            provide: BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER,
                            useClass: config.strategy.searchIndexHandler,
                        },
                        ...config.strategy.module.providers,
                    ],
                );

                break;
            }
        }

        return {
            module: BazaNcIntegrationSearchApiGlobalModule,
            exports: [BazaNcIntegrationSearchApiGlobalModule],
            providers,
            imports,
        };
    }
}

@Global()
@Module({
    imports: [CqrsModule, BazaNcIntegrationListingsApiModule],
    controllers: [SearchController, SearchCmsController],
    providers: [
        ...COMMAND_HANDLERS,
        ...EVENT_HANDLERS,
        BazaNcIntegrationSearchService,
        BazaNcIntegrationSearchIndexService,
        DisabledStrategyHandler,
        DisabledStrategyIndexHandler,
        MemoryStrategyHandler,
        MemoryStrategyIndexHandler,
        RedisStrategyHandler,
        RedisStrategyIndexHandler,
        BazaNcIntegrationSearchListingDtoService,
        BazaNcIntegrationSearchCmsService,
    ],
    exports: [BazaNcIntegrationSearchService, BazaNcIntegrationSearchIndexService, BazaNcIntegrationSearchListingDtoService],
})
class BazaNcIntegrationSearchApiGlobalModule implements OnModuleInit, OnApplicationBootstrap, OnApplicationShutdown {
    constructor(
        @Inject(BAZA_NC_INTEGRATION_SEARCH_INDEX_HANDLER) private readonly searchIndexHandler: BazaNcIntegrationSearchIndexHandler,
    ) {}

    async onModuleInit(): Promise<void> {
        if (this.searchIndexHandler.onNestjsModuleInit) {
            await this.searchIndexHandler.onNestjsModuleInit();
        }
    }

    async onApplicationBootstrap(): Promise<void> {
        if (this.searchIndexHandler.onNestjsApplicationStart) {
            await this.searchIndexHandler.onNestjsApplicationStart();
        }
    }

    async onApplicationShutdown(): Promise<void> {
        if (this.searchIndexHandler.onNestjsApplicationShutdown) {
            await this.searchIndexHandler.onNestjsApplicationShutdown();
        }
    }
}
