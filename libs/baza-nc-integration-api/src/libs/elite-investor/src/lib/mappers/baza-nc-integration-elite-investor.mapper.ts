import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationEliteInvestorEntity } from '../entities/baza-nc-integration-elite-investor.entity';
import { BazaAccountMapper } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationEliteInvestorsDto } from '@scaliolabs/baza-nc-integration-shared';

@Injectable()
export class BazaNcIntegrationEliteInvestorMapper {
    constructor(
        private readonly accountMapper: BazaAccountMapper,
    ) {}

    entityToDTO(input: BazaNcIntegrationEliteInvestorEntity): BazaNcIntegrationEliteInvestorsDto {
        return {
            id: input.id,
            account: this.accountMapper.entityToDto(input.account),
            dateJoinedAt: input.dateJoinedAt.toISOString(),
        };
    }

    entitiesToDTOs(input: Array<BazaNcIntegrationEliteInvestorEntity>): Array<BazaNcIntegrationEliteInvestorsDto> {
        return input.map((entity) => this.entityToDTO(entity));
    }
}
