import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationEliteInvestorsCmsController } from './controllers/baza-nc-integration-elite-investors-cms.controller';
import { BazaNcIntegrationEliteInvestorMapper } from './mappers/baza-nc-integration-elite-investor.mapper';
import { BazaNcIntegrationEliteInvestorRepository } from './repositories/baza-nc-integration-elite-investor.repository';
import { BazaNcIntegrationEliteInvestorCmsService } from './services/baza-nc-integration-elite-investor-cms.service';
import { BazaNcIntegrationUpdateTradeStatusEventHandler } from './event-handlers/baza-nc-integration-update-trade-status.event-handler';
import { CqrsModule } from '@nestjs/cqrs';

const EVENT_HANDLERS = [
    BazaNcIntegrationUpdateTradeStatusEventHandler,
];

@Module({
    imports: [
        CqrsModule,
        BazaCrudApiModule,
    ],
    controllers: [
        BazaNcIntegrationEliteInvestorsCmsController,
    ],
    providers: [
        BazaNcIntegrationEliteInvestorMapper,
        BazaNcIntegrationEliteInvestorRepository,
        BazaNcIntegrationEliteInvestorCmsService,
        ...EVENT_HANDLERS,
    ],
    exports: [
        BazaNcIntegrationEliteInvestorMapper,
        BazaNcIntegrationEliteInvestorRepository,
        BazaNcIntegrationEliteInvestorCmsService,
    ],
})
export class BazaNcIntegrationEliteInvestorApiModule {}
