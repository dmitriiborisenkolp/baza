import { Body, Controller, Logger, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import { WithAccessGuard } from '@scaliolabs/baza-core-api';
import { AclNodes } from '@scaliolabs/baza-core-api';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationCmsOpenApi,
    BazaNcIntegrationEliteInvestorsCmsEndpoint,
    BazaNcIntegrationEliteInvestorsCmsEndpointPaths,
    BazaNcIntegrationEliteInvestorsCmsExcludeRequest,
    BazaNcIntegrationEliteInvestorsCmsExcludeResponse,
    BazaNcIntegrationEliteInvestorsCmsGetByIdRequest,
    BazaNcIntegrationEliteInvestorsCmsGetByIdResponse,
    BazaNcIntegrationEliteInvestorsCmsIncludeRequest,
    BazaNcIntegrationEliteInvestorsCmsIncludeResponse,
    BazaNcIntegrationEliteInvestorsCmsListRequest,
    BazaNcIntegrationEliteInvestorsCmsListResponse,
    BazaNcIntegrationEliteInvestorsCmsSyncAccountRequest,
    BazaNcIntegrationEliteInvestorsDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationEliteInvestorMapper } from '../mappers/baza-nc-integration-elite-investor.mapper';
import { BazaNcIntegrationEliteInvestorCmsService } from '../services/baza-nc-integration-elite-investor-cms.service';

@Controller()
@ApiTags(BazaNcIntegrationCmsOpenApi.EliteInvestors)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingsEliteInvestors])
export class BazaNcIntegrationEliteInvestorsCmsController implements BazaNcIntegrationEliteInvestorsCmsEndpoint {
    constructor(
        private readonly mapper: BazaNcIntegrationEliteInvestorMapper,
        private readonly service: BazaNcIntegrationEliteInvestorCmsService,
    ) {}

    @Post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.include)
    @ApiOperation({
        summary: 'include',
        description: 'Add account to Elite Investors. Endpoint will not emit any error if account is already in Elite Investors',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationEliteInvestorsDto,
    })
    async include(
        @Body() request: BazaNcIntegrationEliteInvestorsCmsIncludeRequest,
    ): Promise<BazaNcIntegrationEliteInvestorsCmsIncludeResponse> {
        const entity = await this.service.include(request, {
            markAsManuallyAdded: true,
        });

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.exclude)
    @ApiOperation({
        summary: 'exclude',
        description: 'Remove account from Elite Investors. Endpoint will not emit any error if account is not in Elite Investors',
    })
    @ApiOkResponse({})
    async exclude(
        @Body() request: BazaNcIntegrationEliteInvestorsCmsExcludeRequest,
    ): Promise<BazaNcIntegrationEliteInvestorsCmsExcludeResponse> {
        await this.service.exclude(request);
    }

    @Post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List of Elite Investors accounts',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationEliteInvestorsCmsListResponse,
    })
    async list(@Body() request: BazaNcIntegrationEliteInvestorsCmsListRequest): Promise<BazaNcIntegrationEliteInvestorsCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns Elite Investors entity',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationEliteInvestorsDto,
    })
    async getById(
        @Body() request: BazaNcIntegrationEliteInvestorsCmsGetByIdRequest,
    ): Promise<BazaNcIntegrationEliteInvestorsCmsGetByIdResponse> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.syncAccount)
    @ApiOperation({
        summary: 'syncAccount',
        description: 'Update Elite Investors Statuses of Account',
    })
    @ApiOkResponse()
    async syncAccount(@Body() request: BazaNcIntegrationEliteInvestorsCmsSyncAccountRequest): Promise<void> {
        await this.service.syncAccount(request);
    }

    @Post(BazaNcIntegrationEliteInvestorsCmsEndpointPaths.syncAllAccounts)
    @ApiOperation({
        summary: 'syncAllAccounts',
        description: 'Starts updating Elite Investors Statuses for all Accounts',
    })
    @ApiOkResponse()
    async syncAllAccounts(): Promise<void> {
        const logger = new Logger('BazaNcIntegrationEliteInvestorsCmsController');

        this.service.syncAllAccounts().catch((err) => {
            logger.error(`Failed to sync Elite Investor status for all accounts:`);
            logger.error(err);
        });
    }
}
