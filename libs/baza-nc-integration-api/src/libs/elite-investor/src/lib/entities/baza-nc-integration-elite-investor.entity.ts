import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';

@Entity()
export class BazaNcIntegrationEliteInvestorEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    dateJoinedAt: Date;

    @ManyToOne(() => BazaNcIntegrationListingsEntity, {
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    listing: BazaNcIntegrationListingsEntity;

    @ManyToOne(() => AccountEntity, {
        cascade: true,
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
    })
    @JoinColumn()
    account: AccountEntity;

    @Column({
        default: false,
        type: 'boolean',
    })
    isManuallyAdded = false;
}
