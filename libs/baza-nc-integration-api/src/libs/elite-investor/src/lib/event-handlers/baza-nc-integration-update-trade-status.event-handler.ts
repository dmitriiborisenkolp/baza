import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcTradeCreatedEvent, BazaNcTradeUpdatedEvent, OfferingId, TransactionState } from '@scaliolabs/baza-nc-shared';
import { TradeId } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity, BazaNcOfferingRepository, BazaNcTransactionRepository } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationEliteInvestorCmsService } from '../services/baza-nc-integration-elite-investor-cms.service';
import { BazaNcIntegrationListingsRepository, BazaNcIntegrationListingsService } from '../../../../listings/src';
import { cqrs } from '@scaliolabs/baza-core-api';

type Event = BazaNcTradeCreatedEvent | BazaNcTradeUpdatedEvent;

@EventsHandler(BazaNcTradeCreatedEvent, BazaNcTradeUpdatedEvent)
export class BazaNcIntegrationUpdateTradeStatusEventHandler implements IEventHandler<Event> {
    constructor(
        private readonly service: BazaNcIntegrationEliteInvestorCmsService,
        private readonly ncTransaction: BazaNcTransactionRepository,
        private readonly listingsService: BazaNcIntegrationListingsService,
        private readonly listingsRepository: BazaNcIntegrationListingsRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
    ) {}

    handle(event: Event): void {
        cqrs(BazaNcIntegrationUpdateTradeStatusEventHandler.name, async () => {
            await this.includeAccountToEliteInvestors(event.request.ncTradeId);
            await this.updateAccountEliteInvestorStatus(event.request.ncTradeId);
        });
    }

    async includeAccountToEliteInvestors(ncTradeId: TradeId): Promise<void> {
        const transaction = await this.ncTransaction.findByNcTradeId(ncTradeId);

        if (!transaction) {
            return;
        }

        const offering = await this.offeringRepository.findOfferingWithId(transaction.ncOfferingId);

        if (!offering) {
            return;
        }

        const listing = await this.listingsRepository.findByOffering(offering);

        if (!listing) {
            return;
        }

        const totalSharesBought = await this.totalSharesBoughtByInvestorAccount(transaction.ncOfferingId, transaction.investorAccount);

        if (totalSharesBought >= listing.sharesToJoinEliteInvestors) {
            await this.service.include({
                accountId: transaction.account.id,
                listingId: listing.id,
            });
        }
    }

    async updateAccountEliteInvestorStatus(ncTradeId: TradeId): Promise<void> {
        const transaction = await this.ncTransaction.findByNcTradeId(ncTradeId);

        if (!transaction) {
            return;
        }

        await this.service.syncAccount({
            accountId: transaction.account.id,
        });
    }

    async totalSharesBoughtByInvestorAccount(ncOfferingId: OfferingId, investorAccount: BazaNcInvestorAccountEntity): Promise<number> {
        let total = 0;

        const allTransactionsOfAccount = await this.ncTransaction.findAllTransactionsOfInvestorAccount(ncOfferingId, investorAccount);

        for (const transaction of allTransactionsOfAccount) {
            if (
                [TransactionState.PendingPayment, TransactionState.PaymentFunded, TransactionState.PaymentConfirmed].includes(
                    transaction.state,
                )
            ) {
                total += transaction.shares;
            }
        }

        return total;
    }
}
