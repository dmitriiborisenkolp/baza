import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationEliteInvestorEntity } from '../entities/baza-nc-integration-elite-investor.entity';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';
import { BazaNcIntegrationEliteInvestorNotFoundException } from '../exceptions/baza-nc-integration-elite-investor-not-found.exception';

export const BAZA_NC_INTEGRATION_ELITE_INVESTOR_RELATIONS = [
    'listing',
    'account',
];

@Injectable()
export class BazaNcIntegrationEliteInvestorRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaNcIntegrationEliteInvestorEntity> {
        return this.connection.getRepository(BazaNcIntegrationEliteInvestorEntity);
    }

    async save(entity: BazaNcIntegrationEliteInvestorEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async upsert(entity: BazaNcIntegrationEliteInvestorEntity): Promise<BazaNcIntegrationEliteInvestorEntity> {
        const existing = await this.findByAccountAndListing(entity.account, entity.listing);

        if (existing) {
            return existing;
        } else {
            await this.save(entity);

            return entity;
        }
    }

    async remove(entity: BazaNcIntegrationEliteInvestorEntity): Promise<void> {
        await this.repository.remove(entity);
    }

    async exists(account: number | AccountEntity, listing: number | BazaNcIntegrationListingsEntity): Promise<boolean> {
        return !! await this.findByAccountAndListing(account, listing);
    }

    async findByAccountAndListing(account: number | AccountEntity, listing: number | BazaNcIntegrationListingsEntity): Promise<BazaNcIntegrationEliteInvestorEntity | void> {
        return this.repository.findOne({
            where: [{
                account,
                listing,
            }],
            relations: BAZA_NC_INTEGRATION_ELITE_INVESTOR_RELATIONS,
        });
    }

    async deleteByAccount(account: AccountEntity): Promise<void> {
        await this.repository.delete({
            account,
        });
    }

    async getById(id: number): Promise<BazaNcIntegrationEliteInvestorEntity> {
        const entity = await this.repository.findOne({
            where: [{
                id,
            }],
            relations: BAZA_NC_INTEGRATION_ELITE_INVESTOR_RELATIONS,
        });

        if (! entity) {
            throw new BazaNcIntegrationEliteInvestorNotFoundException();
        }

        return entity;
    }
}
