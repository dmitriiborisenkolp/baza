import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcIntegrationEliteInvestorsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import {
    BazaNcE2eNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcTransactionsCmsNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { AccountTypeCheckingSaving, NorthCapitalAccountId, OrderStatus, TradeId } from '@scaliolabs/baza-nc-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-elite-investor/api/integration-tests/tests/02-baza-nc-integration-auto-add-account-to-elite-investor.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessEliteInvestors = new BazaNcIntegrationEliteInvestorsCmsNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessNcE2e = new BazaNcE2eNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessTransactionsCMS = new BazaNcTransactionsCmsNodeAccess(http);

    let USED_ID: number;
    let LISTINGS_ID: number;
    let LISTINGS: Array<BazaNcIntegrationListingsDto>;
    let ACCOUNT_ID: NorthCapitalAccountId;
    let TRADE_ID: TradeId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFullFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        LISTINGS = (
            await dataAccessListings.list({
                index: 1,
                size: -1,
            })
        ).items;

        LISTINGS_ID = LISTINGS[1].id;

        await http.authE2eUser();

        USED_ID = http.authResponse.jwtPayload.accountId;

        const investorAccount = await dataAccessInvestorAccount.current();

        ACCOUNT_ID = investorAccount.northCapitalAccountId;
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will successfully purchase 1 share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            numberOfShares: 1,
            offeringId: LISTINGS[1].offeringId,
            requestDocuSignUrl: false,
            amount: 1000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const bankAccount = dataAccessBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();

        const submit = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();

        TRADE_ID = session.id;
    });

    it('will have trade in transactions list', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessTransactionsCMS.list({
                    index: 1,
                    size: -1,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(1);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully update trade status', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.Funded,
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not add account to elite investor (1 share purchased only when 2 shares are required)', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessEliteInvestors.list({
                    index: 1,
                    size: 1,
                    listingId: LISTINGS_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(0);
                expect(response.pager.total).toBe(0);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully purchase 1 more share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            numberOfShares: 1,
            offeringId: LISTINGS[1].offeringId,
            requestDocuSignUrl: false,
            amount: 1000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submit = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();

        TRADE_ID = session.id;
    });

    it('will have trade #2 in transactions list', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessTransactionsCMS.list({
                    index: 1,
                    size: -1,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();
                expect(response.items.length).toBe(2);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully update trade #2 status', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.Funded,
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will add account to elite investor (2 share purchased when 2 shares are required)', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessEliteInvestors.list({
                    index: 1,
                    size: 1,
                    listingId: LISTINGS_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.pager.total).toBe(1);
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will successfully update trade #2 status from FUNDED to SETTLED', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.Settled,
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will keep account in Elite Investors', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessEliteInvestors.list({
                    index: 1,
                    size: 1,
                    listingId: LISTINGS_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(1);
                expect(response.pager.total).toBe(1);
            },
            null,
            { intervalMillis: 3000 },
        );
    });

    it('will successfully update trade #2 status from SETTLED to UNWIND SETTLED', async () => {
        const response = await dataAccessNcE2e.updateTradeStatus({
            accountId: ACCOUNT_ID,
            orderStatus: OrderStatus.UnwindSettled,
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will exclude account from Elite Investors', async () => {
        await http.authE2eAdmin();

        await asyncExpect(
            async () => {
                const response = await dataAccessEliteInvestors.list({
                    index: 1,
                    size: 1,
                    listingId: LISTINGS_ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(0);
                expect(response.pager.total).toBe(0);
            },
            null,
            { intervalMillis: 3000 },
        );
    });
});
