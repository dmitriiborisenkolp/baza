import 'reflect-metadata';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaE2eFixturesNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures } from '@scaliolabs/baza-core-shared';
import { isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcIntegrationEliteInvestorsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@baza-nc-integration/baza-nc-integration-elite-investor/api/integration-tests/tests/01-baza-nc-integration-elite-investor-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessEliteInvestors = new BazaNcIntegrationEliteInvestorsCmsNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);

    let USED_ID: number;
    let LISTINGS_ID: number;
    let LISTINGS_2_ID: number;
    let LISTINGS: Array<BazaNcIntegrationListingsDto>;
    let ELITE_INVESTOR_ENTRY_ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFullFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        LISTINGS = (
            await dataAccessListings.list({
                index: 1,
                size: -1,
            })
        ).items;

        LISTINGS_ID = LISTINGS[0].id;
        LISTINGS_2_ID = LISTINGS[1].id;

        await http.authE2eUser();

        USED_ID = http.authResponse.jwtPayload.accountId;
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will display that all elite investors are empty', async () => {
        for (const listings of LISTINGS) {
            const response = await dataAccessEliteInvestors.list({
                index: 1,
                size: -1,
                listingId: listings.id,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();
            expect(response.pager.total).toBe(0);
            expect(response.items.length).toBe(0);
        }
    });

    it('will successfully create a EliteInvestors entry in CMS', async () => {
        const response = await dataAccessEliteInvestors.include({
            listingId: LISTINGS_ID,
            accountId: USED_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.account.id).toBe(USED_ID);

        ELITE_INVESTOR_ENTRY_ID = response.id;
    });

    it('will display elite investors entry for current and will not display EliteInvestors entry in other offerings', async () => {
        for (const listings of LISTINGS) {
            const response = await dataAccessEliteInvestors.list({
                index: 1,
                size: -1,
                listingId: listings.id,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            if (listings.id === LISTINGS_ID) {
                expect(response.pager.total).toBe(1);
                expect(response.items.length).toBe(1);
            } else {
                expect(response.pager.total).toBe(0);
                expect(response.items.length).toBe(0);
            }
        }
    });

    it('will allow to get elite investors entry by id', async () => {
        const response = await dataAccessEliteInvestors.getById({
            id: ELITE_INVESTOR_ENTRY_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ELITE_INVESTOR_ENTRY_ID);
        expect(response.account).toBeDefined();
        expect(response.account.id).toBe(USED_ID);
    });

    it('will successfully sync account', async () => {
        const response = await dataAccessEliteInvestors.syncAccount({
            accountId: USED_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not drop elite investors entry after sync accounts (single) because it is added manually', async () => {
        for (const listings of LISTINGS) {
            const response = await dataAccessEliteInvestors.list({
                index: 1,
                size: -1,
                listingId: listings.id,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            if (listings.id === LISTINGS_ID) {
                expect(response.pager.total).toBe(1);
                expect(response.items.length).toBe(1);
            } else {
                expect(response.pager.total).toBe(0);
                expect(response.items.length).toBe(0);
            }
        }
    });

    it('will successfully sync all elite investors', async () => {
        const response = await dataAccessEliteInvestors.syncAllAccounts();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        // syncAllAccount works asynchronously. Wait for results...
    });

    it('will not drop elite investors entry after sync accounts (bulk) because it is added manually', async () => {
        await asyncExpect(
            async () => {
                for (const listings of LISTINGS) {
                    const response = await dataAccessEliteInvestors.list({
                        index: 1,
                        size: -1,
                        listingId: listings.id,
                    });

                    expect(isBazaErrorResponse(response)).toBeFalsy();

                    if (listings.id === LISTINGS_ID) {
                        expect(response.pager.total).toBe(1);
                        expect(response.items.length).toBe(1);
                    } else {
                        expect(response.pager.total).toBe(0);
                        expect(response.items.length).toBe(0);
                    }
                }
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will successfully add same account to another elite investors', async () => {
        const response = await dataAccessEliteInvestors.include({
            listingId: LISTINGS_2_ID,
            accountId: USED_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.account.id).toBe(USED_ID);

        ELITE_INVESTOR_ENTRY_ID = response.id;
    });

    it('will display same account in different elite investors', async () => {
        for (const listings of LISTINGS) {
            const response = await dataAccessEliteInvestors.list({
                index: 1,
                size: -1,
                listingId: listings.id,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            if (listings.id === LISTINGS_ID || listings.id === LISTINGS_2_ID) {
                expect(response.pager.total).toBe(1);
                expect(response.items.length).toBe(1);
                expect(response.items[0].account.id).toBe(USED_ID);
            } else {
                expect(response.pager.total).toBe(0);
                expect(response.items.length).toBe(0);
            }
        }
    });

    it('will successfully exclude account from elite investors', async () => {
        const response = await dataAccessEliteInvestors.exclude({
            accountId: USED_ID,
            listingId: LISTINGS_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display that account was removed from elite investors and will display account in another Elite Investors', async () => {
        for (const listings of LISTINGS) {
            const response = await dataAccessEliteInvestors.list({
                index: 1,
                size: -1,
                listingId: listings.id,
            });

            expect(isBazaErrorResponse(response)).toBeFalsy();

            if (listings.id === LISTINGS_2_ID) {
                expect(response.pager.total).toBe(1);
                expect(response.items.length).toBe(1);
                expect(response.items[0].account.id).toBe(USED_ID);
            } else {
                expect(response.pager.total).toBe(0);
                expect(response.items.length).toBe(0);
            }
        }
    });
});
