import { Injectable, Logger } from '@nestjs/common';
import { BazaAccountRepository } from '@scaliolabs/baza-core-api';
import { CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { CrudService } from '@scaliolabs/baza-core-api';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { Like } from 'typeorm';
import { postgresLikeEscape } from '@scaliolabs/baza-core-shared';
import { OfferingId, TransactionState } from '@scaliolabs/baza-nc-shared';
import { BazaNcInvestorAccountEntity, BazaNcInvestorAccountRepository, BazaNcTransactionRepository } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationEliteInvestorMapper } from '../mappers/baza-nc-integration-elite-investor.mapper';
import {
    BAZA_NC_INTEGRATION_ELITE_INVESTOR_RELATIONS,
    BazaNcIntegrationEliteInvestorRepository,
} from '../repositories/baza-nc-integration-elite-investor.repository';
import { BazaNcIntegrationListingsRepository } from '../../../../listings/src';
import { BazaNcIntegrationEliteInvestorEntity } from '../entities/baza-nc-integration-elite-investor.entity';
import {
    BazaNcIntegrationEliteInvestorsCmsExcludeRequest,
    BazaNcIntegrationEliteInvestorsCmsGetByIdRequest,
    BazaNcIntegrationEliteInvestorsCmsIncludeRequest,
    BazaNcIntegrationEliteInvestorsCmsListRequest,
    BazaNcIntegrationEliteInvestorsCmsSyncAccountRequest,
    BazaNcIntegrationEliteInvestorsDto,
} from '@scaliolabs/baza-nc-integration-shared';

interface IncludeOptions {
    markAsManuallyAdded: boolean;
}

@Injectable()
export class BazaNcIntegrationEliteInvestorCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: BazaNcIntegrationEliteInvestorMapper,
        private readonly repository: BazaNcIntegrationEliteInvestorRepository,
        private readonly accountRepository: BazaAccountRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
        private readonly ncTransaction: BazaNcTransactionRepository,
    ) {}

    async include(
        request: BazaNcIntegrationEliteInvestorsCmsIncludeRequest,
        options: IncludeOptions = {
            markAsManuallyAdded: false,
        },
    ): Promise<BazaNcIntegrationEliteInvestorEntity> {
        const entity = new BazaNcIntegrationEliteInvestorEntity();

        entity.account = await this.accountRepository.getActiveAccountWithId(request.accountId);
        entity.listing = await this.listingRepository.getById(request.listingId);
        entity.dateJoinedAt = new Date();
        entity.isManuallyAdded = options.markAsManuallyAdded;

        return this.repository.upsert(entity);
    }

    async exclude(request: BazaNcIntegrationEliteInvestorsCmsExcludeRequest): Promise<void> {
        const existing = await this.repository.findByAccountAndListing(request.accountId, request.listingId);

        if (existing) {
            await this.repository.remove(existing);
        }
    }

    async list(request: BazaNcIntegrationEliteInvestorsCmsListRequest): Promise<CrudListResponseDto<BazaNcIntegrationEliteInvestorsDto>> {
        const findOptions: FindManyOptions<BazaNcIntegrationEliteInvestorEntity> = (() => {
            if (request.queryString) {
                return {
                    where: (qb) => {
                        qb.where(
                            '"BazaNcIntegrationEliteInvestorEntity"."listingId" = :listingId AND ("BazaNcIntegrationEliteInvestorEntity"."email" ILIKE :qs OR "BazaNcIntegrationEliteInvestorEntity"."fullName" ILIKE :qs)',
                            {
                                qs: `%${postgresLikeEscape(request.queryString)}%`,
                                listingId: request.listingId.toString(),
                            },
                        );
                    },
                    relations: BAZA_NC_INTEGRATION_ELITE_INVESTOR_RELATIONS,
                };
            } else {
                return {
                    where: [
                        {
                            listing: request.listingId,
                        },
                    ],
                    relations: BAZA_NC_INTEGRATION_ELITE_INVESTOR_RELATIONS,
                };
            }
        })();

        if (request.queryString) {
            findOptions.where[0] = {
                ...findOptions[0],
                'account.email': Like(`%${postgresLikeEscape(request.queryString)}%`),
            };
        }

        return this.crud.find<BazaNcIntegrationEliteInvestorEntity, BazaNcIntegrationEliteInvestorsDto>({
            request,
            findOptions,
            entity: BazaNcIntegrationEliteInvestorEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });
    }

    async getById(request: BazaNcIntegrationEliteInvestorsCmsGetByIdRequest): Promise<BazaNcIntegrationEliteInvestorEntity> {
        return this.repository.getById(request.id);
    }

    async syncAccount(request: BazaNcIntegrationEliteInvestorsCmsSyncAccountRequest): Promise<void> {
        const account = await this.accountRepository.getActiveOrDeactivatedAccountWithId(request.accountId);
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(account.id);
        const listings = await this.listingRepository.listAll();

        if (!investorAccount || account.isDeactivated) {
            await this.repository.deleteByAccount(account);
        } else {
            for (const listing of listings) {
                const existing = await this.repository.findByAccountAndListing(request.accountId, listing.id);

                if (existing && !existing.isManuallyAdded) {
                    const totalSharesBought = await this.totalSharesBoughtByInvestorAccount(listing.offering.ncOfferingId, investorAccount);

                    if (totalSharesBought >= listing.sharesToJoinEliteInvestors) {
                        await this.include({
                            accountId: account.id,
                            listingId: listing.id,
                        });
                    } else {
                        await this.exclude({
                            accountId: account.id,
                            listingId: listing.id,
                        });
                    }
                }
            }
        }
    }

    async syncAllAccounts(): Promise<void> {
        const logger = new Logger('BazaNcIntegrationEliteInvestorCmsService');

        const allAccount = await this.accountRepository.repository.find({
            select: ['id'],
        });

        for (const account of allAccount) {
            logger.log(`Sync Elite Investor statuses for Account (ID: "${account.id}")`);

            await this.syncAccount({
                accountId: account.id,
            });
        }

        logger.log('Sync Elite Investors statuses for all accounts finished');
    }

    async totalSharesBoughtByInvestorAccount(ncOfferingId: OfferingId, investorAccount: BazaNcInvestorAccountEntity): Promise<number> {
        let total = 0;

        const allTransactionsOfAccount = await this.ncTransaction.findAllTransactionsOfInvestorAccount(ncOfferingId, investorAccount);

        for (const transaction of allTransactionsOfAccount) {
            if (
                [TransactionState.PendingPayment, TransactionState.PaymentFunded, TransactionState.PaymentConfirmed].includes(
                    transaction.state,
                )
            ) {
                total += transaction.shares;
            }
        }

        return total;
    }
}
