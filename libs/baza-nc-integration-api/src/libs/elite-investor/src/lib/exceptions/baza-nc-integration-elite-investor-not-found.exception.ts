import { BazaAppException } from '@scaliolabs/baza-core-api';
import { HttpStatus } from '@nestjs/common';
import { BazaNcIntegrationEliteInvestorsErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationEliteInvestorNotFoundException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationEliteInvestorsErrorCodes.BazaNcIntegrationEliteInvestorsNotFound, 'EliteInvestors Entity was not found', HttpStatus.NOT_FOUND);
    }
}
