import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { GetSubscriptionStatusCommand, GetSubscriptionStatusCommandResponse } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSubscriptionsService } from '../services/baza-nc-integration-subscriptions.service';
import { BazaAccountRepository, cqrsCommand } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsRepository } from '../../../../listings/src';

@CommandHandler(GetSubscriptionStatusCommand)
export class GetSubscriptionStatusCommandHandler
    implements ICommandHandler<GetSubscriptionStatusCommand, GetSubscriptionStatusCommandResponse>
{
    constructor(
        private readonly service: BazaNcIntegrationSubscriptionsService,
        private readonly accountRepository: BazaAccountRepository,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
    ) {}

    async execute(command: GetSubscriptionStatusCommand): Promise<GetSubscriptionStatusCommandResponse> {
        return cqrsCommand<GetSubscriptionStatusCommandResponse>(GetSubscriptionStatusCommandHandler.name, async () => {
            const status = await this.service.status(
                await this.accountRepository.getActiveAccountWithId(command.payload.accountId),
                await this.listingRepository.getById(command.payload.listingId),
            );

            return {
                status,
            };
        });
    }
}
