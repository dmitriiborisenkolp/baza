import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { BazaNcMailNotificationOfferingStatusCommand } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSubscriptionMailNotificationService } from '../services/baza-nc-integration-subscription-mail-notification.service';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcMailNotificationOfferingStatusCommand)
export class BazaNcIntegrationSubscriptionMailNotificationCommandHandler
    implements ICommandHandler<BazaNcMailNotificationOfferingStatusCommand>
{
    constructor(private readonly subscriptionMailNotificationService: BazaNcIntegrationSubscriptionMailNotificationService) {}

    async execute(command: BazaNcMailNotificationOfferingStatusCommand): Promise<void> {
        return cqrsCommand<void>(BazaNcIntegrationSubscriptionMailNotificationCommandHandler.name, async () => {
            const { listingId } = command.request;

            await this.subscriptionMailNotificationService.sendOfferingStatusEmail(listingId);
        });
    }
}
