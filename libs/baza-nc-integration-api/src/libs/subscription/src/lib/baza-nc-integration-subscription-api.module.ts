import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSubscriptionCmsMapper } from './mappers/baza-nc-integration-subscription-cms.mapper';
import { BazaNcIntegrationSubscriptionCsvMapper } from './mappers/baza-nc-integration-subscription-csv.mapper';
import { BazaNcIntegrationSubscriptionRepository } from './repositories/baza-nc-integration-subscription.repository';
import { BazaNcIntegrationSubscriptionCmsService } from './services/baza-nc-integration-subscription-cms.service';
import { BazaNcIntegrationSubscriptionsService } from './services/baza-nc-integration-subscriptions.service';
import { BazaNcIntegrationSubscriptionController } from './controllers/baza-nc-integration-subscription.controller';
import { BazaNcIntegrationSubscriptionCmsController } from './controllers/baza-nc-integration-subscription-cms.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { GetSubscriptionStatusCommandHandler } from './command-handlers/get-subscription-status.command-handler';
import { BazaNcIntegrationSubscriptionMailNotificationService } from './services/baza-nc-integration-subscription-mail-notification.service';
import { BazaNcIntegrationSubscriptionMailNotificationCommandHandler } from './command-handlers/baza-nc-integration-subscription-mail-notification.command-handler';

const COMMAND_HANDLERS = [GetSubscriptionStatusCommandHandler, BazaNcIntegrationSubscriptionMailNotificationCommandHandler];

@Module({
    imports: [CqrsModule, BazaCrudApiModule],
    controllers: [BazaNcIntegrationSubscriptionController, BazaNcIntegrationSubscriptionCmsController],
    providers: [
        BazaNcIntegrationSubscriptionCmsMapper,
        BazaNcIntegrationSubscriptionCsvMapper,
        BazaNcIntegrationSubscriptionRepository,
        BazaNcIntegrationSubscriptionCmsService,
        BazaNcIntegrationSubscriptionsService,
        BazaNcIntegrationSubscriptionMailNotificationService,
        ...COMMAND_HANDLERS,
    ],
    exports: [
        BazaNcIntegrationSubscriptionCmsMapper,
        BazaNcIntegrationSubscriptionCsvMapper,
        BazaNcIntegrationSubscriptionRepository,
        BazaNcIntegrationSubscriptionCmsService,
        BazaNcIntegrationSubscriptionsService,
        BazaNcIntegrationSubscriptionMailNotificationService,
    ],
})
export class BazaNcIntegrationSubscriptionApiModule {}
