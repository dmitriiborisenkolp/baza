import { BazaNcIntegrationSubscriptionEntity } from '../entities/baza-nc-integration-subscription.entity';
import { BazaNcIntegrationSubscribeStatus, BazaNcIntegrationSubscriptionCsvRow } from '@scaliolabs/baza-nc-integration-shared';

export class BazaNcIntegrationSubscriptionCsvMapper {
    entityToCSVRow(input: BazaNcIntegrationSubscriptionEntity): BazaNcIntegrationSubscriptionCsvRow {
        return {
            id: input.id,
            email: input.account.email,
            account: input.account.fullName,
            sent: input.sent.join(';'),
            subscribed: input.status === BazaNcIntegrationSubscribeStatus.Subscribed
                ? 'Yes'
                : 'No',
        };
    }

    entitiesToCSVRows(input: Array<BazaNcIntegrationSubscriptionEntity>): Array<BazaNcIntegrationSubscriptionCsvRow> {
        return input.map((e) => this.entityToCSVRow(e));
    }
}
