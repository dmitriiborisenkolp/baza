import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationSubscriptionDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaAccountMapper } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSubscriptionEntity } from '../entities/baza-nc-integration-subscription.entity';
import { BazaNcIntegrationListingsCmsMapper, BazaNcIntegrationListingsRepository } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationSubscriptionCmsMapper {
    constructor(
        private readonly accountMapper: BazaAccountMapper,
        private readonly listingMapper: BazaNcIntegrationListingsCmsMapper,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
    ) {}

    async entityToDTO(input: BazaNcIntegrationSubscriptionEntity): Promise<BazaNcIntegrationSubscriptionDto> {
        const listing = await this.listingRepository.getById(input.listing.id);

        return {
            id: input.id,
            status: input.status,
            account: this.accountMapper.entityToDto(input.account),
            listing: await this.listingMapper.entityToDTO(listing),
            sent: input.sent,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcIntegrationSubscriptionEntity>): Promise<Array<BazaNcIntegrationSubscriptionDto>> {
        const result: Array<BazaNcIntegrationSubscriptionDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
