import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationSubscriptionCmsNodeAccess,
    BazaNcIntegrationSubscriptionNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationListingsErrorCodes, BazaNcIntegrationSubscribeStatus } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../listings/src';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/001-baza-nc-integration-subscription-mail-notify.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessSubscriptions = new BazaNcIntegrationSubscriptionNodeAccess(http);
    const dataAccessSubscriptionCms = new BazaNcIntegrationSubscriptionCmsNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListings],
            specFile: __filename,
        });
    });

    it('will return NotSubscribed status for auth user', async () => {
        await http.authE2eUser();

        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items[0].isSubscribed).toBeFalsy();
        expect(response.items[0].subscribeStatus).toBe(BazaNcIntegrationSubscribeStatus.NotSubscribed);
        ID = response.items[0].id;
    });

    it('will successfully subscribe to listing', async () => {
        await http.authE2eUser();

        const response = await dataAccessSubscriptions.subscribe({
            listingId: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will return Subscribed status', async () => {
        await http.authE2eUser();

        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items[0].isSubscribed).toBeTruthy();
        expect(response.items[0].subscribeStatus).toBe(BazaNcIntegrationSubscribeStatus.Subscribed);
    });

    it('will return list of 1 subscribed user from cms', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessSubscriptionCms.list({
            listingId: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaNcIntegrationSubscribeStatus.Subscribed);
    });

    it('will have an empty mail box', async () => {
        const mailboxBefore = await dataAccessE2e.mailbox();

        expect(mailboxBefore.length).toBe(0);
    });

    it('will throw error with invalid listing id', async () => {
        await http.authE2eAdmin();

        const response: BazaError = (await dataAccessSubscriptionCms.notifyUsers({ listingId: 123123123 })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();
        expect(response.code).toBe(BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound);
    });

    it('will successfully notify users about offering status', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessSubscriptionCms.notifyUsers({ listingId: ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will have received a mail', async () => {
        const mailboxBefore = await dataAccessE2e.mailbox();

        expect(mailboxBefore.length).toBe(1);
    });

    it('will successfully unsubscribe to listing', async () => {
        await http.authE2eUser();

        const response = await dataAccessSubscriptions.unsubscribe({
            listingId: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will return list of 1 subscribed user from cms', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessSubscriptionCms.list({
            listingId: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);
        expect(response.items[0].status).toBe(BazaNcIntegrationSubscribeStatus.Unsubscribed);
    });

    it('will have an empty mail box', async () => {
        await dataAccessE2e.flushMailbox();
        const mailboxBefore = await dataAccessE2e.mailbox();

        expect(mailboxBefore.length).toBe(0);
    });

    it('will successfully notify users about offering status', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessSubscriptionCms.notifyUsers({ listingId: ID });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not have received a mail', async () => {
        const mailboxBefore = await dataAccessE2e.mailbox();

        expect(mailboxBefore.length).toBe(0);
    });
});
