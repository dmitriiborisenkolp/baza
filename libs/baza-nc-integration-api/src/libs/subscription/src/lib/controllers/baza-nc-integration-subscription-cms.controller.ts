import { Body, Controller, Header, Post, UseGuards } from '@nestjs/common';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationCmsOpenApi,
    BazaNcIntegrationSubscriptionCmsEndpoint,
    BazaNcIntegrationSubscriptionCmsEndpointPaths,
    BazaNcIntegrationSubscriptionCmsExcludeRequest,
    BazaNcIntegrationSubscriptionCmsExcludeResponse,
    BazaNcIntegrationSubscriptionCmsExportToCsvRequest,
    BazaNcIntegrationSubscriptionCmsIncludeRequest,
    BazaNcIntegrationSubscriptionCmsIncludeResponse,
    BazaNcIntegrationSubscriptionCmsListRequest,
    BazaNcIntegrationSubscriptionCmsListResponse,
    BazaNcIntegrationSubscriptionCmsNotifyUsersRequest,
    BazaNcIntegrationSubscriptionCmsNotifyUsersResponse,
    BazaNcIntegrationSubscriptionDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthGuard, AuthRequireAdminRoleGuard } from '@scaliolabs/baza-core-api';
import { WithAccessGuard } from '@scaliolabs/baza-core-api';
import { AclNodes } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSubscriptionCmsService } from '../services/baza-nc-integration-subscription-cms.service';
import { BazaNcIntegrationSubscriptionCmsMapper } from '../mappers/baza-nc-integration-subscription-cms.mapper';

@Controller()
@ApiTags(BazaNcIntegrationCmsOpenApi.Subscription)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingSubscription])
export class BazaNcIntegrationSubscriptionCmsController implements BazaNcIntegrationSubscriptionCmsEndpoint {
    constructor(
        private readonly service: BazaNcIntegrationSubscriptionCmsService,
        private readonly mapper: BazaNcIntegrationSubscriptionCmsMapper,
    ) {}

    @Post(BazaNcIntegrationSubscriptionCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'List of subscriptions',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSubscriptionCmsListResponse,
    })
    async list(@Body() request: BazaNcIntegrationSubscriptionCmsListRequest): Promise<BazaNcIntegrationSubscriptionCmsListResponse> {
        return await this.service.list(request);
    }

    @Post(BazaNcIntegrationSubscriptionCmsEndpointPaths.include)
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement])
    @ApiOperation({
        summary: 'include',
        description: 'Include (subscribe) user',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSubscriptionDto,
    })
    async include(
        @Body() request: BazaNcIntegrationSubscriptionCmsIncludeRequest,
    ): Promise<BazaNcIntegrationSubscriptionCmsIncludeResponse> {
        const entity = await this.service.include(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationSubscriptionCmsEndpointPaths.exclude)
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement])
    @ApiOperation({
        summary: 'exclude',
        description: 'Exclude user from subscription',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSubscriptionDto,
    })
    async exclude(
        @Body() request: BazaNcIntegrationSubscriptionCmsExcludeRequest,
    ): Promise<BazaNcIntegrationSubscriptionCmsExcludeResponse> {
        await this.service.exclude(request);
    }

    @Post(BazaNcIntegrationSubscriptionCmsEndpointPaths.exportToCsv)
    @Header('Content-Type', 'text/csv')
    @UseGuards(WithAccessGuard)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement])
    @ApiOperation({
        summary: 'exportToCsv',
        description: 'Export subscriptions to CSV',
    })
    @ApiOkResponse()
    async exportToCsv(@Body() request: BazaNcIntegrationSubscriptionCmsExportToCsvRequest): Promise<string> {
        return this.service.exportToCsv(request);
    }

    @Post(BazaNcIntegrationSubscriptionCmsEndpointPaths.notifyUsers)
    @ApiOperation({
        summary: 'notifyUsers',
        description: 'Notify subscribed users about offering status',
    })
    @ApiOkResponse()
    async notifyUsers(
        @Body() request: BazaNcIntegrationSubscriptionCmsNotifyUsersRequest,
    ): Promise<BazaNcIntegrationSubscriptionCmsNotifyUsersResponse> {
        return this.service.notifyUsers(request);
    }
}
