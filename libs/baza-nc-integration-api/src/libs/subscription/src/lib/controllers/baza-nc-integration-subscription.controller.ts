import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationSubscriptionEndpoint,
    BazaNcIntegrationSubscriptionEndpointPaths,
    BazaNcIntegrationSubscribeToRequest,
    BazaNcIntegrationSubscribedResponse,
    BazaNcIntegrationUnsubscribeRequest,
    BazaNcIntegrationUnsubscribedResponse,
    BazaNcIntegrationPublicOpenApi,
} from '@scaliolabs/baza-nc-integration-shared';
import { AccountEntity, AuthGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSubscriptionsService } from '../services/baza-nc-integration-subscriptions.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNcIntegrationPublicOpenApi.Subscription)
@UseGuards(AuthGuard)
export class BazaNcIntegrationSubscriptionController implements BazaNcIntegrationSubscriptionEndpoint {
    constructor(private readonly service: BazaNcIntegrationSubscriptionsService) {}

    @Post(BazaNcIntegrationSubscriptionEndpointPaths.subscribe)
    @ApiOperation({
        summary: 'subscribe',
        description:
            'Subscribe user to listing. Statuses: "Subscribed" - user had no subscriptions before and subscribed; "AlreadySubscribed" - user already has subscription',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSubscribedResponse,
    })
    async subscribe(
        @Body() request: BazaNcIntegrationSubscribeToRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationSubscribedResponse> {
        const response = await this.service.subscribe(request, account);

        return {
            status: response,
        };
    }

    @Post(BazaNcIntegrationSubscriptionEndpointPaths.unsubscribe)
    @ApiOperation({
        summary: 'unsubscribe',
        description:
            'Unsubscribe user from listing. Statuses: "Unsubscribed" - user had subscription before and unsubscribed; "AlreadyUnsubscribed" - user had subscription before and already unsubscribed; "NotSubscribed" - user had no subscription before',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationSubscribedResponse,
    })
    async unsubscribe(
        @Body() request: BazaNcIntegrationUnsubscribeRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationUnsubscribedResponse> {
        const response = await this.service.unsubscribe(request, account);

        return {
            status: response,
        };
    }
}
