import { Injectable } from '@nestjs/common';

import { BazaRegistryService, BAZA_CRUD_DEFAULT_INDEX, MailService } from '@scaliolabs/baza-core-api';
import { CrudListResponseDto, replaceTags } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSubscribeStatus, BazaNcIntegrationSubscriptionDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSubscriptionCmsService } from './baza-nc-integration-subscription-cms.service';
import { BazaNcMail } from '@scaliolabs/baza-nc-api';

/**
 * Mail notification service to send offering status updates to subscribed users
 */
@Injectable()
export class BazaNcIntegrationSubscriptionMailNotificationService {
    constructor(
        private readonly mail: MailService,
        private readonly registry: BazaRegistryService,
        private readonly subscriptionCmsService: BazaNcIntegrationSubscriptionCmsService,
    ) {}

    /**
     * Sends notification via email about offering status for all the subscribed users for a listing
     * @param listingId
     */
    async sendOfferingStatusEmail(listingId: number): Promise<void> {
        let index = BAZA_CRUD_DEFAULT_INDEX;
        const size = 50;
        let total: number;

        do {
            const subscriptionList = await this.subscriptionCmsService.list({ index, size, listingId });
            await this.sendMailToOnlySubscribedUsers(subscriptionList);
            total = subscriptionList.pager.total;
            index = index + size;
        } while (index <= total);
    }

    private async sendMailToOnlySubscribedUsers(subscriptionList: CrudListResponseDto<BazaNcIntegrationSubscriptionDto>) {
        const filteredSubscriptionList = subscriptionList.items.filter(
            (subscription) => subscription.status === BazaNcIntegrationSubscribeStatus.Subscribed,
        );
        for (const subscriber of filteredSubscriptionList) {
            await this.sendEmailNotification(subscriber);
        }
    }

    private async sendEmailNotification(subscriber: BazaNcIntegrationSubscriptionDto) {
        await this.mail.send({
            name: BazaNcMail.BazaNcOfferingClientNotification,
            uniqueId: subscriber.listing.offering.id
                ? `baza_nc_integration_subscription_offering_${subscriber.listing.offering.id}_${subscriber.id}`
                : undefined,
            subject: replaceTags(this.registry.getValue('bazaNc.offeringStatusSubject'), {
                offeringName: subscriber.listing.offering.ncOfferingName,
            }),
            to: [
                {
                    email: subscriber.account.email,
                    name: subscriber.account.fullName,
                },
            ],
            variables: () => ({
                userFullName: subscriber.account.fullName,
                offeringName: subscriber.listing.offering.ncOfferingName,
                clientName: this.registry.getValue('bazaCommon.clientName'),
            }),
        });
    }
}
