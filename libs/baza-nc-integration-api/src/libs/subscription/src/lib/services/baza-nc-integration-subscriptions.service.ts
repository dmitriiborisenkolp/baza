import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationSubscribeResponse, BazaNcIntegrationSubscribeStatus, BazaNcIntegrationSubscribeToRequest, BazaNcIntegrationUnsubscribeRequest, BazaNcIntegrationUnsubscribeResponse } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSubscriptionRepository } from '../repositories/baza-nc-integration-subscription.repository';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsEntity, BazaNcIntegrationListingsRepository } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationSubscriptionsService {
    constructor(
        private readonly repository: BazaNcIntegrationListingsRepository,
        private readonly usRepository: BazaNcIntegrationSubscriptionRepository,
    ) {}

    async status(account: AccountEntity, listing: BazaNcIntegrationListingsEntity): Promise<BazaNcIntegrationSubscribeStatus> {
        const hasSubscription = await this.usRepository.hasSubscription({
            account,
            listing,
        });

        if (hasSubscription) {
            const entity = await this.usRepository.touchSubscription({
                listing,
                account,
            });

            return entity.status;
        } else {
            return BazaNcIntegrationSubscribeStatus.NotSubscribed;
        }
    }

    async subscribe(request: BazaNcIntegrationSubscribeToRequest, account: AccountEntity): Promise<BazaNcIntegrationSubscribeResponse> {
        const listing = await this.repository.getById(request.listingId);
        const entity = await this.usRepository.touchSubscription({
            listing,
            account,
        });

        if (entity.status === BazaNcIntegrationSubscribeStatus.Subscribed) {
            return BazaNcIntegrationSubscribeResponse.AlreadySubscribed;
        } else {
            await this.usRepository.setSubscriptionStatus({
                listing,
                account,
            }, BazaNcIntegrationSubscribeStatus.Subscribed);

            return BazaNcIntegrationSubscribeResponse.Subscribed;
        }
    }

    async unsubscribe(request: BazaNcIntegrationUnsubscribeRequest, account: AccountEntity): Promise<BazaNcIntegrationUnsubscribeResponse> {
        const listing = await this.repository.getById(request.listingId);
        const entity = await this.usRepository.touchSubscription({
            listing,
            account,
        });

        if (entity.status === BazaNcIntegrationSubscribeStatus.Subscribed) {
            await this.usRepository.setSubscriptionStatus({
                listing,
                account,
            }, BazaNcIntegrationSubscribeStatus.Unsubscribed);

            return BazaNcIntegrationUnsubscribeResponse.Unsubscribed;
        } else if (entity.status === BazaNcIntegrationSubscribeStatus.Unsubscribed) {
            return BazaNcIntegrationUnsubscribeResponse.AlreadyUnsubscribed;
        } else {
            return BazaNcIntegrationUnsubscribeResponse.NotSubscribed;
        }
    }
}
