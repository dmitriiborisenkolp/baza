import { Injectable } from '@nestjs/common';
import {
    BAZA_NC_INTEGRATION_SUBSCRIPTION_RELATIONS,
    BazaNcIntegrationSubscriptionRepository,
} from '../repositories/baza-nc-integration-subscription.repository';
import { CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSubscriptionEntity } from '../entities/baza-nc-integration-subscription.entity';
import {
    BazaNcIntegrationSubscribeStatus,
    BazaNcIntegrationSubscriptionCmsExcludeRequest,
    BazaNcIntegrationSubscriptionCmsExportToCsvRequest,
    BazaNcIntegrationSubscriptionCmsIncludeRequest,
    BazaNcIntegrationSubscriptionCmsListRequest,
    BazaNcIntegrationSubscriptionCmsNotifyUsersRequest,
    BazaNcIntegrationSubscriptionCsvRow,
    BazaNcIntegrationSubscriptionDto,
    BazaNcMailNotificationOfferingStatusCommand,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSubscriptionsService } from './baza-nc-integration-subscriptions.service';
import { BazaAccountRepository } from '@scaliolabs/baza-core-api';
import { CrudCsvService, CrudService } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSubscriptionCmsMapper } from '../mappers/baza-nc-integration-subscription-cms.mapper';
import { In } from 'typeorm';
import { FindManyOptions } from 'typeorm/browser';
import { BazaNcIntegrationSubscriptionCsvMapper } from '../mappers/baza-nc-integration-subscription-csv.mapper';
import { BazaNcIntegrationListingsRepository } from '../../../../listings/src';
import { CommandBus } from '@nestjs/cqrs';

@Injectable()
export class BazaNcIntegrationSubscriptionCmsService {
    constructor(
        private readonly repository: BazaNcIntegrationSubscriptionRepository,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
        private readonly accountRepository: BazaAccountRepository,
        private readonly service: BazaNcIntegrationSubscriptionsService,
        private readonly crud: CrudService,
        private readonly cmsMapper: BazaNcIntegrationSubscriptionCmsMapper,
        private readonly crudCsv: CrudCsvService,
        private readonly csvMapper: BazaNcIntegrationSubscriptionCsvMapper,
        private readonly commandBus: CommandBus,
    ) {}

    list(request: BazaNcIntegrationSubscriptionCmsListRequest): Promise<CrudListResponseDto<BazaNcIntegrationSubscriptionDto>> {
        return this.crud.find<BazaNcIntegrationSubscriptionEntity, BazaNcIntegrationSubscriptionDto>({
            request,
            entity: BazaNcIntegrationSubscriptionEntity,
            mapper: async (items) => this.cmsMapper.entitiesToDTOs(items),
            findOptions: this.findOptions(request),
        });
    }

    async include(request: BazaNcIntegrationSubscriptionCmsIncludeRequest): Promise<BazaNcIntegrationSubscriptionEntity> {
        const account = await this.accountRepository.getActiveAccountWithEmail(request.email);
        const listing = await this.listingRepository.getById(request.listingId);

        await this.service.subscribe(
            {
                listingId: request.listingId,
            },
            account,
        );

        return this.repository.touchSubscription({
            account,
            listing: listing,
        });
    }

    async exclude(request: BazaNcIntegrationSubscriptionCmsExcludeRequest): Promise<void> {
        const account = await this.accountRepository.getActiveAccountWithEmail(request.email);
        const listing = await this.listingRepository.getById(request.listingId);

        if (
            await this.repository.hasSubscription({
                account,
                listing: listing,
            })
        ) {
            await this.service.unsubscribe(
                {
                    listingId: request.listingId,
                },
                account,
            );
        }
    }

    async exportToCsv(request: BazaNcIntegrationSubscriptionCmsExportToCsvRequest): Promise<string> {
        return this.crudCsv.exportToCsv<BazaNcIntegrationSubscriptionEntity, BazaNcIntegrationSubscriptionCsvRow>({
            request: {
                queryString: request.listRequest.queryString,
            },
            delimiter: request.delimiter,
            entity: BazaNcIntegrationSubscriptionEntity,
            findOptions: this.findOptions(request.listRequest),
            fields: request.fields,
            mapper: async (items) => this.csvMapper.entitiesToCSVRows(items),
        });
    }

    private findOptions(request: BazaNcIntegrationSubscriptionCmsListRequest): FindManyOptions<BazaNcIntegrationSubscriptionDto> {
        return {
            where: [
                {
                    listing: request.listingId,
                    status: In([BazaNcIntegrationSubscribeStatus.Subscribed, BazaNcIntegrationSubscribeStatus.Unsubscribed]),
                },
            ],
            relations: BAZA_NC_INTEGRATION_SUBSCRIPTION_RELATIONS,
        };
    }

    async notifyUsers(request: BazaNcIntegrationSubscriptionCmsNotifyUsersRequest): Promise<void> {
        // to check whether the listing id is a valid and existant one
        const listing = await this.listingRepository.getById(request.listingId);
        return this.commandBus.execute(new BazaNcMailNotificationOfferingStatusCommand(request));
    }
}
