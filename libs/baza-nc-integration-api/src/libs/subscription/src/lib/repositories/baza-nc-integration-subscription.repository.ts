import { Injectable } from '@nestjs/common';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { Connection, Repository } from 'typeorm';
import { BazaNcIntegrationSubscriptionEntity } from '../entities/baza-nc-integration-subscription.entity';
import { BazaNcIntegrationSubscribeStatus } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';

export const BAZA_NC_INTEGRATION_SUBSCRIPTION_RELATIONS = [
    'account',
    'listing',
];

export interface BazaNcIntegrationSubscriptionContext {
    account: AccountEntity;
    listing: BazaNcIntegrationListingsEntity;
}

@Injectable()
export class BazaNcIntegrationSubscriptionRepository {
    constructor(
        private readonly connection: Connection,
    ) {}

    get repository(): Repository<BazaNcIntegrationSubscriptionEntity> {
        return this.connection.getRepository(BazaNcIntegrationSubscriptionEntity);
    }

    async save(entity: BazaNcIntegrationSubscriptionEntity): Promise<void> {
        await this.repository.save(entity);
    }

    async hasSubscription(context: BazaNcIntegrationSubscriptionContext): Promise<boolean> {
        return !! (await this.repository.findOne({
            where: [{
                account: context.account,
                listing: context.listing,
            }],
            relations: BAZA_NC_INTEGRATION_SUBSCRIPTION_RELATIONS,
        }));
    }

    async touchSubscription(context: BazaNcIntegrationSubscriptionContext): Promise<BazaNcIntegrationSubscriptionEntity> {
        if (await this.hasSubscription(context)) {
            return this.repository.findOne({
                where: [{
                    account: context.account,
                    listing: context.listing,
                }],
                relations: BAZA_NC_INTEGRATION_SUBSCRIPTION_RELATIONS,
            });
        } else {
            const entity = new BazaNcIntegrationSubscriptionEntity();

            entity.status = BazaNcIntegrationSubscribeStatus.NotSubscribed;
            entity.listing = context.listing;
            entity.account = context.account;

            await this.repository.save(entity);

            return entity;
        }
    }

    async setSubscriptionStatus(context: BazaNcIntegrationSubscriptionContext, newStatus: BazaNcIntegrationSubscribeStatus): Promise<BazaNcIntegrationSubscriptionEntity> {
        const entity = await this.touchSubscription(context);

        entity.status = newStatus;

        await this.repository.save(entity);

        return entity;
    }

    async numSubscriptionsOfListing(listing: BazaNcIntegrationListingsEntity | number): Promise<number> {
        return this.repository.count({
            where: [{
                listing,
                status: BazaNcIntegrationSubscribeStatus.Subscribed,
            }],
        });
    }

    async findActiveSubscriptionsOfListing(listing: BazaNcIntegrationListingsEntity | number): Promise<Array<BazaNcIntegrationSubscriptionEntity>> {
        return this.repository.find({
            where: [{
                listing,
                status: BazaNcIntegrationSubscribeStatus.Subscribed,
            }],
            relations: BAZA_NC_INTEGRATION_SUBSCRIPTION_RELATIONS,
        });
    }
}
