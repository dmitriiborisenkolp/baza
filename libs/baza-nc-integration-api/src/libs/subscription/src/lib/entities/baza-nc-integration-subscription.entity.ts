import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSubscribeMessages, BazaNcIntegrationSubscribeStatus } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';

@Entity()
export class BazaNcIntegrationSubscriptionEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @JoinColumn()
    @ManyToOne(() => BazaNcIntegrationListingsEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    listing: BazaNcIntegrationListingsEntity;

    @JoinColumn()
    @ManyToOne(() => AccountEntity, {
        cascade: true,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    account: AccountEntity;

    @Column({
        type: 'varchar',
    })
    status: BazaNcIntegrationSubscribeStatus;

    @Column({
        type: 'json',
        default: [],
    })
    sent: Array<BazaNcIntegrationSubscribeMessages> = [];
}
