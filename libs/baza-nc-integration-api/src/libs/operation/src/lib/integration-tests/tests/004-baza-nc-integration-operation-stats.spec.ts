import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationOperationNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcAccountVerificationFixtures, BazaNcApiOfferingFixtures } from '@scaliolabs/baza-nc-api';
import { BazaDwollaPaymentFixtures } from '@scaliolabs/baza-dwolla-api';
import { BazaNcOperationType } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-integration-api/operation/004-baza-nc-integration-operation-stats.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcIntegrationOperationNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaDwollaPaymentFixtures.BazaDwollaPaymentSampleFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will returns operation stats for current investor', async () => {
        const response = await dataAccess.stats();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.total).toBe(12);

        expect(response.types.find((next) => next.type === BazaNcOperationType.Investment)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Investment).total).toBe(0);

        expect(response.types.find((next) => next.type === BazaNcOperationType.Dividend)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Dividend).total).toBe(0);

        expect(response.types.find((next) => next.type === BazaNcOperationType.Transfer)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Transfer).total).toBe(6);

        expect(response.types.find((next) => next.type === BazaNcOperationType.Withdraw)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Withdraw).total).toBe(6);
    });
});
