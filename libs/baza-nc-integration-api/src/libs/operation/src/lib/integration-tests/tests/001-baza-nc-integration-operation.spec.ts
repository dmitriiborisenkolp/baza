import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaDwollaPaymentFixtures } from '@scaliolabs/baza-dwolla-api';
import {
    BazaNcOfferingCmsNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    AccountTypeCheckingSaving,
    BazaNcOperationDirection,
    BazaNcOperationPayloadDividendDto,
    BazaNcOperationPayloadInvestmentDto,
    BazaNcOperationPayloadTransferDto,
    BazaNcOperationPayloadWithdrawDto,
    BazaNcOperationType,
    BazaNcPurchaseFlowTransactionType,
    OfferingId,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';
import { BazaNcAccountVerificationFixtures, BazaNcApiOfferingFixtures, BazaNcDividendFixtures } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationOperationNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-integration-api/operation/001-baza-nc-integration-operation.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcIntegrationOperationNodeAccess(http);
    const dataAccessOfferingCms = new BazaNcOfferingCmsNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessPurchaseFlowSession = new BazaNcPurchaseFlowNodeAccess(http);

    let offeringId: OfferingId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaDwollaPaymentFixtures.BazaDwollaPaymentSampleFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will fetch offering', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessOfferingCms.listAll();

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.length).toBe(1);

        offeringId = response[0].ncOfferingId;
    });

    it('will fetch operations (Page 1)', async () => {
        const response = await dataAccess.list({
            index: 1,
            size: 6,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(12);

        expect(
            response.items.map((next) => ({
                amount: next.amount,
                amountCents: next.amountCents,
                direction: next.direction,
                payload: {
                    type: next.payload.type,
                    canBeReprocessed: next.payload.canBeReprocessed,
                },
            })),
        ).toEqual([
            {
                amount: '159.00',
                amountCents: 15900,
                direction: BazaNcOperationDirection.Out,
                payload: {
                    type: BazaNcOperationType.Withdraw,
                    canBeReprocessed: true,
                },
            },
            {
                amount: '258.00',
                amountCents: 25800,
                direction: BazaNcOperationDirection.Out,
                payload: {
                    type: BazaNcOperationType.Withdraw,
                    canBeReprocessed: false,
                },
            },
            {
                amount: '257.00',
                amountCents: 25700,
                direction: BazaNcOperationDirection.In,
                payload: {
                    type: BazaNcOperationType.Transfer,
                    canBeReprocessed: false,
                },
            },
            {
                amount: '156.00',
                amountCents: 15600,
                direction: BazaNcOperationDirection.Out,
                payload: {
                    type: BazaNcOperationType.Withdraw,
                    canBeReprocessed: false,
                },
            },
            {
                amount: '54.00',
                amountCents: 5400,
                direction: BazaNcOperationDirection.In,
                payload: {
                    type: BazaNcOperationType.Transfer,
                    canBeReprocessed: false,
                },
            },
            {
                amount: '53.00',
                amountCents: 5300,
                direction: BazaNcOperationDirection.Out,
                payload: {
                    type: BazaNcOperationType.Withdraw,
                    canBeReprocessed: false,
                },
            },
        ]);

        expect((response.items[0].payload as BazaNcOperationPayloadWithdrawDto).ulid).toBeDefined();
        expect((response.items[1].payload as BazaNcOperationPayloadWithdrawDto).ulid).toBeDefined();
        expect((response.items[2].payload as BazaNcOperationPayloadTransferDto).ulid).toBeDefined();
        expect((response.items[3].payload as BazaNcOperationPayloadWithdrawDto).ulid).toBeDefined();
        expect((response.items[4].payload as BazaNcOperationPayloadTransferDto).ulid).toBeDefined();
        expect((response.items[5].payload as BazaNcOperationPayloadWithdrawDto).ulid).toBeDefined();

        expect((response.items[0].payload as BazaNcOperationPayloadWithdrawDto).status).toBeDefined();
        expect((response.items[1].payload as BazaNcOperationPayloadWithdrawDto).status).toBeDefined();
        expect((response.items[2].payload as BazaNcOperationPayloadTransferDto).status).toBeDefined();
        expect((response.items[3].payload as BazaNcOperationPayloadWithdrawDto).status).toBeDefined();
        expect((response.items[4].payload as BazaNcOperationPayloadTransferDto).status).toBeDefined();
        expect((response.items[5].payload as BazaNcOperationPayloadWithdrawDto).status).toBeDefined();
    });

    it('will fetch operations (Page 2)', async () => {
        const response = await dataAccess.list({
            index: 2,
            size: 6,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(12);

        expect(
            response.items.map((next) => ({
                amount: next.amount,
                amountCents: next.amountCents,
                direction: next.direction,
                payload: {
                    type: next.payload.type,
                    canBeReprocessed: next.payload.canBeReprocessed,
                },
            })),
        ).toEqual([
            {
                amount: '152.00',
                amountCents: 15200,
                direction: BazaNcOperationDirection.In,
                payload: {
                    type: BazaNcOperationType.Transfer,
                    canBeReprocessed: true,
                },
            },
            {
                amount: '351.00',
                amountCents: 35100,
                direction: BazaNcOperationDirection.In,
                payload: {
                    type: BazaNcOperationType.Transfer,
                    canBeReprocessed: false,
                },
            },
            {
                amount: '59.00',
                amountCents: 5900,
                direction: BazaNcOperationDirection.Out,
                payload: {
                    type: BazaNcOperationType.Withdraw,
                    canBeReprocessed: false,
                },
            },
            {
                amount: '158.00',
                amountCents: 15800,
                direction: BazaNcOperationDirection.In,
                payload: {
                    type: BazaNcOperationType.Transfer,
                    canBeReprocessed: false,
                },
            },
            {
                amount: '55.00',
                amountCents: 5500,
                direction: BazaNcOperationDirection.In,
                payload: {
                    type: BazaNcOperationType.Transfer,
                    canBeReprocessed: false,
                },
            },
            {
                amount: '353.00',
                amountCents: 35300,
                direction: BazaNcOperationDirection.Out,
                payload: {
                    type: BazaNcOperationType.Withdraw,
                    canBeReprocessed: false,
                },
            },
        ]);

        expect((response.items[0].payload as BazaNcOperationPayloadTransferDto).ulid).toBeDefined();
        expect((response.items[1].payload as BazaNcOperationPayloadTransferDto).ulid).toBeDefined();
        expect((response.items[2].payload as BazaNcOperationPayloadWithdrawDto).ulid).toBeDefined();
        expect((response.items[3].payload as BazaNcOperationPayloadTransferDto).ulid).toBeDefined();
        expect((response.items[4].payload as BazaNcOperationPayloadTransferDto).ulid).toBeDefined();
        expect((response.items[5].payload as BazaNcOperationPayloadWithdrawDto).ulid).toBeDefined();

        expect((response.items[0].payload as BazaNcOperationPayloadTransferDto).status).toBeDefined();
        expect((response.items[1].payload as BazaNcOperationPayloadTransferDto).status).toBeDefined();
        expect((response.items[2].payload as BazaNcOperationPayloadWithdrawDto).status).toBeDefined();
        expect((response.items[3].payload as BazaNcOperationPayloadTransferDto).status).toBeDefined();
        expect((response.items[4].payload as BazaNcOperationPayloadTransferDto).status).toBeDefined();
        expect((response.items[5].payload as BazaNcOperationPayloadWithdrawDto).status).toBeDefined();
    });

    it('will process dividends', async () => {
        const response = await dataAccessE2eFixtures.up({
            fixtures: [BazaNcDividendFixtures.BazaNcDividendProcessedTransactionFixture],
            specFile: __filename,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will fetch operations with dividends', async () => {
        const response = await dataAccess.list({
            index: 3,
            size: 6,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(16);

        expect(
            response.items.map((next) => ({
                amount: next.amount,
                amountCents: next.amountCents,
                direction: next.direction,
                payload: {
                    type: next.payload.type,
                    canBeReprocessed: next.payload.canBeReprocessed,
                    offeringTitle: (next.payload as BazaNcOperationPayloadDividendDto).offeringTitle,
                    amount: (next.payload as BazaNcOperationPayloadDividendDto).amount,
                },
            })),
        ).toEqual([
            {
                direction: BazaNcOperationDirection.In,
                amount: '12.00',
                amountCents: 1200,
                payload: {
                    type: BazaNcOperationType.Dividend,
                    canBeReprocessed: false,
                    offeringTitle: 'E2E NC Offering Fixture',
                    amount: '12.00',
                },
            },
            {
                direction: BazaNcOperationDirection.In,
                amount: '10.00',
                amountCents: 1000,
                payload: {
                    type: BazaNcOperationType.Dividend,
                    canBeReprocessed: false,
                    offeringTitle: 'E2E NC Offering Fixture',
                    amount: '10.00',
                },
            },
            {
                direction: BazaNcOperationDirection.In,
                amount: '14.97',
                amountCents: 1497,
                payload: {
                    type: BazaNcOperationType.Dividend,
                    canBeReprocessed: false,
                    offeringTitle: 'E2E NC Offering Fixture',
                    amount: '14.97',
                },
            },
            {
                direction: BazaNcOperationDirection.In,
                amount: '12.50',
                amountCents: 1250,
                payload: {
                    type: BazaNcOperationType.Dividend,
                    canBeReprocessed: false,
                    offeringTitle: 'E2E NC Offering Fixture',
                    amount: '12.50',
                },
            },
        ]);

        expect((response.items[0].payload as BazaNcOperationPayloadDividendDto).ulid).toBeDefined();
        expect((response.items[1].payload as BazaNcOperationPayloadDividendDto).ulid).toBeDefined();
        expect((response.items[2].payload as BazaNcOperationPayloadDividendDto).ulid).toBeDefined();
        expect((response.items[3].payload as BazaNcOperationPayloadDividendDto).ulid).toBeDefined();

        expect((response.items[0].payload as BazaNcOperationPayloadDividendDto).status).toBeDefined();
        expect((response.items[1].payload as BazaNcOperationPayloadDividendDto).status).toBeDefined();
        expect((response.items[2].payload as BazaNcOperationPayloadDividendDto).status).toBeDefined();
        expect((response.items[3].payload as BazaNcOperationPayloadDividendDto).status).toBeDefined();
    });

    it('will successfully purchase share', async () => {
        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse)).toBeFalsy();

        const sessionResponse = await dataAccessPurchaseFlowSession.session({
            offeringId,
            amount: 2000,
            numberOfShares: 2,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(sessionResponse)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlowSession.submit({
            id: sessionResponse.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display a Purchase on first page', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccess.list({
                    index: 1,
                    size: 1,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.pager.total).toBe(17);

                expect(
                    response.items.map((next) => ({
                        amount: next.amount,
                        amountCents: next.amountCents,
                        direction: next.direction,
                        payload: {
                            type: next.payload.type,
                            canBeReprocessed: next.payload.canBeReprocessed,
                            state: (next.payload as BazaNcOperationPayloadInvestmentDto).state,
                            shares: (next.payload as BazaNcOperationPayloadInvestmentDto).shares,
                            feeCents: (next.payload as BazaNcOperationPayloadInvestmentDto).feeCents,
                            pricePerShareCents: (next.payload as BazaNcOperationPayloadInvestmentDto).pricePerShareCents,
                            totalCents: (next.payload as BazaNcOperationPayloadInvestmentDto).totalCents,
                            transactionFeesCents: (next.payload as BazaNcOperationPayloadInvestmentDto).transactionFeesCents,
                        },
                    })),
                ).toEqual([
                    {
                        amount: '20.00',
                        amountCents: 2000,
                        direction: BazaNcOperationDirection.Out,
                        payload: {
                            type: BazaNcOperationType.Investment,
                            canBeReprocessed: false,
                            state: TransactionState.PendingPayment,
                            shares: 2,
                            feeCents: 0,
                            pricePerShareCents: 1000,
                            totalCents: 2000,
                            transactionFeesCents: 0,
                        },
                    },
                ]);

                expect((response.items[0].payload as BazaNcOperationPayloadInvestmentDto).id).toBeDefined();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will filter by operation type', async () => {
        const response = await dataAccess.list({
            index: 1,
            size: 10,
            types: [BazaNcOperationType.Investment],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(1);

        expect(
            response.items.map((next) => ({
                amount: next.amount,
                amountCents: next.amountCents,
                direction: next.direction,
                payload: {
                    type: next.payload.type,
                    canBeReprocessed: next.payload.canBeReprocessed,
                    state: (next.payload as BazaNcOperationPayloadInvestmentDto).state,
                    shares: (next.payload as BazaNcOperationPayloadInvestmentDto).shares,
                    feeCents: (next.payload as BazaNcOperationPayloadInvestmentDto).feeCents,
                    pricePerShareCents: (next.payload as BazaNcOperationPayloadInvestmentDto).pricePerShareCents,
                    totalCents: (next.payload as BazaNcOperationPayloadInvestmentDto).totalCents,
                    transactionFeesCents: (next.payload as BazaNcOperationPayloadInvestmentDto).transactionFeesCents,
                },
            })),
        ).toEqual([
            {
                amount: '20.00',
                amountCents: 2000,
                direction: BazaNcOperationDirection.Out,
                payload: {
                    type: BazaNcOperationType.Investment,
                    canBeReprocessed: false,
                    state: TransactionState.PendingPayment,
                    shares: 2,
                    feeCents: 0,
                    pricePerShareCents: 1000,
                    totalCents: 2000,
                    transactionFeesCents: 0,
                },
            },
        ]);

        expect((response.items[0].payload as BazaNcOperationPayloadInvestmentDto).id).toBeDefined();
    });

    it('will filter by operation types (multiple, count only)', async () => {
        const response = await dataAccess.list({
            index: 1,
            size: 10,
            types: [BazaNcOperationType.Investment, BazaNcOperationType.Dividend],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(5);
    });
});
