import { Injectable } from '@nestjs/common';
import { BazaAccountCmsService, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaService } from '../../../../../schema/src';
import { BazaNcIntegrationListingsCmsService, BazaNcIntegrationListingsCmsStatusService } from '../../../../../listings/src';
import {
    BazaNcIntegrationListingsCmsCreateRequest,
    BazaNcIntegrationSchemaCmsCreateRequest,
    BazaNcIntegrationSchemaType,
} from '@scaliolabs/baza-nc-integration-shared';
import { e2eExampleAttachment } from '@scaliolabs/baza-core-shared';
import { BazaNcOfferingStatus, NC_OFFERING_E2E } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationOperationFixtures } from '../baza-nc-integration-operation.fixtures';

const FIXTURE: Array<{
    schema: BazaNcIntegrationSchemaCmsCreateRequest;
    listings: Array<BazaNcIntegrationListingsCmsCreateRequest>;
}> = [
    {
        schema: {
            title: 'schema-a',
            displayName: 'Schema A',
            published: true,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'text',
                        title: 'Example Text Field',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Select,
                    payload: {
                        field: 'select',
                        title: 'Example Select Field',
                        required: false,
                        isDisplayedInDetails: true,
                        values: {
                            foo: 'Foo',
                            bar: 'Bar',
                            baz: 'Baz',
                        },
                    },
                },
            ],
        },
        listings: [
            {
                isPublished: true,
                title: 'Example Listing 1',
                description: 'Example Listing Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 1,
                offering: {
                    ncTargetAmount: 1000,
                    ncMinAmount: 10,
                    ncMaxAmount: 1000,
                    ncCurrentValueCents: 1000,
                    ncMaxSharesPerAccount: 10,
                    ncUnitPrice: 10,
                    ncStartDate: '01-01-2021',
                    ncEndDate: '01-01-2022',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Poseidon',
                    select: 'Foo',
                },
                statements: [],
            },
            {
                isPublished: false,
                title: 'Example Listing 2',
                description: 'Example Listing Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 1,
                offering: {
                    ncTargetAmount: 100,
                    ncMinAmount: 10,
                    ncMaxAmount: 100,
                    ncCurrentValueCents: 100,
                    ncMaxSharesPerAccount: 15,
                    ncUnitPrice: 25,
                    ncStartDate: '02-03-2020',
                    ncEndDate: '02-05-2023',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Athena',
                    select: 'Bar',
                },
                statements: [],
            },
            {
                isPublished: true,
                title: 'Example Listing 3',
                description: 'Example Listing Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 10,
                offering: {
                    ncTargetAmount: 5000,
                    ncMinAmount: 150,
                    ncMaxAmount: 5000,
                    ncCurrentValueCents: 5000,
                    ncMaxSharesPerAccount: 5,
                    ncUnitPrice: 50,
                    ncStartDate: '02-01-2021',
                    ncEndDate: '10-01-2023',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Hermes',
                },
                statements: [],
            },
        ],
    },
    {
        schema: {
            title: 'schema-b',
            displayName: 'Schema B',
            published: true,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'text',
                        title: 'Example Text Field',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Select,
                    payload: {
                        field: 'select',
                        title: 'Example Select Field',
                        required: false,
                        isDisplayedInDetails: true,
                        values: {
                            foo: 'Foo',
                            bar: 'Bar',
                            baz: 'Baz',
                        },
                    },
                },
            ],
        },
        listings: [
            {
                isPublished: true,
                title: 'Example Listing 1',
                description: 'Example Listing Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 1,
                offering: {
                    ncTargetAmount: 1000,
                    ncMinAmount: 10,
                    ncMaxAmount: 1000,
                    ncCurrentValueCents: 1000,
                    ncMaxSharesPerAccount: 10,
                    ncUnitPrice: 10,
                    ncStartDate: '01-01-2021',
                    ncEndDate: '01-01-2022',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Poseidon',
                    select: 'Foo',
                },
                statements: [],
            },
            {
                isPublished: false,
                title: 'Example Listing 2',
                description: 'Example Listing Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 1,
                offering: {
                    ncTargetAmount: 100,
                    ncMinAmount: 10,
                    ncMaxAmount: 100,
                    ncCurrentValueCents: 100,
                    ncMaxSharesPerAccount: 15,
                    ncUnitPrice: 25,
                    ncStartDate: '02-03-2020',
                    ncEndDate: '02-05-2023',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Athena',
                    select: 'Bar',
                },
                statements: [],
            },
            {
                isPublished: true,
                title: 'Example Listing 3',
                description: 'Example Listing Description',
                cover: e2eExampleAttachment(),
                images: [e2eExampleAttachment()],
                sharesToJoinEliteInvestors: 10,
                offering: {
                    ncTargetAmount: 5000,
                    ncMinAmount: 150,
                    ncMaxAmount: 5000,
                    ncCurrentValueCents: 5000,
                    ncMaxSharesPerAccount: 5,
                    ncUnitPrice: 50,
                    ncStartDate: '02-01-2021',
                    ncEndDate: '10-01-2023',
                    ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                },
                metadata: {},
                properties: {
                    text: 'Hermes',
                },
                statements: [],
            },
        ],
    },
    {
        schema: {
            title: 'schema-c',
            displayName: 'Schema C',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'text',
                        title: 'Example Text Field',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Select,
                    payload: {
                        field: 'select',
                        title: 'Example Select Field',
                        required: false,
                        isDisplayedInDetails: true,
                        values: {
                            foo: 'Foo',
                            bar: 'Bar',
                            baz: 'Baz',
                        },
                    },
                },
            ],
        },
        listings: [],
    },
];

@Injectable()
export class BazaNcIntegrationOperationCaseAFixture implements BazaE2eFixture {
    constructor(
        private readonly account: BazaAccountCmsService,
        private readonly schema: BazaNcIntegrationSchemaService,
        private readonly listings: BazaNcIntegrationListingsCmsService,
        private readonly statusService: BazaNcIntegrationListingsCmsStatusService,
    ) {}

    async up(): Promise<void> {
        const account = (await this.account.listAdminAccounts())[0];

        for (const fixtureRequest of FIXTURE) {
            const schema = await this.schema.create(fixtureRequest.schema);

            for (const listingFixtureRequest of fixtureRequest.listings) {
                const result = await this.listings.create(account, {
                    ...listingFixtureRequest,
                    schemaId: schema.id,
                });

                await this.statusService.changeStatus({
                    id: result.id,
                    newStatus: BazaNcOfferingStatus.Open,
                });
            }
        }
    }
}

defineE2eFixtures([
    {
        fixture: BazaNcIntegrationOperationFixtures.BazaNcIntegrationOperationCaseAFixture,
        provider: BazaNcIntegrationOperationCaseAFixture,
    },
]);
