import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationListingsNodeAccess, BazaNcIntegrationOperationNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import { BazaDwollaPaymentFixtures } from '@scaliolabs/baza-dwolla-api';
import { AccountTypeCheckingSaving, BazaNcOperationType } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcIntegrationOperationFixtures } from '../baza-nc-integration-operation.fixtures';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-integration-api/operation/005-baza-nc-integration-operation-filters.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccess = new BazaNcIntegrationOperationNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaDwollaPaymentFixtures.BazaDwollaPaymentSampleFixture,
                BazaNcIntegrationOperationFixtures.BazaNcIntegrationOperationCaseAFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will returns filters for current investor', async () => {
        const response = await dataAccess.filters();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.types.map((next) => next.type)).toEqual([
            BazaNcOperationType.Investment,
            BazaNcOperationType.Dividend,
            BazaNcOperationType.Withdraw,
            BazaNcOperationType.Transfer,
        ]);

        expect(response.schemas).toEqual(['Schema A', 'Schema B']);
        expect(response.listings.length).toBe(0);

        expect(response.types.find((next) => next.type === BazaNcOperationType.Investment)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Investment).count).toBe(0);

        expect(response.types.find((next) => next.type === BazaNcOperationType.Dividend)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Dividend).count).toBe(0);

        expect(response.types.find((next) => next.type === BazaNcOperationType.Transfer)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Transfer).count).toBe(6);

        expect(response.types.find((next) => next.type === BazaNcOperationType.Withdraw)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Withdraw).count).toBe(6);
    });

    it('will purchase 1 share of Example Listings 1', async () => {
        const listings = await dataAccessListings.list({});

        expect(isBazaErrorResponse(listings)).toBeFalsy();

        const listing = listings.items.find((next) => next.title === 'Example Listing 1');

        expect(listing).toBeDefined();

        const session = await dataAccessPurchaseFlow.session({
            numberOfShares: 1,
            offeringId: listing.offeringId,
            requestDocuSignUrl: false,
            amount: 1000,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const bankAccount = dataAccessBankAccount.setBankAccount({
            accountName: 'FOO BAR',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(bankAccount)).toBeFalsy();

        const submit = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submit)).toBeFalsy();
    });

    it('will returns filters for current investor with investment item', async () => {
        const response = await dataAccess.filters();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.types.map((next) => next.type)).toEqual([
            BazaNcOperationType.Investment,
            BazaNcOperationType.Dividend,
            BazaNcOperationType.Withdraw,
            BazaNcOperationType.Transfer,
        ]);

        expect(response.schemas).toEqual(['Schema A', 'Schema B']);
        expect(response.listings.length).toBe(1);
        expect(response.listings[0].title).toEqual('Example Listing 1');

        expect(response.count).toBe(13);
        expect(response.types.find((next) => next.type === BazaNcOperationType.Investment)).toBeDefined();
        expect(response.types.find((next) => next.type === BazaNcOperationType.Investment).count).toBe(1);
    });
});
