import { Injectable } from '@nestjs/common';
import { BazaNcInvestorAccountEntity, BazaNcOperationService } from '@scaliolabs/baza-nc-api';
import {
    BazaNcIntegrationOperationFiltersDto,
    BazaNcIntegrationOperationListRequest,
    BazaNcSearchFields,
    BazaNcSearchOperator,
} from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcDividendPaymentStatus,
    BazaNcOperationListResponse,
    BazaNcOperationType,
    bazaNcOperationTypeAliases,
    OfferingId,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';
import { BazaDwollaPaymentStatus } from '@scaliolabs/baza-dwolla-shared';
import { BazaNcIntegrationSchemaRepository, BazaNcIntegrationSchemaService } from '../../../../schema/src';
import { BazaNcIntegrationListingsRepository } from '../../../../listings/src';
import { BazaNcIntegrationSearchService } from '../../../../search/src';
import * as _ from 'lodash';

@Injectable()
export class BazaNcIntegrationOperationService {
    constructor(
        private readonly service: BazaNcOperationService,
        private readonly schemaService: BazaNcIntegrationSchemaService,
        private readonly schemaRepository: BazaNcIntegrationSchemaRepository,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
        private readonly searchService: BazaNcIntegrationSearchService,
    ) {}

    /**
     * Returns Filters & Other Settings for Operations FE Implementation
     * @param investorAccount
     */
    async filters(investorAccount: BazaNcInvestorAccountEntity): Promise<BazaNcIntegrationOperationFiltersDto> {
        const stats = await this.service.stats(investorAccount);
        const listings = (await this.listingRepository.findManyPublishedByOfferingIds(stats.offeringIds)).map((next) => ({
            title: next.title,
            offeringId: next.offeringId,
            schema: next.schema,
        }));

        return {
            listings,
            canFilterByListings: [BazaNcOperationType.Investment, BazaNcOperationType.Dividend],
            canFilterBySchema: [BazaNcOperationType.Investment, BazaNcOperationType.Dividend],
            canFilterByStatuses: [
                {
                    type: BazaNcOperationType.Investment,
                    statuses: Object.values(TransactionState).filter((next) => next !== TransactionState.None),
                },
                {
                    type: BazaNcOperationType.Dividend,
                    statuses: Object.values(BazaNcDividendPaymentStatus),
                },
                {
                    type: BazaNcOperationType.Withdraw,
                    statuses: Object.values(BazaDwollaPaymentStatus),
                },
                {
                    type: BazaNcOperationType.Transfer,
                    statuses: Object.values(BazaDwollaPaymentStatus),
                },
            ],
            count: stats.total,
            schemas: await this.schemaService.listPublicSchema(),
            types: Object.values(BazaNcOperationType).map((type) => ({
                type,
                // eslint-disable-next-line security/detect-object-injection
                alias: bazaNcOperationTypeAliases[type],
                count: stats.types.find((next) => next.type === type)?.total || 0,
                listings: (() => {
                    const offeringIds = stats.types.find((next) => next.type === type).offeringIds;

                    if (offeringIds && offeringIds.length > 0) {
                        return listings.filter((listing) => offeringIds.includes(listing.offeringId));
                    } else {
                        return [];
                    }
                })(),
                schemas: (() => {
                    const offeringIds = stats.types.find((next) => next.type === type).offeringIds;

                    if (offeringIds && offeringIds.length > 0) {
                        return listings.filter((listing) => offeringIds.includes(listing.offeringId)).map((listing) => listing.schema);
                    } else {
                        return [];
                    }
                })(),
            })),
        };
    }

    /**
     * Returns operations search results
     * @param investorAccount
     * @param request
     */
    async list(
        investorAccount: BazaNcInvestorAccountEntity,
        request: BazaNcIntegrationOperationListRequest,
    ): Promise<BazaNcOperationListResponse> {
        let offeringIds: Array<OfferingId> = request.offeringIds;

        if (Array.isArray(request.listings) && request.listings.length > 0) {
            const searchResponse = await this.searchService.searchOfferingIds({
                filter: [
                    {
                        type: BazaNcSearchOperator.In,
                        field: BazaNcSearchFields.Title,
                        values: request.listings,
                    },
                ],
            });

            offeringIds = offeringIds === undefined ? searchResponse : _.intersection(offeringIds, searchResponse);
        }

        if (Array.isArray(request.schema) && request.schema.length > 0) {
            const schemaIds = await this.schemaRepository.findAllPublishedSchemaIdsByDisplayNames(request.schema);

            const searchResponse = await this.searchService.searchOfferingIds({
                filter: [
                    {
                        type: BazaNcSearchOperator.In,
                        field: BazaNcSearchFields.SchemaId,
                        values: schemaIds,
                    },
                ],
            });

            offeringIds = offeringIds === undefined ? searchResponse : _.intersection(offeringIds, searchResponse);
        }

        return this.service.list(investorAccount, {
            ...request,
            offeringIds,
        });
    }
}
