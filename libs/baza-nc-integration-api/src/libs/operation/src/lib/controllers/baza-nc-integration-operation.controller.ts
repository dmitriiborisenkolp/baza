import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiExtraModels, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcInvestorAccountEntity,
    BazaNcOperationService,
    InvestorAccountOptionalGuard,
    ReqInvestorAccount,
} from '@scaliolabs/baza-nc-api';
import {
    BazaNcIntegrationOperationEndpoint,
    BazaNcIntegrationOperationEndpointPaths,
    BazaNcIntegrationOperationFiltersDto,
    bazaNcIntegrationOperationFiltersStatusesModels,
    BazaNcIntegrationOperationListRequest,
    BazaNcIntegrationPublicOpenApi,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcOperationListResponse, BazaNcOperationStatsResponse, BazaNcOperationType } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationOperationService } from '../services/baza-nc-integration-operation.service';
import { bazaEmptyCrudListResponse } from '@scaliolabs/baza-core-shared';

@Controller()
@ApiBearerAuth()
@UseGuards(InvestorAccountOptionalGuard)
@ApiTags(BazaNcIntegrationPublicOpenApi.Operation)
@ApiExtraModels(...bazaNcIntegrationOperationFiltersStatusesModels)
export class BazaNcIntegrationOperationController implements BazaNcIntegrationOperationEndpoint {
    constructor(private readonly service: BazaNcIntegrationOperationService, private readonly baseService: BazaNcOperationService) {}

    @Get(BazaNcIntegrationOperationEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns operations search results',
    })
    @ApiOkResponse({
        type: BazaNcOperationListResponse,
    })
    async list(
        @Query() request: BazaNcIntegrationOperationListRequest,
        @ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity | undefined,
    ): Promise<BazaNcOperationListResponse> {
        if (!investorAccount) {
            return bazaEmptyCrudListResponse();
        }

        return this.service.list(investorAccount, request);
    }

    @Get(BazaNcIntegrationOperationEndpointPaths.stats)
    @ApiOperation({
        summary: 'stats',
        description: 'Operation stats for Investor Account',
    })
    @ApiOkResponse({
        type: BazaNcOperationStatsResponse,
    })
    async stats(@ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity): Promise<BazaNcOperationStatsResponse> {
        if (!investorAccount) {
            return {
                total: 0,
                types: Object.values(BazaNcOperationType).map((type) => ({
                    type,
                    total: 0,
                    offeringIds: [],
                })),
                offeringIds: [],
            };
        }

        return this.baseService.stats(investorAccount);
    }

    @Get(BazaNcIntegrationOperationEndpointPaths.filters)
    @ApiOperation({
        summary: 'filters',
        description: 'Returns filters & other settings which could be used to fully automate FE implementation',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationOperationFiltersDto,
    })
    async filters(@ReqInvestorAccount() investorAccount: BazaNcInvestorAccountEntity): Promise<BazaNcIntegrationOperationFiltersDto> {
        return this.service.filters(investorAccount);
    }
}
