import { forwardRef, Module } from '@nestjs/common';
import { BazaNcIntegrationOperationCaseAFixture } from './integration-tests/fixtures/baza-nc-integration-operation-case-a.fixture';
import { BazaNcIntegrationListingsApiModule } from '../../../listings/src';
import { BazaNcIntegrationSchemaApiModule } from '../../../schema/src/lib/baza-nc-integration-schema-api.module';

const E2E_FIXTURES = [BazaNcIntegrationOperationCaseAFixture];

@Module({
    imports: [forwardRef(() => BazaNcIntegrationListingsApiModule), forwardRef(() => BazaNcIntegrationSchemaApiModule)],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcIntegrationOperationE2eModule {}
