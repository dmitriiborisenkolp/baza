import { Global, Module } from '@nestjs/common';
import { BazaNcOperationApiModule } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationOperationController } from './controllers/baza-nc-integration-operation.controller';
import { BazaNcIntegrationSchemaApiModule } from '../../../schema/src';
import { BazaNcIntegrationSearchApiModule } from '../../../search/src';
import { BazaNcIntegrationOperationService } from './services/baza-nc-integration-operation.service';

@Global()
@Module({
    imports: [BazaNcOperationApiModule, BazaNcIntegrationSchemaApiModule, BazaNcIntegrationSearchApiModule],
    controllers: [BazaNcIntegrationOperationController],
    providers: [BazaNcIntegrationOperationService],
    exports: [BazaNcIntegrationOperationService],
})
export class BazaNcIntegrationOperationApiModule {}
