import { Module } from '@nestjs/common';
import { BazaNcTransactionApiModule } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationListingsApiModule } from '../../../listings/src';
import { BazaNcIntegrationInvestmentsController } from './controllers/baza-nc-integration-investments.controller';
import { BazaNcIntegrationInvestmentsService } from './services/baza-nc-integration-investments.service';

@Module({
    imports: [
        BazaNcTransactionApiModule,
        BazaNcIntegrationListingsApiModule,
    ],
    controllers: [
        BazaNcIntegrationInvestmentsController,
    ],
    providers: [
        BazaNcIntegrationInvestmentsService,
    ],
    exports: [
        BazaNcIntegrationInvestmentsService,
    ],
})
export class BazaNcIntegrationInvestmentsModule {}
