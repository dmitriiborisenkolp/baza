import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationInvestmentDto,
    BazaNcIntegrationInvestmentsEndpoint,
    BazaNcIntegrationInvestmentsEndpointPaths,
    BazaNcIntegrationPublicOpenApi,
    BazaNcIntegrationsInvestmentsListRequest,
    BazaNcIntegrationsInvestmentsListResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { AccountEntity, AuthGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationInvestmentsService } from '../services/baza-nc-integration-investments.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNcIntegrationPublicOpenApi.Investments)
@UseGuards(AuthGuard)
export class BazaNcIntegrationInvestmentsController implements BazaNcIntegrationInvestmentsEndpoint {
    constructor(private readonly service: BazaNcIntegrationInvestmentsService) {}

    @Post(BazaNcIntegrationInvestmentsEndpointPaths.investments)
    @ApiOperation({
        summary: 'investments',
        description: 'Returns investments list',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationInvestmentDto,
        isArray: true,
    })
    async investments(
        @Body() request: BazaNcIntegrationsInvestmentsListRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationsInvestmentsListResponse> {
        return this.service.listInvestmentsOfAccount(account, request, {
            aggregate: 'listings-and-states',
            reverse: false,
        });
    }
}
