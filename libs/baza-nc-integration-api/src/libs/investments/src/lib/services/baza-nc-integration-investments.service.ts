import { Injectable } from '@nestjs/common';
import { AccountEntity, BAZA_CRUD_DEFAULT_SIZE } from '@scaliolabs/baza-core-api';
import {
    TransactionState,
    TRANSACTION_INVESTMENTS_COUNTABLE_STATES,
    convertFromCents,
    TRANSACTION_INVESTMENTS_RETURNED_STATES,
    TRANSACTION_INVESTMENTS_COUNTABLE_STATES_GROUPS,
    TRANSACTION_INVESTMENTS_COUNTABLE_STATES_GROUPS_MAIN_CARD,
} from '@scaliolabs/baza-nc-shared';
import { CrudListRequestDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { paginate } from '@scaliolabs/baza-core-shared';
import { BazaNcTransactionRepository, BazaNcTransactionMapper } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationListingsMapper, BazaNcIntegrationListingsRepository } from '../../../../listings/src';
import { BazaNcIntegrationInvestmentDto } from '@scaliolabs/baza-nc-integration-shared';
import * as _ from 'lodash';

/**
 * Investments of Investor Service
 */
@Injectable()
export class BazaNcIntegrationInvestmentsService {
    constructor(
        private readonly repository: BazaNcIntegrationListingsRepository,
        private readonly mapper: BazaNcIntegrationListingsMapper,
        private readonly transactions: BazaNcTransactionRepository,
        private readonly transactionMapper: BazaNcTransactionMapper,
    ) {}

    /**
     * Lists Investments of account (CRUD support)
     * @param account
     * @param request
     * @param options
     */
    async listInvestmentsOfAccount(
        account: AccountEntity,
        request: CrudListRequestDto<BazaNcIntegrationInvestmentDto>,
        options: {
            aggregate: 'listings' | 'listings-and-states';
            reverse: boolean;
        } = { reverse: false, aggregate: 'listings' },
    ): Promise<CrudListResponseDto<BazaNcIntegrationInvestmentDto>> {
        const allInvestments =
            options.aggregate === 'listings'
                ? await this.allInvestmentsOfAccount(account)
                : await this.allInvestmentsOfAccountByStates(account);

        allInvestments.sort((a, b) => {
            const dateA = new Date(a.updatedAt || a.createdAt).getTime();
            const dateB = new Date(b.updatedAt || b.createdAt).getTime();

            return dateB - dateA;
        });

        if (options.reverse) {
            allInvestments.reverse();
        }

        if (!request.index) {
            request.index = 1;
        }

        if (!request.size) {
            request.size = BAZA_CRUD_DEFAULT_SIZE;
        }

        const offset = Math.max(0, request.index - 1) * request.size;

        if (!request.index) {
            request.index = 1;
        }
        if (!request.size) {
            request.size = allInvestments.length;
        }

        return {
            items: paginate<BazaNcIntegrationInvestmentDto>(allInvestments, offset, request.size),
            pager: {
                total: allInvestments.length,
                size: request.size,
                index: request.index,
            },
        };
    }

    /**
     * Returns Investments of Investor, aggregates by Listings only
     * @param account
     */
    async allInvestmentsOfAccount(account: AccountEntity): Promise<Array<BazaNcIntegrationInvestmentDto>> {
        const listings = await this.repository.listAll();
        const investments: Array<BazaNcIntegrationInvestmentDto> = [];

        for (const listing of listings) {
            const allTransactions = await this.transactions.repository.find({
                relations: this.transactions.relations,
                where: {
                    account,
                    ncOfferingId: listing.offering.ncOfferingId,
                },
                order: {
                    createdAt: 'ASC',
                },
            });

            let investmentObject: BazaNcIntegrationInvestmentDto;

            const countableTransactions = allTransactions.filter((transaction) =>
                TRANSACTION_INVESTMENTS_COUNTABLE_STATES.includes(transaction.state),
            );

            const failedTransactions = allTransactions.filter((transaction) =>
                TRANSACTION_INVESTMENTS_RETURNED_STATES.includes(transaction.state),
            );

            if (countableTransactions.length > 0) {
                let purchasedShares = 0;
                let purchasedAmountTotal = 0;
                let purchasedAmountTotalCents = 0;
                let lastTransactionState: TransactionState = undefined;
                let lastTransactionDate: Date;

                for (const transaction of countableTransactions) {
                    if (!lastTransactionState) {
                        lastTransactionState = transaction.state;
                    }

                    if (countableTransactions.length > 1) {
                        if (!lastTransactionDate || lastTransactionDate.getTime() < transaction.createdAt.getTime()) {
                            lastTransactionDate = transaction.createdAt;
                        }
                    }

                    purchasedShares += transaction.shares;
                    purchasedAmountTotal += convertFromCents(transaction.amountCents);
                    purchasedAmountTotalCents += transaction.amountCents;
                }

                investmentObject = {
                    isPrimaryCard: true,
                    createdAt: countableTransactions[0].createdAt.toISOString(),
                    updatedAt: lastTransactionDate?.toISOString(),
                    listing: await this.mapper.entityToDTO(listing),
                    lastTransactionState,
                    purchasedShares,
                    purchasedAmountTotal: purchasedAmountTotal.toFixed(2),
                    purchasedAmountTotalCents,
                };
            }

            if (investmentObject && Object.getOwnPropertyNames(investmentObject).length > 0) {
                if (failedTransactions && failedTransactions.length > 0) {
                    failedTransactions.sort((transactionA, transactionB) => {
                        return transactionB.createdAt.getTime() - transactionA.createdAt.getTime();
                    });

                    investmentObject.lastFailedTransaction = await this.transactionMapper.entityToDTO(failedTransactions[0]);
                }

                investments.push(investmentObject);
            }
        }

        return investments;
    }

    /**
     * Returns Investments of Investor, aggregated by Listing + State
     * @param account
     */
    async allInvestmentsOfAccountByStates(account: AccountEntity): Promise<Array<BazaNcIntegrationInvestmentDto>> {
        const listings = await this.repository.listAll();
        const investments: Array<BazaNcIntegrationInvestmentDto> = [];

        for (const listing of listings) {
            const listingInvestments: Array<BazaNcIntegrationInvestmentDto> = [];

            const allTransactions = await this.transactions.repository.find({
                relations: this.transactions.relations,
                where: {
                    account,
                    ncOfferingId: listing.offering.ncOfferingId,
                },
                order: {
                    createdAt: 'ASC',
                },
            });

            const allStateGroupsOfListingTransactions = _.uniq(
                allTransactions
                    .map((next) => next.state)
                    .filter((next) => TRANSACTION_INVESTMENTS_COUNTABLE_STATES.includes(next))
                    .map((next) => TRANSACTION_INVESTMENTS_COUNTABLE_STATES_GROUPS.find((sg) => sg.includes(next)))
                    .filter((next) => !!next),
            );

            let isPrimaryCard: BazaNcIntegrationInvestmentDto;

            for (const states of allStateGroupsOfListingTransactions) {
                let investmentObject: BazaNcIntegrationInvestmentDto;

                const transactionsContext = allTransactions.filter((next) => states.includes(next.state));

                if (transactionsContext.length > 0) {
                    let purchasedShares = 0;
                    let purchasedAmountTotal = 0;
                    let purchasedAmountTotalCents = 0;
                    let lastTransactionDate: Date;

                    for (const transaction of transactionsContext) {
                        if (transactionsContext.length > 1) {
                            if (!lastTransactionDate || lastTransactionDate.getTime() < transaction.createdAt.getTime()) {
                                lastTransactionDate = transaction.createdAt;
                            }
                        }

                        purchasedShares += transaction.shares;
                        purchasedAmountTotal += convertFromCents(transaction.amountCents);
                        purchasedAmountTotalCents += transaction.amountCents;
                    }

                    investmentObject = {
                        isPrimaryCard: states === TRANSACTION_INVESTMENTS_COUNTABLE_STATES_GROUPS_MAIN_CARD,
                        createdAt: transactionsContext[0].createdAt.toISOString(),
                        updatedAt: lastTransactionDate?.toISOString(),
                        listing: await this.mapper.entityToDTO(listing),
                        lastTransactionState: transactionsContext[transactionsContext.length - 1].state,
                        purchasedShares,
                        purchasedAmountTotal: purchasedAmountTotal.toFixed(2),
                        purchasedAmountTotalCents,
                    };

                    if (investmentObject.isPrimaryCard) {
                        isPrimaryCard = investmentObject;
                    }

                    listingInvestments.push(investmentObject);
                }
            }

            if (listingInvestments.length === 1) {
                listingInvestments[0].isPrimaryCard = true;
            }

            const attachFailedTransactionTo = isPrimaryCard ? isPrimaryCard : listingInvestments[0];

            if (attachFailedTransactionTo) {
                const failedTransactions = allTransactions.filter((transaction) =>
                    TRANSACTION_INVESTMENTS_RETURNED_STATES.includes(transaction.state),
                );

                if (failedTransactions && failedTransactions.length > 0) {
                    failedTransactions.sort((transactionA, transactionB) => {
                        return transactionB.createdAt.getTime() - transactionA.createdAt.getTime();
                    });

                    attachFailedTransactionTo.lastFailedTransaction = await this.transactionMapper.entityToDTO(failedTransactions[0]);
                }
            }

            investments.push(...listingInvestments);
        }

        return investments;
    }
}
