import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { AccountTypeCheckingSaving, OfferingId, BazaNcPurchaseFlowTransactionType, TransactionState } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationInvestmentsNodeAccess, BazaNcIntegrationListingsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

xdescribe('@baza-nc-integration/baza-nc-integration-api/investments/integration-tests/tests/001-baza-nc-integration-investments.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessInvestments = new BazaNcIntegrationInvestmentsNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);

    let TRANSACTION_ID: string;
    let LISTING_OFFERING_ID_1: OfferingId;
    let LISTING_OFFERING_ID_2: OfferingId;
    let LAST_UPDATED_AT_DATE: Date;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display empty list before any actions', async () => {
        const response = await dataAccessInvestments.investments({
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
        expect(response.pager.total).toBe(0);
    });

    it('will start trade session', async () => {
        const listings = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(listings)).toBeFalsy();

        LISTING_OFFERING_ID_1 = listings.items[0].offeringId;
        LISTING_OFFERING_ID_2 = listings.items[1].offeringId;

        expect(LISTING_OFFERING_ID_1).toBeDefined();
        expect(LISTING_OFFERING_ID_2).toBeDefined();

        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID_1,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        TRANSACTION_ID = session.id;
    });

    it('will still display empty list before any actions', async () => {
        const response = await dataAccessInvestments.investments({
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
        expect(response.pager.total).toBe(0);
    });

    it('will successfully finish trade session', async () => {
        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: TRANSACTION_ID,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display Listing 1 after purchase shares of Listing 1', async () => {
        const response = await dataAccessInvestments.investments({
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.pager.total).toBe(1);

        expect(response.items[0].createdAt).toBeDefined();
        expect(response.items[0].updatedAt).not.toBeDefined();
        expect(response.items[0].listing).toBeDefined();
        expect(response.items[0].listing.offeringId).toBe(LISTING_OFFERING_ID_1);
        expect(response.items[0].lastTransactionState).toBe(TransactionState.PendingPayment);
    });

    it('will successfully purchase Listing 2 share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID_2,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display Listing 1 and Listing 2 after purchase shares of Listing 2', async () => {
        const response = await dataAccessInvestments.investments({
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.pager.total).toBe(2);

        expect(response.items[0].createdAt).toBeDefined();
        expect(response.items[0].updatedAt).not.toBeDefined();
        expect(response.items[0].listing).toBeDefined();
        expect(response.items[0].listing.offeringId).toBe(LISTING_OFFERING_ID_2);
        expect(response.items[0].lastTransactionState).toBe(TransactionState.PendingPayment);
        expect(response.items[0].purchasedShares).toBe(1);
        expect(response.items[0].purchasedAmountTotalCents).toBe(1000);

        expect(response.items[1].createdAt).toBeDefined();
        expect(response.items[1].updatedAt).not.toBeDefined();
        expect(response.items[1].listing).toBeDefined();
        expect(response.items[1].listing.offeringId).toBe(LISTING_OFFERING_ID_1);
        expect(response.items[1].lastTransactionState).toBe(TransactionState.PendingPayment);
        expect(response.items[1].purchasedShares).toBe(1);
        expect(response.items[1].purchasedAmountTotalCents).toBe(1000);
    });

    it('will successfully purchase one more shares of Listing 2', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID_2,
            numberOfShares: 3,
            amount: 3000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display Listing 1 and Listing 2 after purchase more shares of Listing 2', async () => {
        const response = await dataAccessInvestments.investments({
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.pager.total).toBe(2);

        expect(response.items[0].createdAt).toBeDefined();
        expect(response.items[0].updatedAt).toBeDefined();
        expect(response.items[0].listing).toBeDefined();
        expect(response.items[0].listing.offeringId).toBe(LISTING_OFFERING_ID_2);
        expect(response.items[0].lastTransactionState).toBe(TransactionState.PendingPayment);
        expect(response.items[0].purchasedShares).toBe(4);
        expect(response.items[0].purchasedAmountTotalCents).toBe(4000);

        expect(response.items[1].createdAt).toBeDefined();
        expect(response.items[1].updatedAt).not.toBeDefined();
        expect(response.items[1].listing).toBeDefined();
        expect(response.items[1].listing.offeringId).toBe(LISTING_OFFERING_ID_1);
        expect(response.items[1].lastTransactionState).toBe(TransactionState.PendingPayment);
        expect(response.items[1].purchasedShares).toBe(1);
        expect(response.items[1].purchasedAmountTotalCents).toBe(1000);

        expect(new Date(response.items[0].createdAt).getTime()).toBeLessThan(new Date(response.items[0].updatedAt).getTime());

        LAST_UPDATED_AT_DATE = new Date(response.items[0].updatedAt);
    });

    it('will successfully purchase more shares of Listing 2', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID_2,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will update updatedAt date for investments to Listing 2', async () => {
        const response = await dataAccessInvestments.investments({
            size: 10,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.pager.total).toBe(2);

        expect(response.items[0].createdAt).toBeDefined();
        expect(response.items[0].updatedAt).toBeDefined();
        expect(response.items[0].listing).toBeDefined();
        expect(response.items[0].listing.offeringId).toBe(LISTING_OFFERING_ID_2);
        expect(response.items[0].lastTransactionState).toBe(TransactionState.PendingPayment);
        expect(response.items[0].purchasedShares).toBe(5);
        expect(response.items[0].purchasedAmountTotalCents).toBe(5000);

        expect(response.items[1].createdAt).toBeDefined();
        expect(response.items[1].updatedAt).not.toBeDefined();
        expect(response.items[1].listing).toBeDefined();
        expect(response.items[1].listing.offeringId).toBe(LISTING_OFFERING_ID_1);
        expect(response.items[1].lastTransactionState).toBe(TransactionState.PendingPayment);
        expect(response.items[1].purchasedShares).toBe(1);
        expect(response.items[1].purchasedAmountTotalCents).toBe(1000);

        expect(new Date(response.items[0].createdAt).getTime()).toBeLessThan(new Date(response.items[0].updatedAt).getTime());
        expect(new Date(response.items[0].updatedAt).getTime()).toBeGreaterThan(LAST_UPDATED_AT_DATE.getTime());
    });
});
