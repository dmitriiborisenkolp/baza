import { Global, Module } from '@nestjs/common';
import { BazaNcIntegrationListingsController } from './controllers/baza-nc-integration-listings.controller';
import { BazaNcIntegrationListingsCmsController } from './controllers/baza-nc-integration-listings-cms.controller';
import { BazaNcIntegrationListingsRepository } from './repositories/baza-nc-integration-listings.repository';
import { BazaNcIntegrationListingsMapper } from './mappers/baza-nc-integration-listings.mapper';
import { BazaNcIntegrationListingsCmsMapper } from './mappers/baza-nc-integration-listings-cms.mapper';
import { BazaNcIntegrationListingsService } from './services/baza-nc-integration-listings.service';
import { BazaNcIntegrationListingsCmsService } from './services/baza-nc-integration-listings-cms.service';
import { BazaAttachmentModule, BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcOfferingApiModule } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationListingsCmsStatusService } from './services/baza-nc-integration-listings-cms-status.service';
import { BazaNcIntegrationListingsBaseMapper } from './mappers/baza-nc-integration-listings-base.mapper';
import { BazaNcIntegrationListingsListItemMapper } from './mappers/baza-nc-integration-listings-list-item.mapper';
import { CqrsModule } from '@nestjs/cqrs';
import { SendFundsTransferredNotifications } from './event-handlers/send-funds-transferred-notifications';
import { SubmitAdminAndClientNotificationsAfterCompletingPurchase } from './event-handlers/submit-admin-and-client-notifications-after-completing-purchase';
import { BazaNcIntegrationListingsPropertiesMapper } from './mappers/baza-nc-integration-listings-properties.mapper';
import { BazaNcIntegrationListingsTimelineService } from './services/baza-nc-integration-listings-timeline.service';
import { BazaNcIntegrationSubscriptionApiModule } from '../../../subscription/src';
import { BazaNcIntegrationGetListingCommandHandler } from './command-handlers/baza-nc-integration-get-listing.command-handler';
import { BazaNcIntegrationFavoriteApiModule } from '../../../favorite/src';
import { SchemaSoftUpdated } from './event-handlers/schema-soft-updated';

const EVENT_HANDLERS = [SendFundsTransferredNotifications, SubmitAdminAndClientNotificationsAfterCompletingPurchase, SchemaSoftUpdated];

const COMMAND_HANDLERS = [BazaNcIntegrationGetListingCommandHandler];

@Global()
@Module({
    imports: [
        CqrsModule,
        BazaAttachmentModule,
        BazaCrudApiModule,
        BazaNcOfferingApiModule,
        BazaNcIntegrationSubscriptionApiModule,
        BazaNcIntegrationFavoriteApiModule,
    ],
    controllers: [BazaNcIntegrationListingsController, BazaNcIntegrationListingsCmsController],
    providers: [
        BazaNcIntegrationListingsRepository,
        BazaNcIntegrationListingsMapper,
        BazaNcIntegrationListingsBaseMapper,
        BazaNcIntegrationListingsCmsMapper,
        BazaNcIntegrationListingsListItemMapper,
        BazaNcIntegrationListingsPropertiesMapper,
        BazaNcIntegrationListingsService,
        BazaNcIntegrationListingsCmsService,
        BazaNcIntegrationListingsCmsStatusService,
        BazaNcIntegrationListingsTimelineService,
        ...EVENT_HANDLERS,
        ...COMMAND_HANDLERS,
    ],
    exports: [
        BazaNcIntegrationListingsRepository,
        BazaNcIntegrationListingsMapper,
        BazaNcIntegrationListingsCmsMapper,
        BazaNcIntegrationListingsListItemMapper,
        BazaNcIntegrationListingsPropertiesMapper,
        BazaNcIntegrationListingsService,
        BazaNcIntegrationListingsCmsService,
        BazaNcIntegrationListingsCmsStatusService,
        BazaNcIntegrationListingsTimelineService,
    ],
})
export class BazaNcIntegrationListingsApiModule {}
