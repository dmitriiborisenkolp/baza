import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcIntegrationSchemaRepository, BazaNcIntegrationSchemaUpdateSoftChangesEvent } from '../../../../schema/src';
import { BazaNcIntegrationListingsRepository } from '../repositories/baza-nc-integration-listings.repository';
import { cqrs } from '@scaliolabs/baza-core-api';

@EventsHandler(BazaNcIntegrationSchemaUpdateSoftChangesEvent)
export class SchemaSoftUpdated implements IEventHandler<BazaNcIntegrationSchemaUpdateSoftChangesEvent> {
    constructor(
        private readonly repository: BazaNcIntegrationListingsRepository,
        private readonly schemaRepository: BazaNcIntegrationSchemaRepository,
    ) {}

    handle(event: BazaNcIntegrationSchemaUpdateSoftChangesEvent): void {
        cqrs(SchemaSoftUpdated.name, async () => {
            const schema = await this.schemaRepository.getById(event.payload.schemaId);
            const entities = await this.repository.findBySchemaTitle(event.payload.oldSchemaTitle);

            for (const entity of entities) {
                entity.schema.title = event.payload.newSchemaTitle;
                entity.schema.definitions = schema.definitions;
            }

            await this.repository.save(entities);
        });
    }
}
