import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaLogger, BazaRegistryService, cqrs } from '@scaliolabs/baza-core-api';
import { MailService } from '@scaliolabs/baza-core-api';
import { BazaNcPurchaseFlowSessionSubmittedEvent, convertFromCents } from '@scaliolabs/baza-nc-shared';
import { BazaNcMail, BazaNcOfferingRepository, BazaNcOfferingsService, BazaNcTransactionRepository } from '@scaliolabs/baza-nc-api';
import { awaitTimeout, BazaRegistryNodeEmailRecipientValue } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationListingsRepository } from '../repositories/baza-nc-integration-listings.repository';

@EventsHandler(BazaNcPurchaseFlowSessionSubmittedEvent)
export class SubmitAdminAndClientNotificationsAfterCompletingPurchase implements IEventHandler<BazaNcPurchaseFlowSessionSubmittedEvent> {
    constructor(
        private readonly logger: BazaLogger,
        private readonly mail: MailService,
        private readonly registry: BazaRegistryService,
        private readonly offeringsService: BazaNcOfferingsService,
        private readonly offerings: BazaNcOfferingRepository,
        private readonly repository: BazaNcIntegrationListingsRepository,
        private readonly transactionsRepository: BazaNcTransactionRepository,
    ) {
        this.logger.setContext(this.constructor.name);
    }

    handle(command: BazaNcPurchaseFlowSessionSubmittedEvent): void {
        cqrs(SubmitAdminAndClientNotificationsAfterCompletingPurchase.name, async () => {
            /**
             * TODO: We should find another way to avoid using awaitTimeout here
             */
            await awaitTimeout(5000);
            await this.sendEmailNotifications(command);
        });
    }

    private async sendEmailNotifications(command: BazaNcPurchaseFlowSessionSubmittedEvent): Promise<void> {
        const tradeId = command.request.ncTradeId;

        const transaction = await this.transactionsRepository.findByNcTradeId(tradeId);

        if (!transaction) {
            this.logger.warn(`User purchase was not found for tradeId ${tradeId}`);
            return;
        }

        const offering = await this.offerings.findOfferingWithId(transaction.ncOfferingId);

        if (!offering) {
            this.logger.warn(`Offering was not found for tradeId ${tradeId}`);
            return;
        }

        const listing = await this.repository.findByOffering(offering);

        if (!listing) {
            this.logger.warn(`Listing was not found for purchase ${transaction.ncOfferingId}`);
            return;
        }

        const adminEmails = this.registry.getValue('adminEmail') as BazaRegistryNodeEmailRecipientValue;

        const variables = {
            offeringName: listing.title,
            numberOfShares: transaction.shares,
            shares: transaction.shares === 1 ? 'share' : 'shares',
            totalInvestments: convertFromCents(transaction.amountCents).toFixed(2),
            reservedBy: transaction.account.fullName,
            reservedByEmail: transaction.account.email,
        };

        await this.mail.send({
            name: BazaNcMail.BazaNcInvestmentSubmitted,
            uniqueId: `baza_nc_integration_investment_submitted_${command.request.ncTradeId}`,
            to: [
                {
                    name: transaction.account.fullName,
                    email: transaction.account.email,
                },
            ],
            subject: 'baza-nc.mail.investmentSubmitted.subject',
            variables: () => variables,
        });

        await this.mail.send({
            name: BazaNcMail.BazaNcInvestmentNotification,
            uniqueId: `baza_nc_integration_investment_submitted_admin_${command.request.ncTradeId}`,
            to: [adminEmails],
            subject: 'baza-nc.mail.investmentNotification.subject',
            variables: () => variables,
        });
    }
}
