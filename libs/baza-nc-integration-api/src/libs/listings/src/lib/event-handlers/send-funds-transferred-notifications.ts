import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BazaNcTradeStatusUpdatedEvent, OrderStatus } from '@scaliolabs/baza-nc-shared';
import { cqrs, MailService } from '@scaliolabs/baza-core-api';
import { BazaNcMail, BazaNcTransactionRepository } from '@scaliolabs/baza-nc-api';
import { awaitTimeout } from '@scaliolabs/baza-core-shared';

@EventsHandler(BazaNcTradeStatusUpdatedEvent)
export class SendFundsTransferredNotifications implements IEventHandler<BazaNcTradeStatusUpdatedEvent> {
    constructor(private readonly mail: MailService, private readonly transactionsRepository: BazaNcTransactionRepository) {}

    handle(command: BazaNcTradeStatusUpdatedEvent): void {
        cqrs(SendFundsTransferredNotifications.name, async () => {
            /**
             * TODO: We should find another way to avoid using awaitTimeout here
             */
            await awaitTimeout(5000);
            await this.sendEmailNotification(command);
        });
        setTimeout(() => {
            this.sendEmailNotification(command);
        }, 5000);
    }

    private async sendEmailNotification(command: BazaNcTradeStatusUpdatedEvent): Promise<void> {
        const tradeId = command.request.ncTradeId;
        const transaction = await this.transactionsRepository.findByNcTradeId(tradeId);

        if (!transaction) {
            return;
        }

        if (command.request.newOrderStatus === OrderStatus.Funded) {
            await this.mail.send({
                name: BazaNcMail.BazaNcFundsTransferred,
                uniqueId: `baza_nc_integration_funds_transferred_${command.request.ncTradeId}`,
                to: [
                    {
                        name: transaction.account.fullName,
                        email: transaction.account.email,
                    },
                ],
                subject: 'baza-nc.mail.fundsTransferred.subject',
                variables: () => ({}),
            });
        }
    }
}
