import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';

export type BazaNCIntegrationListingsCmsSortedId = Pick<BazaNcIntegrationListingsEntity, 'id' | 'sortOrder'>;
