import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import {
    BazaNcIntegrationGetListingCommand,
    BazaNcIntegrationGetListingCommandResult,
} from '../commands/baza-nc-integration-get-listing.command';
import { BazaNcIntegrationListingsRepository } from '../repositories/baza-nc-integration-listings.repository';
import { cqrsCommand } from '@scaliolabs/baza-core-api';

@CommandHandler(BazaNcIntegrationGetListingCommand)
export class BazaNcIntegrationGetListingCommandHandler
    implements ICommandHandler<BazaNcIntegrationGetListingCommand, BazaNcIntegrationGetListingCommandResult>
{
    constructor(private readonly repository: BazaNcIntegrationListingsRepository) {}

    async execute(command: BazaNcIntegrationGetListingCommand): Promise<BazaNcIntegrationGetListingCommandResult> {
        return cqrsCommand<BazaNcIntegrationGetListingCommandResult>(BazaNcIntegrationGetListingCommandHandler.name, async () => {
            return this.repository.getById(command.listingId);
        });
    }
}
