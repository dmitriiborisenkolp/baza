import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';
import {
    BazaNcIntegrationListingDocumentsDto,
    BazaNcIntegrationListingsDto,
    BazaNcIntegrationSchemaDefinition,
    BazaNcIntegrationSchemaDto,
    BazaNcIntegrationSubscribeStatus,
    GetSubscriptionStatusCommand,
    GetSubscriptionStatusCommandResponse,
    BazaNcIntegrationSchemaType,
} from '@scaliolabs/baza-nc-integration-shared';
import { AccountEntity, AttachmentImageService, AwsService, EnvService } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsBaseMapper } from './baza-nc-integration-listings-base.mapper';
import { BazaNcIntegrationListingRoundFailureException } from '../exceptions/baza-nc-integration-listing-round-failure.exception';
import { BazaNcIntegrationListingsPropertiesMapper } from './baza-nc-integration-listings-properties.mapper';
import { BazaNcIntegrationListingsTimelineService } from '../services/baza-nc-integration-listings-timeline.service';
import { TimelineMapper } from '@scaliolabs/baza-content-types-api';
import { CommandBus } from '@nestjs/cqrs';
import { AttachmentImageSetDto } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFavoriteRepository } from '../../../../favorite/src';
import { bazaNcApiConfig, convertToCents } from '@scaliolabs/baza-nc-shared';

function isInt(input: number): boolean {
    return input > 0 && Math.floor(input) === input;
}

const DEFAULT_SCHEMA_TAB = 'Properties';

@Injectable()
export class BazaNcIntegrationListingsMapper {
    constructor(
        private readonly env: EnvService,
        private readonly cqrs: CommandBus,
        private readonly baseMapper: BazaNcIntegrationListingsBaseMapper,
        private readonly attachmentImage: AttachmentImageService,
        private readonly propertiesMapper: BazaNcIntegrationListingsPropertiesMapper,
        private readonly listingsTimelineService: BazaNcIntegrationListingsTimelineService,
        private readonly timelineMapper: TimelineMapper,
        private readonly favoriteRepository: BazaNcIntegrationFavoriteRepository,
        private readonly awsService: AwsService,
    ) {}

    async entityToDTO(input: BazaNcIntegrationListingsEntity, account?: AccountEntity): Promise<BazaNcIntegrationListingsDto> {
        const subscription: GetSubscriptionStatusCommandResponse = account
            ? await this.cqrs.execute<GetSubscriptionStatusCommand>(
                  new GetSubscriptionStatusCommand({
                      listingId: input.id,
                      accountId: account.id,
                  }),
              )
            : undefined;

        const favorite = account ? await this.favoriteRepository.findByAccountAndListing(account, input) : undefined;

        const imageList: Array<AttachmentImageSetDto> = [];

        for (const imageItem of input.images) {
            imageList.push(await this.attachmentImage.toImageSet(imageItem));
        }

        const statementList: Array<BazaNcIntegrationListingDocumentsDto> = [];

        for (const statement of input.statements) {
            statementList.push({
                description: statement.description,
                name: statement.name,
                documentUrl: await this.awsService.presignedUrl(statement.attachment),
            });
        }

        const dto: BazaNcIntegrationListingsDto = {
            ...(await this.baseMapper.entityToDTO(input)),
            status: input.offering.status,
            offeringId: input.offering.ncOfferingId,
            pricePerShareCents: convertToCents(input.offering.ncUnitPrice),
            numSharesAvailable: input.offering.ncRemainingShares,
            percentFunded: Math.floor(input.offering.percentsFunded) || 0,
            totalSharesValueCents: convertToCents(input.offering.ncUnitPrice * input.offering.ncTotalShares),
            numSharesMin: Math.ceil(input.offering.ncMinAmount / input.offering.ncUnitPrice),
            isPublished: input.isPublished,
            cover: await this.attachmentImage.toImageSet(input.cover),
            images: imageList,
            properties: await this.propertiesMapper.propertiesForPublicApi(input),
            timeline: await this.timelineMapper.entitiesToDTOs(await this.listingsTimelineService.getListingsTimeline(input)),
            subscribeStatus: subscription?.status || BazaNcIntegrationSubscribeStatus.NotSubscribed,
            isSubscribed: subscription?.status === BazaNcIntegrationSubscribeStatus.Subscribed,
            isFavorite: !!favorite,
            dateAddedToFavorite: favorite?.dateCreated.toISOString(),
            statements: statementList,
        };

        const intChecks = [isInt(dto.numSharesAvailable), isInt(dto.numSharesMin)];

        if (dto.schema?.tabs) dto.schema.tabs = this.generateSchemaTabsWithDetails(dto);

        if (!intChecks.every((valid) => valid)) {
            if (bazaNcApiConfig().strictIntegerChecks) {
                throw new BazaNcIntegrationListingRoundFailureException();
            } else {
                dto.numSharesAvailable = Math.floor(dto.numSharesAvailable);
                dto.numSharesMin = Math.floor(dto.numSharesMin);
            }
        }

        return dto;
    }

    async entitiesToDTOs(
        input: Array<BazaNcIntegrationListingsEntity>,
        account?: AccountEntity,
    ): Promise<Array<BazaNcIntegrationListingsDto>> {
        const result: Array<BazaNcIntegrationListingsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity, account));
        }

        return result;
    }

    /**
     * Will gather schema tabs and properties together inside schema->tabs array
     * @param input BazaNcIntegrationListingsDto
     */
    private generateSchemaTabsWithDetails(input: BazaNcIntegrationListingsDto) {
        const schema: BazaNcIntegrationSchemaDto = input.schema;
        const schemaTabs: any = [
            {
                title: DEFAULT_SCHEMA_TAB,
                fields: [],
            },
            ...(schema?.tabs ?? []).map((tab) => ({
                ...tab,
                fields: [],
            })),
        ];
        const definitions = [...(schema?.definitions ?? [])];

        definitions.forEach((definition: BazaNcIntegrationSchemaDefinition & { value?: any }) => {
            if (!definition.value) definition.value = [];

            const field = Object.keys(input.properties).find((key) => key === definition.payload.field);
            const value = {
                id: definition.payload.id,
                name: definition.payload.title,
                type: definition.type,
                key: field,
                required: definition.payload.required,
                value: (() => {
                    if (definition.type == BazaNcIntegrationSchemaType.KeyValue) {
                        const resultArr = [];
                        for (const key in input.properties[field]) {
                            resultArr.push({ key, value: input.properties[field][key] });
                        }

                        return resultArr;
                    } else {
                        return input.properties[field] ?? null;
                    }
                })(),
            };

            if (field) {
                definition.value.push(value);
            }

            if (definition.payload.tabId) {
                schemaTabs.forEach((tab) => {
                    if (tab.id === definition.payload.tabId) {
                        tab.fields.push(value);
                    }
                });
            } else {
                // Push into default tab

                schemaTabs[0].fields.push(value);
            }
        });

        if (!schemaTabs[0].fields.length) {
            schemaTabs.splice(0, 1);
        }

        return schemaTabs;
    }
}
