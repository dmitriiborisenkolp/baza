import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';
import {
    BazaNcIntegrationListingsBaseDto,
    BazaNcIntegrationSchemaDefinition,
    bazaNcIntegrationSchemaFieldId,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcOfferingMapper } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationSchemaRepository } from '../../../../schema/src';

@Injectable()
export class BazaNcIntegrationListingsBaseMapper {
    constructor(
        private readonly ncOfferingMapper: BazaNcOfferingMapper,
        private readonly schemaRepository: BazaNcIntegrationSchemaRepository,
    ) {}

    async entityToDTO(input: BazaNcIntegrationListingsEntity): Promise<BazaNcIntegrationListingsBaseDto> {
        const sourceSchema = input.schema?.id ? await this.schemaRepository.findById(input.schema.id) : undefined;

        return {
            id: input.id,
            sid: input.sid,
            sortOrder: input.sortOrder,
            dateCreatedAt: input.dateCreatedAt.toISOString(),
            dateUpdatedAt: input.dateUpdatedAt?.toISOString(),
            title: input.title,
            description: input.description,
            sharesToJoinEliteInvestors: input.sharesToJoinEliteInvestors,
            offering: this.ncOfferingMapper.entityToDTO(input.offering),
            metadata: input.metadata,
            properties: input.properties,
            schema: input.schema
                ? {
                      ...input.schema,
                      definitions: input.schema.definitions.map((field) => {
                          const sourceField = sourceSchema?.definitions.find((next) => {
                              const nextId = bazaNcIntegrationSchemaFieldId(next.type, next.payload);
                              const sourceId = bazaNcIntegrationSchemaFieldId(field.type, field.payload);

                              return nextId === sourceId;
                          });

                          return {
                              ...field,
                              payload: {
                                  ...field.payload,
                                  title: sourceField ? sourceField.payload.title : field.payload.title,
                                  isDisplayedInDetails: sourceField
                                      ? sourceField.payload.isDisplayedInDetails
                                      : field.payload.isDisplayedInDetails,
                              },
                          };
                      }) as Array<BazaNcIntegrationSchemaDefinition>,
                  }
                : null,
            schemaId: input.schema?.id,
        };
    }

    async entitiesToDTOs(input: Array<BazaNcIntegrationListingsEntity>): Promise<Array<BazaNcIntegrationListingsBaseDto>> {
        const result: Array<BazaNcIntegrationListingsBaseDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
