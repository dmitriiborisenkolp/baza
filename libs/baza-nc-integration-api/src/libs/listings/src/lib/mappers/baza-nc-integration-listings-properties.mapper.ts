import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';
import { AttachmentImageService, AwsService } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';
import { AttachmentDto } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaNcIntegrationListingsPropertiesMapper {
    constructor(
        private readonly aws: AwsService,
        private readonly imageHelper: AttachmentImageService,
    ) {
    }

    async propertiesForPublicApi(entity: BazaNcIntegrationListingsEntity): Promise<any> {
        if (! entity.schema || ! entity.schema.definitions || entity.schema.definitions.length === 0) {
            return {};
        }

        const newProperties = {};

        for (const definition of entity.schema.definitions) {
            if (! entity.properties) {
                newProperties[definition.payload.field] = null;
            } else {
                const field = definition.payload.field;
                const value = entity.properties[field];

                if (! value) {
                    newProperties[field] = null;
                } else {
                    switch (definition.type) {
                        default: {
                            newProperties[field] = value;

                            break;
                        }

                        case BazaNcIntegrationSchemaType.Image: {
                            newProperties[field] = await this.imageHelper.toImageSet(value);

                            break;
                        }

                        case BazaNcIntegrationSchemaType.ImageMulti: {
                            const images = [];

                            for (const attachmentDTO of value) {
                                images.push(await this.imageHelper.toImageSet(attachmentDTO));
                            }

                            newProperties[field] = images;

                            break;
                        }

                        case BazaNcIntegrationSchemaType.Slider: {
                            newProperties[field] = {
                                min: definition.payload.min,
                                max: definition.payload.max,
                                value,
                            };

                            break;
                        }

                        case BazaNcIntegrationSchemaType.File: {
                            const file: AttachmentDto = value;

                            const fileDTO = {
                                url: await this.aws.presignedUrl({
                                    s3ObjectId: file.s3ObjectId,
                                }),
                            };

                            newProperties[field] = fileDTO;

                            break;
                        }

                        case BazaNcIntegrationSchemaType.FileMulti: {
                            const files: Array<AttachmentDto> = value;
                            const newValue: Array<{
                                url: string,
                            }> = [];

                            for (const file of files) {
                                const fileDTO = {
                                    url: await this.aws.presignedUrl({
                                        s3ObjectId: file.s3ObjectId,
                                    }),
                                };

                                newValue.push(fileDTO);
                            }

                            newProperties[field] = newValue;

                            break;
                        }
                    }
                }
            }
        }

        return newProperties;
    }
}
