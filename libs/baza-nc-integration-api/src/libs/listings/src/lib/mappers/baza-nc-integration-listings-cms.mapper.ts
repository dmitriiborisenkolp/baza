import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';
import { BazaNcIntegrationListingsCmsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsBaseMapper } from './baza-nc-integration-listings-base.mapper';

@Injectable()
export class BazaNcIntegrationListingsCmsMapper {
    constructor(
        private readonly baseMapper: BazaNcIntegrationListingsBaseMapper,
    ) {}

    async entityToDTO(input: BazaNcIntegrationListingsEntity): Promise<BazaNcIntegrationListingsCmsDto> {
        const dto = await this.baseMapper.entityToDTO(input);

        return {
            ...dto,
            isPublished: input.isPublished,
            categoryId: input.category?.id,
            cover: input.cover,
            images: input.images,
            statements: input.statements,
            sortOrder: input.sortOrder
        };
    }

    async entitiesToDTOs(input: Array<BazaNcIntegrationListingsEntity>): Promise<Array<BazaNcIntegrationListingsCmsDto>> {
        const result: Array<BazaNcIntegrationListingsCmsDto> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(entity));
        }

        return result;
    }
}
