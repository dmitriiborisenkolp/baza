import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';
import { BazaNcIntegrationListingsListItemDto } from '@scaliolabs/baza-nc-integration-shared';
import { AttachmentImageService } from '@scaliolabs/baza-core-api';
import { AttachmentImageSetDto } from '@scaliolabs/baza-core-shared';
import { convertToCents } from '@scaliolabs/baza-nc-shared';

@Injectable()
export class BazaNcIntegrationListingsListItemMapper {
    constructor(
        private readonly attachmentImageHelper: AttachmentImageService,
    ) {}

    async entityToListDTO(input: BazaNcIntegrationListingsEntity): Promise<BazaNcIntegrationListingsListItemDto> {
        const imageList: Array<AttachmentImageSetDto> = [];

        for (const imageItem of input.images) {
            imageList.push(await this.attachmentImageHelper.toImageSet(imageItem));
        }

        return {
            id: input.id,
            offeringId: input.offering?.ncOfferingId,
            sortOrder: input.sortOrder,
            title: input.title,
            cover: await this.attachmentImageHelper.toImageSet(input.cover),
            images: imageList,
            status: input.offering.status,
            pricePerShareCents: convertToCents(input.offering.ncUnitPrice),
            numSharesAvailable: input.offering.ncRemainingShares,
            percentFunded: Math.floor(input.offering.percentsFunded) || 0,
            totalSharesValueCents: convertToCents((input.offering.ncUnitPrice * input.offering.ncTotalShares)),
            numSharesMin: Math.ceil(input.offering.ncMinAmount / input.offering.ncUnitPrice),
        };
    }

    async entitiesToDTOs(input: Array<BazaNcIntegrationListingsEntity>): Promise<Array<BazaNcIntegrationListingsListItemDto>> {
        const results: Array<BazaNcIntegrationListingsListItemDto> = [];

        for (const entity of input) {
            results.push(await this.entityToListDTO(entity));
        }

        return results;
    }
}
