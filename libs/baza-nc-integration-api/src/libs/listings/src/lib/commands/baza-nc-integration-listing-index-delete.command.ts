import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';

export class BazaNcIntegrationListingIndexDeleteCommand {
    constructor(
        public readonly payload: Array<{
            listingId: number;
            listingUlid: string;
            listingEntity: BazaNcIntegrationListingsEntity;
        }>,
    ) {}
}
