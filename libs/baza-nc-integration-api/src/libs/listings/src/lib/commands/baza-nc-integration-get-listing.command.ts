import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';

export class BazaNcIntegrationGetListingCommand {
    constructor(
        public listingId: number,
    ) {
    }
}

export type BazaNcIntegrationGetListingCommandResult = BazaNcIntegrationListingsEntity;
