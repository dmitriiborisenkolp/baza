import { Controller, Get, Param, ParseArrayPipe, Query, UseGuards } from '@nestjs/common';
import {
    BazaNcIntegrationListingsDto,
    BazaNcIntegrationListingsEndpoint,
    BazaNcIntegrationListingsEndpointPaths,
    BazaNcIntegrationListingsGetByIdRequest,
    BazaNcIntegrationListingsGetByOfferingIdRequest,
    BazaNcIntegrationListingsListRequest,
    BazaNcIntegrationListingsListResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsService } from '../services/baza-nc-integration-listings.service';
import { BazaNcIntegrationListingsMapper } from '../mappers/baza-nc-integration-listings.mapper';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaNcIntegrationPublicOpenApi } from '@scaliolabs/baza-nc-integration-shared';
import { AccountEntity, AuthOptionalGuard, ReqAccount } from '@scaliolabs/baza-core-api';

@Controller()
@ApiTags(BazaNcIntegrationPublicOpenApi.Listings)
@UseGuards(AuthOptionalGuard)
export class BazaNcIntegrationListingsController implements BazaNcIntegrationListingsEndpoint {
    constructor(private readonly service: BazaNcIntegrationListingsService, private readonly mapper: BazaNcIntegrationListingsMapper) {}

    @Get(BazaNcIntegrationListingsEndpointPaths.list)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsListResponse,
    })
    async list(
        @Query() request: BazaNcIntegrationListingsListRequest,
        @Query('schemas', new ParseArrayPipe({ items: String, separator: ',', optional: true })) schemas: Array<string>,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationListingsListResponse> {
        return this.service.list({ ...request, schemas }, account);
    }

    @Get(BazaNcIntegrationListingsEndpointPaths.getById)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsDto,
    })
    async getById(
        @Param() request: BazaNcIntegrationListingsGetByIdRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationListingsDto> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity, account);
    }

    @Get(BazaNcIntegrationListingsEndpointPaths.getByOfferingId)
    @ApiBearerAuth()
    @ApiOperation({
        summary: 'getByOfferingId',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsDto,
    })
    async getByOfferingId(
        @Param() request: BazaNcIntegrationListingsGetByOfferingIdRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationListingsDto> {
        const entity = await this.service.getByOfferingId(request);

        return this.mapper.entityToDTO(entity, account);
    }
}
