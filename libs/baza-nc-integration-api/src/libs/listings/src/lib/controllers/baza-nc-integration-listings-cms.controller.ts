import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AccountEntity, AclNodes, AuthGuard, AuthRequireAdminRoleGuard, ReqAccount, WithAccessGuard } from '@scaliolabs/baza-core-api';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationChangeStatusRequest,
    BazaNcIntegrationCmsOpenApi,
    BazaNcIntegrationListingsCmsCreateRequest,
    BazaNcIntegrationListingsCmsDeleteRequest,
    BazaNcIntegrationListingsCmsDto,
    BazaNcIntegrationListingsCmsEndpoint,
    BazaNcIntegrationListingsCmsEndpointPaths,
    BazaNcIntegrationListingsCmsGetByIdRequest,
    BazaNcIntegrationListingsCmsGetByUlidRequest,
    BazaNcIntegrationListingsCmsListRequest,
    BazaNcIntegrationListingsCmsListResponse,
    BazaNcIntegrationListingsCmsUpdateRequest,
    BazaNcIntegrationListingsListItemDto,
    BazaNcIntegrationListingsCmsSortOrderResponse,
    BazaNcIntegrationListingsCmsSetSortOrderRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsCmsService } from '../services/baza-nc-integration-listings-cms.service';
import { BazaNcIntegrationListingsCmsMapper } from '../mappers/baza-nc-integration-listings-cms.mapper';
import { BazaNcIntegrationListingsCmsStatusService } from '../services/baza-nc-integration-listings-cms-status.service';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNcIntegrationCmsOpenApi.Listings)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListing])
export class BazaNcIntegrationListingsCmsController implements BazaNcIntegrationListingsCmsEndpoint {
    constructor(
        private readonly service: BazaNcIntegrationListingsCmsService,
        private readonly statusService: BazaNcIntegrationListingsCmsStatusService,
        private readonly mapper: BazaNcIntegrationListingsCmsMapper,
    ) {}

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.create)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListing, BazaNcIntegrationAcl.BazaNcIntegrationListingManagement])
    @ApiOperation({
        summary: 'create',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsCmsDto,
    })
    async create(
        @Body() request: BazaNcIntegrationListingsCmsCreateRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationListingsCmsDto> {
        const entity = await this.service.create(account, request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.update)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListing, BazaNcIntegrationAcl.BazaNcIntegrationListingManagement])
    @ApiOperation({
        summary: 'update',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsCmsDto,
    })
    async update(
        @Body() request: BazaNcIntegrationListingsCmsUpdateRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationListingsCmsDto> {
        const entity = await this.service.update(account, request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.delete)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListing, BazaNcIntegrationAcl.BazaNcIntegrationListingManagement])
    @ApiOperation({
        summary: 'delete',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaNcIntegrationListingsCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsCmsListResponse,
    })
    async list(@Body() request: BazaNcIntegrationListingsCmsListRequest): Promise<BazaNcIntegrationListingsCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.listAll)
    @ApiOperation({
        summary: 'listAll',
        description: 'Returns all listings in short format',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsListItemDto,
        isArray: true,
    })
    async listAll(): Promise<Array<BazaNcIntegrationListingsListItemDto>> {
        return this.service.listAll();
    }

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsCmsDto,
    })
    async getById(@Body() request: BazaNcIntegrationListingsCmsGetByIdRequest): Promise<BazaNcIntegrationListingsCmsDto> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.getByUlid)
    @ApiOperation({
        summary: 'getByUlid',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsCmsDto,
    })
    async getByUlid(@Body() request: BazaNcIntegrationListingsCmsGetByUlidRequest): Promise<BazaNcIntegrationListingsCmsDto> {
        const entity = await this.service.getByUlid(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.changeStatus)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListing, BazaNcIntegrationAcl.BazaNcIntegrationListingChangeStatus])
    @ApiOperation({
        summary: 'changeStatus',
    })
    @ApiOkResponse()
    async changeStatus(@Body() request: BazaNcIntegrationChangeStatusRequest): Promise<void> {
        await this.statusService.changeStatus(request);
    }

    @Post(BazaNcIntegrationListingsCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingsCmsSortOrderResponse,
    })
    async setSortOrder(
        @Body() request: BazaNcIntegrationListingsCmsSetSortOrderRequest,
    ): Promise<BazaNcIntegrationListingsCmsSortOrderResponse> {
        return this.service.setSortOrder(request);
    }
}
