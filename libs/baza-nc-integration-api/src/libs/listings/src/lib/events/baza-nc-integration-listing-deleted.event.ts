import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';

export class BazaNcIntegrationListingDeletedEvent {
    constructor(
        public readonly payload: Array<{
            listingId: number;
            listingUlid: string;
            listingEntity: BazaNcIntegrationListingsEntity;
        }>,
    ) {}
}
