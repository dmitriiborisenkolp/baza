import { AttachmentDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { BazaNcOfferingEntity, NcOfferingSupportedEntity } from '@scaliolabs/baza-nc-api';
import { Column, CreateDateColumn, Entity, JoinColumn, JoinTable, ManyToOne, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { BazaNcIntegrationSchemaDto, BazaNcIntegrationListingCmsDocumentsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaContentTypeCategoryEntity } from '@scaliolabs/baza-content-types-api';

export const BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS = [
    'offering', 'category',
];

@Entity()
export class BazaNcIntegrationListingsEntity implements NcOfferingSupportedEntity, CrudSortableEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true,
        nullable: true,
    })
    sid: string;

    @CreateDateColumn()
    dateCreatedAt: Date;

    @UpdateDateColumn()
    dateUpdatedAt?: Date;

    @Column({ default: 1 })
    sortOrder: number;

    @Column({
        default: true,
    })
    isPublished: boolean;

    @ManyToOne(() => BazaContentTypeCategoryEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinTable()
    category: BazaContentTypeCategoryEntity;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column({
        type: 'jsonb',
    })
    cover: AttachmentDto;

    @Column({
        type:'jsonb',
        default: [],
    })
    images: Array<AttachmentDto>;

    @Column({
        nullable: true,
    })
    sharesToJoinEliteInvestors?: number;

    @OneToOne(() => BazaNcOfferingEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    offering: BazaNcOfferingEntity;

    @Column({
        // Don't use JSONB here - JSONB does not keep requested keys order!
        type: 'json',
        default: {},
    })
    metadata: Record<string, string>;

    @Column({
        // Don't use JSONB here - JSONB does not keep requested keys order!
        type: 'json',
        default: {},
        nullable: true,
    })
    schema?: BazaNcIntegrationSchemaDto;

    @Column({
        // Don't use JSONB here - JSONB does not keep requested keys order!
        type: 'json',
        default: {},
        nullable: true,
    })
    properties?: Record<string, any>;

    @Column({
        // Don't use JSONB here - JSONB does not keep requested keys order!
        type: 'json',
        default: [],
    })
    statements: Array<BazaNcIntegrationListingCmsDocumentsDto>;
}
