import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess, BazaNcIntegrationListingsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/008-baza-nc-integration-listings-statements.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);

    let id: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will create a listing with statement documents', async () => {
        const response = await dataAccessListingsCms.create({
            isPublished: true,
            title: 'Example Listings',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [
                {
                    name: 'Statement 1',
                    description: 'State 1 Description',
                    attachment: e2eExampleAttachment(),
                },
                {
                    name: 'Statement 2',
                    description: 'State 2 Description',
                    attachment: e2eExampleAttachment(),
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.statements.length).toBe(2);
        expect(response.statements[0].name).toBe('Statement 1');
        expect(response.statements[1].name).toBe('Statement 2');

        id = response.id;
    });

    it('will return statement documents with get-by-id request', async () => {
        const response = await dataAccessListingsCms.getById({ id });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.statements.length).toBe(2);
        expect(response.statements[0].name).toBe('Statement 1');
        expect(response.statements[1].name).toBe('Statement 2');
    });

    it('will return parsigned url for statement documents with get-by-id request on public listing', async () => {
        const response = await dataAccessListings.getById({ id });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.statements).toBeDefined();
        expect(response.statements[0].documentUrl).toBeDefined();
        expect(response.statements[1].documentUrl).toBeDefined();
        expect(response.statements[0].name).toBe('Statement 1');
        expect(response.statements[1].name).toBe('Statement 2');
    });

    it('will successfully update order of statements', async () => {
        const response = await dataAccessListingsCms.update({
            id,
            isPublished: true,
            title: 'Example Listings',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [
                {
                    name: 'Statement 2',
                    description: 'State 2 Description',
                    attachment: e2eExampleAttachment(),
                },
                {
                    name: 'Statement 1',
                    description: 'State 1 Description',
                    attachment: e2eExampleAttachment(),
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.statements.length).toBe(2);
        expect(response.statements[0].name).toBe('Statement 2');
        expect(response.statements[1].name).toBe('Statement 1');
    });

    it('will display update statement documents order with get-by-id request', async () => {
        const response = await dataAccessListingsCms.getById({ id });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.statements.length).toBe(2);
        expect(response.statements[0].name).toBe('Statement 2');
        expect(response.statements[1].name).toBe('Statement 1');
    });

    it('will display updated statement documents order with get-by-id request on public listing', async () => {
        const response = await dataAccessListings.getById({ id });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.statements).toBeDefined();
        expect(response.statements.length).toBe(2);
        expect(response.statements[0].name).toBe('Statement 2');
        expect(response.statements[1].name).toBe('Statement 1');
    });
});
