export enum BazaNcIntegrationApiListingsFixtures {
    BazaNcIntegrationApiListings = 'BazaNcIntegrationApiListings',
    BazaNcIntegrationApiListingsMany = 'BazaNcIntegrationApiListingsMany',
    BazaNcIntegrationApiListingsManyDividends = 'BazaNcIntegrationApiListingsManyDividends',
    BazaNcIntegrationApiListingsUnpublished = 'BazaNcIntegrationApiListingsUnpublished',
    BazaNcIntegrationApiListingsSchema = 'BazaNcIntegrationApiListingsSchema',
    BazaNcIntegrationApiListingsWithStatements = 'BazaNcIntegrationApiListingsWithStatements',
}
