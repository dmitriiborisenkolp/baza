import { Injectable } from '@nestjs/common';
import { BazaAccountRepository, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationApiListingsFixtures } from '../baza-nc-integration-listings.fixtures';
import { BazaNcIntegrationListingsRepository } from '../../repositories/baza-nc-integration-listings.repository';
import { BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY } from './baza-nc-integration-listings-many.fixture';
import {
    BazaNcDividendProcessingService,
    BazaNcDividendTransactionCmsService,
    BazaNcDividendTransactionEntryCmsService,
    BazaNcInvestorAccountRepository,
} from '@scaliolabs/baza-nc-api';
import { BazaNcDividendPaymentSource } from '@scaliolabs/baza-nc-shared';

interface FixtureRequest {
    listingRefId: number;
    dividends: Array<{
        userEmail: string;
        amountCents: number;
    }>;
}

const BAZA_NC_INTEGRATION_LISTINGS_MANY_DIVIDENDS_FIXTURE: Array<FixtureRequest> = [
    {
        listingRefId: 3,
        dividends: [
            {
                amountCents: 1000,
                userEmail: 'e2e@scal.io',
            },
            {
                amountCents: 1200,
                userEmail: 'e2e@scal.io',
            },
            {
                amountCents: 800,
                userEmail: 'e2e-2@scal.io',
            },
        ],
    },
    {
        listingRefId: 2,
        dividends: [
            {
                amountCents: 2000,
                userEmail: 'e2e@scal.io',
            },
        ],
    },
];

@Injectable()
export class BazaNcIntegrationListingsManyDividendsFixture implements BazaE2eFixture {
    constructor(
        private readonly accountRepository: BazaAccountRepository,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
        private readonly dividendTransactionService: BazaNcDividendTransactionCmsService,
        private readonly dividendTransactionEntriesService: BazaNcDividendTransactionEntryCmsService,
        private readonly dividendTransactionProcessService: BazaNcDividendProcessingService,
    ) {}

    async up(): Promise<void> {
        const dividendTransaction = await this.dividendTransactionService.create({
            title: 'E2E Dividends Fixture <BazaNcIntegrationListingsManyDividendsFixture>',
        });

        for (const fixtureRequest of BAZA_NC_INTEGRATION_LISTINGS_MANY_DIVIDENDS_FIXTURE) {
            const listingRef = BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY.find((next) => next.$refId === fixtureRequest.listingRefId);
            const listing = await this.listingRepository.getById(listingRef.$id);

            for (const dividendFixtureRequest of fixtureRequest.dividends) {
                const account = await this.accountRepository.getActiveAccountWithEmail(dividendFixtureRequest.userEmail);
                const investorAccount = await this.investorAccountRepository.getInvestorAccountByUserId(account.id);

                await this.dividendTransactionEntriesService.create({
                    investorAccountId: investorAccount.id,
                    dividendTransactionUlid: dividendTransaction.ulid,
                    offeringId: listing.offering.ncOfferingId,
                    amountCents: dividendFixtureRequest.amountCents,
                    source: BazaNcDividendPaymentSource.NC,
                    date: new Date().toISOString(),
                });
            }
        }

        await this.dividendTransactionProcessService.process({
            dividendTransactionUlid: dividendTransaction.ulid,
            async: false,
        });
    }
}

defineE2eFixtures<BazaNcIntegrationApiListingsFixtures>([
    {
        fixture: BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsManyDividends,
        provider: BazaNcIntegrationListingsManyDividendsFixture,
    },
]);
