import { Injectable } from '@nestjs/common';
import { BazaCoreAuthUsersFixture, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsCmsService } from '../../services/baza-nc-integration-listings-cms.service';
import { BazaNcIntegrationListingsCmsCreateRequest } from '@scaliolabs/baza-nc-integration-shared';
import { e2eExampleAttachment } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E, BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../baza-nc-integration-listings.fixtures';
import { BazaNcIntegrationListingsCmsStatusService } from '../../services/baza-nc-integration-listings-cms-status.service';

export const BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_WITH_STATEMENTS: Array<{
    $id?: number;
    $refId: number;
    request: BazaNcIntegrationListingsCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            isPublished: true,
            title: 'Example Listings 1',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 1000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [
                {
                    name: 'Statement 1.1',
                    description: 'State 1.1 Description',
                    attachment: e2eExampleAttachment(),
                },
            ],
        },
    },
    {
        $refId: 2,
        request: {
            isPublished: true,
            title: 'Example Listings 2',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 2,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 1000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [
                {
                    name: 'Statement 2.1',
                    description: 'State 2.1 Description',
                    attachment: e2eExampleAttachment(),
                },
                {
                    name: 'Statement 2.2',
                    description: 'State 2.2 Description',
                    attachment: e2eExampleAttachment(),
                },
            ],
        },
    },
    {
        $refId: 3,
        request: {
            isPublished: true,
            title: 'Example Listings 3',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 3,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 1000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [
                {
                    name: 'Statement 3.1',
                    description: 'State 3.1 Description',
                    attachment: e2eExampleAttachment(),
                },
            ],
        },
    },
];

@Injectable()
export class BazaNcIntegrationListingsWithStatementsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcIntegrationListingsCmsService,
        private readonly statusService: BazaNcIntegrationListingsCmsStatusService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_WITH_STATEMENTS) {
            const entity = await this.service.create(BazaCoreAuthUsersFixture._e2eAdmin, fixtureRequest.request);

            fixtureRequest.$id = entity.id;

            await this.statusService.changeStatus({
                id: entity.id,
                newStatus: BazaNcOfferingStatus.Open,
            });
        }
    }
}

defineE2eFixtures<BazaNcIntegrationApiListingsFixtures>([
    {
        fixture: BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsWithStatements,
        provider: BazaNcIntegrationListingsWithStatementsFixture,
    },
]);
