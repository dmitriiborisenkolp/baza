import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationSchemaService } from '../../../../../schema/src';
import { BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../baza-nc-integration-listings.fixtures';

@Injectable()
export class BazaNcIntegrationListingsSchemaFixture implements BazaE2eFixture {
    constructor(private readonly service: BazaNcIntegrationSchemaService) {}

    async up(): Promise<void> {
        await this.service.create({
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'text',
                        title: 'Text',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.TextArea,
                    payload: {
                        field: 'textarea',
                        title: 'Text Area',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.HTML,
                    payload: {
                        field: 'html',
                        title: 'HTML',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Email,
                    payload: {
                        field: 'email',
                        title: 'Email',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Select,
                    payload: {
                        field: 'select',
                        title: 'Select',
                        required: false,
                        isDisplayedInDetails: true,
                        values: {
                            a: 'A',
                            b: 'B',
                            c: 'C',
                        },
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Checkbox,
                    payload: {
                        field: 'checkbox',
                        title: 'Checkbox',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Date,
                    payload: {
                        field: 'date',
                        title: 'Date',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Time,
                    payload: {
                        field: 'time',
                        title: 'Time',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Number,
                    payload: {
                        field: 'number',
                        title: 'Number',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Rate,
                    payload: {
                        field: 'rate',
                        title: 'Rate',
                        required: false,
                        isDisplayedInDetails: true,
                        count: 5,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Slider,
                    payload: {
                        field: 'slider',
                        title: 'Slider',
                        required: false,
                        isDisplayedInDetails: true,
                        min: 0,
                        max: 100,
                        step: 1,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Switcher,
                    payload: {
                        field: 'switcher',
                        title: 'Switcher',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.KeyValue,
                    payload: {
                        field: 'key_value',
                        title: 'Key Value',
                        required: false,
                        isDisplayedInDetails: false,
                    },
                },
            ],
        });
    }
}

defineE2eFixtures([
    {
        fixture: BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsSchema,
        provider: BazaNcIntegrationListingsSchemaFixture,
    },
]);
