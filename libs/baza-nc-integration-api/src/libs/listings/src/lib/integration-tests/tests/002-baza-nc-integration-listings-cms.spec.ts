import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, BazaError, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E, BazaNcOfferingStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationListingsErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/002-baza-nc-integration-api-listings-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsCmsNodeAccess(http);

    let ID: number;
    let ULID: string;
    let OFFERING_ID: OfferingId;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will create new listings', async () => {
        const response = await dataAccessListings.create({
            isPublished: true,
            title: 'Example Listings A',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.offering.ncMinAmount).toBe(10);
        expect(response.offering.ncMaxAmount).toBe(1000);
        expect(response.offering.ncCurrentValueCents).toBe(100000);

        ID = response.id;
        ULID = response.sid;
        OFFERING_ID = response.offering.ncOfferingId;
    });

    it('will display new listings in list', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].id).toBe(ID);
        expect(response.items[0].offering.ncOfferingId).toBe(OFFERING_ID);
    });

    it('will returns listings by id', async () => {
        const response = await dataAccessListings.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(ID);
        expect(response.offering.ncOfferingId).toBe(OFFERING_ID);
    });

    it('will returns listings by ulid', async () => {
        const response = await dataAccessListings.getByUlid({
            ulid: ULID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(ID);
        expect(response.sid).toBe(ULID);
        expect(response.offering.ncOfferingId).toBe(OFFERING_ID);
    });

    it('will update listings', async () => {
        const response = await dataAccessListings.update({
            id: ID,
            isPublished: true,
            title: 'Example Listings *',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncOfferingId: OFFERING_ID,
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 20,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(ID);
        expect(response.offering.ncOfferingId).toBe(OFFERING_ID);
    });

    it('will reflect changes of listings in list', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings *');
        expect(response.items[0].id).toBe(ID);
        expect(response.items[0].offering.ncOfferingId).toBe(OFFERING_ID);
        expect(response.items[0].offering.ncUnitPrice).toBe(20);
    });

    it('will update listings with replacing offering', async () => {
        const response = await dataAccessListings.update({
            id: ID,
            isPublished: true,
            title: 'Example Listings A',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 20,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(ID);
        expect(response.offering.ncOfferingId).not.toBe(OFFERING_ID);
    });

    it('will successfully update offering status', async () => {
        const response = await dataAccessListings.changeStatus({
            id: ID,
            newStatus: BazaNcOfferingStatus.Open,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        const getResponse = await dataAccessListings.getById({
            id: ID,
        });

        expect(getResponse.offering.status).toBe(BazaNcOfferingStatus.Open);
    });

    it('will successfully add one more new listings', async () => {
        const response = await dataAccessListings.create({
            isPublished: true,
            title: 'Example Listings B',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 11,
                ncMaxAmount: 1100,
                ncCurrentValueCents: 110000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.offering.ncMinAmount).toBe(11);
        expect(response.offering.ncMaxAmount).toBe(1100);
        expect(response.offering.ncCurrentValueCents).toBe(110000);
    });

    it('will successfully update sort order in list', async () => {
        const response = await dataAccessListings.setSortOrder({
            id: ID,
            setSortOrder: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.entity.sortOrder).toBe(2);
    });

    it('will display updated sort order in list', async () => {
        const listResponse = await dataAccessListings.list({});

        expect(isBazaErrorResponse(listResponse)).toBeFalsy();

        expect(listResponse.items.length).toBe(2);
        expect(listResponse.items[0].id).toBe(ID);
        expect(listResponse.items[0].sortOrder).toBe(2);
        expect(listResponse.items[0].title).toBe('Example Listings A');
        expect(listResponse.items[1].title).toBe('Example Listings B');
        expect(listResponse.items[1].sortOrder).toBe(1);
    });

    it('will delete listings', async () => {
        const response = await dataAccessListings.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted listings in list', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.items.length).toBe(1);
    });

    it('will not access deleted listings by id', async () => {
        const response: BazaError = (await dataAccessListings.getById({
            id: ID,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound);
    });
});
