import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess, BazaNcIntegrationListingsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../baza-nc-integration-listings.fixtures';
import { NC_OFFERING_E2E } from '@scaliolabs/baza-nc-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/001-baza-nc-integration-api-feature-listings.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);

    let ITEM_ID_1: number;
    let ITEM_ID_3: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListings],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
        await http.authE2eAdmin();
    });

    it('will display list of listings', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Example Listings 1');

        expect(response.items[0].pricePerShareCents).toBe(1000);
        expect(response.items[0].numSharesAvailable).toBe(100);
        expect(response.items[0].percentFunded).toBe(0);
        expect(response.items[0].totalSharesValueCents).toBe(100000);
        expect(response.items[0].numSharesMin).toBe(1);

        ITEM_ID_1 = response.items[0].id;
    });

    it('will returns listings by id', async () => {
        const response = await dataAccessListings.getById({
            id: ITEM_ID_1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Example Listings 1');
    });

    it('will add new item to the listing through cms', async () => {
        const response = await dataAccessListingsCms.create({
            isPublished: true,
            title: 'Example Listings 2',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1001,
                ncMinAmount: 12,
                ncMaxAmount: 1100,
                ncCurrentValueCents: 110000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.offering.ncMinAmount).toBe(12);
        expect(response.offering.ncMaxAmount).toBe(1100);
        expect(response.offering.ncCurrentValueCents).toBe(110000);
    });

    it('will display list of listings with first listing item still on top', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Example Listings 1');
        expect(response.items[0].id).toBe(ITEM_ID_1);
    });

    it('will succefully update setOrder of first item and move it down', async () => {
        const response = await dataAccessListingsCms.setSortOrder({
            id: ITEM_ID_1,
            setSortOrder: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.entity.sortOrder).toBe(2);
    });

    it('will display list of listings with second listing item on top', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Example Listings 2');
        expect(response.items[0].id).not.toBe(ITEM_ID_1);
    });

    it('will add another item to the listing through cms', async () => {
        const response = await dataAccessListingsCms.create({
            isPublished: true,
            title: 'Example Listings 3',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1001,
                ncMinAmount: 12,
                ncMaxAmount: 1100,
                ncCurrentValueCents: 110001,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();
    });

    it('will display list of listings with third item 3 on bottom', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[2].title).toBe('Example Listings 3');
        ITEM_ID_3 = response.items[2].id;
    });

    it('will succefully update setOrder of Item 3', async () => {
        const response = await dataAccessListingsCms.setSortOrder({
            id: ITEM_ID_3,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.entity.sortOrder).toBe(1);
    });

    it('will display list of listings with item 3 on top and second item Item 2', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 1');
    });

    it('will succefully update setOrder of Item 1 and move it up to first item again', async () => {
        const response = await dataAccessListingsCms.setSortOrder({
            id: ITEM_ID_1,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.entity.sortOrder).toBe(1);
    });

    it('will display list of listings with item 1 on top and item 3 at second place', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 1');
        expect(response.items[1].title).toBe('Example Listings 3');
        expect(response.items[2].title).toBe('Example Listings 2');
    });
});
