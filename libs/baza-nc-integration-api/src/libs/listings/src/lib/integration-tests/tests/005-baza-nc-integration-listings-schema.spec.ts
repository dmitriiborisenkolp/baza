import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationSchemaCmsNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../baza-nc-integration-listings.fixtures';
import { BazaNcIntegrationSchemaDto } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/005-baza-nc-integration-listings-schema.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let ID: number;
    let SCHEMA: BazaNcIntegrationSchemaDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsSchema,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns schema', async () => {
        const response = await dataAccessSchemaCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);

        SCHEMA = response.items[0];
    });

    it('will create a new offering with schema', async () => {
        const response = await dataAccessListingsCms.create({
            isPublished: true,
            title: 'Example Listings',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            schema: SCHEMA,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            properties: {
                text: 'Example Text',
                textarea: 'Example Text Area',
                html: '<span>Example HTML</span>',
                email: 'info@example.com',
                select: 'a',
                checkbox: true,
                date: 'Tue Mar 01 2022 00:10:59 GMT+0300 (MSK)',
                time: '2022-02-28T23:17:23.392Z',
                number: 12,
                rate: 5,
                slider: 32,
                switcher: true,
                key_value: {
                    a: 'Value A',
                    b: 'Value B',
                },
            },
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        ID = response.id;
    });

    it('will returns correctly properties', async () => {
        const response = await dataAccessListings.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.properties).toEqual({
            text: 'Example Text',
            textarea: 'Example Text Area',
            html: '<span>Example HTML</span>',
            email: 'info@example.com',
            select: 'a',
            checkbox: true,
            date: 'Tue Mar 01 2022 00:10:59 GMT+0300 (MSK)',
            time: '2022-02-28T23:17:23.392Z',
            number: 12,
            rate: 5,
            slider: {
                min: 0,
                max: 100,
                value: 32,
            },
            switcher: true,
            key_value: {
                a: 'Value A',
                b: 'Value B',
            },
        });
    });
});
