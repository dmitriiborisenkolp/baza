import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E, BazaNcOfferingDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcE2eNodeAccess } from '@scaliolabs/baza-nc-node-access';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/009-baza-nc-integration-listings-cms-create-new-offering.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsCmsNodeAccess(http);

    let ID: number;
    let OFFERING: BazaNcOfferingDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will create a new NC Offering', async () => {
        const response = await dataAccessE2eNc.createExampleOffering();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        OFFERING = response;
    });

    it('will create new listings', async () => {
        const response = await dataAccessListings.create({
            isPublished: true,
            title: 'Example Listings',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncOfferingId: OFFERING.ncOfferingId,
                ncTargetAmount: 10000,
                ncMinAmount: 10,
                ncMaxAmount: 10000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 1000,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2030',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBe(OFFERING.ncOfferingId);

        ID = response.id;
    });

    it('will display new listings in list', async () => {
        const response = await dataAccessListings.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].id).toBe(ID);
        expect(response.items[0].offering.ncOfferingId).toBe(OFFERING.ncOfferingId);
    });

    it('will returns listings by id', async () => {
        const response = await dataAccessListings.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.id).toBe(ID);
        expect(response.offering.ncOfferingId).toBe(OFFERING.ncOfferingId);
    });
});
