import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsNodeAccess, BazaNcIntegrationSubscriptionNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../baza-nc-integration-listings.fixtures';
import { BazaNcIntegrationSubscribeStatus } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/006-baza-nc-integration-subscription-status.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessSubscriptions = new BazaNcIntegrationSubscriptionNodeAccess(http);

    let ID: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListings],
            specFile: __filename,
        });
    });

    it('will return NotSubscribed status for non-auth user', async () => {
        http.noAuth();

        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items[0].isSubscribed).toBeFalsy();
        expect(response.items[0].subscribeStatus).toBe(BazaNcIntegrationSubscribeStatus.NotSubscribed);

        ID = response.items[0].id;
    });

    it('will return NotSubscribed status for auth user', async () => {
        await http.authE2eUser();

        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items[0].isSubscribed).toBeFalsy();
        expect(response.items[0].subscribeStatus).toBe(BazaNcIntegrationSubscribeStatus.NotSubscribed);
    });

    it('will successfully subscribe to listing', async () => {
        await http.authE2eUser();

        const response = await dataAccessSubscriptions.subscribe({
            listingId: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will return Subscribed status', async () => {
        await http.authE2eUser();

        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items[0].isSubscribed).toBeTruthy();
        expect(response.items[0].subscribeStatus).toBe(BazaNcIntegrationSubscribeStatus.Subscribed);
    });
});
