import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess, BazaNcIntegrationListingsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../baza-nc-integration-listings.fixtures';
import { BazaNcIntegrationListingsErrorCodes } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/006-baza-nc-integration-subscription-status.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsUnpublished,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display list of only published listings', async () => {
        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 1');
    });

    it('will not returns unpublished listing with getById or getByOfferingId method', async () => {
        await http.authE2eAdmin();

        const listings = await dataAccessListingsCms.list({});

        expect(isBazaErrorResponse(listings)).toBeFalsy();

        expect(listings.items.length).toBe(3);
        expect(listings.items[1].isPublished).toBeFalsy();

        await http.authE2eUser();

        const getById: BazaError = (await dataAccessListings.getById({
            id: listings.items[1].id,
        })) as any;

        expect(isBazaErrorResponse(getById)).toBeTruthy();
        expect(getById.code).toBe(BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound);

        const getByOfferingId: BazaError = (await dataAccessListings.getByOfferingId({
            offeringId: listings.items[1].offering.ncOfferingId,
        })) as any;

        expect(isBazaErrorResponse(getByOfferingId)).toBeTruthy();
        expect(getByOfferingId.code).toBe(BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound);
    });
});
