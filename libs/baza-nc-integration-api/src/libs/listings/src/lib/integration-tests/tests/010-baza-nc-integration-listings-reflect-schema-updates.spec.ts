import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationSchemaCmsNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationSchemaDto, BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/010-baza-nc-integration-listings-reflect-schema-updates.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let ID: number;
    let SCHEMA: BazaNcIntegrationSchemaDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will create a new schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'example',
                        title: 'Example Field 1',
                        required: false,
                        isDisplayedInDetails: false,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        SCHEMA = response;
    });

    it('will create a new offering with schema', async () => {
        const response = await dataAccessListingsCms.create({
            isPublished: true,
            title: 'Example Listings',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            schema: SCHEMA,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            properties: {
                example: 'Example Value',
            },
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.schema.definitions[0].payload.title).toBe('Example Field 1');
        expect(response.schema.definitions[0].payload.isDisplayedInDetails).toBeFalsy();

        ID = response.id;
    });

    it('will update field title and enable displaying field "Example Field 1" in details', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA.id,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'example',
                        title: 'Example Field 1 *',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display in Listing that flag "isDisplayedInDetails" is enabled and field title is updated', async () => {
        const response = await dataAccessListings.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.schema.definitions[0].payload.title).toBe('Example Field 1 *');
        expect(response.schema.definitions[0].payload.isDisplayedInDetails).toBeTruthy();
    });

    it('will make a breaking change in schema', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA.id,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'example',
                        title: 'Example Field 1 *',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.HTML,
                    payload: {
                        field: 'example_html',
                        title: 'Example Field 2',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will still display updated field title and flags even after breaking changes in schema [1]', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessListings.getById({
                    id: ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.schema.definitions[0].payload.title).toBe('Example Field 1 *');
                expect(response.schema.definitions[0].payload.isDisplayedInDetails).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will change sort order of fields', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA.id,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.HTML,
                    payload: {
                        field: 'example_html',
                        title: 'Example Field 2',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'example',
                        title: 'Example Field 1 *',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will still display updated field title and flags even after breaking changes in schema [2]', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessListings.getById({
                    id: ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.schema.definitions[0].payload.title).toBe('Example Field 1 *');
                expect(response.schema.definitions[0].payload.isDisplayedInDetails).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will change field name', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA.id,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.HTML,
                    payload: {
                        field: 'example_html',
                        title: 'Example Field 2',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'example_updated',
                        title: 'Example Field 1 *',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will update it properly with soft update', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessListings.getById({
                    id: ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.schema.definitions[0].payload.title).toBe('Example Field 1 *');
                expect(response.schema.definitions[0].payload.isDisplayedInDetails).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will remove field completely', async () => {
        const response = await dataAccessSchemaCms.update({
            id: SCHEMA.id,
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.HTML,
                    payload: {
                        field: 'example_html',
                        title: 'Example Field 2',
                        required: false,
                        isDisplayedInDetails: true,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display initial saved schema field [2]', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessListings.getById({
                    id: ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.schema.definitions[0].payload.title).toBe('Example Field 1 *');
                expect(response.schema.definitions[0].payload.isDisplayedInDetails).toBeTruthy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
