import { Injectable } from '@nestjs/common';
import { BazaCoreAuthUsersFixture, BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsCmsService } from '../../services/baza-nc-integration-listings-cms.service';
import { BazaNcIntegrationListingsCmsCreateRequest } from '@scaliolabs/baza-nc-integration-shared';
import { e2eExampleAttachment } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E, BazaNcOfferingStatus } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../baza-nc-integration-listings.fixtures';
import { BazaNcIntegrationListingsCmsStatusService } from '../../services/baza-nc-integration-listings-cms-status.service';

export const BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY: Array<{
    $id?: number;
    $ulid?: string;
    $refId: number;
    request: BazaNcIntegrationListingsCmsCreateRequest;
}> = [
    {
        $refId: 1,
        request: {
            isPublished: true,
            title: 'Example Listings 1',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 1000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        },
    },
    {
        $refId: 2,
        request: {
            isPublished: true,
            title: 'Example Listings 2',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 2,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 1000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        },
    },
    {
        $refId: 3,
        request: {
            isPublished: true,
            title: 'Example Listings 3',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 3,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncMaxSharesPerAccount: 10,
                ncCurrentValueCents: 1000,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        },
    },
];

@Injectable()
export class BazaNcIntegrationListingsManyFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcIntegrationListingsCmsService,
        private readonly statusService: BazaNcIntegrationListingsCmsStatusService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY) {
            const entity = await this.service.create(BazaCoreAuthUsersFixture._e2eAdmin, fixtureRequest.request);

            fixtureRequest.$id = entity.id;
            fixtureRequest.$ulid = entity.sid;

            await this.statusService.changeStatus({
                id: entity.id,
                newStatus: BazaNcOfferingStatus.Open,
            });
        }
    }
}

defineE2eFixtures<BazaNcIntegrationApiListingsFixtures>([
    {
        fixture: BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
        provider: BazaNcIntegrationListingsManyFixture,
    },
]);
