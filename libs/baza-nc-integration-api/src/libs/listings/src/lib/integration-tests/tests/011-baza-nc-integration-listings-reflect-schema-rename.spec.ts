import 'reflect-metadata';
import '@angular/compiler';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationSchemaCmsNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationSchemaDto, BazaNcIntegrationSchemaType } from '@scaliolabs/baza-nc-integration-shared';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/011-baza-nc-integration-listings-reflect-schema-rename.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let ID: number;
    let SCHEMA: BazaNcIntegrationSchemaDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will create a new schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        field: 'example',
                        title: 'Example Field 1',
                        required: false,
                        isDisplayedInDetails: false,
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        SCHEMA = response;
    });

    it('will create a new offering with schema', async () => {
        const response = await dataAccessListingsCms.create({
            isPublished: true,
            title: 'Example Listings',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            schema: SCHEMA,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            properties: {
                example: 'Example Value',
            },
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.schema.definitions[0].payload.title).toBe('Example Field 1');
        expect(response.schema.definitions[0].payload.isDisplayedInDetails).toBeFalsy();

        ID = response.id;
    });

    it('will rename schema', async () => {
        const response = await dataAccessSchemaCms.rename({
            id: SCHEMA.id,
            title: 'Renamed Schema',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will reflect rename in Listing', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessListings.getById({
                    id: ID,
                });

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.title).toBe('Example Listings');
                expect(response.schema.title).toBe('Renamed Schema');
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
