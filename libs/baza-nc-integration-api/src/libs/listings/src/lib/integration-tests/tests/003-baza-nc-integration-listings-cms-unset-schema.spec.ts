import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess, BazaNcIntegrationSchemaCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E, OfferingId } from '@scaliolabs/baza-nc-shared';
import {
    BazaNcIntegrationListingsCmsDto,
    BazaNcIntegrationSchemaDto,
    BazaNcIntegrationSchemaType,
} from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/003-baza-nc-integration-listings-cms-unset-schema.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessSchemaCms = new BazaNcIntegrationSchemaCmsNodeAccess(http);

    let ID: number;
    let OFFERING_ID: OfferingId;
    let SCHEMA_ID: number;
    let SCHEMA: BazaNcIntegrationSchemaDto;
    let LISTING: BazaNcIntegrationListingsCmsDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create new schema', async () => {
        const response = await dataAccessSchemaCms.create({
            title: 'Example Schema',
            published: false,
            definitions: [
                {
                    type: BazaNcIntegrationSchemaType.Text,
                    payload: {
                        required: false,
                        isDisplayedInDetails: true,
                        title: 'Foo',
                        field: 'foo',
                    },
                },
            ],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.updateId).toBeDefined();

        SCHEMA = response;
        SCHEMA_ID = response.id;
    });

    it('will create new listing with schema', async () => {
        const response = await dataAccessListings.create({
            isPublished: true,
            title: 'Example Listing',
            description: 'Example Listing Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 100000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            schema: SCHEMA,
            properties: {
                foo: 'bar',
            },
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        ID = response.id;
        LISTING = response;
        OFFERING_ID = response.offering.ncOfferingId;
    });

    it('will display schema in get listing response', async () => {
        const response = await dataAccessListings.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBe(ID);
        expect(response.schema).toBeDefined();
        expect(response.properties).toBeDefined();

        expect(response.schema).toEqual(SCHEMA);
        expect(response.properties).toEqual({
            foo: 'bar',
        });

        expect(response.offering.ncOfferingId).toBe(OFFERING_ID);
    });

    it('will unset schema with update request', async () => {
        const response = await dataAccessListings.update({
            ...LISTING,
            schema: null,
            schemaId: null,
            properties: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.schema).toBeNull();
        expect(response.properties).toBeNull();

        expect(response.offering.ncOfferingId).toBe(OFFERING_ID);
    });

    it('will display that no schema or properties set in get response', async () => {
        const response = await dataAccessListings.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.schema).toBeNull();
        expect(response.properties).toBeNull();

        expect(response.offering.ncOfferingId).toBe(OFFERING_ID);
    });
});
