import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { NC_OFFERING_E2E, BazaNcOfferingStatus, OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationListingsCmsDto } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/listings/integration-tests/tests/004-baza-nc-integration-status-should-not-reset-after-edit.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsCmsNodeAccess(http);

    let ID: number;
    let OFFERING_ID: OfferingId;
    let LISTING: BazaNcIntegrationListingsCmsDto;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will create new listing with schema', async () => {
        const response = await dataAccessListings.create({
            isPublished: true,
            title: 'Example Listing',
            description: 'Example Listing Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncMaxSharesPerAccount: 10,
                ncCurrentValueCents: 100000,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
        expect(response.offering.ncOfferingId).toBeDefined();

        expect(response.offering.status).toBe(BazaNcOfferingStatus.ComingSoon);

        ID = response.id;
        LISTING = response;
        OFFERING_ID = response.offering.ncOfferingId;
    });

    it('will successfully update listing', async () => {
        const response = await dataAccessListings.changeStatus({
            id: ID,
            newStatus: BazaNcOfferingStatus.Open,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not reset status after updating listing', async () => {
        const response = await dataAccessListings.update({
            ...LISTING,
            title: 'Example Listing 1*',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.offering.ncOfferingId).toBe(OFFERING_ID);
        expect(response.offering.status).toBe(BazaNcOfferingStatus.Open);
    });

    it('will still have updated status for get response', async () => {
        const response = await dataAccessListings.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.offering).toBeDefined();
        expect(response.offering.ncOfferingId).toBe(OFFERING_ID);
        expect(response.offering.status).toBe(BazaNcOfferingStatus.Open);
    });
});
