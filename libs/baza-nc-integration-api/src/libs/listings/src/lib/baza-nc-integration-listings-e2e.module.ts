import { forwardRef, Module } from '@nestjs/common';
import { BazaNcIntegrationListingsApiModule } from './baza-nc-integration-listings-api.module';
import { BazaNcIntegrationListingsFixture } from './integration-tests/fixtures/baza-nc-integration-listings.fixture';
import { BazaNcIntegrationListingsSchemaFixture } from './integration-tests/fixtures/baza-nc-integration-listings-schema.fixture';
import { BazaNcIntegrationListingsManyFixture } from './integration-tests/fixtures/baza-nc-integration-listings-many.fixture';
import { BazaNcIntegrationListingsUnpublishedFixture } from './integration-tests/fixtures/baza-nc-integration-listings-unpublished.fixture';
import { BazaNcIntegrationListingsWithStatementsFixture } from './integration-tests/fixtures/baza-nc-integration-listings-with-statements.fixture';
import { BazaNcIntegrationListingsManyDividendsFixture } from './integration-tests/fixtures/baza-nc-integration-listings-many-dividends.fixture';

const E2E_FIXTURES = [
    BazaNcIntegrationListingsFixture,
    BazaNcIntegrationListingsManyFixture,
    BazaNcIntegrationListingsManyDividendsFixture,
    BazaNcIntegrationListingsSchemaFixture,
    BazaNcIntegrationListingsUnpublishedFixture,
    BazaNcIntegrationListingsWithStatementsFixture,
];

@Module({
    imports: [forwardRef(() => BazaNcIntegrationListingsApiModule)],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcIntegrationListingsE2eModule {}
