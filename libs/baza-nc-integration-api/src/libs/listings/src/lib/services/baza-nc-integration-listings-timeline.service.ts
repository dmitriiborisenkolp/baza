import { Injectable } from '@nestjs/common';
import { CategoryService, TimelineEntity, TimelineRepository } from '@scaliolabs/baza-content-types-api';
import { BazaNcIntegrationListingsEntity } from '../entities/baza-nc-integration-listings.entity';
import { BazaNcIntegrationListingsRepository } from '../repositories/baza-nc-integration-listings.repository';

@Injectable()
export class BazaNcIntegrationListingsTimelineService {
    constructor(
        private readonly categoryService: CategoryService,
        private readonly listingsRepository: BazaNcIntegrationListingsRepository,
        private readonly timelineRepository: TimelineRepository,
    ) {}

    async getListingsTimeline(listings: BazaNcIntegrationListingsEntity): Promise<Array<TimelineEntity>> {
        if (! listings.category) {
            listings.category = await this.categoryService.createInternalCategory({
                title: `baza-nc-integration-listings-${listings.id}`,
            });

            await this.listingsRepository.save([listings]);
        }

        return this.timelineRepository.findTimelineOfCategory(listings.category);
    }
}
