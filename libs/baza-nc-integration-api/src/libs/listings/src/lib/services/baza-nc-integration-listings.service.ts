import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingsMapper } from '../mappers/baza-nc-integration-listings.mapper';
import { BazaNcIntegrationListingsRepository } from '../repositories/baza-nc-integration-listings.repository';
import { AccountEntity, CrudService } from '@scaliolabs/baza-core-api';
import {
    BazaNcIntegrationListingsDto,
    BazaNcIntegrationListingsGetByIdRequest,
    BazaNcIntegrationListingsGetByOfferingIdRequest,
    BazaNcIntegrationListingsListRequest,
    BazaNcIntegrationListingsListResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import {
    BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
    BazaNcIntegrationListingsEntity,
} from '../entities/baza-nc-integration-listings.entity';
import { BazaNcIntegrationListingsNotFoundException } from '../exceptions/baza-nc-integration-listings-not-found.exception';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { bazaEmptyCrudListResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationFavoriteRepository } from '../../../../favorite/src';
import { In, Raw } from 'typeorm';
import { OfferingId } from 'aws-sdk/clients/ec2';

/**
 * Baza NC Integratio nListings Service
 */
@Injectable()
export class BazaNcIntegrationListingsService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: BazaNcIntegrationListingsMapper,
        private readonly repository: BazaNcIntegrationListingsRepository,
        private readonly favoritesRepository: BazaNcIntegrationFavoriteRepository,
    ) {}

    /**
     * Returns Listing by ID
     * Un-published Listing will be considered as Not Found
     */
    async getById(request: BazaNcIntegrationListingsGetByIdRequest): Promise<BazaNcIntegrationListingsEntity> {
        const entity = await this.repository.getById(request.id);

        if (!entity.isPublished) {
            throw new BazaNcIntegrationListingsNotFoundException();
        }

        return entity;
    }

    /**
     * Returns Listing by Offering ID
     * Un-published Listing will be considered as Not Found
     */
    async getByOfferingId(request: BazaNcIntegrationListingsGetByOfferingIdRequest): Promise<BazaNcIntegrationListingsEntity> {
        const entity = await this.repository.getByOfferingId(request.offeringId);

        if (!entity.isPublished) {
            throw new BazaNcIntegrationListingsNotFoundException();
        }

        return entity;
    }

    /**
     * Returns List of Listings
     * Will returns only Published Listings
     */
    async list(request: BazaNcIntegrationListingsListRequest, account?: AccountEntity): Promise<BazaNcIntegrationListingsListResponse> {
        const whereOptions: Array<FindConditions<BazaNcIntegrationListingsEntity>> = [];

        const sortOrder = request.reverse ? 'DESC' : 'ASC';

        const findOptions: FindManyOptions<BazaNcIntegrationListingsEntity> = {
            order: {
                sortOrder,
            },
            where: whereOptions,
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        };

        const allFavorites = request.isFavorite ? await this.favoritesRepository.findAllFavoriteListing(account) : [];

        if (request.isFavorite && (!account || allFavorites.length === 0)) {
            return bazaEmptyCrudListResponse();
        }

        if (Array.isArray(request.schemas) && request.schemas.length > 0) {
            whereOptions.push(
                ...request.schemas.map((title, index) => {
                    const parameter = `json${index}`;

                    return {
                        schema: Raw((alias) => `${alias} ::jsonb @> :${parameter}`, {
                            [parameter]: {
                                title,
                            },
                        }),
                    };
                }),
            );
        }

        if (!whereOptions.length) {
            whereOptions.push({
                isPublished: true,
            });
        }

        for (const whereCond of whereOptions) {
            if (request.isFavorite) {
                whereCond.id = In(allFavorites.map((next) => next.listing.id));
            }

            whereCond.isPublished = true;
        }

        return this.crud.find<BazaNcIntegrationListingsEntity, BazaNcIntegrationListingsDto>({
            request,
            entity: BazaNcIntegrationListingsEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items, account),
            findOptions,
            withMaxSortOrder: true,
        });
    }
}
