import { Injectable } from '@nestjs/common';
import { AccountEntity, CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsCmsMapper } from '../mappers/baza-nc-integration-listings-cms.mapper';
import { BazaNcIntegrationListingsRepository } from '../repositories/baza-nc-integration-listings.repository';
import {
    BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
    BazaNcIntegrationListingsEntity,
} from '../entities/baza-nc-integration-listings.entity';
import {
    BazaNcIntegrationListingsCmsCreateRequest,
    BazaNcIntegrationListingsCmsDeleteRequest,
    BazaNcIntegrationListingsCmsDto,
    BazaNcIntegrationListingsCmsEntityBody,
    BazaNcIntegrationListingsCmsGetByIdRequest,
    BazaNcIntegrationListingsCmsGetByUlidRequest,
    BazaNcIntegrationListingsCmsListRequest,
    BazaNcIntegrationListingsCmsListResponse,
    BazaNcIntegrationListingsCmsUpdateRequest,
    BazaNcIntegrationListingsListItemDto,
    BazaNcIntegrationListingsCmsSetSortOrderRequest,
    BazaNcIntegrationListingsCmsSortOrderResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcOfferingPopulateService } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationListingsListItemMapper } from '../mappers/baza-nc-integration-listings-list-item.mapper';
import { ulid } from 'ulid';
import { BazaNcIntegrationListingOfferingDuplicateException } from '../exceptions/baza-nc-integration-listing-offering-duplicate.exception';
import { BazaNCIntegrationListingsCmsSortedId } from '../types/baza-nc-integration-listings-cms-sorted-id.type';
import { BazaNcIntegrationSchemaMapper, BazaNcIntegrationSchemaService } from '../../../../schema/src';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { Raw } from 'typeorm';

@Injectable()
export class BazaNcIntegrationListingsCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly mapper: BazaNcIntegrationListingsCmsMapper,
        private readonly listMapper: BazaNcIntegrationListingsListItemMapper,
        private readonly repository: BazaNcIntegrationListingsRepository,
        private readonly ncOfferingPopulate: BazaNcOfferingPopulateService,
        private readonly schemaService: BazaNcIntegrationSchemaService,
        private readonly schemaMapper: BazaNcIntegrationSchemaMapper,
    ) {}

    async create(issuer: AccountEntity, request: BazaNcIntegrationListingsCmsCreateRequest): Promise<BazaNcIntegrationListingsEntity> {
        const entity = new BazaNcIntegrationListingsEntity();

        await this.populate(entity, request, issuer);
        await this.repository.save([entity]);

        return entity;
    }

    async update(issuer: AccountEntity, request: BazaNcIntegrationListingsCmsUpdateRequest): Promise<BazaNcIntegrationListingsEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request, issuer);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: BazaNcIntegrationListingsCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);
    }

    async getById(request: BazaNcIntegrationListingsCmsGetByIdRequest): Promise<BazaNcIntegrationListingsEntity> {
        return this.repository.getById(request.id);
    }

    async getByUlid(request: BazaNcIntegrationListingsCmsGetByUlidRequest): Promise<BazaNcIntegrationListingsEntity> {
        return this.repository.getByUlid(request.ulid);
    }

    async list(request: BazaNcIntegrationListingsCmsListRequest): Promise<BazaNcIntegrationListingsCmsListResponse> {
        const sortOrder: 'ASC' | 'DESC' = 'DESC' as const;
        const findOptions: FindManyOptions<BazaNcIntegrationListingsEntity> = {
            order: {
                sortOrder,
            },
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        };

        if (Array.isArray(request.schemas) && request.schemas.length > 0) {
            findOptions.where = request.schemas.map((title, index) => {
                const parameter = `json${index}`;

                return {
                    schema: Raw((alias) => `${alias} ::jsonb @> :${parameter}`, {
                        [parameter]: {
                            title,
                        },
                    }),
                };
            });
        }

        return this.crud.find<BazaNcIntegrationListingsEntity, BazaNcIntegrationListingsCmsDto>({
            request,
            findOptions,
            entity: BazaNcIntegrationListingsEntity,
            mapper: async (items) => await this.mapper.entitiesToDTOs(items),
            withMaxSortOrder: true,
            sortMode: sortOrder,
        });
    }

    async listAll(): Promise<Array<BazaNcIntegrationListingsListItemDto>> {
        const allListings = await this.repository.listAll();

        return this.listMapper.entitiesToDTOs(allListings);
    }

    async setSortOrder(request: BazaNcIntegrationListingsCmsSetSortOrderRequest): Promise<BazaNcIntegrationListingsCmsSortOrderResponse> {
        const ids = await this.repository.getSortedIds();

        const response = await this.crudSort.setSortOrder<BazaNCIntegrationListingsCmsSortedId>({
            id: request.id,
            setSortOrder: request.setSortOrder,
            entities: ids,
        });

        await this.repository.updateSortOrder(ids);

        return response;
    }

    private async populate(
        target: BazaNcIntegrationListingsEntity,
        entityBody: BazaNcIntegrationListingsCmsEntityBody,
        issuer: AccountEntity,
    ): Promise<void> {
        if (target.id) {
            target.dateUpdatedAt = new Date();
        }

        if (!target.id) {
            target.sortOrder = (await this.repository.maxSortOrder()) + 1;
        }

        if (entityBody.offering.ncOfferingId) {
            const existing = await this.repository.findByOfferingId(entityBody.offering.ncOfferingId);

            if (existing && existing.id !== target.id) {
                throw new BazaNcIntegrationListingOfferingDuplicateException();
            }
        }

        if (!target.sid) {
            const date = target.dateCreatedAt || target.dateUpdatedAt || new Date();

            target.sid = ulid(date.getTime());
        }

        target.isPublished = entityBody.isPublished;
        target.title = entityBody.title;
        target.description = entityBody.description;
        target.sharesToJoinEliteInvestors = entityBody.sharesToJoinEliteInvestors;
        target.cover = entityBody.cover;
        target.images = entityBody.images;
        target.metadata = entityBody.metadata;
        target.properties = entityBody.properties;
        target.statements = entityBody.statements;

        if (entityBody.schema) {
            target.schema = entityBody.schema;
        } else if (entityBody.schemaId) {
            const schema = await this.schemaService.getById({
                id: entityBody.schemaId,
            });

            target.schema = this.schemaMapper.entityToDTO(schema);
        } else {
            target.schema = null;
        }

        if (!target.schema) {
            target.schema = null;
            target.properties = null;
        }

        await this.ncOfferingPopulate.populateOffering(issuer, target, entityBody.offering, {
            offeringName: target.title,
            offeringDescription: target.description,
        });
    }
}
