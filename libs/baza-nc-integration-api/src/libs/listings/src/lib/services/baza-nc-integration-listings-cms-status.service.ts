import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingsRepository } from '../repositories/baza-nc-integration-listings.repository';
import { BazaNcOfferingStatusService } from '@scaliolabs/baza-nc-api';
import { BazaNcIntegrationChangeStatusRequest } from '@scaliolabs/baza-nc-integration-shared';

@Injectable()
export class BazaNcIntegrationListingsCmsStatusService {
    constructor(
        private readonly repository: BazaNcIntegrationListingsRepository,
        private readonly ncOfferingStatus: BazaNcOfferingStatusService,
    ) {}

    async changeStatus(request: BazaNcIntegrationChangeStatusRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.ncOfferingStatus.updateStatus(entity.offering.ncOfferingId, request.newStatus);
    }
}
