import { Injectable } from '@nestjs/common';
import { Connection, In, Raw, Repository } from 'typeorm';
import {
    BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
    BazaNcIntegrationListingsEntity,
} from '../entities/baza-nc-integration-listings.entity';
import { BazaNcIntegrationListingsNotFoundException } from '../exceptions/baza-nc-integration-listings-not-found.exception';
import { OfferingId } from '@scaliolabs/baza-nc-shared';
import { BazaNcOfferingEntity, BazaNcOfferingRepository } from '@scaliolabs/baza-nc-api';
import { CommandBus, EventBus } from '@nestjs/cqrs';
import { BazaNcIntegrationListingCreatedEvent } from '../events/baza-nc-integration-listing-created.event';
import { BazaNcIntegrationListingUpdatedEvent } from '../events/baza-nc-integration-listing-updated.event';
import { BazaNcIntegrationListingDeletedEvent } from '../events/baza-nc-integration-listing-deleted.event';
import { BazaNcIntegrationListingIndexPutCommand } from '../commands/baza-nc-integration-listing-index-put.command';
import { BazaNcIntegrationListingIndexDeleteCommand } from '../commands/baza-nc-integration-listing-index-delete.command';
import { BazaNCIntegrationListingsCmsSortedId } from '../types/baza-nc-integration-listings-cms-sorted-id.type';

/**
 * Repository Service for BazaNcIntegrationListingsEntity
 */
@Injectable()
export class BazaNcIntegrationListingsRepository {
    constructor(
        private readonly eventBus: EventBus,
        private readonly commandBus: CommandBus,
        private readonly connection: Connection,
        private readonly offeringRepository: BazaNcOfferingRepository,
    ) {}

    /**
     * Returns TypeORM Repository for BazaNcIntegrationListingsEntity
     */
    get repository(): Repository<BazaNcIntegrationListingsEntity> {
        return this.connection.getRepository(BazaNcIntegrationListingsEntity);
    }

    /**
     * Saves BazaNcIntegrationListingsEntity entities to DB
     * Additional the method will trigger events to CommandBus/EventBus:
     *  - BazaNcIntegrationListingIndexPutCommand
     *  - BazaNcIntegrationListingCreatedEvent
     *  - BazaNcIntegrationListingUpdatedEvent
     * @param entities
     */
    async save(entities: Array<BazaNcIntegrationListingsEntity>): Promise<void> {
        const createdEntities = entities
            .filter((entity) => !entity.id)
            .map((listingEntity) => ({
                listingId: listingEntity.id,
                listingUlid: listingEntity.sid,
                listingEntity,
            }));

        const updatedEntities = entities
            .filter((entity) => !!entity.id)
            .map((listingEntity) => ({
                listingId: listingEntity.id,
                listingUlid: listingEntity.sid,
                listingEntity,
            }));

        await this.repository.save(entities);

        await this.commandBus.execute(new BazaNcIntegrationListingIndexPutCommand([...createdEntities, ...updatedEntities]));

        if (createdEntities.length) {
            this.eventBus.publish(new BazaNcIntegrationListingCreatedEvent(createdEntities));
        }

        if (updatedEntities.length) {
            this.eventBus.publish(new BazaNcIntegrationListingUpdatedEvent(updatedEntities));
        }
    }

    /**
     * Removes BazaNcIntegrationListingsEntity entities from DB
     * Additionally the method will trigger events to CommandBus/EventBus:
     *  - BazaNcIntegrationListingIndexDeleteCommand
     *  - BazaNcIntegrationListingDeletedEvent
     * @param entities
     */
    async remove(entities: Array<BazaNcIntegrationListingsEntity>): Promise<void> {
        const eventEntities = entities.map((listingEntity) => ({
            listingId: listingEntity.id,
            listingUlid: listingEntity.sid,
            listingEntity,
        }));

        await this.repository.remove(entities);

        await this.commandBus.execute(new BazaNcIntegrationListingIndexDeleteCommand(eventEntities));

        this.eventBus.publish(new BazaNcIntegrationListingDeletedEvent(eventEntities));
    }

    /**
     * Returns BazaNcIntegrationListingsEntity by ID
     * The method will returns `undefined` in case if entity with given ID was not found
     * @param id
     */
    async findById(id: number): Promise<BazaNcIntegrationListingsEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    id,
                },
            ],
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        });
    }

    /**
     * Returns BazaNcIntegrationListingsEntity entities with given IDs
     * @param ids
     */
    async findByIds(ids: Array<number>): Promise<Array<BazaNcIntegrationListingsEntity>> {
        return this.repository.find({
            where: [
                {
                    id: In(ids),
                },
            ],
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        });
    }

    /**
     * Returns BazaNcIntegrationListingsEntity by ID
     * The method will throw a BazaNcIntegrationListingsNotFoundException error with
     * BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound error code in case if there is no entity
     * with given ID
     * @param id
     */
    async getById(id: number): Promise<BazaNcIntegrationListingsEntity> {
        const entity = await this.findById(id);

        if (!entity) {
            throw new BazaNcIntegrationListingsNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcIntegrationListingsEntity with given ULID
     * The method will returns `undefined` in case if there is no record with given ULID
     * @param ulid
     */
    async findByUlid(ulid: string): Promise<BazaNcIntegrationListingsEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    sid: ulid,
                },
            ],
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        });
    }

    /**
     * Returns BazaNcIntegrationListingsEntity with given ULID
     * The method will throw a BazaNcIntegrationListingsNotFoundException error with
     * BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound error code in case if there is no entity
     * with given ULID
     * @param ulid
     */
    async getByUlid(ulid: string): Promise<BazaNcIntegrationListingsEntity> {
        const entity = await this.findByUlid(ulid);

        if (!entity) {
            throw new BazaNcIntegrationListingsNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcIntegrationListingsEntity by Offering
     * The method can return `undefined` if there is no BazaNcIntegrationListingsEntity entity associated with given offering
     * @param offering
     */
    async findByOffering(offering: BazaNcOfferingEntity): Promise<BazaNcIntegrationListingsEntity | undefined> {
        return this.repository.findOne({
            where: [
                {
                    offering,
                },
            ],
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        });
    }

    /**
     * Returns BazaNcIntegrationListingsEntity by Offering (ID)
     * The method can return `undefined` if there is no BazaNcIntegrationListingsEntity entity associated with given offering
     * @param offeringId
     */
    async findByOfferingId(offeringId: OfferingId): Promise<BazaNcIntegrationListingsEntity> {
        const offering = await this.offeringRepository.findOfferingWithId(offeringId);

        if (!offering) {
            return undefined;
        }

        return this.findByOffering(offering);
    }

    /**
     * Returns BazaNcIntegrationListingsEntity by Offering (ID)
     * The method will throw a BazaNcIntegrationListingsNotFoundException error with
     * BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound error code in case if there is no entity
     * associated with given Offering
     * @param offeringId
     */
    async getByOfferingId(offeringId: OfferingId): Promise<BazaNcIntegrationListingsEntity> {
        const offering = await this.offeringRepository.findOfferingWithId(offeringId);

        if (!offering) {
            throw new BazaNcIntegrationListingsNotFoundException();
        }

        const entity = await this.findByOffering(offering);

        if (!entity) {
            throw new BazaNcIntegrationListingsNotFoundException();
        }

        return entity;
    }

    /**
     * Returns BazaNcIntegrationListingsEntity associated with given Offerings (IDs)
     * @param offeringIds
     */
    async findManyByOfferingIds(offeringIds: Array<OfferingId>): Promise<Array<BazaNcIntegrationListingsEntity>> {
        // TODO: Rework it
        const allListings = await this.repository.find({
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
        });

        return allListings.filter((listing) => offeringIds.includes(listing.offering?.ncOfferingId));
    }

    /**
     * Returns BazaNcIntegrationListingsEntity associated with given Offerings (IDs)
     * (Published Listings only)
     * @param offeringIds
     */
    async findManyPublishedByOfferingIds(offeringIds: Array<OfferingId>): Promise<
        Array<{
            id: number;
            sid: string;
            title: string;
            offeringId: OfferingId;
            schema: string;
        }>
    > {
        // TODO: Rework it
        const records = await this.repository.find({
            select: ['id', 'sid', 'title', 'offering', 'schema'],
            order: {
                id: 'DESC',
            },
            relations: ['offering'],
            where: [{ isPublished: true }],
        });

        return records
            .filter((next) => offeringIds.includes(next.offering?.ncOfferingId))
            .map((next) => ({
                id: next.id,
                sid: next.sid,
                title: next.title,
                offeringId: next.offering.ncOfferingId,
                schema: next.schema?.displayName,
            }));
    }

    /**
     * Returns all Listings (Sorted by ID DESC)
     * The method should be used only in exceptional cases, because it will be slow for big numbers of Listings
     */
    async listAll(): Promise<Array<BazaNcIntegrationListingsEntity>> {
        // TODO: Optimize for BazaNcIntegrationListingsListItemDto usage
        return this.repository.find({
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
            order: {
                id: 'DESC',
            },
        });
    }

    /**
     * Returns all Listings (in Short format)
     */
    async listAllPublished(): Promise<
        Array<{
            id: number;
            sid: string;
            title: string;
            offeringId: OfferingId;
        }>
    > {
        const records = await this.repository.find({
            select: ['id', 'sid', 'title', 'offering'],
            order: {
                id: 'DESC',
            },
            relations: ['offering'],
            where: [{ isPublished: true }],
        });

        return records.map((next) => ({
            id: next.id,
            sid: next.sid,
            title: next.title,
            offeringId: next.offering.ncOfferingId,
        }));
    }

    /**
     * Returns all Listings (Sorted by ID ASC)
     * The method should be used only in exceptional cases, because it will be slow for big numbers of Listings
     */
    async findAll(): Promise<Array<BazaNcIntegrationListingsEntity>> {
        return this.repository.find({
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
            order: {
                sortOrder: 'ASC',
            },
        });
    }

    /**
     * Returns [id, sortOrder] list of BazaNcIntegrationListingsEntity entities
     */
    async getSortedIds(): Promise<Array<BazaNCIntegrationListingsCmsSortedId>> {
        return this.repository.find({
            select: ['id', 'sortOrder'],
            order: {
                sortOrder: 'ASC',
            },
        });
    }

    /**
     * Updates sortOrder of BazaNcIntegrationListingsEntity entities
     * @param ids
     */
    async updateSortOrder(ids: Array<BazaNCIntegrationListingsCmsSortedId>): Promise<void> {
        await this.repository.save(ids);
    }

    /**
     * Returns ULID of all BazaNcIntegrationListingsEntity entities
     */
    async findAllULIDs(): Promise<Array<string>> {
        const result = await this.repository.createQueryBuilder('listing').select(['listing.sid']).getRawMany();

        return (result || []).map((next) => next.listing_sid);
    }

    /**
     * Returns Listings with specified Schema (Title)
     * @param title
     */
    async findBySchemaTitle(title: string): Promise<Array<BazaNcIntegrationListingsEntity>> {
        return this.repository.find({
            relations: BAZA_NC_INTEGRATION_FEED_LISTINGS_RELATIONS,
            where: [
                {
                    schema: Raw((alias) => `${alias} ::jsonb @> :json`, {
                        json: {
                            title,
                        },
                    }),
                },
            ],
        });
    }

    /**
     * Returns current maximum `sortOrder` value
     */
    async maxSortOrder(): Promise<number> {
        const result: {
            max: number;
        } = await this.repository.createQueryBuilder('e').select('MAX(e.sortOrder)', 'max').getRawOne();

        return result.max || 0;
    }
}
