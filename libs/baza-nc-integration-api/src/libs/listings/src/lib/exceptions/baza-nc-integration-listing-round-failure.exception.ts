import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsErrorCodes, bazaNcIntegrationListingsErrorCodesI18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationListingRoundFailureException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingRoundFailure, bazaNcIntegrationListingsErrorCodesI18n[BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingRoundFailure], HttpStatus.NOT_FOUND);
    }
}
