import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsErrorCodes, bazaNcIntegrationListingsErrorCodesI18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationListingsNotFoundException extends BazaAppException {
    constructor() {
        super(BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound, bazaNcIntegrationListingsErrorCodesI18n[BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound], HttpStatus.NOT_FOUND);
    }
}
