import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationPortfolioNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    AccountTypeCheckingSaving,
    NC_OFFERING_E2E,
    BazaNcPurchaseFlowTransactionType,
    TransactionState,
    NorthCapitalAccountId,
    TradeId,
} from '@scaliolabs/baza-nc-shared';
import {
    BazaNcE2eNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures, BazaNcAchBankAccountMockTranasctionStatus } from '@scaliolabs/baza-nc-api';
import { asyncExpect } from '@scaliolabs/baza-core-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/portfolio/integration-tests/tests/003-baza-nc-integration-portfolio-assets-total.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessPortfolio = new BazaNcIntegrationPortfolioNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessNcE2e = new BazaNcE2eNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);

    let listings: Array<BazaNcIntegrationListingsDto>;
    let transactionId: string;
    let ACCOUNT_ID: NorthCapitalAccountId;
    let TRADE_ID: TradeId;
    let TRADE_ID_2: TradeId;
    let FAILED_TRANSACTION_ID: number;
    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsWithStatements,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        ACCOUNT_ID = investorAccount.northCapitalAccountId;
    });

    it('will display list of listings', async () => {
        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 1');

        listings = response.items;
    });

    it('will display no assets initially', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will display no assets for user w/o investor account', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will start trade session', async () => {
        const LISTING_OFFERING_ID_1 = listings[0].offeringId;

        expect(LISTING_OFFERING_ID_1).toBeDefined();

        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID_1,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        transactionId = session.id;
    });

    it('will still display no assets', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will successfully finish trade session', async () => {
        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: transactionId,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display assets after purchasing shares', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].listing.title).toBe('Example Listings 3');
        expect(response.items[0].totalPurchasedAmountCents).toBe(1000);
        expect(response.items[0].totalPurchasedShares).toBe(1);
        expect(response.items[0].purchasePriceCents).toBe(1000);
        expect(response.items[0].currentValueCents).toBe(1000);
        expect(response.items[0].currentPricePerShareCents).toBe(1000);
        expect(response.items[0].totalCurrentValueCents).toBe(1000);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].totalReturnValueCents).toBe(0);
        expect(response.items[0].totalReturnPercent).toBe(0);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);
    });

    it('will not display any assets for another user', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will successfully purchase Listing 2 share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: listings[1].offeringId,
            numberOfShares: 2,
            amount: 2000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display assets of purchases shares for Listing 2 and Listing 3', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].listing.title).toBe('Example Listings 2');
        expect(response.items[0].totalPurchasedShares).toBe(2);
        expect(response.items[0].totalPurchasedAmountCents).toBe(2000);
        expect(response.items[0].purchasePriceCents).toBe(1000);
        expect(response.items[0].currentValueCents).toBe(1000);
        expect(response.items[0].currentPricePerShareCents).toBe(1000);
        expect(response.items[0].totalCurrentValueCents).toBe(2000);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].totalReturnValueCents).toBe(0);
        expect(response.items[0].totalReturnPercent).toBe(0);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].listing.title).toBe('Example Listings 3');
        expect(response.items[1].totalPurchasedAmountCents).toBe(1000);
        expect(response.items[1].totalPurchasedShares).toBe(1);
        expect(response.items[1].purchasePriceCents).toBe(1000);
        expect(response.items[1].currentValueCents).toBe(1000);
        expect(response.items[1].totalCurrentValueCents).toBe(1000);
        expect(response.items[1].dividendsCents).toBe(0);
        expect(response.items[1].totalReturnValueCents).toBe(0);
        expect(response.items[1].totalReturnPercent).toBe(0);
        expect(response.items[1].state).toBe(TransactionState.PendingPayment);
    });

    it('will display assets of purchases shares for Listing 2 and Listing 3 in reversed order', async () => {
        const response = await dataAccessPortfolio.assetsTotal({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].listing.title).toBe('Example Listings 3');
        expect(response.items[0].totalPurchasedAmountCents).toBe(1000);
        expect(response.items[0].totalPurchasedShares).toBe(1);
        expect(response.items[0].purchasePriceCents).toBe(1000);
        expect(response.items[0].currentValueCents).toBe(1000);
        expect(response.items[0].currentPricePerShareCents).toBe(1000);
        expect(response.items[0].totalCurrentValueCents).toBe(1000);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].totalReturnValueCents).toBe(0);
        expect(response.items[0].totalReturnPercent).toBe(0);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].listing.title).toBe('Example Listings 2');
        expect(response.items[1].totalPurchasedShares).toBe(2);
        expect(response.items[1].totalPurchasedAmountCents).toBe(2000);
        expect(response.items[1].purchasePriceCents).toBe(1000);
        expect(response.items[1].currentValueCents).toBe(1000);
        expect(response.items[1].currentPricePerShareCents).toBe(1000);
        expect(response.items[1].totalCurrentValueCents).toBe(2000);
        expect(response.items[1].dividendsCents).toBe(0);
        expect(response.items[1].totalReturnValueCents).toBe(0);
        expect(response.items[1].totalReturnPercent).toBe(0);
        expect(response.items[1].state).toBe(TransactionState.PendingPayment);
    });

    it('will successfully update purchase price for Listing 3', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessListingsCms.update({
            id: listings[0].id,
            isPublished: true,
            title: 'Example Listings 3',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 3,
            offering: {
                ncOfferingId: listings[0].offeringId,
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 15,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                ncCurrentValueCents: 1500,
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will update assets response', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].listing.title).toBe('Example Listings 2');
        expect(response.items[0].totalPurchasedShares).toBe(2);
        expect(response.items[0].totalPurchasedAmountCents).toBe(2000);
        expect(response.items[0].purchasePriceCents).toBe(1000);
        expect(response.items[0].currentValueCents).toBe(1000);
        expect(response.items[0].currentPricePerShareCents).toBe(1000);
        expect(response.items[0].totalCurrentValueCents).toBe(2000);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].totalReturnValueCents).toBe(0);
        expect(response.items[0].totalReturnPercent).toBe(0);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].listing.title).toBe('Example Listings 3');
        expect(response.items[1].totalPurchasedAmountCents).toBe(1000);
        expect(response.items[1].totalPurchasedShares).toBe(1);
        expect(response.items[1].purchasePriceCents).toBe(1500);
        expect(response.items[1].currentValueCents).toBe(1500);
        expect(response.items[1].currentPricePerShareCents).toBe(1500);
        expect(response.items[1].totalCurrentValueCents).toBe(1500);
        expect(response.items[1].dividendsCents).toBe(0);
        expect(response.items[1].totalReturnValueCents).toBe(500);
        expect(response.items[1].totalReturnPercent).toBe(50);
        expect(response.items[1].state).toBe(TransactionState.PendingPayment);
    });

    it('will successfully purchase more Listing 3 shares', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: listings[0].offeringId,
            numberOfShares: 2,
            amount: 3000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will update once more assets response', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].listing.title).toBe('Example Listings 3');
        expect(response.items[0].totalPurchasedAmountCents).toBe(4000);
        expect(response.items[0].totalPurchasedShares).toBe(3);
        expect(response.items[0].purchasePriceCents).toBe(1500);
        expect(response.items[0].currentValueCents).toBe(1500);
        expect(response.items[0].currentPricePerShareCents).toBe(1500);
        expect(response.items[0].totalCurrentValueCents).toBe(4500);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].totalReturnValueCents).toBe(500);
        expect(response.items[0].totalReturnPercent).toBe(12.5);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].listing.title).toBe('Example Listings 2');
        expect(response.items[1].totalPurchasedShares).toBe(2);
        expect(response.items[1].totalPurchasedAmountCents).toBe(2000);
        expect(response.items[1].purchasePriceCents).toBe(1000);
        expect(response.items[1].currentValueCents).toBe(1000);
        expect(response.items[1].currentPricePerShareCents).toBe(1000);
        expect(response.items[1].totalCurrentValueCents).toBe(2000);
        expect(response.items[1].dividendsCents).toBe(0);
        expect(response.items[1].totalReturnValueCents).toBe(0);
        expect(response.items[1].totalReturnPercent).toBe(0);
        expect(response.items[1].state).toBe(TransactionState.PendingPayment);
    });

    it('will works correctly for -1 size', async () => {
        const response = await dataAccessPortfolio.assetsTotal({
            size: -1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].listing.title).toBe('Example Listings 3');
        expect(response.items[0].totalPurchasedAmountCents).toBe(4000);
        expect(response.items[0].totalPurchasedShares).toBe(3);
        expect(response.items[0].purchasePriceCents).toBe(1500);
        expect(response.items[0].currentValueCents).toBe(1500);
        expect(response.items[0].currentPricePerShareCents).toBe(1500);
        expect(response.items[0].totalCurrentValueCents).toBe(4500);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].totalReturnValueCents).toBe(500);
        expect(response.items[0].totalReturnPercent).toBe(12.5);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].listing.title).toBe('Example Listings 2');
        expect(response.items[1].totalPurchasedShares).toBe(2);
        expect(response.items[1].totalPurchasedAmountCents).toBe(2000);
        expect(response.items[1].purchasePriceCents).toBe(1000);
        expect(response.items[1].currentValueCents).toBe(1000);
        expect(response.items[1].currentPricePerShareCents).toBe(1000);
        expect(response.items[1].totalCurrentValueCents).toBe(2000);
        expect(response.items[1].dividendsCents).toBe(0);
        expect(response.items[1].totalReturnValueCents).toBe(0);
        expect(response.items[1].totalReturnPercent).toBe(0);
        expect(response.items[1].state).toBe(TransactionState.PendingPayment);
    });

    it('will set new bank account for nc to make return the payments', async () => {
        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: BazaNcAchBankAccountMockTranasctionStatus.ReturnedInsufficientFunds,
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));
    });

    it('will successfully purchase another 2 shares for Listing 2', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: listings[1].offeringId,
            numberOfShares: 2,
            amount: 2000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        TRADE_ID = session.id;

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display assets of purchases shares for Listing 2 and Listing 3', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].listing.title).toBe('Example Listings 2');
        expect(response.items[0].totalPurchasedShares).toBe(4);
        expect(response.items[0].totalPurchasedAmountCents).toBe(4000);
        expect(response.items[0].purchasePriceCents).toBe(1000);
        expect(response.items[0].currentValueCents).toBe(1000);
        expect(response.items[0].currentPricePerShareCents).toBe(1000);
        expect(response.items[0].totalCurrentValueCents).toBe(4000);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].listing.title).toBe('Example Listings 3');
        expect(response.items[1].totalPurchasedAmountCents).toBe(4000);
        expect(response.items[1].totalPurchasedShares).toBe(3);
        expect(response.items[1].purchasePriceCents).toBe(1500);
        expect(response.items[1].currentValueCents).toBe(1500);
        expect(response.items[1].currentPricePerShareCents).toBe(1500);
        expect(response.items[1].totalCurrentValueCents).toBe(4500);
        expect(response.items[1].dividendsCents).toBe(0);
        expect(response.items[1].totalReturnValueCents).toBe(500);
        expect(response.items[1].totalReturnPercent).toBe(12.5);
        expect(response.items[1].state).toBe(TransactionState.PendingPayment);
    });

    it('will update newly bought share transaction status to RETURNED', async () => {
        const response = await dataAccessNcE2e.updateExternalFundMoveStatus({
            accountId: ACCOUNT_ID,
            fundMoveStatus: 'RETURNED',
            tradeId: TRADE_ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display assets of purchases shares for Listing 2 and Listing 3 including failed(returned) transaction as lasttransaction for listing 2', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].lastFailedTransaction).toBeNull();
        expect(response.items[0].listing.title).toBe('Example Listings 3');
        expect(response.items[0].totalPurchasedAmountCents).toBe(4000);
        expect(response.items[0].totalPurchasedShares).toBe(3);
        expect(response.items[0].purchasePriceCents).toBe(1500);
        expect(response.items[0].currentValueCents).toBe(1500);
        expect(response.items[0].currentPricePerShareCents).toBe(1500);
        expect(response.items[0].totalCurrentValueCents).toBe(4500);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].totalReturnValueCents).toBe(500);
        expect(response.items[0].totalReturnPercent).toBe(12.5);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].listing.title).toBe('Example Listings 2');
        expect(response.items[1].totalPurchasedShares).toBe(2);
        expect(response.items[1].totalPurchasedAmountCents).toBe(2000);
        expect(response.items[1].purchasePriceCents).toBe(1000);
        expect(response.items[1].currentValueCents).toBe(1000);
        expect(response.items[1].currentPricePerShareCents).toBe(1000);
        expect(response.items[1].totalCurrentValueCents).toBe(2000);
        expect(response.items[1].dividendsCents).toBe(0);
        expect(response.items[1].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].lastFailedTransaction).not.toBeNull();
        expect(response.items[1].lastFailedTransaction.amountCents).toBe(2000);
        expect(response.items[1].lastFailedTransaction.shares).toBe(2);
        expect(response.items[1].lastFailedTransaction.state).toBe(TransactionState.PaymentReturned);
    });

    it('will successfully purchase 10 more Listing 3 shares', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: listings[1].offeringId,
            numberOfShares: 5,
            amount: 5000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        TRADE_ID_2 = session.id;

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display assets of purchases shares for Listing 2 and Listing 3', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessPortfolio.assetsTotal({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(2);

                expect(response.items[0].listing.title).toBe('Example Listings 2');
                expect(response.items[0].totalPurchasedShares).toBe(7);
                expect(response.items[0].totalPurchasedAmountCents).toBe(7000);
                expect(response.items[0].purchasePriceCents).toBe(1000);
                expect(response.items[0].currentValueCents).toBe(1000);
                expect(response.items[0].currentPricePerShareCents).toBe(1000);
                expect(response.items[0].totalCurrentValueCents).toBe(7000);
                expect(response.items[0].dividendsCents).toBe(0);
                expect(response.items[0].state).toBe(TransactionState.PendingPayment);

                expect(response.items[1].listing.title).toBe('Example Listings 3');
                expect(response.items[1].totalPurchasedAmountCents).toBe(4000);
                expect(response.items[1].totalPurchasedShares).toBe(3);
                expect(response.items[1].purchasePriceCents).toBe(1500);
                expect(response.items[1].currentValueCents).toBe(1500);
                expect(response.items[1].currentPricePerShareCents).toBe(1500);
                expect(response.items[1].totalCurrentValueCents).toBe(4500);
                expect(response.items[1].dividendsCents).toBe(0);
                expect(response.items[1].totalReturnValueCents).toBe(500);
                expect(response.items[1].totalReturnPercent).toBe(12.5);
                expect(response.items[1].state).toBe(TransactionState.PendingPayment);
            },
            null,
            { intervalMillis: 5000 },
        );
    });

    it('will update newly bought share transaction status to RETURNED', async () => {
        const response = await dataAccessNcE2e.updateExternalFundMoveStatus({
            accountId: ACCOUNT_ID,
            fundMoveStatus: 'RETURNED',
            tradeId: TRADE_ID_2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display assets of purchases shares for Listing 2 and Listing 3 including the last failed(returned) transaction (10 shares) as lasttransaction for listing 2', async () => {
        const response = await dataAccessPortfolio.assetsTotal({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[0].lastFailedTransaction).toBeNull();
        expect(response.items[0].listing.title).toBe('Example Listings 3');
        expect(response.items[0].totalPurchasedAmountCents).toBe(4000);
        expect(response.items[0].totalPurchasedShares).toBe(3);
        expect(response.items[0].purchasePriceCents).toBe(1500);
        expect(response.items[0].currentValueCents).toBe(1500);
        expect(response.items[0].currentPricePerShareCents).toBe(1500);
        expect(response.items[0].totalCurrentValueCents).toBe(4500);
        expect(response.items[0].dividendsCents).toBe(0);
        expect(response.items[0].totalReturnValueCents).toBe(500);
        expect(response.items[0].totalReturnPercent).toBe(12.5);
        expect(response.items[0].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].listing.title).toBe('Example Listings 2');
        expect(response.items[1].totalPurchasedShares).toBe(2);
        expect(response.items[1].totalPurchasedAmountCents).toBe(2000);
        expect(response.items[1].purchasePriceCents).toBe(1000);
        expect(response.items[1].currentValueCents).toBe(1000);
        expect(response.items[1].currentPricePerShareCents).toBe(1000);
        expect(response.items[1].totalCurrentValueCents).toBe(2000);
        expect(response.items[1].dividendsCents).toBe(0);
        expect(response.items[1].state).toBe(TransactionState.PendingPayment);

        expect(response.items[1].lastFailedTransaction).not.toBeNull();
        expect(response.items[1].lastFailedTransaction.amountCents).toBe(5000);
        expect(response.items[1].lastFailedTransaction.shares).toBe(5);
        expect(response.items[1].lastFailedTransaction.state).toBe(TransactionState.PaymentReturned);

        FAILED_TRANSACTION_ID = response.items[1].lastFailedTransaction.id;
    });

    it('request to reprocess the payment for 10 shares', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessPurchaseFlow.reprocessPayment({
                    id: FAILED_TRANSACTION_ID,
                });
                expect(isBazaErrorResponse(response)).toBeFalsy();
            },
            null,
            { intervalMillis: 2000 },
        );
    });

    it('will have the reprocess payment for 10 shares again in the assets list and the recent failed payment for 2 shares set as lastFailedTransaction ', async () => {
        await asyncExpect(
            async () => {
                const response = await dataAccessPortfolio.assetsTotal({});

                expect(isBazaErrorResponse(response)).toBeFalsy();

                expect(response.items.length).toBe(2);

                expect(response.items[0].listing.title).toBe('Example Listings 2');
                expect(response.items[0].totalPurchasedShares).toBe(7);
                expect(response.items[0].totalPurchasedAmountCents).toBe(7000);
                expect(response.items[0].purchasePriceCents).toBe(1000);
                expect(response.items[0].currentValueCents).toBe(1000);
                expect(response.items[0].currentPricePerShareCents).toBe(1000);
                expect(response.items[0].totalCurrentValueCents).toBe(7000);
                expect(response.items[0].dividendsCents).toBe(0);
                expect(response.items[0].state).toBe(TransactionState.PendingPayment);

                expect(response.items[1].listing.title).toBe('Example Listings 3');
                expect(response.items[1].totalPurchasedAmountCents).toBe(4000);
                expect(response.items[1].totalPurchasedShares).toBe(3);
                expect(response.items[1].purchasePriceCents).toBe(1500);
                expect(response.items[1].currentValueCents).toBe(1500);
                expect(response.items[1].currentPricePerShareCents).toBe(1500);
                expect(response.items[1].totalCurrentValueCents).toBe(4500);
                expect(response.items[1].dividendsCents).toBe(0);
                expect(response.items[1].totalReturnValueCents).toBe(500);
                expect(response.items[1].totalReturnPercent).toBe(12.5);
                expect(response.items[1].state).toBe(TransactionState.PendingPayment);

                expect(response.items[0].lastFailedTransaction).not.toBeNull();
                expect(response.items[0].lastFailedTransaction.amountCents).toBe(2000);
                expect(response.items[0].lastFailedTransaction.shares).toBe(2);
                expect(response.items[0].lastFailedTransaction.state).toBe(TransactionState.PaymentReturned);
            },
            null,
            { intervalMillis: 2000 },
        );
    });
});
