import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsNodeAccess, BazaNcIntegrationPortfolioNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { AccountTypeCheckingSaving, BazaNcPurchaseFlowTransactionType } from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/portfolio/integration-tests/tests/001-baza-nc-integration-portfolio-statements.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessPortfolio = new BazaNcIntegrationPortfolioNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);

    let listings: Array<BazaNcIntegrationListingsDto>;
    let transactionId: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsWithStatements,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display list of listings', async () => {
        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 1');

        listings = response.items;
    });

    it('will display no statement documents initially', async () => {
        const response = await dataAccessPortfolio.statements({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will display no statement documents initially for user w/o investor account', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPortfolio.statements({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will start trade session', async () => {
        const LISTING_OFFERING_ID_1 = listings[0].offeringId;

        expect(LISTING_OFFERING_ID_1).toBeDefined();

        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID_1,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        transactionId = session.id;
    });

    it('will still display no statement documents', async () => {
        const response = await dataAccessPortfolio.statements({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will successfully finish trade session', async () => {
        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: transactionId,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display statement documents of purchases shares', async () => {
        const response = await dataAccessPortfolio.statements({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].name).toBe('Example Listings 3');
        expect(response.items[0].statements.length).toBe(1);
        expect(response.items[0].statements[0].name).toBe('Statement 3.1');
    });

    it('will successfully purchase Listing 2 share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: listings[1].offeringId,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display statement documents of purchases shares for Listing 3 and Listing 2', async () => {
        const response = await dataAccessPortfolio.statements({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect(response.items[1].name).toBe('Example Listings 2');
        expect(response.items[1].statements.length).toBe(2);
        expect(response.items[1].statements[0].name).toBe('Statement 2.1');
        expect(response.items[1].statements[1].name).toBe('Statement 2.2');

        expect(response.items[0].name).toBe('Example Listings 3');
        expect(response.items[0].statements.length).toBe(1);
        expect(response.items[0].statements[0].name).toBe('Statement 3.1');
    });
});
