import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationPortfolioNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';
import {
    BazaNcE2eNodeAccess,
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import {
    AccountTypeCheckingSaving,
    BazaNcPurchaseFlowTransactionType,
    NC_OFFERING_E2E,
    OrderStatus,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';

jest.setTimeout(120000);

describe('@scaliolabs/baza-nc-integration-api/portfolio/integration-tests/tests/006-baza-nc-integration-portfolio-assets-with-dividends.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessE2eNc = new BazaNcE2eNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessPortfolio = new BazaNcIntegrationPortfolioNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);

    let listings: Array<BazaNcIntegrationListingsDto>;
    let transactionId: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUser2Fixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsManyDividends,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display list of listings', async () => {
        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 1');

        listings = response.items;
    });

    it('will display that dividends applied to e2e@scal.io user', async () => {
        const totalStats = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(totalStats)).toBeFalsy();

        expect(totalStats.totalDividendCents).toBe(4200);
        expect(totalStats.totalReturnValueCents).toBe(4200);
    });

    it('will display that dividends applied to e2e-2@scal.io user', async () => {
        await http.authE2eUser2();

        const totalStats = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(totalStats)).toBeFalsy();

        expect(totalStats.totalDividendCents).toBe(800);
        expect(totalStats.totalReturnValueCents).toBe(800);
    });

    it('will purchase share of Listing 3', async () => {
        const listingOfferingId = listings[0].offeringId;

        expect(listingOfferingId).toBeDefined();

        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: listingOfferingId,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        transactionId = session.id;

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: transactionId,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display correct assets card with dividends for e2e@scal.io user (1)', async () => {
        const assets = await dataAccessPortfolio.assets({});

        expect(isBazaErrorResponse(assets)).toBeFalsy();

        expect(assets.items.length).toBe(1);

        expect(assets.items[0].listing.title).toBe('Example Listings 3');
        expect(assets.items[0].dividendsCents).toBe(2200);
        expect(assets.items[0].totalPurchasedAmountCents).toBe(1000);
        expect(assets.items[0].totalCurrentValueCents).toBe(1000);
        expect(assets.items[0].totalReturnValueCents).toBe(2200);
        expect(assets.items[0].totalReturnPercent).toBe(220);
    });

    it('will purchase share of Listing 2', async () => {
        const listingOfferingId = listings[1].offeringId;

        expect(listingOfferingId).toBeDefined();

        const session = await dataAccessPurchaseFlow.session({
            offeringId: listingOfferingId,
            numberOfShares: 2,
            amount: 2000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        transactionId = session.id;

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: transactionId,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display correct assets card with dividends for e2e@scal.io user (2)', async () => {
        const assets = await dataAccessPortfolio.assets({});

        expect(isBazaErrorResponse(assets)).toBeFalsy();

        expect(assets.items.length).toBe(2);

        expect(assets.items[0].listing.title).toBe('Example Listings 2');
        expect(assets.items[0].isPrimaryCard).toBeTruthy();
        expect(assets.items[0].dividendsCents).toBe(2000);
        expect(assets.items[0].totalPurchasedAmountCents).toBe(2000);
        expect(assets.items[0].totalCurrentValueCents).toBe(2000);
        expect(assets.items[0].totalReturnValueCents).toBe(2000);
        expect(assets.items[0].totalReturnPercent).toBe(100);

        expect(assets.items[1].listing.title).toBe('Example Listings 3');
        expect(assets.items[1].isPrimaryCard).toBeTruthy();
        expect(assets.items[1].dividendsCents).toBe(2200);
        expect(assets.items[1].totalPurchasedAmountCents).toBe(1000);
        expect(assets.items[1].totalCurrentValueCents).toBe(1000);
        expect(assets.items[1].totalReturnValueCents).toBe(2200);
        expect(assets.items[1].totalReturnPercent).toBe(220);
    });

    it('will update current value for Listing 3', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessListingsCms.update({
            id: listings[0].id,
            isPublished: true,
            title: 'Example Listings 3',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 1,
            offering: {
                ncOfferingId: listings[0].offeringId,
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncCurrentValueCents: 1200,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 10,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display correct assets card with dividends for e2e@scal.io user (3)', async () => {
        const assets = await dataAccessPortfolio.assets({});

        expect(isBazaErrorResponse(assets)).toBeFalsy();

        expect(assets.items.length).toBe(2);

        expect(assets.items[0].listing.title).toBe('Example Listings 2');
        expect(assets.items[0].isPrimaryCard).toBeTruthy();
        expect(assets.items[0].dividendsCents).toBe(2000);
        expect(assets.items[0].totalPurchasedAmountCents).toBe(2000);
        expect(assets.items[0].totalCurrentValueCents).toBe(2000);
        expect(assets.items[0].totalReturnValueCents).toBe(2000);
        expect(assets.items[0].totalReturnPercent).toBe(100);

        expect(assets.items[1].listing.title).toBe('Example Listings 3');
        expect(assets.items[1].isPrimaryCard).toBeTruthy();
        expect(assets.items[1].dividendsCents).toBe(2200);
        expect(assets.items[1].totalPurchasedAmountCents).toBe(1000);
        expect(assets.items[1].totalCurrentValueCents).toBe(1200);
        expect(assets.items[1].totalReturnValueCents).toBe(2400);
        expect(assets.items[1].totalReturnPercent).toBe(240);
    });

    it('will purchase share & mark as funded one more share of Listing 2', async () => {
        const listingOfferingId = listings[1].offeringId;

        expect(listingOfferingId).toBeDefined();

        const session = await dataAccessPurchaseFlow.session({
            offeringId: listingOfferingId,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();

        const setTradeResponse = await dataAccessE2eNc.setTradeStatus({
            tradeId: transactionId,
            transactionState: TransactionState.PaymentFunded,
            orderStatus: OrderStatus.Funded,
        });

        expect(isBazaErrorResponse(setTradeResponse)).toBeFalsy();
    });

    it('will display correct assets card with dividends for e2e@scal.io user (4)', async () => {
        const assets = await dataAccessPortfolio.assets({});

        expect(isBazaErrorResponse(assets)).toBeFalsy();

        expect(assets.items.length).toBe(3);

        expect(assets.items[0].listing.title).toBe('Example Listings 2');
        expect(assets.items[0].isPrimaryCard).toBeFalsy();
        expect(assets.items[0].dividendsCents).toBe(0);
        expect(assets.items[0].totalPurchasedAmountCents).toBe(1000);
        expect(assets.items[0].totalCurrentValueCents).toBe(1000);
        expect(assets.items[0].totalReturnValueCents).toBe(0);
        expect(assets.items[0].totalReturnPercent).toBe(0);
        expect(assets.items[0].state).toBe(TransactionState.PendingPayment);

        expect(assets.items[1].listing.title).toBe('Example Listings 2');
        expect(assets.items[1].isPrimaryCard).toBeTruthy();
        expect(assets.items[1].dividendsCents).toBe(2000);
        expect(assets.items[1].totalPurchasedAmountCents).toBe(2000);
        expect(assets.items[1].totalCurrentValueCents).toBe(2000);
        expect(assets.items[1].totalReturnValueCents).toBe(2000);
        expect(assets.items[1].totalReturnPercent).toBe(100);
        expect(assets.items[1].state).toBe(TransactionState.PaymentFunded);

        expect(assets.items[2].listing.title).toBe('Example Listings 3');
        expect(assets.items[2].dividendsCents).toBe(2200);
        expect(assets.items[2].isPrimaryCard).toBeTruthy();
        expect(assets.items[2].totalPurchasedAmountCents).toBe(1000);
        expect(assets.items[2].totalCurrentValueCents).toBe(1200);
        expect(assets.items[2].totalReturnValueCents).toBe(2400);
        expect(assets.items[2].totalReturnPercent).toBe(240);
        expect(assets.items[2].state).toBe(TransactionState.PendingPayment);
    });
});
