import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsNodeAccess, BazaNcIntegrationPortfolioNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsDto, BazaNcIntegrationPortfolioTransactionInvestmentDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    AccountTypeCheckingSaving,
    BazaNcPurchaseFlowTransactionType,
    NorthCapitalAccountId,
    TradeId,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';
import {
    BazaNcPurchaseFlowBankAccountNodeAccess,
    BazaNcPurchaseFlowNodeAccess,
    BazaNcInvestorAccountNodeAccess,
    BazaNcE2eNodeAccess,
} from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures, BazaNcApiOfferingFixtures, BazaNcDividendFixtures } from '@scaliolabs/baza-nc-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/portfolio/integration-tests/tests/007-baza-nc-integration-portfolio-filter-transactions-by-status.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessPortfolio = new BazaNcIntegrationPortfolioNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);
    const dataAccessInvestorAccount = new BazaNcInvestorAccountNodeAccess(http);
    const dataAccessNcE2e = new BazaNcE2eNodeAccess(http);

    let TRADE_ID_LISTING_2: TradeId;
    let ACCOUNT_ID: NorthCapitalAccountId;
    let listings: Array<BazaNcIntegrationListingsDto>;
    let transactionId: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsWithStatements,
                BazaNcApiOfferingFixtures.BazaNcOfferingFixture,
                BazaNcDividendFixtures.BazaNcDividendTransactionFixture,
            ],
            specFile: __filename,
        });

        await http.authE2eUser();

        const investorAccount = await dataAccessInvestorAccount.current();

        ACCOUNT_ID = investorAccount.northCapitalAccountId;
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display list of listings', async () => {
        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 1');

        listings = response.items;
    });

    it('will display no transactions initially', async () => {
        const response = await dataAccessPortfolio.transactions({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will display no transactions for user w/o investor account', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPortfolio.transactions({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will start trade session', async () => {
        const LISTING_OFFERING_ID_1 = listings[0].offeringId;

        expect(LISTING_OFFERING_ID_1).toBeDefined();

        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID_1,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        transactionId = session.id;
    });

    it('will still display no transactions', async () => {
        const response = await dataAccessPortfolio.transactions({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will successfully finish trade session', async () => {
        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: transactionId,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display transaction after purchasing shares', async () => {
        const response = await dataAccessPortfolio.transactions({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.name).toBe('Example Listings 3');
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.shares).toBe(1);
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.amountCents).toBe(1000);
    });

    it('will successfully purchase Listing 2 share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: listings[1].offeringId,
            numberOfShares: 2,
            amount: 2000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();

        TRADE_ID_LISTING_2 = session.id;
    });

    it('will display transactions of purchases shares for Listing 3 and Listing 2', async () => {
        const response = await dataAccessPortfolio.transactions({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.name).toBe('Example Listings 2');
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.shares).toBe(2);
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.amountCents).toBe(2000);

        expect((response.items[1] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.name).toBe('Example Listings 3');
        expect((response.items[1] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.shares).toBe(1);
        expect((response.items[1] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.amountCents).toBe(1000);
    });

    it('will update transaction status to RETURNED', async () => {
        const response = await dataAccessNcE2e.updateExternalFundMoveStatus({
            accountId: ACCOUNT_ID,
            fundMoveStatus: 'RETURNED',
            tradeId: TRADE_ID_LISTING_2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display transactions of purchases shares for Listing 3 and Listing 2', async () => {
        const response = await dataAccessPortfolio.transactions({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.state).toBe(
            TransactionState.PaymentReturned,
        );
    });

    it('will filter transactions based on returned status', async () => {
        const response = await dataAccessPortfolio.transactions({
            states: [TransactionState.PaymentReturned],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);

        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.name).toBe('Example Listings 2');
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.shares).toBe(2);
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.amountCents).toBe(2000);
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.state).toBe(
            TransactionState.PaymentReturned,
        );
    });

    it('will filter transactions based on returned and pending payemnt status', async () => {
        const response = await dataAccessPortfolio.transactions({
            states: [TransactionState.PaymentReturned, TransactionState.PendingPayment],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(2);

        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.name).toBe('Example Listings 2');
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.shares).toBe(2);
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.amountCents).toBe(2000);
        expect((response.items[0] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.state).toBe(
            TransactionState.PaymentReturned,
        );

        expect((response.items[1] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.name).toBe('Example Listings 3');
        expect((response.items[1] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.shares).toBe(1);
        expect((response.items[1] as BazaNcIntegrationPortfolioTransactionInvestmentDto).entity.amountCents).toBe(1000);
    });
});
