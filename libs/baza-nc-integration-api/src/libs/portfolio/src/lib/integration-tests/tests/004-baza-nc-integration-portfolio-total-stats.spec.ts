import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationPortfolioNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    AccountTypeCheckingSaving,
    NC_OFFERING_E2E,
    BazaNcPurchaseFlowTransactionType,
    TransactionState,
} from '@scaliolabs/baza-nc-shared';
import { BazaNcPurchaseFlowBankAccountNodeAccess, BazaNcPurchaseFlowNodeAccess } from '@scaliolabs/baza-nc-node-access';
import { BazaNcAccountVerificationFixtures } from '@scaliolabs/baza-nc-api';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/portfolio/integration-tests/tests/004-baza-nc-integration-portfolio-total-stats.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessPortfolio = new BazaNcIntegrationPortfolioNodeAccess(http);
    const dataAccessPurchaseFlow = new BazaNcPurchaseFlowNodeAccess(http);
    const dataAccessPurchaseFlowBankAccount = new BazaNcPurchaseFlowBankAccountNodeAccess(http);

    let listings: Array<BazaNcIntegrationListingsDto>;
    let transactionId: string;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcAccountVerificationFixtures.BazaNcAccountVerificationE2eUserFixture,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsWithStatements,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will display list of listings', async () => {
        const response = await dataAccessListings.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);
        expect(response.items[0].title).toBe('Example Listings 3');
        expect(response.items[1].title).toBe('Example Listings 2');
        expect(response.items[2].title).toBe('Example Listings 1');

        listings = response.items;
    });

    it('will works correctly for user w/o investor account', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalHoldings).toBe(0);
        expect(response.totalHoldingAmountCents).toBe(0);
        expect(response.totalHoldingCurrentValueCents).toBe(0);
        expect(response.totalReturnValueCents).toBe(0);
        expect(response.totalReturnPercent).toBe(0);
    });

    it('will display no total amounts initially', async () => {
        const response = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalHoldings).toBe(0);
        expect(response.totalHoldingAmountCents).toBe(0);
        expect(response.totalHoldingCurrentValueCents).toBe(0);
        expect(response.totalReturnValueCents).toBe(0);
        expect(response.totalReturnPercent).toBe(0);
    });

    it('will start trade session', async () => {
        const LISTING_OFFERING_ID_1 = listings[0].offeringId;

        expect(LISTING_OFFERING_ID_1).toBeDefined();

        const setBankAccountResponse = await dataAccessPurchaseFlowBankAccount.setBankAccount({
            accountName: 'Foo Bar',
            accountRoutingNumber: '011401533',
            accountNumber: '1111222233330000',
            accountType: AccountTypeCheckingSaving.Savings,
        });

        expect(isBazaErrorResponse(setBankAccountResponse));

        const session = await dataAccessPurchaseFlow.session({
            offeringId: LISTING_OFFERING_ID_1,
            numberOfShares: 1,
            amount: 1000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        transactionId = session.id;
    });

    it('will still display no total amounts', async () => {
        const response = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalHoldings).toBe(0);
        expect(response.totalHoldingAmountCents).toBe(0);
        expect(response.totalHoldingCurrentValueCents).toBe(0);
        expect(response.totalReturnValueCents).toBe(0);
        expect(response.totalReturnPercent).toBe(0);
    });

    it('will successfully finish trade session', async () => {
        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: transactionId,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display total amounts after purchasing shares', async () => {
        const response = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalHoldings).toBe(1);
        expect(response.totalHoldingAmountCents).toBe(1000);
        expect(response.totalHoldingCurrentValueCents).toBe(1000);
        expect(response.totalReturnValueCents).toBe(0);
        expect(response.totalReturnPercent).toBe(0);
    });

    it('will not display any total amounts for another user', async () => {
        await http.authE2eUser2();

        const response = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalHoldings).toBe(0);
        expect(response.totalHoldingAmountCents).toBe(0);
        expect(response.totalHoldingCurrentValueCents).toBe(0);
        expect(response.totalReturnValueCents).toBe(0);
        expect(response.totalReturnPercent).toBe(0);
    });

    it('will successfully purchase Listing 2 share', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: listings[1].offeringId,
            numberOfShares: 2,
            amount: 2000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will display assets of purchases shares for Listing 2 and Listing 3', async () => {
        const response = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalHoldings).toBe(3);
        expect(response.totalHoldingAmountCents).toBe(3000);
        expect(response.totalHoldingCurrentValueCents).toBe(3000);
        expect(response.totalReturnValueCents).toBe(0);
        expect(response.totalReturnPercent).toBe(0);
    });

    it('will successfully update purchase price for Listing 3', async () => {
        await http.authE2eAdmin();

        const response = await dataAccessListingsCms.update({
            id: listings[0].id,
            isPublished: true,
            title: 'Example Listings 3',
            description: 'Example Listings Description',
            cover: e2eExampleAttachment(),
            images: [e2eExampleAttachment()],
            sharesToJoinEliteInvestors: 3,
            offering: {
                ncOfferingId: listings[0].offeringId,
                ncTargetAmount: 1000,
                ncMinAmount: 10,
                ncMaxAmount: 1000,
                ncMaxSharesPerAccount: 10,
                ncUnitPrice: 15,
                ncStartDate: '01-01-2021',
                ncEndDate: '01-01-2099',
                ncSubscriptionOfferingId: NC_OFFERING_E2E.docuSignExampleTemplateIds[0],
                ncCurrentValueCents: 1500,
            },
            metadata: {},
            statements: [],
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will update assets response', async () => {
        const response = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalHoldings).toBe(3);
        expect(response.totalHoldingAmountCents).toBe(3000);
        expect(response.totalHoldingCurrentValueCents).toBe(3500);
        expect(response.totalReturnValueCents).toBe(500);
        expect(response.totalReturnPercent).toBe(16.67);
    });

    it('will successfully purchase more Listing 3 shares', async () => {
        const session = await dataAccessPurchaseFlow.session({
            offeringId: listings[0].offeringId,
            numberOfShares: 2,
            amount: 3000,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        });

        expect(isBazaErrorResponse(session)).toBeFalsy();

        const submitResponse = await dataAccessPurchaseFlow.submit({
            id: session.id,
        });

        expect(isBazaErrorResponse(submitResponse)).toBeFalsy();
    });

    it('will update once more assets response', async () => {
        const response = await dataAccessPortfolio.total();

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.totalHoldings).toBe(5);
        expect(response.totalHoldingAmountCents).toBe(6000);
        expect(response.totalHoldingCurrentValueCents).toBe(6500);
        expect(response.totalReturnValueCents).toBe(500);
        expect(response.totalReturnPercent).toBe(8.33);
    });
});
