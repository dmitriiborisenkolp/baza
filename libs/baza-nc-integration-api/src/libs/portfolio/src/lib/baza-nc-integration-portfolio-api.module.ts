import { Module } from '@nestjs/common';
import { BazaNcIntegrationPortfolioController } from './controllers/baza-nc-integration-portfolio.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { BazaNcIntegrationPortfolioService } from './services/baza-nc-integration-portfolio.service';
import { BazaNcIntegrationPortfolioAssetsService } from './services/baza-nc-integration-portfolio-assets.service';
import { BazaNcIntegrationPortfolioStatementsService } from './services/baza-nc-integration-portfolio-statements.service';
import { BazaNcIntegrationPortfolioTotalStatsService } from './services/baza-nc-integration-portfolio-total-stats.service';
import { BazaNcIntegrationPortfolioTransactionsService } from './services/baza-nc-integration-portfolio-transactions.service';
import { BazaNcTransferApiModule, BazaNcWithdrawApiModule } from '@scaliolabs/baza-nc-api';

const providers = [
    BazaNcIntegrationPortfolioService,
    BazaNcIntegrationPortfolioAssetsService,
    BazaNcIntegrationPortfolioStatementsService,
    BazaNcIntegrationPortfolioTotalStatsService,
    BazaNcIntegrationPortfolioTransactionsService,
];

@Module({
    imports: [CqrsModule, BazaNcWithdrawApiModule, BazaNcTransferApiModule],
    controllers: [BazaNcIntegrationPortfolioController],
    providers,
    exports: providers,
})
export class BazaNcIntegrationPortfolioApiModule {}
