import { BazaNcDividendService, BazaNcInvestorAccountRepository, BazaNcTransactionsService } from '@scaliolabs/baza-nc-api';
import {
    BazaNcIntegrationPortfolioTransactionDividendDto,
    BazaNcIntegrationPortfolioTransactionDto,
    BazaNcIntegrationPortfolioTransactionInvestmentDto,
    BazaNcIntegrationPortfolioTransactionsRequest,
    BazaNcIntegrationPortfolioTransactionsResponse,
    BazaNcIntegrationPortfolioTransactionType,
} from '@scaliolabs/baza-nc-integration-shared';
import { bazaEmptyCrudListResponse, paginate } from '@scaliolabs/baza-core-shared';
import { Injectable } from '@nestjs/common';
import { AccountEntity, BAZA_CRUD_DEFAULT_SIZE } from '@scaliolabs/baza-core-api';

@Injectable()
export class BazaNcIntegrationPortfolioTransactionsService {
    constructor(
        private readonly ncTransactions: BazaNcTransactionsService,
        private readonly dividends: BazaNcDividendService,
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
    ) {}

    async transactions(request: {
        account: AccountEntity;
        listRequest: BazaNcIntegrationPortfolioTransactionsRequest;
    }): Promise<BazaNcIntegrationPortfolioTransactionsResponse> {
        const investorAccount = await this.investorAccountRepository.findInvestorAccountByUserId(request.account.id);

        request.listRequest.index = request.listRequest.index || 1;
        request.listRequest.size = request.listRequest.size || BAZA_CRUD_DEFAULT_SIZE;

        if (!investorAccount) {
            return bazaEmptyCrudListResponse();
        }

        const dividends = await this.dividends.list(
            {
                status: request.listRequest.states as any,
                index: -1,
                size: -1,
            },
            investorAccount.id,
        );

        const investments = await this.ncTransactions.listEntities({
            isSubmitted: true,
            accountId: request.account.id,
            status: request.listRequest.states as any,
            index: -1, // we want all the result
            size: -1,
        });

        const investmentEntities: Array<BazaNcIntegrationPortfolioTransactionInvestmentDto> = investments.items.map((next) => ({
            type: BazaNcIntegrationPortfolioTransactionType.Investment,
            date: next.createdAt,
            entity: {
                id: next.id,
                createdAt: next.createdAt,
                name: next.ncOfferingName,
                state: next.state,
                type: BazaNcIntegrationPortfolioTransactionType.Investment,
                amountCents: next.amountCents,
                transactionFeesCents: next.transactionFeesCents,
                totalCents: next.totalCents,
                shares: next.shares,
                pricePerShareCents: next.pricePerShareCents,
                offeringId: next.ncOfferingId,
            },
        }));

        const dividendsEntities: Array<BazaNcIntegrationPortfolioTransactionDividendDto> = dividends.items.map((next) => ({
            type: BazaNcIntegrationPortfolioTransactionType.Dividends,
            date: next.date,
            entity: next,
        }));

        let items: Array<BazaNcIntegrationPortfolioTransactionDto> = [];
        let totalItemsCount: number;

        items.push(...investmentEntities, ...dividendsEntities);

        if (request.listRequest.scopes) {
            items = items.filter((next) => request.listRequest.scopes.includes(next.type));

            totalItemsCount =
                (request.listRequest.scopes.includes(BazaNcIntegrationPortfolioTransactionType.Investment) ? investments.pager.total : 0) +
                (request.listRequest.scopes.includes(BazaNcIntegrationPortfolioTransactionType.Dividends) ? dividends.pager.total : 0);
        } else {
            totalItemsCount = investments.pager.total + dividends.pager.total;
        }

        if (request.listRequest.reverse) {
            items.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());
        } else {
            items.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
        }

        const offset = Math.max(0, request.listRequest.index - 1) * request.listRequest.size;

        return {
            pager: {
                size: request.listRequest.size,
                index: request.listRequest.index,
                total: totalItemsCount,
            },
            items: paginate(items, offset, request.listRequest.size),
        };
    }
}
