import { BazaNcIntegrationInvestmentsService } from '../../../../../libs/investments/src';
import {
    BazaNcIntegrationPortfolioDocumentsDto,
    BazaNcIntegrationPortfolioStatementsDto,
    BazaNcIntegrationPortfolioStatementsRequest,
    BazaNcIntegrationPortfolioStatementsResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsRepository } from '../../../../../libs/listings/src';
import { AccountEntity, AwsService, BAZA_CRUD_DEFAULT_SIZE } from '@scaliolabs/baza-core-api';
import { paginate } from '@scaliolabs/baza-core-shared';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BazaNcIntegrationPortfolioStatementsService {
    constructor(
        private readonly aws: AwsService,
        private readonly investments: BazaNcIntegrationInvestmentsService,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
    ) {}

    async statements(request: {
        account: AccountEntity;
        listRequest: BazaNcIntegrationPortfolioStatementsRequest;
    }): Promise<BazaNcIntegrationPortfolioStatementsResponse> {
        const allStatements = await this.getAllStatements(request);

        if (request.listRequest.reverse) {
            allStatements.reverse();
        }

        const size = request.listRequest.size || BAZA_CRUD_DEFAULT_SIZE;
        const index = Math.max(1, request.listRequest.index) || 1;
        const offset = Math.max(0, index - 1) * size;

        return {
            pager: {
                size,
                index,
                total: allStatements.length,
            },
            items: paginate(allStatements, offset, size),
        };
    }

    private async getAllStatements(request: {
        account: AccountEntity;
        listRequest: BazaNcIntegrationPortfolioStatementsRequest;
    }): Promise<Array<BazaNcIntegrationPortfolioStatementsDto>> {
        const result: Array<BazaNcIntegrationPortfolioStatementsDto> = [];
        const investments = await this.investments.allInvestmentsOfAccount(request.account);

        for (const listingInvestment of investments) {
            const listing = await this.listingRepository.getById(listingInvestment.listing.id);
            const statements: Array<BazaNcIntegrationPortfolioDocumentsDto> = [];

            for (const statement of listing.statements) {
                statements.push({
                    name: statement.name,
                    description: statement.description,
                    documentUrl: await this.aws.presignedUrl({ s3ObjectId: statement.attachment.s3ObjectId }),
                });
            }

            result.push({
                id: listing.id,
                sid: listing.sid,
                name: listing.title,
                statements,
            });
        }

        return result;
    }
}
