import { BazaNcIntegrationInvestmentsService } from '../../../../investments/src';
import { numberOrDefault, roundedPercent } from '@scaliolabs/baza-core-shared';
import { BazaNcDividendService, BazaNcInvestorAccountService } from '@scaliolabs/baza-nc-api';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationPortfolioTotalStatsDto } from '@scaliolabs/baza-nc-integration-shared';

@Injectable()
export class BazaNcIntegrationPortfolioTotalStatsService {
    constructor(
        private readonly investments: BazaNcIntegrationInvestmentsService,
        private readonly dividends: BazaNcDividendService,
        private readonly investorAccountService: BazaNcInvestorAccountService,
    ) {}

    async stats(request: { account: AccountEntity }): Promise<BazaNcIntegrationPortfolioTotalStatsDto> {
        const investments = await this.investments.allInvestmentsOfAccount(request.account);

        const investorAccount = await this.investorAccountService.findInvestorAccountByUserId(request.account.id);
        const investorAccountId = investorAccount ? investorAccount.id : null;

        const totalDividendCents = (await this.dividends.totalAmount(investorAccountId)).totalAmountCents;

        let totalHoldings = 0;
        let totalHoldingAmountCents = 0;
        let totalHoldingCurrentValueCents = 0;
        let totalReturnValueCents = 0;
        let totalReturnPercent = 0;

        totalReturnValueCents += totalDividendCents;

        for (const investment of investments) {
            // Asset Holding
            const totalPurchaseAmountCents = numberOrDefault(investment.purchasedAmountTotalCents, 0, {
                min: 0,
            });
            totalHoldingAmountCents += totalPurchaseAmountCents;
            totalHoldings += numberOrDefault(investment.purchasedShares, 0, {
                min: 0,
            });

            // Asset Current Value
            const currentValueCents = numberOrDefault(investment.listing.offering.ncCurrentValueCents, 0, {
                min: 0,
            });
            const totalCurrentValueCents = currentValueCents * investment.purchasedShares;
            totalHoldingCurrentValueCents += totalCurrentValueCents;

            // Asset returns
            const returnValueCents = totalCurrentValueCents - totalPurchaseAmountCents;

            totalReturnValueCents += returnValueCents;
        }

        // To avoid divide by 0 case.
        if (totalHoldingAmountCents) {
            totalReturnPercent = roundedPercent((totalReturnValueCents / totalHoldingAmountCents) * 100);
        }

        return {
            totalHoldings,
            totalHoldingAmountCents,
            totalHoldingCurrentValueCents,
            totalReturnValueCents,
            totalReturnPercent,
            totalDividendCents,
        };
    }
}
