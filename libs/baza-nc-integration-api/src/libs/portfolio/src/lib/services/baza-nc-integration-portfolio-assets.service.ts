import { BazaNcIntegrationInvestmentsService } from '../../../../investments/src';
import {
    BazaNcIntegrationPortfolioAssetDto,
    BazaNcIntegrationPortfolioAssetsRequest,
    BazaNcIntegrationPortfolioAssetsResponse,
} from '@scaliolabs/baza-nc-integration-shared';
import { bazaEmptyCrudListResponse, numberOrDefault, roundedPercent } from '@scaliolabs/baza-core-shared';
import { Injectable } from '@nestjs/common';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcDividendRepository, BazaNcInvestorAccountRepository, BazaNcOfferingRepository } from '@scaliolabs/baza-nc-api';

@Injectable()
export class BazaNcIntegrationPortfolioAssetsService {
    constructor(
        private readonly investments: BazaNcIntegrationInvestmentsService,
        private readonly dividendRepository: BazaNcDividendRepository,
        private readonly investmentAccountRepository: BazaNcInvestorAccountRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
    ) {}

    async assets(request: {
        account: AccountEntity;
        listRequest: BazaNcIntegrationPortfolioAssetsRequest;
    }): Promise<BazaNcIntegrationPortfolioAssetsResponse> {
        const investorAccount = await this.investmentAccountRepository.findInvestorAccountByUserId(request.account.id);

        if (!investorAccount) {
            return bazaEmptyCrudListResponse();
        }

        const investments = await this.investments.listInvestmentsOfAccount(
            request.account,
            {
                size: request.listRequest.size,
                index: request.listRequest.index,
            },
            {
                reverse: request.listRequest.reverse,
                aggregate: 'listings-and-states',
            },
        );

        const items: Array<BazaNcIntegrationPortfolioAssetDto> = [];

        for (const investment of investments.items) {
            const dividendsCents = investment.isPrimaryCard
                ? await this.dividendRepository.totalAmount({
                      investorAccount,
                      offering: await this.offeringRepository.getOfferingWithId(investment.listing.offeringId),
                  })
                : 0;

            const currentValueCents = numberOrDefault(investment.listing.offering.ncCurrentValueCents, 0, {
                min: 0,
            });
            const totalCurrentValueCents = currentValueCents * investment.purchasedShares;
            const totalPurchasedAmountCents = investment.purchasedAmountTotalCents;
            const totalReturnValueCents = totalCurrentValueCents - totalPurchasedAmountCents + dividendsCents;
            const totalReturnPercent = roundedPercent((totalReturnValueCents / totalPurchasedAmountCents) * 100);
            const avgPricePerShareCents = Number((totalPurchasedAmountCents / investment.purchasedShares).toFixed(2));

            items.push({
                listing: investment.listing,
                isPrimaryCard: investment.isPrimaryCard,
                totalPurchasedShares: numberOrDefault(investment.purchasedShares, 0, {
                    min: 0,
                }),
                purchasePriceCents: numberOrDefault(investment.listing.pricePerShareCents, 0, {
                    min: 0,
                }),
                currentValueCents: numberOrDefault(currentValueCents, 0),
                currentPricePerShareCents: numberOrDefault(investment.listing.pricePerShareCents, 0, {
                    min: 0,
                }),
                avgPricePerShareCents: numberOrDefault(avgPricePerShareCents, 0, {
                    min: 0,
                }),
                totalPurchasedAmountCents: numberOrDefault(totalPurchasedAmountCents, 0, {
                    min: 0,
                }),
                totalCurrentValueCents: numberOrDefault(totalCurrentValueCents, 0, {
                    min: 0,
                }),
                totalReturnValueCents: numberOrDefault(totalReturnValueCents, 0),
                totalReturnPercent: numberOrDefault(totalReturnPercent, 0),
                dividendsCents: numberOrDefault(dividendsCents, 0, {
                    min: 0,
                }),
                state: investment.lastTransactionState,
                lastFailedTransaction: investment.lastFailedTransaction ?? null,
            });
        }

        return {
            pager: investments.pager,
            items,
        };
    }

    async assetsTotal(request: {
        account: AccountEntity;
        listRequest: BazaNcIntegrationPortfolioAssetsRequest;
    }): Promise<BazaNcIntegrationPortfolioAssetsResponse> {
        const investorAccount = await this.investmentAccountRepository.findInvestorAccountByUserId(request.account.id);

        if (!investorAccount) {
            return bazaEmptyCrudListResponse();
        }

        const investments = await this.investments.listInvestmentsOfAccount(
            request.account,
            {
                size: request.listRequest.size,
                index: request.listRequest.index,
            },
            {
                reverse: request.listRequest.reverse,
                aggregate: 'listings',
            },
        );

        const items: Array<BazaNcIntegrationPortfolioAssetDto> = [];

        for (const investment of investments.items) {
            const dividendsCents = investment.isPrimaryCard
                ? await this.dividendRepository.totalAmount({
                      investorAccount,
                      offering: await this.offeringRepository.getOfferingWithId(investment.listing.offeringId),
                  })
                : 0;

            const currentValueCents = numberOrDefault(investment.listing.offering.ncCurrentValueCents, 0, {
                min: 0,
            });
            const totalCurrentValueCents = currentValueCents * investment.purchasedShares;
            const totalPurchasedAmountCents = investment.purchasedAmountTotalCents;
            const totalReturnValueCents = totalCurrentValueCents - totalPurchasedAmountCents + dividendsCents;
            const totalReturnPercent = roundedPercent((totalReturnValueCents / totalPurchasedAmountCents) * 100);
            const avgPricePerShareCents = Number((totalPurchasedAmountCents / investment.purchasedShares).toFixed(2));

            items.push({
                isPrimaryCard: investment.isPrimaryCard,
                listing: investment.listing,
                totalPurchasedShares: numberOrDefault(investment.purchasedShares, 0, {
                    min: 0,
                }),
                purchasePriceCents: numberOrDefault(investment.listing.pricePerShareCents, 0, {
                    min: 0,
                }),
                currentValueCents: numberOrDefault(currentValueCents, 0),
                currentPricePerShareCents: numberOrDefault(investment.listing.pricePerShareCents, 0, {
                    min: 0,
                }),
                avgPricePerShareCents: numberOrDefault(avgPricePerShareCents, 0, {
                    min: 0,
                }),
                totalPurchasedAmountCents: numberOrDefault(totalPurchasedAmountCents, 0, {
                    min: 0,
                }),
                totalCurrentValueCents: numberOrDefault(totalCurrentValueCents, 0, {
                    min: 0,
                }),
                totalReturnValueCents: numberOrDefault(totalReturnValueCents, 0),
                totalReturnPercent: numberOrDefault(totalReturnPercent, 0),
                dividendsCents: numberOrDefault(dividendsCents, 0, {
                    min: 0,
                }),
                state: investment.lastTransactionState,
                lastFailedTransaction: investment.lastFailedTransaction ?? null,
            });
        }

        return {
            pager: investments.pager,
            items,
        };
    }
}
