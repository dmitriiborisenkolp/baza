import { Injectable } from '@nestjs/common';
import { AccountEntity } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationPortfolioAssetsService } from './baza-nc-integration-portfolio-assets.service';
import { BazaNcIntegrationPortfolioStatementsService } from './baza-nc-integration-portfolio-statements.service';
import { BazaNcIntegrationPortfolioTotalStatsService } from './baza-nc-integration-portfolio-total-stats.service';
import { BazaNcIntegrationPortfolioTransactionsService } from './baza-nc-integration-portfolio-transactions.service';
import {
    BazaNcIntegrationPortfolioAssetsRequest,
    BazaNcIntegrationPortfolioAssetsResponse,
    BazaNcIntegrationPortfolioStatementsRequest,
    BazaNcIntegrationPortfolioStatementsResponse,
    BazaNcIntegrationPortfolioTotalStatsDto,
    BazaNcIntegrationPortfolioTransactionsRequest,
    BazaNcIntegrationPortfolioTransactionsResponse,
} from '@scaliolabs/baza-nc-integration-shared';

/**
 * Baza NC Integration Portfolio Service
 * This is a Facade Service only
 * @see BazaNcIntegrationPortfolioAssetsService
 * @see BazaNcIntegrationPortfolioStatementsService
 * @see BazaNcIntegrationPortfolioTotalStatsService
 * @see BazaNcIntegrationPortfolioTransactionsService
 */
@Injectable()
export class BazaNcIntegrationPortfolioService {
    constructor(
        private readonly assetsService: BazaNcIntegrationPortfolioAssetsService,
        private readonly statementsService: BazaNcIntegrationPortfolioStatementsService,
        private readonly totalStatsService: BazaNcIntegrationPortfolioTotalStatsService,
        private readonly transactionsService: BazaNcIntegrationPortfolioTransactionsService,
    ) {}

    /**
     * Returns aggregated numbers for Total block in Portfolio
     * @param account
     */
    async total(account: AccountEntity): Promise<BazaNcIntegrationPortfolioTotalStatsDto> {
        return this.totalStatsService.stats({
            account,
        });
    }

    /**
     * Returns Assets List (v1)
     * @param account
     * @param listRequest
     */
    async assets(
        account: AccountEntity,
        listRequest: BazaNcIntegrationPortfolioAssetsRequest,
    ): Promise<BazaNcIntegrationPortfolioAssetsResponse> {
        return this.assetsService.assets({
            account,
            listRequest,
        });
    }

    /**
     * Returns Assets List (v2)
     * @param account
     * @param listRequest
     */
    async assetsTotal(
        account: AccountEntity,
        listRequest: BazaNcIntegrationPortfolioAssetsRequest,
    ): Promise<BazaNcIntegrationPortfolioAssetsResponse> {
        return this.assetsService.assetsTotal({
            account,
            listRequest,
        });
    }

    /**
     * DEPRECATED - Returns list of transactions
     * @deprecated
     * @param account
     * @param listRequest
     */
    async transactions(
        account: AccountEntity,
        listRequest: BazaNcIntegrationPortfolioTransactionsRequest,
    ): Promise<BazaNcIntegrationPortfolioTransactionsResponse> {
        return this.transactionsService.transactions({
            account,
            listRequest,
        });
    }

    /**
     * Returns list of Statement Documents of Purchased Offerings
     * @param account
     * @param listRequest
     */
    async statements(
        account: AccountEntity,
        listRequest: BazaNcIntegrationPortfolioStatementsRequest,
    ): Promise<BazaNcIntegrationPortfolioStatementsResponse> {
        return this.statementsService.statements({
            account,
            listRequest,
        });
    }
}
