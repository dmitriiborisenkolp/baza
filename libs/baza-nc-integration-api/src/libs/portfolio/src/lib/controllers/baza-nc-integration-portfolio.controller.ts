import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiExtraModels, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { BazaNcIntegrationPortfolioService } from '../services/baza-nc-integration-portfolio.service';
import { AccountEntity, AuthGuard, ReqAccount } from '@scaliolabs/baza-core-api';
import {
    BazaNcIntegrationPublicOpenApi,
    BazaNcIntegrationPortfolioAssetsRequest,
    BazaNcIntegrationPortfolioAssetsResponse,
    BazaNcIntegrationPortfolioEndpoint,
    PortfolioEndpointPaths,
    BazaNcIntegrationPortfolioStatementsRequest,
    BazaNcIntegrationPortfolioStatementsResponse,
    BazaNcIntegrationPortfolioTotalStatsDto,
    BazaNcIntegrationPortfolioTransactionsRequest,
    BazaNcIntegrationPortfolioTransactionsResponse,
    bazaNcIntegrationPortfolioExportedSwaggerDtos,
} from '@scaliolabs/baza-nc-integration-shared';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNcIntegrationPublicOpenApi.Portfolio)
@ApiExtraModels(...bazaNcIntegrationPortfolioExportedSwaggerDtos)
@UseGuards(AuthGuard)
export class BazaNcIntegrationPortfolioController implements BazaNcIntegrationPortfolioEndpoint {
    constructor(private readonly portfolioService: BazaNcIntegrationPortfolioService) {}

    @Get(PortfolioEndpointPaths.total)
    @ApiOperation({
        summary: 'total',
        description: 'Total stats for Portfolio',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPortfolioTotalStatsDto,
    })
    async total(@ReqAccount() account: AccountEntity): Promise<BazaNcIntegrationPortfolioTotalStatsDto> {
        return this.portfolioService.total(account);
    }

    @Post(PortfolioEndpointPaths.assets)
    @ApiOperation({
        summary: 'assets',
        description: 'Assets list (Aggregated by Listings and Statuses per each Listing)',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPortfolioAssetsResponse,
    })
    async assets(
        @Body() request: BazaNcIntegrationPortfolioAssetsRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationPortfolioAssetsResponse> {
        return this.portfolioService.assets(account, request);
    }

    @Post(PortfolioEndpointPaths.assetsTotal)
    @ApiOperation({
        summary: 'assetsTotal',
        description: 'Assets list (Aggregated by Listings)',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPortfolioAssetsResponse,
    })
    async assetsTotal(
        @Body() request: BazaNcIntegrationPortfolioAssetsRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationPortfolioAssetsResponse> {
        return this.portfolioService.assetsTotal(account, request);
    }

    @Post(PortfolioEndpointPaths.transactions)
    @ApiOperation({
        summary: '[DEPRECATED] transactions',
        description: 'Transactions list',
        deprecated: true,
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPortfolioTransactionsResponse,
    })
    async transactions(
        @Body() request: BazaNcIntegrationPortfolioTransactionsRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationPortfolioTransactionsResponse> {
        return this.portfolioService.transactions(account, request);
    }

    @Post(PortfolioEndpointPaths.statements)
    @ApiOperation({
        summary: 'statements',
        description: 'Statements documents',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPortfolioStatementsResponse,
    })
    async statements(
        @Body() request: BazaNcIntegrationPortfolioStatementsRequest,
        @ReqAccount() account: AccountEntity,
    ): Promise<BazaNcIntegrationPortfolioStatementsResponse> {
        return this.portfolioService.statements(account, request);
    }
}
