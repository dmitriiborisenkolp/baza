import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaCoreE2eFixtures, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcIntegrationListingsNodeAccess,
    BazaNcIntegrationListingTestimonialNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaNcIntegrationListingTestimonialFixtures } from '../baza-nc-integration-listing-testimonial.fixtures';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/testimonial/integration-tests/tests/002-baza-nc-integration-listing-testimonials-public-api.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListing = new BazaNcIntegrationListingsNodeAccess(http);
    const dataAccessListingTestimonials = new BazaNcIntegrationListingTestimonialNodeAccess(http);

    let ULID: string;
    let LISTINGS: Array<BazaNcIntegrationListingsDto>;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcIntegrationListingTestimonialFixtures.BazaNcIntegrationListingTestimonialFixture,
            ],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eUser();
    });

    it('will returns list of listings', async () => {
        const response = await dataAccessListing.list({
            reverse: true,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        LISTINGS = response.items;
    });

    it('will returns list of testimonials for Listing 3', async () => {
        const response = await dataAccessListingTestimonials.list(LISTINGS[2].sid, {});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(3);
        expect(response.items.length).toBe(3);

        expect(response.items[0].name).toBe('Testimonial 1.1');
        expect(response.items[1].name).toBe('Testimonial 1.2');
        expect(response.items[2].name).toBe('Testimonial 1.3');

        ULID = response.items[0].ulid;
    });

    it('will returns another list of testimonials for Listing 2', async () => {
        const response = await dataAccessListingTestimonials.list(LISTINGS[1].sid, {});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(2);
        expect(response.items.length).toBe(2);

        expect(response.items[0].name).toBe('Testimonial 2.1');
        expect(response.items[1].name).toBe('Testimonial 2.2');
    });

    it('will correctly handle page size option for list request', async () => {
        const response = await dataAccessListingTestimonials.list(LISTINGS[2].sid, {
            size: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.pager.total).toBe(3);
        expect(response.pager.size).toBe(2);
        expect(response.pager.index).toBe(1);

        expect(response.items.length).toBe(2);

        expect(response.items[0].name).toBe('Testimonial 1.1');
        expect(response.items[1].name).toBe('Testimonial 1.2');
    });

    it('will returns Testimonial 1.1 by ulid', async () => {
        const response = await dataAccessListingTestimonials.getByUlid(ULID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.ulid).toBe(ULID);
        expect(response.name).toBe('Testimonial 1.1');
    });
});
