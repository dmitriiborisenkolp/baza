import { Injectable } from '@nestjs/common';
import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingTestimonialCmsCreateRequest } from '@scaliolabs/baza-nc-integration-shared';
import { e2eExampleAttachment } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationListingTestimonialCmsService } from '../../services/baza-nc-integration-listing-testimonial-cms.service';
import { BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY } from '../../../../../listings/src';
import { BazaNcIntegrationListingTestimonialFixtures } from '../baza-nc-integration-listing-testimonial.fixtures';

export const BAZA_NC_INTEGRATION_LISTING_TESTIMONIALS_FIXTURE: Array<{
    $id?: number;
    $ulid?: string;
    $refId: number;
    $listingRefId: number;
    request: BazaNcIntegrationListingTestimonialCmsCreateRequest;
}> = [
    {
        $refId: 1,
        $listingRefId: 1,
        request: {
            name: 'Testimonial 1.1',
            role: 'ROLE-A',
            contents: 'Testimonial 1.1 Description',
            image: e2eExampleAttachment(),
            listingUlid: undefined,
            isPublished: true,
        },
    },
    {
        $refId: 2,
        $listingRefId: 1,
        request: {
            name: 'Testimonial 1.2',
            role: 'ROLE-A',
            contents: 'Testimonial 1.2 Description',
            image: e2eExampleAttachment(),
            listingUlid: undefined,
            isPublished: true,
        },
    },
    {
        $refId: 3,
        $listingRefId: 1,
        request: {
            name: 'Testimonial 1.3',
            role: 'ROLE-B',
            contents: 'Testimonial 1.3 Description',
            image: e2eExampleAttachment(),
            listingUlid: undefined,
            isPublished: true,
        },
    },
    {
        $refId: 4,
        $listingRefId: 1,
        request: {
            name: 'Testimonial 1.4',
            role: 'ROLE-A',
            contents: 'Testimonial 1.4 Description',
            image: e2eExampleAttachment(),
            listingUlid: undefined,
            isPublished: false,
        },
    },
    {
        $refId: 5,
        $listingRefId: 2,
        request: {
            name: 'Testimonial 2.1',
            role: 'ROLE-A',
            contents: 'Testimonial 2.1 Description',
            image: e2eExampleAttachment(),
            listingUlid: undefined,
            isPublished: true,
        },
    },
    {
        $refId: 6,
        $listingRefId: 2,
        request: {
            name: 'Testimonial 2.2',
            role: 'ROLE-A',
            contents: 'Testimonial 2.2 Description',
            image: e2eExampleAttachment(),
            listingUlid: undefined,
            isPublished: true,
        },
    },
];

@Injectable()
export class BazaNcIntegrationListingTestimonialsFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcIntegrationListingTestimonialCmsService,
    ) {
    }

    async up(): Promise<void> {
        for (const fixtureRequest of BAZA_NC_INTEGRATION_LISTING_TESTIMONIALS_FIXTURE) {
            const listing = BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY.find((f) => f.$refId === fixtureRequest.$listingRefId);

            const entity = await this.service.create({
                ...fixtureRequest.request,
                listingUlid: listing.$ulid,
            });

            fixtureRequest.$id = entity.id;
            fixtureRequest.$ulid = entity.ulid;
        }
    }
}

defineE2eFixtures<BazaNcIntegrationListingTestimonialFixtures>([{
    fixture: BazaNcIntegrationListingTestimonialFixtures.BazaNcIntegrationListingTestimonialFixture,
    provider: BazaNcIntegrationListingTestimonialsFixture,
}]);
