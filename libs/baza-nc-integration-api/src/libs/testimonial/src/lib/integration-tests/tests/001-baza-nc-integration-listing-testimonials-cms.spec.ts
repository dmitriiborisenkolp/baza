import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcIntegrationListingsCmsNodeAccess,
    BazaNcIntegrationListingTestimonialCmsNodeAccess,
} from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, e2eExampleAttachment, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationListingsCmsDto } from '@scaliolabs/baza-nc-integration-shared';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/testimonial/integration-tests/tests/001-baza-nc-integration-listing-testimonials-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListingsCms = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessListingTestimonialsCMS = new BazaNcIntegrationListingTestimonialCmsNodeAccess(http);

    let ULID: string;
    let LISTINGS: Array<BazaNcIntegrationListingsCmsDto>;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany],
            specFile: __filename,
        });
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will returns list of published listings', async () => {
        const response = await dataAccessListingsCms.list({});

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(3);

        LISTINGS = response.items;
    });

    it('will have no testimonials for Listing 1 and Listing 2', async () => {
        const responseLT1 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[0].sid,
        });

        const responseLT2 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[1].sid,
        });

        expect(isBazaErrorResponse(responseLT1)).toBeFalsy();
        expect(isBazaErrorResponse(responseLT2)).toBeFalsy();

        expect(responseLT1.items.length).toBe(0);
        expect(responseLT2.items.length).toBe(0);
    });

    it('will successfully create a Testimonial for Listing 1', async () => {
        const response = await dataAccessListingTestimonialsCMS.create({
            listingUlid: LISTINGS[0].sid,
            isPublished: true,
            image: e2eExampleAttachment(),
            name: 'Example Testimonial 1.1',
            role: 'ROLE-1.1',
            contents: 'Example Testimonial 1.1 Description',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBeDefined();
        expect(response.ulid).toBeDefined();
        expect(response.isPublished).toBeTruthy();
        expect(response.sortOrder).toBe(1);
        expect(response.dateCreatedAt).toBeDefined();
        expect(response.dateUpdatedAt).not.toBeDefined();
        expect(response.image).toBeDefined();
        expect(response.name).toBe('Example Testimonial 1.1');
        expect(response.role).toBe('ROLE-1.1');
        expect(response.contents).toBe('Example Testimonial 1.1 Description');

        ULID = response.ulid;
    });

    it('will returns created testimonial by ULID', async () => {
        const response = await dataAccessListingTestimonialsCMS.getByUlid({
            ulid: ULID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBeDefined();
        expect(response.ulid).toBeDefined();
        expect(response.isPublished).toBeTruthy();
        expect(response.sortOrder).toBe(1);
        expect(response.dateCreatedAt).toBeDefined();
        expect(response.dateUpdatedAt).not.toBeDefined();
        expect(response.image).toBeDefined();
        expect(response.name).toBe('Example Testimonial 1.1');
        expect(response.role).toBe('ROLE-1.1');
        expect(response.contents).toBe('Example Testimonial 1.1 Description');
    });

    it('[1] will display testimonials lists correctly', async () => {
        const responseLT1 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[0].sid,
        });

        const responseLT2 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[1].sid,
        });

        expect(isBazaErrorResponse(responseLT1)).toBeFalsy();
        expect(isBazaErrorResponse(responseLT2)).toBeFalsy();

        expect(responseLT1.items.length).toBe(1);
        expect(responseLT2.items.length).toBe(0);

        expect(responseLT1.items[0].name).toBe('Example Testimonial 1.1');
    });

    it('will successfully add a more testimonials to Listing 1', async () => {
        const responseT1 = await dataAccessListingTestimonialsCMS.create({
            listingUlid: LISTINGS[0].sid,
            isPublished: true,
            image: e2eExampleAttachment(),
            name: 'Example Testimonial 1.2',
            role: 'ROLE-1.2',
            contents: 'Example Testimonial 1.2 Description',
        });

        const responseT2 = await dataAccessListingTestimonialsCMS.create({
            listingUlid: LISTINGS[0].sid,
            isPublished: true,
            image: e2eExampleAttachment(),
            name: 'Example Testimonial 1.3',
            role: 'ROLE-1.3',
            contents: 'Example Testimonial 1.3 Description',
        });

        expect(isBazaErrorResponse(responseT2)).toBeFalsy();
        expect(isBazaErrorResponse(responseT1)).toBeFalsy();

        expect(responseT1.id).toBeDefined();
        expect(responseT1.ulid).toBeDefined();
        expect(responseT1.isPublished).toBeTruthy();
        expect(responseT1.sortOrder).toBe(2);
        expect(responseT1.dateCreatedAt).toBeDefined();
        expect(responseT1.dateUpdatedAt).not.toBeDefined();
        expect(responseT1.image).toBeDefined();
        expect(responseT1.name).toBe('Example Testimonial 1.2');
        expect(responseT1.role).toBe('ROLE-1.2');
        expect(responseT1.contents).toBe('Example Testimonial 1.2 Description');

        expect(responseT2.id).toBeDefined();
        expect(responseT2.ulid).toBeDefined();
        expect(responseT2.isPublished).toBeTruthy();
        expect(responseT2.sortOrder).toBe(3);
        expect(responseT2.dateCreatedAt).toBeDefined();
        expect(responseT2.dateUpdatedAt).not.toBeDefined();
        expect(responseT2.image).toBeDefined();
        expect(responseT2.name).toBe('Example Testimonial 1.3');
        expect(responseT2.role).toBe('ROLE-1.3');
        expect(responseT2.contents).toBe('Example Testimonial 1.3 Description');
    });

    it('[2] will display testimonials lists correctly', async () => {
        const responseLT1 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[0].sid,
        });

        const responseLT2 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[1].sid,
        });

        expect(isBazaErrorResponse(responseLT1)).toBeFalsy();
        expect(isBazaErrorResponse(responseLT2)).toBeFalsy();

        expect(responseLT1.items.length).toBe(3);
        expect(responseLT2.items.length).toBe(0);

        expect(responseLT1.items[0].name).toBe('Example Testimonial 1.1');
        expect(responseLT1.items[1].name).toBe('Example Testimonial 1.2');
        expect(responseLT1.items[2].name).toBe('Example Testimonial 1.3');
    });

    it('will successfully create testimonial to Listing 2', async () => {
        const responseT1 = await dataAccessListingTestimonialsCMS.create({
            listingUlid: LISTINGS[1].sid,
            isPublished: true,
            image: e2eExampleAttachment(),
            name: 'Example Testimonial 2.1',
            role: 'ROLE-2.1',
            contents: 'Example Testimonial 2.1 Description',
        });

        const responseT2 = await dataAccessListingTestimonialsCMS.create({
            listingUlid: LISTINGS[1].sid,
            isPublished: true,
            image: e2eExampleAttachment(),
            name: 'Example Testimonial 2.2',
            role: 'ROLE-2.2',
            contents: 'Example Testimonial 2.2 Description',
        });

        expect(isBazaErrorResponse(responseT2)).toBeFalsy();
        expect(isBazaErrorResponse(responseT1)).toBeFalsy();

        expect(responseT1.id).toBeDefined();
        expect(responseT1.ulid).toBeDefined();
        expect(responseT1.isPublished).toBeTruthy();
        expect(responseT1.sortOrder).toBe(1);
        expect(responseT1.dateCreatedAt).toBeDefined();
        expect(responseT1.dateUpdatedAt).not.toBeDefined();
        expect(responseT1.image).toBeDefined();
        expect(responseT1.name).toBe('Example Testimonial 2.1');

        expect(responseT2.id).toBeDefined();
        expect(responseT2.ulid).toBeDefined();
        expect(responseT2.isPublished).toBeTruthy();
        expect(responseT2.sortOrder).toBe(2);
        expect(responseT2.dateCreatedAt).toBeDefined();
        expect(responseT2.dateUpdatedAt).not.toBeDefined();
        expect(responseT2.image).toBeDefined();
        expect(responseT2.name).toBe('Example Testimonial 2.2');
    });

    it('[3] will display testimonials lists correctly', async () => {
        const responseLT1 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[0].sid,
        });

        const responseLT2 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[1].sid,
        });

        expect(isBazaErrorResponse(responseLT1)).toBeFalsy();
        expect(isBazaErrorResponse(responseLT2)).toBeFalsy();

        expect(responseLT1.items.length).toBe(3);
        expect(responseLT2.items.length).toBe(2);

        expect(responseLT1.items[0].name).toBe('Example Testimonial 1.1');
        expect(responseLT1.items[0].sortOrder).toBe(1);
        expect(responseLT1.items[1].name).toBe('Example Testimonial 1.2');
        expect(responseLT1.items[1].sortOrder).toBe(2);
        expect(responseLT1.items[2].name).toBe('Example Testimonial 1.3');
        expect(responseLT1.items[2].sortOrder).toBe(3);

        expect(responseLT2.items[0].name).toBe('Example Testimonial 2.1');
        expect(responseLT2.items[0].sortOrder).toBe(1);
        expect(responseLT2.items[1].name).toBe('Example Testimonial 2.2');
        expect(responseLT2.items[1].sortOrder).toBe(2);
    });

    it('will successfully update sort order for Testimoial 1.1', async () => {
        const response = await dataAccessListingTestimonialsCMS.setSortOrder({
            ulid: ULID,
            setSortOrder: 2,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[4] will display testimonials lists correctly', async () => {
        const responseLT1 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[0].sid,
        });

        const responseLT2 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[1].sid,
        });

        expect(isBazaErrorResponse(responseLT1)).toBeFalsy();
        expect(isBazaErrorResponse(responseLT2)).toBeFalsy();

        expect(responseLT1.items.length).toBe(3);
        expect(responseLT2.items.length).toBe(2);

        expect(responseLT1.items[0].name).toBe('Example Testimonial 1.2');
        expect(responseLT1.items[0].sortOrder).toBe(1);
        expect(responseLT1.items[1].name).toBe('Example Testimonial 1.1');
        expect(responseLT1.items[1].sortOrder).toBe(2);
        expect(responseLT1.items[2].name).toBe('Example Testimonial 1.3');
        expect(responseLT1.items[2].sortOrder).toBe(3);

        expect(responseLT2.items[0].name).toBe('Example Testimonial 2.1');
        expect(responseLT2.items[0].sortOrder).toBe(1);
        expect(responseLT2.items[1].name).toBe('Example Testimonial 2.2');
        expect(responseLT2.items[1].sortOrder).toBe(2);
    });

    it('will successfully revert update sort order for Testimoial 1.1', async () => {
        const response = await dataAccessListingTestimonialsCMS.setSortOrder({
            ulid: ULID,
            setSortOrder: 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[5] will display testimonials lists correctly', async () => {
        const responseLT1 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[0].sid,
        });

        const responseLT2 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[1].sid,
        });

        expect(isBazaErrorResponse(responseLT1)).toBeFalsy();
        expect(isBazaErrorResponse(responseLT2)).toBeFalsy();

        expect(responseLT1.items.length).toBe(3);
        expect(responseLT2.items.length).toBe(2);

        expect(responseLT1.items[0].name).toBe('Example Testimonial 1.1');
        expect(responseLT1.items[0].sortOrder).toBe(1);
        expect(responseLT1.items[1].name).toBe('Example Testimonial 1.2');
        expect(responseLT1.items[1].sortOrder).toBe(2);
        expect(responseLT1.items[2].name).toBe('Example Testimonial 1.3');
        expect(responseLT1.items[2].sortOrder).toBe(3);

        expect(responseLT2.items[0].name).toBe('Example Testimonial 2.1');
        expect(responseLT2.items[0].sortOrder).toBe(1);
        expect(responseLT2.items[1].name).toBe('Example Testimonial 2.2');
        expect(responseLT2.items[1].sortOrder).toBe(2);
    });

    it('will successfully update Testimonial 1.1', async () => {
        const response = await dataAccessListingTestimonialsCMS.update({
            ulid: ULID,
            isPublished: true,
            image: e2eExampleAttachment(),
            name: 'Example Testimonial 1.1 *',
            role: 'ROLE-1.1 *',
            contents: 'Example Testimonial 1.1 Description *',
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.id).toBeDefined();
        expect(response.ulid).toBeDefined();
        expect(response.isPublished).toBeTruthy();
        expect(response.sortOrder).toBe(1);
        expect(response.dateCreatedAt).toBeDefined();
        expect(response.dateUpdatedAt).toBeDefined();
        expect(response.image).toBeDefined();
        expect(response.name).toBe('Example Testimonial 1.1 *');
        expect(response.role).toBe('ROLE-1.1 *');
        expect(response.contents).toBe('Example Testimonial 1.1 Description *');
    });

    it('[6] will display testimonials lists correctly', async () => {
        const responseLT1 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[0].sid,
        });

        const responseLT2 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[1].sid,
        });

        expect(isBazaErrorResponse(responseLT1)).toBeFalsy();
        expect(isBazaErrorResponse(responseLT2)).toBeFalsy();

        expect(responseLT1.items.length).toBe(3);
        expect(responseLT2.items.length).toBe(2);

        expect(responseLT1.items[0].name).toBe('Example Testimonial 1.1 *');
        expect(responseLT1.items[0].sortOrder).toBe(1);
        expect(responseLT1.items[1].name).toBe('Example Testimonial 1.2');
        expect(responseLT1.items[1].sortOrder).toBe(2);
        expect(responseLT1.items[2].name).toBe('Example Testimonial 1.3');
        expect(responseLT1.items[2].sortOrder).toBe(3);

        expect(responseLT2.items[0].name).toBe('Example Testimonial 2.1');
        expect(responseLT2.items[0].sortOrder).toBe(1);
        expect(responseLT2.items[1].name).toBe('Example Testimonial 2.2');
        expect(responseLT2.items[1].sortOrder).toBe(2);
    });

    it('will successfully delete Testimonial 1.1', async () => {
        const response = await dataAccessListingTestimonialsCMS.delete({
            ulid: ULID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('[7] will display testimonials lists correctly', async () => {
        const responseLT1 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[0].sid,
        });

        const responseLT2 = await dataAccessListingTestimonialsCMS.list({
            listingUlid: LISTINGS[1].sid,
        });

        expect(isBazaErrorResponse(responseLT1)).toBeFalsy();
        expect(isBazaErrorResponse(responseLT2)).toBeFalsy();

        expect(responseLT1.items.length).toBe(2);
        expect(responseLT2.items.length).toBe(2);

        expect(responseLT1.items[0].name).toBe('Example Testimonial 1.2');
        expect(responseLT1.items[0].sortOrder).toBe(1);
        expect(responseLT1.items[1].name).toBe('Example Testimonial 1.3');
        expect(responseLT1.items[1].sortOrder).toBe(2);

        expect(responseLT2.items[0].name).toBe('Example Testimonial 2.1');
        expect(responseLT2.items[0].sortOrder).toBe(1);
        expect(responseLT2.items[1].name).toBe('Example Testimonial 2.2');
        expect(responseLT2.items[1].sortOrder).toBe(2);
    });
});
