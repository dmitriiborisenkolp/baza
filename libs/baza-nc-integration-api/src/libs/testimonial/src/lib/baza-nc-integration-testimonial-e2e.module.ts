import { forwardRef, Module } from '@nestjs/common';
import { BazaNcIntegrationTestimonialApiModule } from './baza-nc-integration-testimonial-api.module';
import { BazaNcIntegrationListingTestimonialsFixture } from './integration-tests/fixtures/baza-nc-integration-listing-testimonials.fixture';

const E2E_FIXTURES = [
    BazaNcIntegrationListingTestimonialsFixture,
];

@Module({
    imports: [
        forwardRef(() => BazaNcIntegrationTestimonialApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcIntegrationTestimonialE2eModule {}
