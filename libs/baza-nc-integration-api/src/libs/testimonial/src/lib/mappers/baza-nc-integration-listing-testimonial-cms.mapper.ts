import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingTestimonialEntity } from '../entities/baza-nc-integration-listing-testimonial.entity';
import { BazaNcIntegrationListingTestimonialCmsDto } from '@scaliolabs/baza-nc-integration-shared';
import { AttachmentService } from '@scaliolabs/baza-core-api';
import { Application } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaNcIntegrationListingTestimonialCmsMapper {
    constructor(
        private readonly attachmentMapper: AttachmentService,
    ) {
    }

    async entityToDTO(entity: BazaNcIntegrationListingTestimonialEntity): Promise<BazaNcIntegrationListingTestimonialCmsDto> {
        return {
            id: entity.id,
            ulid: entity.ulid,
            isPublished: entity.isPublished,
            sortOrder: entity.sortOrder,
            dateCreatedAt: entity.dateCreatedAt.toISOString(),
            dateUpdatedAt: entity.dateUpdatedAt?.toISOString(),
            image: await this.attachmentMapper.postprocess(entity.image, Application.CMS),
            name: entity.name,
            role: entity.role,
            contents: entity.contents,
        };
    }

    async entitiesToDTOs(entities: Array<BazaNcIntegrationListingTestimonialEntity>): Promise<Array<BazaNcIntegrationListingTestimonialCmsDto>> {
        return Promise.all(
            entities.map((entity) => this.entityToDTO(entity)),
        );
    }
}
