import { Injectable } from '@nestjs/common';
import { AttachmentImageService } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingTestimonialEntity } from '../entities/baza-nc-integration-listing-testimonial.entity';
import { BazaNcIntegrationListingTestimonialDto } from '@scaliolabs/baza-nc-integration-shared';

@Injectable()
export class BazaNcIntegrationListingTestimonialMapper {
    constructor(
        private readonly attachmentImage: AttachmentImageService,
    ) {
    }

    async entityToDTO(entity: BazaNcIntegrationListingTestimonialEntity): Promise<BazaNcIntegrationListingTestimonialDto> {
        return {
            ulid: entity.ulid,
            sortOrder: entity.sortOrder,
            image: await this.attachmentImage.toImageSet(entity.image),
            name: entity.name,
            role: entity.role,
            contents: entity.contents,
        };
    }

    async entitiesToDTOs(entities: Array<BazaNcIntegrationListingTestimonialEntity>): Promise<Array<BazaNcIntegrationListingTestimonialDto>> {
        return Promise.all(
            entities.map((entity) => this.entityToDTO(entity)),
        );
    }
}
