import { Module } from '@nestjs/common';
import { BazaAttachmentModule, BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsApiModule } from '../../../listings/src';
import { ListingTestimonialController } from './controllers/listing-testimonial.controller';
import { ListingTestimonialCmsController } from './controllers/listing-testimonial-cms.controller';
import { BazaNcIntegrationListingTestimonialMapper } from './mappers/baza-nc-integration-listing-testimonial.mapper';
import { BazaNcIntegrationListingTestimonialCmsMapper } from './mappers/baza-nc-integration-listing-testimonial-cms.mapper';
import { BazaNcIntegrationListingTestimonialRepository } from './repositories/baza-nc-integration-listing-testimonial.repository';
import { BazaNcIntegrationListingTestimonialService } from './services/baza-nc-integration-listing-testimonial.service';
import { BazaNcIntegrationListingTestimonialCmsService } from './services/baza-nc-integration-listing-testimonial-cms.service';
import { BazaNcIntegrationListingTestimonialsFixture } from './integration-tests/fixtures/baza-nc-integration-listing-testimonials.fixture';

const E2E_FIXTURES = [
    BazaNcIntegrationListingTestimonialsFixture,
];

@Module({
    imports: [
        BazaCrudApiModule,
        BazaNcIntegrationListingsApiModule,
        BazaAttachmentModule,
    ],
    controllers: [
        ListingTestimonialController,
        ListingTestimonialCmsController,
    ],
    providers: [
        BazaNcIntegrationListingTestimonialMapper,
        BazaNcIntegrationListingTestimonialCmsMapper,
        BazaNcIntegrationListingTestimonialRepository,
        BazaNcIntegrationListingTestimonialService,
        BazaNcIntegrationListingTestimonialCmsService,
        ...E2E_FIXTURES,
    ],
    exports: [
        BazaNcIntegrationListingTestimonialMapper,
        BazaNcIntegrationListingTestimonialCmsMapper,
        BazaNcIntegrationListingTestimonialRepository,
        BazaNcIntegrationListingTestimonialService,
        BazaNcIntegrationListingTestimonialCmsService,
        ...E2E_FIXTURES,
    ],
})
export class BazaNcIntegrationTestimonialApiModule {}
