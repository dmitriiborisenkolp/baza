import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingTestimonialErrorCodes, bazaNcIntegrationListingTestimonialErrorCodesI18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcIntegrationListingTestimonialNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcIntegrationListingTestimonialErrorCodes.BazaNcIntegrationListingTestimonialNotFound,
            bazaNcIntegrationListingTestimonialErrorCodesI18n[BazaNcIntegrationListingTestimonialErrorCodes.BazaNcIntegrationListingTestimonialNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
