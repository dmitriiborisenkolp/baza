import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS, BazaNcIntegrationListingTestimonialEntity } from '../entities/baza-nc-integration-listing-testimonial.entity';
import { BazaNcIntegrationListingTestimonialNotFoundException } from '../exceptions/baza-nc-integration-listing-testimonial-not-found.exception';

@Injectable()
export class BazaNcIntegrationListingTestimonialRepository {
    constructor(
        private readonly connection: Connection,
    ) {
    }

    get repository(): Repository<BazaNcIntegrationListingTestimonialEntity> {
        return this.connection.getRepository(BazaNcIntegrationListingTestimonialEntity);
    }

    async save(entities: Array<BazaNcIntegrationListingTestimonialEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaNcIntegrationListingTestimonialEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<BazaNcIntegrationListingTestimonialEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS,
        });
    }

    async getById(id: number): Promise<BazaNcIntegrationListingTestimonialEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BazaNcIntegrationListingTestimonialNotFoundException();
        }

        return entity;
    }

    async findByUlid(ulid: string): Promise<BazaNcIntegrationListingTestimonialEntity | undefined> {
        return this.repository.findOne({
            where: [{
                ulid,
            }],
            relations: BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS,
        });
    }

    async getByUlid(ulid: string): Promise<BazaNcIntegrationListingTestimonialEntity> {
        const entity = await this.findByUlid(ulid);

        if (! entity) {
            throw new BazaNcIntegrationListingTestimonialNotFoundException();
        }

        return entity;
    }

    async maxOrder(listingId: number): Promise<number> {
        const result: { max: number } = await this.repository
            .createQueryBuilder('e')
            .select('MAX(e.sortOrder)', 'max')
            .where({
                listing: listingId,
            })
            .getRawOne();

        return result.max || 0;
    }

    async findByListing(listingId: number): Promise<Array<BazaNcIntegrationListingTestimonialEntity>> {
        return this.repository.find({
            where: [{
                listing: listingId,
            }],
            order: {
                sortOrder: 'ASC',
            },
            relations: BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS,
        });
    }
}
