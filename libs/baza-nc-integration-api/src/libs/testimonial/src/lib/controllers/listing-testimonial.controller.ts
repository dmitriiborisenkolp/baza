import { Controller, Get, Param, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationListingTestimonialDto,
    BazaNcIntegrationListingTestimonialEndpoint,
    BazaNcIntegrationListingTestimonialEndpointPaths,
    BazaNcIntegrationListingTestimonialListRequest,
    BazaNcIntegrationListingTestimonialListResponse,
    BazaNcIntegrationPublicOpenApi,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingTestimonialMapper } from '../mappers/baza-nc-integration-listing-testimonial.mapper';
import { BazaNcIntegrationListingTestimonialService } from '../services/baza-nc-integration-listing-testimonial.service';

@Controller()
@ApiTags(BazaNcIntegrationPublicOpenApi.ListingTestimonials)
export class ListingTestimonialController implements BazaNcIntegrationListingTestimonialEndpoint {
    constructor(
        private readonly mapper: BazaNcIntegrationListingTestimonialMapper,
        private readonly service: BazaNcIntegrationListingTestimonialService,
    ) {}

    @Get(BazaNcIntegrationListingTestimonialEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns testimonials for specific Listing',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingTestimonialListResponse,
    })
    async list(
        @Param('ulid') ulid: string,
        @Query() request: BazaNcIntegrationListingTestimonialListRequest,
    ): Promise<BazaNcIntegrationListingTestimonialListResponse> {
        return this.service.list(ulid, request);
    }

    @Get(BazaNcIntegrationListingTestimonialEndpointPaths.getByUlid)
    @ApiOperation({
        summary: 'getByUlid',
        description: 'Returns specific Listing Testimonial',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingTestimonialDto,
    })
    async getByUlid(@Param('ulid') ulid: string): Promise<BazaNcIntegrationListingTestimonialDto> {
        const entity = await this.service.getByUlid(ulid);

        return this.mapper.entityToDTO(entity);
    }
}
