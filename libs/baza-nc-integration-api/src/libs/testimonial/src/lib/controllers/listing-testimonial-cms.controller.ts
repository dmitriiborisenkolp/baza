import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationCmsOpenApi,
    BazaNcIntegrationListingTestimonialCmsCreateRequest,
    BazaNcIntegrationListingTestimonialCmsDeleteRequest,
    BazaNcIntegrationListingTestimonialCmsDto,
    BazaNcIntegrationListingTestimonialCmsEndpoint,
    BazaNcIntegrationListingTestimonialCmsEndpointPaths,
    BazaNcIntegrationListingTestimonialCmsGetByUlidRequest,
    BazaNcIntegrationListingTestimonialCmsListRequest,
    BazaNcIntegrationListingTestimonialCmsListResponse,
    BazaNcIntegrationListingTestimonialCmsSetSortOrderRequest,
    BazaNcIntegrationListingTestimonialCmsUpdateRequest,
} from '@scaliolabs/baza-nc-integration-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingTestimonialCmsMapper } from '../mappers/baza-nc-integration-listing-testimonial-cms.mapper';
import { BazaNcIntegrationListingTestimonialCmsService } from '../services/baza-nc-integration-listing-testimonial-cms.service';

@Controller()
@ApiTags(BazaNcIntegrationCmsOpenApi.Subscription)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonials])
export class ListingTestimonialCmsController implements BazaNcIntegrationListingTestimonialCmsEndpoint {
    constructor(
        private readonly mapper: BazaNcIntegrationListingTestimonialCmsMapper,
        private readonly service: BazaNcIntegrationListingTestimonialCmsService,
    ) {}

    @Post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.create)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement])
    @ApiOperation({
        summary: 'create',
        description: 'Create a new Listing Testimonial',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingTestimonialCmsDto,
    })
    async create(@Body() request: BazaNcIntegrationListingTestimonialCmsCreateRequest): Promise<BazaNcIntegrationListingTestimonialCmsDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.update)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement])
    @ApiOperation({
        summary: 'update',
        description: 'Update existing Listing Testimonial',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingTestimonialCmsDto,
    })
    async update(@Body() request: BazaNcIntegrationListingTestimonialCmsUpdateRequest): Promise<BazaNcIntegrationListingTestimonialCmsDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.delete)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement])
    @ApiOperation({
        summary: 'delete',
        description: 'Delete existing Listing Testimonial',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaNcIntegrationListingTestimonialCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns testimonials for specific Listing',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingTestimonialCmsListResponse,
    })
    async list(
        @Body() request: BazaNcIntegrationListingTestimonialCmsListRequest,
    ): Promise<BazaNcIntegrationListingTestimonialCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.getByUlid)
    @ApiOperation({
        summary: 'getByUlid',
        description: 'Returns Listing Testimonial by ULID',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationListingTestimonialCmsDto,
    })
    async getByUlid(
        @Body() request: BazaNcIntegrationListingTestimonialCmsGetByUlidRequest,
    ): Promise<BazaNcIntegrationListingTestimonialCmsDto> {
        const entity = await this.service.getByUlid(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationListingTestimonialCmsEndpointPaths.setSortOrder)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement])
    @ApiOperation({
        summary: 'setSortOrder',
        description: 'Update sort order for Listing Testimonial (relative to other testimonials specific to Listing)',
    })
    @ApiOkResponse()
    async setSortOrder(@Body() request: BazaNcIntegrationListingTestimonialCmsSetSortOrderRequest): Promise<void> {
        await this.service.setSortOrder(request);
    }
}
