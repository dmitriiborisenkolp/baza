import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ulid } from 'ulid';
import { AttachmentDto, CrudSortableEntity, CrudSortableUlidEntity } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';

export const BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS = ['listing'];

@Entity()
export class BazaNcIntegrationListingTestimonialEntity implements CrudSortableEntity, CrudSortableUlidEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true,
    })
    ulid: string = ulid();

    @Column({
        default: false,
    })
    isPublished: boolean;

    @CreateDateColumn()
    dateCreatedAt: Date;

    @Column({
        nullable: true,
    })
    dateUpdatedAt?: Date;

    @JoinColumn()
    @ManyToOne(() => BazaNcIntegrationListingsEntity, {
        cascade: false,
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    listing: BazaNcIntegrationListingsEntity;

    @Column()
    sortOrder: number;

    @Column({
        type: 'jsonb',
    })
    image: AttachmentDto;

    @Column()
    name: string;

    @Column()
    role: string;

    @Column()
    contents: string;
}
