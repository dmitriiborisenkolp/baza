import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationListingTestimonialDto, BazaNcIntegrationListingTestimonialListRequest, BazaNcIntegrationListingTestimonialListResponse } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingTestimonialRepository } from '../repositories/baza-nc-integration-listing-testimonial.repository';
import { CrudService } from '@scaliolabs/baza-core-api';
import { BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS, BazaNcIntegrationListingTestimonialEntity } from '../entities/baza-nc-integration-listing-testimonial.entity';
import { BazaNcIntegrationListingTestimonialMapper } from '../mappers/baza-nc-integration-listing-testimonial.mapper';
import { BazaNcIntegrationListingTestimonialNotFoundException } from '../exceptions/baza-nc-integration-listing-testimonial-not-found.exception';
import { BazaNcIntegrationListingsRepository } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationListingTestimonialService {
    constructor(
        private readonly crud: CrudService,
        private readonly mapper: BazaNcIntegrationListingTestimonialMapper,
        private readonly repository: BazaNcIntegrationListingTestimonialRepository,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
    ) {}

    async list(ulid: string, request: BazaNcIntegrationListingTestimonialListRequest): Promise<BazaNcIntegrationListingTestimonialListResponse> {
        const listing = await this.listingRepository.getByUlid(ulid);

        return this.crud.find<BazaNcIntegrationListingTestimonialEntity, BazaNcIntegrationListingTestimonialDto>({
            request,
            entity: BazaNcIntegrationListingTestimonialEntity,
            findOptions: {
                order: {
                    sortOrder: 'ASC',
                },
                where: [{
                    isPublished: true,
                    listing,
                }],
                relations: BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS,
            },
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
        });
    }

    async getByUlid(ulid: string): Promise<BazaNcIntegrationListingTestimonialEntity> {
        const entity = await this.repository.getByUlid(ulid);

        if (! entity.isPublished || ! entity.listing.isPublished) {
            throw new BazaNcIntegrationListingTestimonialNotFoundException();
        }

        return entity;
    }
}
