import { Injectable } from '@nestjs/common';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingTestimonialCmsMapper } from '../mappers/baza-nc-integration-listing-testimonial-cms.mapper';
import { BazaNcIntegrationListingTestimonialRepository } from '../repositories/baza-nc-integration-listing-testimonial.repository';
import { BazaNcIntegrationListingTestimonialCmsCreateRequest, BazaNcIntegrationListingTestimonialCmsDeleteRequest, BazaNcIntegrationListingTestimonialCmsDto, BazaNcIntegrationListingTestimonialCmsGetByUlidRequest, BazaNcIntegrationListingTestimonialCmsListRequest, BazaNcIntegrationListingTestimonialCmsListResponse, BazaNcIntegrationListingTestimonialCmsSetSortOrderRequest, BazaNcIntegrationListingTestimonialCmsUpdateRequest, BazaNcIntegrationListingTestimonialEntityBody } from '@scaliolabs/baza-nc-integration-shared';
import { BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS, BazaNcIntegrationListingTestimonialEntity } from '../entities/baza-nc-integration-listing-testimonial.entity';
import { BazaNcIntegrationListingsEntity, BazaNcIntegrationListingsRepository } from '../../../../listings/src';
import { ulid } from 'ulid'
import { CrudSetSortOrderByUlidResponseDto } from '@scaliolabs/baza-core-shared';

@Injectable()
export class BazaNcIntegrationListingTestimonialCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly mapper: BazaNcIntegrationListingTestimonialCmsMapper,
        private readonly repository: BazaNcIntegrationListingTestimonialRepository,
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
    ) {}

    async create(request: BazaNcIntegrationListingTestimonialCmsCreateRequest): Promise<BazaNcIntegrationListingTestimonialEntity> {
        const entity = new BazaNcIntegrationListingTestimonialEntity();
        const listing = await this.listingRepository.getByUlid(request.listingUlid);

        await this.populate(entity, request, listing);
        await this.repository.save([entity]);

        return entity;
    }

    async update(request: BazaNcIntegrationListingTestimonialCmsUpdateRequest): Promise<BazaNcIntegrationListingTestimonialEntity> {
        const entity = await this.repository.getByUlid(request.ulid);

        await this.populate(entity, request, entity.listing);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: BazaNcIntegrationListingTestimonialCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getByUlid(request.ulid);
        const listing = entity.listing;

        await this.repository.remove([entity]);

        const entities = await this.repository.findByListing(listing.id);

        await this.crudSort.normalize(entities);
        await this.repository.save(entities);
    }

    async list(request: BazaNcIntegrationListingTestimonialCmsListRequest): Promise<BazaNcIntegrationListingTestimonialCmsListResponse> {
        const listing = await this.listingRepository.getByUlid(request.listingUlid);

        return this.crud.find<BazaNcIntegrationListingTestimonialEntity, BazaNcIntegrationListingTestimonialCmsDto>({
            request,
            entity: BazaNcIntegrationListingTestimonialEntity,
            findOptions: {
                where: [{
                    listing,
                }],
                relations: BAZA_NC_INTEGRATION_LISTING_TESTIMONIAL_ENTITY_RELATIONS,
                order: {
                    sortOrder: 'ASC',
                },
            },
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            withMaxSortOrder: true,
        });
    }

    async getByUlid(request: BazaNcIntegrationListingTestimonialCmsGetByUlidRequest): Promise<BazaNcIntegrationListingTestimonialEntity> {
        return this.repository.getByUlid(request.ulid);
    }

    async setSortOrder(request: BazaNcIntegrationListingTestimonialCmsSetSortOrderRequest): Promise<CrudSetSortOrderByUlidResponseDto<BazaNcIntegrationListingTestimonialEntity>> {
        const entity = await this.repository.getByUlid(request.ulid);
        const entities = await this.repository.findByListing(entity.listing.id);

        const response = await this.crudSort.setSortOrderUlidEntities<BazaNcIntegrationListingTestimonialEntity>({
            ulid: request.ulid,
            setSortOrder: request.setSortOrder,
            entities,
        });

        await this.repository.save(entities);

        return response;
    }

    private async populate(
        target: BazaNcIntegrationListingTestimonialEntity,
        entityBody: BazaNcIntegrationListingTestimonialEntityBody,
        listing: BazaNcIntegrationListingsEntity,
    ): Promise<void> {
        if (! target.id) {
            target.dateCreatedAt = new Date();
            target.sortOrder = await this.repository.maxOrder(listing.id) + 1;
        } else {
            target.dateUpdatedAt = new Date();
        }

        if (! target.ulid) {
            const date = target.dateCreatedAt || target.dateUpdatedAt || new Date();

            target.ulid = ulid(date.getTime());
        }

        target.listing = listing;
        target.isPublished = entityBody.isPublished;
        target.image = entityBody.image;
        target.name = entityBody.name;
        target.role = entityBody.role;
        target.contents = entityBody.contents;
    }
}
