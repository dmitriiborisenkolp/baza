import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationPerkEntity } from '../entities/baza-nc-integration-perk.entity';
import { BazaNcIntegrationPerkDto } from '@scaliolabs/baza-nc-integration-shared';

@Injectable()
export class BazaNcIntegrationPerkMapper {
    entityToDTO(input: BazaNcIntegrationPerkEntity): BazaNcIntegrationPerkDto {
        return {
            id: input.id,
            sortOrder: input.sortOrder,
            title: input.title,
            type: input.type,
            isForEliteInvestors: input.isForEliteInvestors,
            metadata: input.metadata,
        };
    }

    entitiesToDTOs(input: Array<BazaNcIntegrationPerkEntity>): Array<BazaNcIntegrationPerkDto> {
        return input.map((e) => this.entityToDTO(e));
    }
}
