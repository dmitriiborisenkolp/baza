import { forwardRef, Module } from '@nestjs/common';
import { BazaNcIntegrationPerkApiModule } from './baza-nc-integration-perk-api.module';
import { BazaNcIntegrationPerksFixture } from './integration-tests/fixtures/baza-nc-integration-perks.fixture';

const E2E_FIXTURES = [
    BazaNcIntegrationPerksFixture,
];

@Module({
    imports: [
        forwardRef(() => BazaNcIntegrationPerkApiModule),
    ],
    providers: E2E_FIXTURES,
    exports: E2E_FIXTURES,
})
export class BazaNcIntegrationPerkE2eModule {}
