import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationCmsOpenApi,
    BazaNcIntegrationPerkCmsCreateRequest,
    BazaNcIntegrationPerkCmsDeleteRequest,
    BazaNcIntegrationPerkCmsEndpoint,
    BazaNcIntegrationPerkCmsEndpointPaths,
    BazaNcIntegrationPerkCmsGetByIdRequest,
    BazaNcIntegrationPerkCmsListRequest,
    BazaNcIntegrationPerkCmsListResponse,
    BazaNcIntegrationPerkCmsSetSortOrderRequest,
    BazaNcIntegrationPerkCmsUpdateRequest,
    BazaNcIntegrationPerkDto,
} from '@scaliolabs/baza-nc-integration-shared';
import { AclNodes, AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationPerkCmsService } from '../services/baza-nc-integration-perk-cms.service';
import { BazaNcIntegrationPerkMapper } from '../mappers/baza-nc-integration-perk.mapper';

@Controller()
@ApiBearerAuth()
@ApiTags(BazaNcIntegrationCmsOpenApi.Perk)
@UseGuards(AuthGuard, AuthRequireAdminRoleGuard, WithAccessGuard)
@AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationPerk])
export class PerkCmsController implements BazaNcIntegrationPerkCmsEndpoint {
    constructor(private readonly mapper: BazaNcIntegrationPerkMapper, private readonly service: BazaNcIntegrationPerkCmsService) {}

    @Post(BazaNcIntegrationPerkCmsEndpointPaths.create)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement])
    @ApiOperation({
        summary: 'create',
        description: 'create',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPerkDto,
    })
    async create(@Body() request: BazaNcIntegrationPerkCmsCreateRequest): Promise<BazaNcIntegrationPerkDto> {
        const entity = await this.service.create(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationPerkCmsEndpointPaths.update)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement])
    @ApiOperation({
        summary: 'update',
        description: 'Update (Edit) Perk',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPerkDto,
    })
    async update(@Body() request: BazaNcIntegrationPerkCmsUpdateRequest): Promise<BazaNcIntegrationPerkDto> {
        const entity = await this.service.update(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationPerkCmsEndpointPaths.delete)
    @AclNodes([BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement])
    @ApiOperation({
        summary: 'delete',
        description: 'Delete Perk',
    })
    @ApiOkResponse()
    async delete(@Body() request: BazaNcIntegrationPerkCmsDeleteRequest): Promise<void> {
        await this.service.delete(request);
    }

    @Post(BazaNcIntegrationPerkCmsEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns perk by ID',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPerkDto,
    })
    async getById(@Body() request: BazaNcIntegrationPerkCmsGetByIdRequest): Promise<BazaNcIntegrationPerkDto> {
        const entity = await this.service.getById(request);

        return this.mapper.entityToDTO(entity);
    }

    @Post(BazaNcIntegrationPerkCmsEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns list of listings perks',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPerkCmsListResponse,
    })
    async list(@Body() request: BazaNcIntegrationPerkCmsListRequest): Promise<BazaNcIntegrationPerkCmsListResponse> {
        return this.service.list(request);
    }

    @Post(BazaNcIntegrationPerkCmsEndpointPaths.setSortOrder)
    @ApiOperation({
        summary: 'setSortOrder',
        description: 'Set sort order of perk',
    })
    @ApiOkResponse()
    async setSortOrder(@Body() request: BazaNcIntegrationPerkCmsSetSortOrderRequest): Promise<void> {
        await this.service.setSortOrder(request);
    }
}
