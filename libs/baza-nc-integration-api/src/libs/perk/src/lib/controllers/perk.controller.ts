import { Controller, Param, ParseIntPipe, Get } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import {
    BazaNcIntegrationPerkDto,
    BazaNcIntegrationPerkEndpoint,
    BazaNcIntegrationPerkEndpointPaths,
    BazaNcIntegrationPublicOpenApi,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationPerkService } from '../services/baza-nc-integration-perk.service';
import { BazaNcIntegrationPerkMapper } from '../mappers/baza-nc-integration-perk.mapper';

@Controller()
@ApiTags(BazaNcIntegrationPublicOpenApi.Perk)
export class PerkController implements BazaNcIntegrationPerkEndpoint {
    constructor(private readonly mapper: BazaNcIntegrationPerkMapper, private readonly service: BazaNcIntegrationPerkService) {}

    @Get(BazaNcIntegrationPerkEndpointPaths.getById)
    @ApiOperation({
        summary: 'getById',
        description: 'Returns perk by ID',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPerkDto,
    })
    async getById(@Param('id', ParseIntPipe) id: number): Promise<BazaNcIntegrationPerkDto> {
        const entity = await this.service.getById(id);

        return this.mapper.entityToDTO(entity);
    }

    @Get(BazaNcIntegrationPerkEndpointPaths.list)
    @ApiOperation({
        summary: 'list',
        description: 'Returns perks by listing ID',
    })
    @ApiOkResponse({
        type: BazaNcIntegrationPerkDto,
        isArray: true,
    })
    async list(@Param('listingId', ParseIntPipe) listingId: number): Promise<Array<BazaNcIntegrationPerkDto>> {
        const entities = await this.service.list(listingId);

        return this.mapper.entitiesToDTOs(entities);
    }
}
