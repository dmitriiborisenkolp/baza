import { Injectable } from '@nestjs/common';
import { BazaNcIntegrationPerkRepository } from '../repositories/baza-nc-integration-perk.repository';
import { BazaNcIntegrationPerkMapper } from '../mappers/baza-nc-integration-perk.mapper';
import { BazaNcIntegrationPerkEntity } from '../entities/baza-nc-integration-perk.entity';
import { BazaNcIntegrationListingsRepository } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationPerkService {
    constructor(
        private readonly mapper: BazaNcIntegrationPerkMapper,
        private readonly repository: BazaNcIntegrationPerkRepository,
        private readonly listingsRepository: BazaNcIntegrationListingsRepository,
    ) {}

    async getById(id: number): Promise<BazaNcIntegrationPerkEntity> {
        return this.repository.getById(id);
    }

    async list(listingId: number): Promise<Array<BazaNcIntegrationPerkEntity>> {
        const listings = await this.listingsRepository.getById(listingId);

        return this.repository.findByListings(listings);
    }
}
