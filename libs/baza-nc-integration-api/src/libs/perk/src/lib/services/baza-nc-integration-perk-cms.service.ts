import { Injectable } from '@nestjs/common';
import { CrudService, CrudSortService } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationPerkMapper } from '../mappers/baza-nc-integration-perk.mapper';
import { BazaNcIntegrationPerkRepository } from '../repositories/baza-nc-integration-perk.repository';
import { BazaNcIntegrationPerkCmsCreateRequest, BazaNcIntegrationPerkCmsDeleteRequest, BazaNcIntegrationPerkCmsGetByIdRequest, BazaNcIntegrationPerkCmsListRequest, BazaNcIntegrationPerkCmsListResponse, BazaNcIntegrationPerkCmsSetSortOrderRequest, BazaNcIntegrationPerkCmsUpdateRequest, BazaNcIntegrationPerkDto, BazaNcIntegrationPerkEntityBody } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationPerkEntity } from '../entities/baza-nc-integration-perk.entity';
import { BazaNcIntegrationListingsRepository } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationPerkCmsService {
    constructor(
        private readonly crud: CrudService,
        private readonly crudSort: CrudSortService,
        private readonly mapper: BazaNcIntegrationPerkMapper,
        private readonly repository: BazaNcIntegrationPerkRepository,
        private readonly listingsRepository: BazaNcIntegrationListingsRepository,
    ) {}

    async create(request: BazaNcIntegrationPerkCmsCreateRequest): Promise<BazaNcIntegrationPerkEntity> {
        const entity = new BazaNcIntegrationPerkEntity();
        const listings = await this.listingsRepository.findById(request.listingId);
        const existing = await this.repository.findByListings(listings);

        await this.populate(entity, request);

        entity.listings = listings;
        entity.sortOrder = existing.length + 1;

        await this.repository.save([entity]);

        return entity;
    }

    async update(request: BazaNcIntegrationPerkCmsUpdateRequest): Promise<BazaNcIntegrationPerkEntity> {
        const entity = await this.repository.getById(request.id);

        await this.populate(entity, request);
        await this.repository.save([entity]);

        return entity;
    }

    async delete(request: BazaNcIntegrationPerkCmsDeleteRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);

        await this.repository.remove([entity]);
    }

    async getById(request: BazaNcIntegrationPerkCmsGetByIdRequest): Promise<BazaNcIntegrationPerkEntity> {
        return this.repository.getById(request.id);
    }

    async list(request: BazaNcIntegrationPerkCmsListRequest): Promise<BazaNcIntegrationPerkCmsListResponse> {
        return this.crud.find<BazaNcIntegrationPerkEntity, BazaNcIntegrationPerkDto>({
            request,
            entity: BazaNcIntegrationPerkEntity,
            mapper: async (items) => this.mapper.entitiesToDTOs(items),
            withMaxSortOrder: true,
            findOptions: {
                where: [{
                    listings: request.listingId,
                }],
                order: {
                    sortOrder: 'ASC',
                },
            },
        });
    }

    async setSortOrder(request: BazaNcIntegrationPerkCmsSetSortOrderRequest): Promise<void> {
        const entity = await this.repository.getById(request.id);
        const entities = await this.repository.findByListings(entity.listings);

        await this.crudSort.setSortOrder<BazaNcIntegrationPerkEntity>({
            id: entity.id,
            setSortOrder: request.setSortOrder,
            entities,
        });

        await this.repository.save(entities);
    }

    private async populate(target: BazaNcIntegrationPerkEntity, entityBody: BazaNcIntegrationPerkEntityBody): Promise<void> {
        target.title = entityBody.title;
        target.type = entityBody.type;
        target.isForEliteInvestors = entityBody.isForEliteInvestors;
        target.metadata = entityBody.metadata;
    }
}
