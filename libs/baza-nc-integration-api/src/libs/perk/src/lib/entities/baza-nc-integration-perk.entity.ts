import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';
import { CrudSortableEntity } from '@scaliolabs/baza-core-shared';

export const BAZA_NC_INTEGRATION_PERK_RELATIONS = [
    'listings',
];

@Entity()
export class BazaNcIntegrationPerkEntity implements CrudSortableEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    sortOrder: number;

    @ManyToOne(() => BazaNcIntegrationListingsEntity, {
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    })
    listings: BazaNcIntegrationListingsEntity;

    @Column()
    title: string;

    @Column({
        nullable: true,
    })
    type?: string;

    @Column()
    isForEliteInvestors: boolean;

    @Column({
        type: 'json',
    })
    metadata: Record<string, string> = {};
}
