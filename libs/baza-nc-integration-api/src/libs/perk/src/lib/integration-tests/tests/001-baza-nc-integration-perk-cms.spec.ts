import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess, BazaNcIntegrationPerkCmsNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationListingsCmsDto, BazaNcIntegrationPerkErrorCodes } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/perk/integration-tests/tests/001-baza-nc-integration-perk-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessPerks = new BazaNcIntegrationPerkCmsNodeAccess(http);

    let ID: number;
    let LISTINGS: Array<BazaNcIntegrationListingsCmsDto>;
    let SORT_ORDER: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [BazaCoreE2eFixtures.BazaCoreAuthExampleUser, BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        LISTINGS = (
            await dataAccessListings.list({
                index: 1,
                size: 10,
            })
        ).items;
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will successfully create perk 1.1 (with type)', async () => {
        const response = await dataAccessPerks.create({
            title: 'Perk 1.1',
            type: 'tag-a',
            listingId: LISTINGS[0].id,
            isForEliteInvestors: false,
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Perk 1.1');
        expect(response.type).toBe('tag-a');

        ID = response.id;
    });

    it('will returns perk 1.1 by id', async () => {
        const response = await dataAccessPerks.getById({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Perk 1.1');
        expect(response.type).toBe('tag-a');
    });

    it('will not returns unknown perk', async () => {
        const response: BazaError = (await dataAccessPerks.getById({
            id: ID + 1000,
        })) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcIntegrationPerkErrorCodes.BazaNcIntegrationPerkNotFound);
    });

    it('will display perk 1.1 in list for Listings 1', async () => {
        const response = await dataAccessPerks.list({
            size: 10,
            index: 1,
            listingId: LISTINGS[0].id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(1);
        expect(response.items[0].title).toBe('Perk 1.1');
        expect(response.items[0].type).toBe('tag-a');
    });

    it('will not display perk 1.1 in Listings 2', async () => {
        const response = await dataAccessPerks.list({
            size: 10,
            index: 1,
            listingId: LISTINGS[1].id,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.items.length).toBe(0);
    });

    it('will successfully add perk 1.2 without type', async () => {
        const response = await dataAccessPerks.create({
            title: 'Perk 1.2',
            listingId: LISTINGS[0].id,
            isForEliteInvestors: false,
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Perk 1.2');
        expect(response.type).toBeNull();

        ID = response.id;
    });

    it('will successfully add perk 1.3 with type and metadata', async () => {
        const response = await dataAccessPerks.create({
            title: 'Perk 1.3',
            type: 'tag-c',
            listingId: LISTINGS[0].id,
            isForEliteInvestors: false,
            metadata: {
                foo: 'bar',
                baz: 'woof',
            },
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Perk 1.3');
        expect(response.type).toBe('tag-c');
        expect(response.metadata).toEqual({
            foo: 'bar',
            baz: 'woof',
        });

        ID = response.id;
    });

    it('will display correct order of perks for Listing 1', async () => {
        const response = await dataAccessPerks.list({
            size: 10,
            index: 1,
            listingId: LISTINGS[0].id,
        });

        expect(response.items.length).toBe(3);

        expect(response.items[0].title).toBe('Perk 1.1');
        expect(response.items[1].title).toBe('Perk 1.2');
        expect(response.items[2].title).toBe('Perk 1.3');

        ID = response.items[1].id;
    });

    it('will successfully update perk 1.2', async () => {
        const response = await dataAccessPerks.update({
            id: ID,
            title: 'Perk 1.2 *',
            isForEliteInvestors: false,
            metadata: {},
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display updates for Listing 1', async () => {
        const response = await dataAccessPerks.list({
            size: 10,
            index: 1,
            listingId: LISTINGS[0].id,
        });

        expect(response.items.length).toBe(3);

        expect(response.items[0].title).toBe('Perk 1.1');
        expect(response.items[1].title).toBe('Perk 1.2 *');
        expect(response.items[2].title).toBe('Perk 1.3');

        ID = response.items[2].id;
        SORT_ORDER = response.items[2].sortOrder;
    });

    it('will update sort order of Perk 1.3', async () => {
        const response = await dataAccessPerks.setSortOrder({
            id: ID,
            setSortOrder: SORT_ORDER - 1,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will display new perks order for Listing 1', async () => {
        const response = await dataAccessPerks.list({
            size: 10,
            index: 1,
            listingId: LISTINGS[0].id,
        });

        expect(response.items.length).toBe(3);

        expect(response.items[0].title).toBe('Perk 1.1');
        expect(response.items[1].title).toBe('Perk 1.3');
        expect(response.items[2].title).toBe('Perk 1.2 *');
    });

    it('will successfully delete perk 1.2', async () => {
        const response = await dataAccessPerks.delete({
            id: ID,
        });

        expect(isBazaErrorResponse(response)).toBeFalsy();
    });

    it('will not display deleted perk for Listing 1', async () => {
        const response = await dataAccessPerks.list({
            size: 10,
            index: 1,
            listingId: LISTINGS[0].id,
        });

        expect(response.items.length).toBe(2);

        expect(response.items[0].title).toBe('Perk 1.1');
        expect(response.items[1].title).toBe('Perk 1.2 *');
    });
});
