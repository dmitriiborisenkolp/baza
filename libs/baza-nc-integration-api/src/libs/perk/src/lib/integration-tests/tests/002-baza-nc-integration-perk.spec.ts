import 'reflect-metadata';
import { BazaDataAccessNode, BazaE2eFixturesNodeAccess, BazaE2eNodeAccess } from '@scaliolabs/baza-core-node-access';
import { BazaNcIntegrationListingsCmsNodeAccess, BazaNcIntegrationPerkNodeAccess } from '@scaliolabs/baza-nc-integration-node-access';
import { BazaCoreE2eFixtures, BazaError, isBazaErrorResponse } from '@scaliolabs/baza-core-shared';
import {
    BazaNcIntegrationListingsCmsDto,
    BazaNcIntegrationListingsErrorCodes,
    BazaNcIntegrationPerkErrorCodes,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationApiListingsFixtures } from '../../../../../listings/src';
import { BazaNcIntegrationPerksFixtures } from '../baza-nc-integration-perks.fixtures';

jest.setTimeout(240000);

describe('@scaliolabs/baza-nc-integration-api/perk/integration-tests/tests/001-baza-nc-integration-perk-cms.spec.ts', () => {
    const http = new BazaDataAccessNode();

    const dataAccessE2e = new BazaE2eNodeAccess(http);
    const dataAccessE2eFixtures = new BazaE2eFixturesNodeAccess(http);
    const dataAccessListings = new BazaNcIntegrationListingsCmsNodeAccess(http);
    const dataAccessPerks = new BazaNcIntegrationPerkNodeAccess(http);

    let ID: number;
    let LISTINGS: Array<BazaNcIntegrationListingsCmsDto>;
    let SORT_ORDER: number;

    beforeAll(async () => {
        await dataAccessE2e.flush();
        await dataAccessE2eFixtures.up({
            fixtures: [
                BazaCoreE2eFixtures.BazaCoreAuthExampleUser,
                BazaNcIntegrationApiListingsFixtures.BazaNcIntegrationApiListingsMany,
                BazaNcIntegrationPerksFixtures.BazaNcIntegrationPerks,
            ],
            specFile: __filename,
        });

        await http.authE2eAdmin();

        LISTINGS = (
            await dataAccessListings.list({
                index: 1,
                size: 10,
            })
        ).items;
    });

    beforeEach(async () => {
        await http.authE2eAdmin();
    });

    it('will correctly display list of perks for Listing 1', async () => {
        const response = await dataAccessPerks.list(LISTINGS[LISTINGS.length - 1].id);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(3);

        expect(response[0].title).toBe('Perk 1.1');
        expect(response[1].title).toBe('Perk 1.2');
        expect(response[2].title).toBe('Perk 1.3');
    });

    it('will correctly display list of perks for Listing 2', async () => {
        const response = await dataAccessPerks.list(LISTINGS[LISTINGS.length - 2].id);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.length).toBe(2);

        expect(response[0].title).toBe('Perk 2.1');
        expect(response[1].title).toBe('Perk 2.2');

        ID = response[0].id;
    });

    it('will returns perk by id', async () => {
        const response = await dataAccessPerks.getById(ID);

        expect(isBazaErrorResponse(response)).toBeFalsy();

        expect(response.title).toBe('Perk 2.1');
    });

    it('will throw an error for unknown perk', async () => {
        const response: BazaError = (await dataAccessPerks.getById(ID + 1000)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcIntegrationPerkErrorCodes.BazaNcIntegrationPerkNotFound);
    });

    it('will throw an error for unknown listing', async () => {
        const response: BazaError = (await dataAccessPerks.list(LISTINGS[0].id + 1000)) as any;

        expect(isBazaErrorResponse(response)).toBeTruthy();

        expect(response.code).toBe(BazaNcIntegrationListingsErrorCodes.BazaNcIntegrationListingsNotFound);
    });
});
