import { BazaE2eFixture, defineE2eFixtures } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationPerkCmsCreateRequest } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationPerkCmsService } from '../../services/baza-nc-integration-perk-cms.service';
import { Injectable } from '@nestjs/common';
import { BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY } from '../../../../../listings/src';
import { BazaNcIntegrationPerksFixtures } from '../baza-nc-integration-perks.fixtures';

export const BAZA_NC_INTEGRATION_PERKS_FIXTURE: Array<{
    $id?: number;
    $refId: number;
    $listingRefId: number;
    request: BazaNcIntegrationPerkCmsCreateRequest;
}> = [
    {
        $refId: 1,
        $listingRefId: 1,
        request: {
            title: 'Perk 1.1',
            type: 'tag-a',
            isForEliteInvestors: false,
            listingId: -1,
            metadata: {},
        },
    },
    {
        $refId: 2,
        $listingRefId: 1,
        request: {
            title: 'Perk 1.2',
            type: 'tag-a',
            isForEliteInvestors: false,
            listingId: -1,
            metadata: {},
        },
    },
    {
        $refId: 3,
        $listingRefId: 1,
        request: {
            title: 'Perk 1.3',
            type: 'tag-b',
            isForEliteInvestors: true,
            listingId: -1,
            metadata: {},
        },
    },
    {
        $refId: 4,
        $listingRefId: 2,
        request: {
            title: 'Perk 2.1',
            type: 'tag-a',
            isForEliteInvestors: false,
            listingId: -1,
            metadata: {},
        },
    },
    {
        $refId: 5,
        $listingRefId: 2,
        request: {
            title: 'Perk 2.2',
            type: 'tag-c',
            isForEliteInvestors: true,
            listingId: -1,
            metadata: {},
        },
    },
];

@Injectable()
export class BazaNcIntegrationPerksFixture implements BazaE2eFixture {
    constructor(
        private readonly service: BazaNcIntegrationPerkCmsService,
    ) {}

    async up(): Promise<void> {
        for (const fixtureRequest of BAZA_NC_INTEGRATION_PERKS_FIXTURE) {
            const listing = BAZA_NC_INTEGRATION_LISTINGS_FIXTURE_MANY.find((f) => f.$refId === fixtureRequest.$listingRefId);

            const entity = await this.service.create({
                ...fixtureRequest.request,
                listingId: listing.$id,
            });

            fixtureRequest.$id = entity.id;
        }
    }
}

defineE2eFixtures<BazaNcIntegrationPerksFixtures>([{
    fixture: BazaNcIntegrationPerksFixtures.BazaNcIntegrationPerks,
    provider: BazaNcIntegrationPerksFixture,
}]);
