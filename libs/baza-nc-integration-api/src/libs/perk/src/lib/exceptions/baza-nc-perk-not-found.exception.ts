import { BazaAppException } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationPerkErrorCodes, bazaNcIntegrationPerkNotFoundI18n } from '@scaliolabs/baza-nc-integration-shared';
import { HttpStatus } from '@nestjs/common';

export class BazaNcPerkNotFoundException extends BazaAppException {
    constructor() {
        super(
            BazaNcIntegrationPerkErrorCodes.BazaNcIntegrationPerkNotFound,
            bazaNcIntegrationPerkNotFoundI18n[BazaNcIntegrationPerkErrorCodes.BazaNcIntegrationPerkNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
