import { Module } from '@nestjs/common';
import { BazaCrudApiModule } from '@scaliolabs/baza-core-api';
import { BazaNcIntegrationListingsApiModule } from '../../../listings/src';
import { PerkController } from './controllers/perk.controller';
import { PerkCmsController } from './controllers/perk-cms.controller';
import { BazaNcIntegrationPerkMapper } from './mappers/baza-nc-integration-perk.mapper';
import { BazaNcIntegrationPerkRepository } from './repositories/baza-nc-integration-perk.repository';
import { BazaNcIntegrationPerkService } from './services/baza-nc-integration-perk.service';
import { BazaNcIntegrationPerkCmsService } from './services/baza-nc-integration-perk-cms.service';

@Module({
    imports: [
        BazaCrudApiModule,
        BazaNcIntegrationListingsApiModule,
    ],
    controllers: [
        PerkController,
        PerkCmsController,
    ],
    providers: [
        BazaNcIntegrationPerkMapper,
        BazaNcIntegrationPerkRepository,
        BazaNcIntegrationPerkService,
        BazaNcIntegrationPerkCmsService,
    ],
    exports: [
        BazaNcIntegrationPerkMapper,
        BazaNcIntegrationPerkRepository,
        BazaNcIntegrationPerkService,
        BazaNcIntegrationPerkCmsService,
    ],
})
export class BazaNcIntegrationPerkApiModule {}
