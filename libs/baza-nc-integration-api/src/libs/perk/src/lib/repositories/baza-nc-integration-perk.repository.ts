import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { BAZA_NC_INTEGRATION_PERK_RELATIONS, BazaNcIntegrationPerkEntity } from '../entities/baza-nc-integration-perk.entity';
import { BazaNcPerkNotFoundException } from '../exceptions/baza-nc-perk-not-found.exception';
import { BazaNcIntegrationListingsEntity } from '../../../../listings/src';

@Injectable()
export class BazaNcIntegrationPerkRepository {
    constructor(
        private readonly connection: Connection,
    ) {
    }

    get repository(): Repository<BazaNcIntegrationPerkEntity> {
        return this.connection.getRepository(BazaNcIntegrationPerkEntity);
    }

    async save(entities: Array<BazaNcIntegrationPerkEntity>): Promise<void> {
        await this.repository.save(entities);
    }

    async remove(entities: Array<BazaNcIntegrationPerkEntity>): Promise<void> {
        await this.repository.remove(entities);
    }

    async findById(id: number): Promise<BazaNcIntegrationPerkEntity | undefined> {
        return this.repository.findOne({
            where: [{
                id,
            }],
            relations: BAZA_NC_INTEGRATION_PERK_RELATIONS,
        });
    }

    async getById(id: number): Promise<BazaNcIntegrationPerkEntity> {
        const entity = await this.findById(id);

        if (! entity) {
            throw new BazaNcPerkNotFoundException();
        }

        return entity;
    }

    async findByListings(listings: BazaNcIntegrationListingsEntity | number): Promise<Array<BazaNcIntegrationPerkEntity>> {
        return this.repository.find({
            where: [{
                listings,
            }],
            relations: BAZA_NC_INTEGRATION_PERK_RELATIONS,
            order: {
                sortOrder: 'ASC',
            },
        });
    }
}
