import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzModalModule, NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzProgressModule } from 'ng-zorro-antd/progress';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { IMaskModule } from 'angular-imask';
import { BazaPhoneCountryCodesDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BackLinkModule, PhoneCodeModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { DwollaVerificationStateModule } from './data-access';
import { VerificationComponent } from './verification.component';
import { VerificationFileUploadComponent } from './file-upload/file-upload.component';
import { VerificationInfoComponent } from './stepper-info/stepper-info.component';
import { VerificationInvestorComponent } from './stepper-investor/stepper-investor.component';
import { StepperComponent } from './stepper/stepper.component';
import { CheckComponent } from './check/check.component';

@NgModule({
    declarations: [
        VerificationComponent,
        VerificationFileUploadComponent,
        VerificationInfoComponent,
        VerificationInvestorComponent,
        StepperComponent,
        CheckComponent,
    ],
    imports: [
        BackLinkModule,
        BazaPhoneCountryCodesDataAccessModule,
        CommonModule,
        FormsModule,
        IMaskModule,
        NzAlertModule,
        NzButtonModule,
        NzCardModule,
        NzCheckboxModule,
        NzDatePickerModule,
        NzDividerModule,
        NzEmptyModule,
        NzFormModule,
        NzGridModule,
        NzInputModule,
        NzInputNumberModule,
        NzModalModule,
        NzNotificationModule,
        NzPopoverModule,
        NzProgressModule,
        NzRadioModule,
        NzSelectModule,
        NzStepsModule,
        NzTableModule,
        NzTabsModule,
        NzToolTipModule,
        NzUploadModule,
        PhoneCodeModule,
        ReactiveFormsModule,
        RouterModule,
        UtilModule,
        DwollaVerificationStateModule,
    ],
    providers: [NzModalService],
    exports: [
        VerificationComponent,
        VerificationFileUploadComponent,
        VerificationInfoComponent,
        VerificationInvestorComponent,
        StepperComponent,
        CheckComponent,
    ],
})
export class BazaDwollaWebVerificationFlowModule {}
