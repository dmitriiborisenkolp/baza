import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

@UntilDestroy()
@Component({
    selector: 'app-verification-stepper-dwolla',
    templateUrl: './stepper.component.html',
    styleUrls: ['./stepper.component.less'],
})
export class StepperComponent {
    currentTab: number;
    i18nBasePath = 'dwvf.vf';

    constructor(private readonly route: ActivatedRoute, private readonly store: Store, public readonly wts: BazaWebUtilSharedService) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });
    }
}
