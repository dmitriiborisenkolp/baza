import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BazaWebUtilSharedService, LinkConfig } from '@scaliolabs/baza-web-utils';
import { DwollaVFLinksConfig, DwollaVFQueryParamConfig } from './data-access';
import { DwollaVerificationSharedService } from './data-access/services/dwolla-vf-shared.service';

@UntilDestroy()
@Component({
    selector: 'app-verification-dwolla',
    templateUrl: './verification.component.html',
    styleUrls: ['./verification.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerificationComponent implements OnInit {
    @Input()
    linksConfig?: DwollaVFLinksConfig;

    vfQueryParamConfig: DwollaVFQueryParamConfig;
    backLinkConfig: LinkConfig;
    currentTab: number;
    i18nBasePath = 'dwvf.vf';

    constructor(
        private readonly route: ActivatedRoute,
        private readonly dvs: DwollaVerificationSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {
        this.dvs.vfLinkConfiguration = {};
    }

    ngOnInit(): void {
        // this is the input configuration passed to this component
        this.checkInputLinksConfig();

        // check the query params. They can be "redirect" and "buy" parameters
        this.dvs.checkQueryParamsConfig(this.route.snapshot);

        // create back link for Dwolla VF based on query params
        this.checkBackLinkConfig();
    }

    checkInputLinksConfig() {
        const defaultConfig: DwollaVFLinksConfig = {
            buyShares: '/buy-shares',
            verification: '/verification',
            defaultRedirect: '/account',
        };

        this.linksConfig = this.wts.mergeConfig(defaultConfig, this.linksConfig);

        // update link configuration
        this.dvs.vfLinkConfiguration.buyShares = this.linksConfig.buyShares;
        this.dvs.vfLinkConfiguration.verification = this.linksConfig.verification;
        this.dvs.vfLinkConfiguration.defaultRedirect = this.linksConfig.defaultRedirect;
    }

    checkBackLinkConfig() {
        const defaultConfig: LinkConfig = {
            appLink: {
                commands: [this.dvs.vfLinkConfiguration.defaultRedirect],
            },
        };

        const updatedConfig: LinkConfig = {
            appLink: {
                commands: [`${this.dvs.vfLinkConfiguration.redirect}`],
            },
        };

        this.backLinkConfig = this.wts.mergeConfig(defaultConfig, updatedConfig);
    }
}
