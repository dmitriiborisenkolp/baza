import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { mapTo, take, tap } from 'rxjs/operators';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { DwollaUploadDocument } from '../data-access';
import { BazaWebUtilSharedService, catchErrorMessage, Message } from '@scaliolabs/baza-web-utils';

@Component({
    selector: 'app-verification-file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['./file-upload.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerificationFileUploadComponent {
    @Input()
    isVisible: boolean;

    public message$: Observable<Message | undefined>;
    public fileList: NzUploadFile[];
    i18nBasePath = 'dwvf.fileUpload';

    constructor(private readonly store: Store, public readonly wts: BazaWebUtilSharedService) {
        this.isVisible = true;
    }

    // onFileUpload
    public onFileUpload = (file: NzUploadFile): boolean => {
        this.fileList = [file];
        return false;
    };

    // onFormSubmit
    public onFormSubmit(): void {
        this.message$ = this.store.dispatch(new DwollaUploadDocument(this.fileList[0])).pipe(
            take(1),
            tap(() => (this.isVisible = false)),
            mapTo(undefined),
            catchErrorMessage,
        );
    }

    // onModalClose
    public onModalClose(): void {
        this.isVisible = false;
    }
}
