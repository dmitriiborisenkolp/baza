import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { AccountVerificationStep, BazaNcBootstrapDto } from '@scaliolabs/baza-nc-shared';
import {
    BazaLinkUtilSharedService,
    BazaWebUtilSharedService,
    I18nLinkifyConfig,
    PricePipe,
    ValidationsPreset,
    i18nValidationTypesEnum,
} from '@scaliolabs/baza-web-utils';
import { Observable } from 'rxjs';
import { share, skipWhile, take, tap } from 'rxjs/operators';
import {
    DwollaApplyInvestorProfile,
    DwollaVerification,
    DwollaVerificationState,
    InvestorProfile,
    VFInvestorConfig,
    VerificationInvestorForm,
} from '../data-access';
import { DwollaVerificationSharedService } from '../data-access/services/dwolla-vf-shared.service';

@UntilDestroy()
@Component({
    selector: 'app-verification-stepper-investor-dwolla',
    templateUrl: './stepper-investor.component.html',
    styleUrls: ['./stepper-investor.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerificationInvestorComponent implements OnInit {
    @Input()
    config?: VFInvestorConfig;

    @Input()
    initData: BazaNcBootstrapDto;

    verification$: Observable<DwollaVerification>;
    public fileUploading: boolean;
    public verificationInvestorForm: VerificationInvestorForm;

    i18nBasePath = 'dwvf.investor';
    i18nFormPath = `${this.i18nBasePath}.form`;
    i18nFormFieldsPath = `${this.i18nBasePath}.form.fields`;

    loadConsentSection = false;
    public dollarFormat = (value: number) => this.pricePipe.transform(value);
    public dollarParse = (value: string) => value.replace('$', '').replace(',', '');

    constructor(
        private readonly cdr: ChangeDetectorRef,
        private readonly fb: FormBuilder,
        private readonly pricePipe: PricePipe,
        private readonly router: Router,
        private readonly store: Store,
        public readonly bazaFormValidatorService: BazaFormValidatorService,
        public readonly dvs: DwollaVerificationSharedService,
        public readonly uts: BazaLinkUtilSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {
        this.onFormReset();
    }

    public ngOnInit(): void {
        this.checkDefaultConfig();
        this.loadConsentSection = false;

        this.verification$ = this.store.select(DwollaVerificationState.verification).pipe(
            skipWhile((res) => !res),
            share(),
            tap((verification) => {
                this.fileUploading = verification?.currentStep === AccountVerificationStep.RequestDocuments ? true : false;

                if (verification.investorProfile) {
                    this.toggleDwollaConsentCheckbox();

                    this.verificationInvestorForm.patchValue(verification.investorProfile);
                    this.verificationInvestorForm.updateValueAndValidity();
                }

                this.loadConsentSection = true;
                this.cdr.markForCheck();
            }),
        );
    }

    // onFormReset
    public onFormReset(): void {
        this.verificationInvestorForm = this.fb.group({
            isAccreditedInvestor: ['', Validators.required],
            isAssociatedWithFINRA: ['', Validators.required],
        }) as VerificationInvestorForm;
    }

    // onFormSubmit
    public onFormSubmit(investorFormEl: HTMLFormElement): void {
        if (this.bazaFormValidatorService.isFormValid(this.verificationInvestorForm, investorFormEl)) {
            const data: InvestorProfile = {
                ...this.verificationInvestorForm.value,
            };

            this.store
                .dispatch(new DwollaApplyInvestorProfile(data))
                .pipe(take(1))
                .subscribe(() => {
                    this.wts.refreshInitData$.next();

                    if (this.dvs.vfLinkConfiguration.buy) {
                        this.router.navigate([this.dvs.vfLinkConfiguration.buyShares]);
                    } else {
                        this.router.navigate([this.dvs.vfLinkConfiguration.redirect]);
                    }
                });
        }
    }

    public backToPreviousStep() {
        const queryParams = {
            redirect: this.dvs.vfLinkConfiguration.redirect,
        };

        if (this.dvs.vfLinkConfiguration.buy) {
            queryParams['buy'] = this.dvs.vfLinkConfiguration.buy;
        }

        this.router.navigate([this.dvs.vfLinkConfiguration.verification, 'info'], {
            queryParams: queryParams,
        });
    }

    public getErrorMessage(control: FormControl, controlName: string): string {
        const validationsPreset: ValidationsPreset = new Map([
            ['isAccreditedInvestor', [{ key: i18nValidationTypesEnum.required }]],
            ['isAssociatedWithFINRA', [{ key: i18nValidationTypesEnum.required }]],
            ['dwollaConsentProvided', [{ key: i18nValidationTypesEnum.required }]],
        ]);

        return this.wts.geti18nValidationErrorMessages({
            control,
            controlName,
            i18nFormFieldsPath: this.i18nFormFieldsPath,
            i18nGenericValidationsPath: `${this.i18nFormPath}.genericValidators`,
            validationsPreset,
        });
    }

    private toggleDwollaConsentCheckbox() {
        if (this.showConsentCheckbox()) {
            this.addDwollaConsentCheckboxAndValidation();
        }
    }

    private addDwollaConsentCheckboxAndValidation() {
        this.verificationInvestorForm.removeControl('dwollaConsentProvided');
        this.verificationInvestorForm.updateValueAndValidity();

        this.verificationInvestorForm.addControl('dwollaConsentProvided', new FormControl(false, [Validators.requiredTrue]));
    }

    acceptConsent() {
        const consentCheckboxCtrl = this.verificationInvestorForm.get('dwollaConsentProvided');
        consentCheckboxCtrl?.setValue(!consentCheckboxCtrl.value);
    }

    showConsentCheckbox() {
        const isForeign = this.initData?.investorAccount?.status?.isForeign;
        const isAccountVerificationCompleted = this.initData?.investorAccount?.isAccountVerificationCompleted;

        return !isForeign && !isAccountVerificationCompleted;
    }

    checkDefaultConfig() {
        const defaultConfig: VFInvestorConfig = {
            links: {
                termsOfService: {
                    extLink: {
                        link: 'bazaCommon.links.termsOfService',
                        isCMSLink: true,
                    },
                },
                privacyPolicy: {
                    extLink: {
                        link: 'bazaCommon.links.privacyPolicy',
                        isCMSLink: true,
                    },
                },
                dwollaTOS: {
                    extLink: {
                        link: 'https://www.dwolla.com/legal/tos/#legal-content',
                    },
                },
                dwollaPriacyPolicy: {
                    extLink: {
                        link: 'https://www.dwolla.com/legal/privacy',
                    },
                },
            },
        };

        this.config = this.wts.mergeConfig(defaultConfig, this.config);
    }

    public get consentLinksConfig(): Array<I18nLinkifyConfig> {
        return this.wts.getI18nLinksConfig([
            {
                path: `${this.i18nFormFieldsPath}.dwollaConsentProvided.tosLinkConfig`,
                config: this.config?.links?.termsOfService,
            },
            {
                path: `${this.i18nFormFieldsPath}.dwollaConsentProvided.privacyLinkConfig`,
                config: this.config?.links?.privacyPolicy,
            },
            {
                path: `${this.i18nFormFieldsPath}.dwollaConsentProvided.dwTosLinkConfig`,
                config: this.config?.links?.dwollaTOS,
            },
            {
                path: `${this.i18nFormFieldsPath}.dwollaConsentProvided.dwPrivacyLinkConfig`,
                config: this.config?.links?.dwollaPriacyPolicy,
            },
        ]);
    }
}
