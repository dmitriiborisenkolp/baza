import { Component, OnInit } from '@angular/core';
import { Params, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { DwollaVerificationState } from '../data-access';
import { DwollaVerificationSharedService } from '../data-access/services/dwolla-vf-shared.service';

@UntilDestroy()
@Component({
    selector: 'app-dwolla-verification-check',
    templateUrl: './check.component.html',
    styleUrls: ['./check.component.less'],
})
export class CheckComponent implements OnInit {
    verification$ = this.store.select(DwollaVerificationState.verification);

    public hasStarted: boolean;
    public showContainer = false;
    navigationUrl: string;
    i18nBasePath = 'dwvf.check';

    constructor(
        private readonly router: Router,
        private readonly store: Store,
        private readonly dvs: DwollaVerificationSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    ngOnInit(): void {
        this.checkVerificationStatus();
    }

    checkVerificationStatus() {
        this.verification$.pipe(untilDestroyed(this)).subscribe((response) => {
            if (response && response !== null) {
                if (response.currentStep !== 'Completed') {
                    this.showContainer = true;
                }
            }
            const queryParams = {
                redirect: this.dvs.vfLinkConfiguration.redirect,
            };

            if (this.dvs.vfLinkConfiguration.buy) {
                queryParams['buy'] = this.dvs.vfLinkConfiguration.buy;
            }

            if (response && response.currentStep) {
                switch (response.currentStep) {
                    case AccountVerificationStep.Completed:
                        this.router.navigate([this.dvs.vfLinkConfiguration.buyShares]);
                        break;

                    case AccountVerificationStep.PhoneNumber:
                        this.hasStarted = false;
                        this.populateNavigationUrl([this.dvs.vfLinkConfiguration.verification, 'info'], queryParams);
                        break;

                    case AccountVerificationStep.Citizenship:
                    case AccountVerificationStep.DateOfBirth:
                    case AccountVerificationStep.LegalName:
                    case AccountVerificationStep.RequestSSNDocument:
                    case AccountVerificationStep.ResidentialAddress:
                    case AccountVerificationStep.SSN:
                        this.hasStarted = true;
                        this.populateNavigationUrl([this.dvs.vfLinkConfiguration.verification, 'info'], queryParams);
                        break;

                    case AccountVerificationStep.RequestDocuments:
                    case AccountVerificationStep.InvestorProfile:
                        this.hasStarted = true;
                        this.populateNavigationUrl([this.dvs.vfLinkConfiguration.verification, 'investor'], queryParams);
                        break;

                    default:
                        this.hasStarted = false;
                        this.populateNavigationUrl([this.dvs.vfLinkConfiguration.verification, 'info'], queryParams);
                }
            }
        });
    }

    populateNavigationUrl(commands: unknown[], queryParams?: Params) {
        const routerTree = this.router.createUrlTree(commands, {
            queryParams: queryParams,
        });
        this.navigationUrl = this.router.serializeUrl(routerTree);
    }

    continueCheckVerification() {
        this.router.navigateByUrl(this.navigationUrl);
    }
}
