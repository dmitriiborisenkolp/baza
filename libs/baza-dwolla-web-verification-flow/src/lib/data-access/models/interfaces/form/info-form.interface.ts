import { AbstractControl, FormGroup } from '@angular/forms';

// Verification Info form interface
export interface VerificationInfoForm extends FormGroup {
    controls: {
        firstName: AbstractControl;
        lastName: AbstractControl;
        dateOfBirth: AbstractControl;
        hasntSsn: AbstractControl;
        ssn: AbstractControl;
        phone: AbstractControl;
        citizenship: AbstractControl;
        residentialStreetAddress1: AbstractControl;
        residentialStreetAddress2: AbstractControl;
        residentialCity: AbstractControl;
        residentialState: AbstractControl;
        residentialZipCode: AbstractControl;
        residentialCountry: AbstractControl;
    };
}
