import { AbstractControl, FormGroup } from '@angular/forms';

// Verification Investor form interface
export interface VerificationInvestorForm extends FormGroup {
    controls: {
        isAccreditedInvestor: AbstractControl;
        isAssociatedWithFINRA: AbstractControl;
    };
}
