export interface DwollaVFLinksConfig {
    /**
     * Link
     */
    buyShares?: string;
    /**
     * Link
     */
    verification?: string;
    // this parameter is optional. In case user tampers with address bar or there are no query params, library will check if defaultRedirect is avaialble. If yes, pick that value, else pick '/account' route by default
    defaultRedirect?: string;
}

export interface DwollaVFQueryParamConfig {
    redirect?: string;
    buy?: boolean;
}

export type DwollaVFLinkConfiguration = DwollaVFQueryParamConfig & DwollaVFLinksConfig;
