import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * VFInvestorConfig interface for Investor link configuration
 * @see {@link https://baza-docs.test.scaliolabs.com/web-development/customizations/route-configurations/01-route-configuration-guide}
 */
export interface VFInvestorConfig {
    /**
     * Links configuration
     */
    links?: VFInvestorLinksConfig;
}

/**
 * Interface for navigation links in the investor component
 */
export interface VFInvestorLinksConfig {
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    termsOfService?: LinkConfig;
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    privacyPolicy?: LinkConfig;
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    dwollaTOS?: LinkConfig;
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    dwollaPriacyPolicy?: LinkConfig;
}
