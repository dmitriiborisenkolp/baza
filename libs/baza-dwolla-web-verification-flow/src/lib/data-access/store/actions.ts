import { NzUploadFile } from 'ng-zorro-antd/upload';
import { InvestorProfile, PersonalInformation } from '../models';

export class DwollaApplyPersonalInfo {
    static readonly type = '[Verification] DwollaApplyPersonalInfo';

    constructor(public data: PersonalInformation, public files: NzUploadFile[]) {}
}

export class DwollaApplyInvestorProfile {
    static readonly type = '[Verification] DwollaApplyInvestorProfile';

    constructor(public data: InvestorProfile) {}
}

export class DwollaClearVerificationState {
    static readonly type = '[Verification] DwollaClearVerificationState';
}

export class DwollaGetListStates {
    static readonly type = '[Verification] DwollaGetListStates';

    constructor(public country: string) {}
}

export class DwollaLoadFormResources {
    static readonly type = '[Verification] DwollaLoadFormResources';
}

export class DwollaLoadVerification {
    static readonly type = '[Verification] DwollaLoadVerification';
}

export class DwollaUploadDocument {
    static readonly type = '[Verification] DwollaUploadDocument';

    constructor(public file: NzUploadFile) {}
}
