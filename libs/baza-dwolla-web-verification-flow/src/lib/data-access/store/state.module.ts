import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { DwollaVerificationState } from './state';
import { BazaNcDataAccessModule } from '@scaliolabs/baza-nc-data-access';

@NgModule({
    imports: [NgxsModule.forFeature([DwollaVerificationState]), BazaNcDataAccessModule],
})
export class DwollaVerificationStateModule {}
