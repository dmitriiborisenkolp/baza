/* eslint-disable no-useless-escape */
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable, empty } from 'rxjs';
import { concat, tap } from 'rxjs/operators';
import { AccountVerificationDto, ListStatesResponseDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcAccountVerificationDataAccess } from '@scaliolabs/baza-nc-data-access';
import { BazaWebUtilSharedService, EffectsUtil } from '@scaliolabs/baza-web-utils';

import { FormResources, PersonalInformation, DwollaVerification } from '../models';
import {
    DwollaApplyPersonalInfo,
    DwollaApplyInvestorProfile,
    DwollaLoadFormResources,
    DwollaGetListStates,
    DwollaLoadVerification,
    DwollaUploadDocument,
    DwollaClearVerificationState,
} from './actions';

export interface VerificationStateModel {
    formResources: FormResources;
    listStates: ListStatesResponseDto;
    verification: DwollaVerification | null;
}

const initState = {
    name: 'dwollaVerification',
    defaults: {
        formResources: null,
        listStates: null,
        verification: null,
    },
};

const i18nBasePath = 'dwvf.vf.notifications';

@State<VerificationStateModel>(initState)
@Injectable()
export class DwollaVerificationState {
    constructor(
        private readonly dataAccess: BazaNcAccountVerificationDataAccess,
        private readonly datePipe: DatePipe,
        private readonly effectsUtil: EffectsUtil,
        private readonly wts: BazaWebUtilSharedService,
    ) {}

    @Selector()
    static formResources(state: VerificationStateModel): FormResources {
        return state.formResources;
    }

    @Selector()
    static listStates(state: VerificationStateModel): ListStatesResponseDto {
        return state.listStates;
    }

    @Selector()
    static personal(state: VerificationStateModel): PersonalInformation {
        return state.verification?.personalInformation;
    }

    @Selector()
    static verification(state: VerificationStateModel): DwollaVerification {
        return state.verification;
    }

    @Action(DwollaApplyPersonalInfo, { cancelUncompleted: true })
    DwollaApplyPersonalInfo(
        ctx: StateContext<VerificationStateModel>,
        action: DwollaApplyPersonalInfo,
    ): Observable<AccountVerificationDto> {
        const uploadRequest =
            !action.data.hasSsn && action.files && action.files.length > 0
                ? this.dataAccess.uploadPersonalInformationSSNDocument(action.files[0] as unknown as File)
                : empty();

        // transform date into NC date format
        const info = {
            ...action.data,
            dateOfBirth: this.datePipe.transform(action.data.dateOfBirth, 'MM-dd-yyyy'),
        };

        return this.dataAccess.applyPersonalInformation(info).pipe(
            concat(uploadRequest),
            tap((response: DwollaVerification) => {
                return ctx.patchState({ verification: this.parseDateFromNc(response) });
            }),
            this.effectsUtil.tryCatch$(this.wts.getI18nLabel(i18nBasePath, 'apply_personal_info_success'), ''),
        );
    }

    @Action(DwollaApplyInvestorProfile, { cancelUncompleted: true })
    DwollaApplyInvestorProfile(
        ctx: StateContext<VerificationStateModel>,
        action: DwollaApplyInvestorProfile,
    ): Observable<AccountVerificationDto> {
        return this.dataAccess.applyInvestorProfile(action.data).pipe(
            tap((response: DwollaVerification) => {
                return ctx.patchState({ verification: this.parseDateFromNc(response) });
            }),
            this.effectsUtil.tryCatch$(this.wts.getI18nLabel(i18nBasePath, 'apply_investor_profile_success'), ''),
        );
    }

    @Action(DwollaClearVerificationState, { cancelUncompleted: true })
    DwollaClearVerificationState(ctx: StateContext<VerificationStateModel>): void {
        ctx.setState(initState.defaults);
    }

    @Action(DwollaGetListStates, { cancelUncompleted: true })
    DwollaGetListStates(ctx: StateContext<VerificationStateModel>, action: DwollaGetListStates): Observable<ListStatesResponseDto> {
        return this.dataAccess.listStates({ country: action.country }).pipe(
            tap((response: ListStatesResponseDto) => {
                return ctx.patchState({ listStates: response });
            }),
        );
    }

    @Action(DwollaLoadFormResources, { cancelUncompleted: true })
    DwollaLoadFormResources(ctx: StateContext<VerificationStateModel>): Observable<FormResources> {
        return this.dataAccess.formResources().pipe(
            tap((response) => {
                return ctx.patchState({ formResources: response });
            }),
        );
    }

    @Action(DwollaLoadVerification, { cancelUncompleted: true })
    DwollaLoadVerification(ctx: StateContext<VerificationStateModel>): Observable<DwollaVerification> {
        return this.dataAccess.index().pipe(
            tap((response: DwollaVerification) => {
                return ctx.patchState({ verification: this.parseDateFromNc(response) });
            }),
            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_verification_fail')),
        );
    }

    @Action(DwollaUploadDocument, { cancelUncompleted: true })
    DwollaUploadDocument(ctx: StateContext<VerificationStateModel>, action: DwollaUploadDocument): Observable<AccountVerificationDto> {
        return this.dataAccess.uploadPersonalInformationDocument(action.file as unknown as File).pipe(
            tap((response: DwollaVerification) => {
                return ctx.patchState({ verification: this.parseDateFromNc(response) });
            }),
            this.effectsUtil.tryCatch$(
                this.wts.getI18nLabel(i18nBasePath, 'upload_doc_success'),
                this.wts.getI18nLabel(i18nBasePath, 'upload_doc_fail'),
            ),
        );
    }

    // parse Date from NC format
    private parseDateFromNc(verification: DwollaVerification): DwollaVerification {
        const data = Object.assign({}, verification);

        if (data && data.personalInformation && data.personalInformation.dateOfBirth) {
            data.personalInformation = {
                ...data.personalInformation,
                dateOfBirth: data.personalInformation.dateOfBirth.replace(/\-/g, '/'),
            };
        }
        return data;
    }
}
