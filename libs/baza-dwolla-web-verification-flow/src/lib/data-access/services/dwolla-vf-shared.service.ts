import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { isEmpty } from 'lodash';
import { DwollaVFLinkConfiguration } from '../models';
@Injectable({
    providedIn: 'root',
})
export class DwollaVerificationSharedService {
    constructor(private readonly wts: BazaWebUtilSharedService) {}
    public vfLinkConfiguration: DwollaVFLinkConfiguration;

    checkQueryParamsConfig(route: ActivatedRouteSnapshot) {
        const queryParams = route.queryParams;

        let vfQueryParamConfig = {
            redirect: this.vfLinkConfiguration.defaultRedirect,
            buy: false,
        };

        if (!isEmpty(queryParams ?? {})) {
            const routeQueryParamConfig = {
                redirect: queryParams.redirect,
                buy: queryParams.buy ?? false,
            };

            vfQueryParamConfig = this.wts.mergeConfig(vfQueryParamConfig, routeQueryParamConfig);
        }

        this.vfLinkConfiguration.redirect = vfQueryParamConfig.redirect;
        this.vfLinkConfiguration.buy = vfQueryParamConfig.buy;
    }
}
