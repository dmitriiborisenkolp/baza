import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { FormResources } from '../models';
import { DwollaLoadFormResources, DwollaVerificationState } from '../store';

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class FormResourcesResolver implements Resolve<FormResources> {
    constructor(private readonly store: Store) {}

    resolve(): Observable<FormResources> {
        const result$ = this.store.selectOnce(DwollaVerificationState.formResources).pipe(
            untilDestroyed(this),
            switchMap((response) => {
                if (response === null) {
                    return this.store.dispatch(new DwollaLoadFormResources()).pipe(
                        switchMap(() => {
                            return of(this.store.selectSnapshot(DwollaVerificationState.formResources));
                        }),
                    );
                } else {
                    return of(response);
                }
            }),
        );

        return result$.pipe(
            map((response) => {
                return response;
            }),
        );
    }
}
