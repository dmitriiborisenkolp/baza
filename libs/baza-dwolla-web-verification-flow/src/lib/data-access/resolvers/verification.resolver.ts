import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { StorageService } from '@scaliolabs/baza-web-utils';
import { Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';
import { DwollaVerification } from '../models';
import { DwollaLoadVerification, DwollaVerificationState } from '../store';

@UntilDestroy()
@Injectable({ providedIn: 'root' })
export class DwollaVerificationResolver implements Resolve<DwollaVerification> {
    constructor(private readonly store: Store, private readonly storageService: StorageService) {}

    resolve(): Observable<DwollaVerification> {
        const result$ = this.store.select(DwollaVerificationState.verification).pipe(
            untilDestroyed(this),
            take(1),
            switchMap((response) => {
                if (!response) {
                    return this.store.dispatch(new DwollaLoadVerification()).pipe(
                        switchMap(() => {
                            return of(this.store.selectSnapshot(DwollaVerificationState.verification));
                        }),
                    );
                } else {
                    return of(response);
                }
            }),
        );

        return result$.pipe(
            map((response) => {
                return response;
            }),
        );
    }
}
