import { DwollaVFCheckEnI18n } from '../check/i18n/vf-check.i18n';
import { DwollaVFFileUploadEnI18n } from '../file-upload/i18n/vf-file-upload-i18n';
import { DwollaVFInfoEnI18n } from '../stepper-info/i18n/vf-stepper-info.i18n';
import { DwollaVFInvestorEnI18n } from '../stepper-investor/i18n/vf-stepper-investor.i18n';
import { DwollaVFEnI18n } from './vf.18n';

export const bazaDwollaWebVFEnI18n = {
    dwvf: {
        vf: DwollaVFEnI18n,
        check: DwollaVFCheckEnI18n,
        info: DwollaVFInfoEnI18n,
        investor: DwollaVFInvestorEnI18n,
        fileUpload: DwollaVFFileUploadEnI18n,
    },
};
