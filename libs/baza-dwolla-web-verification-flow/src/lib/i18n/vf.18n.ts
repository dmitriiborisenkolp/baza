export const DwollaVFEnI18n = {
    backLink: {
        confirmation: {
            title: 'Are you sure you want to exit?',
            text: 'All completed steps will be saved. You can continue verification again later.',
        },
        linkText: 'Go Back',
    },
    steps: {
        info: 'Personal Information',
        profile: 'Investor Profile',
    },
    notifications: {
        apply_investor_profile_success: 'Investor Profile successfully saved',
        apply_personal_info_success: 'Personal Information successfully saved',
        load_verification_fail: 'There was an error loading the verification information.',
        upload_doc_success: 'Additional Document successfully uploaded.',
        upload_doc_fail: 'There was an error uploading the additional document.',
        verify_upload_doc: 'Please upload a passport to verify your identity.',
    },
};
