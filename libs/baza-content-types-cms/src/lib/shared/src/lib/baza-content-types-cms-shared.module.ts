import { ModuleWithProviders, NgModule } from '@angular/core';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { CommonModule } from '@angular/common';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { BazaCkEditorModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesConfiguration, bazaContentTypesConfigure } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypesCategoryControlComponent } from './form-controls/baza-content-types-category-control/baza-content-types-category-control.component';
import { BazaContentTypesCategoriesControlComponent } from './form-controls/baza-content-types-categories-control/baza-content-types-categories-control.component';
import { BazaContentTypesContentControlComponent } from './form-controls/baza-content-types-content-control/baza-content-types-content-control.component';
import { BazaContentTypesTagsControlComponent } from './form-controls/baza-content-types-tags-control/baza-content-types-tags-control.component';
import { NzSelectModule } from 'ng-zorro-antd/select';

interface ForRootOptions {
    configFactory?: (defaults: BazaContentTypesConfiguration) => Partial<BazaContentTypesConfiguration>;
}

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NzTreeSelectModule,
        NzSpinModule,
        NzIconModule,
        BazaCkEditorModule,
        BazaContentTypesDataAccessModule,
        NzSelectModule,
    ],
    declarations: [
        BazaContentTypesCategoryControlComponent,
        BazaContentTypesCategoriesControlComponent,
        BazaContentTypesContentControlComponent,
        BazaContentTypesTagsControlComponent,
    ],
    exports: [
        BazaContentTypesCategoryControlComponent,
        BazaContentTypesCategoriesControlComponent,
        BazaContentTypesContentControlComponent,
        BazaContentTypesTagsControlComponent,
    ],
})
export class BazaContentTypesCmsSharedModule {
    static forRoot(options: ForRootOptions = {}): ModuleWithProviders<BazaContentTypesCmsSharedModule> {
        if (options && options.configFactory) {
            bazaContentTypesConfigure(options.configFactory);
        }

        return {
            ngModule: BazaContentTypesCmsSharedModule,
        };
    }
}
