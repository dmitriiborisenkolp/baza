import { BazaContentTypesCategoryControlComponent } from './baza-content-types-category-control/baza-content-types-category-control.component';
import { BazaFormControl, registerFormControl } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesContentControlComponent } from './baza-content-types-content-control/baza-content-types-content-control.component';
import { BazaContentTypesCategoriesControlComponent } from './baza-content-types-categories-control/baza-content-types-categories-control.component';
import { BazaContentTypesTagsControlComponent } from './baza-content-types-tags-control/baza-content-types-tags-control.component';

export enum BazaContentTypesFormField {
    BazaContentTypeContent = 'BazaContentTypeContent',
    BazaContentTypeCategory = 'BazaContentTypeCategory',
    BazaContentTypeCategories = 'BazaContentTypeCategories',
    BazaContentTypeTags = 'BazaContentTypeTags',
}

export interface BazaContentTypesFormContentField extends BazaFormControl<BazaContentTypesContentControlComponent, BazaContentTypesFormField> {
    type: BazaContentTypesFormField.BazaContentTypeContent;
    props?: Partial<BazaContentTypesContentControlComponent>;
}

export interface BazaContentTypesFormCategoryField extends BazaFormControl<BazaContentTypesCategoryControlComponent, BazaContentTypesFormField> {
    type: BazaContentTypesFormField.BazaContentTypeCategory;
    props?: Partial<BazaContentTypesCategoryControlComponent>;
}

export interface BazaContentTypesFormCategoriesField extends BazaFormControl<BazaContentTypesCategoriesControlComponent, BazaContentTypesFormField> {
    type: BazaContentTypesFormField.BazaContentTypeCategories;
    props?: Partial<BazaContentTypesCategoriesControlComponent>;
}

export interface BazaContentTypesFormTagsField extends BazaFormControl<BazaContentTypesTagsControlComponent, BazaContentTypesFormField> {
    type: BazaContentTypesFormField.BazaContentTypeTags;
    props?: Partial<BazaContentTypesTagsControlComponent>;
}

export type BazaContentTypesFormFields =
    BazaContentTypesFormCategoryField |
    BazaContentTypesFormContentField |
    BazaContentTypesFormCategoriesField |
    BazaContentTypesFormTagsField
;

registerFormControl<BazaContentTypesFormField>(BazaContentTypesFormField.BazaContentTypeContent, {
    component: BazaContentTypesContentControlComponent,
    enableNzFormContainer: true,
});

registerFormControl<BazaContentTypesFormField>(BazaContentTypesFormField.BazaContentTypeCategory, {
    component: BazaContentTypesCategoryControlComponent,
    enableNzFormContainer: true,
});

registerFormControl<BazaContentTypesFormField>(BazaContentTypesFormField.BazaContentTypeCategories, {
    component: BazaContentTypesCategoriesControlComponent,
    enableNzFormContainer: true,
});

registerFormControl<BazaContentTypesFormField>(BazaContentTypesFormField.BazaContentTypeTags, {
    component: BazaContentTypesTagsControlComponent,
    enableNzFormContainer: true,
});
