import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { forwardRef } from '@nestjs/common/utils/forward-ref.util';
import { Subject } from 'rxjs';
import { TagsCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { BazaLoadingService, genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { distinctUntilChanged, finalize, retryWhen, takeUntil } from 'rxjs/operators';
import { NzSelectOptionInterface } from 'ng-zorro-antd/select/select.types';

interface FormValue {
    values: Array<number>;
}

interface State {
    ready: boolean;
    form: FormGroup;
    nzOptions: Array<NzSelectOptionInterface>;
}

@Component({
    selector: 'baza-content-types-tags-control',
    templateUrl: './baza-content-types-tags-control.component.html',
    styleUrls: ['./baza-content-types-tags-control.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaContentTypesTagsControlComponent),
            multi: true,
        },
    ],
})
export class BazaContentTypesTagsControlComponent implements ControlValueAccessor, OnInit, OnDestroy {
    @Input() excludeTagIds: Array<number> = [];

    private readonly ngOnDestroy$: Subject<void> = new Subject();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    public state: State = {
        ready: false,
        form: this.fb.group({
            values: [],
        }),
        nzOptions: [],
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly loading: BazaLoadingService,
        private readonly dataAccess: TagsCmsDataAccess,
    ) {}

    ngOnInit(): void {
        const loadTags = () => {
            const loading = this.loading.addLoading();

            this.dataAccess
                .getAll()
                .pipe(
                    retryWhen(genericRetryStrategy()),
                    finalize(() => loading.complete()),
                    takeUntil(this.ngOnDestroy$),
                )
                .subscribe((response) => {
                    const nzOptions: Array<NzSelectOptionInterface> = response.map((tag) => ({
                        label: tag.title,
                        value: tag.id,
                    }));

                    this.state = {
                        ...this.state,
                        ready: true,
                        nzOptions,
                    };

                    this.cdr.markForCheck();
                });
        };

        const updateValues = () => {
            this.state.form
                .get('values')
                .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$))
                .subscribe((next: Array<string>) => {
                    if (!this.onChange) {
                        return;
                    }

                    if (Array.isArray(next) && next.length > 0) {
                        this.onChange(next.map((id) => parseInt(id, 10)));
                    } else {
                        this.onChange([]);
                    }
                });
        };

        loadTags();
        updateValues();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    writeValue(tagIds: Array<number>): void {
        if (Array.isArray(tagIds) && tagIds.length > 0) {
            this.state.form.patchValue({
                values: tagIds,
            } as FormValue);
        } else {
            this.state.form.reset();
        }
    }

    registerOnChange(fn: () => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
