import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { forwardRef } from '@nestjs/common/utils/forward-ref.util';
import { Subject } from 'rxjs';
import { NzTreeNode, NzTreeNodeOptions } from 'ng-zorro-antd/core/tree';
import { CategoriesCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { BazaLoadingService, genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { distinctUntilChanged, finalize, retryWhen, takeUntil } from 'rxjs/operators';
import { CategoriesDto } from '@scaliolabs/baza-content-types-shared';

interface FormValue {
    values: Array<string>;
}

interface State {
    ready: boolean;
    form: FormGroup;
    expandKeys: Array<string>;
    nodes: Array<NzTreeNode | NzTreeNodeOptions>;
}

@Component({
    selector: 'baza-content-types-categories-control',
    templateUrl: './baza-content-types-categories-control.component.html',
    styleUrls: ['./baza-content-types-categories-control.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaContentTypesCategoriesControlComponent),
            multi: true,
        },
    ],
})
export class BazaContentTypesCategoriesControlComponent implements ControlValueAccessor, OnInit, OnDestroy {
    @Input() excludeCategoryIds: Array<number> = [];

    private readonly ngOnDestroy$: Subject<void> = new Subject();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    public state: State = {
        ready: false,
        form: this.fb.group({
            values: [],
        }),
        expandKeys: [],
        nodes: [],
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly loading: BazaLoadingService,
        private readonly dataAccess: CategoriesCmsDataAccess,
    ) {}

    ngOnInit(): void {
        const loadCategories = () => {
            const loading = this.loading.addLoading();

            this.dataAccess
                .getAll()
                .pipe(
                    retryWhen(genericRetryStrategy()),
                    finalize(() => loading.complete()),
                    takeUntil(this.ngOnDestroy$),
                )
                .subscribe((response) => {
                    const nodes: Array<NzTreeNode | NzTreeNodeOptions> = [];

                    const deep = (node: CategoriesDto, parentNzNode?: NzTreeNodeOptions) => {
                        if (node.category) {
                            const isNotExcluded =
                                !Array.isArray(this.excludeCategoryIds) ||
                                this.excludeCategoryIds.length === 0 ||
                                !this.excludeCategoryIds.includes(node.category.id);

                            if (isNotExcluded) {
                                const newNzNode: NzTreeNodeOptions = {
                                    title: node.category.title,
                                    key: node.category.id.toString(),
                                    children: [],
                                };

                                if (node.children) {
                                    for (const child of node.children) {
                                        deep(child, newNzNode);
                                    }
                                }

                                if (parentNzNode) {
                                    parentNzNode.children.push(newNzNode);
                                } else {
                                    nodes.push(newNzNode);
                                }
                            }
                        } else if (node.children) {
                            if (node.children) {
                                for (const child of node.children) {
                                    deep(child);
                                }
                            }
                        }
                    };

                    deep(response);

                    this.state = {
                        ...this.state,
                        ready: true,
                        nodes,
                    };

                    this.cdr.markForCheck();
                });
        };

        const updateValueOnNzTreeSelectChange = () => {
            this.state.form
                .get('values')
                .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$))
                .subscribe((next: Array<string>) => {
                    if (!this.onChange) {
                        return;
                    }

                    if (Array.isArray(next) && next.length > 0) {
                        this.onChange(next.map((id) => parseInt(id, 10)));
                    } else {
                        this.onChange([]);
                    }
                });
        };

        loadCategories();
        updateValueOnNzTreeSelectChange();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    writeValue(parentIds: Array<number>): void {
        if (Array.isArray(parentIds) && parentIds.length > 0) {
            this.state.form.patchValue({
                values: parentIds.map((id) => id.toString()),
            } as FormValue);
        } else {
            this.state.form.reset();
        }
    }

    registerOnChange(fn: () => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
