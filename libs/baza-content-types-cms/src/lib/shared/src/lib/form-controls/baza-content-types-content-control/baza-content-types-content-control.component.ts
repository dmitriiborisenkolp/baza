import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { BazaContentBlock, BazaContentBlockPairHtml, BazaContentDto, emptyBazaContentDto } from '@scaliolabs/baza-content-types-shared';
import { takeUntil } from 'rxjs/operators';

// TODO: Finish Content editor. It's HTML single block only now

interface State {
    form: FormGroup;
    content: BazaContentDto;
}

interface FormValue {
    value: string;
}

@Component({
    selector: 'baza-content-types-content-control',
    templateUrl: './baza-content-types-content-control.component.html',
    styleUrls: ['./baza-content-types-content-control.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesContentControlComponent implements ControlValueAccessor, OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    public state: State = {
        form: this.fb.group({
            value: [],
        }),
        content: (() => {
            const dto = emptyBazaContentDto();

            dto.blocks.push({
                type: BazaContentBlock.HTML,
                payload: {
                    contents: '',
                },
            });

            return dto;
        })(),
    };

    constructor(private readonly fb: FormBuilder) {}

    ngOnInit(): void {
        const updateValueOnHtmlEditorChanges = () => {
            this.state.form
                .get('value')
                .valueChanges.pipe(takeUntil(this.ngOnDestroy$))
                .subscribe((next) => {
                    if (this.onChange) {
                        if (!next) {
                            this.onChange(emptyBazaContentDto());
                        } else {
                            const htmlBlock: BazaContentBlockPairHtml = this.state.content.blocks.find(
                                (b) => b.type === BazaContentBlock.HTML,
                            ) as BazaContentBlockPairHtml;

                            htmlBlock.payload.contents = next;

                            this.onChange(this.state.content);
                        }
                    }
                });
        };

        updateValueOnHtmlEditorChanges();
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    writeValue(contents: BazaContentDto): void {
        if (contents && contents.blocks.length > 0) {
            const htmlBlock = contents.blocks.find((b) => b.type === BazaContentBlock.HTML);

            if (htmlBlock) {
                this.state.form.patchValue({
                    value: (htmlBlock as BazaContentBlockPairHtml).payload.contents,
                } as FormValue);
            } else {
                this.state.form.reset();
            }
        } else {
            this.state.form.reset();
        }
    }

    registerOnChange(fn: () => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
