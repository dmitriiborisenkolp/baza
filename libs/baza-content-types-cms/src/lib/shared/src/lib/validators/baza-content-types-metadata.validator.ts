import { AbstractControl, ValidatorFn } from "@angular/forms";
import { isNotNullOrUndefined } from "@scaliolabs/baza-core-shared";

export const bazaContentTypesMetadataValidator: ValidatorFn = (control: AbstractControl) => {
    const input: Record<string, string> = control.value;

    if(! isNotNullOrUndefined(input) || ! Object.keys(input).length) {
        return {
            required: true
        };
    }

    return null;
}