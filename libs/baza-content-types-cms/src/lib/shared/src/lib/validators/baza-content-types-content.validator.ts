import { AbstractControl, ValidatorFn } from '@angular/forms';
import { isNotNullOrUndefined } from '@scaliolabs/baza-core-shared';
import { BazaContentDto } from '@scaliolabs/baza-content-types-shared';

export const bazaContentTypesContentValidator: ValidatorFn = (control: AbstractControl) => {
    const input: BazaContentDto = control.value;
    
    if (! isNotNullOrUndefined(input) || ! input?.blocks?.length) {
        return {
            required: true,
        };
    }
    
    return null;
};
