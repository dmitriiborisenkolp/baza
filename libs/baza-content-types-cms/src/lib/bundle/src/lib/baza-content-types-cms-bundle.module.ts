import { NgModule } from "@angular/core";
import { BazaContentTypesCmsBundleRoutingModule } from './baza-content-types-cms-bundle-routing.module';

@NgModule({
    imports: [
        BazaContentTypesCmsBundleRoutingModule,
    ],
})
export class BazaContentTypesCmsBundleModule {}
