import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { BazaContentTypesCategoriesCmsModule } from "../../../libs/categories/src";
import { BazaContentTypesContactsCmsModule } from "../../../libs/contacts/src";
import { BazaContentTypesFaqCmsModule } from '../../../libs/faq/src';
import { BazaContentTypesNewslettersCmsModule } from '../../../libs/newsletters/src';
import { BazaContentTypesTeamsCmsModule } from '../../../libs/teams/src';
import { BazaContentTypesTestimonialsCmsModule } from '../../../libs/testimonials/src';
import { BazaContentTypesTagsCmsModule } from '../../../libs/tags/src';
import { BazaContentTypesNewsCmsModule } from '../../../libs/news/src';
import { BazaContentTypesPagesCmsModule } from '../../../libs/pages/src';
import { BazaContentTypesBlogCmsModule } from '../../../libs/blog/src';
import { BazaContentTypesJobsCmsModule } from '../../../libs/jobs/src';
import { BazaContentTypesEventsCmsModule } from '../../../libs/events/src';
import { BazaContentTypesTimelineCmsModule } from '../../../libs/timeline/src';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'categories',
                loadChildren: () => BazaContentTypesCategoriesCmsModule,
            },
            {
                path: 'contacts',
                loadChildren: () => BazaContentTypesContactsCmsModule,
            },
            {
                path: 'faq',
                loadChildren: () => BazaContentTypesFaqCmsModule,
            },
            {
                path: 'newsletters',
                loadChildren: () => BazaContentTypesNewslettersCmsModule,
            },
            {
                path: 'teams',
                loadChildren: () => BazaContentTypesTeamsCmsModule,
            },
            {
                path: 'testimonials',
                loadChildren: () => BazaContentTypesTestimonialsCmsModule,
            },
            {
                path: 'tags',
                loadChildren: () => BazaContentTypesTagsCmsModule,
            },
            {
                path: 'news',
                loadChildren: () => BazaContentTypesNewsCmsModule,
            },
            {
                path: 'pages',
                loadChildren: () => BazaContentTypesPagesCmsModule,
            },
            {
                path: 'blog',
                loadChildren: () => BazaContentTypesBlogCmsModule,
            },
            {
                path: 'jobs',
                loadChildren: () => BazaContentTypesJobsCmsModule,
            },
            {
                path: 'event',
                loadChildren: () => BazaContentTypesEventsCmsModule,
            },
            {
                path: 'timeline',
                loadChildren: () => BazaContentTypesTimelineCmsModule,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesCmsBundleRoutingModule {}
