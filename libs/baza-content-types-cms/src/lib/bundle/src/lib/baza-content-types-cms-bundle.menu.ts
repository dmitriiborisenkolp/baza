import { CmsSidePanelMenuGroupConfig } from '@scaliolabs/baza-core-cms';
import { BazaContentTypeAcl, BazaContentTypes, isBazaContentTypeEnabled } from '@scaliolabs/baza-content-types-shared';

export function bazaContentTypesCmsMenu(): Array<CmsSidePanelMenuGroupConfig> {
    const cmsMenu: Array<CmsSidePanelMenuGroupConfig> = [
        {
            title: 'bazaContentTypes.menu.contentTypes',
            items: [
                {
                    title: BazaContentTypes.Faq,
                    translate: true,
                    icon: 'question',
                    routerLink: ['/content-types/faq'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesFAQ],
                },
                {
                    title: BazaContentTypes.Blogs,
                    translate: true,
                    icon: 'edit',
                    routerLink: ['/content-types/blog'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesBlog],
                },
                {
                    title: BazaContentTypes.Events,
                    translate: true,
                    icon: 'calendar',
                    routerLink: ['/content-types/event'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesEvents],
                },
                {
                    title: BazaContentTypes.Jobs,
                    translate: true,
                    icon: 'idcard',
                    routerLink: ['/content-types/jobs'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesJobs],
                },
                {
                    title: BazaContentTypes.Teams,
                    translate: true,
                    icon: 'team',
                    routerLink: ['/content-types/teams'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesTeams],
                },
                {
                    title: BazaContentTypes.Testimonials,
                    translate: true,
                    icon: 'comment',
                    routerLink: ['/content-types/testimonials'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesTestimonials],
                },
                {
                    title: BazaContentTypes.News,
                    translate: true,
                    icon: 'file-text',
                    routerLink: ['/content-types/news'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesNews],
                },
                {
                    title: BazaContentTypes.Pages,
                    translate: true,
                    icon: 'file',
                    routerLink: ['/content-types/pages'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesPages],
                },
                {
                    title: BazaContentTypes.Timeline,
                    translate: true,
                    icon: 'file',
                    routerLink: ['/content-types/timeline'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesTimeline],
                },
            ],
        },
        {
            title: 'bazaContentTypes.menu.taxonomy',
            items: [
                {
                    title: BazaContentTypes.Tags,
                    translate: true,
                    icon: 'tag',
                    routerLink: ['/content-types/tags'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesTags],
                },
                {
                    title: BazaContentTypes.Categories,
                    translate: true,
                    icon: 'tag',
                    routerLink: ['/content-types/categories'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesCategories],
                },
            ],
        },
        {
            title: 'bazaContentTypes.menu.formData',
            items: [
                {
                    title: BazaContentTypes.Contacts,
                    translate: true,
                    icon: 'mail',
                    routerLink: ['/content-types/contacts'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesContacts],
                },
                {
                    title: BazaContentTypes.Newsletters,
                    translate: true,
                    icon: 'mail',
                    routerLink: ['/content-types/newsletters'],
                    requiresACL: [BazaContentTypeAcl.BazaContentTypesNewsletters],
                },
            ],
        }
    ];

    for (const group of cmsMenu) {
        group.items = group.items
            .filter((item) => isBazaContentTypeEnabled(item.title as BazaContentTypes))
            .map((item) => ({
                ...item,
                title: `bazaContentTypes.${item.title.toLowerCase()}.title`,
            }));
    }

    return cmsMenu
        .filter((group) => group.items.length > 0);
}
