import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesFaqCmsComponent } from './components/baza-content-types-faq-cms/baza-content-types-faq-cms.component';
import { BazaContentTypesFaqCmsRoutingModule } from './baza-content-types-faq-cms-routing.module';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesFaqCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesFaqCmsComponent,
    ],
})
export class BazaContentTypesFaqCmsModule {
}
