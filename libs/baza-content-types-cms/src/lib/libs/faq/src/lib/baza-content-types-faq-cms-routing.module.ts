import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BazaContentTypesFaqCmsComponent } from './components/baza-content-types-faq-cms/baza-content-types-faq-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesFaqCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesFaqCmsRoutingModule {
}
