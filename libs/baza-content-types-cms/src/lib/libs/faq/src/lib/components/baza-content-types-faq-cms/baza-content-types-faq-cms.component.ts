import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { catchError, tap } from 'rxjs/operators';
import { of, throwError } from 'rxjs';
import { FaqCmsListResponse, FaqDto } from '@scaliolabs/baza-content-types-shared';
import { FaqCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesFormField, BazaContentTypesFormFields } from '../../../../../../shared/src';

export const BAZA_CONTENT_TYPES_FAQ_CMS_ID = 'baza-content-types-faq-cms-id';
export const BAZA_CONTENT_TYPES_FAQ_FORM_ID = 'baza-content-types-faq-form-id';

interface State {
    config: BazaCrudConfig<FaqDto>;
}

interface FormValue {
    isPublished: boolean;
    categoryId: number;
    question: string;
    answer: string;
    seo: BazaSeoDto;
}

interface State {
    config: BazaCrudConfig<FaqDto>;
}

@Component({
    templateUrl: './baza-content-types-faq-cms.component.html',
    styleUrls: ['./baza-content-types-faq-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesFaqCmsComponent {
    public crud: BazaCrudComponent<FaqDto>;

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly endpoint: FaqCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    i18n(key: string): string {
        return `bazaContentTypes.faq.components.bazaContentTypesFaqCms.${key}`;
    }

    private crudConfig(): BazaCrudConfig<FaqDto> {
        let lastResponse: FaqCmsListResponse;

        return {
            id: BAZA_CONTENT_TYPES_FAQ_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<FaqDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (entity, formValue) => this.endpoint.create(formValue).pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.endpoint.list(request).pipe(tap((response) => (lastResponse = response))),
                    inactive: (row) => !row.isPublished,
                },
                actions: [
                    {
                        title: this.i18n('actions.edit'),
                        icon: 'edit',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<FaqDto, FormValue>({
                                formGroup: this.formGroup(),
                                formBuilder: this.formBuilder(entity),
                                title: this.i18n('actions.edit'),
                                fetch: () =>
                                    this.endpoint.getById({
                                        id: entity.id,
                                    }),
                                onSubmit: (fetched, formValue) =>
                                    this.endpoint
                                        .update({
                                            id: fetched.id,
                                            ...formValue,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            }),
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'up',
                        visible: (entity) => of(entity.sortOrder > 1),
                        action: (entity) =>
                            this.endpoint
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder - 1,
                                })
                                .subscribe(() => this.crud.refresh()),
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'down',
                        visible: (entity) => of(entity.sortOrder !== lastResponse.maxSortOrder),
                        action: (entity) =>
                            this.endpoint
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder + 1,
                                })
                                .subscribe(() => this.crud.refresh()),
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.endpoint
                                        .remove({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.success'),
                                                    translateParams: entity,
                                                }),
                                            ),
                                            catchError((err) => {
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.failed'),
                                                    translateParams: entity,
                                                });

                                                return throwError(err);
                                            }),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                    },
                    {
                        field: 'dateCreated',
                        title: this.i18n('columns.dateCreated'),
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        width: 180,
                    },
                    {
                        field: 'question',
                        title: this.i18n('columns.question'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 'auto',
                    },
                    {
                        field: 'category',
                        title: this.i18n('columns.category'),
                        source: (entity) => (entity.category ? entity.category.title : '–'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 280,
                    },
                    {
                        field: 'isPublished',
                        title: this.i18n('columns.isPublished'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        width: 150,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isPublished: [false, [Validators.required]],
            categoryId: [undefined],
            question: [undefined, [Validators.required]],
            answer: [undefined, [Validators.required]],
            seo: [bazaSeoDefault()],
        });
    }

    private formBuilder(entity?: FaqDto): BazaFormBuilder<BazaCoreFormBuilderComponents | BazaContentTypesFormFields> {
        return {
            id: BAZA_CONTENT_TYPES_FAQ_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('tabs.common'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.Checkbox,
                                    formControlName: 'isPublished',
                                    label: this.i18n('fields.isPublished'),
                                },
                                {
                                    type: BazaFormBuilderStaticComponentType.Divider,
                                },
                                {
                                    formControlName: 'categoryId',
                                    type: BazaContentTypesFormField.BazaContentTypeCategory,
                                    label: this.i18n('fields.categoryId'),
                                },
                                {
                                    type: BazaFormBuilderControlType.Text,
                                    formControlName: 'question',
                                    label: this.i18n('fields.question'),
                                    placeholder: this.i18n('placeholders.question'),
                                    required: true,
                                },
                                {
                                    type: BazaFormBuilderControlType.BazaHtml,
                                    formControlName: 'answer',
                                    label: this.i18n('fields.answer'),
                                    placeholder: this.i18n('placeholders.answer'),
                                    required: true,
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('tabs.seo'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'seo',
                                    type: BazaFormBuilderControlType.BazaSeo,
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }
}
