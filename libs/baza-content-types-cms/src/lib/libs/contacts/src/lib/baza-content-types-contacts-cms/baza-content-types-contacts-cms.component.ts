import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ContactsCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { ContactsCmsDto } from '@scaliolabs/baza-content-types-shared';

export const BAZA_CONTENT_TYPES_CONTACTS_CMS_ID = 'baza-content-types-contacts-cms-id';
export const BAZA_CONTENT_TYPES_CONTACTS_FORM_ID = 'baza-content-types-contacts-form-id';

interface FormValue {
    subject: string;
    name: string;
    email: string;
    message: string;
    isProcessed: boolean;
}

interface State {
    config: BazaCrudConfig<ContactsCmsDto>;
}

@Component({
    templateUrl: './baza-content-types-contacts-cms.component.html',
    styleUrls: ['./baza-content-types-contacts-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesContactsCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<ContactsCmsDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly formLayout: BazaFormLayoutService,
        private readonly dataAccess: ContactsCmsDataAccess,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.contacts.components.bazaContentTypesContactsCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<ContactsCmsDto> {
        return {
            id: BAZA_CONTENT_TYPES_CONTACTS_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.refresh'),
                            icon: 'sync',
                            callback: () => this.crud.refresh(),
                            type: BazaNzButtonStyle.Link,
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            icon: 'inbox',
                            callback: () => this.router.navigate(['/content-types/contacts/subjects']),
                            title: this.i18n('actions.subjects'),
                            type: BazaNzButtonStyle.Link,
                        },
                    },
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request),
                    inactive: (row) => !!row.dateProcessedAt,
                },
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: true,
                        title: this.i18n('columns.id'),
                    },
                    {
                        field: 'subject',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('columns.subject'),
                        source: (entity) => (entity.subject ? entity.subject.subject : '–'),
                    },
                    {
                        field: 'name',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('columns.name'),
                        width: 260,
                    },
                    {
                        field: 'email',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('columns.email'),
                        width: 260,
                    },
                    {
                        field: 'dateCreatedAt',
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        sortable: true,
                        title: this.i18n('columns.dateCreatedAt'),
                        width: 210,
                    },
                    {
                        field: 'dateProcessedAt',
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        sortable: true,
                        title: this.i18n('columns.dateProcessedAt'),
                        width: 210,
                    },
                    {
                        field: 'sent',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        sortable: true,
                        title: this.i18n('columns.sent'),
                        width: 120,
                    },
                ],
                actions: [
                    {
                        icon: 'eye',
                        title: this.i18n('actions.view'),
                        action: (input) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<ContactsCmsDto, FormValue, void>({
                                formGroup: this.formGroup(),
                                formBuilder: this.formBuilder(),
                                fetch: () =>
                                    this.dataAccess.getById({
                                        id: input.id,
                                    }),
                                title: this.i18n('actions.view'),
                                populate: (entity, formGroup) => {
                                    formGroup.patchValue({
                                        isProcessed: !!entity.dateProcessedAt,
                                        subject: entity.subject ? entity.subject.subject : '–',
                                        email: entity.email,
                                        name: entity.name,
                                        message: entity.message,
                                    } as FormValue);
                                },
                                onSubmit: (entity, formValue) => {
                                    if (formValue.isProcessed) {
                                        return this.dataAccess
                                            .markAsProcessed({
                                                id: entity.id,
                                            })
                                            .pipe(tap(() => this.crud.refresh()));
                                    } else {
                                        return this.dataAccess
                                            .markAsUnprocessed({
                                                id: entity.id,
                                            })
                                            .pipe(tap(() => this.crud.refresh()));
                                    }
                                },
                            }),
                    },
                    {
                        icon: 'check',
                        title: this.i18n('actions.markAsProcessed'),
                        visible: (entity) => !entity.dateProcessedAt,
                        action: (entity) =>
                            this.dataAccess
                                .markAsProcessed({
                                    id: entity.id,
                                })
                                .pipe(tap(() => this.crud.refresh()))
                                .subscribe(),
                    },
                    {
                        icon: 'close',
                        title: this.i18n('actions.markAsUnprocessed'),
                        visible: (entity) => !!entity.dateProcessedAt,
                        action: (entity) =>
                            this.dataAccess
                                .markAsUnprocessed({
                                    id: entity.id,
                                })
                                .pipe(tap(() => this.crud.refresh()))
                                .subscribe(),
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isProcessed: [false, [Validators.required]],
            subject: [undefined, [Validators.required]],
            name: [undefined, [Validators.required]],
            email: [undefined, [Validators.required]],
            message: [undefined, [Validators.required]],
        });
    }

    private formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_CONTENT_TYPES_CONTACTS_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Checkbox,
                        formControlName: 'isProcessed',
                        label: this.i18n('fields.isProcessed'),
                    },
                    {
                        type: BazaFormBuilderStaticComponentType.Divider,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'subject',
                        label: this.i18n('fields.subject'),
                        required: true,
                        readonly: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'name',
                        label: this.i18n('fields.name'),
                        required: true,
                        readonly: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'email',
                        label: this.i18n('fields.email'),
                        required: true,
                        readonly: true,
                    },
                    {
                        type: BazaFormBuilderControlType.TextArea,
                        formControlName: 'message',
                        label: this.i18n('fields.message'),
                        required: true,
                        readonly: true,
                        rows: 4,
                    },
                ],
            },
        };
    }
}
