import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { of, Subject } from 'rxjs';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { TranslateService } from '@ngx-translate/core';
import { takeUntil, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactsSubjectCmsDto } from '@scaliolabs/baza-content-types-shared';
import { ContactsSubjectCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { Router } from '@angular/router';

export const BAZA_CONTENT_TYPES_CONTACT_SUBJECTS_CMS_ID = 'baza-content-types-contact-subjects-cms-id';
export const BAZA_CONTENT_TYPES_CONTACT_SUBJECTS_FORM_ID = 'baza-content-types-contact-subjects-form-id';

interface FormValue {
    isEnabled: boolean;
    subject: string;
}

interface State {
    config: BazaCrudConfig<ContactsSubjectCmsDto>;
}

@Component({
    templateUrl: './baza-content-types-contacts-subject-cms.component.html',
    styleUrls: ['./baza-content-types-contacts-subject-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesContactsSubjectCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<ContactsSubjectCmsDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly formLayout: BazaFormLayoutService,
        private readonly dataAccess: ContactsSubjectCmsDataAccess,
        private readonly bazaConfirm: BazaNgConfirmService,
        private readonly translate: TranslateService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.contacts.components.bazaContentTypesContactsSubjectCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<ContactsSubjectCmsDto> {
        return {
            id: BAZA_CONTENT_TYPES_CONTACT_SUBJECTS_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            icon: 'mail',
                            callback: () => this.router.navigate(['/content-types/contacts/inbox']),
                            title: this.i18n('actions.inbox'),
                            type: BazaNzButtonStyle.Link,
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<ContactsSubjectCmsDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.create'),
                                    onSubmit: (entity, formValue) =>
                                        this.dataAccess
                                            .create({
                                                isEnabled: formValue.isEnabled,
                                                subject: formValue.subject,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                }),
                            title: this.i18n('actions.create'),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request),
                    inactive: (row) => !row.isEnabled,
                },
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: true,
                        title: this.i18n('columns.id'),
                    },
                    {
                        field: 'subject',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('columns.subject'),
                    },
                    {
                        field: 'isEnabled',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        sortable: true,
                        title: this.i18n('columns.isEnabled'),
                    },
                ],
                actions: [
                    {
                        icon: 'edit',
                        title: this.i18n('actions.update'),
                        action: (input) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<ContactsSubjectCmsDto, FormValue>({
                                formGroup: this.formGroup(),
                                formBuilder: this.formBuilder(),
                                fetch: () =>
                                    this.dataAccess.getById({
                                        id: input.id,
                                    }),
                                title: this.i18n('actions.update'),
                                onSubmit: (entity, formValue) =>
                                    this.dataAccess
                                        .update({
                                            id: input.id,
                                            isEnabled: formValue.isEnabled,
                                            subject: formValue.subject,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            }),
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'up',
                        visible: (entity) => of(entity.sortOrder > 0),
                        action: (entity) =>
                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder - 1,
                                })
                                .subscribe(() => this.crud.refresh()),
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'down',
                        action: (entity) =>
                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder + 1,
                                })
                                .subscribe(() => this.crud.refresh()),
                    },
                    {
                        icon: 'delete',
                        title: this.i18n('actions.delete.title'),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.bazaConfirm.open({
                                message: this.translate.instant(this.i18n('actions.delete.confirm'), {
                                    subject: entity.subject,
                                }),
                                withoutMessageTranslate: true,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() => this.crud.refresh()),
                                            takeUntil(this.ngOnDestroy$),
                                        )
                                        .toPromise(),
                            });
                        },
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isEnabled: [false, [Validators.required]],
            subject: [undefined, [Validators.required]],
        });
    }

    private formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_CONTENT_TYPES_CONTACT_SUBJECTS_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Checkbox,
                        formControlName: 'isEnabled',
                        label: this.i18n('fields.isEnabled'),
                    },
                    {
                        type: BazaFormBuilderStaticComponentType.Divider,
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'subject',
                        label: this.i18n('fields.subject'),
                        required: true,
                    },
                ],
            },
        };
    }
}
