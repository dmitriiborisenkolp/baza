import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaContentTypesContactsSubjectCmsComponent } from './baza-content-types-contacts-subject-cms/baza-content-types-contacts-subject-cms.component';
import { BazaContentTypesContactsCmsComponent } from './baza-content-types-contacts-cms/baza-content-types-contacts-cms.component';
import { BazaContentTypesContactsRoutingModule } from './baza-content-types-contacts-routing.module';
import { BazaCmsCrudBundleModule, BazaFormBuilderModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';

@NgModule({
    imports: [
        CommonModule,
        BazaContentTypesContactsRoutingModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaFormBuilderModule,
    ],
    declarations: [
        BazaContentTypesContactsCmsComponent,
        BazaContentTypesContactsSubjectCmsComponent,
    ],
})
export class BazaContentTypesContactsCmsModule {
}
