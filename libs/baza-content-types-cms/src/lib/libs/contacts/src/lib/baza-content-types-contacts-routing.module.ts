import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BazaContentTypesContactsCmsComponent } from './baza-content-types-contacts-cms/baza-content-types-contacts-cms.component';
import { BazaContentTypesContactsSubjectCmsComponent } from './baza-content-types-contacts-subject-cms/baza-content-types-contacts-subject-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'inbox',
            },
            {
                path: 'inbox',
                component: BazaContentTypesContactsCmsComponent,
            },
            {
                path: 'subjects',
                component: BazaContentTypesContactsSubjectCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesContactsRoutingModule {}
