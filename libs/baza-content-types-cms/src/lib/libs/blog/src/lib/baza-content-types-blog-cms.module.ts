import { NgModule } from '@angular/core';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesBlogCmsComponent } from './components/baza-content-types-blog-cms/baza-content-types-blog-cms.component';
import { BazaContentTypesBlogCmsRoutingModule } from './baza-content-types-blog-cms-routing.module';

@NgModule({
    imports: [
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesBlogCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesBlogCmsComponent,
    ],
})
export class BazaContentTypesBlogCmsModule {}
