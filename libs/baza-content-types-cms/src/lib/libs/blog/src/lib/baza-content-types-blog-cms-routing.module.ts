import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BazaContentTypesBlogCmsComponent } from './components/baza-content-types-blog-cms/baza-content-types-blog-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesBlogCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesBlogCmsRoutingModule {}
