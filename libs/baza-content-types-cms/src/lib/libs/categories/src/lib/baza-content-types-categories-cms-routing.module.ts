import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BazaContentTypesCategoriesCmsComponent } from './components/baza-content-types-categories-cms/baza-content-types-categories-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesCategoriesCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesCategoriesCmsRoutingModule {}
