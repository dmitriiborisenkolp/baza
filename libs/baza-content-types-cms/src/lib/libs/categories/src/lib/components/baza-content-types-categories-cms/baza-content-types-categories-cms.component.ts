import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    BazaContentDto,
    CategoriesDto,
    CategoryListDto,
    BazaContentTypes,
    emptyBazaContentDto,
} from '@scaliolabs/baza-content-types-shared';
import { CategoriesCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { finalize, map, takeUntil, tap } from 'rxjs/operators';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaContentTypesFormField, BazaContentTypesFormFields } from '../../../../../../shared/src';

export const BAZA_CONTENT_TYPES_CATEGORIES_CMS_ID = 'baza-content-types-categories-cms-id';
export const BAZA_CONTENT_TYPES_CATEGORIES_FORM_ID = 'baza-content-types-categories-form-id';

interface State {
    config: BazaCrudConfig<CategoryListDto>;
    levels: { [id: number]: number };
    lastResponse?: CategoriesDto;
    lastItems: Array<CategoryListDto>;
}

interface FormValue {
    isActive: boolean;
    parentId?: number;
    title: string;
    contents: BazaContentDto;
    seo: BazaSeoDto;
}

@Component({
    templateUrl: './baza-content-types-categories-cms.component.html',
    styleUrls: ['./baza-content-types-categories-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesCategoriesCmsComponent implements OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<CategoryListDto>;

    public state: State = {
        config: this.config,
        levels: {},
        lastItems: [],
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly formLayout: BazaFormLayoutService,
        private readonly ngLoading: BazaLoadingService,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly dataAccess: CategoriesCmsDataAccess,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.${BazaContentTypes.Categories.toLowerCase()}.components.bazaContentTypesCategoriesCms.${key}`;
    }

    get config(): BazaCrudConfig<CategoryListDto> {
        return {
            id: BAZA_CONTENT_TYPES_CATEGORIES_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.create'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<CategoryListDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.create'),
                                    onSubmit: (entity, formValue) =>
                                        this.dataAccess
                                            .create({
                                                ...formValue,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    array: () =>
                        this.dataAccess.getAll().pipe(
                            map((response) => {
                                const items: Array<CategoryListDto> = [];

                                this.state.levels = {};
                                this.state.lastResponse = response;

                                const deep = (input: Array<CategoriesDto>) => {
                                    for (const dto of input) {
                                        if (dto.category) {
                                            items.push(dto.category);

                                            this.state.levels[dto.category.id] = dto.level;
                                        }

                                        if (Array.isArray(dto.children) && dto.children.length > 0) {
                                            deep(dto.children);
                                        }
                                    }
                                };

                                deep([response]);

                                this.state.lastItems = items;

                                return {
                                    items,
                                };
                            }),
                        ),
                    inactive: (entity) => {
                        const deep = (category: CategoryListDto) => {
                            if (!category.isActive) {
                                return true;
                            } else if (category.parentId) {
                                const parent = this.state.lastItems?.find((e) => e.id === category.parentId);

                                return parent ? deep(parent) : false;
                            } else {
                                return false;
                            }
                        };

                        return deep(entity);
                    },
                },
                actions: [
                    {
                        icon: 'edit',
                        title: this.i18n('actions.update'),
                        action: (input) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<CategoryListDto, FormValue>({
                                formGroup: this.formGroup(),
                                formBuilder: this.formBuilder(input),
                                fetch: () => this.dataAccess.getById(input.id),
                                title: this.i18n('actions.create'),
                                onSubmit: (entity, formValue) =>
                                    this.dataAccess
                                        .update({
                                            id: input.id,
                                            ...formValue,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            }),
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'up',
                        visible: (entity) => entity.sortOrder > 1,
                        action: (entity) => {
                            const loading = this.ngLoading.addLoading();

                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder - 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'down',
                        visible: (entity) => {
                            const nearby = entity.parentId
                                ? this.state.lastItems.filter((e) => e.parentId === entity.parentId)
                                : this.state.lastItems.filter((e) => !e.parentId);

                            return entity.sortOrder !== _.max(nearby.map((e) => e.sortOrder));
                        },
                        action: (entity) => {
                            const loading = this.ngLoading.addLoading();

                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder + 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        icon: 'delete',
                        title: this.i18n('actions.delete.title'),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.ngConfirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () => {
                                    const loading = this.ngLoading.addLoading();

                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            finalize(() => loading.complete()),
                                            takeUntil(this.ngOnDestroy$),
                                        )
                                        .subscribe(
                                            () => {
                                                this.ngMessages.success({
                                                    message: this.i18n('actions.delete.success'),
                                                    translate: true,
                                                    translateParams: entity,
                                                });
                                                this.crud.refresh();
                                            },
                                            () =>
                                                this.ngMessages.error({
                                                    message: this.i18n('actions.delete.failed'),
                                                    translate: true,
                                                    translateParams: entity,
                                                }),
                                        );
                                },
                            });
                        },
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('columns.id'),
                    },
                    {
                        field: 'title',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.title'),
                        level: (entity) => this.state.levels[entity.id] || 0,
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'url',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('columns.url'),
                        width: 420,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isActive: [false, [Validators.required]],
            parentId: [undefined],
            title: [undefined, [Validators.required]],
            contents: [emptyBazaContentDto()],
            seo: [bazaSeoDefault()],
        });
    }

    private formBuilder(entity?: CategoryListDto): BazaFormBuilder<BazaCoreFormBuilderComponents | BazaContentTypesFormFields> {
        return {
            id: BAZA_CONTENT_TYPES_CATEGORIES_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('form.tabs.common'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'isActive',
                                    type: BazaFormBuilderControlType.Checkbox,
                                    label: this.i18n('form.fields.isActive'),
                                },
                                {
                                    type: BazaFormBuilderStaticComponentType.Divider,
                                },
                                {
                                    formControlName: 'title',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('form.fields.title'),
                                },
                                {
                                    formControlName: 'parentId',
                                    type: BazaContentTypesFormField.BazaContentTypeCategory,
                                    label: this.i18n('form.fields.parentId'),
                                    props: {
                                        excludeCategoryIds: (() => {
                                            return entity ? [entity.id] : [];
                                        })(),
                                    },
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('form.tabs.contents'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'contents',
                                    type: BazaContentTypesFormField.BazaContentTypeContent,
                                    label: this.i18n('form.fields.contents'),
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('form.tabs.seo'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'seo',
                                    type: BazaFormBuilderControlType.BazaSeo,
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }
}
