import { NgModule } from '@angular/core';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesCategoriesCmsComponent } from './components/baza-content-types-categories-cms/baza-content-types-categories-cms.component';
import { BazaContentTypesCategoriesCmsRoutingModule } from './baza-content-types-categories-cms-routing.module';

@NgModule({
    imports: [
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesCategoriesCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesCategoriesCmsComponent,
    ],
})
export class BazaContentTypesCategoriesCmsModule {}
