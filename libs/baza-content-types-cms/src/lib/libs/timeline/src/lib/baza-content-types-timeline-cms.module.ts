import { NgModule } from '@angular/core';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesTimelineCmsComponent } from './components/baza-content-types-timeline-cms/baza-content-types-timeline-cms.component';
import { BazaContentTypesTimelineCmsService } from './services/baza-content-types-timeline-cms.service';
import { BazaContentTypesTimelineCmsFormModule } from './baza-content-types-timeline-cms-form.module';
import { BazaContentTypesTimelineCmsRoutingModule } from './baza-content-types-timeline-cms-routing.module';

@NgModule({
    imports: [
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesTimelineCmsFormModule,
        BazaContentTypesTimelineCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesTimelineCmsComponent,
    ],
    providers: [
        BazaContentTypesTimelineCmsService,
    ],
})
export class BazaContentTypesTimelineCmsModule {}
