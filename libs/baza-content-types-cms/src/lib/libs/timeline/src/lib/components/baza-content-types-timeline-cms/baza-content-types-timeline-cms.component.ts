import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { TimelineCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesTimelineCmsService } from '../../services/baza-content-types-timeline-cms.service';
import { Subject } from 'rxjs';
import { BazaCrudComponent, BazaCrudConfig } from '@scaliolabs/baza-core-cms';
import { TimelineCmsDto } from '@scaliolabs/baza-content-types-shared';

interface State {
    crud: BazaCrudConfig<TimelineCmsDto>;
}

@Component({
    templateUrl: './baza-content-types-timeline-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesTimelineCmsComponent implements OnDestroy {
    private readonly ngOnDestroy$ = new Subject<void>();

    public crud: BazaCrudComponent<TimelineCmsDto>;

    public state: State = {
        crud: this.timelineCrudService.crudConfig(
            () => this.crud, {
                ngOnDestroy$: this.ngOnDestroy$,
            },
        ),
    };

    constructor(
        private readonly dataAccess: TimelineCmsDataAccess,
        private readonly timelineCrudService: BazaContentTypesTimelineCmsService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.timeline.components.bazaContentTypesTimelineCms.${key}`;
    }
}
