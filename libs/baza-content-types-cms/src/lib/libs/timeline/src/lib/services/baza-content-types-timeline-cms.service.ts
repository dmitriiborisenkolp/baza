import { Injectable } from '@angular/core';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormBuilderTab,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BAZA_CONTENT_TYPES_CONFIGURATION, TimelineCmsDto } from '@scaliolabs/baza-content-types-shared';
import { TimelineCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { catchError, takeUntil, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AttachmentDto, bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';
import { BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { Subject, throwError } from 'rxjs';
import { BazaContentTypesFormField, BazaContentTypesFormFields, bazaContentTypesMetadataValidator } from '../../../../../shared/src';

export const BAZA_CONTENT_TYPES_TIMELINE_CMS_ID = 'baza-content-types-timeline-cms-id';
export const BAZA_CONTENT_TYPES_TIMELINE_FORM_ID = 'baza-content-types-timeline-form-id';

interface CrudConfigOptions {
    i18n: (key: string) => string;
    ngOnDestroy$: Subject<void>;
    categoryId?: number;
}

interface FormValue {
    isPublished: boolean;
    tagIds: Array<number>;
    categoryIds: Array<number>;
    date: Date;
    title: string;
    textDescription: string;
    htmlDescription: string;
    imageAttachment?: AttachmentDto;
    metadata: Record<string, string>;
    seo: BazaSeoDto;
}

const defaultOptions: () => CrudConfigOptions = () => ({
    i18n: (key: string) => `bazaContentTypes.timeline.components.bazaContentTypesTimelineCms.${key}`,
    ngOnDestroy$: new Subject<void>(),
});

@Injectable()
export class BazaContentTypesTimelineCmsService {
    constructor(
        private readonly fb: FormBuilder,
        private readonly formLayout: BazaFormLayoutService,
        private readonly dataAccess: TimelineCmsDataAccess,
        private readonly messages: BazaNgMessagesService,
        private readonly confirm: BazaNgConfirmService,
    ) {}

    crudConfig(
        crudComponent: () => BazaCrudComponent<TimelineCmsDto>,
        withOptions: Partial<CrudConfigOptions> = {},
    ): BazaCrudConfig<TimelineCmsDto> {
        const options: CrudConfigOptions = { ...defaultOptions(), ...withOptions };

        return {
            id: BAZA_CONTENT_TYPES_TIMELINE_CMS_ID,
            title: options.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            icon: 'plus',
                            title: options.i18n('actions.create'),
                            callback: () => this.create(crudComponent, options),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccess.list({
                            ...request,
                            categoryId: options.categoryId,
                        }),
                },
                columns: [
                    {
                        field: 'id',
                        title: options.i18n('fields.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                    },
                    {
                        field: 'date',
                        title: options.i18n('fields.date'),
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        width: 180,
                    },
                    {
                        field: 'title',
                        title: options.i18n('fields.title'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                    {
                        field: 'isPublished',
                        title: options.i18n('fields.isPublished'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        width: 120,
                    },
                ],
                actions: [
                    {
                        icon: 'edit',
                        title: options.i18n('actions.update'),
                        action: (entity) => this.update(entity, crudComponent, options),
                    },
                    {
                        icon: 'delete',
                        title: options.i18n('actions.delete.title'),
                        action: (entity) => this.delete(entity, crudComponent, options),
                    },
                ],
            },
        };
    }

    formGroup(): FormGroup {
        return this.fb.group({
            isPublished: [true, [Validators.required]],
            tagIds: [[]],
            categoryIds: [[]],
            date: [undefined, [Validators.required]],
            title: ['', [Validators.required]],
            textDescription: [''],
            htmlDescription: [''],
            imageAttachment: [],
            metadata: [{}, [Validators.required, bazaContentTypesMetadataValidator]],
            seo: [bazaSeoDefault(), [Validators.required]],
        });
    }

    formBuilder(options: CrudConfigOptions): BazaFormBuilder<BazaCoreFormBuilderComponents | BazaContentTypesFormFields> {
        const tabs: Array<BazaFormBuilderTab<BazaCoreFormBuilderComponents | BazaContentTypesFormFields>> = [
            {
                title: options.i18n('tabs.common'),
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Checkbox,
                            formControlName: 'isPublished',
                            label: options.i18n('fields.isPublished'),
                        },
                        {
                            type: BazaFormBuilderStaticComponentType.Divider,
                        },
                        {
                            formControlName: 'title',
                            type: BazaFormBuilderControlType.Text,
                            label: options.i18n('fields.title'),
                        },
                        {
                            formControlName: 'tagIds',
                            type: BazaContentTypesFormField.BazaContentTypeTags,
                            label: options.i18n('fields.tags'),
                            visible: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.withTagsSupport,
                        },
                        {
                            formControlName: 'categoryIds',
                            type: BazaContentTypesFormField.BazaContentTypeCategories,
                            label: options.i18n('fields.categories'),
                            visible: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.withCategoriesSupport,
                        },
                        {
                            formControlName: 'date',
                            type: BazaFormBuilderControlType.Date,
                            label: options.i18n('fields.date'),
                        },
                        {
                            formControlName: 'textDescription',
                            type: BazaFormBuilderControlType.TextArea,
                            label: options.i18n('fields.textDescription'),
                            visible: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.withTextDescription,
                        },
                        {
                            formControlName: 'htmlDescription',
                            type: BazaFormBuilderControlType.BazaHtml,
                            label: options.i18n('fields.htmlDescription'),
                            visible: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.withHTMLDescription,
                        },
                        {
                            formControlName: 'imageAttachment',
                            type: BazaFormBuilderControlType.BazaImage,
                            label: options.i18n('fields.image'),
                            visible: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.withImageSupport,
                        },
                        {
                            formControlName: 'metadata',
                            type: BazaFormBuilderControlType.BazaKeyValue,
                            label: options.i18n('fields.metadata'),
                            visible: BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.withMetadata,
                        },
                    ],
                },
            },
        ];

        if (BAZA_CONTENT_TYPES_CONFIGURATION.configs.timeline.withSEO) {
            tabs.push({
                title: options.i18n('tabs.seo'),
                contents: {
                    fields: [
                        {
                            formControlName: 'seo',
                            type: BazaFormBuilderControlType.BazaSeo,
                        },
                    ],
                },
            });
        }

        return {
            id: BAZA_CONTENT_TYPES_TIMELINE_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs,
            },
        };
    }

    create(crudComponent: () => BazaCrudComponent<TimelineCmsDto>, options: CrudConfigOptions): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<TimelineCmsDto, FormValue>({
            title: options.i18n('actions.create'),
            formGroup: this.formGroup(),
            formBuilder: this.formBuilder(options),
            onSubmit: (entity, formValue) => {
                return this.dataAccess
                    .create({
                        ...formValue,
                        tagIds: formValue.tagIds || [],
                        categoryIds: formValue.categoryIds || [],
                        categoryId: options.categoryId,
                        date: formValue.date.toISOString(),
                    })
                    .pipe(
                        tap(() => crudComponent().refresh()),
                        catchError((err) => {
                            this.messages.bazaError(err);

                            return throwError(err);
                        }),
                        takeUntil(options.ngOnDestroy$),
                    );
            },
        });
    }

    update(entity: TimelineCmsDto, crudComponent: () => BazaCrudComponent<TimelineCmsDto>, options: CrudConfigOptions): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<TimelineCmsDto, FormValue>({
            title: options.i18n('actions.update'),
            formGroup: this.formGroup(),
            formBuilder: this.formBuilder(options),
            fetch: () =>
                this.dataAccess.getById({
                    id: entity.id,
                }),
            populate: (input, formGroup) => {
                formGroup.patchValue({
                    ...input,
                    tagIds: (input.tags || []).map((t) => t.id),
                    categoryIds: (input.categories || []).map((c) => c.id),
                    date: new Date(input.date),
                } as FormValue);
            },
            onSubmit: (entity, formValue) => {
                return this.dataAccess
                    .update({
                        ...formValue,
                        id: entity.id,
                        categoryId: options.categoryId,
                        tagIds: formValue.tagIds || [],
                        categoryIds: formValue.categoryIds || [],
                        date: formValue.date.toISOString(),
                    })
                    .pipe(
                        tap(() => crudComponent().refresh()),
                        catchError((err) => {
                            this.messages.bazaError(err);

                            return throwError(err);
                        }),
                        takeUntil(options.ngOnDestroy$),
                    );
            },
        });
    }

    delete(entity: TimelineCmsDto, crudComponent: () => BazaCrudComponent<TimelineCmsDto>, options: CrudConfigOptions): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.confirm.open({
            message: options.i18n('actions.delete.confirm'),
            messageArgs: entity,
            confirm: () =>
                this.dataAccess
                    .delete({
                        id: entity.id,
                    })
                    .pipe(
                        tap(() =>
                            this.messages.success({
                                translate: true,
                                message: options.i18n('actions.delete.success'),
                                translateParams: entity,
                            }),
                        ),
                        catchError((err) => {
                            this.messages.bazaError(err);

                            return throwError(err);
                        }),
                        tap(() => crudComponent().refresh()),
                    )
                    .toPromise(),
        });
    }
}
