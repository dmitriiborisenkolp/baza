import { NgModule } from '@angular/core';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesTimelineCmsService } from './services/baza-content-types-timeline-cms.service';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';

@NgModule({
    imports: [
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
    ],
    providers: [
        BazaContentTypesTimelineCmsService,
    ],
})
export class BazaContentTypesTimelineCmsFormModule {}
