import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BazaContentTypesTimelineCmsComponent } from './components/baza-content-types-timeline-cms/baza-content-types-timeline-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesTimelineCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesTimelineCmsRoutingModule {}
