import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFieldUploadPreset,
    BazaFileUploadTransport,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormBuilderUploadComponent,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    BAZA_CONTENT_TYPES_CONFIGURATION,
    NewslettersDto,
    TestimonialCmsDto,
    TestimonialsListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { of, Subject, throwError } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { catchError, finalize, takeUntil, tap } from 'rxjs/operators';
import { AttachmentImageType, AttachmentType, bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesFormFields } from '../../../../../../shared/src';
import { TestimonialsCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';

export const BAZA_CONTENT_TYPES_TESTIMONIALS_CMS_ID = 'baza-content-types-testimonials-cms-id';
export const BAZA_CONTENT_TYPES_TESTIMONIALS_FORM_ID = 'baza-content-types-testimonials-form-id';

interface FormValue {
    isPublished: boolean;
    name: string;
    title?: string;
    company?: string;
    link?: string;
    location?: string;
    email?: string;
    imageS3AwsKey?: string;
    quote: string;
    preview: string;
    seo: BazaSeoDto;
}

interface State {
    config: BazaCrudConfig<TestimonialCmsDto>;
}

@Component({
    templateUrl: './baza-content-types-testimonials-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesTestimonialsCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<NewslettersDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly dataAccess: TestimonialsCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.testimonials.components.bazaContentTypesTestimonialsCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<TestimonialCmsDto> {
        let lastResponse: TestimonialsListResponse;

        return {
            id: BAZA_CONTENT_TYPES_TESTIMONIALS_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.create'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<TestimonialCmsDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.create'),
                                    onSubmit: (entity, formValue) => this.dataAccess.create(formValue).pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request).pipe(tap((response) => (lastResponse = response))),
                    inactive: (row) => !row.isPublished,
                },
                actions: [
                    {
                        title: this.i18n('actions.update'),
                        icon: 'edit',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<TestimonialCmsDto, FormValue>({
                                formGroup: this.formGroup(),
                                formBuilder: this.formBuilder(),
                                title: this.i18n('actions.update'),
                                fetch: () =>
                                    this.dataAccess.getById({
                                        id: entity.id,
                                    }),
                                onSubmit: (fetched, formValue) =>
                                    this.dataAccess
                                        .update({
                                            id: fetched.id,
                                            ...formValue,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            }),
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'up',
                        visible: (entity) => of(entity.sortOrder > 1),
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder - 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'down',
                        visible: (entity) => of(entity.sortOrder !== lastResponse.maxSortOrder),
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder + 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.success'),
                                                    translateParams: entity,
                                                }),
                                            ),
                                            catchError((err) => {
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.failed'),
                                                    translateParams: entity,
                                                });

                                                return throwError(err);
                                            }),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: true,
                        title: this.i18n('fields.id'),
                    },
                    {
                        field: 'image',
                        type: {
                            kind: BazaTableFieldType.Image,
                            available: (entity: TestimonialCmsDto) => !!entity.imageUrl,
                            nzSrc: (entity: TestimonialCmsDto) => entity.imageUrl,
                        },
                        sortable: false,
                        title: this.i18n('fields.image'),
                        width: 140,
                    },
                    {
                        field: 'name',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.name'),
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'title',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.title'),
                        width: 280,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'url',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.url'),
                        width: 300,
                    },
                    {
                        field: 'isPublished',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        sortable: true,
                        title: this.i18n('fields.isPublished'),
                        width: 180,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isPublished: [false, [Validators.required]],
            name: [undefined, [Validators.required]],
            title: [],
            company: [],
            link: [],
            location: [],
            email: [undefined, [Validators.email]],
            imageS3AwsKey: [],
            quote: [undefined, [Validators.required]],
            preview: [undefined, [Validators.required]],
            seo: [bazaSeoDefault()],
        });
    }

    private formBuilder(): BazaFormBuilder<BazaCoreFormBuilderComponents | BazaContentTypesFormFields> {
        return {
            id: BAZA_CONTENT_TYPES_TESTIMONIALS_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('tabs.common'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.Checkbox,
                                    formControlName: 'isPublished',
                                    label: this.i18n('fields.isPublished'),
                                },
                                {
                                    type: BazaFormBuilderStaticComponentType.Divider,
                                },
                                {
                                    formControlName: 'name',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.name'),
                                },
                                {
                                    formControlName: 'title',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.title'),
                                },
                                {
                                    formControlName: 'company',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.company'),
                                },
                                {
                                    formControlName: 'link',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.link'),
                                },
                                {
                                    formControlName: 'location',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.location'),
                                },
                                {
                                    formControlName: 'email',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.email'),
                                },
                                {
                                    formControlName: 'imageS3AwsKey',
                                    type: BazaFormBuilderControlType.BazaUpload,
                                    label: this.i18n('fields.image'),
                                    props: {
                                        withOptions: {
                                            preset: BazaFieldUploadPreset.Image,
                                            transport: {
                                                type: BazaFileUploadTransport.BazaAttachment,
                                                options: () => ({
                                                    type: AttachmentType.ImageWithThumbnail,
                                                    payload: {
                                                        original: [
                                                            {
                                                                maxWidth:
                                                                    BAZA_CONTENT_TYPES_CONFIGURATION.configs.testimonials.maxFullImageWidth,
                                                            },
                                                            {
                                                                maxWidth:
                                                                    BAZA_CONTENT_TYPES_CONFIGURATION.configs.testimonials.maxFullImageWidth,
                                                                type: AttachmentImageType.WEBP,
                                                            },
                                                        ],
                                                        thumbnails: [
                                                            {
                                                                id: 'preview',
                                                                maxWidth:
                                                                    BAZA_CONTENT_TYPES_CONFIGURATION.configs.testimonials
                                                                        .maxPreviewImageWidth,
                                                            },
                                                            {
                                                                id: 'preview',
                                                                maxWidth:
                                                                    BAZA_CONTENT_TYPES_CONFIGURATION.configs.testimonials
                                                                        .maxPreviewImageWidth,
                                                                type: AttachmentImageType.WEBP,
                                                            },
                                                        ],
                                                    },
                                                }),
                                            },
                                        },
                                    } as Partial<BazaFormBuilderUploadComponent>,
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('tabs.description'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'preview',
                                    label: this.i18n('fields.preview'),
                                    type: BazaFormBuilderControlType.BazaHtml,
                                },
                                {
                                    formControlName: 'quote',
                                    label: this.i18n('fields.quote'),
                                    type: BazaFormBuilderControlType.BazaHtml,
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('tabs.seo'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'seo',
                                    type: BazaFormBuilderControlType.BazaSeo,
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }
}
