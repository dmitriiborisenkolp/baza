import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { BazaContentTypesTestimonialsCmsComponent } from './components/baza-content-types-testimonials-cms/baza-content-types-testimonials-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesTestimonialsCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesTestimonialsCmsRoutingModule {}
