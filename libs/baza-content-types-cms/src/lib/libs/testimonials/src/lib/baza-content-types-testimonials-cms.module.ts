import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesTestimonialsCmsRoutingModule } from './baza-content-types-testimonials-cms-routing.module';
import { BazaContentTypesTestimonialsCmsComponent } from './components/baza-content-types-testimonials-cms/baza-content-types-testimonials-cms.component';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesTestimonialsCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesTestimonialsCmsComponent,
    ],
})
export class BazaContentTypesTestimonialsCmsModule {}
