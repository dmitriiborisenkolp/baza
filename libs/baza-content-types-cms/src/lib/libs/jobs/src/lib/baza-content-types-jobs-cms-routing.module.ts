import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BazaContentTypesJobsCmsComponent } from './components/baza-content-types-jobs-cms/baza-content-types-jobs-cms.component';
import { BazaContentTypesJobsApplicationsCmsComponent } from './components/baza-content-types-jobs-applications-cms/baza-content-types-jobs-applications-cms.component';
import { BazaContentTypesJobsApplicationsCmsResolve } from './components/baza-content-types-jobs-applications-cms/baza-content-types-jobs-applications-cms.resolve';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesJobsCmsComponent,
            },
            {
                path: ':id',
                component: BazaContentTypesJobsApplicationsCmsComponent,
                resolve: {
                    data: BazaContentTypesJobsApplicationsCmsResolve,
                },
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesJobsCmsRoutingModule {}
