import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaContentDto, emptyBazaContentDto, JobCmsDto, JobsCmsListResponse } from '@scaliolabs/baza-content-types-shared';
import { of, Subject, throwError } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { catchError, finalize, takeUntil, tap } from 'rxjs/operators';
import { bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';
import { JobsCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { bazaContentTypesContentValidator, BazaContentTypesFormField, BazaContentTypesFormFields } from '../../../../../../shared/src';

export const BAZA_CONTENT_TYPES_JOBS_CMS_ID = 'baza-content-types-jobs-cms-id';
export const BAZA_CONTENT_TYPES_JOBS_FORM_ID = 'baza-content-types-jobs-form-id';

interface FormValue {
    isPublished: boolean;
    datePublishedAt: string;
    primaryCategoryId?: number;
    additionalCategoryIds?: Array<number>;
    title: string;
    team?: string;
    link?: string;
    location?: string;
    jobSpecificEmail?: string;
    shortContents: string;
    contents: BazaContentDto;
    seo: BazaSeoDto;
}

interface State {
    config: BazaCrudConfig<JobCmsDto>;
}

@Component({
    templateUrl: './baza-content-types-jobs-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesJobsCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<JobCmsDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly dataAccess: JobsCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.jobs.components.bazaContentTypesJobsCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<JobCmsDto> {
        let lastResponse: JobsCmsListResponse;

        const formGroup = this.formGroup();

        return {
            id: BAZA_CONTENT_TYPES_JOBS_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.create'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<JobCmsDto, FormValue>({
                                    formGroup,
                                    formBuilder: this.formBuilder(formGroup),
                                    title: this.i18n('actions.create'),
                                    onSubmit: (entity, formValue) => this.dataAccess.create(formValue).pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request).pipe(tap((response) => (lastResponse = response))),
                    inactive: (row) => !row.isPublished,
                },
                actions: [
                    {
                        title: this.i18n('actions.update'),
                        icon: 'edit',
                        action: (entity) => {
                            const formGroup = this.formGroup();

                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<JobCmsDto, FormValue>({
                                formGroup,
                                formBuilder: this.formBuilder(formGroup),
                                title: this.i18n('actions.update'),
                                fetch: () =>
                                    this.dataAccess.getById({
                                        id: entity.id,
                                    }),
                                populate: (input, formGroup) =>
                                    formGroup.patchValue({
                                        ...input,
                                        additionalCategoryIds: (input.additionalCategories || []).map((c) => c.id),
                                        primaryCategoryId: input.primaryCategory ? input.primaryCategory.id : undefined,
                                    } as FormValue),
                                onSubmit: (fetched, formValue) =>
                                    this.dataAccess
                                        .update({
                                            id: fetched.id,
                                            ...formValue,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            });
                        },
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'up',
                        visible: (entity) => of(entity.sortOrder !== lastResponse.maxSortOrder),
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder + 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'down',
                        visible: (entity) => of(entity.sortOrder > 1),
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder - 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.success'),
                                                    translateParams: entity,
                                                }),
                                            ),
                                            catchError((err) => {
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.failed'),
                                                    translateParams: entity,
                                                });

                                                return throwError(err);
                                            }),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('fields.id'),
                    },
                    {
                        field: 'datePublishedAt',
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        title: this.i18n('fields.datePublishedAt'),
                        textAlign: BazaTableFieldAlign.Center,
                        width: 260,
                    },
                    {
                        field: 'title',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.title'),
                    },
                    {
                        field: 'url',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.url'),
                        width: 300,
                    },
                    {
                        field: 'applications',
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (entity) => ({
                                routerLink: ['/content-types/jobs', entity.id],
                            }),
                            format: (entity) => {
                                if (entity.countApplications === 0) {
                                    return this.i18n('applications.no_applications');
                                } else if (entity.countApplications === 1) {
                                    return this.i18n('applications.one_application');
                                } else {
                                    return this.i18n('applications.many_applications');
                                }
                            },
                            translate: true,
                            translateArgs: (entity) => ({
                                count: entity.countApplications,
                            }),
                        },
                        title: this.i18n('fields.applications'),
                        textAlign: BazaTableFieldAlign.Center,
                        width: 190,
                    },
                    {
                        field: 'isPublished',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        title: this.i18n('fields.isPublished'),
                        width: 180,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isPublished: [false, [Validators.required]],
            datePublishedAt: [new Date(), [Validators.required]],
            primaryCategoryId: [],
            additionalCategoryIds: [[]],
            title: [undefined, [Validators.required]],
            team: [],
            link: [],
            location: [],
            jobSpecificEmail: [undefined, [Validators.email]],
            shortContents: [undefined, [Validators.required]],
            contents: [emptyBazaContentDto(), [Validators.required, bazaContentTypesContentValidator]],
            seo: [bazaSeoDefault()],
        });
    }

    private formBuilder(form: FormGroup): BazaFormBuilder<BazaCoreFormBuilderComponents | BazaContentTypesFormFields> {
        return {
            id: BAZA_CONTENT_TYPES_JOBS_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('tabs.common'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.Checkbox,
                                    formControlName: 'isPublished',
                                    label: this.i18n('fields.isPublished'),
                                },
                                {
                                    type: BazaFormBuilderControlType.Date,
                                    formControlName: 'datePublishedAt',
                                    label: this.i18n('fields.datePublishedAt'),
                                    visible: form.get('isPublished').valueChanges,
                                },
                                {
                                    type: BazaFormBuilderStaticComponentType.Divider,
                                },
                                {
                                    formControlName: 'primaryCategoryId',
                                    type: BazaContentTypesFormField.BazaContentTypeCategory,
                                    label: this.i18n('fields.primaryCategory'),
                                },
                                {
                                    formControlName: 'additionalCategoryIds',
                                    type: BazaContentTypesFormField.BazaContentTypeCategories,
                                    label: this.i18n('fields.additionalCategories'),
                                },
                                {
                                    formControlName: 'title',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.title'),
                                },
                                {
                                    formControlName: 'link',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.link'),
                                },
                                {
                                    formControlName: 'team',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.team'),
                                },
                                {
                                    formControlName: 'jobSpecificEmail',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.jobSpecificEmail'),
                                },
                                {
                                    formControlName: 'shortContents',
                                    type: BazaFormBuilderControlType.BazaHtml,
                                    label: this.i18n('fields.shortContents'),
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('tabs.contents'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'contents',
                                    type: BazaContentTypesFormField.BazaContentTypeContent,
                                    label: this.i18n('fields.contents'),
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('tabs.seo'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'seo',
                                    type: BazaFormBuilderControlType.BazaSeo,
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }
}
