import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { JobApplicationCmsListResponse, JobApplicationDto } from '@scaliolabs/baza-content-types-shared';
import { Subject, throwError } from 'rxjs';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { catchError, tap } from 'rxjs/operators';
import { JobsApplicationCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { ActivatedRoute } from '@angular/router';
import { BazaContentTypesJobsApplicationsCmsResolveData } from './baza-content-types-jobs-applications-cms.resolve';

export const BAZA_CONTENT_TYPES_JOB_APPLICATIONS_CMS_ID = 'baza-content-types-job-applications-cms-id';

interface State {
    config: BazaCrudConfig<JobApplicationDto>;
}

@Component({
    templateUrl: './baza-content-types-jobs-applications-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesJobsApplicationsCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<JobApplicationDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly dataAccess: JobsApplicationCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    get resolvedData(): BazaContentTypesJobsApplicationsCmsResolveData {
        return this.activatedRoute.snapshot.data['data'];
    }

    i18n(key: string): string {
        return `bazaContentTypes.jobs.components.bazaContentTypesJobsApplicationsCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<JobApplicationDto> {
        let lastResponse: JobApplicationCmsListResponse;

        return {
            id: BAZA_CONTENT_TYPES_JOB_APPLICATIONS_CMS_ID,
            title: this.i18n('title'),
            titleArguments: this.resolvedData.job,
            items: {},
            backRouterLink: {
                routerLink: ['/content-types/jobs'],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccess
                            .list({
                                ...request,
                                jobId: this.resolvedData.job.id,
                            })
                            .pipe(tap((response) => (lastResponse = response))),
                },
                actions: [
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.success'),
                                                    translateParams: entity,
                                                }),
                                            ),
                                            catchError((err) => {
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.failed'),
                                                    translateParams: entity,
                                                });

                                                return throwError(err);
                                            }),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('fields.id'),
                    },
                    {
                        field: 'dateCreatedAt',
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        title: this.i18n('fields.dateCreatedAt'),
                        textAlign: BazaTableFieldAlign.Center,
                        width: 260,
                    },
                    {
                        field: 'name',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.name'),
                        source: (entity) => `${entity.firstName} ${entity.lastName}`,
                    },
                    {
                        field: 'location',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.location'),
                        width: 400,
                    },
                    {
                        field: 'email',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.email'),
                        width: 300,
                    },
                    {
                        field: 'phone',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.phone'),
                        width: 260,
                    },
                ],
            },
        };
    }
}
