import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesJobsCmsRoutingModule } from './baza-content-types-jobs-cms-routing.module';
import { BazaContentTypesJobsCmsComponent } from './components/baza-content-types-jobs-cms/baza-content-types-jobs-cms.component';
import { BazaContentTypesJobsApplicationsCmsResolve } from './components/baza-content-types-jobs-applications-cms/baza-content-types-jobs-applications-cms.resolve';
import { BazaContentTypesJobsApplicationsCmsComponent } from './components/baza-content-types-jobs-applications-cms/baza-content-types-jobs-applications-cms.component';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesJobsCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesJobsCmsComponent,
        BazaContentTypesJobsApplicationsCmsComponent,
    ],
    providers: [
        BazaContentTypesJobsApplicationsCmsResolve,
    ],
})
export class BazaContentTypesJobsCmsModule {}
