import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { JobCmsDto } from '@scaliolabs/baza-content-types-shared';
import { JobsCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { map } from "rxjs/operators";

interface ResolveData {
    job: JobCmsDto;
}

export { ResolveData as BazaContentTypesJobsApplicationsCmsResolveData };

@Injectable()
export class BazaContentTypesJobsApplicationsCmsResolve implements Resolve<ResolveData | void> {
    constructor(
        private readonly router: Router,
        private readonly dataAccess: JobsCmsDataAccess,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> | Promise<ResolveData> | ResolveData | void {
        const jobId = parseInt(route.params['id'], 10);

        if (! jobId || isNaN(jobId)) {
            this.router.navigate(['/content-types/jobs']);

            return;
        }

        const observables: [
            Observable<JobCmsDto>,
        ] = [
            this.dataAccess.getById({ id: jobId }),
        ];

        return combineLatest(observables).pipe(
            map(([job]) => ({
                job,
            })),
        );
    }
}
