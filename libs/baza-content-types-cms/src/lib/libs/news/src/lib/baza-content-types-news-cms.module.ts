import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesNewsCmsRoutingModule } from './baza-content-types-news-cms-routing.module';
import { BazaContentTypesNewsCmsComponent } from './components/baza-content-types-news-cms/baza-content-types-news-cms.component';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesNewsCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesNewsCmsComponent,
    ],
})
export class BazaContentTypesNewsCmsModule {}
