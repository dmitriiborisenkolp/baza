import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BazaContentTypesNewsCmsComponent } from './components/baza-content-types-news-cms/baza-content-types-news-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesNewsCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesNewsCmsRoutingModule {}
