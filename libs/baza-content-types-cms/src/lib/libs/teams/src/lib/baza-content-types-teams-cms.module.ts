import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesTeamsCmsRoutingModule } from './baza-content-types-teams-cms-routing.module';
import { BazaContentTypesTeamsCmsComponent } from './components/baza-content-types-teams-cms/baza-content-types-teams-cms.component';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesTeamsCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesTeamsCmsComponent,
    ],
})
export class BazaContentTypesTeamsCmsModule {}
