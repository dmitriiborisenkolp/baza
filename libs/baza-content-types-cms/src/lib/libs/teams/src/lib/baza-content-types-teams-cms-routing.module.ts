import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { BazaContentTypesTeamsCmsComponent } from './components/baza-content-types-teams-cms/baza-content-types-teams-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesTeamsCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesTeamsCmsRoutingModule {}
