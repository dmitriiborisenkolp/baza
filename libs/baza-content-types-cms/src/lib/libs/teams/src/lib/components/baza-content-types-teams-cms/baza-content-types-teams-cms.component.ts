import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFieldUploadPreset,
    BazaFileUploadTransport,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormBuilderUploadComponent,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    BAZA_CONTENT_TYPES_CONFIGURATION,
    BazaContentDto,
    emptyBazaContentDto,
    TeamMemberCmsDto,
    TeamsMemberListResponse,
} from '@scaliolabs/baza-content-types-shared';
import { of, Subject, throwError } from 'rxjs';
import { TeamsMembersCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { catchError, finalize, takeUntil, tap } from 'rxjs/operators';
import { AttachmentType, bazaSeoDefault, BazaSeoDto, BazaSocialNetworksDto } from '@scaliolabs/baza-core-shared';
import { BazaContentTypesFormField, BazaContentTypesFormFields } from '../../../../../../shared/src';

export const BAZA_CONTENT_TYPES_TEAMS_CMS_ID = 'baza-content-types-teams-cms-id';
export const BAZA_CONTENT_TYPES_TEAMS_FORM_ID = 'baza-content-types-teams-form-id';

interface FormValue {
    isPublished: boolean;
    name: string;
    title?: string;
    bio: BazaContentDto;
    fullImageS3Key?: string;
    previewImageS3Key?: string;
    networks: BazaSocialNetworksDto;
    seo: BazaSeoDto;
}

interface State {
    config: BazaCrudConfig<TeamMemberCmsDto>;
}

@Component({
    templateUrl: './baza-content-types-cms.component.html',
    styleUrls: ['./baza-content-types-teams-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesTeamsCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<TeamMemberCmsDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly dataAccess: TeamsMembersCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.teams.components.bazaContentTypesTeamsCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<TeamMemberCmsDto> {
        let lastResponse: TeamsMemberListResponse;

        return {
            id: BAZA_CONTENT_TYPES_TEAMS_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.create'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<TeamMemberCmsDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.create'),
                                    onSubmit: (entity, formValue) => this.dataAccess.create(formValue).pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request).pipe(tap((response) => (lastResponse = response))),
                    inactive: (row) => !row.isPublished,
                },
                actions: [
                    {
                        title: this.i18n('actions.update'),
                        icon: 'edit',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<TeamMemberCmsDto, FormValue>({
                                formGroup: this.formGroup(),
                                formBuilder: this.formBuilder(),
                                title: this.i18n('actions.update'),
                                fetch: () =>
                                    this.dataAccess.getById({
                                        id: entity.id,
                                    }),
                                onSubmit: (fetched, formValue) =>
                                    this.dataAccess
                                        .update({
                                            id: fetched.id,
                                            ...formValue,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            }),
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'up',
                        visible: (entity) => of(entity.sortOrder > 1),
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder - 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'down',
                        visible: (entity) => of(entity.sortOrder !== lastResponse.maxSortOrder),
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.dataAccess
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder + 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.success'),
                                                    translateParams: entity,
                                                }),
                                            ),
                                            catchError((err) => {
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.failed'),
                                                    translateParams: entity,
                                                });

                                                return throwError(err);
                                            }),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: true,
                        title: this.i18n('fields.id'),
                    },
                    {
                        field: 'previewImage',
                        type: {
                            kind: BazaTableFieldType.Image,
                            available: (entity: TeamMemberCmsDto) => !!entity.previewImageUrl,
                            nzSrc: (entity: TeamMemberCmsDto) => entity.previewImageUrl,
                        },
                        sortable: false,
                        title: this.i18n('fields.previewImage'),
                        width: 140,
                    },
                    {
                        field: 'name',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.name'),
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'title',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.title'),
                        width: 280,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'url',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.url'),
                        width: 300,
                    },
                    {
                        field: 'isPublished',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        sortable: true,
                        title: this.i18n('fields.isPublished'),
                        width: 180,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isPublished: [false, [Validators.required]],
            name: [undefined, [Validators.required]],
            title: [],
            bio: [emptyBazaContentDto()],
            fullImageS3Key: [],
            previewImageS3Key: [],
            networks: [[]],
            seo: [bazaSeoDefault()],
        });
    }

    private formBuilder(): BazaFormBuilder<BazaCoreFormBuilderComponents | BazaContentTypesFormFields> {
        return {
            id: BAZA_CONTENT_TYPES_TEAMS_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('tabs.common'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.Checkbox,
                                    formControlName: 'isPublished',
                                    label: this.i18n('fields.isPublished'),
                                },
                                {
                                    type: BazaFormBuilderStaticComponentType.Divider,
                                },
                                {
                                    formControlName: 'name',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.name'),
                                },
                                {
                                    formControlName: 'title',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.title'),
                                },
                                {
                                    formControlName: 'fullImageS3Key',
                                    type: BazaFormBuilderControlType.BazaUpload,
                                    label: this.i18n('fields.fullImage'),
                                    props: {
                                        withOptions: {
                                            preset: BazaFieldUploadPreset.Image,
                                            transport: {
                                                type: BazaFileUploadTransport.BazaAttachment,
                                                options: () => ({
                                                    type: AttachmentType.Image,
                                                    payload: {
                                                        maxWidth: BAZA_CONTENT_TYPES_CONFIGURATION.configs.teams.maxFullImageWidth,
                                                    },
                                                }),
                                            },
                                        },
                                    } as Partial<BazaFormBuilderUploadComponent>,
                                },
                                {
                                    formControlName: 'previewImageS3Key',
                                    type: BazaFormBuilderControlType.BazaUpload,
                                    label: this.i18n('fields.previewImage'),
                                    props: {
                                        withOptions: {
                                            preset: BazaFieldUploadPreset.Image,
                                            transport: {
                                                type: BazaFileUploadTransport.BazaAttachment,
                                                options: () => ({
                                                    type: AttachmentType.Image,
                                                    payload: {
                                                        maxWidth: BAZA_CONTENT_TYPES_CONFIGURATION.configs.teams.maxPreviewImageWidth,
                                                    },
                                                }),
                                            },
                                        },
                                    } as Partial<BazaFormBuilderUploadComponent>,
                                },
                                {
                                    formControlName: 'bio',
                                    type: BazaContentTypesFormField.BazaContentTypeContent,
                                    label: this.i18n('fields.bio'),
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('tabs.seo'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'seo',
                                    type: BazaFormBuilderControlType.BazaSeo,
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }
}
