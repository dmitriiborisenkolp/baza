import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BazaContentTypesTagsCmsComponent } from './components/baza-content-types-tags-cms/baza-content-types-tags-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesTagsCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesTagsCmsRoutingModule {}
