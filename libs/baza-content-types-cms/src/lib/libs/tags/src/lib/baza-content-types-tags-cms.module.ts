import { NgModule } from "@angular/core";
import { BazaContentTypesTagsCmsComponent } from './components/baza-content-types-tags-cms/baza-content-types-tags-cms.component';
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesTagsCmsRoutingModule } from './baza-content-types-tags-cms-routing.module';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesTagsCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesTagsCmsComponent,
    ],
})
export class BazaContentTypesTagsCmsModule {}
