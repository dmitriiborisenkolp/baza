import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BazaContentTypesPagesCmsComponent } from './components/baza-content-types-pages-cms/baza-content-types-pages-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesPagesCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesPagesCmsRoutingModule {}
