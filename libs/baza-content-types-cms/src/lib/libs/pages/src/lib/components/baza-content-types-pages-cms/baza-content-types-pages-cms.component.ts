import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaCrudItems,
    BazaFieldUploadPreset,
    BazaFileUploadTransport,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormBuilderUploadComponent,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    PagesCmsListResponse,
    BazaContentDto,
    emptyBazaContentDto,
    BAZA_CONTENT_TYPES_CONFIGURATION,
    PageCmsDto,
} from '@scaliolabs/baza-content-types-shared';
import { Subject, throwError } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { catchError, tap } from 'rxjs/operators';
import { AttachmentType, bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';
import { PagesCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { bazaContentTypesContentValidator, BazaContentTypesFormField, BazaContentTypesFormFields } from '../../../../../../shared/src';

export const BAZA_CONTENT_TYPES_PAGES_CMS_ID = 'baza-content-types-pages-cms-id';
export const BAZA_CONTENT_TYPES_PAGES_FORM_ID = 'baza-content-types-pages-form-id';

interface FormValue {
    isPublished: boolean;
    primaryCategoryId?: number;
    additionalCategoryIds?: Array<number>;
    title: string;
    headerImageAwsS3Key?: string;
    contents: BazaContentDto;
    seo: BazaSeoDto;
}

interface State {
    config: BazaCrudConfig<PageCmsDto>;
}

@Component({
    templateUrl: './baza-content-types-pages-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesPagesCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<PageCmsDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly dataAccess: PagesCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.pages.components.bazaContentTypesPagesCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<PageCmsDto> {
        let lastResponse: PagesCmsListResponse;

        const topRight: Array<BazaCrudItems> = [];

        if (BAZA_CONTENT_TYPES_CONFIGURATION.configs.pages.allowModifyPagesList) {
            topRight.push({
                type: BazaCrudItem.Button,
                options: {
                    title: this.i18n('actions.create'),
                    callback: () =>
                        // eslint-disable-next-line security/detect-non-literal-fs-filename
                        this.formLayout.open<PageCmsDto, FormValue>({
                            formGroup: this.formGroup(),
                            formBuilder: this.formBuilder(),
                            title: this.i18n('actions.create'),
                            onSubmit: (entity, formValue) => this.dataAccess.create(formValue).pipe(tap(() => this.crud.refresh())),
                        }),
                },
            });
        }

        return {
            id: BAZA_CONTENT_TYPES_PAGES_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight,
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request).pipe(tap((response) => (lastResponse = response))),
                    inactive: (row) => !row.isPublished,
                },
                actions: [
                    {
                        title: this.i18n('actions.update'),
                        icon: 'edit',
                        action: (entity) => {
                            const formGroup = this.formGroup();

                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<PageCmsDto, FormValue>({
                                formGroup,
                                formBuilder: this.formBuilder(),
                                title: this.i18n('actions.update'),
                                fetch: () =>
                                    this.dataAccess.getById({
                                        id: entity.id,
                                    }),
                                populate: (input, formGroup) =>
                                    formGroup.patchValue({
                                        ...input,
                                        additionalCategoryIds: (input.additionalCategories || []).map((c) => c.id),
                                        primaryCategoryId: input.primaryCategory ? input.primaryCategory.id : undefined,
                                    } as FormValue),
                                onSubmit: (fetched, formValue) =>
                                    this.dataAccess
                                        .update({
                                            id: fetched.id,
                                            ...formValue,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            });
                        },
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        visible: () => BAZA_CONTENT_TYPES_CONFIGURATION.configs.pages.allowModifyPagesList,
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.success'),
                                                    translateParams: entity,
                                                }),
                                            ),
                                            catchError((err) => {
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.failed'),
                                                    translateParams: entity,
                                                });

                                                return throwError(err);
                                            }),
                                            tap(() => this.crud.refresh()),
                                        )
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        title: this.i18n('fields.id'),
                    },
                    {
                        field: 'title',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.title'),
                    },
                    {
                        field: 'url',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        title: this.i18n('fields.url'),
                        width: 300,
                    },
                    {
                        field: 'isPublished',
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        title: this.i18n('fields.isPublished'),
                        width: 180,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            isPublished: [false, [Validators.required]],
            primaryCategoryId: [],
            additionalCategoryIds: [[]],
            title: [undefined, [Validators.required]],
            headerImageAwsS3Key: [],
            contents: [emptyBazaContentDto(), [Validators.required, bazaContentTypesContentValidator]],
            seo: [bazaSeoDefault()],
        });
    }

    private formBuilder(): BazaFormBuilder<BazaCoreFormBuilderComponents | BazaContentTypesFormFields> {
        return {
            id: BAZA_CONTENT_TYPES_PAGES_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('tabs.common'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.Checkbox,
                                    formControlName: 'isPublished',
                                    label: this.i18n('fields.isPublished'),
                                },
                                {
                                    type: BazaFormBuilderStaticComponentType.Divider,
                                },
                                {
                                    formControlName: 'primaryCategoryId',
                                    type: BazaContentTypesFormField.BazaContentTypeCategory,
                                    label: this.i18n('fields.primaryCategory'),
                                },
                                {
                                    formControlName: 'additionalCategoryIds',
                                    type: BazaContentTypesFormField.BazaContentTypeCategories,
                                    label: this.i18n('fields.additionalCategories'),
                                },
                                {
                                    formControlName: 'title',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.title'),
                                },
                                {
                                    formControlName: 'headerImageAwsS3Key',
                                    type: BazaFormBuilderControlType.BazaUpload,
                                    label: this.i18n('fields.headerImage'),
                                    props: {
                                        withOptions: {
                                            preset: BazaFieldUploadPreset.Image,
                                            transport: {
                                                type: BazaFileUploadTransport.BazaAttachment,
                                                options: () => ({
                                                    type: AttachmentType.Image,
                                                    payload: {
                                                        maxWidth: BAZA_CONTENT_TYPES_CONFIGURATION.configs.pages.maxHeaderImageWidth,
                                                    },
                                                }),
                                            },
                                        },
                                    } as Partial<BazaFormBuilderUploadComponent>,
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('tabs.contents'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'contents',
                                    type: BazaContentTypesFormField.BazaContentTypeContent,
                                    label: this.i18n('fields.contents'),
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('tabs.seo'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'seo',
                                    type: BazaFormBuilderControlType.BazaSeo,
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }
}
