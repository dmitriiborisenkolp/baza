import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesPagesCmsComponent } from './components/baza-content-types-pages-cms/baza-content-types-pages-cms.component';
import { BazaContentTypesPagesCmsRoutingModule } from './baza-content-types-pages-cms-routing.module';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesPagesCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesPagesCmsComponent,
    ],
})
export class BazaContentTypesPagesCmsModule {}
