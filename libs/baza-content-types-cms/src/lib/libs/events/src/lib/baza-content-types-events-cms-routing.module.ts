import { NgModule } from "@angular/core";
import { RouterModule } from '@angular/router';
import { BazaContentTypesEventsCmsComponent } from './components/baza-content-types-events-cms/baza-content-types-events-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesEventsCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesEventsCmsRoutingModule {}
