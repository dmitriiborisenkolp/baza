import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';
import { BazaContentTypesEventsCmsRoutingModule } from './baza-content-types-events-cms-routing.module';
import { BazaContentTypesEventsCmsComponent } from './components/baza-content-types-events-cms/baza-content-types-events-cms.component';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesEventsCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesEventsCmsComponent,
    ],
})
export class BazaContentTypesEventsCmsModule {}
