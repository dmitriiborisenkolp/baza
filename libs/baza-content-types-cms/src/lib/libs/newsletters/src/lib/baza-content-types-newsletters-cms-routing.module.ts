import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BazaContentTypesNewslettersCmsComponent } from './components/baza-content-types-newsletters-cms/baza-content-types-newsletters-cms.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BazaContentTypesNewslettersCmsComponent,
            },
        ]),
    ],
    exports: [
        RouterModule,
    ],
})
export class BazaContentTypesNewslettersCmsRoutingModule {}
