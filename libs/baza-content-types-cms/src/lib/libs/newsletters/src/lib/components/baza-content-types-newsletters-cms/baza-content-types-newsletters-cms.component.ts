import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutModalService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { BAZA_CONTENT_TYPES_CONFIGURATION, NewslettersDto } from '@scaliolabs/baza-content-types-shared';
import { NewslettersCmsDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export const BAZA_CONTENT_TYPES_NEWSLETTERS_CMS_ID = 'baza-content-types-newsletters-cms-id';
export const BAZA_CONTENT_TYPES_NEWSLETTERS_FORM_ID = 'baza-content-types-newsletters-form-id';

interface State {
    config: BazaCrudConfig<NewslettersDto>;
}

interface FormValue {
    name?: string;
    email: string;
}

@Component({
    templateUrl: './baza-content-types-newsletters-cms.component.html',
    styleUrls: ['./baza-content-types-newsletters-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaContentTypesNewslettersCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<NewslettersDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly dataAccess: NewslettersCmsDataAccess,
        private readonly bazaConfirm: BazaNgConfirmService,
        private readonly translate: TranslateService,
        private readonly formLayout: BazaFormLayoutModalService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaContentTypes.newsletters.components.bazaContentTypesNewslettersCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<NewslettersDto> {
        const config: BazaCrudConfig<NewslettersDto> = {
            id: BAZA_CONTENT_TYPES_NEWSLETTERS_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('actions.exportToCSV'),
                            icon: 'download',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (request) =>
                                this.dataAccess.exportToCSV({
                                    ...request,
                                    listRequest: this.crud.data.state.lastRequest as any,
                                    fields: ['email', 'dateCreated'].map((field) => ({
                                        field: field as any,
                                        title: this.translate.instant(this.i18n(`fields.${field}`)),
                                    })),
                                }),
                            fileNameGenerator: () => 'newsletters-subscriptions-' + moment(new Date()).format('MM-DD-YYYY'),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            icon: 'plus',
                            type: BazaNzButtonStyle.Default,
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<NewslettersDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (entity, formValue) => this.dataAccess.add(formValue).pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request),
                },
                columns: [
                    {
                        field: 'email',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                        title: this.i18n('fields.email'),
                    },
                ],
                actions: [
                    {
                        icon: 'delete',
                        title: this.i18n('actions.remove.title'),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.bazaConfirm.open({
                                message: this.translate.instant(this.i18n('actions.remove.confirm'), {
                                    email: entity.email,
                                }),
                                withoutMessageTranslate: true,
                                confirm: () =>
                                    this.dataAccess
                                        .remove({
                                            email: entity.email,
                                        })
                                        .pipe(
                                            tap(() => this.crud.refresh()),
                                            takeUntil(this.ngOnDestroy$),
                                        )
                                        .toPromise(),
                            });
                        },
                    },
                ],
            },
        };

        if (BAZA_CONTENT_TYPES_CONFIGURATION.configs.newsletters.withName) {
            config.data.columns.push({
                field: 'name',
                type: {
                    kind: BazaTableFieldType.Text,
                },
                sortable: true,
                title: this.i18n('fields.name'),
                width: 550,
            });
        }

        config.data.columns.push({
            field: 'dateCreated',
            type: {
                kind: BazaTableFieldType.DateTime,
            },
            sortable: true,
            title: this.i18n('fields.dateCreated'),
            width: 280,
        });

        return config;
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            name: [],
            email: [undefined, [Validators.required, Validators.email]],
        });
    }

    private formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_CONTENT_TYPES_NEWSLETTERS_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'name',
                        label: this.i18n('fields.name'),
                        placeholder: this.i18n('placeholders.name'),
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'email',
                        label: this.i18n('fields.email'),
                        required: true,
                    },
                ],
            },
        };
    }
}
