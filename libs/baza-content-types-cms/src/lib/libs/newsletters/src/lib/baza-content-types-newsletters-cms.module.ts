import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaContentTypesNewslettersCmsComponent } from './components/baza-content-types-newsletters-cms/baza-content-types-newsletters-cms.component';
import { BazaContentTypesNewslettersCmsRoutingModule } from './baza-content-types-newsletters-cms-routing.module';
import { BazaContentTypesDataAccessModule } from '@scaliolabs/baza-content-types-data-access';

@NgModule({
    imports: [
        CommonModule,
        BazaCmsCrudBundleModule,
        BazaContentTypesDataAccessModule,
        BazaContentTypesNewslettersCmsRoutingModule,
    ],
    declarations: [
        BazaContentTypesNewslettersCmsComponent,
    ],
})
export class BazaContentTypesNewslettersCmsModule {}
