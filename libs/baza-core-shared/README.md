# baza-core-shared

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-core-shared` to execute the unit tests via [Jest](https://jestjs.io).

## Build

Run `nx build baza-core-shared`

## Publish

Run `yarn publish:baza-core-shared`
