import { bazaAuthI18nApiEn } from '../../../../libs/baza-auth/src';
import { bazaAccountApiEnI18n } from '../../../../libs/baza-account/src';
import { bazaInviteCodeApiEnI18n } from '../../../../libs/baza-invite-code/src';
import { bazaReferralCodeApiEnI18n } from '../../../../libs/baza-referral-code/src';

export const bazaApiBundleEnI18n = {
    'baza-auth': bazaAuthI18nApiEn,
    'baza-account': bazaAccountApiEnI18n,
    'baza-invite-code': bazaInviteCodeApiEnI18n,
    'baza-referral-code': bazaReferralCodeApiEnI18n,
}
