import { bazaAccountAclI18nEn, bazaAccountI18nEn } from '../../../../libs/baza-account/src';
import { bazaDashboardI18nEn } from '../../../../libs/baza-dashboard/src';
import { bazaCrudEni18n } from '../../../../libs/baza-crud/src';
import { bazaFormBuilderCmsEn } from '../../../../libs/baza-form-builder/src';
import { bazaMaintenanceCmsEnI18n } from '../../../../libs/baza-maintenance/src';
import { bazaRegistryCmsEnI18n } from '../../../../libs/baza-registry/src';
import { bazaCommonCmsEnI18n } from '../../../../libs/baza-common/src';
import { bazaAuthI18nCmsEn } from '../../../../libs/baza-auth/src';
import { bazaAuthSessionCmsI18n } from '../../../../libs/baza-auth-sessions/src';
import { bazaCoreWhitelistAccountCmsEnI18n } from '../../../../libs/baza-whitelist-account/src';
import { bazaInviteCodeAclI18n, bazaInviteCodeCmsEnI18n } from '../../../../libs/baza-invite-code/src';
import { bazaReferralCodeCmsEnI18n, bazaReferralCodesAclI18n } from '../../../../libs/baza-referral-code/src';
import { bazaDeviceTokenCmsI18n } from '../../../../libs/baza-device-token/src';
import { bazaMailAclI18n, bazaMailCmsEnI18n } from '../../../../libs/baza-mail/src';

export const bazaCmsBundleEnI18n = {
    __http_error: 'Something went wrong',
    __baza_error: '{{ error.message }}',
    __acl: {
        core: {
            group: 'General',
            nodes: {
                SystemRoot: 'Full Access',
                BazaAcl: 'Access to ACL tools',
                BazaSetAcl: 'Update ACL of administrator',
                BazaRegistry: 'Registry',
                BazaRegistryManagement: 'Registry (Edit)',
                BazaAuthMasterPassword: 'Auth Master Password',
                BazaMailCustomerIoLink: 'Customer.IO Link',
            },
        },
        ...bazaAccountAclI18nEn,
        ...bazaInviteCodeAclI18n,
        ...bazaReferralCodesAclI18n,
        ...bazaMailAclI18n,
    },
    baza: {
        ...bazaCommonCmsEnI18n,
        ...bazaCrudEni18n,
        ...bazaAccountI18nEn,
        ...bazaDashboardI18nEn,
        ...bazaFormBuilderCmsEn,
        ...bazaMaintenanceCmsEnI18n,
        ...bazaRegistryCmsEnI18n,
        ...bazaCoreWhitelistAccountCmsEnI18n,
        ...bazaAuthI18nCmsEn,
        ...bazaAuthSessionCmsI18n,
        ...bazaInviteCodeCmsEnI18n,
        ...bazaReferralCodeCmsEnI18n,
        ...bazaDeviceTokenCmsI18n,
        ...bazaMailCmsEnI18n,
        _unhandledError: 'Something went wrong',
        title: 'Baza',
        core: {
            _languages: {
                en: 'English',
                ru: 'Русский',
            },
            _validators: {
                required: 'Field is required',
                min: 'Field must be greater than or equal to {{ min }}',
                max: 'Field must be less than or equal to {{ max }}',
                minlength: 'Field must contains at least {{ requiredLength }} symbols',
                maxlength: 'Field can contains up to {{ requiredLength }} symbols',
                email: 'Field must contains email',
                int: 'Field must contains integer value',
                float: 'Field must contains float value',
                more_than_zero: 'Field must contains positive integer value',
                https_link: 'Field should starts with "https://"',
                password:
                    'Password should be minimum 8 characters long and contain at least 1 upper case letter, 1 lower case letter and 1 special character',
                keyword: 'This field should only contain alphabetic characters, digits, or underscores and cannot begin with a digit',
            },
        },
        cms: {
            auth: {
                components: {
                    bazaCmsAuthSignIn: {
                        fields: {
                            email: 'Email',
                            password: 'Password',
                        },
                        actions: {
                            submit: 'Login',
                            forgotPassword: 'Forgot password?',
                        },
                        errors: {
                            AuthEmailIsNotVerified: 'Email is not verified yet',
                            AuthAccountNotFound: 'Account with given email was not found',
                            AuthInvalidCredentials: 'Invalid password',
                            AuthAdminRoleIsRequired: 'Account has no admin access',
                        },
                        unsupported: 'Unsupported 2FA method',
                    },
                    bazaCmsAuthVerify: {
                        fields: {
                            code: 'Verification Code',
                            otp: 'One Time Password',
                        },
                        actions: {
                            submit: 'Sign In',
                            cancel: 'Back to sign-in form',
                        },
                        errors: {
                            Auth2FAInvalidToken: 'Invalid code',
                            AuthGoogle2FAInvalidToken: 'Invalid OTP',
                        },
                        message: {
                            email: 'Check your inbox and paste Verification Code',
                            'phone-sms': 'Check your and paste Verification Code',
                            'phone-call': 'Verification code will be sent to you via phone call',
                            'google-authenticator': 'Open Google Authenticator on your phone and follow instructions to sign in',
                            'microsoft-authenticator': 'Open Microsoft Authenticator on your phone and follow instructions to sign in',
                        },
                        unsupported: 'Unsupported 2FA method',
                    },
                    bazaCmsAuthMethod: {
                        select: 'Please select verification method:',
                        actions: {
                            cancel: 'Back to sign-in form',
                        },
                        methods: {
                            email: {
                                title: 'Use a verification code from inbox',
                            },
                            'phone-sms': {
                                title: 'Send a verification code with SMS',
                            },
                            'phone-call': {
                                title: 'Phone call with verification code',
                            },
                            'google-authenticator': {
                                title: 'Approve a request on Google Authenticator app',
                            },
                            'microsoft-authenticator': {
                                title: 'Approve a request on Microsoft Authenticator app',
                            },
                        },
                        unsupported: 'Unsupported 2FA method',
                    },
                },
            },
            layout: {
                layouts: {
                    authLayout: {
                        components: {
                            bazaCmsAuthLayout: {
                                title: 'Welcome to the',
                                project: '{{ project }} CMS',
                            },
                        },
                    },
                    cmsLayout: {
                        components: {
                            bazaCmsNavBar: {
                                twoFactorAuthSettings: '2FA Settings',
                                changePassword: 'Change password',
                                signOut: 'Sign out',
                                signOutConfirm: {
                                    nzTitle: 'Sign out',
                                    nzContent: 'Do you really want to sign out from CMS?',
                                    nzOkText: 'Sign out',
                                    nzCancelText: 'Cancel',
                                },
                            },
                        },
                    },
                    formLayout: {
                        actions: {
                            submit: 'Save',
                            cancel: 'Cancel',
                        },
                    },
                    formModal: {
                        actions: {
                            submit: 'Save',
                            cancel: 'Cancel',
                        },
                    },
                },
                shared: {
                    components: {
                        bazaLayoutLang: {
                            select: 'Select language',
                        },
                    },
                },
            },
            account: {
                components: {
                    bazaCmsChangePassword: {
                        title: 'Change password',
                        fields: {
                            oldPassword: 'Current password',
                            newPassword: 'New password',
                            repeatPassword: 'Repeat',
                        },
                        errors: {
                            invalidPassword: 'Invalid password',
                            passwordsAreNotSame: 'Passwords are not same',
                        },
                    },
                    bazaCmsForgotPassword: {
                        fields: {
                            email: 'Email',
                        },
                        actions: {
                            submit: 'Send restore link',
                            cancel: 'Back to sign-in form',
                        },
                        errors: {
                            AccountNotFound: 'Account with given email was not found',
                        },
                        success: 'Check your inbox and follow link to restore password',
                    },
                    bazaCmsResetPassword: {
                        fields: {
                            newPassword: 'New password',
                            repeatPassword: 'Repeat password',
                        },
                        actions: {
                            submit: 'Update password',
                            cancel: 'Back to sign-in form',
                        },
                        errors: {
                            passwordsAreNotSame: 'Passwords are not same',
                        },
                        success: 'Password has been successfully updated',
                        failed: 'Failed to update password',
                        noToken: 'No token provided',
                        used: 'Link is outdated',
                    },
                    bazaCms2FASettings: {
                        title: '2FA Settings',
                        force: 'This 2FA method is enabled by default for your account',
                        info: {
                            secured: {
                                title: 'Your account is secured with Two-Factor authorization.',
                                description: 'You can enable more 2FA methods. CMS will display you options how to authenticate next time.',
                            },
                            unsecured: {
                                title: 'Your account is not secured with Two-Factor authorization!',
                                description: 'We strongly recommend to enable at least one Two-Factor authorization option.',
                            },
                        },
                        methods: {
                            email: {
                                title: 'Email 2FA Confirmation',
                                description: 'Verification codes will be sent to your inbox',
                            },
                            'phone-sms': {
                                title: 'SMS 2FA Confirmation',
                                description: 'Verification codes will be sent via SMS to your phone',
                            },
                            'phone-call': {
                                title: 'Phone Call 2FA Confirmation',
                                description: 'You will receive phone calls with verification code',
                            },
                            'google-authenticator': {
                                title: 'Google Authenticator App',
                                description: 'Install Google Authenticator to your phone and use it to confirm your sign-in requests',
                            },
                            'microsoft-authenticator': {
                                title: 'Microsoft Authenticator App',
                                description: 'Install Microsoft Authenticator to your phone and use it to confirm your sign-in requests',
                            },
                        },
                    },
                    bazaCmsPasswordModal: {
                        title: 'Please enter your password',
                        password: 'Current password',
                        cancel: 'Cancel',
                        submit: 'Submit',
                        invalid: 'Invalid password',
                    },
                },
            },
        },
    },
};
