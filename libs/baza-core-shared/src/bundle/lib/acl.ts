import { AccountAcl } from '../../libs/baza-account/src';
import { BazaAclConfig, BazaAclGroupConfig, BazaCoreACL, configureBazaAcl } from '../../libs/baza-common/src';
import { BazaInviteCodeAcl } from '../../libs/baza-invite-code/src';
import { BazaReferralCodesAcl } from '../../libs/baza-referral-code/src';
import { BazaMailACL } from '../../libs/baza-mail/src';

export function defaultBazaBundleAclGroupsConfig(): Array<BazaAclGroupConfig> {
    return [
        {
            group: BazaCoreACL.Account,
            nodes: Object.values(AccountAcl),
        },
        {
            group: BazaCoreACL.InviteCodes,
            nodes: Object.values(BazaInviteCodeAcl),
        },
        {
            group: BazaCoreACL.ReferralCodes,
            nodes: Object.values(BazaReferralCodesAcl),
        },
        {
            group: BazaCoreACL.Mail,
            nodes: [...Object.values(BazaMailACL)],
        },
    ];
}

export function configureBazaBundleAcl(config: Partial<BazaAclConfig>): void {
    const newConfig: BazaAclConfig = {
        ...config,
        groups: [...defaultBazaBundleAclGroupsConfig(), ...config.groups],
    };

    configureBazaAcl(newConfig);
}
