import { Observable } from 'rxjs';

export enum BazaAwsE2eEndpointPaths {
    exampleDocFile = '/baza-aws/e2e/example-doc',
}

export interface BazaAwsE2eEndpoint {
    exampleDocFile(): Promise<string> | Observable<string>;
}
