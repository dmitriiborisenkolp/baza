export enum BazaAwsErrorCodes {
    BazaAwsFailedToUpload = 'BazaAwsFailedToUpload',
    BazaAwsAccessDefined = 'BazaAwsAccessDefined',
}

export const bazaAwsErrorCodesI18n = {
    [BazaAwsErrorCodes.BazaAwsFailedToUpload]: 'AWS Failed To Upload File',
    [BazaAwsErrorCodes.BazaAwsAccessDefined]: 'AWS Access Denied',
};
