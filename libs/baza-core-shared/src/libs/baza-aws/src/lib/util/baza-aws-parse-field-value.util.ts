import { BAZA_AWS_FIELD_PREFIX } from '../constants/aws.constants';

export function bazaAwsParseFieldValue(s3FieldValue: string): {
    s3ObjectId: string;
} {
    if (s3FieldValue.startsWith(BAZA_AWS_FIELD_PREFIX)) {
        return {
            s3ObjectId: s3FieldValue.slice(BAZA_AWS_FIELD_PREFIX.length + 1),
        };
    } else {
        return {
            s3ObjectId: undefined,
        };
    }
}
