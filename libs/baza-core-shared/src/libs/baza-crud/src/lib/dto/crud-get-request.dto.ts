export class CrudGetRequestDto<PrimaryKey = number> {
    id: PrimaryKey;
}
