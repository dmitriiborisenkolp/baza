import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { IsArray, IsDateString, IsInt, IsOptional, IsPositive, IsString, ValidateNested } from 'class-validator';
import { CrudListSortDto } from './crud-list-sort.dto';
import { Type } from 'class-transformer';

export class CrudListRequestDto<T = any> {
    @ApiModelProperty({
        description: 'Page (from 1 to ...)',
        required: false,
    })
    @IsPositive()
    @IsInt()
    @IsOptional()
    index?: number;

    @ApiModelProperty({
        description: 'Items per page. Provide 0 or negative value to get all entities',
        required: false,
    })
    @IsInt()
    @IsOptional()
    size?: number;

    @ApiModelProperty({
        description: 'Sort order',
        required: false,
    })
    @ValidateNested()
    @Type(() => CrudListSortDto)
    @IsOptional()
    sort?: CrudListSortDto<T>;

    @ApiModelProperty({
        description: 'Search by query string',
        required: false,
    })
    @IsString()
    @IsOptional()
    queryString?: string;

    @ApiModelProperty({
        description: 'Filter records by Date From',
        example: new Date().toISOString(),
        required: false,
    })
    @IsDateString()
    @IsOptional()
    dateFrom?: string;

    @ApiModelProperty({
        description: 'Filter records by Date To',
        example: new Date().toISOString(),
        required: false,
    })
    @IsDateString()
    @IsOptional()
    dateTo?: string;

    @ApiModelProperty({
        description: 'Include only set of fields',
        required: false,
        type: 'string',
        isArray: true,
    })
    @IsString({ each: true })
    @IsArray()
    @IsOptional()
    includeFields?: Array<string>;

    @ApiModelProperty({
        description: 'Exclude set of fields',
        required: false,
        type: 'string',
        isArray: true,
    })
    @IsString({ each: true })
    @IsArray()
    @IsOptional()
    excludeFields?: Array<string>;
}
