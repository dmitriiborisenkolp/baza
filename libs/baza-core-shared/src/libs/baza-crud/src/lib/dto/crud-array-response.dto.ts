import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class CrudArrayResponseDto<T = unknown> {
    @ApiModelProperty({
        isArray: true,
    })
    items: Array<T>;
}
