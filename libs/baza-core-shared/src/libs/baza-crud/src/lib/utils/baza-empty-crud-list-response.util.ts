import { CrudListResponseDto } from '../dto/crud-list-response.dto';

export function bazaEmptyCrudListResponse<T = any>(): CrudListResponseDto<T> {
    return {
        items: [],
        pager: {
            index: 1,
            size: 0,
            total: 0,
        },
        maxSortOrder: 0,
    };
}
