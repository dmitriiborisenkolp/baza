export const BAZA_CRUD_CONSTANTS = {
    /**
     * Default delimiter for import/export CSV files
     */
    csvDefaultDelimiter: ';',
};
