export const bazaCrudRui18n: Record<string, unknown> = {
    components: {
        bazaCrudItems: {
            search: 'Поиск...',
        },
        bazaCrudData: {
            total: {
                '0': 'Нет записей',
                '1': 'Всего 1 запись',
                many: 'Всего {{ total }} записей',
            },
        },
    },
};
