export const bazaCrudEni18n: Record<string, unknown> = {
    crud: {
        components: {
            bazaCrudItems: {
                search: 'Search...',
            },
            bazaCrudData: {
                total: {
                    '0': 'No items',
                    '1': 'Total 1 item',
                    many: 'Total {{ total }} items',
                },
            },
        },
        modals: {
            bazaCrudExportToCsv: {
                title: 'Export to CSV',
                delimiters: {
                    semicolon: 'Export CSV for Microsoft Excel (";" separator)',
                    commas: 'Export CSV for other applications ("," separator)',
                },
                submit: 'Export',
                cancel: 'Cancel',
            },
        },
    },
};
