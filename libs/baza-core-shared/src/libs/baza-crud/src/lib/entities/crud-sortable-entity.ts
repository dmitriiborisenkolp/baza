export interface CrudSortableEntity {
    id: number;
    sortOrder: number;
}
