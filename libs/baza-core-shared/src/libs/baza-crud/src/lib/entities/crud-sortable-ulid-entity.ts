export interface CrudSortableUlidEntity {
    ulid: string;
    sortOrder: number;
}
