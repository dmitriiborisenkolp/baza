import { VersionDto } from '../dto/version.dto';
import { Observable } from 'rxjs';

export enum VersionEndpointPaths {
    version = '/version',
}

export interface VersionEndpoint {
    version(): Promise<VersionDto> | Observable<VersionDto>;
}
