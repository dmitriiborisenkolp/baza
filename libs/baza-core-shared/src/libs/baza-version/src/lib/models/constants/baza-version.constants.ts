export const BAZA_VERSION_DEFAULT_MESSAGE = 'This version of {{ project }} is no longer supported. Please update to the latest version of the application to continue using it.';
export const BAZA_VERSION_DEFAULT_MESSAGE_REGISTRY_KEY = 'bazaCoreVersion.message';
