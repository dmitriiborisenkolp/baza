import { ApiEvent, ApiEventPayload } from './api-event';

export enum BazaEventBusKafkaTopic {
    ApiEvent = 'ApiEvent',
}

export const BazaEventBusKafkaTopics: Array<string> = [
    BazaEventBusKafkaTopic.ApiEvent,
];

export type BazaEventBusKafkaMessage<TOPIC, PAYLOAD extends ApiEvent<TOPIC>> =
    { topic: BazaEventBusKafkaTopic.ApiEvent, payload: ApiEventPayload<TOPIC, PAYLOAD> }
;
