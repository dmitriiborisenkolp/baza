export type API_EVENT_ID = string;

export enum ApiEventSource {
    CurrentNode = 'CurrentNode',
    EveryNode = 'EveryNode',
}

export interface ApiEvent<TOPIC> {
    topic: TOPIC;
    payload: unknown;
}

export class ApiEventPayload<TOPIC, PAYLOAD extends ApiEvent<TOPIC>> {
    id: API_EVENT_ID;
    source: ApiEventSource;
    event: PAYLOAD;
}

export class ApiCqrsEvent<TOPIC, PAYLOAD extends ApiEvent<TOPIC>> {
    constructor(public apiEvent: ApiEventPayload<TOPIC, PAYLOAD>) {}
}
