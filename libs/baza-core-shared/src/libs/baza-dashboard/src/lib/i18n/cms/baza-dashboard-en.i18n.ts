export const bazaDashboardI18nEn: Record<string, unknown> = {
    dashboard: {
        title: 'Dashboard',
        components: {
            bazaDashboard: {
                welcome: 'Welcome to the {{ project }} CMS',
                version: 'API version {{ projectCodeName }} {{ version }}',
            },
        },
    },
};
