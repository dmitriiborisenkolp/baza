export const bazaInviteCodeCmsEnI18n = {
    inviteCodes: {
        title: 'Invite Codes',
        components: {
            bazaInviteCodes: {
                title: 'Invite Codes',
                columns: {
                    code: 'Code',
                    isConsumed: 'Is consumed?',
                },
                actions: {
                    add: 'Add new invite code',
                    edit: 'Edit invite code',
                    delete: {
                        title: 'Delete invite code',
                        confirm: 'Do you really want to delete invite code?',
                    },
                },
                form: {
                    fields: {
                        code: 'Code',
                    },
                    placeholders: {
                        code: 'Unique invite code',
                    },
                },
            },
        },
    },
};
