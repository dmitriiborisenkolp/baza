export const bazaInviteCodeApiEnI18n = {
    inviteCodeRequest: {
        subject: 'Invite code request for {{ email }}',
    },
}
