export enum BazaInviteCodeErrorCodes {
    BazaInviteCodeIsInvalid = 'BazaInviteCodeIsInvalid',
    BazaInviteCodeIsAlreadyCompletelyConsumed = 'BazaInviteCodeIsAlreadyCompletelyConsumed',
    BazaRequestForInviteCodesAlreadySent = 'BazaRequestForInviteCodesAlreadySent',
    BazaInviteCodeUserIsAlreadyRegistered = 'BazaInviteCodeUserIsAlreadyRegistered',
    BazaInviteCodeWithGivenCodeAlreadyExists = 'BazaInviteCodeWithGivenCodeAlreadyExists',
    BazaInviteCodeOwnerNotFound = 'BazaInviteCodeOwnerNotFound',
}
