export enum BazaInviteCodeAcl {
    BazaInviteCodes = 'BazaInviteCodes',
    BazaInviteCodesManagement = 'BazaInviteCodesManagement',
}

export const bazaInviteCodeAclI18n = {
    'invite-codes': {
        group: 'Invite Codes',
        nodes: {
            [BazaInviteCodeAcl.BazaInviteCodes]: 'Invite codes',
            [BazaInviteCodeAcl.BazaInviteCodesManagement]: 'Invite codes management',
        },
    },
};
