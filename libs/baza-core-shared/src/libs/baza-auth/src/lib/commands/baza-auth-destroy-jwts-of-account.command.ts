export class BazaAuthDestroyJwtsOfAccountCommand {
    constructor(
        public request: {
            accountId: number;
        }
    ) {
    }
}
