export enum Auth2FAErrorCodes {
    Auth2FARequired = 'Auth2FARequired',
    Auth2FAUnknownVerificationMethod = 'Auth2FAUnknownVerificationMethod',
    Auth2FAInvalidToken = 'Auth2FAInvalidToken',
    AuthGoogle2FAInvalidToken = 'AuthGoogle2FAInvalidToken',
    AuthGoogle2FANotEnabled = 'AuthGoogle2FANotEnabled'
}

export const auth2FAErrorCodesI18nEn = {
    [Auth2FAErrorCodes.Auth2FARequired]: 'Two-factor authorization is required',
    [Auth2FAErrorCodes.Auth2FAUnknownVerificationMethod]: 'Unknown Two-factor verification method',
    [Auth2FAErrorCodes.Auth2FAInvalidToken]: 'Invalid code',
    [Auth2FAErrorCodes.AuthGoogle2FAInvalidToken]: 'Invalid OTP',
    [Auth2FAErrorCodes.AuthGoogle2FANotEnabled]: 'Google authorization is not enabled',
};
