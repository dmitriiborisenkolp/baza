import { AccountRole } from '../../../../baza-account/src';

export interface AccessTokenExpiresSettings {
    role: AccountRole;
    expiresIn: number;
}

export type AccessTokenExpiresSettingsMany = Array<AccessTokenExpiresSettings>;

export interface RefreshTokenExpiresSettings {
    role: AccountRole;
    expiresIn: number;
}

export type RefreshTokenExpiresSettingsMany = Array<RefreshTokenExpiresSettings>;
