export enum Auth2FAVerificationMethod {
    Email = 'email',
    PhoneSMS = 'phone-sms',
    PhoneCall = 'phone-call',
    GoogleAuthenticator = 'google-authenticator',
    MicrosoftAuthenticator = 'microsoft-authenticator',
}
