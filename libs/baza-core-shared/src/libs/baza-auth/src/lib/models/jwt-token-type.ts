export enum JwtTokenType {
    AccessToken = 'AccessToken',
    RefreshToken = 'RefreshToken',
}
