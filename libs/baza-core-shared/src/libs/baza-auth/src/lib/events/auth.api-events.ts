import { Application } from '../../../../baza-common/src';
import { AccountSettingsDto } from '../../../../baza-account/src';
import { JwtPayload } from '../dto/jwt-payload.dto';
import { AuthErrorCodes } from '../error-codes/auth.error-codes';

export enum AuthApiEvent {
    BazaAuth = 'BazaAuth',
    BazaAuthFailed = 'BazaAuthFailed',
    BazaAuth2FA = 'BazaAuth2FA', // User requested to send email with 2FA Verification code
    BazaAuth2FAVerified = 'BazaAuth2FAVerified', // User successfully confirmed Auth attempt with Verification code
    BazaAuth2FAFailed = 'BazaAuth2FAFailed', // User failed to confirm Auth attempt with invalid Verification code.
    BazaAuthVerifySuccess = 'BazaVerifySuccess',
    BazaAuthVerifyFailed = 'BazaVerifyFailed',
    BazaAuthInvalidatedAllTokens = 'BazaAuthInvalidatedAllTokens',
    BazaAuthInvalidatedToken = 'BazaAuthInvalidatedToken',
    BazaAuthInvalidatedRefreshToken = 'BazaAuthInvalidatedRefreshToken',
    BazaAuthAccessTokenAttached = 'BazaAuthAccessTokenAttached',
}

export type AuthApiEvents =
    | BazaAuthEvent
    | BazaAuthFailedEvent
    | BazaAuth2FAEvent
    | BazaAuth2FAVerifiedEvent
    | BazaAuth2FAFailedEvent
    | BazaAuthVerifySuccessEvent
    | BazaAuthVerifyFailedEvent
    | BazaAuthInvalidatedAllTokensEvent
    | BazaAuthInvalidatedTokenEvent
    | BazaAuthInvalidatedRefreshTokenEvent
    | BazaAuthAccessTokenAttachedEvent;

export type BazaAuthEventPayload<T = AccountSettingsDto> = {
    jwt: string;
    jwtPayload: JwtPayload;
    accountId: number;
    accountSettings: T;
    ip: string;
    userAgent: string;
};
export type BazaAuthFailedEventPayload = { errorCode: AuthErrorCodes; accountId?: number; ip?: string; userAgent?: string };
export type BazaAuth2FAEventPayload = { accountId: number; accountEmail: string; accountFullName: string };
export type BazaAuth2FAVerifiedEventPayload = { accountId: number; accountEmail: string; accountFullName: string };
export type BazaAuth2FAFailedEventPayload = { errorCode: AuthErrorCodes; accountId?: number; ip?: string; userAgent?: string };
export type BazaAuthVerifySuccessEventPayload = { jwt: string; accountId: number; accountEmail: string };
export type BazaAuthVerifyFailedEventPayload = { jwt: string; errorCode: AuthErrorCodes };
export type BazaAuthInvalidatedAllTokensEventPayload = { accountId: number; accountEmail: string };
export type BazaAuthInvalidatedTokenEventPayload = { jwt: string };
export type BazaAuthInvalidatedRefreshTokenEventPayload = { jwt: string };
export type BazaAuthAccessTokenAttachedPayload = { application: Application; jwt: string; accountId: number; accountEmail: string };

export interface BazaAuthEvent {
    topic: AuthApiEvent.BazaAuth;
    payload: BazaAuthEventPayload;
}
export interface BazaAuthFailedEvent {
    topic: AuthApiEvent.BazaAuthFailed;
    payload: BazaAuthFailedEventPayload;
}
export interface BazaAuth2FAEvent {
    topic: AuthApiEvent.BazaAuth2FA;
    payload: BazaAuth2FAEventPayload;
}
export interface BazaAuth2FAVerifiedEvent {
    topic: AuthApiEvent.BazaAuth2FAVerified;
    payload: BazaAuth2FAVerifiedEventPayload;
}
export interface BazaAuth2FAFailedEvent {
    topic: AuthApiEvent.BazaAuth2FAFailed;
    payload: BazaAuth2FAFailedEventPayload;
}
export interface BazaAuthVerifySuccessEvent {
    topic: AuthApiEvent.BazaAuthVerifySuccess;
    payload: BazaAuthVerifySuccessEventPayload;
}
export interface BazaAuthVerifyFailedEvent {
    topic: AuthApiEvent.BazaAuthVerifyFailed;
    payload: BazaAuthVerifyFailedEventPayload;
}
export interface BazaAuthInvalidatedAllTokensEvent {
    topic: AuthApiEvent.BazaAuthInvalidatedAllTokens;
    payload: BazaAuthInvalidatedAllTokensEventPayload;
}
export interface BazaAuthInvalidatedTokenEvent {
    topic: AuthApiEvent.BazaAuthInvalidatedToken;
    payload: BazaAuthInvalidatedTokenEventPayload;
}
export interface BazaAuthInvalidatedRefreshTokenEvent {
    topic: AuthApiEvent.BazaAuthInvalidatedRefreshToken;
    payload: BazaAuthInvalidatedRefreshTokenEventPayload;
}
export interface BazaAuthAccessTokenAttachedEvent {
    topic: AuthApiEvent.BazaAuthAccessTokenAttached;
    payload: BazaAuthAccessTokenAttachedPayload;
}
