export const bazaAuthI18nCmsEn: Record<string, unknown> = {
    auth: {
        masterPassword: {
            title: 'Master Password',
            components: {
                bazaAuthCmsMasterPassword: {
                    title: 'Master Password',
                    description:
                        'Master Password allows you to sign in with any non-administrator account. Please use it only for testing reasons and disable Master Password after. Master Password will be automatically disabled after 1 hour.',
                    enable: 'Enable Master Password',
                    disable: 'Disable Master Password',
                    current: 'Current Master Password (Automatically updates every 2 minutes)',
                    show: 'Show',
                    hide: 'Hide',
                },
            },
        },
    },
};
