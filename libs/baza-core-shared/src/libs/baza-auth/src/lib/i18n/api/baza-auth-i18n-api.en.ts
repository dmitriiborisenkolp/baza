export const bazaAuthI18nApiEn = {
    'two-factor-auth': {
        'subject': '{{ clientName }}: Confirm Your Sign-in Request',
    },
};
