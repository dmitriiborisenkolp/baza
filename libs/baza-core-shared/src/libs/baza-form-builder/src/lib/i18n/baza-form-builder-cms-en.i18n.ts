export const bazaFormBuilderCmsEn: Record<string, unknown> = {
    formBuilder: {
        components: {
            bazaFieldUpload: {
                dragFile: 'Drag file here',
                dragImage: 'Drag image here',
                dragVideo: 'Drag video here',
                dragDocument: 'Drag document here',
                failed: 'Failed to upload file',
                replaceFile: 'Replace file',
                deleteFile: 'Delete',
                maxFileSizeError: 'Uploaded file should be less than {{ maxSizeMb }}MB in size',
                fileTypeError: 'File does not meet file type requirements',
                cropper: {
                    crop: 'Crop Image',
                },
                queue: {
                    modal: {
                        title: 'Upload Manager',
                        upload: 'Upload Files',
                        cancel: 'Cancel',
                        save: 'Save',
                        stop: 'Stop Uploading Files',
                        tooltip: {
                            retry: 'Retry Upload',
                            exclude: 'Remove File from List',
                            crop: 'Crop Image',
                        },
                    },
                },
            },
            bazaFieldSeo: {
                addMeta: 'Add meta',
                tabs: {
                    common: 'General',
                    meta: 'Meta',
                    card: 'Card for social networks',
                },
                fields: {
                    title: 'Title',
                    urn: 'URN',
                    meta: {
                        name: 'Name',
                        contents: 'Contents',
                    },
                    card: {
                        title: 'Title',
                        description: 'Text',
                        url: 'URL',
                        coverImageUrl: 'Cover',
                    },
                },
                placeholders: {
                    title: 'HTML HEAD Title',
                    urn: 'SEO Url',
                    card: {
                        title: 'Title for card',
                        description: 'Text inside card',
                        url: 'Full URL to page with hostname',
                    },
                },
            },
            bazaFieldAccount: {
                searchModal: {
                    title: 'Accounts',
                    cancel: 'Cancel',
                },
            },
            bazaFieldMailRecipient: {
                name: 'Name',
                email: 'Email',
                to: 'TO',
                cc: 'CC',
                addCC: 'Add CC',
            },
            bazaFieldNetworks: {
                addNetwork: 'Add Network',
                networks: 'Networks',
                userName: 'Network Username',
            },
            bazaFieldUploadMulti: {
                add: 'Select Files',
                remove: 'Remove',
                failed: 'Failed to upload file.',
                maxMultiFileUploadLimitError: 'Only {{ maxMultiFileUploadLimit }} file uploads are supported at a time.',
            },
            bazaFieldUploadAttachmentMulti: {
                add: 'Add File',
                remove: 'Remove',
            },
            bazaFieldKeyValue: {
                key: 'Key',
                value: 'Value',
                add: 'Add Entry',
            },
        },
    },
};
