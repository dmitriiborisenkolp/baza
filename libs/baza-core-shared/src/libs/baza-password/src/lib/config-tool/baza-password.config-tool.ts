import {
    BAZA_PASSWORD_VALIDATOR_INJECTS,
    BazaPasswordMandatory,
    BazaPasswordValidator,
    bazaPasswordValidatorFactory,
    BazaPasswordValidatorFunc,
    BazaPasswordValidatorInjects,
} from '../models/baza-password-validator';

/**
 * Baza Password configuration tool
 * Allows to update configuration for password validation
 */
export function bazaPasswordConfigTool(): BazaPasswordConfigTool {
    return new BazaPasswordConfigTool();
}

/**
 * Password Configuration tool (private, non-exported)
 */
class BazaPasswordConfigTool {
    /**
     * Returns options for Baza Password validations
     * Changing values in returned object will not affects; use `setOption` to update options
     */
    getOptions(): BazaPasswordValidatorInjects {
        return {
            ...BAZA_PASSWORD_VALIDATOR_INJECTS,
        };
    }

    /**
     * Updates options for Baza Password validations
     * @param partial
     */
    setOptions(partial: Partial<BazaPasswordValidatorInjects>): this {
        Object.assign(BAZA_PASSWORD_VALIDATOR_INJECTS, partial);

        return this;
    }

    setMandatoryLevel(type: BazaPasswordValidator | string, mandatory: BazaPasswordMandatory): this {
        const validator = BAZA_PASSWORD_VALIDATOR_INJECTS.validators.find((next) => next.type === type);

        if (!validator) {
            throw new Error(`Validator with type "${type}" was not found`);
        }

        validator.mandatory = mandatory;

        return this;
    }

    /**
     * Enables default validator with specified type
     * @param type
     */
    addDefaultValidator(type: BazaPasswordValidator): this {
        this.setValidator(bazaPasswordValidatorFactory(type));

        return this;
    }

    /**
     * Set validator
     * Duplicate validators will not be added
     * @param validator
     */
    setValidator(validator: BazaPasswordValidatorFunc): this {
        this.removeValidator(validator.type);

        BAZA_PASSWORD_VALIDATOR_INJECTS.validators.push(validator);

        return this;
    }

    /**
     * Removes validator
     * @param type
     */
    removeValidator(type: BazaPasswordValidator | string): this {
        BAZA_PASSWORD_VALIDATOR_INJECTS.validators = BAZA_PASSWORD_VALIDATOR_INJECTS.validators.filter((next) => next.type !== type);

        return this;
    }
}
