import { BAZA_PASSWORD_VALIDATOR_INJECTS, BazaPasswordValidator } from './baza-password-validator';

type TestCase = [string, boolean]; // Input string, Result

describe('@scaliolabs/baza-core-shared/baza-password/models/baza-password-validator.spec.ts', () => {
    const alphabetUpperCase = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const alphabetLowerCase = 'abcdefghijklmnopqrstuvwxyz';
    const digits = '0123456789';
    const special = '!?@#$%^&*()_+=-[]{}\\|/.<>,~`\'"';

    it('will correctly validate min length', () => {
        const item = BAZA_PASSWORD_VALIDATOR_INJECTS.validators.find((next) => next.type === BazaPasswordValidator.BazaMinCharacters);

        // We should not trim symbols in validator - unless it will be possible to save password with spaces
        const tests: Array<TestCase> = [
            ['', false],
            ['a', false],
            ['aa', false],
            ['aaa', false],
            ['aaaaaaaa', true],
            ['aaaaaaa ', true],
            [' aaaaaaa', true],
            [' aaaaaaa   ', true],
        ];

        for (const testCase of tests) {
            expect(item.validator(testCase[0])).toBe(testCase[1]);
        }
    });

    it('will correctly validate max length', () => {
        const item = BAZA_PASSWORD_VALIDATOR_INJECTS.validators.find((next) => next.type === BazaPasswordValidator.BazaMaxCharacters);
        const base = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

        const tests: Array<TestCase> = [
            [base, true],
            [`${base}`, true],
            [`${base}a`, false],
            [`${base} `, false],
            [` ${base}`, false],
            [` ${base} `, false],
            [`  ${base}  `, false],
            ['aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', true],
            ['aaaa', true],
            [base, true],
            [base, true],
            [base, true],
        ];

        for (const testCase of tests) {
            expect(item.validator(testCase[0])).toBe(testCase[1]);
        }
    });

    it('will correctly detect uppercase', () => {
        const item = BAZA_PASSWORD_VALIDATOR_INJECTS.validators.find((next) => next.type === BazaPasswordValidator.Baza1UppercaseCharacter);

        const tests: Array<TestCase> = [
            ['A', true],
            ['a', false],
            ['aaa', false],
            ['AA', true],
            ['AaA', true],
        ];

        for (const testCase of tests) {
            expect(item.validator(testCase[0])).toBe(testCase[1]);
        }

        for (const symbol of alphabetUpperCase.split('')) {
            expect(item.validator(symbol)).toBeTruthy();
        }

        for (const symbol of alphabetLowerCase.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of digits.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of special.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }
    });

    it('will correctly detect lowercase', () => {
        const item = BAZA_PASSWORD_VALIDATOR_INJECTS.validators.find((next) => next.type === BazaPasswordValidator.Baza1LowercaseCharacter);

        const tests: Array<TestCase> = [
            ['a', true],
            ['A', false],
            ['AAA', false],
            ['aa', true],
            ['aAa', true],
        ];

        for (const testCase of tests) {
            expect(item.validator(testCase[0])).toBe(testCase[1]);
        }

        for (const symbol of alphabetUpperCase.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of alphabetLowerCase.split('')) {
            expect(item.validator(symbol)).toBeTruthy();
        }

        for (const symbol of digits.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of special.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }
    });

    it('will correctly detect digits', () => {
        const item = BAZA_PASSWORD_VALIDATOR_INJECTS.validators.find((next) => next.type === BazaPasswordValidator.Baza1Number);

        const tests: Array<TestCase> = [
            ['0', true],
            ['1', true],
            ['A', false],
            ['AAA', false],
            ['aa', false],
            ['aAa', false],
            ['a1Aa', true],
            ['0aAA', true],
            ['10aAA', true],
            ['a123AA', true],
        ];

        for (const testCase of tests) {
            expect(item.validator(testCase[0])).toBe(testCase[1]);
        }

        for (const symbol of alphabetUpperCase.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of alphabetLowerCase.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of digits.split('')) {
            expect(item.validator(symbol)).toBeTruthy();
        }

        for (const symbol of special.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }
    });

    it('will correctly detect special symbols', () => {
        const item = BAZA_PASSWORD_VALIDATOR_INJECTS.validators.find((next) => next.type === BazaPasswordValidator.Baza1SpecialCharacter);

        const tests: Array<TestCase> = [
            ['$', true],
            ['?', true],
            ['!', true],
            ["'", true],
            ['A', false],
            ['A1AA', false],
            ['aa', false],
            ['a2a', false],
            ['aAa', false],
            ['aAa2', false],
            ['a!Aa', true],
            ['?aAA', true],
            ['!?aAA', true],
            ['a12?3A??A', true],
        ];

        for (const testCase of tests) {
            expect(item.validator(testCase[0])).toBe(testCase[1]);
        }

        for (const symbol of alphabetUpperCase.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of alphabetLowerCase.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of digits.split('')) {
            expect(item.validator(symbol)).toBeFalsy();
        }

        for (const symbol of special.split('')) {
            expect(item.validator(symbol)).toBeTruthy();
        }
    });
});
