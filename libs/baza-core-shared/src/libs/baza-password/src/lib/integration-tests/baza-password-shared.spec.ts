import 'reflect-metadata';
import { bazaValidatePasswordResources } from '../util/baza-validate-password-resources.util';
import { bazaValidatePassword } from '../util/baza-validate-password.util';

describe('@scaliolabs/baza-core-shared/integration-tests/baza-password-shared.spec.ts', () => {
    it('returns responses correctly', async () => {
        const response = bazaValidatePasswordResources();

        expect(response).toEqual({
            minLength: 8,
            maxLength: 32,
            validators: [
                {
                    type: '{{ minPasswordLength }} characters',
                    label: '8 characters',
                },
                {
                    type: '{{ maxPasswordLength }} characters',
                    label: '32 characters',
                },
                {
                    type: '1 uppercase letter',
                    label: '1 uppercase letter',
                },
                {
                    type: '1 lowercase letter',
                    label: '1 lowercase letter',
                },
                {
                    type: '1 number',
                    label: '1 number',
                },
                {
                    type: '1 special character',
                    label: '1 special character',
                },
            ],
        });
    });

    it('correctly validates Scalio#1337! password', async () => {
        const response = bazaValidatePassword('Scalio#1337!');

        expect(response).toEqual({
            isValid: true,
            difficult: 'hard',
            failed: [],
            passed: [
                {
                    type: '{{ minPasswordLength }} characters',
                    label: '8 characters',
                },
                {
                    type: '{{ maxPasswordLength }} characters',
                    label: '32 characters',
                },
                {
                    type: '1 uppercase letter',
                    label: '1 uppercase letter',
                },
                {
                    type: '1 lowercase letter',
                    label: '1 lowercase letter',
                },
                {
                    type: '1 number',
                    label: '1 number',
                },
                {
                    type: '1 special character',
                    label: '1 special character',
                },
            ],
        });
    });

    it('correctly disallow 123 password', async () => {
        const response = bazaValidatePassword('123');

        expect(response.isValid).toBeFalsy();
        expect(response).toEqual({
            isValid: false,
            difficult: 'easy',
            failed: [
                {
                    type: '{{ minPasswordLength }} characters',
                    label: '8 characters',
                },
                {
                    type: '1 uppercase letter',
                    label: '1 uppercase letter',
                },
                {
                    type: '1 lowercase letter',
                    label: '1 lowercase letter',
                },
                {
                    type: '1 special character',
                    label: '1 special character',
                },
            ],
            passed: [
                {
                    type: '{{ maxPasswordLength }} characters',
                    label: '32 characters',
                },
                {
                    type: '1 number',
                    label: '1 number',
                },
            ],
        });
    });

    it('correctly disallow 1234567890 password', async () => {
        const response = bazaValidatePassword('1234567890');

        expect(response.isValid).toBeFalsy();
        expect(response).toEqual({
            isValid: false,
            difficult: 'easy',
            failed: [
                {
                    type: '1 uppercase letter',
                    label: '1 uppercase letter',
                },
                {
                    type: '1 lowercase letter',
                    label: '1 lowercase letter',
                },
                {
                    type: '1 special character',
                    label: '1 special character',
                },
            ],
            passed: [
                {
                    type: '{{ minPasswordLength }} characters',
                    label: '8 characters',
                },
                {
                    type: '{{ maxPasswordLength }} characters',
                    label: '32 characters',
                },
                {
                    type: '1 number',
                    label: '1 number',
                },
            ],
        });
    });

    it('correctly disallow aA1234567890 password', async () => {
        const response = bazaValidatePassword('aA1234567890');

        expect(response.isValid).toBeFalsy();
        expect(response).toEqual({
            isValid: false,
            difficult: 'easy',
            failed: [
                {
                    type: '1 special character',
                    label: '1 special character',
                },
            ],
            passed: [
                {
                    type: '{{ minPasswordLength }} characters',
                    label: '8 characters',
                },
                {
                    type: '{{ maxPasswordLength }} characters',
                    label: '32 characters',
                },
                {
                    type: '1 uppercase letter',
                    label: '1 uppercase letter',
                },
                {
                    type: '1 lowercase letter',
                    label: '1 lowercase letter',
                },
                {
                    type: '1 number',
                    label: '1 number',
                },
            ],
        });
    });

    it('correctly disallow aA1234567890 password', async () => {
        const response = bazaValidatePassword('aA1234567890');

        expect(response.isValid).toBeFalsy();
        expect(response).toEqual({
            isValid: false,
            difficult: 'easy',
            failed: [
                {
                    type: '1 special character',
                    label: '1 special character',
                },
            ],
            passed: [
                {
                    type: '{{ minPasswordLength }} characters',
                    label: '8 characters',
                },
                {
                    type: '{{ maxPasswordLength }} characters',
                    label: '32 characters',
                },
                {
                    type: '1 uppercase letter',
                    label: '1 uppercase letter',
                },
                {
                    type: '1 lowercase letter',
                    label: '1 lowercase letter',
                },
                {
                    type: '1 number',
                    label: '1 number',
                },
            ],
        });
    });

    it('correctly allow aA1234567890! password', async () => {
        const response = bazaValidatePassword('aA1234567890!');

        expect(response.isValid).toBeTruthy();
        expect(response).toEqual({
            isValid: true,
            difficult: 'hard',
            failed: [],
            passed: [
                {
                    type: '{{ minPasswordLength }} characters',
                    label: '8 characters',
                },
                {
                    type: '{{ maxPasswordLength }} characters',
                    label: '32 characters',
                },
                {
                    type: '1 uppercase letter',
                    label: '1 uppercase letter',
                },
                {
                    type: '1 lowercase letter',
                    label: '1 lowercase letter',
                },
                {
                    type: '1 number',
                    label: '1 number',
                },
                {
                    type: '1 special character',
                    label: '1 special character',
                },
            ],
        });
    });
});
