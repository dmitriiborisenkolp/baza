import { BazaPasswordValidator, BAZA_PASSWORD_VALIDATOR_INJECTS } from '../models/baza-password-validator';
import { BazaPasswordResourcesDto } from '../dto/baza-password-resources.dto';
import { replaceTags } from '../../../../baza-common/src';

export function bazaValidatePasswordResources(): BazaPasswordResourcesDto {
    return {
        minLength: BAZA_PASSWORD_VALIDATOR_INJECTS.minPasswordLength,
        maxLength: BAZA_PASSWORD_VALIDATOR_INJECTS.maxPasswordLength,
        validators: Object.values(BazaPasswordValidator).map((v) => ({
            type: v,
            label: replaceTags(v, BAZA_PASSWORD_VALIDATOR_INJECTS),
        })),
    };
}
