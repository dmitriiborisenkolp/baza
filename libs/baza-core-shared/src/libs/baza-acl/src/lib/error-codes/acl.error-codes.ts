export enum AclErrorCodes {
    AclDenied = 'AclDenied',
    AclNoDefaultsAvailable = 'AclNoDefaultsAvailable',
    AclNotFound = 'AclNotFound',
    AclOutOfScope = 'AclOutOfScope',
}
