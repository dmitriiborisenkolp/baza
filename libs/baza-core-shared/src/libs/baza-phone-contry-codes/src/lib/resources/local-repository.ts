import { PhoneCountryCodesRepositoryDto } from '../dto/phone-country-codes-repository.dto';

export const phoneCountryCodesLocalRepository: PhoneCountryCodesRepositoryDto = [{
    'numericCode': '004',
    'phoneCode': '93',
    'countryName': 'Afghanistan',
    'countryCode': 'AF',
    'unicode': 'U+1F1E6 U+1F1EB'
}, {
    'numericCode': '248',
    'phoneCode': '358',
    'countryName': 'Aland Islands',
    'countryCode': 'AX',
    'unicode': 'U+1F1E6 U+1F1FD'
}, {
    'numericCode': '008',
    'phoneCode': '355',
    'countryName': 'Albania',
    'countryCode': 'AL',
    'unicode': 'U+1F1E6 U+1F1F1'
}, {
    'numericCode': '012',
    'phoneCode': '213',
    'countryName': 'Algeria',
    'countryCode': 'DZ',
    'unicode': 'U+1F1E9 U+1F1FF'
}, {
    'numericCode': '016',
    'phoneCode': '1',
    'countryName': 'American Samoa',
    'countryCode': 'AS',
    'unicode': 'U+1F1E6 U+1F1F8'
}, {
    'numericCode': '020',
    'phoneCode': '376',
    'countryName': 'Andorra',
    'countryCode': 'AD',
    'unicode': 'U+1F1E6 U+1F1E9'
}, {
    'numericCode': '024',
    'phoneCode': '244',
    'countryName': 'Angola',
    'countryCode': 'AO',
    'unicode': 'U+1F1E6 U+1F1F4'
}, {
    'numericCode': '660',
    'phoneCode': '1',
    'countryName': 'Anguilla',
    'countryCode': 'AI',
    'unicode': 'U+1F1E6 U+1F1EE'
}, {
    'numericCode': '028',
    'phoneCode': '1',
    'countryName': 'Antigua and Barbuda',
    'countryCode': 'AG',
    'unicode': 'U+1F1E6 U+1F1EC'
}, {
    'numericCode': '032',
    'phoneCode': '54',
    'countryName': 'Argentina',
    'countryCode': 'AR',
    'unicode': 'U+1F1E6 U+1F1F7'
}, {
    'numericCode': '051',
    'phoneCode': '374',
    'countryName': 'Armenia',
    'countryCode': 'AM',
    'unicode': 'U+1F1E6 U+1F1F2'
}, {
    'numericCode': '533',
    'phoneCode': '297',
    'countryName': 'Aruba',
    'countryCode': 'AW',
    'unicode': 'U+1F1E6 U+1F1FC'
}, {
    'numericCode': '036',
    'phoneCode': '61',
    'countryName': 'Australia',
    'countryCode': 'AU',
    'unicode': 'U+1F1E6 U+1F1FA'
}, {
    'numericCode': '040',
    'phoneCode': '43',
    'countryName': 'Austria',
    'countryCode': 'AT',
    'unicode': 'U+1F1E6 U+1F1F9'
}, {
    'numericCode': '031',
    'phoneCode': '994',
    'countryName': 'Azerbaijan',
    'countryCode': 'AZ',
    'unicode': 'U+1F1E6 U+1F1FF'
}, {
    'numericCode': '044',
    'phoneCode': '1',
    'countryName': 'Bahamas',
    'countryCode': 'BS',
    'unicode': 'U+1F1E7 U+1F1F8'
}, {
    'numericCode': '048',
    'phoneCode': '973',
    'countryName': 'Bahrain',
    'countryCode': 'BH',
    'unicode': 'U+1F1E7 U+1F1ED'
}, {
    'numericCode': '050',
    'phoneCode': '880',
    'countryName': 'Bangladesh',
    'countryCode': 'BD',
    'unicode': 'U+1F1E7 U+1F1E9'
}, {
    'numericCode': '052',
    'phoneCode': '1',
    'countryName': 'Barbados',
    'countryCode': 'BB',
    'unicode': 'U+1F1E7 U+1F1E7'
}, {
    'numericCode': '112',
    'phoneCode': '375',
    'countryName': 'Belarus',
    'countryCode': 'BY',
    'unicode': 'U+1F1E7 U+1F1FE'
}, {
    'numericCode': '056',
    'phoneCode': '32',
    'countryName': 'Belgium',
    'countryCode': 'BE',
    'unicode': 'U+1F1E7 U+1F1EA'
}, {
    'numericCode': '084',
    'phoneCode': '501',
    'countryName': 'Belize',
    'countryCode': 'BZ',
    'unicode': 'U+1F1E7 U+1F1FF'
}, {
    'numericCode': '204',
    'phoneCode': '229',
    'countryName': 'Benin',
    'countryCode': 'BJ',
    'unicode': 'U+1F1E7 U+1F1EF'
}, {
    'numericCode': '060',
    'phoneCode': '1',
    'countryName': 'Bermuda',
    'countryCode': 'BM',
    'unicode': 'U+1F1E7 U+1F1F2'
}, {
    'numericCode': '064',
    'phoneCode': '975',
    'countryName': 'Bhutan',
    'countryCode': 'BT',
    'unicode': 'U+1F1E7 U+1F1F9'
}, {
    'numericCode': '068',
    'phoneCode': '591',
    'countryName': 'Bolivia',
    'countryCode': 'BO',
    'unicode': 'U+1F1E7 U+1F1F4'
}, {
    'numericCode': '535',
    'phoneCode': '599',
    'countryName': 'Bonaire, Sint Eustatius and Saba',
    'countryCode': 'BQ',
    'unicode': 'U+1F1E7 U+1F1F6'
}, {
    'numericCode': '070',
    'phoneCode': '387',
    'countryName': 'Bosnia and Herzegovina',
    'countryCode': 'BA',
    'unicode': 'U+1F1E7 U+1F1E6'
}, {
    'numericCode': '072',
    'phoneCode': '267',
    'countryName': 'Botswana',
    'countryCode': 'BW',
    'unicode': 'U+1F1E7 U+1F1FC'
}, {
    'numericCode': '076',
    'phoneCode': '55',
    'countryName': 'Brazil',
    'countryCode': 'BR',
    'unicode': 'U+1F1E7 U+1F1F7'
}, {
    'numericCode': '086',
    'phoneCode': '246',
    'countryName': 'British Indian Ocean Territory',
    'countryCode': 'IO',
    'unicode': 'U+1F1EE U+1F1F4'
}, {
    'numericCode': '096',
    'phoneCode': '673',
    'countryName': 'Brunei Darussalam',
    'countryCode': 'BN',
    'unicode': 'U+1F1E7 U+1F1F3'
}, {
    'numericCode': '100',
    'phoneCode': '359',
    'countryName': 'Bulgaria',
    'countryCode': 'BG',
    'unicode': 'U+1F1E7 U+1F1EC'
}, {
    'numericCode': '854',
    'phoneCode': '226',
    'countryName': 'Burkina Faso',
    'countryCode': 'BF',
    'unicode': 'U+1F1E7 U+1F1EB'
}, {
    'numericCode': '108',
    'phoneCode': '257',
    'countryName': 'Burundi',
    'countryCode': 'BI',
    'unicode': 'U+1F1E7 U+1F1EE'
}, {
    'numericCode': '116',
    'phoneCode': '855',
    'countryName': 'Cambodia',
    'countryCode': 'KH',
    'unicode': 'U+1F1F0 U+1F1ED'
}, {
    'numericCode': '120',
    'phoneCode': '237',
    'countryName': 'Cameroon',
    'countryCode': 'CM',
    'unicode': 'U+1F1E8 U+1F1F2'
}, {
    'numericCode': '124',
    'phoneCode': '1',
    'countryName': 'Canada',
    'countryCode': 'CA',
    'unicode': 'U+1F1E8 U+1F1E6'
}, {
    'numericCode': '132',
    'phoneCode': '238',
    'countryName': 'Cabo Verde',
    'countryCode': 'CV',
    'unicode': 'U+1F1E8 U+1F1FB'
}, {
    'numericCode': '136',
    'phoneCode': '1',
    'countryName': 'Cayman Islands',
    'countryCode': 'KY',
    'unicode': 'U+1F1F0 U+1F1FE'
}, {
    'numericCode': '140',
    'phoneCode': '236',
    'countryName': 'Central African Republic',
    'countryCode': 'CF',
    'unicode': 'U+1F1E8 U+1F1EB'
}, {
    'numericCode': '148',
    'phoneCode': '235',
    'countryName': 'Chad',
    'countryCode': 'TD',
    'unicode': 'U+1F1F9 U+1F1E9'
}, {
    'numericCode': '152',
    'phoneCode': '56',
    'countryName': 'Chile',
    'countryCode': 'CL',
    'unicode': 'U+1F1E8 U+1F1F1'
}, {
    'numericCode': '156',
    'phoneCode': '86',
    'countryName': 'China',
    'countryCode': 'CN',
    'unicode': 'U+1F1E8 U+1F1F3'
}, {
    'numericCode': '162',
    'phoneCode': '61',
    'countryName': 'Christmas Island',
    'countryCode': 'CX',
    'unicode': 'U+1F1E8 U+1F1FD'
}, {
    'numericCode': '166',
    'phoneCode': '61',
    'countryName': 'Cocos (Keeling) Islands',
    'countryCode': 'CC',
    'unicode': 'U+1F1E8 U+1F1E8'
}, {
    'numericCode': '170',
    'phoneCode': '57',
    'countryName': 'Colombia',
    'countryCode': 'CO',
    'unicode': 'U+1F1E8 U+1F1F4'
}, {
    'numericCode': '174',
    'phoneCode': '269',
    'countryName': 'Comoros',
    'countryCode': 'KM',
    'unicode': 'U+1F1F0 U+1F1F2'
}, {
    'numericCode': '178',
    'phoneCode': '242',
    'countryName': 'Congo',
    'countryCode': 'CG',
    'unicode': 'U+1F1E8 U+1F1E9'
}, {
    'numericCode': '180',
    'phoneCode': '243',
    'countryName': 'Congo (DRC)',
    'countryCode': 'CD',
    'unicode': 'U+1F1E8 U+1F1E9'
}, {
    'numericCode': '184',
    'phoneCode': '682',
    'countryName': 'Cook Islands',
    'countryCode': 'CK',
    'unicode': 'U+1F1E8 U+1F1F0'
}, {
    'numericCode': '188',
    'phoneCode': '506',
    'countryName': 'Costa Rica',
    'countryCode': 'CR',
    'unicode': 'U+1F1E8 U+1F1F7'
}, {
    'numericCode': '191',
    'phoneCode': '385',
    'countryName': 'Croatia',
    'countryCode': 'HR',
    'unicode': 'U+1F1ED U+1F1F7'
}, {
    'numericCode': '192',
    'phoneCode': '53',
    'countryName': 'Cuba',
    'countryCode': 'CU',
    'unicode': 'U+1F1E8 U+1F1FA'
}, {
    'numericCode': '531',
    'phoneCode': '599',
    'countryName': 'Curaçao',
    'countryCode': 'CW',
    'unicode': 'U+1F1E8 U+1F1FC'
}, {
    'numericCode': '196',
    'phoneCode': '357',
    'countryName': 'Cyprus',
    'countryCode': 'CY',
    'unicode': 'U+1F1E8 U+1F1FE'
}, {
    'numericCode': '203',
    'phoneCode': '420',
    'countryName': 'Czech Republic',
    'countryCode': 'CZ',
    'unicode': 'U+1F1E8 U+1F1FF'
}, {
    'numericCode': '208',
    'phoneCode': '45',
    'countryName': 'Denmark',
    'countryCode': 'DK',
    'unicode': 'U+1F1E9 U+1F1F0'
}, {
    'numericCode': '262',
    'phoneCode': '253',
    'countryName': 'Djibouti',
    'countryCode': 'DJ',
    'unicode': 'U+1F1E9 U+1F1EF'
}, {
    'numericCode': '212',
    'phoneCode': '1',
    'countryName': 'Dominica',
    'countryCode': 'DM',
    'unicode': 'U+1F1E9 U+1F1F2'
}, {
    'numericCode': '214',
    'phoneCode': '1',
    'countryName': 'Dominican Republic',
    'countryCode': 'DO',
    'unicode': 'U+1F1E9 U+1F1F2'
}, {
    'numericCode': '218',
    'phoneCode': '593',
    'countryName': 'Ecuador',
    'countryCode': 'EC',
    'unicode': 'U+1F1EA U+1F1E8'
}, {
    'numericCode': '818',
    'phoneCode': '20',
    'countryName': 'Egypt',
    'countryCode': 'EG',
    'unicode': 'U+1F1EA U+1F1EC'
}, {
    'numericCode': '222',
    'phoneCode': '503',
    'countryName': 'El Salvador',
    'countryCode': 'SV',
    'unicode': 'U+1F1F8 U+1F1FB'
}, {
    'numericCode': '226',
    'phoneCode': '240',
    'countryName': 'Equatorial Guinea',
    'countryCode': 'GQ',
    'unicode': 'U+1F1EC U+1F1F6'
}, {
    'numericCode': '232',
    'phoneCode': '291',
    'countryName': 'Eritrea',
    'countryCode': 'ER',
    'unicode': 'U+1F1EA U+1F1F7'
}, {
    'numericCode': '233',
    'phoneCode': '372',
    'countryName': 'Estonia',
    'countryCode': 'EE',
    'unicode': 'U+1F1EA U+1F1EA'
}, {
    'numericCode': '231',
    'phoneCode': '251',
    'countryName': 'Ethiopia',
    'countryCode': 'ET',
    'unicode': 'U+1F1EA U+1F1F9'
}, {
    'numericCode': '238',
    'phoneCode': '500',
    'countryName': 'Falkland Islands',
    'countryCode': 'FK',
    'unicode': 'U+1F1EB U+1F1F0'
}, {
    'numericCode': '234',
    'phoneCode': '298',
    'countryName': 'Faroe Islands',
    'countryCode': 'FO',
    'unicode': 'U+1F1EB U+1F1F4'
}, {
    'numericCode': '242',
    'phoneCode': '679',
    'countryName': 'Fiji',
    'countryCode': 'FJ',
    'unicode': 'U+1F1EB U+1F1EF'
}, {
    'numericCode': '246',
    'phoneCode': '358',
    'countryName': 'Finland',
    'countryCode': 'FI',
    'unicode': 'U+1F1EB U+1F1EE'
}, {
    'numericCode': '250',
    'phoneCode': '33',
    'countryName': 'France',
    'countryCode': 'FR',
    'unicode': 'U+1F1EB U+1F1F7'
}, {
    'numericCode': '258',
    'phoneCode': '689',
    'countryName': 'French Polynesia',
    'countryCode': 'PF',
    'unicode': 'U+1F1F5 U+1F1EB'
}, {
    'numericCode': '266',
    'phoneCode': '241',
    'countryName': 'Gabon',
    'countryCode': 'GA',
    'unicode': 'U+1F1EC U+1F1E6'
}, {
    'numericCode': '270',
    'phoneCode': '220',
    'countryName': 'Gambia',
    'countryCode': 'GM',
    'unicode': 'U+1F1EC U+1F1F2'
}, {
    'numericCode': '268',
    'phoneCode': '995',
    'countryName': 'Georgia',
    'countryCode': 'GE',
    'unicode': 'U+1F1EC U+1F1EA'
}, {
    'numericCode': '276',
    'phoneCode': '49',
    'countryName': 'Germany',
    'countryCode': 'DE',
    'unicode': 'U+1F1E9 U+1F1EA'
}, {
    'numericCode': '288',
    'phoneCode': '233',
    'countryName': 'Ghana',
    'countryCode': 'GH',
    'unicode': 'U+1F1EC U+1F1ED'
}, {
    'numericCode': '292',
    'phoneCode': '350',
    'countryName': 'Gibraltar',
    'countryCode': 'GI',
    'unicode': 'U+1F1EC U+1F1EE'
}, {
    'numericCode': '300',
    'phoneCode': '30',
    'countryName': 'Greece',
    'countryCode': 'GR',
    'unicode': 'U+1F1EC U+1F1F7'
}, {
    'numericCode': '304',
    'phoneCode': '299',
    'countryName': 'Greenland',
    'countryCode': 'GL',
    'unicode': 'U+1F1EC U+1F1F1'
}, {
    'numericCode': '308',
    'phoneCode': '1',
    'countryName': 'Grenada',
    'countryCode': 'GD',
    'unicode': 'U+1F1EC U+1F1E9'
}, {
    'numericCode': '316',
    'phoneCode': '1',
    'countryName': 'Guam',
    'countryCode': 'GU',
    'unicode': 'U+1F1EC U+1F1FA'
}, {
    'numericCode': '320',
    'phoneCode': '502',
    'countryName': 'Guatemala',
    'countryCode': 'GT',
    'unicode': 'U+1F1EC U+1F1F9'
}, {
    'numericCode': '831',
    'phoneCode': '44',
    'countryName': 'Guernsey',
    'countryCode': 'GG',
    'unicode': 'U+1F1EC U+1F1EC'
}, {
    'numericCode': '324',
    'phoneCode': '224',
    'countryName': 'Guinea',
    'countryCode': 'GN',
    'unicode': 'U+1F1EC U+1F1F6'
}, {
    'numericCode': '624',
    'phoneCode': '245',
    'countryName': 'Guinea-Bissau',
    'countryCode': 'GW',
    'unicode': 'U+1F1EC U+1F1F3'
}, {
    'numericCode': '332',
    'phoneCode': '509',
    'countryName': 'Haiti',
    'countryCode': 'HT',
    'unicode': 'U+1F1ED U+1F1F9'
}, {
    'numericCode': '340',
    'phoneCode': '504',
    'countryName': 'Honduras',
    'countryCode': 'HN',
    'unicode': 'U+1F1ED U+1F1F3'
}, {
    'numericCode': '344',
    'phoneCode': '852',
    'countryName': 'Hong Kong',
    'countryCode': 'HK',
    'unicode': 'U+1F1ED U+1F1F0'
}, {
    'numericCode': '348',
    'phoneCode': '36',
    'countryName': 'Hungary',
    'countryCode': 'HU',
    'unicode': 'U+1F1ED U+1F1FA'
}, {
    'numericCode': '352',
    'phoneCode': '354',
    'countryName': 'Iceland',
    'countryCode': 'IS',
    'unicode': 'U+1F1EE U+1F1F8'
}, {
    'numericCode': '356',
    'phoneCode': '91',
    'countryName': 'India',
    'countryCode': 'IN',
    'unicode': 'U+1F1EE U+1F1F4'
}, {
    'numericCode': '360',
    'phoneCode': '62',
    'countryName': 'Indonesia',
    'countryCode': 'ID',
    'unicode': 'U+1F1EE U+1F1E9'
}, {
    'numericCode': '384',
    'phoneCode': '225',
    'countryName': 'Ivory Coast',
    'countryCode': 'CI',
    'unicode': 'U+1F1E8 U+1F1EE'
}, {
    'numericCode': '364',
    'phoneCode': '98',
    'countryName': 'Iran',
    'countryCode': 'IR',
    'unicode': 'U+1F1EE U+1F1F7'
}, {
    'numericCode': '368',
    'phoneCode': '964',
    'countryName': 'Iraq',
    'countryCode': 'IQ',
    'unicode': 'U+1F1EE U+1F1F6'
}, {
    'numericCode': '372',
    'phoneCode': '353',
    'countryName': 'Ireland',
    'countryCode': 'IE',
    'unicode': 'U+1F1EE U+1F1EA'
}, {
    'numericCode': '833',
    'phoneCode': '44',
    'countryName': 'Isle of Man',
    'countryCode': 'IM',
    'unicode': 'U+1F1EE U+1F1F2'
}, {
    'numericCode': '376',
    'phoneCode': '972',
    'countryName': 'Israel',
    'countryCode': 'IL',
    'unicode': 'U+1F1EE U+1F1F1'
}, {
    'numericCode': '380',
    'phoneCode': '39',
    'countryName': 'Italy',
    'countryCode': 'IT',
    'unicode': 'U+1F1EE U+1F1F9'
}, {
    'numericCode': '388',
    'phoneCode': '1',
    'countryName': 'Jamaica',
    'countryCode': 'JM',
    'unicode': 'U+1F1EF U+1F1F2'
}, {
    'numericCode': '392',
    'phoneCode': '81',
    'countryName': 'Japan',
    'countryCode': 'JP',
    'unicode': 'U+1F1EF U+1F1F5'
}, {
    'numericCode': '832',
    'phoneCode': '44',
    'countryName': 'Jersey',
    'countryCode': 'JE',
    'unicode': 'U+1F1EF U+1F1EA'
}, {
    'numericCode': '400',
    'phoneCode': '962',
    'countryName': 'Jordan',
    'countryCode': 'JO',
    'unicode': 'U+1F1EF U+1F1F4'
}, {
    'numericCode': '398',
    'phoneCode': '7',
    'countryName': 'Kazakhstan',
    'countryCode': 'KZ',
    'unicode': 'U+1F1F0 U+1F1FF'
}, {
    'numericCode': '404',
    'phoneCode': '254',
    'countryName': 'Kenya',
    'countryCode': 'KE',
    'unicode': 'U+1F1F0 U+1F1EA'
}, {
    'numericCode': '414',
    'phoneCode': '965',
    'countryName': 'Kuwait',
    'countryCode': 'KW',
    'unicode': 'U+1F1F0 U+1F1FC'
}, {
    'numericCode': '417',
    'phoneCode': '996',
    'countryName': 'Kyrgyzstan',
    'countryCode': 'KG',
    'unicode': 'U+1F1F0 U+1F1EC'
}, {
    'numericCode': '418',
    'phoneCode': '856',
    'countryName': 'Laos',
    'countryCode': 'LA',
    'unicode': 'U+1F1F1 U+1F1E6'
}, {
    'numericCode': '428',
    'phoneCode': '371',
    'countryName': 'Latvia',
    'countryCode': 'LV',
    'unicode': 'U+1F1F1 U+1F1FB'
}, {
    'numericCode': '422',
    'phoneCode': '961',
    'countryName': 'Lebanon',
    'countryCode': 'LB',
    'unicode': 'U+1F1F1 U+1F1E7'
}, {
    'numericCode': '426',
    'phoneCode': '266',
    'countryName': 'Lesotho',
    'countryCode': 'LS',
    'unicode': 'U+1F1F1 U+1F1F8'
}, {
    'numericCode': '430',
    'phoneCode': '231',
    'countryName': 'Liberia',
    'countryCode': 'LR',
    'unicode': 'U+1F1F1 U+1F1F7'
}, {
    'numericCode': '434',
    'phoneCode': '218',
    'countryName': 'Libya',
    'countryCode': 'LY',
    'unicode': 'U+1F1F1 U+1F1FE'
}, {
    'numericCode': '438',
    'phoneCode': '423',
    'countryName': 'Liechtenstein',
    'countryCode': 'LI',
    'unicode': 'U+1F1F1 U+1F1EE'
}, {
    'numericCode': '440',
    'phoneCode': '370',
    'countryName': 'Lithuania',
    'countryCode': 'LT',
    'unicode': 'U+1F1F1 U+1F1F9'
}, {
    'numericCode': '442',
    'phoneCode': '352',
    'countryName': 'Luxembourg',
    'countryCode': 'LU',
    'unicode': 'U+1F1F1 U+1F1FA'
}, {
    'numericCode': '446',
    'phoneCode': '853',
    'countryName': 'Macao',
    'countryCode': 'MO',
    'unicode': 'U+1F1F2 U+1F1F4'
}, {
    'numericCode': '807',
    'phoneCode': '389',
    'countryName': 'Macedonia',
    'countryCode': 'MK',
    'unicode': 'U+1F1F2 U+1F1F0'
}, {
    'numericCode': '450',
    'phoneCode': '261',
    'countryName': 'Madagascar',
    'countryCode': 'MG',
    'unicode': 'U+1F1F2 U+1F1EC'
}, {
    'numericCode': '454',
    'phoneCode': '265',
    'countryName': 'Malawi',
    'countryCode': 'MW',
    'unicode': 'U+1F1F2 U+1F1FC'
}, {
    'numericCode': '458',
    'phoneCode': '60',
    'countryName': 'Malaysia',
    'countryCode': 'MY',
    'unicode': 'U+1F1F2 U+1F1FE'
}, {
    'numericCode': '462',
    'phoneCode': '960',
    'countryName': 'Maldives',
    'countryCode': 'MV',
    'unicode': 'U+1F1F2 U+1F1FB'
}, {
    'numericCode': '466',
    'phoneCode': '223',
    'countryName': 'Mali',
    'countryCode': 'ML',
    'unicode': 'U+1F1F2 U+1F1F1'
}, {
    'numericCode': '470',
    'phoneCode': '356',
    'countryName': 'Malta',
    'countryCode': 'MT',
    'unicode': 'U+1F1F2 U+1F1F9'
}, {
    'numericCode': '584',
    'phoneCode': '692',
    'countryName': 'Marshall Islands',
    'countryCode': 'MH',
    'unicode': 'U+1F1F2 U+1F1ED'
}, {
    'numericCode': '474',
    'phoneCode': '596',
    'countryName': 'Martinique',
    'countryCode': 'MQ',
    'unicode': 'U+1F1F2 U+1F1F6'
}, {
    'numericCode': '478',
    'phoneCode': '222',
    'countryName': 'Mauritania',
    'countryCode': 'MR',
    'unicode': 'U+1F1F2 U+1F1F7'
}, {
    'numericCode': '480',
    'phoneCode': '230',
    'countryName': 'Mauritius',
    'countryCode': 'MU',
    'unicode': 'U+1F1F2 U+1F1FA'
}, {
    'numericCode': '484',
    'phoneCode': '52',
    'countryName': 'Mexico',
    'countryCode': 'MX',
    'unicode': 'U+1F1F2 U+1F1FD'
}, {
    'numericCode': '583',
    'phoneCode': '691',
    'countryName': 'Micronesia',
    'countryCode': 'FM',
    'unicode': 'U+1F1EB U+1F1F2'
}, {
    'numericCode': '498',
    'phoneCode': '373',
    'countryName': 'Moldova',
    'countryCode': 'MD',
    'unicode': 'U+1F1F2 U+1F1E9'
}, {
    'numericCode': '492',
    'phoneCode': '377',
    'countryName': 'Monaco',
    'countryCode': 'MC',
    'unicode': 'U+1F1F2 U+1F1E8'
}, {
    'numericCode': '496',
    'phoneCode': '976',
    'countryName': 'Mongolia',
    'countryCode': 'MN',
    'unicode': 'U+1F1F2 U+1F1F3'
}, {
    'numericCode': '499',
    'phoneCode': '382',
    'countryName': 'Montenegro',
    'countryCode': 'ME',
    'unicode': 'U+1F1F2 U+1F1EA'
}, {
    'numericCode': '500',
    'phoneCode': '1',
    'countryName': 'Montserrat',
    'countryCode': 'MS',
    'unicode': 'U+1F1F2 U+1F1F8'
}, {
    'numericCode': '504',
    'phoneCode': '212',
    'countryName': 'Morocco',
    'countryCode': 'MA',
    'unicode': 'U+1F1F2 U+1F1E6'
}, {
    'numericCode': '508',
    'phoneCode': '258',
    'countryName': 'Mozambique',
    'countryCode': 'MZ',
    'unicode': 'U+1F1F2 U+1F1FF'
}, {
    'numericCode': '104',
    'phoneCode': '95',
    'countryName': 'Myanmar',
    'countryCode': 'MM',
    'unicode': 'U+1F1F2 U+1F1F2'
}, {
    'numericCode': '516',
    'phoneCode': '264',
    'countryName': 'Namibia',
    'countryCode': 'NA',
    'unicode': 'U+1F1F3 U+1F1E6'
}, {
    'numericCode': '520',
    'phoneCode': '674',
    'countryName': 'Nauru',
    'countryCode': 'NR',
    'unicode': 'U+1F1F3 U+1F1F7'
}, {
    'numericCode': '524',
    'phoneCode': '977',
    'countryName': 'Nepal',
    'countryCode': 'NP',
    'unicode': 'U+1F1F3 U+1F1F5'
}, {
    'numericCode': '528',
    'phoneCode': '31',
    'countryName': 'Netherlands',
    'countryCode': 'NL',
    'unicode': 'U+1F1E7 U+1F1F6'
}, {
    'numericCode': '554',
    'phoneCode': '64',
    'countryName': 'New Zealand',
    'countryCode': 'NZ',
    'unicode': 'U+1F1F3 U+1F1FF'
}, {
    'numericCode': '558',
    'phoneCode': '505',
    'countryName': 'Nicaragua',
    'countryCode': 'NI',
    'unicode': 'U+1F1F3 U+1F1EE'
}, {
    'numericCode': '562',
    'phoneCode': '227',
    'countryName': 'Niger',
    'countryCode': 'NE',
    'unicode': 'U+1F1F3 U+1F1EA'
}, {
    'numericCode': '566',
    'phoneCode': '234',
    'countryName': 'Nigeria',
    'countryCode': 'NG',
    'unicode': 'U+1F1F3 U+1F1EA'
}, {
    'numericCode': '570',
    'phoneCode': '683',
    'countryName': 'Niue',
    'countryCode': 'NU',
    'unicode': 'U+1F1F3 U+1F1FA'
}, {
    'numericCode': '574',
    'phoneCode': '672',
    'countryName': 'Norfolk Island',
    'countryCode': 'NF',
    'unicode': 'U+1F1F3 U+1F1EB'
}, {
    'numericCode': '408',
    'phoneCode': '850',
    'countryName': 'North Korea',
    'countryCode': 'KP',
    'unicode': 'U+1F1F0 U+1F1F5'
}, {
    'numericCode': '580',
    'phoneCode': '1',
    'countryName': 'Northern Mariana Islands',
    'countryCode': 'MP',
    'unicode': 'U+1F1F2 U+1F1F5'
}, {
    'numericCode': '578',
    'phoneCode': '47',
    'countryName': 'Norway',
    'countryCode': 'NO',
    'unicode': 'U+1F1F3 U+1F1F4'
}, {
    'numericCode': '512',
    'phoneCode': '968',
    'countryName': 'Oman',
    'countryCode': 'OM',
    'unicode': 'U+1F1F4 U+1F1F2'
}, {
    'numericCode': '586',
    'phoneCode': '92',
    'countryName': 'Pakistan',
    'countryCode': 'PK',
    'unicode': 'U+1F1F5 U+1F1F0'
}, {
    'numericCode': '585',
    'phoneCode': '680',
    'countryName': 'Palau',
    'countryCode': 'PW',
    'unicode': 'U+1F1F5 U+1F1FC'
}, {
    'numericCode': '275',
    'phoneCode': '970',
    'countryName': 'Palestine, State of',
    'countryCode': 'PS',
    'unicode': 'U+1F1F5 U+1F1F8'
}, {
    'numericCode': '591',
    'phoneCode': '507',
    'countryName': 'Panama',
    'countryCode': 'PA',
    'unicode': 'U+1F1F5 U+1F1E6'
}, {
    'numericCode': '598',
    'phoneCode': '675',
    'countryName': 'Papua New Guinea',
    'countryCode': 'PG',
    'unicode': 'U+1F1EC U+1F1F3'
}, {
    'numericCode': '600',
    'phoneCode': '595',
    'countryName': 'Paraguay',
    'countryCode': 'PY',
    'unicode': 'U+1F1F5 U+1F1FE'
}, {
    'numericCode': '604',
    'phoneCode': '51',
    'countryName': 'Peru',
    'countryCode': 'PE',
    'unicode': 'U+1F1F5 U+1F1EA'
}, {
    'numericCode': '608',
    'phoneCode': '63',
    'countryName': 'Philippines',
    'countryCode': 'PH',
    'unicode': 'U+1F1F5 U+1F1ED'
}, {
    'numericCode': '612',
    'phoneCode': '64',
    'countryName': 'Pitcairn',
    'countryCode': 'PN',
    'unicode': 'U+1F1F5 U+1F1F3'
}, {
    'numericCode': '616',
    'phoneCode': '48',
    'countryName': 'Poland',
    'countryCode': 'PL',
    'unicode': 'U+1F1F5 U+1F1F1'
}, {
    'numericCode': '620',
    'phoneCode': '351',
    'countryName': 'Portugal',
    'countryCode': 'PT',
    'unicode': 'U+1F1F5 U+1F1F9'
}, {
    'numericCode': '630',
    'phoneCode': '1',
    'countryName': 'Puerto Rico',
    'countryCode': 'PR',
    'unicode': 'U+1F1F5 U+1F1F7'
}, {
    'numericCode': '634',
    'phoneCode': '974',
    'countryName': 'Qatar',
    'countryCode': 'QA',
    'unicode': 'U+1F1F6 U+1F1E6'
}, {
    'numericCode': '_KOS',
    'phoneCode': '383',
    'countryName': 'Republic of Kosovo',
    'countryCode': 'XK',
    'unicode': 'U+1F1FD U+1F1F0'
}, {
    'numericCode': '642',
    'phoneCode': '40',
    'countryName': 'Romania',
    'countryCode': 'RO',
    'unicode': 'U+1F1F4 U+1F1F2'
}, {
    'numericCode': '643',
    'phoneCode': '7',
    'countryName': 'Russian Federation',
    'countryCode': 'RU',
    'unicode': 'U+1F1F7 U+1F1FA'
}, {
    'numericCode': '646',
    'phoneCode': '250',
    'countryName': 'Rwanda',
    'countryCode': 'RW',
    'unicode': 'U+1F1F7 U+1F1FC'
}, {
    'numericCode': '652',
    'phoneCode': '590',
    'countryName': 'Saint Barthélemy',
    'countryCode': 'BL',
    'unicode': 'U+1F1E7 U+1F1F1'
}, {
    'numericCode': '659',
    'phoneCode': '1',
    'countryName': 'Saint Kitts and Nevis',
    'countryCode': 'KN',
    'unicode': 'U+1F1F0 U+1F1F3'
}, {
    'numericCode': '662',
    'phoneCode': '1',
    'countryName': 'Saint Lucia',
    'countryCode': 'LC',
    'unicode': 'U+1F1F1 U+1F1E8'
}, {
    'numericCode': '670',
    'phoneCode': '1',
    'countryName': 'Saint Vincent and the Grenadines',
    'countryCode': 'VC',
    'unicode': 'U+1F1FB U+1F1E8'
}, {
    'numericCode': '882',
    'phoneCode': '685',
    'countryName': 'Samoa',
    'countryCode': 'WS',
    'unicode': 'U+1F1E6 U+1F1F8'
}, {
    'numericCode': '674',
    'phoneCode': '378',
    'countryName': 'San Marino',
    'countryCode': 'SM',
    'unicode': 'U+1F1F8 U+1F1F2'
}, {
    'numericCode': '678',
    'phoneCode': '239',
    'countryName': 'Sao Tome and Principe',
    'countryCode': 'ST',
    'unicode': 'U+1F1F8 U+1F1F9'
}, {
    'numericCode': '682',
    'phoneCode': '966',
    'countryName': 'Saudi Arabia',
    'countryCode': 'SA',
    'unicode': 'U+1F1F8 U+1F1E6'
}, {
    'numericCode': '686',
    'phoneCode': '221',
    'countryName': 'Senegal',
    'countryCode': 'SN',
    'unicode': 'U+1F1F8 U+1F1F3'
}, {
    'numericCode': '688',
    'phoneCode': '381',
    'countryName': 'Serbia',
    'countryCode': 'RS',
    'unicode': 'U+1F1F7 U+1F1F8'
}, {
    'numericCode': '690',
    'phoneCode': '248',
    'countryName': 'Seychelles',
    'countryCode': 'SC',
    'unicode': 'U+1F1F8 U+1F1E8'
}, {
    'numericCode': '694',
    'phoneCode': '232',
    'countryName': 'Sierra Leone',
    'countryCode': 'SL',
    'unicode': 'U+1F1F8 U+1F1F1'
}, {
    'numericCode': '702',
    'phoneCode': '65',
    'countryName': 'Singapore',
    'countryCode': 'SG',
    'unicode': 'U+1F1F8 U+1F1EC'
}, {
    'numericCode': '534',
    'phoneCode': '1',
    'countryName': 'Sint Maarten',
    'countryCode': 'SX',
    'unicode': 'U+1F1F8 U+1F1FD'
}, {
    'numericCode': '703',
    'phoneCode': '421',
    'countryName': 'Slovakia',
    'countryCode': 'SK',
    'unicode': 'U+1F1F8 U+1F1F0'
}, {
    'numericCode': '705',
    'phoneCode': '386',
    'countryName': 'Slovenia',
    'countryCode': 'SI',
    'unicode': 'U+1F1F8 U+1F1EE'
}, {
    'numericCode': '090',
    'phoneCode': '677',
    'countryName': 'Solomon Islands',
    'countryCode': 'SB',
    'unicode': 'U+1F1F8 U+1F1E7'
}, {
    'numericCode': '706',
    'phoneCode': '252',
    'countryName': 'Somalia',
    'countryCode': 'SO',
    'unicode': 'U+1F1F2 U+1F1F1'
}, {
    'numericCode': '710',
    'phoneCode': '27',
    'countryName': 'South Africa',
    'countryCode': 'ZA',
    'unicode': 'U+1F1FF U+1F1E6'
}, {
    'numericCode': '410',
    'phoneCode': '82',
    'countryName': 'South Korea',
    'countryCode': 'KR',
    'unicode': 'U+1F1F0 U+1F1F7'
}, {
    'numericCode': '728',
    'phoneCode': '211',
    'countryName': 'South Sudan',
    'countryCode': 'SS',
    'unicode': 'U+1F1F8 U+1F1F8'
}, {
    'numericCode': '724',
    'phoneCode': '34',
    'countryName': 'Spain',
    'countryCode': 'ES',
    'unicode': 'U+1F1EA U+1F1F8'
}, {
    'numericCode': '144',
    'phoneCode': '94',
    'countryName': 'Sri Lanka',
    'countryCode': 'LK',
    'unicode': 'U+1F1F1 U+1F1F0'
}, {
    'numericCode': '729',
    'phoneCode': '249',
    'countryName': 'Sudan',
    'countryCode': 'SD',
    'unicode': 'U+1F1F8 U+1F1F8'
}, {
    'numericCode': '740',
    'phoneCode': '597',
    'countryName': 'Suriname',
    'countryCode': 'SR',
    'unicode': 'U+1F1F8 U+1F1F7'
}, {
    'numericCode': '752',
    'phoneCode': '46',
    'countryName': 'Sweden',
    'countryCode': 'SE',
    'unicode': 'U+1F1F8 U+1F1EA'
}, {
    'numericCode': '756',
    'phoneCode': '41',
    'countryName': 'Switzerland',
    'countryCode': 'CH',
    'unicode': 'U+1F1E8 U+1F1ED'
}, {
    'numericCode': '760',
    'phoneCode': '963',
    'countryName': 'Syrian Arab Republic',
    'countryCode': 'SY',
    'unicode': 'U+1F1F8 U+1F1FE'
}, {
    'numericCode': '158',
    'phoneCode': '886',
    'countryName': 'Taiwan',
    'countryCode': 'TW',
    'unicode': 'U+1F1F9 U+1F1FC'
}, {
    'numericCode': '762',
    'phoneCode': '992',
    'countryName': 'Tajikistan',
    'countryCode': 'TJ',
    'unicode': 'U+1F1F9 U+1F1EF'
}, {
    'numericCode': '834',
    'phoneCode': '255',
    'countryName': 'Tanzania, United Republic of',
    'countryCode': 'TZ',
    'unicode': 'U+1F1F9 U+1F1FF'
}, {
    'numericCode': '764',
    'phoneCode': '66',
    'countryName': 'Thailand',
    'countryCode': 'TH',
    'unicode': 'U+1F1F9 U+1F1ED'
}, {
    'numericCode': '626',
    'phoneCode': '670',
    'countryName': 'Timor-Leste',
    'countryCode': 'TL',
    'unicode': 'U+1F1F9 U+1F1F1'
}, {
    'numericCode': '768',
    'phoneCode': '228',
    'countryName': 'Togo',
    'countryCode': 'TG',
    'unicode': 'U+1F1F9 U+1F1EC'
}, {
    'numericCode': '772',
    'phoneCode': '690',
    'countryName': 'Tokelau',
    'countryCode': 'TK',
    'unicode': 'U+1F1F9 U+1F1F0'
}, {
    'numericCode': '776',
    'phoneCode': '676',
    'countryName': 'Tonga',
    'countryCode': 'TO',
    'unicode': 'U+1F1F9 U+1F1F4'
}, {
    'numericCode': '780',
    'phoneCode': '1',
    'countryName': 'Trinidad and Tobago',
    'countryCode': 'TT',
    'unicode': 'U+1F1F9 U+1F1F9'
}, {
    'numericCode': '788',
    'phoneCode': '216',
    'countryName': 'Tunisia',
    'countryCode': 'TN',
    'unicode': 'U+1F1F9 U+1F1F3'
}, {
    'numericCode': '792',
    'phoneCode': '90',
    'countryName': 'Turkey',
    'countryCode': 'TR',
    'unicode': 'U+1F1F9 U+1F1F7'
}, {
    'numericCode': '795',
    'phoneCode': '993',
    'countryName': 'Turkmenistan',
    'countryCode': 'TM',
    'unicode': 'U+1F1F9 U+1F1F2'
}, {
    'numericCode': '796',
    'phoneCode': '1',
    'countryName': 'Turks and Caicos Islands',
    'countryCode': 'TC',
    'unicode': 'U+1F1F9 U+1F1E8'
}, {
    'numericCode': '798',
    'phoneCode': '688',
    'countryName': 'Tuvalu',
    'countryCode': 'TV',
    'unicode': 'U+1F1F9 U+1F1FB'
}, {
    'numericCode': '800',
    'phoneCode': '256',
    'countryName': 'Uganda',
    'countryCode': 'UG',
    'unicode': 'U+1F1FA U+1F1EC'
}, {
    'numericCode': '804',
    'phoneCode': '380',
    'countryName': 'Ukraine',
    'countryCode': 'UA',
    'unicode': 'U+1F1FA U+1F1E6'
}, {
    'numericCode': '784',
    'phoneCode': '971',
    'countryName': 'United Arab Emirates',
    'countryCode': 'AE',
    'unicode': 'U+1F1E6 U+1F1EA'
}, {
    'numericCode': '826',
    'phoneCode': '44',
    'countryName': 'United Kingdom',
    'countryCode': 'GB',
    'unicode': 'U+1F1EE U+1F1EA'
}, {
    'numericCode': '840',
    'phoneCode': '1',
    'countryName': 'United States of America',
    'countryCode': 'US',
    'unicode': 'U+1F1FA U+1F1F8'
}, {
    'numericCode': '858',
    'phoneCode': '598',
    'countryName': 'Uruguay',
    'countryCode': 'UY',
    'unicode': 'U+1F1FA U+1F1FE'
}, {
    'numericCode': '860',
    'phoneCode': '998',
    'countryName': 'Uzbekistan',
    'countryCode': 'UZ',
    'unicode': 'U+1F1FA U+1F1FF'
}, {
    'numericCode': '548',
    'phoneCode': '678',
    'countryName': 'Vanuatu',
    'countryCode': 'VU',
    'unicode': 'U+1F1FB U+1F1FA'
}, {
    'numericCode': '862',
    'phoneCode': '58',
    'countryName': 'Venezuela',
    'countryCode': 'VE',
    'unicode': 'U+1F1FB U+1F1EA'
}, {
    'numericCode': '704',
    'phoneCode': '84',
    'countryName': 'Viet Nam',
    'countryCode': 'VN',
    'unicode': 'U+1F1FB U+1F1F3'
}, {
    'numericCode': '887',
    'phoneCode': '967',
    'countryName': 'Yemen',
    'countryCode': 'YE',
    'unicode': 'U+1F1FE U+1F1EA'
}, {
    'numericCode': '894',
    'phoneCode': '260',
    'countryName': 'Zambia',
    'countryCode': 'ZM',
    'unicode': 'U+1F1FF U+1F1F2'
}, {
    'numericCode': '716',
    'phoneCode': '263',
    'countryName': 'Zimbabwe',
    'countryCode': 'ZW',
    'unicode': 'U+1F1FF U+1F1FC'
}];
