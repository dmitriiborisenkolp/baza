import { PhoneCountryCodesDto } from './phone-country-codes.dto';

export type PhoneCountryCodesRepositoryDto = Array<PhoneCountryCodesDto>;
