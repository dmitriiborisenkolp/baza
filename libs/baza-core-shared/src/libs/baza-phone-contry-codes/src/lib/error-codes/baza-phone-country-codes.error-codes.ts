export enum BazaPhoneCountryCodesErrorCodes {
    BazaPCCInvalidAPIModuleConfiguration = 'BazaPCCInvalidAPIModuleConfiguration',
    BazaPCCNotFound = 'BazaPCCNotFound',
}
