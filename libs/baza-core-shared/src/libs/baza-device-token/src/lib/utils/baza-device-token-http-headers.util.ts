import { Request } from 'express';
import { BazaHttpHeaders } from '../../../../../libs/baza-common/src';
import { BazaDeviceTokenActionSet, BazaDeviceTokenActionSets } from '../models/baza-device-token-set';
import { BazaDeviceTokenType } from '../models/baza-device-token-type';

/**
 * Extracts all device tokens from the HTTP request headers
 * @param request HTTP request
 * @returns Two sets of device tokens (linked and unlinked)
 */
export function extractDeviceTokensFromRequest(request: Request): BazaDeviceTokenActionSets {
    const link = [
        [BazaDeviceTokenType.FCM, request.header(BazaHttpHeaders.HTTP_HEADER_LINK_FCM_TOKEN)],
        [BazaDeviceTokenType.Apple, request.header(BazaHttpHeaders.HTTP_HEADER_LINK_APPLE_TOKEN)],
    ].filter(([, token]) => !!token) as BazaDeviceTokenActionSet;

    const unlink = [
        [BazaDeviceTokenType.FCM, request.header(BazaHttpHeaders.HTTP_HEADER_UNLINK_FCM_TOKEN)],
        [BazaDeviceTokenType.Apple, request.header(BazaHttpHeaders.HTTP_HEADER_UNLINK_APPLE_TOKEN)],
    ].filter(([, token]) => !!token) as BazaDeviceTokenActionSet;

    return { link, unlink };
}
