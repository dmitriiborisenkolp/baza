import { BazaDeviceTokenDestroyScope } from '../models/baza-device-token-destroy-scope';

export class BazaDeviceTokenDestroyCommand {
    constructor(
        public request: {
            accountId?: number;
            scope?: BazaDeviceTokenDestroyScope;
            beforeDate?: Date;
        },
    ) {}
}
