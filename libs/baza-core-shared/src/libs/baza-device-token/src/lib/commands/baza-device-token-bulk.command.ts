import { BazaDeviceTokenActionSets } from '../models/baza-device-token-set';

export class BazaDeviceTokenBulkCommand {
    constructor(
        public request: {
            accountId: number;
            actionSets: BazaDeviceTokenActionSets;
        },
    ) {}
}
