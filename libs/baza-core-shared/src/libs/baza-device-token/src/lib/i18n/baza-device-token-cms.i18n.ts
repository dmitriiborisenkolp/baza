export const bazaDeviceTokenCmsI18n = {
    deviceTokens: {
        components: {
            tokens: {
                title: 'Device tokens for {{ fullName }} (ID: {{ id }})',
                breadcrumbs: {
                    accounts: 'Accounts',
                },
                items: {
                    linkToken: 'Link device token',
                },
                columns: {
                    id: 'ID',
                    type: 'Type',
                    token: 'Token',
                    created: 'Created',
                    updated: 'Latests update',
                },
                actions: {
                    unlink: 'Unlink',
                },
                unlink: {
                    message: 'Do you really want to unlink the device token ({{ id }})?',
                },
                forms: {
                    linkToken: {
                        title: 'Link device token',
                        fields: {
                            type: 'Type',
                            token: 'Token',
                        },
                        placeholders: {
                            type: 'FCM / Apple',
                        },
                    },
                },
            },
        },
    },
};
