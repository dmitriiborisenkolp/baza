export enum BazaDeviceTokenErrorCodes {
    BazaDeviceTokenNotFound = 'BazaDeviceTokenNotFound',
    BazaDeviceTokenBadAccountToken = 'BazaDeviceTokenBadAccountToken',
}

export const bazaDeviceTokenErrorCodesI18n = {
    [BazaDeviceTokenErrorCodes.BazaDeviceTokenNotFound]: 'Device token was not found',
    [BazaDeviceTokenErrorCodes.BazaDeviceTokenBadAccountToken]: 'Device token is already linked to account {{accountId}}',
};
