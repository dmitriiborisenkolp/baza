import { BazaDeviceTokenType } from './baza-device-token-type';

export type BazaDeviceTokenDestroyScope = `${BazaDeviceTokenType | 'All'}`;
