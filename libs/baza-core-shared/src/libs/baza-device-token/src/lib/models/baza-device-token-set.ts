import { BazaDeviceTokenType } from './baza-device-token-type';

export type BazaDeviceTokenActionSet = Array<[BazaDeviceTokenType, string]>;

export type BazaDeviceTokenActionSets = {
    link: BazaDeviceTokenActionSet;
    unlink: BazaDeviceTokenActionSet;
};
