import { StatusDto } from '../dto/status.dto';
import { Observable } from 'rxjs';

export enum BazaStatusEndpointPaths {
    index = '/',
}

export interface StatusEndpoint {
    index(...args: unknown[]): Promise<BazaStatusIndexResponse> | Observable<BazaStatusIndexResponse>;
}

export type BazaStatusIndexResponse = StatusDto;
