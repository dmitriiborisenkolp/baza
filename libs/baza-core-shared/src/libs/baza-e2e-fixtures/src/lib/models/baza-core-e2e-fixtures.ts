export enum BazaCoreE2eFixtures {
    BazaCoreAuthExampleUser = 'BazaCoreAuthExampleUser',
    BazaCoreAuthExampleUniqueUser = 'BazaCoreAuthExampleUniqueUser',
    BazaCoreAuthAdditionalExampleAdmins = 'BazaCoreAuthAdditionalExampleAdmins',
}

export class BazaCoreE2eFixturesAdditionalOptions {}
