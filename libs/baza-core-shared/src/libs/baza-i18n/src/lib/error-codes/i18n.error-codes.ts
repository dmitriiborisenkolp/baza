export enum I18nErrorCodes {
    I18nTranslationNotFound = 'I18nTranslationNotFound',
    I18nLanguageNotFound = 'I18nLanguageNotFound',
}
