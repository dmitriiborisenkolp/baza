import { AttachmentImageSize, AttachmentImageType, AttachmentSizeSetItem } from '../models/attachment-image';

export type HexColor = string; // #FFFFFF, #000000, ...

export const AttachmentConstants: {
    DEFAULT_IMAGE_QUALITY: number;
    DEFAULT_IMAGE_FORMAT: AttachmentImageType;
    DEFAULT_IMAGE_SIZES: Array<AttachmentSizeSetItem>;
    DEFAULT_JPEG_BG_COLOR: HexColor;
} = {
    DEFAULT_IMAGE_QUALITY: 81,
    DEFAULT_IMAGE_FORMAT: AttachmentImageType.JPEG,
    DEFAULT_IMAGE_SIZES: [
        { size: AttachmentImageSize.XS, maxWidth: 300 },
        { size: AttachmentImageSize.SM, maxWidth: 600 },
        { size: AttachmentImageSize.MD, maxWidth: 1200 },
        { size: AttachmentImageSize.XL, maxWidth: 3500 },
    ],
    DEFAULT_JPEG_BG_COLOR: '#FFFFFF',
}
