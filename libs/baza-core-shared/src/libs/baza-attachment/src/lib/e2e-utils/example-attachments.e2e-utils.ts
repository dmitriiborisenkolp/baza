import { AttachmentDto } from '../dto/attachment.dto';
import { AttachmentType } from '../models/attachment';

export function e2eExampleAttachment(): AttachmentDto {
    return {
        s3ObjectId: 'example-attachment',
        payload: {
            type: AttachmentType.None,
        },
    };
}
