import { AttachmentDto } from '../dto/attachment.dto';
import { AttachmentFile, AttachmentRequest } from '../models/attachment';
import { Observable } from 'rxjs';

export enum AttachmentEndpointPaths {
    upload = '/baza-attachment/upload',
}

export interface AttachmentEndpoint {
    upload(file: AttachmentFile | File, request: AttachmentUploadRequest): Promise<AttachmentUploadResponse> | Observable<AttachmentUploadResponse>;
}

export type AttachmentUploadRequest = AttachmentRequest;
export type AttachmentUploadResponse = AttachmentDto;
