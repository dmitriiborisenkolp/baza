export enum AttachmentErrorCodes {
    AttachmentFileIsNotProvided = 'AttachmentFileIsNotProvided',
    AttachmentStrategyNotFound = 'AttachmentStrategyNotFound',
    AttachmentFileIsTooSmall = 'AttachmentFileIsTooSmall',
    AttachmentFileIsTooLarge = 'AttachmentFileIsTooLarge',
    AttachmentIsNotAnImage = 'AttachmentIsNotAnImage',
    AttachmentMIMETypeMismatch = 'AttachmentMIMETypeMismatch',
    AttachmentUnknownProcessImageType = 'AttachmentUnknownProcessImageType',
    AttachmentSizeSetIsNotSpecified = 'AttachmentSizeSetIsNotSpecified',
    AttachmentImageCannotBeConvertedToSet = 'AttachmentImageCannotBeConvertedToSet',
}
