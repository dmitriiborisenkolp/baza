export const bazaMaintenanceCmsEnI18n: Record<string, unknown> = {
    maintenance: {
        title: 'Maintenance',
        components: {
            bazaMaintenanceCms: {
                title: 'Maintenance',
                description: 'Turn off flags below to temporary disallow visitors/users to use services.',
                enabled: 'Service {{ application }} enabled',
                disabled: 'Service {{ application }} disabled',
                failed: 'Failed to change maintenance status for {{ application }} service',
                message: 'Message',
                messageDescription: 'This message will be displayed to visitors when service will be under maintenance:',
                messageEdit: 'Change maintenance message',
                messageForm: {
                    title: 'Maintenance Message',
                    fields: {
                        message: 'Message',
                    },
                },
            },
        },
    },
};
