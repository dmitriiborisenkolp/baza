export enum BazaMaintenanceErrorCodes {
    BazaMaintenanceServiceUnavailable = 'BazaMaintenanceServiceUnavailable',
    BazaMaintenanceServiceCannotBeUnderMaintenance = 'BazaMaintenanceServiceCannotBeUnderMaintenance',
}
