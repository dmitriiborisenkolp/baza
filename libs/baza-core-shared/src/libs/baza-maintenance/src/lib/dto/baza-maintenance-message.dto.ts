import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class BazaMaintenanceMessageDto {
    @ApiModelProperty()
    message: string;
}
