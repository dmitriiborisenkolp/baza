export const BAZA_MAINTENANCE_REGISTRY_KEY = 'bazaCoreMaintenance.message';

export const BAZA_MAINTENANCE_DEFAULT_MESSAGE = 'Application is under maintenance!';
