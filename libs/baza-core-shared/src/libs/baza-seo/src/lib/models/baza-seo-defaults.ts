import { BazaSeoDto } from '../dto/baza-seo.dto';

export function bazaSeoDefault(): BazaSeoDto {
    return {
        meta: [],
    };
}
