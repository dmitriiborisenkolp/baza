import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BazaSeoMetaDto } from './baza-seo-meta.dto';
import { BazaSeoCardDto } from './baza-seo-card.dto';
import { IsArray, IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class BazaSeoDto {
    @ApiModelProperty({
        description: 'Title for HEAD TITLE element',
        required: false,
    })
    @IsString()
    @IsOptional()
    title?: string;

    @ApiModelProperty({
        description: 'URN (can be named as URI ("slug") of resources',
        required: false,
    })
    @IsString()
    @IsOptional()
    urn?: string;

    @ApiModelProperty({
        description: 'Additional meta tags',
    })
    @IsArray()
    @ValidateNested()
    @Type(() => BazaSeoMetaDto)
    meta: Array<BazaSeoMetaDto>;

    @ApiModelProperty({
        description: 'Card for social networks (https://ogp.me/)',
        required: false,
    })
    @Type(() => BazaSeoCardDto)
    @IsOptional()
    card?: BazaSeoCardDto;
}
