export enum BazaReferralCampaignErrorCodes {
    BazaReferralCampaignNotFound = 'BazaReferralCampaignNotFound',
    BazaReferralCampaignWithSameNameAlreadyExists = 'BazaReferralCampaignWithSameNameAlreadyExists',
}
