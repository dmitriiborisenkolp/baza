export enum BazaReferralCodeErrorCodes {
    BazaReferralCodeNotFound = 'BazaReferralCodeNotFound',
    BazaReferralCodeDuplicate = 'BazaReferralCodeDuplicate',
    BazaReferralCodeIsNotAvailableAnymore = 'BazaReferralCodeIsNotAvailableAnymore',
    BazaReferralCodeIsNotActive = 'BazaReferralCodeIsNotActive',
    BazaReferralCodeNoAssociationsSetException = 'BazaReferralCodeNoAssociationsSetException',
    BazaReferralCodeIsInvalid = 'BazaReferralCodeIsInvalid',
}
