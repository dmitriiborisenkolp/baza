export enum BazaReferralCodesAcl {
    BazaReferralCodes = 'BazaReferralCodes',
    BazaReferralCodesManagement = 'BazaReferralCodesManagement',
}

export const bazaReferralCodesAclI18n = {
    'referral-codes': {
        group: 'Referral Codes',
        nodes: {
            [BazaReferralCodesAcl.BazaReferralCodes]: 'Referral Campaigns & Codes',
            [BazaReferralCodesAcl.BazaReferralCodesManagement]: 'Referral Campaigns & Codes management',
        },
    },
};
