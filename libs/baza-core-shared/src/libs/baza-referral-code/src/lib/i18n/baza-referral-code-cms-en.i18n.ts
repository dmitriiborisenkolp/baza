export const bazaReferralCodeCmsEnI18n = {
    referralCodes: {
        title: 'Referral Codes',
        validators: {
            bazaReferralCodeValidator:
                'Referral code is not valid. It should only contain letters, numbers, _ and !. No spaces or special characters are allowed.',
        },
        components: {
            referral: {
                title: 'Referral Codes',
                navigation: {
                    referral: 'Referral Codes',
                },
                tabs: {
                    accounts: 'Accounts',
                    campaigns: 'Campaign',
                },
                accounts: {
                    exportToCsv: 'Export to CSV',
                    columns: {
                        id: 'ID',
                        email: 'Email',
                        lastSignIn: 'Latest Sign In',
                        fullName: 'Full Name',
                        dateCreated: 'Date Created',
                        isEmailConfirmed: 'Is Email Confirmed?',
                        referralCode: 'Referral Code',
                        signedUpWithClient: 'Signed Up With Client',
                        signedUpWithReferralCode: 'Signed Up With Referral Code',
                        referralCodes: 'Referral Codes',
                        firstName: 'First Name',
                        lastName: 'Last Name',
                    },
                },
                campaigns: {
                    actions: {
                        add: 'New referral campaign',
                        edit: 'Edit referral campaign',
                        delete: {
                            title: 'Delete referral campaign',
                            confirm:
                                'Do you really want to remove referral campaign? All referral codes and usage statistics will be also removed!',
                        },
                        referralCodes: 'Manage referral codes of campaign',
                    },
                    columns: {
                        id: 'ID',
                        name: 'Campaign Name',
                        isActive: 'Is active?',
                        referralCodes: 'Referral Codes',
                        usedCount: 'Used Count',
                    },
                    forms: {
                        fields: {
                            name: 'Campaign Name',
                            isActive: 'Is Active?',
                        },
                        placeholders: {
                            name: 'Campaign Name',
                        },
                    },
                },
            },
            referralCodes: {
                exportToCsv: 'Export Statistics to CSV',
                navigation: {
                    referral: 'Referral Codes',
                },
                campaigns: {
                    title: '{{ campaign.name }}',
                    usage: 'Usage Statistics',
                    messages: {
                        isActive: {
                            title: 'Campaign is online!',
                            description: 'Campaign is active and active referral codes inside campaign can be used',
                        },
                        isNotActive: {
                            title: 'Campaign is not ready',
                            description: 'Campaign is not active yet. None of referral codes inside campaign can be used',
                        },
                    },
                },
                accounts: {
                    title: '{{ account.fullName }}',
                    usage: 'Usage Statistics',
                    messages: {
                        isActive: {
                            title: 'Account is confirmed',
                            description: 'All referral codes attached to account can be used',
                        },
                        isNotActive: {
                            title: 'Account is not confirmed',
                            description: 'Referral codes attached to account cannot be used',
                        },
                        isDeactivated: {
                            title: 'Account is deleted',
                            description: 'Referral codes attached to account cannot be used, enabled or modified',
                        },
                    },
                },
                actions: {
                    add: 'New referral code',
                    edit: 'Edit referral code',
                    delete: {
                        title: 'Delete referral code',
                        confirm: 'Do you really want to remove referral code? All usage statistics will be also removed!',
                    },
                },
                columns: {
                    id: 'ID',
                    codeDisplay: 'Code',
                    isActive: 'Is active?',
                    isDefault: 'Is default?',
                    usedCount: 'Used Count',
                    usage: 'Statistics',
                },
                forms: {
                    fields: {
                        codeDisplay: 'Code',
                        isActive: 'Is Active?',
                        isDefault: 'Is default?',
                    },
                    placeholders: {
                        codeDisplay: 'Code',
                    },
                },
                usage: {
                    csv: {
                        referralCode: 'Referral Code',
                        joinedWithClient: 'Joined With Client',
                        joinedAccount: 'Account',
                        dateJoinedAt: 'Joined At',
                    },
                },
            },
            referralCodesUsage: {
                exportToCsv: 'Export Statistics to CSV',
                navigation: {
                    referral: 'Referral Codes',
                    usage: 'Usage Statistics',
                },
                campaigns: {
                    title: '{{ campaign.name }} Statistics',
                },
                accounts: {
                    title: '{{ account.fullName }} Referral Codes Usage Statistics',
                },
                columns: {
                    referralCode: 'Referral Code',
                    joinedAccount: 'Joined Account',
                    joinedWithClient: 'Joined With Client',
                    dateJoinedAt: 'Date Joined',
                },
                usage: {
                    csv: {
                        referralCode: 'Referral Code',
                        joinedWithClient: 'Joined With Client',
                        joinedAccount: 'Account',
                        dateJoinedAt: 'Joined At',
                    },
                },
            },
        },
    },
    subscription: {
        title: '{{ listing.title }} / Subscriptions',
        navigation: 'Subscriptions',
        exportToCsv: 'Export to CSV',
        columns: {
            id: 'ID',
            email: 'Email',
            account: 'Account',
            status: 'Status',
            subscribed: 'Subscribed',
            sent: 'Sent',
        },
        actions: {
            add: 'Add subscription',
            exclude: {
                label: 'Unsubscribe account',
                confirm: 'Do you really want to unsubscribe account?',
                success: 'Account unsubscribed',
            },
            include: {
                label: 'Subscribe account',
                confirm: 'Do you really want to subscribe account?',
                success: 'Account subscribed',
            },
        },
        form: {
            fields: {
                email: 'Email',
            },
        },
    },
};
