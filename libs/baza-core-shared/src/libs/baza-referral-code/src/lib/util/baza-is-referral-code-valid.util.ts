export function bazaIsReferralCodeValid(input: string): boolean {
    const filtered = (input || '').toString().trim();

    return ! /([^a-zA-Z0-9_!\d])/.test(filtered);
}
