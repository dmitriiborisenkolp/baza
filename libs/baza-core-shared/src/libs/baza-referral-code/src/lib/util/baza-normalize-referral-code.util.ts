export function bazaNormalizeReferralCode(input: string): string {
    return (input || '').toString()
        .toLowerCase()
        .trim()
        .split('')
        .filter((s) => /[a-zA-Z0-9_!\d]/.test(s))
        .join('');
}
