export function bazaNormalizeReferralCodeDisplay(input: string): string {
    return (input || '').toString()
        .trim()
        .split('')
        .filter((s) => /[a-zA-Z0-9_!\d]/.test(s))
        .join('');
}
