export function bazaNormalizeReferralCampaignName(input: string): string {
    return (input || '').toString().trim();
}
