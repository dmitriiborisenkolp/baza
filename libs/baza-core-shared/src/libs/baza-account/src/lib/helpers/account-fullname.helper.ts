import { isEmpty } from '../../../../baza-common/src';

interface AccountLikeEntity {
    firstName: string;
    lastName?: string;
}

// In case if your project has different full name schema, you can inject your own full name generator here.
export const BAZA_ACCOUNT_FULL_NAME_GENERATOR = {
    generate: (account: AccountLikeEntity /* For your generator you can use different type */) => {
        if (! isEmpty(account.lastName)) {
            return `${account.firstName.trim()} ${account.lastName.trim()}`;
        } else if (! isEmpty(account.firstName)) {
            return account.firstName.trim();
        } else {
            return '–';
        }
    },
};

export function accountFullName(account: AccountLikeEntity): string {
    return BAZA_ACCOUNT_FULL_NAME_GENERATOR.generate(account);
}
