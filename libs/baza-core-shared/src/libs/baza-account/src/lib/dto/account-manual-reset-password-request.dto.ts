export class AccountManualResetPasswordRequestDto {
    subject: string;
    message: string;
    attemptToSignInMessage: string;
}
