export const bazaAccountApiEnI18n: Record<string, { subject: string }> = {
    'confirm-email': {
        subject: `{{ clientName }}: Confirm Your Email Address`,
    },
    'password-changed': {
        subject: '{{ clientName }}: Password Updated',
    },
    'reset-password': {
        subject: '{{ clientName }}: Reset Password',
    },
    'already-registered': {
        subject: 'You already have a {{ project }} account',
    },
    'change-email': {
        subject: `Change your email address for {{ project }}`,
    },
    'change-email-notification': {
        subject: `Change email address requested for {{ project }}`,
    },
    'account-deactivated': {
        subject: `Your {{ project }} account has been deleted`,
    },
    'deactivate-account': {
        subject: `Confirm your delete account request for {{ project }}`,
    },
};
