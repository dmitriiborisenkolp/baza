import { accountAclI18n } from '../../acl/account.acl';

export const bazaAccountAclI18nEn = {
    account: {
        ...accountAclI18n,
    },
};
