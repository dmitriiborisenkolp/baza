export const bazaAccountI18nEn: Record<string, unknown> = {
    account: {
        title: 'Accounts',
        components: {
            bazaAccount: {
                title: 'Accounts',
                newAdministrator: 'Add administrator',
                newUser: 'Add user',
                users: 'Users',
                admins: 'Administrators',
                resetPasswordForAllUsers: 'Reset passwords for ALL users',
                fields: {
                    id: 'ID',
                    email: 'Email',
                    fullName: 'Full Name',
                    firstName: 'First Name',
                    lastName: 'Last Name',
                    role: 'Role',
                    dateCreated: 'Date Created',
                    dateUpdated: 'Date Updated',
                    lastSignIn: 'Latest Sign In',
                    isEmailConfirmed: 'Email Confirmed?',
                    signedUpWithClient: 'Signed Up With Client',
                },
                actions: {
                    delete: 'Delete account',
                    deactivate: 'Deactivate account',
                    assign: 'Assign admin access',
                    revoke: 'Revoke admin access',
                    setAcl: 'Set ACL',
                    updateAccount: 'Edit account',
                    confirmAccount: 'Confirm email',
                    unConfirmAccount: "Mark account's email as not confirmed",
                    exportToCsv: 'Export accounts to CSV',
                    resetPassword: 'Send request to reset password',
                    changeEmail: 'Change Email',
                    sessions: 'Sessions',
                    deviceTokens: 'Device tokens',
                },
                delete: {
                    message: 'Do you really want to remove account?',
                },
                deactivate: {
                    message: 'Do you really want to deactivate account?',
                },
                revoke: {
                    message: 'Do you really want to revoke administrator access from account?',
                },
                assign: {
                    message: 'Do you really want to assign administrator access to account?',
                },
                forms: {
                    fields: {
                        email: 'Email',
                        firstName: 'First name',
                        lastName: 'Last name',
                        phone: 'Phone',
                        password: 'Password',
                        acl: 'ACL',
                        profileImages: 'Profile images',
                    },
                    addAdministrator: {
                        title: 'Add administrator',
                        common: 'Account',
                        acl: 'ACL',
                        placeholders: {
                            password: 'Leave empty to generate random password',
                        },
                    },
                    addUser: {
                        title: 'Add user',
                        common: 'Account',
                        acl: 'ACL',
                        placeholders: {
                            password: 'Leave empty to generate random password',
                        },
                    },
                    setAcl: {
                        title: 'Set ACL',
                    },
                    updateAccount: {
                        title: 'Edit account',
                        placeholders: {
                            password: 'Fill new password if you want to reset password',
                        },
                    },
                    resetPassword: {
                        title: 'Request for password reset',
                        fields: {
                            subject: 'Subject',
                            message: 'Message',
                            attemptToSignInErrorMessage: 'Error message for sign in attempts',
                        },
                    },
                    resetPasswordForAllUsers: {
                        title: 'Request for password reset for ALL users',
                        fields: {
                            subject: 'Subject',
                            message: 'Message',
                            attemptToSignInErrorMessage: 'Error message for sign in attempts',
                        },
                    },
                    changeEmail: {
                        title: 'Change Email',
                        fields: {
                            fullName: 'Full Name',
                            currentEmail: 'Current Email',
                            email: 'New Email',
                        },
                    },
                },
            },
        },
    },
};
