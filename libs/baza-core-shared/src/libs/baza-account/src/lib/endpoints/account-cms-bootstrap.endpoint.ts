import { Observable } from 'rxjs';

export enum AccountCmsBootstrapEndpointPaths {
    bootstrap = '/baza-account/cms/bootstrap',
}

export interface AccountCmsBootstrapEndpoint {
    bootstrap(): Promise<void> | Observable<void>;
}
