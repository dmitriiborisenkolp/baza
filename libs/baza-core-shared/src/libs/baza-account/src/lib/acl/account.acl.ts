export enum AccountAcl {
    Accounts = 'Accounts',
    AccountsCreate = 'AccountsCreate',
    AccountsAdminCreate = 'AccountsAdminCreate',
    AccountsAssign = 'AccountsAssign',
    AccountsRevoke = 'AccountsRevoke',
    AccountsDelete = 'AccountsDelete',
    AccountsUpdate = 'AccountsUpdate',
    AccountsActivate = 'AccountsActivate',
    AccountsResetPassword = 'AccountsResetPassword',
    AccountsDeactivate = 'AccountsDeactivate',
    AccountsChangeEmail = 'AccountsChangeEmail',
    AccountsAuthSession = 'AccountsAuthSession',
    AccountsDeviceTokens = 'AccountsDeviceTokens',
}

export const accountAclI18n = {
    group: 'Accounts & ACL',
    nodes: {
        [AccountAcl.Accounts]: 'Accounts',
        [AccountAcl.AccountsCreate]: 'Create user account',
        [AccountAcl.AccountsAdminCreate]: 'Create administrator account',
        [AccountAcl.AccountsAssign]: 'Assign administrator access to account',
        [AccountAcl.AccountsRevoke]: 'Revoke administrator access to account',
        [AccountAcl.AccountsDelete]: 'Delete Account',
        [AccountAcl.AccountsUpdate]: 'Edit Account',
        [AccountAcl.AccountsActivate]: 'Activate Account',
        [AccountAcl.AccountsResetPassword]: 'Reset Password',
        [AccountAcl.AccountsDeactivate]: 'Deactivate Account',
        [AccountAcl.AccountsChangeEmail]: 'Change Email',
        [AccountAcl.AccountsAuthSession]: 'Sessions',
        [AccountAcl.AccountsDeviceTokens]: 'Device tokens',
    },
};
