export enum ConfirmationEmailExpiryResendStrategy {
    AUTO = 'AUTO',
    MANUAL = 'MANUAL',
}
