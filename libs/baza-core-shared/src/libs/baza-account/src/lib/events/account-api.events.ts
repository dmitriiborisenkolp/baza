import { AccountSettingsDto } from '../dto/account-settings.dto';
import { AccountRole } from '../models/account-role';

export enum AccountApiEvent {
    BazaAccountAclUpdated = 'BazaAccountAclUpdated',
    BazaAccountRegistered = 'BazaAccountRegistered',
    BazaAccountConfirmEmailSent = 'BazaAccountConfirmEmailSent',
    BazaAccountConfirmedEmail = 'BazaAccountConfirmedEmail',
    BazaAccountUnConfirmedEmail = 'BazaAccountUnConfirmedEmail',
    BazaAccountResetPasswordSent = 'BazaAccountResetPasswordSent',
    BazaAccountResetPasswordComplete = 'BazaAccountResetPasswordComplete',
    BazaAccountChangeEmailSent = 'BazaAccountChangeEmailSent',
    BazaAccountEmailChanged = 'BazaAccountEmailChanged',
    BazaAccountPasswordChanged = 'BazaAccountPasswordChanged',
    BazaAccountSettingsUpdated = 'BazaAccountSettingsUpdated',
    BazaAccountDeleted = 'BazaAccountDeleted',
    BazaAccountDeactivated = 'BazaAccountDeactivated',
    BazaAccountDeactivateRequestSent = 'BazaAccountDeactivateRequestSent',
    BazaAccountUpdated = 'BazaAccountUpdated',
    BazaAccountAdminAccessAssigned = 'BazaAccountAdminAccessAssigned',
    BazaAccountAdminAccessRevoked = 'BazaAccountAdminAccessRevoked',
}

export type AccountApiEvents =
    BazaAccountRegistered |
    BazaAccountConfirmEmailSent |
    BazaAccountConfirmedEmail |
    BazaAccountUnConfirmedEmail |
    BazaAccountResetPasswordSent |
    BazaAccountResetPasswordComplete |
    BazaAccountChangeEmailSent |
    BazaAccountEmailChanged |
    BazaAccountPasswordChanged |
    BazaAccountSettingsUpdated |
    BazaAccountUpdated |
    BazaAdminAccessAssigned |
    BazaAdminAccessRevoked |
    BazaAccountAclUpdated |
    BazaAccountDeleted |
    BazaAccountDeactivated |
    BazaAccountDeactivateRequestSent
;

export interface BazaAccountRegistered { topic: AccountApiEvent.BazaAccountRegistered, payload: { accountId: number, accountEmail: string; accountRole: AccountRole; isRetryAttempt: boolean; firstName: string; lastName: string; } }
export interface BazaAccountConfirmEmailSent { topic: AccountApiEvent.BazaAccountConfirmEmailSent, payload: { accountId: number, accountEmail: string, accountConfirmEmailRequestId: number, token: string } }
export interface BazaAccountConfirmedEmail { topic: AccountApiEvent.BazaAccountConfirmedEmail, payload: { accountId: number, accountEmail: string, accountConfirmEmailRequestId?: number, token?: string } }
export interface BazaAccountUnConfirmedEmail { topic: AccountApiEvent.BazaAccountUnConfirmedEmail, payload: { accountId: number, accountEmail: string, accountConfirmEmailRequestId?: number, token?: string } }
export interface BazaAccountResetPasswordSent { topic: AccountApiEvent.BazaAccountResetPasswordSent, payload: { accountId: number, accountEmail: string, accountResetPasswordRequestId: number, token: string } }
export interface BazaAccountDeactivateRequestSent { topic: AccountApiEvent.BazaAccountDeactivateRequestSent, payload: { accountId: number, accountEmail: string, accountDeactivateRequestId: number, token: string } }
export interface BazaAccountResetPasswordComplete { topic: AccountApiEvent.BazaAccountResetPasswordComplete, payload: { accountId: number, accountEmail: string, accountResetPasswordRequestId: number, token: string } }
export interface BazaAccountChangeEmailSent { topic: AccountApiEvent.BazaAccountChangeEmailSent, payload: { accountId: number, accountEmail: string, accountChangeEmailRequestId: number, token: string; newEmail: string } }
export interface BazaAccountEmailChanged { topic: AccountApiEvent.BazaAccountEmailChanged, payload: { accountId: number, accountEmail: string; previousEmail: string } }
export interface BazaAccountPasswordChanged { topic: AccountApiEvent.BazaAccountPasswordChanged, payload: { accountId: number, accountEmail: string } }
export interface BazaAccountSettingsUpdated<T = AccountSettingsDto> { topic: AccountApiEvent.BazaAccountSettingsUpdated, payload: { accountId: number, accountEmail: string, previousSettings: T, newSettings: T } }
export interface BazaAccountUpdated { topic: AccountApiEvent.BazaAccountUpdated, payload: { accountId: number, accountEmail: string } }
export interface BazaAdminAccessAssigned { topic: AccountApiEvent.BazaAccountAdminAccessAssigned, payload: { accountId: number, accountEmail: string } }
export interface BazaAdminAccessRevoked { topic: AccountApiEvent.BazaAccountAdminAccessRevoked, payload: { accountId: number, accountEmail: string } }
export interface BazaAccountAclUpdated<T = string> { topic: AccountApiEvent.BazaAccountAclUpdated, payload: { accountId: number, accountEmail: string, previousAcl: Array<T>, newAcl: Array<T> } }
export interface BazaAccountDeleted { topic: AccountApiEvent.BazaAccountDeleted, payload: { accountId: number, accountEmail: string } }
export interface BazaAccountDeactivated { topic: AccountApiEvent.BazaAccountDeactivated, payload: { accountId: number, accountEmail: string } }
