export enum BazaMailCustomerioLinkErrorCodes {
    BazaMailCustomerioLinkUnknownTemplate = 'BazaMailCustomerioLinkUnknownTemplate',
    BazaMailCustomerioLinkIsNotAvailable = 'BazaMailCustomerioLinkIsNotAvailable',
}

export const bazaMailCustomerioLinkErrorCodesI18nEn = {
    [BazaMailCustomerioLinkErrorCodes.BazaMailCustomerioLinkUnknownTemplate]:
        'Mail Template "{{ name }}" was not linked with CustomerIO yet',
    [BazaMailCustomerioLinkErrorCodes.BazaMailCustomerioLinkIsNotAvailable]:
        'Mail Template "{{ name }}" is not linked with Customer.IO yet',
};
