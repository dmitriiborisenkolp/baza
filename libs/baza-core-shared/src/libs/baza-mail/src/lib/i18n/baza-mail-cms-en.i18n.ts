export const bazaMailCmsEnI18n = {
    mail: {
        messages: {
            customerIoIsNotReady:
                'There are missing configuration for Customer.IO Template. Please go to Customer.IO CMS section and Link Mail Templates with Customer.IO',
        },
        customerioLink: {
            title: 'Customer.IO',
            components: {
                tags: {
                    title: 'Customer.IO',
                    navigation: {
                        customerio: 'Customer.IO',
                        tags: 'Tags',
                    },
                    columns: {
                        tag: 'Mail Templates Group',
                        templates: 'Templates',
                    },
                },
                templates: {
                    title: `Customer.IO – {{ tag }}`,
                    navigation: {
                        customerio: 'Customer.IO',
                        tags: 'Tags',
                    },
                    legend: {
                        noCustomerIo: 'Mail Template is not linked yet with Customer.IO',
                        hasCustomerIo: 'Mail Template is ready to use',
                    },
                    columns: {
                        title: 'Template Name',
                        description: 'Description',
                        name: 'ID',
                        customerioId: 'Customer.IO ID',
                    },
                    actions: {
                        link: 'Link Template with Customer.IO',
                        documentation: 'Documentation',
                        unlink: {
                            title: 'Unlink Template from Customer.IO',
                            message: 'Do you really want to remove "{{ name }}" link with Customer.IO?',
                            success: 'Template "{{ name }}" unlinked',
                        },
                    },
                    modals: {
                        link: {
                            title: '{{ name }}',
                            fields: {
                                customerIoId: 'Customer.IO Template Id',
                            },
                            hints: {
                                customerIoId: 'Displayed on Transaction Page in Customer.IO',
                            },
                        },
                    },
                },
            },
        },
    },
};
