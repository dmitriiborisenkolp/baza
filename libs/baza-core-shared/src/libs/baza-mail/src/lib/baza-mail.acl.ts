export enum BazaMailACL {
    BazaMailCustomerIoLink = 'BazaMailCustomerIoLink',
    BazaMailCustomerIoLinkManagement = 'BazaMailCustomerIoLinkManagement',
}

export const bazaMailAclI18n = {
    mail: {
        group: 'Mail',
        nodes: {
            [BazaMailACL.BazaMailCustomerIoLink]: 'Customer.Io Links',
            [BazaMailACL.BazaMailCustomerIoLinkManagement]: 'Customer.Io Links – Management',
        },
    },
};
