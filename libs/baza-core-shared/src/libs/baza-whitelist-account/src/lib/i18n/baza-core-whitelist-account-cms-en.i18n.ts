

export const bazaCoreWhitelistAccountCmsEnI18n = {
    whitelistAccount: {
        title: 'Whitelisted Accounts',
        components: {
            bazaCoreWhitelistAccountCms: {
                title: 'Whitelisted Accounts',
                search: 'Search',
                fields: {
                    id: 'Id',
                    email: 'Email',
                    dateCreated: 'Created At',
                },
                placeholders: {
                    email: 'Email for subscribed user',
                },
                actions: {
                    add: 'Add Account',
                    importCsv: {
                        title: 'Import from CSV',
                        hint: 'CSV should be a single column with header Email.',
                        success: 'Accounts imported successfully.',
                    },
                    remove: {
                        title: 'Remove account',
                        confirm: 'Do you really want to remove {{ email }} from Whitelisted Accounts list?'
                    }
                },
            },
        },
    }
};

