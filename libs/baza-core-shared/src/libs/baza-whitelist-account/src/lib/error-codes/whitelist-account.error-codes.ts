export enum BazaWhitelistAccountErrorCodes {
    BazaWhitelistAccountAlreadyExists = 'BazaWhitelistAccountAlreadyExists',
    BazaWhitelistAccountNotFound = 'BazaWhitelistAccountNotFound',
    BazaWhitelistAccountInvalidCsv = 'BazaWhitelistAccountInvalidCsv',
    WhitelistAccountCsvMimeTypeMismatch = 'WhitelistAccountCsvMimeTypeMismatch',
    WhitelistAccountAccessDeniedException = 'WhitelistAccountAccessDeniedException'
}

export const bazaWhitelistAccountErrorCodesI18n = {
    [BazaWhitelistAccountErrorCodes.BazaWhitelistAccountAlreadyExists]: 'Account with given email already exists.',
    [BazaWhitelistAccountErrorCodes.BazaWhitelistAccountNotFound]: 'Account with given email does not exists.',
};
