export function isEmpty(input: unknown): boolean {
    return input === null || input === undefined || (input || '').toString().trim().length === 0;
}
