export function isId(input: unknown): boolean {
    return typeof input === 'number' && !isNaN(input) && input > 0 && Math.floor(input) === input;
}
