/**
 * @deprecated
 * @param timeoutMs
 * @returns {Promise<unknown>}
 */
export async function awaitTimeout(timeoutMs: number) {
    return new Promise((resolve) => {
        setTimeout(() => resolve(undefined), timeoutMs);
    });
}
