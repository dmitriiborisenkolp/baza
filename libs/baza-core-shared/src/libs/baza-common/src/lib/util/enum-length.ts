/**
 * Returns length of max item in Enum
 * Should be used for `char` type in postgres definitions
 * @param enumInput
 */
export function enumLength(enumInput: Record<string, string>): number {
    // eslint-disable-next-line prefer-spread
    return Math.max.apply(
        Math,
        Object.values(enumInput).map((next) => next.length),
    );
}
