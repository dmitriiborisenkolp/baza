export function isBazaErrorResponse(input: unknown): boolean {
    return !!input && typeof input === 'object' && 'code' in input && 'message' in input && 'statusCode' in input;
}
