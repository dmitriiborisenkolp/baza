import { maskString } from './mask-string.util';

describe('@scaliolabs/baza-core-shared/util/mask-string.util.spec.ts', () => {
    it('will correctly mask strings', async () => {
        const cases: Array<[string, unknown]> = [
            ['', ''],
            [' ', ''],
            ['  ', ''],
            ['   ', ''],
            ['1', '*'],
            [' 1', '*'],
            ['1 ', '*'],
            [' 1 ', '*'],
            ['12', '**'],
            ['123', '***'],
            ['1234', '****'],
            ['1234 ', '****'],
            [' 1234', '****'],
            ['12345', '*2345'],
            ['123456789', '*****6789'],
        ];

        for (const testCase of cases) {
            expect(maskString(testCase[0])).toBe(testCase[1]);
        }
    });

    it('will correctly handle null and undefined cases', async () => {
        expect(maskString(undefined)).toBeUndefined();
        expect(maskString(null)).toBeNull();
    });
});
