/**
 * Shifts a month index to a 1-12 range.
 * @param startingMonth The original month index (0-based).
 * @param offset The number of months to shift by.
 * @returns The shifted month index (1-based).
 */
export function shiftMonth(startingMonth: number, offset: number): number {
    return ((startingMonth - 1 + (offset % 12) + 12) % 12) + 1;
}
