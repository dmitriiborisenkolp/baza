/**
 * Returns true if input string is valid SSN
 * This solution allows to pass SSN as `104-50-0034`, `10450-0034`, `104-500034` or `104500034`
 * @see https://stackoverflow.com/questions/68672456/regex-expression-for-validating-ssn-with-exceptions
 * @param ssn
 */
export function isValidSSN(ssn: string): boolean {
    return /^(?!000|666|9\d\d)\d{3}-?(?!00)\d\d-?\d{4}$/.test(ssn);
}
