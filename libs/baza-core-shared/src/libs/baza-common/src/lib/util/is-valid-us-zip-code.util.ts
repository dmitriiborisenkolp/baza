/**
 * Returns true if ZIP code is valid
 * @param zipCode
 */
export function isValidUsZipCodeUtil(zipCode: string): boolean {
    return /(^\d{5}$)|(^\d{5}-\d{4}$)/.test(zipCode);
}
