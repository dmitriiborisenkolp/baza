export interface BazaError<T = any> {
    code: T;
    message: string;
    statusCode: number;
}
