export interface UploadedFile {
    fieldname: string;
    originalname: string;
    encoding: string;
    mimetype: string;
    size: number;
    destination: string;
    location: string;
    filename: string;
    path: string;
    buffer: Buffer;
}
