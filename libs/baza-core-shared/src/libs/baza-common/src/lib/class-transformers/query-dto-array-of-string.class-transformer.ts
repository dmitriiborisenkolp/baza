/**
 * Converts "example=Foo,Bar" to ['Foo', 'Bar'] value
 */
export const queryDtoArrayOfString = ({ value }) => {
    if (value && typeof value === 'string' && value.trim().length > 0) {
        return value.split(',');
    } else if (Array.isArray(value) && value.length > 0) {
        return value;
    } else {
        return undefined;
    }
};
