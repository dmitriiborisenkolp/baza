/**
 * ULID Length
 * Use it for @PrimaryColumn definitions:
 *
 * @PrimaryColumn({
 *      type: 'varchar',
 *      length: ULID_LENGTH,
 *  })
 */
export const ULID_LENGTH = 26;
