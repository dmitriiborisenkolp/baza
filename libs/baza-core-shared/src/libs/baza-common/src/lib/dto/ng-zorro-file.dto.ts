export interface NgZorroFileDto {
    uid: string;
    size?: number;
    name: string;
    filename?: string;
    url?: string;
    thumbUrl?: string;
    status: 'error' | 'success' | 'done' | 'uploading' | 'removed';
}
