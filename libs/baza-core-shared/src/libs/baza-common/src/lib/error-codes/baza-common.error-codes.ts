export enum BazaCommonErrorCodes {
    BazaInternalServerError = 'InternalServerError',
    BazaJSError = 'BazaJSError',
    BazaValidationError = 'BazaValidationError',
    BazaCoreEndpointUrlWasNotFound = 'BazaCoreEndpointUrlWasNotFound',
    BazaEndpointBlacklisted = 'BazaEndpointBlacklisted',
    BazaFeatureDisabled = 'BazaFeatureDisabled',
    BazaSessionIsLocked = 'BazaSessionIsLocked',
    BazaSessionUpdateIdMismatch = 'BazaSessionIdMismatch',
}
