export enum CoreOpenApiTags {
    BazaAccountCMS = 'Baza Account CMS',
    BazaAccount = 'Baza Account',
    BazaAws = 'Baza AWS',
    BazaAttachment = 'Baza Attachment',
    BazaACL = 'Baza ACL',
    BazaAuth = 'Baza Auth',
    BazaAuth2FA = 'Baza Auth 2FA',
    BazaAuth2FASettings = 'Baza Auth 2FA Settings',
    BazaPassword = 'Baza Password',
    BazaAuthMasterPassword = 'Baza Auth Master Password',
    BazaStatus = 'Baza Status',
    BazaVersion = 'Baza Version',
    BazaI18n = 'Baza I18n',
    BazaPhoneCountryCodes = 'Baza Phone Country Codes',
    BazaMaintenance = 'Baza Maintenance',
    BazaRegistry = 'Baza Registry',
    BazaE2e = 'Baza E2E',
    BazaE2eFixtures = 'Baza E2E Fixtures',
    BazaCreditCard = 'Baza Credit Card',
    BazaWhitelistAccount = 'Baza Whitelist Account',
    BazaAuthSessionCMS = 'Baza Auth Sessions CMS',
    BazaInviteCode = 'Baza Invite Code',
    BazaInviteCodeCMS = 'Baza Invite Code',
    BazaReferralCode = 'Baza Referral Code',
    BazaReferralCodeCMS = 'Baza Referral Code',
    BazaHealthCheck = 'Baza Health Check',
    BazaMail = 'Baza Mail',
    BazaMailCustomerIoLinkCMS = 'Baza Mail CustomerIO Link CMS',
}
