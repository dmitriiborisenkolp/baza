export class KafkaEvent<TOPIC, PAYLOAD> {
    topic: TOPIC;
    payload: PAYLOAD;
}
