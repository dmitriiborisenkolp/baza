export const bazaRegistryCmsEnI18n = {
    registry: {
        title: 'Registry',
        components: {
            bazaRegistryCms: {
                title: 'Registry',
                columns: {
                    name: 'Name',
                    value: 'Value',
                    public: 'Available via API?',
                },
                actions: {
                    edit: 'Edit',
                },
                forms: {
                    fields: {
                        value: 'Value',
                        hint: 'Default value: {{ defaults }}',
                        hint_with_node_hint: '{{ hint }}. Default value: {{ defaults }}',
                        hint_with_node_hint_no_defaults: '{{ hint }}',
                    },
                    boolean: {
                        yes: 'Yes',
                        no: 'No',
                    },
                    actions: {
                        save: 'Save',
                        cancel: 'Cancel',
                        setDefaults: 'Set defaults',
                    },
                }
            },
        },
    },
};
