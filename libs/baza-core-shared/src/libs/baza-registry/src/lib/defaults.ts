import { RegistrySchema, RegistryType } from './models/registry.model';

export function defaultsRegistrySchema(): RegistrySchema {
    return {
        api: {
            requestLogDebugMode: {
                name: 'Enable Full Request Debug Log (Kibana)',
                type: RegistryType.Boolean,
                public: false,
                defaults: false,
                hiddenFromList: false,
            },
        },
    };
}
