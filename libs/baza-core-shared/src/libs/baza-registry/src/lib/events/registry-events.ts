export enum RegistryApiEvent {
    BazaRegistryNodeUpdated = 'BazaRegistryNodeUpdated',
}

export type RegistryApiEvents = { topic: RegistryApiEvent.BazaRegistryNodeUpdated; payload: RegistryApiEventsRegistryNodeUpdated };

export interface RegistryApiEventsRegistryNodeUpdated<T = unknown> {
    path: string;
    label?: string;
    value: T;
}
