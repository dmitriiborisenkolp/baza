export enum BazaRegistryErrorCodes {
    BazaRegistryRecordNotFound = 'BazaRegistryRecordNotFound',
    BazaRegistryNodeNotFound = 'BazaRegistryNodeNotFound',
}
