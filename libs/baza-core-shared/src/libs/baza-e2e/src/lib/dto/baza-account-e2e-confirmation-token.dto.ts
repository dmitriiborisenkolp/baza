import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';

export class BazaAccountE2eConfirmationTokenDto {
    @ApiModelProperty({
        description: 'Confirmation Token',
    })
    token: string;
}
