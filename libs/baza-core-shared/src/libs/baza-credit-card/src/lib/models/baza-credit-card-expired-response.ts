export enum BazaCreditCardExpiredResponse {
    Valid = 'Valid',
    Expired = 'Expired',
    OutOfRange = 'OutOfRange',
}
