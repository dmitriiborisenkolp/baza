import 'reflect-metadata';
import { BazaCreditCardDto } from '../dto/baza-credit-card.dto';
import { bazaCreditCardDetect } from './baza-credit-card-detect';
import { BazaCreditCardType } from '../models/baza-credit-card-type';
import { bazaCreditCardIsExpired } from './baza-credit-card-is-expired';
import { BazaCreditCardExpiredResponse } from '../models/baza-credit-card-expired-response';

describe('@scaliolabs-core/baza-credit-card/shared/util/baza-credit-card-detect.spec.ts', () => {
    const testCases = {
        '4916338506082832': BazaCreditCardType.Visa,
        '4556015886206505': BazaCreditCardType.Visa,
        '4539048040151731': BazaCreditCardType.Visa,
        '4024007198964305': BazaCreditCardType.Visa,
        '4716175187624512': BazaCreditCardType.Visa,

        '5280934283171080': BazaCreditCardType.MasterCard,
        '5456060454627409': BazaCreditCardType.MasterCard,
        '5331113404316994': BazaCreditCardType.MasterCard,
        '5259474113320034': BazaCreditCardType.MasterCard,
        '5442179619690834': BazaCreditCardType.MasterCard,

        '6011894492395579': BazaCreditCardType.Discover,
        '6011388644154687': BazaCreditCardType.Discover,
        '6011880085013612': BazaCreditCardType.Discover,
        '6011652795433988': BazaCreditCardType.Discover,
        '6011375973328347': BazaCreditCardType.Discover,
    };

    it('detect credit cards correctly', () => {
        for (const cardNumber of Object.keys(testCases)) {
            expect(bazaCreditCardDetect(cardNumber)).not.toBeFalsy();
            expect((bazaCreditCardDetect(cardNumber) as BazaCreditCardDto).type).toBe(testCases[`${cardNumber}`]);
        }
    });

    it('correctly works with LUHN check', () => {
        expect((bazaCreditCardDetect('6011894492395579') as BazaCreditCardDto).isCreditCardNumberValid).toBeTruthy();
        expect((bazaCreditCardDetect('6011894492395578') as BazaCreditCardDto).isCreditCardNumberValid).toBeFalsy();
    });

    it('check credit card expire data was valid', () => {
        expect(bazaCreditCardIsExpired(1, parseInt((new Date().getUTCFullYear() + 1).toString().slice(2)))).toBe(
            BazaCreditCardExpiredResponse.Valid,
        );
    });

    it('check credit card expire data was expired', () => {
        expect(bazaCreditCardIsExpired(1, parseInt((new Date().getUTCFullYear() - 1).toString().slice(2)))).toBe(
            BazaCreditCardExpiredResponse.Expired,
        );
    });
});
