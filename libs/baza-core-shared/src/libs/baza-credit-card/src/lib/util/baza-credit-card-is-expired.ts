import { BazaCreditCardExpiredResponse } from '../models/baza-credit-card-expired-response';
import { BazaCreditCardMonth } from '../models/baza-credit-card-month';

export function bazaCreditCardIsExpired(creditCardExpireMonth: BazaCreditCardMonth, creditCardExpireYear: number): BazaCreditCardExpiredResponse {
    const isRequestInRanges =
        Math.floor(creditCardExpireYear) === creditCardExpireYear
        && Math.floor(creditCardExpireMonth) === creditCardExpireMonth
        && creditCardExpireYear >= 0
        && creditCardExpireYear <= 99
        && creditCardExpireMonth >= 1
        && creditCardExpireMonth <= 12;

    if (! isRequestInRanges) {
        return BazaCreditCardExpiredResponse.OutOfRange;
    }

    const now = new Date();
    const card = new Date(parseInt(`20${creditCardExpireYear}`), creditCardExpireMonth - 1);

    if (card.getFullYear() === now.getFullYear() ? card.getMonth() >= now.getMonth() : card.getFullYear() >= now.getFullYear()) {
        return BazaCreditCardExpiredResponse.Valid;
    } else {
        return BazaCreditCardExpiredResponse.Expired;
    }
}
