import { BazaAuthSessionRecordType } from './baza-auth-session-record-type';

export class BazaAuthSessionsIncrement {
    accountId: number;
    ip: string;
    userAgent: string;
    type: BazaAuthSessionRecordType;
}
