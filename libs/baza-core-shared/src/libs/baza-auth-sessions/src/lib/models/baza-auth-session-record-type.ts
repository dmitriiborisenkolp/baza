export enum BazaAuthSessionRecordType {
    Attempt = 'attempts',
    SignIn = 'sign-in',
}
