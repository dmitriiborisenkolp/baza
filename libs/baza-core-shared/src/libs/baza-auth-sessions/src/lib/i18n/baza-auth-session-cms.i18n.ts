export const bazaAuthSessionCmsI18n = {
    authSessions: {
        title: 'Sessions',
        _: {
            types: {
                BazaAuthSessionRecordType: {
                    'attempts': 'Attempt to Sign In',
                    'sign-in': 'Sign In',
                },
            },
        },
        components: {
            accounts: {
                title: 'Sessions',
                columns: {
                    id: 'ID',
                    user: 'User',
                    lastActive: 'Last Active',
                    createdAt: 'Created',
                    authCountAttempts: 'Auth Attempts',
                    authCountSignIn: 'Success Auth',
                    details: 'Details',
                },
            },
            sessions: {
                title: 'Sessions for {{ fullName }} (ID: {{ id }})',
                sessions: 'Sessions',
                columns: {
                    id: 'ID',
                    ip: 'IP',
                    type: 'Type',
                    date: 'Date',
                    os: 'OS',
                    browser: 'Browser',
                    userAgent: 'User Agent',
                },
            },
        },
    },
};
