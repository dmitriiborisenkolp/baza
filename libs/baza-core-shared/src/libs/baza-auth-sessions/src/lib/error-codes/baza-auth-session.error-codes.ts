export enum BazaAuthSessionErrorCodes {
    BazaAuthSessionNotFound = 'BazaAuthSessionNotFound',
    BazaAuthSessionAccountNotFound = 'BazaAuthSessionAccountNotFound',
}

export const bazaAuthSessionErrorCodesI18n = {
    [BazaAuthSessionErrorCodes.BazaAuthSessionNotFound]: 'Auth session was not found',
    [BazaAuthSessionErrorCodes.BazaAuthSessionAccountNotFound]: 'Account not found',
};
