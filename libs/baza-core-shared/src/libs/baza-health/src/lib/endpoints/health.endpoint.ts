import { Observable } from 'rxjs';
import { HealthDto } from '../dto/health.dto';

export enum BazaHealthCheckEndpointPaths {
    check = '/health',
}

export interface HealthCheckEndpoint {
    check(...args: unknown[]): Promise<BazaHealthCheckResponse> | Observable<BazaHealthCheckResponse>;
}
export type BazaHealthCheckResponse = HealthDto;
