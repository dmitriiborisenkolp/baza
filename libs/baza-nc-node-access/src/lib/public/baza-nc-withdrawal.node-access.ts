import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcWithdrawEndpointPaths,
    BazaNcWithdrawEndpoint,
    BazaNcWithdrawRequest,
    BazaNcWithdrawReprocessRequest,
    BazaNcWithdrawListRequest,
    BazaNcWithdrawListResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaDwollaPaymentDto } from '@scaliolabs/baza-dwolla-shared';

/**
 * Data-Access for Baza NC Withdraw
 */
export class BazaNcWithdrawalNodeAccess implements BazaNcWithdrawEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of withdraw requests
     * @param request
     */
    async list(request: BazaNcWithdrawListRequest): Promise<BazaNcWithdrawListResponse> {
        return this.http.get(BazaNcWithdrawEndpointPaths.list, request);
    }

    /**
     * Withdraw available balance
     * Withdrawing available balance from Dwolla account to an external bank account
     * @param request
     */
    withdraw(request: BazaNcWithdrawRequest): Promise<BazaDwollaPaymentDto> {
        return this.http.post(BazaNcWithdrawEndpointPaths.withdraw, request);
    }

    /**
     * Reprocess failed withdrawals
     * Will reprocess the failed withdrawal request
     * @param request
     */
    reprocess(request: BazaNcWithdrawReprocessRequest): Promise<BazaDwollaPaymentDto> {
        return this.http.post(BazaNcWithdrawEndpointPaths.reprocess, request, {
            asJsonResponse: false,
        });
    }
}
