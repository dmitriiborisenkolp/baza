import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';
import {
    BazaNcE2eEndpoint,
    BazaNcE2eEndpointPaths,
    BazaNcE2eGetNcAccountRequest,
    BazaNcE2eGetNcPartyRequest,
    BazaNcE2eGetTradeStatusRequest,
    BazaNcE2eGetTradeStatusResponse,
    BazaNcE2eSetFundingSourceNameRequest,
    BazaNcE2eSetTradeStatusRequest,
    BazaNcE2eUpdateCCFundMoveStatusRequest,
    BazaNcE2eUpdateExternalFundMoveStatusRequest,
    BazaNcE2eUpdateTradeStatusRequest,
    BazaNcOfferingDto,
    GetAccountResponse,
    GetPartyResponse,
    NorthCapitalTopic,
} from '@scaliolabs/baza-nc-shared';

/**
 * E2E Helpers to Test NC API
 */
export class BazaNcE2eNodeAccess implements BazaNcE2eEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * E2E Helper - Update trade status
     * @param request
     */
    async updateTradeStatus(request: BazaNcE2eUpdateTradeStatusRequest): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.updateTradeStatus, request, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Returns Trade status
     * @param request
     */
    getTradeStatus(request: BazaNcE2eGetTradeStatusRequest): Promise<BazaNcE2eGetTradeStatusResponse> {
        return this.http.post(BazaNcE2eEndpointPaths.getTradeStatus, request);
    }

    /**
     * E2E Helper - Set trade status (immediately, w/o webhooks)
     * @param request
     */
    async setTradeStatus(request: BazaNcE2eSetTradeStatusRequest): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.setTradeStatus, request, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Enable Mask Configuration (SSN, Credit Card, Bank Account Details)
     */
    async enableMaskConfig(): Promise<void> {
        return this.http.post(
            BazaNcE2eEndpointPaths.enableMaskConfig,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    /**
     * E2E Helper - Disable Mask Configuration (SSN, Credit Card, Bank Account Details)
     */
    async disableMaskConfig(): Promise<void> {
        return this.http.post(
            BazaNcE2eEndpointPaths.disableMaskConfig,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    /**
     * E2E Helper - Create an example NC Offering
     */
    async createExampleOffering(): Promise<BazaNcOfferingDto> {
        return this.http.post(BazaNcE2eEndpointPaths.createExampleOffering);
    }

    /**
     * E2E Helper - Enable Transaction Fee feature
     */
    async enableTransactionFeeFeature(): Promise<void> {
        return this.http.post(
            BazaNcE2eEndpointPaths.enableTransactionFee,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    /**
     * E2E Helper - Disable Transaction Fee feature
     */
    async disableTransactionFeeFeature(): Promise<void> {
        return this.http.post(
            BazaNcE2eEndpointPaths.disableTransactionFee,
            {},
            {
                asJsonResponse: false,
            },
        );
    }

    /**
     * E2E Helper - Update CC Transaction status
     * @param request
     */
    async updateCCFundMoveStatus(request: BazaNcE2eUpdateCCFundMoveStatusRequest): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.updateCCFundMoveStatus, request, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Update ACH Transaction status
     * @param request
     */
    async updateExternalFundMoveStatus(request: BazaNcE2eUpdateExternalFundMoveStatusRequest): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.updateExternalFundMoveStatus, request, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Enable Dwolla Customer Auto-Touch during Dwolla Dividends Processing
     */
    async enableDwollaAutoTouch(): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.enableDwollaAutoTouch, undefined, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Disable Dwolla Customer Auto-Touch during Dwolla Dividends Processing
     */
    async disableDwollaAutoTouch(): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.disableDwollaAutoTouch, undefined, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Set Funding Source Name for Investor's Dwolla Wallet
     * Use DwollaFailureCodes to simulate different issues
     * @see DwollaFailureCodes
     * @param request
     */
    async setFundingSourceName(request: BazaNcE2eSetFundingSourceNameRequest): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.setFundingSourceName, request, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Emulates NC Webhook
     * @param payload
     * @param topic
     */
    async simulateNcWebhook(payload: unknown, topic: NorthCapitalTopic): Promise<void> {
        return this.http.post(replacePathArgs(BazaNcE2eEndpointPaths.simulateNcWebhook, { topic }), payload, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Enables sync between NBA and LBA APIs
     */
    async enableLBASync(): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.enableLBASync, undefined, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Disables sync between NBA and LBA APIs
     */
    async disableLBASync(): Promise<void> {
        return this.http.post(BazaNcE2eEndpointPaths.disableLBASync, undefined, {
            asJsonResponse: false,
        });
    }

    /**
     * E2E Helper - Returns NC Account
     * @param request
     */
    async getNcAccount(request: BazaNcE2eGetNcAccountRequest): Promise<GetAccountResponse> {
        return this.http.post(BazaNcE2eEndpointPaths.getNcAccount, request);
    }

    /**
     * E2E Helper - Returns NC Party
     * @param request
     */
    async getNcParty(request: BazaNcE2eGetNcPartyRequest): Promise<GetPartyResponse> {
        return this.http.post(BazaNcE2eEndpointPaths.getNcParty, request);
    }
}
