import {
    BazaNcCreditCardDto,
    BazaNcPurchaseFlowCreditCardEndpoint,
    BazaNcPurchaseFlowCreditCardEndpointPaths,
    BazaNcPurchaseFlowDeleteCreditCardResponse,
    BazaNcPurchaseFlowGetCreditCardResponse,
    BazaNcPurchaseFlowSetCreditCardRequest,
    BazaNcPurchaseFlowValidateCreditCardRequest,
    BazaNcPurchaseFlowValidateCreditCardResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';

export class BazaNcPurchaseFlowCreditCardNodeAccess implements BazaNcPurchaseFlowCreditCardEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async setCreditCard(request: BazaNcPurchaseFlowSetCreditCardRequest): Promise<BazaNcCreditCardDto> {
        return this.http.post(BazaNcPurchaseFlowCreditCardEndpointPaths.setCreditCard, request);
    }

    async getCreditCard(): Promise<BazaNcPurchaseFlowGetCreditCardResponse> {
        return this.http.post(BazaNcPurchaseFlowCreditCardEndpointPaths.getCreditCard);
    }

    async deleteCreditCard(): Promise<BazaNcPurchaseFlowDeleteCreditCardResponse> {
        return this.http.post(BazaNcPurchaseFlowCreditCardEndpointPaths.deleteCreditCard);
    }

    async validateCreditCard(request: BazaNcPurchaseFlowValidateCreditCardRequest): Promise<BazaNcPurchaseFlowValidateCreditCardResponse> {
        return this.http.post(BazaNcPurchaseFlowCreditCardEndpointPaths.validateCreditCard, request);
    }
}
