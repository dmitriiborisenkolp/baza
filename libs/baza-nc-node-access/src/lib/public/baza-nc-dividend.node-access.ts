import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendDto,
    BazaNcDividendEndpoint,
    BazaNcDividendEndpointPaths,
    BazaNcDividendListRequest,
    BazaNcDividendListResponse,
    BazaNcDividendReprocessRequest,
    BazaNcDividendTotalAmountResponse,
} from '@scaliolabs/baza-nc-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BazaNcDividendNodeAccess implements BazaNcDividendEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: BazaNcDividendListRequest): Promise<BazaNcDividendListResponse> {
        return this.http.post(BazaNcDividendEndpointPaths.list, request);
    }

    async getByUlid(ulid: string): Promise<BazaNcDividendDto> {
        return this.http.get(
            replacePathArgs(BazaNcDividendEndpointPaths.getByUlid, {
                ulid,
            }),
        );
    }

    async totalAmount(): Promise<BazaNcDividendTotalAmountResponse> {
        return this.http.get(BazaNcDividendEndpointPaths.totalAmount);
    }

    async reprocess(request: BazaNcDividendReprocessRequest): Promise<BazaNcDividendDto> {
        return this.http.post(BazaNcDividendEndpointPaths.reprocess, request);
    }
}
