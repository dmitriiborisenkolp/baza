import {
    BazaNcOfferingCmsEndpoint,
    BazaNcOfferingCmsEndpointPaths,
    NcOfferingDetailsRequest,
    NcOfferingDetailsResponse,
    BazaNcOfferingDto,
    NcOfferingForgetRequest,
    NcOfferingGetByIdRequest,
    NcOfferingGetByIdResponse,
    NcOfferingHealthCheckRequest,
    NcOfferingHealthCheckResponse,
    NcOfferingImportRequest,
    NcOfferingListRequest,
    NcOfferingListResponse,
    NcOfferingSyncAllOfferingsResponse,
    BazaNcOfferingListItemDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';

export class BazaNcOfferingCmsNodeAccess implements BazaNcOfferingCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: NcOfferingListRequest): Promise<NcOfferingListResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.list, request);
    }

    async listAll(): Promise<Array<BazaNcOfferingListItemDto>> {
        return this.http.get(BazaNcOfferingCmsEndpointPaths.listAll);
    }

    async getByNcOfferingId(request: NcOfferingGetByIdRequest): Promise<NcOfferingGetByIdResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.getByNcOfferingId, request);
    }

    async ncOfferingDetails(request: NcOfferingDetailsRequest): Promise<NcOfferingDetailsResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.ncOfferingDetails, request);
    }

    async syncAllOfferings(): Promise<NcOfferingSyncAllOfferingsResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.syncAllOfferings);
    }

    async importNcOffering(request: NcOfferingImportRequest): Promise<BazaNcOfferingDto> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.importNcOffering, request);
    }

    async forgetNcOffering(request: NcOfferingForgetRequest): Promise<void> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.forgetNcOffering, request, {
            asJsonResponse: false,
        });
    }

    async healthCheck(request: NcOfferingHealthCheckRequest): Promise<NcOfferingHealthCheckResponse> {
        return this.http.post(BazaNcOfferingCmsEndpointPaths.healthCheck, request);
    }
}
