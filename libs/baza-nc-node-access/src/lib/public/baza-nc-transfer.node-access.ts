import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcTransferEndpoint,
    BazaNcTransferEndpointPaths,
    BazaNcTransferListRequest,
    BazaNcTransferListResponse,
    BazaNcTransferReprocessRequest,
    BazaNcTransferRequest,
} from '@scaliolabs/baza-nc-shared';
import { BazaDwollaPaymentDto } from '@scaliolabs/baza-dwolla-shared';

export class BazaNcTransferNodeAccess implements BazaNcTransferEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of transfers
     * @param request
     */
    async list(request: BazaNcTransferListRequest): Promise<BazaNcTransferListResponse> {
        return this.http.get(BazaNcTransferEndpointPaths.list, request);
    }

    /**
     * Transfer Funds from Bank Account into dwolla balance
     * @param request
     */
    async transfer(request: BazaNcTransferRequest): Promise<BazaDwollaPaymentDto> {
        return this.http.post(BazaNcTransferEndpointPaths.transfer, request);
    }

    /**
     * Transfer Funds from Bank Account into dwolla balance
     * @param request
     */
    async reprocess(request: BazaNcTransferReprocessRequest): Promise<BazaDwollaPaymentDto> {
        return this.http.post(BazaNcTransferEndpointPaths.transfer, request);
    }
}
