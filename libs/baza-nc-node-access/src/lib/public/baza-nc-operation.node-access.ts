import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcOperationEndpoint,
    BazaNcOperationEndpointPaths,
    BazaNcOperationListRequest,
    BazaNcOperationListResponse,
    BazaNcOperationStatsResponse,
} from '@scaliolabs/baza-nc-shared';

/**
 * Node Access service for Baza NC Operations
 */
export class BazaNcOperationNodeAccess implements BazaNcOperationEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of operations. The endpoint returns combined list of
     * purchases, withdraw and transfer operations in single list
     * @param request
     */
    async list(request: BazaNcOperationListRequest): Promise<BazaNcOperationListResponse> {
        const qp: Record<string, unknown> = {
            ...request,
        };

        if (Array.isArray(request.types) && request.types.length) {
            qp.types = request.types.join(',');
        }

        return this.http.get(BazaNcOperationEndpointPaths.list, qp);
    }

    /**
     * Returns Operation Stats for Investor Account
     */
    async stats(): Promise<BazaNcOperationStatsResponse> {
        return this.http.get(BazaNcOperationEndpointPaths.stats);
    }
}
