import {
    BazaNcPurchaseFlowEndpoint,
    BazaNcPurchaseFlowEndpointPaths,
    DestroySessionDto,
    OfferingId,
    PurchaseFlowCurrentDto,
    PurchaseFlowDestroyResponse,
    PurchaseFlowDto,
    PurchaseFlowPersonalInfoDto,
    PurchaseFlowSessionDto,
    PurchaseFlowSubmitResponse,
    ReProcessPaymentDto,
    ReProcessPaymentResponse,
    StatsDto,
    StatsRequestDto,
    SubmitPurchaseFlowDto,
    ValidateResponseDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BazaNcPurchaseFlowNodeAccess implements BazaNcPurchaseFlowEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns current status of Purchase Flow and Purchase Flow DTO, if session is started
     * Also updates TTL of session, if available.
     * @param offeringId
     */
    current(offeringId: OfferingId): Promise<PurchaseFlowCurrentDto> {
        return this.http.get(replacePathArgs(BazaNcPurchaseFlowEndpointPaths.current, { offeringId }));
    }

    /**
     * Stats for Purchase Flow
     * @param request
     */
    stats(request: StatsRequestDto): Promise<StatsDto> {
        return this.http.post(BazaNcPurchaseFlowEndpointPaths.stats, request);
    }

    /**
     * Validates session request before submitting it to North Capital
     * @param request
     */
    validate(request: PurchaseFlowSessionDto): Promise<ValidateResponseDto> {
        return this.http.post(BazaNcPurchaseFlowEndpointPaths.validate, request);
    }

    /**
     * Returns existing session or creates a new purchase flow session
     * @param request
     * @param args
     */
    session(request: PurchaseFlowSessionDto): Promise<PurchaseFlowDto> {
        return this.http.post(BazaNcPurchaseFlowEndpointPaths.session, request);
    }

    /**
     * Submits (Finishes) Purchase. Current Purchase Flow session will be destroyed on successful Submit.
     * @param request
     * @param args
     */
    submit(request: SubmitPurchaseFlowDto): Promise<PurchaseFlowSubmitResponse> {
        return this.http.post(BazaNcPurchaseFlowEndpointPaths.submit, request);
    }

    /**
     * Destroys all existing Purchase Flow Sessions with given parameters
     * @param request
     * @param args
     */
    destroy(request: DestroySessionDto): Promise<PurchaseFlowDestroyResponse> {
        return this.http.post(BazaNcPurchaseFlowEndpointPaths.destroy, request);
    }

    /**
     * Helper method which returns data for Personal Information block
     */
    getPersonalInfo(): Promise<PurchaseFlowPersonalInfoDto> {
        return this.http.post(BazaNcPurchaseFlowEndpointPaths.getPersonalInfo);
    }

    /**
     * Reprocesses Failed Payment
     * @param request
     */
    reprocessPayment(request: ReProcessPaymentDto): Promise<ReProcessPaymentResponse> {
        return this.http.post(BazaNcPurchaseFlowEndpointPaths.reprocessPayment, request);
    }
}
