import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcInvestorAccountEndpoint,
    BazaNcInvestorAccountEndpointPaths,
    BazaNcInvestorAccountDto,
    BazaNcInvestorAccountStatusDto,
    BazaNcPurchaseFlowTransactionType,
} from '@scaliolabs/baza-nc-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BazaNcInvestorAccountNodeAccess implements BazaNcInvestorAccountEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns investor account details
     */
    async current(): Promise<BazaNcInvestorAccountDto> {
        return this.http.get(BazaNcInvestorAccountEndpointPaths.current);
    }

    /**
     * Returns investor account verification statuses and whether is a foreign investor or not
     */
    async status(): Promise<BazaNcInvestorAccountStatusDto> {
        return this.http.get(BazaNcInvestorAccountEndpointPaths.status);
    }

    /**
     * Sets default payment method of Investor
     * @param paymentMethod
     */
    async setDefaultPaymentMethod(paymentMethod: BazaNcPurchaseFlowTransactionType): Promise<void> {
        return this.http.post(
            replacePathArgs(BazaNcInvestorAccountEndpointPaths.setDefaultPaymentMethod, {
                paymentMethod,
            }),
            undefined,
            {
                asJsonResponse: false,
            },
        );
    }
}
