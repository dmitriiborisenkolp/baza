import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcLimitsDto,
    BazaNcPurchaseFlowLimitsEndpoint,
    BazaNcPurchaseFlowLimitsEndpointPaths,
    BazaNcPurchaseFlowLimitsForPurchaseRequest,
    BazaNcPurchaseFlowLimitsForPurchaseResponse,
} from '@scaliolabs/baza-nc-shared';

export class BazaNcPurchaseFlowLimitsNodeAccess implements BazaNcPurchaseFlowLimitsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    limits(): Promise<BazaNcLimitsDto> {
        return this.http.post(BazaNcPurchaseFlowLimitsEndpointPaths.limits);
    }

    limitsForPurchase(request: BazaNcPurchaseFlowLimitsForPurchaseRequest): Promise<BazaNcPurchaseFlowLimitsForPurchaseResponse> {
        return this.http.post(BazaNcPurchaseFlowLimitsEndpointPaths.limitsForPurchase, request);
    }
}
