import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDwollaCustomerDetailDto,
    BazaNcDwollaEndpoint,
    BazaNcDwollaEndpointPaths,
    BazaNcDwollaTouchResponse,
} from '@scaliolabs/baza-nc-shared';

export class BazaNcDwollaNodeAccess implements BazaNcDwollaEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async touch(): Promise<BazaNcDwollaTouchResponse> {
        return this.http.post(BazaNcDwollaEndpointPaths.touch, undefined);
    }

    async current(): Promise<BazaNcDwollaCustomerDetailDto> {
        return this.http.get(BazaNcDwollaEndpointPaths.current);
    }
}
