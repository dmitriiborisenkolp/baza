import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcBankAccountAddRequest,
    BazaNcBankAccountDto,
    BazaNcBankAccountEndpoint,
    BazaNcBankAccountEndpointPaths,
    BazaNcBankAccountExportRequest,
    BazaNcBankAccountGetDefaultByTypeRequest,
    BazaNcBankAccountGetDefaultByTypeResponse,
    BazaNcBankAccountLinkOnSuccessRequest,
    BazaNcBankAccountLinkOnSuccessResponse,
    BazaNcBankAccountRemoveResponse,
    BazaNcBankAccountSetRequest,
    BazaNcBankAccountType,
} from '@scaliolabs/baza-nc-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';
import { BazaPlaidLinkRequestDto, BazaPlaidLinkResponseDto } from '@scaliolabs/baza-plaid-shared';
import { BazaNcBankAccountAchDto } from '@scaliolabs/baza-nc-shared';

export class BazaNcBankAccountsNodeAccess implements BazaNcBankAccountEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns list of all available bank accounts
     */
    async list(): Promise<Array<BazaNcBankAccountDto>> {
        return this.http.get(BazaNcBankAccountEndpointPaths.list);
    }

    /**
     * Returns list of bank accounts for each BankNcAccountType which are selected as default
     * Please note that user may didn't select bank account for some types yet - in this case there will be no
     * bank account with this type in response available
     */
    async defaults(): Promise<Array<BazaNcBankAccountDto>> {
        return this.http.get(BazaNcBankAccountEndpointPaths.defaults);
    }

    /**
     * Returns default bank account of given type
     * If bank account is not available for given type and request.throwError is false, the endpoint will returns { isAvailable: false }
     * If bank account is not available for given type and request.throwError is true, the endpoint throw an error message with "BazaNcBankAccountNoDefault" error code
     * You can also receive list of bank accounts of given type if you enable request.withOptions
     * @see BazaNcBankAccountGetDefaultByTypeRequest
     * @param type
     * @param request
     */
    async default(
        type: BazaNcBankAccountType,
        request: BazaNcBankAccountGetDefaultByTypeRequest = {},
    ): Promise<BazaNcBankAccountGetDefaultByTypeResponse> {
        const url = replacePathArgs(BazaNcBankAccountEndpointPaths.default, {
            type,
        });

        return this.http.get(url, {
            withOptions: request.withOptions ? '1' : '0',
            throwError: request.throwError ? '1' : '0',
        });
    }

    /**
     * Returns Bank Account by ULID
     * @param ulid
     */
    async get(ulid: string): Promise<BazaNcBankAccountDto> {
        return this.http.get(replacePathArgs(BazaNcBankAccountEndpointPaths.get, { ulid }));
    }

    /**
     * Adds a Bank Account (MANUAL)
     * Use `setAsDefault: true` to automatically set Bank Account as Default, i.e. use this account instantly for Purchase Flow / Future Payments
     * @param request
     */
    async add(request: BazaNcBankAccountAddRequest): Promise<BazaNcBankAccountDto> {
        return this.http.post(BazaNcBankAccountEndpointPaths.add, request);
    }

    /**
     * Returns Plaid Link Token
     * You should use the Link Token for displaying Plaid Link widget. Use `linkOnSuccess` endpoint for handling
     * onSuccess callback
     * @param request
     */
    async link(request: BazaPlaidLinkRequestDto): Promise<BazaPlaidLinkResponseDto> {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        return this.http.get(BazaNcBankAccountEndpointPaths.link);
    }

    /**
     * Plaid Link On-Success Handler
     * The method returns Bank Account. Additionally it warns if there is a duplicate account already exists
     * @param request
     */
    async linkOnSuccess(request: BazaNcBankAccountLinkOnSuccessRequest): Promise<BazaNcBankAccountLinkOnSuccessResponse> {
        return this.http.post(BazaNcBankAccountEndpointPaths.linkOnSuccess, request);
    }

    /**
     * Removes Bank Account by ULID
     * If it's possible, a first Bank Account in list of Bank Accounts of same type will be selected as default
     * @param ulid
     */
    async remove(ulid: string): Promise<BazaNcBankAccountRemoveResponse> {
        return this.http.delete(replacePathArgs(BazaNcBankAccountEndpointPaths.remove, { ulid }));
    }

    /**
     * Set Bank Account as default for given Bank Account Type
     * @param request
     */
    async set(request: BazaNcBankAccountSetRequest): Promise<Array<BazaNcBankAccountDto>> {
        return this.http.post(BazaNcBankAccountEndpointPaths.set, request);
    }

    /**
     * Exports Bank Account to external services (North Capital or Dwolla)
     * Only one Bank Account could be exported to NC at once. Additional export calls will mark previosly exported
     * bank accounts as not exported to NC. Also, you can export only Cash-In Bank Accounts which will be used
     * for Cash-In operations or for purchasing shares using legacy API
     * Any number of Bank Accounts of any type could be exported to Dwolla.
     * @param request
     */
    async export(request: BazaNcBankAccountExportRequest): Promise<BazaNcBankAccountDto> {
        return this.http.post(BazaNcBankAccountEndpointPaths.export, request);
    }

    /**
     * Returns Bank Account Details for ACH transactions which are currently set on NC
     * These details will be immediately used for next purchasing shares session
     * It's recommended to use this endpoint instead of `default` for displaying actual data on NC side instead of
     * possible data on Baza side
     */
    async ncAchBankAccount(): Promise<BazaNcBankAccountAchDto> {
        return this.http.get(BazaNcBankAccountEndpointPaths.ncAchBankAccount);
    }
}
