import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcTaxDocumentDto,
    BazaNcTaxDocumentEndpoint,
    BazaNcTaxDocumentEndpointPaths,
    BazaNcTaxDocumentGetRequest,
    BazaNcTaxDocumentListRequest,
    BazaNcTaxDocumentListResponse,
} from '@scaliolabs/baza-nc-shared';
import { replacePathArgs } from '@scaliolabs/baza-core-shared';

export class BazaNcTaxDocumentNodeAccess implements BazaNcTaxDocumentEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async get(request: BazaNcTaxDocumentGetRequest): Promise<BazaNcTaxDocumentDto> {
        return this.http.get(
            replacePathArgs(BazaNcTaxDocumentEndpointPaths.get, {
                id: request.id,
            }),
        );
    }

    async list(request: BazaNcTaxDocumentListRequest): Promise<BazaNcTaxDocumentListResponse> {
        return this.http.get(BazaNcTaxDocumentEndpointPaths.list, request);
    }
}
