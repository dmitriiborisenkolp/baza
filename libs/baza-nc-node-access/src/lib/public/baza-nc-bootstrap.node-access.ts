import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { BazaNcBootstrapDto, BazaNcBootstrapEndpoint, BazaNcBootstrapEndpointPaths } from '@scaliolabs/baza-nc-shared';

export class BazaNcBootstrapNodeAccess implements BazaNcBootstrapEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    bootstrap(): Promise<BazaNcBootstrapDto> {
        return this.http.get(BazaNcBootstrapEndpointPaths.Bootstrap);
    }
}
