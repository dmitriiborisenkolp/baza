import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcTransactionEndpoint,
    BazaNcTransactionEndpointPaths,
    BazaNcTransactionGetByIdRequest,
    BazaNcTransactionGetByIdResponse,
    BazaNcTransactionInvestmentsListRequest,
    BazaNcTransactionInvestmentsResponse,
    BazaNcTransactionListRequest,
    BazaNcTransactionListResponse,
} from '@scaliolabs/baza-nc-shared';

export class BazaNcTransactionsNodeAccess implements BazaNcTransactionEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    list(request: BazaNcTransactionListRequest): Promise<BazaNcTransactionListResponse> {
        return this.http.post(BazaNcTransactionEndpointPaths.list, request);
    }

    getById(request: BazaNcTransactionGetByIdRequest): Promise<BazaNcTransactionGetByIdResponse> {
        return this.http.post(BazaNcTransactionEndpointPaths.getById, request);
    }

    investments(request: BazaNcTransactionInvestmentsListRequest): Promise<BazaNcTransactionInvestmentsResponse> {
        return this.http.post(BazaNcTransactionEndpointPaths.investments, request);
    }
}
