import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    AccountVerificationDto,
    AccountVerificationFormResourcesDto,
    AccountVerificationInvestorProfileDto,
    AccountVerificationIsCompletedDto,
    BazaNcAccountVerificationEndpoint,
    BazaNcAccountVerificationEndpointPaths,
    ListStatesRequestDto,
    ListStatesResponseDto,
    PersonalInformationApplyRequest,
} from '@scaliolabs/baza-nc-shared';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const FormData = require('form-data');

export class BazaNcAccountVerificationNodeAccess implements BazaNcAccountVerificationEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    index(): Promise<AccountVerificationDto> {
        return this.http.get(BazaNcAccountVerificationEndpointPaths.index);
    }

    formResources(): Promise<AccountVerificationFormResourcesDto> {
        return this.http.get(BazaNcAccountVerificationEndpointPaths.formResources);
    }

    listStates(request: ListStatesRequestDto): Promise<ListStatesResponseDto> {
        return this.http.post(BazaNcAccountVerificationEndpointPaths.listStates, request);
    }

    isCompleted(): Promise<AccountVerificationIsCompletedDto> {
        return this.http.get(BazaNcAccountVerificationEndpointPaths.isCompleted);
    }

    uploadPersonalInformationSSNDocument(file: File | unknown): Promise<AccountVerificationDto> {
        const formData = new FormData();

        formData.append('file', file);

        return this.http.post(BazaNcAccountVerificationEndpointPaths.uploadPersonalInformationSSNDocument, formData, {
            asFormData: true,
            asJsonResponse: true,
            withJwt: true,
        });
    }

    uploadPersonalInformationDocument(file: File | unknown): Promise<AccountVerificationDto> {
        const formData = new FormData();

        formData.append('file', file);

        return this.http.post(BazaNcAccountVerificationEndpointPaths.uploadPersonalInformationDocument, formData, {
            asFormData: true,
            asJsonResponse: true,
            withJwt: true,
        });
    }

    usePreviouslyUploadedDocument(): Promise<AccountVerificationDto> {
        return this.http.post(BazaNcAccountVerificationEndpointPaths.usePreviouslyUploadedDocument);
    }

    applyPersonalInformation(request: PersonalInformationApplyRequest): Promise<AccountVerificationDto> {
        return this.http.post(BazaNcAccountVerificationEndpointPaths.applyPersonalInformation, request);
    }

    applyInvestorProfile(request: AccountVerificationInvestorProfileDto): Promise<AccountVerificationDto> {
        return this.http.post(BazaNcAccountVerificationEndpointPaths.applyInvestorProfile, request);
    }
}
