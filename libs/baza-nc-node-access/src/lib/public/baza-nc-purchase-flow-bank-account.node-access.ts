import {
    BazaNcPurchaseFlowBankAccountEndpoint,
    BazaNcPurchaseFlowBankAccountEndpointPaths,
    PurchaseFlowBankAccountDto,
    PurchaseFlowPlaidLinkDto,
    PurchaseFlowSetBankAccountDetailsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';

/**
 * Node Access Service for Legacy Bank Account System
 * @deprecated Please use New Bank Account System
 */
export class BazaNcPurchaseFlowBankAccountNodeAccess implements BazaNcPurchaseFlowBankAccountEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns link to register bank account with Plaid
     * @deprecated Please use New Bank Account System
     */
    getPlaidLink(): Promise<PurchaseFlowPlaidLinkDto> {
        return this.http.post(BazaNcPurchaseFlowBankAccountEndpointPaths.getPlaidLink);
    }

    /**
     * Returns bank account details fetched with Plaid
     * @deprecated Please use New Bank Account System
     */
    getBankAccount(): Promise<PurchaseFlowBankAccountDto> {
        return this.http.get(BazaNcPurchaseFlowBankAccountEndpointPaths.getBankAccount);
    }

    /**
     * Manual setup bank account details
     * @deprecated Please use New Bank Account System
     */
    setBankAccount(request: PurchaseFlowSetBankAccountDetailsDto): Promise<PurchaseFlowBankAccountDto> {
        return this.http.post(BazaNcPurchaseFlowBankAccountEndpointPaths.setBankAccount, request);
    }

    /**
     * Delete linked bank account
     * @deprecated Please use New Bank Account System
     */
    deleteBankAccount(): Promise<void> {
        return this.http.post(BazaNcPurchaseFlowBankAccountEndpointPaths.deleteBankAccount, undefined, {
            asJsonResponse: false,
        });
    }
}
