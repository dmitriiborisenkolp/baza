import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendTransactionEntryCmsCreateRequest,
    BazaNcDividendTransactionEntryCmsDeleteRequest,
    BazaNcDividendTransactionEntryCmsEndpoint,
    BazaNcDividendTransactionEntryCmsEndpointPaths,
    BazaNcDividendTransactionEntryCmsGetByUlidRequest,
    BazaNcDividendTransactionEntryCmsListRequest,
    BazaNcDividendTransactionEntryCmsListResponse,
    BazaNcDividendTransactionEntryCmsUpdateRequest,
    BazaNcDividendTransactionEntryDto,
    BazaNcDividendTransactionEntryExportCsvRequest,
    BazaNcDividendTransactionEntryImportCsvRequest,
    BazaNcDividendTransactionEntryImportCsvResponse,
} from '@scaliolabs/baza-nc-shared';
import * as FormData from 'form-data';

export class BazaNcDividendTransactionEntryCmsNodeAccess implements BazaNcDividendTransactionEntryCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: BazaNcDividendTransactionEntryCmsCreateRequest): Promise<BazaNcDividendTransactionEntryDto> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.create, request);
    }

    async update(request: BazaNcDividendTransactionEntryCmsUpdateRequest): Promise<BazaNcDividendTransactionEntryDto> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.update, request);
    }

    async delete(request: BazaNcDividendTransactionEntryCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async getByUlid(request: BazaNcDividendTransactionEntryCmsGetByUlidRequest): Promise<BazaNcDividendTransactionEntryDto> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.getByUlid, request);
    }

    async list(request: BazaNcDividendTransactionEntryCmsListRequest): Promise<BazaNcDividendTransactionEntryCmsListResponse> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.list, request);
    }

    async importCsv(
        uploadedCsvFile: File | Buffer | unknown,
        request: BazaNcDividendTransactionEntryImportCsvRequest,
    ): Promise<BazaNcDividendTransactionEntryImportCsvResponse> {
        const formData = new FormData();

        formData.append('file', uploadedCsvFile);
        formData.append('source', request.source);
        formData.append('dividendTransactionUlid', request.dividendTransactionUlid);

        if (request.csvDelimiter) {
            formData.append('csvDelimiter', request.csvDelimiter);
        }

        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.importCsv, formData, {
            asFormData: true,
        });
    }

    async exportToCsv(request: BazaNcDividendTransactionEntryExportCsvRequest): Promise<string> {
        return this.http.post(BazaNcDividendTransactionEntryCmsEndpointPaths.exportToCsv, request, {
            asJsonResponse: false,
        });
    }
}
