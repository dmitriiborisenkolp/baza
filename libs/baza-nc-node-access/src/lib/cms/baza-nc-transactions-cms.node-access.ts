import {
    BazaNcTransactionCmsEndpoint,
    BazaNcTransactionCmsEndpointPaths,
    ExportToCsvTransactionCmsRequest,
    GetByIdTransactionCmsRequest,
    GetByIdTransactionCmsResponse,
    ListTransactionsCmsRequest,
    ListTransactionsCmsResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';

export class BazaNcTransactionsCmsNodeAccess implements BazaNcTransactionCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    list(request: ListTransactionsCmsRequest): Promise<ListTransactionsCmsResponse> {
        return this.http.post(BazaNcTransactionCmsEndpointPaths.list, request);
    }

    getById(request: GetByIdTransactionCmsRequest): Promise<GetByIdTransactionCmsResponse> {
        return this.http.post(BazaNcTransactionCmsEndpointPaths.getById, request);
    }

    exportToCsv(request: ExportToCsvTransactionCmsRequest): Promise<string> {
        return this.http.post(BazaNcTransactionCmsEndpointPaths.exportToCsv, request, {
            asJsonResponse: false,
        });
    }
}
