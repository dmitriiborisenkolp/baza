import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendCmsDeleteRequest,
    BazaNcDividendCmsDto,
    BazaNcDividendCmsEndpoint,
    BazaNcDividendCmsEndpointPaths,
    BazaNcDividendCmsExportToCsvRequest,
    BazaNcDividendCmsGetByUlidRequest,
    BazaNcDividendCmsListRequest,
    BazaNcDividendCmsListResponse,
} from '@scaliolabs/baza-nc-shared';

export class BazaNcDividendCmsNodeAccess implements BazaNcDividendCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async list(request: BazaNcDividendCmsListRequest): Promise<BazaNcDividendCmsListResponse> {
        return this.http.post(BazaNcDividendCmsEndpointPaths.list, request);
    }

    async getByUlid(request: BazaNcDividendCmsGetByUlidRequest): Promise<BazaNcDividendCmsDto> {
        return this.http.post(BazaNcDividendCmsEndpointPaths.getByUlid, request);
    }

    async delete(request: BazaNcDividendCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaNcDividendCmsEndpointPaths.delete, request);
    }

    async exportToCsv(request: BazaNcDividendCmsExportToCsvRequest): Promise<string> {
        return this.http.post(BazaNcDividendCmsEndpointPaths.exportToCsv, request, {
            asJsonResponse: false,
        });
    }
}
