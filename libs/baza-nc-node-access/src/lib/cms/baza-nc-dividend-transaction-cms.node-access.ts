import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDividendTransactionCmsCreateRequest,
    BazaNcDividendTransactionCmsDeleteRequest,
    BazaNcDividendTransactionCmsEndpoint,
    BazaNcDividendTransactionCmsEndpointPaths,
    BazaNcDividendTransactionCmsExportToCsvRequest,
    BazaNcDividendTransactionCmsGetByUlidRequest,
    BazaNcDividendTransactionCmsListRequest,
    BazaNcDividendTransactionCmsListResponse,
    BazaNcDividendTransactionCmsProcessRequest,
    BazaNcDividendTransactionCmsProcessResponse,
    BazaNcDividendTransactionCmsReprocessRequest,
    BazaNcDividendTransactionCmsReprocessResponse,
    BazaNcDividendTransactionCmsSendProcessConfirmationRequest,
    BazaNcDividendTransactionCmsSyncRequest,
    BazaNcDividendTransactionDto,
} from '@scaliolabs/baza-nc-shared';

export class BazaNcDividendTransactionCmsNodeAccess implements BazaNcDividendTransactionCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: BazaNcDividendTransactionCmsCreateRequest): Promise<BazaNcDividendTransactionDto> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.create, request);
    }

    async delete(request: BazaNcDividendTransactionCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: BazaNcDividendTransactionCmsListRequest): Promise<BazaNcDividendTransactionCmsListResponse> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.list, request);
    }

    async getByUlid(request: BazaNcDividendTransactionCmsGetByUlidRequest): Promise<BazaNcDividendTransactionDto> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.getByUlid, request);
    }

    async process(request: BazaNcDividendTransactionCmsProcessRequest): Promise<BazaNcDividendTransactionCmsProcessResponse> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.process, request);
    }

    async reprocess(request: BazaNcDividendTransactionCmsReprocessRequest): Promise<BazaNcDividendTransactionCmsReprocessResponse> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.reprocess, request);
    }

    async sync(request: BazaNcDividendTransactionCmsSyncRequest): Promise<void> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.sync, request, {
            asJsonResponse: false,
        });
    }

    async sendProcessConfirmation(request: BazaNcDividendTransactionCmsSendProcessConfirmationRequest): Promise<void> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.sendProcessConfirmation, request, {
            asJsonResponse: false,
        });
    }

    exportToCsv(request: BazaNcDividendTransactionCmsExportToCsvRequest): Promise<string> {
        return this.http.post(BazaNcDividendTransactionCmsEndpointPaths.exportToCsv, request, {
            asJsonResponse: false,
        });
    }
}
