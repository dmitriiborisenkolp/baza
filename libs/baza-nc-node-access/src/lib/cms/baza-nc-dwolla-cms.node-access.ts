import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import { CreateDocumentRequest, BazaDwollaCreatePersonalCustomerResponse } from '@scaliolabs/baza-dwolla-shared';
import {
    BazaNcDwollaCmsEndpoint,
    BazaNcDwollaCmsEndpointPaths,
    BazaNcDwollaCmsTouchAllRequest,
    BazaNcTouchDwollaCustomerRequest,
    BazaNcTouchDwollaCustomerResponse,
} from '@scaliolabs/baza-nc-shared';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const FormData = require('form-data');

/**
 * Node-Access Service for Baza NC Dwolla CMS API
 */
export class BazaNcDwollaCmsNodeAccess implements BazaNcDwollaCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Attempts to create a Dwolla Customer for a specific Investor Account
     * @param request
     */
    async touch(request: BazaNcTouchDwollaCustomerRequest): Promise<BazaNcTouchDwollaCustomerResponse> {
        return this.http.post(BazaNcDwollaCmsEndpointPaths.touch, request);
    }

    /**
     * Executes "touch" operation for all existing Investor Accounts
     * @param request
     */
    async touchAllInvestors(request: BazaNcDwollaCmsTouchAllRequest): Promise<void> {
        return this.http.post(BazaNcDwollaCmsEndpointPaths.touchAllInvestors, request);
    }

    /**
     * Attempts to retry customer verification for Dwolla Customer
     * @param request
     */
    async retryVerificationForPersonalCustomer(
        request: BazaNcTouchDwollaCustomerRequest,
    ): Promise<BazaDwollaCreatePersonalCustomerResponse> {
        return this.http.post(BazaNcDwollaCmsEndpointPaths.retryVerification, request);
    }

    /**
     * Attempts to upload document for Dwolla Customer
     * @param file
     * @param request
     */
    async createDocumentForCustomer(file: File | unknown, request: CreateDocumentRequest): Promise<void> {
        const formData = new FormData();

        formData.append('file', file);

        for (const key of Object.keys(request)) {
            formData.append(key, request[`${key}`]);
        }

        return this.http.post(BazaNcDwollaCmsEndpointPaths.createDocument, formData, {
            asFormData: true,
            asJsonResponse: false,
        });
    }
}
