import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcInvestorAccountCmsEndpoint,
    BazaNcInvestorAccountCmsEndpointPaths,
    BazaNcInvestorAccountUpdatePersonalInformationRequest,
    NcAddInvestorAccountRequest,
    NcAddInvestorAccountResponse,
    NcExportToCsvInvestorAccountRequest,
    NcGetInvestorAccountByUserIdRequest,
    NcGetInvestorAccountByUserIdResponse,
    NcGetInvestorAccountPersonalInfoRequest,
    NcGetInvestorAccountPersonalInfoResponse,
    NcGetInvestorAccountRequest,
    NcGetInvestorAccountResponse,
    NcListInvestorAccountRequest,
    NcListInvestorAccountResponse,
    NcRemoveInvestorAccountRequest,
    NcRemoveInvestorAccountResponse,
} from '@scaliolabs/baza-nc-shared';

export class BazaNcInvestorAccountCmsNodeAccess implements BazaNcInvestorAccountCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async getInvestorAccount(request: NcGetInvestorAccountRequest): Promise<NcGetInvestorAccountResponse> {
        return this.http.post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccount, request);
    }

    async getInvestorAccountByUserId(request: NcGetInvestorAccountByUserIdRequest): Promise<NcGetInvestorAccountByUserIdResponse> {
        return this.http.post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccountByUserId, request);
    }

    async listInvestorAccounts(request: NcListInvestorAccountRequest): Promise<NcListInvestorAccountResponse> {
        return this.http.post(BazaNcInvestorAccountCmsEndpointPaths.listInvestorAccounts, request);
    }

    async addInvestorAccount(request: NcAddInvestorAccountRequest): Promise<NcAddInvestorAccountResponse> {
        return this.http.post(BazaNcInvestorAccountCmsEndpointPaths.addInvestorAccount, request);
    }

    async removeInvestorAccount(request: NcRemoveInvestorAccountRequest): Promise<NcRemoveInvestorAccountResponse> {
        return this.http.post(BazaNcInvestorAccountCmsEndpointPaths.removeInvestorAccount, request);
    }

    async exportInvestorAccountsToCsv(request: NcExportToCsvInvestorAccountRequest): Promise<string> {
        return this.http.post(BazaNcInvestorAccountCmsEndpointPaths.exportInvestorAccountsToCsv, request);
    }

    async getInvestorAccountPersonalInfo(
        request: NcGetInvestorAccountPersonalInfoRequest,
    ): Promise<NcGetInvestorAccountPersonalInfoResponse> {
        return this.http.post(BazaNcInvestorAccountCmsEndpointPaths.getInvestorAccountPersonalInfo, request);
    }

    async updateInvestorAccountPersonalInfo(request: BazaNcInvestorAccountUpdatePersonalInformationRequest): Promise<void> {
        return this.http.post(BazaNcInvestorAccountCmsEndpointPaths.updateInvestorAccountPersonalInfo, request, {
            asJsonResponse: false,
        });
    }
}
