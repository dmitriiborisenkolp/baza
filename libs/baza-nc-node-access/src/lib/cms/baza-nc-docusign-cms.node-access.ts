import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcDocusignCmsEndpoint,
    NcDocusignCmsEndpointPaths,
    NcDocusignCmsGetByIdRequest,
    BazaNcDocusignDto,
} from '@scaliolabs/baza-nc-shared';

export class BazaNcDocusignCmsNodeAccess implements BazaNcDocusignCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async getAll(): Promise<Array<BazaNcDocusignDto>> {
        return this.http.post(NcDocusignCmsEndpointPaths.getAll);
    }

    async getById(request: NcDocusignCmsGetByIdRequest): Promise<BazaNcDocusignDto> {
        return this.http.post(NcDocusignCmsEndpointPaths.getById, request);
    }
}
