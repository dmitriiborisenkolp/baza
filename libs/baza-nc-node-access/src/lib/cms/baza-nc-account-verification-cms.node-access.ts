import {
    BazaNcAccountVerificationCmsEndpoint,
    BazaNcAccountVerificationCmsEndpointPaths,
    BazaNcCmsAccountVerificationFormResourcesDto,
    ListStatesRequestCmsDto,
    ListStatesResponseCmsDto,
    NcAccountVerificationKycLogsRequest,
    NcAccountVerificationKycLogsResponse,
} from '@scaliolabs/baza-nc-shared';
import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';

export class BazaNcAccountVerificationCmsNodeAccess implements BazaNcAccountVerificationCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    kycLogs(request: NcAccountVerificationKycLogsRequest): Promise<NcAccountVerificationKycLogsResponse> {
        return this.http.post(BazaNcAccountVerificationCmsEndpointPaths.kycLogs, request);
    }

    formResources(): Promise<BazaNcCmsAccountVerificationFormResourcesDto> {
        return this.http.get(BazaNcAccountVerificationCmsEndpointPaths.formResources);
    }

    listStates(request: ListStatesRequestCmsDto): Promise<ListStatesResponseCmsDto> {
        return this.http.post(BazaNcAccountVerificationCmsEndpointPaths.listStates, request);
    }
}
