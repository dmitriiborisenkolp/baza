import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcReportCmsEndpoint,
    BazaNcReportCmsEndpointPaths,
    BazaNcReportCmsExportToCsvRequest,
    BazaNcReportCmsGetByIdRequest,
    BazaNcReportCmsListRequest,
    BazaNcReportCmsListResponse,
    BazaNcReportCmsResendOneRequest,
    BazaNcReportCmsSendManyRequest,
    BazaNcReportCmsSendManyResponse,
    BazaNcReportCmsSendOneRequest,
    BazaNcReportCmsSyncAllRequest,
    BazaNcReportCmsSyncRequest,
    BazaNcReportDto,
} from '@scaliolabs/baza-nc-shared';

/**
 * Node Access Service for Baza NC Daily Reports CMS
 */
export class BazaNcReportCmsNodeAccess implements BazaNcReportCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Returns List of Reports
     * @param request
     */
    async list(request: BazaNcReportCmsListRequest): Promise<BazaNcReportCmsListResponse> {
        return this.http.post(BazaNcReportCmsEndpointPaths.list, request);
    }

    /**
     * Returns Report by ULID
     * @param request
     */
    async getById(request: BazaNcReportCmsGetByIdRequest): Promise<BazaNcReportDto> {
        return this.http.post(BazaNcReportCmsEndpointPaths.getById, request);
    }

    /**
     * Sends Report
     * @param request
     */
    async sendOne(request: BazaNcReportCmsSendOneRequest): Promise<BazaNcReportDto> {
        return this.http.post(BazaNcReportCmsEndpointPaths.sendOne, request);
    }

    /**
     * Sends all Reports. Could be filtered by Date From and Date To
     * @param request
     */
    async sendMany(request: BazaNcReportCmsSendManyRequest): Promise<BazaNcReportCmsSendManyResponse> {
        return this.http.post(BazaNcReportCmsEndpointPaths.sendMany, request);
    }

    /**
     * Resends Report which was sent recently
     * @param request
     */
    async resendOne(request: BazaNcReportCmsResendOneRequest): Promise<BazaNcReportDto> {
        return this.http.post(BazaNcReportCmsEndpointPaths.resendOne, request);
    }

    /**
     * Exports all Reports to CSV. Could be filtered by Date From and Date To
     * @param request
     */
    async exportToCsv(request: BazaNcReportCmsExportToCsvRequest): Promise<string> {
        return this.http.post(BazaNcReportCmsEndpointPaths.exportToCsv, request, {
            asJsonResponse: false,
        });
    }

    /**
     * Sync Status for Report
     * @param request
     */
    async sync(request: BazaNcReportCmsSyncRequest): Promise<BazaNcReportDto> {
        return this.http.post(BazaNcReportCmsEndpointPaths.sync, request);
    }

    /**
     * Sync Statuses for all unreported Reports. Works in asyc way.
     */
    async syncAll(request: BazaNcReportCmsSyncAllRequest): Promise<void> {
        return this.http.post(BazaNcReportCmsEndpointPaths.syncAll, request, {
            asJsonResponse: false,
        });
    }
}
