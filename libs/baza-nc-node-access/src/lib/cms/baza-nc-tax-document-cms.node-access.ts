import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcTaxDocumentCmsDto,
    BazaNcTaxDocumentCmsCreateRequest,
    BazaNcTaxDocumentCmsDeleteRequest,
    BazaNcTaxDocumentCmsEndpoint,
    BazaNcTaxDocumentCmsEndpointPaths,
    BazaNcTaxDocumentCmsGetByIdRequest,
    BazaNcTaxDocumentCmsListRequest,
    BazaNcTaxDocumentCmsListResponse,
    BazaNcTaxDocumentCmsUpdateRequest,
} from '@scaliolabs/baza-nc-shared';

export class BazaNcTaxDocumentCmsNodeAccess implements BazaNcTaxDocumentCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    async create(request: BazaNcTaxDocumentCmsCreateRequest): Promise<BazaNcTaxDocumentCmsDto> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.create, request);
    }

    async update(request: BazaNcTaxDocumentCmsUpdateRequest): Promise<BazaNcTaxDocumentCmsDto> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.update, request);
    }

    async delete(request: BazaNcTaxDocumentCmsDeleteRequest): Promise<void> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.delete, request, {
            asJsonResponse: false,
        });
    }

    async list(request: BazaNcTaxDocumentCmsListRequest): Promise<BazaNcTaxDocumentCmsListResponse> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.list, request);
    }

    async getById(request: BazaNcTaxDocumentCmsGetByIdRequest): Promise<BazaNcTaxDocumentCmsDto> {
        return this.http.post(BazaNcTaxDocumentCmsEndpointPaths.getById, request);
    }
}
