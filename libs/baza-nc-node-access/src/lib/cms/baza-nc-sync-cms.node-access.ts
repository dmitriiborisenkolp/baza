import { BazaDataAccessNode } from '@scaliolabs/baza-core-node-access';
import {
    BazaNcSyncEndpoint,
    NcSyncEndpointPaths,
    NcSyncInvestorAccountRequest,
    NcSyncInvestorAccountResponse,
    NcSyncInvestorAccountPaymentMethodsRequest,
    NcSyncTransactionsRequest,
    NcSyncPercentsFundedForOfferingRequest,
    NcSyncRemoveInvestorAccountRequest,
    NcSyncRemoveInvestorAccountResponse,
} from '@scaliolabs/baza-nc-shared';

/**
 * Baza NC Sync Node Access Service
 */
export class BazaNcSyncCmsNodeAccess implements BazaNcSyncEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    /**
     * Sync all investor accounts and investor account transactions
     */
    syncAllInvestorAccounts(): Promise<void> {
        return this.http.post(NcSyncEndpointPaths.syncAllInvestorAccounts);
    }

    /**
     * Sync investor account and transactions
     * @param request
     */
    syncInvestorAccount(request: NcSyncInvestorAccountRequest): Promise<NcSyncInvestorAccountResponse> {
        return this.http.post(NcSyncEndpointPaths.syncInvestorAccount, request);
    }

    /**
     * Sync investor account payment methods
     * @param request
     */
    syncInvestorAccountPaymentMethods(request: NcSyncInvestorAccountPaymentMethodsRequest): Promise<void> {
        return this.http.post(NcSyncEndpointPaths.syncInvestorAccountPaymentMethods, request, {
            asJsonResponse: false,
        });
    }

    /**
     * Sync payment methods for every investor account
     */
    syncAllInvestorAccountsPaymentMethods(): Promise<void> {
        return this.http.post(NcSyncEndpointPaths.syncAllInvestorAccountsPaymentMethods);
    }

    /**
     * Sync transactions only for investor account
     * @param request
     */
    syncTransactions(request: NcSyncTransactionsRequest): Promise<void> {
        return this.http.post(NcSyncEndpointPaths.syncTransactions, request);
    }

    /**
     * Sync percents funded for all offerings
     */
    syncPercentsFunded(): Promise<void> {
        return this.http.post(NcSyncEndpointPaths.syncPercentsFunded);
    }

    /**
     * Sync percents funded for offering
     * @param request
     */
    syncPercentsFundedOfOffering(request: NcSyncPercentsFundedForOfferingRequest): Promise<void> {
        return this.http.post(NcSyncEndpointPaths.syncPercentsFundedOfOffering, request);
    }

    /**
     * Remove (exclude) previously synced data
     * @param request
     */
    syncRemove(request: NcSyncRemoveInvestorAccountRequest): Promise<NcSyncRemoveInvestorAccountResponse> {
        return this.http.post(NcSyncEndpointPaths.syncRemove, request);
    }
}
