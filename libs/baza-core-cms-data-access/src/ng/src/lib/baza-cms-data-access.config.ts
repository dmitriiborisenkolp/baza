import { Injectable } from '@angular/core';

@Injectable()
export class BazaCmsDataAccessConfig {
    apiEndpointUrl: string;
}
