import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '../../../../ng/src';
import { BazaMailCustomerioLinksCmsDataAccess } from './ng/baza-mail-customerio-links-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaMailCustomerioLinksCmsDataAccess],
})
export class BazaMailCustomerioLinksCmsDataAccessModule {}
