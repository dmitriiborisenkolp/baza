import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';
import {
    BazaMailCustomerioLinkDto,
    BazaMailCustomerioLinkEndpoint,
    BazaMailCustomerioLinEndpointPaths,
    BazaMailCustomerioLinkSetRequest,
    replacePathArgs,
    BazaMailCustomerioLinksUnsetRequest,
} from '@scaliolabs/baza-core-shared';

/**
 * Data-Access Service for Baza Mail Customer.IO Links CMS API
 */
@Injectable()
export class BazaMailCustomerioLinksCmsDataAccess implements BazaMailCustomerioLinkEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    /**
     * Returns list of Mail Templates with Customer.IO Link associations
     */
    list(): Observable<Array<BazaMailCustomerioLinkDto>> {
        return this.http.get(BazaMailCustomerioLinEndpointPaths.list);
    }

    /**
     * Returns list if Mail Templates with Customer.IO Link associations
     * Filtered by Tag
     */
    listByTag(tag: string): Observable<Array<BazaMailCustomerioLinkDto>> {
        return this.http.get(replacePathArgs(BazaMailCustomerioLinEndpointPaths.listByTag, { tag }));
    }

    /**
     * Set Customer.IO Template Id for Mail Template
     * @param request
     */
    set(request: BazaMailCustomerioLinkSetRequest): Observable<BazaMailCustomerioLinkDto> {
        return this.http.post(BazaMailCustomerioLinEndpointPaths.set, request);
    }

    /**
     * Removes Customer.IO Link for given Mail Template
     * @param request
     */
    unset(request: BazaMailCustomerioLinksUnsetRequest): Observable<void> {
        return this.http.post(BazaMailCustomerioLinEndpointPaths.unset, request);
    }
}
