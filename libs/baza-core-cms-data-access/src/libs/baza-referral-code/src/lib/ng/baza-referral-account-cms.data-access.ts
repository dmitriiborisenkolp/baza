import { Injectable } from '@angular/core';
import {
    BazaReferralAccountCmsEndpoint,
    BazaReferralAccountCmsEndpointPaths,
    BazaReferralAccountCmsExportToCsvListRequest,
    BazaReferralAccountCmsListRequest,
    BazaReferralAccountCmsListResponse,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaReferralAccountCmsDataAccess implements BazaReferralAccountCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    list(request: BazaReferralAccountCmsListRequest): Observable<BazaReferralAccountCmsListResponse> {
        return this.http.post(BazaReferralAccountCmsEndpointPaths.list, request);
    }

    exportListToCSV(request: BazaReferralAccountCmsExportToCsvListRequest): Observable<string> {
        return this.http.post(BazaReferralAccountCmsEndpointPaths.exportListToCSV, request, {
            responseType: 'text',
        });
    }
}
