import { Injectable } from '@angular/core';
import {
    BazaReferralCodeCmsCreateRequest,
    BazaReferralCodeCmsDeleteRequest,
    BazaReferralCodeCmsEditRequest,
    BazaReferralCodeCmsEndpoint,
    BazaReferralCodeCmsEndpointPaths,
    BazaReferralCodeCmsExportListToCSVRequest,
    BazaReferralCodeCmsGetByIdRequest,
    BazaReferralCodeCmsGetByIdResponse,
    BazaReferralCodeCmsListRequest,
    BazaReferralCodeCmsListResponse,
    BazaReferralCodeDto,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaReferralCodeCmsDataAccess implements BazaReferralCodeCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    list(request: BazaReferralCodeCmsListRequest): Observable<BazaReferralCodeCmsListResponse> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.list, request);
    }

    getById(request: BazaReferralCodeCmsGetByIdRequest): Observable<BazaReferralCodeCmsGetByIdResponse> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.getById, request);
    }

    create(request: BazaReferralCodeCmsCreateRequest): Observable<BazaReferralCodeDto> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.create, request);
    }

    edit(request: BazaReferralCodeCmsEditRequest): Observable<BazaReferralCodeDto> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.edit, request);
    }

    delete(request: BazaReferralCodeCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.delete, request);
    }

    exportListToCSV(request: BazaReferralCodeCmsExportListToCSVRequest): Observable<string> {
        return this.http.post(BazaReferralCodeCmsEndpointPaths.exportListToCSV, request);
    }
}
