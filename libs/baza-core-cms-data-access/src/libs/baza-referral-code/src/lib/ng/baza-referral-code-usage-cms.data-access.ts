import { Injectable } from '@angular/core';
import {
    BazaReferralCodeCmsUsageByAccountExportListToCSVRequest,
    BazaReferralCodeCmsUsageByAccountListRequest,
    BazaReferralCodeCmsUsageByAccountListResponse,
    BazaReferralCodeCmsUsageByCampaignExportListToCSVRequest,
    BazaReferralCodeCmsUsageByCampaignListRequest,
    BazaReferralCodeCmsUsageByCampaignListResponse,
    BazaReferralCodeCmsUsageByCodeExportListToCSVRequest,
    BazaReferralCodeCmsUsageByCodeListRequest,
    BazaReferralCodeCmsUsageByCodeListResponse,
    BazaReferralCodeUsageCmsEndpoint,
    ReferralCodeUsageCmsEndpointPaths,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaReferralCodeUsageCmsDataAccess implements BazaReferralCodeUsageCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    listByReferralCode(request: BazaReferralCodeCmsUsageByCodeListRequest): Observable<BazaReferralCodeCmsUsageByCodeListResponse> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.listByReferralCode, request);
    }

    exportListByReferralCodeToCSV(request: BazaReferralCodeCmsUsageByCodeExportListToCSVRequest): Observable<string> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralCodeToCSV, request);
    }

    listByReferralCampaign(
        request: BazaReferralCodeCmsUsageByCampaignListRequest,
    ): Observable<BazaReferralCodeCmsUsageByCampaignListResponse> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.listByReferralCampaign, request);
    }

    exportListByReferralCampaignToCSV(request: BazaReferralCodeCmsUsageByCampaignExportListToCSVRequest): Observable<string> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralCampaignToCSV, request, {
            responseType: 'text',
        });
    }

    listByReferralAccount(
        request: BazaReferralCodeCmsUsageByAccountListRequest,
    ): Observable<BazaReferralCodeCmsUsageByAccountListResponse> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.listByReferralAccount, request);
    }

    exportListByReferralAccountToCSV(request: BazaReferralCodeCmsUsageByAccountExportListToCSVRequest): Observable<string> {
        return this.http.post(ReferralCodeUsageCmsEndpointPaths.exportListByReferralAccountToCSV, request, {
            responseType: 'text',
        });
    }
}
