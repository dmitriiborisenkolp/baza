import { Injectable } from '@angular/core';
import {
    BazaReferralCampaignCmsEndpoint,
    BazaReferralCampaignCmsEndpointPaths,
    BazaReferralCampaignDto,
    ReferralCampaignCmsCreateRequest,
    ReferralCampaignCmsDeleteRequest,
    ReferralCampaignCmsEditRequest,
    ReferralCampaignCmsExportListToCsvRequest,
    ReferralCampaignCmsGetByIdRequest,
    ReferralCampaignCmsGetByIdResponse,
    ReferralCampaignCmsListRequest,
    ReferralCampaignCmsListResponse,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaReferralCampaignCmsDataAccess implements BazaReferralCampaignCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    list(request: ReferralCampaignCmsListRequest): Observable<ReferralCampaignCmsListResponse> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.list, request);
    }

    getById(request: ReferralCampaignCmsGetByIdRequest): Observable<ReferralCampaignCmsGetByIdResponse> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.getById, request);
    }

    create(request: ReferralCampaignCmsCreateRequest): Observable<BazaReferralCampaignDto> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.create, request);
    }

    edit(request: ReferralCampaignCmsEditRequest): Observable<BazaReferralCampaignDto> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.edit, request);
    }

    delete(request: ReferralCampaignCmsDeleteRequest): Observable<void> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.delete, request);
    }

    exportListToCSV(request: ReferralCampaignCmsExportListToCsvRequest): Observable<string> {
        return this.http.post(BazaReferralCampaignCmsEndpointPaths.exportListToCSV, request);
    }
}
