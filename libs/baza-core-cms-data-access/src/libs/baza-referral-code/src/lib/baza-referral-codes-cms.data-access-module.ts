import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '../../../../ng/src';
import { BazaReferralAccountCmsDataAccess } from './ng/baza-referral-account-cms.data-access';
import { BazaReferralCampaignCmsDataAccess } from './ng/baza-referral-campaign-cms.data-access';
import { BazaReferralCodeCmsDataAccess } from './ng/baza-referral-code-cms.data-access';
import { BazaReferralCodeUsageCmsDataAccess } from './ng/baza-referral-code-usage-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [
        BazaReferralCampaignCmsDataAccess,
        BazaReferralCodeCmsDataAccess,
        BazaReferralCodeUsageCmsDataAccess,
        BazaReferralAccountCmsDataAccess,
    ],
})
export class BazaReferralCodesCmsDataAccessModule {}
