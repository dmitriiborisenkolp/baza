import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '../../../../ng/src';
import { BazaAuthSessionCmsDataAccess } from './ng/baza-auth-session-cms.data-access';

// @dynamic
@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaAuthSessionCmsDataAccess],
})
export class BazaAuthSessionCmsDataAccessModule {}
