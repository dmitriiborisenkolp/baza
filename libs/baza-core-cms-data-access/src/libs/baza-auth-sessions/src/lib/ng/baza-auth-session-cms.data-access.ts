import { Injectable } from '@angular/core';
import {
    BazaAuthSessionCmsAccountsRequest,
    BazaAuthSessionCmsAccountsResponse,
    BazaAuthSessionCmsEndpoint,
    BazaAuthSessionCmsEndpointPaths,
    BazaAuthSessionCmsSessionsRequest,
    BazaAuthSessionCmsSessionsResponse,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaAuthSessionCmsDataAccess implements BazaAuthSessionCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    accounts(request: BazaAuthSessionCmsAccountsRequest): Observable<BazaAuthSessionCmsAccountsResponse> {
        return this.http.post(BazaAuthSessionCmsEndpointPaths.accounts, request);
    }

    sessions(request: BazaAuthSessionCmsSessionsRequest): Observable<BazaAuthSessionCmsSessionsResponse> {
        return this.http.post(BazaAuthSessionCmsEndpointPaths.sessions, request);
    }
}
