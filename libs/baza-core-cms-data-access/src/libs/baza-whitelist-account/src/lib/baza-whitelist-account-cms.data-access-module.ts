import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '../../../../ng/src';
import { BazaWhitelistAccountCmsDataAccess } from './ng/baza-whitelist-account-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaWhitelistAccountCmsDataAccess],
})
export class BazaWhitelistAccountCmsDataAccessModule {}
