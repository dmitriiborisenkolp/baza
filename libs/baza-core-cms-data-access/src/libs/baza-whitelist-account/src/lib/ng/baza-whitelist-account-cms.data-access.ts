import { Injectable } from '@angular/core';
import {
    WhitelistAccountEndpoint,
    WhitelistAccountEndpointPaths,
    WhitelistAccountDto,
    WhitelistAccountListRequest,
    WhitelistAccountListResponse,
    WhitelistAccountRemoveRequest,
    WhitelistAccountAddRequest,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaWhitelistAccountCmsDataAccess implements WhitelistAccountEndpoint {
    constructor(private readonly ngEndpoint: BazaCmsDataAccessService) {}

    list(request: WhitelistAccountListRequest): Observable<WhitelistAccountListResponse> {
        return this.ngEndpoint.post(WhitelistAccountEndpointPaths.list, request);
    }

    add(request: WhitelistAccountAddRequest): Observable<WhitelistAccountDto> {
        return this.ngEndpoint.post(WhitelistAccountEndpointPaths.add, request);
    }

    remove(request: WhitelistAccountRemoveRequest): Observable<void> {
        return this.ngEndpoint.post(WhitelistAccountEndpointPaths.remove, request);
    }

    importCsv(file: File): Observable<void> {
        const formData = new FormData();

        formData.append('file', file);

        return this.ngEndpoint.post(WhitelistAccountEndpointPaths.importCsv, formData);
    }
}
