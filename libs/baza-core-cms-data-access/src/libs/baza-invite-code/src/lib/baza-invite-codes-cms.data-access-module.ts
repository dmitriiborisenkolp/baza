import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '../../../../ng/src';
import { BazaInviteCodesCmsDataAccess } from './ng/baza-invite-codes-cms.data-access';

@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaInviteCodesCmsDataAccess],
})
export class BazaInviteCodesCmsDataAccessModule {}
