import { Injectable } from '@angular/core';
import {
    BazaCreateInviteCodeRequest,
    BazaDeleteInviteCodeRequest,
    BazaInviteCodeCmsDto,
    BazaInviteCodeCmsEndpoint,
    BazaInviteCodeCmsEndpointPaths,
    BazaInviteCodeGetByIdRequest,
    BazaListOfSharedInviteCodesRequest,
    BazaListOfSharedInviteCodesResponse,
    BazaUpdateInviteCodeRequest,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaInviteCodesCmsDataAccess implements BazaInviteCodeCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    getById(request: BazaInviteCodeGetByIdRequest): Observable<BazaInviteCodeCmsDto> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.getById, request);
    }

    listOfSharedInviteCodes(request: BazaListOfSharedInviteCodesRequest): Observable<BazaListOfSharedInviteCodesResponse> {
        return this.http.post<BazaListOfSharedInviteCodesResponse>(BazaInviteCodeCmsEndpointPaths.listOfSharedInviteCodes, request);
    }

    createInviteCode(request: BazaCreateInviteCodeRequest): Observable<BazaInviteCodeCmsDto> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.createInviteCode, request);
    }

    updateInviteCode(request: BazaUpdateInviteCodeRequest): Observable<BazaInviteCodeCmsDto> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.updateInviteCode, request);
    }

    deleteInviteCode(request: BazaDeleteInviteCodeRequest): Observable<void> {
        return this.http.post(BazaInviteCodeCmsEndpointPaths.deleteInviteCode, request);
    }
}
