import { Injectable } from '@angular/core';
import {
    AccountCmsEndpoint,
    AccountCmsEndpointPaths,
    AssignAdminAccessCmsRequest,
    AssignAdminAccessCmsResponse,
    ChangeEmailAccountCmsRequest,
    ChangeEmailAccountCmsResponse,
    ConfirmEmailAccountByIdCmsRequest,
    ConfirmEmailAccountByIdCmsResponse,
    DeactivateAccountCmsRequest,
    DeactivateAccountCmsResponse,
    DeleteAccountByIdCmsRequest,
    DeleteAccountByIdCmsResponse,
    ExportToCsvAccountRequest,
    GetAccountByEmailCmsRequest,
    GetAccountByEmailCmsResponse,
    GetAccountByIdCmsRequest,
    GetAccountByIdCmsResponse,
    ListAccountsCmsRequest,
    ListAccountsCmsResponse,
    RegisterAdminAccountCmsRequest,
    RegisterAdminAccountCmsResponse,
    RegisterUserAccountCmsRequest,
    RegisterUserAccountCmsResponse,
    RevokeAdminAccessCmsRequest,
    RevokeAdminAccessCmsResponse,
    UpdateAccountCmsRequest,
    UpdateAccountCmsResponse,
} from '@scaliolabs/baza-core-shared';
import { BazaCmsDataAccessService } from '../../../../../ng/src/lib/baza-cms-data-access.service';
import { Observable } from 'rxjs';

@Injectable()
export class BazaAccountCmsDataAccess implements AccountCmsEndpoint {
    constructor(private readonly http: BazaCmsDataAccessService) {}

    getAccountByEmail(request: GetAccountByEmailCmsRequest): Observable<GetAccountByEmailCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.getAccountByEmail, request);
    }

    getAccountById(request: GetAccountByIdCmsRequest): Observable<GetAccountByIdCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.getAccountById, request);
    }

    listAccounts(request: ListAccountsCmsRequest): Observable<ListAccountsCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.listAccounts, request);
    }

    registerAdminAccount(request: RegisterAdminAccountCmsRequest): Observable<RegisterAdminAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.registerAdminAccount, request);
    }

    registerUserAccount(request: RegisterUserAccountCmsRequest): Observable<RegisterUserAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.registerUserAccount, request);
    }

    assignAdminAccess(request: AssignAdminAccessCmsRequest): Observable<AssignAdminAccessCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.assignAdminAccess, request);
    }

    revokeAdminAccess(request: RevokeAdminAccessCmsRequest): Observable<RevokeAdminAccessCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.revokeAdminAccess, request);
    }

    confirmEmailAccountById(request: ConfirmEmailAccountByIdCmsRequest): Observable<ConfirmEmailAccountByIdCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.confirmEmailAccountById, request);
    }

    deleteAccountById(request: DeleteAccountByIdCmsRequest): Observable<DeleteAccountByIdCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.deleteAccountById, request);
    }

    updateAccount(request: UpdateAccountCmsRequest): Observable<UpdateAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.updateAccount, request);
    }

    exportToCsv(request: ExportToCsvAccountRequest): Observable<string> {
        return this.http.post(AccountCmsEndpointPaths.exportToCsv, request, {
            responseType: 'text',
        });
    }

    deactivateAccount(request: DeactivateAccountCmsRequest): Observable<DeactivateAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.deactivateAccount, request);
    }

    changeEmail(request: ChangeEmailAccountCmsRequest): Observable<ChangeEmailAccountCmsResponse> {
        return this.http.post(AccountCmsEndpointPaths.changeEmail, request);
    }
}
