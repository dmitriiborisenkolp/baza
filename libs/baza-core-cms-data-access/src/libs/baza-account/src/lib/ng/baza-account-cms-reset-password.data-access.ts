import { Injectable } from '@angular/core';
import {
    AccountCmsResetPasswordEndpoint,
    AccountCmsResetPasswordEndpointPaths,
    RequestAllAccountsResetPasswordCmsAccountRequest,
    RequestResetPasswordCmsAccountRequest,
} from '@scaliolabs/baza-core-shared';
import { BazaCmsDataAccessService } from '../../../../../ng/src/lib/baza-cms-data-access.service';
import { Observable } from 'rxjs';

@Injectable()
export class BazaAccountCmsResetPasswordDataAccess implements AccountCmsResetPasswordEndpoint {
    constructor(private readonly ngEndpoint: BazaCmsDataAccessService) {}

    requestResetPassword(request: RequestResetPasswordCmsAccountRequest): Observable<void> {
        return this.ngEndpoint.post(AccountCmsResetPasswordEndpointPaths.requestResetPassword, request);
    }

    requestResetPasswordAllAccounts(request: RequestAllAccountsResetPasswordCmsAccountRequest): Observable<void> {
        return this.ngEndpoint.post(AccountCmsResetPasswordEndpointPaths.requestResetPasswordAllAccounts, request);
    }
}
