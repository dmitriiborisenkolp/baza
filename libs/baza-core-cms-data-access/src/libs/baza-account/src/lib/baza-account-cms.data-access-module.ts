import { NgModule } from '@angular/core';
import { BazaAccountCmsDataAccess } from './ng/baza-account-cms.data-access';
import { BazaAccountCmsResetPasswordDataAccess } from './ng/baza-account-cms-reset-password.data-access';
import { BazaCmsDataAccessModule } from '../../../../ng/src/lib/baza-cms-data-access.module';

// @dynamic
// @dynamic
@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaAccountCmsDataAccess, BazaAccountCmsResetPasswordDataAccess],
})
export class BazaAccountCmsDataAccessModule {}
