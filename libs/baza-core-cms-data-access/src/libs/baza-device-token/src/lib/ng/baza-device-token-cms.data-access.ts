import { Injectable } from '@angular/core';
import {
    BazaDeviceTokenCmsEndpoint,
    BazaDeviceTokenCmsEndpointPaths,
    BazaDeviceTokenGetAllRequest,
    BazaDeviceTokenGetAllResponse,
    BazaDeviceTokenLinkRequest,
    BazaDeviceTokenLinkResponse,
    BazaDeviceTokenUnlinkRequest,
    BazaDeviceTokenUnlinkResponse,
} from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { BazaCmsDataAccessService } from '../../../../../ng/src';

@Injectable()
export class BazaDeviceTokenCmsDataAccess implements BazaDeviceTokenCmsEndpoint {
    constructor(private readonly ngEndpoint: BazaCmsDataAccessService) {}

    getAll(request: BazaDeviceTokenGetAllRequest): Observable<BazaDeviceTokenGetAllResponse> {
        return this.ngEndpoint.post(BazaDeviceTokenCmsEndpointPaths.GetAll, request);
    }

    link(request: BazaDeviceTokenLinkRequest): Observable<BazaDeviceTokenLinkResponse> {
        return this.ngEndpoint.post(BazaDeviceTokenCmsEndpointPaths.Link, request);
    }

    unlink(request: BazaDeviceTokenUnlinkRequest): Observable<BazaDeviceTokenUnlinkResponse> {
        return this.ngEndpoint.post(BazaDeviceTokenCmsEndpointPaths.Unlink, request);
    }
}
