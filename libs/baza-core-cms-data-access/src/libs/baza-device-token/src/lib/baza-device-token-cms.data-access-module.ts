import { NgModule } from '@angular/core';
import { BazaCmsDataAccessModule } from '../../../../ng/src/lib/baza-cms-data-access.module';
import { BazaDeviceTokenCmsDataAccess } from './ng/baza-device-token-cms.data-access';

// @dynamic
@NgModule({
    imports: [BazaCmsDataAccessModule],
    providers: [BazaDeviceTokenCmsDataAccess],
})
export class BazaDeviceTokenCmsDataAccessModule {}
