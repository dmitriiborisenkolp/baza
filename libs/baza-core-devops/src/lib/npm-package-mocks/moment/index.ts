/**
 * Moment Disabler
 *
 * Moment Mock will disable accident usage of moment.js library in ng / shared space
 * Moment library is really huge ang usually we'd like to avoid usage of moment
 */

/* eslint @typescript-eslint/no-empty-function: 0 */

function throwError() {
    throw new Error('Moment.js is disabled for shared / ng spaces');
}

// Any attempts to import typeorm package will lead to error and failed e2e tests.
throwError();

// Exported functions
export function moment(...args: any) { throwError() }
export function Moment(...args: any) { throwError() }

// Export default
export default moment;
