/**
 * Class-Transformer Mock
 *
 * Class-Transformer mock will allow to use class-transformer decorators for shared/ng space, but implementation will
 * be mocked.
 * All DTO's from Shared space may have proper class transformrs with class-transformer which is heavily used at API.
 * But in the same time we don't need validations on Ng side
 *
 * @see https://www.npmjs.com/package/class-transformer
 */

/* eslint @typescript-eslint/no-empty-function: 0 */

import "reflect-metadata";

const mock = Symbol('bazaDevopsClassTransformerMock');

function throwError() {
    throw new Error('Class-transform package is disabled for shared / ng spaces. You can use decorators from class-transformer of class-validator packages, but usage of exported functions is prohibited');
}

// Exported functions

export function serialize(...args: any) { throwError(); }
export function deserialize(...args: any) { throwError(); }
export function deserializeArray(...args: any) { throwError(); }
export function plainToClass(...args: any) { throwError(); }
export function plainToClassFromExist(...args: any) { throwError(); }
export function classToPlain(...args: any) { throwError(); }
export function classToClass(...args: any) { throwError(); }

// Exported decorators

export function Type(...args: any) { return Reflect.metadata(mock, true); }
export function Expose(...args: any) { return Reflect.metadata(mock, true); }
export function Exclude(...args: any) { return Reflect.metadata(mock, true); }
export function Transform(...args: any) { return Reflect.metadata(mock, true); }

