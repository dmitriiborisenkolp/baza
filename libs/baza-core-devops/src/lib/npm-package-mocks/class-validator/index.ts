/**
 * Class-Validator Mock
 *
 * Class-Validator mock will allow to use class-validator decorators for shared/ng space, but implementation will
 * be mocked.
 * All DTO's from Shared space should have proper validation with class-validator which is heavily used at API. But in
 * the same time we don't need validations on Ng side
 *
 * @see https://github.com/typestack/class-validator#validation-decorators
 */

/* eslint @typescript-eslint/no-empty-function: 0 */

import "reflect-metadata";

const mock = Symbol('bazaDevopsClassValidatorMock');

function throwError() {
    throw new Error('Class-validator package is disabled for shared / ng spaces. You can use decorators from class-transformer of class-validator packages, but usage of exported functions is prohibited');
}

// Exported Interfaces

export class ValidatorOptions { [key: string]: any }
export class ValidationError { [key: string]: any }
export class ValidationArguments { [key: string]: any }

// Exported Functions

export function validate(...args: any) { throwError(); }
export function validateOrReject(...args: any) { throwError(); }
export function useContainer(...args: any) { throwError(); }

// Common validation decorators
export function IsDefined(...args: any) { return Reflect.metadata(mock, true); }
export function IsOptional(...args: any) { return Reflect.metadata(mock, true); }
export function Equals(...args: any) { return Reflect.metadata(mock, true); }
export function NotEquals(...args: any) { return Reflect.metadata(mock, true); }
export function IsEmpty(...args: any) { return Reflect.metadata(mock, true); }
export function IsNotEmpty(...args: any) { return Reflect.metadata(mock, true); }

// Type validation decorators
export function IsIn(...args: any) { return Reflect.metadata(mock, true); }
export function IsNotIn(...args: any) { return Reflect.metadata(mock, true); }
export function IsBoolean(...args: any) { return Reflect.metadata(mock, true); }
export function IsDate(...args: any) { return Reflect.metadata(mock, true); }
export function IsString(...args: any) { return Reflect.metadata(mock, true); }
export function IsNumber(...args: any) { return Reflect.metadata(mock, true); }
export function IsInt(...args: any) { return Reflect.metadata(mock, true); }
export function IsArray(...args: any) { return Reflect.metadata(mock, true); }
export function IsEnum(...args: any) { return Reflect.metadata(mock, true); }

// Number validation decorators
export function IsDivisibleBy(...args: any) { return Reflect.metadata(mock, true); }
export function IsPositive(...args: any) { return Reflect.metadata(mock, true); }
export function IsNegative(...args: any) { return Reflect.metadata(mock, true); }
export function Min(...args: any) { return Reflect.metadata(mock, true); }
export function Max(...args: any) { return Reflect.metadata(mock, true); }

// Date validation decorators
export function MinDate(...args: any) { return Reflect.metadata(mock, true); }
export function MaxDate(...args: any) { return Reflect.metadata(mock, true); }

// String-type validation decorators
export function IsBooleanString(...args: any) { return Reflect.metadata(mock, true); }
export function IsDateString(...args: any) { return Reflect.metadata(mock, true); }
export function IsNumberString(...args: any) { return Reflect.metadata(mock, true); }

// String validation decorators
export function Contains(...args: any) { return Reflect.metadata(mock, true); }
export function NotContains(...args: any) { return Reflect.metadata(mock, true); }
export function IsAlpha(...args: any) { return Reflect.metadata(mock, true); }
export function IsAlphanumeric(...args: any) { return Reflect.metadata(mock, true); }
export function IsDecimal(...args: any) { return Reflect.metadata(mock, true); }
export function IsAscii(...args: any) { return Reflect.metadata(mock, true); }
export function IsBase32(...args: any) { return Reflect.metadata(mock, true); }
export function IsBase64(...args: any) { return Reflect.metadata(mock, true); }
export function IsIBAN(...args: any) { return Reflect.metadata(mock, true); }
export function IsBIC(...args: any) { return Reflect.metadata(mock, true); }
export function IsByteLength(...args: any) { return Reflect.metadata(mock, true); }
export function IsCreditCard(...args: any) { return Reflect.metadata(mock, true); }
export function IsCurrency(...args: any) { return Reflect.metadata(mock, true); }
export function IsEthereumAddress(...args: any) { return Reflect.metadata(mock, true); }
export function IsBtcAddress(...args: any) { return Reflect.metadata(mock, true); }
export function IsDataURI(...args: any) { return Reflect.metadata(mock, true); }
export function IsEmail(...args: any) { return Reflect.metadata(mock, true); }
export function IsFQDN(...args: any) { return Reflect.metadata(mock, true); }
export function IsFullWidth(...args: any) { return Reflect.metadata(mock, true); }
export function IsHalfWidth(...args: any) { return Reflect.metadata(mock, true); }
export function IsVariableWidth(...args: any) { return Reflect.metadata(mock, true); }
export function IsHexColor(...args: any) { return Reflect.metadata(mock, true); }
export function IsHSLColor(...args: any) { return Reflect.metadata(mock, true); }
export function IsRgbColor(...args: any) { return Reflect.metadata(mock, true); }
export function IsIdentityCard(...args: any) { return Reflect.metadata(mock, true); }
export function IsPassportNumber(...args: any) { return Reflect.metadata(mock, true); }
export function IsHexadecimal(...args: any) { return Reflect.metadata(mock, true); }
export function IsOctal(...args: any) { return Reflect.metadata(mock, true); }
export function IsMACAddress(...args: any) { return Reflect.metadata(mock, true); }
export function IsIP(...args: any) { return Reflect.metadata(mock, true); }
export function IsISBN(...args: any) { return Reflect.metadata(mock, true); }
export function IsEAN(...args: any) { return Reflect.metadata(mock, true); }
export function IsISIN(...args: any) { return Reflect.metadata(mock, true); }
export function IsISO8601(...args: any) { return Reflect.metadata(mock, true); }
export function IsJSON(...args: any) { return Reflect.metadata(mock, true); }
export function IsJWT(...args: any) { return Reflect.metadata(mock, true); }
export function IsObject(...args: any) { return Reflect.metadata(mock, true); }
export function IsNotEmptyObject(...args: any) { return Reflect.metadata(mock, true); }
export function IsLowercase(...args: any) { return Reflect.metadata(mock, true); }
export function IsLatLong(...args: any) { return Reflect.metadata(mock, true); }
export function IsLatitude(...args: any) { return Reflect.metadata(mock, true); }
export function IsLongitude(...args: any) { return Reflect.metadata(mock, true); }
export function IsMobilePhone(...args: any) { return Reflect.metadata(mock, true); }
export function IsISO31661Alpha2(...args: any) { return Reflect.metadata(mock, true); }
export function IsISO31661Alpha3(...args: any) { return Reflect.metadata(mock, true); }
export function IsLocale(...args: any) { return Reflect.metadata(mock, true); }
export function IsPhoneNumber(...args: any) { return Reflect.metadata(mock, true); }
export function IsMongoId(...args: any) { return Reflect.metadata(mock, true); }
export function IsMultibyte(...args: any) { return Reflect.metadata(mock, true); }
export function IsSurrogatePair(...args: any) { return Reflect.metadata(mock, true); }
export function IsUrl(...args: any) { return Reflect.metadata(mock, true); }
export function IsMagnetURI(...args: any) { return Reflect.metadata(mock, true); }
export function IsUUID(...args: any) { return Reflect.metadata(mock, true); }
export function IsFirebasePushId(...args: any) { return Reflect.metadata(mock, true); }
export function IsUppercase(...args: any) { return Reflect.metadata(mock, true); }
export function Length(...args: any) { return Reflect.metadata(mock, true); }
export function MinLength(...args: any) { return Reflect.metadata(mock, true); }
export function MaxLength(...args: any) { return Reflect.metadata(mock, true); }
export function Matches(...args: any) { return Reflect.metadata(mock, true); }
export function IsMilitaryTime(...args: any) { return Reflect.metadata(mock, true); }
export function IsHash(...args: any) { return Reflect.metadata(mock, true); }
export function IsMimeType(...args: any) { return Reflect.metadata(mock, true); }
export function IsSemVer(...args: any) { return Reflect.metadata(mock, true); }
export function IsISSN(...args: any) { return Reflect.metadata(mock, true); }
export function IsISRC(...args: any) { return Reflect.metadata(mock, true); }
export function IsRFC3339(...args: any) { return Reflect.metadata(mock, true); }

// Array validation decorators
export function ArrayContains(...args: any) { return Reflect.metadata(mock, true); }
export function ArrayNotContains(...args: any) { return Reflect.metadata(mock, true); }
export function ArrayNotEmpty(...args: any) { return Reflect.metadata(mock, true); }
export function ArrayMinSize(...args: any) { return Reflect.metadata(mock, true); }
export function ArrayMaxSize(...args: any) { return Reflect.metadata(mock, true); }
export function ArrayUnique(...args: any) { return Reflect.metadata(mock, true); }

// Object validation decorators
export function IsInstance(...args: any) { return Reflect.metadata(mock, true); }

// Other decorators
export function Allow(...args: any) { return Reflect.metadata(mock, true); }
export function Validate(...args: any) { return Reflect.metadata(mock, true); }
export function ValidatorConstraint(...args: any) { return Reflect.metadata(mock, true); }
export function ValidateNested(...args: any) { return Reflect.metadata(mock, true); }
export function ValidatePromise(...args: any) { return Reflect.metadata(mock, true); }
export function ValidateIf(...args: any) { return Reflect.metadata(mock, true); }
