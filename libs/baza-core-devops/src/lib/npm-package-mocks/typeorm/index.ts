/**
 * TypeOrm Disabler
 *
 * TypeORM mock will prevents accident usage of TypeORM decorators in shared / cms / web libraries.
 * Any attempts to use TypeOrm decorators in DTO's will lead to "this expression is not callable" for web build
 *
 * @see https://github.com/typeorm/typeorm/blob/master/docs/decorator-reference.md
 */

/* eslint @typescript-eslint/no-empty-function: 0 */

// Entity decorators
export function Entity(...args: any) {}
export function ViewEntity(...args: any) {}

// Column decorators
export function Column(...args: any) {}
export function PrimaryColumn(...args: any) {}
export function ObjectIdColumn(...args: any) {}
export function CreateDateColumn(...args: any) {}
export function UpdateDateColumn(...args: any) {}
export function DeleteDateColumn(...args: any) {}
export function VersionColumn(...args: any) {}
export function Generated(...args: any) {}

// Relation decorators
export function OneToOne(...args: any) {}
export function ManyToOne(...args: any) {}
export function OneToMany(...args: any) {}
export function ManyToMany(...args: any) {}
export function JoinColumn(...args: any) {}
export function JoinTable(...args: any) {}
export function RelationId(...args: any) {}

// Subscriber and listener decorators
export function AfterLoad(...args: any) {}
export function BeforeInsert(...args: any) {}
export function AfterInsert(...args: any) {}
export function BeforeUpdate(...args: any) {}
export function AfterUpdate(...args: any) {}
export function BeforeRemove(...args: any) {}
export function AfterRemove(...args: any) {}
export function EventSubscriber(...args: any) {}

// Other decorators
export function Index(...args: any) {}
export function Unique(...args: any) {}
export function Check(...args: any) {}
export function Exclusion(...args: any) {}
export function Transaction(...args: any) {}
export function TransactionManager(...args: any) {}
export function TransactionRepository(...args: any) {}
export function EntityRepository(...args: any) {}
