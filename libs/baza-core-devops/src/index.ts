// Do not export NPM-mock, import only .
import './lib/npm-package-mocks/class-transformer';
import './lib/npm-package-mocks/class-validator';
import './lib/npm-package-mocks/moment';
import './lib/npm-package-mocks/typeorm';
