# baza-core-devops

This library contains useful resources for devops.

## NPM Package Mocks

- This directory contains mocks for some of NPM libraries for Angular applications.
- Additionally, these packages will prevent usage of some packages in Ng space.
- Mocks should be applied specifically on WEB or CMS builds. WEB/CMS may have different set of NPM libraries to mock.

### How to use NPM package Mocks

1. Define list of packages which should me mocked for each Angular Application
2. Remove NPM libraries with `rm -rf node_modules/(target build)` which you are going to mock
3. Copy mocks from `npm-package-mocks` to `node_modules` directory
4. Build application using `yarn cms:build` / `yarn web:build` or using `nx` CLI tool

Please note that different Angular applications (CMS or WEB) may require different set of mocked NPM libraries.
