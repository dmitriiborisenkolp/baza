import { Component } from '@angular/core';

@Component({
    selector: 'app-marketing-footer-social',
    templateUrl: './footer-social.component.html',
    styleUrls: ['./footer-social.component.less'],
})
export class FooterSocialComponent {
    today = new Date();
}
