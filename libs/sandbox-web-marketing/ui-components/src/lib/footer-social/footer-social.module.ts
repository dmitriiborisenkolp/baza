import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { FooterSocialComponent } from './footer-social.component';

@NgModule({
    declarations: [FooterSocialComponent],
    exports: [FooterSocialComponent],
    imports: [CommonModule, NzGridModule, RouterModule, UtilModule, NzLayoutModule],
})
export class FooterSocialModule {}
