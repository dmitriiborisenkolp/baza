import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { MarketingHeaderComponent } from './header.component';

@NgModule({
    declarations: [MarketingHeaderComponent],
    exports: [MarketingHeaderComponent],
    imports: [CommonModule, NzGridModule, RouterModule, UtilModule, NzLayoutModule],
})
export class MarketingHeaderModule {}
