import { Component } from '@angular/core';

@Component({
    selector: 'app-marketing-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less'],
})
export class MarketingHeaderComponent {}
