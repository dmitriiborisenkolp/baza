import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NewslettersDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { FooterSocialModule, MarketingHeaderModule } from '@scaliolabs/sandbox-web-marketing/ui-components';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing.component';

@NgModule({
    declarations: [LandingComponent],
    imports: [
        CommonModule,
        LandingRoutingModule,
        NzButtonModule,
        NzFormModule,
        NzGridModule,
        NzInputModule,
        ReactiveFormsModule,
        UtilModule,
        MarketingHeaderModule,
        FooterSocialModule,
        NzLayoutModule,
        NzNotificationModule,
    ],
    providers: [NewslettersDataAccess],
})
export class LandingModule {}
