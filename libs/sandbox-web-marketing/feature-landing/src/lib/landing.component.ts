import { ChangeDetectionStrategy, Component, NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NewslettersDataAccess } from '@scaliolabs/baza-content-types-data-access';
import { LoaderService } from '@scaliolabs/baza-web-utils';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { BehaviorSubject, EMPTY } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';

@UntilDestroy()
@Component({
    selector: 'app-marketing-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingComponent {
    success$ = new BehaviorSubject<boolean>(false);
    form = this.fb.group({
        email: ['', Validators.compose([Validators.required, Validators.email])],
    });

    constructor(
        private readonly fb: FormBuilder,
        private newslettersDataAccess: NewslettersDataAccess,
        private readonly notification: NzNotificationService,
        private loader: LoaderService,
        private ngZone: NgZone,
    ) {}

    onFormSubmit() {
        const value = this.form.value;
        this.loader.show();
        this.newslettersDataAccess
            .subscribe({ email: value.email })
            .pipe(
                untilDestroyed(this),
                tap(() => {
                    this.success$.next(true);
                }),
                catchError(() => {
                    this.notification.error('Oh no! Something went wrong. Try again shortly.', '');
                    return EMPTY;
                }),
                finalize(() => {
                    this.onFormReset();
                    this.ngZone.run(() => {
                        this.loader.hide();
                    });
                }),
            )
            .subscribe();
    }

    onFormReset(): void {
        this.form.reset();
    }
}
