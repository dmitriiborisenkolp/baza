import { Injectable } from '@angular/core';
import { BazaDocsDto, BazaDocsEndpoint, BazaDocsEndpointPaths, BazaDocsFileDto, BazaDocsFileRequest } from '@scaliolabs/baza-docs-shared';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import { Observable } from 'rxjs';

@Injectable()
export class BazaDocsDataAccess implements BazaDocsEndpoint {
    constructor(
        private readonly http: BazaDataAccessService,
    ) {}

    file(request: BazaDocsFileRequest): Observable<BazaDocsFileDto> {
        return this.http.get(BazaDocsEndpointPaths.File, request);
    }

    structure(): Observable<BazaDocsDto> {
        return this.http.get(BazaDocsEndpointPaths.Structure);
    }
}
