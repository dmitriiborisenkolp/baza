import { NgModule } from '@angular/core';
import { BazaDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaDocsDataAccess } from './baza-docs.data-access';

@NgModule({
    imports: [
        BazaDataAccessModule,
    ],
    providers: [
        BazaDocsDataAccess,
    ],
})
export class BazaDocsDataAccessModule {
}
