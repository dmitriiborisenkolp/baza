/**
 * Baza NC Integration Schema Bundle Configuration (Config)
 */
import { InjectionToken } from '@angular/core';

export class BazaNcIntegrationBundlesCmsConfig {
    withSchemaFilter: Array<string>;
    withSchemaCMS: boolean;
    withEditableSchemaCMS: boolean;
    withStatementsFeature: boolean;
    withTimelineFeature: boolean;
    withTestimonialsFeature: boolean;
    withListingMetadataFieldFeature: boolean;
}

/**
 * Baza NC Integration Schema Bundle Configuration (Config Value)
 */
const BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG: BazaNcIntegrationBundlesCmsConfig = {
    withSchemaFilter: [],
    withSchemaCMS: true,
    withEditableSchemaCMS: true,
    withStatementsFeature: false,
    withTimelineFeature: false,
    withTestimonialsFeature: false,
    withListingMetadataFieldFeature: false,
};

/**
 * Baza NC Integration Schema Bundle Configuration (Injection Token for Angular Modules)
 */
export const BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN = new InjectionToken('BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN');

/**
 * Returns configuration for BazaNcIntegration CMS bundle
 * If `partialConfig` is passed, additionally updates current configuration
 * @param partialConfig
 */
export function bazaNcIntegrationBundleCmsConfig(
    partialConfig?: Partial<BazaNcIntegrationBundlesCmsConfig>,
): BazaNcIntegrationBundlesCmsConfig {
    if (partialConfig) {
        Object.assign(BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG, partialConfig);
    }

    return BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG;
}
