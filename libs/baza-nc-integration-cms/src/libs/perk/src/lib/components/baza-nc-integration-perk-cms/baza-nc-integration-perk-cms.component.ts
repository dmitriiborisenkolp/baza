import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationAcl, BazaNcIntegrationPerkDto } from '@scaliolabs/baza-nc-integration-shared';
import { Subject, throwError } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BazaNcIntegrationPerkCmsResolveData } from './baza-nc-integration-perk-cms.resolve';
import { BazaNcIntegrationPerkCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { catchError, takeUntil, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { bazaNcBundleCmsConfig } from '@scaliolabs/baza-nc-cms';

export const BAZA_NC_INTEGRATION_PERKS_CMS_ID = 'baza-nc-integration-perks-cms-id';
export const BAZA_NC_INTEGRATION_PERKS_FORM_ID = 'baza-nc-integration-perks-form-id';

interface State {
    crud: BazaCrudConfig<BazaNcIntegrationPerkDto>;
    maxSortOrder: number;
}

interface FormValue {
    title: string;
    type?: string;
    isForEliteInvestors: boolean;
    metadata: Record<string, string>;
}

@Component({
    templateUrl: './baza-nc-integration-perk-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationPerkCmsComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<BazaNcIntegrationPerkDto>;

    public state: State = {
        crud: this.config,
        maxSortOrder: 0,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly messages: BazaNgMessagesService,
        private readonly dataAccess: BazaNcIntegrationPerkCmsDataAccess,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.perk.components.bazaIntegrationPerkCms.${key}`;
    }

    get config(): BazaCrudConfig<BazaNcIntegrationPerkDto> {
        return {
            id: BAZA_NC_INTEGRATION_PERKS_CMS_ID,
            title: this.i18n('title'),
            titleArguments: {
                listings: this.resolvedData.listings,
            },
            backRouterLink: {
                routerLink: ['/baza-nc-integration/listings'],
            },
            navigation: {
                nodes: [
                    {
                        title: 'bazaNcIntegration.listings.title',
                        routerLink: {
                            routerLink: ['/baza-nc-integration/listings'],
                        },
                    },
                    {
                        title: this.resolvedData.listings.title,
                        translateArgs: this.resolvedData,
                    },
                    {
                        title: this.i18n('navigation'),
                        routerLink: {
                            routerLink: ['/baza-nc-integration/listings', this.resolvedData.listings.id, 'perks'],
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccess
                            .list({
                                ...request,
                                listingId: this.resolvedData.listings.id,
                            })
                            .pipe(tap((response) => (this.state.maxSortOrder = response.maxSortOrder))),
                },
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                    },
                    {
                        field: 'title',
                        title: this.i18n('columns.title'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                    {
                        field: 'type',
                        title: this.i18n('columns.type'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 230,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'isForEliteInvestors',
                        title: this.i18n('columns.isForEliteInvestors'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        width: 120,
                        disabled: !bazaNcBundleCmsConfig().withEliteInvestorsFeature,
                    },
                    {
                        field: 'isPublished',
                        title: this.i18n('columns.isPublished'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        width: 120,
                    },
                ],
                actions: [
                    {
                        title: this.i18n('actions.update'),
                        icon: 'edit',
                        action: (entity) => this.update(entity),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement],
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'arrow-up',
                        action: (entity) => this.moveUp(entity),
                        visible: (entity) => entity.sortOrder > 1,
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement],
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'arrow-down',
                        action: (entity) => this.moveDown(entity),
                        visible: (entity) => entity.sortOrder < this.state.maxSortOrder,
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement],
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) => this.delete(entity),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement],
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.create'),
                            callback: () => this.create(),
                        },
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationPerkManagement],
                    },
                ],
            },
        };
    }

    get resolvedData(): BazaNcIntegrationPerkCmsResolveData {
        return this.activatedRoute.snapshot.data['data'];
    }

    formGroup(): FormGroup {
        return this.fb.group({
            title: [undefined, [Validators.required]],
            type: [undefined, []],
            isForEliteInvestors: [false, [Validators.required]],
            metadata: [{}, [Validators.required]],
        });
    }

    formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_NC_INTEGRATION_PERKS_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        formControlName: 'title',
                        type: BazaFormBuilderControlType.Text,
                        label: this.i18n('fields.title'),
                        required: true,
                    },
                    {
                        formControlName: 'type',
                        type: BazaFormBuilderControlType.Text,
                        label: this.i18n('fields.type'),
                    },
                    {
                        formControlName: 'isForEliteInvestors',
                        type: BazaFormBuilderControlType.Checkbox,
                        label: this.i18n('fields.isForEliteInvestors'),
                    },
                    {
                        formControlName: 'metadata',
                        type: BazaFormBuilderControlType.BazaKeyValue,
                        label: this.i18n('fields.metadata'),
                    },
                ],
            },
        };
    }

    create(): void {
        const formGroup = this.formGroup();

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaNcIntegrationPerkDto, FormValue>({
            formGroup,
            formBuilder: this.formBuilder(),
            title: this.i18n('actions.create'),
            onSubmit: (entity, formValue) =>
                this.dataAccess
                    .create({
                        ...formValue,
                        listingId: this.resolvedData.listings.id,
                    })
                    .pipe(tap(() => this.crud.refresh())),
        });
    }

    update(entity: BazaNcIntegrationPerkDto): void {
        const formGroup = this.formGroup();

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaNcIntegrationPerkDto, FormValue>({
            formGroup,
            formBuilder: this.formBuilder(),
            title: this.i18n('actions.update'),
            fetch: () =>
                this.dataAccess.getById({
                    id: entity.id,
                }),
            onSubmit: (entity, formValue) =>
                this.dataAccess
                    .update({
                        ...formValue,
                        id: entity.id,
                    })
                    .pipe(tap(() => this.crud.refresh())),
        });
    }

    delete(entity: BazaNcIntegrationPerkDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.confirm.open({
            message: this.i18n('actions.delete.confirm'),
            messageArgs: entity,
            confirm: () =>
                this.dataAccess
                    .delete({
                        id: entity.id,
                    })
                    .pipe(
                        tap(() =>
                            this.messages.success({
                                translate: true,
                                message: this.i18n('actions.delete.success'),
                                translateParams: entity,
                            }),
                        ),
                        tap(() => this.crud.refresh()),
                        catchError((err) => {
                            this.messages.error(err);

                            return throwError(err);
                        }),
                    )
                    .toPromise(),
        });
    }

    moveUp(entity: BazaNcIntegrationPerkDto): void {
        this.dataAccess
            .setSortOrder({
                id: entity.id,
                setSortOrder: entity.sortOrder - 1,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => this.crud.refresh(),
                (err) => this.messages.bazaError(err),
            );
    }

    moveDown(entity: BazaNcIntegrationPerkDto): void {
        this.dataAccess
            .setSortOrder({
                id: entity.id,
                setSortOrder: entity.sortOrder + 1,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => this.crud.refresh(),
                (err) => this.messages.bazaError(err),
            );
    }
}
