import { NgModule } from '@angular/core';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import {
    BazaNcIntegrationListingsCmsDataAccessModule,
    BazaNcIntegrationPerkCmsDataAccessModule,
} from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNcIntegrationPerkCmsComponent } from './components/baza-nc-integration-perk-cms/baza-nc-integration-perk-cms.component';
import { BazaNcIntegrationPerkCmsResolve } from './components/baza-nc-integration-perk-cms/baza-nc-integration-perk-cms.resolve';

@NgModule({
    imports: [BazaCmsCrudBundleModule, BazaNcIntegrationListingsCmsDataAccessModule, BazaNcIntegrationPerkCmsDataAccessModule],
    declarations: [BazaNcIntegrationPerkCmsComponent],
    providers: [BazaNcIntegrationPerkCmsResolve],
})
export class BazaNcIntegrationPerkModule {}
