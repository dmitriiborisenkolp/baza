import { Routes } from '@angular/router';
import { BazaNcIntegrationPerkCmsComponent } from './components/baza-nc-integration-perk-cms/baza-nc-integration-perk-cms.component';
import { BazaNcIntegrationPerkCmsResolve } from './components/baza-nc-integration-perk-cms/baza-nc-integration-perk-cms.resolve';

export const bazaNcIntegrationPerkRoutes: Routes = [
    {
        path: 'perks/:listingId',
        component: BazaNcIntegrationPerkCmsComponent,
        resolve: {
            data: BazaNcIntegrationPerkCmsResolve,
        },
    },
];
