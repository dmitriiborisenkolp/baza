import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaNcIntegrationListingsCmsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';

export interface BazaNcIntegrationSubscriptionCmsResolveData {
    listing: BazaNcIntegrationListingsCmsDto;
}

@Injectable()
export class BazaNcIntegrationSubscriptionCmsResolve implements Resolve<BazaNcIntegrationSubscriptionCmsResolveData> {
    constructor(private readonly dataAccess: BazaNcIntegrationListingsCmsDataAccess) {}

    resolve(route: ActivatedRouteSnapshot): Observable<BazaNcIntegrationSubscriptionCmsResolveData> {
        const observables = [
            this.dataAccess.getById({
                id: parseInt(route.params['id'], 10),
            }),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([listing]) => ({
                listing,
            })),
        );
    }
}
