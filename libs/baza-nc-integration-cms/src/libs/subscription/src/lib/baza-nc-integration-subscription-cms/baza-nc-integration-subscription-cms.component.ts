import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { CrudExportToCsvField } from '@scaliolabs/baza-core-shared';
import { bazaNcBundleCmsConfig } from '@scaliolabs/baza-nc-cms';
import { BazaNcIntegrationSubscriptionCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationSubscribeMessages,
    BazaNcIntegrationSubscribeStatus,
    BazaNcIntegrationSubscriptionCsvRow,
    BazaNcIntegrationSubscriptionDto,
} from '@scaliolabs/baza-nc-integration-shared';
import * as moment from 'moment';
import { BehaviorSubject, firstValueFrom, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { BazaNcIntegrationSubscriptionCmsResolveData } from './baza-nc-integration-subscription-cms.resolve';

export const BAZA_NC_INTEGRATION_SUBSCRIPTIONS_CMS_ID = 'baza-nc-integration-subscriptions-cms-id';
export const BAZA_NC_INTEGRATION_SUBSCRIPTIONS_FORM_ID = 'baza-nc-integration-subscriptions-form-id';

interface State {
    config: BazaCrudConfig<BazaNcIntegrationSubscriptionDto>;
}

interface FormValue {
    email: string;
}

@Component({
    templateUrl: './baza-nc-integration-subscription-cms.component.html',
    styleUrls: ['./baza-nc-integration-subscription-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationSubscriptionCmsComponent {
    public crud: BazaCrudComponent<BazaNcIntegrationSubscriptionDto>;
    private disableNotifyUsers$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly endpoint: BazaNcIntegrationSubscriptionCmsDataAccess,
        private readonly activatedRoute: ActivatedRoute,
        private readonly formLayout: BazaFormLayoutService,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly translate: TranslateService,
    ) {}

    i18n(key: string): string {
        return `bazaNcIntegration.subscription.${key}`;
    }

    i18nStatus(status: BazaNcIntegrationSubscribeStatus): string {
        return `bazaNcIntegration.__types.BazaNcIntegrationSubscribeStatus.${status}`;
    }

    i18nSubscriptionMessage(status: BazaNcIntegrationSubscribeMessages): string {
        return `bazaNcIntegration.__types.BazaNcIntegrationSubscribeMessages.${status}`;
    }

    get resolvedData(): BazaNcIntegrationSubscriptionCmsResolveData {
        return this.activatedRoute.snapshot.data['listing'];
    }

    private crudConfig(): BazaCrudConfig<BazaNcIntegrationSubscriptionDto> {
        return {
            id: BAZA_NC_INTEGRATION_SUBSCRIPTIONS_CMS_ID,
            title: this.i18n('title'),
            titleArguments: {
                listing: this.resolvedData.listing,
            },
            backRouterLink: {
                routerLink: ['/baza-nc-integration/listings'],
            },
            items: {
                topRight: [
                    {
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement],
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('exportToCsv'),
                            endpoint: (request) =>
                                this.endpoint.exportToCsv({
                                    ...request,
                                    fields: [
                                        {
                                            field: 'id',
                                            title: this.translate.instant(this.i18n('columns.id')),
                                        },
                                        {
                                            field: 'email',
                                            title: this.translate.instant(this.i18n('columns.email')),
                                        },
                                        {
                                            field: 'account',
                                            title: this.translate.instant(this.i18n('columns.account')),
                                        },
                                        {
                                            field: 'subscribed',
                                            title: this.translate.instant(this.i18n('columns.subscribed')),
                                        },
                                        {
                                            field: 'sent',
                                            title: this.translate.instant(this.i18n('columns.sent')),
                                        },
                                    ] as Array<CrudExportToCsvField<BazaNcIntegrationSubscriptionCsvRow>>,
                                    listRequest: {
                                        ...this.crud.data.state.lastRequest,
                                        listingId: this.resolvedData.listing.id,
                                    },
                                }),
                            fileNameGenerator: () => 'subscriptions-' + moment(new Date()).format('mm-dd-yyyy'),
                        },
                    },
                    {
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement],
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<BazaNcIntegrationSubscriptionDto, FormValue>({
                                    formGroup: this.formGroup(),
                                    formBuilder: this.formBuilder(),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (entity, formValue) =>
                                        this.endpoint
                                            .include({
                                                ...formValue,
                                                listingId: this.resolvedData.listing.id,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                }),
                        },
                    },
                    {
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement],
                        type: BazaCrudItem.Button,
                        visible: of(bazaNcBundleCmsConfig().withNotifySubscribedUsersFeature),
                        options: {
                            type: BazaNzButtonStyle.Primary,
                            title: this.i18n('actions.notify.label'),
                            disabled: this.disableNotifyUsers$.asObservable(),
                            callback: () =>
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.ngConfirm.open({
                                    message: this.i18n('actions.notify.confirm'),
                                    confirmText: this.i18n('actions.notify.okText'),
                                    confirm: () => firstValueFrom(this.endpoint.notifyUsers({ listingId: this.resolvedData.listing.id })),
                                }),
                        },
                    },
                ],
            },
            navigation: {
                nodes: [
                    {
                        title: 'bazaNcIntegration.listings.title',
                        routerLink: {
                            routerLink: ['/baza-nc-integration/listings'],
                        },
                    },
                    {
                        title: this.resolvedData.listing.title,
                        translateArgs: this.resolvedData,
                    },
                    {
                        title: this.i18n('navigation'),
                        routerLink: {
                            routerLink: ['/baza-nc-integration/listings', this.resolvedData.listing.id, 'subscriptions'],
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.endpoint
                            .list({
                                ...request,
                                listingId: this.resolvedData.listing.id,
                            })
                            .pipe(
                                tap((data) => {
                                    const subscribedList = data.items.filter(
                                        (item) => item.status === BazaNcIntegrationSubscribeStatus.Subscribed,
                                    );
                                    subscribedList.length ? this.disableNotifyUsers$.next(false) : this.disableNotifyUsers$.next(true);
                                }),
                            ),
                },
                actions: [
                    {
                        title: this.i18n('actions.include.label'),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement],
                        icon: 'mail',
                        visible: (entity) =>
                            [BazaNcIntegrationSubscribeStatus.Unsubscribed, BazaNcIntegrationSubscribeStatus.NotSubscribed].includes(
                                entity.status,
                            ),
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.ngConfirm.open({
                                message: this.i18n('actions.include.confirm'),
                                confirm: () =>
                                    this.endpoint
                                        .include({
                                            email: entity.account.email,
                                            listingId: entity.listing.id,
                                        })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            }),
                    },
                    {
                        title: this.i18n('actions.exclude.label'),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingSubscriptionManagement],
                        icon: 'delete',
                        visible: (entity) => [BazaNcIntegrationSubscribeStatus.Subscribed].includes(entity.status),
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.ngConfirm.open({
                                message: this.i18n('actions.exclude.confirm'),
                                confirm: () =>
                                    this.endpoint
                                        .exclude({
                                            email: entity.account.email,
                                            listingId: entity.listing.id,
                                        })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        width: 80,
                    },
                    {
                        field: 'email',
                        title: this.i18n('columns.email'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => entity.account.email,
                        },
                        width: 380,
                    },
                    {
                        field: 'account',
                        title: this.i18n('columns.account'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => entity.account.fullName,
                        },
                    },
                    {
                        field: 'sent',
                        title: this.i18n('columns.sent'),
                        source: (entity) => entity.sent.map((s) => this.i18nSubscriptionMessage(s)),
                        type: {
                            kind: BazaTableFieldType.Tags,
                            translate: true,
                        },
                    },
                    {
                        field: 'subscribed',
                        title: this.i18n('columns.subscribed'),
                        source: (entity) => entity.status === BazaNcIntegrationSubscribeStatus.Subscribed,
                        type: {
                            kind: BazaTableFieldType.Boolean,
                            withFalseIcon: false,
                        },
                        width: 120,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            email: [undefined, [Validators.required]],
        });
    }

    private formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_NC_INTEGRATION_SUBSCRIPTIONS_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Email,
                        label: this.i18n('form.fields.email'),
                        formControlName: 'email',
                    },
                ],
            },
        };
    }
}
