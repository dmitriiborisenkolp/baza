import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsUiModule } from '@scaliolabs/baza-nc-cms';
import { BazaNcIntegrationSubscriptionCmsDataAccessModule } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNcIntegrationSubscriptionCmsComponent } from './baza-nc-integration-subscription-cms/baza-nc-integration-subscription-cms.component';
import { BazaNcIntegrationSubscriptionCmsResolve } from './baza-nc-integration-subscription-cms/baza-nc-integration-subscription-cms.resolve';

@NgModule({
    imports: [CommonModule, BazaNcCmsUiModule, BazaCmsCrudBundleModule, BazaNcIntegrationSubscriptionCmsDataAccessModule],
    declarations: [BazaNcIntegrationSubscriptionCmsComponent],
    providers: [BazaNcIntegrationSubscriptionCmsResolve],
})
export class BazaNcIntegrationSubscriptionCmsModule {}
