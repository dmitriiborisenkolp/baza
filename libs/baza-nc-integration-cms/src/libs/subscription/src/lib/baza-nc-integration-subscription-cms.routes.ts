import { Routes } from '@angular/router';
import { BazaNcIntegrationSubscriptionCmsComponent } from './baza-nc-integration-subscription-cms/baza-nc-integration-subscription-cms.component';
import { BazaNcIntegrationSubscriptionCmsResolve } from './baza-nc-integration-subscription-cms/baza-nc-integration-subscription-cms.resolve';

export const bazaNcIntegrationSubscriptionCmsRoutes: Routes = [
    {
        path: 'subscriptions/:id',
        component: BazaNcIntegrationSubscriptionCmsComponent,
        resolve: {
            listing: BazaNcIntegrationSubscriptionCmsResolve,
        },
    },
];
