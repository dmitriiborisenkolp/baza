import { CmsSidePanelMenuItemConfig } from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationAcl } from '@scaliolabs/baza-nc-integration-shared';

export const bazaNcIntegrationFeedCmsMenu: Array<CmsSidePanelMenuItemConfig> = [
    {
        title: 'bazaNcIntegration.feed.title',
        translate: true,
        icon: 'read',
        routerLink: ['/baza-nc-integration/feed'],
        requiresACL: [
            BazaNcIntegrationAcl.BazaNcIntegrationFeed,
        ],
    },
];
