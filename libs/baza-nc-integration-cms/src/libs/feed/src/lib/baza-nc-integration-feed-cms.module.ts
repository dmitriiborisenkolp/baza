import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaCmsLayoutModule, BazaCrudCmsModule, BazaFormBuilderModule } from '@scaliolabs/baza-core-cms';
import {
    BazaNcIntegrationFeedCmsDataAccessModule,
    BazaNcIntegrationListingsCmsDataAccessModule,
} from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNcIntegrationFeedComponent } from './components/baza-nc-integration-feed/baza-nc-integration-feed.component';
import { BazaNcIntegrationFeedResolve } from './components/baza-nc-integration-feed/baza-nc-integration-feed.resolve';

@NgModule({
    imports: [
        CommonModule,
        BazaNcIntegrationListingsCmsDataAccessModule,
        BazaNcIntegrationFeedCmsDataAccessModule,
        BazaCrudCmsModule,
        BazaFormBuilderModule,
        BazaCmsLayoutModule,
    ],
    declarations: [BazaNcIntegrationFeedComponent],
    providers: [BazaNcIntegrationFeedResolve],
})
export class BazaNcIntegrationFeedCmsModule {}
