import { Routes } from '@angular/router';
import { BazaNcIntegrationFeedComponent } from './components/baza-nc-integration-feed/baza-nc-integration-feed.component';
import { BazaNcIntegrationFeedResolve } from './components/baza-nc-integration-feed/baza-nc-integration-feed.resolve';

export const bazaNcIntegrationFeedRoutes: Routes = [
    {
        path: 'feed',
        component: BazaNcIntegrationFeedComponent,
        resolve: {
            feed: BazaNcIntegrationFeedResolve,
        },
    },
];
