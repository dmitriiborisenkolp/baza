import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaNcIntegrationListingsCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNcIntegrationListingsListItemDto } from '@scaliolabs/baza-nc-integration-shared';

interface ResolveData {
    listings: Array<BazaNcIntegrationListingsListItemDto>;
}

export { ResolveData as BazaNcIntegrationFeedResolveData };

@Injectable()
export class BazaNcIntegrationFeedResolve implements Resolve<ResolveData> {
    constructor(private readonly listings: BazaNcIntegrationListingsCmsDataAccess) {}

    resolve(): Observable<ResolveData> | Promise<ResolveData> | ResolveData {
        const observables = [this.listings.listAll()];

        return combineLatest(...observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([listings]) => ({
                listings,
            })),
        );
    }
}
