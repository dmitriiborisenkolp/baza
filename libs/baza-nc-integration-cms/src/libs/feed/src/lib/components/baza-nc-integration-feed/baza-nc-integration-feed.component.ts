import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCkEditorUploadTransport,
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFieldUploadPreset,
    BazaFileUploadTransport,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute } from '@angular/router';
import { BazaNcIntegrationFeedResolveData } from './baza-nc-integration-feed.resolve';
import { distinctUntilChanged, finalize, map, takeUntil, tap } from 'rxjs/operators';
import { BazaLoadingService, BazaNgConfirmService } from '@scaliolabs/baza-core-ng';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { AttachmentType, bazaSeoDefault, BazaSeoDto } from '@scaliolabs/baza-core-shared';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationFeedCmsPostDto,
    BazaNcIntegrationFeedDisplay,
    BazaNcIntegrationFeedPostType,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationFeedCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaContentTypesFormField, BazaContentTypesFormFields } from '@scaliolabs/baza-content-types-cms';

export const BAZA_NC_INTEGRATION_FEED_CMS_ID = 'baza-nc-integration-feed-cms-id';
export const BAZA_NC_INTEGRATION_FEED_FORM_ID = 'baza-nc-integration-feed-form-id';

const MAX_FILE_SIZE_IMAGE = 128 /* mb */ * 1024 /* kb */ * 1024; /* b */
const MAX_FILE_SIZE_VIDEO = 512 /* mb */ * 1024 /* kb */ * 1024; /* b */

interface FormValue {
    isPublished: boolean;
    allowPublicAccess: boolean;
    type: BazaNcIntegrationFeedPostType;
    title: string;
    intro: string;
    contents: string;
    previewImage: string;
    previewVideo: string;
    tagIds: Array<number>;
    seo: BazaSeoDto;
    listingId?: number;
    display?: BazaNcIntegrationFeedDisplay;
}

interface State {
    config: BazaCrudConfig<BazaNcIntegrationFeedCmsPostDto>;
    lastSortOrder?: number;
}

function isEmpty(input: string): boolean {
    return input === null || input === undefined || (input || '').toString().trim().length === 0;
}

@Component({
    templateUrl: './baza-nc-integration-feed.component.html',
    styleUrls: ['./baza-nc-integration-feed.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationFeedComponent implements OnDestroy {
    public crud: BazaCrudComponent<BazaNcIntegrationFeedCmsPostDto>;

    private ngOnDestroy$: Subject<void> = new Subject<void>();
    private nextFormBuilder$: Subject<void> = new Subject<void>();

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly route: ActivatedRoute,
        private readonly translate: TranslateService,
        private readonly endpoint: BazaNcIntegrationFeedCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly loading: BazaLoadingService,
    ) {}

    ngOnDestroy(): void {
        this.nextFormBuilder$.next();
        this.ngOnDestroy$.next();
    }

    get resolvedData(): BazaNcIntegrationFeedResolveData {
        return this.route.snapshot.data['feed'];
    }

    i18n(key: string): string {
        return `bazaNcIntegration.feed.components.feed.${key}`;
    }

    i18nType(key: BazaNcIntegrationFeedPostType): string {
        return `bazaNcIntegration.feed.types.BazaNcIntegrationFeedPostType.${key}`;
    }

    i18nDisplay(key: BazaNcIntegrationFeedDisplay): string {
        return `bazaNcIntegration.feed.types.BazaNcIntegrationFeedDisplay.${key}`;
    }

    private crudConfig(): BazaCrudConfig<BazaNcIntegrationFeedCmsPostDto> {
        return {
            id: BAZA_NC_INTEGRATION_FEED_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationFeedManagement],
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () => {
                                const formGroup = this.formGroup();

                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<BazaNcIntegrationFeedCmsPostDto, FormValue>({
                                    formGroup,
                                    formBuilder: this.formBuilder(formGroup),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (entity, formValue) =>
                                        this.endpoint
                                            .create({
                                                ...formValue,
                                                listingId:
                                                    !!formValue.listingId && formValue.listingId > 0 ? formValue.listingId : undefined,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                });
                            },
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.endpoint.list(request).pipe(tap((response) => (this.state.lastSortOrder = response.maxSortOrder))),
                    inactive: (row) => !row.isPublished,
                },
                actions: [
                    {
                        title: this.i18n('actions.edit'),
                        icon: 'edit',
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationFeedManagement],
                        action: (entity) => {
                            const formGroup = this.formGroup();

                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<BazaNcIntegrationFeedCmsPostDto, FormValue>({
                                formGroup,
                                formBuilder: this.formBuilder(formGroup),
                                populate: (input, form) => {
                                    form.patchValue(input);
                                    form.get('tagIds').patchValue((input.tags || []).map((tag) => tag.id));

                                    if (!input.listingId || input.listingId < 0) {
                                        form.get('listingId').setValue(-1);
                                    }
                                },
                                title: this.i18n('actions.edit'),
                                fetch: () =>
                                    this.endpoint.getById({
                                        id: entity.id,
                                    }),
                                onSubmit: (entity, formValue) =>
                                    this.endpoint
                                        .update({
                                            ...formValue,
                                            id: entity.id,
                                            listingId: !!formValue.listingId && formValue.listingId > 0 ? formValue.listingId : undefined,
                                        })
                                        .pipe(tap(() => this.crud.refresh())),
                            });
                        },
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'up',
                        visible: (entity) => of(entity.sortOrder !== this.state.lastSortOrder),
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.endpoint
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder + 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'down',
                        visible: (entity) => of(entity.sortOrder > 1),
                        action: (entity) => {
                            const loading = this.loading.addLoading();

                            this.endpoint
                                .setSortOrder({
                                    id: entity.id,
                                    setSortOrder: entity.sortOrder - 1,
                                })
                                .pipe(
                                    finalize(() => loading.complete()),
                                    takeUntil(this.ngOnDestroy$),
                                )
                                .subscribe(() => this.crud.refresh());
                        },
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                confirm: () =>
                                    this.endpoint
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            }),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationFeedManagement],
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        width: 80,
                    },
                    {
                        field: 'title',
                        title: this.i18n('columns.title'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => {
                                switch (entity.type) {
                                    default: {
                                        return entity.title || this.translate.get(this.i18n('noTitle'));
                                    }

                                    case BazaNcIntegrationFeedPostType.Image: {
                                        return entity.title || this.translate.get(this.i18n('imagePost'));
                                    }

                                    case BazaNcIntegrationFeedPostType.Video: {
                                        return entity.title || this.translate.get(this.i18n('videoPost'));
                                    }
                                }
                            },
                        },
                        ngStyle: () => ({
                            'font-weight': '500',
                        }),
                    },
                    {
                        field: 'applause',
                        title: this.i18n('columns.applause'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            translate: true,
                        },
                        width: 40,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'type',
                        title: this.i18n('columns.type'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            translate: true,
                            format: (entity) => this.i18nType(entity.type),
                        },
                        width: 140,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'listingId',
                        title: this.i18n('columns.listing'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            translate: true,
                            translateArgs: (entity) => ({
                                listing: entity.listing,
                            }),
                            format: (entity) => {
                                return entity.listing ? this.i18n('types.listings') : this.i18n('types.common');
                            },
                        },
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group(
            {
                isPublished: [true, [Validators.required]],
                allowPublicAccess: [true, [Validators.required]],
                type: [BazaNcIntegrationFeedPostType.Text, [Validators.required]],
                title: [],
                intro: [],
                contents: [],
                previewImage: [],
                previewVideo: [],
                tagIds: [[]],
                seo: [bazaSeoDefault(), [Validators.required]],
                listingId: [-1, [Validators.required]],
                display: [BazaNcIntegrationFeedDisplay.Public, [Validators.required]],
            },
            {
                validators: (form) => {
                    const formValue: FormValue = form.value;

                    switch (formValue.type) {
                        default: {
                            return null;
                        }

                        case BazaNcIntegrationFeedPostType.Text: {
                            return isEmpty(formValue.intro) || isEmpty(formValue.title) ? { required: true } : null;
                        }

                        case BazaNcIntegrationFeedPostType.Image: {
                            return isEmpty(formValue.previewImage) ? { required: true } : null;
                        }

                        case BazaNcIntegrationFeedPostType.Article: {
                            return isEmpty(formValue.previewImage) || isEmpty(formValue.intro) ? { required: true } : null;
                        }

                        case BazaNcIntegrationFeedPostType.Video: {
                            return isEmpty(formValue.previewVideo) ? { required: true } : null;
                        }
                    }
                },
            },
        );
    }

    private formBuilder(form: FormGroup): BazaFormBuilder<BazaCoreFormBuilderComponents | BazaContentTypesFormFields> {
        this.nextFormBuilder$.next();

        const formValueChanges$: BehaviorSubject<FormValue> = new BehaviorSubject<FormValue>(form.value);

        form.get('type')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextFormBuilder$))
            .subscribe(() => {
                setTimeout(() => {
                    formValueChanges$.next(form.value);
                });
            });

        form.get('listingId')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextFormBuilder$))
            .subscribe(() => {
                setTimeout(() => {
                    formValueChanges$.next(form.value);
                });
            });

        return {
            id: BAZA_NC_INTEGRATION_FEED_FORM_ID,
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('form.tabs.common'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.Checkbox,
                                    label: this.i18n('form.fields.isPublished'),
                                    formControlName: 'isPublished',
                                },
                                {
                                    type: BazaFormBuilderControlType.Checkbox,
                                    label: this.i18n('form.fields.allowPublicAccess'),
                                    formControlName: 'allowPublicAccess',
                                },
                                {
                                    type: BazaFormBuilderStaticComponentType.Divider,
                                },
                                {
                                    type: BazaFormBuilderControlType.Select,
                                    label: this.i18n('form.fields.type'),
                                    formControlName: 'type',
                                    values: Object.values(BazaNcIntegrationFeedPostType).map((value) => ({
                                        label: this.i18nType(value),
                                        translate: true,
                                        value,
                                    })),
                                },
                                {
                                    type: BazaFormBuilderControlType.Select,
                                    label: this.i18n('form.fields.listing.label'),
                                    formControlName: 'listingId',
                                    values: [
                                        {
                                            value: -1,
                                            translate: true,
                                            label: this.i18n('form.fields.listing.none'),
                                        },
                                        ...this.resolvedData.listings.map((listing) => ({
                                            value: listing.id,
                                            translate: true,
                                            translateArgs: {
                                                listing,
                                            },
                                            label: this.i18n('form.fields.listing.item'),
                                        })),
                                    ],
                                },
                                {
                                    type: BazaFormBuilderControlType.Select,
                                    label: this.i18n('form.fields.display'),
                                    formControlName: 'display',
                                    values: Object.values(BazaNcIntegrationFeedDisplay).map((value) => ({
                                        value,
                                        label: this.i18nDisplay(value),
                                        translate: true,
                                    })),
                                    visible: formValueChanges$.pipe(
                                        map((next) => next.listingId),
                                        distinctUntilChanged(),
                                        map((listingId) => !!listingId && listingId > 0),
                                    ),
                                },
                                {
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('form.fields.title'),
                                    formControlName: 'title',
                                    required: form.valueChanges.pipe(
                                        takeUntil(this.nextFormBuilder$),
                                        map((formValue: FormValue) => [BazaNcIntegrationFeedPostType.Text].includes(formValue.type)),
                                    ),
                                },
                                {
                                    type: BazaContentTypesFormField.BazaContentTypeTags,
                                    label: this.i18n('form.fields.tags'),
                                    formControlName: 'tagIds',
                                },
                                {
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('form.fields.intro'),
                                    formControlName: 'intro',
                                    required: form.valueChanges.pipe(
                                        takeUntil(this.nextFormBuilder$),
                                        map((formValue: FormValue) =>
                                            [BazaNcIntegrationFeedPostType.Text, BazaNcIntegrationFeedPostType.Article].includes(
                                                formValue.type,
                                            ),
                                        ),
                                    ),
                                },
                                {
                                    type: BazaFormBuilderControlType.BazaUpload,
                                    label: this.i18n('form.fields.previewImageUrl'),
                                    formControlName: 'previewImage',
                                    props: {
                                        withOptions: {
                                            preset: BazaFieldUploadPreset.Image,
                                            transport: {
                                                type: BazaFileUploadTransport.BazaAttachment,
                                                options: () => ({
                                                    type: AttachmentType.Image,
                                                    payload: {
                                                        maxFileSize: MAX_FILE_SIZE_IMAGE,
                                                    },
                                                }),
                                            },
                                            maxFileSizeBytes: MAX_FILE_SIZE_IMAGE,
                                        },
                                    },
                                    visible: formValueChanges$.pipe(
                                        distinctUntilChanged(),
                                        map((next: FormValue) =>
                                            [
                                                BazaNcIntegrationFeedPostType.Image,
                                                BazaNcIntegrationFeedPostType.Article,
                                                BazaNcIntegrationFeedPostType.Video,
                                            ].includes(next.type),
                                        ),
                                    ),
                                    required: form.valueChanges.pipe(
                                        takeUntil(this.nextFormBuilder$),
                                        map((formValue: FormValue) =>
                                            [BazaNcIntegrationFeedPostType.Image, BazaNcIntegrationFeedPostType.Article].includes(
                                                formValue.type,
                                            ),
                                        ),
                                    ),
                                },
                                {
                                    type: BazaFormBuilderControlType.BazaUpload,
                                    label: this.i18n('form.fields.previewVideoUrl'),
                                    formControlName: 'previewVideo',
                                    props: {
                                        withOptions: {
                                            preset: BazaFieldUploadPreset.Video,
                                            transport: {
                                                type: BazaFileUploadTransport.BazaAttachment,
                                                options: () => ({
                                                    type: AttachmentType.None,
                                                    payload: {
                                                        maxFileSize: MAX_FILE_SIZE_VIDEO,
                                                    },
                                                }),
                                            },
                                            maxFileSizeBytes: MAX_FILE_SIZE_VIDEO,
                                            message: {
                                                text: this.i18n('video'),
                                                translate: true,
                                                translateArgs: {
                                                    maxSizeMb: Math.ceil(MAX_FILE_SIZE_VIDEO / 1024 / 1024),
                                                },
                                            },
                                        },
                                    },
                                    visible: formValueChanges$.pipe(
                                        distinctUntilChanged(),
                                        map((next: FormValue) => [BazaNcIntegrationFeedPostType.Video].includes(next.type)),
                                    ),
                                    required: form.valueChanges.pipe(
                                        takeUntil(this.nextFormBuilder$),
                                        map((formValue: FormValue) => [BazaNcIntegrationFeedPostType.Video].includes(formValue.type)),
                                    ),
                                },
                                {
                                    type: BazaFormBuilderControlType.BazaHtml,
                                    label: this.i18n('form.fields.contents'),
                                    formControlName: 'contents',
                                    visible: formValueChanges$.pipe(
                                        distinctUntilChanged(),
                                        map((next: FormValue) => [BazaNcIntegrationFeedPostType.Article].includes(next.type)),
                                    ),
                                    props: {
                                        options: {
                                            transport: {
                                                type: BazaCkEditorUploadTransport.BazaAttachment,
                                                options: (file, ext) => {
                                                    if (['png', 'gif', 'jpg', 'jpeg', 'webp'].includes(ext)) {
                                                        return {
                                                            type: AttachmentType.Image,
                                                            payload: {},
                                                        };
                                                    } else {
                                                        return {
                                                            type: AttachmentType.None,
                                                            payload: {
                                                                maxFileSize: 512 /* mb */ * 1024 /* kb */ * 1024 /* b */,
                                                            },
                                                        };
                                                    }
                                                },
                                            },
                                            withCkEditorOptions: {
                                                toolbar: {
                                                    items: [
                                                        'heading',
                                                        '|',
                                                        'bold',
                                                        '|',
                                                        'indent',
                                                        'outdent',
                                                        '|',
                                                        'link',
                                                        '|',
                                                        'imageUpload',
                                                    ],
                                                },
                                                heading: {
                                                    options: [
                                                        { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                                                        { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                                                    ],
                                                },
                                            },
                                        },
                                    },
                                },
                            ],
                        },
                    },
                    {
                        title: this.i18n('form.tabs.seo'),
                        contents: {
                            fields: [
                                {
                                    type: BazaFormBuilderControlType.BazaSeo,
                                    formControlName: 'seo',
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }
}
