import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaNcIntegrationListingsCmsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationListingsCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';

export interface BazaNcIntegrationEliteInvestorCmsResolveData {
    listing: BazaNcIntegrationListingsCmsDto;
}

@Injectable()
export class BazaNgIntegrationEliteInvestorCmsResolve implements Resolve<BazaNcIntegrationEliteInvestorCmsResolveData> {
    constructor(private readonly listingNgEndpoint: BazaNcIntegrationListingsCmsDataAccess) {}

    resolve(route: ActivatedRouteSnapshot): Observable<BazaNcIntegrationEliteInvestorCmsResolveData> {
        const observables = [
            this.listingNgEndpoint.getById({
                id: parseInt(route.params['listingId'], 10),
            }),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([listing]) => ({
                listing,
            })),
        );
    }
}
