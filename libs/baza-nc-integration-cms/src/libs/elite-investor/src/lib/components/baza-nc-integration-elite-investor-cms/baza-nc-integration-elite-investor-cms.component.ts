import { ChangeDetectionStrategy, Component, OnDestroy, ViewContainerRef } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFieldAccountComponentOptions,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BazaFormBuilder, BazaFormBuilderControlType, BazaFormBuilderLayout, BazaFormLayoutService } from '@scaliolabs/baza-core-cms';
import { BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { catchError, takeUntil, tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { AccountDto } from '@scaliolabs/baza-core-shared';
import { BazaAccountSearchModalService } from '@scaliolabs/baza-core-cms';
import { Subject, throwError } from 'rxjs';
import { BazaNcIntegrationEliteInvestorsDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationEliteInvestorsCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNcIntegrationEliteInvestorCmsResolveData } from './baza-nc-integration-elite-investor-cms.resolve';

export const BAZA_NC_INTEGRATION_ELITE_INVESTORS_CMS_ID = 'baza-nc-integration-elite-investors-cms-id';
export const BAZA_NC_INTEGRATION_ELITE_INVESTORS_FORM_ID = 'baza-nc-integration-elite-investors-form-id';

interface FormValue {
    account: AccountDto;
}

interface State {
    config: BazaCrudConfig<BazaNcIntegrationEliteInvestorsDto>;
}

@Component({
    templateUrl: './baza-nc-integration-elite-investor-cms.component.html',
    styleUrls: ['./baza-nc-integration-elite-investor-cms.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationEliteInvestorCmsComponent implements OnDestroy {
    private readonly ngOnDestroy$ = new Subject<void>();

    public crud: BazaCrudComponent<BazaNcIntegrationEliteInvestorsDto>;

    public state: State = {
        config: this.crudConfig(),
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly vcr: ViewContainerRef,
        private readonly endpoint: BazaNcIntegrationEliteInvestorsCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly messages: BazaNgMessagesService,
        private readonly activatedRoute: ActivatedRoute,
        private readonly searchModal: BazaAccountSearchModalService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.eliteInvestors.${key}`;
    }

    get resolvedData(): BazaNcIntegrationEliteInvestorCmsResolveData {
        return this.activatedRoute.snapshot.data['listing'];
    }

    private crudConfig(): BazaCrudConfig<BazaNcIntegrationEliteInvestorsDto> {
        return {
            id: BAZA_NC_INTEGRATION_ELITE_INVESTORS_CMS_ID,
            title: this.i18n('title'),
            titleArguments: {
                listing: this.resolvedData.listing,
            },
            backRouterLink: {
                routerLink: ['/baza-nc-integration/listings'],
            },
            navigation: {
                nodes: [
                    {
                        title: 'bazaNcIntegration.listings.title',
                        routerLink: {
                            routerLink: ['/listings'],
                        },
                    },
                    {
                        title: this.resolvedData.listing.title,
                        translateArgs: this.resolvedData,
                    },
                    {
                        title: this.i18n('navigation'),
                        routerLink: {
                            routerLink: ['/baza-nc-integration/elite-investor', this.resolvedData.listing.id],
                        },
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Search,
                        options: {},
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.syncAllAccounts.title'),
                            isDanger: true,
                            callback: () => {
                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.confirm.open({
                                    message: this.i18n('actions.syncAllAccounts.confirm'),
                                    confirm: () => {
                                        this.endpoint
                                            .syncAllAccounts()
                                            .pipe(takeUntil(this.ngOnDestroy$))
                                            .subscribe(
                                                () =>
                                                    this.messages.success({
                                                        message: this.i18n('actions.syncAllAccounts.success'),
                                                        translate: true,
                                                    }),
                                                () =>
                                                    this.messages.error({
                                                        message: this.i18n('actions.syncAllAccounts.failed'),
                                                        translate: true,
                                                    }),
                                            );
                                    },
                                });
                            },
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () => {
                                const formGroup = this.formGroup();

                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<BazaNcIntegrationEliteInvestorsDto, FormValue>({
                                    formGroup,
                                    formBuilder: this.formBuilder(formGroup),
                                    title: this.i18n('actions.add'),
                                    onSubmit: (entity, formValue) =>
                                        this.endpoint
                                            .include({
                                                accountId: formValue.account.id,
                                                listingId: this.resolvedData.listing.id,
                                            })
                                            .pipe(tap(() => this.crud.refresh())),
                                });
                            },
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.endpoint.list({
                            ...request,
                            listingId: this.resolvedData.listing.id,
                        }),
                },
                actions: [
                    {
                        title: this.i18n('actions.syncAccount.title'),
                        icon: 'sync',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.syncAccount.confirm'),
                                messageArgs: {
                                    fullName: entity.account.fullName,
                                },
                                confirm: () =>
                                    this.endpoint
                                        .syncAccount({
                                            accountId: entity.account.id,
                                        })
                                        .pipe(
                                            tap(() => this.crud.refresh()),
                                            tap(() => {
                                                this.messages.success({
                                                    message: this.i18n('actions.syncAccount.success'),
                                                    translate: true,
                                                    translateParams: {
                                                        fullName: entity.account.fullName,
                                                    },
                                                });
                                            }),
                                            catchError((err) => {
                                                this.messages.error({
                                                    message: this.i18n('actions.syncAccount.failed'),
                                                    translate: true,
                                                    translateParams: {
                                                        fullName: entity.account.fullName,
                                                    },
                                                });

                                                return throwError(err);
                                            }),
                                        )
                                        .toPromise(),
                            }),
                    },
                    {
                        title: this.i18n('actions.exclude.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.exclude.confirm'),
                                confirm: () =>
                                    this.endpoint
                                        .exclude({
                                            accountId: entity.account.id,
                                            listingId: this.resolvedData.listing.id,
                                        })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            }),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: true,
                    },
                    {
                        field: 'dateJoinedAt',
                        title: this.i18n('columns.dateJoinedAt'),
                        textAlign: BazaTableFieldAlign.Center,
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        sortable: true,
                        width: 260,
                    },
                    {
                        field: 'account',
                        title: this.i18n('columns.account'),
                        source: (entity) => `${entity.account.fullName} (${entity.account.id})`,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: true,
                    },
                    {
                        field: 'email',
                        title: this.i18n('columns.email'),
                        source: (entity) => entity.account.email,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 310,
                        sortable: false,
                    },
                ],
            },
        };
    }

    private formGroup(): FormGroup {
        return this.fb.group({
            account: [undefined, [Validators.required]],
        });
    }

    private formBuilder(form: FormGroup): BazaFormBuilder {
        return {
            id: BAZA_NC_INTEGRATION_ELITE_INVESTORS_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.BazaAccount,
                        formControlName: 'account',
                        label: this.i18n('form.fields.account'),
                        placeholder: this.i18n('form.placeholders.account'),
                        required: true,
                        props: {
                            options: {
                                searchModal: (selected) =>
                                    // eslint-disable-next-line security/detect-non-literal-fs-filename
                                    this.searchModal
                                        .open({
                                            vcr: this.vcr,
                                            selected,
                                        })
                                        .then((response) => {
                                            if (response && response.selected) {
                                                form.patchValue({
                                                    account: response.selected,
                                                } as Partial<FormValue>);
                                            }
                                        }),
                            } as BazaFieldAccountComponentOptions,
                        },
                    },
                ],
            },
        };
    }
}
