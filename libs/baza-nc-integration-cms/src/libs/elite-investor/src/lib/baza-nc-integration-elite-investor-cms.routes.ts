import { Routes } from '@angular/router';
import { BazaNcIntegrationEliteInvestorCmsComponent } from './components/baza-nc-integration-elite-investor-cms/baza-nc-integration-elite-investor-cms.component';
import { BazaNgIntegrationEliteInvestorCmsResolve } from './components/baza-nc-integration-elite-investor-cms/baza-nc-integration-elite-investor-cms.resolve';

export const bazaNcIntegrationEliteInvestorCmsRoutes: Routes = [
    {
        path: 'elite-investor/:listingId',
        component: BazaNcIntegrationEliteInvestorCmsComponent,
        resolve: {
            listing: BazaNgIntegrationEliteInvestorCmsResolve,
        },
    },
];
