import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsUiModule } from '@scaliolabs/baza-nc-cms';
import {
    BazaNcIntegrationEliteInvestorsCmsDataAccessModule,
    BazaNcIntegrationListingsCmsDataAccessModule,
} from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNcIntegrationEliteInvestorCmsComponent } from './components/baza-nc-integration-elite-investor-cms/baza-nc-integration-elite-investor-cms.component';
import { BazaNgIntegrationEliteInvestorCmsResolve } from './components/baza-nc-integration-elite-investor-cms/baza-nc-integration-elite-investor-cms.resolve';

@NgModule({
    imports: [
        CommonModule,
        BazaNcCmsUiModule,
        BazaCmsCrudBundleModule,
        BazaNcIntegrationListingsCmsDataAccessModule,
        BazaNcIntegrationEliteInvestorsCmsDataAccessModule,
    ],
    declarations: [BazaNcIntegrationEliteInvestorCmsComponent],
    providers: [BazaNgIntegrationEliteInvestorCmsResolve],
})
export class BazaNcIntegrationEliteInvestorCmsModule {}
