import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationCmsListingTestimonialComponent } from './components/baza-nc-integration-cms-listing-testimonial/baza-nc-integration-cms-listing-testimonial.component';
import { BazaNcIntegrationCmsListingTestimonialResolve } from './components/baza-nc-integration-cms-listing-testimonial/baza-nc-integration-cms-listing-testimonial.resolve';
import { BazaNcIntegrationListingTestimonialCmsDataAccessModule } from '@scaliolabs/baza-nc-integration-cms-data-access';

@NgModule({
    imports: [CommonModule, BazaCmsCrudBundleModule, BazaNcIntegrationListingTestimonialCmsDataAccessModule],
    declarations: [BazaNcIntegrationCmsListingTestimonialComponent],
    providers: [BazaNcIntegrationCmsListingTestimonialResolve],
})
export class BazaNcIntegrationCmsListingTestimonialModule {}
