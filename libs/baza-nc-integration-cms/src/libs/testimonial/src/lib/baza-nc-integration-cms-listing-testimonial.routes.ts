import { Routes } from '@angular/router';
import { BazaNcIntegrationCmsListingTestimonialComponent } from './components/baza-nc-integration-cms-listing-testimonial/baza-nc-integration-cms-listing-testimonial.component';
import { BazaNcIntegrationCmsListingTestimonialResolve } from './components/baza-nc-integration-cms-listing-testimonial/baza-nc-integration-cms-listing-testimonial.resolve';

export const bazaNcIntegrationCmsListingTestimonialRoutes: Routes = [
    {
        path: 'listing/:ulid/testimonials',
        component: BazaNcIntegrationCmsListingTestimonialComponent,
        resolve: {
            data: BazaNcIntegrationCmsListingTestimonialResolve,
        },
    },
];
