import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { BazaNcIntegrationListingsCmsDto } from '@scaliolabs/baza-nc-integration-shared';
import { combineLatest, Observable } from 'rxjs';
import { BazaNcIntegrationListingsCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    listing: BazaNcIntegrationListingsCmsDto;
}

export { ResolveData as BazaNcIntegrationCmsListingTestimonialResolveData };

@Injectable()
export class BazaNcIntegrationCmsListingTestimonialResolve implements Resolve<ResolveData> {
    constructor(private readonly listingDataAccess: BazaNcIntegrationListingsCmsDataAccess) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> {
        const ulid = route.params['ulid'];

        const observables: [Observable<BazaNcIntegrationListingsCmsDto>] = [this.listingDataAccess.getByUlid({ ulid })];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([listing]) => ({
                listing,
            })),
        );
    }
}
