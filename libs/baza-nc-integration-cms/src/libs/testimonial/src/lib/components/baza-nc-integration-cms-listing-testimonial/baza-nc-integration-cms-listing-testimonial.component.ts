import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationAcl, BazaNcIntegrationListingTestimonialCmsDto } from '@scaliolabs/baza-nc-integration-shared';
import { AttachmentDto } from '@scaliolabs/baza-core-shared';
import { Subject, throwError } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { BazaNcIntegrationListingTestimonialCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNcIntegrationCmsListingTestimonialResolveData } from './baza-nc-integration-cms-listing-testimonial.resolve';
import { catchError, takeUntil, tap } from 'rxjs/operators';

export const BAZA_NC_INTEGRATION_LISTINGS_TESTIMONIALS_CMS_ID = 'baza-nc-integration-listings-testimonials-cms-id';
export const BAZA_NC_INTEGRATION_LISTINGS_TESTIMONIALS_FORM_ID = 'baza-nc-integration-listings-testimonials-form-id';

interface State {
    crud: BazaCrudConfig<BazaNcIntegrationListingTestimonialCmsDto>;
    maxSortOrder: number;
}

interface FormValue {
    isPublished: boolean;
    image: AttachmentDto;
    name: string;
    role: string;
    contents: string;
}

@Component({
    templateUrl: './baza-nc-integration-cms-listing-testimonial.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationCmsListingTestimonialComponent implements OnDestroy {
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<BazaNcIntegrationListingTestimonialCmsDto>;

    public state: State = {
        crud: this.config,
        maxSortOrder: 0,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly formLayout: BazaFormLayoutService,
        private readonly confirm: BazaNgConfirmService,
        private readonly messages: BazaNgMessagesService,
        private readonly dataAccess: BazaNcIntegrationListingTestimonialCmsDataAccess,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.testimonial.components.bazaIntegrationListingTestimonialCms.${key}`;
    }

    get resolvedData(): BazaNcIntegrationCmsListingTestimonialResolveData {
        return this.activatedRoute.snapshot.data['data'];
    }

    get config(): BazaCrudConfig<BazaNcIntegrationListingTestimonialCmsDto> {
        return {
            id: BAZA_NC_INTEGRATION_LISTINGS_TESTIMONIALS_CMS_ID,
            title: this.i18n('title'),
            titleArguments: {
                listing: this.resolvedData.listing,
            },
            backRouterLink: {
                routerLink: ['/baza-nc-integration/listings'],
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('breadcrumbs.listings'),
                        routerLink: {
                            routerLink: ['/baza-nc-integration/listings'],
                        },
                    },
                    {
                        title: this.resolvedData.listing.title,
                        translateArgs: this.resolvedData,
                    },
                    {
                        title: this.i18n('breadcrumbs.testimonials'),
                        routerLink: {
                            routerLink: ['/baza-nc-integration/listing', this.resolvedData.listing.sid, 'testimonials'],
                        },
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.create'),
                            callback: () => this.create(),
                        },
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement],
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) =>
                        this.dataAccess
                            .list({
                                ...request,
                                listingUlid: this.resolvedData.listing.sid,
                            })
                            .pipe(tap((response) => (this.state.maxSortOrder = response.maxSortOrder))),
                },
                actions: [
                    {
                        title: this.i18n('actions.update'),
                        icon: 'edit',
                        action: (entity) => this.update(entity),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement],
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'arrow-up',
                        action: (entity) => this.moveUp(entity),
                        visible: (entity) => entity.sortOrder > 1,
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement],
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'arrow-down',
                        action: (entity) => this.moveDown(entity),
                        visible: (entity) => entity.sortOrder < this.state.maxSortOrder,
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement],
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) => this.delete(entity),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingsTestimonialsManagement],
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                    },
                    {
                        field: 'image',
                        title: this.i18n('columns.image'),
                        type: {
                            kind: BazaTableFieldType.Image,
                            nzSrc: (entity: BazaNcIntegrationListingTestimonialCmsDto) => entity.image.presignedUrl,
                            available: (entity: BazaNcIntegrationListingTestimonialCmsDto) => !!entity.image?.presignedUrl,
                        },
                        width: 200,
                    },
                    {
                        field: 'name',
                        title: this.i18n('columns.name'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                    {
                        field: 'role',
                        title: this.i18n('columns.role'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 340,
                    },
                    {
                        field: 'isPublished',
                        title: this.i18n('columns.isPublished'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        width: 120,
                    },
                ],
            },
        };
    }

    formGroup(): FormGroup {
        return this.fb.group({
            isPublished: [false, [Validators.required]],
            image: [undefined, [Validators.required]],
            name: [undefined, [Validators.required]],
            role: [undefined, []],
            contents: [undefined, [Validators.required]],
        });
    }

    formBuilder(): BazaFormBuilder {
        return {
            id: BAZA_NC_INTEGRATION_LISTINGS_TESTIMONIALS_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        formControlName: 'isPublished',
                        type: BazaFormBuilderControlType.Checkbox,
                        label: this.i18n('form.fields.isPublished'),
                    },
                    {
                        type: BazaFormBuilderStaticComponentType.Divider,
                    },
                    {
                        formControlName: 'name',
                        type: BazaFormBuilderControlType.Text,
                        label: this.i18n('form.fields.name'),
                        required: true,
                    },
                    {
                        formControlName: 'role',
                        type: BazaFormBuilderControlType.Text,
                        label: this.i18n('form.fields.role'),
                        required: true,
                    },
                    {
                        formControlName: 'image',
                        type: BazaFormBuilderControlType.BazaImage,
                        label: this.i18n('form.fields.image'),
                        required: true,
                        props: {
                            withImageOptions: {
                                ratio: 1,
                            },
                        },
                    },
                    {
                        formControlName: 'contents',
                        type: BazaFormBuilderControlType.TextArea,
                        label: this.i18n('form.fields.contents'),
                        required: true,
                    },
                ],
            },
        };
    }

    create(): void {
        const formGroup = this.formGroup();

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaNcIntegrationListingTestimonialCmsDto, FormValue>({
            formGroup,
            formBuilder: this.formBuilder(),
            title: this.i18n('actions.create'),
            onSubmit: (entity, formValue) =>
                this.dataAccess
                    .create({
                        ...formValue,
                        listingUlid: this.resolvedData.listing.sid,
                    })
                    .pipe(tap(() => this.crud.refresh())),
        });
    }

    update(entity: BazaNcIntegrationListingTestimonialCmsDto): void {
        const formGroup = this.formGroup();

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayout.open<BazaNcIntegrationListingTestimonialCmsDto, FormValue>({
            formGroup,
            formBuilder: this.formBuilder(),
            title: this.i18n('actions.update'),
            fetch: () =>
                this.dataAccess.getByUlid({
                    ulid: entity.ulid,
                }),
            onSubmit: (entity, formValue) =>
                this.dataAccess
                    .update({
                        ...formValue,
                        ulid: entity.ulid,
                    })
                    .pipe(tap(() => this.crud.refresh())),
        });
    }

    delete(entity: BazaNcIntegrationListingTestimonialCmsDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.confirm.open({
            message: this.i18n('actions.delete.confirm'),
            messageArgs: entity,
            confirm: () =>
                this.dataAccess
                    .delete({
                        ulid: entity.ulid,
                    })
                    .pipe(
                        tap(() =>
                            this.messages.success({
                                translate: true,
                                message: this.i18n('actions.delete.success'),
                                translateParams: entity,
                            }),
                        ),
                        tap(() => this.crud.refresh()),
                        catchError((err) => {
                            this.messages.error(err);

                            return throwError(err);
                        }),
                    )
                    .toPromise(),
        });
    }

    moveUp(entity: BazaNcIntegrationListingTestimonialCmsDto): void {
        this.dataAccess
            .setSortOrder({
                ulid: entity.ulid,
                setSortOrder: entity.sortOrder - 1,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => this.crud.refresh(),
                (err) => this.messages.bazaError(err),
            );
    }

    moveDown(entity: BazaNcIntegrationListingTestimonialCmsDto): void {
        this.dataAccess
            .setSortOrder({
                ulid: entity.ulid,
                setSortOrder: entity.sortOrder + 1,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => this.crud.refresh(),
                (err) => this.messages.bazaError(err),
            );
    }
}
