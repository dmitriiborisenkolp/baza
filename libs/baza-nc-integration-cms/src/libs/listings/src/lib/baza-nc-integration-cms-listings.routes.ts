import { Routes } from '@angular/router';
import { BazaNcIntegrationCmsListingsComponent } from './components/baza-nc-integration-cms-listings/baza-nc-integration-cms-listings.component';
import { BazaNcCmsOfferingFieldResolve } from '@scaliolabs/baza-nc-cms';
import { BazaNcIntegrationCmsListingsResolve } from './components/baza-nc-integration-cms-listings/baza-nc-integration-cms-listings.resolve';
import { BazaNcIntegrationTimelineCmsResolve } from './components/baza-nc-integration-cms-listings-timeline/baza-nc-integration-cms-listings-timeline.resolve';
import { BazaNcIntegrationCmsListingsTimelineComponent } from './components/baza-nc-integration-cms-listings-timeline/baza-nc-integration-cms-listings-timeline.component';

export const bazaNcIntegrationCmsListingsRoutes: Routes = [
    {
        path: 'listings',
        component: BazaNcIntegrationCmsListingsComponent,
        resolve: {
            bazaNcCmsOfferingFieldResolve: BazaNcCmsOfferingFieldResolve,
            bazaNcIntegrationCmsListings: BazaNcIntegrationCmsListingsResolve,
        },
    },
    {
        path: 'listings/:listingId/timeline',
        component: BazaNcIntegrationCmsListingsTimelineComponent,
        resolve: {
            bazaNcIntegrationTimelineCmsResolve: BazaNcIntegrationTimelineCmsResolve,
        },
    },
];
