import { BazaFormControl, registerFormControl } from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationCmsStatementsComponent } from './baza-nc-integration-cms-statements.component';

export enum BazaNcIntegrationCmsStatements {
    BazaNcIntegrationCmsStatements = 'BazaNcIntegrationCmsStatements',
}

export interface BazaNcIntegrationCmsStatementsField extends BazaFormControl<BazaNcIntegrationCmsStatementsComponent, BazaNcIntegrationCmsStatements> {
    type: BazaNcIntegrationCmsStatements.BazaNcIntegrationCmsStatements,
    props?: Partial<BazaNcIntegrationCmsStatementsComponent>;
}

export type BazaNcIntegrationCmsStatementsFields = BazaNcIntegrationCmsStatementsField;

registerFormControl<BazaNcIntegrationCmsStatements>(BazaNcIntegrationCmsStatements.BazaNcIntegrationCmsStatements, {
    enableNzFormContainer: false,
    component: BazaNcIntegrationCmsStatementsComponent,
});
