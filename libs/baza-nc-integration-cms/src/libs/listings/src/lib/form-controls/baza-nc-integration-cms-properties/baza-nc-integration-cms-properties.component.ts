import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    forwardRef,
    Input,
    OnChanges,
    OnDestroy,
    SimpleChanges,
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR, ValidatorFn, Validators } from '@angular/forms';
import {
    BazaNcIntegrationSchemaDefinition,
    BazaNcIntegrationSchemaDto,
    BazaNcIntegrationSchemaType,
} from '@scaliolabs/baza-nc-integration-shared';
import { Observable, Subject } from 'rxjs';
import { BazaNgMessagesService, floatValidator } from '@scaliolabs/baza-core-ng';
import {
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderFormControlComponents,
    BazaFormBuilderLayout,
} from '@scaliolabs/baza-core-cms';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { ListingsFormValue } from '../../models/listings-form-model';

export const BAZA_NC_INTEGRATION_PROPERTIES_FORM_ID = 'baza-nc-integration-properties-form-id';

@Component({
    selector: 'baza-nc-integration-cms-properties',
    templateUrl: './baza-nc-integration-cms-properties.component.html',
    styleUrls: ['./baza-nc-integration-cms-properties.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            multi: true,
            useExisting: forwardRef(() => BazaNcIntegrationCmsPropertiesComponent),
        },
    ],
})
export class BazaNcIntegrationCmsPropertiesComponent implements ControlValueAccessor, OnChanges, OnDestroy {
    @Input() schema$: Observable<BazaNcIntegrationSchemaDto>;
    @Input() parentFormGroup: FormGroup;
    @Input() tabId: string | undefined;

    private readonly nextSchema$: Subject<void> = new Subject<void>();
    private readonly ngOnDestroy$: Subject<void> = new Subject();

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    private currentSchema: BazaNcIntegrationSchemaDto;

    public formGroupFields: FormGroup = this.fb.group({});
    public formConfig: BazaFormBuilder<BazaFormBuilderFormControlComponents>;
    public formFields: Array<BazaFormBuilderFormControlComponents> = [];

    constructor(
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['schema$']) {
            this.nextSchema$.next();

            if (this.schema$) {
                this.schema$.pipe(distinctUntilChanged(), takeUntil(this.nextSchema$)).subscribe((next) => {
                    setTimeout(() => {
                        this.currentSchema = next;

                        this.updateFormGroup(next);
                        this.updateFormFields(next);

                        this.cdr.markForCheck();

                        if (!this.currentSchema) {
                            if (this.onChange) {
                                this.onChange({});
                            }
                        }

                        this.formGroupFields.valueChanges.pipe(takeUntil(this.nextSchema$)).subscribe((next) => {
                            if (this.onChange) {
                                if (this.currentSchema) {
                                    this.formGroupFields.valid ? this.onChange(next) : this.onChange(undefined);
                                } else {
                                    this.onChange({});
                                }
                            }
                        });
                    });
                });
            }
        }
    }

    ngOnDestroy(): void {
        this.nextSchema$.next();
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.listings.components.listings.control.${key}`;
    }

    get isAvailable(): boolean {
        return !!this.currentSchema && !!this.formGroupFields && !!this.formFields;
    }

    get hasFieldsInSchema(): boolean {
        return this.isAvailable && this.formFields.length > 0;
    }

    get parentFormValue(): ListingsFormValue | undefined {
        return this.parentFormGroup ? this.parentFormGroup.value : undefined;
    }

    schemaDefinitions(schema?: BazaNcIntegrationSchemaDto): Array<BazaNcIntegrationSchemaDefinition> {
        return this.tabId === undefined
            ? (schema?.definitions || []).filter((next) => next.payload.tabId === undefined)
            : (schema?.definitions || []).filter((next) => next.payload.tabId === this.tabId);
    }

    updateFormGroup(schema?: BazaNcIntegrationSchemaDto): void {
        const schemaDefinitions = this.schemaDefinitions(schema);

        const currentValue =
            this.parentFormValue && this.parentFormValue.properties ? JSON.parse(JSON.stringify(this.parentFormValue.properties)) : {};

        for (const formControlName of Object.keys(this.formGroupFields.controls)) {
            this.formGroupFields.removeControl(formControlName);
        }

        if (schema) {
            for (const definition of schemaDefinitions || []) {
                const next = this.factoryFormControl(definition);

                if (next) {
                    this.formGroupFields.addControl(definition.payload.field, next);
                }
            }

            this.formGroupFields.patchValue(currentValue);
        }
    }

    updateFormFields(schema?: BazaNcIntegrationSchemaDto): void {
        while (this.formFields.length) {
            this.formFields.pop();
        }

        if (schema) {
            const schemaDefinitions = this.schemaDefinitions(schema);

            for (const definition of schemaDefinitions || []) {
                const next = this.factoryFormField(definition);

                if (next) {
                    this.formFields.push(next);
                }
            }
        }

        this.formConfig = {
            id: BAZA_NC_INTEGRATION_PROPERTIES_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: this.formFields,
            },
        };
    }

    writeValue(obj: any): void {
        if (obj) {
            this.formGroupFields.patchValue(obj);
        } else {
            this.formGroupFields.reset();
        }
    }

    registerOnChange(fn: () => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.formGroupFields.disable() : this.formGroupFields.enable();
    }

    private factoryFormControl(definition: BazaNcIntegrationSchemaDefinition): FormControl {
        const defaultValidators: Array<ValidatorFn> = [];

        if (definition.payload.required) {
            defaultValidators.push(Validators.required);
        }

        switch (definition.type) {
            default: {
                return new FormControl(undefined, defaultValidators);
            }

            case BazaNcIntegrationSchemaType.Email: {
                return new FormControl(undefined, [...defaultValidators, Validators.email]);
            }

            case BazaNcIntegrationSchemaType.Number: {
                const validators = [...defaultValidators, floatValidator];

                if (definition.payload.min) {
                    validators.push(Validators.min(definition.payload.min));
                }

                if (definition.payload.max) {
                    validators.push(Validators.max(definition.payload.max));
                }

                return new FormControl(undefined, validators);
            }

            case BazaNcIntegrationSchemaType.Slider: {
                const validators = [...defaultValidators, floatValidator];

                if (definition.payload.min) {
                    validators.push(Validators.min(definition.payload.min));
                }

                if (definition.payload.max) {
                    validators.push(Validators.max(definition.payload.max));
                }

                return new FormControl(undefined, validators);
            }
        }
    }

    private factoryFormField(definition: BazaNcIntegrationSchemaDefinition): BazaFormBuilderFormControlComponents | void {
        const partial: Pick<BazaFormBuilderFormControlComponents, 'formControlName' | 'required' | 'label'> = {
            formControlName: definition.payload.field,
            required: definition.payload.required,
            label: definition.payload.title,
        };

        switch (definition.type) {
            default: {
                this.ngMessages.error({
                    message: 'Unsupported field',
                });

                return;
            }

            case BazaNcIntegrationSchemaType.Text: {
                return {
                    type: BazaFormBuilderControlType.Text,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.TextArea: {
                return {
                    type: BazaFormBuilderControlType.TextArea,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.HTML: {
                return {
                    type: BazaFormBuilderControlType.BazaHtml,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Email: {
                return {
                    type: BazaFormBuilderControlType.Email,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Select: {
                return {
                    type: BazaFormBuilderControlType.Select,
                    values: Object.entries(definition.payload.values).map(([value, label]) => ({
                        value,
                        label,
                    })),
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Checkbox: {
                return {
                    type: BazaFormBuilderControlType.Checkbox,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Date: {
                return {
                    type: BazaFormBuilderControlType.Date,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.DateRange: {
                return {
                    type: BazaFormBuilderControlType.DateRange,
                    props: {
                        mode: definition.payload.mode,
                        showToday: definition.payload.showToday,
                        showTime: definition.payload.showTime,
                        showNow: definition.payload.showNow,
                    },
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Time: {
                return {
                    type: BazaFormBuilderControlType.Time,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Number: {
                return {
                    type: BazaFormBuilderControlType.Number,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Rate: {
                return {
                    type: BazaFormBuilderControlType.Rate,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Slider: {
                return {
                    type: BazaFormBuilderControlType.Slider,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Switcher: {
                return {
                    type: BazaFormBuilderControlType.Switcher,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.File: {
                return {
                    type: BazaFormBuilderControlType.BazaUploadAttachment,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.FileMulti: {
                return {
                    type: BazaFormBuilderControlType.BazaUploadAttachmentMulti,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.Image: {
                return {
                    type: BazaFormBuilderControlType.BazaImage,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.ImageMulti: {
                return {
                    type: BazaFormBuilderControlType.BazaImageMulti,
                    ...partial,
                };
            }

            case BazaNcIntegrationSchemaType.KeyValue: {
                return {
                    type: BazaFormBuilderControlType.BazaKeyValue,
                    ...partial,
                };
            }
        }
    }
}
