import { BazaFormControl, registerFormControl } from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationCmsPropertiesComponent } from './baza-nc-integration-cms-properties.component';

export enum BazaNcIntegrationCmsProperties {
    BazaNcIntegrationCmsProperties = 'BazaNcIntegrationCmsProperties',
}

export interface BazaNcIntegrationCmsPropertiesField extends BazaFormControl<BazaNcIntegrationCmsPropertiesComponent, BazaNcIntegrationCmsProperties> {
    type: BazaNcIntegrationCmsProperties.BazaNcIntegrationCmsProperties,
    props?: Partial<BazaNcIntegrationCmsPropertiesComponent>;
}

export type BazaNcIntegrationCmsPropertiesFields = BazaNcIntegrationCmsPropertiesField;

registerFormControl<BazaNcIntegrationCmsProperties>(BazaNcIntegrationCmsProperties.BazaNcIntegrationCmsProperties, {
    enableNzFormContainer: false,
    component: BazaNcIntegrationCmsPropertiesComponent,
});
