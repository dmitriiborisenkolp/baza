import { Component, forwardRef, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { ControlValueAccessor, FormArray, FormBuilder, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';
import { BazaFormBuilderContents, BazaFormBuilderControlType } from '@scaliolabs/baza-core-cms';
import { AttachmentDto, AttachmentType } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationListingCmsDocumentsDto } from '@scaliolabs/baza-nc-integration-shared';

interface FormValue {
    documentName: string;
    description: string;
    documentAttachment: Array<AttachmentDto>;
}

interface State {
    form: FormArray;
    formItemConfig: BazaFormBuilderContents;
}

@Component({
    templateUrl: './baza-nc-integration-cms-statements.component.html',
    styleUrls: ['./baza-nc-integration-cms-statements.component.less'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => BazaNcIntegrationCmsStatementsComponent),
            multi: true,
        },
    ],
})
export class BazaNcIntegrationCmsStatementsComponent implements ControlValueAccessor, OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    public state: State = {
        form: this.fb.array([]),
        formItemConfig: {
            fields: [
                {
                    type: BazaFormBuilderControlType.Text,
                    formControlName: 'name',
                    label: this.i18n('fields.name'),
                },
                {
                    type: BazaFormBuilderControlType.TextArea,
                    formControlName: 'description',
                    rows: 5,
                    label: this.i18n('fields.description'),
                },
                {
                    type: BazaFormBuilderControlType.BazaUploadAttachment,
                    formControlName: 'attachment',
                    label: this.i18n('fields.attachment'),
                    props: {
                        withOptions: () => ({
                            type: AttachmentType.None,
                            payload: {
                                acceptMimeTypes: ['application/pdf'],
                            },
                        }),
                    },
                },
            ],
        },
    };

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onChange: Function;

    // eslint-disable-next-line @typescript-eslint/ban-types
    private onTouched: Function;

    constructor(private readonly fb: FormBuilder, public readonly vcr: ViewContainerRef) {}

    ngOnInit(): void {
        this.state.form.valueChanges
            .pipe(
                distinctUntilChanged(),
                filter(() => !!this.onChange),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((next: Array<FormValue>) => {
                if (!next.length) {
                    this.onChange([]);
                } else if (this.state.form.valid) {
                    this.onChange(
                        next.map((item) => ({
                            ...item,
                        })),
                    );
                } else {
                    this.onChange(null);
                }
            });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.listings.components.listings.statements.${key}`;
    }

    addItem(item?: BazaNcIntegrationListingCmsDocumentsDto): void {
        const form = this.fb.group({
            name: ['', [Validators.required]],
            description: ['', [Validators.required]],
            attachment: ['', [Validators.required]],
        });

        if (item) {
            form.patchValue(item);
        }

        this.state.form.push(form);
    }

    removeItem(index: number): void {
        this.state.form.removeAt(index);
    }

    moveItemUp(index: number): void {
        const { controls } = this.state.form;

        [controls[`${index}`], controls[index - 1]] = [controls[index - 1], controls[`${index}`]];

        this.state.form.patchValue(controls);
    }

    moveItemDown(index: number): void {
        const { controls } = this.state.form;

        [controls[`${index}`], controls[index + 1]] = [controls[index + 1], controls[`${index}`]];

        this.state.form.patchValue(controls);
    }

    get canAddItem(): boolean {
        return !this.state.form.length || this.state.form.controls.every((c) => c.valid);
    }

    writeValue(obj: Array<BazaNcIntegrationListingCmsDocumentsDto>): void {
        while (this.state.form.length) {
            this.state.form.removeAt(0);
        }

        if (Array.isArray(obj)) {
            for (const item of obj) {
                this.addItem(item);
            }
        }
    }

    registerOnChange(fn: () => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        isDisabled ? this.state.form.disable() : this.state.form.enable();
    }
}
