import { AttachmentDto } from '@scaliolabs/baza-core-shared';
import { BazaNcUiOfferingDto } from '@scaliolabs/baza-nc-shared';
import { BazaNcIntegrationListingCmsDocumentsDto, BazaNcIntegrationSchemaDto } from '@scaliolabs/baza-nc-integration-shared';

export interface ListingsFormValue {
    isPublished: boolean;
    title: string;
    description: string;
    cover: AttachmentDto;
    images: Array<AttachmentDto>;
    sharesToJoinEliteInvestors: number;
    offering: BazaNcUiOfferingDto;
    metadata: Record<string, string>;
    schemaUpdateId: string;
    schema: BazaNcIntegrationSchemaDto;
    properties: Record<'string', any>;
    statements: Array<BazaNcIntegrationListingCmsDocumentsDto>;
}
