import { CmsSidePanelMenuItemConfig } from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationAcl } from '@scaliolabs/baza-nc-integration-shared';

export const bazaNcIntegrationCmsListingsMenu: Array<CmsSidePanelMenuItemConfig> = [
    {
        title: 'Listings',
        icon: 'right-circle',
        routerLink: ['/baza-nc-integration/listings'],
        requiresACL: [BazaNcIntegrationAcl.BazaNcIntegrationListing],
    },
];
