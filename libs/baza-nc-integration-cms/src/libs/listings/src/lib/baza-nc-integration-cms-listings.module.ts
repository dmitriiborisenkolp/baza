import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { BazaContentTypesTimelineCmsFormModule } from '@scaliolabs/baza-content-types-cms';
import { BazaCmsCrudBundleModule } from '@scaliolabs/baza-core-cms';
import { BazaNcCmsUiModule } from '@scaliolabs/baza-nc-cms';
import {
    BazaNcIntegrationListingsCmsDataAccessModule,
    BazaNcIntegrationSchemaCmsDataAccessModule,
    BazaNcIntegrationSearchCmsDataAccessModule,
} from '@scaliolabs/baza-nc-integration-cms-data-access';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { BazaNcIntegrationCmsListingsTimelineComponent } from './components/baza-nc-integration-cms-listings-timeline/baza-nc-integration-cms-listings-timeline.component';
import { BazaNcIntegrationTimelineCmsResolve } from './components/baza-nc-integration-cms-listings-timeline/baza-nc-integration-cms-listings-timeline.resolve';
import { BazaNcIntegrationCmsListingsComponent } from './components/baza-nc-integration-cms-listings/baza-nc-integration-cms-listings.component';
import { BazaNcIntegrationCmsListingsResolve } from './components/baza-nc-integration-cms-listings/baza-nc-integration-cms-listings.resolve';
import { BazaNcIntegrationCmsPropertiesComponent } from './form-controls/baza-nc-integration-cms-properties/baza-nc-integration-cms-properties.component';
import { BazaNcIntegrationCmsStatementsComponent } from './form-controls/baza-nc-integration-cms-statements/baza-nc-integration-cms-statements.component';

@NgModule({
    imports: [
        CommonModule,
        NzAlertModule,
        TranslateModule,
        BazaNcCmsUiModule,
        BazaCmsCrudBundleModule,
        BazaNcIntegrationListingsCmsDataAccessModule,
        BazaNcIntegrationSchemaCmsDataAccessModule,
        BazaNcIntegrationSearchCmsDataAccessModule,
        BazaContentTypesTimelineCmsFormModule,
        NzButtonModule,
        NzIconModule,
    ],
    declarations: [
        BazaNcIntegrationCmsListingsComponent,
        BazaNcIntegrationCmsPropertiesComponent,
        BazaNcIntegrationCmsListingsTimelineComponent,
        BazaNcIntegrationCmsStatementsComponent,
    ],
    providers: [BazaNcIntegrationCmsListingsResolve, BazaNcIntegrationTimelineCmsResolve],
})
export class BazaNcIntegrationCmsListingsModule {}
