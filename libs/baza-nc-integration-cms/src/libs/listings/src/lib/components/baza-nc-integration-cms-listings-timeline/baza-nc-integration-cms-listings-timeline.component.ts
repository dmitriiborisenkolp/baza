import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { BazaCrudComponent, BazaCrudConfig } from '@scaliolabs/baza-core-cms';
import { TimelineCmsDto } from '@scaliolabs/baza-content-types-shared';
import { BazaContentTypesTimelineCmsService } from '@scaliolabs/baza-content-types-cms';
import { ActivatedRoute } from '@angular/router';
import { BazaNcIntegrationTimelineCmsResolveData } from './baza-nc-integration-cms-listings-timeline.resolve';

interface State {
    crud: BazaCrudConfig<TimelineCmsDto>;
}

@Component({
    templateUrl: './baza-nc-integration-cms-listings-timeline.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationCmsListingsTimelineComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$ = new Subject<void>();

    public crud: BazaCrudComponent<TimelineCmsDto>;

    public state: State = {
        crud: this.timelineCrudService.crudConfig(
            () => this.crud, {
                categoryId: this.resolvedData.listings.categoryId,
                ngOnDestroy$: this.ngOnDestroy$,
            },
        ),
    };

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        private readonly timelineCrudService: BazaContentTypesTimelineCmsService,
    ) {}

    ngOnInit(): void {
        this.state.crud.title = this.i18n('title');
        this.state.crud.titleArguments = this.resolvedData.listings;
        this.state.crud.backRouterLink = {
            routerLink: ['/baza-nc-integration/listings'],
        };
        this.state.crud.navigation = {
            nodes: [
                {
                    title: 'bazaNcIntegration.listings.title',
                    routerLink: {
                        routerLink: ['/baza-nc-integration/listings'],
                    },
                },
                {
                    title: this.resolvedData.listings.title,
                    translateArgs: this.resolvedData,
                },
                {
                    title: this.i18n('navigation'),
                    routerLink: {
                        routerLink: ['/baza-nc-integration/listings', this.resolvedData.listings.id, 'timeline'],
                    },
                },
            ],
        };
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.listings.components.listingsTimeline.${key}`;
    }

    get resolvedData(): BazaNcIntegrationTimelineCmsResolveData {
        return this.activatedRoute.snapshot.data['bazaNcIntegrationTimelineCmsResolve'];
    }
}
