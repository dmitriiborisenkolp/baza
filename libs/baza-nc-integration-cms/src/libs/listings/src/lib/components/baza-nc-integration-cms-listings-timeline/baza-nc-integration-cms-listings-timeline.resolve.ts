import { Injectable } from '@angular/core';
import { BazaNcIntegrationListingsCmsDto } from '@scaliolabs/baza-nc-integration-shared';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { BazaNcIntegrationListingsCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNotFoundService } from '@scaliolabs/baza-core-cms';
import { combineLatest, Observable, of } from 'rxjs';
import { isId } from '@scaliolabs/baza-core-shared';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    listings: BazaNcIntegrationListingsCmsDto;
}

export { ResolveData as BazaNcIntegrationTimelineCmsResolveData };

@Injectable()
export class BazaNcIntegrationTimelineCmsResolve implements Resolve<ResolveData> {
    constructor(private readonly notFound: BazaNotFoundService, private readonly dataAccess: BazaNcIntegrationListingsCmsDataAccess) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> {
        const listingId = parseInt(route.params['listingId'], 10);

        if (!isId(listingId)) {
            this.notFound.navigateToNotFound();

            return of(undefined);
        } else {
            const observables: [Observable<BazaNcIntegrationListingsCmsDto>] = [this.dataAccess.getById({ id: listingId })];

            return combineLatest(observables).pipe(
                retryWhen(genericRetryStrategy()),
                map(([listings]) => ({
                    listings,
                })),
            );
        }
    }
}
