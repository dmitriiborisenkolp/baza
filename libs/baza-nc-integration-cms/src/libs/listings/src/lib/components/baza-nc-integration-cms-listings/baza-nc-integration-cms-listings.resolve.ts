import { Injectable } from '@angular/core';
import { BazaNcIntegrationSchemaDto } from '@scaliolabs/baza-nc-integration-shared';
import { Resolve } from '@angular/router';
import { BazaNcIntegrationSchemaCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { combineLatest, Observable } from 'rxjs';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    schemas: Array<BazaNcIntegrationSchemaDto>;
}

export { ResolveData as BazaNcIntegrationCmsListingsResolveData };

@Injectable()
export class BazaNcIntegrationCmsListingsResolve implements Resolve<ResolveData> {
    constructor(private readonly schemaDataAccess: BazaNcIntegrationSchemaCmsDataAccess) {}

    resolve(): Observable<ResolveData> {
        const observables: [Observable<Array<BazaNcIntegrationSchemaDto>>] = [
            this.schemaDataAccess
                .list({
                    size: -1,
                    index: 1,
                })
                .pipe(
                    retryWhen(genericRetryStrategy()),
                    map((response) => response.items),
                ),
        ];

        return combineLatest(observables).pipe(
            map(([schemas]) => ({
                schemas,
            })),
        );
    }
}
