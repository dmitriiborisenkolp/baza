import { ChangeDetectionStrategy, Component, Inject, OnDestroy, ViewEncapsulation } from '@angular/core';
import { BazaNcOfferingStatus, ncOfferingStatusI18n, NcOfferingUiStatus } from '@scaliolabs/baza-nc-shared';
import {
    BazaCoreFormBuilderComponents,
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudDataColumn,
    BazaCrudItem,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormBuilderTab,
    BazaFormBuilderWithAsyncTabsType,
    BazaFormFieldHint,
    BazaFormLayoutModalService,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BehaviorSubject, of, Subject, throwError } from 'rxjs';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
    BazaLoadingService,
    BazaNgConfirmService,
    BazaNgMessagesService,
    BazaNzButtonStyle,
    intValidator,
    moreThanZeroValidator,
} from '@scaliolabs/baza-core-ng';
import { catchError, distinctUntilChanged, map, takeUntil, tap } from 'rxjs/operators';
import {
    BazaNcIntegrationAcl,
    BazaNcIntegrationListingsCmsDto,
    BazaNcIntegrationListingsConstants,
    BazaNcIntegrationSchemaDto,
} from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcIntegrationListingsCmsDataAccess,
    BazaNcIntegrationSearchCmsDataAccess,
} from '@scaliolabs/baza-nc-integration-cms-data-access';
import {
    bazaNcBundleCmsConfig,
    bazaNcCharsetValidator,
    BazaNcCmsUi,
    BazaNcCmsUiFields,
    bazaNcSchemaValidator,
} from '@scaliolabs/baza-nc-cms';
import { ActivatedRoute } from '@angular/router';
import { BazaNcOfferingCmsDataAccess } from '@scaliolabs/baza-nc-cms-data-access';
import { BazaNcIntegrationCmsListingsResolveData } from './baza-nc-integration-cms-listings.resolve';
import {
    BazaNcIntegrationCmsProperties,
    BazaNcIntegrationCmsPropertiesFields,
} from '../../form-controls/baza-nc-integration-cms-properties/baza-nc-integration-cms-properties';
import { ListingsFormValue } from '../../models/listings-form-model';
import { TranslateService } from '@ngx-translate/core';
import {
    BazaNcIntegrationCmsStatements,
    BazaNcIntegrationCmsStatementsFields,
} from '../../form-controls/baza-nc-integration-cms-statements/baza-nc-integration-cms-statements';
import {
    BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN,
    bazaNcIntegrationBundleCmsConfig,
    BazaNcIntegrationBundlesCmsConfig,
} from '../../../../../../config/baza-nc-integration-bundles-cms.config';

export const BAZA_NC_INTEGRATION_LISTINGS_CMS_ID = 'baza-nc-integration-listings-cms-id';
export const BAZA_NC_INTEGRATION_LISTINGS_FORM_ID = 'baza-nc-integration-listings-form-id';
export const BAZA_NC_INTEGRATION_LISTINGS_CHANGE_STATUS_FORM_ID = 'baza-nc-integration-listings-change-status-form-id';

type FormValue = ListingsFormValue;
type FormBuilderConfigTypes =
    | BazaCoreFormBuilderComponents
    | BazaNcCmsUiFields
    | BazaNcIntegrationCmsPropertiesFields
    | BazaNcIntegrationCmsStatementsFields;
type FormBuilderConfig = BazaFormBuilderWithAsyncTabsType<FormBuilderConfigTypes>;

interface ChangeStatusFormValue {
    newStatus: BazaNcOfferingStatus;
}

interface State {
    config: BazaCrudConfig<BazaNcIntegrationListingsCmsDto>;
    schemas: Array<BazaNcIntegrationSchemaDto>;
    maxSortOrder: number;
    sortMode: 'ASC' | 'DESC';
}

const prepareFormValueToSubmit = (formValue: FormValue) => {
    const preparedFormValue = Object.keys(formValue)
        .filter((name) => !name.startsWith('properties_'))
        .reduce(
            (payload, name) => ({
                ...payload,
                [name]: formValue[`${name}`],
            }),
            {},
        );

    if (preparedFormValue['properties']) {
        preparedFormValue['properties'] = JSON.parse(JSON.stringify(preparedFormValue['properties']));
    }

    return preparedFormValue;
};

@Component({
    templateUrl: './baza-nc-integration-cms-listings.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None,
})
export class BazaNcIntegrationCmsListingsComponent implements OnDestroy {
    private nextForm$: Subject<void> = new Subject<void>();
    private nextSchema$: Subject<void> = new Subject<void>();
    private ngOnDestroy$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<BazaNcIntegrationListingsCmsDto>;

    public state: State = {
        config: this.crudConfig,
        schemas: [],
        maxSortOrder: 0,
        sortMode: 'ASC',
    };

    constructor(
        @Inject(BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN) private readonly bundleConfig: BazaNcIntegrationBundlesCmsConfig,
        private readonly fb: FormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly translate: TranslateService,
        private readonly dataAccess: BazaNcIntegrationListingsCmsDataAccess,
        private readonly ncOfferingDataAccess: BazaNcOfferingCmsDataAccess,
        private readonly searchDataAccess: BazaNcIntegrationSearchCmsDataAccess,
        private readonly formLayout: BazaFormLayoutService,
        private readonly formLayoutModal: BazaFormLayoutModalService,
        private readonly loading: BazaLoadingService,
        private readonly confirm: BazaNgConfirmService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.nextForm$.next();
        this.ngOnDestroy$.next();
    }

    get resolvedData(): BazaNcIntegrationCmsListingsResolveData {
        return this.activatedRoute.snapshot.data['bazaNcIntegrationCmsListings'];
    }

    i18n(key: string): string {
        return `bazaNcIntegration.listings.components.listings.${key}`;
    }

    get crudConfig(): BazaCrudConfig<BazaNcIntegrationListingsCmsDto> {
        const columns: BazaCrudDataColumn<BazaNcIntegrationListingsCmsDto>[] = [
            {
                field: 'id',
                type: {
                    kind: BazaTableFieldType.Id,
                },
                title: this.i18n('columns.id'),
            },
            {
                field: 'title',
                type: {
                    kind: BazaTableFieldType.Text,
                },
                title: this.i18n('columns.title'),
            },
            {
                type: {
                    kind: BazaTableFieldType.Link,
                    link: (entity) => ({
                        routerLink: ['/baza-nc-integration/subscriptions', entity.id],
                    }),
                    translate: true,
                    format: () => this.i18n('columns.subscriptions'),
                },
                title: '',
                sortable: false,
                width: 90,
                textAlign: BazaTableFieldAlign.Center,
            },
            {
                type: {
                    kind: BazaTableFieldType.Link,
                    link: (entity) => ({
                        routerLink: ['/baza-nc-integration/perks', entity.id],
                    }),
                    translate: true,
                    format: () => this.i18n('columns.perks'),
                },
                title: '',
                sortable: false,
                width: 90,
                textAlign: BazaTableFieldAlign.Center,
            },
            {
                type: {
                    kind: BazaTableFieldType.Link,
                    link: (entity) => ({
                        routerLink: ['/baza-nc-integration/elite-investor', entity.id],
                    }),
                    translate: true,
                    format: () => this.i18n('columns.eliteInvestors'),
                },
                title: '',
                sortable: false,
                width: 90,
                textAlign: BazaTableFieldAlign.Center,
                disabled: !bazaNcBundleCmsConfig().withEliteInvestorsFeature,
            },
            {
                type: {
                    kind: BazaTableFieldType.Link,
                    link: (entity) => ({
                        routerLink: ['/baza-nc-integration/listing', entity.sid, 'testimonials'],
                    }),
                    translate: true,
                    format: () => this.i18n('columns.testimonials'),
                },
                title: '',
                sortable: false,
                width: 90,
                textAlign: BazaTableFieldAlign.Center,
                disabled: !this.bundleConfig.withTestimonialsFeature,
            },
            {
                type: {
                    kind: BazaTableFieldType.Link,
                    link: (entity) => ({
                        routerLink: ['/baza-nc-integration/listings', entity.id, 'timeline'],
                    }),
                    translate: true,
                    format: () => this.i18n('columns.timeline'),
                },
                title: '',
                sortable: false,
                width: 110,
                textAlign: BazaTableFieldAlign.Center,
                disabled: !this.bundleConfig.withTimelineFeature,
            },
            {
                field: 'ncOfferingId',
                type: {
                    kind: BazaTableFieldType.Text,
                },
                title: this.i18n('columns.ncOfferingId'),
                width: 160,
                textAlign: BazaTableFieldAlign.Center,
                source: (entity) => entity.offering.ncOfferingId,
            },
            {
                field: 'status',
                title: this.i18n('columns.status'),
                type: {
                    kind: BazaTableFieldType.Text,
                },
                width: 160,
                textAlign: BazaTableFieldAlign.Center,
                source: (entity) => ncOfferingStatusI18n[entity.offering.status],
                ngStyle: (entity) => ({
                    color: (() => {
                        switch (entity.offering.status) {
                            default: {
                                return 'var(--black)';
                            }

                            case BazaNcOfferingStatus.Open: {
                                return 'var(--success-color)';
                            }

                            case BazaNcOfferingStatus.Closed: {
                                return 'var(--error-color)';
                            }

                            case BazaNcOfferingStatus.ComingSoon: {
                                return 'var(--processing-color)';
                            }
                        }
                    })(),
                }),
            },
        ];

        return {
            id: BAZA_NC_INTEGRATION_LISTINGS_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.reindexAll.title'),
                            icon: 'sync',
                            type: BazaNzButtonStyle.Link,
                            callback: () => this.reindexAll(),
                        },
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () => {
                                const formGroup = this.formGroup();

                                this.updateSchemaList([]);

                                // eslint-disable-next-line security/detect-non-literal-fs-filename
                                this.formLayout.open<BazaNcIntegrationListingsCmsDto, FormValue>({
                                    formGroup,
                                    formBuilder: this.formBuilder(formGroup),
                                    title: 'Add Listings',
                                    onSubmit: (entity, unfilteredFormValue) => {
                                        const formValue: FormValue = prepareFormValueToSubmit(unfilteredFormValue) as FormValue;

                                        return this.dataAccess
                                            .create({
                                                ...formValue,
                                                metadata: formValue.metadata || {},
                                            })
                                            .pipe(tap(() => this.crud.refresh()));
                                    },
                                });
                            },
                        },
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingManagement],
                    },
                ],
                bottomLeft: [
                    {
                        type: BazaCrudItem.Legend,
                        options: {
                            items: [
                                {
                                    label: this.i18n('legend.invalid'),
                                    color: 'var(--error-color)',
                                },
                                {
                                    label: this.i18n('legend.inactive'),
                                    color: 'var(--disabled-color)',
                                },
                            ],
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => {
                        const schemas = bazaNcIntegrationBundleCmsConfig().withSchemaFilter;

                        return this.dataAccess
                            .list({
                                ...request,
                                schemas: Array.isArray(schemas) && schemas.length > 0 ? schemas : undefined,
                            })
                            .pipe(
                                tap((response) => {
                                    this.state.sortMode = response.sortMode;
                                    this.state.maxSortOrder = response.maxSortOrder;
                                }),
                            );
                    },
                    invalid: (entity) =>
                        !!entity && !!entity.schema && !this.resolvedData.schemas.find((s) => s.updateId === entity.schema.updateId),
                    inactive: (entity) => !entity.isPublished,
                },
                actions: [
                    {
                        title: this.i18n('actions.changeStatus'),
                        icon: 'sync',
                        action: (entity) => this.changeStatus(entity),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingChangeStatus],
                    },
                    {
                        title: this.i18n('actions.edit'),
                        icon: 'edit',
                        action: (entity) => {
                            const formGroup = this.formGroup(entity);

                            this.updateSchemaList([entity.schema]);

                            const schema = entity.schema
                                ? this.state.schemas.find((s) => s.updateId === entity.schema.updateId)
                                : undefined;

                            formGroup.patchValue({
                                ...entity,
                                schema,
                                schemaUpdateId: schema?.updateId,
                            } as ListingsFormValue);

                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.formLayout.open<BazaNcIntegrationListingsCmsDto, FormValue>({
                                formGroup,
                                formBuilder: this.formBuilder(formGroup, entity),
                                title: entity.title,
                                fetch: () =>
                                    this.dataAccess.getById({
                                        id: entity.id,
                                    }),
                                onSubmit: (fetched, unfilteredFormValue) => {
                                    const formValue: FormValue = prepareFormValueToSubmit(unfilteredFormValue) as FormValue;

                                    return this.dataAccess
                                        .update({
                                            ...formValue,
                                            id: fetched.id,
                                            metadata: formValue.metadata || entity.metadata,
                                        })
                                        .pipe(tap(() => this.crud.refresh()));
                                },
                            });
                        },
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingManagement],
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'arrow-up',
                        action: (entity) => this.moveUp(entity),
                        visible: (entity) => {
                            const canShowUp =
                                (this.state.sortMode === 'ASC' && entity.sortOrder > 1) ||
                                (this.state.sortMode === 'DESC' && entity.sortOrder < this.state.maxSortOrder);

                            return canShowUp;
                        },
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingManagement],
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'arrow-down',
                        action: (entity) => this.moveDown(entity),
                        visible: (entity) => {
                            const canShowDown =
                                (this.state.sortMode === 'ASC' && entity.sortOrder < this.state.maxSortOrder) ||
                                (this.state.sortMode === 'DESC' && entity.sortOrder > 1);

                            return canShowDown;
                        },
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingManagement],
                    },
                    {
                        title: this.i18n('actions.reindex'),
                        icon: 'sync',
                        action: (entity) => this.reindex(entity),
                    },
                    {
                        title: this.i18n('actions.delete.title'),
                        icon: 'delete',
                        action: (entity) =>
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.confirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({
                                            id: entity.id,
                                        })
                                        .pipe(
                                            tap(() =>
                                                this.ngMessages.success({
                                                    translate: true,
                                                    message: this.i18n('actions.delete.success'),
                                                    translateParams: entity,
                                                }),
                                            ),
                                            tap(() => this.crud.refresh()),
                                            catchError((err) => {
                                                this.ngMessages.bazaError(err);

                                                return throwError(err);
                                            }),
                                        )
                                        .toPromise(),
                            }),
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationListingManagement],
                    },
                ],
                columns,
            },
        };
    }

    private updateSchemaList(additionalSchemas: Array<BazaNcIntegrationSchemaDto>): void {
        const schemas: Array<BazaNcIntegrationSchemaDto> = [];

        if (additionalSchemas) {
            for (const schema of additionalSchemas) {
                if (schema) {
                    const existing = this.resolvedData.schemas.find((s) => s.updateId === schema.updateId);

                    if (!existing) {
                        const cloned: BazaNcIntegrationSchemaDto = JSON.parse(JSON.stringify(schema));

                        cloned.title = `${cloned.title} *`;

                        schemas.push(cloned);
                    }
                }
            }
        }

        this.state = {
            ...this.state,
            schemas: [...schemas, ...this.resolvedData.schemas],
        };
    }

    private formGroup(entity?: BazaNcIntegrationListingsCmsDto): FormGroup {
        this.nextForm$.next();

        const form = this.fb.group({
            isPublished: [false],
            title: [undefined, [Validators.required, bazaNcCharsetValidator()]],
            description: [undefined, [Validators.required, bazaNcCharsetValidator()]],
            cover: [undefined, [Validators.required]],
            images: [undefined, [Validators.required]],
            offering: [undefined, [Validators.required]],
            metadata: [{}],
            schema: [null],
            schemaUpdateId: [undefined, [Validators.required]],
            properties: [{}],
            statements: [
                [],
                [
                    (control: AbstractControl) => {
                        if (Array.isArray(control.value)) {
                            return null;
                        } else {
                            return {
                                required: true,
                            };
                        }
                    },
                ],
            ],
        });

        if (bazaNcBundleCmsConfig().withEliteInvestorsFeature) {
            form.addControl(
                'sharesToJoinEliteInvestors',
                new FormControl(undefined, [Validators.required, intValidator, moreThanZeroValidator]),
            );
        }

        form.get('schemaUpdateId')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextForm$))
            .subscribe((updateId: string) => {
                this.nextSchema$.next();

                const schema = this.state.schemas.find((s) => s.updateId === updateId);

                form.get('properties').setValue(schema?.id === entity?.schema?.id ? entity?.properties : {});

                const properties = form.get('properties').value || {};

                form.get('schema').setValue(schema);

                Object.keys(form.controls)
                    .filter((name) => name.startsWith('properties_'))
                    .forEach((name) => form.removeControl(name));

                if (schema) {
                    const definitionsInProperties = schema.definitions.filter((next) => next.payload.tabId === undefined);

                    if (definitionsInProperties.length > 0) {
                        const defaultPropertiesValue = schema.definitions
                            .filter((next) => next.payload.tabId === undefined)
                            .reduce(
                                (formValue, next) => ({
                                    ...formValue,
                                    [next.payload.field]: properties[next.payload.field],
                                }),
                                {},
                            );

                        form.addControl(
                            'properties_default',
                            new FormControl(defaultPropertiesValue, bazaNcSchemaValidator(schema.definitions)),
                        );

                        form.get('properties_default')
                            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextSchema$))
                            .subscribe((next) => {
                                form.get('properties').setValue({
                                    ...form.get('properties').value,
                                    ...next,
                                });
                            });
                    }

                    if (Array.isArray(schema.tabs) && schema.tabs.length > 0) {
                        for (const tab of schema.tabs) {
                            const schemaTabDefinitions = schema.definitions.filter((next) => next.payload.tabId === tab.id);

                            if (schemaTabDefinitions.length > 0) {
                                const controlName = `properties_${tab.id}`;

                                const tabPropertiesValue = schemaTabDefinitions.reduce(
                                    (formValue, next) => ({
                                        ...formValue,
                                        [next.payload.field]: properties[next.payload.field],
                                    }),
                                    {},
                                );

                                form.addControl(
                                    controlName,
                                    new FormControl(tabPropertiesValue, bazaNcSchemaValidator(schema.definitions)),
                                );

                                form.get(controlName)
                                    .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextSchema$))
                                    .subscribe((next) => {
                                        form.get('properties').setValue({
                                            ...form.get('properties').value,
                                            ...next,
                                        });
                                    });
                            }
                        }
                    }
                }
            });

        return form;
    }

    private formBuilder(formGroup: FormGroup, entity?: BazaNcIntegrationListingsCmsDto): FormBuilderConfig {
        const schema$ = new BehaviorSubject<BazaNcIntegrationSchemaDto | undefined>(entity?.schema);

        formGroup
            .get('schema')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.nextForm$))
            .subscribe((next) => schema$.next(next));

        return {
            id: BAZA_NC_INTEGRATION_LISTINGS_FORM_ID,
            layout: BazaFormBuilderLayout.WithAsyncTabs,
            contents: schema$.pipe(
                distinctUntilChanged(),
                map((schema) => {
                    const isSchemaUpdated =
                        !!entity && !!entity.schema && !this.resolvedData.schemas.find((s) => s.updateId === entity.schema.updateId);

                    const schemaHint: BazaFormFieldHint = isSchemaUpdated
                        ? {
                              text: this.i18n('form.hints.schemaUpdatedHint'),
                              translate: true,
                          }
                        : undefined;

                    const tabsOfferingFields: (BazaCoreFormBuilderComponents | BazaNcCmsUiFields)[] = [
                        {
                            formControlName: 'sharesToJoinEliteInvestors',
                            type: BazaFormBuilderControlType.Number,
                            label: this.i18n('form.fields.sharesToJoinEliteInvestors'),
                            visible: bazaNcBundleCmsConfig().withEliteInvestorsFeature,
                        },
                        {
                            type: BazaFormBuilderStaticComponentType.Divider,
                            visible: bazaNcBundleCmsConfig().withEliteInvestorsFeature,
                        },
                        {
                            type: BazaNcCmsUi.BazaNcOfferingField,
                            formControlName: 'offering',
                            props: {
                                resolvedData: this.activatedRoute.snapshot.data['bazaNcCmsOfferingFieldResolve'],
                                resolveOffering: (request) =>
                                    this.ncOfferingDataAccess.ncOfferingDetails(request).pipe(
                                        map((details) => ({
                                            details,
                                            status: NcOfferingUiStatus.Available,
                                        })),
                                        catchError(() => {
                                            return of({
                                                status: NcOfferingUiStatus.NotFound,
                                            });
                                        }),
                                    ),
                            },
                        },
                    ];

                    const schemaTabs: Array<BazaFormBuilderTab<FormBuilderConfigTypes>> = [];

                    if (schema) {
                        const definitionsInProperties = schema.definitions.filter((next) => next.payload.tabId === undefined);

                        if (definitionsInProperties.length > 0) {
                            schemaTabs.push({
                                title: this.i18n('tabs.properties'),
                                contents: {
                                    fields: [
                                        {
                                            type: BazaNcIntegrationCmsProperties.BazaNcIntegrationCmsProperties,
                                            formControlName: 'properties_default',
                                            props: {
                                                schema$: schema$,
                                                parentFormGroup: formGroup,
                                                tabId: undefined,
                                            },
                                        },
                                    ],
                                },
                            });
                        }

                        if (Array.isArray(schema.tabs) && schema.tabs.length > 0) {
                            for (const tab of schema.tabs) {
                                const definitionsInTab = schema.definitions.filter((next) => next.payload.tabId === tab.id);

                                if (definitionsInTab.length > 0) {
                                    schemaTabs.push({
                                        title: tab.title,
                                        withoutTranslate: true,
                                        contents: {
                                            fields: [
                                                {
                                                    type: BazaNcIntegrationCmsProperties.BazaNcIntegrationCmsProperties,
                                                    formControlName: `properties_${tab.id}`,
                                                    props: {
                                                        schema$: schema$,
                                                        parentFormGroup: formGroup,
                                                        tabId: tab.id,
                                                    },
                                                },
                                            ],
                                        },
                                    });
                                }
                            }
                        }
                    }

                    const tabs: Array<BazaFormBuilderTab<FormBuilderConfigTypes>> = [
                        {
                            title: this.i18n('tabs.general'),
                            contents: {
                                fields: [
                                    {
                                        type: BazaFormBuilderControlType.Checkbox,
                                        formControlName: 'isPublished',
                                        label: this.i18n('form.fields.isPublished'),
                                    },
                                    {
                                        type: BazaFormBuilderStaticComponentType.Divider,
                                    },
                                    {
                                        type: BazaFormBuilderControlType.Select,
                                        formControlName: 'schemaUpdateId',
                                        label: this.i18n('form.fields.schema'),
                                        values: [
                                            {
                                                label: this.i18n('form.values.noSchema'),
                                                translate: true,
                                                value: null,
                                            },
                                            ...this.state.schemas.map((schema) => ({
                                                value: schema.updateId,
                                                label: schema.title,
                                            })),
                                        ],
                                        props: {
                                            nzAllowClear: true,
                                            // TODO: We  can't preselect NULL or Undefined option. We should wait for fix
                                            // TODO: from ng-zorro team: https://github.com/NG-ZORRO/ng-zorro-antd/issues/5950
                                            nzPlaceHolder: this.translate.instant(this.i18n('form.values.noSchema')),
                                        },
                                        hint: schemaHint,
                                        visible: this.bundleConfig.withSchemaCMS,
                                    },
                                    {
                                        formControlName: 'title',
                                        type: BazaFormBuilderControlType.Text,
                                        label: this.i18n('form.fields.title'),
                                    },
                                    {
                                        formControlName: 'description',
                                        type: BazaFormBuilderControlType.BazaHtml,
                                        label: this.i18n('form.fields.description'),
                                    },
                                    {
                                        formControlName: 'metadata',
                                        type: BazaFormBuilderControlType.BazaKeyValue,
                                        label: this.i18n('form.fields.metadata'),
                                        visible: this.bundleConfig.withListingMetadataFieldFeature,
                                    },
                                ],
                            },
                        },
                        {
                            title: this.i18n('tabs.offering'),
                            contents: {
                                fields: tabsOfferingFields,
                            },
                        },
                        {
                            title: this.i18n('tabs.images'),
                            contents: {
                                fields: [
                                    {
                                        type: BazaFormBuilderControlType.BazaImage,
                                        formControlName: 'cover',
                                        label: this.i18n('form.fields.cover'),
                                        props: {
                                            withImageOptions: {
                                                ratio: BazaNcIntegrationListingsConstants.LISTINGS_COVER_RATIO,
                                            },
                                        },
                                    },
                                    {
                                        type: BazaFormBuilderStaticComponentType.Divider,
                                    },
                                    {
                                        type: BazaFormBuilderControlType.BazaImageMulti,
                                        formControlName: 'images',
                                        label: this.i18n('form.fields.images'),
                                        props: {
                                            withImageOptions: {
                                                ratio: BazaNcIntegrationListingsConstants.LISTINGS_IMAGES_RATIO,
                                            },
                                        },
                                    },
                                ],
                            },
                        },
                    ];

                    if (this.bundleConfig.withStatementsFeature) {
                        tabs.push({
                            title: this.i18n('tabs.statements'),
                            contents: {
                                fields: [
                                    {
                                        type: BazaNcIntegrationCmsStatements.BazaNcIntegrationCmsStatements,
                                        formControlName: 'statements',
                                    },
                                ],
                            },
                        });
                    }

                    if (schemaTabs.length > 0) {
                        tabs.push(...schemaTabs);
                    }

                    return {
                        tabs,
                    };
                }),
            ),
        };
    }

    private changeStatus(entity: BazaNcIntegrationListingsCmsDto): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formLayoutModal.open<BazaNcIntegrationListingsCmsDto, ChangeStatusFormValue, void>({
            title: 'Change Status',
            formGroup: this.fb.group({
                newStatus: [entity.offering.status, [Validators.required]],
            }),
            formBuilder: {
                id: BAZA_NC_INTEGRATION_LISTINGS_CHANGE_STATUS_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            label: 'New Status',
                            formControlName: 'newStatus',
                            type: BazaFormBuilderControlType.Select,
                            values: Object.values(BazaNcOfferingStatus).map((value) => ({
                                label: ncOfferingStatusI18n[`${value}`],
                                value,
                            })),
                        },
                    ],
                },
            },
            onSubmit: (e, formValue) =>
                this.dataAccess
                    .changeStatus({
                        id: entity.id,
                        newStatus: formValue.newStatus,
                    })
                    .pipe(tap(() => this.crud.refresh())),
        });
    }

    moveUp(entity: BazaNcIntegrationListingsCmsDto): void {
        const loading = this.loading.addLoading();

        const setSortOrder = this.state.sortMode === 'ASC' ? entity.sortOrder - 1 : entity.sortOrder + 1;

        this.dataAccess
            .setSortOrder({
                id: entity.id,
                setSortOrder,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => {
                    this.crud.refresh();

                    loading.complete();
                },
                (err) => {
                    this.ngMessages.bazaError(err);

                    loading.fail();
                },
            );
    }

    moveDown(entity: BazaNcIntegrationListingsCmsDto): void {
        const loading = this.loading.addLoading();

        const setSortOrder = this.state.sortMode === 'ASC' ? entity.sortOrder + 1 : entity.sortOrder - 1;

        this.dataAccess
            .setSortOrder({
                id: entity.id,
                setSortOrder,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => {
                    this.crud.refresh();

                    loading.complete();
                },
                (err) => {
                    this.ngMessages.bazaError(err);

                    loading.fail();
                },
            );
    }

    reindex(entity: BazaNcIntegrationListingsCmsDto): void {
        const loading = this.loading.addLoading();

        this.searchDataAccess.reindex(entity.sid).subscribe(
            () => {
                this.crud.refresh();

                loading.complete();
            },
            (err) => {
                this.ngMessages.bazaError(err);

                loading.fail();
            },
        );
    }

    reindexAll(): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.confirm.open({
            message: this.i18n('actions.reindexAll.confirm'),
            confirm: () => {
                this.searchDataAccess
                    .reindexAll()
                    .pipe(takeUntil(this.ngOnDestroy$))
                    .subscribe(
                        () => {
                            this.ngMessages.success({
                                message: this.i18n('actions.reindexAll.success'),
                                translate: true,
                            });
                        },
                        (err) => this.ngMessages.bazaError(err),
                    );
            },
        });
    }
}
