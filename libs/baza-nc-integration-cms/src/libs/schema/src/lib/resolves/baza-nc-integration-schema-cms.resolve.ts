import { Injectable } from '@angular/core';
import { BazaNcIntegrationSchemaDto } from '@scaliolabs/baza-nc-integration-shared';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { BazaNcIntegrationSchemaCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { combineLatest, Observable } from 'rxjs';
import { isId } from '@scaliolabs/baza-core-shared';
import { BazaNotFoundService } from '@scaliolabs/baza-core-cms';
import { map, retryWhen } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';

interface ResolveData {
    entity: BazaNcIntegrationSchemaDto;
    tabId?: string;
}

export { ResolveData as BazaNcIntegrationSchemaCmsResolveData };

@Injectable()
export class BazaNcIntegrationSchemaCmsResolve implements Resolve<ResolveData> {
    constructor(
        private readonly router: Router,
        private readonly notFound: BazaNotFoundService,
        private readonly dataAccess: BazaNcIntegrationSchemaCmsDataAccess,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<ResolveData> | ResolveData {
        const id = parseInt(route.params['id'], 10);
        const tabId = route.params['tabId'];

        if (isId(id)) {
            const observables: [Observable<BazaNcIntegrationSchemaDto>] = [
                this.dataAccess.getById({
                    id,
                }),
            ];

            return combineLatest(observables).pipe(
                retryWhen(genericRetryStrategy()),
                map(([entity]) => ({
                    entity,
                    tabId,
                })),
            );
        } else {
            this.notFound.navigateToNotFound();

            return {
                entity: undefined,
                tabId: undefined,
            };
        }
    }
}
