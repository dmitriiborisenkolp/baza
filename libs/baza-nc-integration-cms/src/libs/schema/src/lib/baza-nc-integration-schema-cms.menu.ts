import { CmsSidePanelMenuItemConfig } from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationAcl } from '@scaliolabs/baza-nc-integration-shared';

export const bazaNcIntegrationSchemaCmsMenu: Array<CmsSidePanelMenuItemConfig> = [
    {
        title: 'bazaNcIntegration.schema.title',
        translate: true,
        icon: 'apartment',
        routerLink: ['/baza-nc-integration/schema'],
        requiresACL: [
            BazaNcIntegrationAcl.BazaNcIntegrationSchemaManagement,
        ],
    },
];
