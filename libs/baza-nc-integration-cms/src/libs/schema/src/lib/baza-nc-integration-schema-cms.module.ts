import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaCmsLayoutModule, BazaCrudCmsModule, BazaFormBuilderModule } from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationSchemaCmsDataAccessModule } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNcIntegrationSchemaCmsComponent } from './components/baza-nc-integration-schema-cms/baza-nc-integration-schema-cms.component';
import { BazaNcIntegrationSchemaEditorCmsComponent } from './components/baza-nc-integration-schema-editor-cms/baza-nc-integration-schema-editor-cms.component';
import { BazaNcIntegrationSchemaCmsResolve } from './resolves/baza-nc-integration-schema-cms.resolve';
import { BazaNcIntegrationSchemaEditorTabCmsComponent } from './components/baza-nc-integration-schema-editor-tab-cms/baza-nc-integration-schema-editor-tab-cms.component';
import { BazaNcIntegrationSchemaCmsGuard } from './baza-nc-integration-schema-cms.guard';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BazaFormBuilderModule,
        BazaCrudCmsModule,
        BazaCmsLayoutModule,
        BazaNcIntegrationSchemaCmsDataAccessModule,
    ],
    declarations: [
        BazaNcIntegrationSchemaCmsComponent,
        BazaNcIntegrationSchemaEditorCmsComponent,
        BazaNcIntegrationSchemaEditorTabCmsComponent,
    ],
    providers: [BazaNcIntegrationSchemaCmsResolve, BazaNcIntegrationSchemaCmsGuard],
})
export class BazaNcIntegrationSchemaCmsModule {}
