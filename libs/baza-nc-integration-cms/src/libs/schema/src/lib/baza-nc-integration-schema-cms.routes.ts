import { Routes } from '@angular/router';
import { BazaNcIntegrationSchemaCmsComponent } from './components/baza-nc-integration-schema-cms/baza-nc-integration-schema-cms.component';
import { BazaNcIntegrationSchemaEditorCmsComponent } from './components/baza-nc-integration-schema-editor-cms/baza-nc-integration-schema-editor-cms.component';
import { BazaNcIntegrationSchemaCmsResolve } from './resolves/baza-nc-integration-schema-cms.resolve';
import { BazaNcIntegrationSchemaCmsGuard } from './baza-nc-integration-schema-cms.guard';
import { BazaNcIntegrationSchemaEditorTabCmsComponent } from './components/baza-nc-integration-schema-editor-tab-cms/baza-nc-integration-schema-editor-tab-cms.component';

export const bazaNcIntegrationSchemaCmsRoutes: Routes = [
    {
        path: 'schema',
        component: BazaNcIntegrationSchemaCmsComponent,
        canActivate: [BazaNcIntegrationSchemaCmsGuard],
    },
    {
        path: 'schema/:id',
        component: BazaNcIntegrationSchemaEditorTabCmsComponent,
        canActivate: [BazaNcIntegrationSchemaCmsGuard],
        resolve: {
            schema: BazaNcIntegrationSchemaCmsResolve,
        },
    },
    {
        path: 'schema/:id/tabs/default',
        component: BazaNcIntegrationSchemaEditorCmsComponent,
        canActivate: [BazaNcIntegrationSchemaCmsGuard],
        resolve: {
            schema: BazaNcIntegrationSchemaCmsResolve,
        },
    },
    {
        path: 'schema/:id/tabs/custom/:tabId',
        component: BazaNcIntegrationSchemaEditorCmsComponent,
        canActivate: [BazaNcIntegrationSchemaCmsGuard],
        resolve: {
            schema: BazaNcIntegrationSchemaCmsResolve,
        },
    },
];
