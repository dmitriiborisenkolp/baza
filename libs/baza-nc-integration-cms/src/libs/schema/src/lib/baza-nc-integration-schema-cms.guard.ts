import { Inject, Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { BazaNotFoundService } from '@scaliolabs/baza-core-cms';
import {
    BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN,
    BazaNcIntegrationBundlesCmsConfig,
} from '../../../../config/baza-nc-integration-bundles-cms.config';

@Injectable()
export class BazaNcIntegrationSchemaCmsGuard implements CanActivate {
    constructor(
        private readonly notFound: BazaNotFoundService,
        @Inject(BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN) private readonly bundleConfig: BazaNcIntegrationBundlesCmsConfig,
    ) {}

    canActivate(): boolean | UrlTree {
        return this.bundleConfig.withSchemaCMS ? true : this.notFound.urlTreeToNotFound();
    }
}
