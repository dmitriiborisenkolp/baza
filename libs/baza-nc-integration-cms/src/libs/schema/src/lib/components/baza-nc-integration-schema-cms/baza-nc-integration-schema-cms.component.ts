import { ChangeDetectionStrategy, Component, Inject, OnDestroy } from '@angular/core';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormBuilderStaticComponentType,
    BazaFormLayoutModalService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaNcIntegrationAcl, BazaNcIntegrationListingsCmsDto, BazaNcIntegrationSchemaDto } from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSchemaCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { Router } from '@angular/router';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { of, Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
    BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN,
    BazaNcIntegrationBundlesCmsConfig,
} from '../../../../../../config/baza-nc-integration-bundles-cms.config';

export const BAZA_NC_INTEGRATION_SCHEMA_CMS_ID = 'baza-nc-integration-schema-cms-id';
export const BAZA_NC_INTEGRATION_SCHEMA_FORM_ID = 'baza-nc-integration-schema-form-id';
export const BAZA_NC_INTEGRATION_SCHEMA_RENAME_FORM_ID = 'baza-nc-integration-schema-rename-form-id';

interface State {
    config: BazaCrudConfig<BazaNcIntegrationSchemaDto>;
    maxSortOrder: number;
}

interface CreateSchemaFormValue {
    title: string;
    displayName?: string;
    published: boolean;
}

interface EditTitleFormValue {
    title: string;
    displayName?: string;
    published: boolean;
}

@Component({
    templateUrl: './baza-nc-integration-schema-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationSchemaCmsComponent implements OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();
    private readonly nextFormGroup$: Subject<void> = new Subject<void>();

    public crud: BazaCrudComponent<BazaNcIntegrationSchemaDto>;

    public state: State = {
        config: this.crudConfig,
        maxSortOrder: 0,
    };

    constructor(
        @Inject(BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN) private readonly bundleConfig: BazaNcIntegrationBundlesCmsConfig,
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly dataAccess: BazaNcIntegrationSchemaCmsDataAccess,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly formModal: BazaFormLayoutModalService,
        private readonly loading: BazaLoadingService,
        private readonly ngMessages: BazaNgMessagesService,
    ) {}

    ngOnDestroy(): void {
        this.nextFormGroup$.next();
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.schema.components.bazaNcIntegrationSchemaCms.${key}`;
    }

    get crudConfig(): BazaCrudConfig<BazaNcIntegrationSchemaDto> {
        return {
            id: BAZA_NC_INTEGRATION_SCHEMA_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () => this.create(),
                        },
                        visible: of(this.bundleConfig.withEditableSchemaCMS),
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request).pipe(tap((next) => (this.state.maxSortOrder = next.maxSortOrder))),
                    inactive: (row) => !row.published,
                },
                actions: [
                    {
                        icon: 'edit',
                        title: this.i18n('actions.edit'),
                        // eslint-disable-next-line security/detect-non-literal-fs-filename
                        action: (entity) => this.edit(entity),
                        visible: (entity) => of(this.bundleConfig.withEditableSchemaCMS && !entity.locked),
                    },
                    {
                        title: this.i18n('actions.moveUp'),
                        icon: 'arrow-up',
                        action: (entity) => this.moveUp(entity),
                        visible: (entity) => entity.sortOrder > 1,
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationSchemaManagement],
                    },
                    {
                        title: this.i18n('actions.moveDown'),
                        icon: 'arrow-down',
                        action: (entity) => this.moveDown(entity),
                        visible: (entity) => entity.sortOrder < this.state.maxSortOrder,
                        acl: [BazaNcIntegrationAcl.BazaNcIntegrationSchemaManagement],
                    },
                    {
                        icon: 'delete',
                        title: this.i18n('actions.delete.title'),
                        action: (entity) => {
                            // eslint-disable-next-line security/detect-non-literal-fs-filename
                            this.ngConfirm.open({
                                message: this.i18n('actions.delete.confirm'),
                                messageArgs: entity,
                                confirm: () =>
                                    this.dataAccess
                                        .delete({ id: entity.id })
                                        .pipe(tap(() => this.crud.refresh()))
                                        .toPromise(),
                            });
                        },
                        visible: (entity) => of(this.bundleConfig.withEditableSchemaCMS && !entity.locked),
                    },
                ],
                columns: [
                    {
                        field: 'id',
                        title: this.i18n('columns.id'),
                        type: {
                            kind: BazaTableFieldType.Id,
                        },
                        sortable: false,
                    },
                    {
                        field: 'title',
                        title: this.i18n('columns.title'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                        width: 320,
                    },
                    {
                        field: 'displayName',
                        title: this.i18n('columns.displayName'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        sortable: false,
                    },
                    {
                        field: 'definitions',
                        title: '',
                        type: {
                            kind: BazaTableFieldType.Link,
                            format: () => this.i18n('actions.definitions'),
                            translate: true,
                            link: (entity) => ({
                                routerLink: ['/baza-nc-integration/schema', entity.id],
                            }),
                        },
                        width: 120,
                        sortable: false,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                ],
            },
        };
    }

    formGroupFactory(entity?: BazaNcIntegrationSchemaDto): FormGroup {
        const formGroup = this.fb.group({
            title: ['', [Validators.required]],
            published: [false],
            displayName: ['', [Validators.required]],
        });

        const fieldPublished = formGroup.get('published');
        const fieldDisplayName = formGroup.get('displayName');

        fieldPublished.valueChanges.subscribe((next) => (next ? fieldDisplayName.enable() : fieldDisplayName.disable()));

        if (entity) {
            formGroup.patchValue(entity);

            setTimeout(
                () =>
                    fieldPublished.updateValueAndValidity({
                        emitEvent: true,
                    }),
                200,
            );
        }

        return formGroup;
    }

    formBuilderFactory(formGroup: FormGroup): BazaFormBuilder {
        return {
            id: BAZA_NC_INTEGRATION_SCHEMA_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'title',
                        label: this.i18n('forms.create.fields.title'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderStaticComponentType.Divider,
                    },
                    {
                        type: BazaFormBuilderControlType.Checkbox,
                        formControlName: 'published',
                        label: this.i18n('forms.create.fields.published.label'),
                        required: true,
                    },
                    {
                        type: BazaFormBuilderStaticComponentType.Hint,
                        props: {
                            text: this.i18n('forms.create.fields.published.hint'),
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        formControlName: 'displayName',
                        label: this.i18n('forms.create.fields.displayName'),
                        required: true,
                        visible: formGroup.get('published').valueChanges,
                    },
                ],
            },
        };
    }

    create(): void {
        const formGroup = this.formGroupFactory();
        const formBuilder = this.formBuilderFactory(formGroup);

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<CreateSchemaFormValue, CreateSchemaFormValue, BazaNcIntegrationSchemaDto>({
            formGroup,
            formBuilder,
            title: this.i18n('forms.create.title'),
            width: 570,
            onSubmit: (entity, formValue) => {
                return this.dataAccess
                    .create({
                        title: formValue.title,
                        published: formValue.published,
                        displayName: formValue.displayName,
                        definitions: [],
                    })
                    .pipe(
                        tap((response) => this.router.navigate(['/baza-nc-integration/schema', response.id])),
                        takeUntil(this.ngOnDestroy$),
                    );
            },
        });
    }

    edit(entity: BazaNcIntegrationSchemaDto): void {
        const formGroup = this.formGroupFactory(entity);
        const formBuilder = this.formBuilderFactory(formGroup);

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        const formModal = this.formModal.open<BazaNcIntegrationSchemaDto, EditTitleFormValue, BazaNcIntegrationSchemaDto>({
            formGroup,
            formBuilder,
            title: this.i18n('forms.edit.title'),
            width: 570,
            onSubmit: (input, formValue) => {
                return this.dataAccess
                    .update({
                        id: entity.id,
                        title: formValue.title,
                        published: formValue.published,
                        displayName: formValue.displayName,
                        definitions: entity.definitions,
                    })
                    .pipe(tap(() => this.crud.refresh()));
            },
        });
    }

    moveUp(entity: BazaNcIntegrationSchemaDto): void {
        const loading = this.loading.addLoading();

        const setSortOrder = Math.max(0, entity.sortOrder - 1);

        this.dataAccess
            .setSortOrder({
                id: entity.id,
                setSortOrder,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => {
                    this.crud.refresh();

                    loading.complete();
                },
                (err) => {
                    this.ngMessages.bazaError(err);

                    loading.fail();
                },
            );
    }

    moveDown(entity: BazaNcIntegrationSchemaDto): void {
        const loading = this.loading.addLoading();

        const setSortOrder = Math.max(0, entity.sortOrder + 1);

        this.dataAccess
            .setSortOrder({
                id: entity.id,
                setSortOrder,
            })
            .pipe(takeUntil(this.ngOnDestroy$))
            .subscribe(
                () => {
                    this.crud.refresh();

                    loading.complete();
                },
                (err) => {
                    this.ngMessages.bazaError(err);

                    loading.fail();
                },
            );
    }
}
