import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ComponentRef, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BazaNcIntegrationSchemaCmsResolveData } from '../../resolves/baza-nc-integration-schema-cms.resolve';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilder,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormControlFieldSelect,
    BazaFormLayoutModalService,
    BazaFormLayoutService,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    bazaNcIntegrationSchemaConfig,
    BazaNcIntegrationSchemaDefinition,
    BazaNcIntegrationSchemaDto,
    BazaNcIntegrationSchemaType,
    SchemaDefinitionPayload,
} from '@scaliolabs/baza-nc-integration-shared';
import { BazaNcIntegrationSchemaCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BehaviorSubject, of, skip, Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
    BazaNgConfirmService,
    BazaNgMessagesService,
    BazaNzButtonStyle,
    floatValidator,
    intValidator,
    keywordValidator,
    moreThanZeroValidator,
} from '@scaliolabs/baza-core-ng';
import { TranslateService } from '@ngx-translate/core';
import { NzSelectComponent } from 'ng-zorro-antd/select';
import {
    BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN,
    BazaNcIntegrationBundlesCmsConfig,
} from '../../../../../../config/baza-nc-integration-bundles-cms.config';
import { bazaNcSchemaDuplicatedFieldValidator } from '@scaliolabs/baza-nc-cms';

export const BAZA_NC_INTEGRATION_SCHEMA_EDITOR_CMS_ID = 'baza-nc-integration-schema-editor-cms-id';
export const BAZA_NC_INTEGRATION_SCHEMA_EDITOR_EDIT_SCHEMA_TITLE_FORM_ID = 'baza-nc-integration-schema-editor-edit-schema-title-form-id';
export const BAZA_NC_INTEGRATION_SCHEMA_EDITOR_ADD_DEFINITION_FORM_ID = 'baza-nc-integration-schema-editor-add-definition-form-id';
export const BAZA_NC_INTEGRATION_SCHEMA_EDITOR_DEFINITION_FORM_BUILDER_FORM_ID =
    'baza-nc-integration-schema-editor-definition-form-builder-form-id';

const SCHEMA_TYPE_VALUES = bazaNcIntegrationSchemaConfig()
    .allowedDefinitionTypes.sort((a, b) => a.localeCompare(b))
    .map((value) => ({
        label: `bazaNcIntegration.schema.__types.BazaNcIntegrationSchemaType.${value}`,
        translate: true,
        value,
    }));

interface State {
    config: BazaCrudConfig<BazaNcIntegrationSchemaDefinition>;
}

interface EditTitleFormValue {
    title: string;
}

interface AddDefinitionFormValue {
    type: BazaNcIntegrationSchemaType;
}

interface DefinitionFormValue {
    updateId: string;
    title: string;
    field: string;
    type: BazaNcIntegrationSchemaType;
    required: boolean;
    isDisplayedInDetails: boolean;
}

@Component({
    templateUrl: './baza-nc-integration-schema-editor-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationSchemaEditorCmsComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    private entity$: BehaviorSubject<BazaNcIntegrationSchemaDto> = new BehaviorSubject<BazaNcIntegrationSchemaDto>(
        this.resolvedData.entity,
    );

    public crud: BazaCrudComponent<BazaNcIntegrationSchemaDefinition>;

    public state: State = {
        config: this.config,
    };

    constructor(
        @Inject(BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN) private readonly bundleConfig: BazaNcIntegrationBundlesCmsConfig,
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly activatedRoute: ActivatedRoute,
        private readonly dataAccess: BazaNcIntegrationSchemaCmsDataAccess,
        private readonly formModal: BazaFormLayoutModalService,
        private readonly formSide: BazaFormLayoutService,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly translate: TranslateService,
    ) {}

    ngOnInit(): void {
        this.entity$.pipe(skip(1), distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            this.dataAccess.update(next).subscribe(() => {
                this.state.config = {
                    ...this.state.config,
                    titleArguments: next,
                };

                this.crud.refresh();

                this.cdr.markForCheck();
            });
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.schema.components.bazaNcIntegrationSchemaEditorCms.${key}`;
    }

    i18nType(type: BazaNcIntegrationSchemaType): string {
        return `bazaNcIntegration.schema.__types.BazaNcIntegrationSchemaType.${type}`;
    }

    get resolvedData(): BazaNcIntegrationSchemaCmsResolveData {
        return this.activatedRoute.snapshot.data['schema'];
    }

    get entity(): BazaNcIntegrationSchemaDto {
        return this.entity$.getValue();
    }

    get config(): BazaCrudConfig<BazaNcIntegrationSchemaDefinition> {
        return {
            id: BAZA_NC_INTEGRATION_SCHEMA_EDITOR_CMS_ID,
            title: this.i18n('title'),
            titleArguments: this.resolvedData.entity,
            backRouterLink: {
                routerLink: ['/baza-nc-integration/schema'],
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('breadcrumbs.schema'),
                        routerLink: {
                            routerLink: ['/baza-nc-integration/schema'],
                        },
                    },
                    {
                        title: this.entity.title,
                        withoutTranslation: true,
                        routerLink: {
                            routerLink: ['/baza-nc-integration/schema', this.resolvedData.entity.id],
                        },
                    },
                    {
                        title: this.resolvedData.tabId
                            ? this.resolvedData.entity.tabs.find((next) => next.id === this.resolvedData.tabId)?.title || '–'
                            : this.i18n('defaultTab'),
                        withoutTranslation: !!this.resolvedData.tabId,
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.editTitle'),
                            callback: () => this.editSchemaTitle(),
                            type: BazaNzButtonStyle.Link,
                        },
                        visible: of(this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked),
                    },
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.addDefinition'),
                            callback: () => this.addDefinition(),
                            type: BazaNzButtonStyle.Primary,
                        },
                        visible: of(this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked),
                    },
                ],
            },
            data: {
                dataSource: {
                    array: () =>
                        of({
                            items: this.entity$
                                .getValue()
                                .definitions.filter((next) =>
                                    this.resolvedData.tabId
                                        ? next.payload.tabId === this.resolvedData.tabId
                                        : next.payload.tabId === undefined,
                                ),
                        }),
                },
                actions: [
                    {
                        icon: 'edit',
                        title: this.i18n('actions.edit'),
                        action: (definition) => this.editDefinition(definition),
                        visible: () => this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked,
                    },
                    {
                        icon: 'up',
                        title: this.i18n('actions.moveUp'),
                        action: (definition) => setTimeout(() => this.moveUp(definition)),
                        visible: (definition) =>
                            this.canMoveUp(definition) && this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked,
                    },
                    {
                        icon: 'down',
                        title: this.i18n('actions.moveDown'),
                        action: (definition) => setTimeout(() => this.moveDown(definition)),
                        visible: (definition) =>
                            this.canMoveDown(definition) && this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked,
                    },
                    {
                        icon: 'delete',
                        title: this.i18n('actions.delete.title'),
                        action: (definition) => setTimeout(() => this.deleteDefinition(definition)),
                        visible: () => this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked,
                    },
                ],
                columns: [
                    {
                        field: 'title',
                        title: this.i18n('columns.title'),
                        source: (entity) => entity.payload.title,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                    {
                        field: 'type',
                        title: this.i18n('columns.type'),
                        source: (entity) => this.translate.instant(this.i18nType(entity.type)),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 180,
                    },
                    {
                        field: 'required',
                        title: this.i18n('columns.isDisplayedInDetails'),
                        source: (entity) => entity.payload.isDisplayedInDetails,
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        width: 165,
                    },
                    {
                        field: 'required',
                        title: this.i18n('columns.required'),
                        source: (entity) => entity.payload.required,
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        width: 80,
                    },
                ],
            },
        };
    }

    editSchemaTitle(): void {
        const formGroup = this.fb.group({
            title: [this.entity.title, [Validators.required]],
        });

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<EditTitleFormValue, EditTitleFormValue, void>({
            formGroup,
            title: this.i18n('forms.editTitle.title'),
            width: 570,
            formBuilder: {
                id: BAZA_NC_INTEGRATION_SCHEMA_EDITOR_EDIT_SCHEMA_TITLE_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'title',
                            label: this.i18n('forms.editTitle.fields.title'),
                            required: true,
                        },
                    ],
                },
            },
            onSubmit: (entity, formValue) => {
                if (this.entity.title.trim() !== formValue.title.trim()) {
                    this.entity$.next({
                        ...this.entity,
                        title: formValue.title.trim(),
                    });
                }

                return of(undefined);
            },
        });
    }

    addDefinition(): void {
        const formGroup = this.fb.group({
            type: [undefined, [Validators.required]],
        });

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<AddDefinitionFormValue, AddDefinitionFormValue, void>({
            formGroup,
            title: this.i18n('forms.addDefinition.title'),
            width: 570,
            formBuilder: {
                id: BAZA_NC_INTEGRATION_SCHEMA_EDITOR_ADD_DEFINITION_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Select,
                            formControlName: 'type',
                            label: this.i18n('forms.addDefinition.fields.type'),
                            required: true,
                            values: SCHEMA_TYPE_VALUES,
                            props: {
                                nzFilterOption: () => true,
                                nzShowSearch: true,
                                nzServerSearch: true,
                            },
                            afterViewInit: (componentRef: ComponentRef<NzSelectComponent>, portal) => {
                                componentRef.instance.nzOnSearch.pipe().subscribe((next) => {
                                    const definition = portal.definition as BazaFormControlFieldSelect;

                                    definition.values = SCHEMA_TYPE_VALUES.filter((option) =>
                                        this.translate.instant(option.label).toLowerCase().includes(next.toLowerCase()),
                                    );

                                    portal.updateProps();
                                });
                            },
                        },
                    ],
                },
            },
            onSubmit: (entity, formValue) => {
                const formGroup = this.factoryDefinitionForm(formValue.type);

                if (!formGroup) {
                    return of(undefined);
                }

                // eslint-disable-next-line security/detect-non-literal-fs-filename
                this.formSide.open<BazaNcIntegrationSchemaDefinition, DefinitionFormValue, any>({
                    formGroup,
                    title: this.i18n('forms.definition.title'),
                    formBuilder: this.factoryDefinitionFormBuilder(formValue.type),
                    onSubmit: (e, definitionFormValue) => {
                        const newDefinition: BazaNcIntegrationSchemaDefinition = {
                            type: formValue.type,
                            payload: {
                                ...definitionFormValue,
                                tabId: this.resolvedData.tabId,
                            } as any,
                        };

                        this.entity$.next({
                            ...this.entity,
                            definitions: [...this.entity.definitions, newDefinition],
                        });
                    },
                });

                return of(undefined);
            },
        });
    }

    editDefinition(defininition: BazaNcIntegrationSchemaDefinition): void {
        const formGroup = this.factoryDefinitionForm(defininition.type, defininition.payload);

        if (!formGroup) {
            return;
        }

        formGroup.patchValue(defininition.payload);

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formSide.open<BazaNcIntegrationSchemaDefinition, DefinitionFormValue, any>({
            formGroup,
            title: this.i18n('forms.definition.title'),
            formBuilder: this.factoryDefinitionFormBuilder(defininition.type),
            onSubmit: (e, definitionFormValue) => {
                const newDefinition: BazaNcIntegrationSchemaDefinition = {
                    type: defininition.type,
                    payload: {
                        ...definitionFormValue,
                        tabId: this.resolvedData.tabId,
                    } as any,
                };

                this.entity$.next({
                    ...this.entity,
                    definitions: this.entity.definitions.map((def) => (def === defininition ? newDefinition : def)),
                });
            },
        });
    }

    deleteDefinition(definition: BazaNcIntegrationSchemaDefinition): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.delete.confirm'),
            messageArgs: {
                title: definition.payload.title,
            },
            confirm: () =>
                this.entity$.next({
                    ...this.entity,
                    definitions: this.entity.definitions.filter((def) => def !== definition),
                }),
        });
    }

    get definitionsOfResolvedTab(): Array<BazaNcIntegrationSchemaDefinition> {
        return this.entity.definitions.filter((next) =>
            this.resolvedData.tabId === undefined ? next.payload.tabId === undefined : next.payload.tabId === this.resolvedData.tabId,
        );
    }

    canMoveUp(definition: BazaNcIntegrationSchemaDefinition): boolean {
        return this.definitionsOfResolvedTab.indexOf(definition) > 0;
    }

    canMoveDown(definition: BazaNcIntegrationSchemaDefinition): boolean {
        return this.definitionsOfResolvedTab.indexOf(definition) < this.definitionsOfResolvedTab.length - 1;
    }

    moveUp(definition: BazaNcIntegrationSchemaDefinition): void {
        const definitions = [...this.entity.definitions];

        const index = definitions.indexOf(definition);
        const swapIndex = this.definitionsOfResolvedTab.indexOf(definition);

        if (index < 1) {
            return;
        }

        const swapElement = this.definitionsOfResolvedTab[swapIndex - 1];

        if (!swapElement) {
            return;
        }

        definitions[definitions.indexOf(swapElement)] = definition;
        definitions[`${index}`] = swapElement;

        this.entity$.next({
            ...this.entity,
            definitions,
        });
    }

    moveDown(definition: BazaNcIntegrationSchemaDefinition): void {
        const definitions = [...this.entity.definitions];

        const index = definitions.indexOf(definition);
        const swapIndex = this.definitionsOfResolvedTab.indexOf(definition);

        if (index >= definitions.length - 1) {
            return;
        }

        const swapElement = this.definitionsOfResolvedTab[swapIndex + 1];

        if (!swapElement) {
            return;
        }

        definitions[definitions.indexOf(swapElement)] = definition;
        definitions[`${index}`] = swapElement;

        this.entity$.next({
            ...this.entity,
            definitions,
        });
    }

    private factoryDefinitionForm(type?: BazaNcIntegrationSchemaType, payload?: SchemaDefinitionPayload): FormGroup | undefined {
        const isDisplayedInDetails = payload
            ? payload.isDisplayedInDetails
            : bazaNcIntegrationSchemaConfig().allowedSchemaFieldTypesForDetails.includes(type);

        const base = this.fb.group({
            field: [
                undefined,
                [
                    Validators.required,
                    keywordValidator,
                    bazaNcSchemaDuplicatedFieldValidator({
                        tabId: this.resolvedData.tabId,
                        definitions: this.entity.definitions,
                    }),
                ],
            ],
            title: [undefined, [Validators.required]],
            type: [this.translate.instant(this.i18nType(type)), [Validators.required]],
            required: [false, [Validators.required]],
            isDisplayedInDetails: [isDisplayedInDetails],
        });

        switch (type) {
            case BazaNcIntegrationSchemaType.Number: {
                base.addControl('min', new FormControl('', [floatValidator]));
                base.addControl('max', new FormControl('', [floatValidator]));

                break;
            }

            case BazaNcIntegrationSchemaType.Rate: {
                base.addControl('count', new FormControl(5, [Validators.required, intValidator, moreThanZeroValidator]));

                break;
            }

            case BazaNcIntegrationSchemaType.Slider: {
                base.addControl('min', new FormControl('', [Validators.required, moreThanZeroValidator, floatValidator]));
                base.addControl('max', new FormControl('', [Validators.required, moreThanZeroValidator, floatValidator]));
                base.addControl('step', new FormControl('', [Validators.required, moreThanZeroValidator, floatValidator]));

                break;
            }

            case BazaNcIntegrationSchemaType.Select: {
                base.addControl('values', new FormControl({}, [Validators.required]));

                break;
            }

            case BazaNcIntegrationSchemaType.DateRange: {
                base.addControl('mode', new FormControl('date', [Validators.required]));
                base.addControl('showToday', new FormControl(false));
                base.addControl('showTime', new FormControl(false));
                base.addControl('showNow', new FormControl(false));

                break;
            }
        }

        return base;
    }

    private factoryDefinitionFormBuilder(type: BazaNcIntegrationSchemaType): BazaFormBuilder {
        const isRequiredFlagAllowed = bazaNcIntegrationSchemaConfig().allowedRequiredFlagForTypes.includes(type);
        const isDisplayedInDetails = bazaNcIntegrationSchemaConfig().allowedSchemaFieldTypesForDetails.includes(type);

        const base: BazaFormBuilder = {
            id: BAZA_NC_INTEGRATION_SCHEMA_EDITOR_DEFINITION_FORM_BUILDER_FORM_ID,
            layout: BazaFormBuilderLayout.FormOnly,
            contents: {
                fields: [
                    {
                        type: BazaFormBuilderControlType.Text,
                        label: this.i18n('forms.definition.fields.type'),
                        formControlName: 'type',
                        readonly: true,
                        disabled: true,
                        hint: {
                            text: this.i18n('forms.definition.hints.type'),
                            translate: true,
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        label: this.i18n('forms.definition.fields.title'),
                        formControlName: 'title',
                        hint: {
                            text: this.i18n('forms.definition.hints.title'),
                            translate: true,
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Text,
                        label: this.i18n('forms.definition.fields.field'),
                        formControlName: 'field',
                        hint: {
                            text: this.i18n('forms.definition.hints.field'),
                            translate: true,
                        },
                    },
                ],
            },
        };

        if (isRequiredFlagAllowed) {
            base.contents.fields.push({
                type: BazaFormBuilderControlType.Switcher,
                label: this.i18n('forms.definition.fields.required'),
                formControlName: 'required',
            });
        }

        if (isDisplayedInDetails) {
            base.contents.fields.push({
                type: BazaFormBuilderControlType.Switcher,
                label: this.i18n('forms.definition.fields.isDisplayedInDetails'),
                formControlName: 'isDisplayedInDetails',
            });
        }

        switch (type) {
            case BazaNcIntegrationSchemaType.Number: {
                base.contents.fields.push(
                    {
                        type: BazaFormBuilderControlType.Number,
                        label: this.i18n('forms.definition.fields.min'),
                        formControlName: 'min',
                    },
                    {
                        type: BazaFormBuilderControlType.Number,
                        label: this.i18n('forms.definition.fields.max'),
                        formControlName: 'max',
                    },
                );

                break;
            }

            case BazaNcIntegrationSchemaType.Slider: {
                base.contents.fields.push(
                    {
                        type: BazaFormBuilderControlType.Number,
                        label: this.i18n('forms.definition.fields.min'),
                        formControlName: 'min',
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Number,
                        label: this.i18n('forms.definition.fields.max'),
                        formControlName: 'max',
                        required: true,
                    },
                    {
                        type: BazaFormBuilderControlType.Number,
                        label: this.i18n('forms.definition.fields.step'),
                        formControlName: 'step',
                        required: true,
                    },
                );

                break;
            }

            case BazaNcIntegrationSchemaType.Rate: {
                base.contents.fields.push({
                    type: BazaFormBuilderControlType.Number,
                    label: this.i18n('forms.definition.fields.stars'),
                    formControlName: 'count',
                    props: {
                        nzMin: 1,
                        nzStep: 1,
                    },
                });

                break;
            }

            case BazaNcIntegrationSchemaType.Select: {
                base.contents.fields.push({
                    type: BazaFormBuilderControlType.BazaKeyValue,
                    label: this.i18n('forms.definition.fields.values'),
                    formControlName: 'values',
                });

                break;
            }

            case BazaNcIntegrationSchemaType.DateRange: {
                base.contents.fields.push(
                    {
                        type: BazaFormBuilderControlType.Select,
                        label: this.i18n('forms.definition.fields.mode'),
                        formControlName: 'mode',
                        required: false,
                        values: ['date', 'week', 'month', 'year'].map((value) => ({ value, label: value })),
                        hint: {
                            text: this.i18n('forms.definition.hints.mode'),
                            translate: true,
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Switcher,
                        label: this.i18n('forms.definition.fields.showToday'),
                        formControlName: 'showToday',
                        required: false,
                        hint: {
                            text: this.i18n('forms.definition.hints.showToday'),
                            translate: true,
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Switcher,
                        label: this.i18n('forms.definition.fields.showTime'),
                        formControlName: 'showTime',
                        required: false,
                        hint: {
                            text: this.i18n('forms.definition.hints.showTime'),
                            translate: true,
                        },
                    },
                    {
                        type: BazaFormBuilderControlType.Switcher,
                        label: this.i18n('forms.definition.fields.showNow'),
                        formControlName: 'showNow',
                        required: false,
                        hint: {
                            text: this.i18n('forms.definition.hints.showNow'),
                            translate: true,
                        },
                    },
                );

                break;
            }
        }

        return base;
    }
}
