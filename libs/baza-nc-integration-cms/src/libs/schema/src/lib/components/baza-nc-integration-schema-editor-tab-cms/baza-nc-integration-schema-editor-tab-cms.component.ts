import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, of, skip, Subject } from 'rxjs';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormBuilderControlType,
    BazaFormBuilderLayout,
    BazaFormLayoutModalService,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import {
    BazaNcIntegrationSchemaDefinition,
    BazaNcIntegrationSchemaDto,
    BazaNcIntegrationSchemaTab,
} from '@scaliolabs/baza-nc-integration-shared';
import {
    BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN,
    BazaNcIntegrationBundlesCmsConfig,
} from '../../../../../../config/baza-nc-integration-bundles-cms.config';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BazaNcIntegrationSchemaCmsDataAccess } from '@scaliolabs/baza-nc-integration-cms-data-access';
import { BazaNgConfirmService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { BazaNcIntegrationSchemaCmsResolveData } from '../../resolves/baza-nc-integration-schema-cms.resolve';
import { ulid } from 'ulid';
import { TranslateService } from '@ngx-translate/core';
import { bazaNcSchemaDuplicatedTabValidator } from '@scaliolabs/baza-nc-cms';

export const BAZA_NC_INTEGRATION_SCHEMA_TAB_CMS_ID = 'baza-nc-integration-schema-tab-cms-id';
export const BAZA_NC_INTEGRATION_SCHEMA_TAB_FORM_ID = 'baza-nc-integration-schema-tab-form-id';

const MAX_TAB_TITLE_LENGTH = 16;

interface SchemaTabFormValue {
    title: string;
}

interface State {
    config: BazaCrudConfig<BazaNcIntegrationSchemaTab>;
}

@Component({
    templateUrl: './baza-nc-integration-schema-editor-tab-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaNcIntegrationSchemaEditorTabCmsComponent implements OnInit, OnDestroy {
    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    private entity$: BehaviorSubject<BazaNcIntegrationSchemaDto> = new BehaviorSubject<BazaNcIntegrationSchemaDto>(
        this.resolvedData.entity,
    );

    public crud: BazaCrudComponent<BazaNcIntegrationSchemaDefinition>;

    public state: State = {
        config: this.config,
    };

    constructor(
        @Inject(BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN) private readonly bundleConfig: BazaNcIntegrationBundlesCmsConfig,
        private readonly fb: FormBuilder,
        private readonly cdr: ChangeDetectorRef,
        private readonly activatedRoute: ActivatedRoute,
        private readonly dataAccess: BazaNcIntegrationSchemaCmsDataAccess,
        private readonly formModal: BazaFormLayoutModalService,
        private readonly formSide: BazaFormLayoutService,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly translate: TranslateService,
    ) {}

    ngOnInit(): void {
        this.entity$.pipe(skip(1), distinctUntilChanged(), takeUntil(this.ngOnDestroy$)).subscribe((next) => {
            this.dataAccess.update(next).subscribe(() => {
                this.crud.refresh();

                this.cdr.markForCheck();
            });
        });
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `bazaNcIntegration.schema.components.bazaNcIntegrationSchemaEditorTabCms.${key}`;
    }

    get resolvedData(): BazaNcIntegrationSchemaCmsResolveData {
        return this.activatedRoute.snapshot.data['schema'];
    }

    get entity(): BazaNcIntegrationSchemaDto {
        return this.entity$.getValue();
    }

    get config(): BazaCrudConfig<BazaNcIntegrationSchemaTab> {
        return {
            id: BAZA_NC_INTEGRATION_SCHEMA_TAB_CMS_ID,
            title: this.i18n('title'),
            titleArguments: this.resolvedData.entity,
            backRouterLink: {
                routerLink: ['/baza-nc-integration/schema'],
            },
            navigation: {
                nodes: [
                    {
                        title: this.i18n('breadcrumbs.schema'),
                        routerLink: {
                            routerLink: ['/baza-nc-integration/schema'],
                        },
                    },
                    {
                        title: this.entity.title,
                        withoutTranslation: true,
                    },
                ],
            },
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.add'),
                            callback: () => this.addTab(),
                            type: BazaNzButtonStyle.Primary,
                        },
                        visible: of(this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked),
                    },
                ],
            },
            data: {
                dataSource: {
                    array: () =>
                        of({
                            items: [
                                {
                                    id: undefined,
                                    title: this.translate.instant(this.i18n('defaultTab')),
                                },
                                ...this.entity$.getValue().tabs,
                            ],
                        }),
                },
                actions: [
                    {
                        icon: 'edit',
                        title: this.i18n('actions.edit'),
                        action: (tab) => this.edit(tab),
                        visible: (tab) => !!tab.id && this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked,
                    },
                    {
                        icon: 'up',
                        title: this.i18n('actions.moveUp'),
                        action: (tab) => setTimeout(() => this.moveUp(tab)),
                        visible: (tab) =>
                            !!tab.id && this.canMoveUp(tab) && this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked,
                    },
                    {
                        icon: 'down',
                        title: this.i18n('actions.moveDown'),
                        action: (tab) => setTimeout(() => this.moveDown(tab)),
                        visible: (tab) =>
                            !!tab.id &&
                            this.canMoveDown(tab) &&
                            this.bundleConfig.withEditableSchemaCMS &&
                            !this.resolvedData.entity.locked,
                    },
                    {
                        icon: 'delete',
                        title: this.i18n('actions.delete.title'),
                        action: (tab) => setTimeout(() => this.delete(tab)),
                        visible: (tab) => !!tab.id && this.bundleConfig.withEditableSchemaCMS && !this.resolvedData.entity.locked,
                    },
                ],
                columns: [
                    {
                        field: 'title',
                        title: this.i18n('columns.title'),
                        source: (entity) => entity.title,
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        ngStyle: (tab) => ({
                            'font-variant': tab.id === undefined ? 'italic' : 'normal',
                        }),
                        width: 320,
                    },
                    {
                        field: 'definitions',
                        title: this.i18n('columns.definitions'),
                        source: (tab) =>
                            this.entity.definitions
                                .filter((next) => (tab.id ? next.payload.tabId === tab.id : next.payload.tabId === undefined))
                                .map((next) => next.payload.title)
                                .join(', ') || '–',
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                    },
                    {
                        field: 'definitions',
                        title: '',
                        type: {
                            kind: BazaTableFieldType.Link,
                            format: () => this.i18n('actions.edit'),
                            translate: true,
                            link: (tab) => ({
                                routerLink:
                                    tab.id === undefined
                                        ? ['/baza-nc-integration/schema/', this.entity.id, 'tabs', 'default']
                                        : ['/baza-nc-integration/schema/', this.entity.id, 'tabs', 'custom', tab.id],
                            }),
                        },
                        width: 120,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                ],
            },
        };
    }

    addTab(): void {
        const formGroup = this.fb.group({
            title: [
                '',
                [
                    Validators.required,
                    Validators.minLength(1),
                    Validators.maxLength(MAX_TAB_TITLE_LENGTH),
                    bazaNcSchemaDuplicatedTabValidator({ tabs: this.entity.tabs }),
                ],
            ],
        });

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<SchemaTabFormValue, SchemaTabFormValue, BazaNcIntegrationSchemaDto>({
            formGroup,
            title: this.i18n('forms.title.add'),
            width: 570,
            formBuilder: {
                id: BAZA_NC_INTEGRATION_SCHEMA_TAB_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'title',
                            label: this.i18n('forms.fields.title'),
                            required: true,
                        },
                    ],
                },
            },
            onSubmit: (entity, formValue) => {
                this.entity$.next({
                    ...this.entity,
                    tabs: [
                        ...this.entity.tabs,
                        {
                            id: ulid(),
                            title: formValue.title,
                        },
                    ],
                });

                return of(undefined);
            },
        });
    }

    edit(tab: BazaNcIntegrationSchemaTab): void {
        const formGroup = this.fb.group({
            title: [
                tab.title,
                [
                    Validators.required,
                    Validators.minLength(1),
                    Validators.maxLength(MAX_TAB_TITLE_LENGTH),
                    bazaNcSchemaDuplicatedTabValidator({ tabs: this.entity.tabs }),
                ],
            ],
        });

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.formModal.open<SchemaTabFormValue, SchemaTabFormValue, BazaNcIntegrationSchemaDto>({
            formGroup,
            title: this.i18n('forms.title.edit'),
            width: 570,
            formBuilder: {
                id: BAZA_NC_INTEGRATION_SCHEMA_TAB_FORM_ID,
                layout: BazaFormBuilderLayout.FormOnly,
                contents: {
                    fields: [
                        {
                            type: BazaFormBuilderControlType.Text,
                            formControlName: 'title',
                            label: this.i18n('forms.fields.title'),
                            required: true,
                        },
                    ],
                },
            },
            onSubmit: (entity, formValue) => {
                this.entity$.next({
                    ...this.entity,
                    tabs: this.entity.tabs.map((next) => {
                        return next.id === tab.id ? { id: next.id, title: formValue.title } : next;
                    }),
                });

                return of(undefined);
            },
        });
    }

    delete(tab: BazaNcIntegrationSchemaTab): void {
        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.delete.confirm'),
            messageArgs: {
                title: tab.title,
            },
            confirm: () =>
                this.entity$.next({
                    ...this.entity,
                    tabs: this.entity.tabs.filter((next) => next.id !== tab.id),
                }),
        });
    }

    canMoveUp(tab: BazaNcIntegrationSchemaTab): boolean {
        return this.entity.tabs.indexOf(tab) > 0;
    }

    canMoveDown(tab: BazaNcIntegrationSchemaTab): boolean {
        return this.entity.tabs.indexOf(tab) < this.entity.tabs.length - 1;
    }

    moveUp(tab: BazaNcIntegrationSchemaTab): void {
        if (!this.canMoveUp(tab)) {
            return;
        }

        const tabs = [...this.entity.tabs];
        const index = tabs.indexOf(tab);

        if (index < 1) {
            return;
        }

        const swapElement = tabs[index - 1];

        tabs[index - 1] = tab;
        tabs[`${index}`] = swapElement;

        this.entity$.next({
            ...this.entity,
            tabs,
        });
    }

    moveDown(tab: BazaNcIntegrationSchemaTab): void {
        if (!this.canMoveDown(tab)) {
            return;
        }

        const tabs = [...this.entity.tabs];
        const index = tabs.indexOf(tab);

        if (index >= tabs.length - 1) {
            return;
        }

        const swapElement = tabs[index + 1];

        tabs[index + 1] = tab;
        tabs[`${index}`] = swapElement;

        this.entity$.next({
            ...this.entity,
            tabs,
        });
    }
}
