import { Routes } from '@angular/router';

import { bazaNcIntegrationCmsListingsRoutes } from '../libs/listings/src';
import { bazaNcIntegrationFeedRoutes } from '../libs/feed/src';
import { bazaNcIntegrationEliteInvestorCmsRoutes } from '../libs/elite-investor/src';
import { bazaNcIntegrationSubscriptionCmsRoutes } from '../libs/subscription/src';
import { bazaNcIntegrationSchemaCmsRoutes } from '../libs/schema/src';
import { bazaNcIntegrationPerkRoutes } from '../libs/perk/src';
import { bazaNcIntegrationCmsListingTestimonialRoutes } from '../libs/testimonial/src';

export const bazaNcIntegrationCmsRoutes: Routes = [
    {
        path: 'baza-nc-integration',
        children: [
            ...bazaNcIntegrationSchemaCmsRoutes,
            ...bazaNcIntegrationCmsListingsRoutes,
            ...bazaNcIntegrationFeedRoutes,
            ...bazaNcIntegrationEliteInvestorCmsRoutes,
            ...bazaNcIntegrationSubscriptionCmsRoutes,
            ...bazaNcIntegrationPerkRoutes,
            ...bazaNcIntegrationCmsListingTestimonialRoutes,
        ],
    },
];
