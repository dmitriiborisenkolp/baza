import { NgModule } from '@angular/core';
import { bazaNcBundleCmsConfig } from '@scaliolabs/baza-nc-cms';
import { BazaNcIntegrationEliteInvestorCmsModule } from '../libs/elite-investor/src';
import { BazaNcIntegrationFeedCmsModule } from '../libs/feed/src';
import { BazaNcIntegrationCmsListingsModule } from '../libs/listings/src';
import { BazaNcIntegrationPerkModule } from '../libs/perk/src';
import { BazaNcIntegrationSchemaCmsModule } from '../libs/schema/src';
import { BazaNcIntegrationSubscriptionCmsModule } from '../libs/subscription/src';
import { BazaNcIntegrationCmsListingTestimonialModule } from '../libs/testimonial/src';

import {
    BazaNcIntegrationListingsCmsDataAccess,
    BazaNcIntegrationListingsCmsDataAccessModule,
} from '@scaliolabs/baza-nc-integration-cms-data-access';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
    bazaNcIntegrationBundleCmsConfig,
    BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN,
} from '../config/baza-nc-integration-bundles-cms.config';

const MODULES = [
    BazaNcIntegrationListingsCmsDataAccessModule,
    BazaNcIntegrationCmsListingsModule,
    BazaNcIntegrationFeedCmsModule,
    BazaNcIntegrationEliteInvestorCmsModule,
    BazaNcIntegrationSubscriptionCmsModule,
    BazaNcIntegrationSchemaCmsModule,
    BazaNcIntegrationPerkModule,
    BazaNcIntegrationCmsListingTestimonialModule,
];

@NgModule({
    imports: MODULES,
    exports: MODULES,
    providers: [
        {
            provide: BAZA_NC_INTEGRATION_BUNDLE_CMS_CONFIG_TOKEN,
            useValue: bazaNcIntegrationBundleCmsConfig(),
        },
    ],
})
export class BazaNcIntegrationCmsModule {
    constructor(private readonly dataAccessListings: BazaNcIntegrationListingsCmsDataAccess) {
        this.enableOfferingFiltersForDividends();
    }

    enableOfferingFiltersForDividends(): void {
        bazaNcBundleCmsConfig({
            filterOfferingsForDividends: (offerings) =>
                this.dataAccessListings.listAll().pipe(
                    switchMap((listings) => {
                        const listingOfferingIds = listings.map((next) => next.offeringId);

                        return of(offerings.filter((next) => listingOfferingIds.includes(next.ncOfferingId)));
                    }),
                ),
        });
    }
}
