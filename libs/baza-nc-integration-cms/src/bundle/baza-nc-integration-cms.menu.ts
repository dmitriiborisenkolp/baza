import { CmsSidePanelMenuGroupConfig } from '@scaliolabs/baza-core-cms';
import { bazaNcIntegrationCmsListingsMenu } from '../libs/listings/src';
import { bazaNcIntegrationFeedCmsMenu } from '../libs/feed/src';
import { bazaNcIntegrationSchemaCmsMenu } from '../libs/schema/src';
import { bazaNcIntegrationBundleCmsConfig } from '../config/baza-nc-integration-bundles-cms.config';

export const bazaNcIntegrationCmsMenu: () => Array<CmsSidePanelMenuGroupConfig> = () => {
    const schemaCmsMenu = bazaNcIntegrationBundleCmsConfig().withSchemaCMS ? bazaNcIntegrationSchemaCmsMenu : [];

    return [
        {
            title: 'bazaNcIntegration.sidebar.title',
            items: [...schemaCmsMenu, ...bazaNcIntegrationCmsListingsMenu, ...bazaNcIntegrationFeedCmsMenu],
        },
    ];
};
