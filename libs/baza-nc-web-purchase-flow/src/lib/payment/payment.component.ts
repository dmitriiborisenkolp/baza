import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import {
    BazaNcPurchaseFlowLimitsForPurchaseResponse,
    BazaNcPurchaseFlowTransactionType,
    PurchaseFlowDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService, I18nLinkifyConfig } from '@scaliolabs/baza-web-utils';
import { combineLatest, debounceTime, distinctUntilChanged, map, of, skipWhile, switchMap, withLatestFrom } from 'rxjs';
import { GetLimits, GetLimitsForPurchase, PatchStartPurchase, PaymentConfig, PurchaseState, SubmitPurchase } from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-nc-purchase-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPurchasePaymentComponent implements OnInit {
    @Input()
    paymentConfig?: PaymentConfig;

    cart$ = this.store.select(PurchaseState.cart);
    purchaseStart$ = this.store.select(PurchaseState.purchaseStart);
    numberOfShares$ = this.store.select(PurchaseState.numberOfShares);
    limitsForPurchase$ = this.store.select(PurchaseState.limitsForPurchase);

    purchaseStart: PurchaseFlowDto;
    limitsForPurchase: BazaNcPurchaseFlowLimitsForPurchaseResponse;
    isPurchaseAboveLimit = false;
    isSubmitBtnDisabled = true;
    purchaseLimitsLoaded = false;
    loadMethods = false;
    public paymentMethodAdded = false;

    public i18nBasePath = 'ncpf.payment';

    constructor(
        private readonly router: Router,
        private readonly store: Store,
        public readonly uts: BazaLinkUtilSharedService,
        public readonly wts: BazaWebUtilSharedService,
        private readonly cdr: ChangeDetectorRef,
    ) {
        this.store.dispatch(new GetLimits());
    }

    ngOnInit(): void {
        this.loadMethods = false;
        this.purchaseLimitsLoaded = false;
        this.checkDefaultConfig();
        this.initializeDataStream();
    }

    initializeDataStream() {
        const result$ = combineLatest([this.purchaseStart$, this.numberOfShares$, this.cart$]).pipe(
            untilDestroyed(this),
            map(([purchaseStart, number, entity]) => ({
                purchaseStart,
                number,
                entity,
            })),
            skipWhile((pair) => !pair || !pair.entity || !pair.number || !pair.purchaseStart),
            distinctUntilChanged(),
            debounceTime(0),
            switchMap((pair) => {
                this.purchaseStart = pair.purchaseStart;
                const total = (pair.number || 1) * pair.entity.pricePerShareCents;

                if (!this.purchaseLimitsLoaded) {
                    return this.store
                        .dispatch(new GetLimitsForPurchase(total, pair.entity.offeringId))
                        .pipe(withLatestFrom(this.limitsForPurchase$));
                } else {
                    return of([]);
                }
            }),
        );

        result$
            .pipe(
                untilDestroyed(this),
                skipWhile((res) => !res || res?.length === 0),
            )
            .subscribe(([, res]) => {
                if (res) {
                    this.limitsForPurchase = res;
                    this.purchaseLimitsLoaded = true;
                    this.validateCCLimitForPurchase();
                }
            });
    }

    // onFormSubmit
    public onFormSubmit(): void {
        this.store.dispatch(new SubmitPurchase(this.purchaseStart.id)).subscribe(() => {
            this.router.navigate(['/buy-shares', 'done']);
        });
    }

    /**
     * Utility method to validate the purchaseLimit
     */
    private validateCCLimitForPurchase() {
        const limitsForPurchase = this.limitsForPurchase;
        const purchaseStart = this.purchaseStart;
        if (limitsForPurchase && purchaseStart) {
            const { amount, transactionFeesCents } = purchaseStart;
            const totalCents = amount + transactionFeesCents;

            const ccLimits = limitsForPurchase.find((limit) => limit.transactionType === BazaNcPurchaseFlowTransactionType.CreditCard);
            this.isPurchaseAboveLimit =
                !ccLimits?.isAvailable ||
                // Based on active Payment method CC fees are added and Total changes.
                // So, Manually verifying if total goes above limit? Yes, restrict the CC.
                totalCents + ccLimits?.feeCents > ccLimits?.maximumAmountCents;
        }

        this.loadMethods = true;
        this.cdr.detectChanges();
    }

    public onBackClick(): void {
        if (this.purchaseStart) {
            this.purchaseStart.fee = null;
            this.store.dispatch(new PatchStartPurchase(this.purchaseStart));
        }

        this.router.navigate(['/buy-shares', 'agreement']);
    }

    onUpdateSubmitBtnDisableStatus(isDisabled: boolean) {
        this.isSubmitBtnDisabled = isDisabled;
    }

    checkDefaultConfig() {
        const defaultConfig: PaymentConfig = {
            paymentMethodConfig: {
                showPurchaseDividendMessage: true,
            },
            paymentStepsConfig: {
                termsOfServicesLink: {
                    appLink: {
                        commands: ['/terms-of-service'],
                    },
                },
                eftDisclosureLink: {
                    appLink: {
                        commands: ['/eft-disclosure'],
                    },
                },
            },
        };

        this.paymentConfig = this.wts.mergeConfig(defaultConfig, this.paymentConfig);
    }

    get authorizationLinksConfig(): Array<I18nLinkifyConfig> {
        return this.wts.getI18nLinksConfig([
            {
                path: `${this.i18nBasePath}.authorization.tosLinkConfig`,
                config: this.paymentConfig?.paymentStepsConfig?.termsOfServicesLink,
            },
            {
                path: `${this.i18nBasePath}.authorization.eftLinkConfig`,
                config: this.paymentConfig?.paymentStepsConfig?.eftDisclosureLink,
            },
        ]);
    }
}
