import { NcPaymentEditAccountEnI18n } from '../account/i18n/account.i18n';
import { NcPaymentEditAddEnI18n } from '../add/i18n/add.i18n';
import { NcPaymentEditListEnI18n } from '../list/i18n/list.i18n';
import { NcPaymentEditUpdateEnI18n } from '../update/i18n/update.i18n';

export const NcPaymentEditEnI18n = {
    account: NcPaymentEditAccountEnI18n,
    add: NcPaymentEditAddEnI18n,
    list: NcPaymentEditListEnI18n,
    update: NcPaymentEditUpdateEnI18n,
};
