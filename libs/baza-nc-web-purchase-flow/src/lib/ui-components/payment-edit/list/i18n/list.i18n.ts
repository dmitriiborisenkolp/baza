export const NcPaymentEditListEnI18n = {
    title: {
        singular: 'Payment Method',
        plural: 'Payment Methods',
    },
    bank: {
        title: 'Bank Account',
    },
    card: {
        title: 'Credit Сard',
        feeSuffix: 'Card processing fee',
        limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account',
    },
    actions: {
        update: 'Update Payment Method',
    },
};
