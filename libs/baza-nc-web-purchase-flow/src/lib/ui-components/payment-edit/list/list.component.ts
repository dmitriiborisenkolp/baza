import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import { BazaNcPurchaseFlowGetCreditCardResponse, PurchaseFlowBankAccountDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { PaymentMethodType, PurchaseState } from '../../../data-access';

@Component({
    selector: 'app-nc-payment-edit-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPaymentEditListComponent implements OnInit {
    limits$ = this.store.select(PurchaseState.limits);

    @Input()
    selectedPaymentMethod: PaymentMethodType;

    @Input()
    creditCardResponse: BazaNcPurchaseFlowGetCreditCardResponse;

    @Input()
    bankAccountResponse: PurchaseFlowBankAccountDto;

    @Input()
    isPurchaseAboveLimit: boolean;

    @Output()
    modalTitleChange = new EventEmitter<string>();

    @Output()
    paymentMethodChange = new EventEmitter<PaymentMethodType>();

    @Output()
    updatePaymentMethodClick = new EventEmitter<void>();

    PAYMENT_METHOD_TYPE = PaymentMethodType;

    public i18nBasePath = 'ncpf.pymtEdit.list';

    constructor(private store: Store, public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit(): void {
        // set modal title in parent component
        const modalTitle =
            this.creditCardResponse?.isAvailable && this.bankAccountResponse?.isAvailable
                ? this.wts.getI18nLabel(this.i18nBasePath, 'title.plural')
                : this.wts.getI18nLabel(this.i18nBasePath, 'title.singular');

        this.modalTitleChange.emit(modalTitle);
    }

    getPurchaseLimitWarningMsg(maxAmount: string) {
        return this.wts.getI18nLabel(this.i18nBasePath, 'card.limitsWarning', { maxAmount });
    }

    onBankAccountSelect() {
        this.paymentMethodChange.emit(PaymentMethodType.bankAccount);
    }

    onCreditCardSelect() {
        if (!this.isPurchaseAboveLimit) {
            this.paymentMethodChange.emit(PaymentMethodType.creditCard);
        }
    }

    onPaymentMethodUpdate() {
        this.updatePaymentMethodClick.emit();
    }
}
