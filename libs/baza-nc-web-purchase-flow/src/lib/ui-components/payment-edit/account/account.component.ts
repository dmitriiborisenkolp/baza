import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { PaymentMethodType, PaymentUpdateAccountType } from '../../../data-access';

@Component({
    selector: 'app-nc-payment-edit-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPaymentEditAccountComponent implements OnInit {
    @Input()
    paymentUpdateAccountType: PaymentUpdateAccountType;

    @Output()
    addAccount: EventEmitter<PaymentMethodType> = new EventEmitter();

    @Output()
    modalTitleChange: EventEmitter<string> = new EventEmitter();

    public i18nBasePath = 'ncpf.pymtEdit.account';

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        // set modal title in parent component
        const modalTitle =
            this.paymentUpdateAccountType === PaymentUpdateAccountType.creditCard
                ? this.wts.getI18nLabel(this.i18nBasePath, 'card.title')
                : this.wts.getI18nLabel(this.i18nBasePath, 'bank.title');

        this.modalTitleChange.emit(modalTitle);
    }

    public addBankAccount() {
        this.addAccount.emit(PaymentMethodType.bankAccount);
    }

    public addCreditCard() {
        this.addAccount.emit(PaymentMethodType.creditCard);
    }
}
