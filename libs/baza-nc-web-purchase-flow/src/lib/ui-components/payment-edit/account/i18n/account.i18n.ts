export const NcPaymentEditAccountEnI18n = {
    bank: {
        title: 'Update Bank Account',
        description:
            'You are about to begin adding a new bank account. If you complete this process, your current payment and dividend method account will be replaced.',
        actions: {
            add: 'Add New Bank Account',
        },
    },
    card: {
        title: 'Update Credit Card',
        description:
            'You are about to begin adding a new сredit or debit сard. If you complete this process, your current card will be replaced.',
        actions: {
            add: 'Add New Card',
        },
    },
};
