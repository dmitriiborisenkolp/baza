import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcPurchaseFlowGetCreditCardResponse, PurchaseFlowBankAccountDto } from '@scaliolabs/baza-nc-shared';
import {
    PaymentEditModalsContentType,
    PaymentMethodType,
    PaymentUpdateAccountType,
    PurchaseState,
    TogglePaymentEditModal,
} from '../../data-access';

@UntilDestroy()
@Component({
    selector: 'app-nc-payment-edit',
    templateUrl: './payment-edit.component.html',
    styleUrls: ['./payment-edit.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPaymentEditComponent implements OnInit {
    isPaymentEditModalOpen$ = this.store.select(PurchaseState.isPaymentEditModalOpen);

    @Input()
    selectedPaymentMethod: PaymentMethodType;

    @Input()
    creditCardResponse: BazaNcPurchaseFlowGetCreditCardResponse;

    @Input()
    bankAccountResponse: PurchaseFlowBankAccountDto;

    @Input()
    isPurchaseAboveLimit: boolean;

    @Input()
    bankDetailsForm: FormGroup;

    @Input()
    cardDetailsForm: FormGroup;

    @Output()
    public bankLink = new EventEmitter<void>();

    @Output()
    public selectPaymentMethod = new EventEmitter<PaymentMethodType>();

    @Output()
    public submitAccountDetailsForm = new EventEmitter<PaymentMethodType>();

    public modalContentType: PaymentEditModalsContentType = PaymentEditModalsContentType.paymentList;
    public modalTitle: string;
    public paymentMethodType: PaymentMethodType;
    public paymentUpdateAccountType: PaymentUpdateAccountType;

    constructor(private store: Store) {
        this.isPaymentEditModalOpen$.pipe(untilDestroyed(this)).subscribe((isPaymentEditModalOpen) => {
            if (!isPaymentEditModalOpen) {
                this.onCloseModal();
            }
        });
    }

    ngOnInit() {
        this.setInitModalData();
        this.modalTitle = 'Modal Window';
    }

    private setInitModalData() {
        this.modalContentType = PaymentEditModalsContentType.paymentList;
    }

    public onCloseModal() {
        this.setInitModalData();
        this.store.dispatch(new TogglePaymentEditModal(false));
    }

    public onAddAccount(addAccountType: PaymentMethodType) {
        this.paymentMethodType = addAccountType;
        this.modalContentType = PaymentEditModalsContentType.paymentAdd;
    }

    public onBackClick() {
        if (this.modalContentType === PaymentEditModalsContentType.paymentUpdate) {
            this.setInitModalData();
        } else if (this.modalContentType === PaymentEditModalsContentType.paymentUpdateAccount) {
            this.onUpdatePaymentMethodClick();
        } else if (
            (this.paymentMethodType === PaymentMethodType.creditCard && !this.creditCardResponse.isAvailable) ||
            (this.paymentMethodType === PaymentMethodType.bankAccount && !this.bankAccountResponse.isAvailable)
        ) {
            this.modalContentType = PaymentEditModalsContentType.paymentUpdate;
        } else if (this.modalContentType === PaymentEditModalsContentType.paymentAdd) {
            this.modalContentType = PaymentEditModalsContentType.paymentUpdateAccount;
        }
    }

    public onModalTitleChange(title: string) {
        this.modalTitle = title;
    }

    public onSubmitAccountDetailsForm(accountFormType: PaymentMethodType) {
        this.submitAccountDetailsForm.emit(accountFormType);
    }

    public onPaymentMethodChange(paymentMethod: PaymentMethodType) {
        this.selectPaymentMethod.emit(paymentMethod);
    }

    public onUpdatePaymentAccount(paymentUpdateAccountType: PaymentUpdateAccountType) {
        if (paymentUpdateAccountType === PaymentUpdateAccountType.linkBankAccount) {
            this.onCloseModal();
            this.bankLink.emit();
        } else {
            if (!this.creditCardResponse.isAvailable && paymentUpdateAccountType === PaymentUpdateAccountType.creditCard) {
                this.paymentMethodType = PaymentMethodType.creditCard;
                this.modalContentType = PaymentEditModalsContentType.paymentAdd;
            } else if (
                !this.bankAccountResponse.isAvailable &&
                paymentUpdateAccountType === PaymentUpdateAccountType.manuallyAddBankAccount
            ) {
                this.paymentMethodType = PaymentMethodType.bankAccount;
                this.modalContentType = PaymentEditModalsContentType.paymentAdd;
            } else {
                this.modalContentType = PaymentEditModalsContentType.paymentUpdateAccount;
            }
            this.paymentUpdateAccountType = paymentUpdateAccountType;
        }
    }

    public onUpdatePaymentMethodClick() {
        this.modalContentType = PaymentEditModalsContentType.paymentUpdate;
    }
}
