import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { BankDetailsComponent, CardDetailsComponent } from '@scaliolabs/baza-web-ui-components';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { NzModalComponent } from 'ng-zorro-antd/modal';
import { PaymentMethodType } from '../../../data-access';

@Component({
    selector: 'app-nc-payment-edit-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPaymentEditAddComponent implements OnInit {
    @Input()
    paymentMethodType: PaymentMethodType;

    @Input()
    bankDetailsForm: FormGroup;

    @Input()
    cardDetailsForm: FormGroup;

    @Input()
    invalidFormScrollContainerRef: NzModalComponent | HTMLElement = null;

    @Output()
    modalTitleChange: EventEmitter<string> = new EventEmitter();

    @Output()
    submitForm: EventEmitter<PaymentMethodType> = new EventEmitter();

    @ViewChild('bankDetails', { static: false }) bankDetails: BankDetailsComponent;
    @ViewChild('cardDetails', { static: false }) cardDetails: CardDetailsComponent;

    public i18nBasePath = 'ncpf.pymtEdit.add';

    constructor(public readonly bazaFormValidatorService: BazaFormValidatorService, public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit() {
        // set modal title in parent component
        const modalTitle =
            this.paymentMethodType === 'bankAccount'
                ? this.wts.getI18nLabel(this.i18nBasePath, 'bank.title')
                : this.wts.getI18nLabel(this.i18nBasePath, 'card.title');

        this.modalTitleChange.emit(modalTitle);
    }

    public addBankAccount() {
        const formElRef = this.bankDetails?.bankDetailsFormRef?.nativeElement ?? null;
        if (this.bazaFormValidatorService.isFormValid(this.bankDetailsForm, formElRef, this.invalidFormScrollContainerRef)) {
            this.submitForm.emit(PaymentMethodType.bankAccount);
        }
    }

    public addCreditCard() {
        const formElRef = this.cardDetails?.cardDetailsFormRef?.nativeElement ?? null;
        if (this.bazaFormValidatorService.isFormValid(this.cardDetailsForm, formElRef, this.invalidFormScrollContainerRef)) {
            this.submitForm.emit(PaymentMethodType.creditCard);
        }
    }
}
