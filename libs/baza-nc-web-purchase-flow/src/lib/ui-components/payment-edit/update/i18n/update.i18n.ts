export const NcPaymentEditUpdateEnI18n = {
    title: 'Update Payment Method',
    bank: {
        title: 'Bank Account',
        plaid: {
            title: 'Link bank account',
        },
        manual: {
            title: 'Manually add bank details',
        },
    },
    card: {
        title: 'Card',
        manual: {
            title: 'Add card',
            feeSuffix: 'Fee',
        },
        limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account',
    },
};
