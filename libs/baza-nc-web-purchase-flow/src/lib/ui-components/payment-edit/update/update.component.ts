import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { PaymentUpdateAccountType, PurchaseState } from '../../../data-access';

@Component({
    selector: 'app-nc-payment-edit-update',
    templateUrl: './update.component.html',
    styleUrls: ['./update.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPaymentEditUpdateComponent implements OnInit {
    limits$ = this.store.select(PurchaseState.limits);

    @Input()
    isPurchaseAboveLimit: boolean;

    @Output()
    modalTitleChange: EventEmitter<string> = new EventEmitter();

    @Output()
    updateAccount: EventEmitter<PaymentUpdateAccountType> = new EventEmitter();

    public i18nBasePath = 'ncpf.pymtEdit.update';

    constructor(private store: Store, public readonly wts: BazaWebUtilSharedService) {}

    ngOnInit(): void {
        // set modal title in parent component
        this.modalTitleChange.emit(this.wts.getI18nLabel(this.i18nBasePath, 'title'));
    }

    public getPurchaseLimitWarningMsg(maxAmount: string) {
        return this.wts.getI18nLabel(this.i18nBasePath, 'card.limitsWarning', { maxAmount });
    }

    public addCreditCard() {
        if (!this.isPurchaseAboveLimit) this.updateAccount.emit(PaymentUpdateAccountType.creditCard);
    }

    public linkBankAccount() {
        this.updateAccount.emit(PaymentUpdateAccountType.linkBankAccount);
    }

    public manuallyAddBankAccount() {
        this.updateAccount.emit(PaymentUpdateAccountType.manuallyAddBankAccount);
    }
}
