import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { BankDetailsModule, CardDetailsModule, PaymentHeaderModule, PaymentRadioModule } from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NcPaymentEditAccountComponent } from './account/account.component';
import { NcPaymentEditAddComponent } from './add/add.component';
import { NcPaymentEditComponent } from './payment-edit.component';
import { NcPaymentEditListComponent } from './list/list.component';
import { NcPaymentEditUpdateComponent } from './update/update.component';
import { BazaNgCoreModule } from '@scaliolabs/baza-core-ng';

@NgModule({
    declarations: [
        NcPaymentEditComponent,
        NcPaymentEditAccountComponent,
        NcPaymentEditAddComponent,
        NcPaymentEditListComponent,
        NcPaymentEditUpdateComponent,
    ],
    exports: [
        NcPaymentEditComponent,
        NcPaymentEditAccountComponent,
        NcPaymentEditAddComponent,
        NcPaymentEditListComponent,
        NcPaymentEditUpdateComponent,
    ],
    imports: [
        BankDetailsModule,
        CardDetailsModule,
        CommonModule,
        NzButtonModule,
        NzModalModule,
        PaymentHeaderModule,
        PaymentRadioModule,
        RouterModule,
        UtilModule,
        BazaNgCoreModule,
    ],
})
export class NcPaymentEditModule {}
