import { NcPaymentBankAccountModalEnI18n } from '../payment-bank-account-modal/i18n/payment-bank-account-modal.i18n';
import { NcPaymentBankAccountEnI18n } from '../payment-bank-account/i18n/payment-bank-account.i18n';
import { NcPaymentCardModalEnI18n } from '../payment-card-modal/i18n/payment-card-modal.i18n';
import { NcPaymentCardEnI18n } from '../payment-card/i18n/payment-card.i18n';

export const NcPaymentMethodsEnI18n = {
    bank: {
        details: NcPaymentBankAccountEnI18n,
        modal: NcPaymentBankAccountModalEnI18n,
    },
    card: {
        details: NcPaymentCardEnI18n,
        modal: NcPaymentCardModalEnI18n,
    },
};
