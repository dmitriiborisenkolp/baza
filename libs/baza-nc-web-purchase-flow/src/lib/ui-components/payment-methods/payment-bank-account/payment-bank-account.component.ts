import { Component, EventEmitter, Input, Output } from '@angular/core';
import { PurchaseFlowBankAccountDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

export type PaymentBankAccountStyle = 'Simple' | 'Bordered';

@Component({
    selector: 'app-nc-payment-bank-account',
    templateUrl: './payment-bank-account.component.html',
    styleUrls: ['./payment-bank-account.component.less'],
})
export class NcPaymentBankAccountComponent {
    @Input()
    srcData: PurchaseFlowBankAccountDto;

    @Input()
    style: PaymentBankAccountStyle = 'Simple';

    @Input()
    isEditModeOn: boolean;

    @Input()
    isAddModeOn: boolean;

    @Output()
    bankLinkClicked = new EventEmitter();

    @Output()
    manualLinkClicked = new EventEmitter();

    public i18nBasePath = 'ncpf.pymtMethods.bank.details';

    constructor(public readonly wts: BazaWebUtilSharedService) {}
}
