export const NcPaymentBankAccountEnI18n = {
    addMode: {
        plaidLinking: 'Link bank account',
        manualLinking: 'Manually add bank details',
    },
    editMode: {
        account: {
            name: 'Account holder name:',
            number: 'Account number:',
            type: 'Account type:',
        },
    },
};
