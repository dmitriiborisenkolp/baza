import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { BankDetailsComponent } from '@scaliolabs/baza-web-ui-components';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

@Component({
    selector: 'app-nc-payment-bank-account-modal',
    templateUrl: './payment-bank-account-modal.component.html',
    styleUrls: ['./payment-bank-account-modal.component.less'],
})
export class NcPaymentBankAccountModalComponent {
    @Input()
    bankDetailsForm: FormGroup;

    @Input()
    isBankDetailsFormVisible: boolean;

    @Output()
    handleBankDetailsCancel = new EventEmitter();

    @Output()
    submitManualBankDetailsForm = new EventEmitter();

    @ViewChild('bankDetails', { static: false }) bankDetails: BankDetailsComponent;

    public i18nBasePath = 'ncpf.pymtMethods.bank.modal';

    constructor(public readonly bazaFormValidatorService: BazaFormValidatorService, public readonly wts: BazaWebUtilSharedService) {}

    public addBankAccount(modalEl) {
        const formElRef = this.bankDetails?.bankDetailsFormRef?.nativeElement;
        if (this.bazaFormValidatorService.isFormValid(this.bankDetailsForm, formElRef, modalEl)) {
            this.submitManualBankDetailsForm.emit();
        }
    }
}
