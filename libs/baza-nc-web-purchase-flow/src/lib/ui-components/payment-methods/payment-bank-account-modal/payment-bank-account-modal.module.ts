import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { BankDetailsModule, PaymentHeaderModule } from '@scaliolabs/baza-web-ui-components';
import { NcPaymentBankAccountModalComponent } from './payment-bank-account-modal.component';

@NgModule({
    declarations: [NcPaymentBankAccountModalComponent],
    imports: [BankDetailsModule, CommonModule, NzButtonModule, NzModalModule, PaymentHeaderModule],
    exports: [NcPaymentBankAccountModalComponent],
})
export class NcPaymentBankAccountModalModule {}
