import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BazaNcLimitsDto, BazaNcPurchaseFlowGetCreditCardResponse } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { Observable } from 'rxjs';

export type PaymentCardStyle = 'Simple' | 'Bordered';

@Component({
    selector: 'app-nc-payment-card',
    templateUrl: './payment-card.component.html',
    styleUrls: ['./payment-card.component.less'],
})
export class NcPaymentCardComponent {
    @Input()
    srcData: BazaNcPurchaseFlowGetCreditCardResponse;

    @Input()
    isPurchaseAboveLimit = false;

    @Input()
    doShowNote = true;

    @Input()
    limits$: Observable<BazaNcLimitsDto>;

    @Input()
    style: PaymentCardStyle = 'Simple';

    @Input()
    isEditModeOn: boolean;

    @Input()
    isAddModeOn: boolean;

    @Output()
    handleAddCard = new EventEmitter();

    public i18nBasePath = 'ncpf.pymtMethods.card.details';

    constructor(public readonly wts: BazaWebUtilSharedService) {}

    getPurchaseLimitWarningMsg(maxAmount: string) {
        return this.wts.getI18nLabel(this.i18nBasePath, 'alerts.limitsWarning', { maxAmount });
    }
}
