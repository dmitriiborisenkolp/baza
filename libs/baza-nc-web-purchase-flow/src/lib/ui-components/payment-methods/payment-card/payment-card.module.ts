import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UtilModule } from '@scaliolabs/baza-web-utils';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NcPaymentCardComponent } from './payment-card.component';

@NgModule({
    declarations: [NcPaymentCardComponent],
    imports: [CommonModule, NzButtonModule, NzGridModule, NzPopoverModule, UtilModule],
    exports: [NcPaymentCardComponent],
})
export class NcPaymentCardModule {}
