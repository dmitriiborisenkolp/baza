import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BazaFormValidatorService } from '@scaliolabs/baza-core-ng';
import { CardDetailsComponent } from '@scaliolabs/baza-web-ui-components';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';

@Component({
    selector: 'app-nc-payment-card-modal',
    templateUrl: './payment-card-modal.component.html',
    styleUrls: ['./payment-card-modal.component.less'],
})
export class NcPaymentCardModalComponent {
    @Input()
    cardDetailsForm: FormGroup;

    @Input()
    isCardDetailsFormVisible: boolean;

    @Output()
    handleCardDetailsCancel = new EventEmitter();

    @Output()
    submitCardForm = new EventEmitter();

    @ViewChild('cardDetails', { static: false }) cardDetails: CardDetailsComponent;

    public i18nBasePath = 'ncpf.pymtMethods.card.modal';

    constructor(public readonly bazaFormValidatorService: BazaFormValidatorService, public readonly wts: BazaWebUtilSharedService) {}

    public addCreditCard(modalEl) {
        const formElRef = this.cardDetails?.cardDetailsFormRef?.nativeElement;
        if (this.bazaFormValidatorService.isFormValid(this.cardDetailsForm, formElRef, modalEl)) {
            this.submitCardForm.emit();
        }
    }
}
