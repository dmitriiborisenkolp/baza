import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CardDetailsModule, PaymentHeaderModule } from '@scaliolabs/baza-web-ui-components';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NcPaymentCardModalComponent } from './payment-card-modal.component';

@NgModule({
    declarations: [NcPaymentCardModalComponent],
    imports: [CommonModule, CardDetailsModule, PaymentHeaderModule, NzButtonModule, NzModalModule],
    exports: [NcPaymentCardModalComponent],
})
export class NcPaymentCardModalModule {}
