import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcBankAccountAchDto,
    BazaNcPurchaseFlowGetCreditCardResponse,
    BazaNcPurchaseFlowTransactionType,
    PurchaseFlowDto,
    PurchaseFlowSessionDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { Subject, combineLatest, concat } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, map, skipWhile, tap } from 'rxjs/operators';
import {
    LoadBankAccount,
    LoadCreditCard,
    PaymentMethodConfig,
    PaymentMethodType,
    PaymentMethodsService,
    PurchaseState,
    StartPurchase,
    TogglePaymentEditModal,
} from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-nc-purchase-methods',
    templateUrl: './methods.component.html',
    styleUrls: ['./methods.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPurchaseMethodsComponent implements OnInit {
    @Input()
    isPurchaseAboveLimit: boolean;

    @Input()
    paymentMethodConfig?: PaymentMethodConfig;

    @Output()
    updateSubmitBtnDisableStatus = new EventEmitter<boolean>();

    @Output()
    public readonly paymentMethodAdded = new EventEmitter<boolean>();

    // store selectors
    cart$ = this.store.select(PurchaseState.cart);
    purchaseStart$ = this.store.select(PurchaseState.purchaseStart);
    numberOfShares$ = this.store.select(PurchaseState.numberOfShares);
    bankAccount$ = this.store.select(PurchaseState.bankAccount);
    creditCard$ = this.store.select(PurchaseState.creditCard);
    limits$ = this.store.select(PurchaseState.limits);

    cardDetailsForm: FormGroup;
    onSubmitCardForm: Subject<FormGroup> = new Subject();
    isCardDetailsFormVisible: boolean;

    bankDetailsForm: FormGroup;
    onSubmitManualBankDetailsForm: Subject<FormGroup> = new Subject();
    isBankDetailsFormVisible: boolean;

    purchaseStart: PurchaseFlowDto;
    creditCardResponse: BazaNcPurchaseFlowGetCreditCardResponse;
    bankAccountResponse: BazaNcBankAccountAchDto;
    selectedPaymentMethod: PaymentMethodType = null;
    entity: BazaNcIntegrationListingsDto;
    numberOfShares: number;

    purchaseSessionStarted = false;
    isCardAvailable = false;
    isNCBankAccountAvailable = false;
    isPaymentEditModalVisible = false;
    dataLoaded = false;

    public i18nBasePath = 'ncpf.methods';

    constructor(
        private readonly store: Store,
        private readonly cdr: ChangeDetectorRef,
        public readonly pms: PaymentMethodsService,
        public readonly wts: BazaWebUtilSharedService,
    ) {
        concat(this.store.dispatch(new LoadCreditCard()), this.store.dispatch(new LoadBankAccount()));
    }

    ngOnInit(): void {
        this.resetBankDetailsForm();
        this.resetCardDetailsForm();
        this.resetVariables();

        this.onSubmitManualBankDetailsForm
            .pipe(
                tap((value) => this.processSubmitManualBankDetailsForm(value)),
                untilDestroyed(this),
            )
            .subscribe();

        this.onSubmitCardForm
            .pipe(
                tap((value) => this.processSubmitCardForm(value)),
                untilDestroyed(this),
            )
            .subscribe();

        this.initializeDataStream();
    }

    private initializeDataStream() {
        combineLatest([this.bankAccount$, this.creditCard$, this.purchaseStart$, this.cart$, this.numberOfShares$])
            .pipe(
                untilDestroyed(this),
                map(([bank, card, purchaseStart, cart, numberOfShares]) => ({
                    bank,
                    card,
                    purchaseStart,
                    cart,
                    numberOfShares,
                })),
                skipWhile((pair) => !pair?.bank || !pair?.card || !pair?.purchaseStart || !pair?.cart || !pair?.numberOfShares),
                distinctUntilChanged(),
                debounceTime(0),
            )
            .subscribe((pair) => {
                this.purchaseStart = pair?.purchaseStart;
                this.creditCardResponse = pair?.card;
                this.bankAccountResponse = pair?.bank;

                this.entity = pair?.cart;
                this.numberOfShares = pair?.numberOfShares || 1;

                this.onDataStreamCompletion();
            });
    }

    private onDataStreamCompletion() {
        this.resetVariables();

        if (!this.selectedPaymentMethod) {
            this.setDefaultPaymentMethod();
        }

        if (!this.purchaseSessionStarted) {
            this.startPurchaseSession();
        }

        this.paymentMethodAdded.emit(this.isCardAvailable || this.isNCBankAccountAvailable);
    }

    private updateTransactionType(transactionType: BazaNcPurchaseFlowTransactionType) {
        const purchase: PurchaseFlowSessionDto = {
            amount: this.entity.pricePerShareCents * this.numberOfShares,
            numberOfShares: this.numberOfShares,
            offeringId: this.entity.offeringId,
            transactionType: transactionType,
        };

        this.updateSubmitBtnDisableStatus.emit(this.isSubmitBtnDisabled);
        return this.store.dispatch(new StartPurchase(purchase));
    }

    private resetBankDetailsForm(): void {
        this.bankDetailsForm = this.pms.generateBankDetailsForm();
    }

    private resetCardDetailsForm(): void {
        this.cardDetailsForm = this.pms.generateCreditCardForm();
    }

    private resetVariables() {
        this.isCardAvailable = this.pms.isCardAvailable(this.creditCardResponse);
        this.isNCBankAccountAvailable = this.pms.isNCBankAccountAvailable(this.bankAccountResponse);
        this.updateSubmitBtnDisableStatus.emit(this.isSubmitBtnDisabled);
    }

    // add debit or credit card
    public handleAddCard() {
        this.isCardDetailsFormVisible = true;
    }

    public triggerPlaidFlow() {
        this.pms.processPlaidDialog(() => {
            this.store.dispatch(new LoadBankAccount());
            this.onSelectPaymentMethod(PaymentMethodType.bankAccount);
        });
    }

    public onManualLink() {
        this.isBankDetailsFormVisible = true;
    }

    public handleBankDetailsCancel() {
        this.isBankDetailsFormVisible = false;
    }

    public submitManualBankDetailsForm() {
        this.onSubmitManualBankDetailsForm.next(this.bankDetailsForm);
    }

    // submit debit or credit card form
    public submitCardForm() {
        this.onSubmitCardForm.next(this.cardDetailsForm);
    }

    // cancel debit or credit card form
    handleCardDetailsCancel() {
        this.isCardDetailsFormVisible = false;
    }

    // on open payment edit modals
    public onOpenPaymentEditModals() {
        this.store.dispatch(new TogglePaymentEditModal(true));
        this.isPaymentEditModalVisible = true;
    }

    // on close payment edit bank modal
    private onClosePaymentEditBankModal() {
        this.resetBankDetailsForm();
        this.handleBankDetailsCancel();
        this.isPaymentEditModalVisible = false;
        return this.store.dispatch(new TogglePaymentEditModal(false));
    }

    // on close payment edit bank modal
    private onClosePaymentEditCardModal() {
        this.resetCardDetailsForm();
        this.handleCardDetailsCancel();
        this.isPaymentEditModalVisible = false;
        return this.store.dispatch(new TogglePaymentEditModal(false));
    }

    public onSelectPaymentMethod(paymentMethod: PaymentMethodType) {
        this.selectedPaymentMethod = paymentMethod;
        const transactionType =
            paymentMethod === PaymentMethodType.creditCard
                ? BazaNcPurchaseFlowTransactionType.CreditCard
                : BazaNcPurchaseFlowTransactionType.ACH;
        return this.updateTransactionType(transactionType);
    }

    // on submit account details from modal from modal
    public onSubmitAccountDetailsFromModal(accountDetailsType: PaymentMethodType) {
        if (accountDetailsType === PaymentMethodType.bankAccount) {
            this.submitManualBankDetailsForm();
        } else {
            this.submitCardForm();
        }
    }

    public onSelectPaymentMethodAndClose(paymentMethod: PaymentMethodType) {
        return concat(this.onSelectPaymentMethod(paymentMethod), this.store.dispatch(new TogglePaymentEditModal(false)));
    }

    processSubmitManualBankDetailsForm = (form) =>
        this.pms.processSubmitManualBankDetailsForm(form, () => {
            this.store.dispatch(new LoadBankAccount());
            this.onSelectPaymentMethod(PaymentMethodType.bankAccount);
            this.onClosePaymentEditBankModal();
            this.wts.refreshInitData$.next();
        });

    processSubmitCardForm = (form) =>
        this.pms.processSubmitCardForm(form, () => {
            this.store.dispatch(new LoadCreditCard());
            this.onSelectPaymentMethod(PaymentMethodType.creditCard);
            this.onClosePaymentEditCardModal();
            this.wts.refreshInitData$.next();
        });

    public get isPaymentMethodEditVisible() {
        return this.isNCBankAccountAvailable || (this.isCardAvailable && !this.isPurchaseAboveLimit);
    }

    public get purchasePopupTooltipText() {
        return `This is your Payment Method. You can add a new Bank Account or Credit Card or switch between your linked payment methods.`;
    }

    public get manualBankAccountAddMode() {
        return !this.isNCBankAccountAvailable && (!this.isCardAvailable || this.isPurchaseAboveLimit);
    }

    public get manualBankAccountEditMode() {
        return this.isNCBankAccountAvailable && this.isNCBankAccPaymentMethod;
    }

    public get cardAddMode() {
        return !this.isNCBankAccountAvailable && !this.isCardAvailable;
    }

    public get cardEditMode() {
        return this.isCardAvailable && this.isCCPaymentMethod;
    }

    startPurchaseSession() {
        const purchase: PurchaseFlowSessionDto = {
            amount: this.entity?.pricePerShareCents * this.numberOfShares,
            numberOfShares: this.numberOfShares,
            offeringId: this.entity?.offeringId,
            transactionType: this.mapPaymentMethodToTransactionType(this.selectedPaymentMethod),
        };

        this.checkSessionDefaultTransactionType(purchase);
        this.updateSubmitBtnDisableStatus.emit(this.isSubmitBtnDisabled);
        this.purchaseSessionStarted = true;

        this.store
            .dispatch(new StartPurchase(purchase))
            .pipe(
                finalize(() => {
                    this.dataLoaded = true;
                    this.cdr.detectChanges();
                }),
            )
            .subscribe();
    }

    private setDefaultPaymentMethod() {
        this.selectedPaymentMethod = this.isNCBankAccountAvailable
            ? PaymentMethodType.bankAccount
            : this.isCardAvailable
            ? PaymentMethodType.creditCard
            : null;
    }

    private checkSessionDefaultTransactionType(purchase: PurchaseFlowSessionDto) {
        // if CC is selected & purchase is above limit, select ACH/TBD as default transaction type for 1st time session start on payment page (BAZA-1686)
        if (this.pms.isCCSelectedAsPaymentMethod(this.selectedPaymentMethod) && this.isPurchaseAboveLimit) {
            const paymentMethod = PaymentMethodType.bankAccount;
            purchase.transactionType = this.mapPaymentMethodToTransactionType(paymentMethod);
        }
    }

    private mapPaymentMethodToTransactionType(paymentMethod: PaymentMethodType) {
        const mappedType =
            paymentMethod === PaymentMethodType.creditCard
                ? BazaNcPurchaseFlowTransactionType.CreditCard
                : BazaNcPurchaseFlowTransactionType.ACH;

        return mappedType;
    }

    public get isCCPaymentMethod(): boolean {
        return this.selectedPaymentMethod === PaymentMethodType.creditCard;
    }

    public get isNCBankAccPaymentMethod(): boolean {
        return this.selectedPaymentMethod === PaymentMethodType.bankAccount;
    }

    public get isSubmitBtnDisabled() {
        const isDisabled =
            (!this.isNCBankAccountAvailable && !this.isCardAvailable) ||
            (!this.isNCBankAccountAvailable && this.isCardAvailable && this.isPurchaseAboveLimit);

        return isDisabled;
    }
}
