export const NcMethodsEnI18n = {
    title: {
        bank: 'Bank Account',
        card: 'Card',
        generic: 'Payment Method',
    },
    tooltip: {
        caption:
            'This is your Payment Method. You can add a new Bank Account or Credit Card or switch between your linked payment methods.',
        actions: {
            edit: 'Edit',
        },
    },
    bank: {
        info: 'Your bank account will be utilized to receive dividends',
    },
};
