import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzSkeletonModule } from 'ng-zorro-antd/skeleton';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { BazaNcDataAccessModule } from '@scaliolabs/baza-nc-data-access';
import { VerificationStateModule } from '@scaliolabs/baza-nc-web-verification-flow';
import {
    BackLinkModule,
    BankDetailsModule,
    CardDetailsModule,
    IframeIntegrationModule,
    PaymentHeaderModule,
    PersonalInfoModule,
    SummaryModule,
} from '@scaliolabs/baza-web-ui-components';
import { UtilModule } from '@scaliolabs/baza-web-utils';

import { NcPurchaseAgreementComponent } from './agreement/agreement.component';
import { PurchaseStateModule } from './data-access';
import { NcPurchaseDetailsComponent } from './details/details.component';
import { NcPurchaseDoneComponent } from './done/done.component';
import { NcPurchaseMethodsComponent } from './methods/methods.component';
import { NcPurchasePaymentComponent } from './payment/payment.component';
import { NcPurchaseComponent } from './purchase.component';

import {
    NcPaymentBankAccountModalModule,
    NcPaymentBankAccountModule,
    NcPaymentCardModalModule,
    NcPaymentCardModule,
    NcPaymentEditModule,
} from './ui-components';

const NZ_MODULES = [
    NzAlertModule,
    NzButtonModule,
    NzGridModule,
    NzInputNumberModule,
    NzModalModule,
    NzPopoverModule,
    NzSkeletonModule,
    NzStepsModule,
];

@NgModule({
    declarations: [
        NcPurchaseAgreementComponent,
        NcPurchaseComponent,
        NcPurchaseDetailsComponent,
        NcPurchaseDoneComponent,
        NcPurchaseMethodsComponent,
        NcPurchasePaymentComponent,
    ],
    imports: [
        ...NZ_MODULES,
        BackLinkModule,
        BankDetailsModule,
        BazaNcDataAccessModule,
        CardDetailsModule,
        CommonModule,
        FormsModule,
        IframeIntegrationModule,
        NcPaymentBankAccountModalModule,
        NcPaymentBankAccountModule,
        NcPaymentCardModalModule,
        NcPaymentCardModule,
        NcPaymentEditModule,
        PaymentHeaderModule,
        PersonalInfoModule,
        PurchaseStateModule,
        ReactiveFormsModule,
        RouterModule,
        SummaryModule,
        UtilModule,
        VerificationStateModule,
    ],
    exports: [
        NcPurchaseAgreementComponent,
        NcPurchaseComponent,
        NcPurchaseDetailsComponent,
        NcPurchaseDoneComponent,
        NcPurchaseMethodsComponent,
        NcPurchasePaymentComponent,
    ],
})
export class BazaNcWebPurchaseFlowModule {}
