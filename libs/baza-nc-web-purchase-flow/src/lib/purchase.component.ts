import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import { DestroySessionDto } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { skipWhile, take } from 'rxjs';
import { CancelPurchase, PurchaseConfig, PurchaseState } from './data-access';

@UntilDestroy()
@Component({
    selector: 'app-nc-purchase',
    templateUrl: './purchase.component.html',
    styleUrls: ['./purchase.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPurchaseComponent implements OnInit {
    @Input()
    purchaseConfig?: PurchaseConfig;
    defaultConfig?: PurchaseConfig;

    cart$ = this.store.select(PurchaseState.cart);

    public currentTab: number;
    public item: BazaNcIntegrationListingsDto;

    public i18nBasePath = 'ncpf.parent';

    constructor(
        private readonly route: ActivatedRoute,
        private readonly router: Router,
        private readonly store: Store,
        public readonly wts: BazaWebUtilSharedService,
        private readonly notification: NzNotificationService,
    ) {}

    ngOnInit(): void {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;

            if (params.numberOfShares === null) {
                this.notification.warning(this.wts.getI18nLabel(this.i18nBasePath, 'warnings.pickShares'), '');

                this.router.navigate(['/buy-shares', 'details']);
            }
        });

        this.cart$
            .pipe(
                untilDestroyed(this),
                skipWhile((res) => !res),
                take(1),
            )
            .subscribe((response) => {
                this.item = response;
                this.checkPurchaseConfig();
            });
    }

    cancelPurchase() {
        const data: DestroySessionDto = {
            offeringId: this.item.offeringId,
        };
        this.store.dispatch(new CancelPurchase(data));
    }

    checkPurchaseConfig() {
        const defaultConfig: PurchaseConfig = {
            backLink: {
                appLink: {
                    commands: ['/items', this.item?.id?.toString()],
                },
            },
        };

        this.purchaseConfig = this.wts.mergeConfig(defaultConfig, this.purchaseConfig);
    }
}
