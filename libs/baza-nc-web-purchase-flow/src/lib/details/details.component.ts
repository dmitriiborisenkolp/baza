import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    AccountVerificationPersonalInformationDto,
    BazaNcBootstrapDto,
    BazaNcPurchaseFlowTransactionType,
    DestroySessionDto,
    PurchaseFlowDto,
    PurchaseFlowSessionDto,
    StatsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { Observable, combineLatest, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, skipWhile, switchMap, take, withLatestFrom } from 'rxjs/operators';
import { CancelPurchase, GetPurchaseStats, PatchStartPurchase, PurchaseState, StartPurchase } from '../data-access';
import { PaymentMethodsService } from '../data-access/services/nc-shared.service';

@UntilDestroy()
@Component({
    selector: 'app-nc-purchase-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NcPurchaseDetailsComponent implements OnInit {
    @Input()
    initData: BazaNcBootstrapDto;

    @Input()
    personal$: Observable<AccountVerificationPersonalInformationDto>;

    cart$ = this.store.select(PurchaseState.cart);
    stats$ = this.store.select(PurchaseState.stats);
    limitsForPurchase$ = this.store.select(PurchaseState.limitsForPurchase);
    numberOfShares$ = this.store.select(PurchaseState.numberOfShares);
    purchaseStart$ = this.store.select(PurchaseState.purchaseStart);

    numberOfShares: number;
    stats: StatsDto;
    numberOfSharesMin: number;
    statsLoaded = false;
    public shareMapping: { [k: string]: string } = {
        '=1': '# share',
        other: '# shares',
    };

    public i18nBasePath = 'ncpf.details';
    kycStatus: string;

    constructor(
        private readonly paymentService: PaymentMethodsService,
        private readonly router: Router,
        private readonly store: Store,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    ngOnInit(): void {
        this.statsLoaded = false;
        this.destroyPreviousSessions();
        this.kycStatus = this.initData?.investorAccount?.status.nc;

        const result$ = combineLatest([this.numberOfShares$, this.cart$]).pipe(
            untilDestroyed(this),
            map(([number, entity]) => ({
                number,
                entity,
            })),
            distinctUntilChanged(),
            debounceTime(0),
            skipWhile((pair) => !pair || !pair.entity),
            switchMap((pair) => {
                const priceAmount = pair.entity.pricePerShareCents * pair.entity.numSharesMin;

                this.numberOfShares = this.paymentService.getMinNumberOfSharesForEntity(pair.number, pair.entity);
                this.numberOfSharesMin = pair.entity.numSharesMin;

                if (!this.statsLoaded) {
                    return this.store.dispatch(new GetPurchaseStats(pair.entity.offeringId, priceAmount)).pipe(withLatestFrom(this.stats$));
                }

                return of([]);
            }),
        );

        result$.pipe(untilDestroyed(this)).subscribe(([, res]) => {
            if (res) {
                this.stats = res;
                this.statsLoaded = true;
            }
        });
    }

    public decreaseShares(): void {
        if (this.numberOfShares - 1 >= 1) {
            this.numberOfShares--;
        }
    }

    public increaseShares(): void {
        if (this.numberOfSharesMin > +this.numberOfShares + 1) {
            this.numberOfShares = this.numberOfSharesMin;
        } else if (this.numberOfShares + 1 <= this.stats.canPurchase) {
            this.numberOfShares++;
        }
    }

    // onEditPersonalInfo
    public onEditPersonalInfo(): void {
        this.store.dispatch(
            new PatchStartPurchase({
                numberOfShares: this.numberOfShares,
            } as PurchaseFlowDto),
        );

        this.router.navigate(['/verification', 'info']);
    }

    // onFormSubmit
    public onFormSubmit(entity: BazaNcIntegrationListingsDto): void {
        const purchase: PurchaseFlowSessionDto = {
            amount: this.numberOfShares * entity.pricePerShareCents,
            numberOfShares: this.numberOfShares,
            offeringId: entity.offeringId,
            requestDocuSignUrl: true,
            transactionType: BazaNcPurchaseFlowTransactionType.ACH,
        };

        this.store
            .dispatch(new StartPurchase(purchase))
            .pipe(withLatestFrom(this.purchaseStart$))
            .subscribe(([, res]) => {
                this.store.dispatch(new PatchStartPurchase(res));
                this.router.navigate(['/buy-shares', 'agreement']);
            });
    }

    checkNumberOfShares(minNumber: number, maxNumber: number): void {
        if (this.numberOfShares < minNumber) {
            this.numberOfShares = minNumber;
        } else if (this.numberOfShares > maxNumber) {
            this.numberOfShares = maxNumber;
        }
    }

    destroyPreviousSessions() {
        this.cart$
            .pipe(
                untilDestroyed(this),
                skipWhile((res) => !res),
                take(1),
            )
            .subscribe((item) => {
                if (item) {
                    const data: DestroySessionDto = {
                        offeringId: item.offeringId,
                    };
                    this.store.dispatch(new CancelPurchase(data));
                }
            });
    }
}
