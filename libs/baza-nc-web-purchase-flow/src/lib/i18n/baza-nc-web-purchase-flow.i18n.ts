import { NcAgreementEnI18n } from '../agreement/i18n/agreement.i18n';
import { NcDetailsEnI18n } from '../details/i18n/details.i18n';
import { NcDoneEnI18n } from '../done/i18n/done.i18n';
import { NcMethodsEnI18n } from '../methods/i18n/methods.i18n';
import { NcPaymentEnI18n } from '../payment/i18n/payment.i18n';
import { NcPaymentEditEnI18n } from '../ui-components/payment-edit/i18n/payment-edit.i18n';
import { NcPaymentMethodsEnI18n } from '../ui-components/payment-methods/i18n/payment-methods.i18n';

export const BazaNcWebPFEnI18n = {
    ncpf: {
        parent: {
            backlink: {
                text: 'Back to Listing Details',
                confirmation: {
                    title: 'Are you sure you want to exit?',
                    text: 'Your purchase will not be completed if you leave this page.',
                },
            },
            steps: {
                detailsLabel: 'Purchase Details',
                agreementLabel: 'Sign Agreement',
                paymentLabel: 'Submit Payment',
            },
            warnings: {
                pickShares: 'Please pick number of shares',
            },
        },
        agreement: NcAgreementEnI18n,
        details: NcDetailsEnI18n,
        done: NcDoneEnI18n,
        methods: NcMethodsEnI18n,
        payment: NcPaymentEnI18n,
        pymtEdit: NcPaymentEditEnI18n,
        pymtMethods: NcPaymentMethodsEnI18n,
        notifications: {
            cancel_purchase_fail: 'There was an error cancelling the Purchase.',
            load_bank_account_fail: 'Failed to load Bank Account information',
            load_credit_card_fail: 'Failed to load Credit Card information',
            load_plaid_link_fail: 'Failed to load Plaid link',
            save_bank_account_fail: 'Failed to save Bank Account information',
            save_credit_card_fail: 'Failed to save Credit Card information',
            set_manual_bank_account_fail: 'There was an error setting the Manual Bank Account.',
            set_manual_bank_account_success: 'Manual Bank Account was successfully set',
            submit_purchase_fail: 'There was an error submitting the Purchase.',
            submit_purchase_success: 'Purchase successfully submitted',
            purchase_start_fail: 'Could not start the session',
            purchase_reprocess_success: 'Payment successfully re-submitted',
            purchase_reprocess_fail: 'Payment retry request failed. Please contact your bank for further details',
        },
    },
};
