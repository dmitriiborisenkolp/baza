import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { JwtService } from '@scaliolabs/baza-core-ng';
import {
    BazaNcPurchaseFlowBankAccountDataAccess,
    BazaNcPurchaseFlowCreditCardDataAccess,
    BazaNcPurchaseFlowDataAccess,
    BazaNcPurchaseFlowLimitsDataAccess,
} from '@scaliolabs/baza-nc-data-access';
import { BazaNcIntegrationListingsDataAccess } from '@scaliolabs/baza-nc-integration-data-access';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcCreditCardDto,
    BazaNcLimitsDto,
    BazaNcPurchaseFlowGetCreditCardResponse,
    BazaNcPurchaseFlowLimitsForPurchaseResponse,
    PurchaseFlowBankAccountDto,
    PurchaseFlowDestroyResponse,
    PurchaseFlowDto,
    PurchaseFlowPlaidLinkDto,
    PurchaseFlowSubmitResponse,
    ReProcessPaymentResponse,
    StatsDto,
} from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService, EffectsUtil, StorageService } from '@scaliolabs/baza-web-utils';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import {
    CancelPurchase,
    ClearPurchaseState,
    DeleteStartPurchase,
    GetLimits,
    GetLimitsForPurchase,
    GetPurchaseStats,
    LoadBankAccount,
    LoadCreditCard,
    LoadPlaidLink,
    PatchStartPurchase,
    ReprocessPayment,
    SaveBankAccount,
    SaveCreditCard,
    SelectEntity,
    StartCart,
    StartPurchase,
    SubmitPurchase,
    TogglePaymentEditModal,
} from './actions';

export interface PurchaseStateModel {
    cart: BazaNcIntegrationListingsDto | null;
    purchaseStart: PurchaseFlowDto | null;
    stats: StatsDto | null;
    numberOfShares: number | null;
    limits: BazaNcLimitsDto;
    limitForPurchase: BazaNcPurchaseFlowLimitsForPurchaseResponse;
    isPaymentEditModalOpen: boolean;
    creditCard: BazaNcPurchaseFlowGetCreditCardResponse;
    bankAccount: PurchaseFlowBankAccountDto;
    plaidLink: string;
}

const initState = {
    name: 'purchase',
    defaults: {
        bankAccount: null,
        cart: undefined,
        creditCard: null,
        isPaymentEditModalOpen: false,
        limitForPurchase: null,
        limits: null,
        numberOfShares: null,
        plaidLink: null,
        purchaseStart: null,
        stats: null,
    },
};

const i18nBasePath = 'ncpf.notifications';

@State<PurchaseStateModel>(initState)
@Injectable()
export class PurchaseState {
    constructor(
        private readonly bankAccountDataAccessService: BazaNcPurchaseFlowBankAccountDataAccess,
        private readonly creditCardDataAccessService: BazaNcPurchaseFlowCreditCardDataAccess,
        private readonly dataAccess: BazaNcPurchaseFlowDataAccess,
        private readonly effectsUtil: EffectsUtil,
        private readonly jwtService: JwtService,
        private readonly limitsDataAccess: BazaNcPurchaseFlowLimitsDataAccess,
        private readonly storageService: StorageService,
        private readonly itemDataAccess: BazaNcIntegrationListingsDataAccess,
        private readonly wts: BazaWebUtilSharedService,
    ) {}

    @Selector()
    static bankAccount(state: PurchaseStateModel): PurchaseFlowBankAccountDto {
        return state.bankAccount;
    }

    @Selector()
    static cart(state: PurchaseStateModel): BazaNcIntegrationListingsDto {
        return state.cart;
    }

    @Selector()
    static creditCard(state: PurchaseStateModel): BazaNcPurchaseFlowGetCreditCardResponse {
        return state.creditCard;
    }

    @Selector()
    static isPaymentEditModalOpen(state: PurchaseStateModel): boolean {
        return state.isPaymentEditModalOpen;
    }

    @Selector()
    static limits(state: PurchaseStateModel): BazaNcLimitsDto {
        return state.limits;
    }

    @Selector()
    static limitsForPurchase(state: PurchaseStateModel): BazaNcPurchaseFlowLimitsForPurchaseResponse {
        return state.limitForPurchase;
    }

    @Selector()
    static numberOfShares(state: PurchaseStateModel): number {
        return state.numberOfShares;
    }

    @Selector()
    static plaidLink(state: PurchaseStateModel): string {
        return state.plaidLink;
    }

    @Selector()
    static purchaseStart(state: PurchaseStateModel): PurchaseFlowDto {
        return state.purchaseStart;
    }

    @Selector()
    static stats(state: PurchaseStateModel): StatsDto {
        return state.stats;
    }

    @Action(GetPurchaseStats, { cancelUncompleted: true })
    getPurchaseStats(ctx: StateContext<PurchaseStateModel>, action: GetPurchaseStats): Observable<StatsDto> {
        return this.dataAccess
            .stats({
                offeringId: action.offeringId,
                requestedAmountCents: action.requestedAmountCents,
                withReservedShares: false,
                withReservedByOtherUsersShares: false,
            })
            .pipe(
                tap((response: StatsDto) => {
                    ctx.patchState({ stats: response });
                }),
                this.effectsUtil.tryCatchNone$(),
            );
    }

    @Action(CancelPurchase, { cancelUncompleted: true })
    cancelPurchase(ctx: StateContext<PurchaseStateModel>, action: CancelPurchase): Observable<PurchaseFlowDestroyResponse> {
        return this.dataAccess.destroy(action.data).pipe(
            tap(() => ctx.patchState({ purchaseStart: null })),
            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'cancel_purchase_fail')),
        );
    }

    @Action(ClearPurchaseState, { cancelUncompleted: true })
    cleanState(ctx: StateContext<PurchaseStateModel>): void {
        ctx.setState(initState.defaults);
    }

    @Action(GetLimits, { cancelUncompleted: true })
    getLimits(ctx: StateContext<PurchaseStateModel>): Observable<BazaNcLimitsDto> {
        return this.limitsDataAccess.limits().pipe(
            tap((response: BazaNcLimitsDto) => {
                ctx.patchState({ limits: response });
            }),
            this.effectsUtil.tryCatchNone$(),
        );
    }

    @Action(GetLimitsForPurchase, { cancelUncompleted: true })
    getLimitsForPurchase(
        ctx: StateContext<PurchaseStateModel>,
        { requestedAmountCents, offeringId },
    ): Observable<BazaNcPurchaseFlowLimitsForPurchaseResponse> {
        return this.limitsDataAccess.limitsForPurchase({ requestedAmountCents, offeringId }).pipe(
            tap((response: BazaNcPurchaseFlowLimitsForPurchaseResponse) => {
                ctx.patchState({ limitForPurchase: response });
            }),
            this.effectsUtil.tryCatchNone$(),
        );
    }

    @Action(LoadBankAccount, { cancelUncompleted: true })
    loadBankAccount(ctx: StateContext<PurchaseStateModel>): Observable<PurchaseFlowBankAccountDto> {
        return this.bankAccountDataAccessService.getBankAccount().pipe(
            tap((response: PurchaseFlowBankAccountDto) => {
                ctx.patchState({ bankAccount: response });
            }),
            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_bank_account_fail')),
        );
    }

    @Action(LoadCreditCard, { cancelUncompleted: true })
    loadCreditCard(ctx: StateContext<PurchaseStateModel>): Observable<BazaNcPurchaseFlowGetCreditCardResponse> {
        return this.creditCardDataAccessService.getCreditCard().pipe(
            tap((response: BazaNcPurchaseFlowGetCreditCardResponse) => {
                ctx.patchState({ creditCard: response });
            }),

            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_credit_card_fail')),
        );
    }

    @Action(LoadPlaidLink, { cancelUncompleted: true })
    loadPlaidLink(ctx: StateContext<PurchaseStateModel>): Observable<PurchaseFlowPlaidLinkDto> {
        return this.bankAccountDataAccessService.getPlaidLink().pipe(
            tap((response: PurchaseFlowPlaidLinkDto) => {
                ctx.patchState({ plaidLink: response.link });
            }),
            this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'load_plaid_link_fail')),
        );
    }

    @Action(PatchStartPurchase, { cancelUncompleted: true })
    patchStartPurchase(ctx: StateContext<PurchaseStateModel>, action: PatchStartPurchase): void {
        ctx.patchState({ numberOfShares: action.purchaseStart.numberOfShares });

        const purchaseStart = { ...ctx.getState().purchaseStart, ...action.purchaseStart };
        ctx.patchState({ purchaseStart: purchaseStart });
    }

    @Action(DeleteStartPurchase, { cancelUncompleted: true })
    deleteStartPurchase(ctx: StateContext<PurchaseStateModel>, action: DeleteStartPurchase): void {
        ctx.patchState({ numberOfShares: action.numberOfShares });
    }

    @Action(SaveBankAccount, { cancelUncompleted: true })
    saveBankAccount(ctx: StateContext<PurchaseStateModel>, { srcAccount }): Observable<PurchaseFlowBankAccountDto> {
        return this.bankAccountDataAccessService
            .setBankAccount(srcAccount)
            .pipe(this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'save_bank_account_fail')));
    }

    @Action(SaveCreditCard, { cancelUncompleted: true })
    saveCreditCard(ctx: StateContext<PurchaseStateModel>, { srcCard }): Observable<BazaNcCreditCardDto> {
        return this.creditCardDataAccessService
            .setCreditCard(srcCard)
            .pipe(this.effectsUtil.tryCatchError$(this.wts.getI18nLabel(i18nBasePath, 'save_credit_card_fail')));
    }

    @Action(SelectEntity, { cancelUncompleted: true })
    selectEntity(ctx: StateContext<PurchaseStateModel>, action: SelectEntity): Observable<BazaNcIntegrationListingsDto> {
        return this.itemDataAccess.getById({ id: action.entityId }).pipe(
            tap((response: BazaNcIntegrationListingsDto) => {
                this.storageService.setObject('cart', action.entityId);

                return ctx.patchState({ cart: response });
            }),
        );
    }

    @Action(StartCart, { cancelUncompleted: true })
    startCart(ctx: StateContext<PurchaseStateModel>): void {
        const entityId = this.storageService.getObject<string>('cart') || null;

        if (this.jwtService.hasJwt() && entityId !== null) {
            ctx.dispatch(new SelectEntity(Number(entityId)));
        } else {
            ctx.patchState({ cart: null });
        }
    }

    @Action(StartPurchase, { cancelUncompleted: true })
    startPurchase(ctx: StateContext<PurchaseStateModel>, action: StartPurchase): Observable<PurchaseFlowDto> {
        ctx.patchState({ numberOfShares: action.data?.numberOfShares });

        return this.dataAccess.session(action.data).pipe(
            tap((response: PurchaseFlowDto) => {
                ctx.patchState({ purchaseStart: response });
            }),
            this.effectsUtil.tryCatchError$(''),
        );
    }

    @Action(SubmitPurchase, { cancelUncompleted: true })
    submitPurchase(ctx: StateContext<PurchaseStateModel>, action: SubmitPurchase): Observable<PurchaseFlowSubmitResponse> {
        return this.dataAccess
            .submit({ id: action.tradeId })
            .pipe(this.effectsUtil.tryCatch$(this.wts.getI18nLabel(i18nBasePath, 'submit_purchase_success'), ''));
    }

    @Action(TogglePaymentEditModal, { cancelUncompleted: true })
    togglePaymentEditModal(ctx: StateContext<PurchaseStateModel>, action: TogglePaymentEditModal): void {
        ctx.patchState({ isPaymentEditModalOpen: action.isOpen });
    }

    @Action(ReprocessPayment, { cancelUncompleted: true })
    reprocessPayment(ctx: StateContext<PurchaseStateModel>, action: ReprocessPayment): Observable<ReProcessPaymentResponse> {
        return this.dataAccess
            .reprocessPayment(action.data)
            .pipe(
                this.effectsUtil.tryCatch$(
                    this.wts.getI18nLabel(i18nBasePath, 'purchase_reprocess_success'),
                    this.wts.getI18nLabel(i18nBasePath, 'purchase_reprocess_fail'),
                ),
            );
    }
}
