import {
    BazaNcPurchaseFlowSetCreditCardRequest,
    DestroySessionDto,
    PurchaseFlowDto,
    PurchaseFlowSessionDto,
    PurchaseFlowSetBankAccountDetailsDto,
    ReProcessPaymentDto,
} from '@scaliolabs/baza-nc-shared';
import { PaymentMethodType } from '../models';

export class CancelPurchase {
    static readonly type = '[Purchase] Cancel Purchase';

    constructor(public data: DestroySessionDto) {}
}

export class ClearPurchaseState {
    static readonly type = '[Purchase] ClearPurchaseState';
}

export class GetLimits {
    static readonly type = '[Purchase] GetLimits';
}

export class GetLimitsForPurchase {
    static readonly type = '[Purchase] GetLimitsForPurchase';

    constructor(public requestedAmountCents: number, public offeringId: string) {}
}

export class GetPurchaseStats {
    static readonly type = '[Purchase] Get Purchase Stats';

    constructor(public offeringId: string, public requestedAmountCents: number) {}
}

export class LoadBankAccount {
    static readonly type = '[Purchase] LoadBankAccount';
}

export class LoadCreditCard {
    static readonly type = '[Purchase] LoadCreditCard';
}

export class LoadPlaidLink {
    static readonly type = '[Purchase] LoadPlaidLink';
}

export class PatchStartPurchase {
    static readonly type = '[Purchase] Patch StartPurchase';

    constructor(public purchaseStart: PurchaseFlowDto) {}
}

export class DeleteStartPurchase {
    static readonly type = '[Purchase] Delete NumberOfShares';

    constructor(public numberOfShares: null) {}
}

export class SaveBankAccount {
    static readonly type = '[Purchase] SaveBankAccount';

    constructor(public srcAccount: PurchaseFlowSetBankAccountDetailsDto) {}
}

export class SaveCreditCard {
    static readonly type = '[Purchase] SaveCreditCard';

    constructor(public srcCard: BazaNcPurchaseFlowSetCreditCardRequest) {}
}

export class SelectEntity {
    static readonly type = '[Purchase] Select Entity';

    constructor(public entityId: number) {}
}

export class StartCart {
    static readonly type = '[Purchase] Start Cart';
}

export class StartPurchase {
    static readonly type = '[Purchase] Start Purchase';

    constructor(public data: PurchaseFlowSessionDto) {}
}

export class SubmitPurchase {
    static readonly type = '[Purchase] Submit Purchase';

    constructor(public tradeId: string) {}
}

export class TogglePaymentEditModal {
    static readonly type = '[Purchase] TogglePaymentEditModal';

    constructor(public isOpen: boolean) {}
}
export class SetSelectedPaymentMethod {
    static readonly type = '[Purchase] DwollaSetSelectedPaymentMethod';

    constructor(public selectedPaymentMethod: PaymentMethodType) {}
}

export class ReprocessPayment {
    static readonly type = '[Purchase] Reprocess Payment';

    constructor(public data: ReProcessPaymentDto) {}
}
