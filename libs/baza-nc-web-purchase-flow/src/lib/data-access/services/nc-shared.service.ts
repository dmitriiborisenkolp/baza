import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaNcIntegrationListingsDto } from '@scaliolabs/baza-nc-integration-shared';
import {
    BazaNcBankAccountAchDto,
    BazaNcBootstrapDto,
    BazaNcCreditCardDto,
    BazaNcPurchaseFlowGetCreditCardResponse,
    BazaNcPurchaseFlowSetCreditCardRequest,
    PurchaseFlowSetBankAccountDetailsDto,
} from '@scaliolabs/baza-nc-shared';
import { IframeIntegrationComponent } from '@scaliolabs/baza-web-ui-components';
import { EffectsUtil, onlyNumbersValidator, restrictedCharsValidator, routingNumberValidator } from '@scaliolabs/baza-web-utils';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PaymentMethodType, CardDetailsForm, BankAccountForm } from '../models';
import { SaveCreditCard, SaveBankAccount, LoadPlaidLink } from '../store';

@UntilDestroy()
@Injectable({
    providedIn: 'root',
})
export class PaymentMethodsService {
    constructor(
        private fb: FormBuilder,
        private modalService: NzModalService,
        private readonly store: Store,
        private readonly notification: NzNotificationService,
        private readonly effectsUtil: EffectsUtil,
    ) {}

    // #region Utility functions
    isNCBankAccountAvailable(source: BazaNcBootstrapDto | BazaNcBankAccountAchDto): boolean {
        if (!source || typeof source !== 'object') {
            return false;
        }

        if ('investorAccount' in source) {
            return (source as BazaNcBootstrapDto)?.investorAccount?.isBankAccountNcAchLinked ?? false;
        } else {
            return (source as BazaNcBankAccountAchDto)?.isAvailable ?? false;
        }
    }

    isCardAvailable(source: BazaNcBootstrapDto | BazaNcPurchaseFlowGetCreditCardResponse): boolean {
        if (!source || typeof source !== 'object') {
            return false;
        }

        if ('investorAccount' in source) {
            return (source as BazaNcBootstrapDto)?.investorAccount?.isCreditCardLinked ?? false;
        } else {
            return (source as BazaNcPurchaseFlowGetCreditCardResponse)?.isAvailable ?? false;
        }
    }

    isNCBankAccSelectedAsPaymentMethod(selectedPaymentMethod: PaymentMethodType): boolean {
        return selectedPaymentMethod === PaymentMethodType.bankAccount;
    }

    isCCSelectedAsPaymentMethod(selectedPaymentMethod: PaymentMethodType): boolean {
        return selectedPaymentMethod === PaymentMethodType.creditCard;
    }

    getMinNumberOfSharesForEntity(number: number, entity: BazaNcIntegrationListingsDto) {
        let numberOfShares;
        if (entity?.numSharesMin && entity?.numSharesMin > number) {
            numberOfShares = entity.numSharesMin;
        } else {
            numberOfShares = number || entity.numSharesMin;
        }
        return numberOfShares;
    }
    // #endregion

    // #region Forms initialization
    public generateCreditCardForm = (): CardDetailsForm =>
        this.fb.group({
            creditCardholderName: [null, Validators.compose([Validators.required, restrictedCharsValidator({ bypassChars: ['-'] })])],
            creditCardNumber: [null, Validators.compose([Validators.required, Validators.minLength(16), onlyNumbersValidator()])],
            expireDate: [null, [Validators.required]],
            creditCardCvv: [
                null,
                Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(4), onlyNumbersValidator()]),
            ],
        }) as CardDetailsForm;

    public generateBankDetailsForm = (): BankAccountForm =>
        this.fb.group({
            accountName: [null, Validators.compose([Validators.required, restrictedCharsValidator({ bypassChars: ['-'] })])],
            accountType: [null, [Validators.required]],
            accountNumber: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            accountRoutingNumber: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(9),
                    Validators.maxLength(9),
                    onlyNumbersValidator(),
                    routingNumberValidator(),
                ]),
            ],
        }) as BankAccountForm;

    // #endregion

    // #region Forms submission
    public submitCardForm = (cardDetailsForm: FormGroup): Observable<BazaNcCreditCardDto> => {
        if (!cardDetailsForm) {
            throw new Error('Source parameter is not defined: CardDetailsForm');
        }

        const expireDate = cardDetailsForm.get('expireDate').value.split('/');
        const month = parseInt(expireDate[0], 10),
            year = parseInt(expireDate[1], 10);
        const data: BazaNcPurchaseFlowSetCreditCardRequest = {
            creditCardholderName: cardDetailsForm.get('creditCardholderName').value,
            creditCardNumber: cardDetailsForm.get('creditCardNumber').value,
            creditCardCvv: cardDetailsForm.get('creditCardCvv').value,
            creditCardExpireMonth: month,
            creditCardExpireYear: year,
        };
        return this.store.dispatch(new SaveCreditCard(data));
    };

    processSubmitCardForm = (srcForm: FormGroup, onSuccess) => {
        this.updateCardFormValidity(srcForm);
        if (srcForm) {
            this.submitCardForm(srcForm)
                .pipe(this.effectsUtil.tryCatchNoRethrow$('', ''))
                .subscribe(() => {
                    onSuccess();
                });
        }
    };

    public submitManualBankDetailsForm = (bankDetailsForm: FormGroup) => {
        if (!bankDetailsForm) {
            throw new Error('Source parameter is not defined: BankAccountForm');
        }

        const data: PurchaseFlowSetBankAccountDetailsDto = {
            ...bankDetailsForm.value,
        };
        return this.store.dispatch(new SaveBankAccount(data));
    };

    processSubmitManualBankDetailsForm = (srcForm: FormGroup, onSuccess) => {
        this.updateBankAccountFormValidity(srcForm);
        if (srcForm.valid) {
            this.submitManualBankDetailsForm(srcForm)
                .pipe(this.effectsUtil.tryCatchNoRethrow$('', ''))
                .subscribe(() => {
                    onSuccess();
                });
        }
    };
    // #endregion

    // #region Forms validity checks
    public updateCardFormValidity = (cardDetailsForm: FormGroup): void => {
        for (const key of Object.keys(cardDetailsForm?.controls)) {
            cardDetailsForm.controls[`${key}`].markAsDirty();
            cardDetailsForm.controls[`${key}`].updateValueAndValidity();
        }
    };

    public updateBankAccountFormValidity = (bankDetailsForm: FormGroup): void => {
        for (const key of Object.keys(bankDetailsForm?.controls)) {
            bankDetailsForm.controls[`${key}`].markAsDirty();
            bankDetailsForm.controls[`${key}`].updateValueAndValidity();
        }
    };
    // #endregion

    // #region Plaid utilities
    public getPlaidLink = () => {
        return this.store.dispatch(new LoadPlaidLink());
    };

    processBankLink = (link: string) => {
        return this.modalService.create({
            nzClassName: 'ant-modal-transparent',
            nzClosable: false,
            nzContent: IframeIntegrationComponent,
            nzComponentParams: {
                events: [],
                height: '633px',
                url: link,
                width: '360px',
            } as IframeIntegrationComponent<string>,
            nzFooter: null,
            nzWidth: 360,
        });
    };

    public processPlaidDialog(onSuccess: () => void) {
        this.getPlaidLink()
            .pipe(
                map(({ purchase }) => {
                    const modalRef = this.processBankLink(purchase.plaidLink);

                    modalRef.afterClose.pipe(untilDestroyed(this)).subscribe();

                    modalRef.componentInstance.iframeEvent.pipe(untilDestroyed(this)).subscribe((result) => {
                        const jsonRes = JSON.parse(result);
                        try {
                            if (Object.keys(jsonRes).includes('statusCode') && jsonRes['statusCode'] === '101') {
                                onSuccess();
                            } else if (jsonRes['error']) {
                                this.notification.error('Could not link payment', '');
                            }
                        } catch (err) {
                            if (jsonRes['error']) {
                                this.notification.error('Could not link the payment', '');
                            }
                        }
                        modalRef.close();
                    });
                }),
                untilDestroyed(this),
            )
            .subscribe();
    }

    // #endregion
}
