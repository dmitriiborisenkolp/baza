/**
 * Interface for configuring the payment method
 */
export interface PaymentMethodConfig {
    /**
     * Indicates whether to show the purchase dividend message or not
     */
    showPurchaseDividendMessage?: boolean;

    /**
     * The config for the payment method options
     */
    methodOptionsConfig?: PaymentMethodOptionsConfig;
}

/**
 * Interface for configuring the payment method options
 */
export interface PaymentMethodOptionsConfig {
    /**
     * The config for bank account
     */
    bankAccount?: PaymentBankAccountConfig;

    /**
     * The config for credit card
     */
    creditCard?: PaymentCreditCardConfig;
}

/**
 * Interface for configuring the payment bank account
 */
export interface PaymentBankAccountConfig {
    /**
     * The config for plaid linking
     */
    plaid?: {
        title?: string;
        order?: number;
    };

    /**
     * The config for manual linking
     */
    manual?: {
        title?: string;
        order?: number;
    };

    /**
     * The order for bank account in relation to other payment options
     */
    order?: number;
}

/**
 * Interface for configuring the payment credit card
 */
export interface PaymentCreditCardConfig {
    /**
     * The config for manual linking
     */
    manual?: {
        title?: string;
        feeCaption?: string;
    };

    /**
     * The order for credit card in relation to other payment options
     */
    order?: number;
}

/**
 * Interface for configuring the payment steps
 */
export interface PaymentStepsConfig {
    /**
     * The link for the terms of service
     */
    termsOfServicesLink?: string;

    /**
     * The link for the EFT disclosure
     */
    eftDisclosureLink?: string;
}

/**
 * Interface for configuring the payment
 */
export interface PaymentConfig {
    /**
     * The config for the payment method
     */
    paymentMethodConfig?: PaymentMethodConfig;

    /**
     * The config for the payment steps
     */
    paymentStepsConfig?: PaymentStepsConfig;
}
