export enum PaymentEditModalsContentType {
    paymentAdd = 'paymentAdd',
    paymentList = 'paymentList',
    paymentUpdate = 'paymentUpdate',
    paymentUpdateAccount = 'paymentUpdateAccount',
    paymentUpdateDetails = 'paymentUpdateDetails',
}

export enum PaymentMethodType {
    bankAccount = 'bankAccount',
    creditCard = 'creditCard',
}

export enum PaymentUpdateAccountType {
    creditCard = 'creditCard',
    linkBankAccount = 'linkBankAccount',
    manuallyAddBankAccount = 'manuallyAddBankAccount',
}
