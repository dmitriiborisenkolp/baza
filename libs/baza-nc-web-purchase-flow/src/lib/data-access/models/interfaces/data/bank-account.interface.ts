export interface BankAccount {
    /**
     *  Represents whether the bank account is available or not.
     */
    isAvailable: boolean;

    /**
     * Additional details of the bank account
     * @see BankAccountDetails
     */
    details?: BankAccountDetails;
}

export interface BankAccountDetails {
    /*
     * The name of the account holder
     */
    accountName: string;

    /**
     * A nickname for the account
     */
    accountNickName: string;

    /**
     * The routing number for the account
     */
    accountRoutingNumber: string;

    /**
     * The account number
     */
    accountNumber: string;

    /**
     * The type of account (e.g. "checking" or "savings")
     */
    accountType: string;
}
