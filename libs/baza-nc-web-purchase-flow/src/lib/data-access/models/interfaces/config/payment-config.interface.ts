import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * PaymentMethodConfig interface for payment method/step component configuration
 */
export interface PaymentMethodConfig {
    /**
     * Show/hide purchase dividend message
     */
    showPurchaseDividendMessage?: boolean;
}

/**
 * PaymentStepsConfig interface for configuration payment steps links
 */
export interface PaymentStepsConfig {
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    termsOfServicesLink?: LinkConfig;
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    eftDisclosureLink?: LinkConfig;
}

/**
 * PaymentConfig interface for payment method component configuration
 */
export interface PaymentConfig {
    /**
     * Method configuration
     */
    paymentMethodConfig?: PaymentMethodConfig;
    /**
     * Step configuration
     */
    paymentStepsConfig?: PaymentStepsConfig;
}
