import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * PurchaseConfig interface for configuration purchase links
 */
export interface PurchaseConfig {
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    backLink?: LinkConfig;
}
