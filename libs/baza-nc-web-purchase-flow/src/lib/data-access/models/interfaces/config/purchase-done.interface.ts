import { LinkConfig } from '@scaliolabs/baza-web-utils';

/**
 * PurchaseDoneConfig interface for configuration purchase done component
 */
export interface PurchaseDoneConfig {
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    buttonLink?: LinkConfig;
    /**
     * Navigation object used for internal or external link configuration
     * @see LinkConfig
     */
    ctaLink?: LinkConfig;
}
