import { AfterViewInit, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { BazaRegistryNgService } from '@scaliolabs/baza-core-ng';
import { PurchaseFlowDto } from '@scaliolabs/baza-nc-shared';
import { BazaLinkUtilSharedService, BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { DeleteStartPurchase, PatchStartPurchase, PurchaseDoneConfig, PurchaseState } from '../data-access';

@UntilDestroy()
@Component({
    selector: 'app-nc-purchase-done',
    templateUrl: './done.component.html',
    styleUrls: ['./done.component.less'],
})
export class NcPurchaseDoneComponent implements OnInit, AfterViewInit, OnDestroy {
    cart$ = this.store.select(PurchaseState.cart);
    numberOfShares$ = this.store.select(PurchaseState.numberOfShares);
    purchaseStart$ = this.store.select(PurchaseState.purchaseStart);

    @Input()
    purchaseDoneConfig?: PurchaseDoneConfig;

    private numberOfShares: number;
    private purchaseStart: PurchaseFlowDto;

    public i18nBasePath = 'ncpf.done';

    constructor(
        private readonly registry: BazaRegistryNgService,
        private readonly cdr: ChangeDetectorRef,
        private readonly store: Store,
        public readonly uts: BazaLinkUtilSharedService,
        public readonly wts: BazaWebUtilSharedService,
    ) {}

    ngOnInit() {
        this.checkDefaultConfig();

        this.numberOfShares$.pipe(untilDestroyed(this)).subscribe((numberOfShares) => {
            this.numberOfShares = numberOfShares;
        });

        this.purchaseStart$.pipe(untilDestroyed(this)).subscribe((purchaseStart) => {
            if (purchaseStart) {
                this.purchaseStart = purchaseStart;
                this.cdr.detectChanges();
            }
        });
    }

    ngAfterViewInit() {
        this.store.dispatch(new DeleteStartPurchase(null));
    }

    ngOnDestroy() {
        this.store.dispatch(new PatchStartPurchase({ fee: null } as PurchaseFlowDto));
    }

    checkDefaultConfig() {
        const defaultConfig: PurchaseDoneConfig = {
            buttonLink: {
                appLink: {
                    commands: ['/portfolio'],
                },
            },
            ctaLink: {
                appLink: {
                    commands: ['', { outlets: { modal: ['contact-us'] } }],
                },
            },
        };

        this.purchaseDoneConfig = this.wts.mergeConfig(defaultConfig, this.purchaseDoneConfig);
    }
}
