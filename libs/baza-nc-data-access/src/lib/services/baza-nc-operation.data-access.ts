import { Injectable } from '@angular/core';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import {
    BazaNcOperationEndpoint,
    BazaNcOperationEndpointPaths,
    BazaNcOperationListRequest,
    BazaNcOperationListResponse,
    BazaNcOperationStatsResponse,
} from '@scaliolabs/baza-nc-shared';
import { Observable } from 'rxjs';

/**
 * Data-Access Service for Baza NC Operations
 */
@Injectable()
export class BazaNcOperationDataAccess implements BazaNcOperationEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    /**
     * Returns list of operations. The endpoint returns combined list of
     * purchases, withdraw and transfer operations in single list
     * @param request
     */
    list(request: BazaNcOperationListRequest): Observable<BazaNcOperationListResponse> {
        const qp: Record<string, unknown> = {
            ...request,
        };

        if (Array.isArray(request.types) && request.types.length) {
            qp.types = request.types.join(',');
        }

        return this.http.get(BazaNcOperationEndpointPaths.list, qp);
    }

    /**
     * Returns Operation Stats for Investor Account
     */
    stats(): Observable<BazaNcOperationStatsResponse> {
        return this.http.get(BazaNcOperationEndpointPaths.stats);
    }
}
