import { Injectable } from '@angular/core';
import { BazaNcWebhooksEndpoint, BazaNcWebhooksEndpointPaths } from '@scaliolabs/baza-nc-shared';
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';
import { Observable } from 'rxjs';

@Injectable()
export class BazaNcWebhookDataAccess implements BazaNcWebhooksEndpoint {
    constructor(private readonly ngEndpoint: BazaDataAccessService) {}

    publish(payload: unknown, secret: string, topic: string): Observable<void> {
        return this.ngEndpoint.post<void>(
            this.ngEndpoint.path(BazaNcWebhooksEndpointPaths.publish, {
                secret,
                topic,
            }),
            payload,
        );
    }
}
