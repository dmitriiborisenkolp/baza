import { BazaDocsDto, BazaDocsNodeType } from '../dto/baza-docs.dto';

export const bazaDocsStructure: BazaDocsDto = {
    index: {
        type: BazaDocsNodeType.File,
        path: 'index.md',
        title: '',
    },
    nodes: [
        {
            title: 'Getting Started',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'Create new project with Skeleton',
                    type: BazaDocsNodeType.File,
                    path: 'getting-started/01-create-application-from-repo-skeleton.md',
                },
                {
                    title: 'Define project & add project libraries',
                    type: BazaDocsNodeType.File,
                    path: 'getting-started/02-create-project-libraries.md',
                },
                {
                    title: 'Run Project',
                    type: BazaDocsNodeType.File,
                    path: 'getting-started/03-run-project',
                },
                {
                    title: 'Open API & Documentation',
                    type: BazaDocsNodeType.File,
                    path: 'getting-started/04-project-openapi.md',
                },
                {
                    title: 'ACL',
                    type: BazaDocsNodeType.File,
                    path: 'getting-started/05-project-acl.md',
                },
                {
                    title: 'I18n',
                    type: BazaDocsNodeType.File,
                    path: 'getting-started/06-project-i18n.md',
                },
                {
                    title: 'Husky & Commitlint',
                    type: BazaDocsNodeType.File,
                    path: 'getting-started/07-configure-husky.md',
                },
                {
                    title: 'Starting Development',
                    type: BazaDocsNodeType.File,
                    path: 'getting-started/08-development-whats-next.md',
                },
            ],
        },
        {
            title: 'Overview',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'Introduction',
                    type: BazaDocsNodeType.File,
                    path: 'overview/01-index.md',
                },
                {
                    title: 'Project',
                    type: BazaDocsNodeType.File,
                    path: 'overview/02-project.md',
                },
                {
                    title: 'Libraries',
                    type: BazaDocsNodeType.File,
                    path: 'overview/02-libraries.md',
                },
                {
                    title: 'Applications',
                    type: BazaDocsNodeType.File,
                    path: 'overview/03-applications.md',
                },
                {
                    title: 'Headers',
                    type: BazaDocsNodeType.File,
                    path: 'overview/04-headers.md',
                },
                {
                    title: 'I18n',
                    type: BazaDocsNodeType.File,
                    path: 'overview/05-i18n.md',
                },
                {
                    title: 'ACL',
                    type: BazaDocsNodeType.File,
                    path: 'overview/06-acl.md',
                },
                {
                    title: 'Uploads with AWS',
                    type: BazaDocsNodeType.File,
                    path: 'overview/07-aws.md',
                },
                {
                    title: 'Uploads with Attachments',
                    type: BazaDocsNodeType.File,
                    path: 'overview/08-attachments.md',
                },
                {
                    title: 'CRUD – API',
                    type: BazaDocsNodeType.File,
                    path: 'overview/09-crud-api.md',
                },
                {
                    title: 'CRUD – CMS',
                    type: BazaDocsNodeType.File,
                    path: 'overview/09-crud-cms.md',
                },
                {
                    title: 'Form Builder',
                    type: BazaDocsNodeType.File,
                    path: 'overview/10-form-builder.md',
                },
                {
                    title: 'Data Access',
                    type: BazaDocsNodeType.File,
                    path: 'overview/11-data-access.md',
                },
                {
                    title: 'E2E Tests – API',
                    type: BazaDocsNodeType.File,
                    path: 'overview/12-e2e-api-tests.md',
                },
                {
                    title: 'E2E Tests – CMS',
                    type: BazaDocsNodeType.File,
                    path: 'overview/13-e2e-cms-tests.md',
                },
                {
                    title: 'E2E Tests – WEB',
                    type: BazaDocsNodeType.File,
                    path: 'overview/14-e2e-web-tests.md',
                },
                {
                    title: 'CLI & CLI Tools',
                    type: BazaDocsNodeType.File,
                    path: 'overview/15-cli-tools.md',
                },
                {
                    title: 'Event Bus',
                    type: BazaDocsNodeType.File,
                    path: 'overview/16-event-bus.md',
                },
                {
                    title: 'Kafka',
                    type: BazaDocsNodeType.File,
                    path: 'overview/17-kafka.md',
                },
                {
                    title: 'Sentry',
                    type: BazaDocsNodeType.File,
                    path: 'overview/18-sentry.md',
                },
                {
                    title: 'Yarn commands',
                    type: BazaDocsNodeType.File,
                    path: 'overview/19-yarn-commands.md',
                },
                {
                    title: 'Auth & Accounts',
                    type: BazaDocsNodeType.File,
                    path: 'overview/20-auth-and-accounts.md',
                },
                {
                    title: 'Credit Card',
                    type: BazaDocsNodeType.File,
                    path: 'overview/21-credit-card.md',
                },
                {
                    title: 'Errors & Exceptions',
                    type: BazaDocsNodeType.File,
                    path: 'overview/22-errors-and-exceptions.md',
                },
                {
                    title: 'Version',
                    type: BazaDocsNodeType.File,
                    path: 'overview/23-versionning.md',
                },
                {
                    title: 'Debug',
                    type: BazaDocsNodeType.File,
                    path: 'overview/24-debug.md',
                },
                {
                    title: 'Environment',
                    type: BazaDocsNodeType.File,
                    path: 'overview/25-environment.md',
                },
                {
                    title: 'Mail',
                    type: BazaDocsNodeType.File,
                    path: 'overview/26-mail.md',
                },
                {
                    title: 'Maintenance',
                    type: BazaDocsNodeType.File,
                    path: 'overview/27-maintenance.md',
                },
                {
                    title: 'Migrations',
                    type: BazaDocsNodeType.File,
                    path: 'overview/28-migrations.md',
                },
                {
                    title: 'Passwords',
                    type: BazaDocsNodeType.File,
                    path: 'overview/29-passwords.md',
                },
                {
                    title: 'Registry',
                    type: BazaDocsNodeType.File,
                    path: 'overview/30-registry.md',
                },
                {
                    title: 'Status',
                    type: BazaDocsNodeType.File,
                    path: 'overview/31-status.md',
                },
                {
                    title: 'Utils & Common',
                    type: BazaDocsNodeType.File,
                    path: 'overview/32-util-and-common.md',
                },
                {
                    title: 'Phone Contry Codes',
                    type: BazaDocsNodeType.File,
                    path: 'overview/33-phone-country-codes.md',
                },
                {
                    title: 'DevOps NPM Library',
                    type: BazaDocsNodeType.File,
                    path: 'overview/34-devops-npm-library.md',
                },
                {
                    title: 'Bundles',
                    type: BazaDocsNodeType.File,
                    path: 'overview/35-bundles.md',
                },
                {
                    title: 'Device Tokens',
                    type: BazaDocsNodeType.File,
                    path: 'overview/36-device-tokens.md',
                },
            ],
        },
        {
            title: 'Tutorial',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'API',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            title: 'API Development Flow',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/01-api-development-flow.md',
                        },
                        {
                            title: "Define DTO's, Error Codes and Endpoints",
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/02-shared-dto-error-codes-and-endpoints.md',
                        },
                        {
                            title: 'TypeOrm Entity',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/03-api-entity.md',
                        },
                        {
                            title: "Mapping entities to DTO's",
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/04-api-mapper.md',
                        },
                        {
                            title: 'Exceptions & Errors',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/05-api-exceptions.md',
                        },
                        {
                            title: 'Repository Service',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/06-api-repository.md',
                        },
                        {
                            title: "Service for CMS and Public API's",
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/07-api-service.md',
                        },
                        {
                            title: 'Public & CMS Controllers',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/08-api-controllers.md',
                        },
                        {
                            title: 'Enabling new API',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/09-api-including-api.md',
                        },
                        {
                            title: 'Writing data-access services',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/10-api-data-access.md',
                        },
                        {
                            title: 'Integration tests',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/11-api-e2e-tests-api.md',
                        },
                        {
                            title: 'Setup ACL',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/12-adding-acl.md',
                        },
                        {
                            title: 'API documentation',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/13-adding-api-documentation.md',
                        },
                        {
                            title: 'Sorting',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/14-api-sort-items.md',
                        },
                        {
                            title: 'Uploads',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/15-api-upload-files.md',
                        },
                        {
                            title: 'Migration with example data',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/16-migration-and-example-data.md',
                        },
                        {
                            title: 'Completed!',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/api/17-api-completed.md',
                        },
                    ],
                },
                {
                    title: 'CMS',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            title: 'CMS Development Flow',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/cms/01-cms-development-flow.md',
                        },
                        {
                            title: 'Add new library and routes',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/cms/02-library-and-routes.md',
                        },
                        {
                            title: 'Display list of items',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/cms/03-item-list.md',
                        },
                        {
                            title: 'Create a Form',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/cms/04-item-form.md',
                        },
                        {
                            title: 'Add Edit and Delete features',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/cms/05-item-actions.md',
                        },
                        {
                            title: 'Add Sort feature',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/cms/06-item-sorting.md',
                        },
                        {
                            title: 'E2E tests',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/cms/07-e2e-tests.md',
                        },
                        {
                            title: 'Completed!',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/cms/08-cms-completed.md',
                        },
                    ],
                },
                {
                    title: 'WEB',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            title: 'Accessing new API',
                            type: BazaDocsNodeType.File,
                            path: 'tutorial/web/01-accessing-new-api.md',
                        },
                    ],
                },
            ],
        },
        {
            title: 'API Development',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'Introduction',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/01-index.md',
                },
                {
                    title: 'API Design',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/02-api-design.md',
                },
                {
                    title: 'Library structure',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/03-api-lib-structure.md',
                },
                {
                    title: 'Entities',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/04-entities.md',
                },
                {
                    title: 'Repositories',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/05-repositories.md',
                },
                {
                    title: 'Mappers',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/07-mappers.md',
                },
                {
                    title: 'Services',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/08-services.md',
                },
                {
                    title: 'Controllers',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/09-controllers.md',
                },
                {
                    title: 'Data Access',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/10-data-access.md',
                },
                {
                    title: 'Integration tests',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/11-e2e.md',
                },
                {
                    title: 'Development cluster with PM2',
                    type: BazaDocsNodeType.File,
                    path: 'api-development/11-cluster-dev.md',
                },
            ],
        },
        {
            title: 'CMS Development',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'Introduction',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/01-index.md',
                },
                {
                    title: 'Adding new CMS section',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/02-generating-new-library.md',
                },
                {
                    title: 'Adding menu groups & items',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/03-adding-new-menu-groups-and-items.md',
                },
                {
                    title: 'Using Data Access services',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/04-using-data-access-and-cms-dto.md',
                },
                {
                    title: 'File Uploads',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/05-dealing-with-uploaded-files.md',
                },
                {
                    title: 'I18n',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/06-dealing-with-i18n.md',
                },
                {
                    title: 'ACL',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/07-dealing-with-acl.md',
                },
                {
                    title: 'Auth &  Accounts',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/21-auth-and-accounts.md',
                },
                {
                    title: 'CRUD List',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/08-crud-list.md',
                },
                {
                    title: 'CRUD List – Accessing Data',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/09-crud-list-data.md',
                },
                {
                    title: 'CRUD List – Columns',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/10-crud-list-columns.md',
                },
                {
                    title: 'CRUD List – Row actions',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/11-crud-list-actions.md',
                },
                {
                    title: 'CRUD List – Items',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/12-crud-list-items.md',
                },
                {
                    title: 'CRUD List – Additional features',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/13-crud-list-additional-features.md',
                },
                {
                    title: 'CRUD – Adding custom column',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/14-crud-adding-custom-columns.md',
                },
                {
                    title: 'CRUD – Adding custom item',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/15-crud-adding-custom-items.md',
                },
                {
                    title: 'Form Builder',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/16-form-builder.md',
                },
                {
                    title: 'Form Builder – Layouts',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/17-form-builder-layouts.md',
                },
                {
                    title: 'Form Builder – Fields',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/18-form-builder-fields.md',
                },
                {
                    title: 'Form Builder – Additional features',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/19-form-builder-additional-features.md',
                },
                {
                    title: 'Form Builder – Adding custom Form Field',
                    type: BazaDocsNodeType.File,
                    path: 'cms-development/20-form-builder-adding-custom-form-controls.md',
                },
            ],
        },
        {
            title: 'Web Development',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'Introduction',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/01-index.md',
                },
                {
                    title: 'Data Access',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/02-data-access.md',
                },
                {
                    title: 'Auth & Accounts',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/03-auth-and-accounts.md',
                },
                {
                    title: 'Versioning',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/04-versionning.md',
                },
                {
                    title: 'Maintenance',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/05-maintenance.md',
                },
                {
                    title: 'Retry Strategy & Offline/Online service',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/06-retry-strategy-and-offline-service.md',
                },
                {
                    title: 'Useful components & utils',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/07-useful-components-and-utils.md',
                },
                {
                    title: 'Using Store From Libraries',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/08-using-store-from-libs.md',
                },
                {
                    title: 'Form Validation Behavior',
                    type: BazaDocsNodeType.File,
                    path: 'web-development/09-form-validation-behavior.md',
                },
                {
                    title: 'Useful Directives',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Amount Mask',
                            path: 'web-development/useful-directives/01-amount-mask.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Appear',
                            path: 'web-development/useful-directives/02-appear.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Autocomplete Off',
                            path: 'web-development/useful-directives/03-autocomplete-off.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Date Mask',
                            path: 'web-development/useful-directives/04-date-mask.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Password Input',
                            path: 'web-development/useful-directives/05-password-input.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Sort Elements',
                            path: 'web-development/useful-directives/06-sort-elements.md',
                        },
                    ],
                },
                {
                    title: 'Routes/Links Customization',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Configuration Guide',
                            path: 'web-development/customizations/route-configurations/01-route-configuration-guide.md',
                        },
                    ],
                },
                {
                    title: 'Verification Flow Customization',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Verification Page Configuration',
                            path: 'web-development/customizations/verification-flow/01-verification-page-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Verification Flow - Query Param Links Configuration',
                            path: 'web-development/customizations/verification-flow/02-vf-query-param-links-config.md',
                        },
                    ],
                },
                {
                    title: 'Account Components Customization',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Account Balance Configuration',
                            path: 'web-development/customizations/account-components/01-account-balance-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Bank Account Configuration',
                            path: 'web-development/customizations/account-components/02-bank-account-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Credit Card Configuration',
                            path: 'web-development/customizations/account-components/03-credit-card-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Bank Account Payout Configuration',
                            path: 'web-development/customizations/account-components/04-bank-account-payout-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Payment Methods Configuration',
                            path: 'web-development/customizations/account-components/05-payment-methods-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Payout Methods Configuration',
                            path: 'web-development/customizations/account-components/06-payout-methods-config.md',
                        },
                    ],
                },
                {
                    title: 'Purchase Flow Customization',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Purchase Page Configuration',
                            path: 'web-development/customizations/purchase-flow/01-purchase-page-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Details Page Configuration',
                            path: 'web-development/customizations/purchase-flow/02-details-page-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Agreement Page Configuration',
                            path: 'web-development/customizations/purchase-flow/03-agreement-page-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Payment Page Configuration',
                            path: 'web-development/customizations/purchase-flow/04-payment-page-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Done Page Configuration',
                            path: 'web-development/customizations/purchase-flow/05-done-page-config.md',
                        },
                    ],
                },
                {
                    title: 'Marketing Components',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Hero Banner Component',
                            path: 'web-development/marketing-components/01-hero-banner.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Text-Image Component',
                            path: 'web-development/marketing-components/02-text-img.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Bullets Component (Text)',
                            path: 'web-development/marketing-components/03-bullets-text.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Bullets Component (Images)',
                            path: 'web-development/marketing-components/04-bullets-image.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Text Section Component',
                            path: 'web-development/marketing-components/05-text-section.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Banner Component',
                            path: 'web-development/marketing-components/06-banner.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Why Invest Component',
                            path: 'web-development/marketing-components/07-why-invest.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'How It Works Component',
                            path: 'web-development/marketing-components/08-how-it-works.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'CTA Component',
                            path: 'web-development/marketing-components/09-cta.md',
                        },
                    ],
                },
                {
                    title: 'i18n Translations (FE)',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Initial Setup',
                            path: 'web-development/i18n/initial-setup/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Web UI Components',
                            path: 'web-development/i18n/web-ui-components/01-index.md',
                            deps: [
                                'web-development/i18n/web-ui-components/02-account-balance-card.md',
                                'web-development/i18n/web-ui-components/03-summary.md',
                                'web-development/i18n/web-ui-components/04-payment-bank.md',
                                'web-development/i18n/web-ui-components/05-payment-bank-modal.md',
                                'web-development/i18n/web-ui-components/06-bank-details.md',
                                'web-development/i18n/web-ui-components/07-payment-card.md',
                                'web-development/i18n/web-ui-components/08-payment-card-modal.md',
                                'web-development/i18n/web-ui-components/09-card-details.md',
                                'web-development/i18n/web-ui-components/10-personal.md',
                                'web-development/i18n/web-ui-components/11-add-funds.md',
                                'web-development/i18n/web-ui-components/15-withdraw-funds.md',
                            ],
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Account Components (Dwolla)',
                            path: 'web-development/i18n/dwolla-account/01-index.md',
                            deps: [
                                'web-development/i18n/dwolla-account/02-account-balance.md',
                                'web-development/i18n/dwolla-account/03-bank-account.md',
                                'web-development/i18n/dwolla-account/04-credit-card.md',
                                'web-development/i18n/dwolla-account/05-payout-bank-account.md',
                            ],
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Verification Flow (Dwolla)',
                            path: 'web-development/i18n/dwolla-vf/01-index-i18n.md',
                            deps: [
                                'web-development/i18n/dwolla-vf/02-vf-i18n.md',
                                'web-development/i18n/dwolla-vf/03-vf-check-i18n.md',
                                'web-development/i18n/dwolla-vf/04-vf-info-i18n.md',
                                'web-development/i18n/dwolla-vf/05-vf-investor-i18n.md',
                                'web-development/i18n/dwolla-vf/06-vf-file-upload-i18n.md',
                            ],
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Purchase Flow (Dwolla)',
                            path: 'web-development/i18n/dwolla-pf/01-index.md',
                            deps: [
                                'web-development/i18n/dwolla-pf/02-details.md',
                                'web-development/i18n/dwolla-pf/03-agreement.md',
                                'web-development/i18n/dwolla-pf/04-payment.md',
                                'web-development/i18n/dwolla-pf/05-methods.md',
                                'web-development/i18n/dwolla-pf/06-done.md',
                                'web-development/i18n/dwolla-pf/07-payment-edit.md',
                                'web-development/i18n/dwolla-pf/08-payment-methods.md',
                            ],
                        },

                        {
                            type: BazaDocsNodeType.File,
                            title: 'Verification Flow (NC)',
                            path: 'web-development/i18n/nc-vf/01-index-i18n.md',
                            deps: [
                                'web-development/i18n/nc-vf/02-vf-i18n.md',
                                'web-development/i18n/nc-vf/03-vf-info-i18n.md',
                                'web-development/i18n/nc-vf/04-vf-investor-i18n.md',
                                'web-development/i18n/nc-vf/05-vf-file-upload-i18n.md',
                            ],
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Purchase Flow (NC)',
                            path: 'web-development/i18n/nc-pf/01-index.md',
                            deps: [
                                'web-development/i18n/nc-pf/02-details.md',
                                'web-development/i18n/nc-pf/03-agreement.md',
                                'web-development/i18n/nc-pf/04-payment.md',
                                'web-development/i18n/nc-pf/05-methods.md',
                                'web-development/i18n/nc-pf/06-done.md',
                                'web-development/i18n/nc-pf/07-payment-edit.md',
                                'web-development/i18n/nc-pf/08-payment-methods.md',
                            ],
                        },
                    ],
                },
                {
                    title: 'Exportable Features',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Withdraw Funds',
                            path: 'web-development/exportable-features/withdraw-funds/01-index.md',
                        },
                    ],
                },
            ],
        },
        {
            title: 'Techniques',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'Detecting Client Application',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/02-detecting-client-application.md',
                },
                {
                    title: 'Preventing Email Duplicates',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/03-prevent-email-duplicates.md',
                },
                {
                    title: 'Injecting & External modifying CMS CRUDs',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/04-injecting-crud-lists.md',
                },
                {
                    title: 'Injecting & External modifying Forms',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/05-injecting-forms.md',
                },
                {
                    title: 'Writing CLI Tools',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/06-writing-cli-tool.md',
                },
                {
                    title: 'Baza with node cluster',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/07-using-baza-multi-node-setup.md',
                },
                {
                    title: 'Web Responsive Images using Attachments',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/08-web-responsive-images-using-attachments.md',
                },
                {
                    title: 'Upload Images with CMS',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/09-cms-upload-images.md',
                },
                {
                    title: 'Export to CSV Implementation Guide',
                    type: BazaDocsNodeType.File,
                    path: 'techniques/10-export-to-csv-implementation.md',
                },
            ],
        },
        {
            title: 'DevOps',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'HTTP Headers',
                    type: BazaDocsNodeType.File,
                    path: 'devops/01-http-headers-api.md',
                },
                {
                    title: 'Environment Variables – API',
                    type: BazaDocsNodeType.File,
                    path: 'devops/02-environment-variables-api.md',
                },
                {
                    title: 'Environment Variables – CMS',
                    type: BazaDocsNodeType.File,
                    path: 'devops/03-environment-variables-cms.md',
                },
                {
                    title: 'Environment Variables – WEB',
                    type: BazaDocsNodeType.File,
                    path: 'devops/04-environment-variables-web.md',
                },
                {
                    title: 'Build Applications',
                    type: BazaDocsNodeType.File,
                    path: 'devops/05-build-applications.md',
                },
                {
                    title: 'Optimizing WEB / CMS bundles',
                    type: BazaDocsNodeType.File,
                    path: 'devops/06-optimizing-bundles-mock-npm-libraries.md',
                },
                {
                    title: 'API Status',
                    type: BazaDocsNodeType.File,
                    path: 'devops/07-status-api.md',
                },
                {
                    title: 'E2E Tests for CI/CD workflows',
                    type: BazaDocsNodeType.File,
                    path: 'devops/08-e2e-tests.md',
                },
                {
                    title: 'Maintenance',
                    type: BazaDocsNodeType.File,
                    path: 'devops/09-maintenance.md',
                },

                {
                    title: 'Set Up Kafka',
                    type: BazaDocsNodeType.File,
                    path: 'devops/10-kafka.md',
                },

                {
                    title: 'Master Node',
                    type: BazaDocsNodeType.File,
                    path: 'devops/11-master-node.md',
                },
            ],
        },
        {
            title: 'Libraries',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'baza-core',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-account',
                            path: 'libraries/baza-core/baza-account/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-acl',
                            path: 'libraries/baza-core/baza-acl/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-auth',
                            path: 'libraries/baza-core/baza-auth/01-index.md',
                            deps: [
                                'libraries/baza-core/baza-auth/02-ng-jwt-services.md',
                                'libraries/baza-core/baza-auth/03-ng-jwt-guards.md',
                                'libraries/baza-core/baza-auth/04-ng-jwt-http-interceptors.md',
                            ],
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-aws',
                            path: 'libraries/baza-core/baza-aws/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-cli',
                            path: 'libraries/baza-core/baza-cli/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-common',
                            path: 'libraries/baza-core/baza-common/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-credit-card',
                            path: 'libraries/baza-core/baza-credit-card/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-crud',
                            path: 'libraries/baza-core/baza-crud/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-debug',
                            path: 'libraries/baza-core/baza-debug/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-e2e',
                            path: 'libraries/baza-core/baza-e2e/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-env',
                            path: 'libraries/baza-core/baza-env/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-event-bus',
                            path: 'libraries/baza-core/baza-event-bus/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-exceptions',
                            path: 'libraries/baza-core/baza-exceptions/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-i18n',
                            path: 'libraries/baza-core/baza-i18n/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-kafka',
                            path: 'libraries/baza-core/baza-kafka/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-logger',
                            path: 'libraries/baza-core/baza-logger/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-mail',
                            path: 'libraries/baza-core/baza-mail/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-maintenance',
                            path: 'libraries/baza-core/baza-maintenance/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-migrations',
                            path: 'libraries/baza-core/baza-migrations/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-password',
                            path: 'libraries/baza-core/baza-password/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-phone-country-codes',
                            path: 'libraries/baza-core/baza-phone-contry-codes/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-registry',
                            path: 'libraries/baza-core/baza-registry/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-sentry',
                            path: 'libraries/baza-core/baza-sentry/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-status',
                            path: 'libraries/baza-core/baza-status/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'baza-version',
                            path: 'libraries/baza-core/baza-version/01-index.md',
                        },
                    ],
                },
                {
                    title: 'baza-content-types',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Introduction',
                            path: 'libraries/baza-content-types/01-index.md',
                        },
                    ],
                },
                {
                    title: 'baza-nc',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Introduction',
                            path: 'libraries/baza-nc/01-index.md',
                        },

                        {
                            type: BazaDocsNodeType.File,
                            title: 'Transact API Configuration',
                            path: 'libraries/baza-nc/02-transact-api-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'API Bundle',
                            path: 'libraries/baza-nc/03-api-bundle-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'CMS Bundle',
                            path: 'libraries/baza-nc/04-cms-bundle-config.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Webhooks',
                            path: 'libraries/baza-nc/05-webhooks.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Security Notes',
                            path: 'libraries/baza-nc/06-security-notes.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Environment',
                            path: 'libraries/baza-nc/07-environment-configuration.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Account Verification',
                            path: 'libraries/baza-nc/08-account-verification.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Purchase Flow',
                            path: 'libraries/baza-nc/09-purchase-flow.md',
                        },
                    ],
                },
                {
                    title: 'baza-nc-integration',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Introduction',
                            path: 'libraries/baza-nc-integration/01-index.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Environment',
                            path: 'libraries/baza-nc-integration/02-environment.md',
                        },
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Data Access',
                            path: 'libraries/baza-nc-integration/03-data-access.md',
                        },
                    ],
                },
                {
                    title: 'baza-plaid',
                    type: BazaDocsNodeType.Directory,
                    nodes: [
                        {
                            type: BazaDocsNodeType.File,
                            title: 'Introduction',
                            path: 'libraries/baza-plaid/01-plaid-redirect-url.md',
                        },
                    ],
                },
            ],
        },
        {
            title: 'Investment Platform',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: 'Introduction',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/01-index.md',
                },
                {
                    title: 'Setup new project',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/02-setup.md',
                },
                {
                    title: 'Sentry Configuration',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/03-sentry.md',
                },
                {
                    title: 'Features',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/04-features.md',
                },
                {
                    title: 'Webhook Configuration',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/05-webhook-configuration.md',
                },
                {
                    title: 'Changing NC Account',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/06-changing-nc-account.md',
                },
                {
                    title: 'Radio Service',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/07-radio-service.md',
                },
                {
                    title: 'Images Ratio Configuration for Listings',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/08-listing-image-ratio-configuration.md',
                },
                {
                    title: 'Web – Data Access',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/09-web-data-access.md',
                },
                {
                    title: 'Web, IOS – Usage of New Bank Account API',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/10-bank-account-api-usage.md',
                },
                {
                    title: 'North Capital Usage',
                    type: BazaDocsNodeType.File,
                    path: 'investment-platform/11-nc-api-usage-stats.md',
                },
            ],
        },
        {
            title: 'Baza Development',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    type: BazaDocsNodeType.File,
                    title: 'Adding new Baza library',
                    path: 'development/01-npm-guide.md',
                },
                {
                    type: BazaDocsNodeType.File,
                    title: 'Publishing Baza libraries to NPM',
                    path: 'development/02-npm-publish.md',
                },
                {
                    type: BazaDocsNodeType.File,
                    title: 'Debugging Baza on Target projects',
                    path: 'development/03-debug-target-projects.md',
                },
            ],
        },
        {
            title: 'Knowledge DB',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: '0001 – API – How to get current account?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0001-get-current-account-api-controller.md',
                },
                {
                    title: '0002 – API – Why DB can contain multiple accounts with same email address?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0002-why-there-are-accounts-with-same-email-in-db-api.md',
                },
                {
                    title: "0003 – API – What's the difference between CrudListRequestDto and CrudListRequestQueryDto?",
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0003-whats-the-difference-between-list-and-query-dto.md',
                },
                {
                    title: '0004 – CMS – How to navigate to Not Found page in CMS?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0004-how-to-navigate-to-not-found-page-cms.md',
                },
                {
                    title: '0005 – CMS, WEB – How to access environment configuration from libraries?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0005-how-to-access-environment-cms-or-web.md',
                },
                {
                    title: '0005 – CMS, API – How to add additional fields to Account?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0006-how-to-add-additional-fields-to-account.md',
                },
                {
                    title: '0006 – CMS, API – How to add additional fields to Account?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0006-how-to-add-additional-fields-to-account.md',
                },
                {
                    title: '0007 – CMS, WEB - How to change default image quality, format and sizes for uploaded images?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0007-how-to-change-default-image-params.md',
                },
                {
                    title: '0008 – API – How to customize email verification token?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0008-how-to-customize-email-verification-token.md',
                },
                {
                    title: '0009 – API – How to disable SSN / Credit Card / Bank Account fields masking?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0009-nc-disable-mask-fields.md',
                },
                {
                    title: '0010 – API, CMS – How to customize list of applications in Maintenance CMS?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0010-how-to-customize-list-maintenance-applications.md',
                },
                {
                    title: '0011 – CMS – How to change Side Panel width for Auth and CMS pages?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0011-how-to-change-cms-side-panel-width.md',
                },
                {
                    title: '0012 - CMS, API - How to change default CSV delimiter?',
                    type: BazaDocsNodeType.File,
                    path: 'knowledge-db/baza-core/0012-change-default-csv-delimeter.md',
                },
            ],
        },
        {
            title: 'Release Notes',
            type: BazaDocsNodeType.Directory,
            nodes: [
                {
                    title: '1.33.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.33.0.md',
                },
                {
                    title: '1.32.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.32.0.md',
                },
                {
                    title: '1.31.3',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.31.3.md',
                },
                {
                    title: '1.31.2',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.31.2.md',
                },
                {
                    title: '1.31.1',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.31.1.md',
                },
                {
                    title: '1.31.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.31.0.md',
                },
                {
                    title: '1.30.3',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.30.3.md',
                },
                {
                    title: '1.30.2',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.30.2.md',
                },
                {
                    title: '1.30.1',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.30.1.md',
                },
                {
                    title: '1.30.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.30.0.md',
                },
                {
                    title: '1.29.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.29.0.md',
                },
                {
                    title: '1.28.6',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.28.6.md',
                },
                {
                    title: '1.28.5',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.28.5.md',
                },
                {
                    title: '1.28.4',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.28.4.md',
                },
                {
                    title: '1.28.3',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.28.3.md',
                },
                {
                    title: '1.28.2',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.28.2.md',
                },
                {
                    title: '1.28.1',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.28.1.md',
                },
                {
                    title: '1.28.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.28.0.md',
                },
                {
                    title: '1.27.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.27.0.md',
                },
                {
                    title: '1.26.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.26.0.md',
                },
                {
                    title: '1.25.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.25.0.md',
                },
                {
                    title: '1.24.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.24.0.md',
                },
                {
                    title: '1.23.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.23.0.md',
                },
                {
                    title: '1.22.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.22.0.md',
                },
                {
                    title: '1.21.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.21.0.md',
                },
                {
                    title: '1.20.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.20.0.md',
                },
                {
                    title: '1.19.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.19.0.md',
                },
                {
                    title: '1.18.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.18.0.md',
                },
                {
                    title: '1.17.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.17.0.md',
                },
                {
                    title: '1.16.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.16.0.md',
                },
                {
                    title: '1.15.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.15.0.md',
                },
                {
                    title: '1.14.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.14.0.md',
                },
                {
                    title: '1.13.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.13.0.md',
                },
                {
                    title: '1.12.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.12.0.md',
                },
                {
                    title: '1.11.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.11.0.md',
                },
                {
                    title: '1.10.13',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.13.md',
                },
                {
                    title: '1.10.11',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.11.md',
                },
                {
                    title: '1.10.10',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.10.md',
                },
                {
                    title: '1.10.9',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.9.md',
                },
                {
                    title: '1.10.8',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.8.md',
                },
                {
                    title: '1.10.7',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.7.md',
                },
                {
                    title: '1.10.6',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.6.md',
                },
                {
                    title: '1.10.5',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.5.md',
                },
                {
                    title: '1.10.3',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.3.md',
                },
                {
                    title: '1.10.2',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.2.md',
                },
                {
                    title: '1.10.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.10.0.md',
                },
                {
                    title: '1.9.7',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.9.7.md',
                },
                {
                    title: '1.9.6',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.9.6.md',
                },
                {
                    title: '1.7.4',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.7.4.md',
                },
                {
                    title: '1.7.3',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.7.3.md',
                },
                {
                    title: '1.7.2',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.7.2.md',
                },
                {
                    title: '1.7.0',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.7.0.md',
                },
                {
                    title: '1.6.2',
                    type: BazaDocsNodeType.File,
                    path: 'release-notes/release-notes-1.6.2.md',
                },
            ],
        },
    ],
};
