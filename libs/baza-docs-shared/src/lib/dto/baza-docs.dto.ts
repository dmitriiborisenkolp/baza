export class BazaDocsDto {
    index: BazaDocsFileNode;
    nodes: Array<BazaDocsNode>;
}

export enum BazaDocsNodeType {
    Directory = 'Directory',
    File = 'File',
}

export class BazaDocsDirectoryNode {
    type: BazaDocsNodeType.Directory;
    title: string;
    nodes: Array<BazaDocsNode>;
}

export class BazaDocsFileNode {
    type: BazaDocsNodeType.File;
    title: string;
    path: string;
    deps?: Array<string>;
}

export type BazaDocsNode = BazaDocsDirectoryNode | BazaDocsFileNode;
