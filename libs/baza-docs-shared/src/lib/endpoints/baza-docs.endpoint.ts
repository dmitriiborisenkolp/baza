import { BazaDocsFileDto } from '../dto/baza-docs-file.dto';
import { Observable } from 'rxjs';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { IsOptional, IsString } from 'class-validator';
import { BazaDocsDto } from '../dto/baza-docs.dto';

export enum BazaDocsEndpointPaths {
    File = '/baza-docs/file',
    Structure = '/baza-docs/structure',
}

export interface BazaDocsEndpoint {
    file(request: BazaDocsFileRequest): Promise<BazaDocsFileDto> | Observable<BazaDocsFileDto>;
    structure(): Promise<BazaDocsDto> | Observable<BazaDocsDto>;
}

export class BazaDocsFileRequest {
    @ApiModelProperty()
    @IsString()
    @IsOptional()
    relativePath?: string;
}
