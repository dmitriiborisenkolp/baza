# baza-dwolla-cms

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test baza-dwolla-cms` to execute the unit tests.

## Build

Run `nx build baza-dwolla-cms`

## Publish

Run `yarn publish:baza-dwolla-cms`