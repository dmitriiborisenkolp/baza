import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { BazaCrudComponent, BazaCrudConfig, BazaTableFieldType } from '@scaliolabs/baza-core-cms';
import { BazaDwollaEscrowFundingSourceDto, DwollaFundingSourceStatus, DwollaFundingSourceType } from '@scaliolabs/baza-dwolla-shared';
import { BazaDwollaEscrowCmsDataAccess } from '@scaliolabs/baza-dwolla-cms-data-access';
import { BazaLoadingService, BazaNgConfirmService, BazaNgMessagesService } from '@scaliolabs/baza-core-ng';
import { finalize, map, Subject, takeUntil } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

interface State {
    config: BazaCrudConfig<BazaDwollaEscrowFundingSourceDto>;
}

export const BAZA_DWOLLA_ESCROW_CMS_ID = 'baza-dwolla-escrow-cms-id';

const bankName = (entity: BazaDwollaEscrowFundingSourceDto) =>
    `${(entity.bankAccountType || '').toString().toUpperCase()} ${entity.bankName}`;

@Component({
    templateUrl: './baza-dwolla-escrow-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDwollaEscrowCmsComponent implements OnDestroy {
    public crud: BazaCrudComponent<BazaDwollaEscrowFundingSourceDto>;

    public state: State = {
        config: this.crudConfig,
    };

    private ngOnDestroy$ = new Subject<void>();

    constructor(
        private readonly dataAccess: BazaDwollaEscrowCmsDataAccess,
        private readonly translate: TranslateService,
        private readonly ngConfirm: BazaNgConfirmService,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly loading: BazaLoadingService,
    ) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    i18n(key: string): string {
        return `baza-dwolla.escrow.components.bazaDwollaEscrowCms.${key}`;
    }

    i18nType(type: DwollaFundingSourceType): string {
        return `baza-dwolla.__types.DwollaFundingSourceType.${type}`;
    }

    i18nStatus(status: DwollaFundingSourceStatus): string {
        return `baza-dwolla.__types.DwollaFundingSourceStatus.${status}`;
    }

    private get crudConfig(): BazaCrudConfig<BazaDwollaEscrowFundingSourceDto> {
        return {
            id: BAZA_DWOLLA_ESCROW_CMS_ID,
            title: this.i18n('title'),
            data: {
                dataSource: {
                    array: () =>
                        this.dataAccess.list().pipe(
                            map((items) => ({
                                items: items.reverse(),
                            })),
                        ),
                },
                actions: [
                    {
                        title: this.i18n('actions.set.title'),
                        icon: 'check-circle',
                        action: (entity) => this.setAsDefault(entity),
                        visible: (entity) => entity.canBeSelectedAsEscrow && !entity.isSelectedAsEscrow,
                    },
                ],
                columns: [
                    {
                        field: 'name',
                        title: this.i18n('columns.name'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            translate: false,
                            format: (entity) =>
                                entity.type === DwollaFundingSourceType.Balance
                                    ? this.translate.instant(this.i18n('balance'))
                                    : bankName(entity),
                        },
                        ngStyle: (entity) => {
                            if (entity.type === DwollaFundingSourceType.Balance) {
                                return {
                                    'font-style': 'italic',
                                };
                            } else {
                                return {
                                    'font-weight': '500',
                                };
                            }
                        },
                    },
                    {
                        field: 'created',
                        title: this.i18n('columns.created'),
                        type: {
                            kind: BazaTableFieldType.Date,
                        },
                        width: 240,
                    },
                    {
                        field: 'type',
                        title: this.i18n('columns.type'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            translate: true,
                            format: (entity) => this.i18nType(entity.type),
                        },
                        width: 200,
                    },
                    {
                        field: 'status',
                        title: this.i18n('columns.status'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            translate: true,
                            format: (entity) => this.i18nStatus(entity.status),
                        },
                        ngStyle: (entity) => ({
                            color: (() => {
                                if (entity.status === DwollaFundingSourceStatus.Verified) {
                                    return 'var(--success-color)';
                                } else if (entity.status === DwollaFundingSourceStatus.Unverified) {
                                    return 'var(--error-color)';
                                } else {
                                    return 'var(--normal-color)';
                                }
                            })(),
                        }),
                        width: 220,
                    },
                    {
                        field: 'isSelectedAsEscrow',
                        title: this.i18n('columns.isSelectedAsEscrow'),
                        type: {
                            kind: BazaTableFieldType.Boolean,
                        },
                        width: 180,
                    },
                ],
            },
        };
    }

    setAsDefault(entity: BazaDwollaEscrowFundingSourceDto): void {
        const name = entity.type === DwollaFundingSourceType.Balance ? this.translate.instant(this.i18n('balance')) : bankName(entity);

        // eslint-disable-next-line security/detect-non-literal-fs-filename
        this.ngConfirm.open({
            message: this.i18n('actions.set.confirm'),
            messageArgs: { name },
            confirm: () => {
                const loading = this.loading.addLoading();

                this.dataAccess
                    .set({
                        dwollaFundingSourceId: entity.dwollaFundingSourceId,
                    })
                    .pipe(
                        finalize(() => loading.complete()),
                        takeUntil(this.ngOnDestroy$),
                    )
                    .subscribe({
                        complete: () => {
                            this.ngMessages.success({
                                message: this.i18n('actions.set.success'),
                                translate: true,
                                translateParams: { name },
                            });

                            this.crud.refresh();
                        },
                        error: () => {
                            this.ngMessages.error({
                                message: this.i18n('actions.set.failed'),
                                translate: true,
                                translateParams: { name },
                            });
                        },
                    });
            },
        });
    }
}
