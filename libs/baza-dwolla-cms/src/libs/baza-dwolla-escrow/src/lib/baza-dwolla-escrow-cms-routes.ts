import { Routes } from '@angular/router';
import { BazaDwollaEscrowCmsComponent } from './components/baza-dwolla-escrow-cms/baza-dwolla-escrow-cms.component';

export const bazaDwollaEscrowCmsRoutes: Routes = [
    {
        path: 'baza-dwolla/escrow',
        component: BazaDwollaEscrowCmsComponent,
    },
];
