import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaCmsLayoutModule, BazaCrudCmsModule } from '@scaliolabs/baza-core-cms';
import { BazaDwollaCmsDataAccessModule } from '@scaliolabs/baza-dwolla-cms-data-access';
import { BazaDwollaEscrowCmsComponent } from './components/baza-dwolla-escrow-cms/baza-dwolla-escrow-cms.component';

@NgModule({
    imports: [CommonModule, BazaCrudCmsModule, BazaCmsLayoutModule, BazaDwollaCmsDataAccessModule],
    declarations: [BazaDwollaEscrowCmsComponent],
})
export class BazaDwollaEscrowCmsModule {}
