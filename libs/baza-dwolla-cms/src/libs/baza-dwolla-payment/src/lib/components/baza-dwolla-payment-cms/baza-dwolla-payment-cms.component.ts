import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {
    BazaCrudComponent,
    BazaCrudConfig,
    BazaCrudItem,
    BazaFormLayoutService,
    BazaTableFieldAlign,
    BazaTableFieldType,
} from '@scaliolabs/baza-core-cms';
import { BazaDwollaPaymentCmsDataAccess } from '@scaliolabs/baza-dwolla-cms-data-access';
import {
    BazaDwollaPaymentCmsDto,
    BazaDwollaPaymentStatus,
    BazaDwollaPaymentType,
    DwollaFailureCodes,
} from '@scaliolabs/baza-dwolla-shared';
import { BazaLoadingService, BazaNgMessagesService, BazaNzButtonStyle } from '@scaliolabs/baza-core-ng';

interface State {
    config: BazaCrudConfig<BazaDwollaPaymentCmsDto>;
}

export const BAZA_DWOLLA_PAYMENTS_CMS_ID = 'baza-dwolla-payments-cms-id';

@Component({
    templateUrl: './baza-dwolla-payment-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDwollaPaymentCmsComponent {
    public crud: BazaCrudComponent<BazaDwollaPaymentCmsDto>;

    public state: State = {
        config: this.crudConfig,
    };

    constructor(
        private readonly fb: FormBuilder,
        private readonly formLayout: BazaFormLayoutService,
        private readonly dataAccess: BazaDwollaPaymentCmsDataAccess,
        private readonly ngMessages: BazaNgMessagesService,
        private readonly loading: BazaLoadingService,
    ) {}

    private get crudConfig(): BazaCrudConfig<BazaDwollaPaymentCmsDto> {
        return {
            id: BAZA_DWOLLA_PAYMENTS_CMS_ID,
            title: this.i18n('title'),
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.Button,
                        options: {
                            title: this.i18n('actions.refresh'),
                            icon: 'sync',
                            callback: () => this.crud.refresh(),
                            type: BazaNzButtonStyle.Link,
                        },
                    },
                    {
                        type: BazaCrudItem.DateRange,
                        options: {
                            nzMode: 'date',
                        },
                    },
                    {
                        type: BazaCrudItem.Search,
                        options: {
                            placeholder: this.i18n('search'),
                        },
                    },
                ],
            },
            data: {
                dataSource: {
                    list: (request) => this.dataAccess.list(request),
                },
                actions: [
                    {
                        title: this.i18n('actions.sync.title'),
                        icon: 'sync',
                        action: (entity) => this.sync(entity),
                    },
                ],
                columns: [
                    {
                        field: 'ulid',
                        title: this.i18n('columns.ulid'),
                        type: {
                            kind: BazaTableFieldType.Ulid,
                        },
                    },
                    {
                        field: 'dateCreatedAt',
                        title: this.i18n('columns.dateCreatedAt'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        width: 220,
                    },
                    {
                        field: 'dateProcessedAt',
                        title: this.i18n('columns.dateProcessedAt'),
                        type: {
                            kind: BazaTableFieldType.DateTime,
                        },
                        width: 220,
                    },
                    {
                        field: 'account',
                        title: this.i18n('columns.account'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => `${entity.account.fullName} (ID: ${entity.account.id})`,
                        },
                    },
                    {
                        field: 'correlationId',
                        title: this.i18n('columns.correlationId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        width: 260,
                    },
                    {
                        field: 'dwollaTransferId',
                        title: this.i18n('columns.dwollaTransferId'),
                        type: {
                            kind: BazaTableFieldType.Text,
                        },
                        textAlign: BazaTableFieldAlign.Center,
                        width: 260,
                    },
                    {
                        field: 'amount',
                        title: this.i18n('columns.amount'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => `$${entity.amount}`,
                        },
                        width: 180,
                    },
                    {
                        field: 'type',
                        title: this.i18n('columns.type'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nType(entity.type),
                            translate: true,
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                    },
                    {
                        field: 'status',
                        title: this.i18n('columns.status'),
                        type: {
                            kind: BazaTableFieldType.Text,
                            format: (entity) => this.i18nStatus(entity.status),
                            translate: true,
                            hint: (entity) => {
                                if (entity.status === BazaDwollaPaymentStatus.Failed && entity.failureReason) {
                                    const isDwollaErrorCode = Object.values(DwollaFailureCodes).includes(
                                        entity.failureReason.code as DwollaFailureCodes,
                                    );
                                    const text = isDwollaErrorCode
                                        ? `${entity.failureReason.code} ${entity.failureReason.reason}`
                                        : entity.failureReason.reason;

                                    return {
                                        text,
                                        translate: false,
                                        icon: 'warning',
                                    };
                                }

                                return undefined;
                            },
                        },
                        width: 160,
                        textAlign: BazaTableFieldAlign.Center,
                        ngStyle: (entity) => ({
                            color: this.cssColorStatus(entity.status),
                        }),
                    },
                ],
            },
        };
    }

    i18n(key: string): string {
        return `baza-dwolla.payment.components.bazaDwollaPaymentCms.${key}`;
    }

    i18nStatus(status: BazaDwollaPaymentStatus): string {
        return `baza-dwolla.__types.BazaDwollaPaymentStatus.${status}`;
    }

    i18nType(type: BazaDwollaPaymentType): string {
        return `baza-dwolla.__types.BazaDwollaPaymentType.${type}`;
    }

    cssColorStatus(status: BazaDwollaPaymentStatus): string {
        switch (status) {
            default: {
                return `var(--black)`;
            }

            case BazaDwollaPaymentStatus.Pending: {
                return `var(--processing-color)`;
            }

            case BazaDwollaPaymentStatus.Failed: {
                return `var(--error-color)`;
            }

            case BazaDwollaPaymentStatus.Processed: {
                return `var(--success-color)`;
            }

            case BazaDwollaPaymentStatus.Cancelled: {
                return `var(--warning-color)`;
            }
        }
    }

    refresh(): void {
        this.crud.refresh();
    }

    sync(payment: BazaDwollaPaymentCmsDto): void {
        const loading = this.loading.addLoading();

        this.dataAccess
            .sync({
                ulid: payment.ulid,
            })
            .pipe()
            .subscribe({
                complete: () => {
                    this.refresh();

                    this.ngMessages.success({
                        translate: true,
                        message: this.i18n('actions.sync.success'),
                    });

                    loading.complete();
                },
                error: () => {
                    this.ngMessages.error({
                        translate: true,
                        message: this.i18n('actions.sync.failed'),
                    });

                    loading.fail();
                },
            });
    }
}
