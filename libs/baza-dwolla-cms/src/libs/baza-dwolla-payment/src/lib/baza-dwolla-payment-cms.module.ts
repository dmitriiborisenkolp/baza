import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BazaCmsLayoutModule, BazaCrudCmsModule } from '@scaliolabs/baza-core-cms';
import { BazaDwollaCmsDataAccessModule } from '@scaliolabs/baza-dwolla-cms-data-access';
import { BazaDwollaPaymentCmsComponent } from './components/baza-dwolla-payment-cms/baza-dwolla-payment-cms.component';

@NgModule({
    imports: [CommonModule, BazaCrudCmsModule, BazaCmsLayoutModule, BazaDwollaCmsDataAccessModule],
    declarations: [BazaDwollaPaymentCmsComponent],
})
export class BazaDwollaPaymentCmsModule {}
