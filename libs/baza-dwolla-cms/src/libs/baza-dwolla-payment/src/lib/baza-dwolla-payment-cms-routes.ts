import { Routes } from '@angular/router';
import { BazaDwollaPaymentCmsComponent } from './components/baza-dwolla-payment-cms/baza-dwolla-payment-cms.component';

export const bazaDwollaPaymentCmsRoutes: Routes = [
    {
        path: 'baza-dwolla/payments',
        component: BazaDwollaPaymentCmsComponent,
    },
];
