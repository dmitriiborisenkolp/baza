import { NgModule } from '@angular/core';
import { BazaDwollaPaymentCmsModule } from '../../../libs/baza-dwolla-payment/src';
import { BazaDwollaEscrowCmsModule } from '../../../libs/baza-dwolla-escrow/src';

@NgModule({
    imports: [BazaDwollaPaymentCmsModule, BazaDwollaEscrowCmsModule],
})
export class BazaDwollaCmsBundleModule {}
