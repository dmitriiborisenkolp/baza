import { CmsSidePanelMenuGroupConfig } from '@scaliolabs/baza-core-cms';
import { BazaDwollaAcl } from '@scaliolabs/baza-dwolla-shared';

export const bazaDwollaCmsMenu: Array<CmsSidePanelMenuGroupConfig> = [
    {
        title: 'baza-dwolla.title',
        items: [
            {
                title: 'baza-dwolla.escrow.title',
                translate: true,
                routerLink: ['/baza-dwolla/escrow'],
                requiresACL: [BazaDwollaAcl.BazaDwollaEscrow],
                icon: 'bank',
            },
            {
                title: 'baza-dwolla.payment.title',
                translate: true,
                routerLink: ['/baza-dwolla/payments'],
                requiresACL: [BazaDwollaAcl.BazaDwollaPayment],
                icon: 'dollar',
            },
        ],
    },
];
