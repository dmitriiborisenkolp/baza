import { Routes } from '@angular/router';
import { bazaDwollaPaymentCmsRoutes } from '../../../libs/baza-dwolla-payment/src';
import { bazaDwollaEscrowCmsRoutes } from '../../../libs/baza-dwolla-escrow/src';

export const bazaDwollaCmsRoutes: Routes = [...bazaDwollaPaymentCmsRoutes, ...bazaDwollaEscrowCmsRoutes];
