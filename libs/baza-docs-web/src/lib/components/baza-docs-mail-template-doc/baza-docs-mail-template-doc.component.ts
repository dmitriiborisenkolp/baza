import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BazaMailTemplate, BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';

@Component({
    selector: 'baza-docs-mail-template-doc',
    templateUrl: './baza-docs-mail-template-doc.component.html',
    styleUrls: ['./baza-docs-mail-template-doc.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDocsMailTemplateDocComponent {
    @Input() mailTemplate: BazaMailTemplate;

    get commonVariables(): Array<BazaMailTemplateVariable> {
        return (this.mailTemplate.variables || []).filter((next) => next.isDefault);
    }

    get templateVariables(): Array<BazaMailTemplateVariable> {
        return (this.mailTemplate.variables || []).filter((next) => !next.isDefault);
    }
}
