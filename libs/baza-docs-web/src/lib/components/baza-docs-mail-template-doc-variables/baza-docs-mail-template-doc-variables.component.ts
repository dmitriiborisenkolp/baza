import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BazaMailTemplateVariable } from '@scaliolabs/baza-core-shared';

@Component({
    selector: 'baza-docs-mail-template-doc-variables',
    templateUrl: './baza-docs-mail-template-doc-variables.component.html',
    styleUrls: ['./baza-docs-mail-template-doc-variables.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDocsMailTemplateDocVariablesComponent {
    @Input() variables: Array<BazaMailTemplateVariable>;
}
