import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    Input,
    OnDestroy,
    OnInit,
    QueryList,
    ViewChildren,
    ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';
import { BazaMailTemplate, naturalCompare, withTrailingSlash } from '@scaliolabs/baza-core-shared';
import { BazaNgWebEnvironment } from '@scaliolabs/baza-core-web';
import { BazaDocsDto, BazaDocsFileNode } from '@scaliolabs/baza-docs-shared';
import { NzOptionSelectionChange } from 'ng-zorro-antd/auto-complete/autocomplete-option.component';
import { NzSubMenuComponent } from 'ng-zorro-antd/menu';
import { BehaviorSubject, Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { BazaDocsMailTemplatesService } from '../../services/baza-docs-mail-templates.service';
import { BazaDocsSearchDocument, BazaDocsSearchService } from '../../services/baza-docs-search.service';

@Component({
    selector: 'baza-docs-nav',
    templateUrl: './baza-docs-nav.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDocsNavComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() public structure: BazaDocsDto;

    @ViewChildren(NzSubMenuComponent)
    public menus: QueryList<NzSubMenuComponent>;

    public form: FormGroup;
    public searchResults$ = new BehaviorSubject<Array<BazaDocsSearchDocument>>([]);
    public mailTemplates: Array<BazaMailTemplate> = [];
    public tags: Array<string> = [];

    private readonly ngOnDestroy$ = new Subject<void>();

    constructor(
        private readonly fb: FormBuilder,
        private readonly router: Router,
        private readonly environment: BazaNgWebEnvironment,
        private readonly domSanitizer: DomSanitizer,
        private readonly searchService: BazaDocsSearchService,
        private readonly mailTemplatesService: BazaDocsMailTemplatesService,
    ) {}

    ngOnInit(): void {
        this.form = this.fb.group({
            search: ['', [Validators.required]],
        });

        this.form
            .get('search')
            .valueChanges.pipe(distinctUntilChanged(), takeUntil(this.ngOnDestroy$))
            .subscribe((next) => {
                if ((next || '').toString().length > 0) {
                    const searchResults = this.searchService.search(next);

                    this.searchResults$.next(searchResults);
                }
            });

        this.mailTemplates = this.mailTemplatesService.mailTemplates;
        this.tags = this.mailTemplatesService.tags;
    }

    ngAfterViewInit(): void {
        const menus = this.menus.toArray();

        this.router.events.pipe(takeUntil(this.ngOnDestroy$)).subscribe((event) => {
            if (event instanceof NavigationEnd && event.url === '/') {
                menus.forEach((menu) => (menu.nzOpen = false));
            }
        });

        for (const menu of menus) {
            const links = menu.listOfNzMenuItemDirective
                .toArray()
                .map((item) => item.listOfRouterLinkWithHref.toArray().find((link) => !!link))
                .filter(Boolean);

            const isItemActive = links.some((link) =>
                this.router.isActive(link.urlTree, {
                    paths: 'exact',
                    fragment: 'exact',
                    matrixParams: 'ignored',
                    queryParams: 'ignored',
                }),
            );

            if (isItemActive) {
                menu.nzOpen = true;

                break;
            }
        }
    }

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    routerLink(node: BazaDocsFileNode): string | string[] {
        return [withTrailingSlash(node.path.slice(0, node.path.length - 3))];
    }

    get apiDocsUrl(): unknown {
        return this.domSanitizer.bypassSecurityTrustUrl(`${this.environment.current.apiEndpoint}/redoc`);
    }

    onSearchSelectionChange($event: NzOptionSelectionChange): void {
        const rawPath = ($event.source.nzValue || '').toString();
        const routePath = rawPath.slice(0, rawPath.length - 3);

        if ($event.isUserInput && routePath) {
            this.form.patchValue(
                {
                    search: '',
                },
                {
                    emitEvent: false,
                    onlySelf: true,
                },
            );

            this.router.navigate(['/', routePath]);
        }
    }

    mailTemplatesOfTag(tag: string): Array<BazaMailTemplate> {
        return this.mailTemplates.filter((next) => next.tag === tag).sort((a, b) => naturalCompare(a.title, b.title));
    }

    mailTemplateRouterLink(mailTemplate: BazaMailTemplate): string[] {
        return ['/mail-templates', mailTemplate.name];
    }
}
