import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BazaDocsRouteComponent } from './routes/baza-docs-route/baza-docs-route.component';
import { BazaDocsRouteNotFoundComponent } from './routes/baza-docs-route-not-found/baza-docs-route-not-found.component';
import { BazaDocsRouteResolve } from './routes/baza-docs-route/baza-docs-route.resolve';
import { BazaDocsRouteFileComponent } from './routes/baza-docs-route-file/baza-docs-route-file.component';
import { BazaDocsRouteFileResolve } from './routes/baza-docs-route-file/baza-docs-route-file.resolve';
import { BazaDocsRouteMailTemplateComponent } from './routes/baza-docs-route-mail-template/baza-docs-route-mail-template.component';
import { BazaDocsRouteMailTemplateResolve } from './routes/baza-docs-route-mail-template/baza-docs-route-mail-template.resolve';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                redirectTo: 'index',
                pathMatch: 'full',
            },
            {
                path: '',
                component: BazaDocsRouteComponent,
                resolve: {
                    bazaDocsRoute: BazaDocsRouteResolve,
                },
                children: [
                    {
                        path: 'not-found',
                        component: BazaDocsRouteNotFoundComponent,
                    },
                    {
                        path: '',
                        component: BazaDocsRouteFileComponent,
                        resolve: {
                            bazaDocsRouteFile: BazaDocsRouteFileResolve,
                        },
                    },
                    {
                        path: 'mail-templates/:name',
                        component: BazaDocsRouteMailTemplateComponent,
                        resolve: {
                            mailTemplate: BazaDocsRouteMailTemplateResolve,
                        },
                    },
                    {
                        path: ':path1',
                        component: BazaDocsRouteFileComponent,
                        resolve: {
                            bazaDocsRouteFile: BazaDocsRouteFileResolve,
                        },
                    },
                    {
                        path: ':path1/:path2',
                        component: BazaDocsRouteFileComponent,
                        resolve: {
                            bazaDocsRouteFile: BazaDocsRouteFileResolve,
                        },
                    },
                    {
                        path: ':path1/:path2/:path3',
                        component: BazaDocsRouteFileComponent,
                        resolve: {
                            bazaDocsRouteFile: BazaDocsRouteFileResolve,
                        },
                    },
                    {
                        path: ':path1/:path2/:path3/:path4',
                        component: BazaDocsRouteFileComponent,
                        resolve: {
                            bazaDocsRouteFile: BazaDocsRouteFileResolve,
                        },
                    },
                    {
                        path: ':path1/:path2/:path3/:path5',
                        component: BazaDocsRouteFileComponent,
                        resolve: {
                            bazaDocsRouteFile: BazaDocsRouteFileResolve,
                        },
                    },
                    {
                        path: '**',
                        component: BazaDocsRouteNotFoundComponent,
                    },
                ],
            },
            {
                path: '**',
                component: BazaDocsRouteNotFoundComponent,
            },
        ]),
    ],
    exports: [RouterModule],
})
export class BazaDocsRoutingModule {}
