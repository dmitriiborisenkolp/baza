import { NgModule } from '@angular/core';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';

const NG_ZORRO_MODULES = [
    NzLayoutModule,
    NzMenuModule,
    NzIconModule,
    NzGridModule,
    NzInputModule,
    NzAutocompleteModule,
    NzButtonModule,
    NzBreadCrumbModule,
    NzResultModule,
];

@NgModule({
    imports: [
        ...NG_ZORRO_MODULES,
    ],
    exports: [
        ...NG_ZORRO_MODULES,
    ],
})
export class BazaDocsNgZorroModule {}
