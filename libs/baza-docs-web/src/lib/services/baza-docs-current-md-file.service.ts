import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export type MdFilePath = string;

@Injectable({
    providedIn: 'root',
})
export class BazaDocsCurrentMdFileService {
    private _current$: BehaviorSubject<MdFilePath> = new BehaviorSubject<MdFilePath>(undefined);

    get current$(): Observable<MdFilePath> {
        return this._current$.asObservable();
    }

    get current(): MdFilePath {
        return this._current$.getValue();
    }

    set current(file: MdFilePath) {
        this._current$.next(file);
    }
}
