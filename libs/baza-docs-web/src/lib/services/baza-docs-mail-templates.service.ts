import { Injectable } from '@angular/core';
import { BazaMailTemplate, naturalCompare } from '@scaliolabs/baza-core-shared';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root',
})
export class BazaDocsMailTemplatesService {
    private _mailTemplates: Array<BazaMailTemplate> = [];

    get mailTemplates(): Array<BazaMailTemplate> {
        return this._mailTemplates;
    }

    set mailTemplates(mailTemplates: Array<BazaMailTemplate>) {
        this._mailTemplates = mailTemplates;
    }

    getMailTemplateByName(name: string): BazaMailTemplate {
        const result = this._mailTemplates.find((next) => next.name === name);

        if (!result) {
            throw new Error('Mail Template was not found');
        }

        return result;
    }

    get tags(): Array<string> {
        return _.uniq(this._mailTemplates.map((next) => next.tag)).sort((a, b) => naturalCompare(a, b));
    }
}
