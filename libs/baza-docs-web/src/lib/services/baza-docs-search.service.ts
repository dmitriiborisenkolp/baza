import { Injectable } from '@angular/core';
import { Index } from 'flexsearch';
import { BazaDocsDto, BazaDocsNode, BazaDocsNodeType } from '@scaliolabs/baza-docs-shared';

const SEARCH_LIMIT = 10;

export interface BazaDocsSearchDocument {
    path: string;
    title: string;
    directory: string;
}

@Injectable()
export class BazaDocsSearchService {
    private index = new Index({
        tokenize: 'full',
        context: true,
    });
    private documents: { [path: string]: BazaDocsSearchDocument } = {};

    setUp(docs: BazaDocsDto): void {
        window['INDEX'] = this.index;

        this.index.add(docs.index.path, docs.index.title);
        this.documents[docs.index.path] = {
            path: docs.index.path,
            title: docs.index.title,
            directory: '',
        };

        const deep = (nodes: Array<BazaDocsNode>, directory: string) => {
            for (const node of nodes) {
                if (node.type === BazaDocsNodeType.File) {
                    this.index.add(node.path, node.title);
                    this.documents[node.path] = {
                        directory,
                        path: node.path,
                        title: node.title,
                    };
                } else if (node.type === BazaDocsNodeType.Directory) {
                    deep(node.nodes, node.title);
                }
            }
        };

        deep(docs.nodes, '');
    }

    search(query: string): Array<BazaDocsSearchDocument> {
        const result = this.index.search({
            query,
            suggest: true,
            limit: SEARCH_LIMIT,
        });

        return result.map((next) => this.documents[next]);
    }
}
