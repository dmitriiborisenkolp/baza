import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { BazaDocsDto } from '@scaliolabs/baza-docs-shared';
import { combineLatest, Observable } from 'rxjs';
import { BazaDocsDataAccess } from '@scaliolabs/baza-docs-data-access';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { map, retryWhen, tap } from 'rxjs/operators';
import { BazaDocsSearchService } from '../../services/baza-docs-search.service';
import { BazaMailTemplate } from '@scaliolabs/baza-core-shared';
import { BazaDocsMailTemplatesService } from '../../services/baza-docs-mail-templates.service';
import { BazaMailDataAccess } from '@scaliolabs/baza-core-data-access';

export interface BazaDocsRouteResolveData {
    structure: BazaDocsDto;
    mailTemplates: Array<BazaMailTemplate>;
}

@Injectable()
export class BazaDocsRouteResolve implements Resolve<BazaDocsRouteResolveData> {
    constructor(
        private readonly dataAccess: BazaDocsDataAccess,
        private readonly dataAccessMailTemplates: BazaMailDataAccess,
        private readonly search: BazaDocsSearchService,
        private readonly mailTemplatesService: BazaDocsMailTemplatesService,
    ) {}

    resolve(): Observable<BazaDocsRouteResolveData> {
        const observables: [Observable<BazaDocsDto>, Observable<Array<BazaMailTemplate>>] = [
            this.dataAccess.structure(),
            this.dataAccessMailTemplates.mailTemplates(),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            map(([structure, mailTemplates]) => ({
                structure,
                mailTemplates,
            })),
            tap((result) => this.search.setUp(result.structure)),
            tap((result) => (this.mailTemplatesService.mailTemplates = result.mailTemplates)),
        );
    }
}
