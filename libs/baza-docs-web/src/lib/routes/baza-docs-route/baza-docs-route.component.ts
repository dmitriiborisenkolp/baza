import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BazaDocsRouteResolveData } from './baza-docs-route.resolve';
import { BazaDocsCurrentMdFileService } from '../../services/baza-docs-current-md-file.service';

interface State {
    isSidebarCollapsed: boolean;
}

@Component({
    templateUrl: './baza-docs-route.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDocsRouteComponent {
    public state: State = {
        isSidebarCollapsed: false,
    };

    constructor(
        private readonly activatedRoute: ActivatedRoute,
        public readonly currentMdFile: BazaDocsCurrentMdFileService,
    ) {}

    get resolvedData(): BazaDocsRouteResolveData {
        return this.activatedRoute.snapshot.data['bazaDocsRoute'];
    }

    toggleSideBar(): void {
        this.state = {
            ...this.state,
            isSidebarCollapsed: ! this.state.isSidebarCollapsed,
        };
    }

    scrollToTop(): void {
        if (window && window.scroll) {
            window.scroll({
                top: 0,
            });
        }
    }
}
