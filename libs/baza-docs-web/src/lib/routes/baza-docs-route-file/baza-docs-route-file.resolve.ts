import { Injectable } from '@angular/core';
import { BazaDocsErrorCodes, BazaDocsFileDto } from '@scaliolabs/baza-docs-shared';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { combineLatest, Observable, throwError } from 'rxjs';
import { BazaDocsDataAccess } from '@scaliolabs/baza-docs-data-access';
import { catchError, map, retryWhen, tap } from 'rxjs/operators';
import { genericRetryStrategy } from '@scaliolabs/baza-core-ng';
import { BazaError, isBazaErrorResponse, isEmpty } from '@scaliolabs/baza-core-shared';
import { BazaDocsCurrentMdFileService } from '../../services/baza-docs-current-md-file.service';

export interface BazaDocsRouteFileResolveData {
    file: BazaDocsFileDto;
}

@Injectable()
export class BazaDocsRouteFileResolve implements Resolve<BazaDocsRouteFileResolveData> {
    constructor(
        private readonly router: Router,
        private readonly dataAccess: BazaDocsDataAccess,
        private readonly currentMdFile: BazaDocsCurrentMdFileService,
    ) {}

    resolve(route: ActivatedRouteSnapshot): Observable<BazaDocsRouteFileResolveData> {
        const relativePath = [
            route.params['path1'],
            route.params['path2'],
            route.params['path3'],
            route.params['path4'],
            route.params['path5'],
        ].filter((p) => !! p).join('/');

        const observables: [
            Observable<BazaDocsFileDto>
        ] = [
            this.dataAccess.file({
                relativePath: isEmpty(relativePath)
                    ? undefined
                    : `${relativePath}.md`,
            }),
        ];

        return combineLatest(observables).pipe(
            retryWhen(genericRetryStrategy()),
            catchError((err) => {
                if (isBazaErrorResponse(err) && (err as BazaError).code === BazaDocsErrorCodes.BazaDocsMdFileNotFound) {
                    this.router.navigate(['/not-found'], {
                        skipLocationChange: true,
                    });
                }

                return throwError(err);
            }),
            map(([file]) => ({
                file,
            })),
            tap(() => this.currentMdFile.current = relativePath),
        );
    }
}
