import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnDestroy,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BazaDocsRouteFileResolveData } from './baza-docs-route-file.resolve';

import * as HighlightJs from 'highlight.js';
import * as MarkdownIt from 'markdown-it';
import { distinctUntilChanged, map, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

window['md'] = MarkdownIt;

@Component({
    templateUrl: './baza-docs-route-file.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDocsRouteFileComponent implements AfterViewInit, OnDestroy {
    @ViewChild('mdContainer') mdContainerRef: ElementRef<HTMLDivElement>;

    private readonly ngOnDestroy$: Subject<void> = new Subject<void>();

    constructor(private readonly cdr: ChangeDetectorRef, private readonly activatedRoute: ActivatedRoute) {}

    ngOnDestroy(): void {
        this.ngOnDestroy$.next();
    }

    ngAfterViewInit(): void {
        this.activatedRoute.data
            .pipe(
                map((next) => next['bazaDocsRouteFile']),
                tap(() =>
                    setTimeout(() => {
                        window?.scroll({
                            top: 0,
                        });
                    }),
                ),
                distinctUntilChanged(),
                takeUntil(this.ngOnDestroy$),
            )
            .subscribe((next: BazaDocsRouteFileResolveData) => {
                setTimeout(() => {
                    this.render();
                });

                this.cdr.markForCheck();
            });
    }

    get resolvedData(): BazaDocsRouteFileResolveData {
        return this.activatedRoute.snapshot.data['bazaDocsRouteFile'];
    }

    private render(): void {
        const defaults: any = {
            html: true,
            xhtmlOut: false,
            breaks: false,
            langPrefix: 'language-',
            linkify: true,
            typographer: true,
            _highlight: true,
            _strict: false,
            _view: 'html',
        };

        defaults.highlight = function (str, lang) {
            const esc = new MarkdownIt().utils.escapeHtml;

            if (lang && HighlightJs.default.getLanguage(lang)) {
                try {
                    return '<pre class="hljs"><code>' + HighlightJs.default.highlight(lang, str, true).value + '</code></pre>';
                } catch (__) {
                    return '';
                }
            } else {
                return '<pre class="hljs"><code>' + esc(str) + '</code></pre>';
            }
        };

        const mdInstance = new MarkdownIt(defaults);

        this.mdContainerRef.nativeElement.innerHTML = mdInstance.render(this.resolvedData.file.mdSource);
    }
}
