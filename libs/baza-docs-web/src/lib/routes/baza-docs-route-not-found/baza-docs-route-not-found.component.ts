import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';

@Component({
    templateUrl: './baza-docs-route-not-found.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDocsRouteNotFoundComponent {}
