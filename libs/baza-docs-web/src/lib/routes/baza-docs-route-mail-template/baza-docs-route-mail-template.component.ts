import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BazaMailTemplate } from '@scaliolabs/baza-core-shared';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
    templateUrl: './baza-docs-route-mail-template.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BazaDocsRouteMailTemplateComponent {
    constructor(private readonly activatedRoute: ActivatedRoute) {}

    get mailTemplate$(): Observable<BazaMailTemplate> {
        return this.activatedRoute.data.pipe(
            map((next) => next['mailTemplate']),
            tap(() =>
                setTimeout(() => {
                    window?.scroll({
                        top: 0,
                    });
                }),
            ),
        );
    }
}
