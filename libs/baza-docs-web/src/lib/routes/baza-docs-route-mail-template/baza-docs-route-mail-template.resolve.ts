import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { BazaMailTemplate } from '@scaliolabs/baza-core-shared';
import { BazaDocsMailTemplatesService } from '../../services/baza-docs-mail-templates.service';

@Injectable()
export class BazaDocsRouteMailTemplateResolve implements Resolve<BazaMailTemplate> {
    constructor(private readonly mailTemplateService: BazaDocsMailTemplatesService) {}

    resolve(route: ActivatedRouteSnapshot): BazaMailTemplate {
        return this.mailTemplateService.getMailTemplateByName(route.params['name']);
    }
}
