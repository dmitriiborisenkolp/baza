import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BazaDocsFileComponent } from './ui/baza-docs-file/baza-docs-file.component';
import { BazaDocsNavComponent } from './ui/baza-docs-nav/baza-docs-nav.component';
import { BazaDocsRouteComponent } from './routes/baza-docs-route/baza-docs-route.component';
import { BazaDocsRouteNotFoundComponent } from './routes/baza-docs-route-not-found/baza-docs-route-not-found.component';
import { BazaDocsRoutingModule } from './baza-docs-routing.module';
import { BazaDocsNgZorroModule } from './baza-docs-ng-zorro.module';
import { BazaDocsDataAccessModule } from '@scaliolabs/baza-docs-data-access';
import { BazaDocsRouteResolve } from './routes/baza-docs-route/baza-docs-route.resolve';
import { BazaDocsRouteFileComponent } from './routes/baza-docs-route-file/baza-docs-route-file.component';
import { BazaDocsRouteFileResolve } from './routes/baza-docs-route-file/baza-docs-route-file.resolve';
import { ReactiveFormsModule } from '@angular/forms';
import { BazaDocsSearchService } from './services/baza-docs-search.service';
import { BazaMailDataAccessModule } from '@scaliolabs/baza-core-data-access';
import { BazaDocsMailTemplateDocComponent } from './components/baza-docs-mail-template-doc/baza-docs-mail-template-doc.component';
import { BazaDocsRouteMailTemplateComponent } from './routes/baza-docs-route-mail-template/baza-docs-route-mail-template.component';
import { BazaDocsRouteMailTemplateResolve } from './routes/baza-docs-route-mail-template/baza-docs-route-mail-template.resolve';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { BazaDocsMailTemplateDocVariablesComponent } from './components/baza-docs-mail-template-doc-variables/baza-docs-mail-template-doc-variables.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BazaDocsRoutingModule,
        BazaDocsNgZorroModule,
        BazaDocsDataAccessModule,
        BazaMailDataAccessModule,
        NzTypographyModule,
    ],
    declarations: [
        BazaDocsRouteComponent,
        BazaDocsRouteNotFoundComponent,
        BazaDocsRouteFileComponent,
        BazaDocsFileComponent,
        BazaDocsNavComponent,
        BazaDocsMailTemplateDocComponent,
        BazaDocsRouteMailTemplateComponent,
        BazaDocsMailTemplateDocVariablesComponent,
    ],
    providers: [BazaDocsRouteResolve, BazaDocsRouteFileResolve, BazaDocsSearchService, BazaDocsRouteMailTemplateResolve],
})
export class BazaDocsWebModule {}
