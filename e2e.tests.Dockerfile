FROM gcr.io/scalio-199620/base-images/baza-e2e-tests-base-image:latest as node
WORKDIR /app
ARG NPM_TOKEN

COPY package.json yarn.lock .npmrc  ./
RUN yarn install --frozen-lockfile

COPY nx.json tsconfig.base.json jest.config.js jest.preset.js workspace.json ./

COPY apps ./apps
COPY libs/ ./libs
COPY envs ./envs
COPY envs/e2e.env ./env/.env
COPY  ./apps/api/ops/entrypoint.sh ./
COPY migrations/ ./migrations
COPY docs/ ./docs
COPY apps/cms/src/environments/environment.e2e.ts ./apps/cms/src/environments/environment.ts
COPY apps/docs/src/environments/environment.e2e.ts ./apps/docs/src/environments/environment.ts

CMD ["/bin/sh"]
