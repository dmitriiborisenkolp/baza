(put your PR description here)
(remove PR checklist items which are not relevant to the PR)

https://scalio.atlassian.net/browse/BAZA-NNNN

---

### **PR:**

-   [ ] PR name follows format: "BAZA-NNNN: scope(...): PR description"
-   [ ] PR has proper description and can be directly used as part of **Release Notes**
-   [ ] Link to JIRA Ticket provided in PR description and JIRA ID is updated
-   [ ] Breaking changes described in PR description

### **General:**

-   [ ] PR has changes in API

    -   [ ] CQRS Event Handlers wrapped with `cqrs` helper
    -   [ ] CQRS Command Handlers wrapped with `cqrsCommand` helper
    -   [ ] DTOs are covered with Swagger and Class-Validator decorators
    -   [ ] Typescript decorators are placed in correct order (`@ApiModelProperty`, class-validator decorators, `@IsNotEmpty` or `@IsOptional`)
    -   [ ] Magic numbers and other values are defined as configurable constants
    -   [ ] CMS API and Public API are not mixed in same controllers or services
    -   [ ] New API uses ULID instead of IDs as Primary Key
    -   [ ] New ULID Primary Keys are defined with our custom `@PrimaryUlidColumn`, instead of TypeOrm decorator.
    -   [ ] TS Docs added for new methods, constants, services or any other valuable resources
    -   [ ] (CMS API ONLY) All methods uses POST HTTP method
    -   [ ] (PUBLIC API ONLY) All methods follows generic conventions about using GET/POST/PUT HTTP methods
    -   [ ] Integration tests covering feature / bugfix are added, if it's possible
    -   [ ] API is covered with `@ApiModelProperty` decorators with proper description
    -   [ ] Optional fields have proper typings (`?` used) and `@ApiModelProperty({ required: false })` is set
    -   [ ] Regular activities ("watchers", i.e. any async processes which are running on interval basis) are wrapped with `EnvService.areWatchersAllowed` if-block

-   [ ] PR has changes in CMS
    -   [ ] Text resources are placed in i18n files
    -   [ ] Feature or fix is manually tested on local environment
    -   [ ] New CMS CRUD and Forms have unique IDs (CRUD and Forms default configuration can be updated by `BazaCrudInjectService` and `BazaFormBuilderInjectService` services)

### **Optional features:**

-   [ ] PR adds a new optional feature
    -   [ ] Feature has proper way to enable or disable it
    -   [ ] Documentation about new feature is added to Baza Docs portal
    -   [ ] If PR contributes new optional feature, API & CMS is correctly working when new optional feature is disabled

### **Complete:**

-   [ ] (OPTIONAL) Documentation added to Baza Docs portal
-   [ ] PR is posted to `proj-baza-web` channel
-   [ ] All checklist items are checked
