module.exports = {
    apps: [
        {
            name: 'baza-api',
            script: './dist/apps/api/main.js',
            args: '',
            cwd: '.',
            autorestart: true,
            watch: false,
            instances: 1,
            exec_mode: 'single',
            max_memory_restart: '4G',
            env: {
                NODE_ENV: '',
                BAZA_MASTER: '1',
            },
        },
        {
            name: 'baza-api',
            script: './dist/apps/api/main.js',
            args: '',
            cwd: '.',
            autorestart: true,
            watch: false,
            instances: 4,
            exec_mode: 'cluster',
            max_memory_restart: '4G',
            env: {
                NODE_ENV: '',
                BAZA_MASTER: '0',
            },
        },
    ],
};
