FROM cypress/browsers:node14.16.0-chrome89-ff86 as node
WORKDIR /app
ARG NPM_TOKEN

COPY package.json yarn.lock .npmrc  ./
RUN yarn install --frozen-lockfile
RUN yarn global add cross-env

RUN apt-get --allow-releaseinfo-change update && apt-get install ffmpeg -y && rm -rf /var/lib/apt/lists/*