# Baza

`Baza` repository includes independent publishable libraries, generated with [Nx](https://nx.dev).

## Generate new libraries

`nx g @nrwl/nest:lib <nest-lib> --publishable --importPath={@myorg/mylib}` - Create Nest.js library

`nx g @nrwl/angular:lib <angular-lib> --publishable --importPath={@myorg/mylib}` - Create Angular library

`--publishable` flag adds package.json file to your library folder and build configuration to workspace.json.
Every new library must have `src/index.ts` file which will contain all library exports.

## Build all libraries

Run `yarn build:baza-core`

## Bump versions of all libraries

Run `node ./baza-bump-version.js {version}`

## Publish all libraries

Run `./publish-baza-core.sh`

## Publish a single library

```shell
nx build baza-core-shared # (or yarn build:baza-core-shared)
cd dist/libs/baza-core-shared
npm publish
```

## Use published libraries

Run `yarn add @scaliolabs/baza-core-shared` (Be sure that NPM_TOKEN is set correctly https://docs.npmjs.com/logging-in-to-an-npm-enterprise-registry-from-the-command-line)

```ts
import { generateRandomHexString } from '@scaliolabs/baza-core-shared';

const hash = generateRandomHexString(8);
```

## How to start API, CMS and Sandbox on local environment

1. `yarn`
2. `docker-compose up -d`
3. `cp envs/local.env envs/.env`
4. `yarn api:start` for API
5. `yarn cms:start` for CMS ([http://localhost:4201]())
6. `yarn sandbox:start` for Sandbox ([http://localhost:4202]())

## How to run integration tests

1. Start Docker-Compose for E2E environment: `docker-compose -f docker-compose.e2e-local.up`
2. Start API with E2E environment: `yarn api:e2e`
3. Run integration tests: `yarn api:tests:integration-tests` (with watch mode) or `yarn api:tests:integration-tests:once` (all tests, w/o watch mode)

## Cypress

-   You will need to run `npx cypress verify` for OS X environment

Note: Due of @nrwl/node issues this command cannot be used for CI/CD. Additionally,

it does not kills process properly after finishing reverting migrations.

## Server-side Rendering (Sandbox)

-   For development purposes with live reloading: `yarn sandbox:dev:ssr`.
-   To build the application in the ssr mode: `yarn sandbox:build:ssr:${env}` where `env` is an environment (test/stage/production).
-   To serve a build app: `yarn sandbox:serve:ssr`.

## Prerender (Sandbox)

Prerenders specified routers at build time.

-   For static routes use a schema field `prerender.options.routes`.
-   For dynamic routes update a script `apps/sandbox/prerender-dynamic-routes/routes.script.ts`.
-   `yarn sandbox:prerender` by default uses production env.
-   To specify a different env run `yarn sandbox:prerender:test` or `yarn sandbox:prerender:stage` accordingly.
-   `yarn sandbox:prerender:dynamic-routes` generates dynamic routes and add them to the `apps/sandbox/prerender-dynamic-routes/routes.txt` file that is specified in schema. This script is included in `yarn sandbox:prerender` so there is no need to run it additionally.
