# Using Store Information From Libraries

BAZA purchase and verification flow libraries allow us to access store information e.g. "Verification" related information in any component of application with the help of store variables.

<br/>

## Example: Accessing Verification Information In Account Component

This example talks about how we can access the "Verification" information from store in the "Account Component" (Custom) in the application.

Following are the things required to do, in order:

-   Include `VerificationResolver` in the routing module and specifically for the relevant page/module route
-   Include `VerificationStateModule` in the module of relevant page/component
-   Use NGXS selectors and get `verification$` (Verification Observable) through `VerificationState` object in the relevant component
-   Use the `verification$` to get any information about verification object

Now, let's look at some samples for each of the step.

<br/>

### Step 1 - Using VerificationResolver

```typescript
    {
        path: 'account',
        canActivate: [JwtVerifyGuard, JwtRequireAuthGuard],
        loadChildren: () => import('@scaliolabs/web/feature-account').then((m) => m.AccountModule),
            resolve: { verification: VerificationResolver },
    },
```

<br/>

### Step 2 - Importing VerificationStateModule in Account Module

```typescript
import { VerificationStateModule } from '@scaliolabs/baza-nc-web-verification-flow';

@NgModule({
    imports: [VerificationStateModule],
})
export class AccountModule {}
```

### Step 3 - Getting Access to Verification Observable Through Verification State

```typescript

import { Verification, VerificationState } from '@scaliolabs/baza-nc-web-verification-flow';

@UntilDestroy()
@Component({
    verification$ = this.store.select(VerificationState.verification);
)}

```

### Step 4 - Using Verification Information

```typescript
<span *ngIf="(verification$ | async)?.personalInformation?.hasSsn">
                    <a
                        nz-popover
                        nzPopoverPlacement="bottom"
                        nzPopoverTrigger="click"
                        nzPopoverContent="Connect with a checking or savings bank account to withdraw distributions payments.">
                        <i class="icon icon-info-circle icon-wt"></i>
                    </a>
                    <a
                        class="portfolio-total__add-bank"
                        [routerLink]="['/account', 'payout']">
                        Add Payout Method
                    </a>
                </span>
                <span *ngIf="!(verification$ | async)?.personalInformation?.hasSsn">
                    <a
                        nz-popover
                        nzPopoverPlacement="bottom"
                        nzPopoverTrigger="click"
                        [nzPopoverContent]="tooltipTemplate">
                        <i class="icon icon-info-circle icon-wt"></i>
                    </a>
                    <a class="portfolio-total__add-bank">Non US Residents</a>
                </span>

```
