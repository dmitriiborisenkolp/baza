# Customizing Dwolla Verification Flow i18n Translations

BAZA Verification Flow Library (Dwolla) now has support for i18n label-by-label translations.

There are default i18n configurations which are already applied within libraries but can be easily overrided by providing custom i18n translations at app. level. This works for labels including titles, description, form labels, validation errors and notifications.

## Example on How To Override i18n Translations

Let's take an example where we would like to override the default i18n translations for Dwolla Verification Flow library. This will include a consolidated i18n file which will have configurations for parent, info & investor components respectively.

Before starting, make sure that `@ngx-translate/core` plugin is installed for project.

### Step 1

Create i18n translation files for each of the components for which translations are required to be overriden. For each specific translation file structure/format (default), check the following links as they have the sample code for each:

-   [Parent/Main Component](web-development/i18n/dwolla-vf/02-vf-i18n)
-   [Check Component](web-development/i18n/dwolla-vf/03-vf-check-i18n)
-   [Stepper Info Component](web-development/i18n/dwolla-vf/04-vf-info-i18n)
-   [Stepper Investor Component](web-development/i18n/dwolla-vf/05-vf-investor-i18n)
-   [Additional Document Required Modal](web-development/i18n/dwolla-vf/06-vf-file-upload-i18n)

### Step 2

Create the i18n override config file in app level e.g. within "feature-purchase" folder. Here's a sample format:

```typescript
import { DwollaVFFileUploadModalEnI18n } from './vf-file-upload-modal-i18n';
import { DwollaVFInfoEnI18nOverride } from './vf-info-override.i18n';
import { DwollaVFInvestorEnI18nOverride } from './vf-investor-override.i18n';
import { DwollaVFEnI18nOverride } from './vf-override.18n';

// override translations can be provided optionally
export const bazaDwollaWebVFEnI18nOverride = {
    dwvf: {
        vf: DwollaVFEnI18nOverride,
        check: DwollaVFCheckEnI18nOverride,
        info: DwollaVFInfoEnI18nOverride,
        investor: DwollaVFInvestorEnI18nOverride,
        fileUpload: DwollaVFFileUploadEnI18nOverride,
    },
};
```

**Note:**
Notice all of the above files are actually override files that would resemble the same structure as we have in default configurations, linked above.

### Step 3

Make sure to configure the translations for application level, within `app.component.ts` file as per following sample format. It's important to note that passing the overriden configurations are optional:

```typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { TranslateService } from '@ngx-translate/core';
import * as deepmerge from 'deepmerge';
import { StartApp } from '@scaliolabs/sandbox-web/data-access';

//default Dwolla VF i18n file from library
import { bazaDwollaWebVFEnI18n } from '@scaliolabs/baza-dwolla-web-verification-flow';
//overriden Dwolla VF i18n file from library (optional)
import { bazaDwollaWebVFEnI18nOverride } from '@scaliolabs/sandbox-web/feature-verification-dwolla';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    constructor(private readonly translate: TranslateService, private readonly store: Store) {
        this.configureI18nOverrides();

        // start the app
        this.store.dispatch(new StartApp());
    }

    configureI18nOverrides() {
        const EnI18n = deepmerge.all([bazaDwollaWebVFEnI18n, bazaDwollaWebVFEnI18nOverride]);
        this.translate.setTranslation('en', EnI18n);
    }
}
```

## Notes:

-   Some default validation error messages are applied by default e.g. "Required" validation message always follows the format `{{ field }} is required`. However, if validators are to be overriden, use the following format, e.g. for "First Name" field.

```typescript
export const DwollaVFInfoEnI18nOverride = {
    title: 'Personal Information',
    descr: 'We collect your information to perform Know Your Customer (KYC) and Anti-Money Laundering (AML) compliance. Please provide correct legal information to verify your account.',
    subtitle: 'You may only use characters A to Z, 0 to 9, and simple punctuation to fill in information.',
    form: {
        fields: {
            firstName: {
                label: 'First Name',
                validators: {
                    required: 'First Name is required - override!',
                },
            },
        },
    },
};
```

-   Apart from just default "Required" validation, custom validations can also be added easily e.g. for "Date of Birth" field, we can have the following structure:

```typescript
export const DwollaVFInfoEnI18nOverride = {
    title: 'Personal Information',
    descr: 'We collect your information to perform Know Your Customer (KYC) and Anti-Money Laundering (AML) compliance. Please provide correct legal information to verify your account.',
    subtitle: 'You may only use characters A to Z, 0 to 9, and simple punctuation to fill in information.',
    form: {
        fields: {
            dateOfBirth: {
                label: 'Date of Birth',
                validators: {
                    isAdult: 'Investor should be {{ age }} or older',
                },
            },
        },
    },
};
```
