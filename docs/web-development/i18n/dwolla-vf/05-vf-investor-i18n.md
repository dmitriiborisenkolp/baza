# JSON structure for Dwolla VF Investor Component

Here's the i18n translation object structure for Dwolla VF Investor Component:

```typescript
export const DwollaVFInvestorEnI18n = {
    title: 'Investor Profile',
    form: {
        fields: {
            isAccreditedInvestor: {
                label: 'Are you an accredited investor?',
                validators: {
                    required: 'Accreditor investor option is required',
                },
                yesOption: {
                    label: 'Yes',
                    descr: 'I have an annual income greater than $200k, joint income greater than $300k, or net worth greater than $1M (excluding primary residence)',
                },
                noOption: {
                    label: 'No',
                    descr: 'I acknowledge that my investment(s) in this offering is less than 10% of my net worth or annual gross income.',
                },
            },
            isAssociatedWithFINRA: {
                label: 'Are you or anyone in your household associated with a FINRA member, organization or the SEC?',
                validators: {
                    required: 'FINRA association option is required',
                },
                yesOption: {
                    label: 'Yes',
                    descr: 'I have an annual income greater than $200k, joint income greater than $300k, or net worth greater than $1M (excluding primary residence)',
                },
                noOption: {
                    label: 'No',
                    descr: 'I acknowledge that my investment(s) in this offering is less than 10% of my net worth or annual gross income.',
                },
            },
            dwollaConsentProvided: {
                validators: {
                    required: 'Please provide the Dwolla consent before proceeding',
                },
                descr: `By checking this box you agree with our <a data-link="tos"></a> and <a data-link="privacy"></a>, as well as our partner Dwolla's  <a data-link="dwTos"></a> and <a data-link="dwPrivacy"></a>.`,
                tosLinkConfig: {
                    key: 'tos',
                    text: 'Terms of Service',
                },
                privacyLinkConfig: {
                    key: 'privacy',
                    text: 'Privacy Policy',
                },
                dwTosLinkConfig: {
                    key: 'dwTos',
                    text: 'Terms of Service',
                },
                dwPrivacyLinkConfig: {
                    key: 'dwPrivacy',
                    text: 'Privacy Policy',
                },
            },
        },
    },
    steps: {
        backBtnLabel: 'Back',
        confirmBtnLabel: 'Confirm',
    },
};
```
