# JSON structure for Dwolla Bank Payout Component

Here's the i18n translation object structure for Dwolla Bank Payout Component

```ts
export const DwollaPayoutBankEnI18nOverride = {
    title: 'Bank Account',
    hint: 'Used to withdraw funds from your wallet',
    alerts: {
        withdrawalSupport: {
            label: 'Reach out to us at <a data-link="withdrawalLink">{{ withdrawalLink }}</a> to get instructions on withdrawal.',
            linkConfig: {
                key: 'withdrawalLink',
                text: 'baza-test-contact-us@scal.io',
            },
        },
        linkBank: 'You need to have linked bank account to withdraw from your balance.',
        walletSupport: {
            label: 'There was a failed attempt to create a Wallet. Please <a data-link="walletLink"></a> for technical support.',
            linkConfig: {
                key: 'walletLink',
                text: 'contact us',
            },
        },
    },
    actions: {
        update: 'Update',
    },
};
```
