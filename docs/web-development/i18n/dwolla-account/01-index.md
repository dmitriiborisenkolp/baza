# Customizing Dwolla Account i18n Translations

BAZA Account (Dwolla) now has support for i18n label-by-label translations.

There are default i18n configurations which are already applied within libraries but can be easily overrided by providing custom i18n translations at app. level. This works for labels including titles, description, form labels, validation errors and notifications.

## Example on How To Override i18n Translations

Let's take an example where we would like to override the default i18n translations for Dwolla Purchase Flow library. This will include a consolidated i18n file which will have configurations for parent, info & investor components respectively.

Before starting, make sure that `@ngx-translate/core` plugin is installed for project.

### Step 1

Create i18n translation files for each of the components for which translations are required to be overriden. For each specific translation file structure/format (default), check the following links as they have the sample code for each:

-   [Account Balance](web-development/i18n/dwolla-account/02-account-balance)
-   [Bank Account](web-development/i18n/dwolla-account/03-bank-account)
-   [Credit Card](web-development/i18n/dwolla-account/04-credit-card)
-   [Bank Account Payout](web-development/i18n/dwolla-account/05-payout-bank-account)

### Step 2

Create the i18n override config file in app level e.g. within "feature-account" folder. Here's a sample format:

```ts
import { DwollaPaymentBankEnI18n } from './dwac-payment-bank.i18n';
import { DwollaPaymentCardEnI18n } from './dwac-payment-card.i18n';
import { DwollaPaymentEnI18n } from './dwac-payment.i18n';
import { DwollaPayoutBankEnI18n } from './dwac-payout-bank.i18n';
import { DwollaPayoutEnI18n } from './dwac-payout.i18n';
import { DwollaPaymentBalanceEnI18n } from './dwac-payment-balance.i18n';

export const BazaDwollaWebAccountEnI18nOverride = {
    dwacc: {
        payment: {
            title: 'Override - Title',
            popover: {
                content: 'Override - Popover description',
            },
            balance: DwollaPaymentBalanceEnI18nOverride,
            bank: DwollaPaymentBankEnI18nOverride,
            card: DwollaPaymentCardEnI18nOverride,
        },
        payout: {
            title: 'Override - Title',
            popover: {
                content: 'Override - Popover description',
            },
            bank: DwollaPayoutBankEnI18nOverride,
        },
    },
};
```

**Note:**
Notice all of the above files are actually override files that would resemble the same structure as we have in default configurations, linked above.

### Step 3

Make sure to configure the translations for application level, within `app.component.ts` file as per following sample format. It's important to note that passing the overriden configurations are optional:

```ts
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    constructor(private readonly translate: TranslateService) {
        this.configureI18nOverrides();
    }

    configureI18nOverrides() {
        const EnI18n = deepmerge.all([BazaDwollaWebAccountEnI18n, BazaDwollaWebAccountEnI18nOverride]);
        this.translate.setTranslation('en', EnI18n);
    }
}
```
