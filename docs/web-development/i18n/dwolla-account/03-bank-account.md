# JSON structure for Dwolla Bank Component

Here's the i18n translation object structure for Dwolla Bank Component

```ts
export const DwollaPaymentBankEnI18nOverride = {
    title: 'Bank Account',
    hint: 'Used to purchase shares',
    details: {
        name: 'Account holder name:',
        number: 'Account number:',
        type: 'Account type:',
    },
    actions: {
        update: 'Update',
    },
};
```
