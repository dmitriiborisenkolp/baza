# JSON structure for Dwolla Card Component

Here's the i18n translation object structure for Dwolla Card Component

```ts
export const DwollaPaymentCardEnI18nOverride = {
    title: 'Card',
    details: {
        number: 'Card number:',
        expiring: 'Expiring:',
        fee: 'Card processing fee',
    },
    alerts: {
        limit: 'Purchases above {{ limit }} require the use of a linked bank account.',
    },
    actions: {
        update: 'Update',
    },
};
```
