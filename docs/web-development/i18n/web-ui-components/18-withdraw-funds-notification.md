# JSON structure for Web UI "Withdraw Funds" Notification Component

Here's the i18n translation object structure for Web UI "Withdraw Funds" Notification Component

```ts
export const WebUiWithdrawFundsNotificationEnI18nOverride = {
    title: 'Funds Withdrawal Initiated - override',
    content: 'Your funds will be available shortly, and we will notify you through email. - override',
    actions: {
        accept: 'Got it - override',
    },
};
```
