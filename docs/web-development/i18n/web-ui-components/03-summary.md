# JSON structure for Web UI Summary Component

Here's the i18n translation object structure for Web UI Summary Component:

```ts
export const WebUiSummaryEnI18nOverride = {
    title: 'Summary',
    numOfShares: 'Number of shares',
    pricePerShare: 'Price per share',
    transactionFee: 'Transaction fee',
    transactionFeePopover:
        'This is a due diligence fee paid to cover costs associated with investigating, evaluating and purchasing underlying portfolio assets',
    paymentMethodFee: 'Payment method fee',
    total: 'Total',
};
```
