# JSON structure for Web UI "Add Funds" Component

Here's the i18n translation object structure for Web UI "Add Funds" Component

```ts
export const WebUiAddFundsEnI18nOverride = {
    form: WebUiAddFundsFormEnI18nOverride,
    modal: WebUiAddFundsModalEnI18nOverride,
    notification: WebUiAddFundsNotificationEnI18nOverride,
};
```

-   [Add Funds Form](web-development/i18n/web-ui-components/12-add-funds-form)
-   [Add Funds Modal](web-development/i18n/web-ui-components/13-add-funds-modal)
-   [Add Funds Notification](web-development/i18n/web-ui-components/14-add-funds-notification)
