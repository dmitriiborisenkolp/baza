# Customizing Web UI i18n Translations

BAZA Web UI components Library now has support for i18n label-by-label translations.

There are default i18n configurations which are already applied within libraries but can be easily overrided by providing custom i18n translations at app. level. This works for labels including titles, description, form labels, validation errors and notifications.

## Example on How To Override i18n Translations

Let's take an example where we would like to override the default i18n translations for Dwolla Web UI components library. This will include a consolidated i18n file which will have configurations.

Before starting, make sure that `@ngx-translate/core` plugin is installed for project.

### Step 1

Create i18n translation files for each of the components for which translations are required to be overriden. For each specific translation file structure/format (default), check the following links as they have the sample code for each:

-   [Account balance card](web-development/i18n/web-ui-components/02-account-balance-card)
-   [Summary](web-development/i18n/web-ui-components/03-summary)
-   [Payment Bank](web-development/i18n/web-ui-components/04-payment-bank)
-   [Payment Bank Modal](web-development/i18n/web-ui-components/05-payment-bank-modal)
-   [Bank Details](web-development/i18n/web-ui-components/06-bank-details)
-   [Payment Card](web-development/i18n/web-ui-components/07-payment-card)
-   [Payment Card Modal](web-development/i18n/web-ui-components/08-payment-card-modal)
-   [Card Details](web-development/i18n/web-ui-components/09-card-details)
-   [Personal](web-development/i18n/web-ui-components/10-personal)
-   [Add funds](web-development/i18n/web-ui-components/11-add-funds)
-   [Withdraw funds](web-development/i18n/web-ui-components/15-withdraw-funds)

### Step 2

Create the i18n override config file in app level. Here's a sample format:

```ts
// override translations can be provided optionally
export const BazaWebUiEnI18nOverride = {
    uic: {
        balance: WebUiAccountBalanceCardEnI18nOverride,
        summary: WebUiSummaryEnI18nOverride,
        paymentBank: DwollaPaymentBankAccountEnI18nOverride,
        paymentBankModal: DwollaPaymentBankAccountModalEnI18nOverride,
        bankDetails: WebUiBankDetailsEnI18nOverride,
        paymentCard: DwollaPaymentCardEnI18nOverride,
        paymentCardModal: DwollaPaymentCardModalEnI18nOverride,
        cardDetails: WebUiCardDetailsEnI18nOverride,
        personal: WebUiPersonalInfoEnI18nOverride,
        addFunds: {
            form: WebUiAddFundsFormEnI18nOverride,
            modal: WebUiAddFundsModalEnI18nOverride,
            notification: WebUiAddFundsNotificationEnI18nOverride,
        },
        withdrawFunds: {
            form: WebUiWithdrawFundsFormEnI18nOverride,
            modal: WebUiWithdrawFundsModalEnI18nOverride,
            notification: WebUiWithdrawFundsNotificationEnI18nOverride,
        },
    },
};
```

**Note:**
Notice all of the above files are actually override files that would resemble the same structure as we have in default configurations, linked above.

### Step 3

Make sure to configure the translations for application level, within `app.component.ts` file as per following sample format. It's important to note that passing the overriden configurations are optional:

```ts
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    constructor(private readonly translate: TranslateService) {
        this.configureI18nOverrides();
    }

    configureI18nOverrides() {
        const EnI18n = deepmerge.all([BazaWebUiEnI18n, BazaWebUiEnI18nOverride]);
        this.translate.setTranslation('en', EnI18n);
    }
}
```
