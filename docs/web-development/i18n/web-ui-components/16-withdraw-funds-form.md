# JSON structure for Web UI "Withdraw Funds" Form Component

Here's the i18n translation object structure for Web UI "Withdraw Funds" Form Component

```ts
export const WebUiWithdrawFundsFormEnI18nOverride = {
    fields: {
        amount: {
            label: 'Withdrawal Amount - override',
            validators: {
                required: 'Please enter the amount to be withdrawn - override',
            },
        },
    },
    alerts: {
        withdrawalError:
            'The funds were not withdrawn from the account. <br/> Please try again later or reach out to <a href="{{ link }}"> Technical support - override',
    },
};
```
