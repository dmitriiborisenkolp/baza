# JSON structure for Web UI Payment Bank Component

Here's the i18n translation object structure for Web UI Payment Bank Component:

```ts
export const WebUiBankAccountEnI18nOverride = {
    addMode: {
        plaidLinking: 'Link Bank Account - Override',
        manualLinking: 'Bank Account - Override',
    },
    editMode: {
        account: {
            name: 'Account holder name: - Override',
            number: 'Account number: - Override',
            type: 'Account type: - Override',
        },
    },
};
```
