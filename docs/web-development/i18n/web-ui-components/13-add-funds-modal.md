# JSON structure for Web UI "Add Funds" Modal Component

Here's the i18n translation object structure for Web UI "Add Funds" Modal Component

```ts
export const WebUiAddFundsModalEnI18nOverride = {
    title: 'Transfer Funds To Account Balance - Override',
    cashIn: {
        heading: 'Transfer From - Override',
        actions: {
            edit: 'Edit - Override',
        },
    },
    actions: {
        transfer: 'Transfer Funds - Override',
    },
};
```
