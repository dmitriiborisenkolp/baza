# JSON structure for Web UI "Withdraw Funds" Component

Here's the i18n translation object structure for Web UI "Withdraw Funds" Component:

```ts
export const WebUiWithdrawFundsEnI18nOverride = {
    form: WebUiWithdrawFundsFormEnI18nOverride,
    modal: WebUiWithdrawFundsModalEnI18nOverride,
    notification: WebUiWithdrawFundsNotificationEnI18nOverride,
};
```

-   [Withdraw Funds Form](web-development/i18n/web-ui-components/16-withdraw-funds-form)
-   [Withdraw Funds Modal](web-development/i18n/web-ui-components/17-withdraw-funds-modal)
-   [Withdraw Funds Notification](web-development/i18n/web-ui-components/18-withdraw-funds-notification)
