# JSON structure for Web UI "Withdraw Funds" Modal Component

Here's the i18n translation object structure for Web UI "Withdraw Funds" Modal Component

```ts
export const WebUiWithdrawFundsModalEnI18nOverride = {
    title: 'Withdraw Funds - override',
    subtitle: 'Withdrawal Balance: {{ amount }} - override',
    cashIn: {
        heading: 'Withdraw To - override',
        actions: {
            edit: 'Edit - override',
        },
    },
    actions: {
        transfer: 'Withdraw Funds - override',
    },
};
```
