# JSON structure for Web UI "Add Funds" Notification Component

Here's the i18n translation object structure for Web UI "Add Funds" Notification Component

```ts
export const WebUiAddFundsNotificationEnI18nOverride = {
    title: 'Funds Transfer Initiated - Override',
    content: 'This transfer has been initiated. Your funds will be available shortly, and we will notify you through email. - Override',
    actions: {
        accept: 'Got it - Override',
    },
};
```
