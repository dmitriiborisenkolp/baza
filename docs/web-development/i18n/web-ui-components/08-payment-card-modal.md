# JSON structure for Web UI Payment Card Modal Component

Here's the i18n translation object structure for Web UI Payment Card Modal Component:

```ts
export const WebUiPaymentCardModalEnI18nOverride = {
    title: 'Card Details - Override',
    actions: {
        add: 'Add Card - Override',
    },
};
```
