# JSON structure for Web UI Account Balance Card Component

Here's the i18n translation object structure for Web UI Account Balance Card Component:

```ts
export const WebUiAccountBalanceCardEnI18nOverride = {
    title: 'Account Balance',
    prefix: 'Current balance:',
    hint: 'Fund your account',
    actions: {
        withdraw: 'Withdraw',
        addFunds: 'Add Funds',
    },
};
```
