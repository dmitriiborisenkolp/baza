# JSON structure for Web UI Payment Bank Modal Component

Here's the i18n translation object structure for Web UI Payment Bank Modal Component:

```ts
export const WebUiBankAccountModalEnI18nOverride = {
    title: 'Bank Details - Override',
    actions: {
        add: 'Add Bank Account - Override',
    },
};
```
