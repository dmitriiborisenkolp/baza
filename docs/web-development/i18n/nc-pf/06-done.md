# JSON structure for NC PF Done Component

Here's the i18n translation object structure for NC PF Done Component:

```ts
export const NcDoneEnI18nOverride = {
    title: 'Your order has been submitted!',
    descr: 'We are verifying the order and will notify you when it has been completed. This typically takes 24-48 hours.',
    support: {
        descr: 'If you have any questions please reach out via',
        linkText: 'Contact Us',
    },
    actions: {
        proceedToPortfolio: 'Proceed to my portfolio',
    },
};
```
