# JSON structure for NC PF - Payment Edit

To customize the translations for the payment edit components in NC, you can add the `pymtEdit.account`, `pymtEdit.add`, `pymtEdit.list`, and `pymtEdit.update` keys to the already created object `BazaNCWebPFEnI18nOverride`. Here's an example:

```ts
export const NcPaymentEditEnI18nOverride = {
    account: {
        bank: {
            title: 'Update Bank Account',
            description:
                'You are about to begin adding a new bank account. If you complete this process, your current payment and dividend method account will be replaced.',
            actions: {
                add: 'Add New Bank Account',
            },
        },
        card: {
            title: 'Update Credit Card',
            description:
                'You are about to begin adding a new сredit or debit сard. If you complete this process, your current card will be replaced.',
            actions: {
                add: 'Add New Card',
            },
        },
    },
    add: {
        bank: {
            title: 'Bank Details - Override',
            actions: {
                add: 'Add Bank Account',
            },
        },
        card: {
            title: 'Card Details - Override',
            actions: {
                add: 'Add Card',
            },
        },
    },
    list: {
        title: {
            singular: 'Payment Method',
            plural: 'Payment Methods',
        },
        bank: {
            title: 'Bank Account',
        },
        card: {
            title: 'Credit Сard',
            feeSuffix: 'Card processing fee',
            limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account',
        },
        actions: {
            update: 'Update Payment Method',
        },
    },
    update: {
        title: 'Update Payment Method',
        bank: {
            title: 'Bank Account',
            plaid: {
                title: 'Link bank account',
            },
            manual: {
                title: 'Manually add bank details',
            },
        },
        card: {
            title: 'Card',
            manual: {
                title: 'Add card',
                feeSuffix: 'Fee',
            },
            limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account',
        },
    },
};
```
