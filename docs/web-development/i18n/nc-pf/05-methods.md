# JSON structure for NC PF Methods Component

Here's the i18n translation object structure for NC PF Methods Component:

```ts
export const NcMethodsEnI18nOverride = {
    title: {
        bank: 'Bank Account',
        card: 'Card',
        generic: 'Payment Method',
    },
    tooltip: {
        caption:
            'This is your Payment Method. You can add a new Bank Account or Credit Card or switch between your linked payment methods.',
        actions: {
            edit: 'Edit',
        },
    },
    bank: {
        info: 'Your bank account will be utilized to receive dividends',
    },
};
```
