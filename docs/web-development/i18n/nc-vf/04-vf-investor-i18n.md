# JSON structure for NC VF Investor Component

Here's the i18n translation object structure for NC VF Investor Component:

```typescript
export const NCVFInvestorEnI18n = {
    title: 'Investor Profile',
    form: {
        fields: {
            isAccreditedInvestor: {
                label: 'Are you an accredited investor?',
                validators: {
                    required: 'Accreditor investor option is required',
                },
                yesOption: {
                    label: 'Yes',
                    descr: 'I have an annual income greater than $200k, joint income greater than $300k, or net worth greater than $1M (excluding primary residence)',
                },
                noOption: {
                    label: 'No',
                    descr: 'I acknowledge that my investment(s) in this offering is less than 10% of my net worth or annual gross income.',
                },
            },
            isAssociatedWithFINRA: {
                label: 'Are you or anyone in your household associated with a FINRA member, organization or the SEC?',
                validators: {
                    required: 'FINRA association option is required',
                },
                yesOption: {
                    label: 'Yes',
                    descr: 'I have an annual income greater than $200k, joint income greater than $300k, or net worth greater than $1M (excluding primary residence)',
                },
                noOption: {
                    label: 'No',
                    descr: 'I acknowledge that my investment(s) in this offering is less than 10% of my net worth or annual gross income.',
                },
            },
            currentAnnualHouseholdIncome: {
                subheading: 'What is your current annual household income (including your spouse)?',
                label: 'Income ($)',
                placeholder: '$0',
                validators: {
                    required: 'Monthly income is required',
                },
            },
            netWorth: {
                subheading: 'What is your net worth (excluding primary residence)?',
                label: 'Worth ($)',
                placeholder: '$0',
                validators: {
                    required: 'Net worth is required',
                },
            },
        },
    },
    steps: {
        backBtnLabel: 'Back',
        confirmBtnLabel: 'Confirm',
    },
};
```
