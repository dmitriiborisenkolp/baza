# Setting Up Application For i18n Translations

Baza now has support for label-by-label translations for VF (Dwolla, NC), PF (Dwolla, NC) and Account components (Dwolla).

### Note:

Before we can implement i18n translation overrides, please setup your application to integrate the default translations. That's required otherwise default translations from libraries would not work.

It's important to note here that e.g. `BazaDwollaWebAccountEnI18n` is only required if project is using Dwolla account components (payment & payout methods). If project is still on NC, please only include NC specific default translations.

**Sample code for setting up default i18n translations in `app.component.ts` file:**

```typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { TranslateService } from '@ngx-translate/core';
import { StartApp } from '@scaliolabs/sandbox-web/data-access';
import * as deepmerge from 'deepmerge';

// Web UI
import { BazaWebUiEnI18n } from '@scaliolabs/baza-web-ui-components';
// Dwolla Account i18n
import { BazaDwollaWebAccountEnI18n } from '@scaliolabs/baza-dwolla-web-purchase-flow';
// Dwolla VF i18n
import { bazaDwollaWebVFEnI18n } from '@scaliolabs/baza-dwolla-web-verification-flow';
// Dwolla PF i18n
import { BazaDwollaWebPFEnI18n } from '@scaliolabs/baza-dwolla-web-purchase-flow';
// NC VF i18n
import { bazaNCWebVFEnI18n } from '@scaliolabs/baza-nc-web-verification-flow';
// NC PF i18n
import { BazaNcWebPFEnI18n } from '@scaliolabs/baza-nc-web-purchase-flow';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    constructor(private readonly translate: TranslateService, private readonly store: Store) {
        this.configureI18nTranslations();
        this.store.dispatch(new StartApp());
    }

    configureI18nTranslations() {
        const EnI18n = deepmerge.all([
            BazaWebUiEnI18n,
            BazaDwollaWebAccountEnI18n,
            bazaDwollaWebVFEnI18n,
            BazaDwollaWebPFEnI18n,
            bazaNCWebVFEnI18n,
            BazaNcWebPFEnI18n,
        ]);
        this.translate.setTranslation('en', EnI18n);
    }
}
```

Now that i18n default translations setup is completed, if required, please follow other guides to override translations for required libraries.
