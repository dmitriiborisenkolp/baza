# JSON structure for NC Dwolla Payment Component

Here's the i18n translation object structure for NC Dwolla Payment Component:

```ts
export const DwollaPaymentEnI18nOverride = {
    title: 'Submit Payment',
    checklist: {
        accountVerified: 'Account verification complete',
        documentSigned: 'Subscription document signed',
        paymentMethodAdded: 'Payment method added',
    },
    authorization: {
        label: `By clicking ‘Submit Payment’ you authorize this transaction and agree to our <a data-link="termsOfService"></a> and <a data-link="eftDisclosure"></a>`,
        tosLinkConfig: {
            key: 'termsOfService',
            text: 'Terms of Service',
        },
        eftLinkConfig: {
            key: 'eftDisclosure',
            text: 'EFT Disclosure',
        },
    },
    actions: {
        submit: 'Submit Payment',
        back: 'Back',
    },
};
```
