# JSON structure for Dwolla PF - Payment Methods

To customize the translations for the payment methods components in Dwolla, you can add the `pymtMethods.bank`, `pymtMethods.bankModal`, `pymtMethods.card`, and `pymtMethods.cardModal` keys to the already created object `BazaDwollaWebPFEnI18nOverride`. Here's an example:

```ts
export const DwollaPaymentMethodsEnI18nOverride = {
    bank: {
        addMode: {
            plaidLinking: 'Link Bank Account',
            manualLinking: 'Bank Account',
        },
        editMode: {
            account: {
                name: 'Account holder name:',
                number: 'Account number:',
                type: 'Account type:',
            },
        },
    },
    bankModal: {
        title: 'Bank Details',
        actions: {
            add: 'Add Bank Account',
        },
    },
    card: {
        addMode: {
            title: 'Card',
            feeSuffix: 'Card processing fee',
        },
        editMode: {
            title: 'Card',
            expiring: 'Expiring',
            cardDetails: {
                number: 'Card number:',
                expiring: 'Expiring:',
                feeSuffix: 'Card processing fee',
            },
        },
        alerts: {
            limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account',
        },
    },
    cardModal: {
        title: 'Card Details',
        actions: {
            add: 'Add Card',
        },
    },
};
```
