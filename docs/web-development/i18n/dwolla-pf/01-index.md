# Customizing Dwolla Purchase Flow i18n Translations

BAZA Purchase Flow Library (Dwolla) now has support for i18n label-by-label translations.

There are default i18n configurations which are already applied within libraries but can be easily overrided by providing custom i18n translations at app. level. This works for labels including titles, description, form labels, validation errors and notifications.

## Example on How To Override i18n Translations

Let's take an example where we would like to override the default i18n translations for Dwolla Purchase Flow library. This will include a consolidated i18n file which will have configurations.

Before starting, make sure that `@ngx-translate/core` plugin is installed for project.

### Step 1

Create i18n translation files for each of the components for which translations are required to be overriden. For each specific translation file structure/format (default), check the following links as they have the sample code for each:

-   [Details](web-development/i18n/dwolla-pf/02-details)
-   [Agreement](web-development/i18n/dwolla-pf/03-agreement)
-   [Payment](web-development/i18n/dwolla-pf/04-payment)
-   [Methods](web-development/i18n/dwolla-pf/05-methods)
-   [Done](web-development/i18n/dwolla-pf/06-done)
-   [Payment Edit](web-development/i18n/dwolla-pf/07-payment-edit)
-   [Payment Methods](web-development/i18n/dwolla-pf/08-payment-methods)

### Step 2

Create the i18n override config file in app level e.g. within "feature-purchase" folder. Here's a sample format:

```ts
import { DwollaAgreementEnI18nOverride } from '../agreement/i18n/agreement.i18n';
import { DwollaDetailsEnI18nOverride } from '../details/i18n/details.i18n';
import { DwollaDoneEnI18nOverride } from '../done/i18n/done.i18n';
import { DwollaMethodsEnI18nOverride } from '../methods/i18n/methods.i18n';
import { DwollaPaymentEnI18nOverride } from '../payment/i18n/payment.i18n';
import { DwollaPaymentEditEnI18nOverride } from '../ui-components/payment-edit/i18n/payment-edit.i18n';
import { DwollaPaymentMethodsEnI18nOverride } from '../ui-components/payment-methods/i18n/payment-methods.i18n';

// override translations can be provided optionally
export const BazaDwollaWebPFEnI18nOverride = {
    dwpf: {
        parent: {
            backlink: {
                text: 'Back to Listing Details',
                confirmation: {
                    title: 'Are you sure you want to exit?',
                    text: 'Your purchase will not be completed if you leave this page.',
                },
            },
            steps: {
                detailsLabel: 'Purchase Details',
                agreementLabel: 'Sign Agreement',
                paymentLabel: 'Submit Payment',
            },
            warnings: {
                pickShares: 'Please pick number of shares',
            },
        },
        details: DwollaDetailsEnI18nOverride,
        agreement: DwollaAgreementEnI18nOverride,
        done: DwollaDoneEnI18nOverride,
        methods: DwollaMethodsEnI18nOverride,
        payment: DwollaPaymentEnI18nOverride,
        pymtEdit: DwollaPaymentEditEnI18nOverride,
        pymtMethods: DwollaPaymentMethodsEnI18nOverride,
        notifications: {
            cancel_purchase_fail: 'There was an error cancelling the Purchase.',
            load_bank_account_fail: 'Failed to load Bank Account information',
            load_credit_card_fail: 'Failed to load Credit Card information',
            load_plaid_link_fail: 'Failed to load Plaid link',
            save_bank_account_fail: 'Failed to save Bank Account information',
            save_credit_card_fail: 'Failed to save Credit Card information',
            set_manual_bank_account_fail: 'There was an error setting the Manual Bank Account.',
            set_manual_bank_account_success: 'Manual Bank Account was successfully set',
            submit_purchase_fail: 'There was an error submitting the Purchase.',
            submit_purchase_success: 'Purchase successfully submitted',
            purchase_start_fail: 'Could not start the session',
            purchase_reprocess_success: 'Payment successfully re-submitted',
            purchase_reprocess_fail: 'Payment retry request failed. Please contact your bank for further details',
        },
    },
};
```

**Note:**
Notice all of the above files are actually override files that would resemble the same structure as we have in default configurations, linked above.

### Step 3

Make sure to configure the translations for application level, within `app.component.ts` file as per following sample format. It's important to note that passing the overriden configurations are optional:

```ts
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    constructor(private readonly translate: TranslateService) {
        this.configureI18nOverrides();
    }

    configureI18nOverrides() {
        const EnI18n = deepmerge.all([BazaDwollaWebPFEnI18n, BazaDwollaWebPFEnI18nOverride]);
        this.translate.setTranslation('en', EnI18n);
    }
}
```
