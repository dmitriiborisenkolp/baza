# JSON structure for Dwolla PF - Payment Edit

To customize the translations for the payment edit components in Dwolla, you can add the `pymtEdit.add`, `pymtEdit.list`, and `pymtEdit.update` keys to the already created object `BazaDwollaWebPFEnI18nOverride`. Here's an example:

```ts
export const DwollaPaymentEditEnI18nOverride = {
    add: {
        bank: {
            title: 'Bank Details - Override',
            actions: {
                add: 'Add Bank Account',
            },
        },
        card: {
            title: 'Card Details - Override',
            actions: {
                add: 'Add Card',
            },
        },
    },
    list: {
        title: {
            singular: 'Payment Method',
            plural: 'Payment Methods',
        },
        balance: {
            title: 'Account Balance',
            prefix: 'Current balance:',
            hint: 'Fund your account',
            errors: {
                insufficientFunds:
                    'There are insufficient funds in your account to complete your purchase. Please choose another payment method below.',
            },
        },
        bank: {
            title: 'Bank Account',
            actions: {
                update: 'Update',
            },
        },
        card: {
            title: 'Card',
            feeSuffix: 'Card processing fee',
            limitsWarning: 'Payment transactions above {{ maxAmount }} require ACH via linked bank account',
            actions: {
                update: 'Update',
            },
        },
    },
};
```
