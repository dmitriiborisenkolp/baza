# BAZA Form Validation Behavior

BAZA now has the ability to add form validation behavior with the help of helper functions available which can be re-used across multiple projects.

<br/>

## Introduced Changes

Here is the list of all changes that were introduced as part of new form validation behavior:

-   Submit buttons will always be enabled by default. However, if form is already filled up, untouched and all required fields are valid, submit button will be disabled. It will be enabled again if any form field is edited.

-   After clicking the button, the required field is highlighted, and an error message is shown under it. Along with that, the form will automatically scroll up to the first required field with an error

-   The scroll behavior can be customized for any "scrolling container". By default, the `window` object is used to scroll entire page but we can also use a "custom scroll container" e.g. `NzModalComponent`, which is explained in details below.

<br/>

## Example: Using Form Validation from BAZA

1. Import `BazaNgCoreModule` into relevant module

2. Attach a `TemplateRef` to the `<form>` element e.g. we attach `#verificationFormEl` in this case. This is important since it will be used to find out the 1st invalid control within this form element and scroll to it incase of any error. Sample code is as follows:

```
<form
    nz-form
    nzLayout="vertical"
    [formGroup]="verificationInfoForm"
    #verificationFormEl>
```

3. Include `BazaFormValidatorService` within constructor of relevant component. Remember to use `public` keyword as we would next use this within the HTML in next step.
   `constructor(public readonly bazaFormValidatorService: BazaFormValidatorService)`

4. Now, we would update the HTML code for submit button. First, we would use `isFormFilledAndValid` method of validator service to check if button should be disabled by default or not. Second, for `onFormSubmit` method, we would also pass the `TemplateRef` we defined earlier. Sample code is as follows:

```
 <button
                    class="btn-angle-right"
                    nz-button
                    nzSize="large"
                    nzType="primary"
                    type="submit"
                    [disabled]="bazaFormValidatorService.isFormFilledAndValid(verificationInfoForm)"
                    (click)="onFormSubmit(verificationFormEl)">
                    Next
</button>
```

5. Finally, we would use `isFormValid` method on validator service in component to make sure that our custom code only runs if the form was valid. Sample code is as follows:

```
public onFormSubmit(formEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.verificationInfoForm, formEl)) {
            // custom logic here
        }
}
```

<br/>

## Example: Using Custom Scroll Container

By default, the window is scrolled back in the form to the 1st element with validation errors. However, there might be cases where you would require a custom scroll container instead of window itself e.g. in case of using a form within a popup modal.

For the sake of simplicity, let's take the example explained in previous steps. We can now imagine that the form is within a `NzModalComponent`.

Here are the steps we can use to achieve this functionality.

1. First, we would assign a `TemplateRef ` variable to the modal element e.g. we assign `verificationModalEl` in this case.

```
<nz-modal
    data-cy="verification"
    nzTitle="Verification"
    [nzFooter]="null"
    [nzVisible]="true"
    (nzOnCancel)="onModalClose()"
    #verificationModalEl>

        <form
            nz-form
            nzLayout="vertical"
            [formGroup]="verificationInfoForm"
            #verificationFormEl>
        </form>

```

2. Now, we would modify our submit button code to pass this new `TemplateRef` element, as follows:

```
 <button
                    class="btn-angle-right"
                    nz-button
                    nzSize="large"
                    nzType="primary"
                    type="submit"
                    [disabled]="bazaFormValidatorService.isFormFilledAndValid(verificationInfoForm)"
                    (click)="onFormSubmit(verificationFormEl, verificationModalEl)">
                    Next
</button>
```

3. Lastly, we would modify our "Submit Handler" logic by including modalEl `TemplateRef` as well, as follows:

```
public onFormSubmit(formEl, modalEl): void {
        if (this.bazaFormValidatorService.isFormValid(this.verificationInfoForm, formEl, modalEl)) {
            // custom logic here
        }
}
```
