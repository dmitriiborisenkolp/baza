# Marketing WhyInvest Component

To start using the marketing why invest component, it's necessary to import the module `WhyInvestModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import {WhyInvestModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [Why InvestModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-why-invest` inside your component template.

```html
<app-why-invest></app-why-invest>
```

The component `app-why-invest` has 3 customizable properties:

-   **heading**: Renders the heading for component
-   **subHeading**: Renders the subheading for component
-   **sectionsConfig**: Customization config for 3 sections of component

Default values for configuration are as follows:

```
     const defaultConfig: WhyInvestConfig = {
            heading: 'Your key to exclusive private market investing',
            subHeading: 'Nunc, dictumst odio aenean et nec ipsum sed mollis.',
            sectionsConfig: []
        };

        const defaultSectionConfig = {
            icon: 'point',
            title: 'Morbi nunc, consectetur ac gravida here.',
            descr: 'Lectus odio at felis faucibus a tincidunt duis nunc.',
        };

        for (let i = 0; i < 4; i++) {
            defaultConfig.sectionsConfig.push(defaultSectionConfig);
        }

```

## Customizing Why Invest Component (Styles):

Following is the HTML structure for why invest component:

-   Why Invest section has an element with class `invest__container` which by defaults applies ngZorro `container` styles, but can be customized
-   The hiw container has a single row with 1 columns, and each element is within the column as separate `<div>` having some default styles and media queries applied for responsiveness
-   To customize heading column, use the class `invest__heading`
-   To customize subheading column, use the class `invest__subheading`
-   To customize section cards, use the class `invest-card` and it's inner classes `invest-card__icon`, `invest-card__title` and `invest-card__descr` to style the SVG sprite icon, title and description sections respectively
