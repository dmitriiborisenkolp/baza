# Marketing Banner Component

To start using the marketing banner component, it's necessary to import the module `BannerModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { BannerModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [BannerModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-banner` inside your component template.

```html
<app-banner></app-banner>
```

The component `app-banner` can optionally receive an input `config` object of type `BannerConfig` which allows for following customizations:

-   **subtitle**: Renders the subtitle for banner component
-   **heading**: Renders the heading for banner component
-   **subHeading**: Renders the subheading for banner component
-   **btnLink**: Used for navigation to internal or external link through anchor link in banner component

Default values for configuration are as follows:

```
     defaultConfig: BannerConfig = {
            subtitle: `Subtitle`,
            heading: `We analyze billions of dollars worth of deals on your behalf`,
            subHeading: `Ut dictum turpis vulputate nunc id eget mauris elit, pellentesque. Faucibus quis dignissim varius vel. Blandit.`,
            btnLink: {
                appLink: {
                    commands: ['/items'],
                    text: 'View Offering',
                },
            }
        };

```

## Customizing Banner Component (Styles):

Following is the HTML structure for banner component:

-   Banner banner section has an element with class `banner__container` which by defaults applies ngZorro `container` styles, but can be customized
-   The banner container has a single row with 1 columns, and each element is within the column as separate `<div>` having some default styles and media queries applied for responsiveness
-   To customize banner section or change background image, use the class `banner`
-   To customize subtitle column, use the class `banner__subtitle`
-   To customize heading column, use the class `banner__heading`
-   To customize subheading column, use the class `banner__subheading`
-   To customize button column, use the class `banner__btn`
