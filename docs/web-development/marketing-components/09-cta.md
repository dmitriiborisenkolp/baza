# Marketing CTA Component

To start using the marketing CTA component, it's necessary to import the module `CTAModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { CTAModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [CTAModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-cta` inside your component template.

```html
<app-cta></app-cta>
```

The component `app-cta` can optionally receive an input `config` object of type `CTAConfig` which allows for following customizations:

-   **heading**: Renders the heading for CTA component
-   **descr**: Renders the description text below heading
-   **btnLink**: Used for navigation to internal or external link through anchor link in CTA component

Default values for configuration are as follows:

```
    defaultConfig: CTAConfig = {
            heading: `Start powering your portfolio today`,
            descr: `Go from zero, to "property owner" with just a few clicks`,
            btnLink: {
                appLink: {
                    commands: ['/items'],
                    text: 'Get Started',
                },
            }
        };

```

## Customizing CTA Component (Styles):

Following is the HTML structure for CTA component:

-   CTA section has an element with class `cta__container` which by defaults applies ngZorro `container` styles, but can be customized
-   The CTA container has a single row with 1 columns, and each element is within the column as separate `<div>` having some default styles and media queries applied for responsiveness
-   To customize heading column, use the class `cta__heading`
-   To customize description column, use the class `cta__descr`
-   To customize button column, use the class `cta__btn`
