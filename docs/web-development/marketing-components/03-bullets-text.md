# Marketing Bullets Text Component

To start using the marketing bullets text component, it's necessary to import the module `BulletsTextModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { BulletsTextModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [BulletsTextModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-bullets-text` inside your component template.

```html
<app-bullets-text></app-bullets-text>
```

The component `app-bullets-text` can optionally receive an input `bullets` object of type `BulletsTextConfig` which allows for following customizations:

-   **bullets**: An array of string values (min: 2, max: 4) that render text as bullets text labels

Default values for configuration are as follows:

```
    @Input()
    bullets: BulletsTextConfig = ['Arcu mauris lectus', 'Arcu mauris lectus', 'Arcu mauris lectus', 'Arcu mauris lectus'];

```

## Customizing BulletsText Text Component (Styles):

Following is the HTML structure for bullets-txt component:

-   BulletsText Text banner section has an element with class `bullets-txt__container` which by defaults applies ngZorro `container` styles, but can be customized
-   The bullets-txt container has a single row with dynamic number of columns, minimum 2 and maximum 4
-   To customize bullet column, use the class `bullets-txt__col` and to customize text within the column, use the class `bullets-txt__text`
