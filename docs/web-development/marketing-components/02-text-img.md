# Marketing Text-Image Component

To start using the marketing text-image component, it's necessary to import the module `TextImgModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { TextImgModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [TextImgModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-text-img` inside your component template.

```html
<app-text-img></app-text-img>
```

The component `app-hero` can optionally receive an input `config` object of type `TextImageConfig` which allows for following customizations:

-   **subtitle**: Renders the subtitle
-   **heading**: Renders the heading
-   **descr**: Renders the description text below the heading
-   **img**: Displays image as background image either on left or right side of text
-   **txtPlacement**: Possible values are 'left' or 'right', default value is 'left'. Used to order the placement of text section.

Default values for configuration are as follows:

```
    defaultConfig: TextImageConfig = {
            subtitle: `Subtitle`,
            heading: `Easy to build a diversified investing strategy`,
            descr: `Mauris bibendum neque sed mauris tristique, at cursus purus volutpat. Duis ac neque gravida, condimentum mauris eget, aliquet magna. Maecenas varius sed libero id eleifend. Maecenas tempus finibus suscipit. Fusce venenatis nibh massa, in condimentum lacus auctor fringilla.`,
            img: `'/assets/images/section1.jpeg'`,
            txtPlacement: TextPlacement.Left,
        };

```

## Customizing Text-Image Component (Styles):

Following is the HTML structure for txt-img component:

-   Text-Image section has an element with class `txt-img__container` which by defaults applies ngZorro `container` styles, but can be customized
-   The txt-img container has a single row with 2 columns, and each element is within the column as separate `<div>` having some default styles and media queries applied for responsiveness
-   Text column has a class `txt-col` and a modifier `txt-col-right` applied when showing text on the right order
-   To customize subtitle column, use the class `txt-col__subtitle`
-   To customize heading column, use the class `txt-col__heading`
-   To customize description column, use the class `txt-col__descr`
-   To customize image column, use the class `img-col`
