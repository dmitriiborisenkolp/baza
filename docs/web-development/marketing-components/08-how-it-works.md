# Marketing How It Works Component

To start using the marketing how it works component, it's necessary to import the module `HowItWorksModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { HowItWorksModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [HowItWorksModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-how-it-works` inside your component template.

```html
<app-how-it-works></app-how-it-works>
```

The component `app-how-it-works` can optionally receive an input `config` object of type `HowItWorksConfig` which allows for following customizations:

-   **heading**: Renders the heading for component
-   **subHeading**: Renders the subheading for component
-   **sectionsConfig**: Customization config for 3 sections of How It Works component

The sectionsConfig allows for 2 further customization properties:

-   **title**: Displays the title for section
-   **descr**: Displays the description for section

Default values for configuration are as follows:

```
    const defaultConfig: HowItWorksConfig = {
            heading: 'From the market to your portfolio',
            subHeading:
                'Mauris bibendum neque sed mauris tristique, at cursus purus volutpat. Duis ac neque gravida, condimentum mauris eget.',
            sectionsConfig: [
                {
                    title: 'Nulla metus tincidunt faucibus.',
                    descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
                },
                {
                    title: 'Nulla metus tincidunt faucibus.',
                    descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
                },
                {
                    title: 'Nulla metus tincidunt faucibus.',
                    descr: 'Viverra interdum egestas laoreet lacus mi vestibulum tellus sed. Orci diam lacinia nunc dictum.',
                },
            ],
        };

```

## Customizing HowItWorks Component (Styles):

Following is the HTML structure for the component:

-   How it works section has an element with class `hiw__container` which by defaults applies ngZorro `container` styles, but can be customized
-   The hiw container has a single row with 1 columns, and each element is within the column as separate `<div>` having some default styles and media queries applied for responsiveness
-   To customize heading column, use the class `hiw__heading`
-   To customize subheading column, use the class `hiw__subheading`
-   To customize section cards, use the class `hiw-card` and it's inner classes `hiw-card__index`, `hiw-card__title` and `hiw-card__descr` to style the index, title and description sections respectively
