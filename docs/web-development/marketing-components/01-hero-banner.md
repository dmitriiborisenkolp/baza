# Marketing Hero Banner Component

To start using the marketing hero banner component, it's necessary to import the module `HeroModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { HeroModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [HeroModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-hero` inside your component template.

```html
<app-hero [config]="config"></app-hero>
```

The component `app-hero` can optionally receive an input `config` object of type `HeroConfig` which allows for following customizations:

-   **heading**: Renders the heading for hero component
-   **subHeading**: Renders the subheading for hero component
-   **btnLink**: Used for navigation to internal or external link through anchor link in hero component
-   **img**: Background image for banner section

Default values for configuration are as follows:

```
    defaultConfig: HeroConfig = {
            heading: `Invest & Earn Passive Income`,
            subHeading: `For Investors who need a reliable platform to browse and purchase offerings, Scalio provides a robust Investment Platform.`,
            btnLink: {
                appLink: {
                    commands: ['/items'],
                    text: 'Get Started',
                },
            }
        };

```

## Customizing Hero Component (Styles):

Following is the HTML structure for hero component:

-   Hero banner section has an element with class `hero__container` which by defaults applies ngZorro `container` styles, but can be customized
-   The hero container has a single row with 1 columns, and each element is within the column as separate `<div>` having some default styles and media queries applied for responsiveness
-   To customize hero banner section or change background image, use the class `hero`
-   To customize heading column, use the class `hero__heading`
-   To customize subheading column, use the class `hero__subheading`
-   To customize button column, use the class `hero__btn`
