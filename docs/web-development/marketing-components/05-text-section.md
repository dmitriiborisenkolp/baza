# Marketing Text Section Component

To start using the marketing text section component, it's necessary to import the module `TextSectionModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { TextSectionModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [TextSectionModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-text-section` inside your component template.

```html
<app-text-section></app-text-section>
```

The component `app-text-section` can optionally receive an input `config` object of type `TextSectionConfig` which allows for following customizations:

-   **heading**: Renders the heading
-   **subHeading**: Renders the sub-heading
-   **subtitle**: Optional - Renders the subtitle text above description
-   **descr**: Renders the description text. Supports HTML input string

Default values for configuration are as follows:

```
    defaultConfig: TextSectionConfig = {
            heading: `Get exclusive access to private market investments`,
            subHeading: `Access to institutional-quality offers made easy through our low-cost, reliable investment platform.`,
            subtitle: `Dui sit aliquam sed nunc, volutpat. Elementum platea vel velit proin iaculis auctor erat. Nulla erat cursus sed et magna tristique.`,
            descr: `<p> Ornare in mattis nisi congue orci. Pulvinar netus eget quam maecenas non dui a. Tellus vitae et tortor nunc proin morbi gravida aliquam. Eu sit posuere neque odio eu sapien adipiscing eget. Orci amet, id cursus arcu facilisi. Non ultricies. </p> <p> Lobortis vel, vestibulum odio laoreet massa ut amet sed. Ultrices odio integer aliquet montes, eget arcu, massa. Tristique adipiscing nulla montes, faucibus. Nisi faucibus a laoreet leo, est. Et consequat justo, mattis risus ipsum posuere a nibh. Lectus ut eget eget faucibus donec quam morbi. Risus eu, sit lectus at faucibus. Dictumst ultrices diam at feugiat sed eget pulvinar convallis nibh. Quis varius amet, porttitor bibendum. Sodales tellus vel, turpis fermentum nulla elementum. Quam pellentesque vestibulum, vulputate ac turpis sit eleifend nisl. Gravida. </p>`,
        };

```

## Customizing Text Section Component (Styles):

Following is the HTML structure for text-section component:

-   Text Section section has a container element with which by defaults applies ngZorro `container` styles, but can be customized
-   The text-section container has a single row with 2 columns having class `txt-sec__col`, and each element is within the column as separate `<div>` having some default styles and media queries applied for responsiveness
-   To customize heading column, use the class `txt-sect__heading`
-   To customize sub-heading column, use the class `txt-sect__subheading`
-   To customize subtitle column, use the class `txt-sect__subtitle`
-   To customize description column, use the class `txt-sect__descr`
