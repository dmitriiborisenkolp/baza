# Marketing Bullets Image Component

To start using the marketing bullets image component, it's necessary to import the module `BulletsImageModule` from the library `@scaliolabs/baza-web-ui-components`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { BulletsImageModule } from '@scaliolabs/baza-web-ui-components';

@NgModule({
    imports: [BulletsImageModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-bullets-image` inside your component template.

```html
<app-bullets-image></app-bullets-image>
```

The component `app-bullets-image` can optionally receive an input `config` object of type `BulletsImageConfig` which allows for following customizations:

-   **label**: A label for bullet image section
-   **images**: An array of string values (min: 2, max: 4) that render image path as bullets images

Default values for configuration are as follows:

```
    @Input()
    config: BulletsImageConfig =
        {
            label: "As seen on",
            images: [
                `/assets/images/marketwatch.png`,
                `/assets/images/yahoo-finance.png`,
                `/assets/images/bloomberg.png`,
                `/assets/images/benzinga.png`
            ]
        }

```

## Customizing Bullets Image Component (Styles):

Following is the HTML structure for bullets-img component:

-   BulletsImage Text banner section has an element with class `bullets-img__container` which by defaults applies ngZorro `container` styles, but can be customized
-   The bullets-img container has a single row with 2 columns, 1 for label and other for images
-   The images section `bullets-img-bg` itself has dynamic 2 to 4 number of columns which can display bullet images based on input property
-   To customize bullet label column, use the class `bullets-img__label` and to customize images column, use the class `bullets-img-bg__col`
