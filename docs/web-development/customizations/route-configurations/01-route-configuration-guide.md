# Route / Links(s) Configuration Guide

BAZA allows certain customizations for Dwolla PF/VF libraries and Account components. This guide explains all concepts and options about how to customize routes.

This PR covers adding route customizations that will allow internal & external links for Dwolla PF and Account components.

## What's New

-   A route can now be defined as internal or external route, but mutually exclusive properties allow to only define 1 out of these options

-   Each route can have a set of properties. All of these properties are nullable and default configurations are also set in relevant components

-   The logic to navigate these links is all encapsulated within `baza-utils` library. All methods/interfaces/configurations etc. are within a shared location

-   When defining routes, we can either define `appLink` or `extLink`, but never both at the same time.

### Internal Routes - Detailed View

1. The internal routes are specifically those which are supposed to be within the application level e.g. for privacy policy page within site, we would have a route "/privacy-policy", which can be defined using `appLink` property.

2. Internal routes are not meant to be for external resources/sites/CMS links etc.

3. A custom text can also be provided for internal routes. By default, it's the same as defined within application but can be overriden by parent component

**For internal routes, the available customizable properties include**

| Property         | Data Type        | Nullable? | Default Value            | Function                                                                                                                               |
| ---------------- | ---------------- | --------- | ------------------------ | -------------------------------------------------------------------------------------------------------------------------------------- |
| text             | string           | yes       | Set in Library/Component | Overrides the text portion on FE for relevant hyperlink is replaced by this value                                                      |
| target           | string           | yes       | "\_self"                 | Overrides the link navigation to open in current tab, new tab etc.                                                                     |
| commands         | any[]            | yes       | Set in Library/Component | Angular way of defining internal routes, with possible queryParams etc. which is used by Angular router to navigate internally in site |
| navigationExtras | NavigationExtras | yes       | null                     | Additional parameters can be passed as well for Angular router based navigation                                                        |

<br/>

---

<br/>

#### Code Samples:

```
**Sample # 1**
```

```typescript

privacyPolicyLink: {
		internalLink: {
			commands: ['/privacy-policy'],
			text: 'Privacy Policy (Custom)'
		}
},

```

The above sample allows redirecting to internal route "/privacy-policy" and text for hyperlink will be "Privacy Policy (Custom)".

```typescript
Sample # 2:
```

```
contactUsLink: {
		internalLink: {
			commands: ['', { outlets: { modal: ['contact-us'] } }]
			text: 'Technical Support'
		}
},
```

The above sample allows redirecting to a modal route which in turn will allow opening the "Contact Us" popup configured on site

### External Routes - Detailed View

1. External routes are navigated using "window.open" function.

**For external routes, the available customizable properties include**

| Property   | Data Type | Nullable? | Default Value            | Function                                                                                                        |
| ---------- | --------- | --------- | ------------------------ | --------------------------------------------------------------------------------------------------------------- |
| link       | string    | yes       | Set in Library/Component | This is the link value. In case of external link, it's either CMS/Mail or External Website link                 |
| text       | string    | yes       | Set in Library/Component | If set, override the text portion on FE for relevant hyperlink is replaced by this value                        |
| target     | string    | yes       | "\_blank"                | If set, the redirections follow the pattern e.g. new tab, same tab etc.                                         |
| isCMSLink  | boolean   | yes       | false                    | If set, the address is fetched from registry based on the value passed in "link" property                       |
| isMailLink | boolean   | yes       | false                    | If set to true, the links would have a "mailto:" prefix attached before the link and behaves like external link |

Example - Account Payout Method (Contact Link):

HTML:

```
<app-dwolla-account-balance  [accBalanceConfig]="accBalanceConfig"></app-dwolla-account-balance>
```

Component:

```
@Input()
	accBalanceConfig?: AccBalanceConfig = {
		contactLink: {
				extLink: {
					link: 'bazaContentTypes.contacts.email',
					text: 'contact us',
					isCMSLink: true,
					isMailLink: true,
			},
		},
	};
```

## Added/Updated Configurations:

**Account Components**

-   Added new configuration for `DwollaAccountBalanceComponent` component
-   Please note: The individual input properties e.g. title, hint & prefix will be refactored later to be part of the config as well
-   The config automatically also handles showing the right email address in "Add Funds" modal, in case we have a funds transfer error

Sample value (default) for this configuration:

```
	config?: AccBalanceConfig = {
		links: {
			contact: {
				extLink: {
					link: 'bazaContentTypes.contacts.email',
					text: 'contact us',
					isCMSLink: true,
					isMailLink: true,
				},
			},
		},
	};
```

---

-   Added new configuration for `DwollaBankAccountPayoutComponent` component

Sample value (default) for this configuration:

```
	config?: AccPayoutConfig = {
		links: {
			contact: {
					extLink: {
						link: 'bazaContentTypes.contacts.email',
						isCMSLink: true,
						isMailLink: true,
					},
				},
			},
		};
```

---

**Dwolla Verification Flow Library**

-   Added new configuration for `VerificationInfoComponent` component.
-   Replaced the individual inputs title and description with single `config?: VFInfoConfig` input

Sample value (default) for this configuration:

```
		 defaultConfig: VFInfoConfig = {
            title: 'Personal Information',
            descr: 'We collect your information to perform Know Your Customer (KYC) and Anti-Money Laundering (AML) compliance. Please provide correct legal information to verify your account.',
        };
```

---

-   Added new configuration for `VerificationInvestorComponent` component
    Added the existing individual title input property into the new `config?: VFInvestorConfig`

Sample value (default) for this configuration:

```
	defaultConfig: VFInvestorConfig = {
            title: 'Investor Profile',
            links: {
                termsOfService: {
                    extLink: {
                        link: 'bazaCommon.links.termsOfService',
                        text: 'Terms of Service',
                        isCMSLink: true,
                    },
                },
                privacyPolicy: {
                    extLink: {
                        link: 'bazaCommon.links.privacyPolicy',
                        text: 'Privacy Policy',
                        isCMSLink: true,
                    },
                },
                dwollaTOS: {
                    extLink: {
                        link: 'https://www.dwolla.com/legal/tos/#legal-content',
                        text: 'Terms of Service',
                    },
                },
                dwollaPriacyPolicy: {
                    extLink: {
                        link: 'https://www.dwolla.com/legal/privacy',
                        text: 'Privacy Policy',
                    },
                },
            },
        };
```

---

**Dwolla Purchase Flow Library**

-   Updated the configuration for DwollaPurchasePaymentComponent component
-   Refactored naming of interfaces

Sample value (default) for this configuration:

```
	config: PaymentConfig = {
        methods: {
            showPurchaseDividendMessage: true,
            accountBalanceTitle: 'Account Balance',
        },
        steps: {
            links: {
                termsOfService: {
                    extLink: {
                        link:  'bazaCommon.links.termsOfService',
                        text: 'Terms of Service',
                        isCMSLink: true,
                    },
                },
                eftDisclosure: {
                    appLink: {
                        commands: ['/eft-disclosure'],
                        text: 'EFT Disclosure',
                    },
                },
            },
        },
    };
```

-   Updated the configuration for `DwollaPurchaseDoneComponent` component
-   Sample value (default) for this configuration:

```
config: PurchaseDoneConfig = {
       title: 'Your order has been submitted!',
       desc: 'We are verifying the order and will notify you when it has been completed. This typically takes 24-48 hours.',
       ctaBtnLink: {
           appLink: {
               text: 'Proceed To My Portfolio',
               commands: ['/portfolio'],
           },
       },
       cta: {
           desc: 'If you have any questions please reach out via',
           link: {
               appLink: {
                   text: 'Contact Us',
                   commands: ['', { outlets: { modal: ['contact-us'] } }],
               },
           },
       },
   };
```
