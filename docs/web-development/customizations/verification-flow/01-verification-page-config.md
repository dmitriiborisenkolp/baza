# Customizing Verification Flow

BAZA purchase flow libraries allow us to customize certain aspects of the verification flow. It's important to note that all components of Dwolla VF library are covered with i18n translations. Please refer to this link to find out more how it can be configured:
https://baza-docs.test.scaliolabs.com/web-development/i18n/dwolla-vf/01-index-i18n

<br/>

## Loading Dwolla Verfication Flow Library

Please follow this guide on how to load and customize the Dwolla VF library.

<br/>

### Step 1 - Create custom wrapper components

First step would be to create different custom wrapper components as follows:

<br/>

<u>**Custom Verification Component**</u>

Create a new custom verification component, sample code as follows. Please note, optionally you can pass input parameters as well which include link configuration.

**HTML**

```typescript
<app-verification-dwolla [linksConfig]="linksConfig"></app-verification-dwolla>

```

**CodeBehind**

```typescript
export class VerificationCustomComponent {
    // optional configuration
    linksConfig: DwollaVFLinksConfig;

    constructor(private readonly route: ActivatedRoute, private readonly store: Store, private readonly wts: BazaWebUtilSharedService) {
        this.linksConfig = {
            buyShares: '/buy-shares-dwolla',
            verification: '/verification-dwolla',
            defaultRedirect: '/account-dwolla',
        };

        // required code - for manual refresh of bootstrap data
        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }
}
```

**Notes**

-   The `DwollaVFLinksConfig` is not mandatory input, it's optional. The default values for links are:

```typescript
const defaultConfig = {
    buyShares: '/buy-shares',
    verification: '/verification',
    defaultRedirect: '/account',
};
```

-   The subject handler for `refreshInitData` is required for proper manual refreshing of bootstrap data as required from library.

-   Dwolla VF can work with query param link configurations for custom back and redirect links. The `defaultRedirect` route input config is just override of default route for both back and redirect links, in case query params are not provided OR user tampers with address bar and removes them.

<br/>

<u>**Custom Verification Check Component**</u>

Create a new custom verification check component, sample code as follows:

**HTML**

```typescript
<app-dwolla-verification-check></app-dwolla-verification-check>
```

<br/>

<u>**Custom Verification Stepper Component**</u>

Create a new custom verification component, sample code as follows. Please note, optionally you can pass input parameters as well which include link configuration.

**HTML**

```typescript

<app-verification-stepper-dwolla>
    <app-verification-stepper-info-dwolla
        *ngIf="currentTab === 0"
        target="info"
        [proceedToNextStep]="proceedToNextStep"
        (moveToNextStep)="onMoveToNextStep()"></app-verification-stepper-info-dwolla>
    <app-verification-stepper-investor-dwolla
        *ngIf="currentTab === 1"
        target="investor"
        [initData]="initData$ | async"></app-verification-stepper-investor-dwolla>
</app-verification-stepper-dwolla>

```

**CodeBehind**

```typescript
export class VerificationStepperCustomComponent {
    initData = this.store.select(BootstrapState.initData);
    proceedToNextStep = false;
    public currentTab: number;

    constructor(private readonly route: ActivatedRoute, private readonly store: Store) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });
    }

    onMoveToNextStep() {
        this.store
            .dispatch(new RequestAppBootstrapInitData())
            .pipe(
                untilDestroyed(this),
                tap(() => {
                    this.proceedToNextStep = true;
                    this.cdr.markForCheck();
                }),
            )
            .subscribe();
    }
}
```

**Notes**

-   The `initData` is a required parameter to pass to Dwolla VF library. It basically is application level boostrap information

-   The stepper investor component selector `<app-verification-stepper-investor-dwolla>` can additionally receive an input `config` of type `VFInvestorConfig`. The default values and JSON structure is as follows:

```typescript
const defaultConfig: VFInvestorConfig = {
    links: {
        termsOfService: {
            extLink: {
                link: 'bazaCommon.links.termsOfService',
                isCMSLink: true,
            },
        },
        privacyPolicy: {
            extLink: {
                link: 'bazaCommon.links.privacyPolicy',
                isCMSLink: true,
            },
        },
        dwollaTOS: {
            extLink: {
                link: 'https://www.dwolla.com/legal/tos/#legal-content',
            },
        },
        dwollaPriacyPolicy: {
            extLink: {
                link: 'https://www.dwolla.com/legal/privacy',
            },
        },
    },
};
```

-   Note, we need to create `onMoveToNextStep` event handler in application level code. That will refresh bootstrap data manually and upon completion, it will update the proceedToNextStep local variable. This is then passed to library which triggers ngOnChanges within library and manages redirect from 1st to 2nd step. This change was done because in Dwolla VF flow, we need to manually refresh bootstrap data once we submit on 1st step and before we move to 2nd step.

<br/>

### Step 2 - Setup Custom Routing Module

Please use the following sample code to setup your verification custom routing module:

```typescript
const routes: Routes = getConfigurableRoutes(
    VerificationCustomComponent,
    VerificationCheckCustomComponent,
    VerificationStepperCustomComponent,
);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class VerificationRoutingCustomModule {}
```
