# Dwolla Verification Flow - Query Param Links Configuration

BAZA purchase flow libraries allow us to customize where would user be redirected from back link and after submission of Dwolla VF.

Since now "Check Account Component" is part of Dwolla VF, it would be the default route for any redirection towards Dwolla VF e.g. when user clicks on "Buy Shares" button.

<br/>

## Setting Up Query Param Links

Here's a sample link configuration for when user wants to click on "Verify Your Investor" account to redirect to Dwolla VF.

-   We can check bootstrap information flags to check if user has already completed VF or not. If he has, we will redirect him directly to "Info" page route, else we would redirect him to root "Verification" route which will load "Check Account" component by default, showing user the disclaimer page.

Here are some sample scenarios as an example:

<br/>

### Example 1 - User clicks on "Verify Your Investor" button present within Account page:

Here's **sample** code for the button click event:

```typescript

redirectToVF() {
        // default route -> root of VF library
        let commands = ['/verification-dwolla']

        // check if user has already completed VF or not? If yes, redirect him directly to /info
        if (this.initData?.investorAccount.isAccountVerificationCompleted) {
            commands = ['/verification-dwolla/info'];
        }

        this.router.navigate(commands, {
            queryParams: {
                redirect: this.router.url
            },
        });
    }

```

**Notes:**

-   Query params are required to be passed when setting up route for proper configuration. In case, they are not passed OR if user tampers with address bar and removes the query params, then by default we pick up redirect `defaultRedirect` from input configuration `DwollaVFLinksConfig` which is passed through verification custom component. And even if that is not available, then default route `/account` will be used for redirections both for back link and after user submits VF 2nd step.

-   In the above example, the query params include `redirect` for back link redirection and for redirection when user submits VF 2nd step.

### Example 2 - User clicks on "Buy Shares" button present within Item Details Page:

Note that in case user is trying to buy shares, then `buy=true` query parameter would also be present in address bar.

We have few cases here e.g. the following:

<br/>

<u>**1. Case - User is already verified**</u>

```typescript
        this.router.navigate(['/verification-dwolla'], {
            queryParams: {
                redirect: '/buy-shares-dwolla',
                buy: true
            },
        });
    }
```

**Note:**
In the above case when user is already verified, the `redirect` points back to purchase flow and buy parameter is true, which means user will be redirected back to purchase flow upon VF submission as well.

<br/>

<u>**2. Case - User is not already verified**</u>

```typescript
        this.router.navigate(['/verification-dwolla'], {
            queryParams: {
                redirect: this.router.url,
                buy: true
            },
        });
    }
```

**Note:**
In the above case when user is already verified, the `redirect` points back to origin page (wherever "Buy Shares" button was) and buy parameter is true, which means user will be redirected back to purchase flow upon VF submission as well.

<br/>

## General Notes:

-   The `redirect` parameter is totally customizable. Also, in case `buy` is true, Dwolla VF library will pick the `buyShares` link through input configuration `DwollaVFLinksConfig` being passed through verification custom component.

-   The back link will always have a generic label e.g. by default it has `Go Back` label, but it can be overriden through i18n configuration. Same is true for back link popup configuration when user tries to exit Dwolla VF.

-   This customization allows trigger buttons to be placed anywhere on target projects.

-   It's important to have the following resolvers in place for application level routes:

```typescript
{
    path: 'verification-dwolla',
    canActivate: [JwtRequireAuthGuard, JwtVerifyGuard],
    loadChildren: () => import('@scaliolabs/sandbox-web/feature-verification-dwolla').then((m) => m.VerificationModule),
        resolve: {
            bootstrap: BootstrapResolver,
            purchase: DwollaCartResolver,       //previously named "DwollaPurchaseResolver"
        },
 },

 {
    path: 'buy-shares-dwolla',
    canActivate: [JwtRequireAuthGuard, JwtVerifyGuard],
    loadChildren: () => import('@scaliolabs/sandbox-web/feature-purchase-dwolla').then((m) => m.DwollaPurchaseModule),
        resolve: {
            bootstrap: BootstrapResolver,
            purchase: DwollaCartResolver,     //previously named "DwollaPurchaseResolver"
        },
 },

```

-   The form resources resolver is now moved to Dwolla VF and also there is no need of including `DwollaVerificationResolver` as it is already being used in Dwolla VF library itself.

-   If user clicks on "Edit Personal Info" link within Dwolla PF, then he lands directly on "Info" page of Dwolla VF as he is already verified. Also, upon clicking back link OR upon submission of Dwolla VF, user goes back to Dwolla PF.
