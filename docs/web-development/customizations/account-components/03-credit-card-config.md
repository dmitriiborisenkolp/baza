# Customizing credit card

To start using the credit card component, it's necessary to import the module `DwollaCreditCardModule` from the library `@scaliolabs/baza-dwolla-web-purchase-flow`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { DwollaCreditCardModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';

@NgModule({
    imports: [DwollaCreditCardModule],
})
export class YourModule {}
```

After that, you should be able to render the component `dwolla-credit-card` inside your component template.

```html
<app-dwolla-credit-card
    [creditCardResponse]="creditCardResponse"
    [limits]="limits"
    (refreshCardData)="onRefreshCardData()"></app-dwolla-credit-card>
```

### Config examples

```ts
const creditCardResponse: BazaNcPurchaseFlowGetCreditCardResponse = {
    isAvailable: true,
    creditCard: {
        creditCardType: 'Visa',
        creditCardNumber: '1234567812345678',
        creditCardholderName: 'Name',
        creditCardExpireMonth: 1,
        creditCardExpireYear: 22,
    },
};

const limits: BazaNcLimitsDto = {
    maxACHTransferAmountCents: 10000000,
    maxCreditCardTransferAmountCents: 500000,
    creditCardPaymentsFee: 3.5,
    maxDwollaTransferAmount: 1000000,
};
```
