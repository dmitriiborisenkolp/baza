# Customizing bank account as payout method

To start using the bank account component, it's necessary to import the module `DwollaPayoutBankAccountModule` from the library `@scaliolabs/baza-dwolla-web-purchase-flow`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { DwollaPayoutBankAccountModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';

@NgModule({
    imports: [DwollaPayoutBankAccountModule],
})
export class YourModule {}
```

After that, you should be able to render the component `dwolla-bank-account` inside your component template.

```html
<app-dwolla-bank-account-payout
    [config]="config"
    [initData]="bazaNcBootstrap"
    [dwollaDefaultCashOutAccount]="dwollaDefaultCashOutAccount"
    (refreshCashOutData)="onRefreshCashOutData()"></app-dwolla-bank-account-payout>
```

Optionally, you can pass an input properties

```ts
// With external link
const config: AccPayoutConfig = {
    links: {
        contact: {
            extLink: {
                text: 'Technical Support',
                link: 'bazaContentTypes.contacts.email',
                isCMSLink: true,
                isMailLink: true,
            },
        },
    },
};

// or

// With internal link
const config: AccPayoutConfig = {
    links: {
        contact: {
            appLink: {
                text: 'Contact us',
                commands: ['', { outlets: { modal: ['contact-us'] } }],
            },
        },
    },
};

const bazaNcBootstrap: BazaNcBootstrapDto = {
    registry: BazaRegistryPublicSchema,
    account: AccountDto,
    investorAccount: BazaNcInvestorAccountDto,
    signedUpWithClient: Application,
    referralCode: 'string',
    referralCodeCopyText: 'string',
    signedUpWithReferralCode: 'string',
};

const dwollaDefaultCashOutAccount: BazaNcBankAccountGetDefaultByTypeResponse = {
    isAvailable: true,
    options: [BazaNcBankAccountDto],
    result: BazaNcBankAccountDto,
};
```
