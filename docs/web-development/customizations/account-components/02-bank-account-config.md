# Customizing bank account

To start using the bank account component, it's necessary to import the module `DwollaBankAccountModule` from the library `@scaliolabs/baza-dwolla-web-purchase-flow`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { DwollaBankAccountModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';

@NgModule({
    imports: [DwollaBankAccountModule],
})
export class YourModule {}
```

After that, you should be able to render the component `dwolla-bank-account` inside your component template.

```html
<app-dwolla-bank-account [bankAccountResponse]="bazaNcBankAccountAch"></app-dwolla-bank-account>
```

### Config examples

```ts
const bazaNcBankAccountAch: BazaNcBankAccountAchDto = {
    isAvailable: boolean,
    details: {
        accountName: 'string',
        accountNickName: 'string',
        accountRoutingNumber: 'string',
        accountNumber: 'string',
        accountType: 'Checking',
    },
};
```
