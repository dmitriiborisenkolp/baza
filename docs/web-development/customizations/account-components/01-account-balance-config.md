# Customizing Account Balance

To start using the account balance component, it's necessary to import the module `DwollaAccountBalanceModule` from the library `@scaliolabs/baza-dwolla-web-purchase-flow`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { DwollaAccountBalanceModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';

@NgModule({
    imports: [DwollaAccountBalanceModule],
})
export class YourModule {}
```

After that, you should be able to render the component `dwolla-account-balance` inside your component template.

```html
<app-dwolla-account-balance [config]="config"></app-dwolla-account-balance>
```

```ts
// With external link
const config = {
    links: {
        contact: {
            extLink: {
                link: 'bazaContentTypes.contacts.email',
                text: 'contact us',
                isCMSLink: true,
                isMailLink: true,
            },
        },
    },
};

// or

// With internal link
const config = {
    links: {
        contact: {
            appLink: {
                text: 'Contact us',
                commands: ['', { outlets: { modal: ['contact-us'] } }],
            },
        },
    },
};
```
