# Customizing payment methods component

To start using the payment methods component, it's necessary to import the module `DwollaPayoutMethodsModule` from the library `@scaliolabs/baza-dwolla-web-purchase-flow`, and add it directly to the `imports` property of your `NgModule` decorator.

```ts
import { DwollaPayoutMethodsModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';

@NgModule({
    imports: [DwollaPayoutMethodsModule],
})
export class YourModule {}
```

After that, you should be able to render the component `app-dwolla-payout-methods` inside your component template.

```html
<app-dwolla-payout-methods
    [initData]="initData$ | async"
    [config]="payoutConfig"></app-dwolla-payout-methods>
```

Optionally, you can pass the following input properties:

| Property | Data Type                        | Nullable? | Default Value                         | Function                                          |
| -------- | -------------------------------- | --------- | ------------------------------------- | ------------------------------------------------- |
| initData | `Observable<BazaNcBootstrapDto>` | no        | Required to be passed from app. level | Sets the boostrap data for component              |
| config   | `AccPayoutConfig`                | yes       | Set in Library/Component              | Overrides the defautl configuration for component |

**Note:**
The config has the following default properties which can be easily customized:

```ts
const payoutConfig: AccPayoutConfig = {
    links: {
        contact: {
            extLink: {
                link: 'bazaContentTypes.contacts.email',
                isCMSLink: true,
                isMailLink: true,
            },
        },
    },
};
```

Additionally, you can specify the title for the section using the `headerSection` content.

```html
<app-dwolla-payout-methods>
    <h3 headerSection>Payout Methods</h3>
</app-dwolla-payout-methods>
```

### Using Bootstrap Data from App. Level:

-   The `DwollaPayoutMethodsComponent` component now has `initData` input parameter which should have the bootstrap data passed down from app. level
-   There is a new subject `refreshInitData$` available in `baza-web-utils` library. Everytime bootstrap data is required to be refreshed from application level, the libraries/components trigger this subject with value 'true'. Target projects will then need to subscribe to this subject in app. level code (e.g. account page), get updated bootstrap information and pass it down.

### Sample Code:

Following is an example on how we can use Dwolla account components with updated bootstrap data logic:

**Account - HTML (App. Level)**

```ts
    <app-dwolla-payout-methods [initData]="initData$ | async">
    </app-dwolla-payout-methods>
```

**Account - TS (App. Level)**

```ts
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngxs/store';
import { DwollaSharedService } from '@scaliolabs/baza-dwolla-web-purchase-flow';
import { DwollaVerificationState } from '@scaliolabs/baza-dwolla-web-verification-flow';
import { AccountVerificationStep } from '@scaliolabs/baza-nc-shared';
import { BazaWebUtilSharedService } from '@scaliolabs/baza-web-utils';
import { BootstrapState, RequestAppBootstrapInitData } from '@scaliolabs/sandbox-web/data-access';

@Component({
    selector: 'app-payment-method-dwolla',
    templateUrl: './payment-method-dwolla.component.html',
    styleUrls: ['./payment-method-dwolla.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentMethodDwollaComponent {
    verification$ = this.store.select(DwollaVerificationState.verification);
    initData = this.store.select(BootstrapState.initData);

    constructor(private readonly store: Store, public readonly dss: DwollaSharedService, private readonly wts: BazaWebUtilSharedService) {
        this.store.dispatch(new RequestAppBootstrapInitData());

        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }

    verificationCompleted(verification: { currentStep: AccountVerificationStep }) {
        return AccountVerificationStep.Completed === verification?.currentStep;
    }
}
```
