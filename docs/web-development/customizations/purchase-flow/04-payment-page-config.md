# Customizing Purchase Flow

BAZA purchase flow libraries allow us to customize certain aspects of the purchase flow which include custom payment page configuration.

<br/>

## Example: Customizing Payment Page

This example talks about adding a custom payment page configuration to purchase flow. Please note that by default, the payment page configuration is as follows:

```html
<app-purchase-payment-dwolla
    [initData]="initData"
    [config]="paymentConfig"></app-purchase-payment-dwolla>
```

```typescript
const paymentConfig: PaymentConfig = {
    methods: {
        showPurchaseDividendMessage: true,
    },
    steps: {
        links: {
            termsOfService: {
                extLink: {
                    link: 'bazaCommon.links.termsOfService',
                    text: 'Terms of Service',
                    isCMSLink: true,
                },
            },
            eftDisclosure: {
                appLink: {
                    commands: ['/eft-disclosure'],
                    text: 'EFT Disclosure',
                },
            },
        },
    },
};
```
