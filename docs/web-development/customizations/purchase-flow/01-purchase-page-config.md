# Customizing Purchase Flow

BAZA purchase flow libraries allow us to customize certain aspects of the purchase flow which include custom back-link configuration.

<br/>

## Example: Changing the Purchase Page Configuration

This example talks about adding a custom purchase configuration to purchase flow. Please note that by default, the purchase configuration is as follows:

```typescript
this.defaultConfig = {
    linksConfig: {
        buyShares: '/buy-shares',
        verification: '/verification',
        backLink: {
            appLink: {
                commands: ['/items', this.item.id.toString()],
            },
        },
    },
};
```

### Config interface

```ts
export interface PurchaseConfig {
    linksConfig?: PFLinksConfig;
}

export interface PFLinksConfig {
    buyShares?: string;
    verification?: string;
    backLink?: LinkConfig;
}
```

<br/>

### Sample Code - Purchase Custom Component (HTML)

```html
<app-purchase
    #purchase
    [purchaseConfig]="purchaseConfig">
    <app-purchase-details
        *ngIf="currentTab === 0"
        target="purchase-details"
        [personal$]="personal$"></app-purchase-details>

    <app-purchase-agreement
        *ngIf="currentTab === 1"
        target="purchase-agreement"
        (cancelPurchase)="purchase.cancelPurchase()"></app-purchase-agreement>

    <app-purchase-payment
        *ngIf="currentTab === 2"
        target="purchase-payment"></app-purchase-payment>
</app-purchase>
```
