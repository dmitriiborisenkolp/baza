# Customizing Purchase Flow

BAZA purchase flow libraries allow us to customize certain aspects of the purchase flow which include custom agreement page configuration.

```html
<app-purchase-agreement-dwolla (DwollaCancelPurchase)="purchase.cancelPurchase()"></app-purchase-agreement-dwolla>
```
