# Customizing Purchase Flow

BAZA purchase flow libraries allow us to customize certain aspects of the purchase flow which include custom details page configuration.

<br/>

## Example: Customizing Details Page

This example talks about adding a custom details page configuration to purchase flow. Please note that by default, the details page configuration is as follows:

```html
<app-purchase-details-dwolla [personal$]="personal$"></app-purchase-details-dwolla>
```

```typescript
const personal = {
    hasAdditionalDocument: true,
    hasSsnDocument: true,
    ssnDocumentFileName: 'string',
    ssnDocumentUrl: 'string',
    ssnDocumentAwsKey: 'string',
    ssnDocumentId: 'string',
    additionalDocumentUrl: 'string',
    additionalDocumentFileName: 'string',
    additionalDocumentId: 'string',
};

const personal$ = of(personal);
```

<br/>
