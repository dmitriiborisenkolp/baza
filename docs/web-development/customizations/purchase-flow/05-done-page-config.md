# Customizing Purchase Flow

BAZA purchase flow libraries allow us to customize certain aspects of the purchase flow which include custom done page (last step) configuration.

<br/>

## Example: Customizing Done Page (Last Step)

This example talks about adding a custom done page configuration to purchase flow. Please note that by default, the done page configuration is as follows:

```html
<app-purchase-done-dwolla [config]="config"></app-purchase-done-dwolla>
```

```typescript
const config: PurchaseDoneConfig = {
    ctaBtnLink: {
        appLink: {
            commands: ['/portfolio'],
        },
    },
    cta: {
        link: {
            appLink: {
                commands: ['', { outlets: { modal: ['contact-us'] } }],
            },
        },
    },
};
```

<br/>

In order to achieve this customization, we would have to make the following changes:

-   Create a new `PurchaseCustomDone` Component
-   Update `PurchaseRoutingCustomModule` module to include this new component instead of using `PurchaseDoneComponent`
-   Update the code in this new `PurchaseCustomDone` component

Following is each step explained via code example:

<br/>

### Sample Code - Purchase Custom Done Component (TS)

```typescript
    customDoneConfig: CustomDoneConfig;

    constructor() {
        this.customDoneConfig = {
            ctaBtnLink: {
                appLink: {
                    commands: ['/portfolio'],
                },
            },
            cta: {
                link: {
                    appLink: {
                        commands: ['', { outlets: { modal: ['contact-us'] } }],
                    },
                },
            },
        };
    }
```

### Sample Code - Purchase Routing Custom Module (TS)

```typescript
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { getConfigurableRoutes, PurchaseDoneComponent } from '@scaliolabs/baza-nc-web-purchase-flow';
import { PurchaseCustomComponent } from './purchase-custom.component';
import { PurchaseCustomDoneComponent } from './purchase-custom-done.component';

const routes: Routes = getConfigurableRoutes(PurchaseCustomComponent, PurchaseCustomDoneComponent);

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PurchaseRoutingCustomModule {}
```
