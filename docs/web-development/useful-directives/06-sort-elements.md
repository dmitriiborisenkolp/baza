# Sort Elements

The `appSortElements` directive is used to sort elements within the same parent element according to the value of the "order" attribute of each element. It sorts all the immediate children of the element it is attached to.

## How to use

To use the directive, it's necessary to import the module `UtilModule` from `@scaliolabs/baza-web-utils`.

```ts
import { UtilModule } from '@scaliolabs/baza-web-utils';

@NgModule({
    imports: [UtilsModule],
})
export class MyModule {}
```

Then, simply add the `appSortElements` selector to the parent element in the component's template and the "order" attribute to its children:

```html
<div appSortElements>
    <div data-order="3">Third</div>
    <div data-order="2">Second</div>
    <div data-order="1">First</div>
</div>
```

After the directive is applied, the elements will be sorted in the following order:

```html
<div appSortElements>
    <div data-order="1">First</div>
    <div data-order="2">Second</div>
    <div data-order="3">Third</div>
</div>
```

It's important to note that this directive will only work on immediate children of the parent element and all elements should contain the "order" attribute, otherwise an error will be thrown.
