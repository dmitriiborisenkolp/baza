# Password Input

The `appPasswordInput` is used to add a toggle button to a password input field. When the button is clicked, the input's type will switch between 'password' and 'text', allowing the user to view or hide the password they have entered.

## How to use

To use the directive, it's necessary to import the module `UtilModule` from `@scaliolabs/baza-web-utils`.

```ts
import { UtilModule } from '@scaliolabs/baza-web-utils';

@NgModule({
    imports: [UtilsModule],
})
export class MyModule {}
```

Then, simply add the `appPasswordInput` selector to the input element in the component's template.

```html
<input
    type="password"
    appPasswordInput />
```
