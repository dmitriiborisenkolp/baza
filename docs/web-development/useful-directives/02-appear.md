# Appear

The `appElementAppear` directive is an Angular directive that detects when an element appears on the screen and emits an event. It is typically used to detect when an element comes into view, and perform some action based on that. The directive is applied to an element in the template and listens to the scroll and resize events on the window. It saves the position of the element and the current scroll position and compares them to determine if the element is currently visible. When the element becomes visible, the directive emits an event using an `EventEmitter`.

## How to use

To use the directive, it's necessary to import the module `UtilModule` from `@scaliolabs/baza-web-utils`.

```ts
import { UtilModule } from '@scaliolabs/baza-web-utils';

@NgModule({
    imports: [UtilsModule],
})
export class MyModule {}
```

Then, you can apply it to an element in the template and listen to the appear event in the component's code. The event will be emitted when the element comes into view.

```html
<div
    appElementAppear
    (appear)="onAppear()"></div>
```

In the component's code, the `onAppear` function can be added to handle the event.

```ts
onAppear() {
    console.log('Element has appeared on screen');
}
```

Some possible use cases for the `appElementAppear` directive are triggering animations or loading additional content when an element appears on the screen.
