# Autocomplete Off

The `appAutocompleteOff` directive can be applied to any element that contains input elements. When the directive is applied, it will search for all input elements within the element it is applied to and set the autocomplete attribute to `new-password`. This will turn off the browser's autocomplete feature for those inputs.

## How to use

To use the directive, it's necessary to import the module `UtilModule` from `@scaliolabs/baza-web-utils`.

```ts
import { UtilModule } from '@scaliolabs/baza-web-utils';

@NgModule({
    imports: [UtilsModule],
})
export class MyModule {}
```

Then, simply apply it to the desired element in the template, like so:

```html
<div appAutocompleteOff>
    <input
        type="text"
        placeholder="Enter your username" />
    <input
        type="password"
        placeholder="Enter your password" />
</div>
```

This will disable the autocomplete feature for both the username and password input fields.
