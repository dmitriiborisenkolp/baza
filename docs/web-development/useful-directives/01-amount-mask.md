# Amount Mask

The `appAmountMask` directive is used to add an amount mask to an input element that uses the ng-zorro-antd input number component. This directive can be used to restrict the input of a user to a valid amount format, which is a number with up to one decimal point.

The directive works by applying a regular expression to the input field, allowing only numbers and one decimal point to be entered. Additionally, it prevents more than one decimal point from being entered. This ensures that the input field only accepts valid amount values.

## How to use

To use the directive, it's necessary to import the module `UtilModule` from `@scaliolabs/baza-web-utils`.

```ts
import { UtilModule } from '@scaliolabs/baza-web-utils';

@NgModule({
    imports: [UtilsModule],
})
export class MyModule {}
```

Then, simply add the `appAmountMask` selector to the desired input field in your component's template. The `maxLength` input property can also be added to set a maximum length for the input field.

```html
<nz-input-number
    appAmountMask
    [maxLength]="10"></nz-input-number>
```
