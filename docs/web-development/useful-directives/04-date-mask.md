# Date Mask

The `appDateMask` directive can be added to a `NzDatePickerComponent` to enable a date input mask. It will format the date input as "MM/DD/YYYY" and will limit the date input to a date range (1-12 for month, 1-31 for day, 1900-9999 for year).

## How to use

To use the directive, it's necessary to import the module `UtilModule` from `@scaliolabs/baza-web-utils`.

```ts
import { UtilModule } from '@scaliolabs/baza-web-utils';

@NgModule({
    imports: [UtilsModule],
})
export class MyModule {}
```

Then, simply add the `appDateMask` selector to the `NzDatePickerComponent` element in the component's template.

```html
<nz-date-picker
    nzPlaceHolder="Select Date"
    appDateMask></nz-date-picker>
```

The directive can also be added directly to an `input` element and it will still work as expected.

```html
<input appDateMask />
```
