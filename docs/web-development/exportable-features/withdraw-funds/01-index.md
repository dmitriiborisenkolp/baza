# Using "Withdraw Funds" Standalone Components

BAZA allow us to include "Withdraw Funds" feature to any target project application by using exportable components. Here's a guide on how to use this feature:

The withdrawal standalone component uses content project to allow target projects to create a custom trigger with keyword `withdrawTrigger` on whatever element you want e.g. below we have a `<div withdrawTrigger>` and a `<button>` element within it, but it's up to target projects to use any trigger they want e.g. `<a>` element.

Here's sample code on how we can use this feature:

**HTML**

```typescript
 <app-dwolla-withdraw
        #withdraw
        [initData]="initData$ | async"
        (withdrawTriggerEnableToggle)="isWithdrawTriggerEnabled = $event">
    <div withdrawTrigger>
        <button
            nz-button
            nzType="primary"
            type="button"
            (click)="withdraw.onWithdrawFundsClicked()"
            [disabled]="!isWithdrawTriggerEnabled">
                Withdraw Funds
        </button>
    </div>
</app-dwolla-withdraw>
```

**Code-Behind (Typescript)**

```typescript
@UntilDestroy()
@Component({
    selector: 'app-payment-method-dwolla',
    templateUrl: './payment-method-dwolla.component.html',
    styleUrls: ['./payment-method-dwolla.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentMethodDwollaComponent {
    // IMPORTANT to include as bootstrap data is required by withdraw component
    initData$ = this.store.select(BootstrapState.initData);

    // IMPORTANT to create a local variable to handle enable/disable state of withdraw trigger
    isWithdrawTriggerEnabled = false;

    constructor(private readonly store: Store, public readonly dss: DwollaSharedService, private readonly wts: BazaWebUtilSharedService) {
        // IMPORTANT to include, this will allow refresh of bootstrap data manually from app. level
        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }
}
```

**Notes:**

It's important to take note of the following points:

-   For bootstrap data to be available and pass to withdraw component as `@Input` parameter, it's important to include application level `BootstrapResolver` on the route in which withdraw component is to be integrated

-   We must include the `refreshInitData$` subscribe within target project component to allow request manual refresh of bootstrap data from application level

-   The withdraw selector is completely hidden in case user is a foreign investor, else disabled in case user does not have Dwolla OR does not have a valid cash-out account linked or has balance below $0.01

-   The withdraw trigger can be anything as components use content project. The withdrawal flow will trigger, for as long as sample HTML code format is followed i.e. we call `onWithdrawFundsClicked` method

-   The disabled state is handled by the withdraw component with help of `@Output` property named `withdrawTriggerEnableToggle` and a local variable, whose by default value would be `false`. In case all checks are verified internally by withdraw standalone component, the `@Output` property would be invoked with value true indicating button should be enabled

-   The withdrawal components are fully customizable from i18n perspective as they are part of "Web UI Components" library, please refer to this guide for more details:
    [Withdraw Funds i18n Customization](web-development/i18n/web-ui-components/11-withdraw-funds)

-   If required, a custom link configuration can be passed to withdraw component as input parameter with help of `@Input` property `config`. Here's a sample configuration:

**HTML**

```typescript
 <app-dwolla-withdraw
        #withdraw
        [initData]="initData$ | async"
        [config]="withdrawConfig"
        (withdrawTriggerEnableToggle)="isWithdrawTriggerEnabled = $event">
    <div withdrawTrigger>
        <button
            nz-button
            nzType="primary"
            type="button"
            (click)="withdraw.onWithdrawFundsClicked()"
            [disabled]="!isWithdrawTriggerEnabled">
                Withdraw Funds
        </button>
    </div>
</app-dwolla-withdraw>
```

**Code-Behind (Typescript)**

```typescript
@UntilDestroy()
@Component({
    selector: 'app-payment-method-dwolla',
    templateUrl: './payment-method-dwolla.component.html',
    styleUrls: ['./payment-method-dwolla.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaymentMethodDwollaComponent {
    // IMPORTANT to include as bootstrap data is required by withdraw component
    initData = this.store.select(BootstrapState.initData);

    // IMPORTANT to create a local variable to handle enable/disable state of withdraw trigger
    isWithdrawTriggerEnabled = false;

    // Custom Link Configuration (Optional)
    withdrawConfig: WithdrawConfig = {
        links: {
            contact: {
                appLink: {
                    text: 'Contact Us - Custom',
                    commands: ['', { outlets: { modal: ['contact-us'] } }],
                },
            },
        },
    };

    constructor(private readonly store: Store, public readonly dss: DwollaSharedService, private readonly wts: BazaWebUtilSharedService) {
        // IMPORTANT to include, this will allow refresh of bootstrap data manually from app. level
        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
            this.store.dispatch(new RequestAppBootstrapInitData());
        });
    }
}
```
