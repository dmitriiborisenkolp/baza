# 0003 – API – What's the difference between `CrudListRequestDto` and `CrudListRequestQueryDto`?

- `CrudListRequestDto` is used for complex `POST` list methods implementations, mostly by CMS.
- `CrudListRequestQueryDto` is used with `GET` method; Usually it's used for Public API.
- Avoid to use `POST` method for Public API methods. You will need to use `CrudListRequestQueryDto` for public API; Use `POST` with `CrudListRequestDto` for CMS API.
