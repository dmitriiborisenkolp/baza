# 0010 – API, CMS – How to customize list of applications in Maintenance CMS?

You can customize list of applications with `bazaApiBundleConfigBuilder` helper:

```typescript
bazaApiBundleConfigBuilder()
        .withApplications([Application.WEB, Application.IOS])
        // some other configuration below
```
