# 0007 – CMS, WEB - How to change default image quality, format and sizes for uploaded images?

You can specify quality, format and images sizes for each field in CMS, but all listed parameters are optional. If some of params are not specific for field, default values will be used.

Defaults parameters located in `AttachmentConstants` namespace. You can change values with `bazaApiBundleConfigBuilder` helper or directly change values in namespace. Your changes must be visible for API application.

## Default parameters

| **Parameter**                               	| **Type**              	| **Defaults**               	|
|---------------------------------------------	|-----------------------	|----------------------------	|
| `AttachmentConstants.DEFAULT_IMAGE_QUALITY` 	| Number                	| 81                        	|
| `AttachmentConstants.DEFAULT_IMAGE_FORMAT`  	| `AttachmentImageType` 	| `AttachmentImageType.JPEG` 	|
| `AttachmentConstants.DEFAULT_IMAGE_SIZES`   	| Array                 	| (see table below)          	|

## Default image sizes

| **Size**                 	| **Maximum Width** 	|
|--------------------------	|-------------------	|
| `AttachmentImageSize.XS` 	| 150               	|
| `AttachmentImageSize.SM` 	| 300               	|
| `AttachmentImageSize.MD` 	| 600               	|
| `AttachmentImageSize.XL` 	| 3500              	|


## Example

```typescript
// apps/api/src/lib/app.module.ts

bazaApiBundleConfigBuilder()
    // Every option below is optional to pass.
    .withImageUploadOptions({
        // Compression / Image quality
        defaultImageQuality: 85,
        
        // Image format. By default it's recommended to use JPEG
        defaultImageFormat: AttachmentImageType.PNG,
        
        // Image set. Uploaded images will be converted to 4 images with different sizes
        defaultImageSizes: [
            { size: AttachmentImageSize.XS, maxWidth: 320 },
            { size: AttachmentImageSize.SM, maxWidth: 720 },
            { size: AttachmentImageSize.MD, maxWidth: 980 },
            { size: AttachmentImageSize.XL, maxWidth: 1280 },
        ],
        
        // Background color for PNG -> JPEG conversion.
        // Background color replaces opacity which is not supported by JPEG
        defaultJpegBgColor: '#FFFFFF',
    })
```
