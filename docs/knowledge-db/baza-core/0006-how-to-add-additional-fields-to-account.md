# 0006 – CMS, API – How to add additional fields to Account?

- Account Entity (`AccountEntity`) contains additional `metadata` JSON object which may contains any custom data
- Metadata object is not published to public endpoints (Public `AccountDTO`). If you need some data from `AccountEntity.metadata`, you should re-export it with your API
- Baza CMS allows defining custom Metadata fields with the `bazaAccountCmsDefineMetadata` helper.
- You can define different metadata field sets depends on requested Account Role (`AccountRole.User`, `AccountRole.Admin`)

Read more at [Overview – Auth & Accounts](/overview/20-auth-and-accounts) documentation section.

## Option 1: Define Metadata Fields in Accounts CMS - Using CMS Bundle Config Builder:

```typescript
// app.module.ts

const asyncLabelEndpoint = (queryString: string) => new Promise((resolve) => {
    setTimeout(() => {
        resolve(
            ['Red', 'Yellow', 'Green', 'Blue'].filter((label) => {
                return (queryString || '').toString().trim().length > 0
                    ? label.toLowerCase().includes(queryString.toLowerCase())
                    : true;
            }),
        );
    }, 500);
});

bazaCmsBundleConfigBuilder()
    .withAccountCmsMetadata((payload /* You can find Account, ActivatedRoute, requested Account Role here */) => ({
        formGroup: {
            note: [undefined, [Validators.required]],
            requisites: [undefined, []],
            label: [undefined, []],
        },
        formBuilder: [
            // Divider
            {
                type: BazaFormBuilderStaticComponentType.Divider,
            },

            // Text Plain Field - Required
            {
                formControlName: 'note',
                type: BazaFormBuilderControlType.Text,
                label: 'Note',
                required: true,
            },

            // Upload Field
            {
                type: BazaFormBuilderControlType.BazaUpload,
                formControlName: 'requisites',
                label: 'Requisites',
            },

            // Select field with Async option
            {
                formControlName: 'label',
                type: BazaFormBuilderControlType.Select,
                label: 'Label',
                props: {
                    nzFilterOption: () => true,
                    nzShowSearch: true,
                    nzServerSearch: true,
                },
                afterViewInit: (componentRef: ComponentRef<NzSelectComponent>, portal) => {
                    componentRef.instance.nzOnSearch.pipe(
                        switchMap((queryString) => fromPromise(asyncLabelEndpoint(queryString))),
                        retryWhen(genericRetryStrategy()),
                    ).subscribe((response) => {
                        const definition = portal.definition as BazaFormControlFieldSelect;

                        definition.values = (response as Array<string>).map((label) => ({
                            value: label.toLowerCase(),
                            label,
                        }));
                        portal.updateProps();
                    });
                },
            },
        ],
    }));
```

## Option 2: Define Metadata Fields in Accounts CMS - Using `bazaAccountCmsDefineMetadata` helper:

```typescript
// Somewhere in your CMS, better app.module.ts

bazaAccountCmsDefineMetadata({
    formGroup: {
        note: [undefined, [Validators.required]],
    },
    formBuilder: [
        // Divider
        {
            type: BazaFormBuilderStaticComponentType.Divider,
        },

        // Text Plain Field - Required
        {
            formControlName: 'note',
            type: BazaFormBuilderControlType.Text,
            label: 'Note',
            required: true,
        },
    ],
});
```
