# 0002 – API – Why DB can contain multiple accounts with same email address?

- BAZA Accounts exists in two states: Active or Deactivated. Active accounts has email as unique identifier and users can use Active accounts to sign in. 
- BAZA Account can be deactivated by user request. Deactivating account will trigger `AccountApiEvent.BazaAccountDeactivated` event and additionally will anonymize personal data. However email address will still be in place due of side packages implementations or restrictions.
- User can register later a *new account* with same email. In this scenario DB will contains multiple accounts with same email with only single Active account and one or more deactivated accounts with same email.

## Additional information

- `BazaAccountRepository` has methods which explicitly says that they will work with active accounts only. There are methods like `hasActiveAccountWithId` but you will not find methods like `getAccount` here.

