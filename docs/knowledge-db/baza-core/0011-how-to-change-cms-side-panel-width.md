# 0011 – CMS – How to change Side Panel width for Auth and CMS pages?

You should use `bazaCmsBundleConfigBuilder` helper:

```typescript
bazaCmsBundleConfigBuilder()
    // Auth page
    .withAuthSidePanelWidth('800px')
    // CMS pages
    .withSidePanelWidth('380px');
```
