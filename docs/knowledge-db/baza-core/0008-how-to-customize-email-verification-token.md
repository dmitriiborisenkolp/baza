# 0008 – API – How to customize email verification token?

Default token generator for email verification could be changes with `bazaApiBundleConfigBuilder` helper:

```typescript
bazaApiBundleConfigBuilder().withCustomEmailVerificationTokenizer(
    () => (Math.floor(Math.random() * (999999 - 100000 + 1)) + 100000).toString(),
);
```
