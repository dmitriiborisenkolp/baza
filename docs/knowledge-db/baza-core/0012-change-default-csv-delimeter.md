# 0012 - CMS, API - How to change default CSV delimiter?

If you would like to change default `;` CSV delimiter, you should update `BAZA_CRUD_CONSTANTS.csvDefaultDelimiter` value. This update should be
placed in shared libraries - unless not all applications (CMS, API) will receive updates:

```typescript
// libs/your-project-shares/src/lib/index.ts

import { BAZA_CRUD_CONSTANTS } from '@scaliolabs/baza-core-shared';

BAZA_CRUD_CONSTANTS.csvDefaultDelimiter = ',';
```
