# 0001 – API – How to get current account?

You will need to use `AuthSessionService` in your controller. The service works only on controller level; You need to bypass account entity down to target services / repositories / anything else.

You don't need to use `@Req()` decorator or anything else.

## Example

```typescript
import { AuthGuard, AuthSessionService } from '@scaliolabs/baza-core-api';

@Controller()
@UseGuards(AuthGuard) // AuthSessionService will not be available without AuthGuard
export class ExampleController implements BazaNcTaxDocumentEndpoint {
    constructor(private readonly authSession: AuthSessionService, private readonly service: ExampleService) {}

    @ApiOkResponse()
    @Get(/* your endpoint */)
    async exampleMethod(): Promise<ExampleResponse> {
        const account = await this.authSession.getAccount();
        const serviceResponse = await this.service.exampleAction(account);

        // Anything else
    }
}
```

## Additional information

-   `AuthSessionService` also contains simple `accountId` getter. If you don't need to fetch full Account entity, use `const accountId = authSessionService.accountId`.
-   `AuthSessionService` contains information about current session. Use `authSessionService.session` snapshot or `authSessionService.session$` observable to fetch information about current JWT, JWT Payload or Account entity.
-   Avoid importing `AuthSessionService` in non-controller services. `AuthSessionService` has `Scope.REQUEST` scope; If you need to use it in services directly, make sure that your services also has `Scope.REQUEST` injection scope.
