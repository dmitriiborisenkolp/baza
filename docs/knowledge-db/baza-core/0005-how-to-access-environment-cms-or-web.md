# 0005 – CMS, WEB – How to access environment configuration (`environment.ts`) from library?

There are two options available:

1) Provide environment variables via NgModule / forRoot configuration (using Dynamic modules on `apps/` level)
2) Use `BazaNgCmsEnvironment` (CMS) or `BazaNgWebEnvironment` (WEB) services. These provided in root services will contains environment configuration.

## Example

```typescript
import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { withTrailingSlash } from '@scaliolabs/baza-core-shared';
import { DomSanitizer } from '@angular/platform-browser';
import { BazaNgWebEnvironment } from '@scaliolabs/baza-core-web';

@Component({
    templateUrl: './example.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExampleComponent {
    constructor(
        private readonly environment: BazaNgWebEnvironment,
        private readonly domSanitizer: DomSanitizer,
    ) {}

    get apiDocsUrl(): any {
        return this.domSanitizer.bypassSecurityTrustUrl(`${this.environment.current.apiEndpoint}/redoc`);
    }
}
```
