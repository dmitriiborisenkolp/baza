# 0004 – CMS – How to navigate to Not Found page in CMS?

You should use `BazaNotFoundService` service:

```typescript
import { BazaNotFoundService } from "@scaliolabs/baza-core-cms";

export class MyCmsComponent {
    constructor(
      private readonly notFound: BazaNotFoundService,
    ) {}
    
    someMethod(): void {
      this.notFound.navigateToNotFound();
    }
}
```
