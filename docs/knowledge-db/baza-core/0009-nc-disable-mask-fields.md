# 0009 – API – How to disable SSN / Credit Card / Bank Account fields masking?

By default Baza mask fields with sensitive information:

-   SSN
-   Bank Account Number
-   Bank Routing Number
-   Credit Card Number

You can disable it with `bazaNcApiConfig` helper:

```typescript
bazaNcApiConfig({
    maskSSNFields: false,
    maskCreditCardFields: false,
    maskBankDetailsFields: false,
});
```
