# API Bundle

To enable Baza NC API, you need:

1) Add environments to all your deployment targets
2) Configure webhooks
3) Import `BazaNcApiBundleModule` into your `ApiModule`

## Environment configuration

Read more about environments here: [Environment](/libraries/baza-nc/07-environment-configuration).

## Webhooks configuration

You will need to set up Webhook radio for your development targets and set up common Baza NC environments for other environments.

Read more about webhook configurations here: [Webhooks](/libraries/baza-nc/05-webhooks).

## API Module

You need to import `BazaNcApiBundleModule` into your `ApiModule`:

```typescript
import { DynamicModule, Global, Module } from '@nestjs/common';
import { bazaApiBundleConfigBuilder, BazaApiBundleModule } from '@scaliolabs/baza-core-api';
import { BazaNcApiBundleModule } from '@scaliolabs/baza-nc-api';
import { bazaNcSetCharset, NcCharset } from '@scaliolabs/baza-nc-shared';

bazaNcSetCharset(NcCharset.En);

@Global()
@Module({})
export class ApiModule {
    static forRootAsync(): DynamicModule {
        return {
            module: ApiModule,
            imports: [
                // baza-core API module
                BazaApiBundleModule.forRootAsync(bazaApiBundleConfigBuilder().config),

                // baza-nc API module
                BazaNcApiBundleModule.forRootAsync({}),
            ],
        };
    }
}
```
