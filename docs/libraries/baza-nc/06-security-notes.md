# Security Notes

Baza NC hugely uses Kafka as event bus. As far as Kafka has limited security options, you should be very careful with everything published via Kafka bus.

Don't use real credit cards or personal information of real clients for non-production environments. You can use [https://stripe.com/docs/testing]() to generate example credit card credentials.

## Development

- Recommended: Kafka instance should be under VPN, and developers should work within VPN space.
- Don't use real credit card credentials or any kind of real personal or bank data for QA & Testing. Kafka may be compromised, and your credentials or personal data can be stolen.

## Production

- Kafka instance for production / stage environments must be located in private network. There must be no external access to Kafka brokers due of possible publish events with sensitive data, such as personal information, bank / credit card details and many more.
- You **MUST NOT** add webhook for `getCreditCard` in NC Client Admin. North Capital publish CVC code and other credit card details with this webhook. Clients will be under huge risk of losing credit card and potentially lose real money, especially if Kafka is not properly secured.
