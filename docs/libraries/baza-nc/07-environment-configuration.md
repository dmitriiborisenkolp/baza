# Environment

You should generate ClientID / DeveloperKey before starting environment configuration.

## Obtaining ClientID / DeveloperKey

-   **Go to NC Client Admin and sign in with your credentials**
-   Navigate to "Administrative" tab
-   You will see client ID in table at top of page and list of generated developer keys. Use any of them or generate a new one.

## Environment variables

Kafka integration should be properly configured. Read [Overview – Kafka](overview/17-kafka) for more details.

## Common configuration

| Variable                                  | Required? | Type   | Values                                                                                                                                                     | Description                                                                                                                                                       | Default (for optional) |
| ----------------------------------------- | --------- | ------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------- |
| **BAZA_NORTH_CAPITAL_CLIENT_ID**          | Yes       | STRING | (Client Id from Transact API panel)                                                                                                                        | Transact API Client Id                                                                                                                                            |                        |
| **BAZA_NORTH_CAPITAL_CLIENT_SECRET**      | Yes       | STRING | (Developer Key from Transact API Panel)                                                                                                                    | Transact API Client Secret                                                                                                                                        |                        |
| **BAZA_NORTH_CAPITAL_BASE_URL**           | Yes       | STRING | Development: https://api-sandboxdash.norcapsecurities.com/tapiv3/index.php/v3 Production: https://api-sandboxdash.norcapsecurities.com/tapiv3/index.php/v3 | Base URL of Transact API                                                                                                                                          |                        |
| **BAZA_NORTH_CAPITAL_DEBUG**              | No        | BOOL   | 0 or 1                                                                                                                                                     | Enable additional debug information for Account Verification. Should be disabled for production environment.                                                      | 0                      |
| **BAZA_NORTH_CAPITAL_DEBUG_NESTJS_LOG**   | No        | BOOL   | 0 or 1                                                                                                                                                     | Enables additional debug log for Transact API. This option leads to huge log usage. It must be enabled anyway for all environments, especially during development | 0                      |
| **BAZA_NORTH_CAPITAL_PURCHASE_FLOW_TTL**  | Yes       | INT    | Seconds (usually 3600)                                                                                                                                     | How long purchase flow will be active for current user session                                                                                                    |                        |
| **BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND** | Yes       | BOOL   | Enables sending Webhooks to Kafka. Required for `test` instance to be enabled.                                                                             | How long purchase flow will be active for current user session                                                                                                    |                        |
| **BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_READ** | Yes       | BOOL   | Enables reading Webhooks from Kafka. Usually it should be disabled                                                                                         | How long purchase flow will be active for current user session                                                                                                    |                        |

## Additional configuration for Webhook Radio instance

You need following environment variable to turn your instance into NC Webhook Radio:

| Variable                                 | Required? | Type | Values | Description                                                                  | Default (for optional) |
| ---------------------------------------- | --------- | ---- | ------ | ---------------------------------------------------------------------------- | ---------------------- |
| **BAZA_NORTH_CAPITAL_PURCHASE_FLOW_TTL** | No        | BOOL | 0 or 1 | Events received with NC Webhooks will processed by every node in Kafka group | 0 /                    |
