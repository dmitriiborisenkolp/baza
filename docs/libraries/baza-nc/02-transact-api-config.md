# Baza NC – Transact API Config

North Capital has some limitations and issues with input arguments. Baza NC package automatically validate & fix JSON Body for every request to NC Transact API.

## Charset Filtering

Baza NC will throw an error with `BazaNcCharacterIsNotAllowed` (`BazaNcApiErrorCodes.BazaNcCharacterIsNotAllowed`) error code for any attempt to use any kind of symbols which may be not correctly handled by NC API. You can use `bazaNcSetCharset` or `bazaNcSetCustomCharset` helpers to configure
range of accepted characters.

This feature is enabled by default with `NcCharset.EN` charset.

```typescript
import {
    bazaNcSetCharset,
    bazaNcSetCustomCharset,
    bazaNcApiConfig,
    NcCharset,
    ncCharsetEn,
    ncCharsetLatin1,
} from '@scaliolabs/baza-nc-shared';

// Enable charset filtering feature
bazaNcApiConfig({
    enableCharsetFiltering: true,
});

// EN Charset - Users will be allowed to use only QWERTY keyboard space
bazaNcSetCharset(NcCharset.En);

// Latin1 Charset - Users will be allowed to use Latin1 space.
// NC uses Latin1 charset for their DB storage - that's why here is specific charset defined for Latin1
bazaNcSetCharset(NcCharset.Latin1);

// You can define your own charset:
bazaNcSetCharset('ですは');

// You can also extends default charset set:
bazaNcSetCharset(ncCharsetEn + 'ですは'); // QWERTY + Custom Character
bazaNcSetCharset(ncCharsetLatin1 + 'ですは'); // Latin1 + Custom Character
```

Some fields may not need charset validation. Usually it's filenames, subscription templates and any other external resources. You can exclude additional fields:

```typescript
import { bazaNcApiConfig, ncDisableCharsetCheckForFields } from '@scaliolabs/baza-nc-shared';

bazaNcApiConfig({
    disableCharsetCheckForFields: [...ncDisableCharsetCheckForFields, 'some_custom_field'],
});
```

## Auto-Trim Feature

Baza NC can automatically trim input strings.

This feature is enabled by default.

```typescript
import { bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';

// Disable Auto-Trim
bazaNcApiConfig({
    enableAutoTrim: false,
});
```

## Auto-Fix Tab Bug

NC API incorrectly translate TAB (\t) character and converts `\t` character to `t` string. Baza NC automaticall replaces tab characters with double space string.

This feature is enabled by default.

```typescript
import { bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';

// Disable Auto-Fix Tab Bug
bazaNcApiConfig({
    fixTabSymbolBug: false,
});
```

## Enable Transaction Fee for offering CMS API

Enable Transaction fee feature for CMS API, this will allow CMS user to set transaction fee for offerings, This features also needs `withTransactionFeeFeature` enabled on CMS side.

`Once enabled and fees set for a offerings cannot be reset, this will also effect webhooks and trade sync's.`

This feature is disabled by default.

```typescript
import { bazaNcApiConfig } from '@scaliolabs/baza-nc-shared';

// Enable Transaction fee for API
bazaNcApiConfig({
    withTransactionFeeFeature: true,
});
```
