# CMS Bundle

If you would like to add North Capital section for CMS, you'll need to import `BazaNcBundlesCmsModule` module and related routes / menu configuration.

Here is minimal setup for Baza NC CMS:

```typescript
// Add menu items
bazaCmsBundleConfigBuilder()
    .withSidePanelGroups(() => ([
        ...bazaNcBundlesCmsMenu,
    ]));

// Extra features
bazaNcBundleCmsConfig({
    withImportOfferingFeature: true,
    withForgetOfferingFeature: true,
    withTaxDocumentsFeature: true,
    withTransactionFeeFeature: true
});

// App module
@NgModule({
    imports: [
        RouterModule.forRoot(bazaCmsBundleRoutesFactory([
            ...bazaNcBundlesCmsRoutes,
        ]), {
            initialNavigation: 'enabled',
            enableTracing: false,
            scrollPositionRestoration: 'enabled',
        }),
        BazaCmsBundleModule.forRoot(bazaCmsBundleConfigBuilder().config),
        BazaNcBundlesCmsModule,
    ],
    bootstrap: [
        BazaCmsAppComponent,
    ],
})
export class AppModule {
}
```

## Extra configuration

Baza NC CMS has additional extra features which are disabled by default. This features can be enabled with `bazaNcBundleCmsConfig` helper.

| Feature                     	| Description                                                                                                                                                        	|
|-----------------------------	|--------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| `withImportOfferingFeature` 	| Displays Import Offering in Offering CMS                                                                                                                           	|
| `withForgetOfferingFeature` 	| Displays Forget Offering in Offering CMS. It may be usefull if you have One-To-One TypeORM relations and you need to remove offerings which are incorrectly added. 	|
| `withTaxDocumentsFeature`   	| Displays Tax Documents column & CMS in Investor Account CMS                                                                                                        	|
| `withTransactionFeeFeature`   | Display Transaction Fees in CMS, this features also needs `withTransactionFeeFeature` enabled on API side for NC logic to increment/decrement fees                                                                                                         	|
| `withEliteInvestorsFeature`           | Displays EliteInvestors Club in Listings CMS                             |
