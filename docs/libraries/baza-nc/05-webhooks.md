# Webhooks

Baza NC requires configured webhooks to properly update trades, offerings, trade statuses, account bank/credit card details and many more.

You can map all webhooks or you can map only webhooks specified in list below.

Webhooks configuration are located:

-   Sandbox: [https://api-sandboxdash.norcapsecurities.com/admin_v3/client/webhook]()
-   Production: [https://api.norcapsecurities.com/admin_v3/client/webhook]()

> ⚠️ **Do not add webhook for `getCreditCard` event!**
>
> North Capital publish CVC code and other credit card details with this webhook. Clients will be under huge risk of losing credit card
> and potentially lose real money, especially if Kafka is not properly secured.

## Webhook key

All webhooks should point at `https://{project-url}/north-capital-proxy/webhooks/publish/${WEBHOOK_KEY}/${WEBHOOK_METHOD}` urls.

-   `WEBHOOK_KEY` is unique password-like string which is specified in API environment variables.
-   `WEBHOOK_METHOD` is method listed in NC Client Admin Webhook selector.

### Examples:

-   Webhook for updateOffering: https://my-project-stg.your-company.io/north-capital-proxy/webhooks/publish/1cf4a604aae93f6fce50de68294d5e549d578be0/updateOffering
-   Webhook for createTrade: https://my-project-stg.your-company.io/north-capital-proxy/webhooks/publish/1cf4a604aae93f6fce50de68294d5e549d578be0/createTrade

## List of required webhooks

| Webhook                      | Description                                                                                                                                                                      |
| ---------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `createTrade`                | Webhook will add transactions to Transaction CMS if any of transaction is matched to existing offering                                                                           |
| `deleteExternalAccount`      | Webhook will update Bank Account details of Account                                                                                                                              |
| `deleteTrade`                | Webhook will delete transactions in Transaction CMS                                                                                                                              |
| `externalFundMove`           | Webhook will display investments in Investments screen in your application. If webhook is not set, users will not be able to see their purchases before next trade status update |
| `linkExternalAccount`        | Webhook will update Bank Account details of Account                                                                                                                              |
| `updateAccount`              | Webhook will update Account Verification details                                                                                                                                 |
| `updateExternalAccount`      | Webhook will update Bank Account details of Account                                                                                                                              |
| `updateKycAml`               | Webhook will update KYC/AML of Account (Account Verification status)                                                                                                             |
| `updateLinkExternalAccount`  | Webhook will update Bank Account details of Account                                                                                                                              |
| `updateOffering`             | Webhook will update Offering details                                                                                                                                             |
| `updateParty`                | Webhook will update Account Verification details of Account                                                                                                                      |
| `updateTrade`                | Webhook will update trade details or status                                                                                                                                      |
| `updateTradeStatus`          | Webhook will update trade status                                                                                                                                                 |
| `updateTradeTransactionType` | Webhook will update transaction type of trade (ACH / CreditCard / WIRE)                                                                                                          |

## Webhook Radio for development environments

Webhooks will be published with `EveryNode` pipeline by default. Deployment target will process events & related updates at least (and usually) once only per node cluster.

Development environments actually should receive webhook events despite is it already processed on other nodes or not. You should have specific test instance of your project, which will publish webhook events
using `EveryNode` pipeline.

You need following environment variables to turn your instance into NC Webhook Radio:

| Environment                             | Value |
| --------------------------------------- | ----- |
| `BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND` | 1     |
