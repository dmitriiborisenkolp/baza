# Introduction

Baza NC libraries enables integration with [North Capital](https://www.northcapital.com/) platform. This libraries provides API for Account Verification, Purchase Flow and cache / process
webhook events to maintain internal DB in actual state.

You can refer to (Transact API)[https://api-sandboxdash.norcapsecurities.com/admin_v3/documentation] documentation to get more details about North Capital API.
