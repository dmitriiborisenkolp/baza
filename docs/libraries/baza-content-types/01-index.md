# Content Types

Content Types include ready-to-use features. Most feature are optional:

- Blogs
- Categories
- Contacts
- Events
- Faq
- Jobs
- News
- Newsletters
- Pages
- Press
- Teams
- Testimonials
- Tags
- Timeline

## Configuration

Content Types has shared configuration with `bazaContentTypesConfigure` helper. You can enable / disable
content types with this helper and customize some options like image sizes.

Configuration should be set up both for API and CMS applications.

### Example: Enabling only Timeline and Tags content types:

```
// apps/api/src/app.module.ts
// apps/cms/src/app.module.ts

bazaContentTypesConfigure(() => ({
    enabled: [
        BazaContentTypes.Timeline,
        BazaContentTypes.Tags,
    ],
}));
```
