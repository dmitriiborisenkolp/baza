# Redirect URL for Plaid Integration

OAuth support is required in all Plaid integrations that connect to financial institutions. Without OAuth support, your end users will not be able to connect accounts from institutions that require OAuth, which includes several of the largest banks in the US. OAuth setup can be skipped only if your Plaid integration is limited to products that do not connect to financial institutions (Enrich, Identity Verification, Monitor, Document Income, and Wallet Onboard).

If you would like to use Plaid Integration with OAuth support, you should set up a page where users should be navigated to after finishing Plaid steps, create and register URL to this page as allowed Redirect URL in Plaid and set up Environment variables for supporting new flow on API side.

More details could be found on Plaid documentation page: https://plaid.com/docs/link/oauth/

## Setting up Redirect URL for API

Baza API is not using any Redirect URLs for Plaid Integration. You can enable it with provided a value for `BAZA_PLAID_REDIRECT_URL` environment variable:

-   You can use `{{ api }}`, `{{ cms }}` or `{{ web }}` tags in environment value. This tags will be replaced with values of `BAZA_APP_API_URL`, `BAZA_APP_CMS_URL` and `BAZA_APP_WEB_URL` environment variables
-   You can use `http` protocol for debugging reasons (Plaid Sandbox allows to use it). For production environment make sure you're using `https` schema in URL

## Using Baza NC Bank Account API with Redirect URL

Baza provides 2 endpoints to link Bank Accounts:

-   `/baza-nc/bank-account/link` - this endpoint provides `linkToken` which should be used to initialize Plaid SDK and open Plaid to user. You can pass `withRedirectUrl=1` in query params to enable redirection after finishing Plaid steps. You will be redirected to `BAZA_PLAID_REDIRECT_URL` with additional query params set by Plaid
-   `/baza-nc/bank-account/linkOnSuccess` - this endpoint should be called on page which is set as Redirect URL. You should have all necessary parameters from Query Params
