# Baza NC Integration Data Access

Package `baza-nc-integration-data-access` exports all Data Access services which could be used for Web application. You
can import / use data access services directly, use Data Access Facade service for easy access to all exported assets or create your
own Data Access services.

## Using Data Access Facade service

1. Import `BazaNcIntegrationDataAccessModule` module from `baza-nc-integration-data-access` package
2. Inject `BazaNcIntegrationDataAccess` service to your component, service or any other unit

### Services

| **API**               | **Service**                                 |
| --------------------- | ------------------------------------------- |
| **Favorite Listings** | `BazaNcIntegrationDataAccess.favorites`     |
| **Feed**              | `BazaNcIntegrationDataAccess.feed`          |
| **Investments**       | `BazaNcIntegrationDataAccess.investments`   |
| **Listings**          | `BazaNcIntegrationDataAccess.listings`      |
| **Perks**             | `BazaNcIntegrationDataAccess.perks`         |
| **Portfolio**         | `BazaNcIntegrationDataAccess.portfolio`     |
| **Search**            | `BazaNcIntegrationDataAccess.search`        |
| **Subscriptions**     | `BazaNcIntegrationDataAccess.subscriptions` |
| **Testimonials**      | `BazaNcIntegrationDataAccess.testimonials`  |

### Example

```typescript
import { BazaNcIntegrationDataAccess } from '@scaliolabs/baza-nc-integration-data-access';

@Component({
    /* ... */
})
export class MyComponent {
    constructor(public readonly dataAccess: BazaNcIntegrationDataAccess) {}

    listings(): void {
        this.dataAccess.listings
            .list({
                index: 1,
                size: 10,
            })
            .subscribe(/* ... */);
    }
}
```

## Using Data Access Modules & Services

You can directly import Data Access modules & services from `baza-nc-integration-data-access` package.

### Services

| **API**               | **Module**                                               | **Services**                                    |
| --------------------- | -------------------------------------------------------- | ----------------------------------------------- |
| **Favorite Listings** | `BazaNcIntegrationFavoriteDataAccessModule`              | `BazaNcIntegrationFavoriteDataAccess`           |
| **Feed**              | `BazaNcIntegrationFeedDataAccessModule`                  | `BazaNcIntegrationFeedDataAccess`               |
| **Investments**       | `BazaNcIntegrationInvestmentsDataAccessModule`           | `BazaNcIntegrationInvestmentsDataAccess`        |
| **Listings**          | `BazaNcIntegrationListingsDataAccessModule`              | `BazaNcIntegrationListingsDataAccess`           |
| **Perks**             | `BazaNcIntegrationPerkDataAccessModule`                  | `BazaNcIntegrationPerkDataAccess`               |
| **Portfolio**         | `BazaNcIntegrationPortfolioDataAccessModule`             | `BazaPortfolioDataAccess`                       |
| **Search**            | `BazaNcIntegrationSearchDataAccessModule`                | `BazaNcIntegrationSearchDataAccess`             |
| **Subscriptions**     | `BazaNcIntegrationSubscriptionDataAccessModule`          | `BazaNcIntegrationSubscriptionDataAccess`       |
| **Testimonials**      | `BazaNcIntegrationListingTestimonialCmsDataAccessModule` | `BazaNcIntegrationListingTestimonialDataAccess` |

## Using custom Data Access modules & services

You can also create your own Data Access services using exported Endpoints & Endpoint Paths from `baza-nc-integration-shared` package.

### Services

| **API**               | **Endpoint**                                  | **Paths (ENUM)**                                   |
| --------------------- | --------------------------------------------- | -------------------------------------------------- |
| **Favorite Listings** | `BazaNcIntegrationFavoriteEndpoint`           | `BazaNcIntegrationFavoriteEndpointPaths`           |
| **Feed**              | `BazaNcIntegrationFeedEndpoint`               | `BazaNcIntegrationFeedEndpointPaths`               |
| **Investments**       | `BazaNcIntegrationInvestmentsEndpoint`        | `BazaNcIntegrationInvestmentsEndpointPaths`        |
| **Listings**          | `BazaNcIntegrationListingsEndpoint`           | `BazaNcIntegrationListingsEndpointPaths`           |
| **Perks**             | `BazaNcIntegrationPerkEndpoint`               | `BazaNcIntegrationPerkEndpointPaths`               |
| **Portfolio**         | `BazaNcIntegrationPortfolioEndpoint`          | `BazaNcIntegrationPortfolioEndpointPaths`          |
| **Search**            | `BazaNcSearchEndpoint`                        | `BazaNcSearchEndpointPaths`                        |
| **Subscriptions**     | `BazaNcIntegrationSubscriptionEndpoint`       | `BazaNcIntegrationSubscriptionEndpointPaths`       |
| **Testimonials**      | `BazaNcIntegrationListingTestimonialEndpoint` | `BazaNcIntegrationListingTestimonialEndpointPaths` |
