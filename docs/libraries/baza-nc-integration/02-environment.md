# Baza NC Integration – Environment

You should set up Bitly account and get Bitly access token. Bitly is used for Feed API.

[https://support.bitly.com/hc/en-us/articles/230647907-How-do-I-generate-an-OAuth-access-token-for-the-Bitly-API-]()

| Variable                                            	| Required? 	| Type   	| Values               |
|-----------------------------------------------------	|-----------	|--------	|--------------------- /
| **BAZA_NC_INTEGRATION_BITLY_ACCESS_TOKEN**            | Yes       	| STRING 	| Bitly Access Token   |
