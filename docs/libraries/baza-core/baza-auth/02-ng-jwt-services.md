# JWT Services

Baza provides basic service layer to work with storing JWT and setting up form validators for sign in / sign up
forms .

```typescript
import { JwtService } from '@scaliolabs/baza-core-ng';
```

JwtService is responsible for storing or destroying JWT session. JWT Session can be stored in Local Storage,
Session Storage and/or Document Cookie. You can enable multiple storages at once.

## (Sign In Feature) Setting up JWT session

When you're implementing Sign In form for your Web application, you will need to:

1) Attempts user to authenticate with `BazaAuthDataAccess.auth` method
2) If successful, you need to set up JWT session:

```typescript
import { JwtService } from '@scaliolabs/baza-core-ng';
import { BazaAuthDataAccess } from '@scaliolabs/baza-core-data-access'; 

export class MyComponent {
    constructor(
        private readonly jwt: JwtService,
        private readonly authDataAccess: BazaAuthDataAccess,
    ) {}

    signIn(email: string, password: string): void {
        this.authDataAccess.auth({ email, password }).subscribe(
            (response) => {
                this.jwtService.setJwtWithAuthResponse(response);
                
                // any post-sign-in actions here
            },
        );
    }
}
```

You also can use `AuthValidatorsService` service which will factory ReactiveForm validators.

## (Sign Up Feature) Sign Up Form

Use `AuthValidatorsService` service which will factory ReactiveForm validators for your Sign Up form.

Password validation has more layers of security. For password validation, you have 2 possible ways to implement:

- FE-based solution: Use helpers from `@scaliolabs/baza-core-shared` package: `bazaValidatePassword` and `bazaValidatePasswordResources` helpers.
- BE-based solution: Use `BazaPasswordDataAccess` Data Access service with `resources` and `validate` methods.

You can read more about Password Validation in [Baza Core - Password](/libraries/baza-core/baza-password/01-index) documentation section.

## (Sign Out Feature) Destroying JWT session

For Sign Out like features, you need to use `destroy()` method.

You must also send sign out requests to API. Unless even if user is signed out, a potential hacker still can
use same access/refresh tokens for a while.

Baza uses "Device" term as combination of Refresh Token + all generated Access Tokens for given Refresh Token.
There are 3 different methods API offers for Sign Out feature:

- `BazaAuthDataAccess.invalidateAllTokens` - works as "Sign Out from All Devices" feature. The method will destroys
all available sessions of account.
- `BazaAuthDataAccess.invalidateByRefreshToken` - works as "Sign Out from Current Device" feature. The method
will destroy current session by Refresh Token and will destroys all Access Tokens which was generated for given
Refresh Token
- `BazaAuthDataAccess.invalidateToken` - the method will destroy specific Access or Refresh Token. It's kinda
technical method and it should not be used in general.

```typescript
import { JwtService } from '@scaliolabs/baza-core-ng';
import { BazaAuthDataAccess } from '@scaliolabs/baza-core-data-access';

export class MyComponent {
    constructor(
        private readonly jwtService: JwtService,
        private readonly authDataAccess: BazaAuthDataAccess,
    ) {}
    
    signOutFromCurrentDevice(): void {
        this.authDataAccess.invalidateAllTokens().subscribe(() => {
            this.jwtService.destroy();

            // any post-sign-out actions here
        });
    }
    
    signOutFromAllDevices(): void {
        this.authDataAccess.invalidateByRefreshToken({
            refreshToken: this.jwtService.jwt.refreshToken,
        }).subscribe(() => {
            this.jwt.destroy();

            // any post-sign-out actions here
        });
    }
}
```

## Tracking Signed In status

JwtServices provides `jwt` and `jwt$` getter which allows you to track Sign In status.

## Setting Up storages

You can configure which storages will be used for JWT with `bazaWebBundleConfigBuilder` helper:

```typescript
import { bazaWebBundleConfigBuilder } from '@scaliolabs/baza-core-web';

bazaWebBundleConfigBuilder()
    .withJwtStorages({
        jwtStorages: {
            localStorage: {
                enabled: false,
            },
            sessionStorage: {
                enabled: true,
                jwtKey: 'myJwt',
                jwtPayloadKey: 'myJwtPayload',
            },
            documentCookieStorage: {
                enabled: false,
            }
        },
    });
```
