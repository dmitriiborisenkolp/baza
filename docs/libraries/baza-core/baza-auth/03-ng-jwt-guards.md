# JWT Guards

Baza provides multiple guards which allows you to control user access to specific routes.

All JWT Guards, Http Interceptors and Services are located in `@scaliolabs/baza-core-ng` package.

## Web Guards
- `JwtVerifyGuard` – This guard automatically verify current JWT access token. Guard verify tokens with some throttle (time intervals) between verification requests. You should cover your
account-protected routes with this guard. `JwtVerifyGuard` has interraction with `JwtHttpInterceptor`; usually you don't need to add additional redirect instruction for `JwtVerifyGuard` guard.
- `JwtRequireAuthGuard` – Protect route and allow users to visit the route or child routes only if current user is Signed In.
- `JwtRequireNoAuthGuard` – Protect route and allow users to visit the route or child routes only if current user is **NOT** Signed In.

## CMS Guards
- `JwtRequireAclGuard` – Requires specific ACL of current Admin account

## Roles guard
These guards have very exceptional cases where they can be used. Mostly for Web applications
you should use `JwtRequireAuthGuard` or `JwtRequireNoAuthGuard` guards.

- `JwtRequireRoleGuard` – Requires specific roles of current Account.
- `JwtRequireUserGuard` – (extends configuration from `JwtRequireRoleGuard`) Requires User role of current Account.
- `JwtRequireAdminGuard` – (extends configuration from `JwtRequireRoleGuard`) Requires Admin role of current Account.

## Guards configuration

Each guard has global configuration and per-route configuration. When guard is applied, guard will merge
global configuration and (if exists) route configuration.

Per-route configuration can be set with Route's Data object. You can use additional helper interfaces like
`BazaJwtRequiredAuthGuardRouteConfig` to properly type your Route's data object:

```typescript
import { Routes, ActivatedRouteSnapshot } from '@angular/router';

export const myRoutes: Routes = [{
    path: 'my-component',
    component: MyComponent,
    canActivateChild: [JwtRequireAuthGuard],
    data: {
        jwtRequireAuthGuard: {
            redirect: (route: ActivatedRouteSnapshot) => ({
                commands: ['/auth'],
            }),
        },
    } as BazaJwtRequiredAuthGuardRouteConfig,
}];
```

Global configuration can be set with:

1) Using `bazaWebBundleConfigBuilder` helper in `app.module.ts`:

```typescript
const config = bazaWebBundleConfigBuilder().withModuleConfigs({
      BazaAuthNgModule: (bundleConfig) => ({
          deps: [],
          useFactory: () => ({
              verifyJwtGuardConfig: {
                  // Your configuration
              },
              // Additional required configuration for BazaAuthNgModule
          }),
      }),
  });
```

2) Using `BAZA_WEB_BUNDLE_GUARD_CONFIGS` injectable constant:

```typescript
import { BAZA_WEB_BUNDLE_GUARD_CONFIGS } from '@scaliolabs/baza-core-web';

BAZA_WEB_BUNDLE_GUARD_CONFIGS.jwtRequireNoAuthGuard.redirect = () => ({
    commands: ['/'],
    navigationExtras: {
        queryParams: {
            signIn: 1,
        },
    },
});
```

## JWT Guards

### JwtRequireAuthGuard

```typescript
import { JwtRequireAuthGuard } from '@scaliolabs/baza-core/ng';
```

Router guard will not allow user to visit route if user is already signed in.
If user is signed in, he will be (by default) redirected to `/`.

You can override redirect configuration with route's data or with module configuration.

Guard can be used with canActivate or canActivateChild configurations.

#### Example: Using JwtRequireAuthGuard + custom redirect configuration

```typescript
const route = {
   path: 'my-component',
   component: MyComponent,
   canActivateChild: [JwtRequireAuthGuard],
   data: {
       // optionally you can override default guard configuration
       jwtRequireAuthGuard: {
           redirect: () => ({
             commands: ['/'],
           }),
       },
   } as BazaJwtRequiredAuthGuardRouteConfig,
};
```
#### Example: Replace redirects for JwtRequireAuthGuard on global level

```typescript
import { BAZA_WEB_BUNDLE_GUARD_CONFIGS } from '@scaliolabs/baza-core-web';
*
BAZA_WEB_BUNDLE_GUARD_CONFIGS.jwtRequireAuthGuard.redirect = () => ({
    commands: ['/'],
    navigationExtras: {
        queryParams: {
            signIn: 1,
        },
    },
});
```

### JwtRequireNoAuthGuard

```typescript
import { JwtRequireNoAuthGuard } from '@scaliolabs/baza-core/ng';
```

Router guard will not allow user to visit route if user is alreadt signed in.
If user is signed in, he will be (by default) redirected to /
You can override redirect configuration with route's data or with module configuration.

Guard can be used with canActivate or canActivateChild configurations.

#### Example: Using JwtRequireNoAuthGuard + custom redirect configuration
```typescript
const route = {
   path: 'my-component',
   component: MyComponent,
   canActivateChild: [JwtRequireNoAuthGuard],
   data: {
       // optionally you can override default guard configuration
       jwtRequireNoAuthGuard: {
           redirect: () => ({
             commands: ['/'],
           }),
       },
   } as BazaJwtRequiredNoAuthGuardRouteConfig,
}
```

#### Example: Replace redirects for JwtRequireNoAuthGuard on global level

```typescript
import { BAZA_WEB_BUNDLE_GUARD_CONFIGS } from '@scaliolabs/baza-core-web';

BAZA_WEB_BUNDLE_GUARD_CONFIGS.jwtRequireNoAuthGuard.redirect = () => ({
    commands: ['/'],
    navigationExtras: {
        queryParams: {
            signIn: 1,
        },
    },
});
```

### JwtRequireAclGuard

```typescript
import { JwtRequireAclGuard } from '@scaliolabs/baza-core/ng';
```

Router guard which will not allow user to access route if he's not
an Admin or don't have required ACL.

Required ACL's are configured `acl` array in Data object of route.

Guard can be used with canActivate or canActivateChild configurations.

#### Example

```typescript
  import { Routes } from '@angular/router';
  import { JwtRequireAclGuard } from '@scaliolabs/baza-core-ng';
  import { MyComponent } from './components/my/my.component';
  import { MyAcl } from '@scaliolabs/my-shared';
 *
 export const myRoutes: Routes = [{
     path: 'my-route',
     component: MyComponent,
     canActivateChild: [
         JwtRequireAclGuard,
     ],
     data: {
         acl: [MyAcl.MyFeature],
     },
}];
```

### JwtRequireAdminGuard

```typescript
import { JwtRequireAdminGuard } from '@scaliolabs/baza-core/ng';
```

Route guard which will requires Admin role from current user to access.

`JwtRequireAdminGuard` uses `JwtRequireRoleGuard` configuration. If you
need to change default redirects or any kind of configuration for `JwtRequireRoleGuard`,
you should do it with implementing `JwtRequireAdminGuardRouteConfig` interface
in route's Data object or with `JwtRequireRoleGuard` configuration.

Guard can be used with canActivate or canActivateChild configurations.

#### Example: Change redirect specifically for route and specifically for JwtRequireAdminGuard:

```typescript
import { MyComponent } from './my/my.cpmponent';
import { ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JwtRequireAdminGuard, JwtRequireAdminGuardRouteConfig } from './libs/baza-core-ng/src';

export const myRoutes: Routes = [
{
       path: 'my',
       component: MyComponent,
       canActivateChild: [JwtRequireAdminGuard],
       data: {
           jwtRequireAdminGuard: {
               redirect: (route: ActivatedRouteSnapshot) => ({
                   commands: ['/'],
                   navigationExtras: {
                       queryParams: {
                           login: 1,
                       },
                   },
               }),
           },
       } as JwtRequireAdminGuardRouteConfig,
   },
];
```

#### Example: Change redirect on global level.

```typescript
import { BAZA_WEB_BUNDLE_GUARD_CONFIGS } from './libs/baza-core-web/src';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AccountRole } from './libs/baza-core-shared/src';

BAZA_WEB_BUNDLE_GUARD_CONFIGS.requireRoleGuardConfig.redirect= (route: ActivatedRouteSnapshot, requestedRoles: Array<AccountRole>) => {
   if (requestedRoles.includes(AccountRole.Admin)) {
       return {
           commands: ['/'],
           navigationExtras: {
               queryParams: {
                   login: 1,
                   admin: 1,
               },
           },
       };
   } else {
       return {
           commands: ['/'],
           navigationExtras: {
               queryParams: {
                   login: 1,
                   user: 1,
               },
           },
       };
   }
};
```

### JwtRequireRoleGuard

```typescript
import { JwtRequireRoleGuard } from '@scaliolabs/baza-core/ng';
```

Common guard which will require that account has one of specified roles
List of allowed roles should be set with `roles` field of route's Data object

Guard can be used with canActivate or canActivateChild configurations.

#### Example

```typescript
import { Routes } from '@angular/router';
import { JwtRequireRoleGuard } from '@scaliolabs/baza-core-ng';
import { MyComponent } from './components/my/my.component';
import { MyAcl } from '@scaliolabs/my-shared';
import { AccountRole } from '@scaliolabs/baza-core-shared';

export const myRoutes: Routes = [{
    path: 'my-route',
    component: MyComponent,
    canActivateChild: [
        JwtRequireRoleGuard,
    ],
    data: {
        jwtRequireRoleGuard: {
            roles: [AccountRole.User, AccountRole.Admin],
            // Optionally - you can set up custom redirect
            redirect: ((route: ActivatedRouteSnapshot, requestedRoles: Array<AccountRole>) => ({
                commands: ['/'],
                navigationExtras: {
                     queryParams: {
                         login: 1,
                     },
                 },
             })),
         },
     },
}];
```

### JwtRequireUserGuard

```typescript
import { JwtRequireUserGuard } from '@scaliolabs/baza-core/ng';
```

Route guard which will requires User role from current user to access.

`JwtRequireUserGuard` uses `JwtRequireRoleGuard` configuration. If you
need to change default redirects or any kind of configuration for `JwtRequireRoleGuard`,
you should do it with implementing `JwtRequireUserGuardRouteConfig` interface
in route's Data object or with `JwtRequireRoleGuard` configuration.

Guard can be used with canActivate or canActivateChild configurations.

#### Example: Change redirect specifically for route and specifically for JwtRequireUserGuard:

```typescript
import { MyComponent } from './my/my.cpmponent';
import { ActivatedRouteSnapshot, Routes } from '@angular/router';
import { JwtRequireUserGuard, JwtRequireUserGuardRouteConfig } from './libs/baza-core-ng/src';

export const myRoutes: Routes = [
{
       path: 'my',
       component: MyComponent,
       canActivateChild: [JwtRequireUserGuard],
       data: {
           jwtRequireUserGuard: {
               redirect: (route: ActivatedRouteSnapshot) => ({
                   commands: ['/'],
                   navigationExtras: {
                       queryParams: {
                           login: 1,
                       },
                   },
               }),
           },
       } as JwtRequireUserGuardRouteConfig,
   },
];
```

#### Example: Change redirect on global level.

```typescript
import { BAZA_WEB_BUNDLE_GUARD_CONFIGS } from './libs/baza-core-web/src';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AccountRole } from './libs/baza-core-shared/src';

BAZA_WEB_BUNDLE_GUARD_CONFIGS.requireRoleGuardConfig.redirect= (route: ActivatedRouteSnapshot, requestedRoles: Array<AccountRole>) => {
   if (requestedRoles.includes(AccountRole.User)) {
       return {
           commands: ['/'],
           navigationExtras: {
               queryParams: {
                   login: 1,
                   user: 1,
               },
           },
       };
   } else {
       return {
           commands: ['/'],
           navigationExtras: {
               queryParams: {
                   login: 1,
                   user: 1,
               },
           },
       };
   }
};
```

### JwtVerifyGuard

```typescript
import { JwtVerifyGuard } from '@scaliolabs/baza-core/ng';
```

JWT Verify guard which will perform JWT Token verification when visiting
current or child routes. If JWT token is not valid, the guard will redirect
user to Sign In page configured in `BazaJwtVerifyGuardConfig`.

JWT verification by default has some throttle, i.e. token verification
will not be performed if it was already executed not so much long time before.

Guard can be used with canActivate or canActivateChild configurations.

#### Example: Global configuration with injectable constants

If you need to replace redirect URL on global level, you can do it somewhere
in you AppModule:

```typescript
import { BAZA_WEB_BUNDLE_GUARD_CONFIGS } from '@scaliolabs/baza-core-web';

BAZA_WEB_BUNDLE_GUARD_CONFIGS.verifyJwtGuardConfig.verifySameJwtThrottle = 300; // every 300 seconds
BAZA_WEB_BUNDLE_GUARD_CONFIGS.verifyJwtGuardConfig.redirect = () => ({
    commands: ['/'],
    navigationExtras: {
        queryParams: {
            signIn: 1,
        },
    },
});
```

#### Example: per-Route configuration

You can override configuration for guard with route Data object implements BazaBazaJwtVerifyGuardRouteConfig
interface

```typescript
import { MyComponent } from './my/my.component';
import { ActivatedRouteSnapshot, Routes } from '@angular/router';
import { BazaBazaJwtVerifyGuardRouteConfig, JwtRequireAdminGuard, JwtRequireAdminGuardRouteConfig, JwtVerifyGuard } from './libs/baza-core-ng/src';

export const myRoutes: Routes = [
{
       path: 'my',
       component: MyComponent,
       canActivateChild: [JwtVerifyGuard],
       data: {
           jwtVerifyGuard: {
               verifySameJwtThrottle: 300, // every 300 seconds
               redirect: (route: ActivatedRouteSnapshot) => ({
                   commands: ['/'],
                   navigationExtras: {
                       queryParams: {
                           login: 1,
                       },
                   },
               }),
           },
       } as BazaBazaJwtVerifyGuardRouteConfig,
   },
];
```

#### Example: Global Level configuration

If you need to replace configuration on global level, you can override default
configuration with bazaWebBundleConfigBuilder.

```typescript
const config = bazaWebBundleConfigBuilder().withModuleConfigs({
      BazaAuthNgModule: (bundleConfig) => ({
          deps: [],
          useFactory: () => ({
              verifyJwtGuardConfig: {
                  // Your configuration
              },
              // Additional required configuration for BazaAuthNgModule
          }),
      }),
  })
```
