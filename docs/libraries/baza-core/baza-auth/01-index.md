# Baza Core – Auth

Baza contains Authentication & Authorization API and shared services & guards for implementing Auth feature
for Web projects.

## Index

Read following articles to implement Sign In, Sign Out and other auth-related features for your application:

- [JWT Services](/libraries/baza-core/baza-auth/02-ng-jwt-services)
- [JWT Guards](/libraries/baza-core/baza-auth/03-ng-jwt-guards)
- [JWT Http Interceptors](/libraries/baza-core/baza-auth/04-ng-jwt-http-interceptors)
