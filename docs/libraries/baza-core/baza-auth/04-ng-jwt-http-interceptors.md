# Jwt HTTP Interceptors

```typescript
import { JwtHttpInterceptor } from '@scaliolabs/baza-core-ng';
```

Baza provides JWT Http Interceptor which automatically attempts to update access tokens if current one
is outdated.

JWT Http Interceptor is set up automatically with `BazaWebBundleModule` module.

## Configuring JwtHttpInterceptor

You can change default configuration of `JwtHttpInterceptor` with `bazaWebBundleConfigBuilder` helper or
with `BAZA_WEB_BUNDLE_GUARD_CONFIGS` injectable constant.

1) Using `bazaWebBundleConfigBuilder` helper:

```typescript
const config = bazaWebBundleConfigBuilder().withModuleConfigs({
      BazaAuthNgModule: (bundleConfig) => ({
          deps: [],
          useFactory: () => ({
              jwtHttpInterceptorConfig: {
                  // Your configuration
              },
              // Additional required configuration for BazaAuthNgModule
          }),
      }),
  })
```

2) Using `BAZA_WEB_BUNDLE_GUARD_CONFIGS` config:

```typescript
import { BAZA_WEB_BUNDLE_GUARD_CONFIGS } from '@scaliolabs/baza-core-web`;

BAZA_WEB_BUNDLE_GUARD_CONFIGS.jwtHttpInterceptorConfig.redirect = () => ({
    commands: ['/'],
    navigationExtras: {
        queryParams: {
            signIn: 1,
        },
    },
});
```
