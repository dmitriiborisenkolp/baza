# Publishing Baza libraries to NPM

This guide describes how to publish Baza libraries to NPM.

## NPM Versions Conventions

- We have **official** and **development** releases
- **Official** releases follows semver with increasing **MINOR** version. Official releases could be released only after approval from Baza team. These releases are attached to JIRA releases and could not be executed at any time.
- **Development** releases follows semver with increasing **PATCH** version. Development releases could be released by anyone.

[https://semver.org]()

## NPM 2FA Conventions

- All Baza libraries could be published only by NPM users which are added to `@scaliolabs` NPM organization.
- You must enable 2FA via QR Code for your account to be able to publish Baza libraries to NPM.

For 2FA application you can use Google Authenticator or Microsoft Authenticator application. Microsoft Authenticator
is recommended because if your device will be lost, it's impossible to restore same  Google Authenticator account
on your new device.

## Adding new NPM account

1. Create new NPM account: [https://www.npmjs.com/signup]()
2. Ask Baza Team to add your account to `@scaliolabs` organization
3. Sign in to your NPM account with `npm login` command
4. Run this command to enable OTP when publishing new NPM libraries:

```sh
npm profile enable-2fa auth-only
```

## Publishing libraries to NPM

1. Create a branch with `release/baza-1.N.M` name

2. Update Baza libraries `package.json` versions and peer dependencies:

```sh
node ./baza-bump-version.js 1.N.M # replace current version with new version
```

3. Commit your changes with `yarn commit` command. Use `release` type and use `1.N.M` as scope.

4. Create a PR with "Release: 1.N.M" title and wait for approval

5. When packages will be ready to process, go to CI/CD and approve publish-to-NPM step.

## Publishing libraries to NPM (local environment)

1. Update Baza libraries `package.json` versions and peer dependencies:

```sh
node ./baza-bump-version.js 1.N.M # replace current version with new version
```

2. (Recommended) Run lint before building and publishing Baza libraries to NPM. Fix lint errors if any occures:

```sh
yarn lint
```

3. Build all Baza libraries with Nx tools. Make sure that everything built without errors and issues:

```
yarn build
```

3. Sign in to your NPM account and generate NPM Publish Token: [https://www.npmjs.com/settings/dmitriiscalio/tokens](). Store your new generated Publish Token in safe place like 1Password if you would like to re-use same token in future
4. Export your NPM token:

```sh
export NPM_TOKEN=(your-new-generated-publish-token)
```

5. Publish NPM libraries using `publish-baza.sh` helper. You should have your 2FA Application open on your device.

```sh
./publish-baza.sh
```
