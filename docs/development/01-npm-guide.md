# Adding new Baza library

This guide describes how to add a new library to Baza which should be published to NPM.

## Naming conventions

- All libraries should be named with `baza-${package}-${scope}` pattern
- `Package` is set of libraries which works together on some features scope and which are supposed to be installed all together.
- `Scope` is target-specific library of package

| Scope           | Description                                                                                                                                      | Nx Generate              | Example              |
|-----------------|--------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------|----------------------|
| **shared**      | Contains shared DTO's, models, endpoint definitions or any resources which are supposed to be used on API, CMS, Web or any other scopes          | `nx g @nrwl/node:lib`    | baza-foo-shared      |
| **node-access** | Node Access is Nx Data Access library ([https://nx.dev/structure/library-types]()) which is used for API E2E integration tests or for API itself | `nx g @nrwl/node:lib`    | baza-foo-node-access |
| **data-access** | Data Access is Nx Data Access library ([https://nx.dev/structure/library-types]()) which is used for CMS, Web or any other Angular applications  | `nx g @nrwl/angular:lib` | baza-foo-data-access |
| **ng**          | Ng scope is used for Angular libraries which could be used in CMS, Web or any other Angular applications at same time                            | `nx g @nrwl/angular:lib` | baza-foo-ng          |
| **cms**         | CMS libraries                                                                                                                                    | `nx g @nrwl/angular:lib` | baza-foo-cms         |
| **web**         | Web libraries                                                                                                                                    | `nx g @nrwl/angular:lib` | baza-foo-web         |
| **web-...**     | Web libraries which implements different aspects of feature / package scope                                                                      | `nx g @nrwl/angular:lib` | baza-foo-web-bar     |
| **api**         | API libraries                                                                                                                                    | `nx g @nrwl/nest:lib`    | baza-foo-api         |

## Step 1: Generate Library

- Use [Nx Generate](https://nx.dev/cli/generate) command to generate new Nx library.
- You should generate library using `--publishable` and `--importPath` flags
- Libraries with same feature scope should have same prefix. As example, `baza-nc` feature includes `baza-nc-shared`, `baza-nc-data-access`, `baza-nc-cms` and `baza-nc-api` libraries.

```sh
nx g @nrwl/angular:lib baza-foo-cms --publishable --importPath=@scaliolabs/baza-foo-cms
```

## Step 2: Add library to `baza-bump-version.js`

- Tool `baza-bump-version.js` is used to synchronize versions and peer dependencies of packages.
- All packages built with Baza/Nx should always have same version
- Peer dependencies are optional, but it's recommended to add it to package definition

```js
const packageJSONs = [
    // First element is path to package.json, second is array of peer dependencies
    [`${__dirname}/libs/baza-foo-cms/package.json`, ['@scaliolabs/baza-foo-cms']],
];
```

## Step 3: Add publish command

- There is `publish-baza.sh` shell file exists in repository. This shell command is used to publish all baza libraries to NPM
- You must not publish some specific libraries with specific versions. If you're updating some of libraries, you should re-publish all baza libraries to NPM with new version.
- You should add new shell commands to `publish-baza.sh` with publish command for new library:

```sh
# ... some previous shell commands

# baza-foo
cd dist/libs/baza-foo-cms && npm publish && cd -
```

## Validate changes

Run `yarn build` and check is everything builds correctly or not.
