# Debugging Baza on Target projects

If you need to debug Baza on a target project and have access to Baza repository, you can replace the installed NPM libraries with the Baza source code. This will greatly simplify the debugging process. 

## Steps

1. Copy & Paste libraries from Baza repository to the `libs/` directory of your project
2. Add paths in `tsconfig.base.json` for each library copied to your project

```js
"@scaliolabs/baza-core-api": ["libs/baza-core-api/src/index.ts"],
```
