# Injecting & External modifying CMS CRUDs

Baza is set of NPM libraries with ready-to-use CRUD CMS. Sometimes you may need to add additional column, add a link, change data source or make some other changes
with some specific CMS.

Every CMS from baza-\* packages has unique IDs. You can modify default CRUD configurations with `BazaCrudInjectService` service, adding additional callback for all CRUD which
are using specified ID.

```typescript
/**
 * Inject Configuration
 */
interface BazaCrudInjectFullConfig<T = any> {
    inject(config: BazaCrudConfig<T>): void;
}

interface BazaCrudInjectService {
    /**
     * Register Inject Configuration for target CRUD
     * ID for target CRUD could be found in `data-baza-crud-id` attribute of `baza-crud-layout` component.
     * You also can call `bazaCrudId()` in console to get ID of CRUD which is displayed currently on page.
     */
    register(id: string, config: BazaCrudInjectConfig): void;

    /**
     * Remove Inject Configuration of target CRUD
     * ID for target CRUD could be found in `data-baza-crud-id` attribute of `baza-crud-layout` component.
     */
    unregister(id: string): void;
}
```

-   You can inject your custom configuration with `BazaCrudInjectService.register(id: string, config: BazaCrudInjectConfig)` method
-   This service should be injected in module which is visible for all app (`app.module.ts`, as example)
-   ID for target CRUD could be found in `data-baza-crud-id` attribute of `baza-crud-layout` component.
-   You also can call `bazaCrudId()` in console to get ID of CRUD which is displayed currently on page.
-   You can call `bazaFormSlider()`, `bazaFormModal()` or `bazaCrud` to get currently opened Slider Modal, Form Modal or CRUD components / component references.

## Examples

### Adding additional link column for Account CMS

```typescript
// apps/cms/src/app/app.module.ts

@NgModule({
    /* ... */
})
export class AppModule {
    constructor(private readonly crudInject: BazaCrudInjectService) {
        this.injectAccountCms();
    }

    injectAccountCms(): void {
        this.crudInject.register<AccountCmsDto>(
            BAZA_CORE_ACCOUNT_CMS_ID /* or 'baza-core-account-cms-id', we are not updating CMS ids for no reason */,
            {
                inject: (config) => {
                    config.data.columns.push({
                        title: 'Statistics',
                        type: {
                            kind: BazaTableFieldType.Link,
                            link: (entity) => ({
                                routerLink: ['/your-cms/statistics', entity.id],
                            }),
                        },
                        width: 120,
                        textAlign: BazaTableFieldAlign.Center,
                    });
                },
            },
        );
    }
}
```
