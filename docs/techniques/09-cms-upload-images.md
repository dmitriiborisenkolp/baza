# Upload Images with CMS

Images could be uploaded with `BazaUpload` / `BazaUploadMulti`, `BazaAttachment`/`BazaAttachmentMulti` and `BazaImage`/`BazaImageMulti` form controls from `baza-form-builder` subpackage.

```typescript
// Form Builder configuration:
{
    title: this.i18n('tabs.images'),
    contents: {
        fields: [
            {
                type: BazaFormBuilderControlType.BazaImage,
                formControlName: 'cover',
                label: this.i18n('form.fields.cover'),
                props: {
                    withImageOptions: {
                        ratio: 16 / 9,
                    },
                },
            },
            {
                type: BazaFormBuilderStaticComponentType.Divider,
            },
            {
                type: BazaFormBuilderControlType.BazaImageMulti,
                formControlName: 'images',
                label: this.i18n('form.fields.images'),
                props: {
                    withImageOptions: {
                        ratio: 4 / 3,
                    },
                },
            },
        ],
    },
},
```

## Difference between upload control types

-   `BazaUpload` / `BazaUploadMulti` controls will emit `S3 Object Id` as Form Value.
-   `BazaAttachment` / `BazaAttachmentMulti` will use Baza Attachment API and will trigger `AttachmentDTO` as Form Value
-   `BazaImage` / `BazaImageMulti` works same as `BazaAttachment` / `BazaAttachmentMulti`, but also forces images validation and force `ImageSet` usage. Image sets contains same image in XS, SM, MD and XL variants.

## Ratio options

`BazaUpload` form components (which is used by `BazaImage` / `BazaImage` form components) have multiple options about cropping images.

-   `ratio` sets Ratio which should follows image
-   `ratioHandler` option defines which handler will be used to crop image

Image could be cropped by used in CMS with Cropper.JS plugin and will be automatically cropped at API side.

-   `ratioHandler: BazaFieldUploadImageRatioHandler.Backend` will use API-only option. Uploaded images will be automatically cropped on API side.
-   `ratioHandler: BazaFieldUploadImageRatioHandler.CropperJS` option will allow CMS user to adjust image before uploading it to API. API will also additionally crop image, if uploaded file does not looks like as image with target ratio.
-   `ratioHandler: BazaFieldUploadImageRatioHandler.Disabled` will completely disables Cropper.JS option at CMS side. This option could be used if you would like to not allow CMS user to crop image, with or without specified ratio.

## Multiselect option

`BazaUpload` form components (which is used by `BazaImage` / `BazaImage` form components) have option to enable or disable multi-select option in system file dialog:

-   `multiselect: true` will enable multi-select option in system file dialog. This option will be ignored, if control allows to upload only 1 file. Multiselect is enabled by default for `BazaUploadMulti`, `BazaAttachmentMulti` and `BazaImageMulti` controls.
-   `multiselect: false` will disable multi-select option in system file dialog. This option affects `BazaUploadMulti`, `BazaAttachmentMulti` and `BazaImageMulti` controls .
