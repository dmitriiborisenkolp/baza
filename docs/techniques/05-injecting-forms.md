# Injection custom configuration for Forms

Same as for [njecting & External modifying CMS CRUDs](techniques/04-injecting-crud-lists.md), you can inject your custom configuration for Forms. You can
modify everything in default form configuration including endpoints which are used to save changes.

-   You can find ID of Form in `data-baza-form-id` attribute of `div.baza-form-builder` wrapper element
-   You can also call `bazaFormId()` in console to get ID of Form which is opened currently on page
-   You can call `bazaFormSlider()`, `bazaFormModal()` or `bazaCrud` to get currently opened Slider Modal, Form Modal or CRUD components / component references.

```typescript
interface BazaFormBuilderInjectFullConfig<T, FORM_VALUE = T, SUBMIT_RESPONSE = T> {
    /**
     * Mutate `config` or `formGroup` options to inject your custom configutation
     */
    inject(
        config: BazaFormBuilder,
        formGroup: FormGroup,
        resources: {
            /**
             * VCR used for Modal window
             */
            childVcr: ViewContainerRef;

            /**
             * ngOnDestroy callback
             */
            takeUntil$: Subject<void>;

            /**
             * Reference BazaFormLayoutModal (one of `modalConfig` or `sliderConfig` will be available)
             */
            modalConfig?: BazaFormLayoutModal<T, FORM_VALUE, SUBMIT_RESPONSE>;

            /**
             * Reference BazaFormLayoutConfig (one of `modalConfig` or `sliderConfig` will be available)
             */
            sliderConfig?: BazaFormLayoutConfig<T, FORM_VALUE, SUBMIT_RESPONSE>;
        },
    ): void;
}
```

## Examples

### Adding new field to Account in Account CMS

This example adds a new field for Account and save it to `metadata` object.

```typescript
export class AppModule {
    constructor(private readonly bazaFormInjectService: BazaFormBuilderInjectService) {
        this.injectAccountForm();
    }

    injectAccountForm(): void {
        this.bazaFormInjectService.register(BAZA_CORE_ACCOUNT_CMS_USER_FORM_ID, {
            inject: (config, formGroup, resources) => {
                // Add new form control
                const colorControl = new FormControl();

                formGroup.addControl('color', colorControl);

                colorControl.valueChanges.pipe(takeUntil(resources.takeUntil$)).subscribe((color) => {
                    console.log('Selected color: ', color);
                });

                // Add form field
                const allTabs = (config.contents as BazaFormBuilderWithTabs).tabs;
                const accountTab = allTabs[0];

                accountTab.contents.fields.push({
                    formControlName: 'color',
                    type: BazaFormBuilderControlType.Select,
                    label: 'Color',
                    values: [
                        {
                            label: 'Red',
                            value: 'red',
                        },
                        {
                            label: 'Green',
                            value: 'green',
                        },
                        {
                            label: 'Blue',
                            value: 'Blue',
                        },
                    ],
                });

                // Change endpoint which should be used to save entity
                resources.sliderConfig.onSubmit = (entity, formValue) => {
                    const observable = !!entity
                        ? this.customAccountDataAccess.update({ ...formValue, ulid: entity.ulid })
                        : this.customAccountDataAccess.create(formValue);

                    return observable.pipe(tap(() => window['bazaCrud'](BAZA_CORE_ACCOUNT_CMS_ID).refresh()));
                };
            },
        });
    }
}
```
