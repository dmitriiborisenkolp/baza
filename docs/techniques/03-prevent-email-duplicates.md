# Preventing Email Duplicates

Baza Mail Service allows you to prevent mail duplicates with using unique ID's for each send mail request. It's recommended to add unique generated ID for every mail send request in your API.

## Example: Notification about adding Task

```typescript
import { MailService } from '@scaliolabs/baza-core-api';

@Injectable()
export class TaskService {
    constructor(
      private readonly mail: MailService,
    ) {}
    
    async createTask(): Promise<void> {
        const task = new TaskEntity();
        
        // some implementation...
        
        const sendMailResponse = this.mail.sendMail({
            // You should generate unique ID depends on something stable & unique. For example, you can use TypeORM's Entity ID:
            uniqueId: `my_project_task_create_${task.id}`,
            
            // Additional options like mail template, variables, etc
        });
    }
}
```
