# Export to CSV Implementation Guide

Baza provides default services to work with CSV exports. This guide describes how to implement export to CSV both API and CMS.
Export to CSV works in combination with CRUD List feature. It's recommended to implement `list` method before making Export To CSV feature.

This guide is based on example implementation of `Newsletters` Export-To-CSV feature.

## API

### 1. Create a CSV DTO

CSV DTO should contains only string/number fields. You can't use objects or arrays for CSV DTO, unless it will not be saved properly
in CSV.

```typescript
// libs/baza-content-types/src/lib/dto/newsletter-csv.dto.ts
export class NewslettersCvsDto {
    @ApiModelProperty()
    id: number;

    @ApiModelProperty({
        required: false,
    })
    name: string;

    @ApiModelProperty()
    email: string;

    @ApiModelProperty({
        required: false,
    })
    dateCreated: string;

    @ApiModelProperty({
        required: false,
    })
    dateUpdated: string;
}
```

### 2. Add an endpoint definition for CSV export

Add a method for your CMS endpoints collection. Export To CSV endpoint should accepts object extended from `CrudExportToCsvRequest` and
should returns `string` as response.

```typescript
// libs/baza-content-types/src/lib/endpoints/newsletter-cms.endpoint.ts
export interface NewsletterCmsEndpoint {
    // ...
    exportToCSV(request: NewsletterCmsExportToCsvExportRequest): Promise<string> | Observable<string>;
    // ...
}
```

```typescript
// Your request should extends from CrudExportToCsvRequest
// Use CSV DTO and CRUD List request
export class NewsletterCmsExportToCsvExportRequest extends CrudExportToCsvRequest<NewslettersCsvDto, NewsletterCmsListRequest> {}
```

### 3. Create an CSV mapper

As far as your created a CSV DTO, you will also need to create an Entity -> DTO mapper for it. This mapper will be used for service later.

```typescript
import { Injectable } from '@nestjs/common';
import { BazaContentTypesNewsletterEntity } from '../entities/baza-content-types-newsletter.entity';
import * as moment from 'moment';
import { NewslettersCsvDto } from '@scaliolabs/baza-content-types-shared';

@Injectable()
export class NewslettersCsvMapper {
    entityToDTO(entity: BazaContentTypesNewsletterEntity): NewslettersCsvDto {
        return {
            id: entity.id,
            email: entity.email,
            name: entity.name,
            dateCreated: moment(entity.dateCreated).format('MM-DD-YYYY HH:mm:ss'),
            dateUpdated: moment(entity.dateCreated).format('MM-DD-YYYY HH:mm:ss'),
        };
    }

    entitiesToDTOs(entities: Array<BazaContentTypesNewsletterEntity>): Array<NewslettersCsvDto> {
        return entities.map((e) => this.entityToDTO(e));
    }
}
```

### 4.Create an Export to CSV service method

Actual exporting is performed by `CrudCsvService` service which works very similar as `CrudService`. The only difference that this service
will remove `index` / `size` options from request.

```typescript
import { CrudCsvService } from '@scaliolabs/baza-core-api';

@Injectable()
export class NewslettersCmsService {
    constructor(private readonly csvMapper: NewslettersCsvMapper, private readonly crudCsv: CrudCsvService) {}

    // ...

    async exportToCSV(request: NewsletterCmsExportToCsvExportRequest): Promise<string> {
        return this.crudCsv.exportToCsv<BazaContentTypesNewsletterEntity, NewslettersCsvDto>({
            ...request, // Request passed from CMS
            entity: BazaContentTypesNewsletterEntity, // TypeORM entity to work with
            mapper: async (items) => this.csvMapper.entitiesToDTOs(items), // Your CSV mapper for DTOs
            findOptions: {
                // Additional FindOptions
                order: {
                    id: 'DESC',
                },
            },
        });
    }

    // ...
}
```

### 5. Add a controller method

A controller method will be very simple:

```typescript
@Controller()
export class NewslettersCmsController implements NewsletterCmsEndpoint {
    constructor(private readonly cmsService: NewslettersCmsService) {}

    // ...

    @Post(NewsletterCmsEndpointPaths.exportToCSV)
    @ApiOperation({
        summary: 'exportToCSV',
        description: 'Export list of subscriptions to CSV',
    })
    @ApiOkResponse()({
        content: {
            'text/csv': {},
        },
    })
    async exportToCSV(@Body() request: NewsletterCmsExportToCsvExportRequest): Promise<string> {
        return this.cmsService.exportToCSV(request);
    }

    // ...
}
```

## Data Access

For Node-Access (E2E, API) services you should specify `asJsonResponse: false`:

```typescript
import { BazaDataAccessNode } from '@scaliolabs/baza-core-data-access';

export class NewslettersCmsNodeAccess implements NewsletterCmsEndpoint {
    constructor(private readonly http: BazaDataAccessNode) {}

    // ...

    async exportToCSV(request: NewsletterCmsExportToCsvExportRequest): Promise<string> {
        return this.http.post(NewsletterCmsEndpointPaths.exportToCSV, request, {
            asJsonResponse: false,
        });
    }

    // ...
}
```

For Data-Access (Angular, CMS / Web) services you should specify `responseType: "text"`:

```typescript
import { BazaDataAccessService } from '@scaliolabs/baza-core-data-access';

@Injectable()
export class NewslettersCmsDataAccess implements NewsletterCmsEndpoint {
    constructor(private readonly http: BazaDataAccessService) {}

    // ...

    exportToCSV(request: NewsletterCmsExportToCsvExportRequest): Observable<string> {
        return this.http.post(NewsletterCmsEndpointPaths.exportToCSV, request, { responseType: 'text' });
    }

    // ...
}
```

## CMS

Last step is add export button to your CMS. You should use `BazaCrudItem.ExportToCsv` which is doing most of work about displaying
modal window with selecting delimiter option & downloading it to user.

```typescript
export class BazaContentTypesNewslettersCmsComponent {
    // In your component

    get crudConfig(): BazaCrudConfig<NewslettersDto> {
        return {
            // ...
            items: {
                topRight: [
                    {
                        type: BazaCrudItem.ExportToCsv,
                        options: {
                            title: this.i18n('actions.exportToCSV'),
                            icon: 'download',
                            type: BazaNzButtonStyle.Link,
                            endpoint: (request) =>
                                this.dataAccess.exportToCSV({
                                    // Base request including LAST USED REQUEST for CRUD List
                                    ...request,
                                    // Your fields - i18n for fields is doing on CMS side
                                    fields: ['email', 'dateCreated'].map((field) => ({
                                        field: field as any,
                                        title: this.translate.instant(this.i18n(`fields.${field}`)),
                                    })),
                                }),
                            // Filename factory - how it will be downloaded by user
                            fileNameGenerator: () => 'sandbox-subscriptions-' + moment(new Date()).format('MM-DD-YYYY'),
                        },
                    },
                ],
            },
            // ...
        };
    }
}
```
