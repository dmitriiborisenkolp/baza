# Applications

Baza knows about 5 different applications: API, CMS, Web, IOS and Android applications. API, CMS and Web applications can be built on Baza. You should already have configured API/CMW/Web applications If you're started project with Baza Skeleton fork.

All applications located under `apps/` directory. You must not put any services, repositories, components or anything else inside `apps/` directory and it's subdirectories.

> Apps and Libs
>
> Apps configure dependency injection and wire up libraries. They should not contain any components, services, or business logic.
> Libs contain services, components, utilities, etc. They have well-defined public API.
>
> *[Using Nx at Enterprises](https://nx.dev/latest/angular/guides/monorepo-nx-enterprise)*

Applications should only import or lazy-import modules from `libs/` directory.

## Supported applications

Baza identifies client applications with `X-Baza-Application` header. If header is not sent with request, Baza considers request as sentz `Web` appplication.

| Application | X-Baza-Application            | Application Enum      | Is Default? |
|-------------|-------------------------------|-----------------------|-------------|
| API         | `X-Baza-Application: api`     | `Application.API`     | No          |
| CMS         | `X-Baza-Application: cms`     | `Application.CMS`     | No          |
| Web         | `X-Baza-Application: web`     | `Application.WEB`     | Yes         |
| IOS         | `X-Baza-Application: ios`     | `Application.IOS`     | No          |
| Android     | `X-Baza-Application: android` | `Application.ANDROID` | No          |

## Adding new Web applications

You may need to create additional web application like marketing site or landing. This applications will be considered by Baza as Web applications. You will need to generate application structure using `nx g @nrwl/angular:app` command and import `BazaCoreWebBundle` with proper configuration. Read [Bundles](/overview/35-bundles) documentation section for more details.

## Identifying application

Every client application (CMS, Web, Android or Web) must send specific headers which allows API to detect current application. If headers are not available, API by default considers all requests as requests for Web applications.

Additional information about headers can be found in [Headers](/overview/04-headers) documentation section.
