# AWS

Baza out-of-box has integration with AWS S3 cloud. All uploads will be stored at S3 bucket and API must returns
presigned url's for uploaded files. The `baza-aws` package from `baza-core` is responsible for all uploads.

You may need additional configuration before making uploads.

Baza AWS only uploads files as is. [Baza Attachment](/overview/08-attachments) additionally make pre- and post-processing with uploaded files,
which allows to generate thumbnails, webp variants, image auto-resize any more.

## AWS strategies

Baza can upload files directly to S3 or using deployed instance of Baza application as proxy. Direct uploads
requires AWS credentials (S3 Url, Access Key/Secret keys and S3 Bucket) and usually Direct strategy should be used for Test/Stage/Production
targets. Proxy strategy is used for development environment and requires deployed instance of application and any admin account credentials.

## Using Direct AWS strategy

> ❗️ Avoid using Direct strategy for Development. Developers may accidently push secret keys
> to repository. Consider to use Proxy strategy for development environments.

For non-development targets you need to set up `AWS_S3_BUCKET`, `AWS_S3_URL`, `AWS_ACCESS_KEY_ID` and `AWS_SECRET_KEY`
environment variables. You should store `AWS_SECRET_KEY` in Vault or any similarly secured storage; Anything else can be
stored in repository.

| Variable              | Required? | Type   | Description                                               | Example                                |
| --------------------- | --------- | ------ | --------------------------------------------------------- | -------------------------------------- |
| **AWS_S3_BUCKET**     | Yes       | STRING | S3 Bucket                                                 | cdn-baza-test                          |
| **AWS_S3_URL**        | Yes       | STRING | S3 Endpoint Url                                           | https://cdn-baza-test.s3.amazonaws.com |
| **AWS_ACCESS_KEY_ID** | Yes       | STRING | S3 Access Key                                             | (Access Key)                           |
| **AWS_SECRET_KEY**    | Yes       | STRING | S3 Secret Key                                             | (Secret Key)                           |
| **AWS_S3_PATH**       | No        | STRING | Additional folder where all uploaded files will be placed | cms/                                   |

## Using Proxy strategy

Proxy strategy requires deployed Baza application with at least one existing admin account. You can use any existing admin account, but
it's better to create a limited admin account with no access to anything else. This account should be used specifically only
for AWS uploads. You need only one AWS Proxy account per project; Consider to use Test or Stage instance as proxy. Production
instance must not have proxy accounts for AWS uploads.

| Variable                    | Required? | Type   | Description                                            | Example                                    |
| --------------------------- | --------- | ------ | ------------------------------------------------------ | ------------------------------------------ |
| **AWS_PROXY_BASE_URL**      | Yes       | STRING | URL of deployed Baza application                       | https://my-project-api.test.scaliolabs.com |
| **AWS_PROXY_AUTH_EMAIL**    | Yes       | STRING | Email of admin account on deployed Baza Application    | aws-proxy@example.io                       |
| **AWS_PROXY_AUTH_PASSWORD** | Yes       | STRING | Password of admin account on deployed Baza Application | (password)                                 |

> ✳️ You may set up Proxy AWS account, environment variables, add them into `envs/development.env` and push it to repository.
> It's recommended to do it with initial commits of your project.

## Upload files with API

```typescript
import { AwsService } from 'baza-core';
```

You should use `AwsService` which is responsible for:

-   Uploading files
-   Deleting existing files by S3 AWS Key
-   Generating presigned URL's for uploaded files

When you're uploading files to S3 buckets, the service will returns S3 AWS Key. You should store it in your entity
and later use it to generate presigned URL's.

| Method                  | Description                                                                                                                                                                         |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **uploadFromBuffer**    | Upload file to S3 using Node buffer. It's used for Controller (Form-Data) uploads and E2E testing                                                                                   |
| **uploadFromLocalFile** | Upload existing local file to S3                                                                                                                                                    |
| **deleteWithAwsKey**    | Delete existing file from S3 by Aws Key                                                                                                                                             |
| **presignedUrl**        | Generates & returns presigned URL of S3 uploaded file                                                                                                                               |
| **resource**            | Accepts same argument as `presignedUrl` + accepts additional `application` argument. Method will returns presigned URL for non-CMS application and will returns S3 Aws Key for CMS. |

> ❗️ **You should never store URL's / presigned URL's for uploaded files.** S3 bucket may be changed later at any moment
> and presigned URL's has expiration time.

> ❗️ **Don't try to "generate" URL's with S3 Url + S3 Aws Key combination**. API should always generate & returns presigned URL's with S3 API.

### Example: Cover image for Task entity

_(We have Task entity and we would like to allow upload Cover image for Tasks with CMS. Public API should returns
URL to display uploaded image to user)_

First, you will need to add a new column to store S3 Key of uploaded file.

```typescript
// libs/your-project-api/task/entities/task.entity.ts
@Entity()
export class TaskEntity {
    @PrimaryGeneratedColumn()
    readonly id: number;

    /* +++ */ @Column()
    /* +++ */
    coverS3AwsKey: string;

    @Column()
    title: string;

    @Column()
    description: string;
}
```

> ❗️ **API should returns S3 Keys of uploaded files to CMS and API should returns
> presigned URL's for Public API.** You must never returns S3 Keys for Public API's. From development perspective
> it means that you will need to implement 2 versions of entity DTOs: DTO for Public API and DTO for CMS API which usually extends
> from Public version of entity DTO.

Next you will need to add cover to DTOs.

-   Web / IOS / Android clients should receives presigned URL's of uploaded files.
-   CMS requires S3 Keys of uploaded files. You will need to create different DTOs and Mappers for CMS and for Public API.

If you're looking for optimization, CMS API shouldn't returns presigned URL's for fields which are used only in Form Builder. Presigned URL's may be used for columns in List table,
but in most of cases CMS don't need presigned URL's of uploaded resources.

```typescript
// libs/your-project-shared/task/dto/task.dto.ts
export class TaskDTO {
    @ApiModelPropery()
    readonly id: number;

    /* +++ */ @ApiModelPropery()
    /* +++ */
    readonly coverUrl: string;

    @ApiModelPropery()
    readonly title: string;

    @ApiModelPropery()
    readonly description: string;
}

// libs/your-project-shared/task/dto/task-cms.dto.ts
export class TaskCmsDTO extends TaskDTO {
    readonly coverS3AwsKey: string;
}
```

You need to add `coverS3AwsKey` fields to create/update requests. Mostly Create and Update requests contains same fields,
so we move common fields into separated EntityBody class and extends create/update request DTOs from EntityBody class.

```typescript
// libs/your-project-shared/task/endpoints/task-cms.endpoint.ts
import { BazaSeoDto, CrudListRequestDto } from '@scaliolabs/baza-core-shared';

export enum TaskCmsEndpointPaths {
    create = '/cms/task/create',
    update = '/cms/task/update',
    list = '/cms/task/list',
}

export inteface TaskCmsEndpoint {
    create(request: TaskCmsCreateRequest): Promise<TaskCmsDTO> | Observable<TaskCmsDTO>;
    update(request: TaskCmsUpdateRequest): Promise<TaskCmsDTO> | Observable<TaskCmsDTO>;
    list(request: TaskCmsListRequest): Promise<TaskCmsListResponse> | Observable<TaskCmsListResponse>;
}

export class TaskCmsEntityBody {
    @ApiModelPropery()
    @IsString()
    @IsNotEmpty()
    title: string;

    @ApiModelPropery()
    @IsString()
    @IsNotEmpty()
    description: string;

    /* +++ */ @ApiModelPropery()
    /* +++ */ @IsString()
    /* +++ */ @IsNotEmpty()
    /* +++ */ coverS3AwsKey: string;
}

export class TaskCmsCreateRequest extends TaskEntityBody {}

export class TaskCmsUpdateRequest extends TaskEntityBody {
    @ApiModelProperty()
    @IsPositive()
    @IsInt()
    @IsNotEmpty()
    id: number;
}

export class TaskCmsListRequest extends CrudListRequestDto<TaskCmsDTO> {}
export class TaskCmsListResponse extends CrudListResponseDto<TaskCmsDTO> {}
```

```typescript
// libs/your-project-shared/task/endpoints/task.endpoint.ts

export enum TaskEndpointPaths {
    list = '/task/list',
}

export interface TaskEndpoint {
    list(request: TaskListRequest): Promise<TaskListResponse> | Observable<TaskListResponse>;
}

export class TaskListRequest extends CrudListRequestQueryDto {}
export class TaskListResponse extends CrudListResponseDto<TaskDTO> {}
```

Now we should do changes in Service:

```typescript
// libs/your-project-api/task/services/task-cms.service.ts
export class TaskCmsService {
    constructor(private readonly repository: TaskRepository) {}

    async create(request: TaskCreateRequest): Promise<TaskEntity> {
        const entity = new TaskEntity();

        // It's better to move populating data to additional private method; It's commonly used
        // because most of CMS API's contains create / update methods
        await this.populate(entity, request);

        // Store entity in DB
        await this.repository.save(entity);

        // Returns entity; It will be used later to factory DTO
        return entity;
    }

    private async populate(target: TaskEntity, entityBody: TaskEntityBody): Promise<void> {
        target.title = entityBody.title;
        /* +++ */ target.coverS3AwsKey = entityBody.coverS3AwsKey;
    }
}
```

Last step will be adding / modify mappers. As far as we have now different DTOs for Public API and CMS, we should
create different versions of mappers. Usually you can create mapper for Public API and use it as base mapper for CMS mapper.

Public API must returns URL's instead of S3 Aws Keys. You should use `AwsService.presignedUrl` method which is responsible
for generating, caching & updating presigned URL's. Presigned URL's are being cached in Redis and Redis will automatically
clean up outdated URL's. Additionally, if presigned URL is close to expire, `AwsService` will automatically generate new
presigned URL. By default presigned URL will be valid for next 24 hours. You can use default expiration time for
presigned URL's or you can specify expiration time with `ttlSeconds` option.

```typescript
// libs/your-project-api/task/mappers/task.mapper.ts
import { AwsService } from 'baza-core';

export class TaskMapper {
    constructor(private readonly aws: AwsService) {}

    async entityToDTO(input: TaskEntity): Promise<TaskDTO> {
        return {
            id: input.id,
            title: input.title,
            description: input.description,
            /* +++ */ coverUrl: await this.aws.presignedUrl({
                /* +++ */ s3ObjectId: input.coverS3AwsKey,
                /* +++ */ // (OPTIONAL) ttlSeconds: 2 /* hours */ * 60 /* min */ * 60, // You may specify expiration time
                /* +++ */
            }),
        };
    }

    async entitiesToDTOs(input: Array<TaskEntity>): Promise<Array<TaskDTO>> {
        const result: Array<TaskDTO> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(input));
        }

        return result;
    }
}
```

```typescript
// libs/your-project-api/task/mappers/task-cms.mapper.ts
import { AwsService } from 'baza-core';
import { TaskMapper } from './task.mapper.ts';

export class TaskCmsMapper {
    constructor(private readonly baseMapper: TaskMapper) {}

    async entityToDTO(input: TaskEntity): Promise<TaskCmsDTO> {
        return {
            ...(await this.baseMapper.entityToDTO(input)),
            coverS3AwsKey: input.coverS3AwsKey,
        };
    }

    async entitiesToDTOs(input: Array<TaskEntity>): Promise<Array<TaskCmsDTO>> {
        const result: Array<TaskCmsDTO> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(input));
        }

        return result;
    }
}
```

That's all. Your uploaded file will be correctly handled both for CMS and Web/IOS/Android applications.

## Uploaded files & Client Applications (Web, IOS, Android)

API will returns uploaded files with presigned URL's. Presigned URL's updates (usually) every 24 hours with some
threshold.

Make sure that any kind of cache (Nginx, SSR or anything else) will properly deal with presigned URL expiration. AWS by
default returns additional HTTP headers about it, but your application should also properly handle it.

## Upload files with CMS

Baza has 2 controls to work with AWS uploads: `BazaUpload` and `BazaUploadMulti`. You can read more about form controls
in [Form Builder](/overview/10-form-builder) documentation section.

Additionally Baza provides `BazaAttachment` and `BazaAttachmentMulti` form controls. These controls uploads files to AWS
with pre- and post-processing options which allows to automatically resize images, generate webp variants and many more.
Read more at [Attachments](/overview/08-attachments) documentation section.

CMS accepts S3 Aws Keys for Form Controls. You don't need presigned URL's of S3 resources in general; Form Controls
will automatically receives presigned URL's using Baza AWS API.

```typescript
// libs/your-project-cms/feature-task/components/task-cms/task-cms.component.ts

@Component({
    templateUrl: './task-cms.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskCmsComponent {
    // ...
    private formGroup(): FormGroup {
        return this.fb.group({
            title: [undefined, [Validators.required]],
            description: [undefined, [Validators.required]],
            /* +++ */ coverS3AwsKey: [undefined, [Validators.required]],
        });
    }

    private formBuilder(form: FormGroup): BazaFormBuilder {
        return {
            layout: BazaFormBuilderLayout.WithTabs,
            contents: {
                tabs: [
                    {
                        title: this.i18n('tabs.common'),
                        contents: {
                            fields: [
                                {
                                    formControlName: 'title',
                                    type: BazaFormBuilderControlType.Text,
                                    label: this.i18n('fields.title'),
                                },
                                {
                                    formControlName: 'description',
                                    type: BazaFormBuilderControlType.TextArea,
                                    label: this.i18n('fields.description'),
                                },
                                /* +++ */ {
                                    /* +++ */ formControlName: 'coverS3AwsKey',
                                    /* +++ */ type: BazaFormBuilderControlType.BazaUpload,
                                    /* +++ */ label: this.i18n('fields.cover'),
                                    /* +++ */
                                },
                            ],
                        },
                    },
                ],
            },
        };
    }
    // ...
}
```

## Custom uploads

```typescript
// 1) Import module:
import { BazaAwsDataAccessModule } from '@scaliolabs/baza-core-data-access';

@NgModule({
    imports: [BazaAwsDataAccessModule],
})
export class MyModule {}

// 2) Inject & Use Data-Access service:
import { BazaAwsDataAccess } from '@scaliolabs/baza-core-data-access';

@Component()
export class MyComponent {
    constructor(private readonly awsDataAccess: BazaAwsDataAccess) {}
}
```

Baza API provides CMS-only endpoints which allows to upload, delete and fetch presigned URL's of uploaded files.

| Endpoint                        | Description                                                                        | Documentation                                                                      |
| ------------------------------- | ---------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| **POST /baza-aws/upload**       | Uploads file to S3 and returns `BazaAwsFileDto`. Accepts Form-Data with `file` key | [Link](https://baza-api.test.scaliolabs.com/redoc/#operation/AwsController_upload) |
| **POST /baza-aws/delete**       | Delete uploaded S3 resource by S3 Aws Key                                          | [Link](https://baza-api.test.scaliolabs.com/redoc/#operation/AwsController_upload) |
| **POST /baza-aws/presignedUrl** | Returns presigned URL of uploaded S3 resource                                      | [Link](https://baza-api.test.scaliolabs.com/redoc/#operation/AwsController_upload) |

Read more at [AWS API Documentation](https://baza-api.test.scaliolabs.com/redoc/#tag/Baza-AWS) section

### Example: Upload File

```html
<!-- my.component.html -->
<input
    #fileInput
    type="file"
    style="display: none"
    (change)="onFileInputChange($event)" />

<button
    type="button"
    (click)="fileInput.click()">
    Click to upload
</button>
```

```typescript
// my.component.ts

@Component()
export class MyComponent {
    constructor(private readonly awsDataAccess: BazaAwsDataAccess) {}

    onFileInputChange($event: Event): void {
        const target = $event.target as HTMLInputElement;
        const file = target.files[0];

        if (!file) {
            return;
        }

        this.awsNgEndpoint.upload(file).subscribe((response /* BazaAwsFileDto */) => {
            console.log('Uploaded', response.s3ObjectId, response.presignedTimeLimitedUrl);
        });
    }
}
```

## Uploaded files with HTML / CkEditor

> ❗️ HTML fields should be additionally processed with `AwsHtmlHelper.process` method

HTML resources which can be edited with CMS also can contains uploaded files. CMS uploads files with same Baza AWS endpoints & services; It means that
we don't have public URL's or uploaded resources.

CMS will send HTML content with presigned URL + `#` + S3 AWS Keys in `src`-like HTML attributes. API should replace these
src attributes with actual presigned URL's on Mapper level each time when DTO is requested. Additionally, Web/IOS/Android applications should not
receives AWS S3 Keys. The `AwsHtmlHelper.process` method is here to automatize it and help to deal with HTML + presigned URL's
issue.

## Example: Making `description` field as HTML

You don't need to do any additional changes anywhere excepts Public API / CMS mappers:

```typescript
// libs/your-project-api/task/mappers/task.mapper.ts
import { AwsService } from 'baza-core';

export class TaskMapper {
    constructor(private readonly aws: AwsService, private readonly awsHtml: AwsHtmlHelper) {}

    // By default awsHtml.process method will delete S3 Aws Keys
    // We added `application` argument here because we need S3 Aws Keys for CMS
    async entityToDTO(input: TaskEntity, /* +++ **/ application: Application = Application.WEB): Promise<TaskDTO> {
        return {
            id: input.id,
            title: input.title,
            /* +++ */ description: await this.awsHtml.process(input.description, application),
            coverUrl: await this.aws.presignedUrl({
                s3ObjectId: input.coverS3AwsKey,
            }),
        };
    }

    async entitiesToDTOs(input: Array<TaskEntity>): Promise<Array<TaskDTO>> {
        const result: Array<TaskDTO> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(input));
        }

        return result;
    }
}
```

```typescript
// libs/your-project-api/task/mappers/task-cms.mapper.ts

import { AwsService } from 'baza-core';
import { TaskMapper } from './task.mapper.ts';

export class TaskCmsMapper {
    constructor(private readonly baseMapper: TaskMapper) {}

    async entityToDTO(input: TaskEntity): Promise<TaskCmsDTO> {
        return {
            ...(await this.baseMapper.entityToDTO(input, /* +++ */ Application.CMS)),
            coverS3AwsKey: input.coverS3AwsKey,
        };
    }

    async entitiesToDTOs(input: Array<TaskEntity>): Promise<Array<TaskCmsDTO>> {
        const result: Array<TaskCmsDTO> = [];

        for (const entity of input) {
            result.push(await this.entityToDTO(input));
        }

        return result;
    }
}
```
