# Registry Config:

# API:

-   Entity: Table(registry_record_entity);
    Fiels: { label?: string, value: T, path, string }

`baza-core-shared/src/libs/baza-registry/src/lib/models/registry.model.ts`

-   Model: Classes, Interfacs, Types, Enum to API Request/Response
    `baza-core-shared/src/libs/baza-registry/src/lib/models/registry.model.ts`

-   Events:
    `baza-core-shared/src/libs/baza-registry/src/lib/events/registry-events.ts`

-   Repository: CRUD
    `baza-core-api/src/libs/baza-registry/src/lib/repositories/registry-record.repository.ts`

-   Service:
    `baza-core-api/src/libs/baza-registry/src/lib/services/baza-registry.service.ts`

-   Controller:
    `baza-core-api/src/libs/baza-registry-controllers/src/lib/controllers/baza-registry.controller.ts`

-   Endpoint:
    `baza-core-shared/src/libs/baza-registry/src/lib/endpoints/baza-registry.endpoint.ts`

Create new record:
To create new registry record you just need send by POST method { value: 'somevalue', path: 'somepath } ', label is optional;

Update registry record:
To update a registry you need send POST method, { path } is mandatory, if it is null the update doesn't will happen.

# CMS:

-   Component:
    `baza-core-cms/src/libs/baza-registry/src/lib/components/baza-registry-cms/baza-registry-cms.component.ts`;
    At the component, there are exhibition of the label and value, to both option we have a default value who can be rechieve
    at any time by clicking the se default button;

We can find all default values to registy here:
`bazaNcApiBundleRegistry`
`bazaAccountRegistry`
`bazaVersionRegistry`
`defaultsRegistrySchema`
`bazaNcIntegrationApiRegistry`

The field hint will always show the default value;
The both values can be changed, the value column must to respect the type of obj.
