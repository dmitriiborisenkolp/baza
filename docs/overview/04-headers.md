# Headers

Baza requires additional headers from client applications. These headers allows API to identifty client application, response with requested language and perform version check. Additionally Baza API returns additional headers which can be used to identify current and supported API versions.

Web applications automatically sends all necessary headers if you're using Baza Skeleton and `BazaCoreWebBundle` module. IOS and Android applications should manually send required headers.

| Header                            	| Description                                                                                                                                     	| Possible values / examples                                                                                                  	| In Request 	| In Response 	|
|-----------------------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------	|-----------------------------------------------------------------------------------------------------------------------------	|------------	|-------------	|
| **X-Baza-Application**            	| Identify application which makes requests to API                                                                                                	| `api`, `cms`, `web`, `ios`, `android`                                                                                       	| Yes        	| No          	|
| **X-Baza-Application-Id**         	| Unique Application Id. Can be any string                                                                                                        	| `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36` 	| Yes        	| No          	|
| **X-Baza-Api-Version**            	| Requested API version. API will returns `406` HTTP Status code if current deployed API is not able to handle client with requested API version. 	| `1.0.0`                                                                                                                     	| Yes        	| No          	|
| **X-Baza-Api-Supported-Versions** 	| Returns list of supported API versions                                                                                                          	| `1.0.0,1.0.1,1.0.2,1.0.3`                                                                                                   	| No         	| Yes         	|
| **X-Baza-Skip-Maintenance**       	| Special header to skip Maintenance mode. Works only with CMS                                                                                  	| `0` or `1`                                                                                                                  	| Yes        	| No          	|
| **X-Baza-I18n-Language**          	| Requested language. API can response with well-translated error messages or anything else if the header is provided in request                  	| `en`                                                                                                                        	| Yes        	| No          	|

## Authorization header

Baza uses JWT and Bearer token for authorization. You can read more about JWT here: [https://jwt.io/introduction]().

You need to pass `Authorization: Bearer your-acccess-token` for every request. Read more about authorization in [Authorization](/overview/20-auth-and-accounts) documentation section.

Web and CMS applications automatically put Authorization header with `JwtHttpInterceptor` HTTP interceptor.

## Application Headers

Client applications should send `X-Baza-Application` header which allows API to identify current application. This header
usually may slightly affects API responses, but if mobile / web applications expects different response body, API must provide
different endpoints specifically for both devices.

Optionally you can provide `X-Baza-Application-Id` header which specifically identify current device with unique ID. You can use any
string as Application Id.

## Version Request Header

Applications should send API version with `X-Baza-Api-Version` for every request. You need to hardcode API version value
in your web / ios / android application and update it manually when API version bumps. 

When API encounter request with unsupported API version, API will response with `406` HTTP status code and
with `VersionIsOutdated` error code in JSON response. 

```json
{
    "statusCode": 406,
    "code": "VersionIsOutdated",
    "message": "This version of YourProject is no longer supported. Please update to the latest version of the application to continue using it."
}
```

IOS / Android applications should ask users to update application; Web application should perform force reload on client side
or ask user to force reload the page. It's especially important for Progressive Web Applications (PWA); Browsers by
default caches angular application and it can be tricky to reload application completely.

Some endpoints will not requires version header. By default Status endpoint (`GET /`) will not ask for version header.

## Supported Versions Response Header

API will returns list of supported versions with `X-Baza-Api-Supported-Versions` for every API response. Header value contains
list of supported semver's joined with comma.

```
X-Baza-Api-Supported-Versions: 1.0.1,1.1.0,1.1.1
```

## I18n Header

API may response with localized messages and errors. API will identify requested language with `X-Baza-I18n-Language` header.

```
X-Baza-I18n-Language: ru
```

```
{
    "statusCode": 406,
    "code": "VersionIsOutdated",
    "message": "Ваше приложение устарело. Пожалуйста, обновите приложение до последней версии."
}
```

Read more about I18n feature at [I18n documentation section](/overview/05-i18n).

## Adding Additional Headers

If you need to add more headers, please ask DevOps for details. 
