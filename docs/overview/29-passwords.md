# Passwords

Baza provides shared TypeScript resources to control password validation and Baza Password API to validate password on non-web platforms.

## Password API

-   `GET /baza/password/resources` - returns min / max length for passwords and list of enabled validators (`BazaPasswordValidator.BazaMinCharacters`, `BazaPasswordValidator.BazaMaxCharacters`, etc)
-   `POST /baza/password/validate`

Baza provides endpoints for password validation on non-js platforms.

## Shared resources

Shared resources from `@scaliolabs/baza-core-shared` affects all possible applications (API, CMS and Web). Baza Password API also will be reflected for any
changes you do with exported password resources.

All passwords also are being validated by `owasp-password-strength-test` package: [https://www.npmjs.com/package/owasp-password-strength-test]()

### Constants

-   `BAZA_PASSWORD_MIN_LENGTH` (8) - default minimal length for passwords
-   `BAZA_PASSWORD_MAX_LENGTH` (32) - default maximum length for passwords
-   `BAZA_PASSWORD_MIN_PHRASE_LENGTH` (8) - default `minPhraseLength` option for `owasp.config` call
-   `BAZA_PASSWORD_MIN_OPTIONAL_TESTS_TO_PASS` (4) - default `minOptionalTestsToPass` option for `owasp.config` call
-   `BAZA_PASSWORD_RESOURCES_UPPERCASE` (`ABCDEFGHIJKLMNOPQRSTUVWXYZ`) - list of uppercase characters
-   `BAZA_PASSWORD_RESOURCES_UPPERCASE` (`abcdefghijklmnopqrstuvwxyz`) - list of lowercase characters
-   `BAZA_PASSWORD_RESOURCES_UPPERCASE` (`0123456789`) - list of digits characters
-   `BAZA_PASSWORD_RESOURCES_UPPERCASE` (` !?@#$%^&*()_+=-[]{}\|/.<>,~``'" `) - list of special characters

These constants cannot be changed, but you can replace default configuration use `bazaPasswordConfigTool` tool:

```typescript
// Add it for each application which should have updated configuration
import { bazaPasswordConfigTool } from '@scaliolabs/baza-core-shared';

bazaPasswordConfigTool().setOptions({
    minPasswordLength: 6,
    maxPasswordLength: 18,
    minPhraseLength: 6,
    minOptionalTestsToPass: 4,
});
```

### Utils

-   `bazaPasswordConfigTool()` will returns password validation tool
-   `bazaValidatePassword(password: string)` will validate password. The util returns `BazaPasswordValidateDto` with full information about password validation.
-   `bazaValidatePasswordResources()` returns information about validators and used constants.

### Validators

```typescript
interface BazaPasswordConfigTool {
    getOptions(): BazaPasswordValidatorInjects;
    setOptions(partial: Partial<BazaPasswordValidatorInjects>): this;
    setMandatoryLevel(type: BazaPasswordValidator | string, mandatory: BazaPasswordMandatory): this;
    addDefaultValidator(type: BazaPasswordValidator): this;
    setValidator(validator: BazaPasswordValidatorFunc): this;
    removeValidator(type: BazaPasswordValidator | string): this;
}
```

Validators configuration could be updated with `bazaPasswordConfigTool` which returns instance of `BazaPasswordConfigTool`. This utility helper allows you to update configuration
or change validators.

> ⚠️ **Make sure that your configuration will affects all applications - you should call `bazaPasswordConfigTool` and make changes from some of shared libraries of your project.**

### Mandatory level

Mandatory level affects password validation result. You can also use it for UI/UX reasons.

```typescript
// Mandatory level
export enum BazaPasswordMandatory {
    /**
     * Validator is required and password validation WILL fails if input does not match requirements
     */
    Mandatory = 'Mandatory',

    /**
     * Validator is recommended and password validation WILL NOT fails if input does not match requirements
     */
    Recommended = 'Recommended',

    /**
     * Validator is optional and password validation WILL NOT fails if input does not match requirements
     * It works same as Recommended, but differences between Optional and Recommended can be used for UI reasons
     */
    Optional = 'Optional',
}

/**
 * Mandatory/Recommended/Optional configuration for different password validators
 */
export const bazaPasswordValidatorsMandatory: Array<{
    type: BazaPasswordValidator;
    mandatory: BazaPasswordMandatory | string;
}> = [
    /* ... */
];

/**
 * Password validators collection
 * You can push additional validators here or change configuration of default validators
 */
export const bazaPasswordValidators: Array<{
    type: BazaPasswordValidator;
    validator: (input: string) => boolean;
}> = [
    /* ... */
];
```

### Default validators

Baza is configured by default to use 6 mandatory validators:

-   `BazaPasswordValidator.BazaMinCharacters` - validates for minimal amount of characters in password (default: 8)
-   `BazaPasswordValidator.BazaMaxCharacters` - validates for maximum amount of characters in password (default: 32)
-   `BazaPasswordValidator.Baza1UppercaseCharacter` - validates for includes at least 1 uppercase character
-   `BazaPasswordValidator.Baza1LowercaseCharacter` - validates for includes at least 1 lowercase character
-   `BazaPasswordValidator.Baza1Number` - validates for includes at least 1 digit character
-   `BazaPasswordValidator.Baza1SpecialCharacter` - validates for includes at least 1 special character (one of ` !?@#$%^&*()_+=-[]{}\|/.<>,~``'" `)

You can add new custom validators or change mandatory level with `bazaPasswordConfigTool` helper.
