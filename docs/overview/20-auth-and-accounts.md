# Auth & Accounts

## Implementing Authentification feature for Web applications

Baza provides necessary services & guards with `baza-core-ng` package.

Read more here:

- [JWT Services](/libraries/baza-core/baza-auth/02-ng-jwt-services)
- [JWT Guards](/libraries/baza-core/baza-auth/03-ng-jwt-guards)
- [JWT Http Interceptors](/libraries/baza-core/baza-auth/04-ng-jwt-http-interceptors)

## Adding custom fields for Accounts

- You can add additional fields to Account CMS with Account Metadata Fields feature. 
- You also can define different metadata field sets depends on Account role (User, Admin).
- To add new metadata fields, use `bazaCmsBundleConfigBuilder` or `bazaAccountCmsDefineMetadata` helpers.
- Use `AccountEntity.metadata` object to access additional fields on API side.
- The `AccountEntity.metadata` object is protected and is not automatically published to users. If you need to publish something from `metadata` to users, you should add it your API implementation.

### Example: Using `bazaCmsBundleConfigBuilder` in `app.module.ts`:

```typescript
bazaCmsBundleConfigBuilder()
            .withAccountCmsMetadata((payload /* You can find Account, ActivatedRoute, requested Account Role here */) => ({
                formGroup: {
                    note: [undefined, [Validators.required]],
                },
                formBuilder: [
                    // Divider
                    {
                        type: BazaFormBuilderStaticComponentType.Divider,
                    },

                    // Text Plain Field - Required
                    {
                        formControlName: 'note',
                        type: BazaFormBuilderControlType.Text,
                        label: 'Note',
                        required: true,
                    },
                ],
            }));
```

### Example: Using `bazaAccountCmsDefineMetadata` helper somewhere in CMS:

```typescript
bazaAccountCmsDefineMetadata({
    formGroup: {
        note: [undefined, [Validators.required]],
    },
    formBuilder: [
        // Divider
        {
            type: BazaFormBuilderStaticComponentType.Divider,
        },

        // Text Plain Field - Required
        {
            formControlName: 'note',
            type: BazaFormBuilderControlType.Text,
            label: 'Note',
            required: true,
        },
    ],
});
```
