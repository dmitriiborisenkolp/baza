# Maintenance

Baza allows put applications to Maintenance mode. If Application is under Maintenance, API will respond with `BazaMaintenanceServiceUnavailable` code and `503` HTTP Status.

## How Maintenance works?

API is checking `X-Baza-Application` HTTP Header in order to detect Application. If header is not set, Baza uses `Web` as default application. For every incoming request Baza checks is Application under Maintenance Mode and if it's true, Baza will throw an exception with `BazaMaintenanceServiceUnavailable` HTTP Code and with message set in CMS & stored in Registry.

## Set Applications list for Maintenance CMS

You can set Applications list with `bazaApiBundleConfigBuilder.withApplications` helper. The list is affecting Maintenance CMS:

```typescript
bazaApiBundleConfigBuilder.withApplications([Application.WEB, Application.IOS]);
```

## Ignore Maintenance Mode for specific request

API will ignore enabled Maintenance Mode if `X-Baza-Skip-Maintenance` HTTP Header is set (with any possible value).

## Adding endpoints which should ignore Maintenance

Baza specify some endpoints (such as `/version`) which should ignore Maintenance Mode. You can specify additional endpoints which should ignore Maintenance Mode:

```typescript
bazaApiBundleConfigBuilder().withIgnoreMaintenanceModeForEndpoints([MyApiEndpoints.someEndpoint]);
```

Also, it could be achieved with `withIgnoreHeadersForEndpoints` tool. The tool is also ignores Version checks:

```typescript
bazaApiBundleConfigBuilder().withIgnoreHeadersForEndpoints([MyApiEndpoints.someEndpoint]);
```

## Set Maintenance Page url for Web applications

Baza is bundled with default HTTP interceptor which is redirecting user to `/service-pages/maintenance` page when Maintenance Mode is enabled for current application. You can change default URL with `bazaWebBundleConfigBuilder.withMaintenancePageUrl` helper:

```typescript
bazaWebBundleConfigBuilder().withMaintenancePageUrl('/maintenance'); // All users will be redirected to /maintenance page
```

## Resources

### `@scaliolabs/baza-core-api`

-   `BazaMaintenanceService` - CMS Service which returns Maintenance Status and allows to enable / disable Maintenance Mode for specific applications
-   `BazaMaintenanceMessageService` - Set or Get Message which should be displayed for Application when it's under Maitenance
-   `XBazaMaintenanceInterceptor` - Global Interceptor applied to all requests. The interceptor is responsible for throwing an exception when Application is under Maintenance Mode

### `@scaliolabs/baza-core-ng`

-   `MaintenanceGuard` - the guard disallow access to page in case when Web Application is under Maintenance Mode
-   `XBazaSkipMaintenanceHttpInterceptor` - Adds a `X-Baza-Skip-Maintenance` HTTP header for requests in case if Module Configuration has `skip: true` configuration. Also controls Redirect URL for all failed requests when Maintenance Mode is enabled for current Application.

### `@scaliolabs/baza-core-data-access`

-   `BazaMaintenanceCmsDataAccess` – Data Access service with CMS tools to work with Maintenance Mode
