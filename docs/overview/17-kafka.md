# Kafka

Baza has Kafka integration out-of-box. Kafka integration is mostly used to work with [Event Bus](/overview/16-event-bus) library
which allow to publish events within current node and within multiple / every node in cluster in same way.

Read more about Kafka at [official documentation](https://kafka.apache.org/documentation/) resource.

## Concepts

-   You should set up environment variables to enable Kafka integration. Additionally, Kafka integration may be enabled / disabled with `bazaApiBundleConfigBuilder` helper.
-   Baza allows processing events with `EveryNode` pipeline (every node will be handed by every node in cluster).
-   Baza uses _namespaces_ to filter events between different environments. Each environment should define its own namespace in order to prevent events being stolen by different environments.
-   Additionally, you can send event as _shared_. Shared events will ignore namespace limitations. It can be useful if you need to set up "event radio" for multiple environments.

## Configuration

| Environment             | Description                                                                                                                                                                                                | Required? | Example                                     |
| ----------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------------------------------------------- |
| `KAFKA_CLIENT_ID`       | Prefix for Client ID for connections. Baza will generate client id with unique hex-string in the end of client id                                                                                          | **Yes**   | my-project-client                           |
| `KAFKA_CLIENT_GROUP_ID` | Prefix for Group ID for connections. Baza will uses environment value + unique hex string for EveryNode pipe                                                                                               | **Yes**   | my-project-group                            |
| `KAFKA_CLIENT_BROKERS`  | Client brokers (Kafkas IP + Port) separated by commas                                                                                                                                                      | **Yes**   | localhost:9200,kafka-stg.my-project.io:9200 |
| `KAFKA_NAMESPACE`       | Optional namespace which will guarantee that events will not be stolen by different deployment targets. It's recommended to set up different namespaces for every project and for every deployment target. | No        | my-project-stg                              |

## Security

-   Every production deployment for each project **MUST** have dedicated Kafka instance.
-   All development / test / stage deployments may use same Kafka instance. You should set unique `KAFKA_NAMESPACE` for each deployment target.
-   Production kafka **MUST** be placed under private network and must be not accessible externally.

## Services

### KafkaService

Kafka service allows you to publish events to Kafka. Kafka Service is additionally responsible for logging events from Kafka and providing public accessors to multi/every node kafka pipes.

-   Use `publish` method publish events to Kafka.
-   Service publish received events from Kafka via `kafkaEvents$` Observable. Feel free to subscribe.

```typescript
import { KafkaService } from '@scaliolabs/baza-core-api';

// List of Kafka topics to work with
enum MyTopic {
    FooTopic = 'MyTopic',
    BarTopic = 'BarTopic',
}

// "Array" of mapped Topic + Payload (Kafka Message)
type MyPayload = { topic: FooTopic; payload: { fooId: number } } | { topic: BarTopic; payload: { barName: string } };

@Injectable()
export class MyService {
    constructor(
        // You'll need to provide generics for your KafkaService usage
        private readonly kafka: KafkaService<MyTopic, MyPayload>,
    ) {}

    async publishFooEvent(fooId: number): Promise<void> {
        // Event will be published to Kafka pipe
        // For EveryNode pipe, use publishEveryNode method
        await this.kafka.publish({
            topic: MyTopic,
            payload: {
                fooId,
            },
        });
    }

    async subscribeToBarEvents(stop$: Subject<void>): Promise<void> {
        this.kafka.kafkaEvents$.pipe(takeUntil(stop$)).subscribe((next) => {
            console.log('TOPIC: ', next.topic);
            console.log('PAYLOAD: ', next.payload);
        });
    }
}
```

### KafkaAdminService

Kafka Admin Service provides accessors to Kafkas clients/producers/consumers and allows you to manually bootstrap topics.
Baza should automatically up topics on event request, but if the feature is disabled, you can manually run `bootstrapTopics` method.

Additionally, Baza Skeleton provides CLI tool `yarn cli:kafka:bootstrap-topics` which should be executed on every deploy.
