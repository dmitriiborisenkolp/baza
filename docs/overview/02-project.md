# Project

## Create new project

Use Baza Skeleton to set up new project. Read more at [Create new project with Skeleton](/getting-started/01-create-application-from-repo-skeleton) documentation section.

You can also start with empty Nx workspace. You need to install `baza-core` libraries set, create necessary applications and add necessary CLI tools.

## Setting up project name

Before starting development you should define project name, project codename and client name.

- Project Name used in mail templates, error codes, messages and any human-readable resources
- Project Codename used as part of local/session storage keys, identificators, logs or any non-human-readable resources
- Client Name is used in mail templates. Client Name can be additionally set in Registry; For non-production environments, Client Name will have additional Environment postfix.

By default `Baza` and `BAZA` keywords used as Project name / codename. It's highly preferrable to redefine project names in order to avoid collisions between other projects.

Best approach here is put `configureBazaProject` call withinto shared and common library which will be included for every application.

```typescript
// libs/your-project-shared/project.ts
import { configureBazaProject } from '@scaliolabs/baza-core-shared';

configureBazaProject({
    projectName: 'Your Project',
    projectCodeName: 'YP',
    clientName: 'Your Project Company',
});

// libs/your-project-shared/index.ts
import './project';
```

## Accessing Project Name / Project Code Name / Client Name

### CMS, Web (Angular)

You can use `BazaNgProjectService` service:

```typescript
@Component({
    template:
        '<strong>Project Name:</strong> {{ view.projectName }}' +
        '<strong>Project Code Name:</strong> {{ view.projectCodeName }}' +
        '<strong>Client Name:</strong> {{ view.clientName }}',
})
export class MyComponent {
    constructor(
        private readonly project: BazaNgProjectService,
    ) {}
    
    get view() {
        return {
            projectName: this.project.projectName, // Your Project
            projectCodeName: this.project.projectCodeName, // YP
            clientName: this.project.clientName, // Your Project Company DEVELOPMENT
        };
    }
}
```

### API (Nest.js)

You can use `ProjectService` from `@scaloplabs/baza-core-api` package to get project configuration.

```typescript
import { Injectable } from '@nestjs/common';
import { ProjectService } from '@scaliolabs/baza-core-api';

@Injectable()
export class MyService {
    constructor(
        private readonly project: ProjectService,
    ) {}
    
    get projectDetails() {
        return {
            projectName: this.project.projectName, // Your Project
            projectCodeName: this.project.projectCodeName, // YP
            clientName: this.project.clientName, // Your Project Company DEVELOPMENT
        };
    }
}
```

### Shared & Non-Angular/Non-Nest.js applications

You can use `getBazaProjectName` and `getBazaProjectCodeName` helpers from `@scaliolabs/baza-core-shared` package.

There is a helper `getBazaClientName` helper available to get Client Name, but it will not works correctly in case if
client will set diffent Client Name in Registry. You will need to additionally get Client Name value with `bazaCommon.clientName`
registry key.
