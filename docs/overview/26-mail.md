# Mail

Baza has a fully-featured Mail services which allows to send emails using different transports, such as SMTP, Mailgun, Customerio and Memory (for E2E reasons).

## Technical Info

-   Package: `baza-core-api`
-   Services: `MailService`
-   Interfaces: `IMailTransport`, `BazaMailInterceptor`
-   Internal Services: `MailDefaultTemplateVariablesService`, `MailHandlebarsService`, `MailInjectVariablesService`
-   Mail Transports: `MailSmtpTransport`, `MailMemoryTransport`, `MailMailgunTransport`, `MailCustomerIoTransport`, `MailSendpulseTransport`
-   Entities: `BazaMailSentEntity`
-   Repositories: `BazaMailSentRepository`
-   Helpers: `mailRecipientsToTOField`, `defaultApplicationForEmail`

## Concepts

Mail layer works on multiple primary items:

-   Service `MailService` in an **entry point** for all features which are related to sending email
-   Service is sending emails using **Mail Transport**. Mail Transports defines which external services will be used to deliver emails and how exactly external service will be used.
-   **Environment configuration** defines which Mail Transport will be used and specify configuration for Mail Transport such as API keys or Login/Password for SMTP
-   E2E tests are working with `MailMemoryTransport` which catches and stores all emails in memory. Access to emails during E2E tests could be achieved with `BazaE2eNodeAccess` Data-Access service.
-   Mail service can be configured to allow sending emails only to recepients with specific domains defined in `MAIL_ALLOWED_DOMAINS` environment variable. Usually it's used for Test, Stage or UAT environment
-   **Mail Template** is primary unit for sending email to user. Each Template will have Text and HTML version of Template and can accepts **Template Variables**
-   **Template Variables** used to pass variable string to emails.
    -   Template Variables are different for Text and HTML versions of Template
    -   Template Variables contains multiple groups of variables:
        -   **Default Template Variables** which are available for all Mail Templates
        -   **Request Template Variables** which are specified for each send email request
        -   **Injected Template Variables** which can be injected using `MailInjectVariablesService` outside of primary usage
-   Every email template is wrapped with **Template Layout** which is specified by **Application** and **Language**.
-   This is possible to intercept all requests to send an email using **Mail Interceptors** (`BazaMailInterceptor`). You can allow/disallow sending an email and additionally you can modify environment variables or request using interceptor.

## Configuration

Mail services are configured by Environment Variables. You can select a Mail Transport and set up a configuration for specific Mail Transport.

Please check [Environment variables – API](/devops/02-environment-variables-api) documentation article for more details.

## Defining Mail Templates

**Mail Templates** should be defined in order to use `MailService.send` method to send email. Mail Templates describes both technical information (like paths to templates in repository) and documentation information which will be visible in CMS and Baza Documentation Portal.

Every Mail Template should contains:

-   **Name** - Name is working as ID in Send Mail Requests
-   **Title** – Human-Readable Name of Mail Template (Used for Documentation Purposes)
-   **Description** – Human-Readable Description of Mail Template (Used for Documentation Purposes)
-   **Tag** – Used to group multiple Mail Templates to Groups
-   **FilePath** – File Path is relative to `apps/api/i18n/en/mail-tempaltes` directory. File Path is used to generate Email content when using Repository solution for generating Email Contents
-   **Variables** - array of Variables Definitions. It's for Documentation Purposes only
-   **Attachments** (optionally) – describes attachments which may be attached to emails generated with Mail Template. It's used for Documentation Purposes only

### Example: Defining Mail Templates for `my-package`

```typescript
// libs/my-packages/lib/constants/my-package.mail.ts

/**
 * List of Mail Templates (Names)
 * You will refer to this ENUM in MailService.send method calls
 * You should prepend package name because all names will be shared in same namespace
 */
export enum MyPackageMail {
    MyPackageNotification = 'MyPackageNotification',
    MyPackageInquiryEmail = 'MyPackageInquiryEmail',
}

/**
 * Tags
 * Your Mail Templates will be put under "MyPackage" group in this example
 */
export enum MyPackageMailTags {
    MyPackage = 'MyPackage',
}

/**
 * my-package registers sets of Mail Templates
 * @see MyPackageMail
 * @see MyPackageMailTags
 */
registerMailTemplates([
    {
        name: MyPackageMail.MyPackageNotification,
        title: 'Notification',
        description: 'Some notification sent to users',
        tag: MyPackageMailTags.MyPackage,
        filePath: 'my-package/notification/notification.{type}.hbs', // {type} is replaced by `text` or `html` strings
        variables: [
            {
                field: 'foo',
                description: 'Foo',
                optional: false,
            },
            {
                field: 'bar',
                description: 'Bar',
                optional: false,
            },
        ],
    },
    {
        name: MyPackageMail.MyPackageInquiryEmail,
        title: 'Inquiry Email',
        description: 'Another example of Mail Template',
        tag: MyPackageMailTags.MyPackage,
        filePath: 'my-package/inquiry-email/inquiry-email.{type}.hbs', // {type} is replaced by `text` or `html` strings
        variables: [
            {
                field: 'foo',
                description: 'Foo',
                optional: false,
            },
            {
                field: 'bar',
                description: 'Bar',
                optional: false,
            },
        ],
    },
]);
```

## Sending an Email

You can send emails after finishing setting up definitions for your Mail Templates. Here is an example:

```typescript
@Injectable()
export class MyService {
    constructor(private readonly mailService: MailService) {}

    async sendNotification(request: { account: AccountEntity; foo: string; bar: string }): Promise<void> {
        await this.mail.send({
            name: MyPackageMail.MyPackageNotification,
            to: [
                {
                    name: request.account.fullName,
                    email: request.account.email,
                },
            ],
            subject: 'baza-account.already-registered.subject', // Subject should be defined in i18n API files
            variables: () => ({
                foo: request.foo,
                bar: request.bar,
            }),
        });
    }
}
```

## Mail Template Validation

Baza is using Mail Templates are Documentation source. This documentation is auto-generated from registered Mail Templates and this documentation is used for Baza CMS sections related to Setting up Email Templates (for Customer.IO integration, as example). It's important to keep Mail Templates definitions and Template Variables definitions up to date, because it will be used by users w/o access to Repository.

You can enable automatic Mail Template Validation which will intercept and fail Send Mail Requests if Request or Mail Template does not have enough data or documentation. While it's recommended to enable Mail Template Validation by default, feature is disabled usually, but it could be enabled with `bazaApiBundleConfigBuilder.withMailTemplateValidation()` helper.

## Customer.IO Integration with Transaction Emails Feature

Baza allows using Customer.IO Templates when Customer.IO Transport is selected. In order to enable this integration, you should:

-   Configure Mail Module to use Customer.IO Transport
-   Enable Usage of Customer.IO templates for API application
-   Enable Customer.IO Mail Link CMS for CMS application
-   Link Baza Mail Templates with Customer.IO templates. You can link it with CMS or predefine links in API application.

### Configure Mail Module to use Customer.IO Transport

You should receive `appKey` from Customer.IO panel and update related `.env` configurations with:

```
MAIL_TRANSPORT=customerio
MAIL_CUSTOMER_IO_APP_KEY=... # appKey
```

### Enable Usage of Customer.IO templates for API application

You can enable Customer.IO integration with `bazaApiBundleConfigBuilder.withCustomerIoTemplates` helper. This method is also accepting a callback which will allow you to predefine Links between Baza Mail Templates and Customer.IO Templates. We're recommending to keep links definitions in separated flags due of huge amount of Mail Templates available in Baza.

```typescript
// apps/api/src/main-customerio.ts
import { BAZA_ENVIRONMENTS_CONFIG, BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaCoreMail, SendMailCustomerIoBootstrap } from '@scaliolabs/baza-core-api';

export const mainCustomerio: (env: BazaEnvironments) => Array<SendMailCustomerIoBootstrap> = (env) => {
    return BAZA_ENVIRONMENTS_CONFIG.testEnvironments.includes(env)
        ? []
        : [
              {
                  name: BazaCoreMail.BazaAccountCmsResetPassword, // Baza Mail Template
                  customerIoId: 5, // ID of Customer.IO template. Displayed on Transacton page
              },
              {
                  name: BazaCoreMail.BazaAccountCmsChangeEmail,
                  customerIoId: 4,
              },
              // Any other Mail Tempaltes from BazaCoreMail, BazaNcMail or BazaContentTypesMail ENUM collections
          ];
};

// apps/api/src/main-config.ts
import { mainCustomerio } from './main-customerio';

bazaApiBundleConfigBuilder()
    .withMailTemplateValidation() // We're recommended to enable Mail Template validation for non-Production environments
    .withCustomerIoTemplates(mainCustomerio);
```

### Enable Customer.IO Mail Link CMS for CMS application

As far as we currently have API and CMS applications in separate places, you should also enable Customer.IO Mail Link CMS with `bazaCmsBundleConfigBuilder.withCustomerIoLinkCms()` helper.

```typescript
// apps/cms/src/app/app.module.ts

bazaCmsBundleConfigBuilder().withCustomerIoLinkCms();
```

### Link Baza Mail Templates with Customer.IO templates

You should set up workspace and create Customer.IO Transaction Email Templates. You can also fork existing example Customer.IO workspace and modify templates for your project.

When it's finished, you should link Baza Mail Templates with Customer.IO Transaction Email Templates.

-   You can do it with CMS manually for each environment or your do it with API (in code repository) to predefine links on code level.
-   Predefined links could be updated from code repository, but it will not overwrite your custom changes in CMS unless you will not provide a different Customer.IO id in code.
-   Both options (CMS & Predefined) are supposed to work at same time. You can use both approaches for better experience.

## Enabling "Unlink Template from Customer.IO" feature

Customer.IO Links CMS has "Unlink Template from Customer.IO" feature which is allowing to remove existing Link with Customer.IO. If you would like to enable it (for QA or for any other reason), you should update `bazaCmsBundleConfigBuilder().withCustomerIoLinkCms()` call with providing `options` parameter:

```typescript
bazaCmsBundleConfigBuilder().withCustomerIoLinkCms({ withCustomerIoLinkCmsUnlinkFeature: true });
```

## Injecting Mail Template Variables

Baza provides multiple features which are sending emails, such as emails related to Authorization or sending notifications about payment statuses. While Baza covers most common cases, you may need to customize your templates and add project-specific information to your email notifications.

Service is allowing to register **Injections** to define your custom template variables. Use it to add additional template variables for mail templates which are bundled with Baza libraries

### How to Use it

#### 1. Register injection in `app.module.ts` or anywhere else in your modules:

```typescript
@Global()
@Module({})
export class ApiModule implements OnModuleInit {
    constructor(private readonly mailInjections: MailInjectVariablesService) {}

    onModuleInit(): any {
        // Register injections for specific templates
        this.mailInjections.register([
            {
                name: BazaCoreMail.BazaAccountWebConfirmEmail,
                callback: async (initialVariables) => {
                    return {
                        foo: `Example of custom injections (HTML Template): ${initialVariables.link}`,
                    };
                },
                documentation: [
                    {
                        field: 'foo',
                        description: 'Foo variable description which will be displayed in CMS',
                        optional: false,
                    },
                ],
            },
        ]);

        // Register injections for all templates
        this.mailInjections.registerGlobal([
            {
                callback: async (defaultVariables) => ({
                    bar: `BAR Global Variable: ${defaultVariables.clientName}`,
                }),
            },
        ]);
    }
}
```

#### 2. Add new variables to your templates, partials or Customer.IO Transaction Email Templates:

```handlebars
{{#> webLayoutHtmlEn }}
    <p>Hello {{../fullName}},</p>
    <p>Please confirm your email by clicking the following link: <a href='{{ ../link }}'>Verify Account</a></p>

    <p>{{ ../foo }} </p> <!-- New custom variable -->
    <p>{{ ../bar }} </p> <!-- New custom variable (global) -->

    <p>Thank You,</p>
    <p>{{ clientName }}</p>
    <p>
        <img src="{{ ../webLogoUrl }}" alt="logo" width="200px">
    </p>
{{/webLayoutHtmlEn}}
```

## Intercepting Send Mail Requests

⚠️ **Interceptors will work only with emails sent using Mail Templates. Using deprecated methods will not allow you to use Interceptors feature.**

Baza allows you to control Send Mail request with **Mail Interceptors**. You can register interceptors which will be able to:

-   Allow or Disallow sending an email
-   Modify Request object, Access to Mail Transport or Modify Template Variables

Service `MailService` has multiple methods to work with interceptors:

-   `MailService.attachInterceptor(interceptor: BazaMailInterceptor): void` - attaches instance of Interceptor to Baza Mail services
-   `MailService.detachInterceptor(interceptor: BazaMailInterceptor): void` - detaches interceptors (by instance)
-   `MailService.detachInterceptorByType(interceptorType: ComponentType<BazaMailInterceptor>): void` - detaches interceptors (by type)
-   `MailService.listInterceptors(): Array<BazaMailInterceptor>` - returns list of current interceptors. You can manually modify interceptors list with array returnd by method. The service will not try to run `undefined` values as interceptors, so you can directly `delete` items in array

Interceptors should return `boolean` value to indicate should Send Mail request proceed or not. Exceptions thrown by Interceptor will work as `false` value of Interceptor.

You can also create an interceptor which will not only allow to stop processing Send Mail request, but also throw an exception which will be visible in logs. Set `mode: 'fail'` property in your interceptor to enable this workflow.

### Example 1: Implementation of custom Interceptor

```typescript
import { Module, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { BazaMailInterceptor, BazaMailInterceptorPayload, MailService } from '@scaliolabs/baza-core-api';

class ExampleMailInterceptor implements BazaMailInterceptor {
    constructor(private readonly myService: MyService) {}

    async intercept(payload: BazaMailInterceptorPayload): Promise<boolean> {
        const isAllowed = await this.myService.allowSendEmailTo(payload.request.to);

        if (!isAllowed) {
            throw new MyAppRecipientNotAllowedException(payload.request.to);
        }

        return true;
    }
}

@Module({
    imports: [MailService],
    providers: [MyService, ExampleMailInterceptor],
})
export class YourAppModule implements OnModuleInit, OnModuleDestroy {
    constructor(private readonly mailService: MailService, private readonly exampleMailInterceptor: ExampleMailInterceptor) {}

    onModuleInit(): void {
        this.mailService.attachInterceptor(this.exampleMailInterceptor);
    }

    onModuleDestroy(): void {
        this.mailService.detachInterceptor(this.exampleMailInterceptor);
    }
}
```

### Example 2: Implementation of anonymous Mail Interceptor

You may use this approach for small interceptors.

```typescript
import { Module, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { BazaMailInterceptor, BazaMailInterceptorPayload, MailService } from '@scaliolabs/baza-core-api';

@Module({
    imports: [MailService],
    providers: [MyService, ExampleMailInterceptor],
})
export class YourAppModule implements OnModuleInit, OnModuleDestroy {
    constructor(private readonly myService: MyService, private readonly mailService: MailService) {}

    private readonly exampleMailInterceptor: BazaMailInterceptor = {
        intercept: async (payload: BazaMailInterceptorPayload) => {
            const isAllowed = await this.myService.allowSendEmailTo(payload.request.to);

            if (!isAllowed) {
                throw new MyAppRecipientNotAllowedException(payload.request.to);
            }

            return true;
        },
    };

    onModuleInit(): void {
        this.mailService.attachInterceptor(this.exampleMailInterceptor);
    }

    onModuleDestroy(): void {
        this.mailService.detachInterceptor(this.exampleMailInterceptor);
    }
}
```

### Example 3: Implementation of `BazaDebugMailInterceptor`

This is an built-in Mail Interceptor from `baza-core-api` package.

```typescript
/**
 * Mail Interceptor which displays Debug Log for each Send Mail Request
 * The Interceptor will be enabled if MAIL_DEBUG environment flag is enabled
 */
export class BazaDebugMailInterceptor implements BazaMailInterceptor {
    constructor(private readonly logger: BazaLogger, @Inject(MAIL_MODULE_CONFIG) private readonly moduleConfig: MailModuleConfig) {}

    async intercept(payload: BazaMailInterceptorPayload): Promise<boolean> {
        if (this.moduleConfig.debug) {
            this.logger.debug(
                {
                    message: 'Sending an email with request:',
                    request: payload.request,
                    variables: payload.variables,
                },
                BazaDebugMailInterceptor.name,
            );
        }

        return true;
    }
}
```

## Unregistering Mail Templates

Depends on Project requirements you may disable some Mail Templates and all validations / CMS banners about it. As example, Baza provides Change Email feature which may be useful for some projects, but some projects would like to not use it and even mention it at all.

You can unregister Mail Template using `unregisterMailTemplate` helper or using `MailTemplateService.unregisterMailTemplate` helper.

### Example: Unregistering Confirm Email features

```typescript
import { unregisterMailTemplate, BazaCoreMail } from '@scaliolabs/baza-core-api';

unregisterMailTemplate([
    BazaCoreMail.BazaAccountCmsChangeEmail,
    BazaCoreMail.BazaAccountCmsChangeEmailNotification,
    BazaCoreMail.BazaAccountWebChangeEmail,
    BazaCoreMail.BazaAccountWebChangeEmailNotification,
]);
```
