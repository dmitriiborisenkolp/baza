# Device Tokens

Baza provides a flexible mechanism to store and manage FCM/Apple device tokens for user accounts.

-   [API usage](#api-usage)

    1. [Link or unlink device tokens](#1-link-or-unlink-device-tokens)
    2. [Device tokens utilities](#2-device-tokens-utilities)
    3. [Destroy device tokens](#3-destroy-device-tokens)
    4. [Bulk actions for device tokens](#4-bulk-actions-for-device-tokens)
    5. [Control time to live of stored device tokens](#5-control-time-to-live-of-stored-device-tokens)
    6. [Enable or disable logs](#6-enable-or-disable-logs)

-   [CMS usage](#cms-usage)

    1. [Enable feature](#enable-feature)

-   [Functional usage](#functional-usage)
    1. [How to link or unlink FCM Tokens (Android)](#how-to-link-or-unlink-fcm-tokens-android)
    2. [How to link or unlink Apple Tokens (iOS)](#how-to-link-or-unlink-apple-tokens-ios)
    3. [Implicit use cases](#implicit-use-cases)

## **API usage**

### 1. link or unlink device tokens

Baza provides the underlying services that can be used to link or unlink device tokens at convenience.
To link a device token the following structure must be provided:

| Property  | Required | Type        | Description                  | Example                                                                 |
| --------- | -------- | ----------- | ---------------------------- | ----------------------------------------------------------------------- |
| accountId | Yes      | INT         | User account ID              | 16                                                                      |
| type      | Yes      | FCM / Apple | Device token type            | Apple                                                                   |
| token     | Yes      | Date        | FCM/Apple device token value | 740f4707 bebcf74f 9b7c25d4 8e335894 5f6aa01d a5ddb387 462c7eaf 61bb78ad |

#### FCM device token

```ts
deviceTokensDataAccess.link({
    accountId: 16,
    type: BazaDeviceTokenType.FCM,
    token: 'some-fcm-device-token',
});

deviceTokensDataAccess.unlink({
    accountId: 16,
    type: BazaDeviceTokenType.FCM,
    token: 'some-fcm-device-token',
});
```

#### Apple device token

```ts
deviceTokensDataAccess.link({
    accountId: 12,
    type: BazaDeviceTokenType.Apple,
    token: 'some-apple-device-token',
});

deviceTokensDataAccess.unlink({
    accountId: 12,
    type: BazaDeviceTokenType.Apple,
    token: 'some-apple-device-token',
});
```

> ℹ️ When trying to link an existing device token, the update timestamp for that token will be refreshed automatically.

```ts
deviceTokensDataAccess.link({
    accountId: 12,
    type: BazaDeviceTokenType.Apple,
    token: 'some-apple-device-token',
});
// Token created at 20:00

setTimeout(() => {
    deviceTokensDataAccess.link({
        accountId: 12,
        type: BazaDeviceTokenType.Apple,
        token: 'some-apple-device-token',
    });
}, 1000 * 60 * 6);
// Token updated six minutes after at 20:06
```

### 2. Device tokens utilities

#### Extract device tokens from HTTP headers

Utility function that allows extracting device tokens from HTTP headers. The result is grouped by linked and unliked sets of device tokens.

```ts
import { extractDeviceTokensFromRequest } from '@scaliolabs/baza-core-shared';

const actionSets = extractDeviceTokensFromRequest(someHttpRequest);
console.log(actionSets);

/*
Result

{
    link: [
        ['FCM', 'new-fcm-token'],
        ['Apple', 'new-apple-token'],
    ],
    unlink: [
         ['Apple', 'old-apple-token'],
    ]
}
*/
```

### 3. Destroy device tokens

Device tokens can be dynamically destroyed based on a set of conditions that look like the following:

| Property   | Required | Type              | Description                                                                                                                      | Example                                                       |
| ---------- | -------- | ----------------- | -------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------- |
| accountId  | No       | INT               | User account ID                                                                                                                  | 16                                                            |
| scope      | No       | All / FCM / Apple | Device token disposal scope                                                                                                      | Apple                                                         |
| beforeDate | No       | Date              | Date where the latest update occurred. It can be read as "Remove device tokens where the latest update was **before this date**" | new Date(new Date().setMinutes(new Date().getMinutes() - 20)) |

There are two providers that can be used to destroy device tokens.

#### BazaDeviceTokenService

Use when you want to receive the number of destroyed device tokens right away.

```ts
import { BazaDeviceTokenService } from '@scaliolabs/baza-core-api';

export class MyCustomClass {
    constructor(private readonly deviceTokenService: BazaDeviceTokenService) {}

    async destroyAllTokens(): Promise<number> {
        const { affected } = await this.deviceTokenService.destroyDeviceTokens({
            scope: 'All',
        });

        return affected;
    }

    async destroyFcmTokensByAccount(accountId: number): Promise<number> {
        const { affected } = await this.deviceTokenService.destroyDeviceTokens({
            accountId,
            scope: 'FCM',
        });

        return affected;
    }

    async destroyOutdatedFcmTokensByAccount(accountId: number): Promise<number> {
        const now = new Date();
        const twentyMinsAgo = new Date(now.setMinutes(now.getMinutes() - 20));

        const { affected } = await this.deviceTokenService.destroyDeviceTokens({
            accountId,
            scope: 'FCM',
            beforeDate: twentyMinsAgo,
        });

        return affected;
    }
}
```

#### BazaDeviceTokenDestroyCommand

Use when you want to apply CQRS pattern and execute a command to destroy device tokens inside a stream of commands.

```ts
import { BazaDeviceTokenDestroyCommand } from '@scaliolabs/baza-core-shared';

export class MyCustomClass {
    constructor(private readonly commandBus: CommandBus) {}

    async destroyAllTokens(): Promise<void> {
        await this.commandBus.execute(
            new BazaDeviceTokenDestroyCommand({
                scope: 'All',
            }),
        );
    }

    async destroyFcmTokensByAccount(accountId: number): Promise<void> {
        await this.commandBus.execute(
            new BazaDeviceTokenDestroyCommand({
                accountId,
                scope: 'FCM',
            }),
        );
    }

    async destroyOutdatedFcmTokensByAccount(accountId: number): Promise<void> {
        const now = new Date();
        const twentyMinsAgo = new Date(now.setMinutes(now.getMinutes() - 20));

        await this.commandBus.execute(
            new BazaDeviceTokenDestroyCommand({
                accountId,
                scope: 'FCM',
                beforeDate: twentyMinsAgo,
            }),
        );
    }
}
```

### 4. Bulk actions for device tokens

There might be some cases when linking/unlinking multiple device tokens at once could be useful (e.g. link/unlink device tokens after extracting them from the HTTP headers).

For the mentioned above, Baza provides a bulk command that allows bulk actions for device tokens. It receives the same action sets mentioned in the [2. Device tokens utilities](#2-device-tokens-utilities) section.

```ts
import { BazaDeviceTokenBulkCommand } from '@scaliolabs/baza-core-shared';

export class MyCustomClass {
    constructor(private readonly commandBus: CommandBus) {}

    async updateTokens(): Promise<void> {
        await this.commandBus.execute(
            new BazaDeviceTokenBulkCommand({
                accountId: 16,
                actionSets: {
                    link: [
                        ['FCM', 'my-new-fcm-token'],
                        ['Apple', 'my-new-apple-token'],
                    ],
                    unlink: [
                        ['Apple', 'my-old-apple-token'],
                        ['Apple', 'another-old-apple-token'],
                        ['FCM', 'an-old-fcm-token-to-unlink'],
                    ],
                },
            }),
        );
    }
}
```

### 5. Control time to live of stored device tokens

Baza can automatically clean up outdated device tokens on a time basis and it does this by running two custom watchers; one to check FCM tokens and the other one to check Apple tokens. They run independently from each other and trigger a validation each minute to see if there are any outdated tokens stored in the database. If any outdated tokens are found, it runs the destroy service to remove them from the database. This behavior can be controlled through environmental variables.

| Variable                              | Required | Type | Description                                                    | Example |
| ------------------------------------- | -------- | ---- | -------------------------------------------------------------- | ------- |
| `BAZA_DEVICE_TOKEN_FCM_TTL_MINUTES`   | Yes      | INT  | Specify time to live in minutes of stored FCM device tokens.   | 5       |
| `BAZA_DEVICE_TOKEN_APPLE_TTL_MINUTES` | Yes      | INT  | Specify time to live in minutes of stored Apple device tokens. | 87600   |

### 6. Enable or disable logs

All logs related to device tokens are disabled by default. To enable device tokens logs it's necessary to set the variable `BAZA_DEVICE_TOKEN_LOGS` equal to `1` or `true`.

```sh
BAZA_DEVICE_TOKEN_LOGS=1

# or

BAZA_DEVICE_TOKEN_LOGS=true
```

---

## **CMS usage**

There is a feature in the CMS application that allows administrators to view and manage FCM/Apple device tokens per user account. The feature comes disabled out of the box, and developers need to add it explicitly to the CMS application.

### Enable feature

To enable the feature, is's necessary to call the method `withDeviceTokenFeature`

```ts
// app.module.ts

bazaCmsBundleConfigBuilder().withDeviceTokenFeature();
```

After that, a new action item called "Device tokens" is added to every user in the user accounts page that upon clicking, navigates to the device tokens page where the user tokens can be visualized, edited, created or removed.

Aditionally, the feature can be disabled explicitly by calling the method `withoutDeviceTokenFeature`

```ts
// app.module.ts

bazaCmsBundleConfigBuilder().withoutDeviceTokenFeature();
```

## **Functional usage**

HTTP headers should be passed from mobile clients on every HTTP request that may be considered convenient to link or unlink device tokens to a specific account. After that, Baza will take care of all the logic to store them and keep them updated.

This is the list of the self-explanatory whitelisted HTTP headers to link or unlink device tokens:

-   X-Baza-Fcm-Token
-   X-Baza-Fcm-Token-Unlink
-   X-Baza-Apns-Token
-   X-Baza-Apns-Token-Unlink

> ℹ️ The following examples will use the "Baza Status", "Baza Maintenance" and "Baza Version" endpoints but have in mind it works with any HTTP request.

### How to link or unlink FCM Tokens (Android)

```sh
# Baza status endpoint -> link token
curl --request GET 'https://baza-api.test.scaliolabs.com' \
  --header 'X-Baza-Fcm-Token: FIRST-FCM-DEVICE-TOKEN'

# Baza status endpoint -> refresh linked token
curl --request GET 'https://baza-api.test.scaliolabs.com' \
  --header 'X-Baza-Fcm-Token: FIRST-FCM-DEVICE-TOKEN'

# Baza maintenance endpoint -> link token
curl --request GET 'https://baza-api.test.scaliolabs.com/baza-maintenance/status' \
  --header 'X-Baza-Fcm-Token: SECOND-FCM-DEVICE-TOKEN'

#
# Result: FIRST-FCM-DEVICE-TOKEN and SECOND-FCM-DEVICE-TOKEN stored in the database
#

# Baza version endpoint -> unlink token
curl --request GET 'https://baza-api.test.scaliolabs.com/version' \
  --header 'X-Baza-Fcm-Token-Unlink: FIRST-FCM-DEVICE-TOKEN'

#
# Result: SECOND-FCM-DEVICE-TOKEN stored in the database
#
```

### How to link or unlink Apple Tokens (iOS)

```sh
# Baza status endpoint -> link token
curl --request GET 'https://baza-api.test.scaliolabs.com' \
  --header 'X-Baza-Apns-Token: FIRST-APPLE-DEVICE-TOKEN'

# Baza status endpoint -> refresh linked token
curl --request GET 'https://baza-api.test.scaliolabs.com' \
  --header 'X-Baza-Apns-Token: FIRST-APPLE-DEVICE-TOKEN'

# Baza maintenance endpoint -> link token
curl --request GET 'https://baza-api.test.scaliolabs.com/baza-maintenance/status' \
  --header 'X-Baza-Apns-Token: SECOND-APPLE-DEVICE-TOKEN'

#
# Result: FIRST-APPLE-DEVICE-TOKEN and SECOND-APPLE-DEVICE-TOKEN stored in the database
#

# Baza version endpoint -> unlink token
curl --request GET 'https://baza-api.test.scaliolabs.com/version' \
  --header 'X-Baza-Apns-Token-Unlink: FIRST-APPLE-DEVICE-TOKEN'

#
# Result: SECOND-APPLE-DEVICE-TOKEN stored in the database
#
```

### Implicit use cases

#### Account deactivation

When deactivating user accounts all of their device tokens are destroyed by default. `BazaDeviceTokenDestroyCommand` is being used with scope `All` to do so. This behavior is not currently open for modifications from external sources at the moment.
