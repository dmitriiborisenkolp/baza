# Errors & Exceptions

API will returns success response (as it's defined in endpoint's API documentation) or `BazaError` JSON response which will contains unique error code identifier, error message and HTTP status code both in headers and JSON body. 

```json
{
    "statusCode": 406,
    "code": "VersionIsOutdated",
    "message": "This version of YourProject is no longer supported. Please update to the latest version of the application to continue using it."
}
```

- You can use `code` for error-specific actions in your client application. Do not rely on `message` field; error message can be updated or translated at any moment.
- You can display error message from `message` field
- If your project support multiple languages, error message will be translated depends on `X-Baza-I18n-Language` request header.

## API exceptions

Every feature should define enum with unique error codes. Make sure that every error unique code is unique within project; you should not name error codes in generic way like `NotFound`.

```typescript
// libs/my-project-shared/feature-foo/error-codes/foo.error-codes.ts

export enum FooErrorCodes {
    FooNotFound = 'FooNotFound',
    FooDuplicate = 'FooDuplicate',
}

export const fooErrorCodesI18nEn = {
    [FooErrorCodes.FooNotFound]: 'Foo Not Found',
    [FooErrorCodes.FooDuplicate]: 'Foo with name "{{ name }}" already exists',
};
```

You should create exception classes for every defined unique error code. All exceptions should extends from `BazaAppException` class.

```typescript
// libs/my-project-api/feature-foo/exceptions/foo-not-found.exception.ts
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { FooErrorCodes, fooErrorCodesI18nEn } from '@scaliolabs/my-project-shared/feature-foo';
import { HttpStatus } from '@nestjs/common';

export class FooNotFoundException extends BazaAppException {
    constructor() {
        super(
            FooErrorCodes.FooNotFound,
            fooErrorCodesI18nEn[FooErrorCodes.FooNotFound],
            HttpStatus.NOT_FOUND,
        );
    }
}
```

Additionally, you can pass additional arguments with constructor and with `args` argument of `super(...)` call.

```typescript
// libs/my-project-api/feature-foo/exceptions/foo-not-found.exception.ts
import { BazaAppException } from '@scaliolabs/baza-core-api';
import { FooErrorCodes, fooErrorCodesI18nEn } from '@scaliolabs/my-project-shared/feature-foo';
import { HttpStatus } from '@nestjs/common';

export class FooDuplicateException extends BazaAppException {
    constructor(name: string) {
        super(
            FooErrorCodes.FooDuplicate,
            fooErrorCodesI18nEn[FooErrorCodes.FooNoFooDuplicatetFound],
            HttpStatus.CONFLICT,
            { name }, // Baza will automatically replace "{{ name }}" in error message
            { duplicatedWithName: name } // Additional details object. It's used for Sentry integration to provide additional details about error
        );
    }
}
```

## Integration & e2e tests

For non-success cases you will need to check that response is Baza error and (usually) you also must check for error unique code.

```typescript

// foo.e2e-spec.ts
it('will not returns Foo with invalid ID', async () => {
    const response: BazaError = await dataAccessAccount.getFooById({
        id: 9999999,
    }) as any;

    expect(isBazaErrorResponse(response)).toBeTruthy();
    expect(response.code).toBe(FooErrorCodes.FooNotFound);
});
```

## Useful utils & helpers

- `isBazaApiError(err)` - returns true if given object looks like as Baza API error response. It's used a lot for integration and e2e tests.
- `BazaError<T = any>` - you can cast error response with `BazaError` type. Error codes enum can be used for `T` generic.

## Override default error messages

You can override default error message for every available error code.

```typescript
import { HttpStatus } from '@nestjs/common';
import { bazaAppExceptionOverrideMessage } from '@scaliolabs/baza-core-api';

// Override Error Message only
bazaAppExceptionOverrideMessage(VersionErrorCodes.VersionIsOutdated, 'Your version is outdated!');

// Override Error Message & Http Status Code
bazaAppExceptionOverrideMessage(VersionErrorCodes.VersionIsOutdated, 'Your version is outdated!', HttpStatus.AMBIGUOUS);
```