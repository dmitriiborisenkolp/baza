# Directory Structure & Libraries

Baza is developed on top of Nx ([https://nx.dev]()). All recomendations, guidelines and conventions related to Nx are also appliable to Baza's projects.

Your main focus is dealing with Angular, Nest.JS and shared scopes when you're working with project structure. The documentation section contains common recomendations about project directory structure and common library types.

## Application

Please read [Applications](/overview/03-applications.md) documentation before starting work with libraries.

## Libraries

Library is common and main unit in Nx development flow. Before starting work with new project, you must read Nx documentation, especially:

- Intro to Nx: [https://nx.dev/latest/angular/getting-started/intro]()
- Mental Model: [https://nx.dev/latest/angular/core-concepts/mental-model]()
- Using Nx at Enterprises: [https://nx.dev/latest/angular/guides/monorepo-nx-enterprise]()
- CLI Generate Tool: [https://nx.dev/latest/angular/cli/generate]()

## Library scopes

Every Nx library has *scope*. Scope defines which applications can use the library and which packages are allowed to use in the library.

There are several scopes defined in common Baza project:

- **Shared** libraries - these libraries contains DTOs, Error Codes, Endpoints and additional models which are used for API, CMS, WEB or Data Access. Shared libraries should be generated with `nx g @nrwl/node:lib` command. Shared libraries should not imports Angular or especially Nest.JS libraries with some few exceptions. Importing resources from shared libraries into Angular application should not lead to huge bundle size increse. Examples: `baza-core-shared`, `my-project-shared/articles`
- **Data Access** libraries contains Angular or Node services which should allow to access project's API. Data access libraries can contains Angular imports, but should not contains Nest.JS imports.
- **Ng** libraries contains shared Angular components & services which can be used for CMS or Web applications. You should not put CMS-only or Web-only resources inside `ng` packages. Examples: `baza-core-ng`, `my-project-ng/article-cards`
- **API** libraries contains Nest.JS (API) modules. Each feature should have dedicated `api` library. Examples: `baza-core-api`, `my-project-api/feature-articles` 
- **CMS** libraries contans CMS modules, components and routings. Each feature should have dedicated `cms` library. These libraries can be imported directly to CMS application or can be lazy loaded with `import(...)` keyword. Examples: `baza-core-cms`, `my-project-cms/feature-articles`
- **Web** libraries contains Web modules, components and routings. Additionally you can define styles here which will works on global level. Like CMS, these libraries can be imported directly to Web application or can be lazy loaded with `import(...)` keyword. Examples: `baza-core-web`, `my-project-web/feature-articles

## Library collections

 Multiple libraries with same scope should be grouped under *library collection*.

Mostly every project will have these library collections:

- `libs/my-project-shared` for Shared DTOs, endpoints, models, error codes and i18n resources
- `libs/my-project-data-access` for Data Access services
- `libs/my-project-api` for API implementations of features
- `libs/my-project-cms` for CMS sections of features
- `libs/my-project-web` for Web application
- `libs/my-project-ng` optionally for shared components both CMS and Web

## Example project structure

```
my-project
├── apps/
│   ├── my-project-api/ (API Nest.JS application)
│   ├── my-project-cms/ (CMS Angular application)
│   ├── my-project-web/ (Web Angular application)
├── libs/
│   ├── my-project-shared/
│   │   └── feature-foo/ (Shared models for Foo feature)
│   │       ├── dto (DTOs for Foo feature)
│   │       ├── endpoints (Endpoints definitions for Foo feature)
│   │       ├── error-codes (Error codes for Foo feature)
│   │       ├── models (optional) (Additional models, enums, interfaces or anything else for Foo feature)
│   │       ├── i18n (optional) (i18n resources for CMS / API / Web of Foo feature)
│   │       ├── feature-foo.open-api.ts (Open API group / tags; used by Swagger/Redoc API documentation)
│   │       └── feature-foo.acl.ts (ACL definitions)
│   ├── my-project-ng/ (Shared Angular components & services used both CMS and Web)
│   │   └── (depends on project)
│   ├── my-project-api/ (API application)
│   │   ├── core-baz/ (Some core & shared within API features library)
│   │   ├── feature-foo/ (Feature library #1)
│   │   ├── feature-bar/ (Fearure library #2)
│   ├── my-project-data-access (Data Access services; used both by CMS and Web)
│   │   └── foo/ (Data access for Foo feature)
│   │       ├── ng (Data Access services for Angular applications)
│   │       │   ├── foo.data-access.ts (Data Access for public API)
│   │       │   └── foo-cms.data-access.ts (Data Access for CMS API)
│   │       └── node (Data Access services for Nest.JS, Node applications and integration tests)
│   │           ├── foo.node-access.ts (Data Access for public API)
│   │           └── foo-cms.node-access.ts (Data Access for CMS API)
│   ├── my-project-cms/ (CMS application)
│   │   ├── feature-foo/ (CMS for Foo feature)
│   │   ├── feature-bar/ (CMS for Bar feature)
│   │   ├── ui-components/ (Shared components for CMS)
│   │   ├── ui-components/ (Shared components for CMS)
│   │   └── utils/ (Additional utils for CMS)
│   └── my-project-web/ (Web application)
│       ├── feature-foo/ (Web for Foo feature)
│       ├── feature-bar/ (Web for Bar feature)
│       ├── ui-components/ (Shared components & services for Web only)
│       ├── ui-styles/ (Web styles)
│       └── util/ (additional utils for Web)
├── tools/
├── workspace.json
├── nx.json
├── package.json
└── tsconfig.base.json
```

## Library bundles

Baza offers bundles for every application. This bundles automatically imports and mostly automatically configures baza modules and services, but most of bundles required some additional configuration before usage. Read [Bundles](/overview/35-bundles) documentation section for more details.

You may need sometimes to group Nx libraries into single bundle module. Name it with additional `Bundle` postfix in class name – it's just a convention, but useful to know.

## API directory structure

API project can contains core libraries with shared services for current project and feature libraries for each project feature.

Directory structure for API depends on project. Usually you need to use only plain directory structure with set of `core-...`/`feature-...` libraries:

```
my-project
├── apps/
│   ├── my-project-api/ (API Nest.JS application)
│   ├── my-project-cms/ (CMS Angular application)
│   ├── my-project-web/ (Web Angular application)
└── libs/
    └── my-project-api/ (API application)
        ├── core-baz/ (Some core & shared within API features library)
        ├── feature-foo/ (Feature library #1)
        └── feature-bar/ (Fearure library #2)
```

If you need to implement hugely different API's for Web and mobile applications and your project is big enough, consider to split services/mappers/repositories and controllers for specific clients with different sets of libraries.

```
my-project
├── apps/
│   ├── my-project-api/ (API Nest.JS application)
│   ├── my-project-cms/ (CMS Angular application)
│   ├── my-project-web/ (Web Angular application)
└── libs/
    └── my-project-api/ (API application)
        ├── core-baz/ (Some core & shared within API features library)
        ├── feature-foo/ (Feature library #1, services/mappers/repositories only + CMS controllers)
        ├── feature-bar/ (Fearure library #2, services/mappers/repositories only + CMS controllers)
        ├── feature-web-foo/ (Web controllers for Fearure library #1)
        ├── feature-web-bar/ (Web controllers for Fearure library #2)
        ├── feature-mobile-foo/ (Mobile controllers for Fearure library #1)
        └── feature-mobile-bar/ (Mobile controllers for Fearure library #2)
```

Avoid using nested directory structure like `core/core-feature-foo` or `mobile/features/foo`. Refactoring nested directory structures may be very time consumable.

Additionally you may decide to extract TypeORM entities into dedicated single library. Avoid it in general, but sometimes there are no choice in terms of circular dependencies

```
my-project
├── apps/
│   ├── my-project-api/ (API Nest.JS application)
│   ├── my-project-cms/ (CMS Angular application)
│   ├── my-project-web/ (Web Angular application)
└── libs/
    └── my-project-api/ (API application)
        ├── core-baz/ (Some core & shared within API features library)
        ├── typeorm/ (TypeORM entities, repositories and common exceptions)
        ├── feature-foo/ (Feature library #1)
        └── feature-bar/ (Fearure library #2)
```

## Web directory structure

Web project can contains several types of libraries:

- `feature-(...)` - feature libraries contains you actual pages.
- `data-access` - you may create your own data-access layer, but it's preferrable to use existing data-access modules & services which should be provided by API/CMS developer.
- `ui-components` - shared library(-ies) which contains components and services used by multiple features.
- `ui-styles` - contains styles only. Use BEM convention and existing web development conventions for styling projects.
- `util` - additional shared helpers, constants, components, services or anything else which can be used for libraries above.

Visit [Library Type](https://nx.dev/latest/angular/structure/library-types) for additional information.

## CMS directory structure

CMS directory structure is same as Web. 
