# Husky & Commitlint

Git commit messages should follow conventional format with issue prefix (Project + Task ID):

```
XXXX-1234: feat(api): example commit message
```

Read more about conventional commits here: [https://github.com/conventional-changelog/commitlint]()

## Configuration for projects forked from Baza Skeleton

Baza Skeleton already has husky & commitlint integration. You should update JIRA Project name in `commitlint.config.js`:

```js
module.exports = {
    extends: ['@commitlint/config-conventional'],
    parserPreset: {
        parserOpts: {
            headerPattern: /^[A-Z]{1,4}-[0-9]{1,4}:\s(\w*)(?:\((.*)\))?: (.*)$/,
            headerCorrespondence: ['type', 'scope', 'subject'],
            issuePrefixes: ['^MYPROJ-[0-9]{1,4}'] // Update MYPROJ with project name
        }
    },
    rules: {
        'scope-empty': [1, 'never'],
    },
};
```

## Configuration for projects which are using Baza libraries

If you're not using Baza Skeleton, you should manually add husky & commitlint integration.

## 1. Install NPM packages

```bash
yarn add husky commitlint @commitlint/config-conventional
```

## 2. Add commitlint configuration

Create `commitlint.config.js` file in project root:

```js
module.exports = {
    extends: ['@commitlint/config-conventional'],
    parserPreset: {
        parserOpts: {
            headerPattern: /^[A-Z]{1,4}-[0-9]{1,4}:\s(\w*)(?:\((.*)\))?: (.*)$/,
            headerCorrespondence: ['type', 'scope', 'subject'],
            issuePrefixes: ['^MYPROJ-[0-9]{1,4}'] // Update MYPROJ with project name
        }
    },
    rules: {
        'scope-empty': [1, 'never'],
    },
};
```

## 3. Add lint command

Add lint commands to `package.json`:

```
"lint": "nx workspace-lint && nx affected:lint --fix",
"lint:all": "nx run-many --all --target=lint",
```

## 4. Create hooks

```
npx husky add .husky/commit-msg "yarn commitlint --edit $1"
npx husky add .husky/pre-commit "yarn lint"
```

Make sure that `.husky/commit-msg` and `.husky/pre-commit` files created. Also, check that `$1` string exists in `commit-msg` file.
