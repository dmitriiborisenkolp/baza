# Baza

## MUST READ

- Nx (for API developers): [https://nx.dev/latest/angular/plugins/nest/overview]()
- Nx (for CMS/Web developers): [https://nx.dev/latest/angular/getting-started/getting-started]()
- All documents from `docs/conventions` directory
- Documents from `docs/recipes` directory related to your development

## Known issues

### WARNING in ... due of "failed exports"

TypeScript 3.8 introduced typed imports feature.

TL;DR: Import types with `import type { ... } from '...'` and import with common import statements constants and functions.

Read here:

- [https://medium.com/javascript-in-plain-english/leveraging-type-only-imports-and-exports-with-typescript-3-8-5c1be8bd17fb]()
- [https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-8.html#type-only-imports-and-export]()

### Local development

```shell script
docker-compose up
yarn cli:typeorm:migrate
yarn api:start

# For CMS
yarn cms:start

# For WEB
yarn web:start
```

### API integration tests

1) `docker-compose -f docker-compose.e2e-dev.yml up`
2) `yarn api:e2e`
3) `yarn api:tests:integration-tests`

### CMS & WEB cypress E2E tests

1) `docker-compose -f docker-compose.e2e-dev.yml up`
2) `yarn api:e2e`
3) `yarn cms:e2e` or `yarn web:e2e`

### Custom configurations for CMS/WEB

If you need to use your own configuration (for example, you may need to use Stage API instead of local), you should create `environment.local.ts` environment
and run `yarn cms:custom` / `yarn web:custom` commands.

## Useful CLI tools

### TypeORM utils

Synchronize (update schema):

```
yarn cli:typeorm:synchronize
```

Run migrations:

```
yarn cli:typeorm:migrate
```

### Redis utils

Flush redis:

```
yarn cli:redis:flush
```


## Kafka utils

Setup topics for Kafka:

```
yarn cli:kafka:bootstrap-topics
```
