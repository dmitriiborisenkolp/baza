# Tutorial

This series of articles covers common development flow with API and CMS. Web implementation is not covered with Tutorial; You will provide only Data Access services for Web usage.

We will create simple catalog of items for some RPG game. We are going to implement API for Item Categories and Items itself, with CMS & Public API and CMS over it.

Tutorial covers  following topics:

- Generic API development flow
- Using CrudService and CrudSortService for display lists and sorting items
- Securing CMS endpoints with ACL
- Providing API documentation for CMS / Web
- Using Migrations to provide example data
- E2E testing
