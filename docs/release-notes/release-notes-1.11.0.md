# Release 1.11.0

We're moved to another version schema for Baza release. Most releases bundles with breaking changes, and we decided to increase MINOR version instead of PATCH version for every release. Also, starting this release we are mentioning JIRA tickets, PRs and changes related to IVP.

## What's new?

-   UsedCound now works correctly with Referral codes
-   New Dwolla services available from `baza-dwolla` packages. Dwolla API is mostly covered with Nest.JS services & TypeScript typings.
-   Listings can be sorted now with CMS
-   Purchase Flow & many more APIs should now correctly works with Transaction Fee. If any possible rounding issue may happens with some specific combination parameters such as Unit Price, Transaction Fee or anything else, CMS will not allow to create Listing. It will allow us to detect possible issues & prevent issues on live projects.
-   "Confirm Password" optional field added to Create / Update Account form.
-   "Reset all password" feature is now optional and it's disabled by default.
-   CMS: Fixed and issue with re-selecting previosly selected image in file dialog.

## Breaking changes

-   Referral and Invite codes are placed now under different `psql` tables. If you would like to migrate data, you can remove new tables and rename previous tables after executing update.
-   Invite Code and Referral Code APIs now uses different endpoints
-   `BAZA_NC_INTEGRATION_API_ENDPOINTS_BLACKLIST` exported constant is not available anymore. Before this constants contains `registerAccount` endpoint which should be before excluded from public API usage. With new changes around Invite/Referral codes, it's not actual and `BAZA_NC_INTEGRATION_API_ENDPOINTS_BLACKLIST` is completely removed.

## Invite & Referral code changes

Invite & Referral code APIs moved from `baza-nc-integration` library to `baza-core`. Target projects which are using Invite/Referral code features should make updates in Mobile / Web applications to reflect changes.

-   Invite Code and Referral Code API & CMS moved to baza-core packages
-   Added new registerAccountOptions endpoint for Baza Account API. It works in same way as it was in Baza NC Integration, but also could be called for authenticated user to show referral code / referry copy-paste text.
-   Register Account moved to CQRS layer. It was required because of cross-dependencies all around

## Baza Repository

-   Node updated from 12 to 14. All projects which are using Baza should use Node 14+.
-   We're added Prettier integration.
-   Add retention period to 1 day for build artifacts in CI
-   All PRs are covered with PR Checklist items
-   NPM dependencies are updated. Previosly Baza had incorrect dependencies, mostly Angular and class-validator dependencies.
-   Baza migrated from CCI to GitHub actions!

## Baza-Skeleton and Baza-IVP

-   Baza Skeleton repository reverted to use only baza-core libraries.
-   Baza IVP new repository is now a place where IVP & IVP demo is placed

## PRs

-   https://github.com/scalio/baza/pull/627
-   https://github.com/scalio/baza/pull/625
-   https://github.com/scalio/baza/pull/624
-   https://github.com/scalio/baza/pull/621
-   https://github.com/scalio/baza/pull/619
-   https://github.com/scalio/baza/pull/618
-   https://github.com/scalio/baza/pull/617
-   https://github.com/scalio/baza/pull/616
-   https://github.com/scalio/baza/pull/615
-   https://github.com/scalio/baza/pull/614
-   https://github.com/scalio/baza/pull/613
-   https://github.com/scalio/baza/pull/612
-   https://github.com/scalio/baza/pull/611
-   https://github.com/scalio/baza/pull/609
-   https://github.com/scalio/baza/pull/608
-   https://github.com/scalio/baza/pull/607
-   https://github.com/scalio/baza/pull/606
-   https://github.com/scalio/baza/pull/603
-   https://github.com/scalio/baza/pull/602
-   https://github.com/scalio/baza/pull/601
-   https://github.com/scalio/baza/pull/600
-   https://github.com/scalio/baza/pull/599
-   https://github.com/scalio/baza/pull/598
-   https://github.com/scalio/baza/pull/597
-   https://github.com/scalio/baza/pull/596
-   https://github.com/scalio/baza/pull/595
-   https://github.com/scalio/baza/pull/593
-   https://github.com/scalio/baza/pull/586
-   https://github.com/scalio/baza/pull/584
-   https://github.com/scalio/baza/pull/583
-   https://github.com/scalio/baza/pull/582
-   https://github.com/scalio/baza/pull/581
-   https://github.com/scalio/baza/pull/579
-   https://github.com/scalio/baza/pull/578
-   https://github.com/scalio/baza/pull/577
-   https://github.com/scalio/baza/pull/576
-   https://github.com/scalio/baza/pull/575
-   https://github.com/scalio/baza/pull/574
-   https://github.com/scalio/baza/pull/573
-   https://github.com/scalio/baza/pull/561
-   https://github.com/scalio/baza/pull/563
-   https://github.com/scalio/baza/pull/564
-   https://github.com/scalio/baza/pull/566
-   https://github.com/scalio/baza/pull/567
-   https://github.com/scalio/baza/pull/569
-   https://github.com/scalio/baza/pull/570
-   https://github.com/scalio/baza/pull/571
-   https://github.com/scalio/baza/pull/572

## JIRA

-   [BAZA-353](https://scalio.atlassian.net/browse/BAZA-353) Add a skeleton web-app into baza-skeleton
-   [BAZA-434](https://scalio.atlassian.net/browse/BAZA-434) Disable reset all passwords feature
-   [BAZA-463](https://scalio.atlassian.net/browse/BAZA-463) CMS -> Add/Edit user - add Password confirmation field
-   [BAZA-509](https://scalio.atlassian.net/browse/BAZA-509) Update baza-skeleton package dependencies
-   [BAZA-703](https://scalio.atlassian.net/browse/BAZA-703) Baza-Dwolla Gateway service for Dwolla Accounts API
-   [BAZA-704](https://scalio.atlassian.net/browse/BAZA-704) Baza-Dwolla Gateway service for Dwolla Customers API
-   [BAZA-705](https://scalio.atlassian.net/browse/BAZA-705) Baza-Dwolla Gateway service for Dwolla Transfers API
-   [BAZA-706](https://scalio.atlassian.net/browse/BAZA-706) Baza-Dwolla Gateway service for Dwolla FundingSources API
-   [BAZA-707](https://scalio.atlassian.net/browse/BAZA-707) Baza-Dwolla Gateway service for Dwolla MassPayments API
-   [BAZA-803](https://scalio.atlassian.net/browse/BAZA-803) Move Invite Code and Referral Code to baza-core package
-   [BAZA-846](https://scalio.atlassian.net/browse/BAZA-846) Re-selecting previously selected image will not trigger file dialog
-   [BAZA-869](https://scalio.atlassian.net/browse/BAZA-869) Rename sandbox app to web, replace sandbox- prefix for libraries to web- and migrate sandbox app to Baza IVP
-   [BAZA-875](https://scalio.atlassian.net/browse/BAZA-875) Used Count for Referral Codes is not incrementing when user signing up using Referral Code
-   [BAZA-836](https://scalio.atlassian.net/browse/BAZA-836) Decimal Transaction Fee causes 500 on starting /session

## JIRA (IVP)

-   [BAZA-912](https://scalio.atlassian.net/browse/BAZA-912) Update bank popup has different font colour when opened from payment methods versus opened from Submit Payment
-   [BAZA-914](https://scalio.atlassian.net/browse/BAZA-914) The yellow alert icon does not have the same width and height as per the zeplin designs
-   [BAZA-915](https://scalio.atlassian.net/browse/BAZA-915) The yellow icon only starts displaying after the user clicks on payment method tab, not before
-   [BAZA-832](https://scalio.atlassian.net/browse/BAZA-832) Updating portfolio menu transaction table with new colors, icons and dividends information.
-   [BAZA-827](https://scalio.atlassian.net/browse/BAZA-827) Create application level alert message / banner on top of the screen if bank was not assigned.
-   [BAZA-828](https://scalio.atlassian.net/browse/BAZA-828) Payment method step should have the icon if there is no bank account associated
-   [BAZA-829](https://scalio.atlassian.net/browse/BAZA-829) Yellow box message "Connect with a checking or savings account to receive dividends"
-   [BAZA-830](https://scalio.atlassian.net/browse/BAZA-830) Add label "Your Bank Account will be utilized to receive dividends" in gray on purchase flow and same message on payment method screen (if a bank account was assigned)
-   [BAZA-831](https://scalio.atlassian.net/browse/BAZA-831) Add label "Your Bank Account will be utilized to receive dividends" in gray on purchase flow and same message on payment method screen (if a bank account was assigned)
-   [BAZA-879](https://scalio.atlassian.net/browse/BAZA-879) User Experience improvements on Account
-   [BAZA-867](https://scalio.atlassian.net/browse/BAZA-867) Opacity of the Gallery backgroung
-   [BAZA-865](https://scalio.atlassian.net/browse/BAZA-865) Purchase Flow Step 2 - Sign Agreement
-   [BAZA-877](https://scalio.atlassian.net/browse/BAZA-877) Listing Image issues on Purchase Flow in mobile and tablet resolutions
-   [BAZA-871](https://scalio.atlassian.net/browse/BAZA-871) UI Adjustments - Back to top button in site
-   [BAZA-876](https://scalio.atlassian.net/browse/BAZA-876) Fix page titles across application
-   [BAZA-500](https://scalio.atlassian.net/browse/BAZA-500) Sandbox - Emails text
-   [BAZA-872](https://scalio.atlassian.net/browse/BAZA-872) Listing Details - Incorrect Financials info for tablet and mobile resolutions
-   [BAZA-861](https://scalio.atlassian.net/browse/BAZA-861) My favorites page
-   [BAZA-855](https://scalio.atlassian.net/browse/BAZA-855) Auth Flow - Unverified email modal differs from Zeplin designs
-   [BAZA-853](https://scalio.atlassian.net/browse/BAZA-853) Layout - Footer fixing
-   [BAZA-830](https://scalio.atlassian.net/browse/BAZA-830) Add label "Your Bank Account will be utilized to receive dividends" in gray on purchase flow and same message on payment method screen (if a bank account was assigned)
-   [BAZA-831](https://scalio.atlassian.net/browse/BAZA-831) Update Bank Account pop up message triggered from EDIT on Payment Methods tab should have a new copy
