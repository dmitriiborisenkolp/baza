# Release 1.9.6

## Critical issues fixed

-   Fixed: HTTP Interceptor was redirecting even on non-auth errors; basically any error from API was leading user to sign in page

## What's new in release

-   Change Email feature for CMS
-   Change Email additional integrations for baza-nc (Account and Party will be automatically updated on NC side with email updates)
-   Huge changes over guards & http interceptors with more flexible configuration options

## Deprecated

-   `JwtRequireUserGuard` and `JwtRequireAdminGuard` from `baza-core-ng` package

## Possible regressions

-   Regressions can be in JWT guards which affects CMS / Web Sign In, Sign Up & JWT session. It's unlikely to happen, but there was a lot of work here about guards.

## PR's

You can find some additional nodes in PR's description:

-   https://github.com/scalio/baza/pull/46
-   https://github.com/scalio/baza/pull/47
-   https://github.com/scalio/baza/pull/48 (Detailed description about guards here)
-   https://github.com/scalio/baza/pull/49
-   https://github.com/scalio/baza/pull/50
