# Release 1.10.7

**What's new:**

-   AWS Strict Mode: AWS strict mode will check AWS resources for containing directory string is S3 Object Id. If AWS Strict Mode is disabled, API will not check for directory. By default AWS Strict Mode is enabled and can be disabled with `AWS_STRICT=0`
-   Environment service reworked: `get` method is deprecated now and will be removed in next breaking change release (1.11.0). Use `getAsBoolean`, `getAsString`, `getAsNumber` or new `getAsEnum` methods to fetch environment values. Additionally, Environment service now works properly with `defaultValue` option.
-   `DB_AUTO_DROP_SCHEMA` environment variable is optional now
-   A lot of issues with ACL fixed, especially in CMS.

**Baza NC Integration:**

A new Schema API & CMS feature added to this release. New upcoming integration with Listings will allow to add & customize list of custom properties for Listings.

-   baza-nc-integration: Added Subscription API & CMS
-   baza-nc-integration: Integration tests coverage added for most API's (Account, Invite Code, Referral Code, G1)
-   baza-nc-integration: Invite Code & Referral Code feature could be actually used now (Invite Code was disabled before due of CMNW changes)
-   baza-nc-integration: Added Registry configuration which allows/denies account registrations with Invite Code, Referral Code or w/o Invite/Referral codes
-   baza-nc-integration: Fixed issues with G1 club (users now properly adds to G1 clubs)

**JIRA:**

-   https://scalio.atlassian.net/browse/BAZA-45 Baza Sentry Integration
-   https://scalio.atlassian.net/browse/BAZA-386 Exception on module initialisation so app wont start
-   https://scalio.atlassian.net/browse/BAZA-321 Vuln 4:Lack of ACL on admin actions
-   https://scalio.atlassian.net/browse/BAZA-323 Vuln 6:Access to S3 objects
-   https://scalio.atlassian.net/browse/BAZA-356 Implement FE interceptor(redirect) for feature-maintenance
-   https://scalio.atlassian.net/browse/BAZA-373 Add Subscriptions endpoint to baza-integration package

**PRs:**

-   https://github.com/scalio/baza/pull/174
-   https://github.com/scalio/baza/pull/173
-   https://github.com/scalio/baza/pull/172
-   https://github.com/scalio/baza/pull/171
-   https://github.com/scalio/baza/pull/170
-   https://github.com/scalio/baza/pull/169
-   https://github.com/scalio/baza/pull/163
-   https://github.com/scalio/baza/pull/162
-   https://github.com/scalio/baza/pull/158
