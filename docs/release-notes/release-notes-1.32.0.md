# Release 1.32.0

This release adds the ability to withdraw funds through standalone components. We also made a lot of refactoring around web libraries and styles, proceeding with migrating codebase to TypeScript Strict rules and fixed minor issues with Master Password feature.

## What's new

-   `baza-nc-api`: session endpoints will correctly handle data de-sync cases with `ncRemainingShares` field. When there are not enough shares, API will respond with `BazaNcPurchaseFlowErrorCodes.NotEnoughShares` error code (default message is `Offering doesn`t have enough shares`. You can override with common tools or changing `bazaNcPurchaseFlowErrorCodesI18n[BazaNcPurchaseFlowErrorCodes.NotEnoughShares]` value
-   `baza-core-api`: Master Password feature will work correctly in multi-node environments. We also changed slightly how this feature will work
-   `baza-core-api`: Master Password will automatically expire & disables after 1 hour. If you would like to use it more, you should go to CMS and re-enable it once more
-   `baza-core-cms`: `updated the `upload-image`and`upload-image-multi` components in order to display the notification for error messages coming from the server whenever the upload service is called.
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-purchase-flow`: fixed the issue reported in PF (both Dwolla and NC libraries) where if DocuSign form is displayed within iFrame popup and user clicks on "Back" button, 409 session lock issue is thrown in console.
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-purchase-flow`: added the ability to withdraw funds through standalone components and also adds code refactoring within Dwolla Account components.
    -   Added new withdraw parent standalone component which can be used to create withdraw trigger and invoke popup modal for withdraw
    -   The withdrawal flow can be integrated by 1 line of code, in any place within target project application
    -   Created base i18n configurations and implemented in all places
    -   Integrated `BazaNcWithdrawalDataAccess` in Dwolla PF store, created new actions/handlers and interfaces for managing withdraw API calls
    -   Updated the behavior subject for requesting manual refresh of bootstrap data at application level
    -   Added ability for target projects to handle enable/disable state of withdraw trigger element
    -   Added and refactored interfaces for Dwolla payment methods section (Account)
-   `baza-web-ui-components`: created new components for withdraw form, modal and notification components respectively
-   `baza-web-ui-components`: added new i18n configurations and created differentiation between "Add Funds" and "Withdraw Funds" by modifications in i18n file/folder structure
-   `baza-dwolla-web-verification-flow`: added new input property to `VerificationInvestorComponent` to handle bootstrap data input from application level
-   `baza-dwolla-web-verification-flow`: updated checks to show / hide Dwolla consent checkbox and associated form validations

-   `baza-dwolla-web-purchase-flow`: removed components for payment bank account / modal and payment card / modal, updated module references and i18n translation configurations
-   `baza-dwolla-web-purchase-flow`: removed `payment-edit-update` component as it was not being used, updated i18n configuration
-   `baza-web-ui-components`: added components for payment bank account / modal and payment card / modal
-   `baza-web-ui-components`: updated i18n translations for new components
-   `baza-nc-api`: `formResources` endpoint will not trigger Investor Account creation for new users
-   `baza-core-ng`: fixed maintenance guard redirection in case the maintenance mode is off. Now it redirects to the root URL.
-   `baza-nc-cms`: fixed validation message for End Date field
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-purchase-flow`, `baza-web-ui-components`: this release refactors the classes being used on the components: `AccountBalanceCardComponent`, `PaymentBankAccountComponent`, `PaymentBankAccountComponent`, `PaymentCardComponent`, and `PaymentEditListComponent`, in order to be able to differentiate and specify different styles for each component.
    -   Multiple classes were renamed, created, and removed.
    -   The `PaymentItemComponent` now has two states: linked and unlinked. When the state is linked, the element is a button with the class `payment-item-btn`, otherwise, it is a div with the class `payment-item-section`.
    -   The `PaymentBankAccount` and `PaymentCard` components also have two different classes depending on if the state is "addMode" (`payment-purchase-btn`) or "editMode" (`payment-purchase-section`)
    -   The `AccountBalanceCard` component also has two different classes depending on if the state is "addMode" (`balance-btn`) or "editMode" (`balance-section`)
    -   The classes in the `PaymentListComponent` were updated to `payment-list-btn-*` variants but the elements remained untouched.
    -   New stylesheets were added to the project, some were modified and removed (more on migration guide section).
    -   **Note:** the changes listed above were only applied to `baza-dwolla-web-purchase-flow` and `baza-web-ui-components` libraries. Everything about NC remains the same.

## Bugfixes

-   `baza-web-utils`: added a NULL check when creating link configurations
-   `baza-dwolla-web-verification-flow`: Fixed the i18n key for File Upload Popup (Dwolla VF) component

## Breaking changes

-   The interface `AccBalanceConfig` has now been updated. Note that it's an **optional** interface to define custom email link when using `dwolla-payment-methods` selector and following is the updated structure for it. Basically, the additional 3 boolean properties are now removed:

```typescript
const defaultAccBalanceConfig: AccBalanceConfig = {
    links: {
        contact: {
            extLink: {
                link: 'bazaContentTypes.contacts.email',
                text: 'contact us',
                isCMSLink: true,
                isMailLink: true,
            },
        },
    },
};
```

-   There's now an interface `DwollaPaymentMethodsConfig` available which can be passed when using `dwolla-payment-methods` as selector in target projects (optional interface). It's just to control showing diff. sections within payment methods i.e. account balance, NC ACH bank account and CC sections. By default, all values are true i.e. all sections are displayed. Please note, previously used input parameters are updated as following new structure:

```typescript
const defaultPaymentMethodsConfig: DwollaPaymentMethodsConfig = {
    showAccountBalance: true,
    showBankAccount: true,
    showCreditCard: true,
};
```

-   If target projects want to override i18n files **for web-ui components**, please now follow the updated **sample** structure as follows, as we now have "addFunds" and "withdrawFunds" objects in structure, to differentiate between "Add Funds" and "Withdraw Funds":

```typescript
export const BazaWebUiEnI18nOverride = {
    uic: {
        balance: WebUiAccountBalanceCardEnI18nOverride,
        summary: WebUiSummaryEnI18nOverride,
        bankDetails: WebUiBankDetailsEnI18nOverride,
        cardDetails: WebUiCardDetailsEnI18nOverride,
        personal: WebUiPersonalInfoEnI18nOverride,
        addFunds: {
            form: WebUiAddFundsFormEnI18nOverride,
            modal: WebUiAddFundsModalEnI18nOverride,
            notification: WebUiAddFundsNotificationEnI18nOverride,
        },
        withdrawFunds: {
            form: WebUiWithdrawFundsFormEnI18nOverride,
            modal: WebUiWithdrawFundsModalEnI18nOverride,
            notification: WebUiWithdrawFundsNotificationEnI18nOverride,
        },
    },
};
```

**Note:**
We only need to provide override i18n structure for components which we want to override, it's not a must to define all of them.

---

-   We have moved 4 new components from Dwolla PF to Web-UI components library, hence `BazaWebUiEnI18n` i18n config has been updated. If overrides are required, target projects can use the following sample structure:

```typescript
export const BazaWebUiEnI18nOverride = {
    uic: {
        balance: WebUiAccountBalanceCardEnI18nOverride,
        summary: WebUiSummaryEnI18nOverride,
        paymentBank: DwollaPaymentBankAccountEnI18nOverride, // new config
        paymentBankModal: DwollaPaymentBankAccountModalEnI18nOverride, // new config
        bankDetails: WebUiBankDetailsEnI18nOverride,
        paymentCard: DwollaPaymentCardEnI18nOverride, // new config
        paymentCardModal: DwollaPaymentCardModalEnI18nOverride, // new config
        cardDetails: WebUiCardDetailsEnI18nOverride,
        personal: WebUiPersonalInfoEnI18nOverride,
        addFunds: {
            form: WebUiAddFundsFormEnI18nOverride,
            modal: WebUiAddFundsModalEnI18nOverride,
            notification: WebUiAddFundsNotificationEnI18nOverride,
        },
        withdrawFunds: {
            form: WebUiWithdrawFundsFormEnI18nOverride,
            modal: WebUiWithdrawFundsModalEnI18nOverride,
            notification: WebUiWithdrawFundsNotificationEnI18nOverride,
        },
    },
};
```

**Notes:**

-   The `paymentBank` i18n config defines the payment bank account details section and `paymentCard` defines the payment card details section displayed in Dwolla PF -> Payment Page (Methods Section) respectively

-   The `paymentBankModal` and `paymentCardModal` are shared components that are displayed in Dwolla PF and Dwolla Account components, used to add/update bank and card details respectively in popup modal.

---

-   The `VerificationInvestorComponent` component now receives an additional input property `initData$` which is supposed to have bootstrap data passed from application level.

**Please follow the below-mentioned guide:**

-   Include `BootstrapResolver` on Dwolla Verification page route

-   Now, update the verification custom component code as follows to enable passing `initData$` as input parameter to `<app-verification-investor-dwolla>` component:

**Verification Custom Component - HTML**

```html
<app-verification-dwolla [config]="config">
    <app-verification-info-dwolla
        *ngIf="currentTab === 0"
        target="info"></app-verification-info-dwolla>
    <app-verification-investor-dwolla
        *ngIf="currentTab === 1"
        target="investor"
        [initData$]="initData$"></app-verification-investor-dwolla>
</app-verification-dwolla>
```

**Verification Custom Component - Code Behind**

```typescript
export class VerificationCustomComponent {

    // add this line of code for manual refresh of init data - requested from library
    this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe(() => {
        this.store.dispatch(new RequestAppBootstrapInitData());
    });
}
```

## Migration

If you're using the following components in your codebase, note that there are some breaking changes in the latest update. Here's what you need to know:

### DwollaMethodsComponent

-   `purchase__title` has been renamed to `purchase__subheading`
-   `purchase__account-balance` has been renamed to `purchase__block`
-   `purchase__bank-account` has been renamed to `purchase__block`
-   `purchase__credit-card` has been renamed to `purchase__block`

### AccountBalanceCardComponent

-   `payment-btn` has been renamed to `balance-btn`
-   `payment-btn__title` has been renamed to `balance-btn__title`
-   `payment-btn__arrow` has been renamed to `payment-arrow`
-   `payment-btn-row` has been renamed to `payment-actions`
-   `payment-btn__hint` has been renamed to `balance-btn__hint`
-   `payment-btn__title-bold` has been removed
-   `payment-btn__title-ellipsis` has been removed
-   `balance-section` is a new class that has been added
-   `balance-section__title` is a new class that has been added

### PaymentBankAccountComponent

-   `payment-btn` has been renamed to `payment-buy-btn`
-   `payment-btn-bordered` has been renamed to `payment-buy-btn-bordered`
-   `payment-btn__icon` has been renamed to `payment-buy-btn__icon`
-   `payment-btn__title` has been renamed to `payment-buy-btn__title`
-   `payment-btn__arrow` has been renamed to `payment-arrow`
-   `purchase__personal-info` has been renamed to `payment-buy-info`
-   `payment-info-list` is a new class that has been added
-   `payment-info-list__item` is a new class that has been added

### PaymentCardComponent

-   `payment-btn` has been renamed to `payment-buy-btn`
-   `payment-btn-bordered` has been renamed to `payment-buy-btn-bordered`
-   `payment-btn__icon` has been renamed to `payment-buy-btn__icon`
-   `payment-btn__title` has been renamed to `payment-buy-btn__title`
-   `payment-btn__fee` has been renamed to `payment-buy-btn__fee`
-   `payment-btn__arrow` has been renamed to `payment-arrow`
-   `payment-buy-btn__desc` is a new class that has been added
-   `payment-buy-section` is a new class that has been added
-   `payment-buy-section-bordered` is a new class that has been added
-   `payment-buy-section__icon` is a new class that has been added
-   `payment-buy-section__icon-dwolla` is a new class that has been added
-   `payment-buy-section__title` is a new class that has been added
-   `payment-buy-section__expiration` is a new class that has been added
-   `payment-buy-section__desc` is a new class that has been added
-   `payment-buy-info` is a new class that has been added
-   `payment-info-list` is a new class that has been added
-   `payment-info-list__item` is a new class that has been added
-   `payment-buy-info__hint` is a new class that has been added

### PaymentEditListComponent

-   `payment-btn` has been renamed to `payment-list-btn`
-   `payment-btn-inverse` has been renamed to `payment-list-btn-inverse`
-   `payment-btn__title` has been renamed to `payment-list-btn__title`
-   `payment-btn__subheading` has been renamed to `payment-list-btn__subheading`
-   `payment-btn__arrow` has been renamed to `payment-arrow`
-   `payment-btn-inactive` has been renamed to `payment-list-btn-inactive`
-   `payment-btn__fee` has been renamed to `payment-list-btn__fee`
-   `payment-btn__info-descr` has been renamed to `payment-list-btn__description`
-   `payment-btn__warning` has been renamed to `payment-list-btn__warning`

### PersonalInfoComponent

-   `purchase__title` has been renamed to `personal__title`
-   `purchase__icon` has been renamed to `personal__icon`
-   `personal` has been renamed to `personal-list`
-   `personal__item` has been renamed to `personal-list__item`
-   `personal__item-uploaded-document` has been renamed to `personal-list__item-uploaded-document`
-   `personal` is a new class that has been added as a parent class

### Stylesheets

The following stylesheets were added, removed, and modified:

#### DwollaMethodsComponent

-   `_purchase.less` was modified

#### PaymentItemComponent

-   `_payment-item-btn.less` was added
-   `_payment-item-section.less` was added
-   `_payment-item-content.less` was added
-   `_payment-item-details.less` was added
-   `_payment-item-actions.less` was added
-   `_payment-item.less` was removed
-   `_payment-item-collapse.less` was modified

#### AccountBalanceCardComponent

-   `_balance-btn.less` was added
-   `_balance-section.less` was added

#### PaymentBankAccountComponent, PaymentCardComponent

-   `_payment-buy-btn.less` was added
-   `_payment-buy-section.less` was added
-   `_payment-buy-info.less` was added
-   `_payment-info-list.less` was added

#### PaymentEditListComponent

-   `_payment-list-btn.less` was added

#### PersonalInfoComponent

-   `_personal.less` was modified

#### Shared

-   `_payment-actions.less` was added
-   `_payment-arrow.less` was added
-   `_payment-subtitle.less` was modified

### Note on `payment-btn-inverse` variant

If you were using the class `payment-btn-inverse` to control the theming for the "Edit Payment" modal dialogs and show text on top of light/dark backgrounds, you need to update the class to `payment-list-btn-inverse`

### New inverse modifiers

New inverse modifiers were added for the following classes:

`payment-item-section` -> `payment-item-section-inverse`
`payment-item-btn` -> `payment-item-btn-inverse`

`balance-section` -> `balance-section-inverse`
`balance-btn` -> `balance-btn-inverse`

`payment-buy-section` -> `payment-buy-section-inverse`
`payment-buy-btn` -> `payment-buy-btn-inverse`

`payment-list-btn` -> `payment-list-btn-inverse`

### Note on legacy NC purchase flow

If your project is using legacy NC PF, please make sure to update the following classes:

-   `payment-buy-info` is a new class that has been added as a parent class
-   `personal` has been renamed to `payment-info-list`
-   `personal__item` has been renamed to `payment-info-list__item`

### Note on Methods component

As part of this PR, the Dwolla component `PaymentEditUpdateComponent` was removed and that also includes its corresponding i18n resource. If you were modifying any caption for this specific component, have in mind that your override configuration won't have any effect.

## Migration – Withdraw Funds

-   Please following this documentation guide on understanding how to migrate and use "Withdraw Funds" feature in your target project:
    https://baza-docs.test.scaliolabs.com/web-development/exportable-features/withdraw-funds/01-index

## JIRA

-   [BAZA-1409](https://scalio.atlassian.net/browse/BAZA-1409) BE: Ability to filter schema name on client apps
-   [BAZA-1848](https://scalio.atlassian.net/browse/BAZA-1848) Refactor Shared Logic Between Account & Dwolla Purchase Flow Library
-   [BAZA-1527](https://scalio.atlassian.net/browse/BAZA-1527) CMS: Create Regression plan in TestRail for CMS
-   [BAZA-1733](https://scalio.atlassian.net/browse/BAZA-1733) Add ability to Withdraw from Account Balance (on the UI)
-   [BAZA-1736](https://scalio.atlassian.net/browse/BAZA-1736) COL - New User Sign Up: User navigates to Account << Investor Info for first time, error is displayed in console
-   [BAZA-1751](https://scalio.atlassian.net/browse/BAZA-1751) CW - BE: /baza-nc/purchase-flow/session responses with HTTP Status Code 500 for certain offering/listing/horse
-   [BAZA-1770](https://scalio.atlassian.net/browse/BAZA-1770) Refactor "payment-btn" styles to be used in multiple places without conflict
-   [BAZA-1801](https://scalio.atlassian.net/browse/BAZA-1801) Migrate baza-nc-web-verification-flow and baza-dwolla-web-verification-flow to strict rules
-   [BAZA-1802](https://scalio.atlassian.net/browse/BAZA-1802) Migrate sandbox-web library to strict rules
-   [BAZA-1804](https://scalio.atlassian.net/browse/BAZA-1804) Migrate sandbox, sandbox-e2e, sandbox-web-marketing and sandbox-web-marketing-e2e applications to strict rules
-   [BAZA-1805](https://scalio.atlassian.net/browse/BAZA-1805) Enable strict rules by default when generating a new library using Nx generator tools
-   [BAZA-1806](https://scalio.atlassian.net/browse/BAZA-1806) Migrate baza-core-cms library to strict rules
-   [BAZA-1807](https://scalio.atlassian.net/browse/BAZA-1807) Migrate baza-content-types-cms library to strict rules
-   [BAZA-1808](https://scalio.atlassian.net/browse/BAZA-1808) Migrate baza-dwolla-cms library to strict rules
-   [BAZA-1809](https://scalio.atlassian.net/browse/BAZA-1809) Migrate baza-nc-cms library to strict rules
-   [BAZA-1810](https://scalio.atlassian.net/browse/BAZA-1810) Migrate baza-nc-integration-cms library to strict rules
-   [BAZA-1811](https://scalio.atlassian.net/browse/BAZA-1811) Migrate cms and cms-e2e applications to strict rules
-   [BAZA-1813](https://scalio.atlassian.net/browse/BAZA-1813) Migrate baza-content-types-api library to strict rules
-   [BAZA-1814](https://scalio.atlassian.net/browse/BAZA-1814) Migrate baza-dwolla-api and baza-plaid-api libraries to strict rules
-   [BAZA-1836](https://scalio.atlassian.net/browse/BAZA-1836) Transactions in our CMS are not synced with North Capital TransactAPI
-   [BAZA-1854](https://scalio.atlassian.net/browse/BAZA-1854) BAZA Doc Portal - Link Scalio logo to first doc in portal
-   [BAZA-1857](https://scalio.atlassian.net/browse/BAZA-1857) \[CMS\] "enabled" master password not working for CMS and Web
-   [BAZA-1863](https://scalio.atlassian.net/browse/BAZA-1863) CMS - Add Listing - No error message displayed when unsupported format of image is uploaded
-   [BAZA-1881](https://scalio.atlassian.net/browse/BAZA-1881) Collab: Purchase Flow: Error Appears on Cancelling Purchase Flow from Docu Sign Step
-   [BAZA-1882](https://scalio.atlassian.net/browse/BAZA-1882) Implement the Withdraw flow on the FE\*\*\*\*
-   [BAZA-1883](https://scalio.atlassian.net/browse/BAZA-1883) Add Withdraw flow test cases to the Regression suite
-   [BAZA-1900](https://scalio.atlassian.net/browse/BAZA-1900) CW - The Dwolla consent check box is not saved based on the prev selection
-   [BAZA-1901](https://scalio.atlassian.net/browse/BAZA-1901) Refactor Data Streams in Dwolla Account Components
-   [BAZA-1902](https://scalio.atlassian.net/browse/BAZA-1902) Refactor Store Logic and Move Dwolla Account Components to Web UI Components Library
-   [BAZA-1904](https://scalio.atlassian.net/browse/BAZA-1904) Form Resources endpoint should not trigger Investor Account creation
-   [BAZA-1913](https://scalio.atlassian.net/browse/BAZA-1913) Update Release Notes for 1.31.0 version as per new format
-   [BAZA-1932](https://scalio.atlassian.net/browse/BAZA-1932) translations for the upload file component in dwolla verification flow uses NC translations
-   [BAZA-1933](https://scalio.atlassian.net/browse/BAZA-1933) Clean up BAZA codeowners file
-   [BAZA-1936](https://scalio.atlassian.net/browse/BAZA-1936) PTCL - Maintenance: 503 error loop on Web when Maintenance mode is enabled
-   [BAZA-1937](https://scalio.atlassian.net/browse/BAZA-1937) CMS: Listings: Validation message is not correct when Listing End date is greater than start date

## PRs

-   https://github.com/scalio/baza/pull/1342
-   https://github.com/scalio/baza/pull/1343
-   https://github.com/scalio/baza/pull/1242
-   https://github.com/scalio/baza/pull/1308
-   https://github.com/scalio/baza/pull/1341
-   https://github.com/scalio/baza/pull/1303
-   https://github.com/scalio/baza/pull/1300
-   https://github.com/scalio/baza/pull/1314
-   https://github.com/scalio/baza/pull/1364
-   https://github.com/scalio/baza/pull/1357
-   https://github.com/scalio/baza/pull/1344
-   https://github.com/scalio/baza/pull/1367
-   https://github.com/scalio/baza/pull/1378
-   https://github.com/scalio/baza/pull/1379

```

```
