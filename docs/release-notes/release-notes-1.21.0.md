# Release 1.21.0

This a development-preview release which is introduction Dwolla Transfer (Cash-In), Purchasing Shares using Account Balance features plus huge amount of
additional changes and bugfixes. We're currently completing Dividends v3 scope both BE and FE side and some potential issues will be fixed with next Baza 1.22.0 release.

### What's new

-   `baza-nc`: Purchase Flow now supports purchasing shares using Dwolla Customer Balance. Check `Purchase Flow - Account Balance` section for details.
-   `baza-nc`: New Dwolla Transfer Feature added which is allowing investors to add funds to Dwolla. Check `Dwolla Transfer Feature` section for details.
-   `baza-dwolla-cms`: New library added with optional Dwolla Payments section
-   `baza-nc-integration`: Search API received multiple fixes around related updating data & URLs.
    -   Search API now updates Listings data with Mappers before submitting result to user (it fixes most of issues with updates, account-related fields, TTL expiration any many more)
    -   Added more event handlers for various operations to properly update Listings in Search Index (Trade Status Update, Percents Funded Update, Purchase Flow Submitted)
    -   Added "Reindex" and "Reindex all Listings" options in Listings CMS to allow manually update Listing in Search Index
    -   Added more integration tests to cover mentioned cases
-   `baza-nc-web-utils`: Added new `mediaEmbed` pipe to work with embedded media data. This pipe could be used to display complex data (especially for SSR) and can be used as replacement for CKEditor.
-   `baza-nc`: Email notifications about failed Dwolla Dividend payments now also includes Failure Reason
-   `baza-dwolla-web-purchase-flow`: This release covers removing all mentions & logic of ACH bank accounts from Dwolla flow. Check `Dwolla Flow FE Changes` section for details.
    -   Triggering of Dwolla plaid flow by clicking "Account Balance"
    -   Ability to see & manage "Account Balance" through edit payment popup
    -   Ability to add funds or purchase shares through account balance
    -   Changes in NC KYC error (under discussion as part of [BAZA-1250](https://scalio.atlassian.net/browse/BAZA-1250))
-   `baza-dwolla-web-verification`: Added checkbox addition to get investors to consent to create Dwolla account.
    -   Display a new checkbox to get the investor's consent to create a Dwolla account.
    -   The checkbox is mandatory to submit the form. and showing an error message when validating the form.
    -   Hide the checkbox if the investor already has a Dwolla account.
    -   Once the form is submitted, the endpoint `dwolla/touch` is called to create the Dwolla account.
    -   Check the flag `isDwollaAvailable` to confirm if the investor has Dwolla account or not.
        -   Dwolla consent message was updated.
    -   The logic to show Dwolla consent was refactored to show it only when the investor is national and the Dwolla customer hasn't been created.
    -   When users click on "Terms of Service", it takes them to `bazaCommon.links.termsOfService` (registry)
    -   When users click on "Privacy Policy", it takes them to `bazaCommon.links.privacyPolicy` (registry)
    -   When users click on "Dwolla's Terms of Service", it takes them to https://www.dwolla.com/legal/tos/#legal-content
    -   When users click on "Dwolla's Privacy Policy", it takes them to https://www.dwolla.com/legal/privacy
-   `baza-nc-web-purchase-flow`, `baza-dwolla-web-purchase-flow`: Added ability to customize "Your order has been submitted successfully" screen. Check out Baza Documentation portal for more details: https://baza-docs.test.scaliolabs.com/web-development/customizing-purchase-flow/05-done-page-config
-   `baza-nc-api`: Failure Reason for Dividends received fixes & changes:
    -   Failure reason now will be set for Dividend Transaction Entries
    -   Failure reason will be set for case A) When errors happens before initiating Dwolla Transfer b) After initiating Dwolla Transfer and receiving Webhook Events
    -   Cancelled webhook events will also clean up Failure Reason
    -   It works both for process and re-process methods
-   `baza-nc-api`: It's not possible to configure should be Dwolla Customers created automatically during processing dividends or not with `bazaNcApiConfig({ dwollaAutoTouchCustomers: boolean })` helper
-   `baza-nc-api`: CSV uploads now works properly for Dividend CSV uploads:
    -   When paying dividends with a CSV, the file can work with the masks "0000.00" with no $ sign or "$ 0,000.00", with the comma and does not error out due to wrong formatting
    -   This specific use case is also applicable for North Capital Dividend Uploads (V1) since the formatting also uses currency or numbers
-   `baza-dwolla-web-purchase-flow`: Added Plaid Integration (NBAS) to Dwolla Purchase Flow. Check `Flow Charts of Purchase Flow (FE)` and `Dwolla Plaid Integration (FE)` sections.
-   `baza-dwolla-web-purchase-flow`: NC ACH Flow is removed from Dwolla Purchase Flow
    -   Removing "Link Bank Account" and "Manually Add Bank Details" options from payment page
        -   NOTE: We will add "Manually Add Bank Details" option back with next release
    -   Removal of ACH options from "Edit Payment Method" popup
    -   Removal of any ACH related labels/error messages from payment page
    -   Only showing "Account Balance" and "Credit Card" options in payment page
    -   If an investor doesn't have an existing Dwolla account, upon clicking "Account Balance" /touch endpoint is called. If there are any errors during the operation, they are displayed accordingly
    -   Creation of new NGXS store action & associated logic for calling /touch endpoint on payment page
    -   Adding checks for international investors to ensure "Account Balance" option is not available to them
-   `baza-core-api`: A new `@PrimaryUlidColumn()` decorator added for ULID columns. Existing ULID columns updated with using new decorator
-   `baza-nc`: Added an option to disable Auto Cancelling Failed Trades feature:
    -   New option `enableAutoCancellingFailedTrades` was added to `nc-api-config`.
    -   Field `days to release failed trades` is hidden from listings (offering tab) if `enableAutoCancellingFailedTrades` is set to false.
    -   "Days to wait before releasing a trade with failed transaction" is hidden from the registry if `enableAutoCancellingFailedTrades` is set to false.
-   `baza-nc-api`: Date Updated At and Date Processed At in Dividends will be filed depends on actions (before it wasn't working properly)
-   `baza-core-api`: Added a new option to disable Device Tokens on CMS
    -   New config option `withDeviceTokenFeature` (disabled by default) was added to `BazaCmsBundleConfig`.
    -   Two new methods were added to enable/disable device token feature: `withDeviceTokenFeature()` and `withoutDeviceTokenFeature()`.
    -   Action item "Device tokens" is not displayed on **Accounts** page if feature is disabled.
    -   New guard was created and implemented to validate `device-tokens/:accountId` path.
    -   Portal documentation was updated accordingly: [https://baza-docs.test.scaliolabs.com/overview/36-device-tokens]()

## Bugfixes

-   `baza-nc`: Fixed issues with ZIP code validation on BE side
-   `baza-nc-web-purchase-flow`, `baza-dwolla-web-purchase-flow`: Fixed the critical bug in which after a new user signs-up and goes through DocuSign, the session was getting locked on payment page with /409 session lock error message
    -   The issue was traced to "Agreement Page" itself and hence the logic to restart session was updated. Due to previous simultaneous calls to /session endpoint, the "409 session lock" issue was happening which is now fixed.
-   `baza-nc`: Fixed issues with sending email notifications about failed Dwolla Dividend payments
-   `baza-nc`: Fixed issues with automatic creating Dwolla Customers when paying Dwolla Dividends
-   `baza-nc`: Fixed typos of `withdraw` endpoints
-   `baza-nc-api`: Added validation for `amount` field for `withdraw` endpoints
-   `baza-dwolla-web-purchase-flow`: Fixed issues with displaying Payment Method modal for >$5000 purchases
-   `baza-core-cms`: When the user changes the search (in any CMS tables), no matter where is located, it will always look up from the first-page index, ensuring the data offset will always be zero.

## Breaking changes

-   `baza-core-cms`: `NotFoundService` was moved to another location; Please update your imports if you're using it in your CMS

## Migration

-   Add `oembed-parser` and `ngx-plaid-link` NPM packages: `yarn add oembed-parser ngx-plaid-link`
-   Add `NgxPlaidLinkModule` module to imports in `app.module.ts` of web application or parent module of whatever component you wish you use Plaid integration
-   (IOS/Android applications only): Withdraw endpoints was updated; Check API documentation for new correct endpoint paths
-   Replace `bazaNcBundlesCmsMenu(environment.ngEnv)` with `bazaNcBundlesCmsMenu()` (helper is not accepting any arguments anymore, but still needs to be callable function)
-   Because of changes with ULID column, you should execute following SQL script before deploying your application with Baza 1.21.0 for all your environments:

```sql
ALTER TABLE "public"."baza_nc_dividend_entity" ALTER COLUMN "ulid" TYPE character(26);
ALTER TABLE "public"."baza_nc_dividend_transaction_entity" ALTER COLUMN "ulid" TYPE character(26);
ALTER TABLE "public"."baza_nc_dividend_transaction_entry_entity" ALTER COLUMN "ulid" TYPE character(26);
ALTER TABLE "public"."baza_nc_bank_account_entity" ALTER COLUMN "ulid" TYPE character(26);
ALTER TABLE "public"."baza_nc_dividend_confirmation_entity" ALTER COLUMN "ulid" TYPE character(26);
ALTER TABLE "public"."baza_nc_transfer_entity" ALTER COLUMN "ulid" TYPE character(26);
ALTER TABLE "public"."baza_nc_withdraw_entity" ALTER COLUMN "ulid" TYPE character(26);
```

## Flow Charts of Purchase Flow (FE)

Following are the flow-charts detailing the actions/events in Dwolla PF Library. Please note, this flow chart would be updated as part of new upcoming changes.

[https://lucid.app/lucidchart/2e77fe8a-20ba-47d2-8b0c-1edd4dce520d/edit?viewport_loc=-404%2C649%2C2219%2C1060%2C5IYmItLvkgNGZ&invitationId=inv_7b08bd0b-a8dd-4844-aeb5-e214f3a0ce1b]()

## Purchase Flow - Account Balance

-   Purchase Flow now supports purchasing shares using Dwolla Customer Balance
-   Bank Accounts API now has support for single bank account per type flow (new `static` option for `set`, `linkOnSuccess` and `add` endpoints)
-   Added endpoint to set default payment method (`ACH`, `CreditCard` or `AccountBalance`)
-   Investor Account endpoints (`status`, `current`) returns `defaultPaymentMethod` field with currently selected default payment method
-   New Dwolla Payments API with common implementation for Dwolla transactions. This API is used for Purchasing Shares feature and later it should be used for Transfer (Cash-In) and Withdraw operations
-   Add new "Dwolla Payments" CMS section which displays current Dwolla Payments information
-   Added new E2E helpers and some more changes
-   **Removed `MultiNode` pipeline (only `CurrentNode` and `EveryNode` pipelines works correctly)**
-   Fully covered with integration tests

https://scalio.atlassian.net/browse/BAZA-988

### Affected Endpoints

| Endpoint                                                          | Data Access                                               | Description                                                                                                                                      |
| ----------------------------------------------------------------- | --------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| `/baza-nc/purchase-flow/limits`                                   | `BazaNcPurchaseFlowLimitsDataAccess.limits`               | Added `maxDwollaTransferAmount` field with maximum transfer amount ($1,000,000)                                                                  |
| `/baza-nc/purchase-flow/limitsForPurchase`                        | `BazaNcPurchaseFlowLimitsDataAccess.limitsForPurchase`    | Added `AccountBalance` item to array with limits information                                                                                     |
| `/baza-nc/purchase-flow/session`                                  | `BazaNcPurchaseFlowDataAccess.session`                    | Added support for `transactionType: BazaNcPurchaseFlowTransactionType.Dwolla` payment method                                                     |
| `/baza-nc/bank-account/add`                                       | `BazaNcBankAccountsDataAccess.add`                        | Addded `static?: boolean` field. Enable this option for Single Bank Account per Type flow                                                        |
| `/baza-nc/bank-account/linkOnSuccess`                             | `BazaNcBankAccountsDataAccess.linkOnSuccess`              | Addded `static?: boolean` field. Enable this option for Single Bank Account per Type flow                                                        |
| `/baza-nc/bank-account/set`                                       | `BazaNcBankAccountsDataAccess.set`                        | Addded `static?: boolean` field. Enable this option for Single Bank Account per Type flow                                                        |
| `/baza/dwolla-payment/list`                                       | `BazaDwollaPaymentDataAccess.list`                        | Returns list of Dwolla Payments (currently used for Purchase Shares only; later it will be used for Withdraw and Cash-In operations)             |
| `/baza/dwolla-payment/get/:ulid`                                  | `BazaDwollaPaymentDataAccess.get`                         | Returns Dwolla Payment by ULID (currently used for Purchase Shares only; later it will be used for Withdraw and Cash-In operations)              |
| `/baza-nc/investor-account/default-payment-method/:paymentMethod` | `BazaNcInvestorAccountDataAccess.setDefaultPaymentMethod` | Added endpoint to set default payment method for Investor. Endpoint does not make any checks for isForeign investor or not, it just stores value |
| `/baza-nc/investor-account/current`                               | `BazaNcInvestorAccountDataAccess.current`                 | Added `defaultPaymentMethod` field to with selected default payment method. By default it will be `BazaNcPurchaseFlowTransactionType.ACH`        |
| `/baza-nc/investor-account/status`                                | `BazaNcInvestorAccountDataAccess.status`                  | Added `defaultPaymentMethod` field with selected default payment method. By default it will be `BazaNcPurchaseFlowTransactionType.ACH`           |

### Purchase Flow - purchasing shares with Account Balance

Baza 1.21.0 officially supports purchasing shares via Dwolla Account Balance.

-   You can create a new session with `BazaNcPurchaseFlowTransactionType.AccountBalance` Payment Type
-   New Dwolla purchase flow automatically creates NC Trade and sync Dwolla Transfer Status with NC Order Status
    -   When Dwolla Transfer is Completed, related NC Trade will have `Funded` Order Status
    -   When Dwolla Transfer is Cancelled, related NC Trade will have `Cancelled` Order Status
    -   When Dwolla Transfer is Failed, related NC Trade will have `Rejected` Order Status
-   Reprocess flow is also supported by new Payment Method
-   New payment method is using Dwolla Payments API. You can track & sync payments with Dwolla Payments CMS
-   `limits` endpoint updated to support new Payment Method

### Bank Accounts - Single Bank Account per Type Flow

-   Bank Account API is working with Multiple Bank Accounts per Type flow by default
-   You can override it with new `static` boolean flag for `add`, `set` and `linkOnSuccess` endpoints
-   Using new flag will allow you to have only 1 account per type (cash-in, cash-out) for operations

### Investor Account – ability to save Default Payment Method

-   Investor Account DTO (`/baza-nc/investor-account/current` endpoint) will contains `defaultPaymentMethod` field with selected default Payment Method
-   Status endpoint (`/baza-nc/investor-account/status`) also have new `defaultPaymentMethod` field
-   You can update Default Payment Method with new `/baza-nc/investor-account/default-payment-method/:paymentMethod` (`BazaNcInvestorAccountDataAccess.setDefaultPaymentMethod`) endpoint

### Dwolla Payments API & CMS

-   We have a lot of duplicate implementation currently with everything around Dwolla Transfers
-   There is a new API & infrastructure with common implementation, such as event handlers, failure reasons and many more
-   New Purchase Flow payment method is implemented with new infrastructure
-   A new "Dwolla Paymennts" CMS section will allow to track & sync payments.
    -   New CMS has Search and Date Ranges features
-   Syncing Dwolla Payments with Dwolla will also triggers all actions (in case if status was outdated), such as updating NC Trade or sending email notifications
-   Withdraw and Transfer (Cash-In) operations will be also moved to new API
-   Dividends will not be moved to new API and will keep same implementation
-   There is a Public Dwolla Payments API available which will displays list of payments made with Dwolla (`BazaDwollaDataAccessModule`, `BazaDwollaPaymentDataAccess`)

### E2E Helpers changes

-   Added new `simulateNcWebhook` helper for NC E2E which is sending NC Webhook to Event Bus
-   Added new `transferFundsFromMasterAccount` helper for Dwolla E2E which is transfering funds from Master Account to given Dwolla Customer
-   Added new `getTransferValue` helper for Dwolla E2E which is returning actual amounts for Dwolla Transfers. This helper is important and we should check for actual amounts in Dividends, Withdraw and Transfer (Cash-In) operations

### Other changes

-   Purchase Flow internal implementation migrated from `switch` to strategies solution:
    -   `BazaNcPurchaseFlowNcAchStrategy` for NC ACH payment method
    -   `BazaNcPurchaseFlowNcCreditCardStrategy` for NC Credit Card payment method
    -   `BazaNcPurchaseFlowDwollaAccountBalanceStrategy` + event handlers for Dwolla Account Balance payment method
-   Purchase Flow can returns additional error codes:
    -   `NoDwollaCustomer` - if you try to submit purchase, but current investor does not have verified Dwolla Customer yet
    -   `DwollaInsufficientFunds` - if investor don't have enough funds to process purchase (on submit)
-   Added `BazaDwollaE2eBundleModule` with E2E helpers & controllers for `baza-dwolla-api` package
-   New `BazaTableFieldType.Ulid` CRUD field type added to use with ULID primary keys
-   Reprocessing methods from `BazaNcPurchaseFlowSessionService` moved to new `BazaNcPurchaseFlowReprocessService` service
-   Added `BazaDwollaCustomerApiModule` module and `BazaDwollaCustomerService` service:
    -   `BazaDwollaCustomerService.getBalance` - returns Balance of Dwolla Customer
    -   `BazaDwollaCustomerServicefindBalanceFundingSource` - Returns Balance Funding Source of Customer. Method may return `undefined` if Balance FS is not available
    -   `BazaDwollaCustomerService.getBalanceFundingSource` - Returns Balance Funding Source of Customer. Method will throw a `BazaDwollaCustomerNoBalanceException` error with `BazaDwollaCustomerErrorCodes.BazaDwollaCustomerNoBalanceAvailable` error code is case if Dwolla Customer don't have Balance FS (i.e. Dwolla Customer is not verified)
    -   `BazaDwollaCustomerService.getBalanceFundingSourceHref` - Returns HREF of Balance Funding Source for Dwolla Customer. Result will be cached in Redis

## Dwolla Flow FE Changes

This release covers removing all mentions & logic of ACH bank accounts from Dwolla flow.

-   Removing "Link Bank Account" and "Manually Add Bank Details" options from payment page
-   Removal of ACH options from "Edit Payment Method" popup
-   Removal of any ACH related labels/error messages from payment page
-   Only showing "Account Balance" and "Credit Card" options in payment page
-   If an investor doesn't have an existing Dwolla account, upon clicking "Account Balance" /touch endpoint is called. If there are any errors during the operation, they are displayed accordingly
-   Creation of new NGXS store action & associated logic for calling /touch endpoint on payment page
-   Adding checks for international investors to ensure "Account Balance" option is not available to them

## Dwolla Plaid Integration (FE):

-   New bank account system endpoints were integrated for Dwolla PF library
-   Dwolla plaid flow integration is now part of new Dwolla libraries. As part of flow, for an investor not already having an existing Dwolla account, first /touch endpoint will be called to create an account for investor upon clicking "Account Balance" option. Please note, Dwolla account creation error message would also be displayed in case this operation doesn't succeed.
-   Once Dwolla account is available, then Dwolla Plaid would be triggered and investor would be able to attach a "Cash In" bank account
-   Investors who don't have an existing cash-in account but have Dwolla account would also be able to purchase shares
-   For now, only 1 cash-in account can be attached. Later, we plan to add support to handle multiple "Cash In" bank accounts
-   Once account is added, the "Add Funds" popup modal would be launched automatically. However, in the scope of this task, this modal is just base implementation of popup modal without ability to actually transfer funds, form validation or ability to edit cash-in bank account. These features will be handled in upcoming tasks
-   New utility functions added for purchase flow library & code cleaned up in Dwolla PF library
-   The plugin `ngx-plaid-link` was integrated for Plaid
-   As part of this task's scope, purchasing shares through account balance is not possible. So a toaster notification is shown if user clicks on "Submit" and account balance is selected as payment method

## Dwolla Transfer Feature

-   The transfer endpoint for investors are updated and both cms and investor endpoints are working fine.
-   A whole set of libs and sub libs and types were added to cover transfer .
-   The only functionality that the transfer endpoint is providing is the ability to move funds from the investor bank account into user Dwolla wallet.

**Public API: `/baza-nc/transfer:`**

-   Method: **POST**
-   Description: the endpoint provides the functionality for the investor to transfer money from a bank account into Dwolla .
-   Note: the `amount` property is mandatory and should be provided.
-   Sample payload:

```json
{
    "amount": "22.33"
}
```

**Public API: `/baza-nc/transfer/list:`**

-   Method: POST
-   Description: this endpoint wont probably be used by the front end since we are only relying on portfolio transactions to show the list of transfer for now. but this was added just in case.
-   Note : `status` property is not mandatory.
-   Sample payload:

```json
{
    "status": ["Pending", "Processed", "Failed", "Cancelled"]
}
```

**CMS API: `/baza-nc/transfers/cms/list:`**

-   Method: POST
-   Description: will list all the transfer requests made by the investors and provides filter functionality to for example let the client find transfer requests for specific investor
-   Note: the `investorAccountId` is optional.
-   Sample payload:

```json
{
    "investorAccountId": 12
}
```

**CMS API: `/baza-nc/transfers/cms/getByUlid:`**

-   Method : POST
-   Description: will find the transfer by ULID and show the details on cms
-   Note: `ulid` property is mandatory
-   Sample payload:

```json
{
    "ulid": "01GF62G9B41XE1VEYSH56FGC2E"
}
```

## JIRA

-   [BAZA-1380](https://scalio.atlassian.net/browse/BAZA-1380) COLLAB - Offering Status: On Updating offering Status from CMS, Its not reflecting in Collab
-   [BAZA-1381](https://scalio.atlassian.net/browse/BAZA-1381) COLLAB - Prod/Stage/Test: Property Images disappear and 403 error appears in the console on Stage/Prod
-   [BAZA-1293](https://scalio.atlassian.net/browse/BAZA-1293) V3: Align Credit Card screen for International investors with existing experience
-   [BAZA-1383](https://scalio.atlassian.net/browse/BAZA-1383) Validate ZIP code for 5 numeric digits only if Country is USA
-   [BAZA-1387](https://scalio.atlassian.net/browse/BAZA-1387) Purchase Flow: New User gets "Could not Start Session" message on first purchase after verification flow
-   [BAZA-988](https://scalio.atlassian.net/browse/BAZA-988) Implement ACH Flow with Dwolla + NC
-   [BAZA-992](https://scalio.atlassian.net/browse/BAZA-992) If sufficient funds to buy a share not available, investor has the ability to add funds
-   [BAZA-1027](https://scalio.atlassian.net/browse/BAZA-1027) Link to the video and embedded video player on FE ARE NOT displayed for successfully saved video link in BAZA CMS
-   [BAZA-1070](https://scalio.atlassian.net/browse/BAZA-1070) Email does not get triggered when Dwolla Dividend is failed to process
-   [BAZA-1076](https://scalio.atlassian.net/browse/BAZA-1076) Processing Dividends for an investor with no dwolla ID, is not creating Dwolla ID when processing the dividend
-   [BAZA-1250](https://scalio.atlassian.net/browse/BAZA-1250) Show Dwolla account creation error in purchase flow
-   [BAZA-1271](https://scalio.atlassian.net/browse/BAZA-1271) Checkbox addition to get investor's consent to create Dwolla Account
-   [BAZA-1273](https://scalio.atlassian.net/browse/BAZA-1273) Populating the reason for Dwolla transaction failed on CMS
-   [BAZA-1276](https://scalio.atlassian.net/browse/BAZA-1276) CSV document formatting change for amount
-   [BAZA-1279](https://scalio.atlassian.net/browse/BAZA-1279) Transfer Funds from Bank Account to Dwolla wallet
-   [BAZA-1286](https://scalio.atlassian.net/browse/BAZA-1286) Dwolla Plaid Integration for Digital Wallet (FE)
-   [BAZA-1315](https://scalio.atlassian.net/browse/BAZA-1315) API endpoint is Wathdraw instead of Withdraw
-   [BAZA-1330](https://scalio.atlassian.net/browse/BAZA-1330) Change copy for Dwolla Consent on Verification Flow
-   [BAZA-1333](https://scalio.atlassian.net/browse/BAZA-1333) Copy on Purchase Flow > Your order has been submited
-   [BAZA-1338](https://scalio.atlassian.net/browse/BAZA-1338) Remove ACH bank account option & all related logic from Dwolla flow
-   [BAZA-1343](https://scalio.atlassian.net/browse/BAZA-1343) Create a `@PrimaryColumn` decorator replacement for ULID columns and replace current ULID column definitions with new decorator
-   [BAZA-1344](https://scalio.atlassian.net/browse/BAZA-1344) Dwolla consent checkbox is displayed for user coming back to verification flow with isDwollaAvailable : true
-   [BAZA-1345](https://scalio.atlassian.net/browse/BAZA-1345) Dwolla consent checkbox is displayed for international/foreign investors
-   [BAZA-1349](https://scalio.atlassian.net/browse/BAZA-1349) Page reload on Verification Flow Steps removes all information from the fields
-   [BAZA-1359](https://scalio.atlassian.net/browse/BAZA-1359) Validation checks missing for negative and zero amount to transfer
-   [BAZA-1360](https://scalio.atlassian.net/browse/BAZA-1360) Webhooks not working for ACH to Dwolla transfers
-   [BAZA-1367](https://scalio.atlassian.net/browse/BAZA-1367) CMS - Ability to turn off the Days to Remove trades
-   [BAZA-1371](https://scalio.atlassian.net/browse/BAZA-1371) Default added card is not shown incase the purchase amount is above $5000, also no displayed in edit payment popup
-   [BAZA-1373](https://scalio.atlassian.net/browse/BAZA-1373) CMS - The Date Updated at field is not left to N/A when the dividends were only Processed and never updated
-   [BAZA-1378](https://scalio.atlassian.net/browse/BAZA-1378) Disable Device Token ID feature for target projects, except CW
-   [BAZA-1382](https://scalio.atlassian.net/browse/BAZA-1382) CMS - Accounts - When any page other than first is selected, searching a keyword does not retrieve all the possible results

## PRs

-   https://github.com/scalio/baza/pull/946
-   https://github.com/scalio/baza/pull/948
-   https://github.com/scalio/baza/pull/924
-   https://github.com/scalio/baza/pull/939
-   https://github.com/scalio/baza/pull/941
-   https://github.com/scalio/baza/pull/811
-   https://github.com/scalio/baza/pull/899
-   https://github.com/scalio/baza/pull/928
-   https://github.com/scalio/baza/pull/917
-   https://github.com/scalio/baza/pull/884
-   https://github.com/scalio/baza/pull/900
-   https://github.com/scalio/baza/pull/936
-   https://github.com/scalio/baza/pull/891
-   https://github.com/scalio/baza/pull/923
-   https://github.com/scalio/baza/pull/933
-   https://github.com/scalio/baza/pull/914
-   https://github.com/scalio/baza/pull/917
-   https://github.com/scalio/baza/pull/938
-   https://github.com/scalio/baza/pull/931
-   https://github.com/scalio/baza/pull/926
-   https://github.com/scalio/baza/pull/927
-   https://github.com/scalio/baza/pull/930
-   https://github.com/scalio/baza/pull/942
-   https://github.com/scalio/baza/pull/951
