# Release 1.7.2

## Bugfixes

-   AWS Proxy strategy now works again (was broken by security changes around not allowing admins to sign in to Web app)
-   export 'NzSpaceItemComponent' error messages fixed

## What's new

-   New upload files form controls for CMS
-   Baza-attachment package enchanced to support more upload files scenarios
-   Baza-attachment now has new `ImageWithThumbnails` strategy. The strategy allow to upload single image file and generate multiple original / thumbnail images with different sizes and different types (usually should be Webp + Png).
-   Fixed integration tests
-   Baza NC: support for initial bank account link flow
-   Baza NC (CMS): Shared form component for Offering tab (CMNW / MIP).

## Baza NC Offering Tab component

Offering Tab with full FE/BE validation and endpoints to fetch NC Offering details is implemented as Custom Form Control. Now it can be injected as is in any NC-related project.

Offering Tab also has feature to check is NC Offering exists and is NC offering is actually used for Property/Horse/Target Entity. There is an endpoint `/baza-nc/offerings/ncOfferingDetails` with default implementation, but it will not warn if offering is already used somewhere. You can implement your own endpoint and inject it to Offering Tab CMS form control with `props` (as is was implemented in MIP).

## Baza AWS and Baza Attachment libraries

Baza Core has two libraries about uploading files:

-   `baza-aws` library is responsible to upload files directly to S3. Baza-aws services don't do any processing with files and just upload files as is.
-   `baza-attachment` library works on top of `baza-aws` library and allow to pre-process input file and generate / upload multiple files and response with fully extensible `AttachmentDTO` which can contains not only S3 Object Id, but additional metadata, files or anything else.

## New Upload controls for CMS

Baza CMS now has more controls to work with uploads.

-   `BazaUpload` – uses AWS or Attachment api and allow to upload single file; returns / accepts S3 Object Id (string) as form value
-   `BazaUploadMulti` (new) – uses AWS or Attachment api & allow to work with multiple files; returns / accepts array of S3 Object Id' as form value
-   `BazaUploadAttachment` (new) - uses Attachment API only; returns / accepts Attachment DTO
-   `BazaUploadAttachmentMulti` (new) - ses Attachment API only and allow to work with multiple files; returns / accepts array of Attachment DTO's.

All Upload controls can work with Baza Attachment API. It can be useful to work with Baza Attachment API with `BazaUpload` if you just need to simple limit max width/ max height of image and you're expecting only single S3 Object Id.

All new upload controls works on top of base `BazaUpload` control. The amount of implementation duplication is close to zero.

## Baza Attachment: `ImageWithThumbnails` strategy.

New strategy allows to generate multiple original / thumbnail files from single image. It's designed to be fully functional for <picture>/<source> responsive markup and allow generate PNG/JPEG + WEBP variants of images.

The strategy also automatically resizes images and provides a lot of options about resizing images.

### How to use it

-   API should define fields with `AttachmentDTO` class.
-   CMS now has multiple controls about uploading files: `BazaUpload`, `BazaUploadMulti`, `BazaUploadAttachment` and `BazaUploadAttachmentMulti`. All these components allow to use Baza Attachment API but it hightly preferrable to use `BazaUploadAttachment` / `BazaUploadAttachmentMulti` component which returns value as `AttachmentDTO` object.
-   Web/ios/android clients can use `AttachmentDTO` fields directly or use simplified version if there is any available.

### CMS: Form Control configuration example:

```typescript
{
    type: BazaFormBuilderControlType.BazaUploadMulti,
    formControlName: 'imageAttachments',
    label: this.i18nField('images'),
    props: {
        withOptions: {
            transport: {
                type: BazaFileUploadTransport.BazaAttachment,
                // You can use different AttachmentRequests depends on input file.
                options: (file, extension) => ({
                    type: AttachmentType.ImageWithThumbnail,
                    payload: {
                        original: [
                            {
                                // Output type
                                type: AttachmentImageType.PNG,

                                // Optional field
                                // ID can be used on target clients or on API side as unique identificator of image variant.
                                id: 'original-default',

                                // Optional field
                                // Maximum image width
                                maxWidth: 1125,

                                // Optional field
                                // Maximum image height. By default `cover` strategy used for autocrop images;
                                // There are additional autocrop strategies availble: https://sharp.pixelplumbing.com/api-resize
                                // Bot maxWidth / maxHeight fields are optional.
                                maxHeight: 730,
                            },
                            {
                                id: 'original-webp',
                                maxWidth: 1125,
                                maxHeight: 730,
                                type: AttachmentImageType.WEBP,
                            }
                        ],
                        thumbnails: [
                            {
                                id: 'thumbnail-default',
                                maxWidth: 769,
                                maxHeight: 406,
                                type: AttachmentImageType.PNG,
                            },
                            {
                                id: 'thumbnail-webp',
                                maxWidth: 769,
                                maxHeight: 406,
                                type: AttachmentImageType.WEBP,
                            },
                        ],
                    },
                })
            },
        },
    },
},
```

### Request DTO:

```typescript
// Output image type
export enum AttachmentImageType {
    PNG = 'png',
    JPEG = 'jpeg',
    WEBP = 'webp',
}

// Request for image variant
export class AttachmentVariantRequest {
    type: AttachmentImageType;
    id?: string;
    maxWidth?: number;
    maxHeight?: number;

    /**
     * Sharp Resize options.
     * @see https://sharp.pixelplumbing.com/api-resize
     */
    fit?: 'contain' | 'cover' | 'fill' | 'inside' | 'outside';
    position?: number | string;
    background?:
        | string
        | {
              r?: number | undefined;
              g?: number | undefined;
              b?: number | undefined;
              alpha?: number | undefined;
          };
    kernel?: 'nearest' | 'cubic' | 'mitchell' | 'lanczos2' | 'lanczos3';
    fastShrinkOnLoad?: boolean;
    withoutEnlargement?: boolean;
}

// Result DTO (located in `payload` field of attachment DTO)
export class AttachmentImageWithThumbnailUpload {
    minFileSize?: number;
    maxFileSize?: number;
    withoutProgressive?: boolean;
    original: AttachmentVariantRequest;
    thumbnails: Array<AttachmentVariantRequest>;
}
```

### Example response DTO:

Response DTO contains complete information about uploaded variants of image.

Sometimes it can be very useful to simplify output DTO. You can use `id`'s to identify required image variants for simplified DTO's.

Web clients can use full DTO definition to generate `<source>` list of all available image variants, both WEBP and PNG/JPEG.

```json
{
    "presignedUrl": "https://cdn-commonwealth-stage.s3.us-west-1.amazonaws.com/cms/1625833796389-d41247fe-png-png?AWSAccessKeyId=AKIAVSK2676HTM6F5YHK&Expires=1625920204&Signature=LhiGRYDPO06TXeNaxrUIe71Pf0Q%3D",
    "s3ObjectId": "cms/1625833796389-d41247fe-png-png",
    "payload": {
        "type": "ImageWithThumbnail",
        "payload": {
            "original": [
                {
                    "id": "original-default",
                    "mime": "image/png",
                    "s3ObjectId": "cms/1625833796389-d41247fe-png-png",
                    "width": 1125,
                    "height": 730
                },
                {
                    "id": "original-webp",
                    "mime": "image/webp",
                    "s3ObjectId": "cms/1625833796389-d41247fe-webp-webp",
                    "width": 1125,
                    "height": 730
                }
            ],
            "thumbnails": [
                {
                    "id": "thumbnail-default",
                    "mime": "image/png",
                    "s3ObjectId": "cms/1625833799514-c5e20856-png-png",
                    "width": 769,
                    "height": 406
                },
                {
                    "id": "thumbnail-webp",
                    "mime": "image/webp",
                    "s3ObjectId": "cms/1625833802441-bc09c418-webp-webp",
                    "width": 769,
                    "height": 406
                }
            ]
        }
    }
}
```
