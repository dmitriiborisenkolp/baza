# Release 1.31.3

This release includes changes on home page component (FE) and adding BCC emails to cms registry and using it on both Mailgunt and CustomerIO transports

## What's new

-   `baza-core-api`: new BCC registry field was added to cms registry whill will provide the ability to use bcc emails on mailgun and customer io email services . the client can add multiple emails by adding comma between each of them (comma separated. e.g foo@gmail.com,bar@gmail.com)

-   `docs`: Updated docs indexing, updated docs for "Bullets (Text)" and added new docs for "Bullets (Image)" components

-   `libs/baza-docs-shared`: Updated the docs routing module

-   `libs/sandbox-web`: New style files added for bullets components, module references added in home module

-   `libs/baza-web-ui-components`: Added new "Bullets Image" component, updated previous "Bullets" component to "Bullets (Text)" to support differentiation, added HTML support for other components, added some fixes to components and updated styles

-   `Breaking Changes`:
-   The "Bullets" component (previous - text only) has now been updated to "Bullets (Text)" component. So, for FE, HTML structure has been updated

** HTML **

```typescript
<app-bullets-text></app-bullets-text>
```

Also note, that `BulletsModule` was renamed to `BulletsTextModule`.

## Migration:

In order to use the new `BulletsImageModule`, do the following:

-   Add the module reference in your application
-   Add the following line of code in HTML:

```typescript
<app-bullets-image></app-bullets-image>
```

-   You can provide additional configuration to this component too. The default configuration is as follows:

```typescript
config: BulletsImageConfig = {
    label: 'As seen on',
    images: [
        `/assets/images/marketwatch.png`,
        `/assets/images/yahoo-finance.png`,
        `/assets/images/bloomberg.png`,
        `/assets/images/benzinga.png`,
    ],
};
```

-   All of the components now have support for HTML so for example if we need to add a bullet list to "Hero Banner", we can easily add HTML directly as part of input configuration e.g. like follows:

```typescript
heroConfig: HeroConfig = {
    heading: `Build wealth and passive income by investing in alternative assets`,
    subHeading: `
        <ul class="hero-list">
        <li class="hero-list__item">Invest in a variety of assets with low barriers and high-growth potential.</li>
        <li class="hero-list__item">Investors of all sizes can purchase fractional shares for as little as $50 with a few clicks.</li> 
        <li class="hero-list__item">Experience true ownership with tangible, high-value assets.</li>
        </ul>`,
    btnLink: {
        appLink: {
            commands: ['/items'],
            text: 'Get Started',
        },
    },
};
```

-   In above example, we can see we are passing HTML content to `subHeading` property. This will load our custom `hero-list` and then we can easily add styles to it in our `_hero.less` file e.g. like below:

```typescript
.hero-list {
    padding-top: 20px;
    padding-left: 15px;
}

.hero-list__item {
    padding-bottom: 20px;
}
```

-   Please check out this documentation for further details:
    https://baza-docs.test.scaliolabs.com/web-development/marketing-components/04-bullets-image

##
