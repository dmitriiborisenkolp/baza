# Release 1.10.5

**JIRA Tickets:**

-   https://scalio.atlassian.net/browse/BAZA-325
-   https://scalio.atlassian.net/browse/BAZA-332
-   https://scalio.atlassian.net/browse/BAZA-322
-   https://scalio.atlassian.net/browse/BAZA-320
-   https://scalio.atlassian.net/browse/BAZA-324
-   https://scalio.atlassian.net/browse/BAZA-336
-   https://scalio.atlassian.net/browse/BAZA-312
-   https://scalio.atlassian.net/browse/BAZA-331
-   https://scalio.atlassian.net/browse/BAZA-327
-   https://scalio.atlassian.net/browse/BAZA-328
-   https://scalio.atlassian.net/browse/MIP-2139
-   https://scalio.atlassian.net/browse/BAZA-301
-   https://scalio.atlassian.net/browse/BAZA-311
-   https://scalio.atlassian.net/browse/BAZA-325
-   https://scalio.atlassian.net/browse/CMNW-1903
-   https://scalio.atlassian.net/browse/BAZA-337
-   https://scalio.atlassian.net/browse/BAZA-338

**What's New:**

-   Account Metadata Feature: additional fields can be added & saved with Account CMS
-   CSV now exports only known fields (no technical or prototype fields could be displayed)
-   API now can throttle incoming requests. Baza Test environment will allow to send Maximum 10 requests per 15 seconds TTL.
-   New ImageSet Upload strategy; Read more in PR https://github.com/scalio/baza/pull/114. New ImageSet is used for Sandbox Listings.
-   Added Search Box for Baza Documentation Portal
-   Additional fields added to Listings (https://baza-api.test.scaliolabs.com/sandbox/listings/list)
-   TestApp Renamed to Sandbox, Web application renamed to Docs
-   Added Change Status action / modal box in Sandbox Listings CMS
-   "Disable DocuSign" integration flag is display now for Test environments.
-   "Common" Tab Name is replaced with "General" for all known CMS sections
-   Added custom key-value fields for Sanbox Listings
-   Added `BazaFieldKeyValue` Form Control for CMS
-   Mailgun and Customer.IO now supports CC/BCC Fields (which is used for Admin email as example)
-   Maintenance and Master Password flags will not glitch anymore with cluster setup

**Possible can broke:**

-   CSV Exports
-   API may warn about "Too much requests" when it shouldn't

**Migration**

No additional changes required for target projects.

**PR's:**

-   https://github.com/scalio/baza/pull/117
-   https://github.com/scalio/baza/pull/116
-   https://github.com/scalio/baza/pull/114
-   https://github.com/scalio/baza/pull/112
-   https://github.com/scalio/baza/pull/111
-   https://github.com/scalio/baza/pull/105
-   https://github.com/scalio/baza/pull/98
-   https://github.com/scalio/baza/pull/95
-   https://github.com/scalio/baza/pull/93
-   https://github.com/scalio/baza/pull/90
-   https://github.com/scalio/baza/pull/89
-   https://github.com/scalio/baza/pull/86
-   https://github.com/scalio/baza/pull/87
-   https://github.com/scalio/baza/pull/122
-   https://github.com/scalio/baza/pull/121
