# Release 1.29.0

This release is all around i18n features for web libraries.

Most bugfix items related to 1.29.0 are fixed in Baza 1.28.1 release and mentioned in Release Notes for 1.28.1.

## What's new

-   `baza-web-ui-components`, `baza-dwolla-web-verification`, `baza-dwolla-web-purchase-flow`, `baza-nc-web-verification`, `baza-nc-web-purchase-flow`: these libraries now have support for i18n label-by-label translations. There are default i18n configurations which are already applied within libraries but can be easily overridden by providing custom i18n translations at app. level. This works for labels including titles, description, form labels, validation errors and notifications.

## Bugfixes

-   `baza-core-api`: Referral Code Usage statistic will not be duplicated when confirming Email Address
-   `baza-core-api`: Referral Code Usage will be written with Application used by user to sign up, not with current Application used to process request
-   `baza-core-cms`: fixed the issues with the attachments fields when used on the CMS schema
-   `baza-nc-cms`: fixed the issue of CMS schema definition validators not triggering the form "valid" status.
-   `baza-nc-api`: Purchase Flow `current` endpoint will update Session TTL
-   `docs`: fixed links in Baza Documentation portal

## Migration (FE):

Baza now has support for label-by-label translations for VF (Dwolla, NC), PF (Dwolla, NC) and Account components (Dwolla).

### Note (Breaking Change):

Before we can implement i18n translation overrides, please setup your application to integrate the default translations. That's required otherwise default translations from libraries would not work.

It's important to note here that e.g. `BazaDwollaWebAccountEnI18n` is only required if project is using Dwolla account components (payment & payout methods). If project is still on NC, please only include NC specific default translations.

**Sample code for setting up default i18n translations in `app.component.ts` file:**

```typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { TranslateService } from '@ngx-translate/core';
import { StartApp } from '@scaliolabs/sandbox-web/data-access';
import * as deepmerge from 'deepmerge';

// Web UI
import { BazaWebUiEnI18n } from '@scaliolabs/baza-web-ui-components';
// Dwolla Account i18n
import { BazaDwollaWebAccountEnI18n } from '@scaliolabs/baza-dwolla-web-purchase-flow';
// Dwolla VF i18n
import { bazaDwollaWebVFEnI18n } from '@scaliolabs/baza-dwolla-web-verification-flow';
// Dwolla PF i18n
import { BazaDwollaWebPFEnI18n } from '@scaliolabs/baza-dwolla-web-purchase-flow';
// NC VF i18n
import { bazaNCWebVFEnI18n } from '@scaliolabs/baza-nc-web-verification-flow';
// NC PF i18n
import { BazaNcWebPFEnI18n } from '@scaliolabs/baza-nc-web-purchase-flow';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    constructor(private readonly translate: TranslateService, private readonly store: Store) {
        this.configureI18nTranslations();
        this.store.dispatch(new StartApp());
    }

    configureI18nTranslations() {
        const EnI18n = deepmerge.all([
            BazaWebUiEnI18n,
            BazaDwollaWebAccountEnI18n,
            bazaDwollaWebVFEnI18n,
            BazaDwollaWebPFEnI18n,
            bazaNCWebVFEnI18n,
            BazaNcWebPFEnI18n,
        ]);
        this.translate.setTranslation('en', EnI18n);
    }
}
```

Now that i18n default translations setup is completed, if required, please follow the guides below to override translations for required libraries.

### 1. Verification Flow (Dwolla):

BAZA Verification Flow Library (Dwolla) now has support for i18n label-by-label translations. There are default i18n configurations which are already applied within libraries but can be easily overridden by providing custom i18n translations at app. level. This works for labels including titles, description, form labels, validation errors and notifications.

#### Breaking Changes:

-   The `Info Component` now doesn't have any input configuration object defined anymore, as previously it was used only for label translations.
-   The config VerificationConfig that's used for Dwolla VF is now updated as per following format:

```ts
defaultConfig: VerificationConfig = {
    links: {
        buyShares: '/buy-shares',
        verification: '/verification',
        backLink: {
            appLink: {
                commands: ['/custom-back-link'],
            },
        },
    },
};
```

-   Route configurations do not have "text" property included anymore. The text is automatically managed through i18n translation file by default and can be overridden through custom i18n translation as described in documentation e.g. the default configuration in `Investor Component` is now as follows:

```ts
const defaultConfig: VFInvestorConfig = {
    links: {
        termsOfService: {
            extLink: {
                link: 'bazaCommon.links.termsOfService',
                isCMSLink: true,
            },
        },
        privacyPolicy: {
            extLink: {
                link: 'bazaCommon.links.privacyPolicy',
                isCMSLink: true,
            },
        },
        dwollaTOS: {
            extLink: {
                link: 'https://www.dwolla.com/legal/tos/#legal-content',
            },
        },
        dwollaPriacyPolicy: {
            extLink: {
                link: 'https://www.dwolla.com/legal/privacy',
            },
        },
    },
};
```

Once defined, it can be passed as input e.g. like the following in app. level:

```html
<app-verification-dwolla [config]="customConfig">
    <app-verification-info-dwolla
        *ngIf="currentTab === 0"
        target="info"></app-verification-info-dwolla>
    <app-verification-investor-dwolla
        *ngIf="currentTab === 1"
        target="investor"></app-verification-investor-dwolla>
</app-verification-dwolla>
```

### 2. Purchase Flow (Dwolla)

-   Created i18n resources for Dwolla purchase flow parent component
-   created i18n resources for Dwolla purchase flow components, including details, agreement, payment, methods, done, payment edit (add, list, update), and payment methods (bank, bankModal, card, cardModal).
-   Created i18n resources for web UI components such as summary, bank details, and card details.
-   Replaced all hardcoded texts with unified translations.

#### Breaking changes

The `DetailsConfig` and `AgreementConfig` interfaces have been removed in favor of i18n files. These interfaces were previously used to configure certain text elements in the Dwolla purchase flow components. If you were using these interfaces, you will need to update your implementation to use the new i18n files instead.

The `PurchaseDoneConfig` interface no longer exposes properties to edit the caption. If you were using these properties to customize the purchase done caption, you will need to update your implementation to use the new format.

The PurchaseConfig interface has been updated to use a new format. The PurchaseBackLinkConfig and CustomLinksConfig interfaces have been replaced with a new linksConfig property. If you were using the old interfaces, you will need to update your implementation to use the new format.

### 3. Account (Dwolla)

-   Created i18n resources for payment components (account balance, bank account, credit card).
-   Created i18n resources for payout components (bank account).
-   Created i18n resources for funds components (form, modal, notification).
-   Replaced all hardcoded texts with unified translations.

#### Breaking changes

the `accBalanceConfig` property from the `PaymentConfig` object has been removed. This property was previously used to configure the text elements related to the account balance in the Dwolla payment flow. If you were using this property, you will need to update your implementation.

Instead of specifying each text individually, you should now override the translations config to change any caption. Please refer to the documentation section for more information on how to do this.

### 4. Verification Flow (NC)

BAZA Verification Flow Library (NC) now has support for i18n label-by-label translations. There are default i18n configurations which are already applied within libraries but can be easily overridden by providing custom i18n translations at app. level. This works for labels including titles, description, form labels, validation errors and notifications.

Apart from that, configuration objects were updated and new route configuration system is integrated for NC configurations.

#### Breaking Changes:

-   The `Info Component` now doesn't have any input configuration object defined anymore, as previously it was used only for label translations.

-   The input parameter name for VerificationConfig used in NC VF is now updated to just "config" and the structure is updated to use new route configuration system, as follows:

```typescript
defaultConfig: VerificationConfig = {
    links: {
        backLink: {
            appLink: {
                commands: ['/custom-back-link'],
            },
        },
    },
};
```

**Note:**
There's no need to pass "text" for link, it would be picked up through i18n translation file.
Once defined, it can be passed as input e.g. like the following in app. level:

```html
<app-verification [config]="customConfig">
    <app-verification-info
        *ngIf="currentTab === 0"
        target="info"></app-verification-info>
    <app-verification-investor
        *ngIf="currentTab === 1"
        target="investor"></app-verification-investor>
</app-verification>
```

### 5. Purchase Flow (NC)

-   Created i18n resources for NC purchase flow parent component
-   Created i18n resources for NC purchase flow components, including details, agreement, payment, methods, done, payment edit (add, list, update), and payment methods (bank, bankModal, card, cardModal).
-   Replaced all hardcoded texts with unified translations.

#### Breaking changes

We have made some changes to the interfaces and configuration types in the Dwolla purchase flow components. These changes may affect your implementation if you were using any of these interfaces or types.

The `DetailsConfig` and `AgreementConfig` interfaces have been removed in favor of i18n files. If you were using these interfaces, you will need to update your implementation.
The `PurchaseDoneConfig` type no longer exposes properties to edit captions. Additionally, the property `purchaseDoneCallToActionLink` has been renamed to `ctaLink`. Please update your implementation accordingly.
The `PurchaseConfig` type no longer exposes properties to edit captions. If you were using these properties, you will need to update your implementation.
The `PaymentMethodOptionsConfig` type structure has been simplified. Captions are no longer supported, and the nesting has been removed. Please refer to the updated example code above and update your implementation accordingly.

## Documentation

Here are the detailed indexed guides on how to use and override i18n translations for libraries:

-   [Initial Setup for i18n Translations](https://baza-docs.test.scaliolabs.com/web-development/i18n/initial-setup/01-index)
-   [Customizing Web UI i18n Translations](https://baza-docs.test.scaliolabs.com/web-development/i18n/web-ui-components/01-index)
-   [Customizing Dwolla Account i18n Translations](https://baza-docs.test.scaliolabs.com/web-development/i18n/dwolla-account/01-index)
-   [Customizing Dwolla Verification Flow i18n Translations](https://baza-docs.test.scaliolabs.com/web-development/i18n/dwolla-vf/01-index-i18n)
-   [Customizing Dwolla Purchase Flow i18n Translations](https://baza-docs.test.scaliolabs.com/web-development/i18n/dwolla-pf/01-index)
-   [Customizing NC Verification Flow i18n Translations](https://baza-docs.test.scaliolabs.com/web-development/i18n/nc-vf/01-index-i18n)
-   [Customizing NC Purchase Flow i18n Translations](https://baza-docs.test.scaliolabs.com/web-development/i18n/nc-pf/01-index)

## JIRA

-   [BAZA-1723](https://scalio.atlassian.net/browse/BAZA-1723) Customer IO: Impossible to sign in CMS as verification code is missed in the 'Confirm Your Sign-in Request' email
-   [BAZA-1620](https://scalio.atlassian.net/browse/BAZA-1620) Duplicated entries in usage statistic of Referral Code when user signups from the iOS app
-   [BAZA-1639](https://scalio.atlassian.net/browse/BAZA-1639) CMS - Schema Tabs - Unable to save schema fields including "files" for the listings
-   [BAZA-1642](https://scalio.atlassian.net/browse/BAZA-1642) CMS - when adding a new Listing, Save CTA should be disabled until all the mandatory fields get filled in.
-   [BAZA-1714](https://scalio.atlassian.net/browse/BAZA-1714) Customer.io Mails - Dividends - Dwolla summary mail is received empty without transactions entries
-   [BAZA-1715](https://scalio.atlassian.net/browse/BAZA-1715) Update readonly session endpoint to also extend session TTL
-   [BAZA-1664](https://scalio.atlassian.net/browse/BAZA-1664) Implement i18n Translations on FE for PF/VF libraries (Both NC & Dwolla) and also Account Components (Dwolla)
-   [BAZA-1665](https://scalio.atlassian.net/browse/BAZA-1665) Implement i18n translations for VF library (Dwolla)
-   [BAZA-1666](https://scalio.atlassian.net/browse/BAZA-1666) Implement i18n translations for PF library (Dwolla)
-   [BAZA-1667](https://scalio.atlassian.net/browse/BAZA-1667) Implement i18n translations for Account Components (Dwolla)
-   [BAZA-1669](https://scalio.atlassian.net/browse/BAZA-1669) Implement i18n translations for VF library (NC)
-   [BAZA-1670](https://scalio.atlassian.net/browse/BAZA-1670) Implement i18n translations for PF library (NC)
-   [BAZA-1724](https://scalio.atlassian.net/browse/BAZA-1724) Add i18n overrides in sandbox-web and test all configurations
-   [BAZA-1678](https://scalio.atlassian.net/browse/BAZA-1678) CMS - Investor Accounts - Clicking on Transactions for one investor opens transactions for all available investors
-   [BAZA-1728](https://scalio.atlassian.net/browse/BAZA-1728) Payments & Payout (Dwolla) - Support email for withdrawal is missing
-   [BAZA-1739](https://scalio.atlassian.net/browse/BAZA-1739) baza-docs portal - i18n documentation links are not opening up properly

## PRs

-   https://github.com/scalio/baza/pull/1217
-   https://github.com/scalio/baza/pull/1220
-   https://github.com/scalio/baza/pull/1145
-   https://github.com/scalio/baza/pull/1214
-   https://github.com/scalio/baza/pull/1169
-   https://github.com/scalio/baza/pull/1183
-   https://github.com/scalio/baza/pull/1206
-   https://github.com/scalio/baza/pull/1208
-   https://github.com/scalio/baza/pull/1187
-   https://github.com/scalio/baza/pull/1218
