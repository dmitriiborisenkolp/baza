# Release 1.10.8

**What's new:**

-   Multi-factor authorization added. Read more about MFA in related PR
-   NC Query Logs will be enabled only for development environment
-   Fixed issues with updating password
-   Fixed issues with changing default image sizes
-   Fixed issues with customer.io mail transport

**JIRA:**

-   https://scalio.atlassian.net/browse/BAZA-396
-   https://scalio.atlassian.net/browse/BAZA-399
-   https://scalio.atlassian.net/browse/BAZA-400
-   https://scalio.atlassian.net/browse/BAZA-401
-   https://scalio.atlassian.net/browse/BAZA-402

**PR:**

-   https://github.com/scalio/baza/pull/188
-   https://github.com/scalio/baza/pull/187
-   https://github.com/scalio/baza/pull/186
-   https://github.com/scalio/baza/pull/185
-   https://github.com/scalio/baza/pull/184
-   https://github.com/scalio/baza/pull/179
-   https://github.com/scalio/baza/pull/177
