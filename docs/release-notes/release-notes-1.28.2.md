# Release 1.28.2

Small fixes over Schema DTO and `schema` property for Listing DTO.

## Bugfixes

-   `baza-nc-integration-api`: Added `id`, `name`, `type`, `required` properties to fields array. We also covered it with integration tests

## JIRA

-   [BAZA-1752](https://scalio.atlassian.net/browse/BAZA-1752) CMNW: Missing name in fields array after the 1.28.1 update.

## PRs

-   https://github.com/scalio/baza/pull/1227
