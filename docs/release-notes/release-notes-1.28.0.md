# Release 1.28.0

With this release we added support for Customer.IO Transactional Email Templates, support for synchronising TBD trades, a lot of documentation, bugfixes and many more. We also possible fixed issues with NC Webhooks and CQRS with new `@nestjs/event-emitter` package added & used in Baza.

## What's new

-   `baza-core-api`: Added support and switch for Event Bus to use CQRS (Default) or EventEmitter2 solution. Read "CQRS vs EventEmitter2" section for more details.
-   `baza-web-utils`: fixed default object initialization issue. Read more details in related section below
-   `baza-nc-integration-api`: added Tab details for Schema DTO for Listings response
-   `baza-nc-api`: Added synchronisation support for Dwolla (TBD) transactions
-   `baza-dwolla-api`: added `ncTradeId` column to `dwolla_payment_entity`. While we would like to keep Dwolla packages isolated from NC packages, we're adding this optional column for performance / optimizations reasons
-   `baza-core-api`: added new package Event Emitter as an optional alternative for CQRS
-   `baza-nc-api`: Sync Transactions feature now correctly works with TBD (Dwolla Payments) transactions
-   `baza-nc-api`: Sync Transactions automatically updates Percents Funded of Offerings, Dwolla Payments and Escrow Daily Report items for TBD transactions
-   `baza-nc-api`: Sync Transactions feature may trigger updates for Order Statuses for TBD trades in cases when associated Dwolla Payment Status is not synced with Order Status which should be set for Trade
-   `baza-nc-api`: `getCreditCard` and `setCreditCard` will not attempt to create an Investor Account when being executed for user w/o Investor Account available yet
-   `baza-nc-api`: Baza NC API is now don't automatically creates an empty Investor Account in most cases. Bootstrap endpoint will return a Mock DTO instead of DTO of real Investor Account
-   `baza-nc-cms`: Sync Transactions feature is working in async mode. Success message updated
-   `baza-nc-web-verification`, `baza-dwolla-web-verification`: added validations for fields in Bank Account forms
-   `baza-web-utils`: added Composable `restrictedCharsValidator` validator that ensures a form control's value only contains allowed characters. Read more in `RestrictedChars Validator` section
-   `baza-nc-integration-api`: fixed issue when unpublished Listings was appearing after using "Reindex all Listings" tool
-   `baza-nc-api`: added new `/baza-nc/dwolla/cms/touch/all-investors` CMS endpoint which will trigger updating Dwolla Customer information for all existing Investor Accounts
-   `baza-nc-cms`: updating / Linking Dwolla Customer action will be available for all Investor Accounts, not only for Investor Accounts w/o associated Dwolla Customer Id
-   `baza-nc-cms`: There is a new Bulk Sync tool "Update/Relink Dwolla Customer for All Investors" available in Investor Account CMS which will run sync with Dwolla for all existing Investor Accounts
-   `baza-core-api`: Device Token clean-up now runs once per 12 hours, not once per-minute
-   `baza-nc-api`: fixed Dividend Report Summary not being sent when the source is mixed in a single transaction.
-   `baza-nc-api`: added `withDwollaFeatures` configuration for `bazaNcApiConfig` helper. You should enable this configuration if you're using Dwolla

## Customer.IO Integration

-   `baza-core-api`: 2FA will be disabled if Customer.IO integration is not configured properly yet to allow process 2FA via Email (when signing in to CMS)
-   `baza-core-cms`: CMS will display a banner in case if Customer.IO Integration is enabled and not all Mail Templates are linked with Customer.IO
-   `baza-core-cms`: Customer.IO Integration CMS has new optional "Unlink Template from Customer.IO" action. It could be enabled with `bazaCmsBundleConfigBuilder().withCustomerIoLinkCms({ withCustomerIoLinkCmsUnlinkFeature: true });`
-   `baza-core-api`: added Baza Mail Customer.IO Link API which allows to associated Baza Mail Templates with Customer.IO Transaction Email Templates
-   `baza-core-cms`: Added CMS which allows link Baza Mail Templates with Customer.IO Transaction Email Templates
-   `baza-core-cms`: Documentation for Mail Templates and Mail Template Variables is available from Mail Customer.IO Link CMS
-   `baza-core-api`: Added new `bazaApiBundleConfigBuilder.withCustomerIoTemplates()` helper for enabling Customer.IO Template integration when using CustomerIO Mail Transport
-   `baza-core-api`: associations between Baza Mail Templates and Customer.IO Mail Templates could be predefined in API. CMS and API implementation of linking different mail templates will works at same time in proper way.
-   `baza-core-api`: Added Mail Template validation feature. Please check "Mail Template Validation" section for details
-   `baza-core-api`: Added `bazaApiBundleConfigBuilder.withMailTemplateValidation()` helper for enabling Mail Template Validation. We are not recommending currently to enable this flag on non-Baza repositories, but we may later change recommendations.
-   `baza-core-api`: Debug tool now produces logs with single log entity in order to increase logs readability (`pino` logger produces a little too much for every line of logs)
-   `baza-core-api`: Added endpoints which will display information about Mail Templates
-   `baza-core-api`: Customer.IO MailTransport will use Customer.IO Transaction Email Templates. ID's for templates will be taken from Mail Customer.IO Link CMS
-   `baza-core-api`: Difference between HTML and TEXT variables for mail templates are removed. Variables for both email variables will be set from common object.
-   `baza-core-api`: Injecting variables to Emails now requires providing documentation (withing `register`/`registerGlobal` calls). You should provide documentation for each variable.
-   `baza-core-api`: It is possible now to unregister Mail Template which will be not used for Target Project. Use `unregisterMailTemplates` to unregister Mail Templates which should not be visible at all.

## Documentation

-   Added documentation about Mail features. You can also find all information about Mail Templates and Customer.IO Integration. You can find it on Overview - Mail page: https://baza-docs.test.scaliolabs.com/overview/26-mail
-   A new "Mail Template" documentation section is available on Baza Documentation portal. This section is generated automatically from Mail Templates definitions defined by Baza.

## Bugfixes

-   `baza-nc-api`: possible fixed webhook stability issues. If you're experiencing issues with handling (not receiving) webhook, please switch event bus from CQRS (Nest.JS) to EventEmitter2. Check migration guides for more details
-   `baza-nc-api`: `touchInvestorAccount` is marked as deprecated. You should avoid creating Investor Account without solid reason to do it. If you still need it, you can use `upsertInvestorAccount` method which is identical to `touchInvestorAccount` implementation
-   `baza-nc-api`: InvestorAccount DTO (in `bootstrap` or in `/baza-nc/investor-account/current`) may be replaced with Virtual (Mock) DTO for new users. An `id` of virtual Investor Account will be `-1` in this case, check your applications does it will works correctly for new user cases
-   `baza-nc-api`: fixed return payment email sending issue in NC webhooks and some minor issues with email notifications
-   `baza-web-ui-components`: the margin/paddings for hero banner, CTA banner, text only and text image sections were fixed.
-   `baza-dwolla-web-verification-flow`: the `Info Component` now doesn't have any input configuration object defined anymore, as previously it was used only for label translations.
-   `baza-nc-api`: operations `list` endpoint now returns empty list instead of throwing error when current Account is not associated yet with Investor Account
-   `baza-nc-api`: fixed critical issue with generation Escrow Ops Report. This issue is affecting Baza 1.27.0/1.27.1 releases.
-   `baza-nc-api`: fixed Report Email with the dividend transaction summary not being sent
-   `baza-nc-cms`: removed possible Dwolla mentions when Dwolla features are disabled
-   `baza-nc-integration-api`: it is not possible to have multiple schemas with same title anymore
-   `baza-dwolla-web-verification-flow`: the config `VerificationConfig` that's used for Dwolla VF is now updated as per following format:

```typescript
defaultConfig: VerificationConfig = {
    links: {
        buyShares: '/buy-shares',
        verification: '/verification',
        backLink: {
            appLink: {
                commands: ['/custom-back-link'],
            },
        },
    },
};
```

## Breaking Changes

-   `baza-core-api`: Please note that `MailService.sendMail` method is marked as deprecated, and in additional this method does not supports both Mail Inteceptors and Mail Injection Variables features. If you would like to use these features, you should define Mail Template for your custom emails and you should migrate to `MailService.send` usage instead of `MailService.sendMail` method.

## Migration

-   Install Nest.JS EventEmitter2 package: `yarn add @nestjs/event-emitter@^1.3.1`
-   If you're **NOT** using Dwolla, you should disable Dwolla Features with `bazaNcApiConfig({ withDwollaFeatures: false })` call
-   If you would like to use Customer.IO, you should visit Baza Documentation Portal for Overview - Mail page. You will find all necessary information here about migrating to new integration: https://baza-docs.test.scaliolabs.com/overview/26-mail
-   We're recommending switch API application to use EventEmitter2 package instead of Nest.JS CQRS. You should add `EventEmitterModule.forRoot()`(from `@nestjs/event-emitter` package) import to `app.module.ts` and add `EVENT_PROVIDER: EventEmitter` environment variable for all environments, including production.
-   If you're using Dwolla Payments, you should execute following migration to set up `ncTradeId` values for `dwolla_payment_entity` table:

```typescript
import { Injectable } from '@nestjs/common';
import { BazaAccountCmsService, BazaLogger, BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaDwollaPaymentRepository, BazaDwollaPaymentService } from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentPurchaseSharesPayload } from '@scaliolabs/baza-dwolla-shared';

@Injectable()
export class BazaNcDwollaPaymentNcTradeIdMigration006 implements BazaMigration {
    constructor(
        private readonly logger: BazaLogger,
        private readonly bazaAccountCmsService: BazaAccountCmsService,
        private readonly bazaDwollaPaymentService: BazaDwollaPaymentService,
        private readonly bazaDwollaPaymentRepository: BazaDwollaPaymentRepository,
    ) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(): Promise<void> {
        const accounts = (await this.bazaAccountCmsService.listAccounts({ size: 0, includeFields: ['id'] })).items.map((item) => item.id);

        for (const id of accounts) {
            const dwollaPayments = (await this.bazaDwollaPaymentService.list(id, { size: 0 })).items;

            for (const payment of dwollaPayments) {
                const payload = payment.payload as BazaDwollaPaymentPurchaseSharesPayload;

                if (payload.ncTradeId) {
                    const entity = await this.bazaDwollaPaymentRepository.findByUlid(payment.ulid);

                    if (entity) {
                        entity.ncTradeId = payload.ncTradeId;

                        await this.bazaDwollaPaymentRepository.save(entity);
                    }
                }
            }
        }
    }

    down(): Promise<void> {
        return Promise.resolve(undefined);
    }
}
```

-   If you're going to use Customer.IO integration with cloned Scalio workspace, you can use this configuration for initial linking Mail Templates with Customer.IO:

```typescript
// apps/api/src/main-customerio.ts
import { BAZA_ENVIRONMENTS_CONFIG, BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaCoreMail, SendMailCustomerIoBootstrap } from '@scaliolabs/baza-core-api';
import { BazaContentTypesMail } from '@scaliolabs/baza-content-types-api';
import { BazaNcMail } from '@scaliolabs/baza-nc-api';

export const mainCustomerio: (env: BazaEnvironments) => Array<SendMailCustomerIoBootstrap> = (env) => {
    return BAZA_ENVIRONMENTS_CONFIG.testEnvironments.includes(env)
        ? []
        : [
              {
                  name: BazaCoreMail.BazaAccountCmsChangeEmail,
                  customerIoId: 4,
              },
              {
                  name: BazaCoreMail.BazaAccountCmsResetPassword,
                  customerIoId: 5,
              },
              {
                  name: BazaCoreMail.BazaAccountCmsChangeEmailNotification,
                  customerIoId: 7,
              },
              {
                  name: BazaCoreMail.BazaAccountCmsPasswordChanged,
                  customerIoId: 6,
              },
              {
                  name: BazaCoreMail.BazaAccountWebAccountDeactivated,
                  customerIoId: 8,
              },
              {
                  name: BazaCoreMail.BazaAccountWebAlreadyRegistered,
                  customerIoId: 9,
              },
              {
                  name: BazaCoreMail.BazaAccountWebChangeEmail,
                  customerIoId: 10,
              },
              {
                  name: BazaCoreMail.BazaAccountWebChangeEmailNotification,
                  customerIoId: 11,
              },
              {
                  name: BazaCoreMail.BazaAccountWebConfirmEmail,
                  customerIoId: 12,
              },
              {
                  name: BazaCoreMail.BazaAccountWebDeactivateAccount,
                  customerIoId: 13,
              },
              {
                  name: BazaCoreMail.BazaAccountWebPasswordChanged,
                  customerIoId: 14,
              },
              {
                  name: BazaCoreMail.BazaAccountWebRequestResetPassword,
                  customerIoId: 15,
              },
              {
                  name: BazaCoreMail.BazaAccountWebResetPassword,
                  customerIoId: 16,
              },
              {
                  name: BazaCoreMail.BazaAuthTwoFactorAuth,
                  customerIoId: 17,
              },
              {
                  name: BazaCoreMail.BazaInviteCodeRequest,
                  customerIoId: 18,
              },
              {
                  name: BazaCoreMail.BazaReferralCodeSignedUpWithReferralCode,
                  customerIoId: 19,
              },
              {
                  name: BazaContentTypesMail.BazaContentTypesContactClientRequest,
                  customerIoId: 21,
              },
              {
                  name: BazaContentTypesMail.BazaContentTypesContactClientResponse,
                  customerIoId: 22,
              },
              {
                  name: BazaContentTypesMail.BazaContentTypesNewslettersInquiryEmail,
                  customerIoId: 23,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaTransferInitiated,
                  customerIoId: 24,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaTransferFailed,
                  customerIoId: 25,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaTransferCompleted,
                  customerIoId: 26,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaTransferCanceled,
                  customerIoId: 27,
              },
              {
                  name: BazaNcMail.BazaNcDividendDwollaReport,
                  customerIoId: 28,
              },
              {
                  name: BazaNcMail.BazaNcDividendConfirmProcessTransaction,
                  customerIoId: 29,
              },
              {
                  name: BazaNcMail.BazaNcDividendSuccessfulPaymentWithOffering,
                  customerIoId: 30,
              },
              {
                  name: BazaNcMail.BazaNcDividendSuccessfulPaymentWithoutOffering,
                  customerIoId: 31,
              },
              {
                  name: BazaNcMail.BazaNcOfferingClientNotification,
                  customerIoId: 32,
              },
              {
                  name: BazaNcMail.BazaNcReportNcEscrowNotification,
                  customerIoId: 33,
              },
              {
                  name: BazaNcMail.BazaNcFundsTransferred,
                  customerIoId: 34,
              },
              {
                  name: BazaNcMail.BazaNcInvestmentNotification,
                  customerIoId: 35,
              },
              {
                  name: BazaNcMail.BazaNcInvestmentSubmitted,
                  customerIoId: 36,
              },
              {
                  name: BazaNcMail.BazaNcPaymentReturned,
                  customerIoId: 37,
              },
              {
                  name: BazaNcMail.BazaNcTradeCanceled,
                  customerIoId: 38,
              },
          ];
};
```

## Customer.IO Integration with Transaction Emails Feature

Baza allows using Customer.IO Templates when Customer.IO Transport is selected. In order to enable this integration, you should:

-   Configure Mail Module to use Customer.IO Transport
-   Enable Usage of Customer.IO templates for API application
-   Enable Customer.IO Mail Link CMS for CMS application
-   Link Baza Mail Templates with Customer.IO templates. You can link it with CMS or predefine links in API application.

### Configure Mail Module to use Customer.IO Transport

You should receive `appKey` from Customer.IO panel and update related `.env` configurations with:

```
MAIL_TRANSPORT=customerio
MAIL_CUSTOMER_IO_APP_KEY=... # appKey
```

### Enable Usage of Customer.IO templates for API application

You can enable Customer.IO integration with `bazaApiBundleConfigBuilder.withCustomerIoTemplates` helper. This method is also accepting a callback which will allow you to predefine Links between Baza Mail Templates and Customer.IO Templates. We're recommending to keep links definitions in separated flags due of huge amount of Mail Templates available in Baza.

```typescript
// apps/api/src/main-customerio.ts
import { BAZA_ENVIRONMENTS_CONFIG, BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaCoreMail, SendMailCustomerIoBootstrap } from '@scaliolabs/baza-core-api';

export const mainCustomerio: (env: BazaEnvironments) => Array<SendMailCustomerIoBootstrap> = (env) => {
    return BAZA_ENVIRONMENTS_CONFIG.testEnvironments.includes(env)
        ? []
        : [
              {
                  name: BazaCoreMail.BazaAccountCmsResetPassword, // Baza Mail Template
                  customerIoId: 5, // ID of Customer.IO template. Displayed on Transacton page
              },
              {
                  name: BazaCoreMail.BazaAccountCmsChangeEmail,
                  customerIoId: 4,
              },
              // Any other Mail Tempaltes from BazaCoreMail, BazaNcMail or BazaContentTypesMail ENUM collections
          ];
};

// apps/api/src/main-config.ts
import { mainCustomerio } from './main-customerio';

bazaApiBundleConfigBuilder()
    .withMailTemplateValidation() // We're recommended to enable Mail Template validation for non-Production environments
    .withCustomerIoTemplates(mainCustomerio);
```

### Enable Customer.IO Mail Link CMS for CMS application

As far as we currently have API and CMS applications in separate places, you should also enable Customer.IO Mail Link CMS with `bazaCmsBundleConfigBuilder.withCustomerIoLinkCms()` helper.

```typescript
// apps/cms/src/app/app.module.ts

bazaCmsBundleConfigBuilder().withCustomerIoLinkCms();
```

### Link Baza Mail Templates with Customer.IO templates

You should set up workspace and create Customer.IO Transaction Email Templates. You can also fork existing example Customer.IO workspace and modify templates for your project.

When it's finished, you should link Baza Mail Templates with Customer.IO Transaction Email Templates.

-   You can do it with CMS manually for each environment or your do it with API (in code repository) to predefine links on code level.
-   Predefined links could be updated from code repository, but it will not overwrite your custom changes in CMS unless you will not provide a different Customer.IO id in code.
-   Both options (CMS & Predefined) are supposed to work at same time. You can use both approaches for better experience.

## Mail Template Validation

Baza is using Mail Templates are Documentation source. This documentation is auto-generated from registered Mail Templates and this documentation is used for Baza CMS sections related to Setting up Email Templates (for Customer.IO integration, as example). It's important to keep Mail Templates definitions and Template Variables definitions up to date, because it will be used by users w/o access to Repository.

You can enable automatic Mail Template Validation which will intercept and fail Send Mail Requests if Request or Mail Template does not have enough data or documentation. While it's recommended to enable Mail Template Validation by default, feature is disabled usually, but it could be enabled with `bazaApiBundleConfigBuilder.withMailTemplateValidation()` helper.

## CQRS vs EventEmitter2

We found out that CQRS implementation which is provided by Nest.JS (`@nestjs/cqrs`) can somehow halt processing all events. Somehow it happens with NC Webhooks, and while an actual reason could be with some improper handling some webhook events, there is no way to have Event Bus implementation which can randomly halt at any moment.

We're migrating from Nest.JS CQRS package to EventEmitter2 usage. It is optional currently, and if you're experiencing issues with webhooks, we're recommending to switch to new new Event Bus as soon as possible.

If you would like to swtich to new Event Bus implementation, you should add `EventEmitterModule.forRoot()`(from `@nestjs/event-emitter` package) import to `app.module.ts` and add `EVENT_PROVIDER: EventEmitter` environment variable for all environments, including production.

## RestrictedChars Validator

This Release adds the `restrictedCharsValidator` to multiple form controls on both NC and Dwolla libraries to prevent server errors due to special characters.

-   Created a new composable validator `restrictedCharsValidator`
-   Validator added to `PaymentMethodsService` -> `generateBankDetailsForm`.
-   Validator added to `DwollaSharedService` -> `generateBankDetailsForm`.
-   Validator added to **FirstName** and **lastName** controls on NC (personal information) form.
-   Validator added to **FirstName** and **lastName** controls on Dwolla (personal information) form.
-   Added meaningful error messages

## Default Object Initialization

BAZA now supports deep merging of nested, complex objects which can handle all primitive data types (numbers, strings, dates etc.) as well as route configuration links (both internal & external links) and arrays.

We have extended the already available `deepmerge` plugin functionality with custom algorithm, which finds all nested object inspection paths recursively and merged the default & override (optional) configuration objects. Previously, we had the issue where arrays & links were not merged properly and since the JSON structure of these objects is completely dynamic, we had to first analyze the inspection paths recursively and then update all places where arrays & link objects are.

This implementation is made generic and available in `BazaWebUtilSharedService` method `mergeConfig` and currently being used in marketing home page components, Dwolla/NC purchase & verification libraries and also account components.

As part of the Release, following changes were made:

-   The method `mergeConfig` was updated with ability to handle all primitive and complex data types
-   Relevant places in internal code i.e. Dwolla/NC library, account & marketing components were updated
-   NULL checks added, default configuration logic updated for configurations
-   Covered the updated implementation with TSDocs

## JIRA

-   [BAZA-1630](https://scalio.atlassian.net/browse/BAZA-1630) Investigate and fix default object initialization issue
-   [BAZA-1673](https://scalio.atlassian.net/browse/BAZA-1673) UI: Home page: UI issues
-   [BAZA-1604](https://scalio.atlassian.net/browse/BAZA-1604) BE: Tabs and properties are disconnected in the API response
-   [BAZA-1672](https://scalio.atlassian.net/browse/BAZA-1672) include TBD transaction handling in transaction sync endpoints
-   [BAZA-1647](https://scalio.atlassian.net/browse/BAZA-1647) Create a BazaMailDocumentation API Module
-   [BAZA-1649](https://scalio.atlassian.net/browse/BAZA-1649) Implement Mail Documentation section of Baza Documentation Portal
-   [BAZA-1648](https://scalio.atlassian.net/browse/BAZA-1648) Refactor MailCustomerIoTransport to allow usage of Templates instead of Files
-   [BAZA-1681](https://scalio.atlassian.net/browse/BAZA-1681) CMS for linking Baza Mail Templates with Customer.IO Transaction Email Templates
-   [BAZA-1679](https://scalio.atlassian.net/browse/BAZA-1679) CMS: Able to create/update schema with existing schema name
-   [BAZA-1274](https://scalio.atlassian.net/browse/BAZA-1274) Payment summary through email after Dwolla payment
-   [BAZA-1674](https://scalio.atlassian.net/browse/BAZA-1674) Payment Method - Special characters in account name gives 500 internal server error for adding bank details manually
-   [BAZA-1695](https://scalio.atlassian.net/browse/BAZA-1695) CMS - Reindexing the listing will display unpublished listings on Investment Platform FE and on Clicking Buy Shares gives error
-   [BAZA-1683](https://scalio.atlassian.net/browse/BAZA-1683) Manual sync of transaction does not sync the status correctly for SETTLED transactions of TBD type
-   [BAZA-1697](https://scalio.atlassian.net/browse/BAZA-1697) Link (Update Link) Dwolla Customer feature for Investor Account CMS
-   [BAZA-1699](https://scalio.atlassian.net/browse/BAZA-1699) Payment Summary Email - Summary mail doesn't work when NC sourced dividends are also included with dwolla sourced
-   [BAZA-1687](https://scalio.atlassian.net/browse/BAZA-1687) 2FA should be ignored when Customer.IO Template integration enabled and 2FA Mail Template is not linked with Customer.IO
-   [BAZA-1700](https://scalio.atlassian.net/browse/BAZA-1700) Display warning message on top of CMS when not all Customer.IO Links (Integrations) are set
-   [BAZA-1702](https://scalio.atlassian.net/browse/BAZA-1702) Allow add documentation for injected variables for Mail Templates
-   [BAZA-1682](https://scalio.atlassian.net/browse/BAZA-1682) Setup Example Customer.IO Workspace with all Mail Templates
-   [BAZA-1703](https://scalio.atlassian.net/browse/BAZA-1703) New user - Before investor account is created, user sees error message on portfolio transactions for new operations endpoint
-   [BAZA-1596](https://scalio.atlassian.net/browse/BAZA-1596) CMS։ Ability to hide all the mentions of Dwolla on target projects.

## PRs

-   https://github.com/scalio/baza/pull/1162
-   https://github.com/scalio/baza/pull/1164
-   https://github.com/scalio/baza/pull/1165
-   https://github.com/scalio/baza/pull/1166
-   https://github.com/scalio/baza/pull/1167
-   https://github.com/scalio/baza/pull/1168
-   https://github.com/scalio/baza/pull/1170
-   https://github.com/scalio/baza/pull/1171
-   https://github.com/scalio/baza/pull/1172
-   https://github.com/scalio/baza/pull/1173
-   https://github.com/scalio/baza/pull/1174
-   https://github.com/scalio/baza/pull/1175
-   https://github.com/scalio/baza/pull/1177
-   https://github.com/scalio/baza/pull/1181
-   https://github.com/scalio/baza/pull/1183
-   https://github.com/scalio/baza/pull/1185
-   https://github.com/scalio/baza/pull/1186
-   https://github.com/scalio/baza/pull/1189
-   https://github.com/scalio/baza/pull/1190
-   https://github.com/scalio/baza/pull/1192
-   https://github.com/scalio/baza/pull/1193
-   https://github.com/scalio/baza/pull/1194
-   https://github.com/scalio/baza/pull/1196
-   https://github.com/scalio/baza/pull/1195
-   https://github.com/scalio/baza/pull/1197
-   https://github.com/scalio/baza/pull/1198
-   https://github.com/scalio/baza/pull/1200
-   https://github.com/scalio/baza/pull/1201
-   https://github.com/scalio/baza/pull/1144
