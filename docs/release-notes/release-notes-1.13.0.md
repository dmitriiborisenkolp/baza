# Release 1.13.0

This release is shorter than usual (previous release was released 1.5 weeks ago instead of 2). New release is focused around fixes & features around Dividends V1 and new Dwolla Customer feature for Investor Accounts.

## What's new

-   baza-nc: Dividends V1 received multiple fixes and additional features
-   baza-nc: Email notifications about successful payment of dividends to investors
-   baza-core-cms: Injecting configuration feature for Form Builders and CRUD. Read more about it on [Injecting & External modifying CMS CRUDs](https://baza-docs.test.scaliolabs.com/techniques/04-injecting-crud-lists) and [Injection custom configuration for Forms](https://baza-docs.test.scaliolabs.com/techniques/05-injecting-forms) documentation pages
-   baza-core-cms: Export To CSV feature reworked; Now all CSV exports display a modal window with delimiter (separator) selector
-   baza-nc: Account Verification now autotically creates / updates Dwolla Customer
-   baza-nc: Added `/baza-nc/dwolla/touch` endpoint which creates a Dwolla Customer for existing investor (current user)
-   baza-nc: Added `/baza-nc/dwolla/current` endpoint which display is Dwolla Customer available for current user and balance, if available
-   baza-nc: Bootstrap endpoint (and `InvestorAccountDTO`) now has additional `isDwollaAvailable` field
-   baza-nc-integration: New Filters API available with `/baza-nc-integration/search` and `/baza-nc-integration/search/ranges` endpoints
-   baza-core: Password API & shared libraries received updates. Read more about Password feature on [Passwords](https://baza-docs.test.scaliolabs.com/overview/29-passwords) documentation page.

## Breaking Changes

-   `baza-core`: Added new NPM dependency `csv-parse`
-   `baza-nc` packages now requires `baza-dwolla-shared`, `baza-dwolla-api` ans `baza-dwolla-cms` packages
-   `baza-nc`: `/destroy` endpoint does not requires `numberOfShares` and `amound` parameters (both removed)
-   New environment variables added:

```
# baza-dwolla

BAZA_DWOLLA_API_KEY=...
BAZA_DWOLLA_API_SECRET=...
BAZA_DWOLLA_BASE_URL=https://api-sandbox.dwolla.com # https://api.dwolla.com for production
BAZA_DWOLLA_DEBUG_NESTJS_LOG=0
BAZA_DWOLLA_DEBUG=0
BAZA_DWOLLA_SANDBOX=1 # 0 for production
BAZA_DWOLLA_WEBHOOK_KAFKA_SEND=0
BAZA_DWOLLA_WEBHOOK_KAFKA_READ=0
BAZA_DWOLLA_WEBHOOK_SECRET=some-webhook-key # should be a random string
BAZA_DWOLLA_WEBHOOK_DEBUG=0
```

## JIRA

-   [BAZA-967](https://scalio.atlassian.net/browse/BAZA-967) Offering should be optional for Dividends
-   [BAZA-634](https://scalio.atlassian.net/browse/BAZA-634) Disable default two-factor authentication in IVP
-   [BAZA-882](https://scalio.atlassian.net/browse/BAZA-882) Update Status mapping
-   [BAZA-901](https://scalio.atlassian.net/browse/BAZA-901) Conduct a proof of concept with Dwolla
-   [BAZA-917](https://scalio.atlassian.net/browse/BAZA-917) Identify the Dwolla account of a given investor user
-   [BAZA-932](https://scalio.atlassian.net/browse/BAZA-932) Email notifications about successful payment of dividends to investors
-   [BAZA-943](https://scalio.atlassian.net/browse/BAZA-943) (QA Only) Mass Payment Transactions Testing with 2FA
-   [BAZA-959](https://scalio.atlassian.net/browse/BAZA-959) Option to download dividends for Investor Account with CSV is unavailable
-   [BAZA-066](https://scalio.atlassian.net/browse/BAZA-066) When dividends opened from Investor accounts, On clicking the back Icon doesn't take back to investor accounts page
-   [BAZA-970](https://scalio.atlassian.net/browse/BAZA-970) Add `?` as special allowed symbol to password validation
-   [BAZA-971](https://scalio.atlassian.net/browse/BAZA-971) Add INJECT_IDs for all CRUD and Forms in CMS
-   [BAZA-983](https://scalio.atlassian.net/browse/BAZA-983) Map Transaction Statuses according to UI
-   [BAZA-986](https://scalio.atlassian.net/browse/BAZA-986) Handle CSV separator for Dividend File Uploader
-   [BAZA-1003](https://scalio.atlassian.net/browse/BAZA-1003) Uploading CSV with invalid separator shows error on the first attempt only, attempts after that do not show the error message
-   [BAZA-1002](https://scalio.atlassian.net/browse/BAZA-1002) CSV upload popup remains open when session is expired and user is taken to the login page
-   [BAZA-1004](https://scalio.atlassian.net/browse/BAZA-1004) Offering is shown mandatory in edit dividend flow, also No way to unselect Offering once its selected
-   [BAZA-489](https://scalio.atlassian.net/browse/BAZA-489) Filters API

## Jira (IVP)

-   [BAZA-895](https://scalio.atlassian.net/browse/BAZA-895) Changing transactions status "Text" Colors
-   [BAZA-842](https://scalio.atlassian.net/browse/BAZA-842) Improving DocuSign Experience For Mobile Phone
-   [BAZA-613](https://scalio.atlassian.net/browse/BAZA-613) Empty stats requests on Purchase Flow
-   [BAZA-1001](https://scalio.atlassian.net/browse/BAZA-1001) Update the social media icon URLs to scalio’s social media pages on IVP and Sandbox

## PRs

-   https://github.com/scalio/baza/pull/673
-   https://github.com/scalio/baza/pull/675
-   https://github.com/scalio/baza/pull/676
-   https://github.com/scalio/baza/pull/678
-   https://github.com/scalio/baza/pull/679
-   https://github.com/scalio/baza/pull/680
-   https://github.com/scalio/baza/pull/681
-   https://github.com/scalio/baza/pull/682
-   https://github.com/scalio/baza/pull/683
-   https://github.com/scalio/baza/pull/684
-   https://github.com/scalio/baza/pull/686
-   https://github.com/scalio/baza/pull/687
-   https://github.com/scalio/baza/pull/691
-   https://github.com/scalio/baza/pull/693
-   https://github.com/scalio/baza/pull/694
-   https://github.com/scalio/baza/pull/695
-   https://github.com/scalio/baza/pull/697
-   https://github.com/scalio/baza/pull/698
-   https://github.com/scalio/baza/pull/699
-   https://github.com/scalio/baza/pull/700
-   https://github.com/scalio/baza/pull/701
-   https://github.com/scalio/baza/pull/702
-   https://github.com/scalio/baza/pull/703
-   https://github.com/scalio/baza/pull/704
-   https://github.com/scalio/baza/pull/705
-   https://github.com/scalio/baza/pull/706
-   https://github.com/scalio/baza/pull/707
-   https://github.com/scalio/baza/pull/708

## PRs (IVP)

-   https://github.com/scalio/baza/pull/677
-   https://github.com/scalio/baza/pull/685
-   https://github.com/scalio/baza/pull/688
-   https://github.com/scalio/baza/pull/692
