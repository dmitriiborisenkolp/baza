# Release 1.31.2

This release fixes critical issue with calculating Offering percentages.

## Bugfixes

-   `baza-nc-api`: fixed the issue with calculating Offering percentages
