# Release 1.18.0

## What's new

-   `baza-nc-api`: Removed automatic creation of Dwolla customer on account verification endpoint `/baza-nc/account-verification/applyPersonalInformation`
-   `baza-nc-web-utils`: New directive appear was added from the old commonwealth project into Baza library `baza-nc-web-utils`
-   `baza-nc-integration-api`: A new property `isPublished` was added to `BazaNcIntegrationListingsDto` and mapper utility was updated accordingly
-   `baza-content-types`: Added new `@scaliolabs/baza-content-types-node-access` library
-   `baza-nc`: A new bank account system was added in Baza

## Bugfixes

-   `baza-nc-shared`: Added translations for NC ACL list
-   `baza-nc-api`: Dividends V1 does not calculate total dividends for Dividends without Listings
-   `baza-nc-api`: Updated failed trade watcher functionality to also send email to the investor and send update offering remaining shares event

## Breaking changes

-   All Node Access services from `@scaliolabs/baza-content-types-data-access` library moved to `@scaliolabs/baza-content-types-node-access`
-   Bootstrap endpoints from `baza-nc-integration` packages was moved to `baza-nc`. All relevant resources was renamed using BazaNc prefix instead of `BazaNcIntegration`

## Migration

-   As far as New Bank Account system is in WIP, you should enable Legacy Bank Account system in API application with:

```typescript
bazaNcApiConfig({ enableLegacyBankAccountsSystem: true });
```

-   The new infrastructure will be available even with enabled Legacy Bank Account system. If you have custom `withdraw` implementation, you should update your implementation and use funding sources created with new bank account system.
-   As far as `bankAccounts` from `baza_nc_investor_account_entity` removed, create a DB migration and move accounts from `baza_nc_investor_account_entity` to new `BazaNcBankAccountEntity` entities.
-   If your project is using Content Types, please install `@scaliolabs/baza-content-types-node-access` library and update your integration tests
-   Update imports related to `baza-nc-integration` bootstrap endpoint / DTOs

## Customization Ability Added In Purchase Flow

This release will add customization ability for the "Purchase Details Description" in details page and the "Agreement Page" in purchase flow.

### Details Page (Purchase Flow)

<br/>

**Overall, this release introduces the following changes:**

-   A new field `listingDetailsSubHeadingText` was added in `DetailsConfig` interface
-   This allows passing custom label value to this field and it will replace the default description label on details page
-   Relevant documentation was updated in the `baza-docs` portal

**Sample Code - Purchase Custom Component (TS)**

```typescript
    detailsConfig: DetailsConfig;

    constructor(private readonly route: ActivatedRoute) {
         this.detailsConfig = {
            listingDetailsSubHeadingText: 'Horse Details',
            purchaseDetailsDescription: 'Please pick the number of shares you would like to purchase.'

        };
    }
```

**Sample Code - Purchase Custom Component (HTML)**

```typescript
<app-purchase-details
        *ngIf="currentTab === 0"
        target="purchase-details"
        [personal$]="personal$"
        [detailsConfig]="detailsConfig">
</app-purchase-details>
```

### Agreement Page (Purchase Flow)

<br/>

**Overall, this release introduces the following changes:**

-   Custom configuration interface `AgreementConfig` was added
-   This allows passing custom configuration (optional) to the `AgreementConfig` interface through which we can modify the PDF signature disclaimer, PDF consent label and PDF signed successful label
-   Relevant documentation was updated in the `baza-docs` portal

**Sample Code - Purchase Custom Component (TS)**

```typescript
    agreementConfig: AgreementConfig;

    constructor() {
         this.agreementConfig = {
            signatureDisclaimer: 'Custom signature disclaimer.',
            pdfConsent: 'Custom PDF consent',
            successfulSigningLabel: 'Custom label for successful document signing'
        };
    }
```

**Sample Code - Purchase Custom Component (HTML)**

```typescript
<app-purchase-agreement
        *ngIf="currentTab === 1"
        target="purchase-agreement"
        [agreementConfig]="agreementConfig"
        (cancelPurchase)="purchase.cancelPurchase()">
</app-purchase-agreement>
```

## Form Validation Behavior Updates

This release fixes the bug where if a form is filled up & has at least 1 disabled field by default, the submit button is not disabled. As per new behavior, the submit button should be disabled by default if form is filled up, regardless of whether there are any specific disabled fields or not. The submit button should only be enabled in case form was changed (edited) or if the form is not filled up already.

## IVP

-   Added customization ability for the "Purchase Details Description" in details page in purchase flow
-   Added customization ability for the "Agreement Page" in purchase flow
-   Fixed form validation bug where if a form is filled up & has at least 1 disabled field by default, the submit button is not disabled
-   Fix design and horizontal scrolling for long SSN document name in verification flow

## JIRA

-   [BAZA-1224](https://scalio.atlassian.net/browse/BAZA-1224) Remove auto-bootstraping Dwolla Customer on Account Verification
-   [BAZA-1232](https://scalio.atlassian.net/browse/BAZA-1232) Request to update util package with appear directive from old CMNW project
-   [BAZA-1234](https://scalio.atlassian.net/browse/BAZA-1234) Add `isPublished` flag to BazaNcIntegrationListingsDto
-   [BAZA-1235](https://scalio.atlassian.net/browse/BAZA-1235) Extract Node Access services & move it to separate `baza-content-types-node-access` library
-   [BAZA-1141](https://scalio.atlassian.net/browse/BAZA-1141) In CMS, North Capital Section some labels are not working well
-   [BAZA-1126](https://scalio.atlassian.net/browse/BAZA-1126) DividendsV1 does not calculate total dividends for Dividends without Listings
-   [BAZA-998](https://scalio.atlassian.net/browse/BAZA-998) Automatically cancel a trade pending payments after X days for NC

## JIRA (IVP)

-   [BAZA-1206](https://scalio.atlassian.net/browse/BAZA-1206) Add customization ability for purchase details page
-   [BAZA-1207](https://scalio.atlassian.net/browse/BAZA-1207) Add customization ability for agreement page
-   [BAZA-1233](https://scalio.atlassian.net/browse/BAZA-1233) Form validation submit button is not disabled for forms filled up with at least 1 disabled field
-   [BAZA-1246](https://scalio.atlassian.net/browse/BAZA-1246) To fix large titles on Listing Details for mobile resolutions

## PRs

-   https://github.com/scalio/baza/pull/837
-   https://github.com/scalio/baza/pull/838
-   https://github.com/scalio/baza/pull/839
-   https://github.com/scalio/baza/pull/840
-   https://github.com/scalio/baza/pull/845
-   https://github.com/scalio/baza/pull/846
-   https://github.com/scalio/baza/pull/850
-   https://github.com/scalio/baza/pull/851

## PRs (IVP)

-   https://github.com/scalio/baza/pull/835
-   https://github.com/scalio/baza/pull/836
-   https://github.com/scalio/baza/pull/841
-   https://github.com/scalio/baza/pull/847
