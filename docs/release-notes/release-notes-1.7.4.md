# Release 1.7.4

## Bugfixes

-   Fixed BazaAttachNodeAccess (request object added)

## What's new

-   Baza NC: Offering shared CMS form control now has one more `ncMinAmount` field with proper validation and error messages.
-   Baza Migrations now can be executed with every update:

```typescript
import { Injectable } from '@nestjs/common';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { MipPropertySearchIndexService } from '@scaliolabs/mip/mip-property-search/api';

@Injectable()
export class MipPropertiesReindexMigration implements BazaMigration {
    constructor(private readonly service: MipPropertySearchIndexService) {}

    forEnvs() {
        return [
            BazaEnvironments.Local,
            BazaEnvironments.Stg,
            BazaEnvironments.Development,
            BazaEnvironments.Test,
            BazaEnvironments.Production,
        ];
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.EveryTime;
    }

    async up(): Promise<void> {
        await this.service.indexAll({
            async: false,
        });
    }

    async down(): Promise<void> {}
}
```
