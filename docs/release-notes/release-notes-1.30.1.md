# Release 1.30.1

Bugfixes for Baza 1.30.0

## Bugfixes

-   `baza-core-api`: CustomerIO previously we were passing email in the identity even when there was multiple users (emails) being passed, so the issue was actually that once we sent the customer io identity with email consisting of multiple emails and fields it would throw error saying we expect single email not multiple one, theissue was fixed the issue related to returned and trade cancelled emails not being triggered.
-   `baza-core-api`: Fix identifiers for sendUsingFileTemplates method
-   `baza-core-cms`: Fixed logic of the function factoryFormControl since it was omitting the min and max validators.
-   `baza-nc-api`: Fixes the issue where the `isBankAccountLinked` field in the investor's account information does not accurately reflect the information from the NC API.

## What's New:

-   `baza-core-cms`: Added safe operators and validation checks since some target projects are experiencing errors of the type "Can't read property of undefined".
-   `baza-core-cms`: Show additional CMS schema validator messages since it was only showing the message for the required validator and omitting the rest.
-   `baza-dwolla-web-verification-flow`: Added a new feature that Dwolla VF can be triggered from outside of Dwolla PF and once exited from back link or after 2nd step submission, user is redirected to the originating request page.
-   `baza-nc-api`: The `updateCreditCardStatus` service has been updated to ensure that the information to be updated is always up-to-date and doesn't affect what has been saved in the service `updateBankAccountStatus`.

## Dwolla VF - Ability to Define Custom Back To Origin Routes

This PR implements a new requirement that Dwolla VF can be triggered from outside of Dwolla PF and once exited from back link or after 2nd step submission, user is redirected to the originating request page.

## What's New:

-   A new button "Verify Your Investor Account" is now available in "Investor Profile (Dwolla)" tab in Account

-   This button appears for both cases when a user has or has not completed the Dwolla VF yet

-   The Dwolla VF can now be triggered outside of Dwolla PF from anywhere within the application. In this case specifically, we will have the originating page url appearing in address bar as `redirectUrl` value. Also, in this case, we would pick specific separate properties from i18n translation file for back link text and confirmation popup text respectively

-   The Dwolla VF would redirect user back to the page he came from, if back link is exited or if the VF is fully submitted. In both cases, the updated VF information would be now displayed back in "Investor Profile (Dwolla)" tab

-   The i18n configurations and relevant documentation on `baza-docs` was also updated respectively

## Migration:

-   The translation DwollaVFEnI18n is now updated to following new structure. 2 new properties `textForRedirectUrl` and `linkTextForRedirectUrl` are now available. Using these properties, target projects can customize the back link text and the confirmation popup text specifically. These new properties will be automatically picked up by Dwolla VF in case user lands on Dwolla VF and has a `redirectUrl` defined in address bar, meaning user landed on VF from anywhere outside of Dwolla PF.

```typescript
export const DwollaVFEnI18n = {
    backLink: {
        confirmation: {
            title: 'Are you sure you want to exit?',
            text: 'All completed steps will be saved. You can continue verification before your next Purchase.',
            textForRedirectUrl: 'All completed steps will be saved. You can continue verification again later.',
        },
        linkText: 'Back to Listing Details',
        linkTextForRedirectUrl: 'Go Back',
    },
    steps: {
        info: 'Personal Information',
        profile: 'Investor Profile',
    },
    notifications: {
        apply_investor_profile_success: 'Investor Profile successfully saved',
        apply_personal_info_success: 'Personal Information successfully saved',
        load_verification_fail: 'There was an error loading the verification information.',
        upload_doc_success: 'Additional Document successfully uploaded.',
        upload_doc_fail: 'There was an error uploading the additional document.',
        verify_upload_doc: 'Please upload a passport to verify your identity.',
    },
};
```

-   In order to trigger Dwolla VF with redirect back to the same page, create a button/link on the page as per the following sample:

**HTML**

```typescript
<button
     nz-button
     nzType="primary"
     (click)="redirectToVF()">
     Verify Your Investor Account
</button>
```

**Typescript**

```typescript

// Step 1: include Angular router in constructor
constructor(private readonly store: Store, private readonly router: Router) {}

// Step 2: Define the button click event handler. Note that `verification-dwolla` URL is the one defined at sandbox level which can be changed e.g. to `verification` or anything else which is loading Dwolla VF library

redirectToVF() {
        this.router.navigate(['/verification-dwolla'], {
            queryParams: {
                redirectUrl: this.router.url,
            },
        });
    }
```

## Notes:

-   Normally when Dwolla VF is triggered, it's via the Dwolla PF route so therefore we have cart information in our store. That way, we generate the "Back to Listing Details" backlink and use the item Id. But in case we land on Dwolla VF outside of PF scope, we would not have any cart information in store.

-   In scope of Baza level testing, the "Verify Your Investor Account" button is placed in "Investor Profile (Dwolla)" tab in Account. But target projects can place a button/link anywhere on their end as long as it's triggering Dwolla VF in the way defined above.

## JIRA

-   [BAZA-1847](https://scalio.atlassian.net/browse/BAZA-1847) Baza: Verification Flow (Dwolla) Add ability to define a route to a certain page from inside the flow
-   [BAZA-1716](https://scalio.atlassian.net/browse/BAZA-1716) Customer.io Mails - Payment returned notification mail not working
-   [BAZA-1885](https://scalio.atlassian.net/browse/BAZA-1885) CMS: Admin can't create new Listing or Edit existing one as Save button is disabled
-   [BAZA-1721](https://scalio.atlassian.net/browse/BAZA-1721) Customer.io Mails - Trade Cancelled notification mail not working
-   [BAZA-1884](https://scalio.atlassian.net/browse/BAZA-1884) COL - Investor Account Table in CMS - Bank Account Status not Syncing

## PRs

-   https://github.com/scalio/baza/pull/1286
-   https://github.com/scalio/baza/pull/1302
-   https://github.com/scalio/baza/pull/1293
-   https://github.com/scalio/baza/pull/1270
-   https://github.com/scalio/baza/pull/1323
