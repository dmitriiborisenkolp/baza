# Release 1.30.2

Bugfixes for Baza 1.30.0

## Bugfixes

-   `baza-nc-api`: Avoid runtime errors when exporting transactions

## What's New:

-   `baza-nc-api`: Added safe operators to possible null objects in transactions mapper

## JIRA

-   [BAZA-1899](https://scalio.atlassian.net/browse/BAZA-1899) COL - Prod: Export Transactions to CSV is not working on Prod

## PRs

-   https://github.com/scalio/baza/pull/1333
