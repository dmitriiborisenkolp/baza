# Release 1.30.3

This Release fixes CRITICAL issues which appears after changes on NC side around `updateParty` method. All packages except `1.28.6`, `1.30.3` and `1.31.1` will be affected by losing SSN numbers issue.

## What's new

-   `baza-core-api`: debug logs produced with `bazaDebug` utility will contains less duplicate entries
-   `baza-nc-api`: debug logs produced with `logRequest` and `logResponse` utilities will contains less duplicate entries
-   `baza-nc-api`: added ability to perform delay before executing NC requests. This is a response to known issue (which we experiencing on NC Sandbox / Integration Tests) with resources shortly being unavailable or correctly updated after executing different operations such as `createAccount`, `updateParty` or `createTrade`

## Bugfixes

-   `baza-nc-api`: critical fixes around updateParty method usages reflecting new changes on NC API side. Fixes issue with SSN numbers disappearing when updating Account Verification personal profile

https://scalio.atlassian.net/browse/BAZA-1915

## Breaking Changes

-   `baza-nc-api`: All packages except `1.28.6`, `1.30.3` and `1.31.1` _MUST NOT_ be used anymore for production environment. Any packages which will be published after `1.31.1` can be considered as safe to use.

## Migration

-   `baza-nc-api`: We are recommending to add a request delay for executing NC requests and deploy it for all environment. It will lead to increased response time for most NC-related endpoints, but it will prevents most possible data losses which could happens due of current NC API issues

```typescript
// main-config.ts
const currentEnv = process.env.BAZA_ENV as BazaEnvironments;

if ([BazaEnvironments.Production, BazaEnvironments.UAT, BazaEnvironments.Preprod].includes(currentEnv)) {
    bazaNcApiConfig({
        requestsDelay: 150,
    });
} else {
    bazaNcApiConfig({
        requestsDelay: 350,
    });
}
```
