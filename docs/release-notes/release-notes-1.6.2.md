# Release 1.6.2

-   feat(baza-core): baza-account bootstrap fixes
-   feat(baza-core): crud-sort now has mapper option
-   feat(baza-core): cms vertical navbar has links now
-   feat(baza-core): crud list now automatically scrolls up after fetching data
-   feat(baza-core): added i18n for AccountsDeactivate ACL rule
