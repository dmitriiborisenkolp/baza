# Release 1.10.13

This release is huge, really.

## What's new

-   baza-core: 2FA authorisatrion with Google Authenticator is available now!
-   baza-core: Added new configuration methods for 2FA (`with2FAMethods`, `withEnabled2FAForCMS`, `disable2FA`) for `bazaApiBundleConfigBuilder` helper. It's now much easier to enable or disable 2FA.
-   baza-core: Added new configuration methods to control email verification token generator for `bazaApiBundleConfigBuilder` helper: `withRandomHexStringEmailVerificationTokenizer`, `withNDigitsEmailVerificationTokenizer`
-   baza-core: Export Accounts to CSV received updates and works differently depends on Account role.
-   baza-core: `BazaImage` and `BazaImageMulti` now has Cropper.js plugin. Images which are selected by user could be cropped & adjusted before uploading to AWS.
-   baza-core: `BazaImage` and `BazaImageMulti` now has optional `ratio` option which will be applied with Cropper.js plugin
-   baza-core: Added `{{ cmsLogoUrl }}` and `{{ webLogoUrl }}` mail template variables
-   baza-core: New `BazaSessionLock` service for API which allows to limit endpoint calls within account session.
-   baza-core: "Reset all password" feature in Account CMS is optional now and disabled by default. Use `bazaCmsBundleConfigBuilder` helper to enable it.
-   baza-nc: Transaction type will be automatically detected when starting new session. By default ACH will be used, but for specific case when user has no bank account lined, but has credit card transaction session will automatically set transaction type as Credit Card
-   baza-nc: It's not possible anymore to update Transaction Fee for offering if there are existing linked trades already in place
-   baza-nc: G1 Club renamed to Elite Investors
-   baza-nc: New optional feature "Testimonials" for Listings
-   baza-nc: Current Value field added to Offering control / Offering tab. Financial DTO was removed, because it was containing only single Current Value field.
-   baza-nc: Total Return percents added to `/stats` endpoint
-   baza-nc: New flag "Display in Details" added for Schema field. This flag should indicates should field be displayed in Details section or not in FE. New flag could be displayed only for static types like Text, Number or HTML.
-   baza-nc: `/session` and `/submit` endpoint now has Session lock. API will not allow to request endpoints multiple times when previous one are unfinished within current account scope.
-   baza-nc: Total Return and Percentage Return added to Portfolio API and sandbox app.

## Bugfixes

-   baza-core: Added validation for AdminEmail CC in Registry
-   baza-core: Fixed issues with bootstrap default account on initial API start
-   baza-core: Fixed typo for deactivating account in CMS
-   baza-core: Re-selecting same file in upload controls will trigger uploads now (before it was not working properly)
-   baza-core: Fixed visual glitches with modals opened when CMS page is scrolled down
-   baza-core: Fixed typos in error messages for Account API
-   baza-content-types: Deleted category now instantly disappear from the Categories List
-   baza-content-types: Fixed "Name" field in Contacts CMS
-   baza-nc: Fixed issues with importing Offering to CMS
-   baza-nc: Fixed issues with purchasing last available shares
-   baza-nc: Fixed error message when deleting Tax Document
-   baza-nc: Fixed issues with `%` sign in Transaction Fee field
-   baza-nc: Fixed issues with set up Transaction Fee as `0`. Also, it's not possible anymore to set Transaction Fee as negative value.

## Breaking changes

-   baza-core: `Stg` environment renamed to `Stage` for Nest and Angular applications. Update `.env` and `*.environment.ts` files for stage environments and make changes in deployment about it.
-   baza-core: `Development` environment removed in favour to `Local`. Rename `development.env` to `local.env` and set `BAZA_ENV=development` to `BAZA_ENV=` in `local.env`
-   baza-core: `BAZA_AUTH_2FA_DEFAULT_METHODS` global constant is removed in favour to new API config builder methods
-   baza-core: Fixed multiple issues with exporting Accounts to CSV
-   baza-nc: /stats endpoint works differently. Read about changes in section below.
-   baza-nc: all G1 Club endpoints renamed to use `/baza-ng-integration/elite-investors/` prefix instead of `/baza-ng-integration/g1` prefix.
-   baza-nc: Fixed typing & API documentation for `status` and `ncOfferingStatus` fields of Offering DTO
-   baza-nc: All E2E fixtures moved to `BazaNcApiE2eModule` module. API E2E module should includes `BazaNcApiBundleModule` module to work with fixtures provided by baza-nc packages.
-   baza-nc: `/limitsForPurchase` endpoints now counts Transaction Fees and requires `offeringId` in Request DTO.
-   baza-nc: **while it's possible to set Transaction Fee as decimal value, API does not working properly on some specific values.** We will target these issues in next release, but please use Transaction Fee very carefully.

## baza-nc-web libraries

-   baza-nc-web-purchase-flow: JPG support added on additional info popup, label change and file browser fixed
-   baza-nc-web-purchase-flow: First time payment button disabled bug fixed
-   baza-nc-web-purchase-flow: Typo fix on purchase flow
-   baza-nc-web-purchase-flow: Amendments to share picker WHEN message "You have reached the maximum amount of shares per user." is displayed to Investor
-   baza-nc-web-purchase-flow: fixed issues with additional /session calls between purchase flow steps
-   baza-nc-web-purchase-flow: Shares Picker now works correctly with Minumal Amount value
-   baza-nc-web-purchase-flow: Updated CreditCard validation logic to use right keys and calculation + other changes around Credit Card modal
-   baza-nc-web-purchase-flow:
-   baza-nc-web-purchase-flow:
-   baza-nc-web-purchase-flow:

## Stats endpoint reworks

Stats endpoint from baza-nc packages received rework.

Context: API operates with 3 types of shares:

-   Free shares
-   Purchased shares
-   Reserved shares - shares which are reserved during purchase flow

Before: /stats endpoint didn't count reserved shares in calculations
After: /stats endpoint counts reserved shares in calculations

It may be considered as breaking change, and target projects should check does purchasing last shares works or not, and if it's not, fixes should be applied on FE level.

Additionally, /stats endpoints has additional fields about amount of reserved shares for current users and for other users.

## Dwolla Sandbox

Baza repository now has sandbox application to play with Dwolla. We're using it to develop `baza-dwolla` packages and play with funding sources, customers, `dwolla.js` plugin or any other Dwolla API.

New yarn commands added:

-   `yarn dwolla-sandbox-api:start`
-   `yarn dwolla-sandbox-cms:start`

## Baza Repository updates

-   Applied changes around `Stg-to-Stage` and removing `Development` environments
-   E2E tests now executes in smart way. Baza NC tests are very heavy, and we run now tests only for affected libraries.

## Other notes

-   baza-core: All methods & configuration options of `bazaApiBundleConfigBuilder` helper are covered with TSDocs
-   baza-nc: `Financial` DTO was removed
-   baza-nc: `/stats` endpoint has increased kind of security around values (it's not possible to get negative values for some fields anymore)
-   baza-nc: `/destroy` session endpoint now works much faster and more stable.
-   baza-nc: trade session TTL has additional ~30 seconds threshold to be destroyed

## JIRA

-   [BAZA-532](https://scalio.atlassian.net/browse/BAZA-532) Can't Import an Offering to CMS by using Offering ID from NC admin
-   [BAZA-551](https://scalio.atlassian.net/browse/BAZA-551) Property Pool with existing transactions should not be allowed to update pool value
-   [BAZA-678](https://scalio.atlassian.net/browse/BAZA-678) Can't buy the last available share
-   [BAZA-378](https://scalio.atlassian.net/browse/BAZA-378) Rename BazaEnvironments.Stg to BazaEnvironments.Stage
-   [BAZA-406](https://scalio.atlassian.net/browse/BAZA-406) Add AdminEmail CC form validation
-   [BAZA-410](https://scalio.atlassian.net/browse/BAZA-410) 2 Factor authentication via google authenticator
-   [BAZA-494](https://scalio.atlassian.net/browse/BAZA-494) CMS - To rename G1 class to Elite Investors
-   [BAZA-499](https://scalio.atlassian.net/browse/BAZA-499) CMS - Deleted category does not instantly disappear from the Categories list
-   [BAZA-508](https://scalio.atlassian.net/browse/BAZA-508) Confirmation message when deleting a tax document is wrong
-   [BAZA-531](https://scalio.atlassian.net/browse/BAZA-531) Bugs on Import Offering by NC Offering ID
-   [BAZA-677](https://scalio.atlassian.net/browse/BAZA-677) To add '%' sign to Transaction Fee field in CMS
-   [BAZA-638](https://scalio.atlassian.net/browse/BAZA-638) Testimonial per Offering
-   [BAZA-790](https://scalio.atlassian.net/browse/BAZA-790) Fix ncOfferingStatus field typings from BazaNcOfferingEntity
-   [BAZA-791](https://scalio.atlassian.net/browse/BAZA-791) Fix accounts bootstrap on initial API start for local environments
-   [BAZA-795](https://scalio.atlassian.net/browse/BAZA-795) Fix issues with exporting Accounts to CSV
-   [BAZA-796](https://scalio.atlassian.net/browse/BAZA-796) Purchasing shares from Listing with POSITIVE OR NEGATIVE "Transaction Fees" containing DECIMAL PART causes HTTP Status Code 500. Verified Investor can NOT move further
-   [BAZA-799](https://scalio.atlassian.net/browse/BAZA-799) Separate E2E fixtures to E2E modules for baza-nc-integration packages
-   [BAZA-800](https://scalio.atlassian.net/browse/BAZA-800) Separate E2E fixtures to E2E modules for baza-nc-api packages
-   [BAZA-801](https://scalio.atlassian.net/browse/BAZA-801) Separate E2E fixtures to E2E modules for baza-core-api packages
-   [BAZA-809](https://scalio.atlassian.net/browse/BAZA-809) Baza-NC: Autodetect transaction type when create a new purchase flow session
-   [BAZA-817](https://scalio.atlassian.net/browse/BAZA-817) Add "Display in Details section" field for Schema fields
-   [BAZA-818](https://scalio.atlassian.net/browse/BAZA-818) 'Trade status should be in Created' NC error appears when trying to buy last share
-   [BAZA-439](https://scalio.atlassian.net/browse/BAZA-439) Contacts - wrong field name on View form
-   [BAZA-784](https://scalio.atlassian.net/browse/BAZA-784) CMS - To change the text under Current Value field and to move the field down
-   [BAZA-689](https://scalio.atlassian.net/browse/BAZA-689) Implementation of crop selection in CMS for uploaded images
-   [BAZA-839](https://scalio.atlassian.net/browse/BAZA-839) Update SSN document upload label
-   [BAZA-838](https://scalio.atlassian.net/browse/BAZA-838) Submit Payment button is disabled after linking a payment method for the first time
-   [BAZA-824](https://scalio.atlassian.net/browse/BAZA-824) Typo on Purchase details when min purchase = 1
-   [BAZA-773](https://scalio.atlassian.net/browse/BAZA-773) CMS - left menu panel glitch when modals are opened
-   [BAZA-812](https://scalio.atlassian.net/browse/BAZA-813) Additional /session request on Purchase Flow with 409 error
-   [BAZA-434](https://scalio.atlassian.net/browse/BAZA-434) Disable reset all passwords feature
-   [BAZA-794](https://scalio.atlassian.net/browse/BAZA-794) CMS - remove pre-defined value in Transaction Fee field of Listing
-   [BAZA-797](https://scalio.atlassian.net/browse/BAZA-797) To disable Next button on Purchase Details when the minimum amount is higher than remaining shares
-   [BAZA-714](https://scalio.atlassian.net/browse/BAZA-714) Total Return
-   [BAZA-735](https://scalio.atlassian.net/browse/BAZA-736) Card limits: Text change on Card limits
-   [BAZA-755](https://scalio.atlassian.net/browse/BAZA-755) Modal window "Over 5k with bank & card linked" is NOT triggered correctly above limit "Total" >= $5,000.01
-   [BAZA-764](https://scalio.atlassian.net/browse/BAZA-764) Label Adjustments after Purchase Flow
-   [BAZA-698](https://scalio.atlassian.net/browse/BAZA-698) Mobile - To add Personal Info to Purchase Details
-   [BAZA-720](https://scalio.atlassian.net/browse/BAZA-720) Fixes in CSV Report
-   [BAZA-634](https://scalio.atlassian.net/browse/BAZA-634) Disable default two-factor authentication in skeleton
-   [BAZA-576](https://scalio.atlassian.net/browse/BAZA-576) Remove "Development" environment in favour of "Local"
-   [BAZA-716](https://scalio.atlassian.net/browse/BAZA-716) CMS - to add Current Value to Listings
-   [BAZA-760](https://scalio.atlassian.net/browse/BAZA-760) The bottom label 'Fee' appears with lower-case - 'fee'
-   [BAZA-761](https://scalio.atlassian.net/browse/BAZA-761) Unable to upload Tax Documents in CMS
-   [BAZA-618](https://scalio.atlassian.net/browse/BAZA-618) Payment Methods step - UI bugs on different states
-   [BAZA-696](https://scalio.atlassian.net/browse/BAZA-696) CMS - Need a solution for uploading/re-uploading

## PRs (Baza)

-   https://github.com/scalio/baza/pull/553
-   https://github.com/scalio/baza/pull/549
-   https://github.com/scalio/baza/pull/544
-   https://github.com/scalio/baza/pull/542
-   https://github.com/scalio/baza/pull/537
-   https://github.com/scalio/baza/pull/536
-   https://github.com/scalio/baza/pull/534
-   https://github.com/scalio/baza/pull/533
-   https://github.com/scalio/baza/pull/532
-   https://github.com/scalio/baza/pull/531
-   https://github.com/scalio/baza/pull/530
-   https://github.com/scalio/baza/pull/528
-   https://github.com/scalio/baza/pull/527
-   https://github.com/scalio/baza/pull/524
-   https://github.com/scalio/baza/pull/521
-   https://github.com/scalio/baza/pull/519
-   https://github.com/scalio/baza/pull/518
-   https://github.com/scalio/baza/pull/517
-   https://github.com/scalio/baza/pull/513
-   https://github.com/scalio/baza/pull/512
-   https://github.com/scalio/baza/pull/511
-   https://github.com/scalio/baza/pull/510
-   https://github.com/scalio/baza/pull/509
-   https://github.com/scalio/baza/pull/508
-   https://github.com/scalio/baza/pull/506
-   https://github.com/scalio/baza/pull/505
-   https://github.com/scalio/baza/pull/504
-   https://github.com/scalio/baza/pull/501
-   https://github.com/scalio/baza/pull/500
-   https://github.com/scalio/baza/pull/498
-   https://github.com/scalio/baza/pull/495
-   https://github.com/scalio/baza/pull/494
-   https://github.com/scalio/baza/pull/493
-   https://github.com/scalio/baza/pull/492
-   https://github.com/scalio/baza/pull/489
-   https://github.com/scalio/baza/pull/485
-   https://github.com/scalio/baza/pull/484
-   https://github.com/scalio/baza/pull/483
-   https://github.com/scalio/baza/pull/482
-   https://github.com/scalio/baza/pull/480
-   https://github.com/scalio/baza/pull/479
-   https://github.com/scalio/baza/pull/476
-   https://github.com/scalio/baza/pull/475
-   https://github.com/scalio/baza/pull/474
-   https://github.com/scalio/baza/pull/473
-   https://github.com/scalio/baza/pull/472
-   https://github.com/scalio/baza/pull/468
-   https://github.com/scalio/baza/pull/466
-   https://github.com/scalio/baza/pull/465
-   https://github.com/scalio/baza/pull/464
-   https://github.com/scalio/baza/pull/463
-   https://github.com/scalio/baza/pull/460
-   https://github.com/scalio/baza/pull/446
-   https://github.com/scalio/baza/pull/444
-   https://github.com/scalio/baza/pull/475

## PRs (Sandbox)

-   https://github.com/scalio/baza/pull/550
-   https://github.com/scalio/baza/pull/548
-   https://github.com/scalio/baza/pull/546
-   https://github.com/scalio/baza/pull/545
-   https://github.com/scalio/baza/pull/543
-   https://github.com/scalio/baza/pull/541
-   https://github.com/scalio/baza/pull/540
-   https://github.com/scalio/baza/pull/539
-   https://github.com/scalio/baza/pull/538
-   https://github.com/scalio/baza/pull/529
-   https://github.com/scalio/baza/pull/526
-   https://github.com/scalio/baza/pull/525
-   https://github.com/scalio/baza/pull/522
-   https://github.com/scalio/baza/pull/516
-   https://github.com/scalio/baza/pull/515
-   https://github.com/scalio/baza/pull/507
-   https://github.com/scalio/baza/pull/497
-   https://github.com/scalio/baza/pull/496
-   https://github.com/scalio/baza/pull/491
-   https://github.com/scalio/baza/pull/490
-   https://github.com/scalio/baza/pull/488
-   https://github.com/scalio/baza/pull/487
-   https://github.com/scalio/baza/pull/486
-   https://github.com/scalio/baza/pull/481
-   https://github.com/scalio/baza/pull/478
-   https://github.com/scalio/baza/pull/471
-   https://github.com/scalio/baza/pull/470
-   https://github.com/scalio/baza/pull/469
-   https://github.com/scalio/baza/pull/467
-   https://github.com/scalio/baza/pull/461
-   https://github.com/scalio/baza/pull/459
-   https://github.com/scalio/baza/pull/458
-   https://github.com/scalio/baza/pull/457
-   https://github.com/scalio/baza/pull/456
-   https://github.com/scalio/baza/pull/455
-   https://github.com/scalio/baza/pull/454
-   https://github.com/scalio/baza/pull/453
-   https://github.com/scalio/baza/pull/450
-   https://github.com/scalio/baza/pull/445

## PRs (DevOps)

-   https://github.com/scalio/baza/pull/503
-   https://github.com/scalio/baza/pull/514
-   https://github.com/scalio/baza/pull/452
-   https://github.com/scalio/baza/pull/451
-   https://github.com/scalio/baza/pull/449
-   https://github.com/scalio/baza/pull/443

## PRs (Deps)

-   https://github.com/scalio/baza/pull/535
-   https://github.com/scalio/baza/pull/477
