# Release 1.20.0

⚠️ **Baza 1.19.0 is deprecated due of critical bug in Purchase Flow. Please use Baza 1.20.0 instead of 1.19.0, but don't forget to perform Migration actions from Baza 1.19.0.**

The release is short 1-week sprint only with mostly bugfixes and changes around International Investors. We're planning to release Dwolla Purchase API and new Dwolla Transfer (Cash-In) API with upcoming 1.21.0 release.

### What's new

-   `baza-dwolla-web-purchase-flow`: We did a lot of changes for International Investors. Please check `Changes for International Investors` for details.
-   `baza-core-cms, baza-nc-cms`: Fixed issues with Date Range filters on Dividends CMS.
    -   A new field type `BazaTableFieldTypeDateUTC` is added.
    -   A new method `dateUTC` is now a part of `baza-crus-data.component.ts` to apply UTC formatting.
    -   A new field type DateUTC added to BazaTableFieldType enum.
    -   Relevant logic updated in baza-crud-component.html to apply UTC formatting.
-   `baza-nc-api`: Legacy bank account API has integration now with New Bank Account API.
-   `baza-nc-api`: We updated paths for `successfull-payment-client` email templates. Please check Migration section for changes.
-   `baza-nc-api`: We added ZIP code validation on BE side for US residents
    -   Validate zipcode for US addresses.
    -   Only accept 5 digit numeric zip code for US citizens.
-   `baza-nc-api`: `/baza-nc/bank-account/set` endpoint now can return multiple bank accounts depends on `types` request field.
-   `baza-dwolla-web-purchase-flow`: We display now Dwolla Balance now
    -   Investor is able to see the "Account Balance" option on the payment page
    -   If the investor doesn't have an existing Dwolla account, /touch endpoint would be called from FE upon clicking "Account Balance"
    -   If Dwolla account creation is unsuccessful, we would show the error message section.
-   `baza-core-api`: We finalized changes about updating default credentials for default Admin and User accounts:
    -   Default admin is `scalio-admin@scal.io`.
    -   Default user is `scalio-user@scal.io`.
    -   Default password remains same as before.
    -   Old `root@scal.io` and `user@scal.io` emails are deprecated and should not be used anymore.
-   `ng`: All `@Select` decorators was replaced with `store.select` calls for SSR support: [https://github.com/ngxs/store/issues/1854#issuecomment-1167955493]()
-   `baza-nc-web-purchase-flow, baza-dwolla-web-purchase-flow`: We now display error message about KYC only for Dwolla library.
-   `baza-nc-integration-api`: Search API is now working with `Select` schema fields properly. Fixed issues with `ranges` endpoint.
-   `baza-nc-integration-api`: Added support for `dateCreatedAt` and `dateUpdatedAt` fields for filtering and ranges with Search API.
-   `baza-nc-integration-api`: Added `dateCreatedAt` and `dateUpdatedAt` fields to Listing DTO.
-   `baza-core-api`: Added "Terms of Service" (bazaCommon.links.termsOfService) registry config to specify link to Terms of Service The new registry value available via `/baza-core/baza-registry/publicSchema` endpoint.
-   `baza-core-api`: Added "Privacy Policy" (bazaCommon.links.privacyPolicy) regisry config to specify link to Privacy Policy. The new registry value available via `/baza-core/baza-registry/publicSchema` endpoint.
-   `baza-core-api`: Added validation for Device Tokens to prevent duplicates
    -   Unique index added to `token` field in `account_device_token_entity` to guarantee data integrity.
    -   New repository method was added to get a device token by just the type and the token itself.
    -   Existing repository method to get a device token by account was renamed to `getDeviceTokenByAccount`.
    -   400 status (BAD_REQUEST) is returned if an FCM/Apple device token that belongs to account `a` is tried to be attached to account `b`.
        -   If the validation is triggered via the CMS, a notification is shown.
        -   If the validation is triggered via HTTP headers, the action is ignored.

## Bugfixes

-   `baza-nc-web-purchase-flow, baza-dwolla-web-purchase-flow`: Fixed critical issue with displaying Plaid.
-   `baza-nc-web-purchase-flow, baza-dwolla-web-purchase-flow`: Fixed the bug where unhandled exceptions were not being caught properly on FE in purchase flow upon clicking "Submit Payment" button.
-   `baza-nc-web-purchase-flow, baza-dwolla-web-purchase-flow`: Fixed the bug where KYC error was not being displayed properly in Dwolla flow. Now, the error appears fine on 1st step (purchase details) personal information section in purchase flow, based on the "NC" flag value in investor status. If the value is "Disapproved", we show the error message section else it's hidden.
-   `baza-nc-web-purchase-flow, baza-dwolla-web-purchase-flow`: Fixed the bug where International Invetors was not able to see message about $5000 maximum purchase limit
-   `baza-nc-api`: Fixed API documentation for `/baza-nc/bank-account/set` endpoint
-   `baza-nc-cms`: Fixed issue when CMS was displaying success message on attempts to touch Dwolla Customer when it's actually failed
-   `baza-core-cms`: Fixed overflow issue on the CMS layout when the user scrolls horizontally on lower screen resolutions. The fix removes the scroll behavior on page level and implement it on inner layout (content) level.

## Breaking changes

-   `baza-nc-api`: We are using now different email templates for successful payment notifications depends on with/without offering. Please check Migration section for changes.
-   `baza-nc-api`: `/baza-nc/bank-account/set` now returns array of Bank Accounts, not single Bank Account DTO only

## Migration

-   Update `successful-payment-client` email templates. Rename it to ``successful-payment` and check out Baza repository for email template contents:

```handlebars
apps/api/i18n/en/mail-templates/baza-nc/dividend/successful-payment/with-offering/successful-payment.html.hbs:

{{#> webLayoutTextEn }}
    <p>Congratulations {{ ../investorAccount.userFullName }}! You have successfully received {{ ../amount }} USD as dividends from your investment {{ ../offering.ncOfferingName }}! We thank you for your business with us!</p>
{{/webLayoutTextEn}}
```

```handlebars
apps/api/i18n/en/mail-templates/baza-nc/dividend/successful-payment/with-offering/successful-payment.text.hbs:

{{#> webLayoutHtmlEn }}
    Congratulations {{ ../investorAccount.userFullName }}! You have successfully received {{ ../amount }} USD as dividends from your investment {{ ../offering.ncOfferingName }}! We thank you for your business with us!
{{/webLayoutHtmlEn}}
```

```handlebars
apps/api/i18n/en/mail-templates/baza-nc/dividend/successful-payment/without-offering/successful-payment.html.hbs:

{{#> webLayoutTextEn }}
    <p>Congratulations {{ ../investorAccount.userFullName }}! You have successfully received {{ ../amount }} USD as dividends from your investment! We thank you for your business with us!</p>
{{/webLayoutTextEn}}
```

```handlebars
apps/api/i18n/en/mail-templates/baza-nc/dividend/successful-payment/without-offering/successful-payment.text.hbs:

{{#> webLayoutHtmlEn }}
    Congratulations {{ ../investorAccount.userFullName }}! You have successfully received {{ ../amount }} USD as dividends from your investment! We thank you for your business with us!
{{/webLayoutHtmlEn}}
```

-   We recommend removing `BazaNcInvestorUpdateIsForeignInvestorMigration002` and replace it with new migration script. We experienced issues with previous migration script not updating all investors:

```typescript
import { Injectable } from '@nestjs/common';
import { BazaLogger, BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { QueryRunner } from 'typeorm';
import { BazaNcDwollaCustomerService, BazaNcInvestorAccountRepository, INVESTOR_ACCOUNT_RELATIONS } from '@scaliolabs/baza-nc-api';

@Injectable()
export class BazaNcInvestorUpdateIsForeignInvestorMigration003 implements BazaMigration {
    constructor(
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly dwollaService: BazaNcDwollaCustomerService,
        private readonly logger: BazaLogger,
    ) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(queryRunner: QueryRunner): Promise<void> {
        const investors = await this.investorAccountRepository.repository.find({
            relations: INVESTOR_ACCOUNT_RELATIONS,
        });

        for (const investor of investors) {
            try {
                investor.isForeignInvestor = !(await this.dwollaService.canHaveDwollaCustomer(investor.user.id));
                await this.investorAccountRepository.save(investor);
            } catch (error) {
                this.logger.warn(
                    `[BazaMigratiosn] [BazaNcInvestorUpdateIsForeignInvestorMigration002] isForeignInvestor flag set failed for user ${investor.user.id} and investor account id of ${investor.id}`,
                );
                this.logger.warn(error);
            }
        }
    }

    down(queryRunner: QueryRunner): Promise<void> {
        return Promise.resolve(undefined);
    }
}
```

## `DateUTC` CRUD Column Type added

-   A new field type `BazaTableFieldTypeDateUTC` is added
-   A new method `dateUTC` is now a part of `baza-crus-data.component.ts` to apply UTC formatting
-   A new field type `DateUTC` added to `BazaTableFieldType` enum
-   Relevant logic updated in `baza-crud-component.html` to apply UTC formatting

### How To Apply UTC Date Formatting In CMS Grids

If we have to use UTC date formatting for any grid, we just have to use the following syntax when defining columns:

```typescript
columns: [
    {
        field: 'date',
        title: this.i18n('columns.date'),
        type: {
            kind: BazaTableFieldType.DateUTC,
        },
    },
];
```

## Legacy Bank Account System integration with new API

Legacy endpoints now also creates Cash-Out Bank Account entry. It could be used for easier migration later to NBAS.

This integration will keep only 1 account per user, even if user requested to set / update bank accounts multiple times.

-   LBAS (Legacy Bank Account System) is now fully integrated with NBAS (New Bank Account System)
-   Bank Accounts set by LBAS will be automatically exported to NBAS. There will be always only single account created or updated in NBAS after any actions with LBAS
-   Bank Accounts Flags for Investor Account will be correctly updated using both LBAS and NBAS
-   Sync tool is now also works correctly with latest changes. All investors on projects which are using old approach could be easily synced with NBAS
-   Covered with Integration Tests
-   Services, methods & data-access Services marked with @deprecated flags
-   API documentation also covered with @deprecated flags
-   No migration or breaking changes are required for projects which will not going to use NBAS; Projects which would like to use NBAS in future need to use "Sync Bank Accounts" in Investor Account CMSs

## Changes for International Investors

This release covers adding a UI experience for international investors which mainly focuses the purchase flow. Since currently we have 2 purchase flow libraries i.e. NC (existing, exportable to other projects) and Dwolla (new, not-exportable), these changes were made in the Dwolla library.

As part of the release, following changes were made specifically for foreign investors:

-   The endpoint `baza-nc/investor-account/status` was integrated in purchase flow to find out if the currently active investor is foreign/international or not. Based on this flag value, we show/hide different sections in purchase flow
-   Hide the bank account option in the "Payment Method" section on payment page and associated options in add/edit payment method popups
-   Add a new info. label just below CC section as per Zeplin
-   Update the edit tooltip text in payment methods section
-   Added new form validation behavior in "Add Card" popup (in case no CC was already attached)
-   In case user has already added CC, then select that by default and also display "Payment Method Fee" in summary section by default i.e. his session is created against "Credit Card" by default
-   Hide the "Dividend Alert" (yellow) banner on top which appears if only CC is linked (1st time, new user) for international investors
-   Hide the radio button when opening the Payment Modal, which has a list of payment methods available. For this case, we would only see added CC
-   If an investor is international but has only BA added, the payment method section should appear as if no payment method is added i.e. "Card" section should appear, submit button should be disabled and edit tooltip should be hidden.

## Code Conventions Changes

We're now using `@ApiOkResponse` decorator and we do not use `@HttpCode(HttpStatus.OK)` in controllers anymore. We're recommending to update your projects to follow same code conventions as we do in Baza now.

**Before:**

```typescript
class MyController {
    @HttpCode(HttpStatus.OK)
    @ApiResponse({
        status: HttpStatus.OK,
        type: SomeDto,
    })
    async someMethod(): Promise<SomeDto> {}
}
```

**After:**

```typescript
class MyController {
    @ApiOkresponse({
        type: SomeDto,
    })
    async someMethod(): Promise<SomeDto> {}
}
```

## JIRA

-   [BAZA-1335](https://scalio.atlassian.net/browse/BAZA-1335) Offerings: (Coming Soon) When User Clicks on Notify Me for coming soon properties and refresh or navigate back, Old state is dsiplayed
-   [BAZA-1341](https://scalio.atlassian.net/browse/BAZA-1341) isForeign flag is not updated for old existing users prior to this implementation
-   [BAZA-1346](https://scalio.atlassian.net/browse/BAZA-1346) Plaid link is not working in Link Bank account flow
-   [BAZA-1022](https://scalio.atlassian.net/browse/BAZA-1022) Date range filters for Dividends are not working as per the selected date range
-   [BAZA-1130](https://scalio.atlassian.net/browse/BAZA-1130) Rework /baza-nc/purchase-flow bank account endpoints
-   [BAZA-1137](https://scalio.atlassian.net/browse/BAZA-1137) International Investors - UI Experience (Purchase Flow)
-   [BAZA-1226](https://scalio.atlassian.net/browse/BAZA-1226) BE - Add mail template variables for baza-nc/dividend/successful-payment-client emails
-   [BAZA-1227](https://scalio.atlassian.net/browse/BAZA-1227) BE - Use different email templates for successfull dividend notifications when dividend is paid w/o Offering specified
-   [BAZA-1240](https://scalio.atlassian.net/browse/BAZA-1240) Remove HttpStatus.OK and replace it with ApiOkResponse everywhere
-   [BAZA-1259](https://scalio.atlassian.net/browse/BAZA-1259) Time-Out Error On Purchase Flow
-   [BAZA-1260](https://scalio.atlassian.net/browse/BAZA-1260) BE: Issue in failed transaction
-   [BAZA-1277](https://scalio.atlassian.net/browse/BAZA-1277) Zip code validation (BE)
-   [BAZA-1284](https://scalio.atlassian.net/browse/BAZA-1284) Request parameter in set default bank account in the API Documentation needs correction
-   [BAZA-1285](https://scalio.atlassian.net/browse/BAZA-1285) Ability to view account balance (FE)
-   [BAZA-1292](https://scalio.atlassian.net/browse/BAZA-1292) Ranges API is not retrieving all of the schema properties in the response
-   [BAZA-1301](https://scalio.atlassian.net/browse/BAZA-1301) Replace default admin and user emails for Baza
-   [BAZA-1302](https://scalio.atlassian.net/browse/BAZA-1302) Success message shows as dwolla id created even when its actually failed due to any reason
-   [BAZA-1303](https://scalio.atlassian.net/browse/BAZA-1303) Update NGXS Store Selector References for SSR Support in Baza app
-   [BAZA-1304](https://scalio.atlassian.net/browse/BAZA-1304) Update logic for error message about personal information on purchase flow step 1
-   [BAZA-1305](https://scalio.atlassian.net/browse/BAZA-1305) Page reload on Purchase Flow Step 1 ends up with blank white screen
-   [BAZA-1318](https://scalio.atlassian.net/browse/BAZA-1318) On Purchase Flow - KYC Error Message Banner Change
-   [BAZA-1323](https://scalio.atlassian.net/browse/BAZA-1323) Baza NC Integration Search API - Add ability to sort by dateCreatedAt
-   [BAZA-1328](https://scalio.atlassian.net/browse/BAZA-1328) Removal of NC KYC error message on Purchase Flow
-   [BAZA-1331](https://scalio.atlassian.net/browse/BAZA-1331) Create registry items for Terms of Service and Privacy Policy on CMS
-   [BAZA-1332](https://scalio.atlassian.net/browse/BAZA-1332) Same FCM/Apple device token that is linked to one account is being allowed to link with other accounts
-   [BAZA-1340](https://scalio.atlassian.net/browse/BAZA-1340) international user is unable to see message about $5000 maximum purchase limit
-   [BAZA-1321](https://scalio.atlassian.net/browse/BAZA-1321) CMS - left side menu UI glitches when pages are scrolled horizontally

## PRs

-   https://github.com/scalio/baza/pull/915
-   https://github.com/scalio/baza/pull/921
-   https://github.com/scalio/baza/pull/920
-   https://github.com/scalio/baza/pull/895
-   https://github.com/scalio/baza/pull/829
-   https://github.com/scalio/baza/pull/854
-   https://github.com/scalio/baza/pull/863
-   https://github.com/scalio/baza/pull/886
-   https://github.com/scalio/baza/pull/852
-   https://github.com/scalio/baza/pull/912
-   https://github.com/scalio/baza/pull/896
-   https://github.com/scalio/baza/pull/892
-   https://github.com/scalio/baza/pull/890
-   https://github.com/scalio/baza/pull/874
-   https://github.com/scalio/baza/pull/877
-   https://github.com/scalio/baza/pull/887
-   https://github.com/scalio/baza/pull/803
-   https://github.com/scalio/baza/pull/908
-   https://github.com/scalio/baza/pull/901
-   https://github.com/scalio/baza/pull/880
-   https://github.com/scalio/baza/pull/908
-   https://github.com/scalio/baza/pull/902
-   https://github.com/scalio/baza/pull/906
-   https://github.com/scalio/baza/pull/904
-   https://github.com/scalio/baza/pull/897
