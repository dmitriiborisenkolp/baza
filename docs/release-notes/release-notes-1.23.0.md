# Release 1.23.0

The release is covering design changes with LBAS (Legacy Bank Account System) and NBAS (New Bank Account System) usages. Please read official guide of Bank Account API usage described below or on Baza Documentation Portal: [Web, IOS – Usage of New Bank Account API
](https://baza-docs.test.scaliolabs.com/investment-platform/10-bank-account-api-usage)

Additionally, we're working on `whitelist` option enabled for all Requests, but currently you should disable it on target projects.

## What's new

-   `baza-core-api`: **Whitelist enabled by default for `ValidationPipe`**. Read more at Nest.JS documentation page: [https://docs.nestjs.com/techniques/validation]()
-   `baza-dwolla-web-purchase-flow`: Updates on NBAS / LBAS usages performed with the Release. Please check `NBAS Integration to Dwolla Web Libraries ` section for details
-   `baza-dwolla-web-purchase-flow`: This Release will bring back the NC ACH bank account option (Manual) for investors so that they can purchase shares. It also includes major refactoring done for Dwolla PF library.
-   `baza-nc-api`: LBA integration with NBA is now an opt-in feature. The feature is disabled by default, but could be enabled with `bazaNcApiConfig` helper (`enableLegacyBankSyncWithNBAS` config)
-   `baza-nc-api`: Added new `ncAchBankAccount` method to New Bank Account API. The method is returning NC ACH account which will be used for next purchase (using NC ACH method)
-   `baza-nc-api`: Investor Account DTO now contains additional flags: `isBankAccountNcAchLinked` and `isBankAccountCashInLinked`.
-   `baza-nc-api`: Flag `isBankAccountLinked` will be set as true in case if Bank Account was set with NC ACH or with adding Cash-In Bank Account.
-   `baza-nc-api`: Added new `secure` option for `add` and `linkOnSuccess` endpoints. Setting this flag as `true` (which is **MANDATORY** for production) will mask Account and Routing Numbers in Database. You will be able to export Bank Account immediately to NC or Dwolla, but it will be impossible to export Bank Account later to external services.
-   `baza-nc-web-purchase-flow`, `baza-dwolla-web-purchase-flow`: minor changes for 1st step (VF) in light of recent IVP team feedback
    -   The heading "Residential Address" is now a `<div>` element with custom class name for is customizable for other projects
    -   The copy below "I don't have SSN" checkbox is now updated with new class name `uploaded-file__descr` which has default style and is customizable for other projects
-   `baza-core`: Removed `ng-responsive` resources, services, components and anything else related to `ng-responsive`
-   `baza-nc-integration-api`: Fixed issues with Schema API when whitelist option is enabled
-   `baza-nc-integration-api`: Fixed `updateId` should not be changed (i.e. Schema should not be marked as Breaking Changed) with Tabs updates
-   `baza-core-api`: Added helpers to disable or enabled `whitelist` option for `ValidationPipe`: `bazaApiBundleConfigBuilder.withWhitelist` (enables) and `bazaApiBundleConfigBuilder.disableWhitelist()` (disables)
-   `baza-core`: Added Terminus integration. Terminus could be used as Health-Check tool instead of `GET /` status endpoint. Read more about Terminus: [https://github.com/godaddy/terminus]()

## Bugfixes

-   `baza-core-api`: **Fixed issues which prevents to use migrations feature overall (there was missing await in app.init() call). Migrations should now works correctly**
-   `baza-nc-api`: Fixed Session not being updated on "Submit Payment"
-   `baza-nc-api`: Fixes a bug caused by the fields totalCents and transactionFeesCents. Read more about changes in [PR-1016](https://github.com/scalio/baza/pull/1016)
-   `baza-nc-api`: Zip Code validation for us citizenship added for updating investor personal info cms. For getting investor personal info endpoint (cms) there will be an error thrown in case there is no NC account created for the investor\
-   `baza-nc-api`: Fixed incorrect error message "Password must be longer than or equal to 3 characters" is displayed for non-existing user
-   `baza-nc-web-purchase-flow`, `baza-dwolla-web-purchase-flow`: Fixed Verification Flow - Page reload on both verification flow Steps removes "back to the listing details" link from the top. You can find a very detailed description of bugfix in [PR-988](https://github.com/scalio/baza/pull/988)

## Breaking changes

-   `baza-core-api`: **Due of `whitelist` option being enabled by default, it may lead to different and unexpected issues, especially with API implemented outside of Baza.**
-   `baza-nc-api`: (New Bank Account API) There is a difference now between not specifying `setAsDefault` property and specify `setAsDefault` with `false` value. If `setAsDefault` is not specified, API will automatically select and set bank account, if non existing. For `setAsDefault: false` case API will not attempt to select any account as default, even if there are no default accounts available yet

## Migration

-   Install Terminus NPM Package: `yarn add @nestjs/terminus`
-   Install Nest.JS Mapper Types NPM Package: `yarn add @nestjs/mapped-types`
-   Please run "Sync Payment Methods" tool for all existing Investor Accounts after deployment
-   You should add a migration for securing Bank Account details in Database. The migration also fixes potential "duplicate error" issues which may happens due of current data.

```typescript
// Dont' forget to include it to MigrationModule!
// migrations/src/lib/migrations/005-baza-nc-bank-account-secure.migration.ts

import { Injectable } from '@nestjs/common';
import { BazaLogger, BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments, maskString } from '@scaliolabs/baza-core-shared';
import { QueryRunner } from 'typeorm';
import { BazaNcBankAccountEntity, BazaNcBankAccountRepository } from '@scaliolabs/baza-nc-api';

let bankAccountRepository: BazaNcBankAccountRepository;

@Injectable()
export class BazaNcBankAccountSecureMigration005 implements BazaMigration {
    constructor(private readonly logger: BazaLogger, readonly injectedBankAccountRepository: BazaNcBankAccountRepository) {
        bankAccountRepository = injectedBankAccountRepository;
    }

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.connection.transaction('READ COMMITTED', this.performMigration());
    }

    private performMigration(): () => Promise<void> {
        return async () => {
            const ulids: Array<string> = await (async () => {
                const qb = await bankAccountRepository.repository.createQueryBuilder().select(['ulid']).getRawMany();

                return (qb || []).map((next: Pick<BazaNcBankAccountEntity, 'ulid'>) => next.ulid);
            })();

            for (const ulid of ulids) {
                const entity = await bankAccountRepository.findByUlid(ulid);

                if (ulid) {
                    entity.secure = true;
                    entity.accountNumber = maskString(entity.accountNumber);
                    entity.accountRoutingNumber = maskString(entity.accountRoutingNumber);

                    await bankAccountRepository.save([entity]);

                    this.logger.debug(`Secured Bank Account "${entity.ulid}" (${entity.accountName})`);
                }
            }
        };
    }
}
```

-   Add new environment variables for Terminus integration
    -   `BAZA_HEALTHCHECK_MEMORY_HEAP_MAX_MBYTES=2048`
    -   `BAZA_HEALTHCHECK_MEMORY_RSS_MAX_MBYTES=2048`
-   ⚡️ **We enabled `whitelist` option for Baza by default**. The option is mandatory from security perspective, as far as it automatically secures API from different security vulnerabilities like `prototype` injections. We strictly recommend disabling `whitelist` option with `bazaApiBundleConfigBuilder.withoutWhitelist()` helper for production environment and perform migration actions to prepare your API to work correctly with enabled `whitelist` option
    -   Make sure that all DTOs (including Request, Response, Entity and any other type of DTOs) has defined `@ApiModelProperty()`, `class-validator`, `class-transformers` and especially `@Type` decorators for all fields.
    -   Make sure that all Array fields have both `@Type()` and specified `{ each: true }` option for `class-transform` decorators
    -   Enable `whitelist` option for Test environment with `bazaApiBundleConfigBuilder.withWhitelist()` **helper with next Baza 1.24.0 release** (Do not enable it with current release, we're still working on this feature!)
    -   Perform regression testing on Test environment
    -   When everything is working ok, enable `whitelist` option for all environments. We are recommending to wait for next Baza 1.24.0 release or even 1.25.0 release, because we are also may find different issues with enabled `whitelist` option (and fix it)
    -   **Ignoring this migration step may lead to unresolvable data issues in Database!**

## Investor Account Flags Changes

Additionally please take attention on changes around isBankAccount\* flags family.

-   `isBankAccountLinked`: will be set as true if there is a Bank Account set with NC ACH or There is a Cash-In Bank Account Linked (This one is important; There may be various cases for non-US and US-investors)
-   (NEW)`isBankAccountNcAchLinked` flag will be set as true when NC ACH Bank Account is currently set and available to use
-   `isBankAccountCashInLinked` will be set when there is Cash In bank account is available. Additionally this account should be set as default.
-   `isBankAccountCashOutLinked` flag will be set when there is Cash Out bank account is available. Additionally this account should be set as default.

## Usage of New Bank Account API (Official Flow)

The Guide covers API usage for UI implementation of Payment Methods component with ability to Purchase Shares using Account Balance (Plaid), NC ACH (Manual & Plaid) and linking Cash-Out Bank Account to Withdraw dividends. The following implementation will use different bank accounts for Purchase Shares using NC ACH, Purchasing Shares using Account Balance and Withdrawing Dividends using Cash-Out Bank Accounts. The implementation will not use multiple bank accounts per each type feature due of masking and securing Account and Routing Numbers in Database.

### General

We're using **LBA** as Legacy Bank Account API and **NBA** for New Bank Account API in Guide below.

-   NC ACH (Set Manual Bank Account) should be set with **NBA** (`add` method) with exports to NC only,`setAsDefault` as `false`, `static` as `false` and `secure` as true. It will immediately allow you to use Bank Account for purchasing shares using NC ACH API.
-   NC ACH (Using Plaid Integration) should be set with **NBA** (`link` and `linkOnSuccess` endpoints) with `exports` option to `['NC']` only and with `setAsDefault` as `false`, `static` as `false` and `secure` as `true`. It will immediately allow you to use Bank Account for purchasing shares using NC ACH API
-   Using `setAsDefault` as `false` will prevent overwrite of Cash-In Bank Account, which will be used for Account Balance flow.
-   Both NC ACH options will work correctly with Legacy API which is used for `baza-nc-web-*` libraries.
-   Both **NBA** usages above **MUST NOT** be used with `static: true` flag, because it will drop previously saved Cash In bank account.
-   Displaying Bank Account Details (Account, Purchase Flow) should be implemented with using `ncAchBankAccount` method instead of using `default('cash-in`)` method. This approach will guarantee that User will see action Bank Account for next Purchase Flow session.
-   Cash-In Bank Account (with Plaid) should be set with **NBA** with exports to `['Dwolla']` only and with `secure` , `setAsDefault` and `static` flags as true . Usage of `static` flag here is optional, but as far we would like to not have multiple bank accounts in nearby future, you should use this flag to clean up bank accounts sometimes.
-   Cash-In Bank Account (with Manual Bank Account) is not a supported case and will not be in focus in nearby future.
-   Cash-Out Bank Account should be set using **NBA** with `['Dwolla']` exports only and with `setAsDefault`, `static` and new `secure` flags enabled. It will always keep single bank account only for Cash Out and it will mask bank account details in Database.
    Reading Cash-Out Bank Account should be implemented using **NBA** with `default('cash-out')` endpoint.
-   Every NBA usage **MUST** be with `secure` flag enabled. A new flag is allowing exporting Bank Account immediately when adding bank account using `add` or `linkOnSuccess` endpoints, but it will not be possible to use `export` method on existing bank accounts later.

### Using this approach, we:

-   Completely removes all sensible data from DB
-   We will primarily use NBA only. LBA usage will be completely unused.
-   We're not using **LBA** in latest UI implementations, but you may use **LBA** only for old UI implementations. **LBA** usage in pair with **NBA** usage will work without conflicts to each other.
-   Two-way sync between LBA and NBA is an opt-in feature now; We may decide to completely remove it, if we will not find it useful
-   Allows you to use Cash In bank account type effectively for both NC ACH and Account Balances scenarios. There will be different accounts set for each operation.

## NBAS Integration to Dwolla Web Libraries

In light of new changes made in context of [BAZA-1469](https://scalio.atlassian.net/browse/BAZA-1469) regarding NBA (New Bank Account System) API, relevant changes have been made on FE Dwolla Library as well.

**Note:**
We're using LBA as Legacy Bank Account API and NBA for New Bank Account API in description below.

**This PR covers adding the following points:**

1. The LBA is now completely removed Dwolla PF library. We are exclusively using NBA for all endpoints now
2. The endpoints to get & set NC ACH (Manual) accounts is now using NBA
3. The request payload for `/linkOnSuccess` endpoint to attach Cash-In bank account (Dwolla Plaid) is now updated as per new flags
4. Relevant components where LBA data was being used is now updated with models from NBA
5. Purchase flow was re-tested. Manual bank account & update is working fine as expected. For testing purposes, "Add Funds" button was temporarily added back & attaching & updating cash-in account also works
6. As per new context of NBA, the NC ACH (Manual) and Cash-In/Cash-Out accounts (through Dwolla Plaid) will be completely isolated

## Bringing back NC ACH Bank Option in Dwolla PF

As part of the Release, following changes were made:

-   The ability to manually add bank account is now available for investors (both US national and International). However, for international investors, they cannot use account balance as payment method
-   The selection of default payment method is now updated as per new requirements. If Dwolla balance is available and has enough funds, it's given preference else NC ACH, and finally Credit Card (if limit is not exceeded)
-   Investor would be able to manually add bank account exactly like in NC flow, both from payment page & also from Edit popup
-   Investors will now be able to see 3 payment methods available, in case they have account balance with enough funds to initiate purchase of shares
-   Updated checks added to enable/disable and show/hide various sections including submit button, edit tooltip and payment methods section, both on payment screen & edit popup
-   A total of 15+ cases were handled for various situations as per user story
-   As per updated requirements, the ability to create Dwolla account, attach/update cash-in account, transfer funds from cash-in to Dwolla balance and "Add Funds" button is now hidden. These functionalities maybe reverted back so code is in place.

### Dwolla PF Library - Code Refactoring - [BAZA-1416](https://scalio.atlassian.net/browse/BAZA-1405)

As part of this PR, the following code refactoring changes were made as well:

-   The existing `payment-methods.service.ts` is now renamed as `dwolla-shared.service.ts` and available under data-access/services folder
-   Shared variables/methods created in this service and extra/duplicated code removed from other places including `methods.component`, `payment.component` etc.
-   Submit button enable/disable logic moved out of `methods.component` into the parent `payments.component` through usage of `@Output` event emitter
-   Utility functions were added to this service allowing them to be used from any page/component using Dwolla PF library
-   A lot of extra code, duplicate observable data streams and redundant functionality was removed from `payment.component` which streamlined overall flow and increase performance
-   Observable data streams within `methods.component` were optimized to skip if null observables are received, reset variables on every pass and check for different cases
-   Updated code to follow "single data stream" sequence which will be invoked upon 1st page load and subsequent changes on the page due to any events e.g. adding new CC

## JIRA

-   [BAZA-1463](https://scalio.atlassian.net/browse/BAZA-1463) Ability to disable two-way synchronisation between Legacy Bank Account and New Bank Account API
-   [BAZA-1469](https://scalio.atlassian.net/browse/BAZA-1469) Update Dwolla Endpoints
-   [BAZA-1465](https://scalio.atlassian.net/browse/BAZA-1465) Integrate IVP Feedback In Verification Flow
-   [BAZA-1457](https://scalio.atlassian.net/browse/BAZA-1457) Purchase Flow - "Could Not Start Session" error appears on Purchase Flow for some specific decimal values of purchase Price
-   [BAZA-1256](https://scalio.atlassian.net/browse/BAZA-1256) Remove baza-ng-responsive libraries & resources
-   [BAZA-1455](https://scalio.atlassian.net/browse/BAZA-1455) CMS - Schema Tabs - Newly added tabs undo all the changes after we save them and go back
-   [BAZA-1453](https://scalio.atlassian.net/browse/BAZA-1453) Refactor code in Dwolla PF library components
-   [BAZA-1454](https://scalio.atlassian.net/browse/BAZA-1454) Add a configuration to Enable or Disable Whitelist option
-   [BAZA-1205](https://scalio.atlassian.net/browse/BAZA-1205) Terminus Integration
-   [BAZA-1447](https://scalio.atlassian.net/browse/BAZA-1447) getInvestorAccountPersonalInfo returns 500 internal server error for investor id which is newly signed up and has not submitted investor information to NC
-   [BAZA-1449](https://scalio.atlassian.net/browse/BAZA-1449) validation checks missing for personal information in updateInvestorAccountPersonalInfo
-   [BAZA-1421](https://scalio.atlassian.net/browse/BAZA-1421) Change Password - Validation does not work for the field "New password" at account -> password
-   [BAZA-1372](https://scalio.atlassian.net/browse/BAZA-1372) Login - Incorrect error message "Password must be longer than or equal to 3 characters" is displayed for non existing user
-   [BAZA-1237](https://scalio.atlassian.net/browse/BAZA-1237) Enable Whitelist option for Validation pipe
-   [BAZA-1385](https://scalio.atlassian.net/browse/BAZA-1385) Dwolla: Purchase Flow: Bring back NC manual bank linking
-   [BAZA-1405](https://scalio.atlassian.net/browse/BAZA-1405) FE - Ability to Link Bank Account Manually Using NC
-   [BAZA-1428](https://scalio.atlassian.net/browse/BAZA-1428) Testing of story tasks and regression testing
-   [BAZA-1242](https://scalio.atlassian.net/browse/BAZA-1242) Add Nest.JS Mapped Type NPM packages
-   [BAZA-1283](https://scalio.atlassian.net/browse/BAZA-1283) Able to set cash-out bank ULID as default with cash-in type in the request
-   [BAZA-1284](https://scalio.atlassian.net/browse/BAZA-1284) Request parameter in set default bank account in the API Documentation needs correction
-   [BAZA-1364](https://scalio.atlassian.net/browse/BAZA-1364) Get dwolla customer verification status on touch endpoint
-   [BAZA-1369](https://scalio.atlassian.net/browse/BAZA-1369) An endpoint to return specific investor information
-   [BAZA-1416](https://scalio.atlassian.net/browse/BAZA-1416) Create a Dwolla purchase flow shared service
-   [BAZA-1429](https://scalio.atlassian.net/browse/BAZA-1429) Verification Flow - Page reload on both verification flow Steps removes "back to the listing details" link from the top
-   [BAZA-1440](https://scalio.atlassian.net/browse/BAZA-1440) Mark Dwolla libraries in release 1.22.0
-   [BAZA-1456](https://scalio.atlassian.net/browse/BAZA-1456) Baza web - Sorting is reverted for all the listings

## PRs

-   https://github.com/scalio/baza/pull/1029
-   https://github.com/scalio/baza/pull/1028
-   https://github.com/scalio/baza/pull/1025
-   https://github.com/scalio/baza/pull/1023
-   https://github.com/scalio/baza/pull/1022
-   https://github.com/scalio/baza/pull/1018
-   https://github.com/scalio/baza/pull/1016
-   https://github.com/scalio/baza/pull/1015
-   https://github.com/scalio/baza/pull/1014
-   https://github.com/scalio/baza/pull/1013
-   https://github.com/scalio/baza/pull/1011
-   https://github.com/scalio/baza/pull/1010
-   https://github.com/scalio/baza/pull/1008
-   https://github.com/scalio/baza/pull/1007
-   https://github.com/scalio/baza/pull/1005
-   https://github.com/scalio/baza/pull/1004
-   https://github.com/scalio/baza/pull/986
-   https://github.com/scalio/baza/pull/988

## PRs (dependabot)

-   https://github.com/scalio/baza/pull/1019
-   https://github.com/scalio/baza/pull/1009
