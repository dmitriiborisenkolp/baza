# Release 1.28.4

Added `isAccreditedInvestor` flag. You should perform additional migrations to set values for `isAccreditedInvestor` flag.

A new flag is available under `InvestorAccountDTO`, i.e. using `/baza-nc/investor-account/current` or bootstrap endpoints (under `investorAccount` object).

## What's new

-   `baza-nc-api`: added `isAccreditedInvestor` flag to Investor Account DTO

## Migration

-   `baza-nc-api`: You should sync all investor accounts for setting up `isAccreditedInvestor` flag value. Alternatively you can create a migration script:

```typescript
import { Injectable } from '@nestjs/common';
import { BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaNcInvestorAccountRepository, InvestorAccountSyncService } from '@scaliolabs/baza-nc-api';

@Injectable()
export class BazaNcIsAccrInvestorMigration007 implements BazaMigration {
    constructor(
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly investorAccountSync: InvestorAccountSyncService,
    ) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(): Promise<void> {
        const ids = await this.investorAccountRepository.findAllInvestorAccountIds();

        for (const investorAccountId of ids) {
            const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);

            if (investorAccount.northCapitalAccountId && investorAccount.northCapitalPartyId) {
                await this.investorAccountSync.updateInvestorAccount(investorAccount.user, {
                    userId: investorAccount.userId,
                    ncPartyId: investorAccount.northCapitalPartyId,
                    ncAccountId: investorAccount.northCapitalAccountId,
                });
            }
        }
    }
}
```

## JIRA

-   [BAZA-1852](https://scalio.atlassian.net/browse/BAZA-1852) Add "isAccreditedInvestor" flag in Bootstap

## PRs

-   https://github.com/scalio/baza/pull/1279
