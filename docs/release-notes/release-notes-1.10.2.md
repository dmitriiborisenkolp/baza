# Release 1.10.2

**What's new:**

-   Client Name Feature
-   Credit Card utils now wil not use moment.js library anymore
-   JSON Encoder issue with NC Transact API client-side Fix
-   CRUD Query DTO will response with number types for index / size / page fields
-   Customer.IO Mail Transport
-   Response Type for Deactive Accounts methods fixed for BazaAccountDataAccess Service
-   `ProjectService` from baza-core-api now has additional methods to get current project name, project codename and client name
-   `BazaProjectNgService` from baza-core-ng added which will helps you to get current project name, project codename and client name in Angular Applications

**Migration**

-   Recommended: Set Client Name where `configureBazaProject` called in your project
-   Recommended: Replace `getBazaProjectName` / `getBazaProjectCodeName` calls with `ProjectService` usage (API) or `BazaNgProjectService` usage (Web, CMS). Use these services to get client name if need.
-   Recommended: Replace hardcoded client names ("Commonwealth Team", "My Income Property") with `{{ clientName }}` template variable in mail templates

Read more about Project & Client names here: https://baza.test.scaliolabs.com/overview/02-project

**PR's:**

-   https://github.com/scalio/baza/pull/75
-   https://github.com/scalio/baza/pull/74
-   https://github.com/scalio/baza/pull/72
-   https://github.com/scalio/baza/pull/71
-   https://github.com/scalio/baza/pull/70
-   https://github.com/scalio/baza/pull/68
