# Release 1.7.0

## BREAKING CHANGES:

-   Baza Form Builder form definitions reworked. All nz-zorro related properies moved under `props` or `directiveProps` partial objects

## What's new:

-   New release contains huge breaking changes for baza-form-builder and baza-crud packages.
-   Baza Form Builder now allows to use any of available Ng-Zorro component properties, not only some limited set as before
-   Baza Form Builder now completely allows to create & inject custom form controls or static components
-   `BazaCmsCrudBundleModule` – the bundle module should be used for CRUD / FormBuilder modules instead of manual list of baza-core modules. Mostly you will need to use bundle module for every CMS section in project.

## New Baza Form builder configuration syntax

https://gist.github.com/dborisenkowork/e1fbd45db3180092c7589f8c5b274889

-   Every control now accepts component props as partial object.
-   You can check code example above to check new syntax
-   Also, code examples

## Custom Baza Form builder components

Baza Form Builder can accept any kind of ControlValueAccessor components. DI and modules/services resolving will also works correctly; you can use your own services, modules or anything else without worriyng about DI issues.

Additionally, you can put any other components, not only ControlValueAccessor. These components called as "Static Components" inside Baza Form Builder implementation.

Before using your custom components / form control components, you will need to register component for Baza Form builder.

## Define new component / form control component with fully-functional typings

The guide describes how to create your custom components _with fully functional_ typings. If you want to make your life simplier, you don't need to follow all these steps; You will need only to create your component and register component for Baza Form builder:0

This is simpliest way to use your own components:

```
// That's all. Use MyComponent class as identifier for your forms.
registerFormControl<any>(MyComponent, {
    component: MyComponent,
    enableNzFormContainer: false,
});
```

### 1. Create component

For form control components you will need to implement ControlValueAccessor.

### 2. Define Enum of your custom components

Form Builder use unique (per each component type) identifier to detect which component you need to use for the field. You need to define you custom components list to use it in form builder:

```
export enum ExampleCmsCoreUiFormFields {
    ExampleCityDeliveries = 'ExampleCityDeliveries',
}
```

Note: you can actually skip this step and use Component class as identifier.

### 3. Define typings for component

You need to define type + props + any additional options definition of your new components:

```
export interface ExampleCmsCoreUiCityDeliveries extends BazaFormControl<ExampleCmsCoreCityDeliveriesComponent, ExampleCmsCoreUiFormFields> {
    // Type from Step 1. You can use here "ExampleCmsCoreCityDeliveriesComponent" actually instead of enums
    type: ExampleCmsCoreUiFormFields.ExampleCityDeliveries;

    // Define properties which can be passed to component.
    props?: Partial<ExampleCmsCoreCityDeliveriesComponent>;

    // Example of additional options
    someExampleOption: boolean;
}
```

### 4. Register component for Baza Form Builder.

Baza Form builder should know that your component should be used for your component identifier.

```
registerFormControl<ExampleCmsCoreUiFormFields>(ExampleCmsCoreUiFormFields.ExampleCityDeliveries, {
    component: ExampleCmsCoreCityDeliveriesComponent,
    enableNzFormContainer: false,
});
```

For static components use `registerStaticComponent` helper.

### 5. Done

Now you can use your component:

```
private formBuilder(form: FormGroup): BazaFormBuilder<ExampleCmsCoreUiComponents | BazaCoreFormBuilderComponents> {
    return {
        layout: BazaFormBuilderLayout.WithTabs,
        contents: {
            tabs: [
                {
                    title: 'Custom Example',
                    contents: {
                        fields: [
                            {
                                type: ExampleCmsCoreUiFormFields.ExampleCityDeliveries,
                                formControlName: 'fieldCustomComponent',
                            },
                        ],
                    },
                },
            ],
            nzTabPosition: 'top',
        },
    };
}
```

## Using Form Builder inside custom form components

Example: You have array DTO's inside Entity which you want to use as shared form component.

You can use form builder itself inside your custom components.

Here you can find example: https://gist.github.com/dborisenkowork/d3784f52a8fc73921c60e92a9c1e494f
