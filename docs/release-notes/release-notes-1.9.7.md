# Release 1.9.7

**What's new:**

-   Ability to customize actions buttons for CMS Modal/Slider forms: https://scalio.atlassian.net/browse/BAZA-298

**PR's:**

-   https://github.com/scalio/baza/pull/57
