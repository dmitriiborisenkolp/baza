# Release 1.26.0

The Release is adding multiple new tools for Investor Account CMS, additional Customisation options for FE libraries, introduces new Operations endpoint and many more.

We are also applying more changes to support `whitelist` validation flag. We may recommend you enable `withWhitelist` configuration with future releases.

We are going to migrate to the latest version of Node, Angular, Cypress and Nx.Dev tools with Baza 1.27.0 release! While we were stuck on very much outdated Node.JS versions and we were stuck on pretty much old Angular 12, with new release of Baza we'll update our packages and perform migrations. We'll include a guide for target projects how to migrate to new stack.

## What's new

-   `baza-nc-web-purchase-flow`: added the ability to customize and rename the "call to action" names for the available payment method options on the Purchase Flow (NC library). Read more in "Customize and Sort Payment Method Sections" section.
-   `baza-dwolla-web-purchase-flow`: added a new feature that enables hybrid usage of the `DwollaSharedServices` utilities: `isDwollaAvailable`, `isNCBankAccountAvailable`, and `isCardAvailable` by having a single parameter that can be of type `BazaNcBootstrapDto` or the specific object type.
    -   `bankAccountResponse` was removed from the `account-balance` component because `initData` is being used to get the value of `isNCBankAccountAvailable`.
    -   `creditCardResponse` was removed from the `account-balance` component because `initData` is being used to get the value of `isCardAvailable`.
-   `baza-nc-api`, `baza-dwolla-api`: added new `/baza-nc/transfer/reprocess` endpoint for reprocessing failed Cash-In transfers
-   `baza-nc-integration-api`: Fixed a bug when `on-trade-updated.event-handler` was incorrectly triggers on wrong event (leading to additional error logs)
-   `baza-nc-api`: Added `isDwollaAvailable` status flag in `BazaNcDwollaTouchResponse` DTO
-   `baza-nc-api`: Added new `/baza-nc/operation/list` endpoint for displaying multiple operations (Purchases, Dividends, Withdraws and Transfers) in single list
-   `baza-nc-api`: added `list` endpoints for `withdraw` (`/baza-nc/withdraw/list`) and `transfer` (`/baza-nc/transfer/list`) Public APIs
-   `baza-nc-api`: `withdraw` and `transfer` modules received slight reworks, and now `BazaNcWithdrawService` and `BazaNcTransferService` services has more tools
-   `baza-dwolla-api`: `list` endpoints for `withdraw` and `transfers` now can filter both by Statuses and Types
-   `baza-web-ui-components`: Baza now has support for new customizable marketing components that can be used on target projects. This release is 1st of the series of PRs that will cover adding new marketing components. Read more in "Marketing Hero Banner Component" section.
-   `docs`: Added new documentation section for account payment and payout methods under under `web-development/customization`
    -   New section "Account Components Customization" was created.
    -   Documentation for account balance was added.
    -   Documentation for bank account was added.
    -   Documentation for credit card was added.
    -   Documentation for bank account as payout method was added.
-   `baza-nc-api`: Added `identity` option for `linkOnSuccess` callback. It should be bypassed from `onSuccess` callback from Plaid Integration to help API to identify bank details. If `identity` is not passed, API will try to fetch details using Plaid Identity API, but it will be possible only if used Plaid account has Identity product purchased.
-   `baza-dwolla-web-purchase-flow`: updated configurations after code refactoring and some internal refactoring in libraries. Read more in "Configurations Update and Refactoring" section
-   `baza-nc-api`: Added CMS endpoints for "Retry Dwolla Customer Verification" feature
-   `baza-nc-cms`: Added "Retry Dwolla Customer Verification" feature to Investor Account CMS. Read more in "Retry Dwolla Customer Verification CMS" section.
-   `baza`: Added `@Type` class-validator for Request DTOs. We had some `@ValidateNested` decorators in some `DTOs` , without any `@Type` decorator, that actually did not work correctly. This PR added `@Type` decorator to each of them.
-   `baza-nc-cms`: Added option to Edit Investor Account using Investor Account CMS.
    -   New drop-down option was added to the investor accounts CMS page.
    -   New form is added to CMS (investor accounts) to allow updating investor's personal information.
        -   Implemented logic to mask the SSN field.
        -   Implemented logic for dependant combo boxes (country -> state)
        -   According to the [user story,](https://scalio.atlassian.net/browse/BAZA-1361) the "SSN" field is disabled if scenarios 2, 4, 5, or 6 are `true`.
        -   According to the user story, the "Date of Birth" field is disabled if Dwolla verification or KYC has passed.
    -   Implemented new CMS endpoint `listStates` to return a list of states by country name.
-   `baza-web-ui-components`: added support for new customizable marketing bullets component that can be used on target projects. Read more in "Marketing Bullets Section" section.
-   `baza-dwolla-web-purchase-flow`: Performed some refactoring actions:
    -   All headings are replaced with `div` tags and specific heading styles are inherited inside their CSS classes on Dwolla web purchase & verification libraries.
    -   Grid system implementation is replaced from nz attributes to mixin `flex-col-width` on Dwolla web purchase & verification libraries.
    -   Verified the current design is not affected.
    -   Verified components don't have scoped styles.
    -   Class `payment-block__alert-warning` was removed because warning alerts style was standardized [here](https://github.com/scalio/baza/pull/1012/files#diff-acaba1a28617d1cd069145e3364849519c5a5083d400d38a429622805cc2a808R830)

## Bugfixes

-   `baza-nc-web-verification-flow`: FIXED: a new user signs up and country is selected by default but state validation was not applied.
-   `baza-nc-integration-api`: FIXED: notifyUsers - 500 internal server error instead of proper error message is shown for invalid or nonexistent listingId in the request
-   `baza-dwolla-api`: FIXED: Current account balance throws 500 internal server error when `isDwollaAvailable` is `true` with Dwolla Customer Status as `retry`
-   `baza-core-api`: FIXED: Offering update template - Client name is not taken from the value specified in CMS registry
-   `baza-nc-api`: Fixed "max 5 bank accounts" error. Autofix for existing Investor Accounts included. We'll apply additional fixes around it later.
-   `baza-dwolla-web-purchase-flow`: FIXED: 500 error message was thrown if a call to endpoint `/baza-nc/dwolla/current` was made on Dwolla purchase flow library and Account, but Dwolla itself was unavailable for investor i.e. isDwollaAvailable flag value was false.
-   `baza-nc-integration-api`: changed the sort order for `null` values in `search` API. The fix modify Listing sort order logic for `null` values such that it appears at last for Descending order and first for Ascending order
-   `baza-nc-api`, `baza-dwolla-api`: Add a new intermediate status as "Document Processing" in DwollaCustomerStatus
    -   Added `Document_Processing` status in `DwollaCustomerStatus` enum.
    -   Modified dwolla touch endpoint such that when the existing status is "Document Processing", it shouldn’t update the status from dwolla.
    -   Updated `nc-investor-accounts-cms.component.ts` file to support new intermediate status.
-   `baza-dwolla-web-verification-flow`, `baza-nc-web-verification-flow`: fixed radio button validations for Investor page in both NC and Dwolla VF libraries.
    -   Radio buttons are now highlighted if required validation is thrown
    -   Relevant error messages are also displayed
    -   Both of these don't show if any of the radio button in group is selected
-   `baza-dwolla-web-purchase-flow`: fixed the bug where "Add Funds" was not working properly in "Account" component.
-   `baza-nc-api`: Fixed tag in logs for Baza NC Webhook events (it was displayed as "Dwolla" webhooks)

## Breaking changes

-   `baza-nc-api`: method `getDefaultCashoutBankAccountFundingSrouceId ` renamed to `touchWithdrawDwollaWallet`
-   `baza-nc-api`: There was missed `BazaNc` prefixes for some resources of Withdraw and Transfer operations. If you have any imports about it, update it with common conventions
-   `baza-nc-web-utils`: library was renamed to `baza-web-utils`. Please update `package.json` and update any existing references in your applications
-   `baza-nc-web-ui-components`: library was renamed to `baza-web-ui-components`. Please update `package.json` and update any existing references in your applications
-   `baza-dwolla-web-purchase-flow`: The config for `DwollaPurchasePaymentComponent` component has been updated. This means when passing config to `app-purchase-payment-dwolla`, the individual properties `accountBalanceTitle`, `accountBalanceHint` and `accountBalancePrefix` are now removed and merged within main config, as per the following format:

```typescript
defaultConfig: PaymentConfig = {
    methods: {
        showPurchaseDividendMessage: true,
        accBalanceConfig: {
            title: 'Account Balance',
            hint: 'Fund your account',
            prefix: 'Current balance:',
        },
    },
    steps: {
        links: {
            termsOfService: {
                extLink: {
                    link: 'bazaCommon.links.termsOfService',
                    text: 'Terms of Service',
                    isCMSLink: true,
                },
            },
            eftDisclosure: {
                appLink: {
                    commands: ['/eft-disclosure'],
                    text: 'EFT Disclosure',
                },
            },
        },
    },
};
```

-   `baza-dwolla-web-purchase-flow`: The config for `DwollaAccountBalanceComponent` component has been updated. This means when passing config to `<app-dwolla-account-balance>` the config should be updated as above. The individual properties `title`, `hint` and `prefix` are now removed and merged within main config, as per the following format:

```typescript
defaultConfig: AccBalanceConfig = {
    title: 'Account Balance',
    hint: 'Fund your account',
    prefix: 'Current balance:',
    links: {
        contact: {
            extLink: {
                link: 'bazaContentTypes.contacts.email',
                text: 'contact us',
                isCMSLink: true,
                isMailLink: true,
            },
        },
    },
};
```

Source: [Migrating to Cypress V11](https://nx.dev/packages/cypress/documents/v11-migration-guide)

## Migration

-   `baza-nc-web-utils`, `baza-nc-web-ui-components`: Libraries was renamed to `baza-web-utils` and `baza-web-ui-components`. Please update `package.json` and update any existing references in your applications
-   `baza-nc-integration-api`: Endpoint `/baza-nc-integration/portfolio/transactions` marked as deprecated. You should migrate to new `/baza-nc/operation/list` endpoint as far as we will remove portfolio transactions endpoint soon completely
-   `baza-nc-api`: it's recommended to update `linkOnSuccess` callback usages on your target projects to include Identity information using `identity` field (i.e. `metadata` from second argument of client `onSuccess` callback). Baza will use Plaid Identity API if `identity` field is not passed, but it has some service costs.

## Marketing Hero Banner Component

Baza now has support for new customizable marketing components that can be used on target projects. This release is 1st of the series of PRs that will cover adding new marketing components.

As part of this release, following changes were made:

-   A new hero banner component is now available with customization ability
-   Default styles are available in `_hero.less` file with custom classes added for each element for easy style overriding
-   Default route is now /home instead of /items which will have all marketing components loaded in it
-   Clicking on "Scalio" logo in header will now navigate to home page
-   Hero component is responsive on all screens and styling is done with mobile-first design approach
-   The marketing components are part of `baza-web-ui-components` library
-   Documentation added on `baza-docs` portal with complete guidance on how to use and customize banner component

### Screenshots:

**Mobile:**
https://www.screencast.com/t/eZ3rokZiLk

**Tablet:**
https://www.screencast.com/t/VHYUkox9VcJb

**Desktop:**
https://www.screencast.com/t/iUlHtx1n

### Documentation URL:

Here's a complete guide through which we can use & customize the hero banner component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/01-hero-banner

## Marketing Bullets Section

Baza now has support for new customizable marketing bullets component that can be used on target projects.

-   A new bullets component is now available with customization ability
-   Default styles are available in `_bullets.less` file with custom classes added for each element for easy style overriding
-   A minimum of 2 and maximum of 4 string values can be passed as input, which will be rendered as info bullets on FE
-   Bullets component is responsive on all screens and styling is done with mobile-first design approach
-   The marketing components are part of `baza-web-ui-components` library
-   Documentation added on `baza-docs` portal with complete guidance on how to use and customize bullets component

### Screenshots:

**Mobile:**
https://www.screencast.com/t/GKZfDnUCH

**Tablet:**
https://www.screencast.com/t/6tkRQeUPTGIZ

**Desktop:**
https://www.screencast.com/t/KHtt37brJv

### Documentation URL:

Here's a complete guide through which we can use & customize the bullets component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/03-bullets

## Operations Endpoint

We're adding a new `/baza-nc/operation/list` endpoint which returns list of all operations (Investments, Cash-In, Cash-Out and Dividends). We created a new endpoint as a replacement for `/baza-nc-integration/portfolio/transactions` which was has multiple issues and which was way too much locked by backward compatibility. Consider to migrate to new endpoint in short time, because we marked old implementation as deprecated and we will remove it soon.

You can filter operations list by `dateTo`, `dateFrom` and `types` fields.

**Example Request:** `/baza-nc/operation/list?size=5&index=1`

**Example Response:**

```json
{
    "pager": {
        "index": 1,
        "size": 5,
        "total": 17
    },
    "items": [
        {
            "date": "2022-12-23T08:20:35.075Z",
            "amount": "20.00",
            "amountCents": 2000,
            "direction": "out",
            "payload": {
                "type": "Investment",
                "id": 1,
                "state": "PENDING_PAYMENT",
                "shares": 2,
                "feeCents": 0,
                "pricePerShareCents": 1000,
                "totalCents": 2000,
                "transactionFeesCents": 0,
                "canBeReprocessed": false
            }
        },
        {
            "date": "2022-11-29T00:26:31.917Z",
            "amount": "258.00",
            "amountCents": 25800,
            "direction": "out",
            "payload": {
                "type": "Withdraw",
                "ulid": "01GMZ1EJP3RNR7SAQ81NEV1HGK",
                "amount": "258.00",
                "canBeReprocessed": false
            }
        },
        {
            "date": "2022-11-28T16:26:31.917Z",
            "amount": "257.00",
            "amountCents": 25700,
            "direction": "in",
            "payload": {
                "type": "Transfer",
                "ulid": "01GMZ1EJNZKH46S2FJRZG9FW8H",
                "amount": "257.00",
                "canBeReprocessed": false
            }
        },
        {
            "date": "2022-11-28T08:26:31.917Z",
            "amount": "156.00",
            "amountCents": 15600,
            "direction": "out",
            "payload": {
                "type": "Withdraw",
                "ulid": "01GMZ1EJNW1PWP6DCJXZ5BG127",
                "amount": "156.00",
                "canBeReprocessed": false
            }
        },
        {
            "date": "2022-07-01T14:28:17.727Z",
            "direction": "in",
            "amount": "12.00",
            "amountCents": 1200,
            "payload": {
                "type": "Dividends",
                "canBeReprocessed": false,
                "ulid": "01GMZ17BGT7M53HW6ENK96B0VN",
                "offeringTitle": "E2E NC Offering Fixture",
                "amount": "12.00"
            }
        }
    ]
}
```

### Data-Source for Operations endpoint

New Operations endpoint works on injectable Data-Source service. This Data-Source should be able to fetch list of all possible operations (Purchases, Dividends, Withdraws and Transfers) and returns specified range of these operation.

Baza provides common solution which is directly collects all operations from multiple sources. While it may be considered as stable solution, for users with really much amount of records it may work slowly. If you will have performance issues, you may cache operations in DB or somewhere else and replace default Data-Source solution with your own implementation. To do this, you should:

1. Create your injectable service with `BazaNcOperationDataSourceInterface` implementation
2. Inject your Data-Source implementation during `onApplicationBootstrap` lifecycle hook with `BazaNcOperationService.dataSource = ....` option

## Customize and Sort Payment Method Sections

-   New customization options for the NC purchase flow payment options were added. Now, the `PaymentMethodConfig` interface provides customization options for titles, captions, and order of payment options.
    -   The `title` field in `PaymentBankAccountConfig` and `PaymentCreditCardConfig` can be used to change the title of payment options.
    -   The `feeCaption` field in `PaymentCreditCardConfig` can be used to change the fee caption of a payment option
    -   The `order` field can be used to specify the position of a payment option relative to others
-   New generic directive `SortElementsDirective` was added and It provides the ability to rearrange the order of the elements in the DOM.

**Note:** These customization options can be used to change the configuration of the "Plaid" bank account, the "Manual" bank account, and the credit card payment options.

**Note:** The `order` field can be used to specify the position of the "Plaid" payment option in relation to the manual bank account option or other alternatives that could come out in the future.

**Note:** Similarly, the `order` field can be used to specify the position of the credit card payment option in relation to the bank account payment option or other alternatives that could come out in the future.

### How to specify the configuration options (guide for target projects)

The default configurations are as follows:

```typescript
paymentConfig: PaymentConfig = {
    paymentMethodConfig: {
        methodOptionsConfig: {
            bankAccount: {
                plaid: {
                    title: 'Link bank account',
                    order: 1,
                },
                manual: {
                    title: 'Manually add bank details',
                    order: 2,
                },
                order: 1,
            },
            creditCard: {
                manual: {
                    title: 'Add card',
                    feeCaption: 'Fee',
                },
                order: 2,
            },
        },
    },
};
```

#### Example (Purchase Flow)

If we want to switch places of the "Link bank account" and "Manually add bank details" options, and we want to update the titles, then the configuration would be as follows:

```typescript
paymentConfig: PaymentConfig = {
    paymentMethodConfig: {
        methodOptionsConfig: {
            creditCard: {
                manual: {
                    title: 'Custom name for card',
                    feeCaption: 'Custom Fee',
                },
                order: 1,
            },
            bankAccount: {
                manual: {
                    title: 'Custom name for manual bank',
                    order: 1,
                },
                plaid: {
                    title: 'Custom name for plaid bank',
                    order: 2,
                },
                order: 2,
            },
        },
    },
};
```

then, pass the object created above as an input property to the component `PurchasePaymentComponent`

```html
<app-purchase-payment [paymentConfig]="paymentConfig"></app-purchase-payment>
```

#### Example (Account - Payment & Payouts NC)

Since we're not exporting a parent component that hosts the bank account and credit card options, it's necessary to import the module `UtilsModule` from `@scaliolabs/baza-web-utils` in your own module.

```ts
import { UtilModule } from '@scaliolabs/baza-web-utils';

@NgModule({
    imports: [UtilsModule],
})
export class MyModule {}
```

Then, create a config object of type `PaymentMethodOptionsConfig` like the one above:

```ts
methodOptionsConfig: PaymentMethodOptionsConfig = {
    creditCard: {
        manual: {
            title: 'Custom name for card',
            feeCaption: 'Custom Fee',
        },
        order: 1,
    },
    bankAccount: {
        manual: {
            title: 'Custom name for manual bank',
            order: 1,
        },
        plaid: {
            title: 'Custom name for plaid bank',
            order: 2,
        },
        order: 2,
    },
};
```

After that, you must add the directive `SortElementsDirective` to the parent element that hosts both your `PaymentBankAccountComponent` and your `PaymentBankAccountComponent`. Additionally, you must add the `data-order` attribute to the first-level children that you want to sort and specify the value from the payment object config that was set above.

```html
<div appSortElements>
    <div [attr.data-order]="methodOptionsConfig?.bankAccount?.order">
        <app-payment-bank-account [config]="methodOptionsConfig.bankAccount"></app-payment-bank-account>
    </div>

    <div [attr.data-order]="methodOptionsConfig.creditCard?.order">
        <app-payment-card [config]="methodOptionsConfig.creditCard"></app-payment-card>
    </div>
</div>
```

After that, everything should be in place with the specified titles and captions

[Submit payment screenshot](https://user-images.githubusercontent.com/64181348/210887840-8981b8c8-7b6c-4da6-91f9-e799fc9ad878.png)
[Update payment screenshot](https://user-images.githubusercontent.com/64181348/210891911-4de256ca-db0a-46e4-9a7f-a9935e433d12.png)
[Account screenshot](https://user-images.githubusercontent.com/64181348/211083480-ccb9dbcc-731c-42b4-9c19-56213cfc758b.png)

**Note:** The `order` value should always be a positive integer and greater or equal to 1.

**Note:** Two or more different options shouldn't have the same `order` value (e.g. "plaid" and "manual" shouldn't both be set to 1 in the config).

## Configurations Update and Refactoring

-   Configurations were refactored to remove additional input properties and those are now merged within parent configs in Dwolla PF and Account components
-   Internal refactoring in Dwolla PF was done to clean references
-   Baza Documentation Portal was updated with updated configuration details

## Retry Dwolla Customer Verification CMS

This release integrates a new option in the Investor Account CMS to retry Dwolla Customer Verification. It generates two possible prompts based on the Dwolla customer status (`retry`, `document`).

-   Refactored warning hint to show them in the following order:
    1. DwollaCustomerVerificationStatus
    2. Dwolla customer ID (if `DwollaCustomerVerificationStatus` is `verified`)
    3. DwollaCustomerError
-   Added warning hints for verification statuses (retry, document, and suspended)
-   Created a new modal component `nc-investor-account-retry-cms` attached and generated from `nc-investor-accounts-cms.component`.
-   Created a new modal component `nc-investor-account-retry-document-cms` attached and generated from `nc-investor-accounts-cms.component`.
-   Implemented logic to handle verification for customers in `nc-investor-account-retry-cms`.
-   Implemented validation rules for file uploads.
-   Created new i18n resources for new modal components.

### Retry Dwolla customer verification for customers with the status `Retry`

![image](https://user-images.githubusercontent.com/64181348/208040062-2dacfe8c-40cb-4582-81df-e798438d3b9b.png)

![image](https://user-images.githubusercontent.com/64181348/208040167-ba9ecc8a-ab32-45a9-9a9a-7e09df6ea5d3.png)

### Retry Dwolla customer verification for customers with the status `Document`

![image](https://user-images.githubusercontent.com/64181348/208040391-eb3cb004-571c-45a7-806c-4e39a659e482.png)

![image](https://user-images.githubusercontent.com/64181348/208040426-9c039cea-0de9-49db-aa01-52c120eb5c86.png)

![image](https://user-images.githubusercontent.com/64181348/208040497-52958b6a-0f59-4ddb-b3a7-b1880785bf70.png)

![image](https://user-images.githubusercontent.com/64181348/208040732-0bfb42ba-26e4-48f2-a548-f5a2ed7a8afa.png)

## JIRA

-   [BAZA-1601](https://scalio.atlassian.net/browse/BAZA-1601) Customization ability for labels of "Payment Method" section
-   [BAZA-1598](https://scalio.atlassian.net/browse/BAZA-1598) Remove Dwolla Dividend Alert Component
-   [BAZA-1597](https://scalio.atlassian.net/browse/BAZA-1597) Update DSS Utility Functions
-   [BAZA-1079](https://scalio.atlassian.net/browse/BAZA-1079) BE - Reprocess the failed payment on Cash out
-   [BAZA-1587](https://scalio.atlassian.net/browse/BAZA-1587) Create Bullets Section
-   [BAZA-1578](https://scalio.atlassian.net/browse/BAZA-1578) Update /touch endpoint to include Dwolla available status flag
-   [BAZA-1534](https://scalio.atlassian.net/browse/BAZA-1534) Create new endpoint to support all transaction types
-   [BAZA-1576](https://scalio.atlassian.net/browse/BAZA-1576) Renaming baza-nc-web-ui-components and baza-nc-web-util libraries
-   [BAZA-1575](https://scalio.atlassian.net/browse/BAZA-1575) NC Verification Flow - By default state field is being sent as N/A for United States unless value is changed from dropdown
-   [BAZA-1566](https://scalio.atlassian.net/browse/BAZA-1566) notifyUsers - proper error message should be displayed for invalid or nonexistent listingId
-   [BAZA-1573](https://scalio.atlassian.net/browse/BAZA-1573) Create marketing hero banner component
-   [BAZA-1569](https://scalio.atlassian.net/browse/BAZA-1569) Add documentation on Account components customization
-   [BAZA-1548](https://scalio.atlassian.net/browse/BAZA-1548) Sync changes between BAZA-1532 and BAZA-1538
-   [BAZA-1488](https://scalio.atlassian.net/browse/BAZA-1488) BE - Current account balance throws 500 internal server error when isDwollaAvailable is True with dwolla customer status as retry
-   [BAZA-1462](https://scalio.atlassian.net/browse/BAZA-1462) CMS - Add retry Dwolla customer verification functionality to cms
-   [BAZA-1564](https://scalio.atlassian.net/browse/BAZA-1564) Offering update template - Client name is not taken from the value specified in CMS registry
-   [BAZA-1363](https://scalio.atlassian.net/browse/BAZA-1363) Endpoint to reprocess dwolla customer verification on cms
-   [BAZA-1501](https://scalio.atlassian.net/browse/BAZA-1501) Clean up new Dwolla libs & components
-   [BAZA-1524](https://scalio.atlassian.net/browse/BAZA-1524) Provide the list of existing email events, templates, variables
-   [BAZA-1563](https://scalio.atlassian.net/browse/BAZA-1563) Include link to the Offering to the into the email template.
-   [BAZA-1239](https://scalio.atlassian.net/browse/BAZA-1239) Add @Type class-validator for Request DTOs
-   [BAZA-1459](https://scalio.atlassian.net/browse/BAZA-1459) CMS - Form to update investor Personal information through CMS
-   [BAZA-1489](https://scalio.atlassian.net/browse/BAZA-1489) FE - when isDwollaAvailable is False, call to fetch current account balance should not be made
-   [BAZA-1531](https://scalio.atlassian.net/browse/BAZA-1531) CMS Dividends Transactions - getByUlid API is being called continuously in the background
-   [BAZA-1570](https://scalio.atlassian.net/browse/BAZA-1570) Add a new intermediate status as "Document Processing" in DwollaCustomerStatus
-   [BAZA-1339](https://scalio.atlassian.net/browse/BAZA-1339) Sorting (Highest Dividend) If monthly dividend set as 0 in CMS and Highest Dividend is applied, Property is displayed on Top
    [BAZA-1555](https://scalio.atlassian.net/browse/BAZA-1555) Verification Flow - No error message/indicator displayed when changes saved without selecting any radio button on step 2
-   [BAZA-1552](https://scalio.atlassian.net/browse/BAZA-1552) "Add funds" not working in account

## PRs

-   https://github.com/scalio/baza/pull/1112
-   https://github.com/scalio/baza/pull/1107
-   https://github.com/scalio/baza/pull/1106
-   https://github.com/scalio/baza/pull/1100
-   https://github.com/scalio/baza/pull/1099
-   https://github.com/scalio/baza/pull/1098
-   https://github.com/scalio/baza/pull/1097
-   https://github.com/scalio/baza/pull/1095
-   https://github.com/scalio/baza/pull/1093
-   https://github.com/scalio/baza/pull/1092
-   https://github.com/scalio/baza/pull/1091
-   https://github.com/scalio/baza/pull/1090
-   https://github.com/scalio/baza/pull/1089
-   https://github.com/scalio/baza/pull/1088
-   https://github.com/scalio/baza/pull/1087
-   https://github.com/scalio/baza/pull/1086
-   https://github.com/scalio/baza/pull/1085
-   https://github.com/scalio/baza/pull/1084
-   https://github.com/scalio/baza/pull/1047
-   https://github.com/scalio/baza/pull/1058
-   https://github.com/scalio/baza/pull/1102
-   https://github.com/scalio/baza/pull/1020
-   https://github.com/scalio/baza/pull/1080
-   https://github.com/scalio/baza/pull/1078
-   https://github.com/scalio/baza/pull/1048
-   https://github.com/scalio/baza/pull/1062
