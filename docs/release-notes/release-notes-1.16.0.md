# Release 1.16.0

## What's new

- **New Web libraries added for Purchase and Account Verification flows: `baza-dwolla-web-purchase-flow` and `baza-dwolla-web-verification-flow`.**
- New `*-node-access` libraries set added in mirror to all `*-data-access* libraries. All Node Access services moved to new libraries.
- `baza-core`: new `BAZA_MASTER` environment variable added to Baza. Please read "Defining Master Node" section
- `baza-core`: It's possible now to replace default labels for Registry keys both with CMS or migration scripts.
- `baza-nc`: A new `isForeignInvestor` flag added to Bootstrap and Investor Account -> Current responses. The flag is displayed is it possible to use Dwolla for current investor or not
- `baza-nc`: It's possible now to change payment method when re-processing failed payments
- `baza-nc`: BE is now storing list of bank accounts available for withdrawing dividends
- `baza-nc`: A new `syncPaymentSources` flag added which is responsible to sync payment sources
- `baza-nc`: Dividends (`BazaNcDividendDto`) now have information about transfer type (`BazaNcDividendDto.fundMoveAction` with `DividendFundMoveAction.PaidWithNC`, `DividendFundMoveAction.DwollaCashIn` and `DividendFundMoveAction.DwollaCashOut` possible values)
- `baza-content-types`: Email template for Newsletter received some changes. These changes will not automatically be populated on upgrading Baza on target projects.
- `baza-content-types`: Some adjustments applied to Newsletter Content Type (removing outdated registry keys, updating email templates, etc)

## Bugfixes

- `baza-core`: Fixed issues when Maintenance interceptor didn't work correctly when `BAZA_API_PREFIX` is set
- `baza-nc`: Fixed issues when Dwolla customer wasn't create automatically after passing account verification flow
- `baza-nc`: Fixed names used for Failed Payments emails
- `baza-nc`: Fixed the bug where "Residential State" is not being saved on Verification Flow.
- `baza-nc`: Dwolla options will not be displayed anymore in CMS for clients which are using Dividends V1 (Dividends-via-NC) only
- `baza-nc`: `accountNickName`, `accountRoutingNumber` and `accountNumber` fields will be masked in Investor Account DTO -> Bank Accounts array
- `baza-nc`: Edit / Delete actions will not be displayed anymore for Dividend Transaction entries when Dividend Transaction is completed
- `baza-nc`: All mentions about Query Log feature was removed
- `baza-nc-integration`: Fixed issues with Portfolio Transactions pagination

## Breaking changes

- All `NodeAccess` services moved from `*-data-access` libraries to `*-node-access` libraries. You should install new `*-node-access* packages and replace imports for Node Access services which are used in your integration tests
- Helper `bazaNcTransactApiConfig` renamed to `bazaNcApiConfig`. It remains same functionality as before.
- Set `BAZA_MASTER: 1` for all available environments; Later it should be reverted (check "Defining Master Node" section)
- You should create & run a migration in order to set up `isForeignInvestor` flag for existing investors (check migration guide)

## Migration

1. Set `BAZA_MASTER=1` for all environments including local environment. Make this change as separate commit which could be easily reverted later
2. Install `*-node-access` packages for Node Access services which are used in your integration tests

| Data Access (Angular)             	| Node Access (Node)                	|
|-----------------------------------	|-----------------------------------	|
| `baza-core-data-access`           	| `baza-core-node-access`             	|
| `baza-dwolla-data-access`         	| `bazao-dwolla-node-access`          	|
| `baza-nc-data-access`             	| `baza-nc-node-access`               	|
| `baza-nc-integration-data-access` 	| `baza-nc-integration-node-access`   	|

3. Create a migration script which will updates `isForeignInvestor` flag:

```typescript
// migrations/src/lib/migrations/002-baza-nc-investor-account-is-foreign-investor.migration.ts
import { Injectable } from '@nestjs/common';
import { BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { QueryRunner } from 'typeorm';
import { BazaNcDwollaCustomerService, BazaNcInvestorAccountRepository } from '@scaliolabs/baza-nc-api';

@Injectable()
export class BazaNcInvestorAccountIsForeignInvestorMigration002 implements BazaMigration {
    constructor(
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly dwollaService: BazaNcDwollaCustomerService,
    ) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(queryRunner: QueryRunner): Promise<void> {
        const investors = await this.investorAccountRepository.repository.find({});

        for (const investor of investors) {
            investor.isForeignInvestor = await this.dwollaService.canHaveDwollaCustomer(investor.user.id);
            await this.investorAccountRepository.save(investor);
        }
    }

    down(queryRunner: QueryRunner): Promise<void> {
        return Promise.resolve(undefined);
    }
}
```

## Dwolla & NC flows for Purchase and Verification

Upcoming changes with Dividends V3 features will perform breaking changes for Purchase and Verification flows. In order to maintain support clients which are not going to fully move to Dwolla including both dividends and purchasing shares, we're releasing & implementing new flows with newly added `baza-dwolla-web-purchase-flow` and `baza-dwolla-web-verification-flow`.** Web libraries

## Form Validation Behaviour changes

This release will cover changing the default behavior of forms. The release will use relevant logic available at BAZA level which can be re-used in all places.

**For now, the following forms were updated with new logic:**

- Purchase Flow: Add Bank Account (Manual), Add Credit Card (Manual)
- Verification Flow: Personal Information & Investor Profile Forms

**Overall, this release introduces the following changes:**

- Submit buttons will always be enabled by default. However, if form is already filled up, untouched and all required fields are valid, submit button will be disabled. It will be enabled again if any form field is edited.
- After clicking the button, the required field is highlighted, and an error message is shown under it. Along with that, the form will automatically scroll up to the first required field with an error
- The scroll behavior can be customized for any "scrolling container". By default, the `window` object is used to scroll entire page but it's also done for `NzModalComponent` for example in "Add Bank Account (Manual)" form in purchase flow.

**The way to use this new validation behavior is as follows:**

1. Import `BazaNgCoreModule` into relevant module

2. Attach a `TemplateRef` to the `<form>` element e.g. we attach `#verificationFormEl` in this case. This is important since it will be used to find out the 1st invalid control within this form element and scroll to it incase of any error. Sample code is as follows:

```
<form
    nz-form
    nzLayout="vertical"
    [formGroup]="verificationInfoForm"
    #verificationFormEl>
```

3. Include `BazaFormValidatorService` within constructor of relevant component. Remember to use `public` keyword as we would next use this within the HTML in next step.
   `constructor(public readonly bazaFormValidatorService: BazaFormValidatorService)`

4. Now, we would update the HTML code for submit button. First, we would use `isFormFilledAndValid` method of validator service to check if button should be disabled by default or not. Second, for `onFormSubmit` method, we would also pass the `TemplateRef` we defined earlier. Sample code is as follows:

```
 <button
                    class="btn-angle-right"
                    nz-button
                    nzSize="large"
                    nzType="primary"
                    type="submit"
                    [disabled]="bazaFormValidatorService.isFormFilledAndValid(verificationInfoForm)"
                    (click)="onFormSubmit(verificationFormEl)">
                    Next              
</button>
```

5. Finally, we would use `isFormValid` method on validator service in component to make sure that our custom code only runs if the form was valid. Sample code is as follows:

```
public onFormSubmit(verificationFormEl: HTMLFormElement): void {
        if (this.bazaFormValidatorService.isFormValid(this.verificationInfoForm, verificationFormEl)) {
            // custom logic here
        }
}
```

### Using Custom Scroll Container
Please check the code in `payment-edit.component` and `add.component` files (both HTML and TS files) for understanding.

## Defining Master Node in Test/Staging/Production environments

> **⚠️ The release is including feature which was bad designed and which will be removed very shortly**

We're discussing currently how we can execute / handle some actions once only within cluster of API nodes. We initially created a solution to define one of API nodes as "master", but we find out that this solution is not working with our common DevOps approaches. We're currently designing new system which will resolve issues with duplicated handling of regular events.

For migration reasons, please set `BAZA_MASTER` environment variable as `1` for all environments. Later you should revert these changes.

## IVP

- Web Marketing application migrated from Baza IVP repository to Baza
- FAQ now can display complex HTML content like videos or images
- New form validation for "Reset Password", "Login", "Sign Up", "Forgot Password" and many more forms (see [PR-805](https://github.com/scalio/baza/pull/805))
- Fixed the bug where the transaction filter dropdowns were not opening properly by either hiding the arrow key on hover or registering on 2nd tap only.
- Added documentation to Baza Documentation portal about customizing account verification and purchase flows
- Added documentation to Baza Documentation portal about using Store from libraries to any componenents
- Added responsible design for Transaction Filter
- `PAYMENT_RETURNED` transactions now displays as part of Failed payments

## JIRA

- [BAZA-1133](https://scalio.atlassian.net/browse/BAZA-1133) New Dwolla account ID is not being created on investor profile verification
- [BAZA-1139](https://scalio.atlassian.net/browse/BAZA-1139) Ability to update payment method (NC Flow) for failed payments (BE)
- [BAZA-1167](https://scalio.atlassian.net/browse/BAZA-1167) Maintenance status endpoint fails with 503 error when Maintenance mode is enabled and API_PREFIX is set
- [BAZA-1085](https://scalio.atlassian.net/browse/BAZA-1085) Different sources for setting bank account to withdraw dividends to external bank acount
- [BAZA-1144](https://scalio.atlassian.net/browse/BAZA-1144) Newsletter email subject should show {Client Name}: Welcome instead of Feedback - {{Subject}}
- [BAZA-1086](https://scalio.atlassian.net/browse/BAZA-1086) Sync payment sources (purchase, withdraw) with checkbox (“Use same bank account for withdraw and purchases)
- [BAZA-996](https://scalio.atlassian.net/browse/BAZA-996) Re-submit payment - NOTIFY clients in case of payment failure - Email template
- [BAZA-1107](https://scalio.atlassian.net/browse/BAZA-1107) Creation of baza-dwolla-web-purchase-library
- [BAZA-1152](https://scalio.atlassian.net/browse/BAZA-1152) Personal Information: State doesn't gets Saved and on Re opening screen, State doesn't appear
- [BAZA-1131](https://scalio.atlassian.net/browse/BAZA-1131) Add a auto-calculable `isForeignInvestor` flag to Investor Account DTO
- [BAZA-1073](https://scalio.atlassian.net/browse/BAZA-1073) Clicking on the last available page on the Portfolio > Transactions pagination removes the whole pagination
- [BAZA-1077](https://scalio.atlassian.net/browse/BAZA-1077) BE - Ability to see and reprocess cash out on Portfolio Transactions
- [BAZA-1083](https://scalio.atlassian.net/browse/BAZA-1083) Replace bazaNcTransactApiConfig with bazaNcApiConfig
- [BAZA-1142](https://scalio.atlassian.net/browse/BAZA-1142) Revert removal of integration tests for jobs applications
- [BAZA-1135](https://scalio.atlassian.net/browse/BAZA-1135) Remove references for BAZA or Dwolla on CMS when uploading files or adding a dividend record
- [BAZA-1057](https://scalio.atlassian.net/browse/BAZA-1057) Ability to customize registry for external projects
- [BAZA-1105](https://scalio.atlassian.net/browse/BAZA-1105) Masking Sensitive Information in APIs and CMS
- [BAZA-1125](https://scalio.atlassian.net/browse/BAZA-1125) For DividendsV1, if we try to delete a published Dividend entry, nothing happens.
- [BAZA-1072](https://scalio.atlassian.net/browse/BAZA-1072) Extrance Node Access services from *-data-access libraries and move it to *-node-access libraries
- [BAZA-1104](https://scalio.atlassian.net/browse/BAZA-1104) Defining MASTER node in Cluster environment
- [BAZA-808](https://scalio.atlassian.net/browse/BAZA-808) Baza-nc: Remove Query Logs API, CMS, ACL and other  related resources completely
- [BAZA-911](https://scalio.atlassian.net/browse/BAZA-911) Clean up registry & adjust verbiage for email capability for Newsletter

## JIRA (IVP)

- [BAZA-1151](https://scalio.atlassian.net/browse/BAZA-1151) Coming soon/marketing page on Sandbox
- [BAZA-1027](https://scalio.atlassian.net/browse/BAZA-1027) Link to the video and embedded video player on FE ARE NOT displayed for successfully saved video link in BAZA CMS
- [BAZA-1071](https://scalio.atlassian.net/browse/BAZA-1071) Change form validation for all forms
- [BAZA-1163](https://scalio.atlassian.net/browse/BAZA-1163) Tapping on filter dropdowns will not open the dropdown list in the first tap, it removes the arrow icon only
- [BAZA-1091](https://scalio.atlassian.net/browse/BAZA-1091) Add documentation around FE libs
- [BAZA-1143](https://scalio.atlassian.net/browse/BAZA-1143) Create a new dwolla-web-verification library
- [BAZA-1090](https://scalio.atlassian.net/browse/BAZA-1090) Status Filter on Portfolio Transactions (Mobile)
- [BAZA-1036](https://scalio.atlassian.net/browse/BAZA-1036) Status Filter on Portfolio Transactions (FE)

## PRs

- https://github.com/scalio/baza/pull/815
- https://github.com/scalio/baza/pull/812
- https://github.com/scalio/baza/pull/810
- https://github.com/scalio/baza/pull/809
- https://github.com/scalio/baza/pull/808
- https://github.com/scalio/baza/pull/816
- https://github.com/scalio/baza/pull/801
- https://github.com/scalio/baza/pull/800
- https://github.com/scalio/baza/pull/799
- https://github.com/scalio/baza/pull/795
- https://github.com/scalio/baza/pull/793
- https://github.com/scalio/baza/pull/791
- https://github.com/scalio/baza/pull/790
- https://github.com/scalio/baza/pull/789
- https://github.com/scalio/baza/pull/788
- https://github.com/scalio/baza/pull/786
- https://github.com/scalio/baza/pull/785
- https://github.com/scalio/baza/pull/784
- https://github.com/scalio/baza/pull/783
- https://github.com/scalio/baza/pull/781
- https://github.com/scalio/baza/pull/778
- https://github.com/scalio/baza/pull/777
- https://github.com/scalio/baza/pull/776
- https://github.com/scalio/baza/pull/775
- https://github.com/scalio/baza/pull/774
- https://github.com/scalio/baza/pull/773
- https://github.com/scalio/baza/pull/772

## PRs (IVP)

- https://github.com/scalio/baza/pull/811
- https://github.com/scalio/baza/pull/807
- https://github.com/scalio/baza/pull/805
- https://github.com/scalio/baza/pull/804
- https://github.com/scalio/baza/pull/797
- https://github.com/scalio/baza/pull/792
- https://github.com/scalio/baza/pull/787
- https://github.com/scalio/baza/pull/782
- https://github.com/scalio/baza/pull/779
