# Release 1.30.0

This release reverts changes about CQRS, brings security for sensitive information in logs and renames a lot of components from web libraries.

We recommend to use 1.30.1 release which will includes some additional items hot-fixed for 1.28.N release line.

## What's new

-   `baza-core-api`: Maintenance mode is respecting current environment now. It is fixing issues with Prod / Preprod environment both enabled or disabled when using Maintenance mode.
-   `baza-core-api`: `BazaLogger` service is now has Transient injection scope, i.e. each injection of `BazaLogger` service will return a new instance.
-   `baza-core-api`: we secured sensitive information in all logs including API, Kafka any any other kind of logs which we are using currently. Any additional logs outside Baza libraries will also be secured with masking sensitive information.
    -   These changes cover every issue related to logs which could expose sensitive information like Credit Card, Bank Account information, JWT token, Emails, Addresses, SSN and more.
    -   `BazaLogger` service is updated in so that it will use `deepMask` function before logging the pass payload so the returned payload from `deepMask` is cleared (masked) from all sensitive information which might security issues .
    -   Removed the logger config for Kafka. Baza will use the new logger configuration for Kafka which was recently out of our centralized logging infrastructure.
    -   Added a new utility function which will build the configuration needed for `pino-http` package setup in the root module.
    -   It is possible to add custom fields to mask sensentive information with `bazaLoggerMaskField` utility
    -   There is a set of utility methods which could be used for masking your custom fields. Read more in "List of fields which will be masked in logs with new changes" section
-   `baza-web-ui-components`: renamed & restructured components into UI library
    -   Renamed several NC components that are common to Dwolla, adding the NC prefix to their names, making them more descriptive and easier to distinguish from Dwolla components.
    -   Improved overall structure of NC and Dwolla VF/PF libraries.
    -   Refactored duplicated components into `@scaliolabs/baza-web-ui-components` library.
    -   Updated related documentation.
-   `baza-core-api`: added new `cqrs` wrapper. All Event Handlers should be wrapped with new helper which prevents possible Nest.JS CQRS crashes and logs useful information (if `BAZA_EVENT_BUS_LOG_EVENTS` enabled). All existing Event Handlers are wrapped with new helper.
-   `baza-core-api`: added new `cqrsCommand` wrapper. All Command Handler should be wrapped with new helper which logs useful information(if `BAZA_EVENT_BUS_LOG_EVENTS` enabled). All existing Command Handlers are wrapped with new helper.
-   `baza-core-api`: `EVENT_PROVIDER` environment variable is no more exists
-   `web-libraries`: refactored / clean up i18n code:
    -   Please note that wherever you see an interpolated parameter, it is only used to pass a value internally. It does not directly affect the underlying functionality, but it does impact the validation message.
        ```ts
        {
            dateOfBirth: {
                label: 'Date of Birth',
                validators: {
                    isAdult: 'Investor should be {{ age }} or older',
                },
            }
        }
        ```
    -   `baza-{nc,dwolla}-web-verification-flow`: Added interpolated parameters for hard-coded values.
    -   `baza-{nc,dwolla}-web-purchase-flow`: Added interpolated parameters for hard-coded values.
    -   `baza-{nc,dwolla}-web-verification-flow`: Removed interpolated parameters inside links
    -   `baza-web-utils`: Removed additional private members and moved functionality to `checki18nGenericValidations`
    -   `baza-web-utils`: Made `checki18nGenericValidations` private.
    -   `baza-web-utils`: Created generic helper on `BazaWebUtilSharedService` to easily handle generic validation errors on components.
-   `baza-nc-api`: A new optional feature added which allows to pre-define Issuer ID which will be used for new Offerings (i.e. Listings). It could be specified with `BAZA_NORTH_CAPITAL_ISSUER_ID` environment variable and it's strictly recommended to configure it on all environments.
-   `baza-core-api`: API will not throw anymore exceptions when Customer.IO Link is not set yet for Mail Template. As far as we have a warning banner about missing Customer.IO Links, we don't need to interrupt requests when we have issues with Mail Templates
-   `baza-core-api`: Health Check endpoint `/health` is updated and should works correctly for all projects / environments. DevOps team could use it for implementing additional health checks on API
-   `baza-nc-web-purchase-flow`: moved dots on buy shares caption to translation files `NcDetailsEnI18n`
-   `baza-dwolla-web-purchase-flow`: moved dots on buy shares caption to translation files `DwollaDetailsEnI18n`

## Bugfixes

-   `baza-nc-integration-cms`: Added safe operator to the schema and entity since they're undefined when trying to add a new listing.
-   `baza-nc-integration-cms`: fixed multiple issues around Schema selector for Listings CMS
-   `baza-dwolla-web-purchase-flow`: fixed the issue with i18n translation label not properly displayed in "Add Funds" transfer popup in case the transfer fails due to any reason

## Breaking Changes

-   `baza-core-api`: As far as we changed which REDIS key will be used for Maintenance mode, updating target project to new Baza will automatically disable Maintenance mode. If you have Maintenance mode enabled, you should re-enable it once more after migration
-   `baza-core-api`: `BazaLogger.newInstance` static method was removed. Use `this.logger.setContext` instead, as far as each `BazaLogger` injection will return a new instance.
-   `web libraries`: A list of NC verification flow and NC purchase flow components selectors were renamed in order to avoid confusion with the Dwolla components.

Here's the summary of the components that were changed:

**Verification Flow:**

-   `app-verification` ➡️ `app-nc-verification`
-   `app-verification-info` ➡️ `app-nc-verification-info`
-   `app-verification-investor` ➡️ `app-nc-verification-investor`

**Purchase Flow:**

-   `app-purchase` ➡️ `app-nc-purchase`
-   `app-purchase-agreement` ➡️ `app-nc-purchase-agreement`
-   `app-purchase-details` ➡️ `app-nc-purchase-details`
-   `app-purchase-payment` ➡️ `app-nc-purchase-payment`

Full lists could be found in "Components Renamed" section.

## Migration

-   `api`: You should update Logger initialisation:

```typescript
// BEFORE:
const logger = app.get(BazaLogger);

// TO:
const logger = await app.resolve(BazaLogger);
```

Please note you will need to change the callback function used to initialize the logger to be an async function:

```typescript
const appBootstrapInstance = new ApiBootstrap(bazaApiBundleConfigBuilder().config);

appBootstrapInstance
    .bootstrap()
    .then(async (app) => {
        const logger = await app.resolve(BazaLogger);

        logger.log(`[main.ts] API started`);
    })
    .catch((err) => {
        console.error(err);
        console.error(`[main.ts] API failed to start`);
    });
```

-   `envs`: remove all `EVENT_PROVIDER` mentions from all charts and environment files
-   `npm`: do not remove `@nestjs/event-emitter` package. We may still use it in future for optimisations reasons
-   `baza-core-api`: We are strongly recommending update your Event Handlers implementation with `cqrs` helper. We found out that Nest.JS CQRS does not works correclty with `async handle(...): Promise<...>` syntax when error happens, and it's even possible to crash completely Nest.JS CQRS. We developed a helper which should be used as wrapped for contentns of `handle` implementation of every Event Handler:

```typescript
// BEFORE
@EventsHandler(MyEvent)
export class MyEventHandler implements IEventHandler<MyEvent> {
    async handle(event: BazaNcWebhookEvent): Promise<void> {
        await someAsyncActions();
    }
}

// AFTER
@EventsHandler(MyEvent)
export class MyEventHandler implements IEventHandler<MyEvent> {
    handle(event: BazaNcWebhookEvent): void {
        cqrs(MyEventHandler.name, async () => {
            await someAsyncActions();
        });
    }
}
```

-   `baza-core-api`: CQRS Command Handlers should be wrapped with `cqrsCommand` in same way (but w/o removing async keyword):

```typescript
// BEFORE
@CommandHandler(MyCommand)
export class MyCommandHandler implements ICommandHandler<MyCommand, MyCommandResult> {
    async execute(command: MyCommand): Promise<MyCommandResult> {
        return someAsyncActions();
    }
}

// AFTER
@CommandHandler(MyCommand)
export class MyCommandHandler implements ICommandHandler<MyCommand, MyCommandResult> {
    async execute(command: MyCommand): Promise<MyCommandResult> {
        return cqrsCommand<MyCommandResult>(MyCommandHandler.name, async () => {
            return someAsyncActions();
        });
    }
}
```

-   `baza-nc-api`: It's recommended to create a new Issuer in NC Client Admin panel and set `BAZA_NORTH_CAPITAL_ISSUER_ID` environment variables with IssuerId. We experienced that it's possible that during creating a new Offering you will receive "Issuer not found" error from NC because there is a short window between creating and availability of Issuer during creating an Offering (Listing)

## Incorrect Session Behavior Bug Fixed for CC (Dwolla Purchase Flow)

This Release fixes a specific edge-case in Dwolla PF where if CC is selected and purchase is above limit, we receive "Session could not be started" error.

As part of the Release, following changes were made:

-   Extra/Duplicated calls removed from Dwolla PF components
-   Data stream and logic for calculating purchase limits updated in `payment.component`. Previously, subscribes were written within other subscribed observables, causing same logic to be repeated multiple times (upon observable change). Fixed that in favor of using `combineLatest` with `take(1)` operator to only fetch the limits once upon loading of payment page. Once loaded, pass this limit to methods component for internal logic
-   Methods component data stream was updated to receive the latest purchase above limit flag. If CC was selected as default payment method and purchase is above limit, logic added to create session with `TBD` transaction type (in case of US national investors) and `ACH` transaction type (in case of foreign investors)
-   Using the above check eliminated the "Session could not be started" issue and also the session lifetime expiry issue shouldn't happen as we will be calling /session endpoint upon landing on payment page
-   Dynamic BE based error message added for /session endpoint e.g. is purchase is above limit on 1st step for TBD/ACH, then this BE error message would be displayed

## Incorrect Session Behavior Bug Fixed for CC (NC Purchase Flow)

This Release fixes a specific edge-case in NC PF where if CC is selected and purchase is above limit, we receive "Session could not be started" error.

As part of the Release, following changes were made:

-   Extra/Duplicated calls removed from NC PF components
-   Data stream and logic for calculating purchase limits updated in `payment.component`. Previously, subscribes were written within other subscribed observables, causing same logic to be repeated multiple times (upon observable change). Fixed that in favor of using `combineLatest` with `take(1)` operator to only fetch the limits once upon loading of payment page. Once loaded, pass this limit to methods component for internal logic
-   Methods component data stream was updated to receive the latest purchase above limit flag. If CC was selected as default payment method and purchase is above limit, logic added to create session with `ACH` transaction type
-   Using the above check eliminated the "Session could not be started" issue and also the session lifetime expiry issue shouldn't happen as we will be calling /session endpoint upon landing on payment page
-   Dynamic BE based error message added for /session endpoint e.g. is purchase is above limit on 1st step for ACH, then this BE error message would be displayed
-   Unused store selectors/actions removed from NC & Dwolla PF state files

## Components Renamed

Here's the complete summary of the components that were changed:

**Verification Flow:**

-   `app-verification` ➡️ `app-nc-verification`
-   `app-verification-file-upload` ➡️ `app-nc-verification-file-upload`
-   `app-verification-info` ➡️ `app-nc-verification-info`
-   `app-verification-investor` ➡️ `app-nc-verification-investor`

**Purchase Flow:**

-   `app-purchase` ➡️ `app-nc-purchase`
-   `app-purchase-agreement` ➡️ `app-nc-purchase-agreement`
-   `app-purchase-details` ➡️ `app-nc-purchase-details`
-   `app-purchase-done` ➡️ `app-nc-purchase-done`
-   `app-purchase-methods` ➡️ `app-nc-purchase-methods`
-   `app-purchase-payment` ➡️ `app-nc-purchase-payment`

---

-   `app-payment-edit` ➡️ `app-nc-payment-edit`
-   `app-payment-edit-account` ➡️ `app-nc-payment-edit-account`
-   `app-payment-edit-add` ➡️ `app-nc-payment-edit-add`
-   `app-payment-edit-list` ➡️ `app-nc-payment-edit-list`
-   `app-payment-edit-update` ➡️ `app-nc-payment-edit-update`

---

-   `app-payment-bank-account` ➡️ `app-nc-payment-bank-account`
-   `app-payment-bank-account-modal` ➡️ `app-nc-payment-bank-account-modal`
-   `app-payment-card` ➡️ `app-nc-payment-card`
-   `app-payment-card-modal` ➡️ `app-nc-payment-card-modal`

Additionally, the NC modules were also renamed to keep consistency.

-   `PaymentEditModule` ➡️ `NcPaymentEditModule`
-   `PaymentBankAccountModalModule` ➡️ `NcPaymentBankAccountModalModule`
-   `PaymentBankAccountModule` ➡️ `NcPaymentBankAccountModule`
-   `PaymentCardModalModule` ➡️ `NcPaymentCardModalModule`
-   `PaymentCardModule` ➡️ `NcPaymentCardModalModule`

The following modules were moved to `@scaliolabs/baza-web-ui-components` library.

-   `DwollaAddFundsFormModule`
-   `DwollaAddFundsModalModule`
-   `DwollaAddFundsNotificationModule`
-   `PaymentHeaderModule`
-   `PaymentRadioModule`

The `PersonalModule` and `PersonalDwollaModule` were removed in favor of `PersonalInfoModule`

## List of fields which will be masked in logs with new changes

Here is a list of fields which will be automatically masked in all logs. It will be not possible to override default list of fields, but you can add your custom fields with `bazaLoggerMaskField` helper.

-   `firstName`
-   `lastName`
-   `fullName`
-   `accountName`
-   `dob`
-   `address1`
-   `address2`
-   `primAddress1`
-   `primAddress2`
-   `address2`
-   `email`
-   `emailAddress`
-   `phone`
-   `ssn`
-   `socialSecurityNumber`
-   `creditCardName`
-   `creditCardNumber`
-   `cvvNumber`
-   `accountNumber`
-   `accountRoutingNumber`
-   `authorization`
-   `refreshToken`
-   `jwt`
-   `accessToken`

Here is a list of masking helpers which you can use for your custom fields:

-   `accountNameMask`
-   `addressMask`
-   `bankAccountNumberMask`
-   `bankAccountRoutingMask`
-   `creditCardNameMask`
-   `creditCardNumberMask`
-   `CVVMask`
-   `dateOfBirthMask`
-   `emailMask`
-   `firstNameMask`
-   `fullNameMask`
-   `jwtTokenMask`
-   `lastNameMask`
-   `phoneMask`
-   `ssnMask`

You can also create custom mask utility for masking custom fields.

## JIRA

-   [BAZA-1726](https://scalio.atlassian.net/browse/BAZA-1726) [CW] Removing sensitive financial data from logs
-   [BAZA-1714](https://scalio.atlassian.net/browse/BAZA-1714) Customer.io Mails - Dividends - Dwolla summary mail is received empty without transactions entries
-   [BAZA-1722](https://scalio.atlassian.net/browse/BAZA-1722) Customer.io Mails - Escrow Funds Transferred mail not working
-   [BAZA-1767](https://scalio.atlassian.net/browse/BAZA-1767) COL - Preprod does not work when Prod is in Maintenance mode
-   [BAZA-1777](https://scalio.atlassian.net/browse/BAZA-1777) Mask for First/Last/Full Name should be different
-   [BAZA-1778](https://scalio.atlassian.net/browse/BAZA-1778) Bring back "BazaLogger.newInstance" method and revert changes around removing newInstance calls
-   [BAZA-1792](https://scalio.atlassian.net/browse/BAZA-1792) Issues in Masking sensitive information for the API logs
-   [BAZA-1528](https://scalio.atlassian.net/browse/BAZA-1528) Sandbox: Create Regression plan in TestRail for web app
-   [BAZA-1663](https://scalio.atlassian.net/browse/BAZA-1663) Updating NC Sandbox account to BAZA
-   [BAZA-1686](https://scalio.atlassian.net/browse/BAZA-1686) Fix start /session logic when user only has CC and limit exceeds (Dwolla PF)
-   [BAZA-1688](https://scalio.atlassian.net/browse/BAZA-1688) Rename & Restructures files in Dwolla/NC libraries and/or Account Components
-   [BAZA-1731](https://scalio.atlassian.net/browse/BAZA-1731) Add masking sensitive information for all logs in API application
-   [BAZA-1737](https://scalio.atlassian.net/browse/BAZA-1737) Remove CQRS Event Provider
-   [BAZA-1740](https://scalio.atlassian.net/browse/BAZA-1740) Fix start /session logic when user only has CC and limit exceeds (NC PF)
-   [BAZA-1742](https://scalio.atlassian.net/browse/BAZA-1742) Fix TSLint Errors/Warnings and Remove Unnecessary Imports
-   [BAZA-1743](https://scalio.atlassian.net/browse/BAZA-1743) Refactor i18n Code (PF, VF and Account)
-   [BAZA-1755](https://scalio.atlassian.net/browse/BAZA-1755) CMS: Uncaught TypeError is returned in console when create a new listing
-   [BAZA-1761](https://scalio.atlassian.net/browse/BAZA-1761) Optional feature to define and use single Issuer for Offerings
-   [BAZA-1765](https://scalio.atlassian.net/browse/BAZA-1765) Do not attempt to send Customer.IO Email if Customer.IO Link is not set
-   [BAZA-1773](https://scalio.atlassian.net/browse/BAZA-1773) HealthCheck endpoint should returns full response body even with 500 status code
-   [BAZA-1775](https://scalio.atlassian.net/browse/BAZA-1775) CMS - Listings - Selecting None in the type field does not remove the selected schema tabs
-   [BAZA-1730](https://scalio.atlassian.net/browse/BAZA-1730) CMNW - IOS: Buy Shares: unexpected dot ' . ' is in the copy
-   [BAZA-1837](https://scalio.atlassian.net/browse/BAZA-1837) Error message not displayed properly

## PRs

-   https://github.com/scalio/baza/pull/1240
-   https://github.com/scalio/baza/pull/1245
-   https://github.com/scalio/baza/pull/1253
-   https://github.com/scalio/baza/pull/1258
-   https://github.com/scalio/baza/pull/1257
-   https://github.com/scalio/baza/pull/1230
-   https://github.com/scalio/baza/pull/1223
-   https://github.com/scalio/baza/pull/1238
-   https://github.com/scalio/baza/pull/1249
-   https://github.com/scalio/baza/pull/1246
-   https://github.com/scalio/baza/pull/1241
-   https://github.com/scalio/baza/pull/1239
-   https://github.com/scalio/baza/pull/1237
-   https://github.com/scalio/baza/pull/1235
-   https://github.com/scalio/baza/pull/1243
-   https://github.com/scalio/baza/pull/1234
-   https://github.com/scalio/baza/pull/1248
-   https://github.com/scalio/baza/pull/1247
-   https://github.com/scalio/baza/pull/1236
-   https://github.com/scalio/baza/pull/1261
