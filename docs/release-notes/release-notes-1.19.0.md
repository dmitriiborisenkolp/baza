# Release 1.19.0

The release is adding Device Token API and New Bank Account System which is replacing our current approaches to work with Bank Accounts within Purchase Flow. We also reworked withdraw feature and made fixes around Listings Search API. The release is preparation to upcoming Dwolla Purchase Flow!

### What's new

-   `baza-core`: Device Token Feature is added to Baza. Check out relevant sections for more details. You can also use Baza Documentation Portal for Usage guide: [https://baza-docs.test.scaliolabs.com/overview/36-device-tokens]()
-   `baza-nc`: New Bank Account System added to Baza. You should use it for Withdraw, Cash-In or any other operations with Dwolla. Check out `New Bank Accounts Systen` and `New Bank Accounts System – Export` sections below for more details.
-   `baza-nc`: `BazaNcBankAccountEntity` now have 2 more additional optional fields: `plaidAccountId`, `dwollaProcessorToken`
-   `baza-nc`: Investment endpoint (`/baza-nc-integration/portfolio/assets`) received changes - now it can display two cards of same listings depends on transactions. Check out `Assets endpoints changes` section for more details
-   `baza-core`: `AccountEntity` now has `ulid` field which is set as optional currently. Please set up ULID for all accounts later with migrations until 1st Jan 2023
-   `baza-nc-web-purchase-flow`, `baza-dwolla-web-purchase-flow`: Show KYC unsuccessful error in purchase flow, so if the investor status is disapproved must show an alert message with 'We had identified issues to validate your personal information. Please review your data.' with a link to the verification page to let the users update their information. the solution covered NC and Dwolla libs.
-   `baza-nc-cms`, `baza-nc-api`: A new menu item was added to the investor's data grid to manually create Dwolla customers for existing investors. Menu item is not visible if the Dwolla customer is already created.
    -   A new endpoint to touch Dwolla customers by `userId` was implemented.
    -   `dwollaCustomerError` field was added to `nc_investor_account` entity.
    -   `dwollaCustomerError` property was added to `BazaNcInvestorAccountDto`.
    -   All Dwolla errors captured by the Dwolla touch service are stored in the new `dwollaCustomerError` field.
    -   A warning hint per row is shown on the **Dwolla Customer Id** when the `dwollaCustomerError` field has a value.

## Bugfixes

-   Fixed potential issues with ReDoc; We also added integration tests to ensure that ReDoc documentation will be working after updating new Baza

## Breaking changes

-   `baza-nc`: Baza NC libraries now requires `@scaliolabs/baza-plaid-api` and `@scaliolabs/baza-plaid-shared` installed
-   `repo` NPM library should be installed
-   `web`: Plaid script should be added to HEAD of index.html
-   `baza-nc`: Bank Accounts reworked; `bankAccounts` field in `BazaNcInvestorAccountEntity` is **DEPRECATED** and should not be used anymore.
-   `baza-nc-integration`: Bootstrap endpoints moved from `baza-nc-integration` to `baza-nc` scope; You should update Node Access and Data Access service imports.

## Migration

-   Install Plaid Node NPM library: `yarn add plaid`
-   Baza NC now requires Plaid account to be Set Up. Read more on Baza Documentation -> Investment Platform -> Plaid page for more details
    -   You should enable Dwolla integration for Plaid Link: https://dashboard.plaid.com/team/integrations
    -   You should set up Single Account Select for Plaid Link: https://dashboard.plaid.com/link/account-select ("Enabled for one account")
-   New environment variables should be added for API (for Device Token Feature):

```dotenv
BAZA_DEVICE_TOKEN_FCM_TTL_MINUTES=5 # Please update this value, 5 minutes is for QA only
BAZA_DEVICE_TOKEN_APPLE_TTL_MINUTES=5 # Please update this value, 5 minutes is for QA only
```

-   New environment variables should be added for API (for Plaid Integration):

```dotenv
BAZA_PLAID_PRODUCTION=0
BAZA_PLAID_CLIENT_ID=5eea29bcadf86d001230f7f2
BAZA_PLAID_CLIENT_SECRET=013dadfa0db7e79e0a64a8ddd1879d
BAZA_PLAID_CLIENT_NAME=Scalio LLC
```

-   Create a Migration script for generating Account ULIDs:

```typescript
import { Injectable } from '@nestjs/common';
import { BazaAccountRepository, BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { ulid } from 'ulid';

@Injectable()
export class BazaCoreAccountUlidMigration003 implements BazaMigration {
    constructor(private readonly accountRepository: BazaAccountRepository) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(): Promise<void> {
        const accountIds = await this.accountRepository.findAllIds();

        for (const accountId of accountIds) {
            const account = await this.accountRepository.findActiveOrDeactivatedAccountWithId(accountId);

            if (accountId && !account.ulid) {
                account.ulid = ulid();

                await this.accountRepository.save(account);
            }
        }
    }

    async down(): Promise<void> {
        return;
    }
}
```

-   You should communicate with DevOps team to allow new HTTP headers in order to use new Device Token Feature.
    -   `X-Baza-Fcm-Token`
    -   `X-Baza-Apns-Token`
    -   `X-Baza-Fcm-Token-Unlink`
    -   `X-Baza-Apns-Token-Unlink`

## Device Token Feature

This release provides the underlying API to Link / Unlink device tokens and a new CMS component that allows users to view, link, and unlink FCM / Apple device tokens.

**You can also use Baza Documentation Portal for Usage guide: [https://baza-docs.test.scaliolabs.com/overview/36-device-tokens]()**

### Features

-   The following sub-packages were created inside `baza-core-*` packages
    -   `baza-core-api/baza-device-token`
    -   `baza-core-cms/baza-device-token`
    -   `baza-core-shared/baza-device-token`
    -   `baza-core-node-access/baza-device-token`
    -   `baza-core-data-access/baza-device-token`
-   New entity (`account_device_token_entity`) was added to the database
-   Controllers, services, repositories, mappers, strategies, exceptions, and e2e tests were implemented on the `baza-core-api/baza-device-token` sub-package.
-   DTOs, endpoints, error codes, i18n files, and models were implemented on the `baza-core-shared/baza-device-token` sub-package.
-   Two new environment variables `BAZA_DEVICE_TOKEN_FCM_TTL_MINUTES` and `BAZA_DEVICE_TOKEN_APPLE_TTL_MINUTES` used to set up the time to live in minutes of the device tokens.
-   Two new watchers `BazaDeviceTokenCleanUpFcmWatcher` and `BazaDeviceTokenCleanUpAppleWatcher` used to run intervals and destroy outdated device tokens on a time basis.
-   Device Tokens will be invalidated on Account Deactivation too.

### API specifications

**Link new device token**

```ts
deviceTokensDataAccess.link({
    accountId: 1,
    type: BazaDeviceTokenType.FCM,
    token: 'my-fcm-token',
});
```

**Refresh existing device token timestamp**

> NOTE: when trying to link an existing token, the `updatedAt` timestamp will get refreshed automatically.

```ts
deviceTokensDataAccess.link({
    accountId: 1,
    type: BazaDeviceTokenType.FCM,
    token: 'my-fcm-token',
});
```

**Unlink device token**

```ts
deviceTokensDataAccess.unlink({
    accountId: 1,
    type: BazaDeviceTokenType.FCM,
    token: 'my-fcm-token',
});
```

### New HTTP headers were added to `BazaHttpHeaders`

You should communicate with DevOps team to allow new HTTP headers in order to use new Device Token Feature.

```ts
HTTP_HEADER_LINK_FCM_TOKEN; // X-Baza-Fcm-Token
HTTP_HEADER_LINK_APPLE_TOKEN; // X-Baza-Apns-Token
HTTP_HEADER_UNLINK_FCM_TOKEN; // X-Baza-Fcm-Token-Unlink
HTTP_HEADER_UNLINK_APPLE_TOKEN; // X-Baza-Apns-Token-Unlink
```

### New utility function

`baza-device-token-http-headers.util` can be used to extract all device tokens from the request HTTP headers grouped in action sets of link and unlink tokens.

```ts
const actionSets = extractDeviceTokensFromRequest(request);
console.log(actionSets);

/*
Result

{
    link: [
        ['FCM', 'my-new-fcm-token'],
        ['Apple', 'my-new-apple-token'],
    ],
    unlink: [
         ['Apple', 'my-old-apple-token'],
    ]
}
*/
```

### New bulk command

`baza-device-token-bulk.command` can be used to automatically link/unlink multiple device tokens at once based on array sets.

```ts
new BazaDeviceTokenBulkCommand({
    accountId: 99999,
    actionSets:  {
        link: [
           ['FCM', 'my-new-fcm-token'],
           ['Apple', 'my-new-apple-token'],
        ],
        unlink: [
           ['Apple', 'my-old-apple-token'],
        ]
   }
}),
```

### New HTTP interceptor

`baza-device-token.interceptor` will intercept all HTTP requests that have an active session and will run the `baza-device-token-bulk.command` if it finds any device tokens attached to the request HTTP headers.

### CMS Features

-   New action named "Device tokens" was added to the CMS accounts data grid.
    ![image](https://user-images.githubusercontent.com/64181348/191898682-e18cc205-87d7-4acf-b168-41b7e5c28f92.png)

-   New device tokens component was created on `baza-core-cms/baza-device-token` sub-package. It shows the device tokens sorted descending by the latest update date.
    ![image](https://user-images.githubusercontent.com/64181348/191899302-9a3a8ce8-59a7-460e-83da-b53d96182bea.png)

-   Device tokens can be manually unlinked
    ![image](https://user-images.githubusercontent.com/64181348/191899519-49c3ab5e-417c-4aca-8884-ff62a8dfbd6a.png)
    ![image](https://user-images.githubusercontent.com/64181348/191899571-d78314a1-d2ba-41fd-b378-56f35f7d2a39.png)

-   Device tokens can be manually linked
    ![image](https://user-images.githubusercontent.com/64181348/191899747-5e8a4dd4-15db-442b-ba36-f778a108800d.png)

-   As mentioned above, if an existing device token is linked again, it will refresh its update timestamp and it will be placed at the top.
    ![image](https://user-images.githubusercontent.com/64181348/191900012-e20666a9-487b-4d4c-b772-02260e7bf356.png)
    ![image](https://user-images.githubusercontent.com/64181348/191900145-a613cca6-c011-4fd1-a2a1-27954049dd1f.png)

## Plaid Integration

Plaid Integration is working with New Bank Account System (NBAS). Migrate to NBAS in order to use withdraw and Plaid Integration features.

-   New endpoints added for Bank Accounts: `GET /baza-nc/bank-account/link` and `POST /baza-nc/bank-account/linkOnSuccess`
-   `GET /baza-nc/bank-account/link` will returns token for Plaid Link UI
-   `POST /baza-nc/bank-account/linkOnSuccess` is responsible for processing on-success callback of Plaid Link UI
    -   It's possible to specify which Account Types (Cash In, Cash Out), Exports (NC, Dwolla) and Set As Default immediattly (true, false) should have added Bank Account
-   New endpoints available with `link` and `linkOnSuccess` method of `BazaNcBankAccountsDataAccess` service
-   New Baza libraries added with common Plaid integration: `@scaliolabs/baza-plaid-api`, `@scaliolabs/baza-plaid-shared`

### How to use?

1. Add Plaid script to `index.html`:

```
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
```

2. Display Plaid Link. There are no arguments here to pass for displaying Plaid Link - all options like desired Bank Account Type, exports to NC / Dwolla and Set as Default will be executed within `onSuccess` callback:

```typescript
/**
 * Step 1: Receive Link Token & open Plaid Link widget
 */
openPlaid(): void {
    this.bankAccountDataAccess
        .link()
        .pipe(takeUntil(this.ngOnDestroy$))
        .subscribe(
            (response) => {
                const handler = Plaid.create({
                    token: response.linkToken,
                    onSuccess: (publicToken) => {
                        // (see Step 3)
                    },
                });

                handler.open();
            },
            (error: BazaError) => this.nzMessage.error(error?.message || 'Unknown error'),
        );
}
```

3. Add On-Success Callback with calling `linkOnSuccess` endpoint:

```typescript
/**
 * Step 1: Receive Link Token & open Plaid Link widget
 */
openPlaid(): void {
    this.bankAccountDataAccess
        .link()
        .pipe(takeUntil(this.ngOnDestroy$))
        .subscribe(
            (response) => {
                const handler = Plaid.create({
                    token: response.linkToken,
                    onSuccess: (publicToken) => {
                        this.linkBankAccount(publicToken); // <<< Updated
                    },
                });

                handler.open();
            },
            (error: BazaError) => this.nzMessage.error(error?.message || 'Unknown error'),
        );
}

/**
* Step 2: On-Success handler
*/
linkBankAccount(publicToken: string): void {
    this.bankAccountDataAccess
        .linkOnSuccess({
            // Public Token received with `bankAccountDataAccess.link`
            publicToken,

            // Account Type (Cash In, Cash Out)
            type: BazaNcBankAccountType.CashIn,

            // Exports to NC or Dwolla
            // For Cash Out you should export Bank Account to Dwolla
            // For Cash In (Dividends v1, v2) you should export to NC
            // For Cash In (Dividends v3) you should export to Dwolla
            export: [BazaNcBankAccountExport.NC, BazaNcBankAccountExport.Dwolla],

            // Automatically use it as default. All operations like withdraw, transfer or paying dividends will be
            // performed with default bank accounts only
            setAsDefault: true,
        })
        .pipe(takeUntil(this.ngOnDestroy$))
        .subscribe(
            (response) => {
                // `response` will have `duplicate` flag and `bankAccount` DTO
                // API automatically handles duplicate attempts to link Bank Account

                this.nzMessage.info('Bank Account exported!');
            },
            (error: BazaError) => this.nzMessage.error(error?.message || 'Unknown error'),
        );
}
```

Plaid Link also have additional callbacks like `onExit`. Read more at [Plaid Documentation](https://plaid.com/docs/link/web/) page.

### Example

You can find example in `libs/dwolla-sandbox-lib-web/src/lib/sections/plaid-integration` of `baza` repository. You also can run it on your local environment (with local API enabled):

-   `yarn dwolla-sadbox-api:start`
-   `yarn dwolla-sadbox-web:start`
-   Go to http://localhost:4200/plaid-integration

## Assets Endpoints Changes

-   Assets endpoint was changed to group investments by Listings + Statuses
-   Previous implementation is placed under` /baza-nc-integration/portfolio/assets-total` endpoint and displays Investments aggregated by Listing
-   New implementation is placed under `/baza-nc-integration/portfolio/assets` endpoint and displays Investments aggregated by Listing + Status. It may display same Listing multiple (2) times:
    -   `[PAYMENT_CONFIRMED, FUNDED]` group (Primary Listing Card which will contains paid dividends and last failed transaction)
    -   `[PAYMENT_PENDING]` group - will display dividends and failed transactions only in case if `[PAYMENT_CONFIRMED, FUNDED]` group is missing
-   Added new `isPrimaryCard` field for Investments items
-   Added `/baza-nc/e2e/setTradeStatus` endpoint to set transaction / order statuses during E2E tests
    **- Duplicated non-main cards will have `0` as dividendsCents and wil not contains information about last failed transaction**
-   Additionally fixed issues with Dwolla Customer ID cache

## New Bank Account System

A new Bank Account system for Baza NC libraries. These endpoints should be used as replacement for any similar features which was created on target project.

### New Bank Account System:

-   A new set of endpoints to work with Baza NC Bank Accounts
-   Check out "Baza NC Bank Accounts" section in API documentation for more details
-   Works with Cash-In (Purchase) and Cash-Out (Withdraw) Bank Accounts
-   A new `isBankAccountCashOutLinked` flag added to InvestorAccountDTO (`isBankAccountLinked` is for Cash-In accounts and `isisBankAccountCashOutLinked` is for Cash-Out accounts)
-   Bank Account Endpoints from Purchase Flow are marked as deprecated (legacy) and should not be used. BE has configuration option to throw an error in attempt to work with these endoints:
    -   `/baza-nc/purchase-flow/getPlaidLink`
    -   `/baza-nc/purchase-flow/getBankAccount`
    -   `/baza-nc/purchase-flow/setBankAccount`
    -   `/baza-nc/purchase-flow/deleteBankAccount`
-   These endpoints should still be used to work on `baza-nc-web-` libraries, but should not be used on `baza-dwolla-web-*` libraries
-   For QA reasons these endpoints are still available to use on Baza (and also these endpoints will be available to use )
-   Use `BazaNcBankAccountsDataAccess` for accessing new endpoints
    -   `list` - Returns list of all available bank accounts
    -   `defaults` - Returns list of bank accounts for each BankNcAccountType which are selected as default. Please note that user may didn't select bank account for some types yet - in this case there will be no bank account with this type in response available
    -   `default` - Returns default bank account of given type
        -   If bank account is not available for given type and request.throwError is false, the endpoint will returns { isAvailable: false }
        -   If bank account is not available for given type and request.throwError is true, the endpoint throw an error message with "BazaNcBankAccountNoDefault" error code
        -   You can also receive list of bank accounts of given type if you enable request.withOptions
    -   `get` - Returns Bank Account by ULID
    -   `add` - Adds a Bank Account (MANUAL)
        -   Use `setAsDefault: true` to automatically set Bank Account as Default, i.e. use this account instantly for Purchase Flow / Future Payments
    -   `remove` - Removes Bank Account by ULID. If it's possible, a first Bank Account in list of Bank Accounts of same type will be selected as default
        -   Endpoint may returns ULID of Bank account which is newly selected as default
    -   `set` - Set Bank Account as default for given Bank Account Type

### What's new (BE):

-   Credit Card and ACH services extracted from Purchase Flow to separated sub-libraries
-   A new `bank-account` sub-library added with fully feature set of services and methods to work with Bank Accounts
    -   Use `BazaNcBankAccountsService` to work with Bank Accounts on BE side
-   Added `enableLegacyBankAccountsSystem` configuration option for `bazaNcApiConfig` which enables Legacy endpoints
-   Fully covered wih integraton tests
-   Fully covered with TSDocs

### Concepts

-   There is a list of Bank Accounts attached to Investor
-   Bank Account can be Cash-In or Cash-Out
-   Bank Accounts of same type (Cash-In, Cash-Out) defines a group of Bank Accounts
-   Each group will always have 1 Bank Accounts selected as Default
-   When removing Bank Accounts which is selected as Default, a first available Bank Account of same group will be selected as default
-   You can set any Bank Accounts as default with `set` method
-   You can also export Bank Account from one group to another when setting the accounts as default with `set` method + `types` array provided. This could be used for "Use same bank account" feature
-   BE will automatically exports Bank Accounts to NC or Dwolla when user is purchasing shares or withraw dividends.

## New Bank Account System - Export Feature

Added Export feature for Bank Accounts.

Export feature is responsible to export Bank Accounts to North Capital and Dwolla services. The tool is designed to allow FE to control different options for settings up Bank Accounts when purchasing shares, withdrawing dividends or adding funds to Dwolla Balance depends on current UI. BE follows concept of supporting all possible options; UI is defining which options will be available for user.

https://scalio.atlassian.net/browse/BAZA-1128

### What's new

-   Bank Accounts created with new New Bank Account System now can be exported to NC or Dwolla
-   Bank Accounts can be automatically exported when adding new Bank Account
-   Data Access / Node Access: new `export` method added to both services
-   Account Nick Name could be specified for Bank Account ("Account Holder Name" and "Account Holder Nick Name" could be different).

### Usage

-   Bank Account system defines list of Bank Accounts
-   Bank Accounts are grouped by Bank Account Type (Cash-In, Cash-Out)
-   You can have duplicate Bank Accounts in different groups (same bank account for Cash-In and Cash-Out)
-   For each Bank Account group there could be a default Bank Account selected
-   When adding a first Bank Account in group, the added Bank Account will be automatically selected as default
-   When default Bank Account is removed, API attempts to select first available Bank Account selected as default
-   All operations (purchasing shares, withdrawing, cash-in) should be processed with selected Default Bank Account only
-   Bank Accounts could be exported to NC or Dwolla
-   Cash-In Bank accounts are supposed to work both with Legacy NC ACH flow (for purchasing shares) and new Dwolla Balanace flow (Dividends V3 scope)
-   Cash-In Bank Accounts could be exported to NC. You should export Bank Accounts to NC for Purchase Flow in case if investor should be able to use NC ACH for purchasing shares
-   Cash-Out Bank Accounts cannot be exported to NC
-   Cash-In Bank Accounts could be exported to Dwolla. Export Cash-In accounts to Dwolla for Dividends v3 scope
-   Cash-Out Bank Accounts could be exported to Dwolla. Use it for withdrawing dividends feature.

### Information for BE

-   `BazaNcBankAccountsExportService` now have a new `export` method. Use the service for withdraw / cash-in (transfer) operations.
-   A new `bazaDwollaExtractId` added for extracting ID's from URL's returned by Dwolla API
-   Fixed some issues with extracting error code / error message from Dwolla API
-   `BazaDwollaApiErrorException` now uses Dwolla Error codes (`BazaDwollaApiErrorException.code`) instead of `BazaDwollaErrorCodes.BazaDwollaApiError` error code
-   `BazaDwollaApiErrorException` now shares `_links` and `_embedded` from Dwolla API

## JIRA

-   [BAZA-1129](https://scalio.atlassian.net/browse/BAZA-1129) Add a Plaid integration for new Bank Account flow
-   [BAZA-1196](https://scalio.atlassian.net/browse/BAZA-1196) BE API - Withdraw Feature Rework
-   [BAZA-1296](https://scalio.atlassian.net/browse/BAZA-1296) Change Asset API so that it lists assets from the same listing but in different statuses separately
-   [BAZA-994](https://scalio.atlassian.net/browse/BAZA-994) Ability to add funds so that I can use it when needed (BE)
-   [BAZA-1069](https://scalio.atlassian.net/browse/BAZA-1069) Migrate dwolla sandbox account to Scalio
-   [BAZA-1128](https://scalio.atlassian.net/browse/BAZA-1128) Add a support for multiple Bank Accounts of same type
-   [BAZA-1175](https://scalio.atlassian.net/browse/BAZA-1175) Device Token (FCM/Apple) storage for Accounts
-   [BAZA-1176](https://scalio.atlassian.net/browse/BAZA-1176) Ability to link FCM token with HTTP Header
-   [BAZA-1177](https://scalio.atlassian.net/browse/BAZA-1177) Ability to unlink device token with HTTP Header
-   [BAZA-1178](https://scalio.atlassian.net/browse/BAZA-1178) Communicate with DevOps about new HTTP Headers
-   [BAZA-1179](https://scalio.atlassian.net/browse/BAZA-1179) Automatically clean up FCM tokens on time basis
-   [BAZA-1204](https://scalio.atlassian.net/browse/BAZA-1204) Baza-Nc-Integration - Rework Bootstrap endpoint
-   [BAZA-1210](https://scalio.atlassian.net/browse/BAZA-1210) Documentation about Device Tokens (for Mobile developers)
-   [BAZA-1211](https://scalio.atlassian.net/browse/BAZA-1211) Automatically clean up Apple tokens on time basis
-   [BAZA-1212](https://scalio.atlassian.net/browse/BAZA-1212) Ability to link Apple token with HTTP Header
-   [BAZA-1213](https://scalio.atlassian.net/browse/BAZA-1213) Destroy all Device Tokens (FCM, Apple) on Deactivating Account
-   [BAZA-1249](https://scalio.atlassian.net/browse/BAZA-1249) Show KYC unsuccessful error in purchase flow
-   [BAZA-1254](https://scalio.atlassian.net/browse/BAZA-1254) Add new option to CMS (investor's accounts) to manually create Dwolla customer
-   [BAZA-1314](https://scalio.atlassian.net/browse/BAZA-1314) Add CMS (Cypress) integration tests which checks that both Redoc and Swagger documentation displays properly

## JIRA (IVP)

-   [BAZA-1299](https://scalio.atlassian.net/browse/BAZA-1299) Unifying Color Scheme of alert component error message

## PRs

-   https://github.com/scalio/baza/pull/865
-   https://github.com/scalio/baza/pull/844
-   https://github.com/scalio/baza/pull/885
-   https://github.com/scalio/baza/pull/881
-   https://github.com/scalio/baza/pull/823
-   https://github.com/scalio/baza/pull/855
-   https://github.com/scalio/baza/pull/849
-   https://github.com/scalio/baza/pull/858
-   https://github.com/scalio/baza/pull/859
-   https://github.com/scalio/baza/pull/867
-   https://github.com/scalio/baza/pull/867
-   https://github.com/scalio/baza/pull/883
-   https://github.com/scalio/baza/pull/872
-   https://github.com/scalio/baza/pull/831
-   https://github.com/scalio/baza/pull/878
-   https://github.com/scalio/baza/pull/872
-   https://github.com/scalio/baza/pull/867
-   https://github.com/scalio/baza/pull/864
-   https://github.com/scalio/baza/pull/866
-   https://github.com/scalio/baza/pull/853
-   https://github.com/scalio/baza/pull/893

## PRs (IVP)

-   https://github.com/scalio/baza/pull/875
