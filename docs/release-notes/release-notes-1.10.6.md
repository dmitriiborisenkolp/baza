# Release 1.10.6

**What's new**

-   New release contains huge changes around Kafka integration and API Event Bus feature.
-   API Bus and Kafka now works with `MultiNode` and `EveryNode` strategies. Events passed with `MultiNode` pipeline will be processed once in cluster, events passed with `EveryNode` pipeline will be processed on every node in cluster
-   NC Webhooks now can works with `MultiNode` pipeline (default, should be used for test/stage/prod instances) or with `EveryNode` for Radio instances (baza-test). Radio instance should be used to allow development applications with NC integration
-   New `baza-nc-integration` package set added to this release. This package provides extended API & CMS for NC applications: Referral Code, Invite Code, Feed, Listings, Account Registration with Referral/Invite codes, Subscriptions, Investments any many more
-   Maintenance and Master Password CMS will works corretly on node cluster
-   Fixed multiple issues around DocuSign selector
-   API Documentation now allows to use Bearer token for endpoints which requires authorization
-   Baza-NC now allows to use DocuSign Templates which are named with special character
-   Additionally, fixed issue with DocuSign Template was not saved properly after updating
-   DocuSign integration can be enabled for Test environment
-   Mailgun Transport didn't work properly with request w/o `cc` parameter

**Important!**

-   New release fixes issues around Throttle feature. Baza incorrectly detects client IP which lead to using same TTL window for all clients.

**Breaking changes**

-   Possible breaking changes may be related with changes around MultiNode/EveryNode pipelines

**Migration**

-   No additional changes required after updating target project from 1.10.5 to 1.10.6

**JIRA**

-   https://scalio.atlassian.net/browse/BAZA-346 PR’s with failed CI pipelines due to NC changes on KYC/AML
-   https://scalio.atlassian.net/browse/BAZA-243 Progress-bar breaks when bar has any odd decimal value
-   https://scalio.atlassian.net/browse/BAZA-337 CMS - Maintenance flags are glitchy
-   https://scalio.atlassian.net/browse/BAZA-338 CMS - Master Password flag is glitchy
-   https://scalio.atlassian.net/browse/BAZA-342 Adjust Kafka event streaming to not create excessive events.
-   https://scalio.atlassian.net/browse/BAZA-343 Make Tax Documents CMS optional
-   https://scalio.atlassian.net/browse/BAZA-344 Disable charset check for DocuSign Template Field
-   https://scalio.atlassian.net/browse/BAZA-350 CMS - Search does not work for investor Accounts list
-   https://scalio.atlassian.net/browse/BAZA-351 Swagger Authorization doesn't work
-   https://scalio.atlassian.net/browse/BAZA-358 Sign up — The 500 error occurs while submitting the sign up/forgot pswrd form
-   https://scalio.atlassian.net/browse/BAZA-363 Selected DocuSign template is not displayed on the Listing edit form after changing Template name
-   https://scalio.atlassian.net/browse/BAZA-366 DocuSign banned for Test environments
-   https://scalio.atlassian.net/browse/BAZA-348 Verified Investor has transactions via Plaid ACH Bank Account but NO Plaid ACH Bank Account payment method shown
-   https://scalio.atlassian.net/browse/BAZA-347 Create NC-integration API package
