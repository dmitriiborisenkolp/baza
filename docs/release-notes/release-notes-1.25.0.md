# Release 1.25.0

This Release is covering 3 major topics:

-   Adding more configuration options to `baza-dwolla-web-*` libraries
-   Adding Daily Reports API & CMS Feature for Baza NC Packages
-   Tracking for actual ACH payment status instead of "fake" one for Dwolla Payments infrastructure
-   Fixing _really_ huge amount of issues when `whitelist` option is enabled for `ValidationPipe`.

The release contains Dwolla Payments rework which is covering support for related transfers which should be tracked as source payment status instead of status of primary transfer.

We are still not recommending currently enable `whitelist` validation option, but you may try it out on your test environments. We're currently using this option on our test environments and we will let you know if there will be any issues.

In order to use the new route configuration format, please go through this detailed guide:

-   [Route / Links(s) Configuration Guide](https://baza-docs.test.scaliolabs.com/web-development/customizations/route-configurations/01-route-configuration-guide)

## What's new

-   `baza-nc`: added Daily Reports API & CMS Feature
-   `baza-nc-api`: added Baza NC Daily Reports CMS API
-   `baza-nc-api`: added new registry configs for Daily Reports feature: `bazaNc.escrowOpsEmail`, `bazaNc.escrowOpsEmailSubject` and `bazaNc.escrowCsvDelimiter`
-   `baza-nc-cms`: added Daily Reports section to North Capital CMS group. The feature is disabed by default; use `bazaNcBundleCmsConfig({ withReportFeature: true });` to enable feature
-   `baza-core-api`: Added `CsvService.exportToCSV` method which allows to export array of array of strings to CSV
-   `baza-core-api`: Added support for Attachments for sending emails. Support added to all available Mail Transports, including SMTP, Mailgun, Customerio and Memory (e2e) transports
-   `baza-core-api`: Added `userId` relation getter for `InvestorAccountEntity`
-   `baza-nc`: Added optional "Sending Notifications to Subscribers about Offerings Updates" feature
-   `baza-nc-api`: Added `/baza-nc-integration/cms/subscription/notifyUsers` CMS endpoint to notify subscribers for offering updates
-   `baza-nc-cms`: Added `withNotifySubscribedUsersFeature` configuration option for `bazaNcBundleCmsConfig` helper. This flag controls "Sending Notifications to Subscribers about Offerings Updates" feature on CMS
-   `baza-nc-api`: Added new registry config `offeringStatusSubject` in registry configuration
-   `baza-dwolla-web-purchase-flow`, `baza-dwolla-web-verification-flow`: A flexible configuration system is now in place for common things e.g. copy changes and routes. Read more at Baza Documentation Portal and in "Custom Route Configurations for Internal/External Links" section
-   `baza-dwolla-web-purchase-flow`: added customization for hint + prefix labels on account balance components and updates UX for editing payment methods in the purchase flow. Check for more details in "Account Balance Components Customization" section
-   `baza-dwolla-web-purchase-flow`: Fixed the bug where CC radio button was not being shown in Dwolla PF library if investor has Dwolla balance and CC, but no ACH account.
-   `baza-dwolla-api`: Dwolla Payments correctly handles Purchase Flow transactions
    -   Dwolla Payments now stores `achDwollaTransferId` with Dwolla Transfer Id of related ACH transfer
    -   Dwolla Payments now stores `individualAchId` with ACH ID. Value of the field will be populated when related ACH Transfer will be processed
    -   Dwolla Payments now has a new `channel` field with `BazaDwollaPaymentChannel.Balance` (default) and `BazaDwollaPaymentChannel.ACH` values. Channel configures the way how Dwolla Payment should be processed
-   `baza-dwolla-api`: Added new `DwollaPaymentAchInitiatedEvent` event. The event will be triggered when ACH Transfer is initiated by Dwolla for recently created Dwolla Transfer
-   `baza-dwolla-api`: Added a new "Dwolla Testing - Enable autorun webhook simulations" registry config to enable or disable automatic run webhook simulations during dwolla-related operations. This configuration will not be displayed on production and preprod (UAT) environments.
-   `baza-nc-api`: added a single webhook event handler reponsible for handling different dwolla customer verification status updates.
-   `baza-nc-api`: updated the investor accounts view table to also return customer verification status and failure reasons.

## Bugfixes

-   `baza-nc-api`: Fixed API documentation issues with Baza NC Transactions API (both Public and CMS APIs) and Baza NC Tax Document API
-   `baza-nc-api`: Fixed duplicate logs issue
-   `baza-nc-api`: Removed `BAZA_NORTH_CAPITAL_WEBHOOK_ENABLE_PUBLISH_KAFKA` and `BAZA_NORTH_CAPITAL_WEBHOOK_EVERY_NODE` unused environment variables and related module configurations
-   `docs`: Fixed multiple issues with some documentation articles & updated information about Baza NC environment variables

## Breaking changes

-   `baza-nc-api`: Method `TransactionRepository.deleteAllTradeHistoryOf` renamed to `TransactionRepository.deleteAllTransactionsOfAccount`
-   `baza-dwolla-web-purchase-flow`, `baza-dwolla-web-verification-flow`: Multiple configurations were added/updated with details mentioned below. Please have the configurations updated on your project(s) as well
    -   All input configurations for Dwolla PF, Dwolla VF and Account components are named as "config" now so e.g. "agreementConfig", "paymentConfig" etc. names are all refactored and we always have the `@Input` property name "config"
    -   If no configurations are passed down, the default configurations from Baza would be applied e.g. contact us email link would be the default link available in Baza

## Migration

-   Remove `BAZA_NORTH_CAPITAL_WEBHOOK_ENABLE_PUBLISH_KAFKA` and `BAZA_NORTH_CAPITAL_WEBHOOK_EVERY_NODE` from environments and charts everywhere
-   If you would like to enable Baza NC Daily Reports feature, you should enable it with CMS configuration:

```typescript
// src/apps/cms/src/app/app.module.ts

bazaNcBundleCmsConfig({
    withReportFeature: true,
});
```

-   If you would like to enable Baza NC Daily Reports feature, you should add a new email templates to repository (under `apps/api/i18n/mail-templates/baza-nc/report/nc-escrow-notification` directory):

**HTML (Primary) Mail Template (`nc-escrow-notification.html.hbs`)**

```handlebars
{{#> webLayoutHtmlEn }}
    <p>Hello.</p>
    <p>Please find attached payment transactions for trades made via Dwolla.</p>
    <p>Thank you!</p>
{{/webLayoutHtmlEn}}
```

**Text (Secondary) Mail Template (`nc-escrow-notification.text.hbs`)**

```handlebars
{{#> webLayoutTextEn }}
Hello.

Please find attached payment transactions for trades made via Dwolla.

Thank you!
{{/webLayoutTextEn}}
```

-   If you would like to use "Sending Notifications to Subscribers about Offerings Updates", you should enable it with `bazaNcBundleCmsConfig({ withNotifySubscribedUsersFeature: true })` configuration

-   If you would like to use "Sending Notifications to Subscribers about Offerings Updates", , you should add a new email templates to repository (under `apps/api/i18n/mail-templates/baza-nc/offering/client-notification` directory):

**HTML (Primary) Mail Template (`offering-notification.html.hbs`)**

```handlebars
{{#> webLayoutHtmlEn }}
    <br>
    <br>Hello {{../userFullName}},
    <br>
    <br>We’re happy to inform you that {{../offeringName}} is now open for investments!
    <br>
    <br>Thank You,
    <br>
    <br>{{clientName}}
    <br>
    <br><img src="{{ ../cmsLogoUrl }}" alt="logo" width="200px">
{{/webLayoutHtmlEn}}
```

**Text (Secondary) Mail Template (`offering-notification.text.hbs`)**

```handlebars
{{#> webLayoutTextEn }}

Hello {{../userFullName}},

We’re happy to inform you that {{../offeringName}} is now open for investments!

Thank You,
{{clientName}}
{{/webLayoutTextEn}}
```

## Custom Route Configurations for Internal/External Links

-   A flexible configuration system is now in place for common things e.g. copy changes and routes
-   A route can now be defined as internal or external route with mutually exclusive properties allowing to only define 1 out of these options for additional validation
-   Each route can have a set of properties. All of these properties are nullable and default configurations are also set in relevant components
-   The logic to navigate these links is all encapsulated within `baza-utils` library. All methods/interfaces/configurations etc. are within a shared location
-   If required, this logic can be used for custom routes within App level as well

### Detailed Guide:

In order to use the new route configuration format, please go through this detailed guide:

-   [Route / Links(s) Configuration Guide](https://baza-docs.test.scaliolabs.com/web-development/customizations/route-configurations/01-route-configuration-guide)

### Notes:

-   In interest of time and not to overcomplicate existing implementation, the already available links such as "back-link", "buy-shares" and "verification" links logic is currently the same.
-   There are some more possible refactorings that will be done later as part of configuration system. Once made, all relevant information will be updated in `baza-docs` portal and release notes

---

### Added/Updated Configurations:

**Account Components**

-   Added new configuration for `DwollaAccountBalanceComponent` component
-   Please note: The individual input properties e.g. title, hint & prefix will be refactored later to be part of the config as well
-   The config automatically also handles showing the right email address in "Add Funds" modal, in case we have a funds transfer error

Sample value (default) for this configuration:

```typescript
config?: AccBalanceConfig = {
    links: {
        contact: {
            extLink: {
                link: 'bazaContentTypes.contacts.email',
                text: 'contact us',
                isCMSLink: true,
                isMailLink: true,
            },
        },
    },
};
```

---

-   Added new configuration for `DwollaBankAccountPayoutComponent` component

Sample value (default) for this configuration:

```typescript
config?: AccPayoutConfig = {
    links: {
        contact: {
                extLink: {
                    link: 'bazaContentTypes.contacts.email',
                    isCMSLink: true,
                    isMailLink: true,
                },
            },
        },
    };
```

---

**Dwolla Verification Flow Library**

-   Added new configuration for `VerificationInfoComponent` component.
-   Replaced the individual inputs title and description with single `config?: VFInfoConfig` input

Sample value (default) for this configuration:

```typescript
defaultConfig: VFInfoConfig = {
    title: 'Personal Information',
    descr: 'We collect your information to perform Know Your Customer (KYC) and Anti-Money Laundering (AML) compliance. Please provide correct legal information to verify your account.',
};
```

---

-   Added new configuration for `VerificationInvestorComponent` component
    Added the existing individual title input property into the new `config?: VFInvestorConfig`

Sample value (default) for this configuration:

```typescript
defaultConfig: VFInvestorConfig = {
    title: 'Investor Profile',
    links: {
        termsOfService: {
            extLink: {
                link: 'bazaCommon.links.termsOfService',
                text: 'Terms of Service',
                isCMSLink: true,
            },
        },
        privacyPolicy: {
            extLink: {
                link: 'bazaCommon.links.privacyPolicy',
                text: 'Privacy Policy',
                isCMSLink: true,
            },
        },
        dwollaTOS: {
            extLink: {
                link: 'https://www.dwolla.com/legal/tos/#legal-content',
                text: 'Terms of Service',
            },
        },
        dwollaPriacyPolicy: {
            extLink: {
                link: 'https://www.dwolla.com/legal/privacy',
                text: 'Privacy Policy',
            },
        },
    },
};
```

---

**Dwolla Purchase Flow Library**

-   Updated the configuration for DwollaPurchasePaymentComponent component
-   Refactored naming of interfaces

Sample value (default) for this configuration:

```typescript
config: PaymentConfig = {
    methods: {
        showPurchaseDividendMessage: true,
        accountBalanceTitle: 'Account Balance',
    },
    steps: {
        links: {
            termsOfService: {
                extLink: {
                    link: 'bazaCommon.links.termsOfService',
                    text: 'Terms of Service',
                    isCMSLink: true,
                },
            },
            eftDisclosure: {
                appLink: {
                    commands: ['/eft-disclosure'],
                    text: 'EFT Disclosure',
                },
            },
        },
    },
};
```

-   Updated the configuration for `DwollaPurchaseDoneComponent` component
-   Sample value (default) for this configuration:

```typescript
config: PurchaseDoneConfig = {
    title: 'Your order has been submitted!',
    desc: 'We are verifying the order and will notify you when it has been completed. This typically takes 24-48 hours.',
    ctaBtnLink: {
        appLink: {
            text: 'Proceed To My Portfolio',
            commands: ['/portfolio'],
        },
    },
    cta: {
        desc: 'If you have any questions please reach out via',
        link: {
            appLink: {
                text: 'Contact Us',
                commands: ['', { outlets: { modal: ['contact-us'] } }],
            },
        },
    },
};
```

## Account Balance Components Customization

-   Two new config options were added to `paymentMethodConfig` to enable customization for hint and prefix labels.
-   Updated payment component to use new options and pass down this config to its children.
-   Updated `account-balance-card` component to receive new options.
-   Baza portal documentation was updated.
-   Edit payment method flow was updated:
    -   Edit icon was replaced with CTA "Edit Your Payment Method"
    -   Reduced the number of user interactions by omitting popover upon clicking.

### Customize account balance in the `DwollaPurchasePaymentComponent` component

```html
<app-purchase-payment-dwolla
    [paymentConfig]="{
        paymentMethodConfig: {
            accountBalanceTitle: 'Custom account balance title',
            accountBalanceHint: 'Custom account balance hint',
            accountBalancePrefix: 'Custom account balance prefix:'
        }
    }"></app-purchase-payment-dwolla>
```

or

```ts
paymentConfig = {
    paymentMethodConfig: {
        accountBalanceTitle: 'Custom account balance title',
        accountBalanceHint: 'Custom account balance hint',
        accountBalancePrefix: 'Custom account balance prefix:',
    },
};
```

```html
<app-purchase-payment-dwolla [paymentConfig]="paymentConfig"></app-purchase-payment-dwolla>
```

### Customize account balance in the `DwollaAccountBalanceComponent` component

```html
<app-dwolla-account-balance
    title="Custom account balance title"
    hint="Custom account balance hint"
    prefix="Custom account balance prefix:"></app-dwolla-account-balance>
```

### Customize account balance in the `AccountBalanceCardComponent` component

```html
<app-dwolla-account-balance
    title="Custom account balance title"
    hint="Custom account balance hint"
    prefix="Custom account balance prefix:"></app-dwolla-account-balance>
```

### Customize account balance styles

```css
.balance-subtitle {
    /* Custom styles for the subtitle */
}

.balance-subtitle-faded {
    /* Custom styles for the subtitle when the wallet is disabled */
}

.balance-subtitle__prefix {
    /* Custom styles for the balance value prefix */
}
```

### Customize CTA styles

```css
.purchase__edit-cta {
    /* custom styles **/
}
```

## JIRA

-   [BAZA-1482](https://scalio.atlassian.net/browse/BAZA-1482) Purchase Shares: Use Escrow funding source from Dwolla Escrow section as a destination for Dwolla purchases
-   [BAZA-1483](https://scalio.atlassian.net/browse/BAZA-1483) Add a Dwolla Escrow section in CMS to setup Escrow funding source ID
-   [BAZA-1485](https://scalio.atlassian.net/browse/BAZA-1485) Daily Emails to North Capital for purchases via Dwolla
-   [BAZA-1509](https://scalio.atlassian.net/browse/BAZA-1509) BE - Sending notification to investors
-   [BAZA-1510](https://scalio.atlassian.net/browse/BAZA-1510) CMS - Sending notification to investors about offering updates
-   [BAZA-1511](https://scalio.atlassian.net/browse/BAZA-1511) BE - Offering Updates email template
-   [BAZA-1532](https://scalio.atlassian.net/browse/BAZA-1532) Config should support both external and internal links
-   [BAZA-1537](https://scalio.atlassian.net/browse/BAZA-1537): UI: Changes based on client feedback
-   [BAZA-1538](https://scalio.atlassian.net/browse/BAZA-1538) Web: Changes based on client feedback
-   [BAZA-1539](https://scalio.atlassian.net/browse/BAZA-1539) Web: Copy changes
-   [BAZA-1540](https://scalio.atlassian.net/browse/BAZA-1540) It is impossible to select Card payment method when having Balance and no Bank Account
-   [BAZA-1551](https://scalio.atlassian.net/browse/BAZA-1551) Need to check the Related Transfer status to change the Trade transaction to "Funded"
-   [BAZA-1365](https://scalio.atlassian.net/browse/BAZA-1365) Add webhook handlers for dwolla customer verification webhooks
-   [BAZA-1563](https://scalio.atlassian.net/browse/BAZA-1563): Include link to the Offering to the into the email template.
-   [BAZA-1564](https://scalio.atlassian.net/browse/BAZA-1564): Offering update template - Client name is not taken from the value specified in CMS registry
-   [BAZA-1237](https://scalio.atlassian.net/browse/BAZA-1237) Enable Whitelist option for Validation pipe

## PRs

-   https://github.com/scalio/baza/pull/1074
-   https://github.com/scalio/baza/pull/1069
-   https://github.com/scalio/baza/pull/1070
-   https://github.com/scalio/baza/pull/1067
-   https://github.com/scalio/baza/pull/1068
-   https://github.com/scalio/baza/pull/1073
-   https://github.com/scalio/baza/pull/1065
-   https://github.com/scalio/baza/pull/1077
-   https://github.com/scalio/baza/pull/1024
-   https://github.com/scalio/baza/pull/1082
-   https://github.com/scalio/baza/pull/1021
