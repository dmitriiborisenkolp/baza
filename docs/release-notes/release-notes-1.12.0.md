# Release 1.12.0

New release is focused around Dividends V1 feature and preparations for Dividends V2 feature.

## What's new:

-   `baza-core`: Added `CsvService` to `baza-core-api` library. This service could be used to parse CSV files
-   `baza-core`: Fixed issues with List requests which are using `size: -1` option.
-   `baza-core`: Fixed a visual glitch when removing last image for image-multi controls in CMS
-   `baza-core`: Multiupload controls received rework. Read more in "Multiupload controls changes" section
-   `baza-nc`: Dividends V1 is completed and ready to use.
-   `baza-nc`: Dividends: Import via CSV tool. Dividends can be imported with CSV
-   `baza-nc-integration`: Added dividends calculations for Portfolio endpoints
-   `baza-nc-integration`: Fixed an issue with not displaying Asset card after purchasing shares
-   `baza-nc-integration`: Move Up / Move Down feature added to Listings CMS
-   `baza-nc-integration`: Added CQRS events for Newsletters subscribe / unsubscribe actions
-   `baza-nc-integration`: Statements added to `nc-integration` listing endpoint
-   `baza-dwolla`: Webhooks, Webhooks Subscriptions, Events and Labels API Gateway Services added

## Breaking changes:

-   `baza-core`: New `csv-parse` NPM dependency added
-   `baza-nc`: Most services, repositories and DTOs are renamed to fix common naming conventions. All these resources now have `BazaNc` prefix.
-   `baza-nc`: Offering Names updated. Before Offerings used `IssueName`, now it uses Listing Title (if possible) of `NC Offering #...` stub for any other cases.
-   `baza-nc-integration`: Portfolio transactions endpoint now uses different Response DTOs

## Dividends:

-   New "Dividends" CMS section under Baza NC menu group
-   Links to Dividends from Investor Account CMS
-   Dividends processing is secured by 2FA
-   Portfolio Transactions endpoint now has scopes filter which selected specific transactions types (Dividends or Investments)
-   Potfolio Stats endpoint now displays total dividends amount + returnPercents / returns also counts dividends

### New APIs:

-   Dividends API
-   Dividends CMS API
-   Dividend Transactions CMS API
-   Dividend Transaction Entries CMS API

### Dividends processing

-   Dividends V1 supports multiple sources of Dividends
-   NC dividends applies instantly
-   Dwolla dividends works in async mode

## Multiupload controls changes

-   All Upload form components received rework
-   Cropper.JS integration extracted as dedicated component + service
-   Upload Queue extracted as dedicated component + service
-   Fixed multiple issues around uploading images, attachments & multi-uploads

### Upload controls now have new options:

-   `multiselect` controls ability to select multiple files at once with file dialog
-   `ratioHandler` controls how images will be cropped. Image could be cropped by used with Cropper.JS (option 1) and image could be automatically cropped with API (option 2)
-   By default `BazaImage`, `BazaUpload` and `BazaAttachment` controls uses Cropper.JS option to crop images
-   Multi-uploads version of `BazaImage` / `BazaAttachment` / `BazaUpload` components also uses Cropper.JS by default (even if ratio is not set, user still be able to customise image before uploading it to AWS). It could be disabled with `Disabled` ratio handler option.

### Example:

```typescript
{
    type: BazaFormBuilderControlType.BazaImageMulti,
    formControlName: 'images',
    label: this.i18n('form.fields.images'),
    props: {
        withImageOptions: {
            ratio: 1.6 / 1,
        },
        multiselect: true,
        withRatioHandler: BazaFieldUploadImageRatioHandler.CropperJS,
    },
},
```

Read more in PR description: https://github.com/scalio/baza/pull/638

## Newsletters CQRS Events

Use `BazaEventBus` service to catch subscribe / unsusbscribe events:

```typescript
bazaEventBus: ApiEventBus<NewslettersApiEvent, NewslettersApiEvents>

// NewslettersApiEvent.BazaNewsletterSubscribed
// NewslettersApiEvent.BazaNewsletterUnSubscribed
```

## Offering Names migration

You should update `baza_nc_offering_entity.offeringName` column to display correct Offering Names in dividends. There a multiple ways how to do it:

1. Manually change Offering Names in Database
2. Re-save all Listings or any other Entities which are using Offerings
3. Add a migration script to `migrations/src/lib/migrations` directory and add the migration to `BAZA_API_MIGRATIONS` constant:

```typescript
// migrations/src/lib/migrations.ts
import { BazaNcOfferingNamesMigration001 } from './migrations/001-baza-nc-offering-names.migration';

export const BAZA_API_MIGRATIONS = [BazaNcOfferingNamesMigration001];

// migrations/src/lib/migrations/001-baza-nc-offering-names.migration.ts

import { Injectable } from '@nestjs/common';
import { BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { QueryRunner } from 'typeorm';
import { BazaNcIntegrationListingsRepository } from '@scaliolabs/baza-nc-integration-api';
import { BazaNcOfferingRepository } from '@scaliolabs/baza-nc-api';

@Injectable()
export class BazaNcOfferingNamesMigration001 implements BazaMigration {
    constructor(
        private readonly listingRepository: BazaNcIntegrationListingsRepository,
        private readonly offeringRepository: BazaNcOfferingRepository,
    ) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(queryRunner: QueryRunner): Promise<void> {
        const listings = await this.listingRepository.listAll();

        for (const listing of listings) {
            const offering = await this.offeringRepository.findOfferingWithId(listing.offering.ncOfferingId);

            if (offering) {
                offering.ncOfferingName = listing.title;

                await this.offeringRepository.save(offering);
            }
        }
    }

    down(queryRunner: QueryRunner): Promise<void> {
        return Promise.resolve(undefined);
    }
}
```

## JIRA:

-   [BAZA-774](https://scalio.atlassian.net/browse/BAZA-774) Design Dividends Public API
-   [BAZA-775](https://scalio.atlassian.net/browse/BAZA-775) Dividends API & CMS
-   [BAZA-776](https://scalio.atlassian.net/browse/BAZA-776) Import Dividends with CSV
-   [BAZA-957](https://scalio.atlassian.net/browse/BAZA-957) Able to add more dividend entries to an already processed transaction through API
-   [BAZA-954](https://scalio.atlassian.net/browse/BAZA-954) Investors being aware of their dividend incomes through Assets
-   [BAZA-847](https://scalio.atlassian.net/browse/BAZA-847) Asset card does not appear in Portfolio after the first Purchase
-   [BAZA-545](https://scalio.atlassian.net/browse/BAZA-545) CMS - to add 'Move up/down' functionality to Listings
-   [BAZA-942](https://scalio.atlassian.net/browse/BAZA-942) drag and drop box for upload is displayed after you delete the uploaded image
-   [BAZA-916](https://scalio.atlassian.net/browse/BAZA-916) Add BazaEventBus events for Newsletters subscribe/unsubscribe actions
-   [BAZA-908](https://scalio.atlassian.net/browse/BAZA-908) Baza-Dwolla Gateway service for Dwolla Webhook Subscriptions API
-   [BAZA-909](https://scalio.atlassian.net/browse/BAZA-909) Baza-Dwolla Gateway service for Dwolla Webhooks API
-   [BAZA-880](https://scalio.atlassian.net/browse/BAZA-880) multi-select and uploading component Implementation in Baza
-   [BAZA-907](https://scalio.atlassian.net/browse/BAZA-907) Baza-Dwolla Gateway service for Dwolla Events API
-   [BAZA-910](https://scalio.atlassian.net/browse/BAZA-910) Baza-Dwolla Gateway service for Dwolla Labels API
-   [BAZA-924](https://scalio.atlassian.net/browse/BAZA-924) Add Statements to Listings API

## JIRA (Sandbox):

-   [BAZA-952](https://scalio.atlassian.net/browse/BAZA-952) Fix prettier formatting throughout Baza project (all files, all folders)
-   [BAZA-956](https://scalio.atlassian.net/browse/BAZA-956) My Portfolio Transaction API integration
-   [BAZA-891](https://scalio.atlassian.net/browse/BAZA-891) Clear out tags usage in Purchase flow
-   [BAZA-953](https://scalio.atlassian.net/browse/BAZA-953) Test "Date of Birth" calendar + masking functionality on Baza
-   [BAZA-826](https://scalio.atlassian.net/browse/BAZA-826) Show the amount of total dividends on Portfolio Menu
-   [BAZA-935](https://scalio.atlassian.net/browse/BAZA-935) Cancel and Confirm buttons have no space under them at the bottom
-   [BAZA-890](https://scalio.atlassian.net/browse/BAZA-890) Removing grid from Verification flow lib
-   [BAZA-892](https://scalio.atlassian.net/browse/BAZA-892) Clear out tags usage in Verification flow
-   [BAZA-934](https://scalio.atlassian.net/browse/BAZA-934) Add Payment method popup does not reset the input fields on reopening it after closing
-   [BAZA-887](https://scalio.atlassian.net/browse/BAZA-887) Create a new item-horizontal component
-   [BAZA-900](https://scalio.atlassian.net/browse/BAZA-900) Sandbox - Correction of text color and typeface on Investor Profile Page
-   [BAZA-897](https://scalio.atlassian.net/browse/BAZA-897) User is able to enter 'e' into Phone Number field on Verification Flow

## PRs:

-   https://github.com/scalio/baza/pull/669
-   https://github.com/scalio/baza/pull/666
-   https://github.com/scalio/baza/pull/665
-   https://github.com/scalio/baza/pull/656
-   https://github.com/scalio/baza/pull/655
-   https://github.com/scalio/baza/pull/654
-   https://github.com/scalio/baza/pull/652
-   https://github.com/scalio/baza/pull/651
-   https://github.com/scalio/baza/pull/650
-   https://github.com/scalio/baza/pull/649
-   https://github.com/scalio/baza/pull/647
-   https://github.com/scalio/baza/pull/641
-   https://github.com/scalio/baza/pull/640
-   https://github.com/scalio/baza/pull/639
-   https://github.com/scalio/baza/pull/638
-   https://github.com/scalio/baza/pull/637
-   https://github.com/scalio/baza/pull/636
-   https://github.com/scalio/baza/pull/633

## PRs (Sandbox):

-   https://github.com/scalio/baza/pull/662
-   https://github.com/scalio/baza/pull/660
-   https://github.com/scalio/baza/pull/658
-   https://github.com/scalio/baza/pull/657
-   https://github.com/scalio/baza/pull/653
-   https://github.com/scalio/baza/pull/648
-   https://github.com/scalio/baza/pull/646
-   https://github.com/scalio/baza/pull/643
-   https://github.com/scalio/baza/pull/642
-   https://github.com/scalio/baza/pull/635
-   https://github.com/scalio/baza/pull/634
-   https://github.com/scalio/baza/pull/632

## PRs (dependencies):

-   https://github.com/scalio/baza/pull/644
