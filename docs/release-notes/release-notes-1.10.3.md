# Release 1.10.3

**JIRA Tickets:**

-   https://scalio.atlassian.net/browse/BAZA-315
-   https://scalio.atlassian.net/browse/BAZA-317
-   https://scalio.atlassian.net/browse/MIP-2083

**What's new:**

-   NC Request Body Filter
-   Added helpers `bazaNcSetCharset`, `bazaNcSetCustomCharset` and `bazaNcApiConfig` which should be used to configure NC Request Body Filter
-   Fixed an issue with `AttachmentService.postprocess` method which leads to hard-to-debug "Node is not exists in current context..." error
-   New `BazaAuthAccessTokenAttached` event added which fires every time when new Access/Refresh Token is **generated** & **attached** to account
-   Account Verification now properly updates `isAccountVerificationCompleted` flag for various scenarios (before the flag could be updated only after calling /apply-investor-profile endpoint, now updating flag could be triggered with /apply-personal-information of with any upload endpoint).
-   NC Tax Documents now works with PDF files only

**NC Request Body Filter**

Baza NC package now automatically trim string resources and fix `\t` Transact API Bug. Additionally, API will throws an error with `BazaNcCharacterIsNotAllowed` error code if user will attempts to use characters which are not supported by Transact API.

There are two available charsets available to use:

1. EN_US: QWERTY-only symbols charset
2. Latin1 charset which is used in MySQL

By default QWERTY EN/US charset is used as allowed set of characters. Additionally, you can set Latin1 charset or define your own charset.

There are additional configuration helpers available:

```typescript
bazaNcSetCharset(NcCharset.Latin1); // CMNW
bazaNcSetCharset(NcCharset.En); // (default) MIP & New applications
bazaNcSetCustomCharset('abcdef123456890-'); // Define & use custom charset
```

Additionally, please note that request body now automatically trim's strings. You can configure it with `bazaNcApiConfig` helper:

```typescript
bazaNcApiConfig({
    enableAutoTrim: true,
    fixTabSymbolBug: false,
}); // defaults
```

Option fixTabSymbolBug fixes an issues with NC API replacing Tab (\t) symbol with t visible string. If for some reason you need to disable auto-trim option, you should enable fixTabSymbolBug option.

**PR's:**

-   https://github.com/scalio/baza/pull/79
-   https://github.com/scalio/baza/pull/80
-   https://github.com/scalio/baza/pull/81
-   https://github.com/scalio/baza/pull/83
