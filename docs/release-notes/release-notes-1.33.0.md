# Release 1.33.0

This release includes mostly FE fixes & refactoring. We also provide Plaid Redirect URL support, but we strongly advise to not use it without solid and clear reason.

## What's new

-   `baza-core-api`: library was migrated to TypeScript Strict rules
-   `web-libraries`: Added TSDocs for frontend library interfaces: `dwolla-web-purchase-flow`, `nc-web-purchase-flow`, `dwolla-web-verification-flow`, `nc-web-verification-flow`, `baza-web-utils`, `web-ui-components`
    `baza-web-ui-components`: Updated i18n form validations and configurations for `Add funds`, `Bank details`, `Card details`, `Withdraw funds`, `Investor`
-   `baza-dwolla-web-verification-flow` : Following points were covered:
    -   New components including check account, stepper, stepper info and stepper investor were created
    -   Logic was moved out from parent verification component to new components and module references were updated
    -   Routing was updated to load check account and stepper components as separate routes.
    -   Verification parent component was updated to use redirect link from query params.
-   `baza-dwolla-web-purchase-flow` : Following points were covered:
    -   Personal info component event emitter for edit account was updated
    -   Redirection from personal info component to Dwolla VF was updated with redirect query parameters
-   `baza-web-ui-components` : - Personal info component event emitter for edit account was updated
-   `baza-{nc, dwolla}-web-verification-flow` : Following points were covered:
    -   Updated routes and added resolvers
    -   Updated resolver logic to return data as part of route
    -   Moved form resources resolver from application to library level
    -   Updated associated internal component implementation to pick resolved data from store
    -   Huge performance gains due to removal of duplicated API calls being made before. After caching results in store 1st time, from 2nd attempt onwards, resolvers get values directly from store which results in fast page loads and redirects.
-   `baza-plaid-api`: added support for Redirect URL. You can define Redirect URL with `BAZA_PLAID_REDIRECT_URL` environment value
-   `baza-nc-api`: `link` endpoint from Baza Bank Account API has a new `withRedirectUrl` Query Param. If you would like to utilise Redirect URL, you should pass `withRedirectUrl=1` for `/baza-nc/bank-account/list` endpoint

## Bugfixes

-   `baza-nc-api`: `transactionFee` values will be forced to be Math.ceil'd in order to avoid float values in `session` endpoint response
-   `baza-core-ng`: fixed maintenance guard redirection in case the maintenance mode is off. Now it redirects to the root URL.
-   `baza-nc-api`: Dwolla Balance `current` endpoint (`/baza-nc/dwolla/current`) slightly updated to not fail with any possible cases
-   `baza-{nc,dwolla}-web-verification-flow`: Fixed i18n label for "Zip Code" field to showing 5 character limit
-   `baza-dwolla-web-purchase-flow`: notifications banner rendering fixes for users without Dwolla account (`account-balance.component`, `dwolla-payment-methods.component`)
-   `baza-dwolla-web-purchase-flow`: fixes for error/warning banners on Dwolla payment methods page. Warning banner is hidden when error banner appears.
-   `baza-dwolla-web-verification-flow`: Fixed Dwolla consent condition, fixed UX bug with consent checkbox click, fixed issues with link configurations

## Breaking changes

-   The method of loading Dwolla VF library and providing configuration is updated. Please refer to the following documentation links:
    https://baza-docs.test.scaliolabs.com/web-development/customizations/verification-flow/01-verification-page-config
    https://baza-docs.test.scaliolabs.com/web-development/customizations/verification-flow/02-vf-query-param-links-config
-   The i18n configurations for Dwolla VF have been updated. Please refer to this documentation:
    https://baza-docs.test.scaliolabs.com/web-development/i18n/dwolla-vf/01-index-i18n
-   Please remove application level "Check Account" component from your code since it's now a part of Dwolla VF library. Please ensure to follow above 3 guides for proper configurations.
-   The form resources resolver is now moved from application to library level. Therefore, it's no longer required to be present on app routes
-   The input configurations of Dwolla VF have changed. Please refer to above-mentioned guides.

## Migration

-   If you would like to use Plaid flow with Redirect URL, please follow the guide "Redirect URL for Plaid Integration"
-   The form resources resolver has been moved to library level (in both NC and Dwolla VF), so there is no need to include that in application routes
-   The resolvers at Baza application and library level now can return data as part of route. So, same code logic can be implemented in target projects if required. And to get resolve data from route, following sample code is a reference:

```typescript
this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
    this.cart$ = of(params.purchase);
});
```

-   The i18n configurations were updated for the following components, please check documentation here (**In case override is required**):
    -   [Card details](https://baza-docs.test.scaliolabs.com/web-development/i18n/web-ui-components/09-card-details)
    -   [Bank details](https://baza-docs.test.scaliolabs.com/web-development/i18n/web-ui-components/06-bank-details)
    -   [Dwolla Investor](https://baza-docs.test.scaliolabs.com/web-development/i18n/dwolla-vf/04-vf-investor-i18n)
    -   [NC Investor](https://baza-docs.test.scaliolabs.com/web-development/i18n/nc-vf/04-vf-investor-i18n)

## Redirect URL for Plaid Integration

OAuth support is required in all Plaid integrations that connect to financial institutions. Without OAuth support, your end users will not be able to connect accounts from institutions that require OAuth, which includes several of the largest banks in the US. OAuth setup can be skipped only if your Plaid integration is limited to products that do not connect to financial institutions (Enrich, Identity Verification, Monitor, Document Income, and Wallet Onboard).

If you would like to use Plaid Integration with OAuth support, you should set up a page where users should be navigated to after finishing Plaid steps, create and register URL to this page as allowed Redirect URL in Plaid and set up Environment variables for supporting new flow on API side.

More details could be found on Plaid documentation page: https://plaid.com/docs/link/oauth/

### Setting up Redirect URL for API

Baza API is not using any Redirect URLs for Plaid Integration. You can enable it with provided a value for `BAZA_PLAID_REDIRECT_URL` environment variable:

-   You can use `{{ api }}`, `{{ cms }}` or `{{ web }}` tags in environment value. This tags will be replaced with values of `BAZA_APP_API_URL`, `BAZA_APP_CMS_URL` and `BAZA_APP_WEB_URL` environment variables
-   You can use `http` protocol for debugging reasons (Plaid Sandbox allows to use it). For production environment make sure you're using `https` schema in URL

### Using Baza NC Bank Account API with Redirect URL

Baza provides 2 endpoints to link Bank Accounts:

-   `/baza-nc/bank-account/link` - this endpoint provides `linkToken` which should be used to initialize Plaid SDK and open Plaid to user. You can pass `withRedirectUrl=1` in query params to enable redirection after finishing Plaid steps. You will be redirected to `BAZA_PLAID_REDIRECT_URL` with additional query params set by Plaid
-   `/baza-nc/bank-account/linkOnSuccess` - this endpoint should be called on page which is set as Redirect URL. You should have all necessary parameters from Query Params

## JIRA

-   [BAZA-1452](https://scalio.atlassian.net/browse/BAZA-1452) Update Purchase Flow Endpoints Document - Includes Dwolla Account (FE)
-   [BAZA-1484](https://scalio.atlassian.net/browse/BAZA-1484) Refactor Shared Logic Between Account & Dwolla Purchase Flow Library
-   [BAZA-1547](https://scalio.atlassian.net/browse/BAZA-1547) Cover Dwolla VF/PF and Account configurations with TSDocs
-   [BAZA-1698](https://scalio.atlassian.net/browse/BAZA-1698) Update Verification Flow Endpoints Document (FE)
-   [BAZA-1753](https://scalio.atlassian.net/browse/BAZA-1753) Data inconsistency in new read-only session endpoint response
-   [BAZA-1785](https://scalio.atlassian.net/browse/BAZA-1785) Migrate Baza codebase to Strict rules
-   [BAZA-1812](https://scalio.atlassian.net/browse/BAZA-1812) Migrate baza-core-api library to strict rules
-   [BAZA-1855](https://scalio.atlassian.net/browse/BAZA-1855) Update Default Redirect Routes for Dwolla VF
-   [BAZA-1856](https://scalio.atlassian.net/browse/BAZA-1856) Move Dwolla Check Account Component to Dwolla VF library
-   [BAZA-1920](https://scalio.atlassian.net/browse/BAZA-1920) Updates for Dwolla VF Library
-   [BAZA-1921](https://scalio.atlassian.net/browse/BAZA-1921) Refactor resolvers to ensure data is returned back
-   [BAZA-1936](https://scalio.atlassian.net/browse/BAZA-1936) PTCL - Maintenance: 503 error loop on Web when Maintenance mode is enabled
-   [BAZA-1938](https://scalio.atlassian.net/browse/BAZA-1938) API "/baza-nc/bank-account/link" is not passing "redirect_uri"
-   [BAZA-1943](https://scalio.atlassian.net/browse/BAZA-1943) Update BE logic for "Dwolla Current" endpoint
-   [BAZA-1945](https://scalio.atlassian.net/browse/BAZA-1945) CW - WEB: Verification flow: incorrect validation of Zip Code
-   [BAZA-1947](https://scalio.atlassian.net/browse/BAZA-1947) CW - WEB: Account: Investor Profile: Go Back button on the Verification Flow redirects user to the Offerings page
-   [BAZA-1948](https://scalio.atlassian.net/browse/BAZA-1948) CW - Both the message appears at the same time while creating a payout method
-   [BAZA-1950](https://scalio.atlassian.net/browse/BAZA-1950) Refactor i18n Translation Based Form Validations
-   [BAZA-1958](https://scalio.atlassian.net/browse/BAZA-1958) Verification Flow - Dwolla consent checkbox is not displayed for US investor without updated bootstrap call

## PRs

-   https://github.com/scalio/baza/pull/1142
-   https://github.com/scalio/baza/pull/1387
-   https://github.com/scalio/baza/pull/1403
-   https://github.com/scalio/baza/pull/1370
-   https://github.com/scalio/baza/pull/1384
-   https://github.com/scalio/baza/pull/1378
-   https://github.com/scalio/baza/pull/1386
-   https://github.com/scalio/baza/pull/1385
-   https://github.com/scalio/baza/pull/1390
-   https://github.com/scalio/baza/pull/1394
-   https://github.com/scalio/baza/pull/1391
-   https://github.com/scalio/baza/pull/1392
-   https://github.com/scalio/baza/pull/1405
