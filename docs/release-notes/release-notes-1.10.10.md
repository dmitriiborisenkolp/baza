# Baza 1.10.10

New release is focused on finishing current features of nc-integration packages, covering nc-integration packages with e2e tests and fixed various issues aroung baza-core package.

### baza-core

-   CKEditor now correctly displays italic, bold, list items and other styles in CMS
-   "Restricted accounts" renamed to "Whitelisted accounts"
    - Fixed issues with displaying preview for uploaded video files
-   Added IP column for Auth Sessions

### baza-nc

-   City field now has additional validation (prevents "Data incomplete/invalid" error)
-   Investments endpoints now returns Created At (first transaction date) and Updated At (last transaction date) fields.

### baza-nc-integration

-   Feed API received huge reworked
-   All APIs are covered with integration tests
-   Subscriptions public API now is displayed in Swagger/Redoc
-   Metadata field for Listings is not marked as required anymore
-   Schema now has additional Key/Value field type
-   Listings now has "Is Published?" flag

## Documentation updates

-   Added documentation about NPM libraries and setting up new NPM libraries with Baza/Nx
-   Added new Investment Platform section with documentation about setting up new project with NC and NC integration packages

## Repository

-   Commitlint integration updated
-   Committizen tool added to repository.
-   New `yarn commit` command added which helps with creating commit messages in correct way

## JIRA

-   https://scalio.atlassian.net/browse/BAZA-502
-   https://scalio.atlassian.net/browse/BAZA-483
-   https://scalio.atlassian.net/browse/BAZA-484
-   https://scalio.atlassian.net/browse/BAZA-419
-   https://scalio.atlassian.net/browse/BAZA-462
-   https://scalio.atlassian.net/browse/BAZA-486
-   https://scalio.atlassian.net/browse/BAZA-485
-   https://scalio.atlassian.net/browse/BAZA-473
-   https://scalio.atlassian.net/browse/BAZA-474
-   https://scalio.atlassian.net/browse/BAZA-472
-   https://scalio.atlassian.net/browse/BAZA-481
-   https://scalio.atlassian.net/browse/BAZA-405
-   https://scalio.atlassian.net/browse/BAZA-466

## PRs

-   https://github.com/scalio/baza/pull/235
-   https://github.com/scalio/baza/pull/236
-   https://github.com/scalio/baza/pull/237
-   https://github.com/scalio/baza/pull/238
-   https://github.com/scalio/baza/pull/239
-   https://github.com/scalio/baza/pull/241
-   https://github.com/scalio/baza/pull/241
-   https://github.com/scalio/baza/pull/242
-   https://github.com/scalio/baza/pull/243
-   https://github.com/scalio/baza/pull/244
-   https://github.com/scalio/baza/pull/245
-   https://github.com/scalio/baza/pull/245
-   https://github.com/scalio/baza/pull/246
-   https://github.com/scalio/baza/pull/247
-   https://github.com/scalio/baza/pull/248
-   https://github.com/scalio/baza/pull/250
-   https://github.com/scalio/baza/pull/251
-   https://github.com/scalio/baza/pull/252
-   https://github.com/scalio/baza/pull/253
-   https://github.com/scalio/baza/pull/254
-   https://github.com/scalio/baza/pull/255
-   https://github.com/scalio/baza/pull/256
-   https://github.com/scalio/baza/pull/257
-   https://github.com/scalio/baza/pull/258
-   https://github.com/scalio/baza/pull/259
-   https://github.com/scalio/baza/pull/260
-   https://github.com/scalio/baza/pull/261
-   https://github.com/scalio/baza/pull/262
-   https://github.com/scalio/baza/pull/263
-   https://github.com/scalio/baza/pull/264
-   https://github.com/scalio/baza/pull/265
-   https://github.com/scalio/baza/pull/266
-   https://github.com/scalio/baza/pull/268
-   https://github.com/scalio/baza/pull/269
-   https://github.com/scalio/baza/pull/270
-   https://github.com/scalio/baza/pull/270
-   https://github.com/scalio/baza/pull/272
-   https://github.com/scalio/baza/pull/273
-   https://github.com/scalio/baza/pull/274
-   https://github.com/scalio/baza/pull/275
-   https://github.com/scalio/baza/pull/276
