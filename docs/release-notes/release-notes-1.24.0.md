# Release 1.24.0

Baza 1.24.0 redefines the way how payments via Dwolla will be processed. Before we used Master Account Balance as target Funding Source for receiving investors funds. This release introducing Dwolla EScrow Feature which is controlling where funds should be placed.

This Release is also focused around Account Verification and Purchase Flow Web libraries with multiple fixes and configuration options. We also added a shared Web Module / Component for Dwolla Payout and Account Balance sections which could be used in Account page.

## What's new

-   `baza-dwolla`: Added new Dwolla Escrow API & CMS. This feature allows select Funding Source which will be used as Account Balance
    -   You can set Bank or Account Balance as Escrow Account.
    -   By Default Account Balance is set as Escrow Account
    -   Production environment: It's not possible to use Account Balance as Escrow. As far as Account Balance is set as default EScrow on initial configuration, API will throw an error in attempt to purchase shares when Account Balance is selected as Escrow
    -   UAT/Preprod environments: It's not possible to use Bank Account as Escrow. As far as Account Balance is set as default EScrow on initial configuration. There is same interraction with Purchase Flow
-   `baza-nc-api`: Bank Account API by defaults secures Account and Routing Numbers. It's possible to disable it with `bazaNcApiConfig.forceSecureBankAccounts` configuration, but it's strongly not recommend to any environments excepts E2E.
-   `baza-nc-api`: Purchase Shares via Dwolla now uses Dwolla Escrow instead of Master Account Balance only
-   `baza-nc-api`: Account Verification now can work without Netwoth/Annual Household Income. Added integration tests for updated flow.
-   `baza-core-api`: `EnvService` received additional getters
    -   (UNCHANGED) `isProductionEnvironment` returns true for Production, Preprod and UAT environments
    -   (NEW) `isUATEnvironment` returns true for UAT and Preprod environments
    -   (NEW) `isPublicProductionEnvironment` returns true for Production only environments
-   `baza-nc-api`: Fields `currentAnnualHouseholdIncome` and `netWorth` from Account Verification are optional now. Investor Profile will be marked as Completed Account Verification without these fields.
-   `baza-dwolla-web-verification`: Fields `Income ($)` and `What is your net worth (excluding primary residence)?` are removed from second step of Account Verification flow.
-   `baza-dwolla-web-purchase-flow`: Added new option to customize Account Balance title
-   `baza-nc-api`: add ssn validation for cms update investor personal details
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-ui-components`: This release covers adding the "Payment Section" in the new "Payment & Payout Methods (Dwolla)" tab in account.
    -   Created and implemented payment section components:
        -   `DwollaAccountBalanceComponent`
        -   `DwollaBankAccountComponent`
        -   `DwollaCreditCardComponent`
    -   Logic to attach a cash-in bank account to the account balance was implemented.
    -   Logic to trigger manual NC ACH account was added to `DwollaBankAccountComponent`
    -   Logic to attach credit card as a payment method was added to `DwollaCreditCardComponent`
    -   Account balance cash-in method was isolated from NC ACH account added manually.
    -   Added missing labels on the different payment sections and fixed the empty state for credit card and account balance.
    -   Created `DwollaWithdrawFundsModalComponent` and `DwollaWithdrawFundsFormComponent` components although they're not being used at the moment due to the latest requirements.
    -   Added tooltips next to section headings" to PR description points.
    -   Additional component `AddFundsNotification` was added to display a notification popup after adding funds.
    -   Warning message was added to `DwollaAddFundsFormComponent` about funds not being immediately added to account balance.
    -   Additionally, a dynamic component was created to easily handle any kind of payment method (e.g. account balance, credit card, etc.). It's not limited to the scope of the payment/payouts module and it can be used across the project. A guide on how to use can be found [here](https://docs.google.com/document/d/1wX-21iTGSaOX0ZvUOg1ewYWA3oh7lGw_PgwbkL9hx0s/edit) and it can be imported from `@scaliolabs/baza-nc-web-ui-components`.
    -   Implemented design for `PaymentItemComponent`

## Bugfixes

-   `baza-dwolla-web-verification`: Fixes the state validation required status and introduces updated zip code field design in different use-cases.
    -   If a country is selected and doesn't have the list of states, the select box will reset and state value sent to API is N/A
    -   If the state select-box is hidden, the zip code field takes all the available space
    -   If the state select-box is shown, the state and zip code fields appear side-by-side
    -   If the selected country has at least one available state, the state field should be required
-   `baza-dwolla-web-verification`, `baza-nc-web-verification`: added UI related fixes in both Dwolla PF and VF libraries.
    -   Dwolla consent checkbox is now selectable upon clicking the consent text as well
    -   When Dwolla Account is created and it's selected as a payment method, the balance section doesn't have any pointer events now indicating it's not a button
    -   When Dwolla account is available, but not a bank account or credit card, the margin issues between the cells are now fixed
    -   When ACH Bank Account is selected as the payment method, in edit popup the whole cell is not clickable anymore, only the "Update" link is clickable
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-purchase-flow`: Updated message for Insufficient Funds case:
    -   BEFORE: `There are insufficient funds to proceed with this payment method. Alternatively, you can proceed with purchases with Manual Bank Account linking or a Card.`
    -   AFTER: `There are insufficient funds to proceed with this payment method. Please choose other payment method below instead.`
-   `baza-dwolla-cms`: Fixed CMS ID for Dwolla Payments CMS (it's not `baza-dwolla-payments-cms-id`)
-   `baza-core-api`: Device Token Logs are disabled by default. You can enable Device Token Logs with `BAZA_DEVICE_TOKEN_LOGS` environment variable
-   `baza-dwolla-web-verification`, `baza-dwolla-web-purchase-flow`: Fixes for some new/re-opened bugs found after feedback related to UI found in Dwolla PF & VF.
    -   A new style file `_payment-subtitle.less` was created to separate out styles
    -   The hover animation effect within edit popup is now applicable only on "Update" link if radio button doesn't exist, else on the entire section
    -   Upon clicking the Dwolla consent label, if links are clicked they are opened in separate tab else if any other area is clicked within the label boundary, then consent checkbox is selected/unselected
-   `baza-dwolla-web-purchase-flow`: Added the `payment-btn-inverse` class on each of the buttons within "Edit Payment" popup modal in Dwolla PF library. This will allow other projects to manage theming and showing text on top of white/dark backgrounds.
-   `baza-dwolla-web-purchase-flow`: Clean up & Refactoring performed on Dwolla Purchase Flow web library:
    -   All headings are replaced with `div` tags and specific heading styles are inherited inside their CSS classes on Dwolla web purchase & verification libraries.
    -   Grid system implementation is replaced from nz attributes to mixin `flex-col-width` on Dwolla web purchase & verification libraries.
    -   Class `payment-block__alert-warning` was removed because warning alerts style was standardized [here](https://github.com/scalio/baza/pull/1012/files#diff-acaba1a28617d1cd069145e3364849519c5a5083d400d38a429622805cc2a808R830)
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-ui-components`: Fixed multiple responsive issues on account payments/payouts
-   `baza-dwolla-web-verification`, `baza-dwolla-web-purchase-flow`: The Release integrations the ability to add custom routes configuration for "buy shares" and "verification" links in Dwolla Purchase Flow and Account Verification libraries.
    -   New properties `buySharesLink` and `verificationLink` were added to `VerificationConfig` and `PurchaseConfig` interfaces respectively
    -   The default values are `'/buy-shares'` and `'/verification'` for purchase & verification route configuration respectively
    -   Custom configuration object was updated in verification-custom and purchase-custom components respectively
    -   In purchase and verification components, logic was added to save configurations in store. This allows for all other components e.g. details, agreement, payment etc. to re-use the same links
    -   All places where hard coded links for `/buy-shares-dwolla` and `/verification-dwolla` were being used are now updated with configuration links
    -   Baza documentation portal was updated with details on how to use new configurations
-   `baza-dwolla-cms`: Fixed translations for cash-in operations in Dwolla Payments CMS
-   `baza-dwolla-web-purchase-flow`: This PR covers adding the "Payout Section" in the new "Payment & Payout Methods (Dwolla)" tab in account.
    -   A new component `DwollaBankAccountPayoutComponent` was added along with module for payout section
    -   Relevant logic to add/update cash-out account (as per new bank account system) was added
    -   This feature is only available for US national investors
    -   If an investor doesn't have a Dwolla account already, an attempt to create account is made which if fails, will disable the payout section itself with an error message
    -   If investor already has a Dwolla account, then clicking on "Payout Section" would trigger Dwolla plaid flow and at the completion, a new cash-out account would be linked with investor account
    -   If a cash-out account already exists, it's relevant details are displayed along with an "Update" for easy updating cash-out bank account through Plaid
    -   All checks as per user story were covered
-   `baza-dwolla-web-purchase-flow`: fixed some bugs for Account components and "Add Funds" form specifically, which is being used in Dwolla PF and Account components.
    -   Fixed mis-spelling error on "Add Funds" warning above amount textbox
    -   Added `ant-form-inverse` to "Add Funds" form (For Common-Wealth Project)
    -   Fixed the flow where if cash-in account was updated (when no cash-out is linked), the cash-out account was successfully linked but not updating on screen
    -   Fixed the flow where every time Dwolla Plaid flow is completed, it was triggering “Add Funds” form but in case of clicking "Update” button directly from within account balance section, the “Add Funds” form should not be triggered after on success.
    -   Fixed a bug where if no payment method is linked and user has Dwolla verified account, then account balance was not showing

## Breaking changes

-   We may encounter regression issues with Elite Investors Feature. Please make an attention on it.

## Migration

-   **PRODUCTION-ONLY**: You should set up a Bank Account to use as Escrow Funding Source in Dwolla Panel. When EScrow FS will be created and verified, you should go to CMS and use newly added Escrow FS (In Dwolla Escrow CMS section).
-   You should replace `bazaDwollaPaymentCmsRoutes` import with `bazaDwollaCmsRoutes` for displaying Dwolla Escrow CMS in `apps/cms/src/app/app.module.ts`
-   You should include ` ...bazaDwollaApiBundleRegistry` to registry schema configuration in `api.registry.ts`

## Account Balance Title Customisation

### Customize the account balance title in the `DwollaPurchasePaymentComponent` component.

```html
<app-purchase-payment-dwolla
    [paymetConfig]="{
        paymentMethodConfig: {
            accountBalanceTitle: 'Custom account balance title'
        }
    }"></app-purchase-payment-dwolla>
```

### Customize the account balance title in `DwollaAccountBalanceComponent`

```html
<app-dwolla-account-balance title="Custom account balance title"></app-dwolla-account-balance>
```

### Customize the account balance title in `AccountBalanceCardComponent`

```html
<app-account-balance-card title="Custom account balance title"></app-account-balance-card>
```

## Guide for Other Projects on How to Use Custom Routes Configurations

Please refer to the following sections in `baza-docs` portal on complete guide on how to use custom route configurations when using Dwolla VF and PF libraries.

-   https://baza-docs.test.scaliolabs.com/web-development/customizing-verification-flow/01-verification-page-config
-   https://baza-docs.test.scaliolabs.com/web-development/customizing-purchase-flow/01-purchase-page-config

## How To Use Dwolla Payout Method Section in Account - Guide For Other Projects (e.g. Common-Wealth):

1. Include `DwollaBankAccountPayoutModule` in relevant module e.g. account module
2. Use the selector `<app-dwolla-bank-account-payout>` to use the payout section anywhere in project
3. All relevant logic is encapsulated within the component itself, including all labels.

## How to Use Dwolla Account Balance Component - Guide For Other Projects (e.g. Common-Wealth):

**Account balance:**

Import `DwollaAccountBalanceModule` inside the relevant module e.g. account module and use it in the template.

```ts
import { DwollaAccountBalanceModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';
```

```html
<app-dwolla-account-balance></app-dwolla-account-balance>
```

**Bank account:**

Import `DwollaBankAccountModule` inside the relevant module e.g. account module and use it in the template.

```ts
import { DwollaBankAccountModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';
```

```html
<app-dwolla-bank-account></app-dwolla-bank-account>
```

**Credit card:**

Import `DwollaCreditCardModule` inside the relevant module e.g. account module and use it in the template.

```ts
import { DwollaCreditCardModule } from '@scaliolabs/baza-dwolla-web-purchase-flow';
```

```html
<app-dwolla-credit-card></app-dwolla-credit-card>
```

**Note:** all relevant logic is encapsulated within the components themselves, including all labels.

## JIRA

-   [BAZA-1466](https://scalio.atlassian.net/browse/BAZA-1466) Update Country-State-Zip Validation & Design on VF library
-   [BAZA-1491](https://scalio.atlassian.net/browse/BAZA-1491) Update copy in the Purchase flow where there are insufficient funds on Dwolla balance
-   [BAZA-1493](https://scalio.atlassian.net/browse/BAZA-1493) Make "Account Balance" label customizable in Purchase Flow and Account: Payment&Payout methods
-   [BAZA-1482](https://scalio.atlassian.net/browse/BAZA-1482) Purchase Shares: Use Escrow funding source from Dwolla Escrow section as a destination for Dwolla purchases
-   [BAZA-1483](https://scalio.atlassian.net/browse/BAZA-1483) Add a Dwolla Escrow section in CMS to setup Escrow funding source ID
-   [BAZA-1498](https://scalio.atlassian.net/browse/BAZA-1498) Disable Device Token API logs by default
-   [BAZA-1487](https://scalio.atlassian.net/browse/BAZA-1487) Purchase Flow Web - UI Issues observed in dwolla library purchase flow
-   [BAZA-1500](https://scalio.atlassian.net/browse/BAZA-1500) Add inverse class for buttons inside modal window
-   [BAZA-1501](https://scalio.atlassian.net/browse/BAZA-1501) Clean up new Dwolla libs & components
-   [BAZA-1503](https://scalio.atlassian.net/browse/BAZA-1503) Account - Payments & Payout - UI issues on mobile web view
-   [BAZA-1507](https://scalio.atlassian.net/browse/BAZA-1507) Account ->Payments & Payouts - Web UI Issues
-   [BAZA-1508](https://scalio.atlassian.net/browse/BAZA-1508) Account ->Payments & Payouts - Irrelevant message is being shown to international investors in withdraw section
-   [BAZA-1497](https://scalio.atlassian.net/browse/BAZA-1497) Create a config to customize VF and PF routes
-   [BAZA-1504](https://scalio.atlassian.net/browse/BAZA-1504) CMS - Trade payment type is not correctly updated
-   [BAZA-1366](https://scalio.atlassian.net/browse/BAZA-1366) Testing handling Customer failed verification
-   [BAZA-1388](https://scalio.atlassian.net/browse/BAZA-1388) BE - New user - On first login user sees error message of not being a verified investor
-   [BAZA-1457](https://scalio.atlassian.net/browse/BAZA-1457) Purchase Flow - "Could Not Start Session" error appears on Purchase Flow for some specific decimal values of purchase Price
-   [BAZA-1411](https://scalio.atlassian.net/browse/BAZA-1411) Account: Payments & Payouts => Payment Methods Integration
-   [BAZA-1412](https://scalio.atlassian.net/browse/BAZA-1412) Account: Payments & Payouts => Payout Methods Integration
-   [BAZA-1418](https://scalio.atlassian.net/browse/BAZA-1418) Account: Payments & Payouts => Account Balance Integration
-   [BAZA-1449](https://scalio.atlassian.net/browse/BAZA-1449) Validation checks missing for personal information in updateInvestorAccountPersonalInfo
-   [BAZA-1458](https://scalio.atlassian.net/browse/BAZA-1458) Re-enable Elite Investor Integration Test
-   [BAZA-1521](https://scalio.atlassian.net/browse/BAZA-1521) Account -> Payments & Payouts - Typo error in message on Add Funds
-   [BAZA-1521](https://scalio.atlassian.net/browse/BAZA-1486) Account - Investor tab - Information does not appear after completing verification flow until we reload page on this tab

## PRs

-   https://github.com/scalio/baza/pull/1038
-   https://github.com/scalio/baza/pull/1039
-   https://github.com/scalio/baza/pull/1040
-   https://github.com/scalio/baza/pull/1041
-   https://github.com/scalio/baza/pull/1042
-   https://github.com/scalio/baza/pull/1043
-   https://github.com/scalio/baza/pull/1044
-   https://github.com/scalio/baza/pull/1046
-   https://github.com/scalio/baza/pull/1048
-   https://github.com/scalio/baza/pull/1049
-   https://github.com/scalio/baza/pull/1051
-   https://github.com/scalio/baza/pull/1052
-   https://github.com/scalio/baza/pull/1034
-   https://github.com/scalio/baza/pull/1036
-   https://github.com/scalio/baza/pull/1054
-   https://github.com/scalio/baza/pull/1012
-   https://github.com/scalio/baza/pull/1050
