# Release 1.7.3

## What's new

-   Baza Crud + Baza Form Builder injection features
-   Every Baza Crud List of Baza Form can now contains unique ID. You can externally change default CMS implementation with your custom configuration.

## How to inject custom configuration (Example):

```typescript
// app.module.ts
export class AppModule {
    constructor(
        private readonly formInjectService: BazaFormBuilderInjectService,
        private readonly crudInjectService: BazaCrudInjectService,
    ) {
        formInjectService.register(BAZA_CORE_ACCOUNT_CMS_FORM_USER, {
            inject: (formConfig, form) => {
                const layout: BazaFormBuilderWithTabs = formConfig.contents as any;
                form.addControl('seo', new FormControl(bazaSeoDefault(), [Validators.required]));
                layout.tabs.push({
                    title: 'SEO',
                    contents: {
                        fields: [
                            {
                                type: BazaFormBuilderControlType.BazaSeo,
                                formControlName: 'seo',
                            },
                        ],
                    },
                });
                // changes endpoints also for custom implementation etc
            },
        });
        crudInjectService.register(BAZA_CORE_ACCOUNT_CMS_ID, {
            inject: (crudConfig) => {
                crudConfig.data.columns.push({
                    type: {
                        kind: BazaTableFieldType.Link,
                        format: () => 'Tax Documents',
                        link: (entity) => ({
                            routerLink: ['/tax-documents', entity.id],
                        }),
                    },
                    title: '',
                    width: 180,
                    textAlign: BazaTableFieldAlign.Center,
                });
            },
        });
    }
}
```
