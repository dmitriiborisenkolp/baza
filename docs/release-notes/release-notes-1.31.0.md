# Release 1.31.0

**WARNING**: This release contains critical issues which leads to losing SSN numbers during Account Verification flow. Consider to use 1.31.1 or any other versions released later.

## What's new

-   `libs`: we moved codebase of most libraries to strict rules. We also fixed a lot of eslint warnings and replaced `any` with `unknown` for most cased.
-   `baza-nc-cms`: created `bazaNcSchemaDuplicatedTabValidator` to implement custom validators to check for duplicated schema tabs & fields titles
-   `baza-core-api`: improve health check endpoint by mapping the health check status from string to HTTP code, also populated the error object with possible errors in health services.
-   `baza-dwolla-web-verification-flow`: added whitespace validator to `firstName` field.
-   `baza-nc-api`: add the `async` processing option to cms dividends transaction processing and set it async processing as default. This fixes issues with "Request Timeout" error when processing Dividend Transaction with a lot of items.
-   `baza-{nc,dwolla}-web-purchase-flow`: Added support for passing bootstrap data from application level to PF (NC, Dwolla) and Dwolla Account Components
-   `baza-dwolla-web-purchase-flow`: Added UX improvement to automatically re-open "Add Funds" popup in case Plaid flow popup is exited without completion
-   `baza-web-utils`: Created a new subject in web util shared service for dynamically requesting bootstrap data from within libs/components
    `sandbox-web`: Following points were covered:
-   Removed deprecated bootstrap actions from `BootstrapState` on application level
-   Added bootstrap resolver in relevant routes (portfolio transactions, maintenance, PF flows, Account) and event handler implementation for refreshing bootstrap data manually in purchase custom components
-   Removed leftover unused `appSortElementsDirective` from code except the directive itself

## Bugfixes

-   `baza-dwolla-web-verification-flow`: fixed issue of user being able to save empty `firstName` field.
-   `baza-{nc,dwolla}-web-purchase-flow`: Following fixes were made:
    -   Fixed the issue of duplicate calls in Dwolla account components
    -   Removed unnecessary code related to "Add Funds" in Dwolla PF, as feature is now moved to Dwolla Account components
        `baza-nc-web-purchase-flow`: Fixed build/runtime issues by adding missing `@UntilDestroy` decorator

## Breaking changes

-   `baza-nc-web-purchase-flow`: removed configuration type `PaymentMethodOptionsConfig`.
-   `baza-nc-web-purchase-flow`: removed directive usage from `NcPaymentEditUpdateComponent`, `NcPaymentBankAccountComponent`, `NcPaymentCardComponent`.
-   `baza-nc-web-purchase-flow`: removed default config object initialization.

The `PaymentMethodOptionsConfig` was completely removed from the project:

```typescript
export interface PaymentMethodConfig {
    showPurchaseDividendMessage?: boolean;
    methodOptionsConfig?: PaymentMethodOptionsConfig; // REMOVED
}

export interface PaymentMethodOptionsConfig {
    // REMOVED
    plaidOrder?: number;
    manualBankOrder?: number;
    bankSectionOrder?: number;
    cardSectionOrder?: number;
}
```

-   `baza-{nc,dwolla}-web-purchase-flow`: Following points are to be noted:
-   The interface `AccBalanceConfig` is now updated as per the following format:

```typescript
const defaultConfig: AccBalanceConfig = {
    showAccountBalance: true,
    showBankAccount: true,
    showCreditCard: true,
    links: {
        contact: {
            extLink: {
                link: 'bazaContentTypes.contacts.email',
                text: 'contact us',
                isCMSLink: true,
                isMailLink: true,
            },
        },
    },
};
```

-   Some of PF (Dwolla, NC both) and Dwolla Account components now have required `initData$` input parameter which should have the bootstrap data passed down from app. level. Also, relevant refresh event handler should be created in parent page.

-   The NC PF "Payment Page" should now be used as follows:

```html
<app-nc-purchase-payment
    *ngIf="currentTab === 2"
    target="purchase-payment"
    [initData$]="initData$"></app-nc-purchase-payment>
```

And relevant code-behind "Purchase Custom (NC)" should be updated as follows:

```typescript
export class PurchaseCustomComponent {
    // Get bootstrap data from application level
    initData$ = this.store.select(BootstrapState.initData);

    constructor(private route: ActivatedRoute, private store: Store, private readonly wts: BazaWebUtilSharedService) {
        // create event handler to manually refresh bootstrap data
        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe((res) => {
            if (res) {
                this.store.dispatch(new RequestAppBootstrapInitData());
            }
        });
    }
}
```

-   Similar to NC PF, the Dwolla PF "Payment Page" should now be used as follows:

```html
<app-purchase-payment-dwolla
    *ngIf="currentTab === 2"
    target="purchase-payment-dwolla"
    [initData$]="initData$"></app-purchase-payment-dwolla>
```

And relevant code-behind "Purchase Custom (Dwolla)" should be updated as follows:

```typescript
export class DwollaPurchaseCustomComponent {
    // Get bootstrap data from application level
    initData$ = this.store.select(BootstrapState.initData);

    constructor(private route: ActivatedRoute, private store: Store, private readonly wts: BazaWebUtilSharedService) {
        // create event handler to manually refresh bootstrap data
        this.wts.refreshInitData$.pipe(untilDestroyed(this)).subscribe((res) => {
            if (res) {
                this.store.dispatch(new RequestAppBootstrapInitData());
            }
        });
    }
}
```

## Changes for Baza Documentation Portal

-   `baza-docs-web`: the sidenav items are highlighted based on if they match the current route.
-   `baza-docs-web`: the sidenav opens automatically the sub-menu that contains an active item.
-   `baza-docs-web`: fixed type errors when assigning `routerLink` and `mailTemplateRouterLink` functions to the `routerLink` attribute.

## Documentation:

Please refer to following updated documentations for Dwolla Account Components migration:

-   https://baza-docs.test.scaliolabs.com/web-development/customizations/account-components/05-payment-methods-config
-   https://baza-docs.test.scaliolabs.com/web-development/customizations/account-components/06-payout-methods-config

## JIRA

-   [BAZA-1496](https://scalio.atlassian.net/browse/BAZA-1496) Update BootstrapStore logic on App level and in Account, Dwolla PF and NC PF
-   [BAZA-1763](https://scalio.atlassian.net/browse/BAZA-1763) CMS: Schema: It's possible to create 2 tabs with the same name and 2 fields with the same Title and Field
-   [BAZA-1779](https://scalio.atlassian.net/browse/BAZA-1779) Initial Setup - Update bootstrap state logic and save information at app. level
-   [BAZA-1780](https://scalio.atlassian.net/browse/BAZA-1780) Update bootstrap store logic for Account Components
-   [BAZA-1781](https://scalio.atlassian.net/browse/BAZA-1781) Update bootstrap store logic for Dwolla PF
-   [BAZA-1782](https://scalio.atlassian.net/browse/BAZA-1782) Update bootstrap store logic for NC PF
-   [BAZA-1784](https://scalio.atlassian.net/browse/BAZA-1784) Revert sorting payment methods logic for NC PF
-   [BAZA-1785](https://scalio.atlassian.net/browse/BAZA-1785) Migrate Baza codebase to Strict rules
-   [BAZA-1786](https://scalio.atlassian.net/browse/BAZA-1786) Migrate baza-core-shared library to strict rules
-   [BAZA-1787](https://scalio.atlassian.net/browse/BAZA-1787) Migrate baza-content-types-shared library to strict rules
-   [BAZA-1788](https://scalio.atlassian.net/browse/BAZA-1788) Migrate baza-dwolla-shared library to strict rules
-   [BAZA-1789](https://scalio.atlassian.net/browse/BAZA-1789) Migrate baza-nc-shared library to strict rules
-   [BAZA-1790](https://scalio.atlassian.net/browse/BAZA-1790) Migrate node-access libraries to strict rules
-   [BAZA-1791](https://scalio.atlassian.net/browse/BAZA-1791) Migrate baza-nc-integration-shared library to strict rules
-   [BAZA-1793](https://scalio.atlassian.net/browse/BAZA-1793) Migrate data-access (non-CMS) libraries to strict rules
-   [BAZA-1794](https://scalio.atlassian.net/browse/BAZA-1794) Migrate baza-plaid-shared library to strict rules
-   [BAZA-1795](https://scalio.atlassian.net/browse/BAZA-1795) Remove dwolla-sandbox-lib-\* libraries family, application and any other related resoures
-   [BAZA-1798](https://scalio.atlassian.net/browse/BAZA-1798) Migrate baza-web-ui-components library to strict rules
-   [BAZA-1799](https://scalio.atlassian.net/browse/BAZA-1799) Migrate baza-core-ng library to strict rules
-   [BAZA-1800](https://scalio.atlassian.net/browse/BAZA-1800) Migrate baza-nc-web-purshase-flow and baza-dwolla-web-purhase-flow to strict rules
-   [BAZA-1819](https://scalio.atlassian.net/browse/BAZA-1819) Need to have the Distribution processed date displayed on wallet transaction history
-   [BAZA-1838](https://scalio.atlassian.net/browse/BAZA-1838) Improve terminus Health Check endpoint
-   [BAZA-1849](https://scalio.atlassian.net/browse/BAZA-1849) Verification Flow: Able to save personal information with only space character in first name
-   [BAZA-1850](https://scalio.atlassian.net/browse/BAZA-1850) Dividend Process endpoint should works in async way by default
-   [BAZA-1853](https://scalio.atlassian.net/browse/BAZA-1853) Baza Documentation Portal - auto-open related section(s) when opening a direct link to documentation article
-   [BAZA-1859](https://scalio.atlassian.net/browse/BAZA-1859) Open the "Add Funds" modal if Plaid flow is opened and exited from "Edit" link within modal
-   [BAZA-1886](https://scalio.atlassian.net/browse/BAZA-1886) Add missing decorators for bootstrap resolver and nc-shared service
-   [BAZA-1907](https://scalio.atlassian.net/browse/BAZA-1907) Update bootstrap store logic in Portfolio, fix usage in NC PF and Clean-up

## PRs

-   https://github.com/scalio/baza/pull/1259
-   https://github.com/scalio/baza/pull/1254
-   https://github.com/scalio/baza/pull/1263
-   https://github.com/scalio/baza/pull/1284
-   https://github.com/scalio/baza/pull/1250
-   https://github.com/scalio/baza/pull/1255
-   https://github.com/scalio/baza/pull/1268
-   https://github.com/scalio/baza/pull/1265
-   https://github.com/scalio/baza/pull/1267
-   https://github.com/scalio/baza/pull/1272
-   https://github.com/scalio/baza/pull/1264
-   https://github.com/scalio/baza/pull/1273
-   https://github.com/scalio/baza/pull/1269
-   https://github.com/scalio/baza/pull/1277
-   https://github.com/scalio/baza/pull/1274
-   https://github.com/scalio/baza/pull/1287
-   https://github.com/scalio/baza/pull/1266
-   https://github.com/scalio/baza/pull/1271
-   https://github.com/scalio/baza/pull/1290
-   https://github.com/scalio/baza/pull/1282
-   https://github.com/scalio/baza/pull/1298
-   https://github.com/scalio/baza/pull/1301
-   https://github.com/scalio/baza/pull/1345
