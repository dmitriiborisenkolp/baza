# Release 1.10.9

## What's new:

Release scope was heavily increased with CMNW issues.

## baza-core

-   Multi-Image component now allows to select multiple files to upload
-   HTML Title registry key removed
-   Empty S3 Object ID for AWS resources will not lead to errors / exceptions anymore; Releated methods will returns `undefined` for such entities.
-   Account exists feature (endpoint) added. New endpoint displays is there any active account with given email exists or/and was there any deactivated accounts before. Disabled by default; see https://github.com/scalio/baza/pull/200 for instruction how to enabled it for target project.
-   Customer.IO mail transport: `fake_bcc: false` configuration added.
-   Phone fied added to Account. New field will not be a part of 2FA via Phone/SMS feature; we will use different approach for confirming that phone is actually owned by user.
-   Unique tokens format for email verification emails could be replaced with custom implementation
-   Form fields in CMS now can have optional Help tooltip
-   You can search by ID in Account CMS or in Account Selector Modal
-   Restricted access feature added.

## baza-nc

-   New Health Check tool added. Health Check tool analyze trades both at NC and local DB and displays Offering health status. If offering is not synced well, there will be suggestions displayed how to fix it.
-   Trades (Transactions) sync tool now also analyze Fund Status of payment. Trades which has `CREATED` order status but `FUNDED`/`SETTLED`/`RETURNED` payments will be correctly displayed.
-   Link Investor Account feature received some updates. Now Link Account feature allows to re-assign Nc Account ID / Nc Party ID to different user, payment methods / transactions could be automatically synced and percents funded for related offerings updated.
-   Emails for NC Account / NC Party automatically updates on linking investor account
-   Form resources and List States endpoints could be used now w/o authentication.
-   New `bazaNcCharsetValidator` validator (Angular) added. Validator is used for CMS fields and should be used for all data which are supposed to be sent to North Capital.
-   Admin are not able now to unverify yourself with Account CMS
-   CMS: Menu items under Baza section could be configured now.

## baza-nc-integration

-   Schema select in Listings now has `– No schema –` option. Fixed a bug which does not allow to clear Schema from Listing.
-   Fixed once more bug when Listing NC Status was resetting to `Coming Soon` after updating Listing.
-   API Documention for Subscriptions added to Swagger / Redoc.
-   Perks feature added for Listings
-   Feed: "Category" field renamed to "Associated Listing"
-   Feed: Tags could be added now to posts.
-   Timeline feature integrated with Listings. Each Listing could have own Timeline.

## baza-content-types

-   New Timeline content type added. Most of fields could be disabled for target projects.
-   Categories now can be `Internal` or `Public`. Public categories works as usual, Internal categories are not displaying to users and could be used to link content types with external entities. Example: each Listings in `baza-nc-integration` package has its own Timeline list; Timeline entities are linked to Interal category which helps to hide Timeline entities for different Listings from usual Content Type CMS section and also categorize Timeline entities between Listings.

# Repository

-   Lint command added & lint will be run on pre-commit hook. All detected lint issues fixed with this
-   Commitlint & Husky added to Baza repository with automatical linting.
-   GIT commit messages format updated. Commit message should follow conventional format: XXXX-1234: feat(api): example commit message. Unfortunately, there was no clear way to make it as feat(web)/PROJ-101: update readme file.
-   Docker-Compose configuration `docker-compose.e2e.yml` removed. There are 2 Dorcker-Compose configuration for e2e available now: `docker-compose.e2e-local.yml` for local development and `docker-compose.e2e-ci.yml` for CI. It's recommended to update target projects. Read more why these changes are performed: https://github.com/scalio/baza/pull/208

# Possible breaking changes / bugs

-   Please check Status updates for Listings-like entities and report about any issues around it.

# JIRA

-   https://scalio.atlassian.net/browse/BAZA-407
-   https://scalio.atlassian.net/browse/BAZA-408
-   https://scalio.atlassian.net/browse/BAZA-409
-   https://scalio.atlassian.net/browse/BAZA-411
-   https://scalio.atlassian.net/browse/BAZA-384
-   https://scalio.atlassian.net/browse/BAZA-33
-   https://scalio.atlassian.net/browse/BAZA-416
-   https://scalio.atlassian.net/browse/BAZA-427
-   https://scalio.atlassian.net/browse/BAZA-428
-   https://scalio.atlassian.net/browse/BAZA-422
-   https://scalio.atlassian.net/browse/BAZA-414
-   https://scalio.atlassian.net/browse/BAZA-400
-   https://scalio.atlassian.net/browse/BAZA-429
-   https://scalio.atlassian.net/browse/BAZA-420
-   https://scalio.atlassian.net/browse/BAZA-418
-   https://scalio.atlassian.net/browse/BAZA-377
-   https://scalio.atlassian.net/browse/BAZA-421
-   https://scalio.atlassian.net/browse/BAZA-419
-   https://scalio.atlassian.net/browse/BAZA-424
-   https://scalio.atlassian.net/browse/BAZA-430
-   https://scalio.atlassian.net/browse/BAZA-407
-   https://scalio.atlassian.net/browse/BAZA-375
-   https://scalio.atlassian.net/browse/BAZA-375
-   https://scalio.atlassian.net/browse/BAZA-419

# PRs:

-   https://github.com/scalio/baza/pull/193
-   https://github.com/scalio/baza/pull/194
-   https://github.com/scalio/baza/pull/195
-   https://github.com/scalio/baza/pull/196
-   https://github.com/scalio/baza/pull/197
-   https://github.com/scalio/baza/pull/198
-   https://github.com/scalio/baza/pull/199
-   https://github.com/scalio/baza/pull/200
-   https://github.com/scalio/baza/pull/203
-   https://github.com/scalio/baza/pull/204
-   https://github.com/scalio/baza/pull/205
-   https://github.com/scalio/baza/pull/206
-   https://github.com/scalio/baza/pull/207
-   https://github.com/scalio/baza/pull/208
-   https://github.com/scalio/baza/pull/209
-   https://github.com/scalio/baza/pull/210
-   https://github.com/scalio/baza/pull/211
-   https://github.com/scalio/baza/pull/212
-   https://github.com/scalio/baza/pull/213
-   https://github.com/scalio/baza/pull/214
-   https://github.com/scalio/baza/pull/215
-   https://github.com/scalio/baza/pull/216
-   https://github.com/scalio/baza/pull/217
-   https://github.com/scalio/baza/pull/218
-   https://github.com/scalio/baza/pull/219
-   https://github.com/scalio/baza/pull/221
-   https://github.com/scalio/baza/pull/222
-   https://github.com/scalio/baza/pull/223
-   https://github.com/scalio/baza/pull/224
-   https://github.com/scalio/baza/pull/227
-   https://github.com/scalio/baza/pull/228
-   https://github.com/scalio/baza/pull/229
-   https://github.com/scalio/baza/pull/232

# Dependencies updated:

-   https://github.com/scalio/baza/pull/190
-   https://github.com/scalio/baza/pull/201
-   https://github.com/scalio/baza/pull/202
-   https://github.com/scalio/baza/pull/220
-   https://github.com/scalio/baza/pull/226
