# Release 1.14.0

The release is adding Dwolla Processing, adds additional customisation options, introduces Email Verification strategies and many more. The release is focused around Dividends V2 and mostly we finished this scope of work.

Next preview release will cover synchronization between NC / Dwolla and Bank Accounts which could be set with new API.

## What's new

-   `npm`: We're migrated from Nest.JS 7 to the latest Nest.JS 9. Please update your NPM packages
-   `docs`: Added Release Notes section to Baza Documentation portal (Release Notes are stored in Notion, GitHub PRs and Baza Repository)
-   `baza-core`: Added a new private method deleteBouncedEmailAddress to handle the deletion of bounced email address from the Maingun suppression list. Added a new enum MAILGUN_API_RESOURCES for Mailgun API resource names The operation will ignore if any errors received from Mailgun API
-   `baza-core`: Email Confirmation now has multiple strategies which could be configured per-project (check "Email Confirmation changes & configuration" section)
-   `baza-nc`: Few changes on the sync-offerings handler-related handling `updateExternalFundMove` status. this change was made due to the issues happening over test environment (the transaction status was not updating correctly)
-   `baza-nc-integration`: Fixed issues with counting Dividends in Transactions endpoint
-   `baza-nc`: Dividends V2 (Dwolla Dividends) are mostly finished and is available to use on client projects!
-   `baza-dwolla`: Error messages from Dwolla API are human-readable now (In most cases, report if you find any invalid cases)
-   `baza-dwolla`: Webhooks should work properly now. Also, added SHA256 verification for incoming (accepted) Dwolla Webhooks.
-   `baza-dwolla`: Added new Dwolla API services: `DwollaRootApiNestjsService` and `DwollaWebhookSandboxSimulationApiNestjsService`
-   `baza-dwolla`: Added new services for implementing domain specific items: `BazaNcDividendDwollaProcessingStrategy` and `DwollaWebhookSandboxSimulationApiNestjsService`
-   `baza-dwolla`: New service added to work with Master Account: `BazaDwollaMasterAccountService`. Added `/baza-dwolla/master-account/balance` endpoint which returns Master Account Balance (Admin-access only)
-   `baza-nc`: Added new services for implementing domain-specific items: `BazaNcDwollaCustomerService`, `BazaNcDwollaTransferService`. These services are using Redis to cache some items.
-   `baza-nc-integration`: Added Status filter for Portfolio Transactions endpoint
-   `baza-nc`: Added Email Notifications for Admin about Transfer Status updates on Dwolla Dividends. There will be notifications about Initiated, Cancelled, Failed and Completed Transfers.
-   `baza-nc`: Added Withdraw endpoint (`/baza-nc/bank-accounts/withdraw`) for Dividends. Check `Withdraw Dividends` section for details.

## What's new (IVP)

-   The release covers adding a "Whitespace Validator" for the personal information from within the verification flow. This validator would throw the "Required" error validation if the user starts typing only whitespaces but in case any actual characters are typed, the textbox becomes valid and an error message is removed.
-   Fixes including UI/UX, layout/style, and more fixes on the "Personal Information" page within Verification Flow. Overall, the following changes were made:
    -   On country change, zip code now resets regardless of which country is selected
    -   Layout fix was made for state dropdown in which for some specific states (e.g. states for country "Poland"), the dropdown was taking more than 100% width and breaking responsive design
    -   If "United States" is selected as the country, then the citizenship value would by default be "U.S. Citizen" else it would default to "Non-U.S. Resident" value
    -   State is now an optional value
-   Added custom configuration option for "Back" link in Purchase Flow and Account Verification flow
-   Added customization ability for Purchase Flow (see "Customization Ability Added For Purchase Flow" section)

## Bugfixes

-   `baza-dwolla`: Fixed an issue with `File` usage in packages which breaks NestJS application builds (there was development release as hotfix)
-   `baza-nc`: Added last failed transaction to portfolio assets list items. Changed updating transaction state after resubmit payment process is successful from `CREATED` to `PENDING` (since the `CREATED` status was the next stat after PENDING so `PENDING` was the initial status on session submit)
-   `baza-nc-cms`: Fixed small issues around i18n resources for different NC CMS sections

## Breaking Changes

-   NPM packages should be updated - please check "Nest.JS 7 -> 9 Migration" section
-   `BAZA_NORTH_CAPITAL_WEBHOOK_*` environment variables set is reworked. Check "Baza North Capital Webhook Changes" and update environment values for all of your deployment targets
-   `BAZA_DWOLLA_BASE_URL` environment value was removed and get back again in the release. Set `BAZA_DWOLLA_BASE_URL` as `https://api-sandbox.dwolla.com` for Non-Production environments and `https://api.dwolla.com` for Production environment.

## Regressions

The release may have some regressions. We will take a look at it for next `1.15.0` release, but we still should mention it in Release Notes.

-   Nest.JS 9 added a new `rawBody` feature for Requests (`@Req()`) which is required for Dwolla Webhook validation, but the feature is not compatible with setting custom limit for `body-parser` middleware. We remove this custom configuration and as result you may not be able to upload files more than `100kb` when using AWS Proxy strategy. The strategy is not used at this moment for most of the projects, but we will still need to fix it / find workarounds.
-   There could be issues with receiving webhooks on Local (Development) environments from NC or Dwolla. We're working currently on it.

## Nest.JS 7 -> 9 Migration

Dwolla Webhooks requires validation which could be performed only with Raw Body object. Raw Body feature is available only on Nest 8 or Nest 9 while we are using Nest 7. This release upgrades Nest to 9 and also fixes Dwolla Webhooks validation.

-   `nestjs-redis` package is removed - it's abandoned, and it's not working properly with new Nest anymore
-   New package `@liaoliaots/nestjs-redis` is used instead of `nestjs-redis`
-   With new upcoming Baza 1.14.0, target projects should update NPM packages:

| Package                  | Old Version | New Version  |
| ------------------------ | ----------- | ------------ |
| @nestjs/common           | ^7.0.0      | ^9.0.3       |
| @nestjs/core             | ^7.0.0      | ^9.0.3       |
| @nestjs/cqrs             | 7.0.1       | ^9.0.1       |
| @nestjs/jwt              | 7.2.0       | ^9.0.0       |
| @nestjs/platform-express | ^7.0.0      | ^9.0.3       |
| @nestjs/schematics       | ^7.0.0      | ^9.0.3       |
| @nestjs/swagger          | ^4.8.0      | ^6.0.4       |
| @nestjs/testing          | ^7.0.0      | ^9.0.3       |
| @nestjs/throttler        | ^2.0.0      | ^3.0.0       |
| @nestjs/typeorm          | 7.1.5       | ^9.0.0       |
| nestjs-redis             | ^1.3.3      | REMOVED      |
| ioredis                  | ADDED       | 5.0.6        |
| rxjs                     | ~6.5.5      | ^7           |
| @liaoliaots/nestjs-redis | ADDED       | 9.0.0-next.1 |

## Baza North Capital Webhook Changes

We refactored NC Webhooks in favour to how it works for Dwolla. Instead of "radio" instances, each environment could control now sending/reading events from Kafka.

-   `BAZA_NORTH_CAPITAL_WEBHOOK_EVERY_NODE` (bool) is **REMOVED**
-   `BAZA_NORTH_CAPITAL_WEBHOOK_DEBUG` (bool) is **ADDED**. The environment variables controls additional debug logs for NC Webhooks.
-   `BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND` (bool) is **ADDED**. When enabled Baza will re-send received (accepted) NC webhooks to Kafka.
-   `BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_READ` (bool) is **ADDED**. When enabled Baza will read NC Webhooks from Kafka and re-send it to Nest.JS CQRS layer (via `BazaEventBus` service).
-   You should enable `SEND` flag for your Test environments in order to receive dividends on Local (Development) environments.
-   Local (Development) and Stage environments should have `READ` flag enabled in order to receive NC Webhooks from Kafka bus.

## Email Confirmation changes & configuration

-   Baza now has configuration to set different strategies for email verification
-   Email Verification strategy could be set with `bazaApiBundleConfig.withConfirmationEmailExpiryResendStrategy` helper
-   Email Verification strategy could be also changed with `CONFIRMATION_EMAIL_EXPIRY_STRATEGY` environment value
-   Possible strategies: `AUTO` (default) and `MANUAL`.
-   `AUTO` strategy automatically resends email confirmation emails on failed / outdated links
-   `MANUAL` strategy could be used for email verifications flows with `[Re-send verification email]` button.

## Setting Up Bank Account for Withdraw and Receiving Dividends

We added endpoint to set Bank Account for Withdraw and Receiving Dividends. The work is still in progress; you could use it for FE development to preview upcoming features.

**Note:** This release does not cover Sync-up with North Capital on selecting the bank account.

-   Added a new field `bankAccounts` to the `BazaNcInvestorAccountEntity` class
-   Created a `bank-accounts` sub-package within the `baza-nc-**` packages
-   Created a new endpoint `/baza-nc/bank-accounts/update` to facilitate the option for setting the bank accounts on purchasing shares, withdrawing dividends
-   Required input should be passed as a request JSON body as below for the above endpoint,

```
	{
		"bankAccountType":"WithdrawDividends",
		"bankAccountDetails":{
			"accountName":"KK-Test1",
			"accountNickName":"KK-Test",
			"accountRoutingNumber":"053208066",
			"accountNumber":"123456565",
			"accountType":"Savings"
		}
	}
```

-   `baza-nc-bank-accounts.data-access.ts` file has been added to access the above endpoint for the Frontend app
-   Added `bankAccounts` field to the `BazaNcInvestorAccountDto` class, and the response would look like below when querying `/baza-nc/investor-account/current` endpoint

```
{
    "id": 10,
    "userId": 25,
    "userFullName": "srinivasan14 kk",
    "userEmail": "srinivasan+14@scal.io",
    "northCapitalAccountId": "A1373715",
    "northCapitalPartyId": "P1353115",
    "isAccountVerificationCompleted": true,
    "isInvestorVerified": false,
    "isBankAccountLinked": true,
    "isCreditCardLinked": true,
    "isPaymentMethodLinked": true,
    "ssnDocumentFileName": null,
    "ssnDocumentId": null,
    "isDwollaAvailable": true,
    "dwollaCustomerId": "a011f2e6-003f-432a-8815-69a1ef460bf8",
    "bankAccounts": [
        {
            "bankAccountType": "Purchase",
            "bankAccountDetails": {
                "accountName": "KK-Test2",
                "accountNickName": "KK-Test",
                "accountRoutingNumber": "053208067",
                "accountNumber": "123456566",
                "accountType": "Savings"
            }
        },
        {
            "bankAccountType": "WithdrawDividends",
            "bankAccountDetails": {
                "accountName": "KK-Test1-Update",
                "accountNickName": "KK-Test",
                "accountRoutingNumber": "053208076",
                "accountNumber": "123456565",
                "accountType": "Savings"
            }
        }
    ]
}
```

## Withdraw Dividends

-   Added a new endpoint POST - `/baza-nc/bank-accounts/withdraw`
-   Added a service method `withdraw` to handle withdrawal available Dwolla Balance to an external bank account of a current investor account
-   Added a `BAZA_DWOLLA_BASE_URL` env variable to all env files, required for creating source and destination URLs to initiate a transfer request
-   Added a new gateway method `createFundingSourceWithPlaidToken` which accepts `plaidToken`, and `name`
-   Added new `BazaNcExternalBankAccountForWithdrawalNotSetException`, `BazaNcExternalBankAccountNotFoundException`, and `BazaNcDwollaFundingSourcesNotExistsException` exception classes
-   Added `dwollaFundingSourceId` to `bankAccounts` field for `InvestorAccountEntity`

## Customization Ability Added For Purchase Flow

This release is adding custom configuration ability for Purchase Flow - Last Step (Done). As part of PR, the following changes were made:

-   Custom configuration interface was created in the purchase flow library
-   `PurchaseDone` component was replaced with `PurchaseCustomDone` component and subsequent changes were made in the routing module
-   This allows passing custom configuration (optional) to the PurchaseCustomDone component e.g. as follows:

**Sample HTML Code**

`<app-purchase-done [customDoneConfig]="customDoneConfig"></app-purchase-done>`

**Sample Component Code**

```
export class PurchaseCustomDoneComponent {

    customDoneConfig: CustomDoneConfig;

    constructor() {
        this.customDoneConfig = {
            buttonText: 'Custom Text',
            buttonLink: '/items'
        };
    }
}
```

## Customization Ability Added For Backlink in Account Verification and Purchase Flow

This release covers adding the ability for customizing the backlink in the purchase & verification flow with help of custom configurations. As part of this PR, the following changes were made:

-   New interfaces were created in purchase & verification libraries
-   Backlink configuration can be passed (optional) as part of PurchaseCustom and VerificationCustom components
-   The interfaces are flexible to support any number of parameters

**Sample Code - Verification Custom Component**

```
verificationConfig: VerificationConfig;

    constructor(private readonly route: ActivatedRoute) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });

        this.verificationConfig = {
            backLinkConfig: {
                  text: 'Custom back link',
                  link: ['/portfolio']
            }
          };
    }
```

**Sample Code - Purchase Custom Component**

```
purchaseConfig: PurchaseConfig;

    constructor(private readonly route: ActivatedRoute) {
        this.route.data.pipe(untilDestroyed(this)).subscribe((params: Params) => {
            this.currentTab = Number(params.tab) || 0;
        });

        this.purchaseConfig = {
          backLinkConfig: {
                text: 'Custom back link',
                link: ['/portfolio']
          }
        };
    }
```

**Task Link:**
https://scalio.atlassian.net/browse/BAZA-1031

## PRs

-   https://github.com/scalio/baza/pull/712
-   https://github.com/scalio/baza/pull/714
-   https://github.com/scalio/baza/pull/716
-   https://github.com/scalio/baza/pull/717
-   https://github.com/scalio/baza/pull/720
-   https://github.com/scalio/baza/pull/741
-   https://github.com/scalio/baza/pull/723
-   https://github.com/scalio/baza/pull/736
-   https://github.com/scalio/baza/pull/725
-   https://github.com/scalio/baza/pull/728
-   https://github.com/scalio/baza/pull/730
-   https://github.com/scalio/baza/pull/733
-   https://github.com/scalio/baza/pull/734
-   https://github.com/scalio/baza/pull/735
-   https://github.com/scalio/baza/pull/737
-   https://github.com/scalio/baza/pull/739

## PRs (IVP)

-   https://github.com/scalio/baza/pull/715
-   https://github.com/scalio/baza/pull/718
-   https://github.com/scalio/baza/pull/719
-   https://github.com/scalio/baza/pull/721
-   https://github.com/scalio/baza/pull/724
-   https://github.com/scalio/baza/pull/727
-   https://github.com/scalio/baza/pull/731

## JIRA

-   [BAZA-1014](https://scalio.atlassian.net/browse/BAZA-1014) Baza-dwolla packages are broken
-   [BAZA-1013](https://scalio.atlassian.net/browse/BAZA-1013) Transaction API isn't counting dividends in the total records causing issues in Pagination
-   [BAZA-1044](https://scalio.atlassian.net/browse/BAZA-1044) Dividends paid through Dwolla account stay in Pending status even after process them through admin's confirmation email
-   [BAZA-918](https://scalio.atlassian.net/browse/BAZA-918) Ability to import a Bulk Upload CSV
-   [BAZA-1043](https://scalio.atlassian.net/browse/BAZA-1043) Labels on Listings > Subscriptions page are not loading
-   [BAZA-928](https://scalio.atlassian.net/browse/BAZA-928) Ability to cash out from my Dwolla account to an external bank
-   [BAZA-929](https://scalio.atlassian.net/browse/BAZA-929) (QA Only) Able to See dividends received to my account on the Portfolio Menu
-   [BAZA-920](https://scalio.atlassian.net/browse/BAZA-920) Keep track of payment status for each CSV record
-   [BAZA-922](https://scalio.atlassian.net/browse/BAZA-922) Ability to export dividends CRUD based on file date range
-   [BAZA-926](https://scalio.atlassian.net/browse/BAZA-926) Create and update Dwolla account for investors in a transparent way
-   [BAZA-927](https://scalio.atlassian.net/browse/BAZA-927) Able to check my Dwolla balance account
-   [BAZA-964](https://scalio.atlassian.net/browse/BAZA-964) Move to Bitnami Kafka packages
-   [BAZA-965](https://scalio.atlassian.net/browse/BAZA-965) Retry requests to North Capital
-   [BAZA-982](https://scalio.atlassian.net/browse/BAZA-982) Add Credit Card e2e test coverage to reprocess payment operation
-   [BAZA-1005](https://scalio.atlassian.net/browse/BAZA-1005) Optimize session destroy capabilities
-   [BAZA-1008](https://scalio.atlassian.net/browse/BAZA-1008) Update Dwolla api url of gateway services when query string is invovled
-   [BAZA-1009](https://scalio.atlassian.net/browse/BAZA-1009) Update/implement optimized findAll method to use on cms->listing->setSortOrder api
-   [BAZA-1034](https://scalio.atlassian.net/browse/BAZA-1034) Service is missing for the endpoint BazaNcDwollaDataAccess
-   [BAZA-836](https://scalio.atlassian.net/browse/BAZA-836) Decimal Transaction Fee causes 500 on starting /session

## JIRA (Development Preview - still on QA)

These tasks are included to the release, but it's still on QA. We may publish some more bugfixes later.

-   [BAZA-919](https://scalio.atlassian.net/browse/BAZA-919) Review the CSV and approve the payment of dividends
-   [BAZA-921](https://scalio.atlassian.net/browse/BAZA-921) Reprocess transactions in error so that investors will receive their dividends
-   [BAZA-928](https://scalio.atlassian.net/browse/BAZA-930) Ability for investors to reprocess failed payments
-   [BAZA-928](https://scalio.atlassian.net/browse/BAZA-1023) Quality Assurance - Check ability to create a Dwolla Account during Dividend Publish for Dwolla
-   [BAZA-1026](https://scalio.atlassian.net/browse/BAZA-1026) Refactor NC Webhooks to fix transaction fund move update issue
-   [BAZA-1011](https://scalio.atlassian.net/browse/BAZA-1011) Last failed payment on Assets API Content
-   [BAZA-646](https://scalio.atlassian.net/browse/BAZA-646) Improve email deliveries via Mailgun
-   [BAZA-883](https://scalio.atlassian.net/browse/BAZA-883) Backend - Re-submit Payment Process
-   [BAZA-873](https://scalio.atlassian.net/browse/BAZA-873) Re-send email for token expiration
-   [BAZA-984](https://scalio.atlassian.net/browse/BAZA-984) Ability to set Bank Account as Bank Account for purchasing shares, receiving or withdrawing dividends
-   [BAZA-923](https://scalio.atlassian.net/browse/BAZA-923) Email notifications on bases of triggered status

## JIRA (IVP)

-   [BAZA-1018](https://scalio.atlassian.net/browse/BAZA-1018) Personal information fields are being updated with only Space character
-   [BAZA-741](https://scalio.atlassian.net/browse/BAZA-741) Investor SHOULD NOT be able to set and save value for "State" field from OTHER/DIFFERENT country
-   [BAZA-1031](https://scalio.atlassian.net/browse/BAZA-1031) Customize Back to Listing component
-   [COL-291](https://scalio.atlassian.net/browse/COL-291) '404 error' after clicking 'Back to Listing Details' link on Verification and Purchase flow
-   [BAZA-1032](https://scalio.atlassian.net/browse/BAZA-1032) Customize the last step of Purchase flow
-   [BAZA-1042](https://scalio.atlassian.net/browse/BAZA-1042) Cleanup PurgeCSS plugin code
