# Release 1.28.1

Bugfixes for Baza 1.28.0, mostly for Plaid integration

## Bugfixes

-   `baza-nc-api`: updated the `linkOnSuccess` field to remove the Dwolla Funding Source created for previous Plaid Account after the new Plaid Funding Source was created. There is also another change in `linkOnSuccess` method which is about replacing the accountHolderName value with `institutionName` (if exists) for the field account name in our database (to address the issue mentioned in the ticket)
-   `baza-nc-api`: there been an issue reported by QA about the previous behavior of the 1580 code. It seemed there was a possibility to have 2 bank accounts (cash-in, cash-out) that pointed at the same funding source, in that case, previously implemented logic would remove the funding source without checking if its also being used by any other bank account, so the bank account would exist but the funding source would be removed. this change will check if there is another bank account using this funding source; if so, it will not remove the funding source.
-   `baza-dwolla-web-purchase-flow`: fixes the `/linkOnSuccess` endpoint call when linking Plaid bank account as Payout Method in Account Tab (Dwolla)
-   `baza-nc-integration-api`: updated the response payload of listing details on the schema tabs section to include field type in the array and also updated the fields of type key-value to show values as array of objects not an object of key values

## JIRA

-   [BAZA-1580](https://scalio.atlassian.net/browse/BAZA-1580) Plaid: On Edit linked bank account, replace existing one. Include the Bank name, Account type and Account name in the response on linking Plaid account
-   [BAZA-1732](https://scalio.atlassian.net/browse/BAZA-1732) Plaid: No new payment source is added on dwolla side during an update of payment method in case Payment/payout methods are linked to same Funding source
-   [BAZA-1734](https://scalio.atlassian.net/browse/BAZA-1734) Plaid: Payout information is not changed on UI during the update of Payout method in case Payment and payout methods are different
-   [BAZA-1712](https://scalio.atlassian.net/browse/BAZA-1712) Offering Details schema API fix for CW
-   [BAZA-1735](https://scalio.atlassian.net/browse/BAZA-1735) Plaid: Incorrect name of bank is shown during the payout method update

## PRs

-   https://github.com/scalio/baza/pull/1191
-   https://github.com/scalio/baza/pull/1213
-   https://github.com/scalio/baza/pull/1210
