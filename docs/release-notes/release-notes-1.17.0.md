# Release 1.17.0

The release is containing only small bugfixes around purchase flow and content types. We're planning to put new upcoming features into next 1.18.0 release.

## Bugfixes

-   `baza-core`: Fixed i18n typo ("Description" field was named as "Images")
-   `baza-content-types`: Fixed notification message when deleting Tag
-   `baza-nc-integration`: Fixed issues around sort order with Listings
-   `baza-nc-web-ui-components`: Added a thousand separator for all fields in summary section of purchase flow.
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-purchase-flow`: fixed issues with displaying payment method fee on twice clicking to back button
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-purchase-flow`: fixed issues around disabling submit button
-   `baza-dwolla-web-purchase-flow`, `baza-nc-web-purchase-flow`: fixed the bug in personal info section (purchase flow) where if uploaded document name string is too long, there is horizontal scrolling on the page. This is now fixed and the document name appears in multiple lines without any horizontal scroll.

## Breaking changes

-   **(POSSIBLE)** Sort order of Listings will be different. You may need update `sortOrder` for Listings

## JIRA

-   BAZA-1171 Shares amount should have thousand separator in Summary
-   BAZA-1165 CMS: Delete Tag: Notification message that appears in case of deleting Tag is not correct
-   BAZA-1134 CMS - Incorrect order of Listings
-   BAZA-1153 Submit Payment: Payment Method fee section (Credit Card method) starts appearing on Purchase Details when we navigate back from Submit Payment to Payment Details
-   BAZA-1192 Remove disabled property from submit buttons on verification flow (New Validation Behavior)
-   BAZA-496 CMS - need to rename a field in FAQ add new Q/A
-   BAZA-1202 Purchase Screen: UI in mobile resolution 375 px needs to be fixed

## PRs

-   https://github.com/scalio/baza/pull/818
-   https://github.com/scalio/baza/pull/826
-   https://github.com/scalio/baza/pull/820
-   https://github.com/scalio/baza/pull/822
-   https://github.com/scalio/baza/pull/824
-   https://github.com/scalio/baza/pull/825
-   https://github.com/scalio/baza/pull/832
