# Release 1.27.0

This release introduces more marketing components and also changes that mainly focus on fixing bugs and making improvements . there are some features like showing warning banner on CMS when the master password is enabled , adding offering title for investments in operations and disabling "Send Bulk Report" for daily report if no reports is available .

## What's new

-   `baza-core-cms`: Added banner message to display app level banner message. Modified master password logic at the backend such that once enabled, it will be disabled in one hour automatically.
-   `sandbox-web`: added documentation for baza Angular directives on the baza docs portal, as well as TSDocs for all the current directives.
    changelog: - created a new "Web Development" section on the Baza docs portal. - created documentation for the directive appAmountMask - created documentation for the directive appElementAppear - created documentation for the directive appAutocompleteOff - created documentation for the directive appDateMask - created documentation for the directive appPasswordInput - created documentation for the directive appSortElements

-   `baza-nc-api`: added offeringTitle for investments in operations
-   `baza-nc-cms`: added info message and disable send many button if no records available
-   `baza-core-api`: Added Mail Interceptor feature for Mail services
-   `baza-core-api`: Added an ability to enable debug logs with MAIL_DEBUG: 1 flag. This flag will enables debug logs for each Send Mail request.
-   `docs`: Added overview/Mail documentation for Mail Services
-   `baza-dwolla-web-purchase-flow`: `DwollaSharedService` method `processDwollaPlaidFlow` was updated with the following:
    -   Added missing `onExit` and `onEvent` callback handlers because they can be useful for consumers.
    -   Created param config object for readability purposes.
    -   Dispatched actions in the `onExit` callback handler to trigger funnel in the parent component and reflect the latest data after the plaid flow process is canceled.
    -   Added correct types to `processDwollaPlaidFlow`
-   `baza-core-cms`: New form builder component `BazaFormBuilderDateRangeComponent` was created.
    Associated `BazaFormBuilderDateRangeComponent` to the type `BazaFormBuilderControlType.DateRange` when registering a new component in the form builder.
    Added possibility to configure the behavior of `BazaFormBuilderDateRangeComponent`.
-   `sandbox-web`: Added remaining 6 marketing components along with UI fixes as part of this release

## Bugfixes

-   `baza-nc-cms`: removed retry option for suspended dwolla users.
-   `baza-nc-cms`: fixed date object client error and 400 server error by formatting the `dateOfBirth` as `MM-DD-YYYY` before sending it to the server since that's what the type NorthCapitalDate expects. added new utility function `shiftMonth` to handle months correctly.
-   `baza-core-api`: fix for the credit card related integration tests , which was caused by an issue related to getMonth method (method returns months in the range of 0-11 and we the code was assuming it returning 1-12)
-   `baza-nc-api`: update all controllers and services in Baza nc api to use investor account by default by using the `InvestorAccountOptionalGuard` decorator. also updated the behaviour of `InvestorAccountGuard` (user account required but investor is optional) and `InvestorAccountOptionalGuard` (both account and investor are required) guards.
-   `baza-nc-api`: api request payload type fix for withdrawal reprocess request (caused by recent changes on whitelist feature)
-   `baza-nc-api`: include nc transaction state in payload objects of for withdraw and transfer in operaions list endpoint.
-   `baza-nc-api`: updated operation list endpoint to return number for index and size values .
-   `baza-core-cms`: make the invalid referral error (`bazaIsReferralCodeValid`) message more clear .
-   `sandbox-web`: Payments & Payout - fix tooltip is not showing the message next to payments and payouts methods
-   `sandbox-web`: Payments & Payout - fix Current Balance doesn't show up after touch endpont call without page reload

---

## Marketing Components

### **Why Invest Section**

Following changes were made:

-   A new "Why Invest" marketing component is now available with customization ability
-   Default styles are available in \_invest.less and \_invest-card.less files with custom classes added for each element for easy style overriding
-   Why invest section component is responsive on all screens and styling is done with mobile-first design approach
-   In order to separate out styles specifically for cards, a new component WhyInvestCardComponent was created and it's relevant styles file is \_invest-card.less
-   The marketing components are part of baza-web-ui-components library along with relevant interfaces
-   All models are covered with TSDocs for developer's reference
-   Documentation added on baza-docs portal with complete guidance on how to use and customize the component

### Screenshots:

**Mobile:**
https://www.screencast.com/t/mRmp85J5sgIT

**Tablet:**
https://www.screencast.com/t/L4THLfU4v

**Desktop:**
https://www.screencast.com/t/JQitmWbg3

### Documentation URL:

Here's a complete guide through which we can use & customize the why invest component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/06-why-invest

---

### **How It Works Section**

Baza now has support for new customizable marketing how it works component that can be used on target projects.

As part of this PR, following changes were made:

-   A new how it works component is now available with customization ability
-   Default styles are available in `_hiw.less` and `_hiw-card.less` files with custom classes added for each element for easy style overriding
-   How it works section component is responsive on all screens and styling is done with mobile-first design approach
-   In order to separate out styles specifically for cards, a new component `HowItWorksCardComponent` was created and it's relevant styles file is `_hiw-card.less`
-   The marketing components are part of `baza-web-ui-components` library along with relevant interfaces
-   All models are covered with TSDocs for developer's reference
-   Documentation added on `baza-docs` portal with complete guidance on how to use and customize the component

### Screenshots:

**Mobile:**
https://www.screencast.com/t/faz9eFTCk

**Tablet:**
https://www.screencast.com/t/xCvnUW72ox

**Desktop:**
https://www.screencast.com/t/pvtxUTum

### Documentation URL:

Here's a complete guide through which we can use & customize the how it works component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/06-how-it-works

---

### **Marketing Banner Section**

Baza now has support for new customizable marketing banner component that can be used on target projects.

As part of this PR, following changes were made:

-   A new marketing banner component is now available with customization ability
-   Default styles are available in `_banner.less` file with custom classes added for each element for easy style overriding
-   Banner section component is responsive on all screens and styling is done with mobile-first design approach
-   The marketing components are part of `baza-web-ui-components` library
-   Documentation added on `baza-docs` portal with complete guidance on how to use and customize text section component

### Screenshots:

**Mobile:**
https://www.screencast.com/t/NCL7pxQWCCWo

**Tablet:**
https://www.screencast.com/t/7NDZJ4e5t

**Desktop:**
https://www.screencast.com/t/C6TIM4Vy

### Documentation URL:

Here's a complete guide through which we can use & customize the banner component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/05-banner

---

### **Marketing Text Section**

Baza now has support for new customizable marketing text only component that can be used on target projects.

As part of this PR, following changes were made:

-   A new text only component is now available with customization ability
-   Default styles are available in `_text-section.less` file with custom classes added for each element for easy style overriding
-   Text section component is responsive on all screens and styling is done with mobile-first design approach
-   The marketing components are part of `baza-web-ui-components` library
-   Documentation added on `baza-docs` portal with complete guidance on how to use and customize text section component

### Screenshots:

**Mobile:**
https://www.screencast.com/t/wZwkWhrO3

**Tablet:**
https://www.screencast.com/t/IRwme5o8nf

**Desktop:**
https://www.screencast.com/t/2sEpcNzs

### Documentation URL:

Here's a complete guide through which we can use & customize the text section component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/04-text-section

---

### **Marketting Text Image Section**

Baza now has support for new customizable marketing Text-Image component that can be used on target projects.

As part of this PR, following changes were made:

-   A new Text-Image component is now available with customization ability
-   Default styles are available in `_text-img.less` file with custom classes added for each element for easy style overriding
-   Text-Image component is responsive on all screens and styling is done with mobile-first design approach
-   The marketing components are part of `baza-web-ui-components` library
-   This component can be used to display text on left or right direction along with an image section
-   Configuration interfaces added and covered with TSDocs
-   Documentation added on `baza-docs` portal with complete guidance on how to use and customize text-image component

### Screenshots:

**Mobile:**
https://www.screencast.com/t/NIidxfkQ8A7

**Tablet:**
https://www.screencast.com/t/TxVcjHxp

**Desktop:**
https://www.screencast.com/t/q0b6ROvznqJ

### Documentation URL:

Here's a complete guide through which we can use & customize the Text-Image component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/02-text-img

---

### **Marketing CTA Section**

Baza now has support for new customizable marketing CTA component that can be used on target projects.

As part of this PR, following changes were made:

-   A new CTA component is now available with customization ability
-   Default styles are available in `_cta.less` file with custom classes added for each element for easy style overriding
-   CTA component is responsive on all screens and styling is done with mobile-first design approach
-   The marketing components are part of `baza-web-ui-components` library
-   Documentation added on `baza-docs` portal with complete guidance on how to use and customize bullets component

### Screenshots:

**Mobile:**
https://www.screencast.com/t/HtoRj4ocuhhG

**Tablet:**
https://www.screencast.com/t/kEJorXCbTBN

**Desktop:**
https://www.screencast.com/t/NmUODjX7m

### Documentation URL:

Here's a complete guide through which we can use & customize the CTA component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/08-cta

---

#### **Adjust Styles and Update Docs for Marketing Components**

following changes were made:

-   CSS selector names were simplified and updated
-   Background color added and number of dummy cards increased from 4 to 6 (as per design) on "Why Invest" section
-   Margin/padding issues in multiple components were fixed as per Zeplin designs
-   Relevant docs were updated

#### **Interfaces for Marketing Hero Banner Component**

-   A new interface `BulletsConfig` was added for configuration of bullets component and covered with TSDocs
    A generic type `RangedArray` was also added to utils folder that allows to create a dynamic array between a certain elements count range
    Baza documentation was updated with guide on how to use and customize the bullets component
-   A new interface `HeroConfig` was added for configuration of hero banner component and covered with TSDocs
    Baza documentation was updated with guide on how to use and customize the hero banner component

**Documentation URL:**
Here's a complete guide through which we can use & customize the hero banner component:
https://baza-docs.test.scaliolabs.com/web-development/marketing-components/01-hero-banner

#### **Interfaces for Marketing Hero Banner Component**

-   A new interface HeroConfig was added for configuration of hero banner component and covered with TSDocs
-   Baza documentation was updated with guide on how to use and customize the hero banner component

---

#### **Fix Minor UI Bugs In Marketing Components**

fixe for some minor UI bugs related to marketing components.

### Screenshots:

https://www.screencast.com/t/HNQSBvcmQ

https://www.screencast.com/t/i2QbgtqFUj

https://www.screencast.com/t/8ocLZpWxyeTO

---

##### **Update Dividend Alert Logic**

-   Call to verification endpoint was removed and code was cleaned-up in `DividendAlertState`
-   A null check was added for credit-card.component.html condition in HTML code
-   The yellow icon was removed from "Payment & Payouts Tab" (Dwolla) since this tab is out of context

## Breaking Changes:

Some of the CSS styles/class names were updated. Please take note of the following:

1. Refer to following updated documentation for "Text Image Section"
   https://baza-docs.test.scaliolabs.com/web-development/marketing-components/02-text-img

2. Refer to following updated documentation for "Text Only Section"
   https://baza-docs.test.scaliolabs.com/web-development/marketing-components/04-text-section

---

### Screenshots:

#### Text Image Section:

**Before:** https://www.screencast.com/t/PiMz1zCs
**After:** https://www.screencast.com/t/69T8cDVQg5

#### Text Only Section:

**Before:** https://www.screencast.com/t/DxJbzynY
**After:** https://www.screencast.com/t/fe262aYXFXvj

## JIRA

-   [BAZA-1585](https://scalio.atlassian.net/browse/BAZA-1585) controllers and services in baza nc api package should work with investor accounts by default
-   [BAZA-1590](https://scalio.atlassian.net/browse/BAZA-1590) Why Invest Section added
-   [BAZA-1079](https://scalio.atlassian.net/browse/BAZA-1079) fix dwolla withdrawal reprocess api request payload type
-   [BAZA-1615](https://scalio.atlassian.net/browse/BAZA-1615) Add "offering name" to the new endpoint for Purchase and Dividends transactions
-   [BAZA-1600](https://scalio.atlassian.net/browse/BAZA-1600) NC Transactions - State (status) is not available in the payload object for Withdraw and Transfer
-   [BAZA-1618](https://scalio.atlassian.net/browse/BAZA-1618) Get the list of the NC APIs used by Baza
-   [BAZA-1618](https://scalio.atlassian.net/browse/BAZA-1653) Endpoint /baza-nc/operation/list should return numbers
-   [BAZA-1553](https://scalio.atlassian.net/browse/BAZA-1553) Daily Reports - Error message not shown when selected date range has no records ready to report for Send Bulk Report
-   [BAZA-1554](https://scalio.atlassian.net/browse/BAZA-1554) Daily Reports - Bulk Send Reports button is not disabled when there are not records ready to report
-   [BAZA-1526](https://scalio.atlassian.net/browse/BAZA-1526) BE: SSN value is not updated for NC 'Field 1' after user edits their info
-   [BAZA-1644](https://scalio.atlassian.net/browse/BAZA-1644) Add Interceptor feature for Baza Mail BE

-   [BAZA-1668](https://scalio.atlassian.net/browse/BAZA-1668) Payments & Payout - tooltip is not showing the message next to payments and payouts methods
-   [BAZA-1671](https://scalio.atlassian.net/browse/BAZA-1671) Payments & Payout - Current Balance doesn't show up after touch endpont call without page reload
-   [BAZA-1650](https://scalio.atlassian.net/browse/BAZA-1650) CMS- Date range appears with one date choosing option
-   [BAZA-1579](https://scalio.atlassian.net/browse/BAZA-1579) CMS - Referral Codes: Error message displayed on referral code is not correct
-   [BAZA-1654](https://scalio.atlassian.net/browse/BAZA-1654) Update NC dividend alert component logic
-   [BAZA-1628](https://scalio.atlassian.net/browse/BAZA-1628) Add Interfaces and TSDocs for Marketing Bullets Component
-   [BAZA-1627](https://scalio.atlassian.net/browse/BAZA-1627) Add Interfaces and TSDocs for Marketing Hero Banner Component
-   [BAZA-1591](https://scalio.atlassian.net/browse/BAZA-1591) Create How It Works Section
-   [BAZA-1589](https://scalio.atlassian.net/browse/BAZA-1589) Create banner section
-   [BAZA-1588](https://scalio.atlassian.net/browse/BAZA-1588) Create text only section
-   [BAZA-1586](https://scalio.atlassian.net/browse/BAZA-1586) Create text / Image section
-   [BAZA-1592](https://scalio.atlassian.net/browse/BAZA-1592) Create Investor CTA Section
-   [BAZA-1673](https://scalio.atlassian.net/browse/BAZA-1673) UI: Home page: UI issues

## PRs

-   https://github.com/scalio/baza/pull/1139
-   https://github.com/scalio/baza/pull/1140
-   https://github.com/scalio/baza/pull/1143
-   https://github.com/scalio/baza/pull/1147
-   https://github.com/scalio/baza/pull/1148
-   https://github.com/scalio/baza/pull/1150
-   https://github.com/scalio/baza/pull/1151
-   https://github.com/scalio/baza/pull/1152
-   https://github.com/scalio/baza/pull/1153
-   https://github.com/scalio/baza/pull/1155
-   https://github.com/scalio/baza/pull/1156
-   https://github.com/scalio/baza/pull/1160
-   https://github.com/scalio/baza/pull/1159
-   https://github.com/scalio/baza/pull/1149
-   https://github.com/scalio/baza/pull/1146
-   https://github.com/scalio/baza/pull/1157
-   https://github.com/scalio/baza/pull/1123
-   https://github.com/scalio/baza/pull/1121
-   https://github.com/scalio/baza/pull/1114
-   https://github.com/scalio/baza/pull/1113
-   https://github.com/scalio/baza/pull/1111
-   https://github.com/scalio/baza/pull/1109
-   https://github.com/scalio/baza/pull/1104
-   https://github.com/scalio/baza/pull/1164
