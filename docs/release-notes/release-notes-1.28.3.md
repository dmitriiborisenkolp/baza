# Release 1.28.3

This Release implements a new requirement that Dwolla VF can be triggered from outside of Dwolla PF and once exited from back link or after second step submission, user is redirected to the originating request page.

## What's New:

-   The Dwolla VF can now be triggered outside of Dwolla PF from anywhere within the application. In this case specifically, we will have the originating page url appearing in address bar as `redirectUrl` value.

-   An optional override configuration for back link text and popup confirmation text can be passed via routing state

-   The Dwolla VF would redirect user back to the page he came from, if back link is exited or if the VF is fully submitted

-   The default text in "Listing Details" page (Dwolla PF) is changed from "Pick number of shares..." to "Pick the number of shares..."

-   Since i18n translations are not available in scope of 1.28.2 version, local storage is used to manage the router state override values for back link configuration

## Migration:

-   In order to trigger Dwolla VF with redirect back to the same page, create a button / link on the page as per the following sample:

**HTML**

```html
<button
    nz-button
    nzType="primary"
    (click)="redirectToVF()">
    Verify Your Investor Account
</button>
```

**Typescript**

```typescript
// Step 1: include Angular router in constructor
constructor(private readonly store: Store, private readonly router: Router) {}

// Step 2: Define the button click event handler. Note that `verification-dwolla` URL is the one defined at sandbox level which can be changed e.g. to `verification` or anything else which is loading Dwolla VF library
redirectToVF() {
         this.router.navigate(
            [
                // this link points to the route defined at application level which loads Dwolla VF library
                '/verification-dwolla',
            ],
            {
                queryParams: {
                    redirectUrl: this.router.url,
                },
                // override information passed through router state (optional) for back link text and popup confirmation text
                state: {
                    // overrides the back link text
                    text: 'Go Back',
                    // overrides the back link popup confirmation text
                    leaveConfirmationText: 'All completed steps will be saved. You can continue verification again later.'
                },
            });
    }

```

## Notes:

-   Normally when Dwolla VF is triggered, it's via the Dwolla PF route so therefore we have cart information in our store. That way, we generate the "Back to Listing Details" backlink and use the item Id. But in case we land on Dwolla VF outside of PF scope, we would not have any cart information in store.

-   Target projects can place a button/link anywhere on their end as long as it's triggering Dwolla VF in the way defined above

-   In case target projects want to further customize the purchase details page description text "Pick the number of shares...", the property `purchaseDetailsDescription` can be used and documentation guide is linked below:
    https://baza-docs.test.scaliolabs.com/web-development/customizations/purchase-flow/02-details-page-config

-   The default values for back link text and popup confirmation text are as follows:

| Case                                             | Backlink Text (Default Value) | Backlink Popup Text (Default Value)                                                           |
| ------------------------------------------------ | ----------------------------- | --------------------------------------------------------------------------------------------- |
| Dwolla VF is triggered from Dwolla PF            | `Back to Listing Details`     | `All completed steps will be saved. You can continue verification before your next Purchase.` |
| Dwolla VF is triggered from outside of Dwolla PF | `Go Back`                     | `All completed steps will be saved. You can continue verification again later.`               |

## JIRA

-   [BAZA-1851](https://scalio.atlassian.net/browse/BAZA-1851) CW: Verification Flow (Dwolla) Add ability to define a route to a certain page from inside the flow
-   [BAZA-1848](https://scalio.atlassian.net/browse/BAZA-1848) CW: HotFix: Tiny change in the text on the first step of Purchase Flow\*\*\*\*
