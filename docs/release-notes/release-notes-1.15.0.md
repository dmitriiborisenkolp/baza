# Release 1.15.0

The release is covering mostly requested bugfixes and features from different projects. We're working currently on new Bank Account system & Dividends V3.

## What's new

-   `baza-core`: Maintenance API received small rework. Now it works correctly & applies changes in Applications configuration immediately (fixed issues with caching)
-   `baza-core`: Managed Users received rework & Managed Users now covered with integration tests. Read more in `Managed Users Changes` section
-   `baza-core`: Added `setEnv` and `resetEnv` helpers for E2E integration tests
-   `baza-core-ng`: New `WhiteSpaceInterceptor` global interceptor added & enabled by default. Before we send any API request, we can use this interceptor to automatically remove any whitespaces (trim values) from strings present in request/payload being sent to API
-   `baza-nc-api`: Webhooks are secured now with AES-256-CBC symmetric encryption
-   `baza-nc-api`: Fixed issues with webhooks & status tracking for Dwolla dividends
-   `baza-nc-api`: Withdraw (from Dwolla) feature received changes. Read more in `Partial Withdraw Changes`
-   `baza-nc-cms`: Added i18n resources for CSV filenames in Dividends CMS:
    -   `baza-nc.dividend.components.bazaNcDividend.items.exportToCsv.fileName`
    -   `baza-nc.dividend.components.bazaNcDividendTransaction.items.exportToCsv.fileName`
    -   `baza-nc.dividend.components.bazaNcDividendTransactionEntries.actions.exportToCsv.fileName`
-   `baza-nc-web-purchase-flow`: fixed issues with "Session is locked" error on leaving Purchase Flow
-   `baza-nc-web-purchase-flow`: Added ability to customize Details and Payment Pages in Purchase Flow. Read more in `Details and Payment Page Customization` section
-   `baza-nc-web-purchase-flow`: Added animated placeholders for Purchase Flow

## Bugfixes

-   Fixed multiple issues with Filters API
    -   Filters API now works in different schema / missing schema data
    -   Filters API now can sort by `sortOrder` field
    -   Filters API works with `$in` + Select field correctly
-   (IVP) Fixed icons for transactions types for Safari browser on Portfolio

## Documentation

-   Added Overview / Maintenance article to Baza Docs portal
-   Maintenance feature is covered with TSDocs
-   Common Environments (from `baza-core` package) are fully covered with TSDocs
-   Baza Account API subpackage fully covered with TSDocs, some additional resources exported

## Breaking changes

This release does not contain any breaking changes.

## Migration

-   Please set `BAZA_NORTH_CAPITAL_WEBHOOK_ENCRYPTION_KEY` environment variable for your testing, staging and production environments. Value of the environment should be same as set in NC panel. Read more about webhook security here: https://support.northcapital.com/knowledge/encrypt-webhook-events
-   Please set `BAZA_MANAGED_USERS_LOCK=1` environment variable for your testing, staging and production environments. It will lock possible changes around managed users which automatically creates on application bootstrap.

## Changes around leaving Purchase Flow

This [PR](https://github.com/scalio/baza/pull/755) covers fixing the `/409` session call bug which was occurring due to multiple` /session` calls being made at the same time, when going back from Step 2 (Agreement) to Step 1 (Details).

Now, the behavior of `/destroy` call would be as follows:

-   Upon loading of details page, `/destroy` call would be made to make sure all active sessions of user are destroyed on BE
-   Upon coming back from step 2 to step 1 (either through "Back" button click OR clicking back in browser), `/destroy` call would be made once as part of details page `ngOnInit` event
-   If back-link is clicked, `/destroy` call would be made once and user redirects back to listings information page, exiting purchase flow

## Managed Users Changes

-   Account Bootstrap reworked. Before it was working correctly only with `root@scal.io` and `user@scal.io` accounts, but now it handles additional emails & correctly sync changes from BAZA_MANAGED_USERS environment variable
-   If `BAZA_MANAGED_USERS` is missing, Account Bootstrap will bootstrap default accounts automatically
-   `BAZA_MANAGED_USERS_LOCK` environment variable added; set `BAZA_MANAGED_USERS_LOCK=1` to lock all changes and disable accident bootstrap / changes in future for production environment
-   Added `/baza-account/cms/bootstrap` endpoint which manually triggers account bootstrap (in addition with application bootstrap)
-   Account Bootstrap feature is covered with integration tests now
-   Added e2e helpers: `/baza-e2e/setEnv` to set environment variables and `/baza-e2e/resetEnv` to reset environment to initial state.
-   Removed all `vault` mentions and replaced with `managedUsers` keywords
-   Baza Account API subpackage fully covered with TSDocs, some additional resources exported
-   Added missing `baza-dwolla` packages to `publish-baza.sh`
-   Added TSDocs for `common-environments.ts` (baza-core environment variables now have proper TSDocs)
-   Added `bazaApiBundleConfigBuilder.withAccountFeatures` helper to configure account features

## Partial Withdraw Changes

-   Updated `/baza-nc/bank-accounts/withdraw` API endpoint (POST), to enable partial withdrawal from the Dwolla wallet option, and it is now ready to accept the amount parameter:

```typescript
{
  "amount": "14.33"
}
```

-   Added `WithdrawNcBankAccountRequest` class for withdrawal input object
-   `createFundingSourceWithPlaidToken` is now modified to return the created FundingSource ID
-   The respective `BazaNcBankAccountsDataAccess` and `BazaNcBankAccountsNodeAccess` services are updated to adopt the changes with `withdraw` method

## Details and Payment Page Customization

This [PR](https://github.com/scalio/baza/pull/763) covers adding the ability for customizing the "Listing Details" sub-heading in "Details Page" of purchase flow and ability to hide the message "Your bank account will be utilized to receive dividends" within the "Payment Page" of purchase flow with the help of custom configurations.

As part of this PR, the following changes were made:

-   New interfaces were created in purchase flow library
-   "DetailsConfig" and "PaymentConfig" be passed (optionally) as part of `PurchaseCustom` component
-   The interfaces are flexible to support any number of parameters

### Sample Code - Passing "PaymentConfig" to handle payment page & all associated sections on the page

**Component**

```typescript
paymentConfig: PaymentConfig;

    constructor(private readonly route: ActivatedRoute) {
         this.paymentConfig = {
            paymentMethodConfig: {
                showPurchaseDividendMessage: false
            }
        };
    }
```

**HTML**

```html
<app-purchase-payment
    *ngIf="currentTab === 2"
    target="purchase-payment"
    [paymentConfig]="paymentConfig"></app-purchase-payment>
```

### Sample Code - Passing "DetailsConfig" to handle details page & all associated sections on the page

**Component**

```typescript
detailsConfig: DetailsConfig;

    constructor(private readonly route: ActivatedRoute) {
         this.detailsConfig = {
            listingDetailsSubHeadingText: 'Horse Details'
        };
    }
```

**HTML**

```html
<app-purchase-details
    *ngIf="currentTab === 0"
    target="purchase-details"
    [personal$]="personal$"
    [detailsConfig]="detailsConfig"></app-purchase-details>
```

## JIRA

-   [BAZA-1060](https://scalio.atlassian.net/browse/BAZA-1060) Purchase Flow - 409 error on /destroy session
-   [BAZA-1081](https://scalio.atlassian.net/browse/BAZA-1081) Customization with the word 'Dividends'
-   [BAZA-489](https://scalio.atlassian.net/browse/BAZA-489) Add filters API
-   [BAZA-873](https://scalio.atlassian.net/browse/BAZA-873) Re-send email for token expiration
-   [BAZA-883](https://scalio.atlassian.net/browse/BAZA-883) Backend - Re-submit Payment Process
-   [BAZA-919](https://scalio.atlassian.net/browse/BAZA-919) Review the CSV and approve the payment of dividends
-   [BAZA-921](https://scalio.atlassian.net/browse/BAZA-921) Reprocess transactions in error so that investors will receive their dividends
-   [BAZA-923](https://scalio.atlassian.net/browse/BAZA-923) Email notifications on bases of triggered status
-   [BAZA-928](https://scalio.atlassian.net/browse/BAZA-928) Ability to cash out from my Dwolla Account to an external bank
-   [BAZA-930](https://scalio.atlassian.net/browse/BAZA-930) Ability for investors to reprocess failed payments
-   [BAZA-984](https://scalio.atlassian.net/browse/BAZA-984) Ability to set Bank Account as Bank Account for purchasing shares, receiving or withdrawing dividends
-   [BAZA-1010](https://scalio.atlassian.net/browse/BAZA-1010) Improper error handling for account verification with invalid postal code
-   [BAZA-1011](https://scalio.atlassian.net/browse/BAZA-1011) Last failed payment on Assets API Content
-   [BAZA-1026](https://scalio.atlassian.net/browse/BAZA-1026) Refactor NC Webhooks to fix transaction fund move update issue
-   [BAZA-1035](https://scalio.atlassian.net/browse/BAZA-1035) Status Filter on Portfolio Transactions (BE)
-   [BAZA-1047](https://scalio.atlassian.net/browse/BAZA-1047) Better security for NC Webhooks
-   [BAZA-1062](https://scalio.atlassian.net/browse/BAZA-1062) Baza-IVP repository issue
-   [BAZA-1065](https://scalio.atlassian.net/browse/BAZA-1065) Maintenance Mode should always take actual configuration
-   [BAZA-1088](https://scalio.atlassian.net/browse/BAZA-1088) CMNW: There are currently looking for withdraw + set bank account for withdraw using Plaid
-   [BAZA-1038](https://scalio.atlassian.net/browse/BAZA-1038) Create a whitespace interceptor service at BAZA level
-   [BAZA-1082](https://scalio.atlassian.net/browse/BAZA-1082) Ability to hide payout message on purchase flow
-   [BAZA-1039](https://scalio.atlassian.net/browse/BAZA-1039) Integrate animated skeleton placeholders in purchase & verification flow libraries

## JIRA (IVP)

-   [BAZA-1093](https://scalio.atlassian.net/browse/BAZA-1093) Icons for transactions types are not working for Safari browser on Portfolio

## PRs

-   https://github.com/scalio/baza/pull/747
-   https://github.com/scalio/baza/pull/752
-   https://github.com/scalio/baza/pull/753
-   https://github.com/scalio/baza/pull/756
-   https://github.com/scalio/baza/pull/757
-   https://github.com/scalio/baza/pull/759
-   https://github.com/scalio/baza/pull/762
-   https://github.com/scalio/baza/pull/763
-   https://github.com/scalio/baza/pull/764

## PRs (IVP)

-   https://github.com/scalio/baza/pull/767
