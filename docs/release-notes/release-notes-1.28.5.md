# Release 1.28.5

Bugfixes for Baza 1.28.0 for Dwolla PF

## Bugfixes

-   `baza-dwolla-web-purchase-flow`: fixes the `/bootstrap` endpoint call not being made properly resulting in payment methods not loading up in Dwolla PF payment page

## JIRA

-   [BAZA-1898](https://scalio.atlassian.net/browse/BAZA-1898) Payment Method Not Showing in Dwolla PF (CW Hotfix)

## PRs

-   https://github.com/scalio/baza/pull/1332
