# Release 1.10.0

New release (+ previous 1.9.\*) is focused on fixed issues with Baza Account CMS, JWT Guards, Offering UI FormControl component and many more. Additionally, there is new Select DocuSign Template for Offering feature which was long time requested for CMNW and may be very useful for MIP.

**What's new:**

-   Select DocuSign Template for Offering Feature - new API, CMS integration, changes in BazaNcCmsOfferingFieldComponent and many more
-   Baza NC DocuSign API with methods to fetch available DocuSign Templates for Offerings
-   Added FetchDocuSignTemplates method for TransactAPI package
-   Added NcOfferingPopulateService services which will allow to update/create offerings for target properties.

**Fixes:**

-   Baza Account CMS had multiple issues with ACL tab & ACL i18n. Now it's fixed.
-   Managed Accounts: managed accounts will be automatically marked as verified on sign in attempts
-   Fixed issue with Tax Documents Public endpoints. Now public endpoints correctly displays list of Tax Documents.
-   All fields in BazaNcCmsOfferingFieldComponent will be disable properly when loading details about offering

**Breaking changes:**

-   `BAZA_NORTH_CAPITAL_OFFERING_PURCHASE_TEMPLATES` environment variable is outdated and is not used anymore.

**Migration:**

-   Delete all `BAZA_NORTH_CAPITAL_OFFERING_PURCHASE_TEMPLATES` environment variables from .env files, charts, Vault and anywhere else.
-   Update offerings and assign DocuSign templates for each offering on test/stage/prod endpoints as fast as possible. Next 1.10.\* releases will mark column as required and if there will be any offerings w/o docusign templates on target environment, you will not be able to deploy updates
-   Replace custom implementation of update/create offering on property populates with NcOfferingPopulateService.populateOffering usage. Additionally, add `implements NcOfferingSupportedEntity` to property entity.

**PR's:**

-   https://github.com/scalio/baza/pull/60
-   https://github.com/scalio/baza/pull/62
-   https://github.com/scalio/baza/pull/63
-   https://github.com/scalio/baza/pull/64
-   https://github.com/scalio/baza/pull/65
-   https://github.com/scalio/baza/pull/66
-   https://github.com/scalio/baza/pull/67
