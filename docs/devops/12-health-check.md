# Health Check

As We are using different services like Kafka, Postgres, ... we want to have health-check module, that can be reached by `/health`.
Baza is using Terminus module to provide this health-check api.
This module will initialize by the application bundler and there is no need to custom installation.
Also, all the services that are checked by health-check module, have a default value like address, memory-size , etc.
So by default , there is no need to configure this module.
But, because of some checks, like Heap usage, are related to the situations, we can manually config them by some environment variables.
like : `BAZA_HEALTHCHECK_MEMORY_HEAP_MAX_MBYTES` , described in [02-environment-variables-api](./02-environment-variables-api.md)

**API** `/health` route can be reached from the main url, without any prefix.

```
curl 'http://localhost:3000/health'
```

**Items**

Health-Check module will check these items , every time `/health` invoked :

-   baza_docs : `BAZA_APP_API_URL`/`docs`.
-   baza_redoc : `BAZA_APP_API_URL`/`redoc`.
-   writable_tmp_dir: We will check our `tmp` directory is writable or not. (`${process.cwd()}/tmp/`)
-   memory_heap: To make sure our process does not exceed a certain memory limit, the `memory_heap` can be used.
-   memory_rss : We can verify the memory RSS of our process with `memory_rss`.
-   database : We want to be sure that we are connected to our database.
-   kafka : We want to check our application can reach kafka clusters.
    -   If `KAFKA_ENABLED=0`, healthcheck will not be triggered / included at all.

**Response**

-   **Successful**: If everything is ok, we get `"status": "ok"` and json response will be like :

```json
{
    "status": "ok",
    "info": {
        "baza_docs": {
            "status": "up"
        },
        "baza_redoc": {
            "status": "up"
        },
        "writable_tmp_dir": {
            "status": "up"
        },
        "memory_heap": {
            "status": "up"
        },
        "memory_rss": {
            "status": "up"
        },
        "database": {
            "status": "up"
        },
        "kafka": {
            "status": "up"
        }
    },
    "error": {},
    "details": {
        "baza_docs": {
            "status": "up"
        },
        "baza_redoc": {
            "status": "up"
        },
        "writable_tmp_dir": {
            "status": "up"
        },
        "memory_heap": {
            "status": "up"
        },
        "memory_rss": {
            "status": "up"
        },
        "database": {
            "status": "up"
        },
        "kafka": {
            "status": "up"
        }
    }
}
```

-   **503**: If one of the check items fail , we have and `503` error in our `/health` route :

```json
{
    "statusCode": 503,
    "timestamp": "2022-11-18T13:16:43.249Z",
    "path": "/health",
    "code": "InternalServerError",
    "message": "Service Unavailable Exception"
}
```
