# E2E Tests for CI/CD workflows

Baza has multiple commands to run E2E tests.

Please note that API integration tests uses DB / Redis flush a lot.

| Command                                   	| Description                                                                                  	| CI/CD? 	|
|-------------------------------------------	|----------------------------------------------------------------------------------------------	|--------	|
| **yarn api:tests:integration-tests**      	| Runs API integrations tests in watch mode. Used for development only                         	| No     	|
| **yarn api:tests:integration-tests:once** 	| Runs all API integration tests, w/o watch option. Used for development only                  	| No     	|
| **yarn api:tests:integration-tests:ci**   	| Runs tests using `e2e-ci.env` environments. You may use it for debug purposes                	| No *   	|
| **yarn api:build**                            | It's common command to build API; Use it with `NODE_ENV=e2e-ci` to build E2E-CI build        	| Yes    	|
| **yarn cms:e2e**                          	| Runs CMS E2E tests in watch mode. Used for development only                                  	| No     	|
| **yarn cms:e2e:ci**                       	| Runs all CMS E2E tests, w/o watch option. Used for development only                          	| No *   	|
| **yarn cms:build:e2e-ci**                 	| Builds CMS using `environment.e2e-ci.ts` environments. You should use it for CI/CD workflows 	| Yes    	|
| **yarn web:e2e**                          	| Runs WEB E2E tests in watch mode. Used for development only                                  	| No     	|
| **yarn web:e2e:ci**                       	| Runs all WEB E2E testsm w/o watch option. Used for development only                          	| No *   	|
| **yarn web:build:e2e-ci**                 	| Builds WEB using `environment.e2e-ci.ts` environments. You should use it for CI/CD workflows 	| Yes    	|


## Kafka and E2E tests

> ⚠️ **Running integration tests with Test/Stage/Production Kafka's GroupID and ClientID may have huge impact to real data and will lead to many and sometimes unresolvable problems**

Please make sure that E2E tests uses Kafka with dedicated group/client ID's (see `KAFKA_CLIENT_ID` and `KAFKA_CLIENT_GROUP_ID` environment variables at [Environment Variables – API](/devops/environment-variables-api.md) page). 
