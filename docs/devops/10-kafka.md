# Set Up Kafka for Different Environments

> ⚠️ Read about Consumers, Clients & Groups on Kafka Documentation: [https://docs.confluent.io/platform/current/clients/consumer.html]()

- You can use single kafka instance for all environments, but you need to ensure that there will be no collisions between different deployment targets. 
- You should avoid multiple events consumes when API runs in cluster mode.
- There is `KAFKA_CLIENT_ID` and `KAFKA_CLIENT_GROUP_ID` environment variables available to set up Client & Group for Kafka connection.
- You should set up different `KAFKA_CLIENT_GROUP_ID` and different `KAFKA_CLIENT_ID` for each deployment target
- You should set uo different `KAFKA_NAMESPACE` for each deployment target
- It's recommended to use different Kafka instances for different projects, especially for projects with North Capital integrations.
- Both environment variables accept `$1` template variables which replaces with unique random 8 symbols; Most projects will not need it.

## Example: Common Setup

Events with common setup will be consumed once per cluster, and there will be no collisions between different environments.

```env
# stg.env
KAFKA_CLIENT_ID=my-project-stg-consumer
KAFKA_CLIENT_GROUP_ID=y-project-stg-group
KAFKA_NAMESPACE=project-stg

# prod.env
KAFKA_CLIENT_ID=y-project-prod-consumer
KAFKA_CLIENT_GROUP_ID=y-project-prod-group
KAFKA_NAMESPACE=project-prod
```

## ⚠️ Special cases for projects with North Capital integrations

It's mandatory to have different Kafka instances for projects which works with North Capital integration. North Capital uses non-unique IDs between customers and there is a low chance that 2 or more customers
will collide each other. You still can use same Kafka instance for different environments within project.
