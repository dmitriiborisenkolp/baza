# API Status

Baza API will return status and some additional information on `/` route (i.e. `BAZA_APP_API_URL` + `BAZA_API_PREFIX` optionally URL):

```json
{
  "live": "OK",
  "memoryUsageMB": 115,
  "clientIp": "178.140.2.71",
  "apiVersion": "1.9.4",
  "supportedApiVersions": [
    "1.0.0"
  ],
  "gitVersion": "v1234",
  "bazaEnvironment": "stg",
  "isProductionEnvironment": false,
  "isE2EEnvironment": false
}
```

## JSON Fields

| Field                   	    | Description                                                                             	| Values / Examples                                          	|
|------------------------------ |-----------------------------------------------------------------------------------------	|------------------------------------------------------------	|
| **live**                      | It's always `OK`                                                                        	| OK                                                         	|
| **memoryUsageMB**             | Memory usage in MBytes (as node's `process` thinks)                                     	| 115                                                        	|
| **clientIp**                  | Your (Client) IP. You need to bypass `X-Forwarded-For` header to see proper value here 	| 178.140.2.71                                               	|
| **supportedApiVersions**      | List of versions supported by API                                                       	| ["1.0.0","1.0.1","1.0.2"]                                    	|
| **gitVersion**                | Build version (`GIT_VERSION` environment value)                                         	| v1234                                                      	|
| **bazaEnvironment**           | Current API environment (`BAZA_ENV` value)                                              	| (empty), `e2e`, `test`, `stage`, `development`, `production` 	|
| **isProductionEnvironment** 	| Will contains `true` for production environment (i.e. `BAZA_ENV` is `production`)       	| `true` or `false`                                          	|
| **isE2EEnvironment**        	| Will contains `true` for E2E environment (i.e. `BAZA_ENV` is `e2e`)                    	| `true` or `false`                                          	|

## Client IP

For more information about `clientIp` field, visit [Http Headers](/devops/http-headers-api) page
