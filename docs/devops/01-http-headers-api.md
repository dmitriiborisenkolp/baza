# HTTP Headers

Baza uses / requires some additional HTTP Headers for applications. There headers should be allowed to use:

| Header                            	| Description                                                                                                                                     	| Possible values / examples                                                                                                  	| In Request 	| In Response 	|
|-----------------------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------	|-----------------------------------------------------------------------------------------------------------------------------	|------------	|-------------	|
| **X-Baza-Application**            	| Identify application which makes requests to API                                                                                                	| `api`, `cms`, `web`, `ios`, `android`                                                                                       	| Yes        	| No          	|
| **X-Baza-Application-Id**         	| Unique Application Id. Can be any string                                                                                                        	| `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36` 	| Yes        	| No          	|
| **X-Baza-Api-Version**            	| Requested API version. API will returns `406` HTTP Status code if current deployed API is not able to handle client with requested API version. 	| `1.0.0`                                                                                                                     	| Yes        	| No          	|
| **X-Baza-Api-Supported-Versions** 	| Returns list of supported API versions                                                                                                          	| `1.0.0,1.0.1,1.0.2,1.0.3`                                                                                                   	| No         	| Yes         	|
| **X-Baza-Skip-Maintenance**       	| Special header to skip Maintenance mode. Works only with specific conditions                                                                    	| `0` or `1`                                                                                                                  	| Yes        	| No          	|
| **X-Baza-I18n-Language**          	| Requested language. API can response with well-translated error messages or anything else if the header is provided in request                  	| `en`                                                                                                                        	| Yes        	| No          	|

## Health Check

Baza will response with `406` HTTP status if application did not provide or provide outdated API version, or with `503` HTTP status if application is under maintenance (see [Maintenance](/devops/01-maintenance) page).

Specifically for devops flows, if request contains `kube` keyword in `User-Agent` header, API will ignore API version check or Maintenance modes.

## Client IP

You will need also to bypass real client IP using `X-Forwarded-For` header.

If you set up it properly, you will see client IP at [status endpoint](/devops/status-api).
