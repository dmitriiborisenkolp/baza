# Master Node

Enables node master to cluster environment which will be enabled for single instance only, the variable is only in local.env

**Interface** defined in [aza-core-api/src/libs/baza-common/src/lib/models/common-environments.ts]

-   @default: 1
-   @type boolean
    BAZA_MASTER: boolean;

**Service** to manipulate the value: EnvService [baza-core-api/src/libs/baza-env/src/lib/services/env.service.ts]
the getter return the value of env:

```
get isMaster(): boolean {
    return this.getAsBoolean('BAZA_MASTER', {
    defaultValue: false,
});
}
```

**Pm2 Config:**
Config about enables master node in pm2, exec_mode: <single> and settled the <BAZA_MASTER>: 1 to enables node master;

```
{
   name: 'baza-api',
   script: './dist/apps/api/main.js',
   args: '',
   cwd: '.',
   autorestart: true,
   watch: false,
   instances: 1,
   exec_mode: 'single',
   max_memory_restart: '4G',
   env: {
       NODE_ENV: '',
       BAZA_MASTER: '1',
   },
},
```
