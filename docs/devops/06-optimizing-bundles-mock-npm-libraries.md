# Optimizing WEB and CMS bundles for Test / Stage / Production

## Issue

We are using shared DTOs & models both for API and CMS/Web. These DTOs and models uses some heavy libraries like `class-validator` which we don't actually need in final bundle. 

To avoid big bundle sizes we need to use mock version of some NPM packages specifically for Web and CMS builds.

Additionally, we want to disable usage of some packages in shared space. There will be no build errors if developer will try to push / build new PR, but CI/CD workflows MUST fails with specific messages.

These libraries are target to mock:

- `class-transformer`
- `class-validator`
- `moment`
- `typeorm`

## Solution

There is a `baza-core-devops` NPM package which can be installed with project. You need to replace some NPM packages in `node_modules` with mock implementations during the WEB / CMS builds only.
This mocks are located in `node_modules/@scaliolabs/baza-core-devops/src/lib/npm-package-mocks`.

**This packages must not be replaced for API build.** It should be only done for WEB / CMS builds;

## CMS

You need to mock these libraries:

- `class-transformer`
- `class-validator`
- `typeorm`

```sh
rm -rf node_modules/class-transformer && cp -R node_modules/@scaliolabs/baza-core-devops/src/lib/npm-package-mocks/class-transformer
rm -rf node_modules/class-validator && cp -R node_modules/@scaliolabs/baza-core-devops/src/lib/npm-package-mocks/class-validator
rm -rf node_modules/typeorm && cp -R node_modules/@scaliolabs/baza-core-devops/src/lib/npm-package-mocks/typeorm
```

## WEB

You need to mock these libraries:

- `class-transformer`
- `class-validator`
- `moment`
- `typeorm`

Note: there is a chance that WEB project will actually need to use `moment`. If there are reasons to not use any lightweight alternatives like `days.js`, you should not mock `moment` library. 

```sh
rm -rf node_modules/class-transformer && cp -R node_modules/@scaliolabs/baza-core-devops/src/lib/npm-package-mocks/class-transformer
rm -rf node_modules/class-validator && cp -R node_modules/@scaliolabs/baza-core-devops/src/lib/npm-package-mocks/class-validator
rm -rf node_modules/moment && cp -R node_modules/@scaliolabs/baza-core-devops/src/lib/npm-package-mocks/moment
rm -rf node_modules/typeorm && cp -R node_modules/@scaliolabs/baza-core-devops/src/lib/npm-package-mocks/typeorm
```
