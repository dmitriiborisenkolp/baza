# Build API, CMS and WEB applications

## Building API

- There is single command to build API for every deploy target: `yarn api:build`
- API will take environment variables from `envs` directory. If `NODE_ENV` is set, it will take `envs/${NODE_ENV}.env` file. If `NODE_ENV` is not set or empty, a `envs/.env` environment will be used.

## CLI Commands

There are additional CLI tools available from Baza:

| Command                                   	| Description                                                                                                                                     	| Should be used on every deploy? 	|
|-------------------------------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------	|---------------------------------	|
| **yarn cli:typeorm:migrate**              	| Run Baza & TypeORM migrations. It builds slowly and contains issues related with @nrwl/node package; It's used only for development environment 	| No *                            	|
| **yarn cli:typeorm:migrate:build**        	| Builds `cli:typeorm:migrate` tool. It should be executed on every deploy, then:   `node dist/apps/cli/typeorm/migrate/main.js`                  	| Yes                             	|
| **yarn cli:typeorm:migrate-down**         	| Down Baza & TypeOrm migrations. Should not be used at all on deploy targets.                                                                    	| No                              	|
| **yarn cli:typeorm:synchronize**          	| Syncronize DB schema. In general it shouldn't be used on deploy targets                                                                         	| No                              	|
| **yarn cli:typeorm:flush**                	| Drop database. It's used only on development environments                                                                                       	| Strongly No                     	|
| **yarn cli:redis:flush**                  	| Drop REDIS. It's used only on development environments                                                                                          	| Strongly No                     	|
| **yarn cli:kafka:bootstrap-topics**       	| Bootstrap Kafka topics. It builds slowly and contains issues related with @nrwl/node package; It's used only for development environment        	| No                              	|
| **yarn cli:kafka:bootstrap-topics:build** 	| Builds `cli:kafka:bootstra-topics` tool. It should be executed on every deploy, then:  `node ./dist/apps/cli/kafka/bootstrap-topics/main.js`    	| Yes                             	|

## Building CMS

Each deploy target has specific command to build CMS:

```sh
yarn cms:build:prod # Production Build
yarn cms:build:stg # Stage build
yarn cms:build:test # Test build
yarn cms:build:e2e-ci # Build for E2E worksflows
```

## Building WEB

Each deploy target has specific command to build WEB:

```sh
yarn web:build:prod # Production Build
yarn web:build:stg # Stage build
yarn web:build:test # Test build
yarn web:build:e2e-ci # Build for E2E worksflows
```

In addition, please read [Optimizing WEB and CMS bundles for Test / Stage / Production](/devops/optimizing-bundles-mock-npm-libraries) article for best results.

