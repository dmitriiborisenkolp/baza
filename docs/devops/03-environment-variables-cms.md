# Environment variables – CMS

Environment configurations for CMS are stored in `apps/cms/environments` directory.

## Ng Environments

All Angular applications uses same `NgEnvironment` enum with list of possible environments:

```typescript
export enum NgEnvironment {
    Local = 'local',
    Prod = 'prod',
    UAT = 'uat',
    Preprod = 'preprod',
    Stage = 'stage',
    Test = 'test',
    E2e = 'e2e',
}
```

## Set up endpoint

You need to keep `BAZA_APP_API_URL` of each deploy target synced with `ngEndpoint` CMS environment field:

```typescript
import { Environment } from '../models/environment';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export const environment: Environment = {
    ngEnv: NgEnvironment.Test,
    enableAngularProduction: true,
    apiEndpoint: 'https://my-project-api.test.scaliolabs.com',
    sentry: {
        dsn: 'https://55bccaf181e84732a3c62cdb8b160062@sentry.production.scaliolabs.com/13',
    },
};
```

## Set up Sentry

If you need to add Sentry integration, you need to provide Sentry DSN in CMS environment configuration:

```typescript
import { Environment } from '../models/environment';
import { NgEnvironment } from '@scaliolabs/baza-core-ng';

export const environment: Environment = {
    ngEnv: NgEnvironment.Stage,
    enableAngularProduction: true,
    apiEndpoint: 'https://baza-api.stage.scaliolabs.com',
    sentry: {
        dsn: '<PLACE-DSN-HERE>',
        browserOptions: {
            // You can set up additional Sentry configuration here
            // @see https://docs.sentry.io/platforms/javascript/configuration/options/
        },
    },
};
```
