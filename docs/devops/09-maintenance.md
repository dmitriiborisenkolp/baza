# Maintenance

Web, Android or IOS application can be temporary put to maintenance mode using Maintenance CMS. If application is under maintenance mode, API will response with `503` HTTP status code for any attempts to use API with halted application.

Health Checks will not be affected my maintenance mode. Baza will ignore maintenance mode if `User-Agent` HTTP headers contains `kube` keyword.

See [Maintenance](/overview/27-maintenance) for additional details.
