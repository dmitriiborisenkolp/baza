# API Library structure

Most features will require these assets for API implementation:

- [TypeORM Entity(-ies)](/api-development/04-entities)
- [Repositories](/api-development/04-entities) contains methods to save/update/get entities
- [Mappers](/api-development/07-mappers) responsible to convert TypeORM Entities to DTOs
- [Services](/api-development/08-services) contains Domain and uses Entities/Repositories/Mappers
- [Controllers](/api-development/09-controllers) uses Services and Mappers to actually implement Endpoints defined on API Design step
- [Data Access](/api-development/10-e2e) to publish API for E2E and Angular applications
- [Integration Test](/api-development/11-e2e) to test & cover your API with integration tests.

You need create NX Library for feature, set up directory structure and code assets mentioned before.

Read more about directory structure on [Directory Structure & Libraries](/overview/02-libraries) documentation page.

## Generate NX Library

```bash
nx g @nrwl/nest:lib your-project-api/feature-task
```

## Set up directory structure for API feature

This example shows common directory structure for API feature. Optionally, you can add `event-handlers`,
`command-handlers` and anything else you need.

```
your-project-api/feature-task
├── controllers/
│   ├── task.controller.ts
│   └── task-cms.controller.ts
├── entities/
│   └─ your-project-task.entity.ts
├── exceptions/
│   └── task-not-found.exception.ts
├── mappers/
│   └── your-project-task.mapper.ts
├── integration-tests/
│   ├── tests
│   │   ├─ 001-your-project-feature-task.spec.ts
│   │   └─ 002-your-project-feature-task.spec.ts
│   ├── fixtures
│   │   └─ your-project-feature-tasks.fixture.ts 
│   └── your-project-feature-task.fixtures.ts
├── repositories/
│  └─ your-project-task.repository.ts
└── services/
│  └─ your-project-task.service.ts
└ index.ts
```

### 1. Directory `controllers/`

Controllers directory should contains Public and CMS controllers for API feature. You should
split controller methods for different clients.

Your controllers should reflect defined Endpoints of feature and implement related endpoint interfaces.

### 2. Directory `entities/`

Your should put here Entities required by feature. Make sure that you entities will not interfere with existing
entities in Baza or with entities from other features. You can do it with using unique names, prefixing entities
with project code name or with `name` option of `@Entity()` decorator.

### 3. Directory `repositories/`

Repository contains methods related to accessing, creating, removing, updating TypeORM entities or anything else
related to external sources, usually DB. Services should not work with TypeORM repositories directly, because it 
leads to code duplication.

### 4. Directory `exceptions/`

You should create exceptions for specific exception cases, like required feature Entity was not found or duplicates
existing one. Usually `exceptions/` directory reflects Error Codes which you declared for feature within API design
step.

Exceptions could be used both by Repositories or Services layers.

### 5. Directory `mappers/`

Mappers are responsible for conversion TypeORM entities to DTOs. You should not use TypeORM entities directly in
API design and especially use them for DTO definitions or Endpoint responses - even if API will builds correctly,
Frontend clients will not be able to use them because of additional dependencies with TypeORM package.

### 6. Directory `services/`

Services uses entities, repositories, mappers, exceptions and other resources and contains actual API implementation.

Sometimes it may be useful to split services in same way as controllers, i.e. split services for Public and CMS API.

### 7. Directory `integration-tests/`

Integration tests are final point of API implementation. Usually you need to create ENUM with Fixtures Set for feature,
create fixtures with sample data and write integration tests for each declared Endpoint.

Jest usually runs tests in alphabetic order. It's recommended to prefix integration tests with `NNN-...` .

### 8. File `index.ts`

Don't forget to publish assets with `index.ts` if required. You should publish entities, mappers, repositories and sometimes
you need to publish services. Don't export exceptions - it creates huge potential for circular dependencies. 

> ⚠️ If you need to use exception from another feature, it's better to duplicate same exception specifically inside target feature.
