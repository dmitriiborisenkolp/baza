# Development cluster with PM2

You can run API as cluster (multi-node) environment using [PM2](https://pm2.keymetrics.io/). It can be useful to debug anything related to events, Kafka or anything else which may works differently depends on
single-node / multi-node setup.

Baza Skeleton already has all necessary yarn commands and PM2 configuration to run API in cluster mode.

## Yarn Commands

| Command                     	| PM Command                                  	| Description                                              	| PM2 Documentation                                                	|
|-----------------------------	|---------------------------------------------	|----------------------------------------------------------	|------------------------------------------------------------------	|
| **yarn api:cluster:up**     	| `pm2 start -f ecosystem-dev.config.js`      	| Start API in Cluster Mode with Max 4 instances           	| [Link](https://pm2.keymetrics.io/docs/usage/process-management/) 	|
| **yarn api:cluster:down**   	| `pm2 stop all -f ecosystem-dev.config.js`   	| Stop API                                                 	| [Link](https://pm2.keymetrics.io/docs/usage/process-management/) 	|
| **yarn api:cluster:update** 	| `pm2 reload all -f ecosystem-dev.config.js` 	| Rebuild API & Restart                                    	| [Link](https://pm2.keymetrics.io/docs/usage/process-management/) 	|
| **yarn api:cluster:status** 	| `pm2 status`                                	| Display nodes statuses                                   	| [Link](https://pm2.keymetrics.io/docs/usage/process-management/) 	|
| **yarn api:cluster:monit**  	| `pm2 monit`                                 	| Displays htop-like tool but for nodes                    	| [Link](https://pm2.keymetrics.io/docs/usage/monitoring/)         	|
| **yarn api:cluster:logs**   	| `pm2 logs`                                  	| Displays cluster logs (all nodes logs) in tail -f manner 	| [Link](https://pm2.keymetrics.io/docs/usage/log-management/)     	|
