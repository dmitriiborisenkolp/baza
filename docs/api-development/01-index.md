# API Development

The following section describes common API Development flow for features. As API developer, you're responsible
to design API (Endpoints, DTOs, Error Codes), Implement API with Entities/Repositories/Services/Controllers and publish
your API with Data Access layer.

Baza  follows Layered Architecture pattern. Every feature could be implemented step-by-step with set of isolated
layers. Most features could be implemented same way as it's described here. 

## API Design

API Design step requires to define Endpoints (set of HTTP methods for feature), DTOs, Error Codes and optionally some
more additional models. These resources are shared and could be used for API, CMS or WEB applications. It looks like as GRPC
and RPC Services like in Protobuf.

Read more about API Design here: [API Design](/api-development/02-api-design)

## Public API vs CMS API

It's usually a good idea to split API for public usage and API for CMS. You should not mix methods for CMS and Public
API's in same controller, an as far as every contoller implements some Endpoint, it's obviously that you can
split API with CMS and Public sections.

## API Implementation

Baza strictly supports & suggests using Layered Architecture. Most API features for your project will require implement
these assets in following order:

- [TypeORM Entity(-ies)](/api-development/04-entities)
- [Repositories](/api-development/04-entities) contains methods to save/update/get entities
- [Mappers](/api-development/07-mappers) responsible to convert TypeORM Entities to DTOs
- [Services](/api-development/08-services) contains Domain and uses Entities/Repositories/Mappers
- [Controllers](/api-development/09-controllers) uses Services and Mappers to actually implement Endpoints defined on API Design step
- [Data Access](/api-development/10-e2e) to publish API for E2E and Angular applications
- [Integration Test](/api-development/11-e2e) to test & cover your API with integration tests.

## API Testing

API Development Flow partially supports TBD flow, but it's recommended to write tests after finishing implementation. You
are able to completely test your API with [Integration Tests](/api-development/11-e2e) only - and it's much easier than using Postman. 

## Migrations

Baza supports [Migrations](/overview/28-migrations). These migrations are simple Nest.JS services. You can use all tools & services
available in your API implementation.
