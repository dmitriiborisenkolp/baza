# API Design

Before actual implementation, you need to design your API. It's mandatory step - you'll need to define Endpoints, DTOs,
Error Codes and additional models. All these resources will be used for API implementation and - in most cases - will be
used for CMS and WEB applications.

API Design should be located in `*-shared` libraries created with `@nrwl/node:lib` schematic.

> ⚡️ **Starting new Project**
> 
> When you're starting a new project, you can discuss designing API for whole project before actual implementation.
> This approach would allow clients to review API first before delivery, and allow you to mock your API before actual
> implementation will be developed & delivered.

## Endpoints

Endpoints are very similar to Protobuf Services ([https://developers.google.com/protocol-buffers/docs/proto]()). You need
to define list of endpoints paths for your feature and define Endpoint interface with list of available methods for feature.
Each method could have Request Body (`POST`, `PUT` HTTP method) or Request Parameters (`GET`, `HEAD`) and each method will
have Response, both in Promise (for API or Node environments) and Observable (Angular) variants.

## DTOs

> ⚠️ Don't use TypeORM Entities directly as API Response - always convert TypeORM Entities to DTOs! You also can't use 
> TypeORM Entities as DTO in shared space - decorators from TypeORM will fail builds for Angular environments.

DTOs represents TypeORM Entiies or any other Objects which you would like to share with clients. DTOs should have `@ApiModelProperty()`
decorators for each field, and additionally decorators from `class-validator` package. By default Baza validates
both Request and Response bodies.

Make sure that every DTO (and additional Request and Response DTOs for Endpoints) will match these requirements:

- All fields have `@ApiModelProperty()` decorator;
- All fields have `@IsNotEmpty()`, `@IsDefined()` or `@IsOptional()` decorators;
- All fields have proper coverage with validators from `class-validator` package;
- If field is optional, `@ApiModelProperty()` should have `required: false` in options;
- If field is array of something, `@ApiModelProperty()` will have `isArray: true` and `type` parameters

## Error Codes

Every feature can define list of Error Codes specific for the feature. It's recommended to also put i18n resources
here for default error messages.

These error codes are used when implementing exceptions and could be used by clients - especially when they need to
implement some steps depends on specific cases.

Read more about error codes in [Errors & Exceptions](/overview/22-errors-and-exceptions) documentation section.

## Additional models

Additional models could contants, enums, additional classes or anything else. The only requirement here is not using any
imports which can broke Angular builds in attempts to use these resources.

## Example: TODO Task API Design

### 1. Create new library

You should use `@nrwl/node:lib` schematic:

```bash
nx g @nrwl/node:lib your-project-shared/feature-task
```

Remove all resources inside `lib/` of generated library - you'll don't need it at all.

### 2. Add DTO for Task

```typescript
// libs/your-project-shared/src/lib/feature-task/dto/task.dto.ts
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { AttachmentDto, CrudSortableEntity } from '@scaliolabs/baza-core-shared';
import { IsArray, IsInt, IsNotEmpty, IsOptional, IsPositive, IsString, Min, ValidateNested } from 'class-validator';

export class TaskDto implements CrudSortableEntity{
    @ApiModelProperty()
    @IsPositive()
    @IsInt()
    @IsNotEmpty()
    id: number;

    @ApiModelProperty()
    @Min(0)
    @IsInt()
    @IsNotEmpty()
    sortOrder: number;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    title: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiModelProperty({
        type: 'string',
        isArray: true,
    })
    @IsString({ each: true })
    @IsArray()
    @IsNotEmpty()
    tags: Array<string>;

    @ApiModelProperty({
        required: false,
    })
    @ValidateNested()
    @IsOptional()
    cover?: AttachmentDto;
}
```

### 3. Define Error Codes for feature

We can define error messages also in same file.

```typescript
// libs/your-project-shared/src/lib/feature-task/error-codes/task.error-code.ts
export enum TaskErrorCodes {
    YourProjectTaskNotFound = 'YourProjectTaskNotFound',
}

export const taskErrorCodesI18n = {
    [TaskErrorCodes.YourProjectTaskNotFound]: 'Task Not Found',
};
```

### 4. Define Endpoint for CMS API

CMS and Public API should be split. Don't mix methods for CMS and Public API in same controller - and if you have same
methods for differrent clients, you should duplicate definitions both for CMS and Public API.

CMS API mostly can be implemented with POST-only approach. It's easier in general to work with POST methods because
of JSON Request Body support.

> ⚠️ Update-like methods should not allow to partially update entities. CMS will always send full DTO of updated entity.

```typescript
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { IsArray, IsInt, IsNotEmpty, IsOptional, IsPositive, IsString, ValidateNested } from 'class-validator';
import { AttachmentDto, CrudListRequestDto, CrudListResponseDto, CrudSetSortOrderRequestDto } from '@scaliolabs/baza-core-shared';
import { TaskDto } from '../dto/task.dto';
import { Observable } from 'rxjs';

export enum TaskCmsEndpointPaths {
    create = '/your-project/cms/task/create',
    update = '/your-project/cms/task/update',
    delete = '/your-project/cms/task/delete',
    list = '/your-project/cms/task/list',
    getById = '/your-project/cms/task/getById',
    setSortOrder = '/your-project/cms/task/setSortOrder',
}

export interface TaskCmsEndpoint {
    create(request: TaskCmsCreateRequest): Promise<TaskDto> | Observable<TaskDto>;
    update(request: TaskCmsUpdateRequest): Promise<TaskDto> | Observable<TaskDto>;
    delete(request: TaskCmsDeleteRequest): Promise<void> | Observable<void>;
    list(request: TaskCmsListRequest): Promise<TaskCmsListResponse> | Observable<TaskCmsListResponse>;
    getById(request: TaskCmsGetByIdRequest): Promise<TaskDto> | Observable<TaskDto>;
    setSortOrder(request: TaskCmsCreateRequest): Promise<void> | Observable<void>;
}

// Entity Body is used as Base class for Create/Update operations
export class TaskCmsEntityBody {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    title: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    description: string;

    @ApiModelProperty({
        type: 'string',
        isArray: true,
    })
    @IsString({ each: true })
    @IsArray()
    @IsNotEmpty()
    tags: Array<string>;

    @ApiModelProperty({
        required: false,
    })
    @ValidateNested()
    @IsOptional()
    cover?: AttachmentDto;
}

export class TaskCmsCreateRequest extends TaskCmsEntityBody {}

export class TaskCmsUpdateRequest extends TaskCmsEntityBody {
    @ApiModelProperty()
    @IsPositive()
    @IsInt()
    @IsNotEmpty()
    id: number;
}

export class TaskCmsDeleteRequest {
    @ApiModelProperty()
    @IsPositive()
    @IsInt()
    @IsNotEmpty()
    id: number;
}

export class TaskCmsGetByIdRequest {
    @ApiModelProperty()
    @IsPositive()
    @IsInt()
    @IsNotEmpty()
    id: number;
}

export class TaskCmsListRequest extends CrudListRequestDto<TaskDto> {}

export class TaskCmsListResponse extends CrudListResponseDto<TaskDto> {
    // You'll need to re-define `CrudListResponseDto.items` field for documentation reasons
    @ApiModelProperty({
        type: TaskDto,
        isArray: true,
    })
    @ValidateNested({ each: true })
    @IsArray()
    @IsNotEmpty()
    items: Array<TaskDto>;
}

export class TaskCmsListSetSortOrderRequest extends CrudSetSortOrderRequestDto {}
```

### 5. Defined Endpoint for WEB API

Public API should follow common recommendations about HTTP Methods. You should use GET, POST, PUT and other
http methods for Public API. Data Access services has additional helpers which allows you to replace arguments in 
endpoint paths.

```typescript
// libs/your-project-shared/src/lib/feature-task/endpoints/task.endpoint.ts
import { CrudListRequestQueryDto, CrudListResponseDto } from '@scaliolabs/baza-core-shared';
import { TaskDto } from '../dto/task.dto';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { IsArray, IsNotEmpty, ValidateNested } from 'class-validator';
import { Observable } from 'rxjs';

export enum TaskEndpointPaths {
    list = '/your-project/task/list',
    getById = '/your-project/task/get/:id',
}

export interface TaskEndpoint {
    list(request: TaskListRequest): Promise<TaskListResponse> | Observable<TaskListResponse>;
    getById(id: number): Promise<TaskDto> | Observable<TaskDto>;
}

export class TaskListRequest extends CrudListRequestQueryDto {}

export class TaskListResponse extends CrudListResponseDto<TaskDto> {
    // You'll need to re-define `CrudListResponseDto.items` field for documentation reasons
    @ApiModelProperty({
        type: TaskDto,
        isArray: true,
    })
    @ValidateNested({ each: true })
    @IsArray()
    @IsNotEmpty()
    items: Array<TaskDto>;
}
```

### 6. Add assets to `index.ts`

It's general requirement for Nx to publish your assets with `index.ts`.

```typescript
// libs/your-project-shared/src/lib/index.ts
export * from './lib/dto/task.dto';

export * from './lib/error-codes/task.error-codes';

export * from './lib/endpoints/task.endpoint';
export * from './lib/endpoints/task-cms.endpoint';
```

### 7. Next steps

Your API is designed, and you can start your actual API implementation.
