# Images Ratio Configuration for Listing

By default `baza-nc-integration` packages requires `1.6` ratio for images. If you would like to change default image ratio for Cover and Listing Details images, you should update
`BazaNcIntegrationListingsConstants.LISTINGS_COVER_RATIO` and `BazaNcIntegrationListingsConstants.LISTINGS_IMAGES_RATIO` options. Your configuration should be duplicated both API
and CMS.

## Example

```typescript
// API:
// apps/api/src/app/main-config.ts
import { BazaNcIntegrationListingsConstants } from '@scaliolabs/baza-nc-integration-shared';

BazaNcIntegrationListingsConstants.LISTINGS_COVER_RATIO = 16 / 9;
BazaNcIntegrationListingsConstants.LISTINGS_IMAGES_RATIO = 4 / 3;

// same for CMS:
// apps/cms/src/app/app.module.ts
import { BazaNcIntegrationListingsConstants } from '@scaliolabs/baza-nc-integration-shared';

BazaNcIntegrationListingsConstants.LISTINGS_COVER_RATIO = 16 / 9;
BazaNcIntegrationListingsConstants.LISTINGS_IMAGES_RATIO = 4 / 3;
```
