## Web, IOS – Usage of New Bank Account API

The Guide covers API usage for UI implementation of Payment Methods component with ability to Purchase Shares using Account Balance (Plaid), NC ACH (Manual & Plaid) and linking Cash-Out Bank Account to Withdraw dividends. The following implementation will use different bank accounts for Purchase Shares using NC ACH, Purchasing Shares using Account Balance and Withdrawing Dividends using Cash-Out Bank Accounts. The implementation will not use multiple bank accounts per each type feature due of masking and securing Account and Routing Numbers in Database.

## General

We're using **LBA** as Legacy Bank Account API and **NBA** for New Bank Account API in Guide below.

-   NC ACH (Set Manual Bank Account) should be set with **NBA** (`add` method) with exports to NC only,`setAsDefault` as `false`, `static` as `false` and `secure` as true. It will immediately allow you to use Bank Account for purchasing shares using NC ACH API.
-   NC ACH (Using Plaid Integration) should be set with **NBA** (`link` and `linkOnSuccess` endpoints) with `exports` option to `['NC']` only and with `setAsDefault` as `false`, `static` as `false` and `secure` as `true`. It will immediately allow you to use Bank Account for purchasing shares using NC ACH API
-   Using `setAsDefault` as `false` will prevent overwrite of Cash-In Bank Account, which will be used for Account Balance flow.
-   Both NC ACH options will work correctly with Legacy API which is used for `baza-nc-web-*` libraries.
-   Both **NBA** usages above **MUST NOT** be used with `static: true` flag, because it will drop previously saved Cash In bank account.
-   Displaying Bank Account Details (Account, Purchase Flow) should be implemented with using `ncAchBankAccount` method instead of using `default('cash-in`)` method. This approach will guarantee that User will see action Bank Account for next Purchase Flow session.
-   Cash-In Bank Account (with Plaid) should be set with **NBA** with exports to `['Dwolla']` only and with `secure` , `setAsDefault` and `static` flags as true . Usage of `static` flag here is optional, but as far we would like to not have multiple bank accounts in nearby future, you should use this flag to clean up bank accounts sometimes.
-   Cash-In Bank Account (with Manual Bank Account) is not a supported case and will not be in focus in nearby future.
-   Cash-Out Bank Account should be set using **NBA** with `['Dwolla']` exports only and with `setAsDefault`, `static` and new `secure` flags enabled. It will always keep single bank account only for Cash Out and it will mask bank account details in Database.
    Reading Cash-Out Bank Account should be implemented using **NBA** with `default('cash-out')` endpoint.
-   Every NBA usage **MUST** be with `secure` flag enabled. A new flag is allowing exporting Bank Account immediately when adding bank account using `add` or `linkOnSuccess` endpoints, but it will not be possible to use `export` method on existing bank accounts later.

### Using this approach, we:

-   Completely removes all sensible data from DB
-   We will primarily use NBA only. LBA usage will be completely unused.
-   We're not using **LBA** in latest UI implementations, but you may use **LBA** only for old UI implementations. **LBA** usage in pair with **NBA** usage will work without conflicts to each other.
-   Two-way sync between LBA and NBA is an opt-in feature now; We may decide to completely remove it, if we will not find it useful
-   Allows you to use Cash In bank account type effectively for both NC ACH and Account Balances scenarios. There will be different accounts set for each operation.
