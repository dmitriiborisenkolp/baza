# Sentry

All environment variables (Sentry DSNs) should be saved in repository.

## Add Sentry projects

You can ask project leads to create Sentry projects. If you have access to Scalio Sentry, you can add it.

1. Go to [sentry.production.scaliolabs.com]() and sign in
2. Create 2 Angular projects: (`${projectName}-cms`, `${projectName}-web`) and 1 Node project (`${projectName}-api`). Copy and save somewhere Sentry DSN of every project - you will need to add it to environment variables.

## Add Sentry integration to API

Add / edit environment variable `SENTRY_DSN` for `test`, `stage` and `production` environments.

```dotenv
SENTRY_DSN=https://${unique-hex-code}@sentry.production.scaliolabs.com/${project-id}
```

## Add Sentry integration to CMS

You should add Sentry configuration for `test`, `stage` and `production` environments. Environment configurations are located in `apps/cms/src/environments` directory:

```typescript
export const environment: Environment = {
    ngEnv: NgEnvironment.Stage,
    enableAngularProduction: true,
    apiEndpoint: 'https://your-api-endpoint',
    sentry: {
        dsn: 'https://172f7d6a40ff4858b0301f590e681fa5@sentry.production.scaliolabs.com/26',
        browserOptions: {
            environment: NgEnvironment.Stage, // << Make sure to update it for every environment
        },
    },
};

```

## Add Sentry integration to WEB

You should add Sentry configuration for `test`, `stage` and `production` environments. Environment configurations are located in `apps/web/src/environments` directory:

```typescript
export const environment: Environment = {
    ngEnv: NgEnvironment.Stage,
    enableAngularProduction: true,
    apiEndpoint: 'https://your-api-endpoint',
    sentry: {
        dsn: 'https://172f7d6a40ff4858b0301f590e681fa5@sentry.production.scaliolabs.com/26',
        browserOptions: {
            environment: NgEnvironment.Stage, // << Make sure to update it for every environment
        },
    },
};

```
