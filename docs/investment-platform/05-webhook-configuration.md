# Webhook configuration

Investment platform requires webhook configuration to properly updates trade statuses, transactions, purchases, offerings and many more.
You should add webhooks in North Capital Client Admin panel. Webhooks for local development/test/stage environments should point to your test instance.

## Steps

1. Sign in to NC Client Admin panel: [https://api-sandboxdash.norcapsecurities.com/admin_v3/client]()
2. Go to Administrative - Webhooks: [https://api-sandboxdash.norcapsecurities.com/admin_v3/client/webhook]()
3. Add following webhooks:
    - createTrade
    - deleteExternalAccount
    - deleteTrade
    - externalFundMove
    - linkExternalAccount
    - updateAccount
    - updateExternalAccount
    - updateKycAml
    - updateLinkExternalAccount
    - updateOffering
    - updateParty
    - updateTrade
    - updateTradeStatus
    - updateTradeTransactionType

Each webhook should points to baza-nc webhook endpoint:

- Example: `https://baza-api.test.scaliolabs.com/north-capital-proxy/webhooks/publish/a453bdafd5390a/updateParty`
- For NC Sandbox account, replace `https://baza-api.test.scaliolabs.com` with API URL for test instance
- For NC Production account, replace `https://baza-api.test.scaliolabs.com` with API URL for production instance
- Place environment value of `BAZA_NORTH_CAPITAL_WEBHOOK_KEY` instead of example `a453bdafd5390a` key
- Replace `updateParty` part with webhook which actual webhook name.
