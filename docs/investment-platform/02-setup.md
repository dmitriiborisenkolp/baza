# Setup new project with Investment Platform

Following guide describe how to set up new project with Investment Platform features.

## Prerequisites

-   Node >= 14
-   PostgreSQL >=11
-   Kafka (latest versions)
-   Redis (latest versions)

## Obtain information about deployment URLs

Ask project leads or devops team which URLs will be used for API, CMS and Web applications for Test, Stage and Production instances.

## North Capital accounts

You need at least 2 North Capital accounts for local development/test environment/stage environment and production environment. Development/test/stage/e2e environments should use
NC Sandbox accounts and production environment should use NC Production account.

Ask team lead or project managers for NC credentials of development account.

# DocuSign

North Capital actively uses DocuSign for purchases. You should a create DocuSign account for new projects, then create and configure DocuSign Templates that will be used for purchases.

After setting up DocuSign account and templates, go to the NC Client Admin Panel, navigate to "DocuSign credentials" page, and fill in your DocuSign account credentials.

## Configure Webhooks

Read more about webhook configuration here: [Webhook Configuration](/investment-platform/05-webhook-configuration)

> ⚠️ Some webhooks have symbols in different cases. Please use only webhook names listed above!

## Obtain Client ID and Developer API key of development North Capital account

Client ID and Developer API key could be already provided by project leds. If not, you'll need to generate it.

1. Sign in to NC Client Admin panel: [https://api-sandboxdash.norcapsecurities.com/admin_v3/client]()
2. Go to Administrative tab. You will be able to obtain Client ID key on this page.
3. You can use any available Developer API key listed at this page. If there are no Developer API keys generated yet, click on "Add New Key" to generate new one.

If you don't see Administrative tab or "Add New Key" button, contact projects leads or North Capital support.

It's recommended to obtain at least 2 different Developer API keys because you will need to use Developer API key for local development reasons. You can store this key
in repository and use different Developer API keys for test / stage environments.

## Obtain NPM token

Investment platform is built on Baza private NPM packages. Ask project leads for any known NPM Tokens if you don't have one.

## Clone baza-skeleton repository

```sh
git clone git@github.com:scalio/baza-skeleton.git
export NPM_TOKEN=${your-npm-token}
yarn
```

## Repository customizations

-   Update `README.MD` and `.github/CODEOWNERS.MD` files
-   Update Project Name, Project Code Name and Client Name in `libs/shared/src/lib/baza-project.ts` file

## Update API environment variables

Environment files for API are located in `envs` directory.

### All environments

-   Update `MAIL_FROM_NAME` and `MAIL_FROM_ADDRESS` variables
-   Update `AWS_*` variables for all environments. Contact project leads for more information. **Do not push `AWS_SECRET_KEY` to repository!**
-   Update `KAFKA_NAMESPACE`, `KAFKA_CLIENT_ID` `KAFKA_CLIENT_GROUP_ID` and `KAFKA_CLIENT_BROKERS` variables - replace `baza` keyword with your project identifier.

### All environments except `e2e` and `e2e-ci`

-   Update `BAZA_NORTH_CAPITAL_CLIENT_ID` variable - all environments except production should use Client ID of NC Sandbox account.
-   Replace `BAZA_NORTH_CAPITAL_WEBHOOK_KEY` with unique generated key. All environments except production should have unique generated key. You should not store `BAZA_NORTH_CAPITAL_WEBHOOK_KEY` for production environment in repository.
-   Make sure that `BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND` is enabled for e2e environments and disabled for all other environments.

### Development (`development.env`)

-   You may need to connect to NC Webhooks bus. Obtain Kafka IP address from devops team and update `KAFKA_CLIENT_BROKERS` variable. You can use local kafka instance, but
    your application will not receive webhooks and events like updating trade statuses will be missed.

### Test (`test.env`)

-   Update `BAZA_APP_API_URL`, `BAZA_APP_CMS_URL` and `BAZA_APP_WEB_URL` environment variables
-   Make sure that `BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND` is enabled

### Stage (`stage.env`), Production (`production.env`)

-   Update `BAZA_APP_API_URL`, `BAZA_APP_CMS_URL` and `BAZA_APP_WEB_URL` environment variables

## Update CMS environment variables

Environment variables for CMS are located in `apps/cms/src/environments` directory.

-   Update `apiEndpoint` for all environments.

## Update WEB environment variables

Environment variables for WEB are located in `apps/web/src/environments` directory.

-   Update `apiEndpoint` for all environments.

## Sentry configuration

It's recommended to configure Sentry integration. Read more at [Sentry Configuration](/investment-platofrm/03-sentry.md) page.

## Commitlint configuration

Update `commitlint.config.js` and replace `BAZA` with JIRA Project ID in `issuePrefixes` config.

```js
module.exports = {
    extends: ['@commitlint/config-conventional'],
    parserPreset: {
        parserOpts: {
            // Replace MYPROJ with actual JIRA ID
            issuePrefixes: ['MYPROJ-'],
        },
    },
    rules: {
        'references-empty': [2, 'never'],
    },
};
```

## Test changes

1. Run API, CMS and Web applications:

```sh
cp envs/development.env envs/.env
docker-compose up
yarn api:start
yarn cms:start
yarn web:start
```

2. Open browser and check API status: [http://localhost:3000]():

```json
{
    "live": "OK",
    "memoryUsageMB": 91,
    "clientIp": "::ffff:127.0.0.1",
    "apiVersion": "1.0.0",
    "supportedApiVersions": ["1.0.0"],
    "gitVersion": "NOT_CONFIGURED",
    "bazaEnvironment": "development",
    "isProductionEnvironment": false,
    "isE2EEnvironment": false
}
```

3. Open API documentation: [http://localhost:3000/redoc]()
4. Open CMS and sign in with `scalio-admin@scal.io` / `Scalio#1337!` credentials: [http://localhost:4200/](). You can find verification code in Mailhog: [http://localhost:8025]()
5. Open Web application: [http://localhost:4201]()

## Test E2E configuration

1. Shutdown API / CMW / Web / Docker-Compose and start Docker-Compose with E2E configuration:

```sh
docker-compose -f docker-compose.e2e-local.yml up
```

2. Start API with E2E configuration:

```sh
yarn api:e2e
```

3. Execute E2E API Tests and make sure it passes:

```sh
yarn api:tests:integration-tests:once
```

4. Execute E2E CMS Tests and make sure it passes:

```sh
yarn cms:e2e
```

5Execute E2E CMS Tests and make sure it passes:

```sh
yarn web:e2e
```

## Deploy changes

-   Create a PR and merge your changes to master.
-   Ask DevOps team to deploy project
