# Features

### 1. **Auth flow**

- Sign In
- Sign Up
- Forgot Password, Reset Password
- Referral Codes
- Invite Codes

### 2. **List of Properties**

*Each property has:*

- Image
- Title
- Price per Share ($)
- Offering Size ($)
- Per Cent funded
- Number of Shares left
- Status (Open, Coming Soon, Closed)
- Add to Favourites

### 3. Property Details

*Property Details page has got:*

- Image
- Title
- Description
- Price per share
- Number of Shares Left
- Per Cent funded

*Financial summary box with:*

- Price per Share, Minimum shares for a purchase, Per cent Funded, Shares Left
- Purchase button

*Need to define:*

- Property statuses and how should it be reflected on the Financial summary box + Purchase button @Christian Becerra
- If the option to subscribe to a Property is needed (Notify Me button when a property is in Coming Soon status) @Christian Becerra

*Need to add:*

- Analysis tab (please write description) ? @Christian Becerra
- Legal tab (please write description) ? @Christian Becerra

### 4. Verification Flow

*Steps of Verification Flow are:*

- Optional step - Info Step (Start/Continue verification)
- Personal Information
    - First Name (does baza have it?)
    - Last Name (does baza have it?)
    - Phone Number (does baza have it?)
    - Residential Address (does baza have it?)
        - Country (does baza have it?)
        - Street Address (does baza have it?)
        - Street Address 2 (optional) (does baza have it?)
        - City (does baza have it?)
        - State (dropdown) (does baza have it?)
        - Zip Code (does baza have it?)
    - Date of Birth (does baza have it?)
    - Social Security Number (does baza have it?)
    - Checkbox “I don’t have a SSN” (does baza have it?)
    - Upload button (for additional document if user doesn’t have a SSN) (does baza have it?)
    - Citizenship (dropdown) (does baza have it?)
- Optional step - Additional Document (upload a document: png, jpeg, pdf only)
- Investor Profile
    - Are you an accredited investor (yes, no) (does baza have it?)
    - Are you or anyone in your household associated with a FINRA member, organization or the SEC? (yes, no) (does baza have it?)
    - What is your current annual household income (including your spouse) (does baza have it?)
    - What is your net worth (excluding primary residence) (does baza have it?)

### 5. Purchase Flow

*Steps of Purchase Flow are:*

- Purchase Details (with and option to Edit Investor Account)
- Sign Agreement (DocuSign implementation)
- Submit Payment (Add/edit payment method through Plaid or manually). ⚠️ See [Payment Method](https://www.notion.so/List-of-features-Baza-vs-iReam-46affb643ea24897b59f1e56862ca9a5) section warning, that part is not implemented in Baza.
- Success screen
