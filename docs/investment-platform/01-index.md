# Investment Platform

Investment Platform is base skeleton project for investment projects which are using North Capital API. Investment Platform includes minimal API, CMS and Web application which could be a starting point
for new investment project.

Read more about Investment Platform feature here: [/investment-platform/04-features.md]()
