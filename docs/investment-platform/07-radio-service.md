# Radio Service

-   We are using some of our test instances (for Baza it's [baza-api.test.scaliolabs.com](http://baza-api.test.scaliolabs.com/) as some sort of Radio Station.
-   Radio station instance should be configured to publish events from webhook endpoint to Kafka (`BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND=1`)
-   NC calls Webhook endpoint to your radio stating instance with endpoint as configured on above webhook section, and API sends events to Kafka through Event Bus -> Every Node pipeline (usual instances uses Multi-Node pipeline)
-   Every other instance (stage, development instances, etc) are subscribed to Kafka bus and receives events from NC -> Webhook -> Kafka and re-publish them via Baza EventBus / CQRS layers

The webhook key is also used as a key to publishing events to individual clients via Radio Service. These events are then caught by individual instances
and further re-published from Kafka to CQRS events / Baza event bus.

For development purposes make sure that you have test instance with `BAZA_NORTH_CAPITAL_WEBHOOK_KAFKA_SEND` environment variable enabled. Developers will be
able to connect to Kafka instances of Test environment and catch / debug projects with NC webhooks properly.
