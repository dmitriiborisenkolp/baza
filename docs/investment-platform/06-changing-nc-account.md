# Changing NC Account

In order to change your NC account, you should:

1. Create Developer API Key in NC Client Panel
2. Update DocuSign credentials in NC Client Panel
3. Setup webhooks: [Webhook Configuration](/investment-platform/05-webhook-configuration)
4. Update `BAZA_NORTH_CAPITAL_CLIENT_ID` and `BAZA_NORTH_CAPITAL_DEVELOPER_API_KEY` environment variables
