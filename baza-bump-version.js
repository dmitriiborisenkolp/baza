const fs = require('fs');

const newVersion = process.argv[2];

const packageJSONs = [
    [`${__dirname}/package.json`, []],
    [`${__dirname}/libs/baza-core-api/package.json`, ['@scaliolabs/baza-core-shared']],
    [
        `${__dirname}/libs/baza-core-cms/package.json`,
        ['@scaliolabs/baza-core-ng', '@scaliolabs/baza-core-data-access', '@scaliolabs/baza-core-shared'],
    ],
    [`${__dirname}/libs/baza-core-cms-data-access/package.json`, ['@scaliolabs/baza-core-shared']],
    [`${__dirname}/libs/baza-core-data-access/package.json`, ['@scaliolabs/baza-core-shared']],
    [
        `${__dirname}/libs/baza-core-ng/package.json`,
        ['@scaliolabs/baza-core-shared', '@scaliolabs/baza-core-data-access', '@scaliolabs/baza-core-devops'],
    ],
    [`${__dirname}/libs/baza-core-shared/package.json`, []],
    [
        `${__dirname}/libs/baza-core-web/package.json`,
        ['@scaliolabs/baza-core-ng', '@scaliolabs/baza-core-data-access', '@scaliolabs/baza-core-shared'],
    ],
    [`${__dirname}/libs/baza-core-devops/package.json`, []],
    [`${__dirname}/libs/baza-nc-shared/package.json`, ['@scaliolabs/baza-core-shared']],
    [`${__dirname}/libs/baza-nc-cms/package.json`, ['@scaliolabs/baza-core-cms']],
    [`${__dirname}/libs/baza-nc-cms-data-access/package.json`, ['@scaliolabs/baza-core-data-access']],
    [`${__dirname}/libs/baza-nc-data-access/package.json`, ['@scaliolabs/baza-core-data-access']],
    [`${__dirname}/libs/baza-nc-api/package.json`, ['@scaliolabs/baza-core-api']],
    [`${__dirname}/libs/baza-content-types-api/package.json`, ['@scaliolabs/baza-core-api']],
    [`${__dirname}/libs/baza-content-types-cms/package.json`, ['@scaliolabs/baza-core-cms']],
    [`${__dirname}/libs/baza-content-types-data-access/package.json`, ['@scaliolabs/baza-core-data-access']],
    [`${__dirname}/libs/baza-content-types-node-access/package.json`, ['@scaliolabs/baza-content-types-shared']],
    [`${__dirname}/libs/baza-content-types-shared/package.json`, ['@scaliolabs/baza-core-shared']],
    [`${__dirname}/libs/baza-nc-integration-shared/package.json`, ['@scaliolabs/baza-nc-shared', '@scaliolabs/baza-content-types-shared']],
    [
        `${__dirname}/libs/baza-nc-integration-cms-data-access/package.json`,
        ['@scaliolabs/baza-nc-integration-shared', '@scaliolabs/baza-content-types-data-access'],
    ],
    [
        `${__dirname}/libs/baza-nc-integration-data-access/package.json`,
        ['@scaliolabs/baza-nc-integration-shared', '@scaliolabs/baza-content-types-data-access'],
    ],
    [
        `${__dirname}/libs/baza-nc-integration-api/package.json`,
        ['@scaliolabs/baza-nc-integration-shared', '@scaliolabs/baza-content-types-api'],
    ],
    [
        `${__dirname}/libs/baza-nc-integration-cms/package.json`,
        [
            '@scaliolabs/baza-nc-integration-shared',
            '@scaliolabs/baza-nc-integration-cms-data-access',
            '@scaliolabs/baza-nc-integration-data-access',
            '@scaliolabs/baza-content-types-cms',
        ],
    ],
    [
        `${__dirname}/libs/baza-nc-web-verification-flow/package.json`,
        ['@scaliolabs/baza-web-utils', '@scaliolabs/baza-core-data-access', '@scaliolabs/baza-core-shared', '@scaliolabs/baza-nc-shared'],
    ],
    [
        `${__dirname}/libs/baza-nc-web-purchase-flow/package.json`,
        [
            '@scaliolabs/baza-nc-integration-cms-data-access',
            '@scaliolabs/baza-nc-integration-data-access',
            '@scaliolabs/baza-nc-shared',
            '@scaliolabs/baza-web-utils',
        ],
    ],
    [`${__dirname}/libs/baza-web-ui-components/package.json`, ['@scaliolabs/baza-web-utils']],
    [`${__dirname}/libs/baza-web-utils/package.json`, []],
    [`${__dirname}/libs/baza-dwolla-shared/package.json`, []],
    [`${__dirname}/libs/baza-dwolla-node-access/package.json`, ['@scaliolabs/baza-dwolla-shared']],
    [`${__dirname}/libs/baza-dwolla-cms-data-access/package.json`, ['@scaliolabs/baza-dwolla-shared']],
    [`${__dirname}/libs/baza-dwolla-data-access/package.json`, ['@scaliolabs/baza-dwolla-shared']],
    [`${__dirname}/libs/baza-dwolla-api/package.json`, ['@scaliolabs/baza-dwolla-shared', '@scaliolabs/baza-dwolla-node-access']],
    [
        `${__dirname}/libs/baza-dwolla-cms/package.json`,
        ['@scaliolabs/baza-dwolla-shared', '@scaliolabs/baza-dwolla-api', '@scaliolabs/baza-dwolla-cms-data-access'],
    ],
    [`${__dirname}/libs/baza-core-node-access/package.json`, ['@scaliolabs/baza-core-shared']],
    [`${__dirname}/libs/baza-nc-node-access/package.json`, ['@scaliolabs/baza-core-node-access']],
    [`${__dirname}/libs/baza-nc-integration-node-access/package.json`, ['@scaliolabs/baza-core-node-access']],
    [`${__dirname}/libs/baza-plaid-shared/package.json`, []],
    [`${__dirname}/libs/baza-plaid-api/package.json`, ['@scaliolabs/baza-plaid-shared']],
    [
        `${__dirname}/libs/baza-dwolla-web-purchase-flow/package.json`,
        [
            '@scaliolabs/baza-plaid-shared',
            '@scaliolabs/baza-nc-shared',
            '@scaliolabs/baza-nc-data-access',
            '@scaliolabs/baza-nc-cms-data-access',
        ],
    ],
    [
        `${__dirname}/libs/baza-dwolla-web-verification-flow/package.json`,
        [
            '@scaliolabs/baza-plaid-shared',
            '@scaliolabs/baza-nc-shared',
            '@scaliolabs/baza-nc-data-access',
            '@scaliolabs/baza-nc-cms-data-access',
        ],
    ],
    [`${__dirname}/libs/baza-plaid-api/package.json`, ['@scaliolabs/baza-plaid-shared']],
];

for (const command of packageJSONs) {
    const filePath = command[0];
    const peers = command[1];

    const json = JSON.parse(fs.readFileSync(filePath, 'utf8'));

    json['version'] = newVersion;

    if (!json['peerDependencies'] || Array.isArray(json['peerDependencies'])) {
        json['peerDependencies'] = {};
    }

    for (const peer of peers) {
        json['peerDependencies'][peer] = newVersion;
    }

    fs.writeFileSync(filePath, JSON.stringify(json, null, 2));
}
