import { Injectable } from '@nestjs/common';
import { BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaNcIntegrationSchemaRepository } from '@scaliolabs/baza-nc-integration-api';

/**
 * Set Sort Order field for all Schema
 * Required to make Sort Order feature works correctly in Schema CMS
 */
@Injectable()
export class BazaNcIntegrationSchemaSortOrderMigration008 implements BazaMigration {
    constructor(private readonly repository: BazaNcIntegrationSchemaRepository) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(): Promise<void> {
        let next = 1;
        const entities = await this.repository.findAll();

        for (const entity of entities) {
            entity.sortOrder = next;

            await this.repository.save([entity]);

            next++;
        }
    }
}
