import { Injectable } from '@nestjs/common';
import { BazaLogger, BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments, maskString } from '@scaliolabs/baza-core-shared';
import { QueryRunner } from 'typeorm';
import { BazaNcBankAccountEntity, BazaNcBankAccountRepository } from '@scaliolabs/baza-nc-api';

let bankAccountRepository: BazaNcBankAccountRepository;

@Injectable()
export class BazaNcBankAccountSecureMigration005 implements BazaMigration {
    constructor(private readonly logger: BazaLogger, readonly injectedBankAccountRepository: BazaNcBankAccountRepository) {
        bankAccountRepository = injectedBankAccountRepository;
    }

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.connection.transaction('READ COMMITTED', this.performMigration());
    }

    private performMigration(): () => Promise<void> {
        return async () => {
            const ulids: Array<string> = await (async () => {
                const qb = await bankAccountRepository.repository.createQueryBuilder().select(['ulid']).getRawMany();

                return (qb || []).map((next: Pick<BazaNcBankAccountEntity, 'ulid'>) => next.ulid);
            })();

            for (const ulid of ulids) {
                const entity = await bankAccountRepository.findByUlid(ulid);

                if (ulid) {
                    entity.secure = true;
                    entity.accountNumber = maskString(entity.accountNumber);
                    entity.accountRoutingNumber = maskString(entity.accountRoutingNumber);

                    await bankAccountRepository.save([entity]);

                    this.logger.debug(`Secured Bank Account "${entity.ulid}" (${entity.accountName})`);
                }
            }
        };
    }
}
