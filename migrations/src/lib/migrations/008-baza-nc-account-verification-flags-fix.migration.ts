import { Injectable } from '@nestjs/common';
import { BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaNcAccountVerificationAccountSetupService, BazaNcInvestorAccountRepository } from '@scaliolabs/baza-nc-api';

/**
 * This migration updates `isAccountVerificationCompleted` and `isAccountVerificationInProgress` flags for all
 * Investor Accounts
 */
@Injectable()
export class BazaNcAccountVerificationFlagsFixMigration implements BazaMigration {
    constructor(
        private readonly repository: BazaNcInvestorAccountRepository,
        private readonly service: BazaNcAccountVerificationAccountSetupService,
    ) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(): Promise<void> {
        const ids = await this.repository.findAllInvestorAccountIds();

        for (const id of ids) {
            const investorAccount = await this.repository.getInvestorAccountById(id);

            await this.service.updateAccountVerificationInProgressAndCompletedFlags(investorAccount.user);
        }
    }
}
