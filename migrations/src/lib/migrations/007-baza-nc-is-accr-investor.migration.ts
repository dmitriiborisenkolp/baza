import { Injectable } from '@nestjs/common';
import { BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaNcInvestorAccountRepository, InvestorAccountSyncService } from '@scaliolabs/baza-nc-api';

@Injectable()
export class BazaNcIsAccrInvestorMigration007 implements BazaMigration {
    constructor(
        private readonly investorAccountRepository: BazaNcInvestorAccountRepository,
        private readonly investorAccountSync: InvestorAccountSyncService,
    ) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(): Promise<void> {
        const ids = await this.investorAccountRepository.findAllInvestorAccountIds();

        for (const investorAccountId of ids) {
            const investorAccount = await this.investorAccountRepository.getInvestorAccountById(investorAccountId);

            if (investorAccount.northCapitalAccountId && investorAccount.northCapitalPartyId) {
                await this.investorAccountSync.updateInvestorAccount(investorAccount.user, {
                    userId: investorAccount.userId,
                    ncPartyId: investorAccount.northCapitalPartyId,
                    ncAccountId: investorAccount.northCapitalAccountId,
                });
            }
        }
    }
}
