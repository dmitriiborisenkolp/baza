import { Injectable } from '@nestjs/common';
import { BazaAccountCmsService, BazaLogger, BazaMigration, BazaMigrationRunMode } from '@scaliolabs/baza-core-api';
import { BazaEnvironments } from '@scaliolabs/baza-core-shared';
import { BazaDwollaPaymentRepository, BazaDwollaPaymentService } from '@scaliolabs/baza-dwolla-api';
import { BazaDwollaPaymentPurchaseSharesPayload } from '@scaliolabs/baza-dwolla-shared';

@Injectable()
export class BazaNcDwollaPaymentNcTradeIdMigration006 implements BazaMigration {
    constructor(
        private readonly logger: BazaLogger,
        private readonly bazaAccountCmsService: BazaAccountCmsService,
        private readonly bazaDwollaPaymentService: BazaDwollaPaymentService,
        private readonly bazaDwollaPaymentRepository: BazaDwollaPaymentRepository,
    ) {}

    forEnvs(): Array<BazaEnvironments> {
        return Object.values(BazaEnvironments);
    }

    runMode(): BazaMigrationRunMode {
        return BazaMigrationRunMode.Once;
    }

    async up(): Promise<void> {
        const accounts = (await this.bazaAccountCmsService.listAccounts({ size: 0, includeFields: ['id'] })).items.map((item) => item.id);

        for (const id of accounts) {
            const dwollaPayments = (await this.bazaDwollaPaymentService.list(id, { size: 0 })).items;

            for (const payment of dwollaPayments) {
                const payload = payment.payload as BazaDwollaPaymentPurchaseSharesPayload;

                if (payload.ncTradeId) {
                    const entity = await this.bazaDwollaPaymentRepository.findByUlid(payment.ulid);

                    if (entity) {
                        entity.ncTradeId = payload.ncTradeId;

                        await this.bazaDwollaPaymentRepository.save(entity);
                    }
                }
            }
        }
    }

    down(): Promise<void> {
        return Promise.resolve(undefined);
    }
}
