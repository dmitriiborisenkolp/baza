import { Global, Module, OnModuleInit } from '@nestjs/common';
import { BazaMigrationsModule, BazaMigrationsService } from '@scaliolabs/baza-core-api';
import { ModuleRef } from '@nestjs/core';
import { BazaNcApiBundleModule, BazaNcDwollaApiModule, BazaNcSyncApiModule } from '@scaliolabs/baza-nc-api';
import { BazaNcBankAccountSecureMigration005 } from './migrations/005-baza-nc-bank-account-secure.migration';
import { BazaNcDwollaPaymentNcTradeIdMigration006 } from './migrations/006-baza-nc-dwolla-payment-nctradeid.migration';
import { BazaNcIsAccrInvestorMigration007 } from './migrations/007-baza-nc-is-accr-investor.migration';
import { BazaNcAccountVerificationFlagsFixMigration } from './migrations/008-baza-nc-account-verification-flags-fix.migration';
import { BazaNcIntegrationSchemaSortOrderMigration008 } from './migrations/008-baza-nc-integration-schema-sort-order.migration';

export const BAZA_API_MIGRATIONS = [
    BazaNcBankAccountSecureMigration005,
    BazaNcDwollaPaymentNcTradeIdMigration006,
    BazaNcIsAccrInvestorMigration007,
    BazaNcAccountVerificationFlagsFixMigration,
    BazaNcIntegrationSchemaSortOrderMigration008,
];

@Global()
@Module({
    imports: [
        BazaMigrationsModule.forRootASync({
            injects: [],
            useFactory: async () => ({
                migrations: BAZA_API_MIGRATIONS,
            }),
        }),
        BazaNcDwollaApiModule,
        BazaNcApiBundleModule,
        BazaNcSyncApiModule,
    ],
    providers: [...BAZA_API_MIGRATIONS],
    exports: [...BAZA_API_MIGRATIONS],
})
export class MigrationModule implements OnModuleInit {
    constructor(private moduleRef: ModuleRef, private readonly service: BazaMigrationsService) {}

    onModuleInit(): void {
        this.service.migrations = BAZA_API_MIGRATIONS.map((migration) => this.moduleRef.get(migration as any));
    }
}
